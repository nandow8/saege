<?php
 
//die('Estamos em manuten&ccedil;&atilde;o. Previs&atilde;o de retorno: 30/09 as 06:00');
//set_time_limit(0);

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));
    
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__)));    

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'development'));


define('HTTP_URL', 'https://saege.com.br');
define('HTTPS_URL', 'https://saege.com.br');

// Ensure library/ is on include_path

set_include_path(implode(PATH_SEPARATOR, array(
	realpath(dirname(__FILE__)) . '/library/',
	realpath(dirname(__FILE__)) . '/library/Zend',
	realpath(dirname(__FILE__)) . '/library/tcpdf',
	realpath(dirname(__FILE__)) . '/library/PHPExcel',
	//realpath(dirname(__FILE__)) . '/library/google-api-php-client-master/src/,
	realpath(APPLICATION_PATH . '/modules/admin/models'),
	realpath(APPLICATION_PATH . '/modules/default/models'),
    realpath(APPLICATION_PATH . '/modules/default/models/db'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

require_once realpath(dirname(__FILE__)) . '/library/vendor/autoload.php';

require_once realpath(dirname(__FILE__)) . '/library/PHPExcel/Classes/PHPExcel.php';
require realpath(dirname(__FILE__)) . '/library/PHPExcel/Classes/PHPExcel/Shared/ZipArchive.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

// Lê arquivo local com as configurações do usuário e senha.
$localConfigFile = APPLICATION_PATH.'/configs/local.ini';
if(is_file($localConfigFile)) {
    $config = new Zend_Config($application->getOptions(), true);
    $local  = new Zend_Config_Ini($localConfigFile);
    $config->merge($local);
    $application->setOptions($config->toArray());
}

// Initialize and retrieve DB resource
$bootstrap = $application->getBootstrap();
$bootstrap->bootstrap('db');
$dbAdapter = $bootstrap->getResource('db');

Zend_Registry::set('db', $dbAdapter);
Zend_Db_Table::setDefaultAdapter($dbAdapter);

Zend_Loader_Autoloader::getInstance()->setFallbackAutoloader(true);

error_reporting(E_ALL ^ E_DEPRECATED);

$application->bootstrap()
            ->run();