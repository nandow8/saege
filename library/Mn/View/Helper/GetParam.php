<?php

class Mn_View_Helper_GetParam  {
	
	public function getParam($param) {
		$front = Zend_Controller_Front::getInstance();
		return $front->getRequest()->getParam($param);		
	}	

}