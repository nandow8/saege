<?php

class Mn_View_Helper_GetImagem {
	
	function getImagem($idimagem, $w=0, $h=0, $alt="", $i = 'usuario', $ext = '') {
		$front = Zend_Controller_Front::getInstance();
		$baseUrl = $front->getBaseUrl();
		
		$sw = "";
		if ($w>0) $sw = "/w/$w";
		
		$sh = "";
		if ($h>0) $sh = "/h/$h";
		
		$img = '<img src="'.$baseUrl.'/admin/imagens/get/id/'.($idimagem).$sw.$sh.'/i/'.$i. $ext .'"  alt="'.$alt.'" />';		
		return $img;
	}
	
}