<?php

class Mn_View_Helper_FormatTextoBranco {
	
	public function formatTextoBranco($texto, $replace = '--') {
		if (trim($texto=='')) {
			return $replace;
		}
		return $texto;
	}

}