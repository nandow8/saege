<?php

class Mn_View_Helper_FormatNomerazao {
	
	public function formatNomerazao($row, $naoEncontrado = false) {
		
		if ((!$row) && ($naoEncontrado)) {
			return $naoEncontrado;
		}
		
		$res = '';
		if ($row['pfpj']=='pf') $res = $row['nomerazao'] . ' ' . $row['sobrenomefantasia'];
		else {
			$res = $row['nomerazao'];// . ' (' . $row['sobrenomefantasia'] .')';	
		}
		return $res;		
	}

}