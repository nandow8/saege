<?php

class Mn_View_Helper_UrlAtual {

	function urlAtual($only_params = false) {
		$protocolo = (strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === false) ? 'http' : 'https';
		$dominio = $_SERVER['HTTP_HOST'];
		$UrlAtual = $protocolo. "://" . $dominio. $_SERVER['REQUEST_URI'];
		 
		if (!$only_params) return $UrlAtual;
		
		$front = Zend_Controller_Front::getInstance();
		$baseUrl = $front->getBaseUrl();
		$param = str_replace($baseUrl, "", $_SERVER['REQUEST_URI']);
		
		return $param;
	}
	
}
