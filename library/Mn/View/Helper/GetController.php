<?php

class Mn_View_Helper_GetController  {
	
	public function getController() {
		$front = Zend_Controller_Front::getInstance();
		return $front->getRequest()->getControllerName();		
	}	

}