<?php

class Mn_View_Helper_ExcludeP {
	
	public function excludeP($e, $converteLrlf = true) {
		if (substr($e, 0, 3)=="<p>") {
		  $e = substr($e, 3, strlen($e)-7);
		}		  
		if ($converteLrlf) $e = str_replace(chr(13).chr(10), '', $e);
		return $e;
	}

}

?>