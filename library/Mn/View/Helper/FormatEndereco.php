<?php

class Mn_View_Helper_FormatEndereco {
	
	public function formatEndereco($row, $reduzido = false, $bairro=true, $complemento=true, $cep = true, $ce_sep = ', ', $ns = '') {
		if (!$row) return '--';
		$enderecos = array();
		
		if ($row[$ns.'endereco']!='') array_push($enderecos, $row[$ns.'endereco']);
		if ($row[$ns.'numero']!='') array_push($enderecos, 'N&#176;. ' . $row[$ns.'numero']);
		if ($complemento) if ($row[$ns.'complemento']!='') array_push($enderecos, $row[$ns.'complemento']);
		if (!$reduzido) {
			if ($bairro) if ($row[$ns.'bairro']!='') array_push($enderecos, 'Bairro: ' . $row[$ns.'bairro']);
			if ($row[$ns.'cidade']!='') array_push($enderecos, $row[$ns.'cidade']);
			if ($row[$ns.'estado']!='') array_push($enderecos, $row[$ns.'estado']);
		}
		if ($cep) if ($row[$ns.'cep']!='') array_push($enderecos, 'CEP: ' . $row[$ns.'cep']);

		$res = '';
		$old_k = '';
		foreach ($enderecos as $k=>$v) {
			if ($k>0) {
				if (($old_k=='cidade') && ($k=='estado')) $res .=  $ce_sep;
				else $res .= ', ';
			}
			$res .= $v;
			$old_k = $k;
		}
		
		$res = trim($res);
		
		return ($res!='') ? $res : '--';
	}

}