<?php

class Mn_View_Helper_CortaPalavra {
	
	public function cortaPalavra($len, $palavra) {
		$palavra = trim($palavra);
		$palavras = explode(" ", $palavra);
		$frase = "";
		foreach ($palavras as $i=>$p) {
			if (strlen($frase)+strlen($p)>$len) break;
			$frase = $frase . $p . " ";
		}
		if (strlen($frase)<strlen($palavra)) $frase = $frase . "...";
		return trim($frase);
	}

}
