<?php

class Mn_View_Helper_ComparativoCor {
	
	public function comparativoCor($rows, $k, $field) {
		$prox = isset($rows[$k+1]) ? $rows[$k+1] : false;
		if (!$prox) return;
		
		$atual = $rows[$k];
		
		$val_atual = $atual[$field];
		$val_prox = $prox[$field];
		
		if ($val_atual>=$val_prox) {
			return 'style="color:#080!Important"';
		} else {
			return 'style="color:#800!Important"';
		}
		
	}

}
