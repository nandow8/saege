<?php

class Mn_View_Helper_BreadCrumbPastas {
	
	public function breadCrumbPastas($id, $baseurl, $_this, $ordem, $controller, $orders = array()) {
		if ($id==0) return false;

		$rows = new Pastasvirtuais();
		$row = $rows->getPastavirtualById($id);

		if((isset($row['id'])) && (isset($row['idparent'])) && ($row['idparent'] > 0)):
			if($ordem==0){
				$orders[$ordem] = '<li class="">'.$row['titulo'].'</li>';
			}else{
				$orders[$ordem] = '<li class=""><a class="" href="'.$baseurl.'admin/'.$controller.'/editar/id/'.$row['id'].'">'.$row['titulo'].'</a>></li>';	
			}

			$ordem = (int)$ordem + 1;
			return $_this->BreadCrumbPastas($row['idparent'], $baseurl, $_this, $ordem, $controller, $orders);
		elseif((isset($row['id'])) && ($row['idparent'] == 0)):		
			if($ordem==0){
				$orders[$ordem] = '<li class="">'.$row['titulo'].'</li>';			
			}else{
				$orders[$ordem] = '<li class=""><a class="" href="'.$baseurl.'admin/'.$controller.'/editar/id/'.$row['id'].'">'.$row['titulo'].'</a>></li>';			
			}
			
			$final_breadcrumb = "";
			for($i=(int)sizeof($orders); $i--; $i==0){
				$final_breadcrumb .= $orders[$i];
				
			}
			return $final_breadcrumb;
		endif;

	}

}
