<?php

class Mn_View_Helper_IsMinimize {
	
	public function isMinimize() {
		$ns = new Zend_Session_Namespace('minimize');
		if (!isset($ns->val)) return false;
		
		return ($ns->val=='nao');		

	}

}
