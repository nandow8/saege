<?php

/**
 * View_Helper para desenhar o breadcrumb no painel
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Mn_View_Helper_FormatCidadeUf {
	
	/**
	 * Format o objeto Cidade de acordo com a especificação {cidade} - {uf}
	 * @param string $obj
	 */
	public function formatCidadeUf($obj) {
		if (!$obj) return "";
		
		if (is_array($obj)) $row = $obj;
		else if ((int)$obj>0) {
			$idcidade = (int)$obj;
			
			$cidades = new Cidades();
			$row = $cidades->fetchRow("id=$idcidade");
			if (!$row) return "";
			$row = $row->toArray();
		}
		
		$cidade = (array_key_exists('cidade', $row)) ? $row['cidade'] : $row['nome'];
		return $cidade . ' - ' . $row['uf'];
	} 
	
	
}