<?php

class Mn_View_Helper_GetArquivo {
	
	function getArquivo($idarquivo, $label = "Download do Arquivo", $color = "#1D8EC7") {
		$front = Zend_Controller_Front::getInstance();
		$baseUrl = $front->getBaseUrl();
		
		$a = '<a style="color: '.$color.'" href="'.$baseUrl.'/admin/arquivos/get/id/'.$idarquivo.'" target="_blank">'.$label.'</a>';		
		return $a;
	}
	
}