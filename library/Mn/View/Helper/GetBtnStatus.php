<?php

class Mn_View_Helper_GetBtnStatus {
	
	public function getBtnStatus($status, $row, $action) {
		$_status = Pedidos::getStatus($status);
		
		if ($status==Pedidos::$_STATUS_PENDENTE) {
			$url = $action->baseUrl() . '/admin/pedidos/editar/id/' . $row['id'];
		} else if ($status==Pedidos::$_STATUS_PROCESSANDO) {
			$url = $action->baseUrl() . '/admin/pedidosprocessando/visualizar/id/' . $row['id']; 
		} else if ($status==Pedidos::$_STATUS_FINALIZADO) {
			$url = $action->baseUrl() . '/admin/pedidosfinalizados/visualizar/id/' . $row['id'];
		}
		
		
		
		if ($row['exclusao']=='sim') {
			return '<button style="cursor:default;" class="btn '.$_status['classe'].'-alt btn-xs">'.$_status['key'].'</button>';
		} else {
			return '<button onclick="location.href=\''.$url.'\'" style="cursor:pointer" class="btn '.$_status['classe'].' btn-xs">'.$_status['key'].'</button>';
		}
		
	}

}
