<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;



class Mn_Util {
	
	public static function getDayOfWeekMondayFirst($date = false) {
		if (!$date) {
			$date = mktime(0,0,0,date('m'), date('d'), date('Y'));
		}
		
		$weekday = (date('w',$date));
		if ($weekday==0) $weekday = 7; //domingo
		
		return $weekday;
		
	}
	
	public static function getRel($exp1, $exp2) {
		if ($exp2==0) {
			$rel = 0;
		} else {
			$rel =  1-($exp1/$exp2);
		}
		$rel = -1*$rel;
		$rel = $rel*100;
		
		$style = "";
		if ($rel>100) {
			$style = 'style="color:#080!Important"';
		} else if ($rel==0) {
			$style = 'style="color:#008!Important"';
		} else {
			$style = 'style="color:#800!Important"';
		}
		
		$span = '<span '.$style.'>' . number_format($rel, 2, ',', '.') . '%</span>';
		return $span;
	}
	
	public static function getDiaSemana($dia) {
		
		$days = array('dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sab', 'dom');
		
		return $days[$dia];
		
	}
	
	/**
	 * 
	 * @param unknown $origins
	 * @param unknown $destinations
	 * @param string $arr_distance para retorno do array de distancia e duracao em metros e segundos, para kilometros, divida a distancia/1000 e arredonde para cima
	 * @return boolean|duracao|array de distância e duração
	 */
	public static function getGoogleDistance($origins, $destinations, $arr_distance = false) {
		//Mn_Util::getGoogleDistance("Rua Alano raizer, 41 - Campinas/SP", "Barão de Itapura, 2137");
		
		$lang = "pt-BR";
		$mode = "walking";
		
		$origins = urlencode($origins);
		$destinations = urlencode($destinations);
		
		$params = "origins=$origins&destinations=$destinations&mode=$mode&language=$lang&sensor=false";
		$url = "http://maps.googleapis.com/maps/api/distancematrix/json?$params";
		
		$json = file_get_contents($url);
		$obj = json_decode($json);		
		
		if (!isset($obj->rows)) return false;
		$rows = $obj->rows;
		
		if (!isset($rows[0])) return false;
		$arr1 = $rows[0];
		
		if (!isset($arr1->elements)) return false;
		$elements = $arr1->elements;		
		
		if (!isset($elements[0])) return false;
		$arr1 = $elements[0];		
		
		if ($arr1->status=='NOT_FOUND') {
			return false;
		}
		
		if ($arr_distance) {
			return array(
				'distance'=>$arr1->distance->value, //em metros
				'duration'=>(int)ceil($arr1->duration->value * 1.15) //em segundos
				// Valor de duration + 15% utilizando ceil para arredondamento do valor + force to int
			);
		}
		
		$value = (int)$arr1->duration->value; //em segundos
		
		return (int)ceil($value * 1.15);
		// Valor de duration + 15% utilizando ceil para arredondamento do valor + force to int
	}
	
	public static function gerar_senha($tamanho, $maiuscula, $minuscula, $numeros, $codigos) {
		$maius = "ABCDEFGHIJKLMNOPQRSTUWXYZ";
		$minus = "abcdefghijklmnopqrstuwxyz";
		$numer = "0123456789";
		$codig = '!@#$%&*()-+.,;?{[}]^><:|';

		$base = '';
		$base .= ($maiuscula) ? $maius : '';
		$base .= ($minuscula) ? $minus : '';
		$base .= ($numeros) ? $numer : '';
		$base .= ($codigos) ? $codig : '';

		srand((float) microtime() * 10000000);
		$senha = '';
		for ($i = 0; $i < $tamanho; $i++) {
			$senha .= substr($base, rand(0, strlen($base)-1), 1);
		}
		return $senha;
	}	
	
	public static function getPaeSysTag($tag) {
		return '[onload.'.$tag.';strconv=no]';
	}
	
	public static function corrigeIdsPlantas($queries) {
		if (isset($queries['idsplantas'])) {
			$idsplantas = trim($queries['idsplantas']);
			$idsplantas = explode(",", $idsplantas);
			
			if (!is_array($idsplantas)) $idsplantas = array();
			array_push($idsplantas, "0");
			$idsplantas = implode(",", $idsplantas);	
			$queries['idsplantas'] = $idsplantas;	
		}		
		return $queries;
	}
	
	public static function getMeses($field = false, $red = false) {
		$d = array(
			1 => 'Janeiro',
			2 => 'Fevereiro',
			3 => 'Março',
			4 => 'Abril',
			5 => 'Maio',
			6 => 'Junho',
			7 => 'Julho',
			8 => 'Agosto',
			9 => 'Setembro',
			10 => 'Outubro',
			11 => 'Novembro',
			12 => 'Dezembro'
		);
		
		if (!$field) return $d;
		
		if ($red) {
			$f = $d[$field];
			return substr($f,0,3);
		}
		
		return $d[$field];
	}
	
	public static function getProgressoCor($p) {
		if ($p>99) $p=99;
		$degrade = array();
		$degrade[0]="#E20302";
		$degrade[1]="#E20503";
		$degrade[2]="#E20803";
		$degrade[3]="#E30B02";
		$degrade[4]="#E40D03";
		$degrade[5]="#E51103";
		$degrade[6]="#E61503";
		$degrade[7]="#E71802";
		$degrade[8]="#E81C02";
		$degrade[9]="#E92002";
		$degrade[10]="#EA2402";
		$degrade[11]="#EC2902";
		$degrade[12]="#ED2E02";
		$degrade[13]="#EF3202";
		$degrade[14]="#F03702";
		$degrade[15]="#F23C02";
		$degrade[16]="#F34102";
		$degrade[17]="#F44602";
		$degrade[18]="#F64C02";
		$degrade[19]="#F85101";
		$degrade[20]="#F95702";
		$degrade[21]="#FB5C02";
		$degrade[22]="#FC6101";
		$degrade[23]="#FD6701";
		$degrade[24]="#FF6D02";
		$degrade[25]="#FF7201";
		$degrade[26]="#FF7801";
		$degrade[27]="#FF7D01";
		$degrade[28]="#FF8201";
		$degrade[29]="#FF8801";
		$degrade[30]="#FF8E01";
		$degrade[31]="#FF9301";
		$degrade[32]="#FF9801";
		$degrade[33]="#FF9D01";
		$degrade[34]="#FFA200";
		$degrade[35]="#FFA701";
		$degrade[36]="#FFAC01";
		$degrade[37]="#FFB000";
		$degrade[38]="#FFB400";
		$degrade[39]="#FFB900";
		$degrade[40]="#FFBC00";
		$degrade[41]="#FFC001";
		$degrade[42]="#FFC401";
		$degrade[43]="#FFC801";
		$degrade[44]="#FFCA00";
		$degrade[45]="#FFCE00";
		$degrade[46]="#FFD000";
		$degrade[47]="#FFD300";
		$degrade[48]="#FFD500";
		$degrade[49]="#FFD600";
		$degrade[50]="#FFD800";
		$degrade[51]="#FDD800";
		$degrade[52]="#F9D800";
		$degrade[53]="#F7D800";
		$degrade[54]="#F3D800";
		$degrade[55]="#EFD800";
		$degrade[56]="#ECD800";
		$degrade[57]="#E8D800";
		$degrade[58]="#E3D800";
		$degrade[59]="#DED800";
		$degrade[60]="#DAD800";
		$degrade[61]="#D6D800";
		$degrade[62]="#CFD700";
		$degrade[63]="#CBD600";
		$degrade[64]="#C5D400";
		$degrade[65]="#BFD300";
		$degrade[66]="#B9D100";
		$degrade[67]="#B3CF00";
		$degrade[68]="#ADCE00";
		$degrade[69]="#A7CB00";
		$degrade[70]="#A2C900";
		$degrade[71]="#9BC700";
		$degrade[72]="#95C500";
		$degrade[73]="#8EC300";
		$degrade[74]="#88C000";
		$degrade[75]="#81BE00";
		$degrade[76]="#7ABB00";
		$degrade[77]="#74B800";
		$degrade[78]="#6EB500";
		$degrade[79]="#68B300";
		$degrade[80]="#61B000";
		$degrade[81]="#5BAE00";
		$degrade[82]="#55AB00";
		$degrade[83]="#4FA900";
		$degrade[84]="#49A600";
		$degrade[85]="#43A300";
		$degrade[86]="#3CA100";
		$degrade[87]="#379E00";
		$degrade[88]="#319C00";
		$degrade[89]="#2C9A00";
		$degrade[90]="#279700";
		$degrade[91]="#229500";
		$degrade[92]="#1D9400";
		$degrade[93]="#199100";
		$degrade[94]="#149000";
		$degrade[95]="#108E00";
		$degrade[96]="#D8D000";
		$degrade[97]="#98C000";
		$degrade[98]="#58A000";
		$degrade[99]="#289000";
		return $degrade[$p];
	}	
	
	public static function formPostVarData($data, $separator = "/") {
		if (($data=='0000-00-00')||($data=='0000-00-00 00:00:00')) return '';
		if ($data=='') return '';
		
		if (self::isDate($data, $separator)) return $data;
		
		$d = date("d".$separator."m".$separator."Y", strtotime($data));
		if ($d=='31/12/1969') return '';
		
		return date("d".$separator."m".$separator."Y", strtotime($data));
	}		
	
	public static function maioridade() {
		return 16;
	}
	
	public static function fullUrl($url = null) {
		return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	}	

	public static function stringToTime($data, $separator = '/') {
		if (trim($data)=='') return false;
		
		$source = $data;
		$data = explode($separator, $data);
		if (sizeof($data)!=3) return false;
		
		foreach ($data as $k=>$v) $data[$k] = (int)$v;
		
		$data = mktime(0, 0, 0, $data[1], $data[0], $data[2]);
		
		return $data;
	}
	
	public static function horaToTime($data, $separator = ':') {
		$source = $data;
		$data = explode($separator, $data);
		if (sizeof($data)!=2) return false;
		
		foreach ($data as $k=>$v) $data[$k] = (int)$v;
		
		$data = mktime($data[0], $data[1], 0, 0, 0, 0);
		
		return $data;
	}	

	/**
	 * 
	 * Forma URL baseado nos parâmetros do MVC
	 * @param array $params
	 */
	public static function setMVCReturnUrl(array $params) {
		$ru = "";
		
		$_params = array();
		$_params['module'] = false;
		foreach ($params as $k=>$v) {
			$_params[$k] = $v;
		}
		$params = $_params;

		
		foreach ($params as $k=>$v) {
			if (($k=='module') && ($v=='default')) {
				continue;
			}
			$k = (($k=='module') || ($k=='action') || ($k=='controller') || ($k=='alias')) ? '' : '/' . $k; 
			$ru .= $k . '/' . $v;
		}
		return $ru;		
	}
	
	/**
	 * Incrementa ou decrementa o ID
	 * @param string $id
	 * @param boolean $decrement
	 */
	public static function idIncrement($id, $decrement = false) {
		if (!$decrement) return 1000000000 + $id;
		return $id - 1000000000;
	}
	
	public static function retira_acentos( $texto )
	{
		$array1 = array(   "á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç"
			, "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç" );
		$array2 = array(   "a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c"
			, "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C" );
		return str_replace( $array1, $array2, $texto );
	}
	
	/**
	 * Converte a URL para caracteres especiais
	 * @param string $str
	 */
	public static function specialChars($str) {
		$espaco = " ";
		$clear_array = array( 
			"á" => "a", 
			"é" => "e", 
			"í" => "i", 
			"ó" => "o", 
			"ú" => "u",
			"à" => "a", 
			"è" => "e", 
			"ì" => "i", 
			"ò" => "o", 
			"ù" => "u",
			"ã" => "a",
			"õ" => "o",
			"â" => "a",
			"ê" => "e",
			"î" => "i",
			"ô" => "o",
			"û" => "u",
			"," => "",
			"!" => "", 
			"#" => "", 
			"%" => "", 
			"¬" => "", 
			"{" => "", 
			"}" => "",
			"^" => "", 
			"´" => "", 
			"`" => "", 
			"" => "" , 
			"/" => "", 
			";" => "", 
			":" => "", 
			"?" => "",
			"¹" => "1", 
			"²" => "2", 
			"³" => "3", 
			"ª" => "a", 
			"º" => "o", 
			"ç" => "c", 
			"ü" => "u",
			"ä" => "a",
			"ï" => "i", 
			"ö" => "o", 
			"ë" => "e", 
			"$" => "s", 
			"ÿ" => "y", 
			"w" => "w", 
			"<" => "",
			">" => "",
			"[" => "", 
			"]" => "", 
			"&" => "e", 
			" " => $espaco, 
			"." => $espaco,
			"'" => '', 
			'"' => "", 
			'0' => '0',
			'á' => 'a', 
			'Á' => 'A',  
			'é' => 'e', 
			'É' => 'E',  
			'í' => 'i', 
			'Í' => 'i',  
			'ó' => 'o' ,
			'Ó' => 'O',  
			'ú' => 'u', 
			'Ú' => 'U',  
			'â' => 'â', 
			'â' => 'â',  
			'ê' => 'ê', 
			'Ê' => 'â',
			'ô' => 'ô', 
			'Ô' => 'â',  
			'à' => 'a', 
			'À' => 'â',  
			'ç' => 'c', 
			'Ç' => 'C',  
			'ã' => 'a',
			'Ã' => 'ã',  
			'õ' => 'o', 
			'Õ' => 'o');
		
		foreach($clear_array as $key=>$val){
			$str = str_replace($key, $val, $str);
		}
		return $str;
		
		//return ereg_replace("[^a-zA-Z0-9_]", "", strtr($s, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ", "aaaaeeiooouucAAAAEEIOOOUUC_"));
	}
	
	public static function dataExtenso($data = false, $_dias = true) {
		if (!$data) $data = strtotime(date('Y-m-d G:i:s'));
		
		$dias = array('Domingo', 'Segunda-Feira', 'Terça-Feira', 'Quarta-Feira', 'Quinta-Feira', 'Sexta-Feira', 'Sábado');
		$meses = array('Janeiro', 'Fevereiro', 'Mar&ccedil;o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
		
		if ($_dias) {
			$data = $dias[(int)date('w', $data)] . ', ' . date('d', $data) . ' de ' . $meses[(int)date('m', $data)-1] . ' de ' . date('Y', $data);
		} else {
			$data = date('d', $data) . ' de ' . strtolower( $meses[(int)date('m', $data)-1] ). ' de ' . date('Y', $data);
		}
		return $data;
	}	
	
	public static function extenso($valor = 0, $maiusculas = false) { 
		$singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão"); 
		$plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões", 
			"quatrilhões"); 
		
		$c = array("", "cem", "duzentos", "trezentos", "quatrocentos", 
			"quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"); 
		$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta", 
			"sessenta", "setenta", "oitenta", "noventa"); 
		$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze", 
			"dezesseis", "dezesete", "dezoito", "dezenove"); 
		$u = array("", "um", "dois", "três", "quatro", "cinco", "seis", 
			"sete", "oito", "nove"); 
		
		$z = 0; 
		$rt = "";
		
		$valor = number_format($valor, 2, ".", "."); 
		$inteiro = explode(".", $valor); 
		for($i=0;$i<count($inteiro);$i++) 
			for($ii=strlen($inteiro[$i]);$ii<3;$ii++) 
				$inteiro[$i] = "0".$inteiro[$i]; 

			$fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2); 
			for ($i=0;$i<count($inteiro);$i++) { 
				$valor = $inteiro[$i]; 
				$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]]; 
				$rd = ($valor[1] < 2) ? "" : $d[$valor[1]]; 
				$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : ""; 

				$r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && 
					$ru) ? " e " : "").$ru; 
				$t = count($inteiro)-1-$i; 
				$r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : ""; 
				if ($valor == "000")$z++; elseif ($z > 0) $z--; 
				if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t]; 
				if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && 
					($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r; 
			} 
		
		$rt = trim($rt);
		
		if(!$maiusculas){ 
			return($rt ? $rt : "zero"); 
		} else { 

			if ($rt) $rt=ereg_replace(" E "," e ",ucwords($rt));
			return (($rt) ? ($rt) : "Zero"); 
		} 

	} 	

	
	/**
	 * Prepara número
	 * @param string $val
	 * @return float
	 */
	public static function formatFloat($val) {
		/*
		$val = str_replace(".", "", $val);
		$val = str_replace(",", ".", $val);
		return floatval($val); */
	}
	
	/**
	 * Prepara número
	 * @param string $val
	 * @return float
	 */
	public static function trataNum($val) {
		if (is_numeric($val)) return $val;
		
		$val = str_replace(".", "", $val);
		$val = str_replace(",", ".", $val);
		return floatval($val);
	}	
	
	
	/**
	 * Captura o namespace do módulo usado
	 * @return string
	 */
	public static function getAdminNameSpace() {
		$front = Zend_Controller_Front::getInstance();
		$module = $front->getRequest()->getModuleName();
		
		if ($module=='finan') $module = 'admin';
		if ($module=='rel') $module = 'admin';
		if ($module=='ptrec') $module = 'admin';
		if ($module=='pae') $module = 'admin';
		if ($module=='aud') $module = 'admin';
		if ($module=='equipamentos') $module = 'admin';
		if ($module=='rh') $module = 'admin';
		if ($module=='epi') $module = 'admin';
		
		return  $module . "_mn";
	}
	
	/**
	 * Verifica se é um email válido
	 * @param string $url
	 */
	public static function isUrl($url) {
		return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
	}    	
	
	/**
	 * Verifica se uma string é um email válido
	 * @param string $email
	 */
	public static function isValidEmail($email){
		return preg_match('|^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]{2,})+$|i', $email);
	}	
	
	/**
	 * Verifica se uma data é válida
	 * @param string data
	 * @param string separator
	 * @return DateTime
	 */
	public static function isDate($data, $separator = "/") {
		$source = $data;
		$data = explode($separator, $data);
		if (sizeof($data)!=3) return false;
		
		foreach ($data as $k=>$v) $data[$k] = (int)$v;
		
		$data = mktime(0, 0, 0, $data[1], $data[0], $data[2]);
		$ndata = date("d".$separator."m".$separator."Y", $data);
		
		if ($source!=$ndata) return false;
		return $data;
	} 
	
	/**
	 * Verifica opção de acesso
	 * @param unknown_type $limite
	 * @param unknown_type $opcao
	 */
	public static function isCadastroAccess($limite, $opcao) {
		//isAccess("usuarios", "editar");

		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->cadastro)) return false;
		$cadastro = unserialize($loginNameSpace->cadastro);
		if (($cadastro) && ($cadastro['idperfil']==0)) {

			
			if (($limite=='ceps') || ($limite=='geograficoregioes') || ($limite=='geograficozonas')) {
				if (($opcao=='editar') || ($opcao=='adicionar') || ($opcao=='excluir')) {
					return false;
				}
			}
			
			return true;

		}	
		
		$nameSpace = Mn_Util::getAdminNameSpace();

		$strsql = "SELECT $opcao as access
		FROM cadastroslimites ul1
		LEFT JOIN cadastrosperfislimites pf1
		ON pf1.idlimite = ul1.id
		WHERE limite='$limite' AND idperfil=".$cadastro["idperfil"];

		$db = Zend_Registry::get("db");
		$row = $db->fetchRow($strsql);

		if (!$row) {
			return false;
		}

		return ($row['access']=='sim');
	}	

	public static function blockCadastroAccess($limite, $actionName, $goto = null) {
		$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');

		if (is_null($goto)) {
			$goto = Licencas::getLicenca('alias') . "/";
		}

		if (($actionName=="index")
			&& (!Mn_Util::isCadastroAccess($limite, "visualizar"))) { $redirector->gotoUrl($goto); }
			if (($actionName=="adicionar")
				&& (!Mn_Util::isCadastroAccess($limite, "adicionar"))) { $redirector->gotoUrl($goto); }
				if (($actionName=="editar")
					&& (!Mn_Util::isCadastroAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }
					if (($actionName=="excluir")
						&& (!Mn_Util::isCadastroAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }
						if (($actionName=="excluirxml")
							&& (!Mn_Util::isCadastroAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }
							if (($actionName=="excluirmultixml")
								&& (!Mn_Util::isCadastroAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }
								if (($actionName=="view")
									&& (!Mn_Util::isCadastroAccess($limite, "visualizar"))) { $redirector->gotoUrl($goto); }
									if (($actionName=="consultar")
										&& (!Mn_Util::isCadastroAccess($limite, "visualizar"))) { $redirector->gotoUrl($goto); }
										if (($actionName=="ordem")
											&& (!Mn_Util::isCadastroAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }
											if (($actionName=="editarsecao")
												&& (!Mn_Util::isCadastroAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }
												if (($actionName=="adicionarsecao")
													&& (!Mn_Util::isCadastroAccess($limite, "adicionar"))) { $redirector->gotoUrl($goto); }
													if (($actionName=="ordemsecao")
														&& (!Mn_Util::isCadastroAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }
														if (($actionName=="excluirsecaomultixml")
															&& (!Mn_Util::isCadastroAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }
															if (($actionName=="excluirsecaoxml")
																&& (!Mn_Util::isCadastroAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }
																if (($actionName=="changestatusxml")
																	&& (!Mn_Util::isCadastroAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }


															}	

	/**
	 * Verifica opção de acesso
	 * @param unknown_type $limite
	 * @param unknown_type $opcao
	 */
	public static function isAccess($limite, $opcao) {
		//isAccess("usuarios", "editar");
		
		$usuario = Usuarios::getUsuarioByIdHelper(Usuarios::getUsuario('id'));
		if (($usuario) && ($usuario['tipo']!=Usuarios::$_TIPO_ADMIN)) {
			
			//if ($limite=='perfis') return false;
			//if ($limite=='produtoscategorias') return false;
			//if ($limite=='produtos') return false;
			
		}
		
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) return false;
		$usuario = unserialize($loginNameSpace->usuario);
		//var_dump($limite);
		$nameSpace = Mn_Util::getAdminNameSpace();
		//if($limite=="rhfrequencias") die($strsql);
		$strsql = "SELECT $opcao as access 
		FROM usuarioslimites ul1 
		LEFT JOIN usuariosperfislimites pf1 
		ON pf1.idlimite = ul1.id 
		WHERE limite='$limite' AND idperfil=".$usuario["idperfil"];
	//if(($usuario['idperfil']=="14") && ($opcao=="rhfrequencias")) die($strsql);
		$db = Zend_Registry::get("db");
		$row = $db->fetchRow($strsql);
		
		if (!$row) {
			return false;
		}
		
		return ($row['access']=='sim');
	}
	
	public static function blockAccessEmpresa($limite, $actionName, $goto = null) {
		$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');

		if (is_null($goto)) {
			$goto = "/";
		}

		if (($actionName=="index")
			&& (!Mn_Util::isAccessEmpresa($limite, "visualizar"))) { $redirector->gotoUrl($goto); }
			if (($actionName=="adicionar")
				&& (!Mn_Util::isAccessEmpresa($limite, "adicionar"))) { $redirector->gotoUrl($goto); }
				if (($actionName=="editar")
					&& (!Mn_Util::isAccessEmpresa($limite, "editar"))) { $redirector->gotoUrl($goto); }
					if (($actionName=="excluir")
						&& (!Mn_Util::isAccessEmpresa($limite, "excluir"))) { $redirector->gotoUrl($goto); }
						if (($actionName=="excluirxml")
							&& (!Mn_Util::isAccessEmpresa($limite, "excluir"))) { $redirector->gotoUrl($goto); }
							if (($actionName=="excluirmultixml")
								&& (!Mn_Util::isAccessEmpresa($limite, "excluir"))) { $redirector->gotoUrl($goto); }
								if (($actionName=="view")
									&& (!Mn_Util::isAccessEmpresa($limite, "visualizar"))) { $redirector->gotoUrl($goto); }
									if (($actionName=="consultar")
										&& (!Mn_Util::isAccessEmpresa($limite, "visualizar"))) { $redirector->gotoUrl($goto); }
										if (($actionName=="ordem")
											&& (!Mn_Util::isAccessEmpresa($limite, "editar"))) { $redirector->gotoUrl($goto); }
											if (($actionName=="editarsecao")
												&& (!Mn_Util::isAccessEmpresa($limite, "editar"))) { $redirector->gotoUrl($goto); }
												if (($actionName=="adicionarsecao")
													&& (!Mn_Util::isAccessEmpresa($limite, "adicionar"))) { $redirector->gotoUrl($goto); }
													if (($actionName=="ordemsecao")
														&& (!Mn_Util::isAccessEmpresa($limite, "editar"))) { $redirector->gotoUrl($goto); }
														if (($actionName=="excluirsecaomultixml")
															&& (!Mn_Util::isAccessEmpresa($limite, "excluir"))) { $redirector->gotoUrl($goto); }
															if (($actionName=="excluirsecaoxml")
																&& (!Mn_Util::isAccessEmpresa($limite, "excluir"))) { $redirector->gotoUrl($goto); }
																if (($actionName=="changestatusxml")
																	&& (!Mn_Util::isAccessEmpresa($limite, "editar"))) { $redirector->gotoUrl($goto); }


															}	


														public static function blockAccessMultiple($limites, $actionName, $goto = null) {
															$acesso = false;
															$chave = array(
																"index" => "visualizar",
																"adicionar" => "adicionar",
																"editar" => "editar",
																"excluir" => "excluir",	
																"excluirxml" => "excluir",	
																"excluirmultixml" => "excluir",	
																"view" => "visualizar",	
																"consultar" => "visualizar",	
																"ordem" => "editar",	
																"editarsecao" => "editar",	
																"adicionarsecao" => "adicionar",	
																"ordemsecao" => "editar",	
																"excluirsecaomultixml" => "excluir",	
																"excluirsecaoxml" => "excluir",	
																"changestatusxml" => "editar",
																"clonedetalhes" => "editar"	,
																"clonarapartirde" => "editar"
															);
															foreach ($limites as $limite) {
																if (Mn_Util::isAccess($limite, $chave[$actionName])) {
																	$acesso = true;
																	break;
																}
															}

															if ($acesso) {
																return true;
															} else {
																$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
																$redirector->gotoUrl($goto);
															}
														}

	/**
	 * 
	 * Verifica nível de acesso às actions
	 * @param unknown_type $limite
	 * @param unknown_type $actionName
	 * @param unknown_type $goto
	 */
	public static function blockAccess($limite, $actionName, $goto = null) {
		$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
		
		if (is_null($goto)) {
			$goto = "/admin";
		}
		
		if (($actionName=="index")
			&& (!Mn_Util::isAccess($limite, "visualizar"))) { $redirector->gotoUrl($goto); }
			if (($actionName=="adicionar")
				&& (!Mn_Util::isAccess($limite, "adicionar"))) { $redirector->gotoUrl($goto); }
				if (($actionName=="editar")
					&& (!Mn_Util::isAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }
					if (($actionName=="excluir")
						&& (!Mn_Util::isAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }	
						if (($actionName=="excluirxml")
							&& (!Mn_Util::isAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }	
							if (($actionName=="excluirmultixml")
								&& (!Mn_Util::isAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }	
								if (($actionName=="view")
									&& (!Mn_Util::isAccess($limite, "visualizar"))) { $redirector->gotoUrl($goto); }	
									if (($actionName=="consultar")
										&& (!Mn_Util::isAccess($limite, "visualizar"))) { $redirector->gotoUrl($goto); }	
										if (($actionName=="ordem")
											&& (!Mn_Util::isAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }	
											if (($actionName=="editarsecao")
												&& (!Mn_Util::isAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }	
												if (($actionName=="adicionarsecao")
													&& (!Mn_Util::isAccess($limite, "adicionar"))) { $redirector->gotoUrl($goto); }	
													if (($actionName=="ordemsecao")
														&& (!Mn_Util::isAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }	
														if (($actionName=="excluirsecaomultixml")
															&& (!Mn_Util::isAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }	
															if (($actionName=="excluirsecaoxml")
																&& (!Mn_Util::isAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }	
																if (($actionName=="changestatusxml")
																	&& (!Mn_Util::isAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }	
															}	



	/**
	 * Verifica opção de acesso
	 * @param unknown_type $limite
	 * @param unknown_type $opcao
	 */
	public static function isDefaultAccess($limite, $opcao) {
		//isAccess("usuarios", "editar");
		
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) return false;
		$usuario = unserialize($loginNameSpace->escolausuario);
		
		if (Escolasusuarios::getUsuario('master')=='sim') return true;
		
		$nameSpace = Mn_Util::getAdminNameSpace();
		
		$strsql = "SELECT $opcao as access 
		FROM escolasusuarioslimites ul1 
		LEFT JOIN escolasusuariosperfislimites pf1 
		ON pf1.idlimite = ul1.id 
		WHERE limite='$limite' AND idperfil=".$usuario["idperfil"];
		
		//if(($limite=="programacoesaulascoordenacao")) die($strsql);
		$db = Zend_Registry::get("db");
		$row = $db->fetchRow($strsql);
		
		if (!$row) {
			return false;
		}
		return ($row['access']=='sim');
	}
	
	/**
	 * 
	 * Verifica nível de acesso às actions
	 * @param unknown_type $limite
	 * @param unknown_type $actionName
	 * @param unknown_type $goto
	 */
	public static function blockDefaultAccess($limite, $actionName, $goto = null) {
		$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
		
		if (is_null($goto)) {
			$goto = "/";
		}
		
		if (($actionName=="index")
			&& (!Mn_Util::isDefaultAccess($limite, "visualizar"))) { $redirector->gotoUrl($goto); }
			if (($actionName=="adicionar")
				&& (!Mn_Util::isDefaultAccess($limite, "adicionar"))) { $redirector->gotoUrl($goto); }
				if (($actionName=="editar")
					&& (!Mn_Util::isDefaultAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }
					if (($actionName=="excluir")
						&& (!Mn_Util::isDefaultAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }	
						if (($actionName=="excluirxml")
							&& (!Mn_Util::isDefaultAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }	
							if (($actionName=="excluirmultixml")
								&& (!Mn_Util::isDefaultAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }	
								if (($actionName=="view")
									&& (!Mn_Util::isDefaultAccess($limite, "visualizar"))) { $redirector->gotoUrl($goto); }	
									if (($actionName=="consultar")
										&& (!Mn_Util::isDefaultAccess($limite, "visualizar"))) { $redirector->gotoUrl($goto); }	
										if (($actionName=="ordem")
											&& (!Mn_Util::isDefaultAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }	
											if (($actionName=="editarsecao")
												&& (!Mn_Util::isDefaultAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }	
												if (($actionName=="adicionarsecao")
													&& (!Mn_Util::isDefaultAccess($limite, "adicionar"))) { $redirector->gotoUrl($goto); }	
													if (($actionName=="ordemsecao")
														&& (!Mn_Util::isDefaultAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }	
														if (($actionName=="excluirsecaomultixml")
															&& (!Mn_Util::isDefaultAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }	
															if (($actionName=="excluirsecaoxml")
																&& (!Mn_Util::isDefaultAccess($limite, "excluir"))) { $redirector->gotoUrl($goto); }	
																if (($actionName=="changestatusxml")
																	&& (!Mn_Util::isDefaultAccess($limite, "editar"))) { $redirector->gotoUrl($goto); }	
															}		

        /**
	 * 
	 * Envia o eamil para a comunicação, criado por Thiago Torres Migliorati
	 * @param string $to
	 * @param string $subject
	 * @param string $body
	 * @param string $header_
	 */
        public static function sendMailComunicacao($to, $subject, $body, $header) {
        	error_reporting(0);

/*		$header_ = "MIME-Version: 1.0\n" ;
        $header_ .= "Content-Type: text/html; charset=\"utf-8\"\n";
		$header_ .= "From: ProprietárioDireto<contato@proprietariodireto.com.br>\n";		
*/
		
		$headers = "MIME-Version: 1.1\n";
		$headers .= "Content-type: text/html; charset=utf-8\n";
		$headers .= "From: Prefeitura de Santa Isabel - Comunicação <comunicacao@santaisabel.com>\n";
		$headers .= "Return-Path: contato@xxxxx.com.br\n";				
//		$headers .= "Reply-To: contato@elitemotors.com.br\n";

		$body = "<html><body>".$body."</body></html>";

		if (mail($to, $subject, $body, $headers, '-contato@santaisabel.com.br')) {
			return "OK";
		} else {
			return "OK";
		}				
	}// end send mail comunicacao	
	   
	/** 
	 * @param string $to
	 * @param string $subject
	 * @param string $body
	 * @param string $header_
	 */
    public static function sendMailProfessoresmensagens($to, $subject, $body, $header) {
        	error_reporting(0);
 
		$headers = "MIME-Version: 1.1\n";
		$headers .= "Content-type: text/html; charset=utf-8\n";
		$headers .= "From: Prefeitura de Santa Isabel - Mensagens <comunicacao@santaisabel.com>\n";
		$headers .= "Return-Path: contato@xxxxx.com.br\n";				


		$body = "<html><body>".$body."</body></html>";

		if (mail($to, $subject, $body, $headers, '-contato@santaisabel.com.br')) {
			return "OK";
		} else {
			return "OK";
		}				
	}	

	/**
	 * 
	 * Envia o eamil
	 * @param string $to
	 * @param string $subject
	 * @param string $body
	 * @param string $header_
	 */
	public static function sendMail($to, $subject, $body, $header_) {
		error_reporting(0);

/*		$header_ = "MIME-Version: 1.0\n" ;
        $header_ .= "Content-Type: text/html; charset=\"utf-8\"\n";
		$header_ .= "From: ProprietárioDireto<contato@proprietariodireto.com.br>\n";		
*/
		
		$headers = "MIME-Version: 1.1\n";
		$headers .= "Content-type: text/html; charset=utf-8\n";
		$headers .= "From: Transportadora xxxxx<contato@xxxxx.com.br>\n";
		$headers .= "Return-Path: contato@xxxxx.com.br\n";				
//		$headers .= "Reply-To: contato@elitemotors.com.br\n";

		$body = "<html><body>".$body."</body></html>";

		if (mail($to, $subject, $body, $headers, '-rcontato@xxxxx.com.br')) {
			return "OK";
		} else {
			return "OK";
		}				
	}		
	
	
	
	public static function sendMailSMTP($from, $fromName, $to, $subject, $body, $idboleto = 0) {

		
		//$config = self::getConfigAssoc();
		
		$mail = new PHPMailer();

		$mail->IsSMTP(); // Define que a mensagem será SMTP
		$mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
		$mail->Username = 'no-reply@fjmx.com.br'; // Usuário do servidor SMTP (endereço de email)
		$mail->Password = 'joh6xosa'; // Senha do servidor SMTP (senha do email usado)
		$mail->Host = 'smtp.fjmx.com.br'; 
		$mail->SMTPAuth = true;
		$mail->SMTP_PORT = 587;
		// Define o remetente
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->From = $from; // Seu e-mail
		$mail->Sender = $from; // Seu e-mail
		$mail->FromName = $fromName; // Seu nome

		// Define os destinatário(s)
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->AddAddress($to);
		//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
		//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

		if ($idboleto>0) {
			$arquivos = new Arquivos();
			$arquivo = $arquivos->fetchRow("id=".$idboleto);
			if ($arquivo) {
				$arquivo = $arquivo->toArray();
				
				$source = BASE_PATH . '/application/modules/servidor/arquivos/' . $idboleto . '.dat';
				$dest = BASE_PATH . '/public/admin/tmp/' . $arquivo['filename'];
				
				copy($source, $dest);
				$mail->AddAttachment($dest);	
			}

		}
		
		// Define os dados técnicos da Mensagem
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
		//$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)

		// Define a mensagem (Texto e Assunto)
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->Subject  = $subject; // Assunto da mensagem
		$mail->Body = $body;
		$mail->AltBody = $body;

		// Define os anexos (opcional)
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		//$mail->AddAttachment("/home/login/documento.pdf", "novo_nome.pdf");  // Insere um anexo

		// Envia o e-mail
		$enviado = $mail->Send();

		// Limpa os destinatários e os anexos
		$mail->ClearAllRecipients();
		//$mail->ClearAttachments();
		
		if (isset($dest)) {
			unlink($dest);
		}
		
		
		if ($enviado) {
			return true;
		} else {
			return $mail->ErrorInfo;

		}
		
		
	} 		
	
	
	public static function sendMailSMTPSistema($from, $fromName, $to, $subject, $body, $cc = false) {

		//$to = 'teste@teste.com.br';
		//$config = self::getConfigAssoc();

		$mail = new PHPMailer();

		$mail->IsSMTP(); // Define que a mensagem será SMTP
		$mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
		$mail->Username = 'sistema@xxxxx.com.br'; // Usuário do servidor SMTP (endereço de email)
		$mail->Password = 'Mercurio1'; // Senha do servidor SMTP (senha do email usado)
		$mail->Host = 'smtp.xxxxx.com.br';
		$mail->SMTPAuth = true;
		$mail->SMTP_PORT = 587;
		// Define o remetente
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->From = $from; // Seu e-mail
		$mail->Sender = $from; // Seu e-mail
		$mail->FromName = $fromName; // Seu nome

		// Define os destinatário(s)
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->AddAddress($to);
		//$mail->AddCC('ciclano@site.net', 'Ciclano'); // Copia
		//$mail->AddBCC('fulano@dominio.com.br', 'Fulano da Silva'); // Cópia Oculta

		// Define os dados técnicos da Mensagem
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
		//$mail->CharSet = 'iso-8859-1'; // Charset da mensagem (opcional)

		// Define a mensagem (Texto e Assunto)
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		$mail->Subject  = $subject; // Assunto da mensagem
		$mail->Body = $body;
		$mail->AltBody = $body;
		if ($cc) {
			$mail->AddBCC($cc);
		}

		// Define os anexos (opcional)
		// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		//$mail->AddAttachment("/home/login/documento.pdf", "novo_nome.pdf");  // Insere um anexo

		// Envia o e-mail
		$enviado = $mail->Send();

		// Limpa os destinatários e os anexos
		$mail->ClearAllRecipients();
		//$mail->ClearAttachments();

		if (isset($dest)) {
		//	unlink($dest);
		}


		if ($enviado) {
			return true;
		} else {
			return $mail->ErrorInfo;

		}


	}	
	
	
	public static function formatar ($string, $tipo = "")
	{
		$string = preg_replace("[^0-9]", "", $string);
		if (!$tipo)
		{
			switch (strlen($string))
			{
				case 10:    $tipo = 'fone';     break;
				case 8:     $tipo = 'cep';      break;
				case 11:    $tipo = 'cpf';      break;
				case 14:    $tipo = 'cnpj';     break;
			}
		}
		switch ($tipo)
		{
			case 'fone':
			$string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 4) .
			'-' . substr($string, 6);
			break;
			case 'cep':
			$string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
			break;
			case 'cpf':
			$string = substr($string, 0, 3) . '.' . substr($string, 3, 3) .
			'.' . substr($string, 6, 3) . '-' . substr($string, 9, 2);
			break;
			case 'cnpj':
			$string = substr($string, 0, 2) . '.' . substr($string, 2, 3) .
			'.' . substr($string, 5, 3) . '/' .
			substr($string, 8, 4) . '-' . substr($string, 12, 2);
			break;
			case 'rg':
			$string = substr($string, 0, 2) . '.' . substr($string, 2, 3) .
			'.' . substr($string, 5, 3);
			break;
		}
		return $string;
	}

	public static function getMes($mes) {
		$meses = array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
		return $meses[$mes-1];
	}	
	
	public static function getMesAbreviado($mes) {
		$meses = array('Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez');
		return $meses[$mes-1];
	}	

        //Função muito utilizada para transformar datas para a classe Datatime
        //public static function getHoraFormatada($hora, $formato){
        //    $dateObj =  date('H:i:s', strtotime($hora));
        //    $Date = new DateTime($dateObj);
        //    return $Date->format($formato);
        //}//end static function getHoraFormatada

	public static function formataBytes($bytes, $decimals = 2) {
		if($bytes == 0) {return '0 Bytes';}

		$k = 1024;
		$dm = $decimals;
		$sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

		$i = floor(log($bytes) / log($k));
		$num = ($bytes / pow($k, $i));

		return number_format($num, $dm, '.', '') . ' ' . $sizes[$i];
	}

	/*
	* Recebe duas datas e retorna a diferença em anos, meses e dias
	*/
	public function getDiffDates($start, $end) {
		$start = new DateTime($start);
		$end = new DateTime($end);

		$interval = $start->diff($end);

		$str = ' ';

		if ($interval->y == 1) $str .= $interval->y.' ano';
		if ($interval->y > 1) $str .= $interval->y.' anos';

		if ($interval->y > 0 && $interval->m > 0) $str .= ', ';

		if ($interval->m == 1) $str .= $interval->m.' mês';
		if ($interval->m > 1) $str .= $interval->m.' meses';

		if (($interval->y > 0 || $interval->m > 0 ) && $interval->d > 0) $str .= ' e ';

		if ($interval->d == 1) $str .= $interval->d.' dia';
		if ($interval->d > 1) $str .= $interval->d.' dias';

		return $str;
	}
}