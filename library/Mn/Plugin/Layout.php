<?php


class Mn_Plugin_Layout extends Zend_Layout_Controller_Plugin_Layout {
	
    public function preDispatch (Zend_Controller_Request_Abstract $request) {
    	$moduleName = $request->getModuleName();
    	$this->_moduleChange($moduleName);
    	
    	$login = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
    	
    	if (isset($login->usuario)) {
    		Zend_Layout::getMvcInstance()->setLayout("layout");
    	} else {
    		Zend_Layout::getMvcInstance()->setLayout("layout");    		
    	}
    }

    protected function _moduleChange ($moduleName)    {
    	if ($moduleName=='finan') $moduleName = 'admin';
    	if ($moduleName=='rel') $moduleName = 'admin';
    	if ($moduleName=='ptrec') $moduleName = 'admin';    	
    	if ($moduleName=='pae') $moduleName = 'admin';
    	if ($moduleName=='aud') $moduleName = 'admin';
    	if ($moduleName=='equipamentos') $moduleName = 'admin';
    	if ($moduleName=='rh') $moduleName = 'admin';
    	if ($moduleName=='epi') $moduleName = 'admin';
    	
        Zend_Layout::getMvcInstance()->setLayoutPath(APPLICATION_PATH . "/modules/$moduleName/layouts/scripts");
    }

}

