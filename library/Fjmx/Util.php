<?php

/*
 * ** Comentários de Atualizações ***

  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "17/07/2018"
  NRO_OS            = ""
  PROGRAMADOR       = "JCSANTOS"
  CLIENTE           = "SANTAISABEL"
  NOME_DA_ROTINA    = "Fjmx_Util.php"
  MANUTENCAO        = "Criação da função formataBytes, realiza a conversão de Bytes em outras bases."
  </IDENTIFICACAO_DE_MANUTENCAO>

  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "06/09/2018"
  NRO_OS            = ""
  PROGRAMADOR       = "JCSANTOS"
  CLIENTE           = "SANTAISABEL"
  NOME_DA_ROTINA    = "Fjmx_Util.php"
  MANUTENCAO        = "Criação da função formataMilhares, realiza a formatação de um número."
  </IDENTIFICACAO_DE_MANUTENCAO>

  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "10/09/2018"
  NRO_OS            = ""
  PROGRAMADOR       = "JCSANTOS"
  CLIENTE           = "SANTAISABEL"
  NOME_DA_ROTINA    = "Fjmx_Util.php"
  MANUTENCAO        = "Criação da função ValidaCampo."
  </IDENTIFICACAO_DE_MANUTENCAO>

  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "13/09/2018"
  NRO_OS            = ""
  PROGRAMADOR       = "JCSANTOS"
  CLIENTE           = "SANTAISABEL"
  NOME_DA_ROTINA    = "Fjmx_Util.php"
  MANUTENCAO        = "Ajustes na função ValidaCampo."
  </IDENTIFICACAO_DE_MANUTENCAO>

  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "14/09/2018"
  NRO_OS            = ""
  PROGRAMADOR       = "JCSANTOS"
  CLIENTE           = "SANTAISABEL"
  NOME_DA_ROTINA    = "Fjmx_Util.php"
  MANUTENCAO        = "Criação da função executaSQL."
  </IDENTIFICACAO_DE_MANUTENCAO>

  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "24/09/2018"
  NRO_OS            = ""
  PROGRAMADOR       = "JCSANTOS"
  CLIENTE           = "SANTAISABEL"
  NOME_DA_ROTINA    = "Fjmx_Util.php"
  MANUTENCAO        = "Criação da função RetirarAcentos Retira caracteres especiais da string."
  </IDENTIFICACAO_DE_MANUTENCAO>

  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "24/09/2018"
  NRO_OS            = ""
  PROGRAMADOR       = "JCSANTOS"
  CLIENTE           = "SANTAISABEL"
  NOME_DA_ROTINA    = "Fjmx_Util.php"
  MANUTENCAO        = "Criação da função geraLog, grava log de informações do sistema."
  </IDENTIFICACAO_DE_MANUTENCAO>

  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "01/11/2018"
  NRO_OS            = ""
  PROGRAMADOR       = "JCSANTOS"
  CLIENTE           = "SANTAISABEL"
  NOME_DA_ROTINA    = "Fjmx_Util.php"
  MANUTENCAO        = "Alteração da função executaSQL, usando PDO."
  </IDENTIFICACAO_DE_MANUTENCAO>

  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "13/11/2018"
  NRO_OS            = ""
  PROGRAMADOR       = "RAFAEL_ALVES"
  CLIENTE           = "SANTAISABEL"
  NOME_DA_ROTINA    = "Fjmx_Util.php"
  MANUTENCAO        = "Alteração da função setStatusSED - retorno de mensagens."
  </IDENTIFICACAO_DE_MANUTENCAO>

  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "19/11/2018"
  NRO_OS            = ""
  PROGRAMADOR       = "JCSANTOS"
  CLIENTE           = "SANTAISABEL"
  NOME_DA_ROTINA    = "Fjmx_Util.php"
  MANUTENCAO        = "Alteração da função executaSQL tratando retorno."
  </IDENTIFICACAO_DE_MANUTENCAO>

 * ** Fim Atualizações ***
 */

class Fjmx_Util {

    /**
     * Retorna tamanho do arquivo em unidades de medida da informática.
     * 
     * @access public 
     * 
     * @param Integer $bytes - Número a ser formatado.
     * @param Integer $decimals - Quantidade de casas após a virgula.
     * 
     * @return $return - Retorna número formatado.
     */
    public static function formataBytes($bytes, $decimals = 2) {
        if ($bytes == 0) {
            return '0 Bytes';
        }

        $k = 1024;
        $dm = $decimals;
        $sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        $i = floor(log($bytes) / log($k));
        $num = ($bytes / pow($k, $i));

        return number_format($num, $dm, '.', '') . ' ' . $sizes[$i];
    }

    /**
     * Retorna um número formatado.
     * 
     * @access public 
     * 
     * @param String $numero - Número a ser formatado.
     * @param Integer $decimais - Quantidade de casas após a virgula.
     * @param String $separador_decimal - Caracter de separação de decimal, default ",".
     * @param String $separador_milhar - Caracter de separação de milhares, default ".".
     * 
     * @return $return - Retorna número formatado.
     */
    public static function formataMilhares($numero, $decimais = 0, $separador_decimal = ',', $separador_milhar = '.') {
        if (is_numeric($numero)) {
            return number_format($numero, $decimais, $separador_decimal, $separador_milhar);
        } else {
            return '';
        }
    }

    /**
     * Retorna um array com as configurações do Soap SED
     *
     * @var array
     * 
     * @access public 
     * 
     * @param String $function - Função que será executada.
     * @param Array $complex - Array com informações para o service SOAP.
     * @param String $ambiente - Flag para identificar se a função rodará em Homologação ("H") ou Produção ("P").
     * 
     * @return $result - Array com retorno dos dados. 
     */
    public static function getSOAP(String $function, Array $complex, String $ambiente) {
        
        try {
            $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $resources = $config->getOption('sed');

            $usuario    = ($ambiente === 'H') ? $resources['soaphomo']['params']['usuario']  : $resources['soapprod']['params']['usuario'];
            $senha      = ($ambiente === 'H') ? $resources['soaphomo']['params']['senha']    : $resources['soapprod']['params']['senha'];
            $wsdl       = ($ambiente === 'H') ? $resources['soaphomo']['params']['wsdl']     : $resources['soapprod']['params']['wsdl'];
            $location   = ($ambiente === 'H') ? $resources['soaphomo']['params']['location'] : $resources['soapprod']['params']['location'];
            $fileWsdl = realpath(dirname(__FILE__) . '/SED/Wsdl/' . $wsdl);

            $soap = ['usuario' => $usuario,
                'senha' => $senha,
                'wsdl' => $wsdl,
                'filewsdl' => $fileWsdl,
                'location' => $location
            ];

            //$xml = simplexml_load_file($fileWsdl);

            $client = new SoapClient($fileWsdl);

            $acesso = [
                'Usuario' => $soap['usuario'],
                'Senha' => $soap['senha']
            ];
            
            //Une informações do usuário com os dados vindos do controller
            $dados = array_merge($acesso, $complex);


            $arguments = array($function => $dados);

            $options = array('location' => $soap['location']);

            $result = $client->__soapCall($function, $arguments, $options);
           
            return is_array($result) ? $result : ['Retorno' => $result];
           
        } catch (SoapFault $fault) {
            Fjmx_Util::geraLog(date('Y-m-d'), 'Função getSOAP', Usuarios::getUsuario('id'), "SoapFault: " . $fault, "E");
            return $fault;

        } catch (Throwable $e) {
            Fjmx_Util::geraLog(date('Y-m-d'), 'Função getSOAP', Usuarios::getUsuario('id'), "Exception: " . $e, "E");
            return false;
            
        }
    }

    /**
     * Executa a validação do campo, por tipo de dado e tamanho.
     * 
     * @access public 
     * 
     * @param String $dado - Valor que será testado.
     * @param String $requerido - (S/N) Verifica se o campo é obrigatório ou não.
     * @param String $tipo - Alfanumérico "AL" ou Numérico "NU".
     * @param String $tamanho - Tamanho do dado.
     * @param String $flag - Altera valor de um campo flag alfanumérico. Ex: A expressão ("0,X;1, "), pega valor "0" e substitui para "X" e "1" para " ", podendo receber n itens.
     * 
     * @return $dado - Valor com o tratamento. 
     */
    public static function ValidaCampoSoap(string $dado = null, string $requerido, string $tipo, int $tamanho = 0, string $flag = '') {

        if (is_null($dado)) {
            $dado = "";
        }

        // Substitui campo flag
        if ($flag != "") {

            $dado = mb_strtoupper(Fjmx_Util::RetirarAcentos($dado));
            $flag = mb_strtoupper(Fjmx_Util::RetirarAcentos($flag));

            $vf = explode(";", $flag);

            foreach ($vf as &$r) {
                $x = explode(",", $r);

                $x[0];
                $x[1];
                $dado = str_replace($x[0], $x[1], $dado);
            }
        }

        if ($requerido === 'S') {
            if ($tipo === 'NU') {
                if (is_numeric($dado)) {
                    if ($tamanho > 0) {
                        if (strlen($dado) !== $tamanho) {
                            $dado = str_pad($dado, $tamanho, "0", STR_PAD_LEFT);
                        } else {
                            if ($dado === '') {
                                $dado = str_repeat("0", $tamanho);
                            }
                        }
                    }
                }
            }
            if ($tipo === 'AL') {
                if ($tamanho > 0) {
                    if (strlen($dado) !== $tamanho) {
                        $dado = str_pad($dado, $tamanho, " ", STR_PAD_RIGHT);
                    } else {
                        if ($dado === '') {
                            $dado = str_repeat(" ", $tamanho);
                        }
                    }
                }
            }
        }

        return $dado;
    }

    /**
     * Executa um comando SQL.
     * 
     * @access public 
     * 
     * @param String $sql - Query a ser executada.
     * 
     * @return $db - Array contendo os valores. 
     */
    public static function executaSQL(string $sql) {

        try {
            $config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $resources = $config->getOption('resources');

            $HOST = $resources['db']['params']['host'];
            $BASE = $resources['db']['params']['dbname'];
            $USER = $resources['db']['params']['username'];
            $PASS = $resources['db']['params']['password'];
            
            $PDO = new PDO('mysql:host=' . $HOST . ';dbname=' . $BASE, $USER, $PASS);

            Fjmx_Util::geraLog(date('Y-m-d'), '$sql', Usuarios::getUsuario('id'), "MySQL: ".$sql);
            
            $result = $PDO->query($sql);
            
            return is_bool($result) ?  $result : (array) $result->fetchAll();
            
        } catch ( PDOException $e ) {
            Fjmx_Util::geraLog(date('Y-m-d'), 'Função executaSQL', Usuarios::getUsuario('id'), "Erro ao conectar com o MySQL: ".$e->getMessage(), "E");
        } catch (Exception $ex) {
            Fjmx_Util::geraLog(date('Y-m-d'), 'Função executaSQL', Usuarios::getUsuario('id'), "Erro ao executar consulta: '$sql' - Exception: '$ex '.", "E");
            return false;
        }
    }

    /**
     * Atualiza Status retornado do SED
     * 
     * @access public 
     * 
     * @param Int $id - Id do registro.
     * @param Int $model - Model que será utilizada.
     * @param Int $soapstatus - Status retornado do servidor.
     * @param Int $soaptipo - Tipo de movimentação.
     * 
     */
    public static function setStatusSED($id, $model, $soapstatus, $soaptipo, $acao) {

        $rows = new $model();
        $linha = $rows->fetchRow('id=' . $id);
        $retorno = '';

        if ($linha) {

            $row = $linha->toArray();

            $soapstatus = json_decode(json_encode($soapstatus), true);

            if (empty($soapstatus)) {
                $retorno = 'Houve algum erro na comunicação com o servidor do SED, seus dados foram salvos, porém é necessário repetir o proceso mais tarde.<br>';
                $row['soapretorno'] = 'ERROR';
            } else if (isset($soapstatus['Erro'])) {
                $retorno = $soapstatus['Erro'] . '<br>';
                $row['soapretorno'] = 'ERROR';
            } else if (isset($soapstatus['MensagensErro']->MensagensErroItem)) {
                $retorno = $soapstatus['MensagensErro']->MensagensErroItem . '<br>';
                $row['soapretorno'] = 'ERROR';
            } else if (isset($soapstatus["Msg$acao"]->outErro)) {
                $retorno = $soapstatus["Msg$acao"]->outErro . '<br>';
                $row['soapretorno'] = 'ERROR';
            } else if (isset($soapstatus["Msg$acao"]->outMensagem)) {               
                $retorno = $soapstatus["Msg$acao"]->outMensagem . '<br>';
                $row['soapretorno'] = 'OK';
            } else if (isset($soapstatus["Msg$acao"]->outSucesso)) {               
                $retorno = $soapstatus["Msg$acao"]->outSucesso . '<br>';
                $row['soapretorno'] = 'OK';               
            } else if (isset($soapstatus['outSucesso']) && $soapstatus['outSucesso'] == 'OK') {
                $retorno = 'Registro alterado com sucesso!<br>';
                $row['soapretorno'] = 'OK';
            } else if (!is_array($soapstatus)) {
                $retorno = $soapstatus;
                $row['soapretorno'] = 'INFO';
            }

            $processoid = isset($soapstatus['outProcessoID']) ? 'Processo ID: ' . $soapstatus['outProcessoID'] : '';

            $row['soapstatus'] = $retorno . $processoid . '<br>'
                . '<br>Atualizado em: ' . date('d/m/Y H:i:s', strtotime($row['logdata']));
            
            $row['soaptipo'] = $soaptipo;

            $rows->save($row);

            Fjmx_Util::geraLog(date('Y-m-d'), 'Status SED', Usuarios::getUsuario('id'), "Operação: '$soaptipo' - Retorno: " . str_replace("<br>", "", $retorno) . " - $processoid.");

            return true;
        }

        return false;
    }

    /**
     * Retira caracteres especiais usando expressão regular
     * 
     * @access public 
     * 
     * @param String $string - Conjunto de caracteres que será avalidado e alterado.
     * @return String - Conjunto sem caracteres especiais.
     */
    public static function RetirarAcentos($string) {
        return preg_replace(
                array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/",
            "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/",
            "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/",
            "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/",
            "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/",
            "/(ñ)/", "/(Ñ)/",
            "/(ç)/", "/(Ç)/"
                ), explode(" ", "a A e E i I o O u U n N c C"), $string);
    }

    /**
     * Inclui LOG.
     * 
     * @access public 
     * 
     * @param String $dtatual - Data de execução do processo.
     * @param String $rotina - Nome da Rotina.
     * @param String $idusuario - Id do Usuário.
     * @param String $mensagem - Mensagem.
     * @param String $tipo - Tipo de log, E para erro, M para mensagem.
     * 
     */
    public static function geraLog(string $dtatual, string $rotina, string $idusuario, string $mensagem, string $tipo = 'M') {

        $usuario = Usuarios::getUsuarioByIdHelper($idusuario);

        $log = new Logs();

        try {
            $dados = array();
            $dados['lgid'] = 0;
            $dados['lgdtatual'] = $dtatual;
            $dados['lgsistema'] = substr("Santa Isabel", 0, 254);
            $dados['lgrotina'] = substr($rotina, 0, 63);
            $dados['lgnomeusuario'] = substr(($usuario['id'] . " - " . $usuario['nomerazao'] . " " . $usuario['sobrenomefantasia'] . " RGF: " . $usuario['rgf']), 0, 63);
            $dados['lgmensagem'] = substr($mensagem, 0, 511);
            $dados['lgtpmensagem'] = substr($tipo, 0, 1);
            $dados['lgbrowser'] = substr(($_SERVER['HTTP_USER_AGENT']), 0, 254);
            $dados['lgipusuario'] = substr(($_SERVER['REMOTE_ADDR'] . ':' . $_SERVER['REMOTE_PORT']), 0, 254);

            $log->save($dados);
        } catch (Exception $e) {
            echo 'Erro ao inserir log: ' . $e->getMessage();
            die();
        }
    }

    /**
     * Cria arquivo XML apartir de um Array.
     * 
     * @access public 
     * 
     * @param Array $Dados - Array com os dados que serão gerados.
     * @param String $arquivo - Nome do arquivo.
     * @param String $elementos - Nome da lista de elementos.
     * @param String $elemento - Nome do elemento.
     * 
     */
    public static function criaXML(array $Dados, string $arquivo, string $elementos, string $elemento) {

        $filePath = $arquivo;

        $dom = new DOMDocument('1.0', 'utf-8');

        $root = $dom->createElement($elementos);

        foreach ($Dados as $k => $r) {

            $element = $dom->createElement($elemento);

            foreach (array_keys($r) as $key) {

                $field = $key;
                $propertie = $r;

                $linha = $dom->createElement($field, $propertie[$field]);

                $element->appendChild($linha);
            }
            $root->appendChild($element);
        }
        $dom->appendChild($root);

        $dom->save($filePath);


        /*
          http://programmerblog.net/how-to-generate-xml-files-using-php/

          CREATE
          OR REPLACE
          VIEW `viacaosuzano_xml` AS (
          select
          nome as Nome,
          'SSP' as Emissor,
          rg as RG,
          cpf as CPF,
          case when sexo = 'Feminino' then "F" else "M" end as Sexo,
          email as Email,
          '07500-000' as CEP,
          'SP' as Estado,
          cidade as Cidade,
          bairro as Bairro,
          endereco as Endereco,
          facebook as Site,
          celular as Celular,
          telefone as Fax,
          telefone as Telefone,
          nomemae as NomeMae,
          1 as SubTipo,
          1 as PerfilCompra,
          255 as LimiteMensal,
          255 as LimiteMensal2,
          '0-1440=10|0-0=0' as LimitesDomingo,
          '0-1440=10|0-0=0' as LimitesSegunda,
          '0-1440=10|0-0=0' as LimitesTerca,
          '0-1440=10|0-0=0' as LimitesQuarta,
          '0-1440=10|0-0=0' as LimitesQuinta,
          '0-1440=10|0-0=0' as LimitesSexta,
          '0-1440=10|0-0=0' as LimitesSabado
          from
          lteestudantes
          where
          identidade = 45  and excluido = 'nao'
          );
          48
         */
    }

}
