var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
$("select[name=origem]").val(form_values.origem);
$("select[name=idfuncionariosecretaria]").val(form_values.idfuncionariosecretaria);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=idfuncionarioescola]").val(form_values.idfuncionarioescola);
$("select[name=aprovacao]").val(form_values.aprovacao);
$("select[name=confirmacaoaprovacao]").val(form_values.confirmacaoaprovacao);
$("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	




	$('input[name=dias]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});
	$("input[name=datainicio]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datainicio]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=datafim]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datafim]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});




	
	
	rules = {};
	rules.origem = "required";
//rules.idescola = "required";
//rules.idfuncionarioescola = "required";
rules.aprovacao = "required";
//rules.confirmacaoaprovacao = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setOrigem(obj) {
	
	var val = $(obj).val();	
	if(val=="Escola"){
		//$('#calendar').html('');
		$(".box-escola").css("display", "block");
		$(".box-secretaria").css("display", "none");
		//$("select[name=iddepartamentosecretaria]").closest('div').find('span').html('Selecione...');
		
	}else{
		//setPage('inicial');
		$(".box-secretaria").css("display", "block");
		$(".box-escola").css("display", "none");	
		//$("select[name=idescola]").closest('div').find('span').html('Selecione...');
		//$("select[name=idfuncionarioescola]").closest('div').find('span').html('Selecione...');
	}
	
	//$(".calendar_ferias").css("display", "block");
	
	
	
	/*
	$("select").each(function (){
		div = $(this).closest('div').find('.chosen-container').css('width', '100%');
	});
*/
}