var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};
	
	if ((typeof form_values)!='undefined') {
		$("select[name=status1]").val(form_values.status1);
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
	}

	$('.checks').find('input').each(function() {
		td = $(this).closest('td');
		div = td.find('div:first');
		setDiv(div.eq(0));
	});

	rules = {};
	rules.perfil = "required";
	rules.status1 = "required";
	
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;	
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("tr.checks div").css('cursor', 'default');
		$("tr.checks div").attr('onclick', '');
		$("label span.required").remove();
		
	}		
	
});

function setDivClick(div) {
	td = $(div).closest('td');
	input = td.find('input');
	if (input.is(':checked')) {
		input.attr('checked', false);
	} else {
		input.attr('checked', true);
	}
	setDiv(div)
}

function setDiv(div) {
	td = $(div).closest('td');
	input = td.find('input');
	
	$(div).removeClass('icon-ok');
	$(div).removeClass('icon-remove');
	
	if (input.is(':checked')) {
		$(div).addClass('icon-ok');
		$(div).css('color', '#080');
	} else {
		$(div).addClass('icon-remove');
		$(div).css('color', '#800');
	}
}

function displayAll(obj) {
	obj = $(obj);
	trs = obj.closest('thead').find('th');
	
	idx = trs.index(obj)+1;
	
	var cells = $('table.perfis tbody td:nth-child('+idx+') input');
	primeiro = cells.eq(0).is(':checked');
	
	if (primeiro) {
		cells.each(function() {
			$(this).attr('checked', false);
			div = $(this).closest('td').find('div');
			setDiv(div.eq(0));
		});
	} else {
		cells.each(function() {
			$(this).attr('checked', true);
			div = $(this).closest('td').find('div');
			setDiv(div.eq(0));
			
		});
	}
	
}

