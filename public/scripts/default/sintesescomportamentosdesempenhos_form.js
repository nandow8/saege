$(document).ready(function() {
	setSeries($("#escola_id"),0,0,0);
	setDiretores($("#escola_id"),0);
	setCoordenadores($("#escola_id"),0);
	setPeriodonotas($("#escola_id"),0);
});

var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

function setSeries(obj, idserie, idescolavinculo, idaluno, dados){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setseries",{idescola: idescola}, function(data) {

		$('select[name=idserie]').html(data);
		$("select[name=idserie]").val(idserie);
		val = $("select[name=idserie]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idserie!=0) && (idserie!="")){
			$("select[name=idserie]").closest('div').find('span').html('Selecione...');
			
			if((idescolavinculo!=0) && (idescolavinculo!="")){
				setTurmas($("select[name=idserie]").val(idserie), idescolavinculo, idaluno, dados);
			}
		}
	});
}

function setTurmas(obj, idturma, idaluno, dados){

	var idserie = $(obj).val();
	var idescola = $("select[name=idescola]").val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setturmas",{idserie: idserie, idescola:idescola}, function(data) {

		$('select[name=idturma]').html(data); 	
		
		$("select[name=idturma]").val(idturma);
		val = $("select[name=idturma]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idturma!=0) && (idturma!="")){
			$("select[name=idturma]").closest('div').find('span').html('Selecione...');

			setAlunos($("select[name=idturma]").val(idturma), idaluno, dados);
		}
	});
}

function setAlunos(obj, idaluno, dados){	
	var idescolavinculo = $(obj).val();
	var idescola = $("select[name=idescola]").val();
	var idserie = $("select[name=idserie]").val();
	
	$.post(baseUrl + "/" + module + "/"+controller+"/setalunos",{idvinculo: idescolavinculo, idserie: idserie, idescola:idescola}, function(data) {

		$('#content_list_alunos').html(data);

		if(idaluno!=undefined && idaluno != ""){
			var alunos = idaluno.split(',');
			var concentracao = (dados[0]).split(',');
			var aquicisao = (dados[1]).split(',');
			var raciocinio = (dados[2]).split(',');
			var motivacao = (dados[3]).split(',');
			var comportamento = (dados[4]).split(',');
			var relacaointerpessoal = (dados[5]).split(',');
			var cooperacao = (dados[6]).split(',');
			var leitura = (dados[7]).split(',');
			var escrita = (dados[8]).split(',');
			var matematica = (dados[9]).split(',');
			var organizacao = (dados[10]).split(',');
			var assiduidade = (dados[11]).split(',');
			var rendimento = (dados[12]).split(',');
			var infocomplementar = (dados[13]).split(',');
			
			$("[name='idaluno[]']").val(alunos);

			var i = 0;
			$("[name='idaluno[]']").each(function (){
				setCampos(this);

				if($(this).attr("checked") == "checked") {
					$(this).parent().parent().find("[name='concentracao[]']").val(concentracao[i]);
					$(this).parent().parent().find("[name='aquicisao[]']").val(aquicisao[i]);
					$(this).parent().parent().find("[name='raciocinio[]']").val(raciocinio[i]);
					$(this).parent().parent().find("[name='motivacao[]']").val(motivacao[i]);
					$(this).parent().parent().find("[name='comportamento[]']").val(comportamento[i]);
					$(this).parent().parent().find("[name='relacaointerpessoal[]']").val(relacaointerpessoal[i]);
					$(this).parent().parent().find("[name='cooperacao[]']").val(cooperacao[i]);
					$(this).parent().parent().find("[name='leitura[]']").val(leitura[i]);
					$(this).parent().parent().find("[name='escrita[]']").val(escrita[i]);
					$(this).parent().parent().find("[name='matematica[]']").val(matematica[i]);
					$(this).parent().parent().find("[name='organizacao[]']").val(organizacao[i]);
					$(this).parent().parent().find("[name='assiduidade[]']").val(assiduidade[i]);
					$(this).parent().parent().find("[name='rendimento[]']").val(rendimento[i]);
					$(this).parent().parent().find("[name='infocomplementar[]']").val(infocomplementar[i]);
					i++;
				}
			});
		}
	});
}

function setProfessores(obj , idprofessor){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setprofessores",{idescola: idescola}, function(data) {
		$('select[name=idprofessor]').html(data);
		
		$("select[name=idprofessor]").val(idprofessor);
		val = $("select[name=idprofessor]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idprofessor!=0) && (idprofessor!="")){
			$("select[name=idprofessor]").closest('div').find('span').html('Selecione...');
		}

	});
}

function setCoordenadores(obj , idcoordenador){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setcoordenadores",{idescola: idescola}, function(data) {
		$('select[name=idcoordenador]').html(data);
		
		$("select[name=idcoordenador]").val(idcoordenador);
		val = $("select[name=idcoordenador]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idcoordenador!=0) && (idcoordenador!="")){
			$("select[name=idcoordenador]").closest('div').find('span').html('Selecione...');
		}

	});
}

function setDiretores(obj , iddiretor){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setdiretores",{idescola: idescola}, function(data) {
		$('select[name=iddiretor]').html(data);
		
		$("select[name=iddiretor]").val(iddiretor);
		val = $("select[name=iddiretor]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((iddiretor!=0) && (iddiretor!="")){
			$("select[name=iddiretor]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setPeriodo(obj, idperiodo) {
	var idescola = $(obj).val();

	$.post(baseUrl + "/" + module + "/"+controller+"/setperiodo",{idescola: idescola}, function(data) {

		$("select[name=periodo]").html(data);

		if(idperiodo != 0){
			$("select[name=periodo]").val(idperiodo);
			$("select[name=periodo]").find("option:selected").html();
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idperiodo!=0) && (idperiodo!="")){
			$("select[name=periodo]").closest('div').find('span').html('Selecione...');
		}
	});
}

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};		

	if ((typeof form_values)!='undefined') {

		var dados = [form_values.concentracao , form_values.aquicisao , form_values.raciocinio , 
		form_values.motivacao , form_values.comportamento, 
		form_values.relacaointerpessoal, form_values.cooperacao, form_values.leitura, form_values.escrita, 
		form_values.matematica, form_values.organizacao, form_values.assiduidade, form_values.rendimento, 
		form_values.infocomplementar ];

		$("select[name=periodo]").val(form_values.periodo);
		$("select[name=idescola]").val(form_values.idescola);
		setSeries($("select[name=idescola]").val(form_values.idescola), form_values.idserie, form_values.idturma, form_values.idaluno, dados);
		setDiretores($("select[name=idescola]").val(form_values.idescola) , form_values.iddiretor);
		setCoordenadores($("select[name=idescola]").val(form_values.idescola) , form_values.idcoordenador);
		setProfessores($("select[name=idescola]").val(form_values.idescola) , form_values.idprofessor);		
		setPeriodo($("select[name=idescola").val(form_values.idescola), form_values.periodo);
		$("select[name=status1]").val(form_values.status1);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});	
	}
	
	rules = {};
	rules.idescola = "required";
	rules.sequencial = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	

	//visualizar = false;
	if (visualizar) {
		setTimeout( function() {		
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').not('[type=checkbox]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;		
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});

		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='Selecione...') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');
		});	
		
		$("label span.required").remove();
		$("[type=checkbox]").attr("disabled",true);},750);
	}

	$("input[name=sequencial]").mask("99/9999", {placeholder:" "});

	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});	
});