var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=idsala]").val(form_values.idsala);
$("select[name=idserie]").val(form_values.idserie);
$("select[name=idturma]").val(form_values.idturma);
$("select[name=idperiodo]").val(form_values.idperiodo);
$("select[name=idprofessor]").val(form_values.idprofessor);
$("select[name=idtipoensino]").val(form_values.idtipoensino);
$("select[name=turno]").val(form_values.turno);
$("select[name=integracao]").val(form_values.integracao);
$("select[name=status1]").val(form_values.status1);
		if ((typeof form_values.idescola)!='undefined') {
			//jQuery("select[name=idescola]").val(form_values.idescola);
			setSalas(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idsala);
			setSeries(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idserie);
			setTurmas(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idturma);
			setPeriodos(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idperiodo);
			setFuncionariosEscolas(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idprofessor, form_values.idsmaterias);
			
			
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	






	$("input[name=ano]").mask("9999", {placeholder:" "});
	$("input[name=horarioinicio]").mask("99:99", {placeholder:" "});
	$("input[name=horariofim]").mask("99:99", {placeholder:" "});

	$("input[name=inicioaulas]").mask("99/99/9999", {placeholder:" "});
	$("input[name=inicioaulas]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=terminoaulas]").mask("99/99/9999", {placeholder:" "});
	$("input[name=terminoaulas]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$('input[name=ano]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});


	
	
	rules = {};
	//rules.idescola = "required";
rules.idsala = "required";
rules.idserie = "required";
rules.idturma = "required";
rules.idperiodo = "required";
rules.idprofessor = "required";
rules.idtipoensino = "required";
rules.inicioaulas = "required";
rules.terminoaulas = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});



function setDados(obj, idescola){
	var val = $(obj).val();
	
	setSalas($("select[name=idescola]").val(val), 0);
	setSeries($("select[name=idescola]").val(val), 0);
	setTurmas($("select[name=idescola]").val(val), 0);
	setPeriodos($("select[name=idescola]").val(val), 0);
	setFuncionariosEscolas($("select[name=idescola]").val(val), 0, 0);
	//setMaterias(jQuery("select[name=idprofessor]").val(form_values.idprofessor), 0);
}

function setSalas(obj, idsala){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idsala]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + 'escolasvinculos/setsalas/', {idescola:0}, function(data) {
		$("select[name=idsala]").html(data);
		$("select[name=idsala]").val(idsala);
		val = $("select[name=idsala]").find('option:selected').html();
		$("select[name=idsala]").closest('div').find('span').html(val);
	
		$("select[name=idsala]").trigger("chosen:updated");
		
		if(idsala==""){
			$("select[name=idsala]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setSeries(obj, idserie){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idserie]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + 'escolasvinculos/setseries/', {idescola:0}, function(data) {
		$("select[name=idserie]").html(data);
		$("select[name=idserie]").val(idserie);
		val = $("select[name=idserie]").find('option:selected').html();
		$("select[name=idserie]").closest('div').find('span').html(val);
	
		$("select[name=idserie]").trigger("chosen:updated");
		
		setVinculos(jQuery("select[name=idserie]").val(idserie), 0);

		if(idserie==""){
			$("select[name=idserie]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setTurmas(obj, idturma){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idturma]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + 'escolasvinculos/setturmas/', {idescola:0}, function(data) {
		$("select[name=idturma]").html(data);
		$("select[name=idturma]").val(idturma);
		val = $("select[name=idturma]").find('option:selected').html();
		$("select[name=idturma]").closest('div').find('span').html(val);
	
		$("select[name=idturma]").trigger("chosen:updated");
		
		if(idturma==""){
			$("select[name=idturma]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setPeriodos(obj, idperiodo){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idperiodo]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + 'escolasvinculos/setperiodos/', {idescola:0}, function(data) {
		$("select[name=idperiodo]").html(data);
		$("select[name=idperiodo]").val(idperiodo);
		val = $("select[name=idperiodo]").find('option:selected').html();
		$("select[name=idperiodo]").closest('div').find('span').html(val);
	
		$("select[name=idperiodo]").trigger("chosen:updated");
		
		if(idperiodo==""){
			$("select[name=idperiodo]").closest('div').find('span').html('Selecione...');
		}
	});
}


function setFuncionariosEscolas(obj, idprofessor, idsmaterias){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idprofessor]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + 'escolasvinculos/setfuncionariosescolas/', {idescola:0}, function(data) {
		$("select[name=idprofessor]").html(data);
		$("select[name=idprofessor]").val(idprofessor);
		val = $("select[name=idprofessor]").find('option:selected').html();
		$("select[name=idprofessor]").closest('div').find('span').html(val);
	
		$("select[name=idprofessor]").trigger("chosen:updated");
		
		if(idprofessor==""){
			$("select[name=idprofessor]").closest('div').find('span').html('Selecione...');
		}
		
		setMaterias(jQuery("select[name=idprofessor]").val(idprofessor), idsmaterias);
	});
}

function setMaterias(obj, idmateria){
	var val = $(obj).val();
	
	data = '';
	$.post(baseUrl + 'escolasvinculosprofessores/setmaterias/', {idprofessor:val}, function(data) {
		$("#idmateria").html(data);
		if ((idmateria!="") && (idmateria!="0")){
			$("[name='idsmaterias[]']").val(idmateria)
		}
	});
}

function setVinculos(obj, idvinculos){
	var val = $(obj).val();
	var idescola = 0;
	//var idperiodo = $('select[name=idperiodo]').val();
	data = '';
	
	$.post(baseUrl + 'escolasvinculosprofessores/setvinculos/', {idserie:val, idescola:idescola}, function(data) {
		$(".box-salas").html(data);		
		setProfessores($("select[name=idserie]").val(val), 0);
	});

}

function setProfessores(obj, idsala){
	var val = $(obj).val();
	var idescola = 0;
	//var idperiodo = $('select[name=idperiodo]').val();
	var professorcomplete = $('input[name=professorcomplete]').val();
	
	var notids = "";
	var verifica = false;
	$("input[name='idsprofessores[]']").each(function() {
		//console.log('notids -> ' + notids);
		if(verifica==false){
			notids = $(this).val();
			verifica = true;
		}else if(verifica==true){
			notids = notids + ',' + $(this).val();
		}
	});
	
	console.log('notids -> ' + notids);
	data = '';
	
	$.post(baseUrl + 'escolasvinculosprofessores/setprofessores', {idserie:val, idescola:idescola, professorcomplete:professorcomplete, notids:notids}, function(data) {
		$(".box-professors").html(data);
	});
}

function ExcluirProfessor(obj, idprofessor, tipo){
	var idserie = $("select[name=idserie]").val();	
	var iditem = $(obj).parent().parent().find("input[name='idsprofessoresmaterias[]']").val();
	var idsvinculos = $(obj).parent().parent().find("input[name='idsvinculos[]']").val();
	var path_box = $(obj).parent().parent().parent();
	jConfirm('Confirmar a exclusão do professor desta sala?', 'Excluir professor', function(r) {
		if (r) {
			if(tipo=="repopula"){
				$.post(baseUrl + 'escolasvinculosprofessores/excluiritem/', {iditem:iditem, idserie:idserie, idsvinculos:idsvinculos}, function(data) {
					var item =  $('.professor_' + idprofessor);	
					//item.removeClass("off");
					//item.addClass("on");
					//setAlunos($("select[name=idserie]").val(idserie), 0);	
					$(obj).parent().parent().remove();	
					
				});
			}else{
		        var item =  $('.professor' + idprofessor);		        
				item.removeClass("off");
				item.addClass("on");
				console.log('idprofessor: ' + idprofessor);
				$(obj).parent().parent().remove();	
			}
			$qtdatual = $(path_box).find(".qtdatual").val();			
			$qtdatual = parseInt($qtdatual) - 1;			
			$(path_box).find(".qtdatual").val($qtdatual);
		}
 	}); 
	
}