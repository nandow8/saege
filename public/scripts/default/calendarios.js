﻿$(document).ready(function() {
    $(window).resize(function() {
		var width = $(".fc-week .fc-first").width();
		var height = width - 15;

    });

});

function setPage(tipo){
	if(tipo=="inicial"){
		count_page = 0;
	}else if(tipo=="mais"){
		count_page++;
	}else if(tipo=="menos"){
		count_page--;
	}
	console.log('count_page: ' + count_page);
	$.post(baseUrl + "/"+controller+"/setcalendario", {page: count_page}, function(data) {
		if (data!="erro") {
			$('#calendar_p').html('');
			$('#calendar_p').html(data);
			
			if(count_page!=0){
				$('.fc-button-today').removeClass('fc-state-disabled');
				$('.fc-button-today').css('font-weight', 'bold');
			}else{
				$('.fc-button-today').addClass('fc-state-disabled');
				$('.fc-button-today').css('font-weight', 'normal');
			}
	
		} else {
			jAlert(data, "Erro!");
		}
	});
	
}

function setEventos(obj, dia){
	$.fancybox({
		type: 'iframe',
		href: baseUrl + "/"+controller+"/seteventosdia/dia/" + dia,
		width: 800,
		height: 600
	});
}

function setEventoById(id){
	$.fancybox({
		type: 'iframe',
		href: baseUrl + "/"+controller+"/setevento/id/" + id,
		width: 800,
		height: 600
	});
}