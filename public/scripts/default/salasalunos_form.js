﻿var unsaved = false;
$("input,select,textarea").change(function() { //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function() { //trigers change in all input fields including text type
    unsaved = true;
});

jQuery(document).ready(function() {
    window.onbeforeunload = function() {
        console.log(unsaved);
        if (unsaved) {
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };

    if ((typeof form_values) != 'undefined') {
        if ((typeof form_values.status1) != 'undefined')
            $("select[name=status1]").val(form_values.status1);
        //$("select").trigger("chosen:updated");
        if ((typeof form_values.idescola) != 'undefined') {
            jQuery("select[name=idescola]").val(form_values.idescola);
            $("select[name=idescola]").val(form_values.idserie);
            //setSeries($("select[name=idescola]").val(form_values.idescola), form_values.idserie, form_values.idperiodo);
            setPeriodos($("select[name=idserie]").val(form_values.idescola), form_values.idperiodo);
            setVinculos(jQuery("select[name=idserie]").val(form_values.idserie), 0);
            //setClassificacoes($("select[name=idescola]").val(form_values.idescola), 0);
        }
        ;



        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function() {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

    }

    $("input[name=alunocomplete]").keyup(function() {
        var idserie = $('select[name=idserie]').val();
        setAlunos($("select[name=idserie]").val(idserie), 0);
    });

    jQuery('select, input, textarea').bind('blur', function() { });

    rules = {};
    rules.idescola = "required";
    rules.status = "required";

    $("#form").validate({
        ignore: [],
        rules: rules,
        errorPlacement: function(error, element) {
            if (element.is('select')) {
                error.insertAfter($(element).parent().find('div.chosen-container'));
            } else {
                error.insertAfter($(element));
            }

        },
        submitHandler: function(form) {
            unsaved = false;
            form.submit();
        }
    });


});

function setDados(obj, idescola) {
    var val = $(obj).val();

}

function setClassificacao(obj) {
    var idserie = $('select[name=idserie]').val();
    //alert(idserie);
    setAlunos($("select[name=idserie]").val(idserie), 0);
}

function setVinculos(obj, idvinculos) {
    var idescolasala = $('input[name=id]').val();
    var val = $(obj).val();
    var idescola = $('select[name=idescola]').val();
    var idperiodo = $('select[name=idperiodo]').val();
    data = '';

    $.post(baseUrl + 'escolassalasalunos/setvinculos/', {idserie: val, idescola: idescola, idescolasalasaluno: idescolasala, idperiodo: idperiodo}, function(data) {
        $(".box-salas").html(data);
        var teste = $("select[name=idserie]").val(val);

        setAlunos($("select[name=idserie]").val(val), 0);

    });
}

function setAlunos(obj, idsala) {
    var val = $(obj).val();
    var idescolasala = $('input[name=id]').val();
    var idescola = 1;
    var classificacao = $('select[name=classificacao]').val();
    var idperiodo = $('select[name=idperiodo]').val();
    var alunocomplete = $('input[name=alunocomplete]').val();

    var notids = "";
    var verifica = false;
    $("input[name='idsalunos[]']").each(function() {
        //console.log('notids -> ' + notids);
        if (verifica == false) {
            notids = $(this).val();
            verifica = true;
        } else if (verifica == true) {
            notids = notids + ',' + $(this).val();
        }
    });

    console.log('notids -> ' + notids);
    data = '';

    $.post(baseUrl + 'escolassalasalunos/setalunos/', {idserie: val, idescola: idescola, classificacao: classificacao, idperiodo: idperiodo, alunocomplete: alunocomplete, idescolasala: idescolasala, notids: notids}, function(data) {
        $(".box-alunos").html(data);
    });
}

function setSeries(obj, idserie, idperiodo) {
    setPeriodos(obj, idperiodo);
    var val = $(obj).val();
    setClassificacoes($("select[name=idescola]").val(val), 0);

    data = '';
    $("select[name=idserie]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + 'escolassalasalunos/setseries/', {idescola: 0, action: action}, function(data) {
        $("select[name=idserie]").html(data);
        $("select[name=idserie]").val(idserie);
        val = $("select[name=idserie]").find('option:selected').html();
        $("select[name=idserie]").closest('div').find('span').html(val);

        $("select[name=idserie]").trigger("chosen:updated");

        setVinculos(jQuery("select[name=idserie]").val(idserie), 0);

        if (idserie == "") {
            $("select[name=idserie]").closest('div').find('span').html('Selecione...');
        }
    });
}

function setPeriodos(obj, idperiodo) {
    var val = $(obj).val();

    data = '';
    $("select[name=idperiodo]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + 'escolassalasalunos/setperiodos/', {idescola: 1}, function(data) {
        $("select[name=idperiodo]").html(data);
        $("select[name=idperiodo]").val(idperiodo);
        console.log('idperiodo: ' + idperiodo)
        val = $("select[name=idperiodo]").find('option:selected').html();
        $("select[name=idperiodo]").closest('div').find('span').html(val);

        $("select[name=idperiodo]").trigger("chosen:updated");

        if (idperiodo == "") {
            $("select[name=idperiodo]").closest('div').find('span').html('Selecione...');
        }
    });
}

function resetVinculo() {/*
 $("input[name='idsalunos[]']").each(function() {
 notids = $(this).val('');
 });*/
    var idserie = $('select[name=idserie]').val();
    var idperiodo = $('select[name=idperiodo]').val();

    setVinculos($("select[name=idserie]").val(idserie), 0);
}

function ExcluirAluno(obj, idaluno, tipo) {
    var idserie = $("select[name=idserie]").val();
    var iditem = $(obj).parent().parent().find("input[name='idsitens[]']").val();
    var path_box = $(obj).parent().parent().parent();
    jConfirm('Confirmar a exclusão do aluno desta sala?', 'Excluir aluno', function(r) {
        if (r) {
            if (tipo == "repopula") {
                $.post(baseUrl + 'escolassalasalunos/excluiritem/', {iditem: iditem, idserie: idserie}, function(data) {
                    var item = $('.aluno_' + idaluno);
                    item.removeClass("off");
                    item.addClass("on");
                    //setAlunos($("select[name=idserie]").val(idserie), 0);
                    $(obj).parent().parent().remove();

                });
            } else {
                var item = $('.aluno_' + idaluno);
                item.removeClass("off");
                item.addClass("on");
                console.log('idaluno: ' + idaluno);
                $(obj).parent().parent().remove();
            }
            $qtdatual = $(path_box).find(".qtdatual").val();
            $qtdatual = parseInt($qtdatual) - 1;
            $(path_box).find(".qtdatual").val($qtdatual);
        }
    });

}

function setClassificacoes(obj, classificacao) {
    var val = $(obj).val();

    data = '';
    $("select[name=classificacao]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + 'escolasalunos/setclassificacoes/', {idescola: val}, function(data) {
        $("select[name=classificacao]").html(data);
        $("select[name=classificacao]").val(classificacao);
        val = $("select[name=classificacao]").find('option:selected').html();
        $("select[name=classificacao]").closest('div').find('span').html(val);

        $("select[name=classificacao]").trigger("chosen:updated");

        if (classificacao == "") {
            $("select[name=classificacao]").closest('div').find('span').html('Selecione...');
        }
    });
}