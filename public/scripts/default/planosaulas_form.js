var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
   // unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
  //  unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
$("select[name=idescolavinculo]").val(form_values.idescolavinculo);
$("select[name=idmateria]").val(form_values.idmateria);
$("select[name=idprofessor]").val(form_values.idprofessor);
$("select[name=idaluno]").val(form_values.idaluno);
$("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	




	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	
	
	rules = {};
//	rules.idescola = "required";
rules.idmateria = "required";
//rules.idprofessor = "required";
//rules.idaluno = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setMes(mes, id){
	$.post(baseUrl + 'escolasalunosfaltas/setmes/', {mes:mes}, function(data) {
		window.location = baseUrl + "/escolasalunosfaltas/editar/id/" + id;
		$('.nomemes').html('');
		$('.nomemes').html(data);
		setFaltas($('select[name=idmateria]'), id)
	});
}

function setMaterias(obj, idescolavinculo, idmateria){
	var idprofessor = $(obj).val();
	$("#box-faltas").html("");
	data = '';
	$("select[name=idmateria]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + 'escolasalunosfaltas/setmaterias/', {idprofessor:idprofessor, idescolavinculo:idescolavinculo}, function(data) {
		$("select[name=idmateria]").html(data);
		$("select[name=idmateria]").val(idmateria);
		console.log(idmateria);
		val = $("select[name=idmateria]").find('option:selected').html();
		$("select[name=idmateria]").closest('div').find('span').html(val);
	
		$("select[name=idmateria]").trigger("chosen:updated");
		
		if(idmateria==""){
			$("select[name=idmateria]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setFaltas(obj, idescolavinculo){
	var idmateria = $(obj).val();
	var idprofessor = $('select[name=idprofessor]').val();

	data = '';
	$.post(baseUrl + 'planosaulas/setfaltas/', {idprofessor:idprofessor, idmateria:idmateria, idescolavinculo:idescolavinculo}, function(data) {
		$("#box-faltas").html(data);
		//idmateria = parseInt(idmateria);
		
		if((idmateria<=0) || (idmateria=="NaN") || (idmateria=="")){
			$("#box-faltas").html("");
		}
	});
}

function removePlano(obj, idfalta, dia, idescolavinculo, mes){
	jConfirm('Remover plano de aula?', 'Remover Plano', function(r) {
		if (r) {
			$.post(baseUrl + 'planosaulas/removeplano/', {id:idfalta}, function(data) {
				if(data=="OK") {
					$(obj).removeAttr('onclick');
					$(obj).css('background-color', '#FF4C4C');
					$(obj).attr('onclick',"enviaplano(this, '"+dia+"', '"+mes+"', '"+idescolavinculo+"')");					
					$(obj).find('i').remove();
					jAlert("Plano Removido com sucesso!", "Sucesso!");
				}
			});
		}
	});
}


function enviaplano(obj, dia, mes, idescolavinculo){
	var idmateria = $('select[name=idmateria]').val();
	var idprofessor = $('select[name=idprofessor]').val();
	var id = $(obj).find('.idsfaltas').val();
	var titulo = $(obj).parent().parent().find('.titulo').val();
	var texto = $(obj).parent().parent().find('.texto').val();

	data = '';
	$.post(baseUrl + 'planosaulas/addplano/', {idprofessor:idprofessor, idmateria:idmateria, idescolavinculo:idescolavinculo, dia:dia, mes:mes, titulo:titulo, texto:texto}, function(data) {
		var data_resp = JSON.parse(data);
		
		if(data_resp.data=="OK") {
			$('.quadro_'+dia).each(function() {
				$(this).css('background-color', '#9AED8C');
				$(this).removeAttr('onclick');
				$(this).attr('onclick',"removePlano(this, '"+data_resp.id+"','"+dia+"', '"+idescolavinculo+"', '"+mes+"')");				
			});

			jAlert(data_resp.mensagem, data_resp.titulo);
		} else {
			jAlert(data_resp.mensagem, data_resp.titulo);
		}
	});



}