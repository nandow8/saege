$(document).ready(function() {
	setSeries($("#escola_id"),0,0,0);
	setDiretores($("#escola_id"),0);
	setCoordenadores($("#escola_id"),0);
	setPeriodonotas($("#escola_id"),0);
});

var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

function setSeries(obj, idserie, idescolavinculo, idaluno, periodo, inicial){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setseries",{idescola: idescola}, function(data) {

		$('select[name=idserie]').html(data);
		$("select[name=idserie]").val(idserie);
		val = $("select[name=idserie]").find('option:selected').html();
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
		if((idserie!=0) && (idserie!="")){
			$("select[name=idserie]").closest('div').find('span').html('Selecione...');
			
			if((idescolavinculo!=0) && (idescolavinculo!="")){
				setTurmas($("select[name=idserie]").val(idserie), idescolavinculo, idaluno,periodo, inicial);
			}
		}
	});
}

function setTurmas(obj, idturma, idaluno, periodo, inicial){
	var idserie = $(obj).val();
	var idescola = $("select[name=idescola]").val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setturmas",{idserie: idserie, idescola:idescola}, function(data) {

		$('select[name=idturma]').html(data); 	
		
		$("select[name=idturma]").val(idturma);
		val = $("select[name=idturma]").find('option:selected').html();
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idturma!=0) && (idturma!="")){
			$("select[name=idturma]").closest('div').find('span').html('Selecione...');
			// if((idaluno!=0) && (idaluno!="")){
			setAlunos($("select[name=idturma]").val(idturma), idaluno, periodo, inicial);
			// }
		}
	});
}

function setAlunos(obj, idaluno, periodo, iniciais){	

	var idescolavinculo = $(obj).val();
	var idescola = $("select[name=idescola]").val();
	var idserie = $("select[name=idserie]").val();
	setPeriodo(idescolavinculo, periodo);
	$.post(baseUrl + "/" + module + "/"+controller+"/setalunos",{idvinculo: idescolavinculo, idserie: idserie, idescola:idescola}, function(data) {

		$('#content_list_alunos').html(data);

		if(idaluno!=undefined && idaluno != ""){
			var alunos = idaluno.split(',');

			var al = $("[name='idaluno[]']").val(alunos);

			$("[name='idaluno[]']").each(function (){
				setCampos(this);
			});

			if(iniciais != null){
				var i = 0;
				$("input[name='inicial[]']").each(function() {
					$(this).val(iniciais[i]);
					i++;
				});
			}
		}
	});
}

function setPeriodo(idvinculo, periodo){
	$.post(baseUrl + "/" + module + "/"+controller+"/setperiodo",{idvinculo: idvinculo}, function(data) {

		$('select[name=idperiodo]').html(data);
		$("select[name=idperiodo]").val(periodo);
		val = $("select[name=idperiodo]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

	});
}

function setCoordenadores(obj , idcoordenador){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setcoordenadores",{idescola: idescola}, function(data) {
		$('select[name=idcoordenador]').html(data);
		
		$("select[name=idcoordenador]").val(idcoordenador);
		val = $("select[name=idcoordenador]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idcoordenador!=0) && (idcoordenador!="")){
			$("select[name=idcoordenador]").closest('div').find('span').html('Selecione...');
		}

	});
}

function setDiretores(obj , iddiretor){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setdiretores",{idescola: idescola}, function(data) {
		$('select[name=iddiretor]').html(data);
		
		$("select[name=iddiretor]").val(iddiretor);
		val = $("select[name=iddiretor]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((iddiretor!=0) && (iddiretor!="")){
			$("select[name=iddiretor]").closest('div').find('span').html('Selecione...');
		}

	});
}

function setPeriodonotas(obj, idperiodo) {
	var idescola = $(obj).val();

	$.post(baseUrl + "/" + module + "/"+controller+"/setperiodonotas",{idescola: idescola}, function(data) {

		$("select[name=periodonotas]").html(data);

		if(idperiodo != 0){
			$("select[name=periodonotas]").val(idperiodo);
			$("select[name=periodonotas]").find("option:selected").html();
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idperiodo!=0) && (idperiodo!="")){
			$("select[name=periodonotas]").closest('div').find('span').html('Selecione...');
		}
	});
}

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};		

	if ((typeof form_values)!='undefined') {
		iniciais = (form_values.inicial != "vazio") ? form_values.inicial.split(',') : null;

		$("select[name=idusuariologado]").val(form_values.idusuariologado);
		$("select[name=idescola]").val(form_values.idescola);
		setSeries($("select[name=idescola]").val(form_values.idescola), form_values.idserie, form_values.idturma, form_values.idaluno, form_values.idperiodo, iniciais);
		setDiretores($("select[name=idescola]").val(form_values.idescola) , form_values.iddiretor);
		setCoordenadores($("select[name=idescola]").val(form_values.idescola) , form_values.idcoordenador);
		setPeriodonotas($("select[name=idescola]").val(form_values.idescola), form_values.periodonotas);

		$("select[name=status1]").val(form_values.status1);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	rules = {};
	rules.idescola = "required";
	rules.sequencial = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	

	//visualizar = false;
	if (visualizar) {
		setTimeout( function() {		
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').not('[type=checkbox]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;		
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});

		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='Selecione...') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');
		});	
		
		$("label span.required").remove();
		$("[type=checkbox]").attr("disabled",true);},750);
	}

	$("input[name=sequencial]").mask("99/9999", {placeholder:" "});
	$("input[name=horalancamento]").mask("99:99", {placeholder:" "});

	$("input[name=datalancamento]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalancamento]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});	
});