$(document).ready(function() {
	
	rules = {};
	rules.email = {required: true,email: true};
	rules.senha = "required";
		
	
	$("#form").validate({
		rules: rules,		
		submitHandler: function(form) {
			form.submit();
		}
	});	
	
	$("#esqueci").click(function() {
		
		jPrompt("Informe seu email", $("input[name=email]").val(), "Esqueci a minha senha", function(r) {
			if (r) {
				$.post(baseUrl + "/auth/esqueci", 
					{email: r}, function(data) {
						alert(data);
				})
			}
		});
		
	});		
	
});
