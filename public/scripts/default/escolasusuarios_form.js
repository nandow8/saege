﻿var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};
	
	if ((typeof form_values)!='undefined') {
		
		$("select[name=idcargo]").val(form_values.idcargo);

		if(form_values.idescola){			
			$("select[name=idescola]").val(form_values.idescola);
			$("select[name=recebesolicitacoes]").val(form_values.recebesolicitacoes);
			$("select[name=idprefeitura]").val(form_values.idprefeitura);
			$("select[name=enviaemails]").val(form_values.enviaemails);
			if ((typeof form_values.idescolas)!='undefined') $("[name='idescolas[]']").val(form_values.idescolas);
			setDepartamentos($("select[name=enviaemails]").val(form_values.enviaemails), form_values.idsdepartamentos);
			if ((typeof form_values.idsdepartamentos)!='undefined') $("[name='idsdepartamentos[]']").val(form_values.idsdepartamentos);
			if ((typeof form_values.professor)!='undefined') setCampos($("select[name=professor]").val(form_values.professor), form_values.professor);
			setPerfil($("select[name=idescola]").val(form_values.idescola), form_values.idperfil);
			setCargo($("select[name=iddepartamento]").val(form_values.iddepartamento), form_values.idcargo);

			//$("select[name=idperfil]").val(form_values.idperfil);
		}
		$("select[name=status1]").val(form_values.status1);
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}

	var v_professor;
    $("select[name=professor]").change(function(){
        v_professor = $(this).val();
		console.log('v_professor: ' + v_professor);
    }).change();
	
	rules = {};
	rules.nomerazao = "required";
	rules.sobrenomefantasia = "required";
	rules.idprefeitura = "required";
	rules.rgf = "required";
	rules.professor = "required";
	rules.email = {required: true,email: true};
	rules.senha = {minlength: 6};
	rules.resenha = {
	      equalTo: "[name=senha]"
	};
	//rules.idperfil = "required";
	rules.idperfil = {
		required: function () {
			return v_professor == "Não";
		}
	}
	rules.iddepartamento = {
		required: function () {
			return v_professor == "Não";
		}
	}
	rules.idcargo = {
		required: function () {
			return v_professor == "Não";
		}
	}
	rules.idescola = {
		required: function () {
			return v_professor == "Não";
		}
	}
	
	rules.status1 = "required";
	
	if (action=='adicionar') {
		rules.senha = "required";
	}
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				//$(element).parent().find('div').append(error);
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
		    
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});	
	
	
});

function setPerfil(obj, idperfil){
	var val = $(obj).val();
	data = '';
	$("select[name=idperfil]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasusuarios/setperfil/', {idescola:val}, function(data) {
		$("select[name=idperfil]").html(data);
		$("select[name=idperfil]").val(idperfil);
		val = $("select[name=idperfil]").find('option:selected').html();
		$("select[name=idperfil]").closest('div').find('span').html(val);
	
		$("select[name=idperfil]").trigger("chosen:updated");
		
		if(idperfil==""){
			$("select[name=idperfil]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setCargo(obj, idcargo){
	var val = $(obj).val();
	data = '';
	$("select[name=idcargo]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasusuarios/setcargos/', {iddepartamento:val}, function(data) {
		$("select[name=idcargo]").html(data);
		$("select[name=idcargo]").val(idcargo);
		val = $("select[name=idcargo]").find('option:selected').html();
		$("select[name=idcargo]").closest('div').find('span').html(val);
	
		$("select[name=idcargo]").trigger("chosen:updated");
		
		if(idcargo==""){
			$("select[name=idcargo]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setDepartamentos(obj, idsdepartamentos){
	var enviaemails = $(obj).val();
	var professor = $('select[name=professor]').val();
	var idescola = $('select[name=idescola]').val();
	data = '';
	if((enviaemails=="Departamentos") && (professor!="Sim")){
		$('.campos_departamentos').css('display', 'block');
		$.post(baseUrl + '/admin/escolasusuarios/setdepartamentos/', {enviaemails:enviaemails, idescola:idescola, professor:professor}, function(data) {
			$(".content_departamentos").html(data);
			if ((typeof idsdepartamentos)!='undefined') $("[name='idsdepartamentos[]']").val(idsdepartamentos);
		});
	}else{
		$('.campos_departamentos').css('display', 'none');

	}

}

function setCampos(obj, professor){	
	/*rules = {};
	rules.nomerazao = "required";
	rules.sobrenomefantasia = "required";
	rules.rgf = "required";
	rules.professor = "required";
	rules.email = {required: true,email: true};
	rules.senha = {minlength: 6};
	rules.resenha = {
		  equalTo: "[name=senha]"
	};	
	*/
	var val = $(obj).val();
	if(val=="Sim"){
		$('.campos_professores').css('display', 'block');
		$('.campos_nao_professores').css('display', 'none');
/*
		rules = {};
		rules.nomerazao = "required";
		rules.sobrenomefantasia = "required";
		rules.rgf = "required";
		rules.professor = "required";
		rules.email = {required: true,email: true};
		rules.senha = {minlength: 6};
		rules.resenha = {
			  equalTo: "[name=senha]"
		};	*/
		
	}else {
		$('.campos_professores').css('display', 'none');
		$('.campos_nao_professores').css('display', 'block');
/*
		rules.idperfil = "required";
		rules.iddepartamento = "required";
		rules.idcargo = "required";
		rules.idescola = "required"; */
	}
	/*rules.status1 = "required";
	if (action=='adicionar') {
		rules.senha = "required";
	}*/
	/*
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				//$(element).parent().find('div').append(error);
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
			
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});	
	*/
	if(professor){
		$("select[name=professor]").val(professor);
	}
}
