var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=idescolaaluno]").val(form_values.idescolaaluno);
$("select[name=status1]").val(form_values.status1);
$("select[name=idserie]").val(form_values.idserie);
setAlunos($("select[name=idserie]").val(form_values.idserie), form_values.idescolaaluno)
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	


	$('input[name=peso]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '.',
	    centsLimit: 2
	});
	$('input[name=altura]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '.',
	    centsLimit: 2
	});
	$('input[name=imc]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '.',
	    centsLimit: 2
	});
	$("input[name=datamedicao]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datamedicao]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	
	
	rules = {};
	rules.idescola = "required";
rules.idescolaaluno = "required";
rules.datamedicao = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


function calculoimc(){
	var peso = $('input[name=peso]').val();
	var altura = $('input[name=altura]').val();
	$.post(baseUrl + '/nutricoesimcs/calculaimc/', {peso:peso, altura:altura}, function(data) {
		$('input[name=imc]').val(data);
	});
}

function setAlunos(obj, idescolaaluno){
	var idescola = $(obj).val();
	
	data = '';
	$("select[name=idescolaaluno]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/nutricoesimcs/setescolasalunos/', {idescola:idescola}, function(data) {
		$("select[name=idescolaaluno]").html(data);

		$("select[name=idescolaaluno]").val(idescolaaluno);
		val = $("select[name=idescolaaluno]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idescolaaluno==""){
			$("select[name=idescolaaluno]").closest('div').find('span').html('Selecione...');
		}
	});
}