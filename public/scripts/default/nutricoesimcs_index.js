$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=idescolaaluno]").val(form_values.idescolaaluno);
$("select[name=tipoimc]").val(form_values.tipoimc);
$("select[name=status1]").val(form_values.status1);
	

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});	
	}
	
	


	$('input[name=peso]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '.',
	    centsLimit: 2
	});
	$('input[name=altura]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '.',
	    centsLimit: 2
	});
	$('input[name=imc]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '.',
	    centsLimit: 2
	});
	$("input[name=datamedicao_i],input[name=datamedicao_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datamedicao_i],input[name=datamedicao_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});


	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Ativo") || (data=="Bloqueado")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 