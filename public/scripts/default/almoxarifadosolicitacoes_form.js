﻿var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

jQuery(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};
	
	if ((typeof form_values)!='undefined') {
		if ((typeof form_values.status)!='undefined') jQuery("select[name=status]").val(form_values.status);
		
		$("select").trigger("chosen:updated");
	}
		
	jQuery('select, input, textarea').bind('blur', function() { });
	
	$("input[name=quantidade]").priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',	    
	    centsLimit: 0    
	});	
	
	rules = {};
	rules.status = "required";
	rules.titulo = "required";
	rules.quantidade = "required";
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
		    
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});	
	
	
});
