﻿$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=status]").val(form_values.status);	
		if ((typeof form_values.idescola)!='undefined') jQuery("select[name=idescola]").val(form_values.idescola);
		if ((typeof form_values.idescola)!='undefined') setSeries(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idserie);		

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});		
	}
	
	$(".line").draggable(
		{cursor: 'pointer', 
		helper: 'clone', 
		opacity: 0.75});
	$(".line").droppable(
		{accept: '.line',
      	drop: function(event, ui) { 
	      	$idTo = $(this).find("input[name='id[]']").attr("value");
	      	$idFrom = ui.draggable.find("input[name='id[]']").attr("value");
	      	
	      	if ($idTo!=$idFrom) {
				
		      	$.post(baseUrl + "/admin/" + controller + "/changeorderxml",
				   	{to: $idTo, from: $idFrom, op: 'change'}, function(data) {
					$message = $(data).find("result").text();
					if ($message=="OK") {
						window.location.reload();
					} else {
						jAlert($message, "Alerta!", function(r) {
							window.location.reload();
						});
					}					      	
			    });		      		
	      	}
	    }
    });
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/admin/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/admin/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/admin/"+controller+"/changestatusxml",
 	 	 	{id: $id, op: controller}, function(data) {
			if ((data=="Ativo") || (data=="Bloqueado")) {
				$obj.html(data);
			} else {
				jAlert(data, "Alerta!");
			}
 	});
} 

function setSeries(obj, idserie){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idserie]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolassalasalunos/setseriesindex/', {idescola:val}, function(data) {
		$("select[name=idserie]").html(data);
		$("select[name=idserie]").val(idserie);
		val = $("select[name=idserie]").find('option:selected').html();
		$("select[name=idserie]").closest('div').find('span').html(val);
	
		$("select[name=idserie]").trigger("chosen:updated");
		
		if(idserie==""){
			$("select[name=idserie]").closest('div').find('span').html('Selecione...');
		}
	});
}