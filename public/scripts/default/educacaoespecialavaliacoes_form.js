var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=status1]").val(form_values.status1);

		if(form_values.idescola != undefined)
		setalunos($("select[name=idescola]"),form_values.idaluno);

		if(form_values.idsperguntas != undefined && form_values.idstiposevolucoes != undefined) {
			var idperguntas = form_values.idsperguntas.split(',');
			var idtiposevolucoes = form_values.idstiposevolucoes.split(',');
			var aux = 0;

			$("input[name='idsperguntas[]']").each(function() {
				var obj = $(this).siblings(".controls").find("select[name='idstiposevolucoes[]']");
				$(obj).val(idtiposevolucoes[aux]);
				console.log($(obj).attr("class"));
				aux++;
			});
		}


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	

	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});





	
	
	rules = {};
	rules.idescola = "required";
rules.idaluno = "required";
rules.data = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setalunos(obj, idaluno){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idaluno]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + 'educacaoespecialavaliacoes/setalunos/', {idescola:val}, function(data) {
		console.log(data);
		$("select[name=idaluno]").html(data);
		$("select[name=idaluno]").val(idaluno);
		val = $("select[name=idaluno]").find('option:selected').html();
		$("select[name=idaluno]").closest('div').find('span').html(val);
	
		$("select[name=idaluno]").trigger("chosen:updated");
		
		if(idaluno==""){
			$("select[name=idaluno]").closest('div').find('span').html('Selecione...');
		}
	});
}