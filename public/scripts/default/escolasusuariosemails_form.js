var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=idescolausuario]").val(form_values.idescolausuario);
$("select[name=emailspais]").val(form_values.emailspais);
$("select[name=emailsdepartamentos]").val(form_values.emailsdepartamentos);
$("select[name=iddepartamentoescola]").val(form_values.iddepartamentoescola);
$("select[name=status1]").val(form_values.status1);
$("select[name=tipo]").val(form_values.tipo);
setTipo($("select[name=tipo]").val(form_values.tipo));
if ((typeof form_values.idsdepartamentossecretarias)!='undefined') $("[name='idsdepartamentossecretarias[]']").val(form_values.idsdepartamentossecretarias);
if ((typeof form_values.idsdepartamentosescolas)!='undefined') $("[name='idsdepartamentosescolas[]']").val(form_values.idsdepartamentosescolas);
setDepartamentos($("select[name=emailsdepartamentos]").val(form_values.emailsdepartamentos));

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	







	
	
	rules = {};
	rules.idescola = "required";
rules.idescolausuario = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setDepartamentos(obj){
	var emaildepartamento = $(obj).val();
	if(emaildepartamento=="Sim"){
		$('.departamento').css('display', 'block');
	}else{
		$('.departamento').css('display', 'none');
	}
}

function setTipo(obj){
	var tipo = $(obj).val();
	if(tipo=="Secretaria"){
		$('.departamentos_secretaria').css('display', 'block');
		$('.departamentos_escola').css('display', 'none');
	}else if(tipo=="Escola"){
		$('.departamentos_escola').css('display', 'block');
		$('.departamentos_secretaria').css('display', 'none');
	}
}