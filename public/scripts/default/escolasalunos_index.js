﻿$(document).ready(function() {

    if ((typeof form_values) != 'undefined') {
        $("select[name=status]").val(form_values.status);
        if ((typeof form_values.idescola) != 'undefined')
            jQuery("select[name=idescola]").val(form_values.idescola);
        if ((typeof form_values.matricula) != 'undefined')
            jQuery("select[name=matricula]").val(form_values.matricula);
        if ((typeof form_values.classificacao) != 'undefined')
            jQuery("select[name=classificacao]").val(form_values.classificacao);
        if ((typeof form_values.idescola) != 'undefined')
            setSeries(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idserie);
        if ((typeof form_values.idescola) != 'undefined')
            setPeriodos(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idperiodo);
        if ((typeof form_values.idescola) != 'undefined')
            setClassificacoes(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idclassificacao);
        if ((typeof form_values.nutricao) != 'undefined')
            jQuery("select[name=nutricao]").val(form_values.nutricao);
        if ((typeof form_values.nomerazao) != 'undefined')
            jQuery("select[name=nomerazao]").val(form_values.nomerazao);
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
    }

    $(".line").draggable(
            {cursor: 'pointer',
                helper: 'clone',
                opacity: 0.75});
    $(".line").droppable(
            {accept: '.line',
                drop: function(event, ui) {
                    $idTo = $(this).find("input[name='id[]']").attr("value");
                    $idFrom = ui.draggable.find("input[name='id[]']").attr("value");

                    if ($idTo != $idFrom) {

                        $.post(baseUrl + "/admin/" + controller + "/changeorderxml",
                                {to: $idTo, from: $idFrom, op: 'change'}, function(data) {
                            $message = $(data).find("result").text();
                            if ($message == "OK") {
                                window.location.reload();
                            } else {
                                jAlert($message, "Alerta!", function(r) {
                                    window.location.reload();
                                });
                            }
                        });
                    }
                }
            });
});

$("select[name=documento]").change(function() {
    var valor = $(this).val();
    input = $("#field-docs");

    switch (valor) {
        case 'rg':
            $(input).mask("99.999.999-9");
            $(input).attr("name", "rg");
            $(input).fadeIn();
            break;
        case 'cpf':
            $(input).mask("999.999.999-99");
            $(input).attr("name", "cpf");
            $(input).fadeIn();
            break;
        default:
            $(input).fadeOut();
            break;
    }
});

function confirmDelete($id, $texto) {
    jConfirm('Confirmar a exclusão de "' + $texto + '"?', 'Excluir registro', function(r) {
        if (r) {
            $.post(baseUrl + "/" + module + "/" + controller + "/excluirxml",
                    {id: $id}, function(data) {
                if (data == "OK") {
                    window.location = baseUrl + "/" + module + '/' + controller;
                } else {
                    jAlert(data, "Erro!");
                }
            });
        }
    });
}

function statusChange($id, $obj) {
    $obj = $($obj);

    $.post(baseUrl + "/" + controller + "/changestatusxml",
            {id: $id, op: controller}, function(data) {
        if ((data == "Ativo") || (data == "Bloqueado")) {
            $obj.html(data);
        } else {
            jAlert(data, "Alerta!");
        }
    });
}

function setDados(obj, idescola) {
    var val = $(obj).val();

    setSeries($("select[name=idescola]").val(val), 0);
    setPeriodos($("select[name=idescola]").val(val), 0);
    setClassificacoes($("select[name=idescola]").val(val), 0);
}

function setSeries(obj, idserie) {

    var val = $(obj).val();

    if (val == "" || val == 0) {
        $("#selectPart").val();
        $("#selectPart").fadeOut('fast', function() {
            $("#selectAll").fadeIn();
        });
    } else {
        data = '';
        $("#selectAll").val();
        $("#selectAll").fadeOut('fast', function() {
            $("#selectPart").fadeIn();
        });
        $("#partseries").html('<option value="">Aguarde...</option>');

        $.post(baseUrl + '/' + module + '/escolasalunos/setseries/', {idescola: val}, function(data) {
            $("#partseries").html(data);
            $("#partseries").val(idserie);
            val = $("#partseries").find('option:selected').html();
            $("#partseries").closest('div').find('span').html(val);

            $("#partseries").trigger("chosen:updated");

            if (idserie == "") {
                $("#partseries").closest('div').find('span').html('Selecione...');
            }

        });
    }
}

function setPeriodos(obj, idperiodo) {
    var val = $(obj).val();

    data = '';
    $("select[name=idperiodo]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/escolasvinculos/setperiodos/', {idescola: val}, function(data) {

        $("select[name=idperiodo]").html(data);
        $("select[name=idperiodo]").val(idperiodo);
        val = $("select[name=idperiodo]").find('option:selected').html();
        $("select[name=idperiodo]").closest('div').find('span').html(val);

        $("select[name=idperiodo]").trigger("chosen:updated");

        if (idperiodo == "") {
            $("select[name=idperiodo]").closest('div').find('span').html('Selecione...');
        }
    });
}

function setClassificacoes(obj, idclassificacao) {
    var val = $(obj).val();

    data = '';
    $("select[name=idclassificacao]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/escolasalunos/setclassificacoes/', {idescola: val}, function(data) {
        $("select[name=idclassificacao]").html(data);
        $("select[name=idclassificacao]").val(idclassificacao);
        val = $("select[name=idclassificacao]").find('option:selected').html();
        $("select[name=idclassificacao]").closest('div').find('span').html(val);

        $("select[name=idclassificacao]").trigger("chosen:updated");

        if (idclassificacao === "") {
            $("select[name=idclassificacao]").closest('div').find('span').html('Selecione...');
        }
    });
}

function statusSED(id) {

    var status = document.getElementById("soapstatus" + id).value;

    var dialog = $('<h5 >' + status + '</h5>').dialog({
        title: '<font color="#052f57">Status do retorno do SED.</font>',
        width: 400,
        modal: true,
        buttons: {
            "Fechar": function() {
                dialog.dialog('close');
            }
        }
    });

}