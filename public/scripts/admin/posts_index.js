$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=status1]").val(form_values.status1);	
		$("select").trigger("chosen:updated");
	}
	
});

jQuery(document).ready(function() {
	jQuery(".line").draggable(
		{cursor: 'pointer', 
		helper: 'clone', 
		opacity: 0.75});
	jQuery(".line").droppable(
		{accept: '.line',
      	drop: function(event, ui) { 
	      	$idTo = $(this).find("input[name='id[]']").attr("value");
	      	$idFrom = ui.draggable.find("input[name='id[]']").attr("value");
	      	
	      	if ($idTo!=$idFrom) {
		      	$.post(baseUrl + "/admin/" + controller + "/changeorderxml" + redirect_modal,
				   	{to: $idTo, from: $idFrom, op: 'change'}, function(data) {
					$message = $(data).find("result").text();
					if ($message=="OK") {
						window.location.reload();
					} else {
						jAlert($message, "Alerta!", function(r) {
							window.location.reload();
						});
					}					      	
			    });		      		
	      	}
	    }
    });
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/admin/" + controller + "/excluirxml" + redirect_modal + params,
		   		{id: $id}, function(data) {
				$message = $(data).find("result").text();
				if ($message=="OK") {
					window.location = baseUrl + '/admin/' + controller + '/' + action + redirect_modal + params;
				} else {
					$.unblockUI();
					jAlert($message);
				}
			});
		}
 	}); 
}

function destaqueChange($id, $obj) {
 	$obj = $($obj);

 	$.post(baseUrl + "/admin/" + controller + "/changedestaquexml" + redirect_modal + params,
 	 	{id: $id, op: 'posts'}, function(data) {
		$message = $(data).find("result").text();
		if ($message=="OK") {
			if ("Destacado"==$obj.html()) $obj.html("Não Destacado");
			else $obj.html("Destacado");
		} else {
			jAlert($message, "Alerta!");
		}
 	});
} 

function statusChange($id, $obj) {
 	$obj = jQuery($obj);

 	$.post(baseUrl + "/admin/" + controller + "/changestatusxml" + redirect_modal + params,
 	 	{id: $id, op: 'posts'}, function(data) {
		$message = $(data).find("result").text();
		
		if ($message=="OK") {
			if ("Ativo"==$obj.html()) $obj.html("Bloqueado");
			else $obj.html("Ativo");
		} else {
			jAlert($message, "Alerta!");
		}
 	});
} 