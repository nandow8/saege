$(document).ready(function(){
	
	if((typeof form_values)!='undefined'){
		console.log(form_values);
		$("select[name=status1]").val(form_values.status1);
		$("input[name=datalancamento]").val(form_values.datalancamento);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=iddepartamento]").val(form_values.iddepartamento);
		
		if(form_values.iddepartamento == 29) $('.comboescola').fadeIn();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);			
		});		
		
	}
	
	$("input[name=datalancamento_i],input[name=datalancamento_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalancamento_i],input[name=datalancamento_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function setEscola(obj){
	valor = $(obj).val();
	
	if(valor==29){
		$('.comboescola').fadeIn();
	}else{
		$("select[name=idescola]").val("");
		$("select[name=idescola]").closest('div').find('span').html("Todos");
		$('.comboescola').fadeOut();
	}

}
/*
* Função: Redimensionar o select da class 'SELECT'
* @param min - tamanho mínimo do select
* @param max - tamanho máximo do select
*/
$(function(){
    $(".select2-container").css({
        'min-width':'140px',
        'max-width':'250px'
    });
});
