﻿$('#imo_fileupload_multiplas_imagens').fileupload({
    dataType: 'json',
    done: function (e, data) {
    	idx = data.result[0].idx;
    	idimagem = data.result[0].idimagem;
    	    	
    	$("#imo_multiplas_imagens li.imo_container_multiplas_imagens").eq(idx).find("div:first").html(imo_getViewImagem_Plantas(idimagem, data.files[0].name));
    	
        imo_resetPaginas();
		imo_aplicaDragAndDrop();
    }
});

$('#imo_fileupload_multiplas_imagens').bind('fileuploadprogress', function (e, data) {
	var progress = parseInt(data.loaded / data.total * 100, 10);
    idx = data.formData.idx;
    $("#imo_multiplas_imagens li.imo_container_multiplas_imagens").eq(idx).find('div.progresso div').css('width', progress + '%');
});

$('#imo_fileupload_multiplas_imagens').bind('fileuploadsubmit', function (e, data) {
	imo_setImagem_multiplas_imagens(0, false);
    data.formData = {idx: $("#imo_multiplas_imagens li.imo_container_multiplas_imagens").length-1};
});	

function imo_resetPaginas() {
	if ($("#imo_table-arquivos tr").not('.nenhum').length==0) {
		imo_setImagem_multiplas_imagens(false, false);
	} else {
		$("#imo_table-arquivos tr.nenhum").remove();
	}
	
    c = 0;
	$("#imo_table-arquivos tr").not('.nenhum').each(function() {
    	c++;
    	pagina = (c<=9) ? '0' + c : c;
    	$(this).find('td:first').attr('align', 'center');
    	$(this).find('td:first').html(pagina);
    });

}

function imo_setImagem_multiplas_imagens(idimagem, filename, legenda) {
	var first = "";
	if(count_li == 0){
		first = "first";
		count_li = 1;
	}	
	
	if (idimagem===false) {
		return;
	}
	
	legenda = ((typeof legenda)=='undefined') ? '' : legenda;
	
	li = $('<li class="'+first+' imo_container_multiplas_imagens" />'); 
    if (filename===false) {
    	li.append('<div class="box_arquivo"><div class="progresso" style="border: 1px solid #F99B16; background-color: #F99B16; height: 21px; left: 0px; position: relative; top: 65px; width: 185px;"><div style="background: #0a0; width:0%; height: 21px"></div></div></div>');
    } else {
    	li.append($("<div class='box_arquivo'>" + imo_getViewImagem_Plantas(idimagem, filename, legenda) + "</div>"));
    }
    ul = $('<ul class="grid-actions" />');
    ul.append('<li class="fechar"><a onclick="imo_excluir(this)" href="javascript:;" title="Delete" class="with-tip"><img src="' + baseUrl + '/public/admin/imagens/fugue/cross-circle.png" width="16" height="16"></a></li>');
    li.append(ul);
    
	$("#imo_multiplas_imagens ul:first").append(li);
}

function imo_getViewImagem_Plantas(idimagem, filename, legenda) {
	legenda = ((typeof legenda)=='undefined') ? '' : legenda;
//	alert('aqui');
	input = '<input type="hidden" name="idmultiplas_imagens[]" value="' + idimagem + '" /><input type="text" name="legendas_multiplas_imagens[]" value="'+legenda+'" class="legenda_fileupload" />';
	imagem = '<div class="box-imagem"><a href="' + baseUrl + '/admin/imagens/get/id/'+idimagem+'/w/192/h/144/i/usuario/marcadagua/true/modo/" download target="_blank"><img alt="" src="' + baseUrl + '/admin/imagens/get/id/'+idimagem+'/w/192/h/144/i/usuario/marcadagua/true/modo/" width="154" ></a></div>';
	return imagem + input;
}

function imo_excluir(obj) {
	$(obj).parent().parent().parent().remove();
	imo_resetPaginas();
}

function imo_aplicaDragAndDrop() {
	$(".imo_container_multiplas_imagens").draggable(
			{cursor: 'pointer', 
			helper: 'clone', 
			opacity: 0.75});
	$(".imo_container_multiplas_imagens").droppable(
			{accept: '.imo_container_multiplas_imagens',
	      	drop: function(event, ui) { 
		    	elmTo = jQuery(this);
		    	elmFrom = ui.draggable;
		    	
		    	elmTo.swap(elmFrom);	 
		    	imo_resetPaginas();
		    }
    });	
}		

