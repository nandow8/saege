﻿$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		console.log(form_values);
		$("select[name=status]").val(form_values.status);	
		if ((typeof form_values.idescola)!='undefined') jQuery("select[name=idescola]").val(form_values.idescola);
		if ((typeof form_values.nutricao)!='undefined') $("select[name=nutricao]").val(form_values.nutricao);
		if ((typeof form_values.matricula)!='undefined') jQuery("select[name=matricula]").val(form_values.matricula);
		if ((typeof form_values.classificacao)!='undefined') jQuery("select[name=classificacao]").val(form_values.classificacao);
		if ((typeof form_values.idescola)!='undefined') setSeries(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idserie);		
		if ((typeof form_values.idescola)!='undefined') setPeriodos(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idperiodo);
		if ((typeof form_values.idescola)!='undefined') setClassificacoes(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idclassificacao);
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});		
	}
	
	$(".line").draggable(
		{cursor: 'pointer', 
		helper: 'clone', 
		opacity: 0.75});
	$(".line").droppable(
		{accept: '.line',
      	drop: function(event, ui) { 
	      	$idTo = $(this).find("input[name='id[]']").attr("value");
	      	$idFrom = ui.draggable.find("input[name='id[]']").attr("value");
	      	
	      	if ($idTo!=$idFrom) {
				
		      	$.post(baseUrl + "/admin/" + controller + "/changeorderxml",
				   	{to: $idTo, from: $idFrom, op: 'change'}, function(data) {
					$message = $(data).find("result").text();
					if ($message=="OK") {
						window.location.reload();
					} else {
						jAlert($message, "Alerta!", function(r) {
							window.location.reload();
						});
					}					      	
			    });		      		
	      	}
	    }
    });
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/admin/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/admin/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/admin/"+controller+"/changestatusxml",
 	 	 	{id: $id, op: controller}, function(data) {
			if ((data=="Ativo") || (data=="Bloqueado")) {
				$obj.html(data);
			} else {
				jAlert(data, "Alerta!");
			}
 	});
} 

function setDados(obj, idescola){
	var val = $(obj).val();
	
	setSeries($("select[name=idescola]").val(val), 0);
	setPeriodos($("select[name=idescola]").val(val), 0);
	setClassificacoes($("select[name=idescola]").val(val), 0);
}

function setSeries(obj, idserie){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idserie]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasvinculos/setseries/', {idescola:val}, function(data) {
		$("select[name=idserie]").html(data);
		$("select[name=idserie]").val(idserie);
		val = $("select[name=idserie]").find('option:selected').html();
		$("select[name=idserie]").closest('div').find('span').html(val);
	
		$("select[name=idserie]").trigger("chosen:updated");
		
		if(idserie==""){
			$("select[name=idserie]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setPeriodos(obj, idperiodo){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idperiodo]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasvinculos/setperiodos/', {idescola:val}, function(data) {
		$("select[name=idperiodo]").html(data);
		$("select[name=idperiodo]").val(idperiodo);
		val = $("select[name=idperiodo]").find('option:selected').html();
		$("select[name=idperiodo]").closest('div').find('span').html(val);
	
		$("select[name=idperiodo]").trigger("chosen:updated");
		
		if(idperiodo==""){
			$("select[name=idperiodo]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setClassificacoes(obj, idclassificacao){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idclassificacao]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasalunos/setclassificacoes/', {idescola:val}, function(data) {
		$("select[name=idclassificacao]").html(data);
		$("select[name=idclassificacao]").val(idclassificacao);
		val = $("select[name=idclassificacao]").find('option:selected').html();
		$("select[name=idclassificacao]").closest('div').find('span').html(val);
	
		$("select[name=idclassificacao]").trigger("chosen:updated");
		
		if(idclassificacao==""){
			$("select[name=idclassificacao]").closest('div').find('span').html('Selecione...');
		}
	});
}