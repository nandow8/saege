var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		jQuery("select[name=idlocal]").val(form_values.idlocal);
                if (form_values.idaluno > 0){
                    setAlunos($("select[name=idescola]").val(form_values.idescola), form_values.idaluno);
                }

                //setAlunos($("select[name=idescola]").val(form_values.idescola), form_values.idaluno);
		$("select[name=idaluno]").val(form_values.idaluno);
		
$("select[name=posicao]").val(form_values.posicao);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
		$("input[name=datalancamento]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalancamento]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});








	$("input[name=datalaudo]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalaudo]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});




	
	
	rules = {};
	rules.datalancamento = "required";
rules.idusuario = "required";
rules.idlocal = "required";
rules.posicao = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setAlunos(obj, idaluno, escola = 0){
        var idescola;
        
        if(escola > 0){
            idescola = escola;
        }else{
            idescola = $(obj).val();
        }

	
	data = '';
	$("select[name=idaluno]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/nutricaomerendasdiferenciadas/setescolasalunos/', {idescola:idescola}, function(data) {
		$("select[name=idaluno]").html(data);

		$("select[name=idaluno]").val(idaluno);
		val = $("select[name=idaluno]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});

		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idaluno==""){
			$("select[name=idaluno]").closest('div').find('span').html('Selecione...');

			$('.box-dados-alunos').css('display', 'none');
			$(".setdado").html('');	
		}

		setDadosAlunos($("select[name=idaluno]").val(idaluno));
		
	});
}

function setDadosAlunos(obj){
	var idaluno = $(obj).val();
	data = '';
	$.post(baseUrl + '/admin/nutricaomerendasdiferenciadas/getdados/', {idaluno:idaluno}, function(data) {
		if(data=="erro"){
			$('.box-dados-alunos').css('display', 'none');
			$(".setdado").html('');			
		}else{
			$('.box-dados-alunos').css('display', 'block');
			$(".setdado").html(data);			
		}

	});
}