var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idaluno]").val(form_values.idaluno);
		getDadosAlunos($("select[name=idaluno]").val(form_values.idaluno), 0);
		$("select[name=iddeficiencia]").val(form_values.iddeficiencia);
		$("select[name=transporte]").val(form_values.transporte);
		$("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	

$("input[name=fone1]").mask("(99)9?9999-9999", {placeholder:" "});
$("input[name=fone2]").mask("(99)9?9999-9999", {placeholder:" "});



	
	
	rules = {};
	rules.idaluno = "required";
rules.fone1 = "required";
// rules.iddeficiencia = "required";
rules.transporte = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function getDadosAlunos(obj, ida){
	var idaluno = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/getdadosalunos", {idaluno: idaluno}, function(data) {
		$('.box-alunosdados').html(' ');
		$('.box-alunosdados').html(data);
	});
}