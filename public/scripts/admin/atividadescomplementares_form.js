var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};

	
	$("input[name=horainicio]").mask("99:99", {placeholder:" "});
	$("input[name=horafim]").mask("99:99", {placeholder:" "});


	$("input[name=datainicio]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datainicio]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=datafim]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datafim]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});


	$("select[name=idescola]").change(function() {
		setAlunos(this, null);
	});

	if ((typeof form_values)!='undefined') {
		$("select[name=status1]").val(form_values.status1);
		
		$("select[name=idtipoatividade]").val(form_values.idtipoatividade);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=diasemana]").val(form_values.diasemana);

		setAlunos($("select[name=idescola]").val(form_values.idescola), form_values.idaluno);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}	
	
	rules = {};
	rules.idtipoatividade = "required";
	rules.titulo = "required";
	rules.idescola = "required";
	//rules.idescolavinculo = "required";
	rules.datainicio = "required";
	rules.diasemana = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	

	//visualizar = false;
	if (visualizar) {

		$("div.control-group input, div.control-group textarea").not('[type=hidden]').not('[type=checkbox]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});

		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');
		});	
		
		$("label span.required").remove();
		setTimeout(function(){ $("input[type=checkbox]").attr("disabled",true) }, 500 );			
	}	
});



function setAlunos(obj, idalunos) {
		var idescola = $(obj).val();

		if( idescola != 0 && idescola != "" )
			$.post(baseUrl + "/" + module + "/"+controller+"/setalunos",{idescola: idescola}, function(data) {

				$("#alunos-in").html(data);
				
				$("#alunos").show();

				if( idalunos != "" && idalunos != undefined )
					$("input[type=checkbox][name='idaluno[]']").val( idalunos.split(',') );

				$("select").each(function() {
					val = $(this).find('option:selected').html();
					$(this).closest('div').find('span').html(val);
				});
		
				$("select").each(function (){
					div = $(this).closest('div').find('.chosen-container').css('width', '100%');
				});
			});
		else
			$("#alunos").hide();
}