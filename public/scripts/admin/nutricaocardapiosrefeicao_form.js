var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {

                $("select[name=status1]").val(form_values.status1);
                $("select[name=tipoensino]").val(form_values.tipoensino);
                $("select[name=tiporefeicao]").val(form_values.tiporefeicao);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-refeicaoiner').css('width', '100%');
		});
	
	}
	
        $('#accordion').accordion({ collapsible: true });  
	
	rules = {};
        rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


function refeicaoAddTr(_values){

	html = "";

	if (_values===false) {
		_values = {};
	}else if(_values === undefined){
		_values = { 
					"idv":$('#ing').val(),
					"titulo":$("#ing option:selected").html(),
					"var_db":true
				  };
	} else {
		_values = jQuery.parseJSON(_values);
		itens = _values;
	}

	modelo = $("#modelo-refeicao").html();
	modelo = '<tr>' + modelo + '</tr>';
	modelo = $(modelo);
	table = $("table.table-refeicoes");
	refeicaoiner = table.find('tbody');
	

	modelo.find('p').html(html);
	modelo.find('input,select,textarea,div').each(function(){
		name = $(this).attr('name');
		name = name.replace("_", "");
		if(_values && _values.var_db == true)name = name.replace("[]", "");

		for (var property in _values) {
			if (property==name) {

				$(this).attr('name', name + '[]');
				if(_values.var_db){
				}
				console.log(_values[property])
				$(this).attr('value', _values[property]);

				$(this).html(_values[property]);

				if ($(this).is("select")) {
						$(this).parent().find('a').remove();
				}

			}
		}
	});

	refeic.append(modelo);
	refeicaoiner.append(modelo);

	$('.numbervalue').priceFormat({
			prefix: '',
			limit: 3,
			centsSeparator: '',
			thousandsSeparator: ''	    
	});

	$('.numbervalue').blur(function(){
			if($(this).val() <= 0){
					$(this).val('');	
			}
	});

	calcTotLinhas();
}

function refeicoesExcluirFromTable(obj) {
	jConfirm("Deseja excluir o registro?", 'Confirmar', function() {
		if(r)
		{
			obj = $(obj);
			content = obj.closest('td');
			content.find('input').remove();
			content.hide();
			calcTotLinhas();
		}
	});
}

function setBtnAddRefeicao(obj){
	let val = $(obj).val();

	document.getElementById('BtnAddRefeicao').disabled = (val === "");
}        

function calcTotLinhas(){
	
	$("#subtotQtd").html((document.getElementsByName('idv[]').length-1).toString());
	
	let str = '';
	
	for(var i = 0; i < document.getElementsByName('idv[]').length; i++)
	{
		str += document.getElementsByName('idv[]')[i].value;
		str += (i < document.getElementsByName('idv[]').length-2) ? "," : "";
	} 
	// ajusta Array para não incluir duas vezes repetidos.
	let x = str.split(',');
	x.sort();
	let z = [...new Set(x)];
	
	str = z.join(',');
	document.getElementById('idsrefeicoes').value = (str);
	
}

function MontaJanela(obj, eventos = []){

	let NomeJanela = '#'+obj;

	let evento = (eventos[0][0]);

	$.fancybox({
		type: 'iframe',
		href: baseUrl + "/admin/nutricaocardapiosrefeicao/setdatasrefeicao/eventos/" + evento,
		width:  $('.'+($(NomeJanela).attr("class"))).width() + 22,
		height: $('.'+($(NomeJanela).attr("class"))).height() + 22,
		autoSize: false,
		helpers: {
			title : {
				type : 'float'
			}
		},
		onComplete: function(){
			alert('Completou');	
		},
		onClosed: function(){
			alert('Fechou');
		}
	});  

}

function setNovoEvento(){

	var dt_exclusao = document.getElementById("dataevento").value;
	var arrData = dt_exclusao.split('/');
	var novaData = new Date(arrData[2] + '-' + arrData[1] + '-' + arrData[0]);
	
	Date.prototype.addDias = function(dias){
		this.setDate(this.getDate() + dias);
	};
	
	novaData.addDias(1);
	
	$.post(baseUrl + '/admin/nutricaocardapiosrefeicao/adicionarevento/', 
		{ 
			title: '* OK *', 
			color: '#ff7f50', 
			start: novaData,
			end: novaData 
		}, function (data) { }
	);
 
	$("#cardapiocalendario").fullCalendar( 'removeEvents' ) ;
	$('#cardapiocalendario').fullCalendar( 'refresh' );       

}

function btnSalvar(){
	var eventos = [];
	
	eventos = $('#cardapiocalendario').fullCalendar('clientEvents');
	
   $.ajax({ 
		   url        : baseUrl + "/admin/nutricaocardapiosrefeicao/setdatasrefeicao.phtml",
		   type       : 'POST',                                              
		   data       : eventos,
		   success    : function(){ }
   });

	MontaJanela('janela', eventos );
	alert('tchau');
//        document.getElementById("form").submit();
}

function setTipoensino_old(obj){
	var val = $(obj).val();

	if ((val === 'CM') || (val === 0)){
		$('#tiporefeicao').css('display', 'block');
		$('#CM').css('display', 'block'); 
		$('#EE').css('display', 'none');                   
	}else if ((val === 'EE') || (val === 1)){    
		$('#tiporefeicao').css('display', 'block');
		$('#CM').css('display', 'none');
		$('#EE').css('display', 'block');       
	}else{
		$('#tiporefeicao').css('display', 'none');
		alert('Selecione');
	}
}

/*
* -> setTipoensino
* Função criada para recuperar os tipos de refeições servidas de acordo com o tipo de ension setado
*
*/
function setTipoensino(obj){
	var val = $(obj).val();
	
	if ((val === 'CM') || (val === 0)){
		console.log('primeiro');
		$('#tiporef').html('');    
		$('#tiporef').append('<option val=""> Selecione... </option>');
		$('#tiporef').append('<option value="CMCAFE">CAFÉ MANHÃ</option>');
		$('#tiporef').append('<option value="CMALMOCO">ALMOÇO</option>');
		$('#tiporef').append('<option value="CMLANCHE">LANCHE</option>');
		$('#tiporef').append('<option value="CMJANTAR">JANTA</option>');
			 
	}else if ((val === 'EE') || (val === 1)){  
		console.log('segundo'); 
		$('#tiporef').html('');
		$('#tiporef').append('<option value=""> Selecione... </option>');
		$('#tiporef').append('<option value="EECAFE DA MANHA">  CAFÉ DA MANHÃ</option>');
		$('#tiporef').append('<option value="EEALMOCO">  ALMOÇO</option>');
		  
	}else{
		console.log('zerado');
		$('#tiporef').html('');
	}
}

function JanelaArquivos(obj, tipo = 0, destino = 0, id = ""){

let NomeJanela = '#' + obj.rel;

if ($(obj).context['name'] === "btnEncaminhar"){
$(".box-encaminhar").css("display", "block");
$(".box-encaminharbotao").css("display", "block");
$(".box-finalizarbotao").css("display", "none");
$(".modal-title").html('Encaminhar Protocolo.');
} else {
$(".box-encaminhar").css("display", "none");
$(".box-encaminharbotao").css("display", "none");
$(".box-finalizarbotao").css("display", "block");
$(".modal-title").html('Finalizar Protocolo.');
}

destino = (destino == 1) ? 'settramitacaoarquivos' : 'settramitacao';

$.fancybox({
type: (tipo == 0) ? 'inline' : 'iframe',
href: (tipo == 0) ? NomeJanela : baseUrl + "/admin/protocolosprocessos/" + destino + "/id/" + id,
width: $('.' + ($(NomeJanela).attr("class"))).width() + 22,
height: $('.' + ($(NomeJanela).attr("class"))).height() + 22,
autoSize: false,
helpers: {
	title: {
		type: 'float'
	}
}
});

}

function fancyboxevent(){

$.fancybox({
	type: 'inline',
	href: $('#divForm'),
	width: 400,
	//height: 500,
	autoSize: true,
	helpers: {
	   title: {
		   type: 'float'
	   }
   }
});
}//end eventfancyboxevent

function cadastrarefeicao(){

// limpando fancybox
$('#titulo_cardapio').val('');
$("#tipoensino_cardapio").val('0');

$("#tipoensino_cardapio").each(function() {
val = $(this).find('option:selected').html();
$(this).closest('div').find('span').html(val);
});

$('#refeicoes-list').html('');

// Iniciando fancybox
$.fancybox({
type: 'inline',
href: $('#divCadastro'),
autoSize: true,
helpers: {
   title: {
	   type: 'float'
   }
}
});
}//end eventfancyboxevent


/* =====================================================
					Novos Códigos
========================================================*/

function adicionarRefeicao(){


valor = $('#ingred :selected').val();

console.log('selecionando: ', valor);

objTable = $('[name=table-refeicoes]').find('tbody');

$.ajax({
	url: baseUrl + '/admin/nutricaocardapiosrefeicao/retornarefeicoesajax',
	type: 'post',
	data: {
		id: valor
	},
	beforeSend: function (){
		//$("#tabela").html("ENVIO");          
	}}).done(function (msg) { 

		mensagem = JSON.parse(msg); 
		if($('#titulo_cardapio').val() == ''){
			$('#titulo_cardapio').val(mensagem.titulo);
		}else{
			//$('#titulo_cardapio').val(mensagem.titulo);
		}
		
		objTable.append("<tr><td name='refid'>" + mensagem.id + "</td><td>" + mensagem.titulo + "</td><td>" + mensagem.favorita + "</td><td>" + mensagem.porcoes + "</td><td><div class='btn btn-info btn-block' onclick='excluirRefeicao(this)'>Excluir</div></td></tr>");
		
	});

total = $('#contadorRefeicoes').text(); // Pegando o valor total atual

total++; //Somando o noto total ao id contadorRefeicoes

$('#contadorRefeicoes').html(total); // Inserindo novo valor ao total

}//end adiconarRefeicao

function cadastracardapio(){

var tituloCardapio = $('#titulo_cardapio').val();
var tipoEnsino  = $('#tipoensino_cardapio :selected').val();
var tipoRefeicao =  $('#tiporef :selected').val();

var refeicoes = new Array();

 objTable = $('[name=table-refeicoes]').find('tbody');

 objTable.find("> *").each(function (i, val){
	 refeicoes.push(val.firstChild.innerHTML);
 });

 $.ajax({
	url: baseUrl + '/admin/nutricaocardapiosrefeicao/cadastracardapioajax',
	type: 'post',
	data: {
		titulo_cardapio : tituloCardapio,
		tipo_ensino : tipoEnsino,
		tipo_refeicao : tipoRefeicao,
		lista_refeicoes: refeicoes,
	},
	beforeSend: function () {
		//$("#tabela").html("ENVIO");          
	}}).done(function (msg) { 
		
		refeicao = JSON.parse(msg);
	   
		$('#external-events-listing').append("<div id='" + refeicao.id + "' class='fc-event event-list' style='color: white; background-color: #8484e1; opacity: 0.6;'>" + refeicao.refeicao  + "</div>");

		$('#external-events .fc-event').each(function(){
			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});
			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});
		});
		
		$.fancybox.close();


	});//end ajax

}//end function cadastracardapio

function excluirRefeicao(obj){

obj.closest('td').closest('tr').remove();

total = $('#contadorRefeicoes').text(); // Pegando o valor total atual

total--; //Somando o noto total ao id contadorRefeicoes

$('#contadorRefeicoes').html(total); // Inserindo novo valor ao total

}//end excluirRefeicao

function somaTotalRefeicoesAdicionar(){

	obj = $('#contadorRefeicoes');

}//end contaTotalRefeicoes

function excluirefeicaocalendario(obj){

var aux = obj.id;

aux = aux.split('-');

$('#cardapiocalendario').fullCalendar('removeEvents',aux[0]);
// Evento Ajax para alterar dados do evento
$.post(baseUrl + "/"+module+"/"+controller+"/excluirefeicaoagendaajax",
{
   id: aux[1],
}, function(data){  
	$.fancybox.close();
});

//console.log('==>',this);



}//end public function

$('div[name=btn]').mouseover(function(){
$(this).stop(true).animate({opacity: 1}, 300);
});
$('div[name=btn]').mouseout(function() {
$(this).stop(true).animate({opacity: 0.6}, 300);
});

$('input[name=pesquisa]').keyup(function(){

var texto = $(this).val();

$('#resultados > div').each(function(index, val){

	var strSearch = $(val).text();
	 $(val).css('display', 'none');
	if( strSearch.search(texto) == 0){
		console.log('==>',strSearch);
		console.log('xx>', strSearch.search(texto));
		$(val).css('display', 'block');
	}//end if


	
});

console.log('--------------------------------');

});


$('div[name=btn]').dblclick(function(){

var idref = this.id;

refeicoesContador = 0;

 $.post(baseUrl + "/" + module + "/"+controller+"/retornarefeicaoajax",
 {id: idref}, function(data){

	lancamento = JSON.parse(data);

	$('#divFormUpdate-id').html(lancamento.refeicao);
	$('input[name=nomecardapio-update]').val(lancamento.refeicao);
	$('input[name=idvalupdate]').val(lancamento.id);
	$('select[name=updateselect-escola]').val(lancamento.tipoensino);
	$("select[name=updateselect-escola]").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});

	console.log(lancamento.tipoensino);

	var html = '';

	$('select[name=updateselect-refeicao]').html(html);

	if(lancamento.tipoensino == 'CM'){

		html += '<option value="CMCAFE"> Creche Municipal Café da Manhã</option>';
		html += '<option value="CMALMOCO"> Creche Municipal Almoço</option>';
		html += '<option value="CMLANCHE"> Creche Municipal Lanche</option>';
		html += '<option value="CMJANTAR"> Creche Municipal Jantar</option>';
		
	}else{

		html += '<option value="EECAFE DA MANHA"> EMEI / EMEF Café da Manhã</option>';
		html += '<option value="EEALMOCO"> EMEI / EMEF Almoço</option>';
	
	}

	$('select[name=updateselect-refeicao]').append(html);

	$('select[name=updateselect-refeicao]').val(lancamento.tiporefeicao);

	$("select[name=updateselect-refeicao]").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});

	var refeicoes = lancamento.idsrefeicoes.split(',');
	var table = $('[name=divFormUpdate-refeicoes]');
	table.html('');

	$.each(refeicoes, function( index, value ){
		var idrefeicao = value;
		$.post(baseUrl + "/" + module + "/"+controller+"/retornarefeicoesajax",
		{id: idrefeicao}, function(data){
			var refDescr = JSON.parse(data);
			table.append('<tr><td>' + refDescr.id + '</td><td>' + refDescr.titulo + '</td><td>' + refDescr.favorita + '</td><td>' + refDescr.porcoes + '</td><td>' + refDescr.status +'</td><td><div class="btn btn-info" onclick="excluirref(this)" style="cursor:pointer">Excluir</div></td></tr>');
			refeicoesContador++;
			
		});

	});// end each for refeições

  });


  
  $('#contadorRefeicoesUpdate').html(refeicoesContador);

$.fancybox({
	type: 'inline',
	href: $('#divFormUpdate '),
	width: 600,
	height: 500,
	autoSize: false,
	helpers: {
	title: {
		type: 'float'
	}
}
});

});

function excluirref(obj){

	linha = $(obj).parent().parent();
	linha.remove();
	
}//end function excluiref

function editarrefeicaocalendario(){

	console.log('editaref');

}//end function editarrefeicaocalendario

function editaAdicionarRefeicao(){


	valor = $('#ingredupdate :selected').val();
	
	objTable = $('[name=divFormUpdate-refeicoes]');
	
	$.ajax({
		url: baseUrl + '/admin/nutricaocardapiosrefeicao/retornarefeicoesajax',
		type: 'post',
		data: {
			id: valor
		},
		beforeSend: function (){
			//$("#tabela").html("ENVIO");          
		}}).done(function (msg) { 
	
			mensagem = JSON.parse(msg); 
			objTable.append("<tr><td name='refid'>" + mensagem.id + "</td><td>" + mensagem.titulo + "</td><td>" + mensagem.favorita + "</td><td>" + mensagem.porcoes + "</td><td>" + mensagem.status + "</td><td><div class='btn btn-info ' onclick='excluirRefeicao(this)' style='cursor:pointer'>Excluir</div></td></tr>");
			
		});
	
	total = $('#contadorRefeicoes').text(); // Pegando o valor total atual
	
	total++; //Somando o noto total ao id contadorRefeicoes
	
	$('#contadorRefeicoes').html(total); // Inserindo novo valor ao total
	
	}//end adiconarRefeicao


	function updatecardapio(){

	var idCardapio = $('input[name=idvalupdate]').val();
	var tituloCardapio = $('input[name=nomecardapio]').val();
	var tipoEnsino  = $('select[name=updateselect-escola] :selected').val();
	var tipoRefeicao =  $('#divFormUpdate-refeicao :selected').val();
	
	var refeicoes = new Array();
	
		objTable = $('[name=divFormUpdate-refeicoes]');
	
		objTable.find("tr").each(function (i, val){
			refeicoes.push(val.firstChild.innerHTML);
		});

		console.log(refeicoes);
	
		$.ajax({
		url: baseUrl + '/admin/nutricaocardapiosrefeicao/updatecardapioajax',
		type: 'post',
		data: {
			id_cardapio : idCardapio,
			titulo_cardapio : tituloCardapio,
			tipo_ensino : tipoEnsino,
			tipo_refeicao : tipoRefeicao,
			lista_refeicoes: refeicoes,
		},
		beforeSend: function () {
			//$("#tabela").html("ENVIO");          
		}}).done(function (msg) { 
			
			refeicao = JSON.parse(msg);
			
			$.fancybox.close();
	
	
		});//end ajax
	
	}//end function cadastracardapio



