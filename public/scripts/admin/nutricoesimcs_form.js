var unsaved = false;

$("input,select,textarea").change(function() { //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function() { //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
    window.onbeforeunload = function() {

        if (unsaved) {
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };

    if ((typeof form_values) != 'undefined') {
        $("select[name=idsecretaria]").val(form_values.idsecretaria);
        $("select[name=idescola]").val(form_values.idescola);
        //$("select[name=idescolaaluno]").val(form_values.idescolaaluno);
        $("select[name=status1]").val(form_values.status1);
        $("select[name=diagnostico]").val(form_values.diagnostico);
        if (form_values.idescolaaluno > 0) {
            setAlunos($("select[name=idescola]").val(form_values.idescola), form_values.idescolaaluno);
            setDadosAlunos($("select[name=idescolaaluno]"), form_values.idescolaaluno);
        }

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function() {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

        if (!adicionar) {
            imc = form_values.imc;
            anos = form_values.anos;
            meses = form_values.meses;
            sexo = form_values.sexo;

            idade = anos + '.' + meses;

            if (idade == "." || idade == "undefined") {
                return false
            } else {
                if ((idade >= 0 && idade < 5) && (sexo == "Masculino" || sexo == "Feminino")) {
                    gerarGraficoBox(0, 5, 12, 10, 22);
                    setValorGrafico(imc, meses, anos);
                } else if ((idade >= 5 && idade <= 19) && (sexo == "Masculino" || sexo == "Feminino")) {
                    gerarGraficoBoxPlus(5, 19, 4, 12, 36, sexo);
                    setValorGraficoPlus(imc, meses, anos);
                } else {
                    //gerar uma mensagem AQUI (A FAZER)
                    return false;
                }
            }
        }
    }

    $('input[name=peso]').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.',
        centsLimit: 2
    });
    $('input[name=altura]').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.',
        centsLimit: 2
    });
    $('input[name=imc]').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.',
        centsLimit: 2
    });
    $("input[name=datamedicao]").mask("99/99/9999", {placeholder: " "});
    $("input[name=datamedicao]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        },
        onClose: function() {
        }
    });

    $("input[name=datamedicao]").change(function() {
        getDadosGrafico();
    });

    rules = {};
    rules.idescola = "required";
    rules.idescolaaluno = "required";
    rules.datamedicao = "required";
    rules.status1 = "required";


    messages = {};

    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function(form) {
            unsaved = false;
            $.blockUI({message: '<h1> Aguarde...</h1>'});
            form.submit();
        }
    });

    //visualizar = false;
    if (visualizar) {
        $("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
            val = $(this).val();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.append(val);
            parent.addClass('visualizar');

            $(this).remove();
        });
        $("div.control-group select").not('[type=hidden]').each(function() {
            val = $(this).find('option:selected').html();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.html(val);
            parent.addClass('visualizar');

        });

        $("label span.required").remove();
    }
});


function setAlunos(obj, idaluno, escola = 0) {
    var idescola;

    if (escola > 0) {
        idescola = escola;
    } else {
        idescola = $(obj).val();
    }

    data = '';
    $("select[name=idescolaaluno]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/nutricoesimcs/setescolasalunos/', {idescola: idescola}, function(data) {
        $("select[name=idescolaaluno]").html(data);

        $("select[name=idescolaaluno]").val(idaluno);
        val = $("select[name=idescolaaluno]").find('option:selected').html();

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function() {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

        if (idaluno == "") {
            $("select[name=idescolaaluno]").closest('div').find('span').html('Selecione...');
        }

    });
}

function carregaGrafico(sexo, idade) {

    if (sexo != "" && idade > 0 && idade != "undefined") {
        var path;
        var divApoio;
        if (idade > 0 && idade < 5) {
            divApoio = "graph-content";

            if (sexo == "Feminino") {
                path = baseUrl + '/public/admin/imagens/feminino_0_5.jpg';
            } else {
                path = baseUrl + '/public/admin/imagens/masculino_0_5.jpg';
            }
        } else {
            divApoio = "graph-plus";
            if (sexo == "Feminino") {
                path = baseUrl + '/public/admin/imagens/feminino_5_19.jpg';
            } else {
                path = baseUrl + '/public/admin/imagens/masculino_5_19.jpg';
            }
        }

        img = '<img src="' + path + '" class="graph-img" alt="' + sexo + '" />';

        $(".graph img").remove();

        $(".infoAdicionar").fadeIn('fast');

        risco = (idade < 5) ? "Risco de " : "";
        sobrepeso = (idade < 5) ? "Sobrepeso" : "Obesidade";
        grave = (idade < 5) ? "" : "grave";

        $("#condicao_risco").html(risco);
        $("#condicao_sobrepeso").html(sobrepeso);
        $("#condicao_obesidade").html(grave);

        div = "<div id='" + divApoio + "'>";
        divApoio = "#" + divApoio;

        $(img).insertBefore(".infoAdicionar");

        if (!$(divApoio).length) {
            $(div).insertAfter(".graph-img");
        }

    }
}

function toDate(dateStr) {
    dateStr = dateStr.substring(0, 10);
    var parts = dateStr.split("-");
    return parts[2] + '/' + parts[1] + '/' + parts[0];
}

function getDadosGrafico() {
    imc = $("input[name=imc]").val();
    datamedicao = $("input[name=datamedicao]").val();

    idaluno = $("select[name=idescolaaluno]").val();

    if (imc != "" && imc > 0 && datamedicao != "" && idaluno > 0) {

        data = '';

        $.post(baseUrl + '/admin/nutricoesimcs/getdadosadd/', {idaluno: idaluno, imc: imc, datamedicao: datamedicao}, function(data) {

            if (data && data != "undefined") {
                if (data.status == "1") {
                    sexo = data.sexo;
                    anos = data.anos;
                    meses = data.meses;
                    nascimento = data.datanascimento;
                    idade = anos + '.' + meses;
                    dtnascimento = toDate(nascimento);

                    $("#aluno_nasc").html(dtnascimento);
                    $("#aluno_idade").html(idade);
                    $("#aluno_imc").html(imc);
                    $("#aluno_sexo").html(sexo);

                    isVisible = $("#main-box").is(":visible");

                    if (!isVisible)
                        $("#main-box").fadeIn('fast');
                    carregaGrafico(sexo, idade);

                    if ((idade >= 0 && idade < 5) && (sexo == "Masculino" || sexo == "Feminino")) {
                        gerarGraficoBox(0, 5, 12, 10, 22);
                        setValorGrafico(imc, meses, anos);
                    } else if ((idade >= 5 && idade <= 19) && (sexo == "Masculino" || sexo == "Feminino")) {
                        gerarGraficoBoxPlus(5, 19, 4, 12, 36, sexo);
                        setValorGraficoPlus(imc, meses, anos);
                    } else {
                        messageErro("A idade deve estar entre 0 e 19 anos. Verifique os dados do aluno");
                    }

                } else {
                    messageErro(data.mensagem);
                }
            }
        }, 'json');
    } else { /*não faça nada */
    }

}

function setDadosAlunos(obj, id = 0) {
    var idaluno;
    if (id > 0) {
        idaluno = id;
    } else {
        idaluno = $(obj).val();
    }

    data = '';
    $.post(baseUrl + '/admin/nutricoesimcs/getdados/', {idaluno: idaluno}, function(data) {
        if (data == "erro") {
            $('.box-dados-alunos').css('display', 'none');
            $(".setdado").html('');
        } else {
            $('.box-dados-alunos').css('display', 'block');
            $(".setdado").html(data);
        }
    });

    if (adicionar)
        getDadosGrafico();
}

function calculoimc() {
    var peso = $('input[name=peso]').val();
    var altura = $('input[name=altura]').val();

    $.post(baseUrl + '/admin/nutricoesimcs/calculaimc/', {peso: peso, altura: altura}, function(data) {
        $('input[name=imc]').val(data);
        var hasClasse = false;
        $(".each-square").each(function() {
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
                hasClasse = true;
            }
        });

        if (!adicionar) {
            idade = form_values.anos + '.' + form_values.meses;
            if (idade < 5) {
                setValorGrafico(data, form_values.meses, form_values.anos);
            } else {
                setValorGraficoPlus(data, form_values.meses, form_values.anos);
            }
        } else {
            getDadosGrafico();
        }
    });
}

function setValorGrafico(imc, meses, anos) {
    //separar o IMC no ponto
    imcparts = imc.toString().split(".");
    mainimc = imcparts[0];
    rest = Math.round(imcparts[1] / 20);
    //garantir que valor não seja menor que 1
    if (rest < 1)
        rest = 1;
    //mostrar ponto do IMC/IDADE no gráfico
    if (mainimc > 22 || mainimc < 9) {
        messageErro("O valor do IMC excede o gráfico. Verifique(altura/peso) e tente novamente");
    } else {
        box = '#sqr_' + mainimc + '_' + rest + '_' + anos + '_' + meses + '';
        $(box).addClass('selected');
    }
}

function setValorGraficoPlus(imc, meses, anos) {
    //pegar valor após o (ponto)
    imcparts = imc.toString().split(".");
    mainimc = imcparts[0];//pegar o imc (inteiro)

    //rest deve ser (1 ou 2)
    rest = Math.round(imcparts[1] / 50);
    //garantir que valor não seja menor que 1
    if (rest < 1)
        rest = 1;

    //meses deve ser de (0 à 3)
    meses = Math.round(meses / 3);
    if (meses > 0)
        meses = meses - 1;
    if (mainimc > 36 || mainimc < 11) {
        messageErro("O valor do IMC excede o gráfico. Verifique(altura/peso) e tente novamente");
    } else {
        box = '#sqr_' + mainimc + '_' + rest + '_' + anos + '_' + (meses);
        $(box).addClass('selected');
    }
}

function messageErro(mensagem) {
    $("#content-error").html(mensagem);
    let NomeJanela = '#modalErro';

    $.fancybox({
        type: 'inline',
        href: NomeJanela,
        width: $('.' + ($(NomeJanela).attr("class"))).width() + 22,
        height: $('.' + ($(NomeJanela).attr("class"))).height() + 22,
        autoSize: false,
        helpers: {
            title: {
                type: 'float'
            }
        }
    });
}

/*
 * Função que gera a separação do gráfico em quadros
 * @param anoinicio //o primeiro ano no gráfico
 * @param anofim //o último ano no gráfico
 * @param mesesgroup //qtd de meses
 * @param imcinicio //IMC mínimo
 * @param imcfim //IMC máximo
 * @return gráfico
 */
function gerarGraficoBox(anoinicio, anofim, mesesgrupo, imcinicio, imcfim) {
    var imcfim = imcfim;
    var element = document.getElementById('graph-content');
    var qntanos = anofim - anoinicio;
    var qntimc = imcfim - imcinicio;

    var meses = mesesgrupo;
    var html;
    html = '<table class="graph-table"><tbody>';

    var first;

    for (first = 0; first < qntanos; first++) {
        html += '<tr class="gph-header">';

        var line;
        for (line = 5; line >= 1; line--) {
            html += '<td class="gph-square">';
            html += '<div class="hold">';
            for (m = 0; m < meses; m++) {
                html += '<div class="each-square" id="sqr_' + imcfim + '_' + line + '_' + first + '_' + m + '"></div>';
            }
            html += '</div>';
            html += '</td>';
        }
        html += '</tr>';
    }
    var col, i, last, collast;
    imcfim = imcfim - 1;
    while (imcfim >= imcinicio) {
        for (j = 0; j < qntanos; j++) {
            html += '<tr class="gph-header">';

            for (i = 5; i >= 1; i--) {
                html += '<td class="gph-square">';
                html += '<div class="hold">';
                for (n = 0; n < meses; n++) {
                    html += '<div class="each-square" id="sqr_' + imcfim + '_' + i + '_' + j + '_' + n + '"></div>';
                }
                html += '</div>';
                html += '</td>';
            }
            html += '</tr>';
        }

        imcfim = imcfim - 1;
    }

    for (last = 0; last < qntanos; last++) {
        html += '<tr class="gph-header">';
        for (collast = 4; collast >= 1; collast--) {
            html += '<td class="gph-square">';
            html += '<div class="hold">';
            for (td = 0; td < meses; td++) {
                html += '<div class="each-square" id="sqr_' + imcfim + '_' + collast + '_' + last + '_' + td + '"></div>';
            }
            html += '</div>';
            html += '</td>';
        }
        html += '</tr>';
    }

    html += '</tbody></table>';

    element.innerHTML = html;
}

function gerarGraficoBoxPlus(anoinicio, anofim, mesesgrupo, imcinicio, imcfim, sexo) {

    var element = document.getElementById('graph-plus');
    var qntanos = anofim - anoinicio;
    var qntimc = imcfim - imcinicio;

    var meses = mesesgrupo;
    var html;
    html = '<table class="graph-table-plus"><tbody>';

    if (sexo == "Feminino") {
        var first;
        for (first = anoinicio; first < anofim; first++) {

            html += '<tr class="gph-header">';

            var line;
            for (line = 2; line >= 1; line--) {
                html += '<td class="gph-square">';
                html += '<div class="hold">';
                for (m = 0; m < meses; m++) {
                    html += '<div class="each-square" id="sqr_' + imcfim + '_' + line + '_' + first + '_' + m + '"></div>';
                }
                html += '</div>';
                html += '</td>';
            }
            html += '</tr>';

        }
    }

    var col, i, last, collast;
    imcfim = imcfim - 1;

    while (imcfim >= imcinicio) {
        for (j = anoinicio; j < anofim; j++) {
            html += '<tr class="gph-header">';

            for (i = 2; i >= 1; i--) {
                html += '<td class="gph-square">';
                html += '<div class="hold">';
                for (n = 0; n < meses; n++) {
                    html += '<div class="each-square" id="sqr_' + imcfim + '_' + i + '_' + j + '_' + n + '"></div>';
                }
                html += '</div>';
                html += '</td>';
            }
            html += '</tr>';
        }
        imcfim = imcfim - 1;
    }

    for (last = anoinicio; last < anofim; last++) {

        html += '<tr class="gph-header">';
        for (collast = 2; collast >= 1; collast--) {
            html += '<td class="gph-square">';
            html += '<div class="hold">';
            for (td = 0; td < meses; td++) {
                html += '<div class="each-square" id="sqr_' + imcfim + '_' + collast + '_' + last + '_' + td + '"></div>';
            }
            html += '</div>';
            html += '</td>';
        }
        html += '</tr>';
    }

    html += '</tbody></table>';

    element.innerHTML = html;
}
