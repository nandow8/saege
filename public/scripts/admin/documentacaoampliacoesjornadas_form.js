var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		// $("select[name=idprofessor]").val(form_values.idprofessor);
		$("select[name=idescola]").val(form_values.idescola);
		setProfessores($("select[name=idescola]"),form_values.idprofessor);
		setSalas($("select[name=idescola]"),form_values.idsala);
		setPeriodos($("select[name=idescola]"),form_values.idperiodo);
		setSeries(form_values.idserie,form_values.idturma);
		// $("select[name=idperiodo]").val(form_values.idperiodo);
		// $("select[name=idsala]").val(form_values.idsala);
		// $("select[name=idserie]").val(form_values.idserie);
		// $("select[name=idturma]").val(form_values.idturma);
		$("select[name=tenhointeresse]").val(form_values.tenhointeresse);
		$("select[name=status1]").val(form_values.status1);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}
	
	$("input[name=horario]").mask("99:99", {placeholder:" "});
	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$('input[name=pontuacao]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});

	rules = {};
	rules.data = "required";
	rules.idprofessor = "required";
	rules.idescola = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
	}
});

function setProfessores(obj , idprofessor){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setprofessores",{idescola: idescola}, function(data) {
		$('select[name=idprofessor]').html(data);
		
		$("select[name=idprofessor]").val(idprofessor);
		val = $("select[name=idprofessor]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idprofessor!=0) && (idprofessor!="")){
			$("select[name=idprofessor]").closest('div').find('span').html('Selecione...');
		
			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
		}

	});
}

function setPeriodos(obj, idperiodo){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idperiodo]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/documentacaoavaliacoesprofissionais/setperiodos/', {idescola:val}, function(data) {
		$("select[name=idperiodo]").html(data);
		$("select[name=idperiodo]").val(idperiodo);
		val = $("select[name=idperiodo]").find('option:selected').html();
		$("select[name=idperiodo]").closest('div').find('span').html(val);
	
		$("select[name=idperiodo]").trigger("chosen:updated");
		
		if(idperiodo==""){
			$("select[name=idperiodo]").closest('div').find('span').html('Selecione...');
		
			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
		}
	});
}

function setSalas(obj, idsala){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idsala]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasvinculos/setsalas/', {idescola:val}, function(data) {
		$("select[name=idsala]").html(data);
		$("select[name=idsala]").val(idsala);
		val = $("select[name=idsala]").find('option:selected').html();
		$("select[name=idsala]").closest('div').find('span').html(val);
	
		$("select[name=idsala]").trigger("chosen:updated");
		
		if(idsala==""){
			$("select[name=idsala]").closest('div').find('span').html('Selecione...');
		
			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
		}
	});
}

function setSeries(idserie, idescolavinculo){
	var idescola = $("select[name=idescola]").val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setseries",{idescola: idescola}, function(data) {

		$('select[name=idserie]').html(data);
		$("select[name=idserie]").val(idserie);
		val = $("select[name=idserie]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idserie!=0) && (idserie!="")){
			$("select[name=idserie]").closest('div').find('span').html('Selecione...');
			
			if((idescolavinculo!=0) && (idescolavinculo!="")){
				$("select[name=idserie]").val(idserie);
				setTurmas(idescolavinculo);
			
				$("select").each(function() {
					val = $(this).find('option:selected').html();
					$(this).closest('div').find('span').html(val);
				});
			}
		}
	});
}

function setTurmas(idturma){

	var idserie = $("select[name=idserie]").val();
	var idescola = $("select[name=idescola]").val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setturmas",{idserie: idserie, idescola:idescola}, function(data) {

		$('select[name=idturma]').html(data); 	
		
		$("select[name=idturma]").val(idturma);
		val = $("select[name=idturma]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idturma!=0) && (idturma!="")){
			$("select[name=idturma]").closest('div').find('span').html('Selecione...');
		
			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
		}
	});
}