var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

function setPesos(preencher, pesos) {
	var qtdquestoes = $("input[name=qtdquestoes]").val();

	$("#input-pesos").html("");

	for (var i = 0; i < qtdquestoes; i++) {
		$("#input-pesos").append(" <label class='label-control'> <input type='text' name='valorquestoes[]' placeholder='Questão "+(i+1)+"'> </label>");
	}

	if( parseInt(qtdquestoes) > 0)
		$("#pesos").show();

	if(preencher) {
		var arr_pesos = new Array();
		arr_pesos = pesos.split(',');

		for (var i = arr_pesos.length - 1; i >= 0; i--) {
			$("input[name='valorquestoes[]'][placeholder='Questão "+(i+1)+"']").val(arr_pesos[i])
		}
	}
}

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	

	$("#pesos").css("display","none");
	
	if ((typeof form_values)!='undefined') {
		setPesos(true, form_values.pesos);

		$("select[name=status1]").val(form_values.status1);
		$("select[name=iddisciplina]").val(form_values.iddisciplina);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}	
	
	rules = {};
	rules.tipoavaliacao = "required";
	rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' });
			form.submitHandlert();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("#pesos input").each(function() {
			$(this).parent().append($(this).attr("placeholder") + ": ");
			$(this).parent().css('font-weight','bold');
		});

		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});

		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();

		$("#btn-pesos").remove();	
	}

	$("input[name=ano]").mask("9999", {placeholder : ""});		
	
});