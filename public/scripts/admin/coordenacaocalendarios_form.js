var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);

$("select[name=status1]").val(form_values.status1);
$("select[name=idescolaForm]").val(form_values.idescolaForm); 


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	  

	$("input[name=ano]").mask("9999", {placeholder:" "});
 

	$("input[name=primeirobimestreinicio]").mask("99/99/9999", {placeholder:" "});
	$("input[name=primeirobimestreinicio]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$("input[name=primeirobimestrefim]").mask("99/99/9999", {placeholder:" "});
	$("input[name=primeirobimestrefim]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$("input[name=segundobimestreinicio]").mask("99/99/9999", {placeholder:" "});
	$("input[name=segundobimestreinicio]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$("input[name=segundobimestrefim]").mask("99/99/9999", {placeholder:" "});
	$("input[name=segundobimestrefim]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$("input[name=terceirobimestreinicio]").mask("99/99/9999", {placeholder:" "});
	$("input[name=terceirobimestreinicio]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$("input[name=terceirobimestrefim]").mask("99/99/9999", {placeholder:" "});
	$("input[name=terceirobimestrefim]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$("input[name=quartobimestreinicio]").mask("99/99/9999", {placeholder:" "});
	$("input[name=quartobimestreinicio]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$("input[name=quartobimestrefim]").mask("99/99/9999", {placeholder:" "});
	$("input[name=quartobimestrefim]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	
	
	rules = {};
	//rules.idsecretaria = "required";
rules.ano = "required";
rules.titulo = "required";
rules.status1 = "required";
rules.idescolaForm = "required"; 
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setTipo(idtipo){
	$.post(baseUrl + "/" + module + "/"+controller+"/settipo", {idtipo: idtipo}, function(data) {
		$('.box-selecionado').html(data);
		$('.idtiposelecionado').val(idtipo);
	});
}

function setDia(mes, dia){
	var idtiposelecionado = $('.idtiposelecionado').val();
	var id = $('input[name=id]').val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setdia", {id:id, idtiposelecionado: idtiposelecionado, mes:mes, dia:dia}, function(data) {
		$('.box_'+mes+'_'+dia).html(data);
	});
}

function setcalendardates(obj){
    obj = $(obj);
    val = obj.val(); 
        $.post(baseUrl + '/admin/coordenacaocalendarios/getcalendardates', {idcalendariobase: val}, function(calendardates){
			calendardates = JSON.parse(calendardates);  

				$('input[name=primeirobimestreinicio]').val("");
				$('input[name=primeirobimestrefim]').val("");
				$('input[name=segundobimestreinicio]').val("");
				$('input[name=segundobimestrefim]').val("");
				$('input[name=terceirobimestreinicio]').val("");
				$('input[name=terceirobimestrefim]').val("");
				$('input[name=quartobimestreinicio]').val("");
				$('input[name=quartobimestrefim]').val(""); 
				var ano = $('input[name=ano]').val(); 
			
				primeirobimestreinicio = calendardates.primeirobimestreinicio.split('-');
				primeirobimestreinicioPTBR = primeirobimestreinicio[2] + "/" +primeirobimestreinicio[1]+"/"+ ano;
			
				primeirobimestrefim = calendardates.primeirobimestrefim.split('-'); 
				primeirobimestrefimPTBR = primeirobimestrefim[2] + "/" +primeirobimestrefim[1]+"/"+ ano;
			
				segundobimestreinicio = calendardates.segundobimestreinicio.split('-');
				segundobimestreinicioPTBR = segundobimestreinicio[2] + "/" +segundobimestreinicio[1]+"/"+ ano;
			
				segundobimestrefim = calendardates.segundobimestrefim.split('-');
				segundobimestrefimPTBR = segundobimestrefim[2] + "/" +segundobimestrefim[1]+"/"+ ano;
			
				terceirobimestreinicio = calendardates.terceirobimestreinicio.split('-');
				terceirobimestreinicioPTBR = terceirobimestreinicio[2] + "/" +terceirobimestreinicio[1]+"/"+ ano;
			
				terceirobimestrefim = calendardates.terceirobimestrefim.split('-');
				terceirobimestrefimPTBR = terceirobimestrefim[2] + "/" +terceirobimestrefim[1]+"/"+ ano;
			
				quartobimestreinicio = calendardates.quartobimestreinicio.split('-');
				quartobimestreinicioPTBR = quartobimestreinicio[2] + "/" +quartobimestreinicio[1]+"/"+ ano;
			
				quartobimestrefim = calendardates.quartobimestrefim.split('-');
				quartobimestrefimPTBR = quartobimestrefim[2] + "/" +quartobimestrefim[1]+"/"+ ano;
				 
				 
				 $('input[name=primeirobimestreinicio]').val(primeirobimestreinicioPTBR);
				 $('input[name=primeirobimestrefim]').val(primeirobimestrefimPTBR);
				 $('input[name=segundobimestreinicio]').val(segundobimestreinicioPTBR);
				 $('input[name=segundobimestrefim]').val(segundobimestrefimPTBR);
				 $('input[name=terceirobimestreinicio]').val(terceirobimestreinicioPTBR);
				 $('input[name=terceirobimestrefim]').val(terceirobimestrefimPTBR);
				 $('input[name=quartobimestreinicio]').val(quartobimestreinicioPTBR);
				 $('input[name=quartobimestrefim]').val(quartobimestrefimPTBR);
 
        }); 
 } 