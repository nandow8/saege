﻿$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=status]").val(form_values.status);	
		if ((typeof form_values.origem)!='undefined') {
			jQuery("select[name=origem]").val(form_values.origem);
			setOrigem(jQuery("select[name=origem]").val(form_values.origem));
		}
		if ((typeof form_values.iddepartamentosecretaria)!='undefined') jQuery("select[name=iddepartamentosecretaria]").val(form_values.iddepartamentosecretaria);
		if ((typeof form_values.idescola)!='undefined') jQuery("select[name=idescola]").val(form_values.idescola);
		if ((typeof form_values.iddepartamentoescola)!='undefined') jQuery("select[name=iddepartamentoescola]").val(form_values.iddepartamentoescola);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});		
	}
	
	$(".line").draggable(
		{cursor: 'pointer', 
		helper: 'clone', 
		opacity: 0.75});
	$(".line").droppable(
		{accept: '.line',
      	drop: function(event, ui) { 
	      	$idTo = $(this).find("input[name='id[]']").attr("value");
	      	$idFrom = ui.draggable.find("input[name='id[]']").attr("value");
	      	
	      	if ($idTo!=$idFrom) {
				
		      	$.post(baseUrl + "/admin/" + controller + "/changeorderxml",
				   	{to: $idTo, from: $idFrom, op: 'change'}, function(data) {
					$message = $(data).find("result").text();
					if ($message=="OK") {
						window.location.reload();
					} else {
						jAlert($message, "Alerta!", function(r) {
							window.location.reload();
						});
					}					      	
			    });		      		
	      	}
	    }
    });
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/admin/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/admin/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/admin/"+controller+"/changestatusxml",
 	 	 	{id: $id, op: controller}, function(data) {
			if ((data=="Ativo") || (data=="Bloqueado")) {
				$obj.html(data);
			} else {
				jAlert(data, "Alerta!");
			}
 	});
} 


function setOrigem(obj) {
	var val = $(obj).val();	
	if(val=="escola"){
		$(".box-escola").css("display", "block");
		$(".box-secretaria").css("display", "none");
		$("select[name=iddepartamentosecretaria]").closest('div').find('span').html('Selecione...');
		
	}else{
		$(".box-secretaria").css("display", "block");
		$(".box-escola").css("display", "none");	
		$("select[name=idescola]").closest('div').find('span').html('Selecione...');
		$("select[name=iddepartamentoescola]").closest('div').find('span').html('Selecione...');
	}
	
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
}

function setDepartamentosEscolas(obj, iddepartamentoescola){
	var val = $(obj).val();
	data = '';
	$("select[name=iddepartamentoescola]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasusuarios/setdepartamentosescolas/', {idescola:val}, function(data) {
		$("select[name=iddepartamentoescola]").html(data);
		$("select[name=iddepartamentoescola]").val(iddepartamentoescola);
		val = $("select[name=iddepartamentoescola]").find('option:selected').html();
		$("select[name=iddepartamentoescola]").closest('div').find('span').html(val);
	
		$("select[name=iddepartamentoescola]").trigger("chosen:updated");
		
		if(iddepartamentoescola==""){
			$("select[name=iddepartamentoescola]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setGrafico(tipo) {
	
	$.fancybox({
		'padding'		: 0,
		'autoScale'		: false,
		'transitionIn'	: 'none',
		'transitionOut'	: 'none',
		'title'			: 'Gráfico',
		'width'		: 800,
		'height'		: 600,
		'href'			: baseUrl + '/admin/'+controller+'/setgraficos/tipo/' + tipo,
		'type'			: 'iframe'
	});

	return false;	
}