function setSeries(idescola, idserie){

	$.post(baseUrl + "/" + module + "/"+controller+"/setseries",{idescola: idescola}, function(data) {

		$("#escolas-series").append(data);

		$("#escolas-series").show();
		
		if( (idserie!=0) && (idserie!="") ) {
			$("div[id=escola-"+idescola+"] select[name='idseries[]']").val(idserie+"-"+idescola);
			val = $("div[id=escola-"+idescola+"] select[name='idseries[]']").find('option:selected').html();
		}
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idserie!=0) && (idserie!="")){
			$("select[name='idserie[]']").closest('div').find('span').html('Selecione...');
		}
	});
}

function hideSeries(idescola, idserie) {

	idescola = idescola.toString();

	$("#escola-"+idescola).remove();

	var i = 0;
	$("#escolas-series div").each(function() { i++ });

	if( i == 0 )
		$("#escolas-series").hide();
}

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};		

	if ((typeof form_values)!='undefined') {

		var idescolas = (form_values.idescolas != "") ? (form_values.idescolas).split(',') : null;
		var idseries = (form_values.idserie != "") ? (form_values.idserie).split(',') : null;

		$("input[type=checkbox][name='idescolas[]']").val(idescolas);

		for (var i = 0; i < idescolas.length; i++) {
			setSeries(idescolas[i],idseries[i]);
		}

		$("select[name=iddisciplina]").val(form_values.iddisciplina);
		$("select[name=idtipoensino]").val(form_values.idtipoensino);
		$("select[name=status1]").val(form_values.status1);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});	
	}

	$("input[name='idescolas[]']").change(function() {
		var checado = ($(this).attr("checked") == 'checked') ? 1 : 0;

		if(checado){
			setSeries($(this).val(), 0);
		} else {
			hideSeries($(this).val(), 0);
		}
	});

	$("input[name=sequencial]").mask("99/9999", {placeholder:" "});

	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	
	rules = {};
	rules.idescola = "required";
	//rules.sequencial = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	

	//visualizar = false;
	if (visualizar) {
		setTimeout( function() {		
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').not('[type=checkbox]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			if ($(this).attr('type') != 'file') 
				val = (val=='') ? '--' : val;		
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});

		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='Selecione...') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');
		});	
		
		$("label span.required").remove();
		$("[type=checkbox]").attr("disabled",true);},750);
	}	
});