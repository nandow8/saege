var unsaved = false;

$("input,select,textarea").change(function () { //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function () { //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function () {
    window.onbeforeunload = function () {
        console.log(unsaved);
        if (unsaved) {
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };
    
    if ((typeof form_values) != 'undefined') {
        
        $("select[name=status1]").val(form_values.status1);
        
        setDadosVeiculo($("select[name=idveiculo]").val(form_values.idveiculo));
        
        $("select").each(function () {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
        
        $("select").each(function () {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });
        
    }
    
    $("input[name=datareserva]").mask("99/99/9999", { placeholder: " " });
    $("input[name=datareserva]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function (date) {
        }
    });
    
    rules = {}; 
    rules.status1 = "required";
    
    messages = {};
    
    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function (form) {
            unsaved = false;
            $.blockUI({ message: '<h1> Aguarde...</h1>' });
            form.submit();
        }
    });
    
    //visualizar = false;
    if (visualizar) {
        $("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function () {
            val = $(this).val();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.append(val);
            parent.addClass('visualizar');
            
            $(this).remove();
        });
        $("div.control-group select").not('[type=hidden]').each(function () {
            val = $(this).find('option:selected').html();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.html(val);
            parent.addClass('visualizar');
            
        });
        
        $("label span.required").remove();
        
    }
    
});

function setDadosVeiculo(obj, values) {
    obj = $(obj);
    val = obj.val();
    var elemento;
    $.post(baseUrl + '/admin/reservasveiculos/getdadosveiculo', { idveiculo: val }, function (veiculo) {
        veiculo = JSON.parse(veiculo);
        console.log(veiculo);
        if (veiculo) {
            elemento = '<div class="control-group">';
                elemento += '<div class="controls">';
                    elemento += '<strong> Marca: </strong>' + veiculo.marca + '<br/>';
                    elemento += '<strong> Modelo: </strong>' + veiculo.modelo + '<br/>';
                    elemento += '<strong> Placa: </strong>' + veiculo.placa + '<br/>';
                    elemento += '<strong> Cor: </strong>' + veiculo.cor + '<br/>';
                    elemento += '<strong> KM: </strong>' + veiculo.kminicial;
                elemento += '</div>';
            elemento += '</div>';
        }
        $('#dados-veiculo').html(elemento);

    });
} 