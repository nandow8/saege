﻿$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=status]").val(form_values.status);	

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});		
	}
	
	$(".line").draggable(
		{cursor: 'pointer', 
		helper: 'clone', 
		opacity: 0.75});
	$(".line").droppable(
		{accept: '.line',
      	drop: function(event, ui) { 
	      	$idTo = $(this).find("input[name='id[]']").attr("value");
	      	$idFrom = ui.draggable.find("input[name='id[]']").attr("value");
	      	
	      	if ($idTo!=$idFrom) {
				
		      	$.post(baseUrl + "/admin/" + controller + "/changeorderxml",
				   	{to: $idTo, from: $idFrom, op: 'change'}, function(data) {
					$message = $(data).find("result").text();
					if ($message=="OK") {
						window.location.reload();
					} else {
						jAlert($message, "Alerta!", function(r) {
							window.location.reload();
						});
					}					      	
			    });		      		
	      	}
	    }
    });
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/admin/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/admin/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/admin/"+controller+"/changestatusxml",
 	 	 	{id: $id, op: controller}, function(data) {
			if ((data=="Ativo") || (data=="Bloqueado")) {
				$obj.html(data);
			} else {
				jAlert(data, "Alerta!");
			}
 	});
} 