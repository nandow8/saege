var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idusuariologado]").val(form_values.idusuariologado);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=idsecretaria]").val(form_values.idsecretaria);
$("select[name=tipo]").val(form_values.tipo);
$("select[name=idaluno]").val(form_values.idaluno);
$("select[name=iddiretora]").val(form_values.iddiretora);
$("select[name=status1]").val(form_values.status1);
setCampos($("select[name=tipo]").val(form_values.tipo));

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	
	$("input[name=datalancamento]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalancamento]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});


$("input[name=hora]").mask("99:99", {placeholder:" "});
$("input[name=horainicio]").mask("99:99", {placeholder:" "});
$("input[name=horafim]").mask("99:99", {placeholder:" "});

$("input[name=ano]").mask("9999", {placeholder:" "});
$("input[name=proximoano]").mask("9999", {placeholder:" "});
$("input[name=vagaano]").mask("9999", {placeholder:" "});
$("input[name=anoregulamentado]").mask("9999", {placeholder:" "});
$("input[name=proximoanoregulamentado]").mask("9999", {placeholder:" "});
$("input[name=solicitacaovaga]").mask("9999", {placeholder:" "});
$("input[name=anosegundoitem]").mask("9999", {placeholder:" "});
$("input[name=anoterceiroitem]").mask("9999", {placeholder:" "});




	$("input[name=validapor]").priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',	    
	    centsLimit: 0    
	});	

	$("input[name=vagadias]").priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',	    
	    centsLimit: 0    
	});	

	$("input[name=diasregulamentado]").priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',	    
	    centsLimit: 0    
	});	

	rules = {};
	rules.datalancamento = "required";
	rules.idescola = "required";
	//rules.idsecretaria = "required";
	rules.idaluno = "required";
	rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setCampos(obj){
	var tipo = $(obj).val();
//	alert(tipo+"_"+"É aluno (a) regularmente matriculado (a) e está frequentando até a presente data o _________ ano do Ensino Fundamental nesta Unidade Escolar no horário das ______ às ______.");
	if(tipo=="É aluno (a) regularmente matriculado (a) e está frequentando até a presente data o _________ ano do Ensino Fundamental nesta Unidade Escolar no horário das ______ às ______."){
		$('.box-tipo1').css('display', 'block');
		$('.box-tipo2').css('display', 'none');
		$('.box-tipo3').css('display', 'none');
		$('.box-tipo4').css('display', 'none');
		$('.box-tipo5').css('display', 'none');
		$('.box-tipo6').css('display', 'none');
	}else if(tipo=="Foi Aluno (a) regularmente matriculado (a) no ____ ano nesta Unidade Escolar, no ano de ____. Tendo sido considerado (a) ___________________________________"){
		$('.box-tipo1').css('display', 'none');
		$('.box-tipo2').css('display', 'block');
		$('.box-tipo3').css('display', 'none');
		$('.box-tipo4').css('display', 'none');
		$('.box-tipo5').css('display', 'none');
		$('.box-tipo6').css('display', 'none');
	}else if(tipo=="Concluiu o __________ ano nesta Unidade Escolar estando apto a cursar o ______________ ano do ensino fundamental"){
		$('.box-tipo1').css('display', 'none');
		$('.box-tipo2').css('display', 'none');
		$('.box-tipo3').css('display', 'block');
		$('.box-tipo4').css('display', 'none');
		$('.box-tipo5').css('display', 'none');
		$('.box-tipo6').css('display', 'none');
	}else if(tipo=="Solicitou uma vaga no __________ ano do Ensino Fundamental nesta Unidade Escolar que poderá ser preenchida pelo (a) mesmo (a) desde que apresente a documentação necessária para a sua matrícula. Esta declaração é válida por _______ dias a partir da presente data."){
		$('.box-tipo1').css('display', 'none');
		$('.box-tipo2').css('display', 'none');
		$('.box-tipo3').css('display', 'none');
		$('.box-tipo4').css('display', 'block');
		$('.box-tipo5').css('display', 'none');
		$('.box-tipo6').css('display', 'none');
	}else if(tipo=="Solicitou nesta data sua transferência para outra Unidade Escolar com direito a matricular-se no _____ ano do Ensino Fundamental. Sua documentação será entregue no prazo máximo de _____ dias."){
		$('.box-tipo1').css('display', 'none');
		$('.box-tipo2').css('display', 'none');
		$('.box-tipo3').css('display', 'none');
		$('.box-tipo4').css('display', 'none');
		$('.box-tipo5').css('display', 'block');
		$('.box-tipo6').css('display', 'none');
	}else if(tipo=="É aluno (a) regularmente matriculado (a) no _______ ano letivo de ________ solicitou sua transferência com direito a matrícula no ________ ano do Ensino Fundamental. Sua documentação será entregue no prazo máximo de _______ dias."){
		$('.box-tipo1').css('display', 'none');
		$('.box-tipo2').css('display', 'none');
		$('.box-tipo3').css('display', 'none');
		$('.box-tipo4').css('display', 'none');
		$('.box-tipo5').css('display', 'none');
		$('.box-tipo6').css('display', 'block');
	}

		$(".select2-container").each(function (){
			$(this).css('max-width', '300px');
		});
		if(tipo != '' && tipo != 'undefined'){
			$('.sample-text').html(tipo).show(); 
			}else{ $('.sample-text').hide(); }
}