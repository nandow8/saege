﻿$(document).ready(function() {

});


function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/admin/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/admin/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/admin/"+controller+"/changestatusxml",
 	 	 	{id: $id, op: controller}, function(data) {
			if ((data=="Ativo") || (data=="Bloqueado")) {
				$obj.html(data);
			} else {
				jAlert(data, "Alerta!");
			}
 	});
} 