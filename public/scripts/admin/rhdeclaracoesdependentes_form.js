var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
$("select[name=idfuncionario]").val(form_values.idfuncionario);
		$("select[name=idfuncionarioescola]").val(form_values.idfuncionarioescola);
$("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	


	
	
	rules = {};
rules.idfuncionario = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});



function iteAddTr(item) {
	
	table = $(".itens table tbody#raiz");
	
	tr = $('<tr />');
	
	var classe_qtd="form-control";
	if (item===false) {
		item = {};
		item.id=0;
		item.nome = '';
		item.grauparentesco = '';
		item.nascimento = '';			
	}
	

	tr.append('<td><input style="width: 95%!Important" value="'+item.nome+'" type="text" maxlength="255" name="nome[]" class="form-control" /></td>');	
	tr.append('<td><input name="idsparentescos[]" type="hidden" value="'+item.id+'" /><input style="width: 95%!Important" value="'+item.grauparentesco+'" type="text" class="form-control" maxlength="255" name="grauparentesco[]" /></td>');
	tr.append('<td><input style="width: 95%!Important" value="'+item.nascimento+'" type="text" maxlength="255" name="nascimento[]" class="form-control data" /></td>');
	
	tr.append('<td style="text-align:center!Important" align="center"><a href="javascript:;" onclick="iteExcluirFromTable(this)"><i class="icon-remove"></i></a></td>');
	qtdMask(tr);
	table.append(tr);
		$(".data").each(function (){

			$(this).mask("99/99/9999", {placeholder:" "});
			$(this).datepicker({
				dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
				dateFormat: 'dd/mm/yy',
				dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
				monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				currentText: 'Hoje',
				prevText: 'Antes',
				nextText: 'Depois',
				onSelect: function(date) {
			    }
			});
		});	
}

function qtdMask(tr) {


		$(".data").each(function (){

			$(this).mask("99/99/9999", {placeholder:" "});
			$(this).datepicker({
				dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
				dateFormat: 'dd/mm/yy',
				dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
				monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
				currentText: 'Hoje',
				prevText: 'Antes',
				nextText: 'Depois',
				onSelect: function(date) {
			    }
			});
		});
	//$("input[name='nascimento[]']").mask("99/99/9999", {placeholder:" "});  
}

function iteExcluirFromTable(obj) {
	$(obj).parent().parent().remove();
}