jQuery(document).ready(function() {
    $("#diasletivos").val(form_values.diasletivos);
    $("#diasletivos_").html(form_values.diasletivos);

    if ((typeof form_values)!='undefined') {
        if ((typeof form_values.status)!='undefined') $("select[name=status]").val(form_values.status);
        if ((typeof form_values.fechado)!='undefined' && form_values.fechado == 'sim') $('#fechado').prop('checked', true);

        $("select").trigger("chosen:updated");
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
    }

    $("[name=datainicio]").mask("99/99/9999");
    $("[name=datafim]").mask("99/99/9999");
    $("[name=valorunitario]").mask("9.99");

    $( function() {
        var dateFormat = "dd/mm/yy",
        from = $( "#from" )
        .datepicker({
          defaultDate: "+1w",
          changeMonth: true,
          numberOfMonths: 1,
          dateFormat: dateFormat
      })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
      }),
        to = $( "#to" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: dateFormat
        })
        .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
        });

        function getDate( element ) {
          var date;
          try {
            date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
            date = null;
        }

        return date;
    }
});

    $("#fechado").change(function () {

        if ($('#fechado').is(':checked')) {
            jAlert('Após confirmar o fechamento não será possivel fazer alterações');
        }
    })

    $(".tipo").change(function() {
        tipo = $("select[name=tipo]").val();
        tipotransporte = $("select[name=tipotransporte]").val();

        if (tipo != '' && tipotransporte != '') {
            $("#div-datas").css('display', 'block');
            $("#from").val('');
            $("#to").val('');
            $("#diasletivos").val('');
            $("#valorunitario").val('');
            $("observacoes").val();
        } else {
            $("#div-datas").css('display', 'none');
            $("#from").val('');
            $("#to").val('');
            $("#diasletivos").val('');
            $("#valorunitario").val('');
            $("observacoes").val();
        }
    });

    $("#from").change(function() {
        inicio = $(this).val();
        aux = inicio.split("/");
        inicio = aux[1]+'/'+aux[0]+'/'+aux[2];
        inicio = new Date(inicio);
    });

    $("#to").change(function() {
        fim = $(this).val();
        aux = fim.split("/");
        fim = aux[1]+'/'+aux[0]+'/'+aux[2];
        fim = new Date(fim);
    });

    $(".fromto").change(function() {
        inicio = $("#from").val();
        fim = $("#to").val();
        
        if (inicio != '' && fim != '') {
            diffDay(inicio, fim);
        }
    });

    $("#diasletivos").change(function() {
        diasletivos = $("#diasletivos").val();
        inicio = $("#from").val();
        fim = $("#to").val();

        if (diffDay(inicio, fim) < diasletivos) {
          $('#diasletivos').val(diffDay(inicio, fim));
      }
  });
});

function diffDay(from, to, tipo, tipotransporte) {

    var diasuteis = 0;

        start = formata_data(from);
        end = formata_data(to);
        $.post(baseUrl + '/admin/ltecalendario/diasuteis/', {start: start, end: end}, function (data) {
            $('#diasletivos_').html(data);
            $('#diasletivos').val(data);
        });
}

//recebe no formato dd/mm/yy e retorna yy/mm/dd
function formata_data(data) {
    aux = data.split("/");
    data = aux[2]+'-'+aux[1]+'-'+aux[0];
    return (data);
}

function setEntidade(obj) {
    var identidade = $(obj).val();
    var idperiodo = $(obj).find('option:selected').data('periodo');

    data = '';
    $.post(baseUrl + '/admin/lteperiodosuniversitarios/setestudantesajax/', {identidade: identidade, idperiodo: idperiodo}, function (data) {
        $("#box-estudantes").html(data);
    });
}