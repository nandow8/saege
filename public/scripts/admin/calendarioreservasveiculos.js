﻿$(document).ready(function() {
		var width = $(".fc-week .fc-first").width();
		var height = width - 15;
		
		$(".fc-week .calendario-dia .box_dia").each(function( index ) {
			$(this).css('height', height);
			var content_evento = $(this).find('.fc-day-content').find('.title-event').height();
			var display = $(this).find('.fc-day-content').find('.title-event').css('display');
			content_evento = parseInt(content_evento) ; 
			
		});
		
    $(window).resize(function() {
		var width = $(".fc-week .fc-first").width();
		var height = width - 15;
		
		$(".fc-week .calendario-dia .box_dia").each(function( index ) {
			$(this).css('height', height);
			var content_evento = $(this).find('.fc-day-content').find('.title-event').height();
			var display = $(this).find('.fc-day-content').find('.title-event').css('display');
			content_evento = parseInt(content_evento) ; 
			
		});
    });

});

function setPage(tipo){
	if(tipo=="inicial"){
		count_page = 0;
	}else if(tipo=="mais"){
		count_page++;
	}else if(tipo=="menos"){
		count_page--;
	}

        $.post(baseUrl + "/admin/calendarioreservasveiculos/setcalendario", {page: count_page}, function(data) {
		if (data!="erro") {
			$('#calendar1').html('');
			$('#calendar1').html(data);
			
			if(count_page!=0){
				$('.fc-button-today').removeClass('fc-state-disabled');
			}else{
				$('.fc-button-today').addClass('fc-state-disabled');
			}
	
		} else {
			jAlert(data, "Erro!");
		}
	});
	
}

function setEventos(obj, dia){
	$.fancybox({
		type: 'iframe',
		href: baseUrl + "/admin/calendarioreservasveiculos/seteventosdia/dia/" + dia,
		width: 300,
		height: 300
	});
}

function setEventoById(id){
	$.fancybox({
		type: 'iframe',
		href: baseUrl + "/admin/calendarioreservasveiculos/setevento/id/" + id,
		width: 300,
		height: 300
	});
}