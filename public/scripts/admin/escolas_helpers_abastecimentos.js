﻿var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

jQuery(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};
	
	if ((typeof form_values)!='undefined') {
		if ((typeof form_values.idtiporede)!='undefined') jQuery("select[name=idtiporede]").val(form_values.idtiporede);
		if ((typeof form_values.idtipotubulacao )!='undefined') jQuery("select[name=idtipotubulacao ]").val(form_values.idtipotubulacao );
		$("select").trigger("chosen:updated");
	}
		
	jQuery('select, input, textarea').bind('blur', function() { });

	rules = {};
	//rules.status = "required";
	//rules.escola = "required";
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
		    
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});	
	
	
});

