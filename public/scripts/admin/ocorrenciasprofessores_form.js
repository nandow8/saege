var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
$("select[name=idaluno]").val(form_values.idaluno);
$("select[name=status1]").val(form_values.status1);
$("select[name=idescolavinculo]").val(form_values.idescolavinculo);
	if ((typeof form_values.idescola)!='undefined' && (typeof form_values.idaluno)!='undefined') {
		setAlunos($("select[name=idescolavinculo]").val(form_values.idescolavinculo), form_values.idaluno);
		//setAlunos($("select[name=idescola]"), form_values.idaluno);
	}


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	
		$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		maxDate: new Date(),
		onSelect: function(date) {
	    }
	});







	
	
	rules = {};
	rules.idaluno = "required";
rules.ocorrencia = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setMaterias(obj, idmateria){
	var idescolavinculo = $(obj).val();
	$("#box-faltas").html("");
	data = '';
	$("select[name=idmateria]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/professoresanexosmateriais/setmaterias/', {idescolavinculo:idescolavinculo}, function(data) {
		$("select[name=idmateria]").html(data);
		$("select[name=idmateria]").val(idmateria);
		
		val = $("select[name=idmateria]").find('option:selected').html();
		$("select[name=idmateria]").closest('div').find('span').html(val);
	
		$("select[name=idmateria]").trigger("chosen:updated");
		
		if(idmateria==""){
			$("select[name=idmateria]").closest('div').find('span').html('Selecione...');
		}
	});
}
function setAlunos(obj, idaluno){
	var idescola = $(obj).val();
	var idescolavinculo = $(obj).val();
	var id = $('input[name=id]').val();
	
	data = '';
	$("select[name=idaluno]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/'+controller+'/setescolasalunos/', {id: id, idescolavinculo:idescolavinculo}, function(data) {
		$("select[name=idaluno]").html(data);

		$("select[name=idaluno]").val(idaluno);
		val = $("select[name=idaluno]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});

		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idaluno==""){
			$("select[name=idaluno]").closest('div').find('span').html('Selecione...');

			//$('.box-dados-alunos').css('display', 'none');
			//$(".setdado").html('');	
		}
		
	});
}
