var unsaved = false;

$("input,select,textarea").change(function() { //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function() { //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
    window.onbeforeunload = function() {
        console.log(unsaved);
        if (unsaved) {
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };

    if ((typeof form_values) != 'undefined') {        
        $("select[name=idsecretaria]").val(form_values.idsecretaria);
        $("select[name=idlocais]").val(form_values.idlocais);
        $("select[name=idsdepartamentossecrtaria]").val(form_values.idsdepartamentossecrtaria);
        $("select[name=idsprofessores]").val(form_values.idsprofessores);
        $("select[name=idescola]").val(form_values.idescola);        
        $("select[name=idsalunos]").val(form_values.idsalunos);
        $("select[name=status1]").val(form_values.status1);
       
        if (form_values.idperfil == 28) {            
            $('select[name=idperfil]').val(29);
            $("select").each(function() {
                val = $(this).find('option:selected').html();
                $(this).closest('div').find('span').html(val);
            });
            $('#cmp-prof').attr('checked', 'true');
            setUsuarios($("select[name=idperfil]"), $("select[name=idescola]"), form_values.idsprofessores);

        } else {
            $("select[name=idperfil]").val(form_values.idperfil);
        }


        if ((typeof form_values.idsprofessores) != 'undefined') {
            setUsuarios($("select[name=idperfil]"), $("select[name=idescola]"), form_values.idsprofessores);
        }

        if (form_values.idescola == '0' || form_values.idescola == '')
            showHideEscolas();

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function() {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

    }
    rules = {};
    rules.titulo = "required";
    messages = {};

    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function(form) {
            unsaved = false;
            $.blockUI({message: '<h1> Aguarde...</h1>'});
            form.submit();
        }
    });

    $('input[name=sendTo]').change(function() {
        setUsuarios('select[name=idperfil]', 'select[name=idescola]', 0);
    });
});

$(window).load(function() {
    //visualizar = false;
    if (visualizar) {
        if (form_values.idperfil == 28) {            
            $('select[name=idperfil]').val(29);
            $("select").each(function() {
                val = $(this).find('option:selected').html();
                $(this).closest('div').find('span').html(val);
            });
            setUsuarios($("select[name=idperfil]"), $("select[name=idescola]"), form_values.idsprofessores);

        }
        $("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
            val = $(this).val();
            val = $.trim(val);
            val = (val === '') ? '--' : val;
            parent = $(this).parent();
            parent.append(val);
            parent.addClass('visualizar');

            $(this).remove();
        });
        $("div.control-group select").not('[type=hidden]').each(function() {
            val = $(this).find('option:selected').html();
            val = $.trim(val);
            val = (val === '' || val === 'Selecione...') ? '--' : val;
            parent = $(this).parent();
            parent.html(val);
            parent.addClass('visualizar');

        });

        $("div.control-group input[type=checkbox]").each(function() {
            /*val = $(this).find('option:selected').html();
             val = $.trim(val);
             val = (val=='') ? '--' : val;
             parent = $(this).parent();
             parent.html(val);
             parent.addClass('visualizar');*/

        });

        $("label span.required").remove();
    }
});

function showHideEscolas() {
    $('input[name="setescola"]').val('nao');
    $('.box-escolas').hide();
    $('.box-receptor').hide();

    $('select[name=idescola]').val('');
    $('#cmp-sec').attr('checked', true);
}

function setUsuarios(obj, idescola, values) {
    obj = $(obj);
    val = obj.val();
    idescola = $(idescola).val();

    prodir = '';
    if (val == '29') {
        prodir = (visualizar) ? $('input[name=sendTo]').val() : $('input[name=sendTo]:checked').val();

        /**28 => professores
         * 29 => secretarias
         **/
        val = (prodir === 'diretoria') ? '29' : '28';

        $('input[name="setescola"]').val('sim');
        $('.box-escolas').show();
        $('.box-receptor').show();
    } else {
        $('input[name="setescola"]').val('nao');
        $('.box-escolas').hide();
        $('.box-receptor').hide();

        $('select[name=idescola]').val('');
        $('#cmp-sec').attr('checked', true);

        $("select[name=idescola]").each(function() {
            valor = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(valor);
        });
    }

    if (idescola === '-1' || idescola === 'undefined' || idescola === '') {
        $('#cmp-sec').attr('checked', true);
        $('#edge_2').fadeOut();
    } else
        $('#edge_2').fadeIn();

    if (val !== '') {
        $.blockUI({message: '<h1> Aguarde...</h1>'});
        $("#professores-container").html("Aguarde!");

        $.post(baseUrl + '/admin/comunicacaocomunicados/getusuarios', {id: val, idescola: idescola, departamento: prodir}, function(usuarios) {

            $.unblockUI();
            usuarios = JSON.parse(usuarios);
            if (usuarios.length == 0) {
                $("#professores-container").html("Nenhum funcionário foi encontrado");
            } else {
                $("#professores-container").html("");
                if (!visualizar)
                {
                    $("#professores-container").append('<p id="marcacao"><div id="marcar" class="btn btn-info" style="cursor: pointer" onclick="setandotudo(this.id)" >Marcar todos</div></p><br>');
                }

                for (var i = 0; i < usuarios.length; i++) {
                    $("#professores-container").append('<div><input name="idsprofessores[]" class="elemento" type="checkbox" value="' + usuarios[i].id + '">' + usuarios[i].rgf + ' - ' + usuarios[i].nomerazao + ' ' + usuarios[i].sobrenomefantasia + ' <br /></div>');
                }
                if (values) {
                    $("[name='idsprofessores[]']").val(values);
                    if (visualizar) {
                        $("div.control-group input[type=checkbox]").each(function() {
                            val = $(this).attr('checked');
                            if (val == "checked") {
                                $(this).remove();
                            } else {
                                $(this).parent().remove();
                            }
                        });
                    }
                }
            }
        });
    } else {
        $("#professores-container").html("Selecione um local!");
    }
}