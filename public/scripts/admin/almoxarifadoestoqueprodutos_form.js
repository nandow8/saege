$(document).ready(function() {
    $('#foto_produto').mouseenter(function () {
        $('#foto_grande').animate({
            width: 'toggle', duration: 0
        });
    });
    $('#foto_produto').mouseleave(function () {
        $('#foto_grande').animate({
            width: 'toggle', duration: 0
        });
    });

    if ((typeof form_values)!='undefined') { 
        $("select[name=autorizacaonumero]").val(form_values.autorizacaonumero);
	
    }
 
    
    rules = {};	
    messages = {};
    rules.autorizacaonumero = "required";
    rules.idperfilsolicitante = "required";
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
    });	
    

    $('#find-produtos').click(function() {

        var idproduto = $('#idproduto').val();
        var codigoproduto = $('#codigoproduto').val();
        var categoria = $('#categoria').find('option:selected').val();
        var unidademedida = $('#unidademedida').find('option:selected').val();
        var produto = $('#produto').val();
        var descricao = $('#descricao').val();
        var categoriaprodutos = $('#categoriaprodutos').val();

        $.post(baseUrl + '/admin/almoxarifadoestoqueprodutos/findprodutos/', {idproduto: idproduto, codigoproduto: codigoproduto, categoria: categoria, unidademedida: unidademedida, produto: produto, descricao: descricao, categoriaprodutos: categoriaprodutos}, function(data) {
            if (data == 0) {
                $('#modal-produtos').html('Nenhum produto encontrado');
            } else if (data == -1) {
                $('#modal-produtos').html('Utilize mais critérios de busca');
            } else {
                $('#produtos').css('height', '550px');
                $('#modal-produtos').html(data);
            } 
        });
    });

    $('#add-produto').click(function() {

        itens = new Array();
        $("input[type=checkbox][name='item[]']:checked").each(function(){
            itens.push($(this).val());
        });
        count = 0;
        itens.forEach(function (iditem) {
             
            var codigoproduto = $('#codigoproduto_'+iditem).val();
            var produto = $('#produto_'+iditem).val();
            var unidademedida = $('#unidademedida_'+iditem).val();
            var idproduto = $('#idproduto_'+iditem).val(); 

            var verifica = findduplicados(iditem);
            if (!verifica) {
                var tr = '<tr>'
                +'<td>'+'<input type="hidden" name="codigoproduto[]  data-item="'+iditem+'" value="'+ codigoproduto +'">'+codigoproduto+'</td>'
                +'<td>'+'<input type="hidden" name="produto[]  data-item="'+iditem+'" value="'+ produto +'">'+produto+'</td>'
                +'<td>'+'<input type="hidden" name="unidademedida[]  data-item="'+iditem+'" value="'+ unidademedida +'">'+unidademedida+'</td>' 
                +'<td><input type="number" name="qtd_selecionada[]" data-item="'+iditem+'" min="0" value="0" id="qtd_selecionada'+iditem+'"><input type="hidden" name="id_selecionado[]" value="'+iditem+'" ></td>'
                +'<td style="text-align: center !important; padding:8px 0 8px 22px;" align="center"><input type="checkbox" name="urgencia'+count+'" id="urgencia" value="urgencia"/></td>'
                +'<td><button type="button" class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal'+iditem+'"  >Observação</button></td>'
                +'<td style="text-align: center !important; padding:8px 0 8px 22px;" align="center"><a href="javascript:;" onclick="membrosExcluirFromTable(this)"><span><i class="icon-remove"></i></span></a></td>'
                +'<input type="hidden" name="idproduto[]  data-item="'+iditem+'" value="'+ idproduto +'">'
                +'</tr>';
                var modal = '<div class="modal fade" id="myModal'+iditem+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">'
                            +'<div class="modal-dialog" role="document">'
                                +'<div class="modal-content">'
                                    +'<div class="modal-header">'
                                        +'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                                        +'<h4 class="modal-title" style="text-align:center;" id="myModalLabel">Observação</h4>'
                                    +'</div>'
                                    +'<div class="modal-body"> '
                                        +'<div class="control-group">' 
                                            +'<textarea class="span12" name="observacaoentrada'+count+'" rows="12" cols="70" ></textarea>'
                                        +'</div>'
                                        
                                        +'<button type="button" class="btn btn-danger" style="float:right; margin-left: 5px;" data-dismiss="modal">Sair</button>' 
                                        +'<button type="button" class="btn btn-success" style="float:right;" data-dismiss="modal">Salvar</button>' 
                                        
                                    +'</div>'
                                +'</div>'
                            +'</div>'
                        +'</div>'
               

                $('#solicitacao').append(tr);
                $('#solicitacao').append(modal);
            }

            count = count +1;
        });
    });
});

function confequantidadenoestoque(){
    itens = new Array();
    $("input[type=checkbox][name='item[]']:checked").each(function(){
        itens.push($(this).val());
    });
    
    var validation = 0;

    itens.forEach(function (iditem) {

        var quantidade = parseInt($('#qtd_selecionada'+iditem).val());
        var qtd_selecionada = parseInt($('#qtd_liberado'+iditem).val());
        
        console.log('quantidade: ' + quantidade + ' - qtd_selecionada: ' + qtd_selecionada)
        
        if( quantidade  <  qtd_selecionada ){
            $('#qtd_selecionada'+iditem).css({"border-color" : "#F00", "padding": "2px"});
            $('#qtd_liberado'+iditem).css({"border-color" : "#F00", "padding": "2px"});
            $( "#valida-campos" ).prop( "disabled", true );
            validation += 0;
        }else{
            $('#qtd_selecionada'+iditem).css({"border-color" : "#00BFFF"});
            $('#qtd_liberado'+iditem).css({"border-color" : "#00BFFF"});
            validation += 1;
        } 
      
    }); 
    
    if(validation >= itens.length)
      $( "#valida-campos" ).prop( "disabled", false ); 

    $("#form").submit(function() {
        if(validation < itens.length){    
            return false;
        }
    });

}

function findduplicados(item) {

    itensduplicados = new Array();
    $("input[type=number][name='qtd_selecionada[]']").each(function() {
        itensduplicados.push($(this).data('item'));
    });

    var i = 0;

    while (i < itensduplicados.length) {
        if (itensduplicados[i] == item) {
            return true;
        } else {
            i++;
        }
    }

    return false;
}

function getProdutos() {
    $.blockUI({ message: '<h1> Aguarde...</h1>' });
    setTimeout(function(){
        var idproduto = $("input[name=idproduto]").val();
        var codigoproduto = $("input[name=codigoproduto]").val();
        var categoria = $("select[name=categoria]").val();
        var unidademedida = $("select[name=unidademedida]").val();
        var produto = $("input[name=produto]").val();
        var descricao = $("input[name=descricao]").val();

        $.post(baseUrl + '/admin/almoxarifadoestoqueprodutos/getprodutos/', {idproduto: idproduto, codigoproduto: codigoproduto, categoria: categoria, unidademedida: unidademedida, produto: produto, descricao: descricao}, function (data) {
            if (data == 0) {
                $("#box-produtos").html("Nenhum produto encontrado");
            } else if (data == -1) {
                $("#box-produtos").html("Utilize mais critérios de busca");
            } else {
                $("#box-produtos").html(data);
            }
        });
        $.unblockUI();
    },600);
}

function ckbox() {
    if($('#all-checkbox').is(':checked')) {
        $('.ckbox').prop('checked', true);
    } else {
        $('.ckbox').prop('checked', false);
    }
}

function membrosExcluirFromTable(obj) {
	jConfirm("Deseja excluir o produto da lista?", 'Confirmar', function(r) {
		
        if(r)
        {
            obj = $(obj);
            content = obj.closest('tr');
            content.find('input').remove();
            content.hide();
        }
		
	});
} 