$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=idescolafuncionario]").val(form_values.idescolafuncionario);
		$("select[name=idequipesupervisao ]").val(form_values.idequipesupervisao );
		$("select[name=secretaria]").val(form_values.secretaria);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idtipoensino]").val(form_values.idtipoensino);
		$("select[name=idperiodosupervisao]").val(form_values.idperiodosupervisao);
		$("select[name=idveiculo]").val(form_values.idveiculo);
		$("select[name=status1]").val(form_values.status1);
		$("select[name=tipo_filter]").val(form_values.tipo_filter);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});

		if(form_values.tipo_filter){			
			changeTipo(form_values.tipo_filter);
		}
	}
	
	$("input[name=data_i],input[name=data_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data_i],input[name=data_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});	
});
$("select[name=tipo_filter]").change(function(){
	valor = $(this).val();
	changeTipo(valor);
});
function changeTipo(valor){
	switch(valor){
		case 'byfunc':			
			$("#idbyequipe, #all").fadeOut('fast', function(){
				$("select[name=idequipesupervisao]").val('');
				$("#idbyfunc").fadeIn('fast');
			});
		break;
		case 'byequipe':			
			$("#idbyfunc").fadeOut('fast', function(){
				$("select[name=idescolafuncionario]").val('');
				$("#idbyequipe").fadeIn('fast');
				$("#all").fadeOut('fast');
			});
		break;
		default:
			$("#idbyfunc").fadeOut('fast', function(){
				$("select[name=idescolafuncionario], select[name=idequipesupervisao]").val('');
				$("#all").fadeIn('fast');
				$("#idbyequipe").fadeOut('fast');
			});			
	}
}

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Ativo") || (data=="Bloqueado")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 