var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=local]").val(form_values.local);
		$("select[name=idresponsavel]").val(form_values.idresponsavel);
		$("select[name=status1]").val(form_values.status1);
		setFuncionarios($("select[name=idescola]"), form_values.idresponsavel);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	
	$("input[name=telefone]").mask("(99)9999-9999", { placeholder: " " });
	$("input[name=telefone2]").mask("(99)9999-99999", { placeholder: " " }); 





	
	
	rules = {};
//	rules.nomerazao = "required";
//rules.sobrenomefantasia = "required";
rules.telefone = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setFuncionarios(obj, idresponsavel){
	var idescola = $(obj).val();
	data = '';
	$("select[name=idresponsavel]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/solicitacoesrecepcoes/setescolausuarioresponsavel/', {idescola:idescola}, function(data) {
		$("select[name=idresponsavel]").html(data);

		$("select[name=idresponsavel]").val(idresponsavel);
		val = $("select[name=idresponsavel]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idresponsavel==""){
			$("select[name=idresponsavel]").closest('div').find('span').html('Selecione...');
		}
	});
}