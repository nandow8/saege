﻿var unsaved = false;
$("input,select,textarea").change(function() { //trigers change in all input fields including text type
    // unsaved = true;
});
$("input,select,textarea").click(function() { //trigers change in all input fields including text type
    // unsaved = true;
});

jQuery(document).ready(function() {

    window.onbeforeunload = function() {
        console.log(unsaved);
        if (unsaved) {
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };

    $('.box-necessidades').css('display', 'none');

    $("select[name=idescola]").val(form_values.idescola);
    setDados(jQuery("input[name=idescola]").val(form_values.idescola), form_values.idescola);
    if ((typeof form_values) != 'undefined') {
        if ((typeof form_values.status1) != 'undefined')
            $("select[name=status1]").val(form_values.status1);
        if ((typeof form_values.matricula) != 'undefined')
            jQuery("select[name=matricula]").val(form_values.matricula);
        if ((typeof form_values.classificacao) != 'undefined')
            jQuery("select[name=classificacao]").val(form_values.classificacao);
        if ((typeof form_values.idserie) != 'undefined')
            setSeries(jQuery("select[name=idescola]").get(0), form_values.idserie);
        if ((typeof form_values.idperiodo) != 'undefined')
            setPeriodos(jQuery("select[name=idescola]").get(0), form_values.idperiodo);
        if ((typeof form_values.idescola) != 'undefined')
            setClassificacoes(jQuery("input[name=idescola]").val(form_values.idescola), form_values.idclassificacao);
        if ((typeof form_values.passeescolar) != 'undefined')
            jQuery("select[name=passeescolar]").val(form_values.passeescolar);
        if ((typeof form_values.transporteescolar) != 'undefined')
            jQuery("select[name=transporteescolar]").val(form_values.transporteescolar);
        if ((typeof form_values.sexo) != 'undefined')
            jQuery("select[name=sexo]").val(form_values.sexo);
        if ((typeof form_values.etnia) != 'undefined')
            jQuery("select[name=etnia]").val(form_values.etnia);
        if ((typeof form_values.quilombola) != 'undefined')
            jQuery("select[name=quilombola]").val(form_values.quilombola);
        if ((typeof form_values.nova_cert_tipo_livro_reg) != 'undefined') {
            jQuery("select[name=nova_cert_tipo_livro_reg]").val(form_values.nova_cert_tipo_livro_reg)
            setTipoCertidao(jQuery("select[name=nova_cert_tipo_livro_reg]").val(form_values.nova_cert_tipo_livro_reg), form_values.nova_cert_tipo_livro_reg);
        }

        if ((typeof form_values.estadocivilmae) != 'undefined')
            jQuery("select[name=estadocivilmae]").val(form_values.estadocivilmae);
        if ((typeof form_values.estadocivilpai) != 'undefined')
            jQuery("select[name=estadocivilpai]").val(form_values.estadocivilpai);
        if ((typeof form_values.estadocivilresponsavel) != 'undefined')
            jQuery("select[name=estadocivilresponsavel]").val(form_values.estadocivilresponsavel);
        if ((typeof form_values.formaingresso) != 'undefined')
            jQuery("select[name=formaingresso]").val(form_values.formaingresso);
        if ((typeof form_values.merendadifenciada) != 'undefined')
            jQuery("select[name=merendadifenciada]").val(form_values.merendadifenciada);
        if ((typeof form_values.merendadifenciadaaprovacao) != 'undefined')
            jQuery("select[name=merendadifenciadaaprovacao]").val(form_values.merendadifenciadaaprovacao);
        if ((typeof form_values.necessidadesespeciais) != 'undefined') {
            jQuery("select[name=necessidadesespeciais]").val(form_values.necessidadesespeciais)
            setCamposNecessidades(jQuery("select[name=necessidadesespeciais]").val(form_values.necessidadesespeciais), form_values.idinstituicaoapoio);
        }

        $("select").trigger("chosen:updated");
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function() {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

    }

    if((typeof doc_values) != 'undefined'){        
        if(doc_values.nacionalidade) $('select[name=inNacionalidade]').val(doc_values.nacionalidade);

        if(doc_values.nacionalidade && doc_values.nacionalidade > 1){
            $("#paisorigem, #entradanopais").fadeIn();
            if(doc_values.paisorigem) $('select[name=inPaisOrigem]').val(doc_values.paisorigem);
        }
        
        if(doc_values.tipocertidao) $('select[name=tipocertidao]').val(doc_values.tipocertidao);
        
        if(doc_values.tipocertidao && doc_values.tipocertidao == 1){
            $("#box-certidaoantiga").fadeIn();
        }else if(doc_values.tipocertidao && doc_values.tipocertidao == 2){
            $("#box-certidaonova").fadeIn();
        }else{ $("#box-certidaoantiga, #box-certidaonova").fadeOut(); }

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function() {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });
    }
    jQuery('select, input, textarea').bind('blur', function() {});

    $('input[name=rendafamiliar]').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.',
        centsLimit: 2
    });

    $('.click_acc').each(function() {
        $(this).click();
    });

    $("[name=nascimento]").mask("99/99/9999");
    $("[name=nascimentomae]").mask("99/99/9999");
    $("[name=nascimentopai]").mask("99/99/9999");
    $("[name=nascimentoresponsavel]").mask("99/99/9999");

    $("[name=cpfcnpj]").mask("999.999.999-99");
    $("[name=cpfcnpjmae]").mask("999.999.999-99");
    $("[name=cpfcnpjpai]").mask("999.999.999-99");
    $("[name=cpfcnpjresponsavel]").mask("999.999.999-99");

    //formata certidão nova
    $("[name=nova_cert_cod_nasc_serv]").mask("999999", {placeholder: ""});
    $("[name=nova_cert_cod_acervo]").mask("99", {placeholder: ""});
    $("[name=nova_cert_cod_serv_reg_civil]").mask("99", {placeholder: ""});
    $("[name=nova_cert_ano_reg_nasc]").mask("9999", {placeholder: ""});
    $("[name=nova_cert_tipo_livro_reg]").mask("9", {placeholder: ""});
    $("[name=nova_cert_num_livro]").mask("99999", {placeholder: ""});
    $("[name=nova_cert_num_folha]").mask("999", {placeholder: ""});
    $("[name=nova_cert_num_termo_reg]").mask("9999999", {placeholder: ""});
    $("[name=digver_certidao_nasc_nova]").mask("99", {placeholder: ""});

    //formata certidão antiga
    $("[name=numero_certidao_nasc]").mask("999999", {placeholder:""});

    $.validator.addMethod("byLength", function(value, element) {

        if (value.length <= 1) {
            $(element).next('.erros').fadeIn();
            return false;
        } else {
            $(element).next('.erros').fadeOut();
            return true;
        }

    }, "Campo obrigatório");

    $.validator.addMethod("notEmpty", function(value, element) {

        if (value == "") {
            $(element).next('.erros').fadeIn();
            return false;
        } else {
            $(element).next('.erros').fadeOut();
            return true;
        }

    }, "Campo obrigatório");

    rules = {}
    //rules.nascimento = "required";
    //rules.nomerazao = "required";
    //rules.rgm = "required";
    //rules.matricula = { byLength: true };		// Tire pq estava travando o cadastro
    // rules.sobrenomefantasia = "required";
    rules.status1 = {notEmpty: true};
    rules.idescola = {notEmpty: true};
    rules.idserie = {notEmpty: true};
    rules.idperiodo = {notEmpty: true};
    rules.cpfcnpj = {required: false, mn_cnpj: true};

    messages = {};
    messages.cpfcnpj = {required: "", mn_cnpj: "CPF inválido"};
    messages.matricula = {notEmpty: "Por favor preencha o campo"};


    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        errorPlacement: function(error, element) {
            if (element.is('select')) {
                error.insertAfter($(element).parent().find('div.chosen-container'));
            } else {
                error.insertAfter($(element));
            }

        },
        submitHandler: function(form) {
            unsaved = false;
            form.submit();
        }
    });

    $(window).load(function() {
        if (visualizar) {
            $("div.control-group input, div.control-group textarea").not('.checkbox, [type=hidden]').each(function() {
                val = $(this).val();
                val = $.trim(val);
                val = (val == '') ? '--' : val;

                parent = $(this).parent();
                parent.append(val);
                parent.addClass('visualizar');

                $(this).remove();
            });
            $("div.control-group select").not('[type=hidden]').each(function() {
                val = $(this).find('option:selected').html();

                val = $.trim(val);
                val = (val == '' || val == 'Selecione...') ? '--' : val;
                parent = $(this).parent();
                parent.html(val);
                parent.addClass('visualizar');
            });

            $("label span.required").remove();

            $('.checkbox').each(function() {
                if ($(this).is(':checked')) {

                    parent = $(this).parent();
                    parent.css({'font-weight': 'bold', 'color': '#09c546'});
                    $(this).remove();
                } else {
                    parent = $(this).parent();

                    $(this).remove();
                }
            });
        }
    });
});


function setDados(obj, idescola) {
    var val = $(obj).val();

    setSeries($("input[name=idescola]").val(val), 0);
    setPeriodos($("input[name=idescola]").val(val), 0);
    setClassificacoes($("input[name=idescola]").val(val), 0);
}

function setSeries(obj, idserie) {
    var val = $(obj).val();
    data = '';
    $("select[name=idserie]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasalunos/setseries/', {idescola: val}, function(data) {

        $("select[name=idserie]").html(data);
        $("select[name=idserie]").val(idserie);

        val = $("select[name=idserie]").find('option:selected').html();

        $("select[name=idserie]").closest('div').find('span').html(val);

        $("select[name=idserie]").trigger("chosen:updated");

        if (idserie == "") {
            $("select[name=idserie]").closest('div').find('span').html('Selecione...');
        }
    });
}

function setAtribuicoesSeries(obj, idserie) {
    var val = $(obj).val();

    data = '';
    $("#all-series").html('<option value="">Aguarde...</option>');

    $.post(baseUrl + '/admin/escolassalasalunos/setseriesatribuicoes/', {idescola: val}, function(data) {

        $("#all-series").html(data);
        $("#all-series").val(idserie);
        val = $("#all-series").find('option:selected').html();
        $("#all-series").closest('div').find('span').html(val);

        $("#all-series").trigger("chosen:updated");

        if (idserie == "") {
            $("#all-series").closest('div').find('span').html('Selecione...');
        }
    });
}

function setPeriodos(obj, idperiodo) {

    var val = $(obj).val();

    data = '';
    $("select[name=idperiodo]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasalunos/setperiodos/', {idescola: val}, function(data) {
        $("select[name=idperiodo]").html(data);
        $("select[name=idperiodo]").val(idperiodo);

        val = $("select[name=idperiodo]").find('option:selected').html();
        $("select[name=idperiodo]").closest('div').find('span').html(val);

        $("select[name=idperiodo]").trigger("chosen:updated");

        if (idperiodo == "") {
            $("select[name=idperiodo]").closest('div').find('span').html('Selecione...');
        }
    });
}

function setClassificacoes(obj, idclassificacao) {
    var val = $(obj).val();

    data = '';
    $("select[name=idclassificacao]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasalunos/setclassificacoes/', {idescola: val}, function(data) {
        $("select[name=idclassificacao]").html(data);
        $("select[name=idclassificacao]").val(idclassificacao);
        val = $("select[name=idclassificacao]").find('option:selected').html();
        $("select[name=idclassificacao]").closest('div').find('span').html(val);

        $("select[name=idclassificacao]").trigger("chosen:updated");

        if (idclassificacao == "") {
            $("select[name=idclassificacao]").closest('div').find('span').html('Selecione...');
        }
    });
}


function setCamposNecessidades(obj, r_val) {
    var necessidade = $(obj).val();

    if (necessidade == "Sim") {
        $('.box-necessidades').css('display', 'block');
    } else {
        $('.box-necessidades').css('display', 'none');
    }

    if (r_val) {
        jQuery("select[name=idinstituicaoapoio]").val(r_val);
    }
    $("select[name=necessidadesespeciais]").trigger("chosen:updated");
    //$("select[name=idinstituicaoapoio]").trigger("chosen:updated");
}

function setPais(obj){
    valor = $(obj).val();

    if(valor > 1){
        $("#paisorigem, #entradanopais").fadeIn();
    }else{
        //zerar campos
        $("select[name=inPaisOrigem]").val("");
        $("input[name=inDiaEntBrasil]").val("");

        val = $("select[name=inPaisOrigem]").find('option:selected').html();        
        $("select[name=inPaisOrigem]").closest('div').find('span').html(val);
        $("#paisorigem, #entradanopais").fadeOut();
    }
}

function setTipoCertidao(obj, r_val) {

    var tipocertidao = $(obj).val();

    if (tipocertidao == "1") {
        document.getElementById('box-certidaoantiga').style.display = 'block';
        document.getElementById('box-certidaonova').style.display = 'none';
    } else if (tipocertidao == "2") {
        document.getElementById('box-certidaoantiga').style.display = 'none';
        document.getElementById('box-certidaonova').style.display = 'block';
    } else {
        document.getElementById('box-certidaoantiga').style.display = 'none';
        document.getElementById('box-certidaonova').style.display = 'none';
    }
}



function iteAddTrProgramasSociais(item) {

    table = $(".itens_programas table tbody#raiz");

    tr = $('<tr />');

    if (item === false) {
        item = {};
        item.id = 0;
        item.idprograma = '';
        item.situacao = '';
    }
    //var item = new Object();
    //alert(item);

    select_programas = $('<select class="form-control c_programas" name="idsprogramas[]" />');
    select_programas.append('<option value="">Selecione...</option>');
    for (var i = 0; i < programas.length; i++) {
        d = programas[i];
        select_programas.append('<option value="' + d.k + '">' + d.v + '</option>');
    }
    select_programas.val(item.idprograma);
    td_programas = $('<td />');
    $(select_programas).trigger("chosen:updated");
    td_programas.append(select_programas);

    tr.append(td_programas);

    select_situacao = $('<select name="situacoes[]" class="form-control c_situacoes" />');
    select_situacao.append('<option value="Inativo">Inativo</option>');
    select_situacao.append('<option value="Ativo">Ativo</option>');
    select_situacao.val(item.situacao);
    td_situacao = $('<td />');
    $(select_situacao).trigger("chosen:updated");
    td_situacao.append('<input name="idsitens[]" type="hidden" value="' + item.id + '" />');
    td_situacao.append(select_situacao);
    tr.append(td_situacao);

    tr.append('<td style="text-align:center!Important" align="center"><a href="javascript:;" onclick="iteExcluirFromTable(this)"><i class="fa fa-trash-o"></i></a></td>');
    //qtdMask(tr);
    table.append(tr);
}

function iteExcluirFromTable(obj) {
    $(obj).parent().parent().remove();
}

function qtdMask(tr) {
    $(tr).find("input[name='anos[]']").mask("9999");

    $(tr).find("input[name='datas[]']").mask("99/99/9999", {placeholder: " "});
    $(tr).find("input[name='datas[]']").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois'
    });
}

window.excluirIrmao = function (id, texto) {
    jConfirm('Excluir "'+texto+'" dos irmãos associados?', 'Excluir registro', function(r) {
        if (r) {
            $("#loading").fadeIn();
               $.post(baseUrl + "/" + module + "/"+controller+"/excluirirmao",
               {id: id}, function(data) {
                $("#loading").fadeOut();
                
                if (data=="OK") {
                    $("#line"+id).fadeOut();
                } else {                   
                    jAlert(data, "Erro!");
                }
            }, 'JSON');
        }
     }); 
}

$("input[name='dataabandono'], input[name='datafalecimento'], input[name=data_emis_certidao_nasc], input[name=inDataEmissCertMatr], input[name=inDiaEntBrasil], input[name=inDataEmissaoRGRNE]").datepicker({
    dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
    dateFormat: 'dd/mm/yy',
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    currentText: 'Hoje',
    prevText: 'Antes',
    nextText: 'Depois'
});

function fecharModal(obj){
    $(obj).dialog('close');
}

function modalAcoes(element, titulo, largura, altura, headercolor){
    var dialog = $(element).dialog({
        title: titulo,
        width: largura,
        height: altura,   
        modal: true        
    });
    $(".ui-draggable .ui-dialog-titlebar").css("background", headercolor);

}
$("#registrarabandono").on("click", function(ev){
    ev.preventDefault();
    modalAcoes("#registrarabandonoBox", "Registrar abandono", 310, "auto", "#5f95ba");
});

$("#estornarabandono").on("click", function(evento){
    evento.preventDefault();
    modalAcoes("#estornarabandonoBox", "Estornar abandono", 310, "auto", "#7aac83");
});

$("#falecimento").on("click", function(e){
    e.preventDefault();
    modalAcoes("#falecimentoBox", "Baixa do aluno por falecimento", 310, "auto", "#d65f4d");
});

$("#naocomparecimento").on("click", function(eve){
    eve.preventDefault();
    modalAcoes("#naocomparecimentoBox", "Não comparecimento do aluno", 310, "auto", "#e49a4d");
});

/**
 * Função: registrar o não comparecimento do aluno
 * @param {int} id *id do aluno*
 */
function naoComparecimento(id){
    idAluno = id;
    
    $('#comp-receiver').html('');
    $('#comp-receiver').html("<img src='"+baseUrl+"/public/admin/img/elements/loaders/8s.gif' alt='Aguarde...'> Aguarde...").fadeIn();

    $.post(baseUrl + "/" + module + "/" + controller + "/registrarnaocomparecimento/", {idAluno : idAluno}, function(retorno){
        
        if(retorno.status == "true"){
            $('#comp-receiver').removeClass("alert-danger").addClass("alert-success").html(retorno.mensagem).fadeIn('slow', function(){
                setTimeout(function(){
                    $('#comp-receiver').fadeOut();
                    $("#naocomparecimentoBox").dialog('close');
                    $("#estornarabandono, #naocomparecimento, #registrarabandono").prop("disabled", true);
                }, 3000);
            });
        }else{
            $('#comp-receiver').html(retorno.mensagem).fadeIn('fast', function(){
                setTimeout(function(){
                    $('#comp-receiver').fadeOut();
                }, 5000);
            });
        }
    },'jSON');
}

/**
 * Função: Dar baixa do aluno por falecimento
 * @param {int} id *id do aluno*
 * @param {string} datafalecimento - *data do falecimento*
 */
function confirmaBaixa(id){
    idAluno = id;
    datafalecimento = $("input=[name='datafalecimento']").val();

    if(datafalecimento=="") {
        $('#receivermsg').text('Preencha a data do falecimento').fadeIn();
        return false;
    }

    $('#receivermsg').html('');
    $('#receivermsg').html("<img src='"+baseUrl+"/public/admin/img/elements/loaders/8s.gif' alt='Aguarde...'> Aguarde...").fadeIn();

    $.post(baseUrl + "/" + module + "/" + controller + "/baixamatfalecimento/", {idAluno : idAluno, datafalecimento : datafalecimento}, function(retorno){
        if(retorno.status == "true"){
            $('#receivermsg').removeClass("alert-danger").addClass("alert-success").html(retorno.mensagem).fadeIn('slow', function(){
                setTimeout(function(){
                    $('#receivermsg').fadeOut();
                    $("#falecimentoBox").dialog('close');
                    $("#falecimento, #estornarabandono, #naocomparecimento, #registrarabandono").prop("disabled", true);
                }, 3000);
            });
        }else{
            $('#receivermsg').html(retorno.mensagem).fadeIn('fast', function(){
                setTimeout(function(){
                    $('#receivermsg').fadeOut();
                }, 5000);
            });
        }
    },"jSON");
}

/**
 * Função: Registrar o abandono do aluno
 * @param {int} id *id do aluno*
 * @param {string} dataabandono - *data do abandono*
 */
function confirmaAbandono(id){
    jConfirm('O aluno só poderá ser estornado em até 15 dias após a data de abandono', 'Tem certeza que deseja registrar abandono?', function(r) {
        if (r) {
            idAluno = id;
            inNumClasse = $("input[name=classe]").val();
            dataabandono = $("input[name='dataabandono']").val();

            if(dataabandono=="") {
                $('#receiver').html('Preencha a data de abandono').fadeIn('fast');
                return false;
            }
            $('#receiver').html('');
            $('#receiver').html("<img src='"+baseUrl+"/public/admin/img/elements/loaders/8s.gif' alt='Aguarde...'> Aguarde...").fadeIn();

            $.post(baseUrl + "/" + module + "/" + controller + "/registrarabandono/", {idAluno : idAluno, dataabandono : dataabandono, inNumClasse: inNumClasse}, function(retorno){
                
                if(retorno.status == "true"){
                    $('#receiver').removeClass("alert-danger").addClass("alert-success").html(retorno.mensagem).fadeIn('slow', function(){
                        setTimeout(function(){
                            $("#qntdias").text(retorno.dias);
                            $("#receiver").fadeOut();
                            $("#registrarabandonoBox").dialog('close');
                            $("#registrarabandono, #naocomparecimento").prop("disabled", true);
                            $("#estornarabandono").prop("disabled", false);

                            $('#receiver').removeClass("alert-success").addClass("alert-danger");
                        }, 3000);
                    });
                }else{
                    $('#receiver').html(retorno.mensagem).fadeIn('fast', function(){
                        setTimeout(function(){
                            $('#receiver').fadeOut();                    
                        }, 5000);
                    });
                }
            },'jSON');
        }
    });
}

/**
 * Função: Estornar o abandono do aluno
 * @param {int} id *id do aluno* 
 */
function confirmaEstorno(id){
    idAluno = id;
    $('#estorno-receiver').html('');
    $('#estorno-receiver').html("<img src='"+baseUrl+"/public/admin/img/elements/loaders/8s.gif' alt='Aguarde...'> Aguarde...").fadeIn();

    $.post(baseUrl + "/" + module + "/" + controller + "/estornoabandono/", {idAluno : idAluno}, function(retorno){
        if(retorno.status == "true"){
            $('#estorno-receiver').removeClass("alert-danger").addClass("alert-success").html(retorno.mensagem).fadeIn('slow', function(){
                setTimeout(function(){
                    $('#estorno-receiver').fadeOut();
                    $("#estornarabandonoBox").dialog('close');
                    $("#estornarabandono").prop("disabled", true);
                    $("#registrarabandono, #naocomparecimento").prop("disabled", false);

                    $('#estorno-receiver').removeClass("alert-success").addClass("alert-danger");

                }, 3000);
            });
        }else{
            $('#estorno-receiver').html(retorno.mensagem).fadeIn('fast', function(){
                setTimeout(function(){
                   $('#estorno-receiver').fadeOut();                    
                }, 5000);
            });
        }
    },'jSON');
}

function setCertidao(obj){
    alert($(this).val());
}

/*
 function iteAddTr(item, classe) {

 table = $(".itens table tbody#raiz_" + classe);

 tr = $('<tr />');

 if (item===false) {
 item = {};
 item.id=0;
 item.mes = '';
 if(classe=='60'){
 item.fundeb = 'fundep60';
 }else{
 item.fundeb = 'fundep40';
 }
 item.rendaaplicada = '';
 item.fundeb60 = '';
 item.pessoalcivil = '';
 item.reembolsoestatal = '';
 item.prev3191 = '';
 if(classe=='60'){
 item.tipo = 'fundeb60';
 }else{
 item.tipo = 'fundeb40';
 }
 item.saldo = '';
 }

 select_meses = $('<select class="form-control c_meses" name="meses[]" />');
 select_meses.append('<option value="">Selecione...</option>');
 for (var i=0; i<meses.length; i++) {
 d = meses[i];
 select_meses.append('<option value="'+d.k+'">'+d.v+'</option>');
 }
 select_meses.val(item.mes);
 td_meses = $('<td />');
 $(select_meses).trigger("chosen:updated");
 td_meses.append(select_meses);
 tr.append(td_meses);
 //tr.append('<td><input style="width: 98%!Important" value="'+item.mes+'" type="text" maxlength="255" class="form-control c_meses" name="meses[]" /></td>');

 tr.append('<td><input style="width: 98%!Important" value="'+item.tipo+'" type="hidden" maxlength="255" class="form-control c_tipos" name="tipos[]" /><input name="idsitens[]" type="hidden" value="'+item.id+'" /><input style="width: 98%!Important" value="'+item.fundeb+'" type="text" maxlength="255" class="form-control c_fundebs" name="fundebs[]" /></td>');

 tr.append('<td><input style="width: 98%!Important" value="'+item.rendaaplicada+'" type="text" maxlength="255" class="form-control c_rendasaplicadas" name="rendasaplicadas[]"/></td>');
 tr.append('<td><input style="width: 98%!Important" value="'+item.fundeb60+'" type="text" maxlength="255" class="form-control c_fundebs60" name="fundebs60[]" readonly="readonly"  /></td>');
 tr.append('<td><input style="width: 98%!Important" value="'+item.pessoalcivil+'" type="text" maxlength="255" class="form-control c_pessoalcivis" name="pessoalcivis[]" /></td>');
 tr.append('<td><input style="width: 98%!Important" value="'+item.reembolsoestatal+'" type="text" maxlength="255" class="form-control c_reembolsosestatais" name="reembolsosestatais[]" /></td>');
 tr.append('<td><input style="width: 98%!Important" value="'+item.prev3191+'" type="text" maxlength="255" class="form-control c_prevs3191" name="prevs3191[]" /></td>');
 tr.append('<td><input style="width: 98%!Important" value="'+item.saldo+'" type="text" maxlength="255" class="form-control c_saldo" name="saldos[]" readonly="readonly" /></td>');

 tr.append('<td style="text-align:center!Important" align="center"><a href="javascript:;" onclick="iteExcluirFromTable(this)"><i class="fa fa-trash-o"></i></a></td>');
 qtdMask(tr);
 table.append(tr);



 $(tr).find( ".c_meses" ).blur(function() {
 atualizaValores(this, 'sim', 'add');
 });

 $(tr).find( ".c_fundebs" ).blur(function() {
 atualizaValores(this, 'sim', 'add');
 });

 $(tr).find( ".c_rendasaplicadas" ).blur(function() {
 atualizaValores(this, 'sim', 'add');
 });

 $(tr).find( ".c_fundebs60" ).blur(function() {
 atualizaValores(this, 'sim', 'add');
 });

 $(tr).find( ".c_pessoalcivis" ).blur(function() {
 atualizaValores(this, 'sim', 'add');
 });

 $(tr).find( ".c_reembolsosestatais" ).blur(function() {
 atualizaValores(this, 'sim', 'add');
 });

 $(tr).find( ".c_prevs3191" ).blur(function() {
 atualizaValores(this, 'sim', 'add');
 });

 $(tr).find( ".c_saldo" ).blur(function() {
 atualizaValores(this, 'sim', 'add');
 });


 }

 function qtdMask(tr) {
 $(tr).find("input[name='fundebs[]']").priceFormat({
 prefix: '',
 centsSeparator: ',',
 thousandsSeparator: '.',
 centsLimit: 2
 });

 $(tr).find("input[name='rendasaplicadas[]']").priceFormat({
 prefix: '',
 centsSeparator: ',',
 thousandsSeparator: '.',
 centsLimit: 2
 });

 $(tr).find("input[name='fundebs60[]']").priceFormat({
 prefix: '',
 centsSeparator: ',',
 thousandsSeparator: '.',
 centsLimit: 2
 });

 $(tr).find("input[name='pessoalcivis[]']").priceFormat({
 prefix: '',
 centsSeparator: ',',
 thousandsSeparator: '.',
 centsLimit: 2
 });

 $(tr).find("input[name='reembolsosestatais[]']").priceFormat({
 prefix: '',
 centsSeparator: ',',
 thousandsSeparator: '.',
 centsLimit: 2
 });

 $(tr).find("input[name='prevs3191[]']").priceFormat({
 prefix: '',
 centsSeparator: ',',
 thousandsSeparator: '.',
 centsLimit: 2
 });

 $(tr).find("input[name='saldos[]']").priceFormat({
 prefix: '',
 centsSeparator: ',',
 thousandsSeparator: '.',
 centsLimit: 2
 });

 }

 function iteExcluirFromTable(obj) {
 var idsitens = $(obj).parent().parent().find("input[name='idsitens[]']").val();
 var iddemonstrativo = $("input[name=id]").val();
 $(obj).parent().parent().remove();
 $.post(baseUrl + "/administrativo/"+controller+"/deletalinha", {idsitens: idsitens}, function(data) {
 if(data=="OK"){
 $.post(baseUrl + "/administrativo/"+controller+"/calculageral", {iddemonstrativo: iddemonstrativo}, function(result) {
 $(obj).parent().parent().parent().parent().find('.c_fundebs_f').val(result.fundebs);
 $(obj).parent().parent().parent().parent().find('.c_rendasaplicadas_f').val(result.rendasaplicadas);
 $(obj).parent().parent().parent().parent().find('.c_fundebs60_f').val(result.fundebs60);
 $(obj).parent().parent().parent().parent().find('.c_pessoalcivis_f').val(result.pessoascivil);
 $(obj).parent().parent().parent().parent().find('.c_reembolsosestatais_f').val(result.reembolsosestatais);
 $(obj).parent().parent().parent().parent().find('.c_prevs3191_f').val(result.prevs3191);
 $(obj).parent().parent().parent().parent().find('.c_saldo_f').val(result.saldos);

 $(obj).parent().parent().remove();
 }, "json");
 }else{ console.log('erro'); }
 });



 }
 */
