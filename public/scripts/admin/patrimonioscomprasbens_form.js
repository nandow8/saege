var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

function setProdutos( qtd , idbem ) {
	$.post(baseUrl + "/" + module + "/"+controller+"/setprodutos",{qtd: qtd, idbem: idbem}, function(data) {
		$("#loop").html(data);

		$("#loop").show();
	});
}

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};

	$("input[type=radio][name='doacao[]']").change(function() {
		if($(this).attr("checked") == "checked"){

			if( $(this).val() == 'sim' )
				$("#doacaoDiv").show();
			else
				$("#doacaoDiv").hide();
		}
	});	

	$("#mostraProdutos").click(function(){
		if( $("input[name=quantidade]").val() != '' )
			setProdutos($("input[name=quantidade]").val(), null);
		else
			alert("Informe alguma quantidade para cadastrar os produtos");
	});	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=status1]").val(form_values.status1);
		$("select[name=idfornecedor]").val(form_values.idfornecedor);

		$("input[type=radio][name='doacao[]']").each(function() {
			if( $(this).val() == form_values.doacao  ){
				$(this).attr("checked","checked");

				if( $(this).val() == 'sim' )
					$("#doacaoDiv").show();
			}
		});

		setProdutos(form_values.qtd,form_values.idbem);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}

	$("input[name=dataemissao]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataemissao]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});







	
	
	rules = {};
	rules.titulo = "required";
rules.datacompra = "required";
rules.idescola = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});