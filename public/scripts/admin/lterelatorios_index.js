jQuery(document).ready(function() {

    data = '';
    $.post(baseUrl + '/admin/lterelatorios/setentidades/', {tipo: ''}, function (data) {
        $("select[name=entidades]").html(data);
    });

    if ((typeof form_values)!='undefined') {
        if ((typeof form_values.tipoestudante)!='undefined') {
            data = '';
            $.post(baseUrl + '/admin/lterelatorios/setentidades/', {tipo: form_values.tipoestudante}, function (data) {
                $("select[name=entidades]").html(data);
            });
            $("select[name=tipoestudante]").val(form_values.tipoestudante);
        }
        if ((typeof form_values.tipotransporte)!='undefined') $("select[name=tipotransporte]").val(form_values.tipotransporte);
        if ((typeof form_values.entidades)!='undefined') $("select[name=entidades]").val(form_values.entidades);
        if ((typeof form_values.entidades_universidades)!='undefined') $("select[name=entidades_universidades]").val(form_values.entidades_universidades);
        if ((typeof form_values.periodo)!='undefined') $("select[name=periodo]").val(form_values.periodo);
        if ((typeof form_values.subsidio)!='undefined') $("select[name=subsidio]").val(form_values.subsidio);

        $("select").trigger("chosen:updated");
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
        
        $("select").each(function (){
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

    }

    $('#tipoestudante').change(function(event) {
        if ($(this).val() == 'estadual') {

            $('#select-curso').prop('disabled', false);

        } else if ($(this).val() != 'estadual') {

            $('#select-curso').val(null).trigger('change');
            $('#select-curso').prop('disabled', true);

        }
    });
});

function setEntidades(obj) {
    var tipo = $(obj).val();

    data = '';
    $.post(baseUrl + '/admin/lterelatorios/setentidades/', {tipo: tipo}, function (data) {
        $("select[name=entidades]").html(data);
    });
}

function relatorioFretado(obj) {
    var idperiodo = $(obj).data('idperiodo');
    var tipoestudante = $(obj).data('tipoestudante');
    var identidade = $(obj).data('identidade');

    $.post(baseUrl + '/admin/lterelatorios/pdffretado', {idperiodo: idperiodo, tipoestudante: tipoestudante, identidade: identidade}, function (data) {
        jAlert(data);
    });
}