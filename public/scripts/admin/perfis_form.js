/* global form_values, visualizar */

var unsaved = false;
$("input,select,textarea").change(function() { //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function() { //trigers change in all input fields including text type
    unsaved = true;
});
$(document).ready(function() {
    window.onbeforeunload = function() {
        console.log(unsaved);
        if (unsaved) {
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };
    if ((typeof form_values) !== 'undefined') {
        $("select[name=status1]").val(form_values.status1);
        $("select[name=individualgeral]").val(form_values.individualgeral);
        $("select[name=recebeprotocolo]").val(form_values.recebeprotocolo);
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
    }

    $('.checks').find('input').each(function() {
        td = $(this).closest('td');
        div = td.find('div:first');
        setDiv(div.eq(0));
    });
    rules = {};
    rules.perfil = "required";
    rules.status1 = "required";
    $("#form").validate({
        ignore: [],
        rules: rules,
        submitHandler: function(form) {
            unsaved = false;
            $.blockUI({message: '<h1> Aguarde...</h1>'});
            form.submit();
        }
    });
    //visualizar = false;
    if (visualizar) {
        $("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
            val = $(this).val();
            val = $.trim(val);
            val = (val === '') ? '--' : val;
            parent = $(this).parent();
            parent.append(val);
            parent.addClass('visualizar');
            $(this).remove();
        });
        $("div.control-group select").not('[type=hidden]').each(function() {
            val = $(this).find('option:selected').html();
            val = $.trim(val);
            val = (val === '') ? '--' : val;
            parent = $(this).parent();
            parent.html(val);
            parent.addClass('visualizar');
        });
        $("tr.checks div").css('cursor', 'default');
        $("tr.checks div").attr('onclick', '');
        $("label span.required").remove();
    }

});
function setDivClick(div) {
    td = $(div).closest('td');
    input = td.find('input');
    if (input.is(':checked')) {
        input.attr('checked', false);
    } else {
        input.attr('checked', true);
    }
    setDiv(div);
}

function setDiv(div) {
    td = $(div).closest('td');
    input = td.find('input');
    $(div).removeClass('icon-ok');
    $(div).removeClass('icon-remove');
    if (input.is(':checked')) {
        $(div).addClass('icon-ok');
        $(div).css('color', '#080');
    } else {
        $(div).addClass('icon-remove');
        $(div).css('color', '#800');
    }
}

function setAllChecks(div, coluna = 0, modulo = 0) {
    if (modulo > 0) {
        checks = 'mk' + coluna + '_' + modulo;
        allMarked = true;
        $("." + checks).each(function() {
            if ($(this).is(':checked')) {
            } else
                allMarked = false;
        });
        if (allMarked)
            uncheckAll(checks);
        else
            checkAll(checks);
}
}

function uncheckAll(checks) {
    $("." + checks).each(function() {
        $(this).prop('checked', false);
        $(this).parent().prev().removeClass('icon-ok');
        $(this).parent().prev().addClass('icon-remove');
        $(this).parent().prev().css('color', '#800');
    });
}

function checkAll(checks) {
    $("." + checks).each(function() {
        $(this).prop('checked', true);
        $(this).parent().prev().removeClass('icon-remove');
        $(this).parent().prev().addClass('icon-ok');
        $(this).parent().prev().css('color', '#080');
    });
}

if ($(window).width() > 960) {
    $(window).scroll(function() {

        atual = $(document).scrollTop();
        if (atual > 500) {
            $('#cabecalho tr').addClass('topHeader');
            $('#cabecalho tr th:first').addClass('topFirstTr');
            $('#cabecalho tr th').not('.topFirstTr').addClass('topTrTh');
        } else {
            $('#cabecalho tr').removeClass('topHeader');
            $('#cabecalho tr th:first').removeClass('topFirstTr');
            $('#cabecalho tr th').removeClass('topTrTh');
        }
    });
}
$(window).scroll(function() {
    documento = $(document).height();
    altura = $(document).scrollTop();

    if (altura > 500 && (altura < 22500)) {
        //botão para subir a página
        $('.btn-up').fadeIn();
    } else {
        //botão para subir a página
        $('.btn-up').fadeOut();
    }
});
$('.btn-up').on('click', function() {
    $('html, body').animate({'scrollTop': '0'}, 'slow');
});