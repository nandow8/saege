﻿var unsaved = false;

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    if(action!="adicionar") {
    	//unsaved = true;
    }
});

jQuery(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};
	
	if ((typeof form_values)!='undefined') {
		if ((typeof form_values.status)!='undefined') jQuery("select[name=status]").val(form_values.status);
		if ((typeof form_values.idestado)!='undefined') jQuery("select[name=idestado]").val(form_values.idestado);
		if ((typeof form_values.idcidade)!='undefined') jQuery("select[name=idcidade]").val(form_values.idcidade);
		if ((typeof form_values.idcidade)!='undefined'){
			setCidades($("select[name=idestado]").val(form_values.idestado), form_values.idcidade);
		}
		if ((typeof form_values.idssecretarias)!='undefined') $("[name='idssecretarias[]']").val(form_values.idssecretarias);
		$("select").trigger("chosen:updated");
	}
		
	jQuery('select, input, textarea').bind('blur', function() { });
	$("[name=cpfcnpj]").mask("99.999.999/9999-99");
	$('input[name=telefone]').mask('(99) 9999-9999?9');

	rules = {};
	rules.status = "required";
	rules.datacontrato = "required";
	rules.databloqueio = "required";
	rules.prefeito = "required";
	rules.secretario = "required";
	rules.partido = "required";
	rules.idestado = "required";
	rules.idcidade = "required";
	rules.cpfcnpj = {required:true, mn_cnpj: true};
	
	messages = {};
	messages.cpfcnpj = {required: "", mn_cnpj:"CNPJ inválido"};
	
	$("input[name=datacontrato]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datacontrato]").datepicker({ 
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois'
	});
	
	$("input[name=databloqueio]").mask("99/99/9999", {placeholder:" "});
	$("input[name=databloqueio]").datepicker({ 
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois'
	});
	
	
	$("#form").validate({
		ignore: [],
		rules: rules,	
		messages: messages,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
		    
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});	
	
	
});

function setCidades(obj, idcidade){
	var val = $(obj).val();
	data = '';
	$("select[name=idcidade]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/prefeituras/setcidades/', {idestado:val}, function(data) {
		$("select[name=idcidade]").html(data);
		$("select[name=idcidade]").val(idcidade);
		val = $("select[name=idcidade]").find('option:selected').html();
		
		$("select[name=idcidade]").trigger("chosen:updated");
		
		if(idcidade==""){
			$("select[name=idcidade]").closest('div').find('span').html('Selecione...');
		}
	});
}