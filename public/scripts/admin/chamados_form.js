var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=destino]").val(form_values.destino);
$("select[name=iddepartamento]").val(form_values.iddepartamento);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=idprioridade]").val(form_values.idprioridade);
$("select[name=idstatus]").val(form_values.idstatus);
$("select[name=finalizacao]").val(form_values.finalizacao);
$("select[name=atendido]").val(form_values.atendido);
$("select[name=idtipo]").val(form_values.idtipo);
$("select[name=idsfuncionarios]").val(form_values.idsfuncionarios);
$("select[name=substituicaopecas]").val(form_values.substituicaopecas);
$("select[name=status1]").val(form_values.status1);
$("select[name=idusuariocriacao]").val(form_values.idusuariocriacao);
$("select[name=iddepartamentocriacao]").val(form_values.iddepartamentocriacao);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	






	$("input[name=dataagendada]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataagendada]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});



















	
	
	rules = {};
	rules.sequencial = "required";
rules.idescola = "required";
rules.descricoes = "required";
rules.idprioridade = "required";
rules.idstatus = "required";
rules.dataagendada = "required";
rules.atendido = "required";
rules.idsfuncionarios = "required";
rules.idusuariocriacao = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});