count_page = 0;
$(document).ready(function() {
		var width = $(".fc-week .fc-first").width();
		var height = width - 15;
		
		$(".fc-week .calendario-dia .box_dia").each(function( index ) {
			$(this).css('height', height);
			var content_evento = $(this).find('.fc-day-content').find('.title-event').height();
			var display = $(this).find('.fc-day-content').find('.title-event').css('display');
			content_evento = parseInt(content_evento) ; 
			
		});
		
    $(window).resize(function() {
		var width = $(".fc-week .fc-first").width();
		var height = width - 15;
		
		$(".fc-week .calendario-dia .box_dia").each(function( index ) {
			$(this).css('height', height);
			var content_evento = $(this).find('.fc-day-content').find('.title-event').height();
			var display = $(this).find('.fc-day-content').find('.title-event').css('display');
			content_evento = parseInt(content_evento) ; 
			
		});
    });

});

function abreAba(id, obj) {
	
	obj = $(obj);
	
	thead = obj.closest('thead');
	is_show = thead.find('tr.aba_' + id).is(':visible');
	//thead.find('tr.aba').hide();
	
	if (!is_show) thead.find('tr.aba_' + id).show();
	else  thead.find('tr.aba_' + id).hide();
	
}


function setGraficosSolicitacoes(tipo) {	
	$.fancybox({
		'padding'		: 0,
		'autoScale'		: false,
		'transitionIn'	: 'none',
		'transitionOut'	: 'none',
		'title'			: 'Gráfico',
		'width'		: 800,
		'height'		: 600,
		'href'			: baseUrl + '/admin/solicitacoes/setgraficos/tipo/' + tipo,
		'type'			: 'iframe'
	});

	return false;	
}

function setInteresse($id, $obj) {
	$obj = $($obj);
 	$.post(baseUrl + "/admin/coordenacaocursos", {id: $id}, function(data) {
			if (data=="OK") {
				console.log('entrou');
				//$(obj).parent().parent().remove();
				$($obj).parent().parent().remove();
				$(".alert-curso").css("display", "block");
				$(".alert-curso").delay(6000).fadeOut(1000);
				console.log('saiu');
			} else {
				jAlert(data, "Alerta!");
			}
 	});
} 



function setPage(tipo){
	if(tipo=="inicial"){
		count_page = 0;
	}else if(tipo=="mais"){
		count_page++;
	}else if(tipo=="menos"){
		count_page--;
	}

	$.post(baseUrl + "/admin/index/setcalendario", {page: count_page}, function(data) {
		if (data!="erro") {
			$('#calendar1').html('');
			$('#calendar1').html(data);
			
			if(count_page!=0){
				$('.fc-button-today').removeClass('fc-state-disabled');
			}else{
				$('.fc-button-today').addClass('fc-state-disabled');
			}
	
		} else {
			jAlert(data, "Erro!");
		}
	});
	
}

function setEventos(obj, dia){
	$.fancybox({
		type: 'iframe',
		href: baseUrl + "/admin/index/seteventosdia/dia/" + dia,
		width: 800,
		height: 600
	});
}

function setEventoById(id){
	$.fancybox({
		type: 'iframe',
		href: baseUrl + "/admin/index/setevento/id/" + id,
		width: 800,
		height: 600
	});
}