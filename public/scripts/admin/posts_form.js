var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});


jQuery(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		if ((typeof form_values.status1)!='undefined') jQuery("select[name=status1]").val(form_values.status1);
		if ((typeof form_values.tipocustom)!='undefined') jQuery("select[name=tipocustom]").val(form_values.tipocustom);
		if ((typeof form_values.destaque)!='undefined') jQuery("select[name=destaque]").val(form_values.destaque);
		if ((typeof form_values.idcategoria)!='undefined') jQuery("select[name=idcategoria]").val(form_values.idcategoria);
		if ((typeof form_values.idusuario)!='undefined') jQuery("select[name=idusuario]").val(form_values.idusuario);
		$("select").trigger("chosen:updated");
	}
		
	jQuery('select, input, textarea').bind('blur', function() { });
	
	rules = {};
	rules.topico = "required";
	rules.status1 = "required";
	rules.tipocustom = "required";
	rules.idcategoria = "required";
	rules.link = {url:true};
	
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				//$(element).parent().find('div').append(error);
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
		    
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});	
	
});

