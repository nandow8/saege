var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idmotorista]").val(form_values.idmotorista);
$("select[name=idfrota]").val(form_values.idfrota);
$("select[name=idmotivoinfracao]").val(form_values.idmotivoinfracao);
$("select[name=idtipomulta]").val(form_values.idtipomulta);
$("select[name=idcidade]").val(form_values.idcidade);
$("select[name=pago]").val(form_values.pago);
$("select[name=status1]").val(form_values.status1);

		setDadosVeiculo($("select[name=idfrota]").val(form_values.idfrota));
		setDadosMultas($("select[name=idmotivoinfracao]").val(form_values.idmotivoinfracao));

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	






	$('input[name=valormulta]').priceFormat({
		prefix: '',
		centsSeparator: ',',
		thousandsSeparator: '.',
		centsLimit: 2
	});



	$("input[name=datamulta]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datamulta]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$("input[name=horamulta]").mask("99:99:99", { placeholder: " " }); 

	$("input[name=datavencimentomulta]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datavencimentomulta]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
 


	
	
	rules = {};
	rules.idmotorista = "required";
rules.idfrota = "required";
rules.idtipomulta = "required";
rules.idcidade = "required";
// rules.ocorrenciamulta = "required";
rules.motivomulta = "required";
rules.datamulta = "required";
rules.horamulta = "required";
rules.datavencimentomulta = "required";
rules.pago = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setDadosVeiculo(obj, values) {
	obj = $(obj);
	val = obj.val();
	var elemento;
	$.post(baseUrl + '/admin/logisticamultas/getdadosveiculo', { idfrota: val }, function (veiculo) {
		veiculo = JSON.parse(veiculo); 
		if (veiculo) {
			elemento = '<div class="control-group">';
				elemento += '<div class="controls">';
					elemento += '<strong> Marca: </strong>' + veiculo.marca + '<br/>';
					elemento += '<strong> Modelo: </strong>' + veiculo.modelo + '<br/>' ;
					elemento += '<strong> Placa: </strong>' + veiculo.placa + '<br/>' ;
					elemento += '<strong> Cor: </strong>' + veiculo.cor ;
				elemento += '</div>';
			elemento += '</div>';
		} else {
			elemento = 'Este Veiculo não tem marcas cadastradas.';
		}
		$('#dados-veiculo').html(elemento);

	});
} 

function setDadosMultas(obj, values) {
	obj = $(obj);
	val = obj.val();
	var elemento;
	$.post(baseUrl + '/admin/logisticamultas/getdadosmulta', { idmotivoinfracao: val }, function (multa) {
		multa = JSON.parse(multa); 
		if (multa) {
			elemento = '<div class="control-group">';
				elemento += '<div class="controls">';
					elemento += '<strong> Artigo: </strong>' + multa.tipo + '<br/>';
					elemento += '<strong> Pontuação: </strong>' + multa.pontuacao + '<br/>';
					elemento += '<strong> Descrição: </strong>' + multa.descricao + '<br/>'; 
				elemento += '</div>';
			elemento += '</div>';
		} else {
			elemento = 'Este funcionário não está cadastrado em um departamento.';
		}
		$('#dados-multa').html(elemento);

	});
} 