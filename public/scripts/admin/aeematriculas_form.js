var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {

	$('.medicacaosim').hide(); 
	$("#box-alunos").css("display", "none");
	
	var medicacaoCampos = $("#medicacaosim").val();

	if(medicacaoCampos != ""){ 
		$('.medicacaosim').show(); 
	}

	var atendSim = $("#atendSim").val(); 
	
	if(atendSim == "Sim"){   
		$(".box-opt-atendimento-especializado").css('display', 'block');
	} 

	var laudoSim = $("#temlaudo").val(); 
 
	if(laudoSim == 'temlaudo'){   
		$(".box-laudosim").css('display', 'block');
	} 
 

	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idaluno]").val(form_values.idaluno);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idperiodo]").val(form_values.idperiodo);
		$("select[name=idauxiliar]").val(form_values.idauxiliar);
		$("select[name=idprofessor]").val(form_values.idprofessor);
		$("select[name=idprofessor2]").val(form_values.idprofessor2);
		$("select[name=iddeficiencia]").val(form_values.iddeficiencia);
		$("select[name=laudo]").val(form_values.laudo);
		$("select[name=apoio]").val(form_values.apoio);
		$("select[name=necessidades]").val(form_values.necessidades);
		$("select[name=hipotesedeescrita]").val(form_values.hipotesedeescrita);
		$("select[name=transporte]").val(form_values.transporte);
		setOpcoes($("select[name=transporte]").val(form_values.transporte), form_values.transporte)
		$("select[name=status1]").val(form_values.status1);

		if ((typeof form_values.transporteopcoes)!='undefined') {
			var values = form_values.transporteopcoes.split(',');
			$("[name='transporteopcoes[]']").val(values);
			//$("[name='transporteopcoes[]']").val(form_values.transporteopcoes);
		}

		if ((typeof form_values.atendimentoespecializado)!='undefined') {
			var values = form_values.atendimentoespecializado.split(',');
			$("[name='atendimentoespecializado[]']").val(values); 
		}

		if ((typeof form_values.tipodeficiencia)!='undefined') {
			var values = form_values.tipodeficiencia.split(',');
			$("[name='tipodeficiencia[]']").val(values); 
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	 
	
	$(".horaaula").mask("99:99", {placeholder:" "});  
	  
	 

	
	
	rules = {};
	rules.idaluno = "required";
rules.idescola = "required";
rules.idperiodo = "required";
rules.idauxiliar = "required";
rules.idprofessor = "required";
rules.idprofessor2 = "required";
// rules.iddeficiencia = "required";
rules.laudo = "required";
rules.cid = "required";
rules.apoio = "required";
rules.necessidades = "required";
rules.transporte = "required";
rules.responsavel = "required";
rules.data = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setOpcoes(obj, idtransporte){
	var transporte = $(obj).val();
	if(transporte=="sim"){
		$(".box-opt-transporte").css('display', 'block');
	}else{
		$(".box-opt-transporte").css('display', 'none');
	}
}

function setAlunosdados(obj, id){
	$("#box-alunos").css("display", "block");

	var responsavel = '';
	var telefonecontato = '';
	var telefoneresidencial = '';

	if(id){
		val = id;
	}else{
		obj = $(obj); 
    	val = obj.val();
	}

    var elemento; 
        $.post(baseUrl + '/admin/aeematriculas/obterdadosaluno', {idaluno: val}, function(aluno){
			aluno = JSON.parse(aluno);  
		
				if(aluno.nomeresponsavel) responsavel = aluno.nomeresponsavel; 
				if(aluno.telefonecontato) telefonecontato = aluno.telefonecontato; 
				if(aluno.telefoneresidencial) telefoneresidencial = aluno.telefoneresidencial; 

				elemento = '<ul class="list-group" style="width: 50%;">'    
								+'<li class="list-group-item"> Nome: ' +  aluno.nomerazao +'</li>'
								+'<li class="list-group-item"> RA: ' +  aluno.ra +'</li>'
								+'<li class="list-group-item"> Endereço: ' +  aluno['endereco']['endereco'] + ' Nº ' + aluno['endereco']['numero']  + '</li>'
								+'<li class="list-group-item"> Nome Pai: ' +  aluno.nomepai +'<li>'
								+'<li class="list-group-item"> Nome Mãe: ' +  aluno.nomemae +'<li>'
								+'<li class="list-group-item"> Responsavel: ' +  responsavel +'</li>'
								+'<li class="list-group-item"> Telefone contato: ' +  telefonecontato +'</li>' 
								+'<li class="list-group-item"> Telefone residencial: ' +  telefoneresidencial +'</li>' 
								+'<input type="hidden" name="escolaregular" value="'+  aluno.escola +'">'
							+'</ul>';

                 $('#box-alunos').html(elemento); 
        }); 
 } 

 
 function medicacaosimnao(valor){
	
	if(valor == 'nao'){
		$('.medicacaosim').hide();
 
	}else if(valor == 'sim'){
		$('.medicacaosim').show();
	} 
} 
 
function atendimentoespecializadosimnaoclick(valor){
	
	if(valor == 'nao'){
		$(".box-opt-atendimento-especializado").css('display', 'none');  
	}else if(valor == 'sim'){
		$(".box-opt-atendimento-especializado").css('display', 'block');
	} 
} 
 
function possuilaudoclick(valor){
	
	if(valor == 'nao'){
		$(".box-laudosim").css('display', 'none');  
	}else if(valor == 'sim'){
		$(".box-laudosim").css('display', 'block');
	} 
} 

function membroAddTr(_values) {
    
	html = "";
	
	if (_values===false) {
		_values = {};
	} else {
		_values = jQuery.parseJSON(_values);
		itens = _values;
	}
	
	modelo = $("#modelo-apm").html();
	modelo = '<tr>' + modelo + '</tr>';
	modelo = $(modelo);
	table = $("table.table-membros");
	membroiner = table.find('tbody');
	
	modelo.find('p').html(html);
	modelo.find('input,select,textarea').each(function() {
		name = $(this).attr('name');
		name = name.replace("_", "");
		if(_values && _values.var_db == true)name = name.replace("[]", "");
		
    
		for (var property in _values) {
			if (property==name) {
				
				$(this).attr('name', name + '[]');
				if(_values.var_db){
				}
				
				$(this).attr('value', _values[property]);
				
				if ($(this).is("select")) {
					$(this).parent().find('a').remove();
				}
			}
		}
	});
	membroiner.append(modelo);
	
	$('.numbervalue').priceFormat({
		prefix: '',
		limit: 3,
        centsSeparator: '',
        thousandsSeparator: ''	    
	});

	$('.numbervalue').blur(function(){
		if($(this).val() <= 0){
			$(this).val('');	
		}
			
	});
}

function membrosExcluirFromTable(obj) {
	jConfirm("Deseja excluir o registro?", 'Confirmar', function(r) {
		
            if(r)
            {
		obj = $(obj);
		content = obj.closest('tr');
		content.find('input').remove();
		content.hide();
            }
		
	});
}