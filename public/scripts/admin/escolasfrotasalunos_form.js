﻿var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

jQuery(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};
	
	if ((typeof form_values)!='undefined') {
		$("select[name=status1]").val(form_values.status1);
		$("select[name=idfrotas]").val(form_values.idfrotas);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idgaragem]").val(form_values.idgaragem);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
		
/*	
   	var map;
    
      map = new GMaps({
        el: '#map',
        lat: 51.5073346,
	    lng: -0.1276831,
	    zoom: 12,
	    zoomControl : true,
	    zoomControlOpt: {
	        style : 'SMALL',
	        position: 'TOP_LEFT'
	    },
      });
   

	map.setContextMenu({
	  control: 'map',
	  options: [{
	    title: 'Add marker',
	    name: 'add_marker',
	    action: function(e){
	      this.addMarker({
	        lat: e.latLng.lat(),
	        lng: e.latLng.lng(),
	        title: 'New marker'
	      });
	    }
	  }, {
	    title: 'Center here',
	    name: 'center_here',
	    action: function(e){
	      this.setCenter(e.latLng.lat(), e.latLng.lng());
	    }
	  }]
	});
*/	
	rules = {};
	rules.status1 = "required";
	rules.idescola = "required";
	rules.idfrotas = "required";
	
	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


function setDados(obj, idescola){
	var val = $(obj).val();
	
}

function setClassificacao(obj){
	var idserie = $('select[name=idserie]').val();
	//alert(idserie);
	setAlunos($("select[name=idserie]").val(idserie), 0);	
}

function setVinculos(obj, idvinculos){
	var idescolasala = $('input[name=id]').val();
	var val = $(obj).val();
	var idescola = $('select[name=idescola]').val();
	var idperiodo = $('select[name=idperiodo]').val();
	data = '';
	
	$.post(baseUrl + '/admin/escolasfrotasalunos/setvinculos/', {idserie:val, idescola:idescola, idescolasalasaluno: idescolasala, idperiodo:idperiodo}, function(data) {
		$(".box-salas").html(data);		
		var teste =  $("select[name=idserie]").val(val);
		
		setAlunos($("select[name=idserie]").val(val), 0);
		
	});
}

function setAlunos(obj, idsala){
	var val = $(obj).val();
	var idescolasala = $('input[name=id]').val();
	var idescola = $('select[name=idescola]').val();
	var classificacao = $('select[name=classificacao]').val();
	var idperiodo = $('select[name=idperiodo]').val();
	var alunocomplete = $('input[name=alunocomplete]').val();
	
	var notids = "";
	var verifica = false;
	$("input[name='idsalunos[]']").each(function() {
		//console.log('notids -> ' + notids);
		if(verifica==false){
			notids = $(this).val();
			verifica = true;
		}else if(verifica==true){
			notids = notids + ',' + $(this).val();
		}
	});
	
	console.log('notids -> ' + notids);
	data = '';
	
	$.post(baseUrl + '/admin/escolasfrotasalunos/setalunos/', {idserie:val, idescola:idescola, classificacao:classificacao, idperiodo:idperiodo, alunocomplete:alunocomplete, idescolasala:idescolasala, notids:notids}, function(data) {
		$(".box-alunos").html(data);
	});
}

function setSeries(obj, idserie, idperiodo){
	setPeriodos(obj, idperiodo);
	var val = $(obj).val();
	setClassificacoes($("select[name=idescola]").val(val), 0);
	
	data = '';
	$("select[name=idserie]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasfrotasalunos/setseries/', {idescola:val, action:action}, function(data) {
		$("select[name=idserie]").html(data);
		$("select[name=idserie]").val(idserie);
		val = $("select[name=idserie]").find('option:selected').html();
		$("select[name=idserie]").closest('div').find('span').html(val);
	
		$("select[name=idserie]").trigger("chosen:updated");
		
		setVinculos(jQuery("select[name=idserie]").val(idserie), 0);
		
		if(idserie==""){
			$("select[name=idserie]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setPeriodos(obj, idperiodo){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idperiodo]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasfrotasalunos/setperiodos/', {idescola:val}, function(data) {
		$("select[name=idperiodo]").html(data);
		$("select[name=idperiodo]").val(idperiodo);
		console.log('idperiodo: ' + idperiodo)
		val = $("select[name=idperiodo]").find('option:selected').html();
		$("select[name=idperiodo]").closest('div').find('span').html(val);
	
		$("select[name=idperiodo]").trigger("chosen:updated");
		
		if(idperiodo==""){
			$("select[name=idperiodo]").closest('div').find('span').html('Selecione...');
		}
	});
}

function resetVinculo(){/*
	$("input[name='idsalunos[]']").each(function() {		
		notids = $(this).val('');		
	});*/
	var idserie = $('select[name=idserie]').val();
	var idperiodo = $('select[name=idperiodo]').val();

	setVinculos($("select[name=idserie]").val(idserie), 0);
}

function ExcluirAluno(obj, idaluno, tipo){
	var idserie = $("select[name=idserie]").val();	
	var iditem = $(obj).parent().parent().find("input[name='idsitens[]']").val();
	var path_box = $(obj).parent().parent().parent();
	jConfirm('Confirmar a exclusão do aluno desta sala?', 'Excluir aluno', function(r) {
		if (r) {
			if(tipo=="repopula"){
				$.post(baseUrl + '/admin/escolasfrotasalunos/excluiritem/', {iditem:iditem, idserie:idserie}, function(data) {
					var item =  $('.aluno_' + idaluno);	
					item.removeClass("off");
					item.addClass("on");
					//setAlunos($("select[name=idserie]").val(idserie), 0);	
					$(obj).parent().parent().remove();	
					
				});
			}else{
		        var item =  $('.aluno_' + idaluno);		        
				item.removeClass("off");
				item.addClass("on");
				console.log('idaluno: ' + idaluno);
				$(obj).parent().parent().remove();	
			}
			$qtdatual = $(path_box).find(".qtdatual").val();			
			$qtdatual = parseInt($qtdatual) - 1;			
			$(path_box).find(".qtdatual").val($qtdatual);
		}
 	}); 
	
}

function setClassificacoes(obj, classificacao){
	var val = $(obj).val();
	
	data = '';
	$("select[name=classificacao]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasfrotasalunos/setclassificacoes/', {idescola:val}, function(data) {
		$("select[name=classificacao]").html(data);
		$("select[name=classificacao]").val(classificacao);
		val = $("select[name=classificacao]").find('option:selected').html();
		$("select[name=classificacao]").closest('div').find('span').html(val);
	
		$("select[name=classificacao]").trigger("chosen:updated");
		
		if(classificacao==""){
			$("select[name=classificacao]").closest('div').find('span').html('Selecione...');
		}
	});
}