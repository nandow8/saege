var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	
	if (!$("#terceirizado_nao").prop("checked") && !$("#terceirizado_sim").prop("checked")) {
		$(".veiculo-box").hide(); 
	} 

	if ($("#escolar_nao").attr("checked")) {
		$(".veiculo-box").show(); 
	} 
	
	$("#terceirizado_nao").click(function () {
		$(".veiculo-box").show();
	});
	
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idfuncionario]").val(form_values.idfuncionario);
		$("select[name=idfornecedor]").val(form_values.idfornecedor);
$("select[name=categoriacnh]").val(form_values.categoriacnh);
$("select[name=status1]").val(form_values.status1);

	
		setMotoristaEscolar($("input[name=transporteescolar]"));

		if ($("#terceirizado_sim").prop("checked")) {
			setTerceirizado($("input[name=terceirizado]"));
		}
		if ($("#terceirizado_nao").prop("checked")) {
			$(".box-tipoveiculo").show();
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	


	$('input[name=diasaviso_vencimento]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});



	$("input[name=telefone]").mask("(99)9999-9999", { placeholder: " " });
	$("input[name=celular]").mask("(99)99999-9999", { placeholder: " " });
	$("input[name=rg]").mask("99.999.999-9", { placeholder: " " });
	$("input[name=cpf]").mask("999.999.999-99", { placeholder: " " });

	$("input[name=datanascimento],input[name=dataprimeiracnh],input[name=datavalidadecnh],input[name=dataemissaocnh]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datanascimento],input[name=dataprimeiracnh],input[name=datavalidadecnh],input[name=dataemissaocnh]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});


	$("input[name=datacertificado]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datacertificado]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	
	
	rules = {};
	// rules.rg = "required";
	// rules.cpf = "required";
	rules.tipoveiculo = "required";
	rules.email = "required";
	rules.celular = "required";
	rules.categoriacnh = "required";
	rules.registrocnh = "required";
	rules.datavalidadecnh = "required";
	rules.dataemissaocnh = "required";
	// rules.transporteescolar = "required";
	rules.terceirizado = "required";
	rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});



function setTerceirizado($obj)
{
	var value = $($obj).val();
	console.log(value)

	if($($obj).is(':checked') && value == 'sim')
	{
		$('.box-terceirizado').show();
		$('.box-tipoveiculo').hide();
		$('.box-interno').hide();
		$('.box-escolar-terceirizado').show();
	}
	else
	{
		$('.box-tipoveiculo').show();
		$('.box-terceirizado').hide();	
		$('.box-interno').show();
		$('.box-escolar-terceirizado').hide();
	}
}


function setMotoristaEscolar($obj)
{
	var value = $($obj).val();
	console.log(value)

	if($($obj).is(':checked') && value == 'sim')
	{
		$('.box-escolar').show();
		$('.box-tipoveiculo').hide();
	}
	else
	{
		$('.box-escolar').hide();
		$('.box-tipoveiculo').show();
	}
}


function setFuncionarioDepartamento(obj, values) {
	obj = $(obj);
	val = obj.val();
	var elemento;
	$.post(baseUrl + '/admin/logisticamotoristas/getfuncioanrio', { idfuncionario: val }, function (funcionario) {
		funcionario = JSON.parse(funcionario);
		console.log(funcionario);
		if (funcionario) {
			elemento = '<input maxlength="500" placeholder="CPF" name="idlocais" value="' + funcionario.id + '" type="hidden"  class="span3"   /> "' + funcionario.departamento + '"';
		}else{
			elemento = 'Este funcionário não está cadastrado em um departamento.';
		}
		$('#departamento').html(elemento);
	 
	});
} 