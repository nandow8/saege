$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idtipoconvenio]").val(form_values.idtipoconvenio);
		$("select[name=idescolasapms]").val(form_values.idescolasapms);
		$("select[name=idprogramasgov]").val(form_values.idprogramasgov);
		$("select[name=status1]").val(form_values.status1);
		
		if(form_values.idtipoconvenio) setConvenios("select[name=idtipoconvenio]", 0);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});	
	}
	$('input[name=saldoaplicacaofinanceira]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
});
$(window).load(function(){
	if(form_values.idprogramasgov) $("select[name=idprogramasgov]").val(form_values.idprogramasgov);
	val = $("select[name=idprogramasgov]").find('option:selected').html();
	$("select[name=idprogramasgov]").closest('div').find('span').html(val);
});
function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + "/" + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Ativo") || (data=="Bloqueado")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
}

function setConvenios(obj, idconvenio){
	var idtipo = $(obj).val();

	$.post(baseUrl + "/" + module + "/"+controller+"/setconvenios",{idtipoconvenio: idtipo}, function(data) {
		console.log(data);
		$('select[name=idprogramasgov]').html(data);
		$("select[name=idprogramasgov]").val(idconvenio);
		val = $("select[name=idprogramasgov]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

			
		if((idconvenio!=0) && (idconvenio!="")){
			$("select[name=idprogramasgov]").val(idconvenio);
			setBancos($("select[name=idprogramasgov]"), form_values.idbanco);
		
			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
		}
	});
}