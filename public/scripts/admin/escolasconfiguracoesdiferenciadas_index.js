$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idserie]").val(form_values.idserie);
		$("select[name=idmateria]").val(form_values.idmateria);
		$("select[name=idtitulonota]").val(form_values.idtitulonota);
		$("select[name=status1]").val(form_values.status1);

		setSeries($("select[name=idescola]"), form_values.idserie);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});	

		$(".select2-container").css({
			'min-width': '140px',
			'max-width': '250px'
		});
	}		
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Ativo") || (data=="Bloqueado")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
}

function setSeries(obj, idserie){
	var val = $(obj).val();

    data = '';
    $("select[name=idserie]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasconfiguracoesdiferenciadas/setseries/', {idescola: val}, function(data) {

        $("select[name=idserie]").html(data);
        $("select[name=idserie]").val(idserie);
        val = $("select[name=idserie]").find('option:selected').html();
        $("select[name=idserie]").closest('div').find('span').html(val);

        $("select[name=idserie]").trigger("chosen:updated");

        if (idserie == "") {
            $("select[name=idserie]").closest('div').find('span').html('Todos');
        }
    });
}