var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
   unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
   unsaved = true;
});

jQuery(document).ready(function() {
    window.onbeforeunload = function() {
        console.log(unsaved);
        if(unsaved){
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };

    if ((typeof form_values)!='undefined') {
        if ((typeof form_values.status)!='undefined') {
            $("select[name=status]").val(form_values.status);
            if (form_values.status == 'Bloqueado') $("#div-cancelamento").css('display', 'block');
        }
        if ((typeof form_values.sexo)!='undefined') $("select[name=sexo]").val(form_values.sexo);
        if ((typeof form_values.identidade)!='undefined') {
            $("select[name=identidade]").val(form_values.identidade);
            $("input[name=_identidade]").val(form_values.identidade);
        }
        if ((typeof form_values.idescola)!='undefined') {
            $("select[name=idescola]").val(form_values.idescola);
            $("input[name=_idescola]").val(form_values.idescola);
        }
        if ((typeof form_values.curso)!='undefined') $("select[name=curso]").val(form_values.curso);
        if ((typeof form_values.idcurso)!='undefined') $("select[name=idcurso]").val(form_values.idcurso);
        if ((typeof form_values.cursoescola)!='undefined') $("select[name=cursoescola]").val(form_values.cursoescola);
        if ((typeof form_values.semestre)!='undefined') $("select[name=semestre]").val(form_values.semestre);
        if ((typeof form_values.periodo)!='undefined') $("select[name=periodo]").val(form_values.periodo);
        if ((typeof form_values.tipoestudante)!='undefined') {
            $("select[name=tipoestudante]").val(form_values.tipoestudante);
            setTipo(form_values.tipoestudante);
            if (form_values.tipoestudante=='universidade') $('#div-anx-entidade').css('display', 'block');
            if (form_values.tipoestudante=='estadual' || form_values.tipoestudante=='municipal') $('#div-anx-escola').css('display', 'block');
        }
        if ((typeof form_values.tipotransporte)!='undefined') $("select[name=tipotransporte]").val(form_values.tipotransporte);
        if ((typeof form_values.valetransporte)!='undefined') {
            $("select[name=valetransporte]").val(form_values.valetransporte);
            setValeTransporte(form_values.valetransporte);
        }

        if ((typeof form_values.idestado)!='undefined') $("select[name=idestado]").val(form_values.idestado);
        if ((typeof form_values.bairro)!='undefined') $("select[name=bairro]").val(form_values.bairro);

        if ((typeof form_values.eja)!='undefined') {
            if ((form_values.eja)=="sim") $("input[name=eja]").attr("checked", true);
            if ((form_values.eja)=="nao") $("input[name=eja]").attr("checked", false);
        }

        if ((typeof form_values.bolsista)!='undefined') {
            if ((form_values.bolsista)=="sim") $("input[name=bolsista]").attr("checked", true);
            if ((form_values.bolsista)=="nao") $("input[name=bolsista]").attr("checked", false);
        }

        $("select").trigger("chosen:updated");
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function (){
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

    }

    jQuery('select, input, textarea').bind('blur', function() { });

    $('input[name=rendafamiliar]').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.',
        centsLimit: 2
    });

    $('.click_acc').each(function() {
        $( this ).click();
    })

    $("[name=nascimento]").mask("99/99/9999");
    $("[name=cpf]").mask("999.999.999-99");
    $("[name=telefone]").mask("(99) 9999-9999");
    $("[name=celular]").mask("(99) 9 9999-9999");
    $("[name=tituloeleitor]").mask("9999.9999.9999");
    $("[name=zonaeleitor]").mask("999");
    $("[name=secaoeleitor]").mask("9999");
    $("[name=cep]").mask("99.999-999");
    $("[name=subsidio]").mask("999.99");
    $("[name=nis]").mask("99999999999");

    rules = {};
    // var status = $("#status").find('option:selected').html();
    // if(status == 'Bloqueado') rules.anxcancelamento = "required";
    rules.nascimento = "required";
    rules.nome = "required";
    // rules.status = "required";
    rules.tipoestudante = "required";

    messages = {};

    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function (form) {
            unsaved = false;
            $.blockUI({ message: '<h1> Aguarde...</h1>' });
            form.submit();
        }
    });

    $("#tipoestudante").change(function () {
        var tipo = $(this).val();
        setTipo(tipo);
    });

    $("#zonaeleitor").keyup(function () {
        var zona = $(this).val();
        var count = $(this).val().length;
        if (zona == '115' && count == 3) {
            $("#msg-zona").html('Eleitor do Município de Santa Isabel');
        } else {
            $("#msg-zona").html('Eleitor de outro Município');
        }
    });
});

function setStatus() {
    var status = $('select[name=status]').find('option:selected').val();
    var tipo = $('select[name=tipoestudante]').find('option:selected').val();
    if (status == 'Bloqueado' && tipo == 'universidade') {
        $("#div-cancelamento").css('display', 'block');
    } else {
        $("#div-cancelamento").css('display', 'none');
    }
}

function setEntidade(obj) {
    var cidade = $(obj).find('option:selected').data('cidade');
    var subsidio = $(obj).find('option:selected').data('subsidio');

    $("#entidade-subsidio").html(cidade+' - R$ '+subsidio.toFixed(2));
}

function setEscola(tipo) {
    data = '';
    $.post(baseUrl + '/admin/lteestudantes/setescola/', {tipo: tipo}, function (data) {
        $("select[name=idescola]").val(null).trigger('change');
        $("select[name=idescola]").html(data);
    });
}

function setPasse(obj) {
    var tipo = $(obj).val();

    switch (tipo) {
        case "passe":
        $('.box-passe').css('display', 'block');
        break;

        default:
        $('.box-passe').css('display', 'none');
        break;
    }
}

function setTipo(tipo) {
    switch (tipo) {
        case "universidade":
        $('#div-anx-escola').css('display', 'none');
        $('#div-anx-entidade').css('display', 'block');
        $('.box-escola').css('display', 'none');
        $('.box-entidade').css('display', 'block');
        $('#error').css('display', 'none');
        break;

        case "estadual":
        $('#div-anx-entidade').css('display', 'none');
        $('#div-anx-escola').css('display', 'block');
        $('.box-entidade').css('display', 'none');
        $('.box-escola').css('display', 'block');
        $('#error').css('display', 'none');
        setEscola(tipo);
        break;

        case "municipal":
        $('#div-anx-entidade').css('display', 'none');
        $('#div-anx-escola').css('display', 'block');
        $('.box-entidade').css('display', 'none');
        $('.box-escola').css('display', 'block');
        $('#error').css('display', 'none');
        setEscola(tipo);
        break;

        default:
        $('#div-anx-entidade').css('display', 'none');
        $('#div-anx-escola').css('display', 'none');
        $('.box-entidade').css('display', 'none');
        $('.box-escola').css('display', 'none');
        $('#error').css('display', 'block');
        break;       
    }
}

function setValeTransporte(obj){
    var tipo = $(obj).val();

    if(tipo=="nao"){
        $('.box-subsidio').css('display', 'block');
    }else{
        $('.box-subsidio').css('display', 'none');
    }
}

function qtdMask(tr) {
    $(tr).find("input[name='anos[]']").mask("9999");

    $(tr).find("input[name='datas[]']").mask("99/99/9999", {placeholder:" "});
    $(tr).find("input[name='datas[]']").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois'
    });
}
