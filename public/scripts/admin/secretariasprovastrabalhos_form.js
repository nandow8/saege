var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }
	};	
	
	if ((typeof form_values)!='undefined') {
	
		$("select[name=idusuariologado]").val(form_values.idusuariologado);
		$("select[name=idescola]").val(form_values.idescola);
		setSeries($("select[name=idescola]").val(form_values.idescola), form_values.idserie, form_values.idescolavinculo, form_values.idaluno);
		$("select[name=idserie]").val(form_values.idserie);
		$("select[name=idescolavinculo]").val(form_values.idescolavinculo);
		$("select[name=idperiodo]").val(form_values.idperiodo);
		$("select[name=idaluno]").val(form_values.idaluno);
		$("select[name=material]").val(form_values.material);
		$("select[name=status1]").val(form_values.status1);
		

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

	}
	
	$("input[name=datalancamento]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalancamento]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	rules = {};
	rules.idescola = "required";
	rules.idserie = "required";
	rules.idescolavinculo = "required";
	rules.idperiodo = "required";
	rules.idaluno = "required";
	rules.material = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
});

function setSeries(obj, idserie, idescolavinculo, idaluno){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setseries",{idescola: idescola}, function(data) {
		//data = JSON.parse(data.trim());
		//if(data.status == "OK"){
		$('select[name=idserie]').html(data);
		
		$("select[name=idserie]").val(idserie);
		val = $("select[name=idserie]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idserie!=0) && (idserie!="")){
			$("select[name=idserie]").closest('div').find('span').html('Selecione...');
			
			if((idescolavinculo!=0) && (idescolavinculo!="")){
				setTurmas($("select[name=idserie]").val(idserie), idescolavinculo, idaluno)
			}
		}else{
			//setDados($('select[name=idescolavinculo]').val(idescolavinculo));
		}

	});
}

function setVisualiza(){
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
}

function setTurmas(obj, idescolavinculo, idaluno){
	var idserie = $(obj).val();
	var idescola = $("select[name=idescola]").val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setturmas",{idserie: idserie, idescola:idescola}, function(data) {

		$('select[name=idescolavinculo]').html(data);
		
		$("select[name=idescolavinculo]").val(idescolavinculo);
		val = $("select[name=idescolavinculo]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idescolavinculo!=0) && (idescolavinculo!="")){
			$("select[name=idescolavinculo]").closest('div').find('span').html('Selecione...');

			if((idaluno!=0) && (idaluno!="")){
				setAlunos($("select[name=idescolavinculo]").val(idescolavinculo), idaluno)
			}
		}else{
			//setDados($('select[name=idescolavinculo]').val(idescolavinculo));
		}
	});
}

function setAlunos(obj, idaluno){
	var idescolavinculo = $(obj).val();
	var idescola = $("select[name=idescola]").val();
	var idserie = $("select[name=idserie]").val();
	setPeriodo(idescolavinculo);
	$.post(baseUrl + "/" + module + "/"+controller+"/setalunos",{idvinculo: idescolavinculo, idserie: idserie, idescola:idescola}, function(data) {

		$('select[name=idaluno]').html(data);
		
		$("select[name=idaluno]").val(idaluno);
		
		val = $("select[name=idaluno]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idaluno==""){
			$("select[name=idaluno]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setPeriodo(idvinculo){
	$.post(baseUrl + "/" + module + "/"+controller+"/setperiodo",{idvinculo: idvinculo}, function(data) {

		$('select[name=idperiodo]').html(data);
		
		$("select[name=idperiodo]").val();
		val = $("select[name=idperiodo]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

	});
}
// Garantir que a função rode apenas depois que todos o página tenha sido carregada
$(window).load(function(){
	setVisualiza();
});