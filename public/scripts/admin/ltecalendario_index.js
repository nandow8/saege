$(document).ready(function() {
    
    $("#form-event").submit(function() {
        var title = $("input[name=title]").val();
        var color = $("select[name=color]").val();
        var start = $("input[name=start]").val();
        var end = $("input[name=end]").val();

        if($('#municipal-cb').is(':checked'))
            var municipal = 1;
        else
            var municipal = 0;

        if($('#estadual-cb').is(':checked'))
            var estadual = 1;
        else
            var estadual = 0;

        $.post(baseUrl + '/admin/ltecalendario/adicionar/', {title: title, color: color, start: start, end: end, municipal: municipal, estadual: estadual}, function (data) {
        });
    });

    $(".checkbox").change(function () {
        if(!$('#municipal-cb').is(':checked') && !$('#estadual-cb').is(':checked')) {
            $('#municipal-cb').prop('checked', true);
            $('#estadual-cb').prop('checked', true);
            jAlert("Obrigatório ser selecionado Municipal e/ou Estadual");
        }
    })
});

function excluirEvento() {
    var id = $("input[name=id-event]").val();
    var title = $("input[name=title-event]").val();
    jConfirm('O evento '+title+', será excluído!', 'Excluir evento', function (ans) {
        if (ans) {
            $.post(baseUrl + '/admin/ltecalendario/excluir/', {id: id}, function (data) {
                if (data) {
                    location.reload();
                }
            });
        }
    });
}

//Mascara para o campo data e hora
function DataHora(evento, objeto){
    var keypress=(window.event)?event.keyCode:evento.which;
    campo = eval (objeto);
    if (campo.value == '00/00/0000'){
        campo.value=""
    }

    caracteres = '0123456789';
    separacao1 = '/';
    
    conjunto1 = 2;
    conjunto2 = 5;
    conjunto3 = 10;

    if ((caracteres.search(String.fromCharCode (keypress))!=-1) && campo.value.length < (11)){
        if (campo.value.length == conjunto1 )
            campo.value = campo.value + separacao1;
        else if (campo.value.length == conjunto2)
            campo.value = campo.value + separacao1;
    }else{
        event.returnValue = false;
    }
}