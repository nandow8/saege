$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=classe]").val(form_values.classe); 
        $("select[name=status]").val(form_values.status); 
	  
	}  
 
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Ativo") || (data=="Bloqueado")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 

function matriculados(obj){ 
	if($(obj).val() != ''){  
		$("select[name=qtd_transf_rem]").prop("disabled", true); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", true); 
		$("select[name=status]").prop("disabled", true);  
	}else{
		$("select[name=qtd_transf_rem]").prop("disabled", false); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", false); 
		$("select[name=status]").prop("disabled", false);  
	}
}

function transferidos_rematriculados(obj){ 
	if($(obj).val() != ''){  
		$("select[name=qtd_matriculados]").prop("disabled", true); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", true); 
		$("select[name=status]").prop("disabled", true);  
	}else{
		$("select[name=qtd_matriculados]").prop("disabled", false); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", false); 
		$("select[name=status]").prop("disabled", false);  
	}
}

function evadidos_faltosos(obj){ 
	if($(obj).val() != ''){  
		$("select[name=qtd_transf_rem]").prop("disabled", true); 
		$("select[name=qtd_matriculados]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", true); 
		$("select[name=status]").prop("disabled", true);  
	}else{
		$("select[name=qtd_transf_rem]").prop("disabled", false); 
		$("select[name=qtd_matriculados]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", false); 
		$("select[name=status]").prop("disabled", false);  
	}
}

function satisfatorio_matematica(obj){ 
	if($(obj).val() != ''){  
		$("select[name=qtd_transf_rem]").prop("disabled", true); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", true); 
		$("select[name=qtd_matriculados]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", true); 
		$("select[name=status]").prop("disabled", true);  
	}else{
		$("select[name=qtd_transf_rem]").prop("disabled", false); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", false); 
		$("select[name=qtd_matriculados]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", false); 
		$("select[name=status]").prop("disabled", false);  
	}
}

function insatisfatorio_matematica(obj){ 
	if($(obj).val() != ''){  
		$("select[name=qtd_transf_rem]").prop("disabled", true); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_matriculados]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", true); 
		$("select[name=status]").prop("disabled", true);  
	}else{
		$("select[name=qtd_transf_rem]").prop("disabled", false); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_matriculados]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", false); 
		$("select[name=status]").prop("disabled", false);  
	}
}

function satisfatorio_portugues(obj){ 
	if($(obj).val() != ''){  
		$("select[name=qtd_transf_rem]").prop("disabled", true); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", true); 
		$("select[name=qtd_matriculados]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", true); 
		$("select[name=status]").prop("disabled", true);  
	}else{
		$("select[name=qtd_transf_rem]").prop("disabled", false); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", false); 
		$("select[name=qtd_matriculados]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", false); 
		$("select[name=status]").prop("disabled", false);  
	}
}

function insatisfatorio_portugues(obj){ 
	if($(obj).val() != ''){  
		$("select[name=qtd_transf_rem]").prop("disabled", true); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_matriculados]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", true); 
		$("select[name=status]").prop("disabled", true);  
	}else{
		$("select[name=qtd_transf_rem]").prop("disabled", false); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_matriculados]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", false); 
		$("select[name=status]").prop("disabled", false);  
	}
}

function statusclick(obj){ 
	if($(obj).val() != ''){  
		$("select[name=qtd_transf_rem]").prop("disabled", true); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", true); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", true); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", true); 
		$("select[name=qtd_matriculados]").prop("disabled", true);  
	}else{
		$("select[name=qtd_transf_rem]").prop("disabled", false); 
		$("select[name=qtd_evadidos_ou_faltosos]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_mat]").prop("disabled", false); 
		$("select[name=qtd_conc_satisfatorio_port]").prop("disabled", false); 
		$("select[name=qtd_conc_insatisfatorio_port]").prop("disabled", false); 
		$("select[name=qtd_matriculados]").prop("disabled", false);  
	}
}