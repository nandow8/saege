var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {

	$("select[name=status1]").val('Ativo');
	$("select").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});
	
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	$('input[name=pausa]').click(function (){

		if(($(this).attr("checked")=="checked")){
			$('#grupo-pausa-label').html('Sim');
			console.log('checado');
			$('#grupo-pausaentrada').css('display', 'block');
			$('#grupo-pausasaida').css('display', 'block');
		}else{
			$('#grupo-pausa-label').html('Não');
			console.log('não');
			$('#grupo-pausaentrada').css('display', 'none');
			$('#grupo-pausasaida').css('display', 'none');
			$('input[name=horariopausaentrada]').val('00:00');
			$('input[name=horariopausasaida]').val('00:00');
		};
		
	});

	
	
	rules = {};
	rules.titulo = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});