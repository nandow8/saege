function setSeries(obj, q){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setseries",{idescola: idescola}, function(data) {

		if( q == '1') {
			$('select[name=idserie-1]').html(data);
			val = $("select[name=idserie-1]").find('option:selected').html();
		} else {
			$('select[name=idserie-2]').html(data);
			val = $("select[name=idserie-2]").find('option:selected').html();
		}
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	});
}

function setPeriodo(obj, q) {
	var idescola = $(obj).val();

	$.post(baseUrl + "/" + module + "/"+controller+"/setperiodo",{idescola: idescola}, function(data) {

		q == '1' ? $("select[name=periodo-1]").html(data) : $("select[name=periodo-2]").html(data);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	});
}

function setTurmas(obj, q){
	var idserie = $(obj).val();
	var idescola = q == '1' ? $("select[name=idescola-1]").val() : $("select[name=idescola-2]").val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setturmas",{idserie: idserie, idescola:idescola}, function(data) {

		q == '1' ? $('select[name=idturma-1]').html(data) : $("select[name=idturma-2]").html(data); 	
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	});
}

function setAlunos(obj, q){	

	var idescolavinculo = $(obj).val();
	var idescola = q == '1' ? $("select[name=idescola-1]").val() : $("select[name=idescola-2]").val();
	var idserie =  q == '1' ? $("select[name=idserie-1]").val() : $("select[name=idserie-2]").val();

	$.post(baseUrl + "/" + module + "/"+controller+"/setalunos",{idvinculo: idescolavinculo, idserie: idserie, idescola:idescola}, function(data) {

		q == '1' ? $('select[name=idalunos-1]').html(data) : $('select[name=idalunos-2]').html(data);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	});
}

function comparar() {

	//Checar se é possível comparar
	var aluno_1 = $("select[name=idalunos-1]").val();
	var aluno_2 = $("select[name=idalunos-2]").val();
	var turma_1 = $("select[name=idturma-1]").val();
	var turma_2 = $("select[name=idturma-2]").val();

	if( (aluno_1 == "" && turma_1 == "" ) || (aluno_2 == "" && turma_2 == "") ) {
		alert("Precisa selecionar ao menos uma turma para cada comparação");
		return 0;
	}

	//ve base da comparação
	var base;

	$("input[type=radio][name='basecomparacao[]']").each(function() {
		if($(this).attr("checked") == "checked")
			base = $(this).val();
	});

	var tipo;

	if( aluno_2 == "" && aluno_1 == "" )
		tipo = "turmaxturma";
	else if( aluno_2 == "" )
		tipo = "alunoxturma"
	else if( aluno_1 == "")
		tipo = "turmaxaluno"
	else 
		tipo = "alunoxaluno";

	idescola_1 = $("select[name=idescola-1").val();
	idescola_2 = $("select[name=idescola-2").val();

	idserie_1 = $("select[name=idserie-1").val();
	idserie_2 = $("select[name=idserie-2").val();

	idturma_1 = $("select[name=idturma-1").val();
	idturma_2 = $("select[name=idturma-2").val();

	idaluno_1 = $("select[name=idalunos-1").val();
	idaluno_2 = $("select[name=idalunos-2").val();

	//esconde e reseta todos os campos
	hideall();	

	$.post(baseUrl + "/" + module + "/"+controller+"/comparar",{tipo: tipo, base: base, idescola_1:idescola_1, 
		idescola_2:idescola_2, idserie_1:idserie_1, idserie_2:idserie_2, idturma_1:idturma_1, 
		idturma_2:idturma_2, idaluno_1:idaluno_1, idaluno_2:idaluno_2}, function(data) {

			$("#tablehelp").html(data);

			$("#tablehelp").show();
	});	
}

function hideall() {
	$("input[type=radio][name='basecomparacao[]']").each(function() {
		$(this).prop("checked", false);
	});

	$("select").each(function() {
		$(this).val("");
	});

	$("select").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});
		
	$("select").each(function (){
		div = $(this).closest('div').find('.chosen-container').css('width', '100%');
	})

	$(".div-escolas").hide();
	$(".div-serie-1").hide();
	$(".div-periodo-1").hide();
	$(".div-serie-2").hide();
	$(".div-periodo-2").hide();
	$(".div-turma-1").hide();
	$(".div-turma-2").hide();
	$(".div-alunos-1").hide();
	$(".div-alunos-2").hide();
}

$(document).ready(function() {

	//CHANGES
	$("input[name='basecomparacao[]']").change(function() {
		$(".div-escolas").show();
	});

	$("select[name=idescola-1]").change(function() {
		if($(this).val() != ""){
			$(".div-serie-1").show();
			$(".div-periodo-1").show();
		}
		else{
			$(".div-serie-1").hide();
			$(".div-periodo-1").hide();
		}
	});

	$("select[name=idescola-2]").change(function() {
		if($(this).val() != ""){
			$(".div-serie-2").show();
			$(".div-periodo-2").show();
		}
		else{
			$(".div-serie-2").hide();
			$(".div-periodo-2").hide();
		}
	});

	$("select[name=idserie-1]").change(function() {
		if($(this).val() != ""){
			$(".div-turma-1").show();
		}
		else{
			$(".div-turma-1").hide();
		}
	});

	$("select[name=idserie-2]").change(function() {
		if($(this).val() != ""){
			$(".div-turma-2").show();
		}
		else{
			$(".div-turma-2").hide();
		}
	});

	$("select[name=idturma-1]").change(function() {
		if( $(this).val() != "" ) {
			$(".div-alunos-1").show();
		} else {
			$(".div-alunos-1").hide();
		}
	});

	$("select[name=idturma-2]").change(function() {
		if( $(this).val() != "" ) {
			$(".div-alunos-2").show();
		} else {
			$(".div-alunos-2").hide();
		}
	});
});