var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	 
	if ((typeof form_values)!='undefined') {
		//$("select[name=idtipoalimento]").val(form_values.idtipoalimento);
                $("select[name=status1]").val(form_values.status1);
                $("select[name=ds_fonte]").val(form_values.ds_fonte);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
        $('#accordion').accordion({ collapsible: true });  

        rules = {};
	//rules.idtipoalimento = "required";
        rules.numero_alimento = "required";
        rules.descricao = "required";
        rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
    
});

$('#adicionar').click(function(){
	
	idselect = $('#selectmedidas option:selected"').val();
	obj = $('#medidas');

    $.post(baseUrl + "/" + module + "/nutricaorefeicoes/unidadescomerciaisajax",
 	{id: idselect}, function(data){
        data = JSON.parse(data);
		obj.append('<tr><td><input name="remove" type="checkbox"></td><td><h4"><input name="opcao[]" type="hidden" value="' + data.id + '">' + data.unidade + '</h4></td><td>' + data.valdefault + '</td></tr>'); 
 	});
	
	
});

$('#remover').click(function(){
	$('#medidas').find('tr > td > input').each(function(index, val){
		if(val.name == 'remove'){
			if(val.checked == true){
				console.log($(this).parent().parent().remove());
			}
		}//end if
	});
});

