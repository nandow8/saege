var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
   // unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
   // unsaved = true;
});

jQuery(document).ready(function() {
    window.onbeforeunload = function() {
        console.log(unsaved);
        if(unsaved){
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }       
    };
    
    if ((typeof form_values)!='undefined') {
        if ((typeof form_values.status)!='undefined') $("select[name=status]").val(form_values.status);
        if ((typeof form_values.tipo)!='undefined') {
            $("select[name=tipo]").val(form_values.tipo);
            if (form_values.tipo=='universidade') $('#div-cidade').css('display', 'block');
            if (form_values.tipo=='estadual') $('#div-bairro').css('display', 'block');
        }
        if ((typeof form_values.idbairro)!='undefined') $("select[name=idbairro]").val(form_values.idbairro);
        if ((typeof form_values.idcidade)!='undefined') $("select[name=idcidade]").val(form_values.idcidade);

        $("select").trigger("chosen:updated");
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
        
        $("select").each(function (){
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });       
    }

    jQuery('select, input, textarea').bind('blur', function() { });
    
    $('input[name=rendafamiliar]').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.',        
        centsLimit: 2    
    });
    
    $('.click_acc').each(function() {
        $( this ).click();
    })

    $("[name=telefone]").mask("(99) 9999-9999");
    $("[name=cep]").mask("99.999-999");
    $("[name=subsidio]").mask("99%");
    
    rules = {};
    rules.status = "required";
    rules.cpf = {required:false, mn_cnpj: true};
    
    messages = {};
    
    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function (form) {
            unsaved = false;
            $.blockUI({ message: '<h1> Aguarde...</h1>' });
            form.submit();
        }
    });

    $("#tipo").change(function () {
        var tipo = $(this).val();
        switch (tipo) {
        case "universidade":
            $('#div-bairro').css('display', 'none');
            $('#div-cidade').css('display', 'block');
            break;

        case "estadual":
            $('#div-cidade').css('display', 'none');
            $('#div-bairro').css('display', 'block');
            break;

        default:
            $('#div-cidade').css('display', 'none');
            $('#div-bairro').css('display', 'none');
            break;
    }
    });
});


function qtdMask(tr) {
    $(tr).find("input[name='anos[]']").mask("9999");
    
    $(tr).find("input[name='datas[]']").mask("99/99/9999", {placeholder:" "});
    $(tr).find("input[name='datas[]']").datepicker({ 
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois'
    }); 
}