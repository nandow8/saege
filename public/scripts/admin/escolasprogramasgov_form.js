var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {		
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=apm]").val(form_values.apm);
$("select[name=idtipoconvenio]").val(form_values.idtipoconvenio);
$("select[name=status1]").val(form_values.status1);

	if ((typeof form_values.apm)!='undefined') {
		setTipo($('select[name=apm]'));
	}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	




	$("input[name=datainicio]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datainicio]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=datafim]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datafim]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$('input[name=exercicio]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$("input[name=cpfcnpj]").mask("99.999.999/9999-99", {placeholder:" "});


	$('input[name=saldoexercicioanterior_custeio]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=saldoexercicioanterior_capital]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=valorcreditadofnde_custeio]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=valorcreditadofnde_capital]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=recursosproprios_custeio]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=recursosproprios_capital]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=rendimentoaplfinanceira_custeio]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=rendimentoaplfinanceira_capital]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=devolucaorecursosfnde_custeio]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=devolucaorecursosfnde_capital]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=valortotalreceita_custeio]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=valortotalreceita_capital]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=valordespesarealizada_custeio]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=valordespesarealizada_capital]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=saldoexercicioseguinte_custeio]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=saldoexercicioseguinte_capital]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=saldodevolvido_custeio]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=saldodevolvido_capital]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	$('input[name=saldoexercicioanterior_apm]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});

	$('input[name=rendimentoaplicacaofinanceira_apm]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});


	
	
	rules = {};
	rules.idescola = "required";
rules.idtipoconvenio = "required";
rules.titulo = "required";
rules.apm = "required";
rules.datainicio = "required";
rules.datafim = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


//CONTAS
function contaAddTr(_values) {
    
	html = "";
	
	if (_values===false) {
		_values = {};
	} else {
		_values = jQuery.parseJSON(_values);
		itens = _values;
	}
	
	modelo = $("#modelo-apm").html();
	modelo = '<tr>' + modelo + '</tr>';
	modelo = $(modelo);
	table = $("table.table-contas");
	container = table.find('tbody');
	
	modelo.find('p').html(html);
	modelo.find('input,select,textarea').each(function() {
		name = $(this).attr('name');
		name = name.replace("_", "");
		if(_values && _values.var_db == true)name = name.replace("[]", "");
		
    
		for (var property in _values) {
			if (property==name) {
				
				$(this).attr('name', name + '[]');
				if(_values.var_db){
				}

				$(this).attr('value', _values[property]);
				
				if ($(this).is("select")) {
					$(this).parent().find('a').remove();
				}
			}
		}
	});
	container.append(modelo);
	
	$('.numbervalue').priceFormat({
		prefix: '',
		limit: 3,
        centsSeparator: '',
        thousandsSeparator: ''	    
	});

	$('.numbervalue').blur(function(){
		if($(this).val() <= 0){
			$(this).val('');	
		}
			
	});
}

function contasExcluirFromTable(obj) {
	jConfirm("Deseja excluir o registro?", 'Confirmar', function(r) {
		
            if(r)
            {
		obj = $(obj);
		content = obj.closest('tr');
		content.find('input').remove();
		content.hide();
            }
		
	});
}

function setTipo(obj)
{
	var tipo = $(obj).val();

	if(tipo == 'sim')
	{
		$('.box-outro').hide();
		$('.box-apm').show();
	}
	else
	{
		$('.box-outro').show();
		$('.box-apm').hide();
	}
}