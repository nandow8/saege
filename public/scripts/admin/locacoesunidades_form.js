var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idlocador]").val(form_values.idlocador);
$("select[name=idimovel]").val(form_values.idimovel);
$("select[name=status1]").val(form_values.status1);
if ((typeof form_values.idlocador)!='undefined') getImoveis($("select[name=idlocador]").val(form_values.idlocador),	form_values.idimovel);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	



	
	
	rules = {};
	rules.idlocador = "required";
rules.idimovel = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


function getImoveis(obj, idimovel) {
	
	val = $(obj).val();
	if (val=='') {
		return;
	}
	
	$("select[name=idimovel]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/'+controller+'/getimoveis', {id:val}, function(data) {
		console.log(data);		
		$("select[name=idimovel]").html(data);
		$("select[name=idimovel]").val(idimovel);
		val = $("select[name=idimovel]").find('option:selected').html();
		$("select[name=idimovel]").closest('div').find('span').html(val);
	
		$("select[name=idimovel]").trigger("chosen:updated");
		
		if(idimovel==""){
			$("select[name=idimovel]").closest('div').find('span').html('Selecione...');
		}
		/*imoveis.html(data);	
		
		if(idlocador){
			$("select[name=idimovel]").val(form_values.idimovel);	
		}*/
	});
	
}