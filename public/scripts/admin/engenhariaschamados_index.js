$(document).ready(function() {

	$("input[name=dataagendada_i],input[name=dataagendada_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataagendada_i],input[name=dataagendada_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date){
	    }
	});


	$("select[name=idescola]").val(idescola);

	$("select[name=idescola]").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});	

	$("select[name=idescola]").each(function (){
		div = $(this).closest('div').find('.chosen-container').css('width', '100%');
	});

	$("select[name=idprioridade]").val(idprioridade);

	$("select[name=idprioridade]").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});	

	$("select[name=idprioridade]").each(function (){
		div = $(this).closest('div').find('.chosen-container').css('width', '100%');
	});
	
	$("select[name=idstatus]").val(idstatus);

	$("select[name=idstatus]").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});	

	$("select[name=idstatus]").each(function (){
		div = $(this).closest('div').find('.chosen-container').css('width', '100%');
	});



	

	//if ((typeof form_values)!='undefined') {
	if (true) {
		
		// $("select[name=destino]").val(form_values.destino);
		// $("select[name=iddepartamento]").val(form_values.iddepartamento);
		// $("select[name=idescola]").val(form_values.idescola);
		
		// $("select[name=idstatus]").val(form_values.idstatus);
		// $("select[name=finalizacao]").val(form_values.finalizacao);
		// $("select[name=atendido]").val(form_values.atendido);
		// $("select[name=idtipo]").val(form_values.idtipo);
		// $("select[name=idsfuncionarios]").val(form_values.idsfuncionarios);
		// $("select[name=status1]").val(form_values.status1);

		

		// $("select[name=idescola]").each(function() {
		// 	val = $(this).find('option:selected').html();
		// 	$(this).closest('div').find('span').html(val);
		// });	

		// $("select[name=idescola]").each(function (){
		// 	div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		// });

		//$("select[name=idescola]").val('21');
		
		// $("select[name=idprioridade]").each(function (){
		// 	div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		// });
	}//end if
	
	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}//end function

// function statusChange($id, $obj) {
//  	$obj = $($obj);
 
//  	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
//  	{id: $id, op: controller}, function(data) {
// 		if ((data=="Ativo") || (data=="Bloqueado")) {
// 			$obj.html(data);
// 		} else {
// 			jAlert(data, "Alerta!");
// 		}
//  	});
// } 