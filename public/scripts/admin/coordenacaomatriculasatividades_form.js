var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idusuariologado]").val(form_values.idusuariologado);
		$("select[name=idatividade]").val(form_values.idatividade);
		$("[name=idsalunos]").val(form_values.idsalunos);
		$("select[name=status1]").val(form_values.status1);

		if ((typeof form_values.idescola)!='undefined' && (typeof form_values.idsalunos)!='undefined') {
			setAlunos($("select[name=idescola]"), form_values.idsalunos);
			//setAlunos($("select[name=idescola]"), form_values.idaluno);
		}

		if ((typeof form_values.idescola)!='undefined' && (typeof form_values.idatividade)!='undefined') {
			setAtividades($("select[name=idescola]"), form_values.idatividade);
			//setAlunos($("select[name=idescola]"), form_values.idaluno);
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	
	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});



	
	
	rules = {};
	rules.idusuariologado = "required";
rules.data = "required";
rules.idatividade = "required";
rules.idaluno = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


function setAtividades(obj, idatividade){
	var idescola = $(obj).val();
	
	data = '';
	$("select[name=idatividade]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/'+controller+'/setatividades/', {idescola:idescola}, function(data) {
		$("select[name=idatividade]").html(data);

		$("select[name=idatividade]").val(idatividade);
		val = $("select[name=idatividade]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});

		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idatividade==""){
			$("select[name=idatividade]").closest('div').find('span').html('Selecione...');

			//$('.box-dados-alunos').css('display', 'none');
			//$(".setdado").html('');	
		}
		
	});
}

function setAlunos(obj, idalunos) {
		var idescola = $(obj).val();

		if( idescola != 0 && idescola != "" )
			$.post(baseUrl + "/" + module + "/"+controller+"/setalunos",{idescola: idescola}, function(data) {

				$("#alunos-in").html(data);
				
				$("#alunos").show();

				if( idalunos != "" && idalunos != undefined )
					$("input[type=checkbox][name='idsalunos[]']").val( idalunos.split(',') );

				$("select").each(function() {
					val = $(this).find('option:selected').html();
					$(this).closest('div').find('span').html(val);
				});
		
				$("select").each(function (){
					div = $(this).closest('div').find('.chosen-container').css('width', '100%');
				});
			});
		else
			$("#alunos").hide();
}
