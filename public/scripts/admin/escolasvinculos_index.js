$(document).ready(function() {
    if ((typeof form_values) != 'undefined') {
        $("select[name=idsecretaria]").val(form_values.idsecretaria);
        $("select[name=idescola]").val(form_values.idescola);
        $("select[name=idsala]").val(form_values.idsala);
        $("select[name=serie]").val(form_values.serie);
        $("select[name=idturma]").val(form_values.idturma);
        $("select[name=idperiodo]").val(form_values.idperiodo);
        $("select[name=idprofessor]").val(form_values.idprofessor);
        $("select[name=idtipoensino]").val(form_values.idtipoensino);
        $("select[name=turno]").val(form_values.turno);
        $("select[name=integracao]").val(form_values.integracao);
        $("select[name=soapretorno]").val(form_values.soapretorno);
        $("select[name=status1]").val(form_values.status1);

        if (form_values.serie != "") {
            setSeries('select[name=idescola]', form_values.serie);
        } else
            setSeries('select[name=idescola]', 0);

        if (form_values.idturma != "") {
            setTurmas('select[name=idescola]', form_values.idturma);
        } else
            setTurmas('select[name=idescola]', 0);

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
    }
    //if($("select[name=idescola]").val()==0) setSeries('select[name=idescola]', 0);

    $("input[name=inicioaulas_i],input[name=inicioaulas_f]").mask("99/99/9999", {placeholder: " "});
    $("input[name=inicioaulas_i],input[name=inicioaulas_f]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });
    $("input[name=terminoaulas_i],input[name=terminoaulas_f]").mask("99/99/9999", {placeholder: " "});
    $("input[name=terminoaulas_i],input[name=terminoaulas_f]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });

    $('input[name=ano]').priceFormat({
        prefix: '',
        centsSeparator: '',
        thousandsSeparator: '',
        centsLimit: 0
    });




});

function confirmDelete($id, $texto) {
    jConfirm('Confirmar a exclusão de "' + $texto + '"?', 'Excluir registro', function(r) {
        if (r) {
            $.post(baseUrl + "/" + module + "/" + controller + "/excluirxml",
                    {id: $id}, function(data) {
                if (data == "OK") {
                    window.location = baseUrl + "/" + module + '/' + controller;
                } else {
                    jAlert(data, "Erro!");
                }
            });
        }
    });
}

function statusChange($id, $obj) {
    $obj = $($obj);

    $.post(baseUrl + "/" + module + "/" + controller + "/changestatusxml",
            {id: $id, op: controller}, function(data) {
        if ((data == "Ativo") || (data == "Bloqueado")) {
            $obj.html(data);
        } else {
            jAlert(data, "Alerta!");
        }
    });
}


function statusSED(id) {

    var status = document.getElementById("soapstatus" + id).value;

    var dialog = $('<h5 >' + status + '</h5>').dialog({
        title: '<font color="#052f57">Status do retorno do SED.</font>',
        width: 400,
        modal: true,
        buttons: {
            "Fechar": function() {
                dialog.dialog('close');
            }
        }
    });
}

function setSeries(obj, serie) {
    var val = $(obj).val();

    data = '';
    $("select[name=serie]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasvinculos/setseriesbyescola/', {idescola: val}, function(data) {
        $("select[name=serie]").html(data);
        $("select[name=serie]").val(serie);
        val = $("select[name=serie]").find('option:selected').html();
        $("select[name=serie]").closest('div').find('span').html(val);

        $("select[name=serie]").trigger("chosen:updated");

        $("input[name=idtipoensino]").val($.trim($("#idserie").children("option:selected").text()).split(".")[0]);

        if (serie === "") {
            $("select[name=serie]").closest('div').find('span').html('Todos');
            $("select[name=idturma]").closest('div').find('span').html('Todos');
        }
    });
}

function setTurmas(obj, idturma) {
    var val = $(obj).val();

    data = '';
    $("select[name=idturma]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasvinculos/setturmasbyescola/', {idescola: val}, function(data) {
        $("select[name=idturma]").html(data);
        $("select[name=idturma]").val(idturma);
        val = $("select[name=idturma]").find('option:selected').html();
        $("select[name=idturma]").closest('div').find('span').html(val);

        $("select[name=idturma]").trigger("chosen:updated");

        if (idturma == "") {
            $("select[name=idturma]").closest('div').find('span').html('Todos');
        }
    });
}

function LimpaTitulo(rotulo) {
   
    var linha = '';
    var Uchar = 'ABCDEFGHIJKLMNOPQRSTUWXYZ';
    var verifica = rotulo.substring(0, 3);//pegar as 3 primeiras letras
   
    if(verifica==="out"){ rotulo = rotulo.substring(3, rotulo.length); } //pega o resto da palavra
        
    p = 1;
    texto = '';
    textoArray = [];//array

    //verifica se há letras maiúsculas
    //se sim, add um espaço separando as palavras
    for (i in rotulo){
        for (c in Uchar){                      
            if ((rotulo.charAt(i) === Uchar.charAt(c)) && i > p){                   
                texto = rotulo.replace(rotulo.charAt(i), ' '+Uchar.charAt(c));
                textoArray.push(texto.split(' ')[0]);//gera array com as palavras                   
                rotulo = rotulo.replace(texto.split(' ')[0], '');
                p = i;
            }                
        }
        
    }
    textoArray.push(rotulo);
    retorno = '';
    //formar a string de retorno
    $.each(textoArray, function(t, v){            
        if(v!=="") retorno += v + ' ';            
    });
    
    return retorno;
}

function modalAcoes(element, titulo, largura, altura, headercolor){
    var dialog = $(element).dialog({
        title: titulo,
        width: largura,
        height: altura,   
        modal: true        
    });
    $(".ui-draggable .ui-dialog-titlebar").css("background", headercolor);

}
/**
 * Função: Buscar alunos cadastrados no SED
 * @param classe - número da classe
 * @param obj - objeto clicado
 * @return alunos - alunos vinculados a classe
 */
function consultaClasseSed(classe, obj){
    modalAcoes("#classe-modal", "Alunos da classe "+classe , 700, 'auto', "#3e95ce");
    loading = '<div class="ud-loader"><img src="/public/admin/img/elements/loaders/6.gif" alt="Aguarde..." /></div>';

   $("#classe-modal").html(loading);
   
    if(classe!=""){      
        $.ajax({
            type: "POST",
            url: baseUrl + "/" + module + "/" + controller + "/consultaclassesed/",
            dataType: "JSON",
            data: { classe: classe },
            success: function(data){                
                content = "";
                
                dados = data.Retorno.Mensagens;
                if(dados.ConsultaClasse.outErro){
                    content = '<div class="alerta">'+dados.ConsultaClasse.outErro+'</div>';
                }else{
                    info = dados.ConsultaClasse;

                    content += '<table class="unique-date">';
                    content += '<thead class="ud-head"><tr>';
                    content += '<th class="ud-subtitle">#</th>';
                    content += '<th class="ud-subtitle">Nome</th>';
                    content += '<th class="ud-subtitle">RA</th>';
                    content += '<th class="ud-subtitle">Status</th><tr>  ';          
                    content += '</thead>';
                    content += '<tbody class="ud-info">';       
                    $.each(info, function(index, element){                       
                        content += '<tr><td class="ud-subtitle">'+(index+1)+'</td>';
                        content += '<td class="ud-subtitle">'+info[index].nomeAluno+'</td>';
                        content +='<td class="ud-subtitle">'+info[index].RA+'-'+info[index].digitoRA+' -'+info[index].UF+' </td>';
                        content +='<td class="ud-subtitle">'+((info[index].status) ? info[index].status : "--") +'</td></tr>';
                    });
                    content += '</tbody>';
                    content += '</table>';
                }

               $("#classe-modal").html(content);
            }
        });
    }
}
 
/**
 * Função: Redimensionar o select da class 'SELECT'
 * @param min - tamanho mínimo do select
 * @param max - tamanho máximo do select
 */
$(function() {
    $(".select2-container").css({
        'min-width': '140px',
        'max-width': '250px'
    });
});