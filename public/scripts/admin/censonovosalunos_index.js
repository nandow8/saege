$(document).ready(function() {

	//carregaValores();
	//retornaDistancias();

	if ((typeof form_values)!='undefined') {

		$("select[name=idcentralvaga]").val(form_values.idcentralvaga);
		$("select[name=sexo]").val(form_values.sexo);
		$("select[name=filiacao1]").val(form_values.filiacao1);
		$("select[name=filiacao2]").val(form_values.filiacao2);
		$("select[name=temnomesocial]").val(form_values.temnomesocial);
		$("select[name=status1]").val(form_values.status1);
		$("select[name=datadecorte]").val(form_values.datadecorte);
		$("select[name=matricula]").val(form_values.matricula);
		$("select[name=estado]").val(form_values.estado);
		
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		$("input[name=datanascimento_i],input[name=datanascimento_f]").mask("99/99/9999", {placeholder:" "});
		$("input[name=datanascimento_i],input[name=datanascimento_f]").datepicker({
			dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
			dateFormat: 'dd/mm/yy',
			dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			currentText: 'Hoje',
			prevText: 'Antes',
			nextText: 'Depois',
			onSelect: function(date) {
			}
		});

 
	}//end if

});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		  if ((data == "Desativado") || (data == "Ativo")) {
			  $obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 
