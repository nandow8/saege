var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
   // unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
   // unsaved = true;
});

jQuery(document).ready(function() {
    window.onbeforeunload = function() {
        console.log(unsaved);
        if(unsaved){
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }       
    };

    if ((typeof form_values)!='undefined') {
        if ((typeof form_values.status)!='undefined') $("select[name=status]").val(form_values.status);

        $("select").trigger("chosen:updated");
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
        
        $("select").each(function (){
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });       
    }

    jQuery('select, input, textarea').bind('blur', function() { });
    
    $('.click_acc').each(function() {
        $( this ).click();
    })

    $("[name=subsidio]").mask("99%");

    $('#valorfretado').priceFormat({
        prefix: '',
        centsSeparator: '.',
        thousandsSeparator: '',
        centsLimit: 2
    });

    // $("[name=placa]").mask(""); // mascara na placa ???
    
    rules = {};
    rules.status = "required";
    
    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function (form) {
            unsaved = false;
            $.blockUI({ message: '<h1> Aguarde...</h1>' });
            form.submit();
        }
    });
});