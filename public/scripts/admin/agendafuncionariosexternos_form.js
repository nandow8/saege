var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=status1]").val(form_values.status1);
		$("select[name=iddepartamento]").val(form_values.iddepartamento);
		setUsuario($("select[name=iddepartamento]").val(form_values.iddepartamento), form_values.idusuario);
		setFuncionario($("select[name=funcionario]").val(form_values.funcionario));
$("select[name=idusuario]").val(form_values.idusuario);
$("select[name=funcionario]").val(form_values.funcionario);
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	
	$("input[name=telefone]").mask("(99)9?9999-9999", {placeholder:" "});
	$("input[name=celular]").mask("(99)9?9999-9999", {placeholder:" "});






	
	
	rules = {};
//	rules.nomerazao = "required";
//rules.sobrenomefantasia = "required";
rules.telefone = "required";
rules.funcionario = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setFuncionario(obj){
	var funcionario = $(obj).val();
	if(funcionario=="Sim"){
		$('.box-usuarios').css('display', 'block');
		$('.box-semnome').css('display', 'none');
	}else{
		$('.box-usuarios').css('display', 'none');
		$('.box-semnome').css('display', 'block');
	}
}

function setUsuario(obj, idusuario){
	var iddepartamento = $(obj).val();
	data = '';
	$("select[name=idusuario]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + "/" + module + "/"+controller+"/setusuarios/", {iddepartamento:iddepartamento}, function(data) {
		$("select[name=idusuario]").html(data);

		$("select[name=idusuario]").val(idusuario);
		val = $("select[name=idusuario]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idusuario==""){
			$("select[name=idusuario]").closest('div').find('span').html('Selecione...');
		}
	});
}