var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idprofessor]").val(form_values.idprofessor);
		$("select[name=idmateria]").val(form_values.idmateria);
		$("select[name=idserie]").val(form_values.idserie);
		$("select[name=confirmacaocoordenacao]").val(form_values.confirmacaocoordenacao);
		$("select[name=status1]").val(form_values.status1);

		if ((typeof form_values.idprofessor)!='undefined' && (typeof form_values.idserie)!='undefined') {
			setSeries($("select[name=idprofessor]"), form_values.idserie);
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	
	$("input[name=datalancamento]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalancamento]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	
	
	rules = {};
	rules.idprofessor = "required";
	rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


function setSeries(obj, idserie){
	var idprofessor = $(obj).val();
	
	data = '';
	$("select[name=idserie]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/'+controller+'/setseries/', {idfuncionario:idprofessor}, function(data) {
		$("select[name=idserie]").html(data);

		$("select[name=idserie]").val(idserie);
		val = $("select[name=idserie]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});

		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idserie==""){
			$("select[name=idserie]").closest('div').find('span').html('Selecione...');

			//$('.box-dados-alunos').css('display', 'none');
			//$(".setdado").html('');	
		}
		
	});
}