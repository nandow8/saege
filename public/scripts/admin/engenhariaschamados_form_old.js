var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idusuariocriacao]").val(form_values.idusuariocriacao);
$("select[name=iddepartamentosecretaria]").val(form_values.iddepartamentosecretaria);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=iddepartamentoescola]").val(form_values.iddepartamentoescola);

setDepartamentos($("select[name=idescola]").val(form_values.idescola), form_values.iddepartamentoescola, form_values.idescolausuarioresponsavel);
$("select[name=origem]").val(form_values.origem);
$("select[name=destinatario]").val(form_values.destinatario);
$("select[name=idusuarioresponsavel]").val(form_values.idusuarioresponsavel);

$("select[name=tipo]").val(form_values.tipo);
$("select[name=prioridade]").val(form_values.prioridade);

$("select[name=_tipo]").val(form_values.tipo);
$("select[name=_prioridade]").val(form_values.prioridade);

$("select[name=status1]").val(form_values.status1);

		if ((typeof form_values.destinatario)!='undefined')
		{
			setTipo($("select[name=destinatario]").val(form_values.destinatario));
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	

		$("input[name=dataagendamento]").mask("99/99/9999", {placeholder:" "});
		
		$("input[name=dataagendamento]").datepicker({ 
			dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
			dateFormat: 'dd/mm/yy',
			dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			currentText: 'Hoje',
			prevText: 'Antes',
			nextText: 'Depois'
		});











	
	
	rules = {};
	rules.numeroprotocolo = "required";
rules.titulo = "required";
rules.dataagendamento = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setTipo(obj){
	tipo = $(obj).val();

	if(tipo == 'Secretaria'){
		$('.box-interno').show();
		$('.box-externo').hide();
	}
	else {
		$('.box-interno').hide();
		$('.box-externo').show();
	}
}

function setDepartamentos(obj, iddepartamentoescola, idescolausuarioresponsavel){
	var idescola = $(obj).val();
	data = '';
	$("select[name=iddepartamentoescola]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/engenhariaschamados/setdepartamentosescolas/', {idescola:idescola}, function(data) {
		$("select[name=iddepartamentoescola]").html(data);

		$("select[name=iddepartamentoescola]").val(iddepartamentoescola);
		val = $("select[name=iddepartamentoescola]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(iddepartamentoescola==""){
			$("select[name=iddepartamentoescola]").closest('div').find('span').html('Selecione...');
		}
		setFuncionarios($("select[name=iddepartamentoescola]"), idescolausuarioresponsavel);
	});
}

function setFuncionarios(obj, idescolausuarioresponsavel){
	var idescola = $('select[name="idescola"]').val();
	var iddepartamento = $(obj).val();
	data = '';
	$("select[name=idescolausuarioresponsavel]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/engenhariaschamados/setescolausuarioresponsavel/', {idescola:idescola, iddepartamento:iddepartamento}, function(data) {
		$("select[name=idescolausuarioresponsavel]").html(data);

		$("select[name=idescolausuarioresponsavel]").val(idescolausuarioresponsavel);
		val = $("select[name=idescolausuarioresponsavel]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idescolausuarioresponsavel==""){
			$("select[name=idescolausuarioresponsavel]").closest('div').find('span').html('Selecione...');
		}
	});
}