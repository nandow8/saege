var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
}); 

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola); 
		$("select[name=ano]").val(form_values.ano); 
		$("select[name=bimestre]").val(form_values.bimestre); 
		$("select[name=classe]").val(form_values.classe); 
		$("select[name=qtd_matriculados]").val(form_values.qtd_matriculados); 
		$("select[name=qtd_incluidos]").val(form_values.qtd_incluidos);
		$("select[name=qtd_transf_rem]").val(form_values.qtd_transf_rem);
		$("select[name=qtd_evadidos_ou_faltosos]").val(form_values.qtd_evadidos_ou_faltosos);
		$("select[name=qtd_cca_port]").val(form_values.qtd_cca_port);
		$("select[name=qtd_cca_mat]").val(form_values.qtd_cca_mat);
		$("select[name=qtd_conc_insatisfatorio_port]").val(form_values.qtd_conc_insatisfatorio_port);
		$("select[name=qtd_conc_insatisfatorio_mat]").val(form_values.qtd_conc_insatisfatorio_mat);
		$("select[name=qtd_conc_satisfatorio_port]").val(form_values.qtd_conc_satisfatorio_port);
		$("select[name=qtd_conc_satisfatorio_mat]").val(form_values.qtd_conc_satisfatorio_mat);
		$("select[name=status]").val(form_values.status);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});

	}
	  
	rules = {}; 
	rules.bimestre = "required"; 
	rules.ano = "required"; 
	rules.classe = "required"; 
	rules.qtd_matriculados = "required"; 
	rules.qtd_incluidos = "required";
	rules.qtd_transf_rem = "required";
	rules.qtd_evadidos_ou_faltosos = "required";
	rules.qtd_cca_port = "required";
	rules.qtd_cca_mat = "required";
	rules.qtd_conc_insatisfatorio_port = "required";
	rules.qtd_conc_insatisfatorio_mat = "required";
	rules.qtd_conc_satisfatorio_port = "required";
	rules.qtd_conc_satisfatorio_mat = "required";
	rules.status = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	

	//visualizar = true;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}	
	 
});
 

function naorepetirbimestre(){
	let ano = $("select[name=ano]").val();  
	let bimestre = $("select[name=bimestre]").val();  
	let classe = $("select[name=classe]").val();  
	let idescola = $("input[name=idescola]").val(); 
 
	$.post(baseUrl + '/admin/coordenacaoquadrodesempenhoaprendizagem/naorepetirbimestre', {bimestre:bimestre , ano: ano, classe: classe, idescola: idescola}, function(dados){
		dados = JSON.parse(dados);

		if(Object.keys(dados).length != 0){ 
			$("select[name=classe]").val("");
		 
			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});

			$("#adderror").append('<span style="color: red" id="spanapend">Já existe um cadastro do ' + dados[0].classe  + ' com o ' + dados[0].bimestre + ' bimestre no sistema.</span>')
		}else{
			$('#adderror #spanapend').remove();
		}
	});
}
 
