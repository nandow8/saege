var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
		$("select[name=aprovacao]").val(form_values.aprovacao);
		$("select[name=confirmaaprovacao]").val(form_values.confirmaaprovacao);
		$("select[name=iddepartamentosecretaria]").val(form_values.iddepartamentosecretaria);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=iddepartamentoescola]").val(form_values.iddepartamentoescola);
		$("select[name=status1]").val(form_values.status1);
		$("select[name=idfornecedor").val(form_values.idfornecedor);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}

	rules = {};
	rules.status1 = "required";	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
	}

	$("input[name=dataentrada]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataentrada]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$("input[name=dataemissao]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataemissao]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});	
});

function iteAddTr(item) {
	
	table = $(".itens table tbody#raiz");
	
	tr = $('<tr />');
	
	if (item===false) {
		item = {};
		item.id=0;
		item.idproduto = '';
		item.idfornecedor = '';
		item.quantidade = '';
		item.observacoes = '';
		item.status = '';			
	}
	
	select_produtos = $('<select style="width: 178px!Important" class="form-control" name="idsprodutos[]" />');
	select_produtos.append('<option value="">Selecione...</option>');
	for (var i=0; i<produtos.length; i++) {
		p = produtos[i];
		select_produtos.append('<option value="'+p.id+'">'+p.produto+'</option>');
	}
	select_produtos.val(item.idproduto);
	$(select_produtos).trigger("chosen:updated");
	td_produtos = $('<td />');
	td_produtos.append(select_produtos);
	
	tr.append('<td><input style="width:25px!important" type="text" maxlength="255" class="form-control" name="cods[]" /></td>');
	tr.append(td_produtos);
	tr.append('<td><input style="width:25px!important" type="text" maxlength="255" class="form-control" name="unids[]" /></td>');
	tr.append('<td><input style="width:25px!important" type="text" maxlength="255" class="form-control" name="marcas[]" /></td>');
	tr.append('<td><input style="width:25px!important" type="text" maxlength="255" class="form-control" name="valoresunitarios[]" /></td>');	
	tr.append('<td><input name="idsentradas[]" type="hidden" value="'+item.id+'" /><input style="width: 25px!Important" value="'+item.quantidade+'" type="text" maxlength="255" class="form-control" name="quantidades[]" /></td>');
	tr.append('<td><input style="width:25px!important" type="text" maxlength="255" class="form-control" name="valorestotais[]" /></td>');	
	tr.append('<td><input style="width:25px!important" type="text" maxlength="255" class="form-control" name="lotes[]" /></td>');	
	tr.append('<td><input style="width:25px!important" type="text" maxlength="255" class="form-control" name="fabricacoes[]" /></td>');	
	tr.append('<td><input style="width:25px!important" type="text" maxlength="255" class="form-control" name="validades[]" /></td>');	
	tr.append('<td><textarea style="width:25px!important" name="itensobservacoes[]" class="form-control">'+item.observacoes+'</textarea></td>');
	
	tr.append('<td style="text-align:center!Important" align="center"><a href="javascript:;" onclick="iteExcluirFromTable(this)"><i class="icon-remove"></i></a></a></td>');
	qtdMask(tr);
	table.append(tr);
}

function iteExcluirFromTable(obj) {
	$(obj).parent().parent().remove();
}

function qtdMask(tr) {
   $("input[name='quantidades[]']").priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',	    
	    centsLimit: 0    
	});	
}

function setOrigem(obj) {
	var val = $(obj).val();	
	if(val=="escola"){
		$(".box-escola").css("display", "block");
		$(".box-secretaria").css("display", "none");
		$("select[name=iddepartamentosecretaria]").closest('div').find('span').html('Selecione...');
		
	}else{
		$(".box-secretaria").css("display", "block");
		$(".box-escola").css("display", "none");	
		$("select[name=idescola]").closest('div').find('span').html('Selecione...');
		$("select[name=iddepartamentoescola]").closest('div').find('span').html('Selecione...');
	}
	
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
}

function setDepartamentosEscolas(obj, iddepartamentoescola){
	var val = $(obj).val();
	data = '';
	$("select[name=iddepartamentoescola]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasusuarios/setdepartamentosescolas/', {idescola:val}, function(data) {
		$("select[name=iddepartamentoescola]").html(data);
		$("select[name=iddepartamentoescola]").val(iddepartamentoescola);
		val = $("select[name=iddepartamentoescola]").find('option:selected').html();
		$("select[name=iddepartamentoescola]").closest('div').find('span').html(val);
	
		$("select[name=iddepartamentoescola]").trigger("chosen:updated");
		
		if(iddepartamentoescola==""){
			$("select[name=iddepartamentoescola]").closest('div').find('span').html('Selecione...');
		}
	});
}