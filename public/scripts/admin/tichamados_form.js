var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=destino]").val(form_values.destino);
		setTipo($("select[name=destino]").val(form_values.destino));
$("select[name=iddepartamento]").val(form_values.iddepartamento);
$("select[name=idescola]").val("21");
$("select[name=idresponsavel]").val(form_values.idresponsavel);
$("select[name=idprioridade]").val(form_values.idprioridade);
$("select[name=idstatus]").val(form_values.idstatus); 
$("select[name=pecaid]").val(form_values.pecaid);
$("select[name=idspatrimonios]").val(form_values.idspatrimonios);
//console.log('pat : ', form_values.idspatrimonios);
$("select[name=idtipoequipamento]").val(form_values.idtipoequipamento);
$("select[name=finalizacao]").val(form_values.finalizacao);
 
setVisualizaTipo($("select[name=finalizacao]").val(form_values.finalizacao));
$("select[name=atendido]").val(form_values.atendido);
$("select[name=substituicaopecas]").val(form_values.substituicaopecas); 
$("select[name=idtipo]").val(form_values.idtipo);
// if ((typeof form_values.idsfuncionarios)!='undefined')  $("[name='idsfuncionarios[]']").val(form_values.idsfuncionarios);
// if ((typeof form_values.idspatrimonios)!='undefined')  $("[name='idspatrimonios[]']").val(form_values.idspatrimonios);

setDadosPatrimonio($("select[name=idpatrimonio]").val('664'));
setSubstPecas($("select[name=substituicaopecas]").val(form_values.substituicaopecas));
$("select[name=status1]").val(form_values.status1);



		$("select").each(function(){
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}

	$("input[name=dataagendada]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataagendada]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date){
	    }
	});
	

	
	rules = {};
	rules.idspatrimonios = "required";
	rules.idtipoequipamento = "required";
	rules.sequencial = "required";
	rules.idescola = "required";
	rules.descricoes = "required";
	rules.idprioridade = "required";
	//rules.idstatus = "required";
	rules.dataagendada = "required";
	rules.atendido = "required"; 
	rules.idresponsavel = "required"; 
	rules.idtipo = "required"; 
	rules.defeitoconstatado = "required"; 
	rules.servicosexecutados = "required"; 
	rules.substituicaopecas = "required"; 
	rules.patrimoniosobservacoes = "required";
	//rules.pecaid = "required";
	rules.observacaosubstituicao = "required";


	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
			
		}
	});	
	
	
	 
	
});



function setTipo(obj){
	var destino = $(obj).val();

	if(destino=='Escola'){
		$('.box-departmento').css('display', 'none');
		$('.box-escola').css('display', 'block');
	}else {
		$('.box-departmento').css('display', 'block');
		$('.box-escola').css('display', 'none');
	}
	
}

function setVisualizaTipo(obj){
	var finalizacao = $(obj).val();

	if(finalizacao=='Sim'){
		$('.box-tipo-avaliacao').css('display', 'block');
	}else {
		$('.box-tipo-avaliacao').css('display', 'none');
	}
}

function setFinalizar(){
	jConfirm('Finalizar Chamado ?', 'Finalizar Chamado', function(r) {
		if (r) {
			$('input[name=finalizar]').val('Sim');
			$('select[finalizacao]').val('Sim');
			$( "#form" ).submit();
		}
 	});	

}

function setDadosPatrimonio(obj){
	console.log(obj);
	var idpatrimonio = $(obj).val();
	data = '';
	$.post(baseUrl + '/admin/patrimoniobaixabens/getdados/', {idpatrimonio:idpatrimonio}, function(data) {
		if(data=="erro"){
			$('.content-patrimonios').css('display', 'none');
			$(".box-patrimonios").html('');			
		}else{
			$('.content-patrimonios').css('display', 'block');
			$(".box-patrimonios").html(data);			
		}

	});
}



function setSubstPecas(obj) {
	var substituicaodepecas = $(obj).val(); 

	if (substituicaodepecas == null) {
		$('#tipos-pecas').css('display', 'none');
		$('#observavaodesubstituicao').css('display', 'none');
	}

	if (substituicaodepecas == 'Sim') {
		$('#tipos-pecas').css('display', 'block');
		$('#observavaodesubstituicao').css('display', 'block');
		$('[name=observacaosubstituicao]').val('');
	} 

	if (substituicaodepecas == 'Não') {
		$('#tipos-pecas').css('display', 'none');
		$('#observavaodesubstituicao').css('display', 'none');
		$('[name=observacaosubstituicao]').val('Nenhuma substituição de peça realizada!');
	} 
}

function atendimentofinalizacao(valor){
	
	if(valor == 'nao'){
		console.log('teste zumbi');

		$("#idtiposelect").val('');

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
		$('#tipo-avaliacao').css('display', 'none');
		//$('[name=observacoes]').attr('disabled',true);
		$("select[name=idtipo]").attr('disabled', true);
	}else if(valor == 'sim'){
		
		$('#tipo-avaliacao').css('display', 'block');
		$('[name=observacoes]').val("");

		$("select[name=idtipo]").attr('disabled',false);
		//$('[name=observacoes]').attr('disabled', false);	
	}//end if else
	
	

	
	
}//end function atendimentofinalizacao

function setandodados(){
		
	var idpatrimonio = form_values.idspatrimonios;
	
	data = '';
	$.post(baseUrl + '/admin/patrimoniobaixabens/getdados/', {idpatrimonio:idpatrimonio}, function(data) {
		if(data=="erro"){
			$('.content-patrimonios').css('display', 'none');
			$(".box-patrimonios").html('');			
		}else{
			$('.content-patrimonios').css('display', 'block');
			$(".box-patrimonios").html(data);			
		}

	});
}

// $('#btn-detalhes').click(function(){
// 	$('#detalhes').toggle(function(){

// 	}, 500)}
// );


$('#btn-detalhes').click(function(){
	

	$.ajax({
	url: baseUrl + '/admin/tichamados/detalhestichamados',
	type: 'POST',
	data: {
			idpatrimonio: form_values.idspatrimonios
			},
	beforeSend: function () {
			
	}}).done(function (data) {

		data = JSON.parse(data);
		$('#table-detalhes').html('');
		// Título da Tabela
		$('#table-detalhes').append('<tr><th>Atributo</th><th>Detalhes</th></tr>');
		// Conteúdo da Tabela
		$('#table-detalhes').append('<tr><td>Nome do Produto</td><td>' + data.patrimonio + '</td></tr>');
		$('#table-detalhes').append('<tr><td>Foto</td><td>--</td></tr>');
		$('#table-detalhes').append('<tr><td>Cor do Produto</td><td>' + data.cor + '</td></tr>');
		$('#table-detalhes').append('<tr><td>Marca</td><td>' + data.marca + '</td></tr>');
		$('#table-detalhes').append('<tr><td>Tipo</td><td>' + data.tipo + '</td></tr>');
		$('#table-detalhes').append('<tr><td>Modelo</td><td>' + data.modelo + '</td></tr>');
		$('#table-detalhes').append('<tr><td>Característica</td><td>' + data.descricoes + '</td></tr>');
		$('#table-detalhes').append('<tr><td>Dimensão</td><td>' + data.dimensoes + '</td></tr>');
		$('#table-detalhes').append('<tr><td>Descrição do Produto</td><td>' + data.descricoesdobem + '</td></tr>');

		var compra = data.datacompra.split('-');
		var dataCompra = new Date(compra[0], compra[1], compra[2]);

		$('#table-detalhes').append('<tr><td>Data da Compra</td><td>' + dataCompra.getDate() + ' / ' + dataCompra.getMonth() + ' / ' + dataCompra.getFullYear() + '</td></tr>');
		
		$('#detalhes').stop().toggle(
			$('#detalhes').animate({
				padding:"20px 0",
				opacity:.8
		  }, 500)
		);
		
	});

});