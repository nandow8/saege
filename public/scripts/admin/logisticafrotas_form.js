var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idfabricante]").val(form_values.idfabricante);
$("select[name=idtipo]").val(form_values.idtipo);
$("select[name=idmarca]").val(form_values.idmarca);
$("select[name=idmodelo]").val(form_values.idmodelo);
$("select[name=idproprietario]").val(form_values.idproprietario);
$("select[name=emprestimo]").val(form_values.emprestimo);
$("select[name=veiculopublico]").val(form_values.veiculopublico);
$("select[name=reservadepartamentos]").val(form_values.reservadepartamentos);
$("select[name=combustivel]").val(form_values.combustivel);
$("select[name=veiculodisponiveldepartamento]").val(form_values.veiculodisponiveldepartamento);
$("select[name=status1]").val(form_values.status1);

	setProprietario($("input[name=tipoveiculo]"));
	setEscolar($("input[name=escolar]"));
	setDeficiente($("input[name=deficiente]"));

		if ($("#veiculo_alugado").prop("checked")) {  //função setProprietario sempre retorna o valor do primeiro checkbox, mesmo que o segundo venha selecionado do banco. Dai fiz isso pra abrir o campo box-proprietario T-T
			$('.box-proprietario').show();
		}

		if ($("#deficiente_nao").prop("checked")) { //idem acima T-T
			$('.box-deficiente').hide();
		}

		if ($("#escolar_nao").prop("checked")) { //idem acima tambem T-T
			$('.box-escolar').hide();
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	



	$("input[name=vencimentovistoriaescolar]").mask("99/99/9999", {placeholder:" "});
	$("input[name=vencimentovistoriaescolar]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});


	$("input[name=vencimentovistoriadeficiente]").mask("99/99/9999", {placeholder:" "});
	$("input[name=vencimentovistoriadeficiente]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});




	$('input[name=anomodelo]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});














	$('input[name=anofabricacao]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});


	$('input[name=quantidade]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});



	
	
	rules = {};
	rules.idfabricante = "required";
	rules.idmarca = "required";
	rules.placa = "required";
	rules.veiculo = "required";
	rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setProprietario($obj)
{
	var value = $($obj).val();

	console.log(value)

	if($($obj).is(':checked') && value == 'alugado')
	{
		$('.box-proprietario').show();
		$('.box-patrimonio').hide();
	}
	else
	{
		$('.box-proprietario').hide();
		$('.box-patrimonio').show();
	}
}


function setEscolar($obj)
{
	var value = $($obj).val();
	console.log(value)

	if($($obj).is(':checked') && value == 'sim')
	{
		$('.box-escolar').show();
	}
	else
	{
		$('.box-escolar').hide();
	}
}

function setDeficiente($obj)
{
	var value = $($obj).val();

	if($($obj).is(':checked') && value == 'sim')
	{
		$('.box-deficiente').show();
	}
	else
	{
		$('.box-deficiente').hide();
	}
}