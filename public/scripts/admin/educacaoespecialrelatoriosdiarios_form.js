var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=status1]").val(form_values.status1);
		$("select[name=idaluno]").val(form_values.idaluno);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	




	
	
	rules = {};
	rules.data = "required";
rules.titulo = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setAlunosdados(obj, id){ 
	$("#box-alunos").css("display", "block");
	$('#resetfield').prop('disabled', false);
 
	if(id){
		val = id;
	}else{
		obj = $(obj); 
    	val = obj.val();
	} 
    
    var elemento; 
        $.post(baseUrl + '/admin/educacaoespecialrelatoriosdiarios/obterprofessoresdosalunos', {idaluno: val}, function(aluno){
			aluno = JSON.parse(aluno);   
				 elemento = '<ul class="list-group" style="width: 50%;">'    
				 				+'<li class="list-group-item"> Escola Regular: ' +  aluno.aee.escolaregular +'</li>'
								+'<li class="list-group-item"> Professor Regular: ' +  aluno.professor.nome +'</li>'
								+'<li class="list-group-item"> Escola Aee: ' +  aluno.escola +'</li>'
								+'<li class="list-group-item"> Professor Aee: ' +  aluno.professorAee.nome +'</li>'
								+'<input type="hidden" value="'+ aluno.aee.idescola +'" name="idescola">'
							+'</ul>';
 
                 $('#box-alunos').html(elemento); 
        }); 
 } 

 function membroAddTr(_values) {
    
	html = "";
	
	if (_values===false) {
		_values = {};
	} else {
		_values = jQuery.parseJSON(_values);
		itens = _values;
	}
	
	modelo = $("#modelo-apm").html();
	modelo = '<tr>' + modelo + '</tr>';
	modelo = $(modelo);
	table = $("table.table-membros");
	membroiner = table.find('tbody');
	
	modelo.find('p').html(html);
	modelo.find('input,select,textarea').each(function() {
		name = $(this).attr('name');
		name = name.replace("_", "");
		if(_values && _values.var_db == true)name = name.replace("[]", "");
		
    
		for (var property in _values) {
			if (property==name) {
				
				$(this).attr('name', name + '[]');
				if(_values.var_db){
				}
				
				$(this).attr('value', _values[property]);
				
				if ($(this).is("select")) {
					$(this).parent().find('a').remove();
				}
			}
		}
	});
	membroiner.append(modelo);
	
	$('.numbervalue').priceFormat({
		prefix: '',
		limit: 3,
        centsSeparator: '',
        thousandsSeparator: ''	    
	});

	$('.numbervalue').blur(function(){
		if($(this).val() <= 0){
			$(this).val('');	
		}
			
	});
}

function membrosExcluirFromTable(obj) {
	jConfirm("Deseja excluir o registro?", 'Confirmar', function(r) {
		
            if(r)
            {
		obj = $(obj);
		content = obj.closest('tr');
		content.find('input').remove();
		content.hide();
            }
		
	});
}