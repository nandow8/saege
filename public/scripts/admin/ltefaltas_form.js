jQuery(document).ready(function() {
    if ((typeof form_values)!='undefined') {

        if ((typeof form_values.status)!='undefined') $("select[name=status]").val(form_values.status);
        if ((typeof form_values.idestudante)!='undefined') $("select[name=idestudante]").val(form_values.idestudante);
        if ((typeof form_values.idperiodo)!='undefined') $("select[name=idperiodo]").val(form_values.idperiodo);


        $("select").trigger("chosen:updated");
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
    }

    $("#idperiodo").change(function() {
        diasletivos = $(this).find(':selected').data('diasletivos');
        $("#diasletivos").val(diasletivos);
    });

    $("#faltas").focusout(function() {
        diasletivos = $("#diasletivos").val();
        faltas = $(this).val();
        if (faltas > diasletivos) {
            jAlert("Número de faltas excede os dias letivos");
            $(this).val(diasletivos);
            $(this).focus();
        }
    })
});