var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=origem]").val(form_values.origem);
		$("select[name=localsolicitante]").val(form_values.localsolicitante);
		$("select[name=funcionario]").val(form_values.funcionario);
		$("select[name=falarcom]").val(form_values.falarcom);
		$("select[name=assunto]").val(form_values.assunto);
		$("select[name=dataefetuada]").val(form_values.dataefetuada);
		$("select[name=horaefetuada]").val(form_values.horaefetuada);
		$("select[name=telefone]").val(form_values.telefone);
$("select[name=status1]").val(form_values.status1);
setDados($("select[name=idusuario]").val(form_values.idusuario));

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	


	$("input[name=telefone]").mask("(99)9?9999-9999", {placeholder:" "});

	$("input[name=horaefetuada]").mask("99:99", { placeholder: " " });
	$("input[name=dataefetuada]").mask("99/99/9999", { placeholder: " " });
	$("input[name=dataefetuada]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function (date) {
		}
	});
	
	
	rules = {};
	rules.idusuario = "required";
//rules.idfuncionario = "required";
rules.telefone = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setDados(obj)
{
	var idusuario = $(obj).val();

	if(idusuario > 0)
	{
		$("#departamento").val('');
		$.post(baseUrl + "/" + module + "/usuarios/getusuario/", {idusuario:idusuario}, function(data) {
			data = JSON.parse(data.trim());

			if(data.status == "OK")
			{
				$('#box-info').show();
				$("#departamento").val(data.response.departamento);
			}
			else
			{
				$('#box-info').hide();
				jAlert(data.response, 'Alerta!');
			}
		});
	}
}