var unsaved = false;

$("input,select,textarea").change(function() { //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function() { //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
    window.onbeforeunload = function() {
        console.log(unsaved);
        if (unsaved) {
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };

    if ((typeof form_values) != 'undefined') {
        $("select[name=idsecretaria]").val(form_values.idsecretaria);
        $("select[name=idescola]").val(form_values.idescola);
        $("select[name=idsala]").val(form_values.idsala);
        $("select[name=idserie]").val(form_values.idserie);
        $("select[name=idturma]").val(form_values.idturma);
        $("select[name=idperiodo]").val(form_values.idperiodo);
        //$("select[name=idprofessor]").val(form_values.idprofessor);
        $("select[name=idtipoensino]").val(form_values.idtipoensino);
        $("select[name=turno]").val(form_values.turno);
        $("select[name=integracao]").val(form_values.integracao);
        $("select[name=status1]").val(form_values.status1);
        $("select[name=idtipodeclasse]").val(form_values.idtipodeclasse);
        $("select[name=idssedatividadescomplementares]").val(form_values.idssedatividadescomplementares);

        if ((typeof form_values.idescola) != 'undefined') {
            jQuery("select[name=idescola]").val(form_values.idescola);
            setSalas(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idsala);
            setSeries(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idserie);
            setTurmas(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idturma);
            setPeriodos(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idperiodo);
            //setFuncionariosEscolas(jQuery("select[name=idescola]").val(form_values.idescola), form_values.idprofessor, form_values.idsmaterias);
        }

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function() {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

    }

    $("input[name=ano]").mask("9999", {placeholder: " "});
    $("input[name=horarioinicio]").mask("99:99", {placeholder: " "});
    $("input[name=horariofim]").mask("99:99", {placeholder: " "});

    $("input[name=inicioaulas]").mask("99/99/9999", {placeholder: " "});
    $("input[name=inicioaulas]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });
    $("input[name=terminoaulas]").mask("99/99/9999", {placeholder: " "});
    $("input[name=terminoaulas]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });

    $('input[name=ano]').priceFormat({
        prefix: '',
        centsSeparator: '',
        thousandsSeparator: '',
        centsLimit: 0
    });

    rules = {};
    rules.idescola = "required";
    rules.idsala = "required";
    rules.idserie = "required";
    rules.idturma = "required";
    rules.idperiodo = "required";
    rules.idtipoensino = "required";
    rules.inicioaulas = "required";
    rules.terminoaulas = "required";
    rules.status1 = "required";
    rules.ano = "required";
    rules.turno = "required";
    rules.status1 = "required";
    rules.idtipodeclasse = "required";

    messages = {};
    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function(form) {
            unsaved = false;
            $.blockUI({message: '<h1> Aguarde...</h1>'});
            form.submit();
        }
    });
});
$(window).load(function() {
    if (visualizar) {
        setTimeout(function() {
            $("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
                val = $(this).val();
                val = $.trim(val);
                val = (val === '') ? '--' : val;
                parent = $(this).parent();
                parent.append(val);
                parent.addClass('visualizar');

                $(this).remove();
            });
            $("label span.required").remove();

            $("div.control-group select").not('[type=hidden]').each(function() {
                val = $(this).find('option:selected').html();
                val = $.trim(val);
                val = (val === '') ? '--' : val;
                parent = $(this).parent();
                parent.html(val);
                parent.addClass('visualizar');

            });
        }, 1000);
    }
});

function setDados(obj, idescola) {
    var val = $(obj).val();

    setSalas($("select[name=idescola]").val(val), 0);
    setSeries($("select[name=idescola]").val(val), 0);
    setTurmas($("select[name=idescola]").val(val), 0);
    setPeriodos($("select[name=idescola]").val(val), 0);
    setFuncionariosEscolas($("select[name=idescola]").val(val), 0, 0);
    //setMaterias(jQuery("select[name=idprofessor]").val(form_values.idprofessor), 0);
}

function setSalas(obj, idsala) {
    var val = $(obj).val();

    data = '';
    $("select[name=idsala]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasvinculos/setsalas/', {idescola: val}, function(data) {
        $("select[name=idsala]").html(data);
        $("select[name=idsala]").val(idsala);
        val = $("select[name=idsala]").find('option:selected').html();
        $("select[name=idsala]").closest('div').find('span').html(val);

        $("select[name=idsala]").trigger("chosen:updated");

        if (idsala == "") {
            $("select[name=idsala]").closest('div').find('span').html('Selecione...');
        }
    });
}

function setSeries(obj, idserie) {

    var val = $(obj).val();

    data = '';
    $("select[name=idserie]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasvinculos/setseries/', {idescola: val}, function(data) {
        $("select[name=idserie]").html(data);
        $("select[name=idserie]").val(idserie);
        val = $("select[name=idserie]").find('option:selected').html();
        $("select[name=idserie]").closest('div').find('span').html(val);

        $("select[name=idserie]").trigger("chosen:updated");

        $("input[name=idtipoensino]").val($.trim($("#idserie").children("option:selected").text()).split(".")[0]);

        if (idserie == "") {
            $("select[name=idserie]").closest('div').find('span').html('Selecione...');
        }
    });
}

function setTurmas(obj, idturma) {
    var val = $(obj).val();

    data = '';
    $("select[name=idturma]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasvinculos/setturmas/', {idescola: val}, function(data) {
        $("select[name=idturma]").html(data);
        $("select[name=idturma]").val(idturma);
        val = $("select[name=idturma]").find('option:selected').html();
        $("select[name=idturma]").closest('div').find('span').html(val);

        $("select[name=idturma]").trigger("chosen:updated");

        if (idturma == "") {
            $("select[name=idturma]").closest('div').find('span').html('Selecione...');
        }
    });
}

function setPeriodos(obj, idperiodo) {
    var val = $(obj).val();

    data = '';
    $("select[name=idperiodo]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasvinculos/setperiodos/', {idescola: val}, function(data) {
        $("select[name=idperiodo]").html(data);
        $("select[name=idperiodo]").val(idperiodo);
        val = $("select[name=idperiodo]").find('option:selected').html();
        $("select[name=idperiodo]").closest('div').find('span').html(val);

        $("select[name=idperiodo]").trigger("chosen:updated");

        if (idperiodo == "") {
            $("select[name=idperiodo]").closest('div').find('span').html('Selecione...');
        }
    });
}


function setFuncionariosEscolas(obj, idprofessor, idsmaterias) {
    var val = $(obj).val();

    data = '';
    $("select[name=idprofessor]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasvinculos/setfuncionariosescolas/', {idescola: val}, function(data) {
        $("select[name=idprofessor]").html(data);
        $("select[name=idprofessor]").val(idprofessor);
        val = $("select[name=idprofessor]").find('option:selected').html();
        $("select[name=idprofessor]").closest('div').find('span').html(val);

        $("select[name=idprofessor]").trigger("chosen:updated");

        if (idprofessor == "") {
            $("select[name=idprofessor]").closest('div').find('span').html('Selecione...');
        }

        setMaterias(jQuery("select[name=idprofessor]").val(idprofessor), idsmaterias);
    });
}

function setMaterias(obj, idmateria) {
    var val = $(obj).val();

    data = '';
    $.post(baseUrl + '/admin/escolasvinculos/setmaterias/', {idprofessor: val}, function(data) {
        $("#idmateria").html(data);
        if ((idmateria != "") && (idmateria != "0")) {
            $("[name='idsmaterias[]']").val(idmateria)
        }
    });
}

function AtividadeAddTr(_values) {

    html = "";

    if (_values === false) {
        _values = {};
    } else if (_values === undefined) {
        _values = {
            "idv": $('#ing').val(),
            "codigo": $("#ing option:selected").html(),
            //"tipo":$("#ing option:selected").html(),
            "var_db": true
        };
    } else {
        _values = jQuery.parseJSON(_values);
        itens = _values;
    }

    modelo = $("#modelo-atividade").html();
    modelo = '<tr>' + modelo + '</tr>';
    modelo = $(modelo);
    table = $("table.table-Atividades");
    atividadeinner = table.find('tbody');

    modelo.find('p').html(html);
    modelo.find('input,select,textarea,span').each(function() {
        name = $(this).attr('name');
        name = name.replace("_", "");
        if (_values && _values.var_db == true)
            name = name.replace("[]", "");

        for (var property in _values) {
            if (property == name) {

                $(this).attr('name', name + '[]');
                if (_values.var_db) {
                }
                console.log(_values[property])
                $(this).attr('value', _values[property]);

                $(this).html(_values[property]);

                if ($(this).is("select")) {
                    $(this).parent().find('a').remove();
                }

            }
        }
    });

    atividadeinner.append(modelo);

    $('.numbervalue').priceFormat({
        prefix: '',
        limit: 3,
        centsSeparator: '',
        thousandsSeparator: ''
    });

    $('.numbervalue').blur(function() {
        if ($(this).val() <= 0) {
            $(this).val('');
        }

    });

    calcTotLinhas();
}

function AtividadesExcluirFromTable(obj) {
    jConfirm("Deseja excluir o registro?", 'Confirmar', function(r) {

        if (r)
        {
            obj = $(obj);
            content = obj.closest('td');
            content.find('input').remove();
            content.hide();
            calcTotLinhas();
        }

    });
}

function setBtnAddAtividade(obj) {
    let val = $(obj).val();

    document.getElementById('BtnAddAtividade').disabled = (val === "");
}

function calcTotLinhas() {

    $("#subtotQtd").html((document.getElementsByName('idv[]').length - 1).toString());

    let str = '';

    for (var i = 0; i < document.getElementsByName('idv[]').length; i++)
    {
        str += document.getElementsByName('idv[]')[i].value;
        str += (i < document.getElementsByName('idv[]').length - 2) ? "," : "";

    }
    // ajusta Array para não incluir duas vezes repetidos.
    let x = str.split(',');
    x.sort();
    let z = [...new Set(x)];

    str = z.join(',');
    document.getElementById('idssedatividadescomplementares').value = (str);

}
