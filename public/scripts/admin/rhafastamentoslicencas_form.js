var unsaved = false;

$("input,select,textarea").change(function () { //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function () { //trigers change in all input fields including text type
    unsaved = true;
});


$(document).ready(function () {
    window.onbeforeunload = function () {
        console.log(unsaved);
        if (unsaved) {
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };

    if ((typeof form_values) != 'undefined') {

        $("select[name=idsecretaria]").val(form_values.idsecretaria);
        $("select[name=origem]").val(form_values.origem);
        $("select[name=idfuncionariosecretaria]").val(form_values.idfuncionariosecretaria);
        $("select[name=idescola]").val(form_values.idescola);
        $("select[name=idfuncionarioescola]").val(form_values.idfuncionarioescola);
        $("select[name=aprovacao]").val(form_values.aprovacao);
        $("select[name=confirmacaoaprovacao]").val(form_values.confirmacaoaprovacao);
        $("select[name=status1]").val(form_values.status1);
        setArquivo($("select[name=aprovacao]").val(form_values.aprovacao));

        $("select").each(function () {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function () {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

    }

    $('input[name=dias]').priceFormat({
        prefix: '',
        centsSeparator: '',
        thousandsSeparator: '',
        centsLimit: 0
    });
    
    $("input[name=datainicio]").mask("99/99/9999", {placeholder: " "});

    $("input[name=datainicio]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onClose: function () {
            var er = /^(((0[1-9]|[12][0-9]|3[01])([-.\/])(0[13578]|10|12)([-.\/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-.\/])(0[469]|11)([-.\/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-.\/])(02)([-.\/])(\d{4}))|((29)(\.|-|\/)(02)([-.\/])([02468][048]00))|((29)([-.\/])(02)([-.\/])([13579][26]00))|((29)([-.\/])(02)([-.\/])([0-9][0-9][0][48]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][2468][048]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][13579][26])))$/;
            
            if (!er.test(this.value)) {
                jAlert('A data informada, "' + this.value + '" é inválida.', 'Atenção' );
                this.value = "";
            }else{
                setDatafim(this);
            }
        }
    });


    $("input[name=datafim]").mask("99/99/9999", {placeholder: " "});

    $("input[name=datafim]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onClose: function () {
            var er = /^(((0[1-9]|[12][0-9]|3[01])([-.\/])(0[13578]|10|12)([-.\/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-.\/])(0[469]|11)([-.\/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-.\/])(02)([-.\/])(\d{4}))|((29)(\.|-|\/)(02)([-.\/])([02468][048]00))|((29)([-.\/])(02)([-.\/])([13579][26]00))|((29)([-.\/])(02)([-.\/])([0-9][0-9][0][48]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][2468][048]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][13579][26])))$/;
            
            if (!er.test(this.value)) {
                jAlert('A data informada, "' + this.value + '" é inválida.', 'Atenção' );
                this.value = "";
            }else{
                setDatafim(this);
            }
        }
    });


    rules = {};
    rules.idarquivo = "required";
    rules.motivo = "required";
    rules.progresso = "required";
    rules.idfuncionarioafastadolicenca = "required";
    rules.datainicio = "required";
    rules.datafim = "required";
    rules.observacao = "required";


    messages = {};

    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function (form) {
            unsaved = false;
            $.blockUI({message: '<h1> Aguarde...</h1>'});
            form.submit();
        }
    });

    //visualizar = false;
    if (visualizar) {
        $("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function () {
            val = $(this).val();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.append(val);
            parent.addClass('visualizar');

            $(this).remove();
        });
        $("div.control-group select").not('[type=hidden]').each(function () {
            val = $(this).find('option:selected').html();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.html(val);
            parent.addClass('visualizar');

        });

        $("label span.required").remove();

    }

});

function setOrigem(obj) {

    var val = $(obj).val();
    if (val == "Escola") {
        //$('#calendar').html('');
        $(".box-escola").css("display", "block");
        $(".box-secretaria").css("display", "none");
        //$("select[name=iddepartamentosecretaria]").closest('div').find('span').html('Selecione...');

    } else {
        //setPage('inicial');
        $(".box-secretaria").css("display", "block");
        $(".box-escola").css("display", "none");
        //$("select[name=idescola]").closest('div').find('span').html('Selecione...');
        //$("select[name=idfuncionarioescola]").closest('div').find('span').html('Selecione...');
    }

    //$(".calendar_ferias").css("display", "block");



    /*
     $("select").each(function (){
     div = $(this).closest('div').find('.chosen-container').css('width', '100%');
     });
     */
}

function setArquivo(obj) {
    var aprovacao = $(obj).val();
    if (aprovacao == "Sim") {
        $(".box-arquivo").css("display", "block");
    } else {
        $(".box-arquivo").css("display", "none");
    }
}

function setDados(obj) {
    var $id = $(obj).val();
    $.post(baseUrl + "/" + module + "/" + controller + "/getdados", {id: $id}, function (data) {
        myobj = JSON.parse(data);
        $('input[name=telefone]').val(myobj.telefone);
        $("select[name=status1]").val(myobj.status);

        $("select").each(function () {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function () {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

        $("input[name=telefone]").mask("(99)9?9999-9999", {placeholder: " "});
    });
}

function setDatafim(Obj, nDias = 7) {

    Date.prototype.addHoras = function (horas) {
        this.setHours(this.getHours() + horas);
    };
    Date.prototype.addMinutos = function (minutos) {
        this.setMinutes(this.getMinutes() + minutos);
    };
    Date.prototype.addSegundos = function (segundos) {
        this.setSeconds(this.getSeconds() + segundos);
    };
    Date.prototype.addDias = function (dias) {
        this.setDate(this.getDate() + dias);
    };
    Date.prototype.addMeses = function (meses) {
        this.setMonth(this.getMonth() + meses);
    };
    Date.prototype.addAnos = function (anos) {
        this.setYear(this.getFullYear() + anos);
    };

    /*
     * Exemplos de utilização dos protótipos.
     * 
     //Criando uma data sem parâmetros (tempo atual)
     var dt = new Date();
     //Exemplo adicionando 1 hora na sua data
     dt.addHora(1);
     //Exemplo adicionando 30 minutos na sua data
     dt.addMinutos(30);
     //Exemplo adicionando 15 segundos na sua data
     dt.addSegundos(15);
     //Exemplo adicionando 10 dias na sua data
     dt.addDias(10);
     //Exemplo adicionando 2 meses na sua data:
     dt.addMeses(2);
     //Exemplo adicionando 1 ano na sua data:
     dt.addAnos(1);
     //Imprimindo ela no console
     console.log(dt);
     */
    if ($("input[name=datafim]").val() == '') {
        let mObj = $("input[name=datainicio]").val();

        let d = parseInt(mObj.substr(0, 2));
        let m = parseInt(mObj.substr(3, 2));
        let a = parseInt(mObj.substr(6, 4));

        let dataS = new Date(a, m, d);

        dataS.addDias(nDias);
        //        
        let dia = dataS.getDate().toString().length < 2 ? '0' + dataS.getDate() : dataS.getDate();
        let mes = dataS.getMonth().toString().length < 2 ? '0' + dataS.getMonth() : dataS.getMonth();

        if (!isNaN(dia) || !isNaN(mes)) {
            $("input[name=datafim]").val(dia + '/' + mes + '/' + dataS.getFullYear());
        }
    }
    if ($("input[name=datainicio]").val() != '' && $("input[name=datafim]").val() != '') {
        setDias();
}
}

function setDias() {
    let mObj1 = $("input[name=datainicio]").val();
    let mObj2 = $("input[name=datafim]").val();

    let d1 = parseInt(mObj1.substr(0, 2));
    let m1 = parseInt(mObj1.substr(3, 2));
    let a1 = parseInt(mObj1.substr(6, 4));
    let d2 = parseInt(mObj2.substr(0, 2));
    let m2 = parseInt(mObj2.substr(3, 2));
    let a2 = parseInt(mObj2.substr(6, 4));

    let dataS1 = new Date(a1, m1, d1);
    let dataS2 = new Date(a2, m2, d2);

    if (dataS2 < dataS1) {
        jAlert('A data final não pode ser inferior a data inicial. ');
        $("input[name=datafim]").val($("input[name=datainicio]").val());
        $("input[name=dias").val(0);
        $("input[name=vizdias").val(0);
    } else {
        let diff = parseInt((dataS2 - dataS1) / 86400000);
        $("input[name=dias").val(diff);
        $("input[name=vizdias").val(diff);
    }
}