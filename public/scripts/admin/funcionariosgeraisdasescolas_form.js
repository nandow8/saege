var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
$("select[name=tipo]").val(form_values.tipo);
$("select[name=e_idenderecoidestado]").val(form_values.e_idenderecoidestado);$("select[name=iddepartamentosecretaria]").val(form_values.iddepartamentosecretaria);
$("select[name=cnh]").val(form_values.cnh);
$("select[name=estrangeiro]").val(form_values.estrangeiro);
$("select[name=grauinstrucao]").val(form_values.grauinstrucao);
$("select[name=estadocivil]").val(form_values.estadocivil);
$("select[name=filhos]").val(form_values.filhos);
$("select[name=regime]").val(form_values.regime);
$("select[name=bolsista]").val(form_values.bolsista);
$("select[name=iddepartamentoescola]").val(form_values.iddepartamentoescola);
$("select[name=status1]").val(form_values.status1);
$("select[name=pertenceescola]").val(form_values.pertenceescola);
		
			$("select[name=idescola]").val(form_values.idescola);
			$("select[name=recebesolicitacoes]").val(form_values.recebesolicitacoes);
			$("select[name=idprefeitura]").val(form_values.idprefeitura);
			$("select[name=enviaemails]").val(form_values.enviaemails);
			if ((typeof form_values.idescolas)!='undefined') $("[name='idescolas[]']").val(form_values.idescolas);
			//setDepartamentos($("select[name=idescola]").val(form_values.idescola), form_values.iddepartamentoescola);
			if ((typeof form_values.idsdepartamentos)!='undefined') $("[name='idsdepartamentos[]']").val(form_values.idsdepartamentos);
			if ((typeof form_values.professor)!='undefined') setCamposUser($("select[name=professor]").val(form_values.professor), form_values.professor);
			setPerfil($("select[name=idescola]").val(form_values.idescola), form_values.idperfil);
			//setCargos($("select[name=iddepartamentoescola]").val(form_values.iddepartamentoescola), form_values.idcargo);
			//setFuncoes($("select[name=idcargo]").val(form_values.idcargo), form_values.idfuncao);
			$("select[name=idcargo]").val(form_values.idcargo);
			$("select[name=idfuncao]").val(form_values.idfuncao);
			$("select[name=idperfil]").val(form_values.idperfil);
		

		//if ((typeof form_values.idescola)!='undefined') setDepartamentos($("select[name=idescola]").val(form_values.idescola), form_values.iddepartamentoescola, form_values.idescolausuarioresponsavel);
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	

	$('input[name=idendereco]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});

	
	
	rules = {};
	rules.rgf = "required";
	//rules.tipo = "required";
	rules.nome = "required";
	//rules.idescola = "required";
	rules.professor = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setOrigem(obj) {
	var val = $(obj).val();	
	if(val=="escola"){
		$(".box-escola").css("display", "block");
		$(".box-secretaria").css("display", "none");
		$("select[name=iddepartamentosecretaria]").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select[name=iddepartamentosecretaria]").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
		//$("select[name=iddepartamentosecretaria]").closest('div').find('span').html('Selecione...');
		
	}else{
		$(".box-secretaria").css("display", "block");
		$(".box-escola").css("display", "none");	
		$("select[name=idescola]").closest('div').find('span').html('Selecione...');
		$("select[name=iddepartamentoescola]").closest('div').find('span').html('Selecione...');
	}
	
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
}

function setDepartamentos(obj, iddepartamentoescola){
	var idescola = $(obj).val();
	data = '';
	$("select[name=iddepartamentoescola]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/funcionariosgeraisescolas/setdepartamentosescolas/', {idescola:idescola}, function(data) {
		$("select[name=iddepartamentoescola]").html(data);

		$("select[name=iddepartamentoescola]").val(iddepartamentoescola);
		val = $("select[name=iddepartamentoescola]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(iddepartamentoescola==""){
			$("select[name=iddepartamentoescola]").closest('div').find('span').html('Selecione...');
		}
	});
	if(iddepartamentoescola==""){
		//setPerfil($(obj).val(), form_values.idperfil);
	}
	setPerfil($(obj), 0);
}

function setFuncoes(obj, idfuncao){
	var idcargo = $(obj).val();
	data = '';
	$("select[name=idfuncao]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/funcionariosgeraisescolas/setfuncoesescolas/', {idcargo:idcargo}, function(data) {
		$("select[name=idfuncao]").html(data);

		$("select[name=idfuncao]").val(idfuncao);
		val = $("select[name=idfuncao]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idfuncao==""){
			$("select[name=idfuncao]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setCargos(obj, idcargo){
	var iddepartamentoescola = $(obj).val();
	data = '';
	$("select[name=idcargo]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/funcionariosgeraisescolas/setcargosescolas/', {iddepartamentoescola:iddepartamentoescola}, function(data) {
		$("select[name=idcargo]").html(data);

		$("select[name=idcargo]").val(idcargo);
		val = $("select[name=idcargo]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idcargo==""){
			$("select[name=idcargo]").closest('div').find('span').html('Selecione...');
		}
	});
}


function setCamposUser(obj, professor){	
	/*rules = {};
	rules.nomerazao = "required";
	rules.sobrenomefantasia = "required";
	rules.rgf = "required";
	rules.professor = "required";
	rules.email = {required: true,email: true};
	rules.senha = {minlength: 6};
	rules.resenha = {
		  equalTo: "[name=senha]"
	};	
	*/
	var val = $(obj).val();

	if(val=="Sim"){
		$('.campos_professores').css('display', 'block');
		$('.campos_nao_professores').css('display', 'none');
/*
		rules = {};
		rules.nomerazao = "required";
		rules.sobrenomefantasia = "required";
		rules.rgf = "required";
		rules.professor = "required";
		rules.email = {required: true,email: true};
		rules.senha = {minlength: 6};
		rules.resenha = {
			  equalTo: "[name=senha]"
		};	*/
		
	}else {
		$('.campos_professores').css('display', 'none');
		$('.campos_nao_professores').css('display', 'block');
/*
		rules.idperfil = "required";
		rules.iddepartamento = "required";
		rules.idcargo = "required";
		rules.idescola = "required"; */
	}
	/*rules.status1 = "required";
	if (action=='adicionar') {
		rules.senha = "required";
	}*/
	/*
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				//$(element).parent().find('div').append(error);
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
			
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});	
	*/
	if(professor){
		$("select[name=professor]").val(professor);
	}
}

function setPerfil(obj, idperfil){
	var val = $(obj).val();
	data = '';
	$("select[name=idperfil]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasusuarios/setperfil/', {idescola:val}, function(data) {
		$("select[name=idperfil]").html(data);
		$("select[name=idperfil]").val(idperfil);
		val = $("select[name=idperfil]").find('option:selected').html();
		$("select[name=idperfil]").closest('div').find('span').html(val);
	
		$("select[name=idperfil]").trigger("chosen:updated");
		
		if(idperfil==""){
			$("select[name=idperfil]").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
			
			$("select[name=idperfil]").each(function (){
				div = $(this).closest('div').find('.chosen-container').css('width', '100%');
			});
			//$("select[name=idperfil]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setCampos(obj, tipo, value){
	var val = $(obj).val();	
	if((tipo=="cnh") && (val=="Sim")){
		$(".box-cnh").css("display", "block");
	}else if((tipo=="filhos") && (val=="Sim")){
		$(".box-filhos").css("display", "block");
	}else if((tipo=="estrangeiro") && (val=="Sim")){
		$(".box-estrangeiro").css("display", "block");
	}else if((tipo=="bolsista") && (val=="Sim")){
		$(".box-bolsista").css("display", "block");
	}else if((tipo=="cnh") && (val!="Sim")){
		$(".box-cnh").css("display", "none");
	}else if((tipo=="filhos") && (val!="Sim")){
		$(".box-filhos").css("display", "none");
	}else if((tipo=="estrangeiro") && (val!="Sim")){
		$(".box-estrangeiro").css("display", "none");
	}else if((tipo=="bolsista") && (val!="Sim")){
		$(".box-bolsista").css("display", "none");
	}else if((tipo=="regime") && (val=="outros"))
		$(".box-regime").css("display", "block");
}