$(document).ready(function () {
    $('input[name="inRA"]').mask('99999999');
    //$('input[name="inDigitoRA"]').mask('9');

    function consultarFicha(inRA, inDigitoRA, inUF){
       $("#loader").fadeIn();
        data = '';

        $.ajax({
            type: "POST",
            url: baseUrl + "/" + module + "/" + controller + "/consultafichaaluno/",
            dataType: "JSON",
            data: {inRA: inRA, inDigitoRA: inDigitoRA, inUF: inUF},
            success: function(data){
                $("#loader").fadeOut('fast', function(){
                    if(data.outSucesso && data.outSucesso=="OK"){
                        $(".boxretorno_error").fadeOut();
                        $("#msg_error").html('');
                        dados = data.FichasAluno.FichaAluno;
                        $("#outRA").html(dados.outRA + '-' + dados.outDigitoRA + '/' + dados.outUF);
                        $("#outNome").html(dados.outNomeAluno);
                        $("#outNascimento").html(dados.outDataNascimento);
                        $("#outMae").html(dados.outNomeMae);
                        $("#outPai").html(dados.outNomePai);
                        $("#outRG").html(dados.outNumRG + ' - ' + dados.outDigitoRG);
                        $("#infoResult").show();
                        $("#btn-associar").show();
        
                        //todos os dados do aluno
                        dadosCompletos = '';
                        dadosCompletos += '<label class="title-extra-info">Dados pessoais</label>';
                        dadosCompletos += '<div class="edge-info"><label class="content-extra-info"><b>Nome completo: </b></label><label class="content-extra-info">'+dados.outNomeAluno+'</label></div>';
                        dadosCompletos += '<div class="edge-info"><label class="content-extra-info"><b>RA: </b></label><label class="content-extra-info">'+(dados.outRA + '-' + dados.outDigitoRA + '/' + dados.outUF)+'</label></div>';
                        dadosCompletos += '<div class="edge-info"><label class="content-extra-info"><b>Nascimento: </b></label><label class="content-extra-info">'+dados.outDataNascimento+'</label></div>';
                        dadosCompletos += '<div class="edge-info"><label class="content-extra-info"><b>Mãe: </b></label><label class="content-extra-info">'+dados.outNomeMae+'</label></div>';
                        dadosCompletos += '<div class="edge-info"><label class="content-extra-info"><b>Pai: </b></label><label class="content-extra-info">'+dados.outNomePai+'</label></div>';
                        dadosCompletos += '<label class="title-extra-info">Endereço</label>';
                        dadosCompletos += '<div class="edge-info"><label class="content-extra-info"><b>Endereço: </b></label><label class="content-extra-info">'+dados.outEndereco+'</label></div>';
                        dadosCompletos += '<div class="edge-info"><label class="content-extra-info"><b>Bairro: </b></label><label class="content-extra-info">'+dados.outBairro+'</label></div>';
                        dadosCompletos += '<div class="edge-info"><label class="content-extra-info"><b>Cidade: </b></label><label class="content-extra-info">'+dados.outCidade+'</label></div>';
                        dadosCompletos += '<div class="edge-info"><label class="content-extra-info"><b>Pais de origem: </b></label><label class="content-extra-info">'+dados.outNomePaisOrigem+'</label></div>';
                        
                    $("#extraDados").html(dadosCompletos);
        
                    }else if(data.outErro){                        
                        $("#infoResult").hide();
                        $("#btn-associar").hide();
    
                        $(".boxretorno_error").fadeIn();
                        $("#msg_error").html("Aluno não encontrado. Verifique o RA do aluno");
                    }
                });
            }, 
            error: function(XMLHttpRequest, textStatus, errorThrown){
                $("#infoResult").hide();
                $("#btn-associar").hide();
                $("#loader").fadeOut();
                $(".boxretorno_error").fadeIn();
                $("#msg_error").html("Falha na requisição. Tente novamente ");
            }
        });
    }
    $('#visualizardados').on("click", function(){
        
        var dialog = $('#extraDados').dialog({
            width: 400,
            modal: true,
            buttons: {
                "Fechar": function() {
                    dialog.dialog('close');
                }
            }
        });
    });
    function showErro(msg, obj){
        if(msg.length > 1){
            $(obj).html(msg);
            $(obj).parent().fadeIn();
        }
        return error = true;
    }

    $('#btn-associar').on('click', function(ev){
        ev.preventDefault();
        $("#loader").fadeIn();
        
        alunoRA = $('input[name=ra]').val();
        digra = $('input[name=digra]').val();
        ufra = $('input[name=ufra]').val();
        inIrmaoRA = $('input[name=inRA]').val();
        inIrmaoDigitoRA = $('input[name=inDigitoRA]').val();
        inIrmaoUFRA = $('select[name=inUF]').val();
        inNome = $("#outNome").text();
        inMae = $("#outMae").text();
        inPai = $("#outPai").text();
        inNascimento = $("#outNascimento").text();
       
        inGemeo = $("#egemeo").is(":checked");
        inGemeo = (inGemeo) ? "S" : "N";
        
        error = false;
        
        if(alunoRA.length == '' || alunoRA.length<8) showErro('Por favor informe um RA válido', '#msg_error'); 
        if(digra.length == '') showErro('Por favor informe o dígito do RA do aluno', '#msg_error');
        if(ufra.length == '') showErro('Por favor informe o UF', '#msg_error');
        if(inIrmaoRA.length == '' || inIrmaoRA.length<8) showErro('Por favor informe um RA válido do irmão do aluno', '#msg_error');
        if(inIrmaoDigitoRA.length == '') showErro('Por favor informe o dígito do RA do irmão do aluno', '#msg_error');
        if(inIrmaoUFRA.length == '') showErro('Por favor informe o UF do irmão do aluno', '#msg_error');
               
        if(!error){
            
            data = '';        
            $.post(baseUrl + "/" + module + "/" + controller + "/associairmao/", 
                {idaluno: idaluno, inRA: alunoRA, inDigitoRA: digra, inUFRA: ufra, inIrmaoRA: inIrmaoRA, inIrmaoDigitoRA: inIrmaoDigitoRA, 
                    inIrmaoUFRA: inIrmaoUFRA, inGemeo: inGemeo, inNome: inNome, inMae: inMae, inPai:inPai, inNascimento : inNascimento}, function(data){
                   
                    $("#loader").fadeOut('fast', function(){
                        $("#infoResult").hide();
                        $("#btn-associar").hide();
                        $("input[name=inRA], input[name=inDigitoRA]").val('');
                    });
                    if(data.idlast && data.novo == "SIM"){
                        idlast = data.idlast;
                        param = "'"+idlast+"','"+inNome+"'";

                        structure = '<tr id="line'+idlast+'">';
                        structure += '<td>' + inIrmaoRA + ' - ' + inIrmaoDigitoRA + ' / '+ ufra +'</td>';
                        structure += '<td>' + inNome + '</td>';
                        structure += '<td>' + inNascimento + '</td>';
                        structure += '<td>' + inMae + '</td>';
                        structure += '<td>' + inPai + '</td>';
                        structure += '<td><a class="btn btn-small btn-danger btn-desassociar" id="btn-desassociar" onclick="excluirIrmao('+param+')" style="cursor:pointer"><span class="icon-trash">&nbsp;</span>Excluir</a></td>';                        
                        structure += '</tr>';

                        $("#empty-register").hide();
                        $(structure).appendTo("#body-results");
                    }
                   
                }, "JSON");
        }else{
            error = false;
            setTimeout(function(){
                $("#msg_error").parent().fade('fast', function(){
                    $("#msg_error").html('');
                });                
            }, 3000)
        }
    });

    $('#btn-pesquisar').on('click', function(ev){
        ev.preventDefault();
        
        inRA = $('input[name=inRA]').val();
        inDigitoRA = $('input[name=inDigitoRA]').val();
        inUF = $('select[name=inUF]').val();

        consultarFicha(inRA, inDigitoRA, inUF);
    });

    /*function formataRG(rg){
       
        for(i=0; i< rg.length; i++){
            if (rg.charAt(i) == 0){
                console.log(i, 'sim');
                rg = rg.substring(i);
            }else{
                console.log(i, 'não');
                i = rg.length;
                return rg;
            }
        }
        console.log(i);
        return rg;
    }*/

});