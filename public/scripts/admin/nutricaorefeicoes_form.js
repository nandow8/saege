var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
                $("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-refeicaoiner').css('width', '100%');
		});
	
	}
	
	

	$("input[name=datainicio]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datainicio]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=datafim]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datafim]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	
	
	rules = {};
	rules.idescola = "required";
        rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function refeicaoAddTr(_values) {

        html = "";

        unidadeSelect = $('#uni option:selected').val();
        
        if(($('#qtd').val() < 1) || ($('#obs').val() < 1) || (unidadeSelect == 'Selecione...')){
            $.fancybox({
                type: 'inline',
                href: $('#divAlerta'),
                autoSize: true,
                helpers: {
                   title: {
                       type: 'float'
                   }
               }
            });
            return false;
        }

        if (_values===false) {
                _values = {};
        }else if(_values === undefined){
                _values = { 
                            "idnutricaorefeicao":$("#id").val(),
                            "idnutricaotacoalimento":$('#ing').val(),
                            'descricaotacoalimento':$("#ing option:selected").html(),
                            "unidade":$("#uni option:selected").html(),
                            "observacao":$('#obs').val(),
                            "quantidade":$("#qtd").val(),
                            "var_db":true
                          };
        } else {
                _values = jQuery.parseJSON(_values);
                itens = _values;
        }

        modelo = $("#modelo-refeicao").html();
        modelo = '<tr>' + modelo + '</tr>';
        modelo = $(modelo);
        table = $("table.table-refeicoes");
        refeicaoiner = table.find('tbody');

        modelo.find('p').html(html);
        modelo.find('input,select,textarea,div').each(function() {
                name = $(this).attr('name');
                name = name.replace("_", "");
                if(_values && _values.var_db == true)name = name.replace("[]", "");

                for (var property in _values) {
                        if (property==name) {

                                $(this).attr('name', name + '[]');
                                if(_values.var_db){
                                }
                                //console.log('==>',_values[property])
                                $(this).attr('value', _values[property]);

                                $(this).html(_values[property]);

                                if ($(this).is("select")) {
                                        $(this).parent().find('a').remove();
                                }
                        }
                }
        });

        refeicaoiner.append(modelo);

        $('.numbervalue').priceFormat({
                prefix: '',
                limit: 3,
                centsSeparator: '',
                thousandsSeparator: ''	    
        });

        $('.numbervalue').blur(function(){
                if($(this).val() <= 0){
                        $(this).val('');	
                }
        });

        // Rodando novamente a tabela para pegar todos os valores necessários para a soma
        table = $("table.table-refeicoes");
        refeicaoiner = table.find('tbody');

        var arrayTotalAlimento = new Array();

        refeicaoiner.find('tr').each(function(int, val){
            var arrayLineAlimento = new Array();
            $(val).find('input').each(function(i, v){

                if($(v).attr('name') == 'quantidade[]'){
                    arrayLineAlimento.push($(v).val());
                }
                
                if($(v).attr('name') == 'idnutricaotacoalimento[]'){
                    arrayLineAlimento.push($(v).val());
                    //console.log('Alimento : ', $(v).val());
                }

                if($(v).attr('name') == 'observacao[]'){
                    arrayLineAlimento.push($(v).val());
                    //console.log('Alimento : ', $(v).val());
                }

                
            });
            arrayTotalAlimento.push(arrayLineAlimento);
        });

        //console.log('==>',arrayTotalAlimento);

        var arraySomatorio = new Array(0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0);
        var lpd = 0;

        $('#somatorio_alimentos').html('');
        $.each(arrayTotalAlimento, function(i, val){

            $.post(baseUrl + "/" + module + "/"+controller+"/retornaalimentoajax",
            {id: val[1]}, function(data){
              
                data = JSON.parse(data);
    
                $('#somatorio_alimentos').append('<tr>');
                    $('#somatorio_alimentos').append('<td>' + data.descricao + '</td>');
                    $('#somatorio_alimentos').append('<td>' + val[0] + '</td>');
                    $('#somatorio_alimentos').append('<td>' + val[2]  +'</td>');

                    
                    total = (parseFloat(data.energia_kcal.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[0] += (parseFloat(data.energia_kcal.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' + total.toFixed(2)  +'</td>');

                    total = (parseFloat(data.vl_carb.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[1] += (parseFloat(data.vl_carb.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' +  total.toFixed(2) +'</td>');

                    total = (parseFloat(data.vl_g_tot.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[2] += (parseFloat(data.vl_g_tot.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' +  total.toFixed(2)   +'</td>');
                   
                    total = (parseFloat(data.vl_prot.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[3] += (parseFloat(data.vl_prot.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' +  total.toFixed(2)   +'</td>');

                    total = (parseFloat(data.vl_fibra.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[4] += (parseFloat(data.vl_fibra.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' + total.toFixed(2)  +'</td>');

                    total = (parseFloat(data.vl_vit_a.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[5] += (parseFloat(data.vl_vit_a.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' +  total.toFixed(2) +'</td>');

                    total = (parseFloat(data.vl_vit_c.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[6] += (parseFloat(data.vl_vit_c.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' +  total.toFixed(2)  +'</td>');

                    total = (parseFloat(data.vl_fe.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[7] += (parseFloat(data.vl_fe.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' +  total.toFixed(2) +'</td>');

                    total = (parseFloat(data.vl_mg.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[8] += (parseFloat(data.vl_mg.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' +  total.toFixed(2) +'</td>');
                    
                    total = (parseFloat(data.vl_ca.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[9] += (parseFloat(data.vl_ca.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' +  total.toFixed(2) +'</td>');

                    total = (parseFloat(data.vl_zinco.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    arraySomatorio[10] += (parseFloat(data.vl_zinco.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                    $('#somatorio_alimentos').append('<td>' +  total.toFixed(2) +'</td>');

                    $('#somatorio_alimentos').append('</tr>');

                console.log(arrayTotalAlimento.length);

                if((arrayTotalAlimento.length-1) == i){
                    $('#somatorio_alimentos').append('<tfoot>');
                    $('#somatorio_alimentos').append('<tr>');
                         $('#somatorio_alimentos').append('<td colspan="3">Total</td>');
                         $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[0]).toFixed(2) + '</td>');
                         $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[1]).toFixed(2) + '</td>');
                         $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[2]).toFixed(2) + '</td>');
                         $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[3]).toFixed(2) + '</td>');
                         $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[4]).toFixed(2) + '</td>');
                        $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[5]).toFixed(2) + '</td>');
                        $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[6]).toFixed(2) + '</td>');
                        $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[7]).toFixed(2) + '</td>');
                        $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[8]).toFixed(2) + '</td>');
                        $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[9]).toFixed(2) + '</td>');
                        $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[10]).toFixed(2) + '</td>');
                        $('#somatorio_alimentos').append('</tr>');
                    $('#somatorio_alimentos').append('</tfoot>');

                    console.log(arraySomatorio[2]);

                }


            });//end post
            
        });
        
      
        somatorioretornoativo();
        calcTotLinhas();
}


/* ======================================================================================
                Inserindo refeições para edição
========================================================================================= */


function refeicaoAddTrInicio(_values) {

    html = "";

    if (_values===false) {
            _values = {};
    }else if(_values === undefined){
            _values = { 
                        "idnutricaorefeicao":$("#id").val(),
                        "idnutricaotacoalimento":$('#ing').val(),
                        'descricaotacoalimento':$("#ing option:selected").html(),
                        "unidade":$("#uni option:selected").html(),
                        "observacao":$('#obs').val(),
                        "quantidade":$("#qtd").val(),
                        "var_db":true
                      };
    } else {
            _values = jQuery.parseJSON(_values);
            itens = _values;
    }

    modelo = $("#modelo-refeicao").html();
    modelo = '<tr>' + modelo + '</tr>';
    modelo = $(modelo);
    table = $("table.table-refeicoes");
    refeicaoiner = table.find('tbody');

    modelo.find('p').html(html);
    modelo.find('input,select,textarea,div').each(function() {
            name = $(this).attr('name');
            name = name.replace("_", "");
            if(_values && _values.var_db == true)name = name.replace("[]", "");

            for (var property in _values) {
                    if (property==name) {

                            $(this).attr('name', name + '[]');
                            if(_values.var_db){
                            }
                            //console.log('==>',_values[property])
                            $(this).attr('value', _values[property]);

                            $(this).html(_values[property]);

                            if ($(this).is("select")) {
                                    $(this).parent().find('a').remove();
                            }
                    }
            }
    });

    refeicaoiner.append(modelo);

    $('.numbervalue').priceFormat({
            prefix: '',
            limit: 3,
            centsSeparator: '',
            thousandsSeparator: ''	    
    });

    $('.numbervalue').blur(function(){
            if($(this).val() <= 0){
                    $(this).val('');	
            }
    });

    // Rodando novamente a tabela para pegar todos os valores necessários para a soma
    table = $("table.table-refeicoes");
    refeicaoiner = table.find('tbody');

    var arrayTotalAlimento = new Array();

    refeicaoiner.find('tr').each(function(int, val){
        var arrayLineAlimento = new Array();
        $(val).find('input').each(function(i, v){

            if($(v).attr('name') == 'quantidade[]'){
                arrayLineAlimento.push($(v).val());
            }
            
            if($(v).attr('name') == 'idnutricaotacoalimento[]'){
                arrayLineAlimento.push($(v).val());
                //console.log('Alimento : ', $(v).val());
            }

            if($(v).attr('name') == 'observacao[]'){
                arrayLineAlimento.push($(v).val());
                //console.log('Alimento : ', $(v).val());
            }
        });
        arrayTotalAlimento.push(arrayLineAlimento);
    });

    //console.log('==>',arrayTotalAlimento);
    
    //somatorioFinal();
    
    calcTotLinhas();
}//end refeiçoes do inicio


function setStarzinhas(obj){

    let inc = 0;
    let campo = parseFloat(document.getElementById("favorita").value);

    if ($(obj).attr('class') === 'icon-star-empty btn-large'){
        $(obj).removeClass($(obj).attr('class')).addClass("icon-star-half btn-large");
        inc += 0.5;
    }else if ($(obj).attr('class') === 'icon-star-half btn-large'){
        $(obj).removeClass($(obj).attr('class')).addClass("icon-star btn-large");
        inc += 0.5;
    }else if ($(obj).attr('class') === 'icon-star btn-large'){    
        $(obj).removeClass($(obj).attr('class')).addClass("icon-star-empty btn-large");
        inc -= 1;
    }

    if (inc < 5){
        document.getElementById("favorita").value = parseFloat(campo + inc).toString();
        $("#nota").text(parseFloat(campo + inc).toString());
    }

}

function setEstrelas(nota){

    if (nota === 0){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
    }else if (nota === 0.5){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star-half btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
    }else if (nota === 1){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
    }else if (nota === 1.5){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star-half btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
    }else if (nota === 2){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
    }else if (nota === 2.5){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star-half btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
    }else if (nota === 3){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
    }else if (nota === 3.5){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star-half btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
    }else if (nota === 4){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star-empty btn-large");
    }else if (nota === 4.5){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star-half btn-large");
    }else if(nota === 5){
       $('span#estrela01').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela02').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela03').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela04').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
       $('span#estrela05').removeClass("icon-star-empty btn-large").addClass("icon-star btn-large");
    }
}

function refeicoesExcluirFromTable(obj) {
    
   
    

        jConfirm("Deseja excluir o registro?", 'Confirmar', function(r){

            obj = $(obj);
            content = obj.closest('tr');
            var idcode = 0;

            content.find('td').each(function(index, value){
                if(index == 0){
                    idcode = $(value).find('input').val();
                    //console.log('id', idcode);
                }// end if
            });

            if(r)
            {
               
                $.post(baseUrl + "/" + module + "/"+controller+"/retiraalimentoajax",
                {id: idcode}, function(data){
                //data = JSON.parse(data);
                    //alert(data);
                    content.find('input').remove();
                    content.hide();
                    somatorioretornoativo();
                    calcTotLinhas();

                });
            }
        });

}

$('#uni').change(function(){
    
    var selecionadoValor = $('#uni option:selected').val();

    $.post(baseUrl + "/" + module + "/"+controller+"/unidadescomerciaisajax",
 	{id: selecionadoValor}, function(data){
        data = JSON.parse(data);
        $('#obs').val(data.valdefault);
 	});
});

function setBtnAddRefeicao(obj){
    let val = $(obj).val();
    document.getElementById('BtnAddRefeicao').disabled = (val === "");

    var selecionadoValor = $('#ing option:selected').val();
    var selecionadoTexto = $('#ing option:selected').text();

    $('#qtd').val('1');
    $('#obs').val('100');
    $('#uni').html(''); // limpando select uni

    $id = selecionadoValor;

    $.post(baseUrl + "/" + module + "/"+controller+"/unidadesajax",
 	{id: $id}, function(data){

        console.log('GGG',data);

        data = JSON.parse(data);

        $('#uni').append('<option value="55">Selecione...</option>');
        $.each(data, function(index, value){
            $('#uni').append("<option value='" + value.id + "'>" + value.unidade + "</option>");
        });

        $('select[name=unidade]').val('55');

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
		
     });
     
     
    
   

}        

function calcTotLinhas(){
        $("#subtotQtd").html((document.getElementsByName('quantidade[]').length-1).toString());
}

function somatorioretornoativo(){

    
        // Rodando novamente a tabela para pegar todos os valores necessários para a soma
     table = $("table.table-refeicoes");
     refeicaoiner = table.find('tbody');

     var arrayTotalAlimento = new Array();

     refeicaoiner.find('tr').each(function(int, val){
         var arrayLineAlimento = new Array();
         $(val).find('input').each(function(i, v){

             if($(v).attr('name') == 'quantidade[]'){
                 arrayLineAlimento.push($(v).val());
             }
             
             if($(v).attr('name') == 'idnutricaotacoalimento[]'){
                 arrayLineAlimento.push($(v).val());
                 //console.log('Alimento : ', $(v).val());
             }

             if($(v).attr('name') == 'observacao[]'){
                 arrayLineAlimento.push($(v).val());
                 //console.log('Alimento : ', $(v).val());
             }

             
         });
         arrayTotalAlimento.push(arrayLineAlimento);
     });

     $('#resultados_soma').html('');

     $.post(baseUrl + "/" + module + "/"+controller+"/retornaalimentototalajax",
     {id: arrayTotalAlimento}, function(data){

        data = JSON.parse(data);

       $.each(data, function(i, value){
            console.log('===>',value);
            html = '<tr>';

            if(i == (data.length-1)){
                html += '<td> Total </td>';
                html += '<td></td>';
                html += '<td></td>';
            }else{
                html += '<td>' + value.descricao  + '</td>';
                html += '<td>' + value.qtde  + '</td>';
                html += '<td>' + value.grama + '</td>';
            }
            if(value.energia_kcal != null){
                html += '<td>' + parseFloat(value.energia_kcal).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            if(value.vl_carb != null){
                html += '<td>' + parseFloat(value.vl_carb).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            if(value.vl_g_tot != null){
                html += '<td>' + parseFloat(value.vl_g_tot).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            if(value.vl_prot != null){
                html += '<td>' + parseFloat(value.vl_prot).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            if(value.vl_fibra != null){
                html += '<td>' + parseFloat(value.vl_fibra).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            if(value.vl_vit_a != null){
                html += '<td>' + parseFloat(value.vl_vit_a).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            if(value.vl_vit_c != null){
                html += '<td>' + parseFloat(value.vl_vit_c).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            if(value.vl_fe != null){
                html += '<td>' + parseFloat(value.vl_fe).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            if(value.vl_mg != null){
                html += '<td>' + parseFloat(value.vl_mg).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            if(value.vl_ca != null){
                html += '<td>' + parseFloat(value.vl_ca).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            if(value.vl_zinco != null){
                html += '<td>' + parseFloat(value.vl_zinco).toFixed(2) + '</td>';
            }else{
                html += '<td> -- </td>';
            }

            html += '</td>';
            $('#resultados_soma').append(html);
        }); 
         
     });
     

}//end function somatorioretorno

function somatorioretorno(){

     // Rodando novamente a tabela para pegar todos os valores necessários para a soma
     table = $("table.table-refeicoes");
     refeicaoiner = table.find('tbody');

     var arrayTotalAlimento = new Array();

     refeicaoiner.find('tr').each(function(int, val){
         var arrayLineAlimento = new Array();
         $(val).find('input').each(function(i, v){

             if($(v).attr('name') == 'quantidade[]'){
                 arrayLineAlimento.push($(v).val());
             }
             
             if($(v).attr('name') == 'idnutricaotacoalimento[]'){
                 arrayLineAlimento.push($(v).val());
                 //console.log('Alimento : ', $(v).val());
             }

             if($(v).attr('name') == 'observacao[]'){
                 arrayLineAlimento.push($(v).val());
                 //console.log('Alimento : ', $(v).val());
             }

             
         });
         arrayTotalAlimento.push(arrayLineAlimento);
     });

     //console.log('==>',arrayTotalAlimento);

     var arraySomatorio = new Array(0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0);
     var lpd = 0;

     $('#somatorio_alimentos').html('');

     contador = 0;

     console.log('taco: ', arrayTotalAlimento);
     jQuery.each(arrayTotalAlimento, function(i, val){
         

         $.post(baseUrl + "/" + module + "/"+controller+"/retornaalimentoajax",
         {id: val[1]}, function(data){
           
             data = JSON.parse(data);
 
                 var html = '';
             
                 html += '<tr><td>' + data.descricao  + '</td>';
                 html += '<td>' + val[0] + '</td>';
                 html += '<td>' + val[2]  +'</td>';

                 
                 total = (parseFloat(data.energia_kcal.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[0] += total;
                 html += '<td>' + total.toFixed(2)  +'</td>';

                 total = (parseFloat(data.vl_carb.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[1] += total;
                 html += '<td>' +  total.toFixed(2) +'</td>';

                 total = (parseFloat(data.vl_g_tot.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[2] += total;
                 html += '<td>' +  total.toFixed(2)   +'</td>';
                
                 total = (parseFloat(data.vl_prot.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[3] += total;
                 html += '<td>' +  total.toFixed(2)   +'</td>';

                 total = (parseFloat(data.vl_fibra.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[4] += total;
                 html += '<td>' + total.toFixed(2)  +'</td>';

                 total = (parseFloat(data.vl_vit_a.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[5] += total;
                 html += '<td>' +  total.toFixed(2) +'</td>';

                 total = (parseFloat(data.vl_vit_c.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[6] += total;
                 html += '<td>' +  total.toFixed(2)  +'</td>';

                 total = (parseFloat(data.vl_fe.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[7] += total;
                 html += '<td>' +  total.toFixed(2) +'</td>';

                 total = (parseFloat(data.vl_mg.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[8] += total;
                 html += '<td>' +  total.toFixed(2) +'</td>';
                 
                 total = (parseFloat(data.vl_ca.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[9] += total;
                 html += '<td>' +  total.toFixed(2) +'</td>';

                 total = (parseFloat(data.vl_zinco.replace(",", ".")) * parseFloat(val[2]/100)) * parseFloat(val[0]);
                 arraySomatorio[10] += total;
                 html += '<td>' +  total.toFixed(2) +'</td></tr>';

                 $('#somatorio_alimentos').append(html);

                 

             console.log(arrayTotalAlimento.length);

             if((arrayTotalAlimento.length-1) == i){
                 console.log('tamanho:', arrayTotalAlimento.length);
                 //$('#somatorio_alimentos').append('<tfoot>');
                 //$('#somatorio_alimentos').append('<tr>');
                 //    $('#somatorio_alimentos').append('<td colspan="3">Total</td>');
                 //    $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[0]).toFixed(2) + '</td>');
                 //    $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[1]).toFixed(2) + '</td>');
                 //    $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[2]).toFixed(2) + '</td>');
                 //    $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[3]).toFixed(2) + '</td>');
                 //    $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[4]).toFixed(2) + '</td>');
                 //    $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[5]).toFixed(2) + '</td>');
                 //    $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[6]).toFixed(2) + '</td>');
                 //    $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[7]).toFixed(2) + '</td>');
                 //    $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[8]).toFixed(2) + '</td>');
                 //    $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[9]).toFixed(2) + '</td>');
                 //   $('#somatorio_alimentos').append('<td>' + parseFloat(arraySomatorio[10]).toFixed(2) + '</td>');
                 //    $('#somatorio_alimentos').append('</tr>');
                 //$('#somatorio_alimentos').append('</tfoot>');

             }

         });//end post
         
     });


}//end function somatorioretorno

function somatorioFinal(){

    obj = $('#somatorio_alimentos');

    var arraySoma = new Array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

    $('#somatorio_alimentos').find('tr').each(function(int, val){
       $(val).find('td').each(function(i, value){
            arraySoma[i] += parseFloat($(value).text());
       });
    });
    
    $('#resultados_soma').html('');

    $.each(arraySoma,function(index,value){
        
        if((index > 2) && (index != (arraySoma.length-1))){
            $('#resultados_soma').append('<td>' + value.toFixed(2) + '</td>');
        }


    });
   


}//end function somatorioFinal

function somageral(){
    
    // Rodando novamente a tabela para pegar todos os valores necessários para a soma
    table = $("table.table-refeicoes");
    refeicaoiner = table.find('tbody');

    var arrayTotalAlimento = new Array();

    refeicaoiner.find('tr').each(function(int, val){
        var arrayLineAlimento = new Array();
        $(val).find('input').each(function(i, v){

            if($(v).attr('name') == 'quantidade[]'){
                arrayLineAlimento.push($(v).val());
            }
            
            if($(v).attr('name') == 'idnutricaotacoalimento[]'){
                arrayLineAlimento.push($(v).val());
                //console.log('Alimento : ', $(v).val());
            }

            if($(v).attr('name') == 'observacao[]'){
                arrayLineAlimento.push($(v).val());
                //console.log('Alimento : ', $(v).val());
            }
            
        });
        arrayTotalAlimento.push(arrayLineAlimento);
    });

    console.log('--->');
    console.log(arrayTotalAlimento);
}//end function somageral