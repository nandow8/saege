var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idprofessor]").val(form_values.idprofessor);
		$("select[name=idcurso]").val(form_values.idcurso);
		$("select[name=posicao]").val(form_values.posicao);
		$("select[name=status1]").val(form_values.status1);

	if ((typeof form_values.idcurso)!='undefined') {
		getDados($("[name=idcurso]"));
	}


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	



	$('input[name=pontuacao]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});





	
	
	rules = {};
	//rules.idprofessor = "required";
	rules.curso = "required";
	rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function getDados(obj)
{
	var idcurso = $(obj).val();

	if(idcurso != '')
	{
		$.post(baseUrl + "/" + module + "/documentacaosolicitacoespontos/getcurso",{idcurso: idcurso}, function(data) 
		{
			data = JSON.parse(data);
			console.log(data)
			if(data.status == 'OK')
			{
				$('.box-dados').show();
				$('#pontuacao_curso').val(data.dados.valorpontos);
				$('input[name=pontuacao]').val(data.dados.valorpontos);
			}
			else
			{
				$('.box-dados').hide();
				jAlert("Ocorreu um erro ao buscar os dados do curso.")
			}

		});
	}
}