var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idfuncionario]").val(form_values.idfuncionario);
		$("select[name=idfuncionarioescola]").val(form_values.idfuncionarioescola);
$("select[name=parentesco]").val(form_values.parentesco);
$("select[name=status1]").val(form_values.status1);
		if ((typeof form_values.parentesco)!='undefined') {
			setCamposParentescos(jQuery("select[name=parentesco]").val(form_values.parentesco));
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	


	
	
	rules = {};
	rules.idfuncionario = "required";
rules.parentesco = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


function setCamposParentescos(obj){	
	var verificaarentescos = $(obj).val();
	if(verificaarentescos=="Sim"){
		$('.box-parentescos').css("display", "block");
	}else{
		$('.box-parentescos').css("display", "none");
	}
}

function iteAddTr(item) {
	
	table = $(".itens table tbody#raiz");
	
	tr = $('<tr />');
	
	var classe_qtd="form-control";
	if (item===false) {
		item = {};
		item.id=0;
		item.nome = '';
		item.grauparentesco = '';
		item.localtrabalho = '';			
	}
	

	tr.append('<td><input style="width: 95%!Important" value="'+item.nome+'" type="text" maxlength="255" name="nome[]" class="form-control" /></td>');	
	tr.append('<td><input name="idsparentescos[]" type="hidden" value="'+item.id+'" /><input style="width: 95%!Important" value="'+item.grauparentesco+'" type="text" class="form-control" maxlength="255" name="grauparentesco[]" /></td>');
	tr.append('<td><input style="width: 95%!Important" value="'+item.localtrabalho+'" type="text" maxlength="255" name="localtrabalho[]" class="form-control" /></td>');
	
	tr.append('<td style="text-align:center!Important" align="center"><a href="javascript:;" onclick="iteExcluirFromTable(this)"><i class="icon-remove"></i></a></td>');
	
	table.append(tr);
	
}


function iteExcluirFromTable(obj) {
	$(obj).parent().parent().remove();
}