$(document).ready(function () {

    /* Habilita texto explicativo nos campos pesquisa */
    $('[data-toggle="tooltip"]').tooltip();


    /* Mascáras de entrada dos campos */
    //$('input[name="inNumCertidaoNova"]').mask('999999 99 99 9999 9 99999 999 9999999 99');
    //$('input[name="inNumNIS"]').mask('9 999999999999');
    //$('input[name="inNumINEP"]').mask('9999999999999');
    $('input[name="inCPF"]').mask('999.999.999-99');

    $("input[name=inNascimento]").mask("99/99/9999", {placeholder: "  /  /    "});
    $("input[name=inNascimento]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onClose: function () {
            var er = /^(((0[1-9]|[12][0-9]|3[01])([-.\/])(0[13578]|10|12)([-.\/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-.\/])(0[469]|11)([-.\/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-.\/])(02)([-.\/])(\d{4}))|((29)(\.|-|\/)(02)([-.\/])([02468][048]00))|((29)([-.\/])(02)([-.\/])([13579][26]00))|((29)([-.\/])(02)([-.\/])([0-9][0-9][0][48]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][2468][048]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][13579][26])))$/;
            
            if (!er.test(this.value)) {
                jAlert('A data informada, "' + this.value + '" é inválida.', 'Atenção' );
                this.value = "";
            }
        }
    });

});

function setCertidao(obj) {
    var Valor = $(obj).context.checked;

    //Tratamento para perfil secretaria Escolar
    if (Valor) {
        $(".certdaonova").css("display", "block");
        $(".certdaoantiga").css("display", "none");
    } else {
        $(".certdaonova").css("display", "none");
        $(".certdaoantiga").css("display", "block");
    }

}

function setAba(id) {

    document.getElementById('tabnav').value = id;

    if (id == 1) {
        $("#1b").removeClass("tab-pane").addClass("tab-pane active");
        $("#2b").removeClass("tab-pane active").addClass("tab-pane");
        $("#3b").removeClass("tab-pane active").addClass("tab-pane");
        $("#4b").removeClass("tab-pane active").addClass("tab-pane");
    } else if (id == 2) {
        $("#1b").removeClass("tab-pane active").addClass("tab-pane");
        $("#2b").removeClass("tab-pane").addClass("tab-pane active active");
        $("#3b").removeClass("tab-pane active").addClass("tab-pane");
        $("#4b").removeClass("tab-pane active").addClass("tab-pane");
    } else if (id == 3) {
        $("#1b").removeClass("tab-pane active").addClass("tab-pane");
        $("#2b").removeClass("tab-pane active").addClass("tab-pane");
        $("#3b").removeClass("tab-pane").addClass("tab-pane active");
        $("#4b").removeClass("tab-pane active").addClass("tab-pane");
    } else if (id == 4) {
        $("#1b").removeClass("tab-pane active").addClass("tab-pane");
        $("#2b").removeClass("tab-pane active").addClass("tab-pane");
        $("#3b").removeClass("tab-pane active").addClass("tab-pane");
        $("#4b").removeClass("tab-pane").addClass("tab-pane active");
    }

}

    function ExibeErro(mensagem){
        
        mensagem = '<h5>'+mensagem+'</h5>';
        
        var dialog = $(mensagem).dialog({
            title: '<font color="#052f57"> Erro Retornado </font>',
            width: 400,
            modal: true,
            buttons: {
                "Fechar": function () {
                    dialog.dialog('close');
                }
            }
        });
        
        setAba(document.getElementById('tabnav').value);
    }
    
    function ConsultarDados(dado) {

        var linha = '';
        var Uchar = 'ABCDEFGHIJKLMNOPQRSTUWXYZ';
        
        for (key in dado) {
            var rotulo = '';
            
            //Remove prefixo "out" do rótulo
            rotulo = key.substring(3, key.length);
            
            //Se o rótulo for maior que 3 caracteres é feito o tratamento dando espaço entre os caracters maiúsculos
            if (rotulo.length > 3){
                p = 1;
                for (i in rotulo){
                    for (c in Uchar){
                        if ((rotulo.charAt(i) === Uchar.charAt(c)) && i > p){ 
                            rotulo = rotulo.replace(rotulo.charAt(i), ' '+Uchar.charAt(c));
                            p = i;
                        }
                    }
                }
            }
            //Compõe a linha da tabela
            if (typeof dado[key] == 'string'){
                linha += '<tr><td><b>'+rotulo+':</b></td><td>'+dado[key]+'</td></tr>'; 
            }else if (typeof dado[key] == 'object'){
                for (subkey in dado[key]) {
                    var subrotulo = '';

                    //Remove prefixo "out" do subrótulo
                    subrotulo = subkey.substring(3, subkey.length);
                    
                    //Se o rótulo for maior que 3 caracteres é feito o tratamento dando espaço entre os caracters maiúsculos
                    if (subrotulo.length > 3){
                        p = 1;
                        for (i in subrotulo){
                            for (c in Uchar){
                                if ((subrotulo.charAt(i) === Uchar.charAt(c)) && i > p){ 
                                    subrotulo = subrotulo.replace(subrotulo.charAt(i), ' '+Uchar.charAt(c));
                                    p = i;
                                }
                            }
                        }
                    }
                    //Compõe a sublinha da tabela
                    linha += '<tr><td><b>'+subrotulo+':</b></td><td>'+dado[key][subkey]+'</td></tr>'; 
                    
                }
            }
            
        }
        
        var html = 
                '<div class="pre-scrollable">'+
                '<table border="1" class="table">'+ 
                '    <tbody>'+
                linha +
                '    </tbody>'+
                '</table>'+
                '</div>';
        
        var dialog = $(html).dialog({
            title: '<font color="#052f57">'+dado["outNomeAluno"]+'</font>',
            minWidth: 100,
            width: 'auto',
            maxWidth: 650,
            modal: true,
            buttons: {
                "Fechar": function () {
                    dialog.dialog('close');
                }
            }
        });

    }
    
    function Limpar(){
        $('div.megablock').find('input').val('');
        $('div.megablock').find('select').val('');
    }


    function setTipoFonetica(){
        
        var Valor = document.getElementById('fonetica').selectedIndex;
        setAba(document.getElementById('tabnav').value);
        
        if (Valor === 1 ) {
            $("#divfonNom").css("display", "block");
            $("#divfonMae").css("display", "none");
            $("#divfonPai").css("display", "none");
        }else if (Valor === 2 ) {
            $("#divfonNom").css("display", "none");
            $("#divfonMae").css("display", "block");
            $("#divfonPai").css("display", "none");
        }else if (Valor === 3 ) {
            $("#divfonNom").css("display", "none");
            $("#divfonMae").css("display", "none");
            $("#divfonPai").css("display", "block");
        }else{
            $("#divfonNom").css("display", "none");
            $("#divfonMae").css("display", "none");
            $("#divfonPai").css("display", "none");
        }
        
    }
