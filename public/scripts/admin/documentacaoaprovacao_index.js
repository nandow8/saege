$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
$("select[name=iddocumentacao]").val(form_values.iddocumentacao);
$("select[name=idprofessor]").val(form_values.idprofessor);
$("select[name=status1]").val(form_values.status1);
	

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});	
	}
	
	






	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

/*
function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Ativo") || (data=="Bloqueado")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 
*/

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/admin/"+controller+"/changeaprovacaoxml",
 	 	 	{id: $id, op: controller}, function(data) {
                            console.log(data)
			if ((data=="Sim") || (data=="Não")) {
				$obj.html(data);
				if(data=="Sim"){
					$($obj).parent().parent().find('.status2').html('Aprovado');
				}else{
					$($obj).parent().parent().find('.status2').html('Cancelado');
				}
			} else {
				jAlert(data, "Alerta!");
			}
 	});
} 