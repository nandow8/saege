function setSeries(obj){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setseries",{idescola: idescola}, function(data) {

		$('select[name=idserie]').html(data);
		val = $("select[name=idserie]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	});
}

function setPeriodo(obj) {
	var idescola = $(obj).val();

	$.post(baseUrl + "/" + module + "/"+controller+"/setperiodo",{idescola: idescola}, function(data) {

		$("select[name=periodo]").html(data);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	});
}

function setTurmas(obj){
	var idserie = $(obj).val();
	var idescola = $("select[name=idescola]").val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setturmas",{idserie: idserie, idescola:idescola}, function(data) {

		$('select[name=idturma]').html(data); 	
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	});
}

function setAlunos(obj){	

	var idescolavinculo = $(obj).val();
	var idescola = $("select[name=idescola]").val();
	var idserie = $("select[name=idserie]").val();

	$.post(baseUrl + "/" + module + "/"+controller+"/setalunos",{idvinculo: idescolavinculo, idserie: idserie, idescola:idescola}, function(data) {

		$('select[name=idalunos]').html(data);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	});
}

function comparar() {

	//Checar se é possível comparar
	var aluno = $("select[name=idalunos]").val();
	var turma = $("select[name=idturma]").val();

	if( aluno == "" || turma == "" ) {
		alert("Precisa selecionar um aluno");
		return 0;
	}

	idescola = $("select[name=idescola").val();
	idserie = $("select[name=idserie").val();
	idturma = $("select[name=idturma").val();
	idaluno = $("select[name=idalunos").val();

	//esconde e reseta todos os campos
	hideall();	

	$.post(baseUrl + "/" + module + "/"+controller+"/comparar",{idescola:idescola, idserie:idserie, idturma:idturma, 
		idaluno:idaluno}, function(data) {

			$("#tablehelp").html(data);

			$("#tablehelp").show();
	});	
}

function hideall() {

	$("select").each(function() {
		$(this).val("");
	});

	$("select").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});
		
	$("select").each(function (){
		div = $(this).closest('div').find('.chosen-container').css('width', '100%');
	})

	$(".div-serie").hide();
	$(".div-periodo").hide();
	$(".div-turma").hide();
	$(".div-alunos").hide();
}

$(document).ready(function() {

	//CHANGES
	$("select[name=idescola]").change(function() {
		if($(this).val() != ""){
			$(".div-serie").show();
			$(".div-periodo").show();
		}
		else{
			$(".div-serie").hide();
			$(".div-periodo").hide();
		}
	});

	$("select[name=idserie]").change(function() {
		if($(this).val() != ""){
			$(".div-turma").show();
		}
		else{
			$(".div-turma").hide();
		}
	});

	$("select[name=idturma]").change(function() {
		if( $(this).val() != "" ) {
			$(".div-alunos").show();
		} else {
			$(".div-alunos").hide();
		}
	});
});