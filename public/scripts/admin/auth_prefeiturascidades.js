﻿var unsaved = false;


jQuery(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};
	
	if ((typeof form_values)!='undefined') {
		if ((typeof form_values.idestado)!='undefined') jQuery("select[name=idestado]").val(form_values.idestado);
		if ((typeof form_values.idcidade)!='undefined') jQuery("select[name=idcidade]").val(form_values.idcidade);
		if ((typeof form_values.idcidade)!='undefined'){
			setCidades($("select[name=idestado]").val(form_values.idestado), form_values.idcidade);
		}
		if ((typeof form_values.idssecretarias)!='undefined') $("[name='idssecretarias[]']").val(form_values.idssecretarias);
		$("select").trigger("chosen:updated");
	}
		
	jQuery('select, input, textarea').bind('blur', function() { });

	rules = {};
	rules.idestado = "required";
	rules.idcidade = "required";
	
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
		    
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});	
	
	
});

function setCidades(obj, idcidade){
	var val = $(obj).val();
	data = '';
	$("select[name=idcidade]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/auth/setcidades/', {idestado:val}, function(data) {
		$("select[name=idcidade]").html(data);
		$("select[name=idcidade]").val(idcidade);
		val = $("select[name=idcidade]").find('option:selected').html();
		$("select[name=idcidade]").closest('div').find('span').html(val);
	
		$("select[name=idcidade]").trigger("chosen:updated");
		
		if(idcidade==""){
			$("select[name=idcidade]").closest('div').find('span').html('Selecione...');
		}
	});
}