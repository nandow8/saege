var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=tipoavaliacao]").val(form_values.tipoavaliacao);
$("select[name=tipoevolucao]").val(form_values.tipoevolucao);
$("select[name=idaluno]").val(form_values.idaluno);
if(form_values.idstiposevolucoes != 'undefined') 
{
	//console.log(form_values.idstiposevolucoes)
	$("[name=idstiposevolucoes]").val(form_values.idstiposevolucoes);
}
$("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	


	
	
	rules = {};
	rules.tipoavaliacao = "required";
rules.tipoevolucao = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setAlunosdados(obj, id){ 
	$("#box-alunos").css("display", "block");
 
	if(id){
		val = id;
	}else{
		obj = $(obj); 
    	val = obj.val();
	}

    
    var elemento; 
        $.post(baseUrl + '/admin/educacaoespecialavaliacoes/obterprofessoresdosalunos', {idaluno: val}, function(aluno){
			aluno = JSON.parse(aluno);   	
				 
				 elemento = '<ul class="list-group" style="width: 50%;">'    
				 				+'<li class="list-group-item"> Escola Regular: ' +  aluno.aee.escolaregular +'</li>' 
								+'<li class="list-group-item"> Professor Regular: ' +  aluno.professor.nome +'</li>'
								+'<li class="list-group-item"> Escola Aee: ' +  aluno.escola +'</li>'
								+'<li class="list-group-item"> Professor Aee: ' +  aluno.professorAee.nome +'</li>'
								+'<input name="escolaaee" value="'+ aluno.escola +'" type="hidden" />'
								+'<input name="nomealuno" value="'+ aluno.nomealuno +'" type="hidden" />'
							+'</ul>';
 
                 $('#box-alunos').html(elemento); 
        }); 
 } 