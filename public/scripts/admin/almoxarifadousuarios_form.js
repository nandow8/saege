var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
   unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
   unsaved = true;
});

jQuery(document).ready(function() {
    window.onbeforeunload = function() {
        console.log(unsaved);
        if(unsaved){
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }       
    };

    if ((typeof form_values)!='undefined') {
        if ((typeof form_values.idlocal)!='undefined') {
            $("select[name=idlocal]").val(form_values.idlocal);
            setresponsaveis(form_values.idlocal);
        }
        if ((typeof form_values.idfuncionario)!='undefined') $("select[name=idfuncionario]").val(form_values.idfuncionario);
        if ((typeof form_values.perfil)!='undefined') $("select[name=perfil]").val(form_values.perfil);
        if ((typeof form_values.status)!='undefined') $("select[name=status]").val(form_values.status);

        $("select").trigger("chosen:updated");
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
        
        $("select").each(function (){
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });       
    }

    jQuery('select, input, textarea').bind('blur', function() { });
    
    $('.click_acc').each(function() {
        $( this ).click();
    });
    
    rules = {};
    rules.idlocal = "required";
    rules.idfuncionario = "required";
    rules.perfil = "required";
    rules.status = "required";

    messages = {};
    
    $("#form").validate({
        ignore: [],
        rules: rules,         
        messages: messages,
        submitHandler: function (form) {
            unsaved = false;
            $.blockUI({ message: '<h1> Aguarde...</h1>' });
            form.submit();
        }
    });

    $('#idestoque').change(function () {
        var idestoque = $(this).val();
        var idtipoestoque = $(this).find('option:selected').data('perfil');

        setfuncionarios(idestoque);
        setperfis(idtipoestoque);
    });
});

function setfuncionarios(idestoque) {

    data = '';
    $.post(baseUrl + '/admin/almoxarifadousuarios/setfuncionarios/', {idestoque: idestoque}, function (data) {
        $("select[name=idfuncionario]").val(null).trigger('change');
        $("select[name=idfuncionario]").html(data);
    });
}

function setperfis(idtipoestoque = 0) {

    data = '';
    $.post(baseUrl + '/admin/almoxarifadousuarios/setperfis/', {idtipoestoque: idtipoestoque}, function(data) {
        $("select[name=idperfil]").val(null).trigger('change');
        $("select[name=idperfil]").html(data);
    });
}