var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idfuncionario]").val(form_values.idfuncionario);
		$("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}

	$("select[name=idfuncionario]").change(function() {
		var idfuncionario = $(this).val();

		preencheDadosFunc(idfuncionario);
	});
	
	
	// var SPMaskBehavior = function (val) {
	//   return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
	// },
	// spOptions = {
	//   onKeyPress: function(val, e, field, options) {
	//       field.mask(SPMaskBehavior.apply({}, arguments), options);
	//     }
	// };

	// $('input[name=telefone]').mask(SPMaskBehavior, spOptions);

	rules = {};
	rules.idfuncionario = "required";
	rules.email = "required";
	rules.status1 = "required";
	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});

		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
	}
});

function preencheDadosFunc($idfuncionario) {

	$.post(baseUrl+'/admin/coordenacaodiretores/getdadosfunc',{idfuncionario:$idfuncionario},function(data) {
		if(data!="Funcionário não achado") {
			data = JSON.parse(data);

			console.log(data);

			$("input[name=email]").val(data.email);
			$("input[name=telefone]").val(data.telefone);
			$("select[name=status1]").val(data.status);

			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
			
			$("select").each(function (){
				div = $(this).closest('div').find('.chosen-container').css('width', '100%');
			});
		} else {
			alert(data);
		}
	});
}