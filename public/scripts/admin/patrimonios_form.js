var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		//console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	function block(type,nameElement){
			$(type+'[name='+nameElement+']').attr('readOnly', true);
			return false;
		}

	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
		$("select[name=iddepartamento]").val(form_values.iddepartamento);
		$("select[name=idsetor]").val(form_values.idsetor);
		$("select[name=idtipo]").val(form_values.idtipo);
		$("select[name=idmarca]").val(form_values.idmarca);
		$("select[name=idfornecedor]").val(form_values.idfornecedor);
		$("select[name=origem]").val(form_values.origem);
		$("select[name=iddepartamentosecretaria]").val(form_values.iddepartamentosecretaria);
		$("select[name=idresponsavelsecretaria]").val(form_values.idresponsavelsecretaria);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idresponsavelescola]").val(form_values.idresponsavelescola);
		$("select[name=aprovado]").val(form_values.aprovado);
		$("select[name=aprovacao]").val(form_values.aprovacao);
		$("select[name=statusaprovacao]").val(form_values.statusaprovacao);
		$("select[name=baixa]").val(form_values.baixa);
		$("select[name=statusbaixa]").val(form_values.statusbaixa);
		$("select[name=roubofurto]").val(form_values.roubofurto);
		$("select[name=statusroubofurto]").val(form_values.statusroubofurto);
		$("select[name=status1]").val(form_values.status1);
		$("select[name=origemaprovacao]").val(form_values.origemaprovacao);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
		
		
		if(form_values.idperfil && form_values.idperfil > 0){
			block('input','numeropatrimonio');
			block('input', 'patrimonio');
			block('textarea', 'descricoesdobem');
		}
	}
	$('input[name=vidautil]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});
	$('input[name=quantidade]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});


$("input[name=anofabricacao]").mask("9999", {placeholder:" "});

$("input[name=dataroubofurto]").mask("99/99/9999", {placeholder:" "});
$("input[name=dataroubofurto]").datepicker({
	dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
	dateFormat: 'dd/mm/yy',
	dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
	monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	currentText: 'Hoje',
	prevText: 'Antes',
	nextText: 'Depois',
	onSelect: function(date) {
    }
});

$('input[name=valordepreciacao]').priceFormat({
    prefix: '',
    centsSeparator: '',
    thousandsSeparator: '',
    centsLimit: 2
});
$('input[name=percentualdepreciacao]').priceFormat({
    prefix: '',
    centsSeparator: '',
    thousandsSeparator: '',
    centsLimit: 2
});

$("input[name=datacompra]").mask("99/99/9999", {placeholder:" "});
$("input[name=datacompra]").datepicker({
	dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
	dateFormat: 'dd/mm/yy',
	dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
	monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	currentText: 'Hoje',
	prevText: 'Antes',
	nextText: 'Depois',
	onSelect: function(date) {
    }
});

	rules = {};
	rules.idsecretaria = "required";
	rules.idsetor = "required";
	rules.idtipo = "required";
	rules.idescola = "required";
	rules.idresponsavelescola = "required";
	rules.numeropatrimonio = "required";
	rules.patrimonio = "required";
	rules.aprovado = "required";
	rules.aprovacao = "required";
	rules.statusaprovacao = "required";
	rules.status1 = "required";
	
	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});		
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function(ev) {
			
			val = $(this).val();
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
			if(ev==0){}else{
				val = $.trim(val);
				val = (val=='') ? '--' : val;
			}
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');
		});		
		
		$("label span.required").remove();
		
	}		
	
	$('input[name=atribuir]').change(function(){
		val = $(this).val();

		if(val == 'sim'){
			$('.box-departamento').fadeIn();
			$('select[name=departamento]').attr("required", true);
		}else{
			$('select[name=departamento]').val("");
			$('.box-departamento, .box-escolas').fadeOut();			
			$('select[name=codescola], select[name=departamento]').attr("required", false);
		}
	});

	$('select[name=departamento]').change(function(){
		val = $(this).val();
		if(val==29){
			$('select[name=codescola]').attr("required", true);
			$('.box-escolas').fadeIn();
		}else{
			$('select[name=codescola]').val("").attr("required", false);
			$('.box-escolas').fadeOut();
		}
	});
});