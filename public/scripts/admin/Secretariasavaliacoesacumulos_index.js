$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=e_idenderecoidestado]").val(form_values.e_idenderecoidestado);$("select[name=e_v2idenderecoidestado]").val(form_values.e_v2idenderecoidestado);$("select[name=idusuariologado]").val(form_values.idusuariologado);
$("select[name=idcargasemanal]").val(form_values.idcargasemanal);
$("select[name=v2idcargasemanal]").val(form_values.v2idcargasemanal);
$("select[name=penaslei]").val(form_values.penaslei);
$("select[name=status1]").val(form_values.status1);
$("select[name=deferido]").val(form_values.deferido);
	

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});	
	}
	
		$('input[name=idendereco]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});
	$('input[name=v2idendereco]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});

	$("input[name=datalancamento_i],input[name=datalancamento_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalancamento_i],input[name=datalancamento_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});





































	$("input[name=data_i],input[name=data_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data_i],input[name=data_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});































	$("input[name=v2data_i],input[name=v2data_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=v2data_i],input[name=v2data_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});





	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Ativo") || (data=="Bloqueado")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 