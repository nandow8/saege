var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') { 
		$("select[name=idaluno]").val(form_values.idaluno);
		$("select[name=dataAno]").val(form_values.dataAno);
		$("select[name=status1]").val(form_values.status1);

		// setalunos($("select[name=idescola]"),form_values.idaluno);

		 
		if ((typeof form_values.idstiposevolucoes)!='undefined') {
			var values = form_values.idstiposevolucoes.split(',');
			$("[name='idstiposevolucoes[]']").val(values); 

			console.log(values);
		} 
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	verificacampoaluno = $("select[name=idaluno]").val();
	if(verificacampoaluno != ""){
		$('#resetfield').prop('disabled', false);
	}else{
		$('#resetfield').prop('disabled', 'disabled');
	}

	$("input[name=dataAno]").mask("9999", {placeholder:" "}); 
	
	rules = {}; 
	rules.idaluno = "required"; 
	rules.status1 = "required";
	rules.dataAno = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}	

});
 

function setAlunosMaterias(obj, id){ 
	$("#box-alunos").css("display", "block");
	$('#resetfield').prop('disabled', false);
 
	if(id){
		val = id;
	}else{
		obj = $(obj); 
    	val = obj.val();
	} 
    
    var elemento; 
        $.post(baseUrl + '/admin/educacaoespecialcontrolefrequencias/aeelistamaterias', {id: val}, function(diasmaterias){
			diasmaterias = JSON.parse(diasmaterias);   
			
			elemento += '<div id="alinha-tabela">'
						+'<table  style="width:50%">' 
								+'<tr><th colspan="5">Aulas</th></tr>'						
								+'<tr>'
									+ '<th>Segunda-feira</th>'
									+ '<th>Terça-feira</th>'
									+ '<th>Quarta-feira</th>'
									+ '<th>Quinta-feira</th> '
									+ '<th>Sexta-feira</th>'
								+'</tr>' ; 
								
			for (let index = 0; index < Object.keys(diasmaterias.aeelistamaterias).length; index++) { 
				elemento += '<tr>'
								+ '<td>'+ diasmaterias.aeelistamaterias[index].segundafeira +'</td>'
								+ '<td>'+ diasmaterias.aeelistamaterias[index].tercafeira +'</td>'
								+ '<td>'+ diasmaterias.aeelistamaterias[index].quartafeira +'</td>'
								+ '<td>'+ diasmaterias.aeelistamaterias[index].quintafeira +'</td> '
								+ '<td>'+ diasmaterias.aeelistamaterias[index].sextafeira +'</td>'
							+'</tr>';
			}	
					 
			elemento += '</table>'
						+'</div>'  ;

			$('#box-diasmaterias').html(elemento); 
        }); 
 } 

function setAlunosdados(obj, id){ 
	$("#box-alunos").css("display", "block");
	$('#resetfield').prop('disabled', false);
 
	if(id){
		val = id;
	}else{
		obj = $(obj); 
    	val = obj.val();
	} 
    
    var elemento; 
        $.post(baseUrl + '/admin/educacaoespecialcontrolefrequencias/obterprofessoresdosalunos', {idaluno: val}, function(aluno){
			aluno = JSON.parse(aluno);   
				 elemento = '<ul class="list-group" style="width: 50%;">'    
				 				+'<li class="list-group-item"> Escola Regular: ' +  aluno.aee.escolaregular +'</li>'
								+'<li class="list-group-item"> Professor Regular: ' +  aluno.professor.nome +'</li>'
								+'<li class="list-group-item"> Escola Aee: ' +  aluno.escola +'</li>'
								+'<li class="list-group-item"> Professor Aee: ' +  aluno.professorAee.nome +'</li>'
								+'<input type="hidden" value="'+ aluno.aee.idescola +'" name="idescola">'
							+'</ul>';
 
                 $('#box-alunos').html(elemento); 
        }); 
 } 

 function selecionadialetivo(dia, mes,  dia365 ){
	var palavra365 = '#dia' + dia365;  
	var dialetivo365 = '#dialetivo' + dia365;  
	var cordialetivo365 = '#cordialetivo' + dia365;  

	if($(palavra365).hasClass('marcado')){
		$(palavra365).removeClass('marcado');
		$(palavra365).css({"background-color": "red", "color": "white"});
		$(dialetivo365).val('falta'); 
		$(cordialetivo365).val('falta'); 
		console.log('verm: ' + dialetivo365 + ' cordialetivo: ' + cordialetivo365 )
	}else{
		$(palavra365).addClass('marcado');
		$(palavra365).css({"background-color": "lightgreen", "color": "black"});
		$(dialetivo365).val(dia+ '' +mes); 
		$(cordialetivo365).val(dia+ '' +mes);
		console.log('verde: ' + dialetivo365 + ' cordialetivo: ' + cordialetivo365 )
	}

}

function selecionadiafalta(dia, mes,  dia365 ){
	// console.log("dia: " + dia + " mes: " + mes + " dia365: " + dia365)

		var palavra365 = '#dia' + dia365;  
		var dialetivo365 = '#dialetivo' + dia365;  
		var cordialetivo365 = '#cordialetivo' + dia365;  

		$(palavra365).removeClass('marcado');
		$(palavra365).css({"background-color": "#FFF", "color": "#555"});
		$(dialetivo365).val(''); 
		$(cordialetivo365).val(''); 
		console.log('branco: ' + dialetivo365 + ' cordialetivo: ' + cordialetivo365 )
}

function verificaano(obj){
	obj = $(obj); 
	ano = obj.val();
	idaluno = $("select[name=idaluno]").val(); 

	$.post(baseUrl + '/admin/educacaoespecialcontrolefrequencias/somenteumcalendarioporaluno', {idaluno: idaluno}, function(aluno){
		aluno = JSON.parse(aluno);

		if(aluno.dataAno == ano){ 
			$("#resetfield").val("");
		 
			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});

			$("#adderror").append('<span style="color: red" id="spanapend">O aluno(a) ' + aluno.nomealuno  + ' já possui um calendario em ' + ano + '</span>')
		}else{
			$('#adderror #spanapend').remove();
		}
	});
}

function setSalas(obj, idsala){
	var val = $(obj).val();
	
	data = '';

	$.post(baseUrl + '/admin/'+controller+'/adicionarcalendario/', {idescola:val, dataAno: val}, function(data) {
		$("#calendario").html(data);
	});
}