var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	 
	$("#simPeriodos").click(function () {
		$("#periodosdiv").hide();
	});
	$("#naoPeriodos").click(function () {
		$("#periodosdiv").show();
	});
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		jQuery("select[name=idlocal]").val(form_values.idlocal);
                $("select[name=periodos]").val(form_values.periodos);
		$("select[name=idperiodos]").val(form_values.idperiodos);
		$("select[name=simPeriodos]").val(form_values.simPeriodos);
		$("select[name=naoPeriodos]").val(form_values.naoPeriodos);
		$("select[name=motivos]").val(form_values.motivos);
                if ((typeof form_values.periodos)!='undefined') setPeriodos(jQuery("select[name=idescola]").get(0), form_values.periodos);
		
		if((typeof form_values.periodos)!='undefined')
		{
			//alert(form_values.periodos)
			$("[name='periodos[]']").val(form_values.periodos);
		}		
		if((typeof form_values.motivos)!='undefined')
		{
			$("[name='motivos[]']").val(form_values.motivos);
		}		

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
		$("input[name=datalancamento]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalancamento]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});





	
	
	rules = {};
	rules.datalancamento = "required";
rules.idusuario = "required";
rules.idlocal = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


function setPeriodos(obj, periodos){
        setSeries(obj, 0);
	var val = $(obj).val();
	
	data = '';
	$("select[name=periodos]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/nutricaomerendasconsumos/setperiodos/', {idescola:val}, function(data) {
		$("select[name=periodos]").html(data);
		$("select[name=periodos]").val(periodos);
		console.log(periodos);
		val = $("select[name=periodos]").find('option:selected').html();
		$("select[name=periodos]").closest('div').find('span').html(val);
	
		$("select[name=periodos]").trigger("chosen:updated");
		
		if(periodos==""){
			$("select[name=periodos]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setSeries(obj, idserie){
	//setPeriodos(obj, idserie);
	var val = $(obj).val();

	data = '';
	$("select[name=idserie]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/nutricaomerendasconsumos/setseries/', {idescola:val, action:action}, function(data) {
		$("#box-series").html(data);
	});
}

