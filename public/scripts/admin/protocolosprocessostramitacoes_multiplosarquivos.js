﻿$('#btn_fileupload_arquivos').fileupload({
    dataType: 'json',
    done: function (e, data) {
    	idx = data.result[0].idx;
    	idarquivo = data.result[0].idarquivo;
    	    	
    	$("#multiplos_arquivos li.container_arquivos").eq(idx).find("div:first").html(getViewArquivos(idarquivo, data.files[0].name));
    	
        resetPaginas();
		aplicaDragAndDrop();
    }
});

$('#btn_fileupload_arquivos').bind('fileuploadprogress', function (e, data) {
	var progress = parseInt(data.loaded / data.total * 100, 10);
    idx = data.formData.idx;
    $("#multiplos_arquivos li.container_arquivos").eq(idx).find('div.progresso div').css('width', progress + '%');
});

$('#btn_fileupload_arquivos').bind('fileuploadsubmit', function (e, data) {
	setMultiplos_arquivos(0, false);
    data.formData = {idx: $("#multiplos_arquivos li.container_arquivos").length-1};
});	

function resetPaginas() {
	if ($("#imo_table-arquivos tr").not('.nenhum').length==0) {
		setMultiplos_arquivos(false, false);
	} else {
		$("#imo_table-arquivos tr.nenhum").remove();
	}
	
    c = 0;
	$("#imo_table-arquivos tr").not('.nenhum').each(function() {
    	c++;
    	pagina = (c<=9) ? '0' + c : c;
    	$(this).find('td:first').attr('align', 'center');
    	$(this).find('td:first').html(pagina);
    });

}

function setMultiplos_arquivos(idarquivo, filename, legenda) {
	var first = "";
	if(count_li == 0){
		first = "first";
		count_li = 1;
	}	
	
	if (idarquivo===false) {
		return;
	}
	
	legenda = ((typeof legenda)=='undefined') ? '' : legenda;
	
	li = $('<li class="'+first+' container_arquivos" />');
    if (filename===false) {
    	li.append('<div class="box_arquivo"><div class="progresso" style="border: 1px solid #F99B16; background-color: #F99B16; height: 21px; left: 0px; position: relative; top: 65px; width: 185px;"><div style="background: #0a0; width:0%; height: 21px"></div></div></div>');
    } else {
    	li.append($("<div class='box_arquivo'>" + getViewArquivos(idarquivo, filename, legenda) + "</div>"));
    }
    ul = $('<ul class="grid-actions" />');
    ul.append('<li class="fechar"><a onclick="excluir_arquivos(this)" href="javascript:;" title="Delete" class="with-tip"><img src="' + baseUrl + '/public/admin/imagens/fugue/cross-circle.png" width="16" height="16"></a></li>');
    li.append(ul);
    
	$("#multiplos_arquivos ul:first").append(li);
}

function getViewArquivos(idarquivo, filename, legenda) {
	legenda = ((typeof legenda)=='undefined') ? '' : legenda;
	
	input = '<input type="hidden" name="idsarquivos[]" value="' + idarquivo + '" /><input type="text" name="legendasarquivos[]" value="'+legenda+'" class="legenda_fileupload" />';
	arquivo = '<div class="box-imagem"><a target="_blank" href="'+baseUrl+'/arquivos/get/id/'+idarquivo+'" style="color: #000088"><img alt="" src="' + baseUrl + '/public/default/imagens/imgtext.gif" width="60"></a></div>';
	return arquivo + input;
}

function excluir_arquivos(obj) {
	$(obj).parent().parent().parent().remove();
	resetPaginas();
}

function aplicaDragAndDrop() {
	$(".container_arquivos").draggable(
			{cursor: 'pointer', 
			helper: 'clone', 
			opacity: 0.75});
	$(".container_arquivos").droppable(
			{accept: '.container_arquivos',
	      	drop: function(event, ui) { 
		    	elmTo = jQuery(this);
		    	elmFrom = ui.draggable;
		    	
		    	elmTo.swap(elmFrom);	 
		    	resetPaginas();
		    }
    });	
}		

