$(document).ready(function () {
    if ((typeof form_values) != 'undefined') {
        $("select[name=lgtpmensagem]").val(form_values.lgtpmensagem);

        $("select").each(function () {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
    }

    $("input[name=data_i],input[name=data_f]").mask("99/99/9999", {placeholder: " "});
    $("input[name=data_i],input[name=data_f]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onClose: function () {
            var er = /^(((0[1-9]|[12][0-9]|3[01])([-.\/])(0[13578]|10|12)([-.\/])(\d{4}))|(([0][1-9]|[12][0-9]|30)([-.\/])(0[469]|11)([-.\/])(\d{4}))|((0[1-9]|1[0-9]|2[0-8])([-.\/])(02)([-.\/])(\d{4}))|((29)(\.|-|\/)(02)([-.\/])([02468][048]00))|((29)([-.\/])(02)([-.\/])([13579][26]00))|((29)([-.\/])(02)([-.\/])([0-9][0-9][0][48]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][2468][048]))|((29)([-.\/])(02)([-.\/])([0-9][0-9][13579][26])))$/;
            
            if (!er.test(this.value)) {
                jAlert('A data informada, "' + this.value + '" é inválida.', 'Atenção' );
                this.value = "";
            }
        }
    });

});



