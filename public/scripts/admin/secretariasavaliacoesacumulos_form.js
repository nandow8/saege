var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=e_idenderecoidestado]").val(form_values.e_idenderecoidestado);$("select[name=e_v2idenderecoidestado]").val(form_values.e_v2idenderecoidestado);$("select[name=idusuariologado]").val(form_values.idusuariologado);
$("select[name=idcargasemanal]").val(form_values.idcargasemanal);
$("select[name=v2idcargasemanal]").val(form_values.v2idcargasemanal);
$("[name=penaslei]").val(form_values.penaslei);
$("select[name=status1]").val(form_values.status1);
$("select[name=deferido]").val(form_values.deferido);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
		$('input[name=idendereco]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});
	$('input[name=v2idendereco]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});

	$("input[name=datalancamento]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalancamento]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});



	$("input[name=horariotrabalho]").mask("9999", {placeholder:" "});
	$("input[name=segundahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=segundahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=segundahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=segundahorafim2]").mask("99:99", {placeholder:" "});		

	$("input[name=tercahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=tercahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=tercahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=tercahorafim2]").mask("99:99", {placeholder:" "});

	$("input[name=quartahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=quartahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=quartahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=quartahorafim2]").mask("99:99", {placeholder:" "});

	$("input[name=quintahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=quintahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=quintahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=quintahorafim2]").mask("99:99", {placeholder:" "});

	$("input[name=sextahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=sextahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=sextahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=sextahorafim2]").mask("99:99", {placeholder:" "});


	$("input[name=htpcsemanahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=htpcsemanahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=htpcsemanahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=htpcsemanahorafim2]").mask("99:99", {placeholder:" "});







	$("input[name=v2horariotrabalho]").mask("9999", {placeholder:" "});
	$("input[name=v2segundahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=v2segundahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=v2segundahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=v2segundahorafim2]").mask("99:99", {placeholder:" "});	
	$("input[name=v2tercahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=v2tercahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=v2tercahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=v2tercahorafim2]").mask("99:99", {placeholder:" "});
	$("input[name=v2quartahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=v2quartahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=v2quartahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=v2quartahorafim2]").mask("99:99", {placeholder:" "});
	$("input[name=v2quintahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=v2quintahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=v2quintahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=v2quintahorafim2]").mask("99:99", {placeholder:" "});
	$("input[name=v2sextahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=v2sextahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=v2sextahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=v2sextahorafim2]").mask("99:99", {placeholder:" "});
	$("input[name=v2htpcsemanahorainicio1]").mask("99:99", {placeholder:" "});
	$("input[name=v2htpcsemanahorafim1]").mask("99:99", {placeholder:" "});
	$("input[name=v2htpcsemanahorainicio2]").mask("99:99", {placeholder:" "});
	$("input[name=v2htpcsemanahorafim2]").mask("99:99", {placeholder:" "});


	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});































	$("input[name=v2data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=v2data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});




	
	
	rules = {};
	rules.nome = "required";
rules.cargo = "required";
rules.penaslei = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});