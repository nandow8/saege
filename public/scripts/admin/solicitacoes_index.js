$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
$("select[name=idusuariocriacao]").val(form_values.idusuariocriacao);
$("select[name=iddepartamentosecretaria]").val(form_values.iddepartamentosecretaria);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=origem]").val(form_values.origem);
$("select[name=destinatario]").val(form_values.destinatario);
$("select[name=status1]").val(form_values.status1);
	
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});		
	}
	













		
	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
 	$.post(baseUrl + "/"+controller+"/changestatusxml",
 	 	 	{id: $id, op: controller}, function(data) {
			if ((data=="Ativo") || (data=="Bloqueado")) {
				
				$.get(baseUrl + '/' + controller, {}, function(r) {
					$("table.table-content tbody").html($(r).find('table.table-content tbody').html());
					$.unblockUI();
				});
				
				
			} else {
				$.unblockUI();
				jAlert(data, "Alerta!");
			}
 	});
} 