var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idtipo]").val(form_values.idtipo);
                $("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	$("input[name=datalancamento]").mask("99/99/9999", {placeholder:" "});
        if (((typeof form_values)!='undefined') && (form_values.status1 !== 'Aprovado') && (form_values.status1 !== 'Finalizado')){
            $("input[name=datalancamento]").datepicker({
                    dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                    dateFormat: 'dd/mm/yy',
                    dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                    currentText: 'Hoje',
                    prevText: 'Antes',
                    nextText: 'Depois',
                    onSelect: function(date) {
                }
            });
        }



	$("input[name=dataof]").mask("99/99/9999", {placeholder:" "});
        if (((typeof form_values)!='undefined') && (form_values.status1 !== 'Aprovado') && (form_values.status1 !== 'Finalizado')){
            $("input[name=dataof]").datepicker({
                    dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                    dateFormat: 'dd/mm/yy',
                    dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                    currentText: 'Hoje',
                    prevText: 'Antes',
                    nextText: 'Depois',
                    onSelect: function(date) {
                }
            });
        }
        
	$("input[name=dataproc]").mask("99/99/9999", {placeholder:" "});
        if (((typeof form_values)!='undefined') && (form_values.status1 !== 'Aprovado') && (form_values.status1 !== 'Finalizado')){
            $("input[name=dataproc]").datepicker({
                    dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                    dateFormat: 'dd/mm/yy',
                    dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                    currentText: 'Hoje',
                    prevText: 'Antes',
                    nextText: 'Depois',
                    onSelect: function(date) {
                }
            });
        }
        
	$("input[name=datareinteracao]").mask("99/99/9999", {placeholder:" "});
        if (((typeof form_values)!='undefined') && (form_values.status1 !== 'Aprovado') && (form_values.status1 !== 'Finalizado')){
            $("input[name=datareinteracao]").datepicker({
                    dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                    dateFormat: 'dd/mm/yy',
                    dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                    currentText: 'Hoje',
                    prevText: 'Antes',
                    nextText: 'Depois',
                    onSelect: function(date) {
                }
            });
        }
        
	$("input[name=datadevolucao]").mask("99/99/9999", {placeholder:" "});
        if (((typeof form_values)!='undefined') && (form_values.status1 !== 'Aprovado') && (form_values.status1 !== 'Finalizado')){
            $("input[name=datadevolucao]").datepicker({
                    dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                    dateFormat: 'dd/mm/yy',
                    dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                    currentText: 'Hoje',
                    prevText: 'Antes',
                    nextText: 'Depois',
                    onSelect: function(date) {
                }
            });
        }


	rules = {};
	rules.datalancamento = "required";
        rules.idtipo = "required";
        rules.numero = "required";
        rules.titulo = "required";
        rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (((typeof form_values)!='undefined') && visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).hide();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
        
        setEncaminharCI($("input[name=cienvioflag]"));
        setRetornarCI($("input[name=ciretornoflag]"));

});

function setEncaminharCI($obj){

	var value = $($obj).val();

	console.log(value)

	if($($obj).is(':checked') && value == 'sim')
	{
		$('.content-campos-cienvio').show();
	}
	else
	{
		$('.content-campos-cienvio').hide();
	}

}

function setRetornarCI($obj){

	var value = $($obj).val();

	console.log(value)

	if($($obj).is(':checked') && value == 'sim')
	{
		$('.content-campos-ciretorno').show();
	}
	else
	{
		$('.content-campos-ciretorno').hide();
	}

}

function setExibir(envio = 'nao', retorno = 'nao'){
    if (envio === 'sim'){
        $('.content-campos-cienvio').show();
    }else{
        $('.content-campos-cienvio').hide();
    }
    
    if (retorno === 'sim'){
        $('.content-campos-ciretorno').show();
    }else{
        $('.content-campos-ciretorno').hide();
    }
}        
