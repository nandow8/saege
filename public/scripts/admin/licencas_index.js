$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=status1]").val(form_values.status1);	
		
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});		
	}
	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/admin/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/admin/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
 	$.post(baseUrl + "/admin/"+controller+"/changestatusxml",
 	 	 	{id: $id, op: controller}, function(data) {
			if ((data=="Ativo") || (data=="Bloqueado")) {
				
				$.get(baseUrl + '/admin/' + controller, {}, function(r) {
					$("table.table-content tbody").html($(r).find('table.table-content tbody').html());
					$.unblockUI();
				});
				
				
			} else {
				$.unblockUI();
				jAlert(data, "Alerta!");
			}
 	});
} 

function modalLicenca($idto)
{
	url = baseUrl + '/admin/licencas/index/modal/true/id/' + $idto;
	
	$.fancybox({
		'padding'		: 0,
		'autoScale'		: false,
		'transitionIn'	: 'none',
		'transitionOut'	: 'none',
		'title'			: "Clonar Licença",
		'width'			: 880,
		'height'		: 600,
		'href'			: url,
		'type'			: 'iframe',
		'onComplete': function() {
			
		}
	});

	return false;	

}

function clonarLicenca($to, $parent)
{
	var row = JSON.parse($parent);
	if(row && row.id == $to)
	{
		jAlert("A licença a ser clonada deve ser diferente da que receberá os dados!","ERRO");
		return;
	}

	if($to && $parent)
	{
		jConfirm("Tem certeza que deseja clonar as informações desta licença (Locais úteis, Assuntos de requerimentos, Tipos de requerimentos, Setores, Subsetores e Tipos de atendimentos) ?", "Confirmar", function(r) {
			
			if(r)
			{
				$.post(baseUrl + "/admin/licencas/clonarlicenca/", {to: $to, parent: $parent}, function(data) {
					if (data!='OK') {
						jAlert(data, "Erro");
					} else {
						jAlert('Sucesso');
					}
				});
			}
			
		});
	}
}