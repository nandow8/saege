var unsaved = false;

$("input,select,textarea").change(function() { //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function() { //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
    window.onbeforeunload = function() {

        if (unsaved) {
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };

    if ((typeof form_values) != 'undefined') {

        $("select[name=idusuariologado]").val(form_values.idusuariologado);
        $("select[name=origem]").val(form_values.origem);
        setEscolas($("select[name=origem]").val(form_values.origem), form_values.idescola);
        $("select[name=idescola]").val(form_values.idescola);
        $("select[name=iddepartamentoorigem]").val(form_values.iddepartamentoorigem);
        $("select[name=idpatrimonio]").val(form_values.idpatrimonio);
        $("select[name=transferencia]").val(form_values.transferencia);
        $("select[name=origemtransferencia]").val(form_values.origemtransferencia);
        setEscolasDestino($("select[name=origem]").val(form_values.origemtransferencia), form_values.idescola);
        $("select[name=idescolatransferencia]").val(form_values.idescolatransferencia);
        $("select[name=iddepartamentodestino]").val(form_values.iddepartamentodestino);
        $("select[name=baixatransferencia]").val(form_values.baixatransferencia);
        $("select[name=status1]").val(form_values.status1);

        setDadosPatrimonio($("select[name=idpatrimonio]"));

        if (form_values.iddepartamentodestino == 29) {
            $('select[name=idescolatransferencia]').val(form_values.idescolatransferencia);
            $('.box-escola-destino').fadeIn();
        }

        $("select[name=status1]").val(form_values.status1);
        $("label[name=statusanexo]").html(form_values.status1);
        iddoc = form_values.iddocumento;
        if (form_values.idarquivo > 0)
            $("input[name=idarquivo").attr('required', false);
        if (form_values.status1 == "Não Aceito")
            $('.campo-nao-aceito').show();

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function() {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });
    }


    $("input[name=datalancamento]").mask("99/99/9999", {placeholder: " "});
    $("input[name=datalancamento]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });


    rules = {};
    //rules.idescola = "required";
    //rules.idescolatransferencia = "required";
    rules.status1 = "required";


    messages = {};

    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function(form) {
            unsaved = false;
            $.blockUI({message: '<h1> Aguarde...</h1>'});
            form.submit();
        }
    });

    if (editar) {

        $("textarea[name=observacoes], input[name=numeroboletim]").each(function() {
            val = $(this).val();

            val = $.trim(val);
            val = (val == '') ? '' : val;

            parent = $(this).parent();
            parent.append(val);
            parent.addClass('visualizar');

            $(this).remove();
        });

        $("select[name=idpatrimonio], select[name=idescola], select[name=iddepartamentodestino], select[name=idescolatransferencia]").each(function() {
            val = $(this).find('option:selected').html();
            val = $.trim(val);
            val = (val == '') ? '' : val;

            parent = $(this).parent();
            parent.html(val);
            parent.addClass('visualizar');

        });

        $("label span.required").remove();
    }

});
$(window).load(function() {

    //visualizar = false;
    if (visualizar) {

        $("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
            val = $(this).val();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.append(val);
            parent.addClass('visualizar');

            $(this).remove();
        });

        $("div.control-group select").not('[type=hidden]').each(function() {
            val = $(this).find('option:selected').html();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.html(val);
            parent.addClass('visualizar');

        });

        $("label span.required").remove();

    }
});
function setEscolas(obj, idescola) {
    var origem = $(obj).val();
    if (origem == "Escola") {
        $('.box-escola').css('display', 'block');
        $('.box-departamento').css('display', 'none');
    } else {
        $('.box-departamento').css('display', 'block');
        $('.box-escola').css('display', 'none');
    }
}

function setEscolasDestino(obj) {
    var origem = $(obj).val();

    if (origem == 29) {
        $('.box-escola-destino').css('display', 'block');
        $('select[name=idescolatransferencia]').attr('required', true);
    } else {
        $('select[name=idescolatransferencia]').attr('required', false);
        $('.box-escola-destino').css('display', 'none');
    }
}

function setDadosPatrimonio(obj) {
    var idpatrimonio = $(obj).val();
    data = '';
    $.post(baseUrl + '/admin/patrimoniobaixabens/getdados/', {idpatrimonio: idpatrimonio}, function(data) {
        if (data == "erro") {
            $('.content-patrimonios').css('display', 'none');
            $(".box-patrimonios").html('');
        } else {
            $('.content-patrimonios').css('display', 'block');
            $(".box-patrimonios").html(data);
        }

    });
}
$('select[name=status1').bind('click', function() {
    val = $(this).val();

    if (val == 'Não Aceito') {
        $('.campo-nao-aceito').fadeIn();
        $('textarea[name=motivonegacao]').attr('required', true);
    } else {
        $('textarea[name=motivonegacao]').attr('required', false);
        $('.campo-nao-aceito').fadeOut();
    }
});
