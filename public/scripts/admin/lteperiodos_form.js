jQuery(document).ready(function() {
    if ((typeof form_values)!='undefined') {
        if ((typeof form_values.status)!='undefined') $("select[name=status]").val(form_values.status);
        if ((typeof form_values.tipo)!='undefined') {
            $("select[name=tipo]").val(form_values.tipo);
            if (form_values.tipo == 'estadual') {
                $('#div-curso').css('display', 'block');
                $('#div-bolsistas').css('display', 'block');
                $('#div-datas').css('display', 'block');
            } else {
                $('#div-curso').css('display', 'none');
                $('#div-bolsistas').css('display', 'none');
            }
        }
        if ((typeof form_values.curso)!='undefined') $("select[name=curso]").val(form_values.curso);
        if ((typeof form_values.tipotransporte)!='undefined') $("select[name=tipotransporte]").val(form_values.tipotransporte);
        if ((typeof form_values.bolsista)!='undefined' && form_values.bolsista == 'sim' || form_values.bolsista == 'on') $('#bolsista').prop('checked', true);
        if ((typeof form_values.fechado)!='undefined' && form_values.fechado == 'sim' || form_values.fechado == 'on') $('#fechado').prop('checked', true);

        $("select").trigger("chosen:updated");
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
    }

    $("[name=datainicio]").mask("99/99/9999");
    $("[name=datafim]").mask("99/99/9999");

    $( function() {
        var dateFormat = "dd/mm/yy",
        from = $( "#from" )
        .datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: dateFormat
        })
        .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
        }),
        to = $( "#to" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            dateFormat: dateFormat
        })
        .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
        });

        function getDate( element ) {
            var date;
            try {
                date = $.datepicker.parseDate( dateFormat, element.value );
            } catch( error ) {
                date = null;
            }

            return date;
        }
    });

    $("#fechado").change(function () {

        if ($('#fechado').is(':checked')) {
            jAlert('Após confirmar o fechamento não será possivel fazer alterações');
        }
    })

    $(".tipo").change(function() {
        tipo = $("select[name=tipo]").val();
        tipotransporte = $("select[name=tipotransporte]").val();

        if (tipo != '' && tipotransporte != '') {
            $("#div-datas").css('display', 'block');
            $("#from").val('');
            $("#to").val('');
            $("#diasletivos").val('');
            $('#diasletivos_').html('');
            $("#valorunitario").val('');
            $("observacoes").val();
        } else {
            $("#div-datas").css('display', 'none');
            $("#from").val('');
            $("#to").val('');
            $("#diasletivos").val('');
            $('#diasletivos_').html('');
            $("#valorunitario").val('');
            $("observacoes").val();
        }
    });

    $("#from").change(function() {
        inicio = $(this).val();
        aux = inicio.split("/");
        inicio = aux[1]+'/'+aux[0]+'/'+aux[2];
        inicio = new Date(inicio);
    });

    $("#to").change(function() {
        fim = $(this).val();
        aux = fim.split("/");
        fim = aux[1]+'/'+aux[0]+'/'+aux[2];
        fim = new Date(fim);
    });

    $(".fromto").change(function() {
        inicio = $("#from").val();
        fim = $("#to").val();
        tipo = $("#tipo").val();
        tipotransporte = $("#tipotransporte").val();
        
        if (inicio != '' && fim != '' && tipo != '' && tipotransporte != '') {
            diffDay(inicio, fim, tipo, tipotransporte);
        }
    });

    $("#diasletivos").change(function() {
        diasletivos = $("#diasletivos").val();
        inicio = $("#from").val();
        fim = $("#to").val();

        if (diffDay(inicio, fim) < diasletivos) {
            $('#diasletivos').val(diffDay(inicio, fim));
        }
    });

    $('#valorunitario').priceFormat({
        prefix: '',
        centsSeparator: '.',
        thousandsSeparator: '',
        centsLimit: 2
    });

    $('#tipo').change(function() {
        if ($(this).val() == 'estadual') {
            $('#div-curso').css('display', 'block');
            $('#div-bolsistas').css('display', 'block');
        } else {
            $('#div-curso').css('display', 'none');
            $('#div-bolsistas').css('display', 'none');
        }
    });

    rules = {};
    rules.tipo = "required";
    rules.tipotransporte = "required";
    if ($('#tipo').val() == 'estadual') rules.curso = "required";
    rules.datainicio = "required";
    rules.datafim = "required";
    rules.valorunitario = "required";
    rules.status = "required";

    messages = {};

    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function (form) {
            unsaved = false;
            $.blockUI({ message: '<h1> Aguarde...</h1>' });
            form.submit();
        }
    });
});

function diffDay(from, to, tipo, tipotransporte) {

    var diasuteis = 0;

    if ((tipo == 'municipal' || tipo == 'estadual') && (tipotransporte == 'passe' || tipotransporte == 'fretado')) {
        start = formata_data(from);
        end = formata_data(to);
        $.post(baseUrl + '/admin/ltecalendario/diasuteis/', {start: start, end: end, tipo: tipo, tipotransporte: tipotransporte}, function (data) {
            $('#diasletivos_').html(data);
            $('#diasletivos').val(data);
        }); 
    }


}

//recebe no formato dd/mm/yy e retorna yy/mm/dd
function formata_data(data) {
    aux = data.split("/");
    data = aux[2]+'-'+aux[1]+'-'+aux[0];
    return (data);
}

function setEntidades(obj) {
    var idperiodo = parseInt(form_values.idperiodo);
    var identidade = $(obj).val();

    data = '';
    $.post(baseUrl + '/admin/lteperiodos/setentidades/', {identidade: identidade, idperiodo: idperiodo}, function (data) {
        $("#box-entidades").html(data);
    });
}