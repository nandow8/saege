$(document).ready(function() {

	//carregaValores();
	//retornaDistancias();
	// alert('teste');

	$("input[name=datanascimento_i],input[name=datanascimento_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datanascimento_i],input[name=datanascimento_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
		}
	}); 

	$("select[name=aluno]").val(form_values.aluno);
	$("select[name=aluno]").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});

	$("select[name=distancia_tipo]").val(form_values.aluno);
	$("select[name=distancia_tipo]").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});


	
	if ((typeof form_values)!='undefined') {

		// $("select[name=idcentralvaga]").val(form_values.idcentralvaga);
		// $("select[name=sexo]").val(form_values.sexo);
		// $("select[name=filiacao1]").val(form_values.filiacao1);
		// $("select[name=filiacao2]").val(form_values.filiacao2);
		// $("select[name=temnomesocial]").val(form_values.temnomesocial);
		// $("select[name=status1]").val(form_values.status1);
		// $("select[name=datadecorte]").val(form_values.datadecorte);
		// $("select[name=matricula]").val(form_values.matricula);
 
	}//end if

});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		  if ((data == "Retirado") || (data == "Ativo")) {
			  $obj.html(data);
			  if((data == "Ativo")){
				$("#" + $id).removeClass("alert alert-warning");
			  }else{
				$("#" + $id).addClass("alert alert-warning");
			  }
			  
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 


function modalgoogledistance(id, aluno, latitude, longitude){

	$('#exampleModalLabel').html(aluno);

	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 14,
		zoomControl: true,
		mapTypeControl: true,
		scaleControl: false,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: true,
		center: {lat: parseFloat(latitude), lng: parseFloat(longitude)},
		
	  });// end map google

	  var marker = new google.maps.Marker({
			position: map.getCenter(),
			animation: google.maps.Animation.DROP,
			icon: {
				icon: null,
				scale: 3
			},
			draggable: false,
			map: map,
			label: { text: aluno },
		});

	// 	var infoWindow = new google.maps.InfoWindow;
            
	// 	marker.addListener('mouseout', function() {
	// 		//alert(infoWindow);
	// 		Lat = marker.position.lat();
	// 		Lgn = marker.position.lng() ;
	// 		$("#latitude").val(Lat);
	// 		$("#longitude").val(Lgn);
	// 		//alert(Lat + " - " + Lgn);
	// 	});
		


	$('#exampleModal').modal({
        show: 'false'
    }); 

}//end function modalgoogledistance