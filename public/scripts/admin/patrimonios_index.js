$(document).ready(function() {
    if ((typeof form_values) != 'undefined') {
        $("select[name=idsecretaria]").val(form_values.idsecretaria);
        $("select[name=iddepartamento]").val(form_values.iddepartamento);
        $("select[name=idsetor]").val(form_values.idsetor);
        $("select[name=idtipo]").val(form_values.idtipo);
        $("select[name=idmarca]").val(form_values.idmarca);
        $("select[name=idfornecedor]").val(form_values.idfornecedor);
        $("select[name=origem]").val(form_values.origem);
        $("select[name=iddepartamentosecretaria]").val(form_values.iddepartamentosecretaria);
        $("select[name=idresponsavelsecretaria]").val(form_values.idresponsavelsecretaria);
        $("select[name=idescola]").val(form_values.idescola);
        $("select[name=idresponsavelescola]").val(form_values.idresponsavelescola);
        $("select[name=aprovado]").val(form_values.aprovado);
        $("select[name=aprovacao]").val(form_values.aprovacao);
        $("select[name=statusaprovacao]").val(form_values.statusaprovacao);
        $("select[name=baixa]").val(form_values.baixa);
        $("select[name=statusbaixa]").val(form_values.statusbaixa);
        $("select[name=roubofurto]").val(form_values.roubofurto);
        $("select[name=statusroubofurto]").val(form_values.statusroubofurto);
        $("select[name=status1]").val(form_values.status1);
        $("select[name=origemaprovacao]").val(form_values.origemaprovacao);
        $("select[name=idperfil]").val(form_values.idperfil);
        $("select[name=atribuido]").val(form_values.atribuido);
        $("select[name=codescola]").val(form_values.codescola);

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        if (form_values.idperfil == 29)
            $('.box-escolas').fadeIn();

        /*if (form_values.atribuido == "atribuido") {
         $('.box-departamento').fadeIn()
         } else {
         $('select[name=idperfil], select[name=codescola').val("");
         $('.box-escolas').fadeOut();
         }*/

        /*$('select[name=atribuido]').change(function() {
         val = $(this).val();

         if (val == 'atribuido') {
         $('.box-departamento').fadeIn();
         } else {
         $('select[name=idperfil], select[name=codescola').val("");
         $('.box-departamento, .box-escolas').fadeOut();
         }

         });*/

        $('select[name=idperfil]').change(function() {
            val = $(this).val();
            if (val == 29) {
                $('.box-escolas').fadeIn();
            } else {
                $('select[name=codescola]').val("");
                $('.box-escolas').fadeOut();
            }
        });

    }
    $('input[name=anofabricacao]').priceFormat({
        prefix: '',
        centsSeparator: '',
        thousandsSeparator: '',
        centsLimit: 0
    });
    $('input[name=vidautil]').priceFormat({
        prefix: '',
        centsSeparator: '',
        thousandsSeparator: '',
        centsLimit: 0
    });
    $('input[name=quantidade]').priceFormat({
        prefix: '',
        centsSeparator: '',
        thousandsSeparator: '',
        centsLimit: 0
    });

    $("input[name=dataroubofurto_i],input[name=dataroubofurto_f]").mask("99/99/9999", {placeholder: " "});
    $("input[name=dataroubofurto_i],input[name=dataroubofurto_f]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });

    $('input[name=valordepreciacao]').priceFormat({
        prefix: '',
        centsSeparator: '',
        thousandsSeparator: '',
        centsLimit: 2
    });
    $('input[name=percentualdepreciacao]').priceFormat({
        prefix: '',
        centsSeparator: '',
        thousandsSeparator: '',
        centsLimit: 2
    });
    $("input[name=datacompra_i],input[name=datacompra_f]").mask("99/99/9999", {placeholder: " "});
    $("input[name=datacompra_i],input[name=datacompra_f]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });

});

function confirmDelete($id, $texto) {
    jConfirm('Confirmar a exclusão de "' + $texto + '"?', 'Excluir registro', function(r) {
        if (r) {
            $.post(baseUrl + "/" + module + "/" + controller + "/excluirxml",
                    {id: $id}, function(data) {
                if (data == "OK") {
                    window.location = baseUrl + "/" + module + '/' + controller;
                } else {
                    jAlert(data, "Erro!");
                }
            });
        }
    });
}

function statusChange($id, $obj) {
    $obj = $($obj);

    $.post(baseUrl + "/" + module + "/" + controller + "/changestatusxml",
            {id: $id, op: controller}, function(data) {
        if ((data == "Ativo") || (data == "Bloqueado")) {
            $obj.html(data);
        } else {
            jAlert(data, "Alerta!");
        }
    });
}
