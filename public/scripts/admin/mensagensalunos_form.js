var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idaluno]").val(form_values.idaluno);
		$("select[name=serieturmaid]").val(form_values.serieturmaid);
		$("select[name=status1]").val(form_values.status1);

		setAlunos($("select[name=idaluno]"), form_values.idaluno);

		if ((typeof form_values.idaluno) != 'undefined') {
            setAlunos($("select[name=idperfil]"), form_values.idaluno);
        }

		if ((typeof form_values.serieturmaid)!='undefined')
		{
			setAlunos($("select[name=serieturmaid]"), form_values.idaluno);
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	



	
	
	rules = {};
	rules.idaluno = "required";
rules.titulo = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


function setAlunos(obj, idaluno){
	var serieturmaid = $(obj).val();
	var idescola = $("#idescola").val();
 
	data = '';
	if(serieturmaid !=  undefined)
	{		
		$.blockUI({message: '<h1> Aguarde...</h1>'});
		$("#alunos-container").html("Aguarde!");	
		$.post(baseUrl + '/admin/'+controller+'/setalunos/', {serieturmaid:serieturmaid, idescola: idescola}, function(data) {
			$("#alunos-container").html(data); 
			
			if (!visualizar)
			{
				$("#alunos-container").append('<p id="marcacao"><div id="marcar" class="btn btn-info" style="cursor: pointer" onclick="setandotudo(this.id)" >Marcar todos</div></p><br>');
			}
			 
			
			$.unblockUI();
		});
	}else {
		$("#alunos-container").html('Selecione uma série');
	}
}

function setandotudo(id) {

	document.getElementById(id).remove();


	if (id == "marcar") {
		$(".elemento").prop("checked", true);
		document.getElementById('marcacao').innerHTML = '<div id="desmarcar" class="btn btn-info" style="cursor: pointer" onclick="setandotudo(this.id)" >Desmarcar todos</div>';
	} else {
		$(".elemento").prop("checked", false);
		document.getElementById('marcacao').innerHTML = '<div id="marcar" class="btn btn-info" style="cursor: pointer" onclick="setandotudo(this.id)" >Marcar todos</div>';
	}//end if else



}//end function setando tudo
