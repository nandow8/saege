var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=id]").val(form_values.idselect);
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
		$("select[name=idfuncaosecretaria]").val(form_values.idfuncaosecretaria);
$("select[name=tipo]").val(form_values.tipo);
$("select[name=modulo]").val(form_values.modulo);
$("select[name=idtipoequipe]").val(form_values.idtipoequipe);
$("select[name=e_idenderecoidestado]").val(form_values.e_idenderecoidestado);$("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	


	//$("input[name=telefone]").mask("(99)?99999-999", {placeholder:" "});

	jQuery("[name=telefone]").mask("(99) 9999-9999?9").focusout(function (event){  
            var target, phone, element;  
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
            phone = target.value.replace(/\D/g, '');
            element = $(target);  
            element.unmask();  
            if(phone.length > 10) {  
                element.mask("(99) 99999-999?9");  
            } else {  
                element.mask("(99) 9999-9999?9");  
            }  
        });




	$('input[name=idendereco]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});

	
	
	rules = {};
	rules.nome = "required";
	if(controller == "supervisaotecnicos"){
		rules.idtipoequipe = "required";
	}
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setDados(obj){
	var $id = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/getdados", {id: $id}, function(data) {		
		myobj = JSON.parse(data);
		$('input[name=telefone]').val(myobj.telefone);
		$("select[name=status1]").val(myobj.status);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		$("input[name=telefone]").mask("(99)9?9999-9999", {placeholder:" "});
	});
}