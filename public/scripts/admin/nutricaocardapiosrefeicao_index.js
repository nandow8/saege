$(document).ready(function() {
	if ((typeof form_values)!='undefined') {

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});	
	}

    $('#accordion').accordion({ collapsible: true });
	
});

$("select[name=selectcalendario]").val(selecionado);

$("select").each(function() {
	val = $(this).find('option:selected').html();
	$(this).closest('div').find('span').html(val);
});	

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}


function visualizarajax(id){

   
    document.getElementById('divForm-id').innerHTML = id;

    $.post(baseUrl + "/" + module + "/"+controller+"/retornarefeicaoajax",
    {id: id}, function(data){
        lancamento = JSON.parse(data);
 
        console.log(data);
        document.getElementById('divForm-id').innerHTML = lancamento.refeicao;
        document.getElementById('divForm-ensino').innerHTML = lancamento.tipoensino;
        document.getElementById('divForm-refeicao').innerHTML = lancamento.tiporefeicao;
      
        var refeicoes = lancamento.idsrefeicoes.split(',');
        var table = $('[name=divForm-refeicoes]');
        table.html('');

        $.each(refeicoes, function( index, value ){
            var idrefeicao = value;
            $.post(baseUrl + "/" + module + "/"+controller+"/retornarefeicoesajax",
            {id: idrefeicao}, function(data){
                
                var refDescr = JSON.parse(data);
                
                table.append('<tr><td>' + refDescr.id + '</td><td>' + refDescr.titulo + '</td><td>' + refDescr.favorita + '</td><td>' + refDescr.porcoes + '</td><td>' + refDescr.status +'</td></tr>');

            });
            
        });
    });

    $.fancybox({
        type: 'inline',
        href: $('#divForm'),
        autoSize: true,
        helpers: {
        title: {
            type: 'float'
        }
    }
    });

 
}

function chartajax(id){ 

    //console.log('id_01', id);

    $.post(baseUrl + "/" + module + "/"+controller+"/nutricaoingredientesajaxtotais",
    {id: id}, function(msg){

        data = JSON.parse(msg);

        //console.log(data);

        /* -------------------------------------------------
                Valores a serem inseridos no Gráfico
           ------------------------------------------------- */

        var dataArray = new Array();

        var val_kcal = 0;
        var val_cho = 0;
        var val_lpd = 0;
        var val_ptn = 0;
        var val_fibra = 0;
        var val_vit_a = 0;
        var val_vit_c = 0;
        var val_fe = 0;
        var val_mg = 0;
        var val_ca = 0;
        var val_zn = 0;


        $.each(data, function(index, value){
          
            val_kcal += parseFloat(value.vl_k.replace(',','.'));    // ok

            val_cho +=   (((parseFloat(value.vl_carb.replace(',','.')) * value.valor_qtde) * (value.valor_observacao/100)) * (value.valor_porcao));   // ok
            val_lpd +=   (((parseFloat(value.vl_g_tot.replace(',','.')) * value.valor_qtde) * (value.valor_observacao/100)) * (value.valor_porcao));  // ok
            val_ptn +=   (((parseFloat(value.vl_prot.replace(',','.')) * value.valor_qtde) * (value.valor_observacao/100)) * (value.valor_porcao));   // ok
            val_fibra += (((parseFloat(value.vl_fibra.replace(',','.')) * value.valor_qtde) * (value.valor_observacao/100)) * (value.valor_porcao));  // ok
            val_vit_a += (((parseFloat(value.vl_vit_a.replace(',','.')) * value.valor_qtde) * (value.valor_observacao/100)) * (value.valor_porcao));  // ok
            val_vit_c += (((parseFloat(value.vl_vit_c.replace(',','.')) * value.valor_qtde) * (value.valor_observacao/100)) * (value.valor_porcao));  // ok
            val_fe +=    (((parseFloat(value.vl_fe.replace(',','.')) * value.valor_qtde) * (value.valor_observacao/100)) * (value.valor_porcao));     // ok
            val_mg +=    (((parseFloat(value.vl_mg.replace(',','.')) * value.valor_qtde) * (value.valor_observacao/100)) * (value.valor_porcao));     // ok
            val_ca +=    (((parseFloat(value.vl_ca.replace(',','.')) * value.valor_qtde) * (value.valor_observacao/100)) * (value.valor_porcao));     // ok
            val_zn +=    (((parseFloat(value.vl_zinco.replace(',','.')) * value.valor_qtde) * (value.valor_observacao/100)) * (value.valor_porcao));  // ok
           
        });

        dataArray.push(val_kcal);  
        dataArray.push(val_cho); 
        dataArray.push(val_lpd);
        dataArray.push(val_ptn);
        dataArray.push(val_fibra);
        dataArray.push(val_vit_a);
        dataArray.push(val_vit_c);
        dataArray.push(val_fe);
        dataArray.push(val_mg);
        dataArray.push(val_ca);
        dataArray.push(val_zn);

        /* ===============================================================
                        Inserindo valores na Table 
           ================================================================ */
           $('table[name=table_nutricional]').find('tbody').html('');

            var tabela = $('table[name=table_nutricional]').find('tbody');

            html_table = '';

            html_table += '<tr>';
                html_table += '<td>' + parseFloat(dataArray[0]).toFixed(2) + '</td>';
                html_table += '<td>' + parseFloat(dataArray[1]).toFixed(2) + '</td>';
                html_table += '<td>' + parseFloat(dataArray[2]).toFixed(2) + '</td>';
                html_table += '<td>' + parseFloat(dataArray[3]).toFixed(2) + '</td>';
                html_table += '<td>' + parseFloat(dataArray[4]).toFixed(2) + '</td>';
                html_table += '<td>' + parseFloat(dataArray[5]).toFixed(2) + '</td>';
                html_table += '<td>' + parseFloat(dataArray[6]).toFixed(2) + '</td>';
                html_table += '<td>' + parseFloat(dataArray[7]).toFixed(2) + '</td>';
                html_table += '<td>' + parseFloat(dataArray[8]).toFixed(2) + '</td>';
                html_table += '<td>' + parseFloat(dataArray[9]).toFixed(2) + '</td>';
                html_table += '<td>' + parseFloat(dataArray[10]).toFixed(2) + '</td>';
            html_table += '</tr>';

            $(tabela).append(html_table);



          /* =========================================================================
                                         Chart JS
             =========================================================================*/

             //ctx.clear();

              var ctx = document.getElementById("myChart");
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: ["kcal", "CHO (g)", "LPD (g)", "PTN (g)", "Fibra (g)", "Vit A (mg)","Vit c (mg)","Fe (mg)", "Mg (mg)", "Ca (mg)", "Zn (mg)"],
                        datasets: [
                            {
                                label: ' Valor Nutricional',
                                data: dataArray,
                                backgroundColor: [
                                    'rgba(51, 204, 255, 0.2)',
                                    'rgba(51, 204, 255, 0.2)',
                                    'rgba(51, 204, 255, 0.2)',
                                    'rgba(51, 204, 255, 0.2)',
                                    'rgba(51, 204, 255, 0.2)',
                                    'rgba(51, 204, 255, 0.2)',
                                    'rgba(51, 204, 255, 0.2)',
                                    'rgba(51, 204, 255, 0.2)',
                                    'rgba(51, 204, 255, 0.2)',
                                    'rgba(51, 204, 255, 0.2)',
                                    'rgba(51, 204, 255, 0.2)'
                                ],
                                borderColor: [
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)',
                                    'rgba(51, 204, 255, 1)'
                                ],
                                borderWidth: 1
                            }
                        ]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });//end chart js

       

    });

    $.fancybox({
        type: 'inline',
        href: $('#divChart'),
        autoSize: true,
        helpers: {
        title: {
            type: 'float'
        }
    }
    });

}// end function chartajax



