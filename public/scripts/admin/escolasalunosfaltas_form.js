var unsaved = false;

$("input,select,textarea").change(function () { //trigers change in all input fields including text type
    // unsaved = true;
});

$("input,select,textarea").click(function () { //trigers change in all input fields including text type
    //  unsaved = true;
});

$(document).ready(function () {
    window.onbeforeunload = function () {
        console.log(unsaved);
        if (unsaved) {
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }
    };

    if ((typeof form_values) != 'undefined') {
        $("select[name=idescola]").val(form_values.idescola);
        $("select[name=idescolavinculo]").val(form_values.idescolavinculo);
        $("select[name=idmateria]").val(form_values.idmateria);
        $("select[name=idprofessor]").val(form_values.idprofessor);
        $("select[name=idaluno]").val(form_values.idaluno);
        $("select[name=status1]").val(form_values.status1);


        $("select").each(function () {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function () {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

    }






    $("input[name=data]").mask("99/99/9999", {placeholder: " "});
    $("input[name=data]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function (date) {
        }
    });



    rules = {};
//	rules.idescola = "required";
    rules.idmateria = "required";
//rules.idprofessor = "required";
//rules.idaluno = "required";
    rules.status1 = "required";


    messages = {};

    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function (form) {
            unsaved = false;
            $.blockUI({message: '<h1> Aguarde...</h1>'});
            form.submit();
        }
    });

    //visualizar = false;
    if (visualizar) {
        $("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function () {
            val = $(this).val();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.append(val);
            parent.addClass('visualizar');

            $(this).remove();
        });
        $("div.control-group select").not('[type=hidden]').each(function () {
            val = $(this).find('option:selected').html();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.html(val);
            parent.addClass('visualizar');

        });

        $("label span.required").remove();

    }

});

function setMes(mes, id) {
    
    var mes_c = parseInt(mes);
    
    if (mes_c >= 1 && mes_c <= 12){
        $.post(baseUrl + '/admin/escolasalunosfaltas/setmes/', {mes: mes}, function (data) {

            $('.btn-mes-anterior').removeAttr('onclick');
            $('.btn-mes-posterior').removeAttr('onclick');

            $('.btn-mes-anterior').attr("onclick", "setMes('" + (mes_c - 1) + "', '" + id + "')");
            $('.btn-mes-posterior').attr("onclick", "setMes('" + (mes_c + 1) + "', '" + id + "')");

            $('.nomemes').html('');
            $('.nomemes').html(data);
            setFaltas($('select[name=idmateria]'), id)
        });
    }
}

function setMaterias(obj, idescolavinculo, idmateria) {
    var idprofessor = $(obj).val();
    $("#box-faltas").html("");
    data = '';


    $("select[name=idmateria]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/escolasalunosfaltas/setmaterias/', {idprofessor: idprofessor, idescolavinculo: idescolavinculo}, function (data) {

        $("select[name=idmateria]").html(data);
        $("select[name=idmateria]").val(idmateria);
        console.log(idmateria);
        val = $("select[name=idmateria]").find('option:selected').html();
        $("select[name=idmateria]").closest('div').find('span').html(val);

        $("select[name=idmateria]").trigger("chosen:updated");

        if (idmateria == "") {
            $("select[name=idmateria]").closest('div').find('span').html('Selecione...');
        }
    });
}

function setFaltas(obj, idescolavinculo) {
    var idmateria = $(obj).val();
    idmateria = parseInt(idmateria);
    
    if(isNaN(idmateria)){
        idmateria = idescolavinculo;
    }
    
    var idprofessor = $('[name=idprofessor]').val();

    data = '';

    var baseUrl = "http://localhost:8090"

    //if((idmateria>0) && (idmateria!="NaN") && (idmateria!="") && (idmateria!="undefined")){
    $.blockUI({message: '<h1> Aguarde...</h1>'});
    $.post(baseUrl + '/admin/escolasalunosfaltas/setfaltas/', {idprofessor: idprofessor, idmateria: idmateria, idescolavinculo: idescolavinculo}, function (data) {
        $("#box-faltas").html("");
        console.log(data);
        $("#box-faltas").html(data);
        $.unblockUI();
        if ((idmateria <= 0) || (idmateria == "NaN") || (idmateria == "") || (idmateria == "undefined")) {
            $("#box-faltas").html("");
        }
    });
    //}


}

function addFalta(obj, dia, tipo, idescolavinculo, idaluno, aula) {
    var idmateria = $('select[name=idmateria]').val();
    var idprofessor = $('[name=idprofessor]').val();
    var id = $(obj).find('.idsfaltas').val();

    console.log(aula)

    jConfirm('Confirmar a falta para o aluno?', 'Adicionar Falta', function (r) {
        if (r) {
            data = '';
            $.post(baseUrl + '/admin/escolasalunosfaltas/addfalta/', {id: id, idprofessor: idprofessor, idmateria: idmateria, idescolavinculo: idescolavinculo, dia: dia, tipo: tipo, idaluno: idaluno, aula: aula}, function (data) {
                var data_resp = JSON.parse(data);

                if (data_resp.data == "OK") {
                    $(obj).find('.idsfaltas').val(data_resp.id);
                    //$(obj).append('<i class="fam-tick"></i>');
                    $(obj).css('background-color', '#FF4C4C');
                    $(obj).css('color', '#FFF');
                    $(obj).text(' ');
                    $(obj).text('F');
                    $(obj).removeAttr('onclick')
                    $(obj).attr('onclick', "removeFalta(this ," + data_resp.id + ", '" + dia + "', '" + idescolavinculo + "', '" + idaluno + "', '" + aula +"')");
                    jAlert(data_resp.mensagem, data_resp.titulo);
                } else {
                    jAlert(data_resp.mensagem, data_resp.titulo);
                }
            });
        }
    });


}

function removeFalta(obj, idfalta, dia, idescolavinculo, idaluno, aula) {
    jConfirm('Remover falta do aluno?', 'Remover Falta', function (r) {
        if (r) {
            $.post(baseUrl + '/admin/escolasalunosfaltas/removefalta/', {id: idfalta}, function (data) {
                if (data == "OK") {
                    $(obj).removeAttr('onclick');
                    $(obj).attr('onclick', "addFalta(this, '" + dia + "', 'falta', '" + idescolavinculo + "', '" + idaluno + "', '" + aula +"')");
                    $(obj).find('i').remove();
                    $(obj).css('background-color', '#9AED8C');
                    $(obj).css('color', '#555');
                    $(obj).text(' ');
                    $(obj).text('C');
                    jAlert("Falta Removida com sucesso!", "Sucesso!");
                }
            });
        }
    });
}


function enviaplano(obj, dia, mes, idescolavinculo, idaluno) {
    var idmateria = $('select[name=idmateria]').val();
    var idprofessor = $('[name=idprofessor]').val();

    var id = $(obj).find('.idsfaltas').val();
    var titulo = $(obj).parent().parent().find('.titulo').val();
    var texto = $(obj).parent().parent().find('.texto').val();

    data = '';
    $.post(baseUrl + '/admin/escolasalunosfaltas/addplano/', {idprofessor: idprofessor, idmateria: idmateria, idescolavinculo: idescolavinculo, dia: dia, mes: mes, titulo: titulo, texto: texto}, function (data) {
        var data_resp = JSON.parse(data);

        if (data_resp.data == "OK") {
            $('.quadro_' + dia).each(function () {
                $(this).css('background-color', '#9AED8C');
                $(this).removeAttr('onclick');
                $(this).attr('onclick', "addFalta(this, '" + dia + "', 'falta', '" + idescolavinculo + "', '" + idaluno + "')");
            });

            jAlert(data_resp.mensagem, data_resp.titulo);
        } else {
            jAlert(data_resp.mensagem, data_resp.titulo);
        }
    });



}