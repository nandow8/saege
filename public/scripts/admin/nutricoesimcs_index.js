$(document).ready(function() {
    if ((typeof form_values) != 'undefined') {
        $("select[name=idsecretaria]").val(form_values.idsecretaria);
        $("select[name=idescola]").val(form_values.idescola);
        $("select[name=idescolaaluno]").val(form_values.idescolaaluno);
        $("select[name=status1]").val(form_values.status1);
        $("select[name=diagnostico]").val(form_values.diagnostico);

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
    }
    if (form_values.idperfil == 29) {
        setAlunos('input[name=idescola]', form_values.idescolaaluno, form_values.idescola);
    } else {
        setAlunos('select[name=idescola]', form_values.idescolaaluno, form_values.idescola);
    }
    $('.type_diag').each(function() {
        valor = $(this).text();
        var cor;
        switch (valor) {
            case "Eutrofia":
                cor = "#009963";
                break;
            case "Magreza":
                cor = "#8a6d3b";
                break;
            case "Magreza acentuada":
                cor = "#afbd07";
                break;
            case "Obesidade":
                cor = "#bd0404";
                break;
            case "Obesidade grave":
                cor = "#ff0008";
                break;
            case "Risco de sobrepeso":
                cor = "#e6c300";
                break;
            case "Sobrepeso":
                cor = "#ffa900";
                break;
            default:
                cor = "#000";
        }

        $(this).css('color', cor);
    });

    $('input[name=peso]').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.',
        centsLimit: 2
    });
    $('input[name=altura]').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.',
        centsLimit: 2
    });
    $('input[name=imc]').priceFormat({
        prefix: '',
        centsSeparator: ',',
        thousandsSeparator: '.',
        centsLimit: 2
    });
    $("input[name=datamedicao_i],input[name=datamedicao_f]").mask("99/99/9999", {placeholder: " "});
    $("input[name=datamedicao_i],input[name=datamedicao_f]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });
});

function confirmDelete($id, $texto) {
    jConfirm('Confirmar a exclusão de "' + $texto + '"?', 'Excluir registro', function(r) {
        if (r) {
            $.post(baseUrl + "/" + module + "/" + controller + "/excluirxml",
                    {id: $id}, function(data) {
                if (data == "OK") {
                    window.location = baseUrl + "/" + module + '/' + controller;
                } else {
                    jAlert(data, "Erro!");
                }
            });
        }
    });
}

function statusChange($id, $obj) {
    $obj = $($obj);

    $.post(baseUrl + "/" + module + "/" + controller + "/changestatusxml",
            {id: $id, op: controller}, function(data) {
        if ((data == "Ativo") || (data == "Bloqueado")) {
            $obj.html(data);
        } else {
            jAlert(data, "Alerta!");
        }
    });
}
function setAlunos(obj, idaluno, escola = 0) {
    var idescola;

    if (escola > 0) {
        idescola = escola;
    } else {
        idescola = $(obj).val();

    }

    data = '';
    $("select[name=idescolaaluno]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/nutricoesimcs/setescolasalunos/', {idescola: idescola}, function(data) {
        $("select[name=idescolaaluno]").html(data);

        $("select[name=idescolaaluno]").val(idaluno);
        val = $("select[name=idescolaaluno]").find('option:selected').html();

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function() {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

        if (idaluno == "") {
            $("select[name=idescolaaluno]").closest('div').find('span').html('Todos');
        }
    });
}

function exibeGrafico(obj, tipo = 0, destino = 0, id = "") {
    let NomeJanela = '#' + obj.rel;

    destino = (destino == 1) ? 'setgrafico' : 'setgrafico';

    if (destino == "")
        return false;

    $.fancybox({
        type: (tipo == 0) ? 'inline' : 'iframe',
        href: (tipo == 0) ? NomeJanela : baseUrl + "/admin/nutricoesimcs/" + destino + "/id/" + id,
        width: $('.' + ($(NomeJanela).attr("class"))).width() + 22,
        height: $('.' + ($(NomeJanela).attr("class"))).height() + 22,
        autoSize: false,
        helpers: {
            title: {
                type: 'float'
            }
        }
    });
}
