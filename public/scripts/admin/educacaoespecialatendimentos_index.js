$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=idprofissional]").val(form_values.idprofissional);
$("select[name=idsolicitacao]").val(form_values.idsolicitacao);
$("select[name=visitaunidade]").val(form_values.visitaunidade);
$("select[name=idtriagem]").val(form_values.idtriagem);
$("select[name=escutafamilia]").val(form_values.escutafamilia);
$("select[name=escutaprofessor]").val(form_values.escutaprofessor);
$("select[name=visitadomiciliar]").val(form_values.visitadomiciliar);
$("select[name=idavaliacao]").val(form_values.idavaliacao);
$("select[name=idencaminhamento]").val(form_values.idencaminhamento);
$("select[name=status1]").val(form_values.status1);
	

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});	
	}
	
		$("input[name=dataabertura_i],input[name=dataabertura_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataabertura_i],input[name=dataabertura_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});










	$("input[name=datadevolutiva_i],input[name=datadevolutiva_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datadevolutiva_i],input[name=datadevolutiva_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});


	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Finalizado") || (data=="Em acompanhamento")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 