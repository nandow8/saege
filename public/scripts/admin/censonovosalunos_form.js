$(document).ready(function() {

	// if ((typeof <?php echo $endereco_prefix ?>idestado)!='undefined') $("select[name=<?php echo $endereco_prefix ?>idestado]").val(<?php echo $endereco_prefix ?>idestado);

	// 	$("input[name=<?php echo $endereco_prefix ?>cep]").unmask();
	// 	//$("input[name=<?php echo $endereco_prefix ?>cep]").mask("99999-999", {placeholder:" "});	
		
	// 	$('input[name=<?php echo $endereco_prefix ?>cep]').blur(function() {
	// 		val = $.trim(this.value);
	// 		if (""==val) return;
	// 			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
	// 			$.post(baseUrl + '/admin/index/buscacep' , {cep: val,modo:'gravar'}, function(cep) {
	// 				$.unblockUI();
	// 				if (cep=='false') {
	// 					jAlert('CEP não encontrado!','Erro');
	// 					return;
	// 				}
					
	// 				cep = jQuery.parseJSON(cep);
					
	// 				$("input[name=<?php echo $endereco_prefix ?>endereco]").val(cep.tipo_logradouro + ' ' + cep.logradouro.replace(cep.tipo_logradouro, ''));
	// 				$("input[name=<?php echo $endereco_prefix ?>cidade]").val(cep.cidade);
	// 				$("select[name=<?php echo $endereco_prefix ?>idestado]").val(cep.idestado);
	// 				$("input[name=<?php echo $endereco_prefix ?>bairro]").val(cep.bairro);

	// 				$("[name=<?php echo $endereco_prefix ?>idestado]").select2("destroy");
	// 				$("[name=<?php echo $endereco_prefix ?>idestado]").select2();			
					
	// 		});	
	// 	});

	

	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	

	var latitude = parseFloat($('#latitude').val());
	var longitude = parseFloat($('#longitude').val());
	var nome = $('#nomealuno').val();

	if($('#nomealuno').val() == ''){
		nome = 'Pre Inscrito';
	}
	
	if ((typeof form_values)!='undefined'){

		$("select[name=idcentralvaga]").val(form_values.idcentralvaga);
		$("select[name=sexo]").val(form_values.sexo);
		$("select[name=filiacao1]").val(form_values.filiacao1);
		$("select[name=filiacao2]").val(form_values.filiacao2);
		$("select[name=temnomesocial]").val(form_values.temnomesocial);
		$("select[name=status1]").val(form_values.status1);
		$("select[name=portadorespecialidade]").val(form_values.portadorespecialidade);
		$("select[name=etapa]").val(form_values.etapa);
		$("select[name=etapa_ensino]").val(form_values.etapa_ensino);
		$("select[name=datadecorte]").val(form_values.datadecorte);
		$("select[name=painaodeclarado]").val(form_values.painaodeclarado);
		$("select[name=matricula]").val(form_values.matricula);
		$("select[name=estado]").val(form_values.estado);
		$("select[name=escola]").val(form_values.escola);
		$("select[name=tipodeficiencia]").val(form_values.tipodeficiencia);
		$("select[name=serie]").val(form_values.serie);
		$("select[name=auxiliosocial]").val(form_values.auxiliosocial);
		$("select[name=escolassets]").val(form_values.escola);
		$("select[name=ensinoset]").val(form_values.id_tipo_ensino);
		

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}//end if

	$("input[name=datanascimento]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datanascimento]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$("input[name=datanascimentoresponsavel]").mask("99/99/9999", { placeholder: " " });
	$("input[name=datanascimentoresponsavel]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function (date) {
		}
	});

	$("input[name=cpffiliacao]").mask("999.999.999-99", { placeholder: " " });
	
	$("input[name=cpfaluno]").mask("999.999.999-99", { placeholder: " " });

	$("input[name=cep]").mask("99999-999", { placeholder: " " });

	//Toda a idéia revolucionária provoca três estágios: 
	// 1º. é impossível - não perca meu tempo. 
	// 2º. é possível, mas não vale o esforço 
	// 3º. eu sempre disse que era uma boa idéia.
	 
	rules = {};
	
	rules.aluno = "required";
	rules.datanascimento = "required"
	rules.sexo = "required";
	rules.cpfaluno = "required";
	rules.portadorespecialidade = "required";
	rules.filiacao1 = "required";
	rules.nomefiliacao1 = "required";
	rules.painaodeclarado = "required";
	rules.temnomesocial = "required";
	rules.cpffiliacao = "required";
	rules.datanascimentoresponsavel = "required";
	rules.cep = "required";
	rules.endereco = "required";
	rules.numero = "required";
	rules.bairro = "required";
	rules.estado = "required";
	rules.cidade = "required";
	rules.tipotelefone1 = "required";
	rules.numerotelefone1 = "required";
	rules.escolassets = "required";
	rules.etapa = "required";
	rules.datadecorte = "required";
	rules.matricula = "required";
	rules.ensinoset = "required";
	rules.status1 = "required";
	rules.idcentralvaga = "required";
	
	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form){
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	


	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}	
	
	$('#loading-map').css("display", "none");
	
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 14,
		zoomControl: true,
		mapTypeControl: true,
		scaleControl: false,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: true,
		center: {lat: latitude, lng: longitude},
		
	  });// end map google

	  var marker = new google.maps.Marker({
			position: map.getCenter(),
			animation: google.maps.Animation.DROP,
			icon: {
				icon: null,
				scale: 3
			},
			draggable: true,
			map: map,
			label: { text: nome },
		});

		var infoWindow = new google.maps.InfoWindow;
            
		marker.addListener('mouseout', function() {
			//alert(infoWindow);
			Lat = marker.position.lat();
			Lgn = marker.position.lng() ;
			$("#latitude").val(Lat);
			$("#longitude").val(Lgn);
			//alert(Lat + " - " + Lgn);
		});
		
		

		// $.ajax({
		// 	url: baseUrl + '/admin/censonovosalunos/rankinggoogleescolas',
		// 	type: 'POST',
		// 	data: {
	
		// 			latitude: latitude,
		// 			longitude: longitude
					 
		// 		  },
		// beforeSend: function () {
				   
		// 	$('#table-ranking').css("display", "none");
		// 	$('#loading-map').css("display", "block");
		// 	$('#map').css("display", "none");
		// }}).done(function (data) {

		// 	$('#loading-map').css("display", "none");	
		// 	$('#table-ranking').css("display", "block");
		// 	$('#map').css("display", "block");
		// 	//$('[name=table-ranking-content]').html('');

		// 	// id="table-ranking" class="table table-striped table-hover"
		// 	$('#table-ranking').addClass("table table-striped table-hover");


			
		// 	var retornoData = JSON.parse(data);

		// 	retornoData.sort(comparer);
			
		// 	// alert(retornoData);
		// 	jQuery.each(retornoData,function(key, value){
				
		// 		$('[name=table-ranking-content]').append('<tr>');
		// 		$('[name=table-ranking-content]').append('<td colspan="3" ><strong>' + value.escola + '&nbsp; </strong> </td>');

		// 		$('[name=table-ranking-content]').append('<td style="width: 10%; text-align: center"><strong>' + value.manha + '</strong></td>');
		// 		$('[name=table-ranking-content]').append('<td style="width: 10%; text-align: center"><strong>' + value.tarde + '</strong></td>');
		// 		$('[name=table-ranking-content]').append('<td style="width: 10%; text-align: center"><strong>' + value.noite + '</strong></td>');
		// 		$('[name=table-ranking-content]').append('<td style="width: 10%; text-align: center"><strong>' + value.integral + '</strong></td>');

		// 		$('[name=table-ranking-content]').append('<td colspan="3" ><h4>' + value.distancia_caminhada + ' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </h4></td>');
		// 		$('[name=table-ranking-content]').append('<td colspan="2" style="width: 15%"  ><div class="btn btn-default btn-block" style="cursor: pointer" onclick="openmodaldetails(' + value.id + ')"> Detalhes </div></td>');
		// 		$('[name=table-ranking-content]').append('<td colspan="3" style="width: 20%" ><div class="btn btn-info btn-block" style="cursor:pointer"  onclick="selescola(\'' + value.escola  + '\',' + value.id + ',' + value.distancia_caminhada + ',' + value.latitude + ',' + value.longitude + ')"> Selecionar </div></td>');
		// 		//$('[name=table-ranking-content]').append('<td></td>');
		// 		//$('[name=table-ranking-content]').append('<td></td>');
		// 		//$('[name=table-ranking-content]').append('<td></td>');
		// 		$('[name=table-ranking-content]').append('</tr>');

		// 		/* ----------------------------------------  
		// 			Setando os Valores das Escolas no MAPS 
		// 		-------------------------------------------*/
		// 		var marker = new google.maps.Marker({
		// 			position: {lat: parseFloat(value.latitude), lng: parseFloat(value.longitude)},
		// 			animation: google.maps.Animation.DROP,
		// 			icon: {
		// 				icon: null,
		// 				scale: 3
		// 			},
		// 			draggable: false,
		// 			map: map,
		// 			label: { text: value.escola },
		// 		});
		

		// 	});

		    
		// });

});//end document ready

/*=================================================================
				Função para Setar Escolas
===================================================================*/

function selescola(nome,id,distancia, latitudedestino, longitudedestino){
	

	$("input[name=idselectescola]").val(id);
	$("input[name=distanciaset]").val(distancia);

	$("select[name=escolassets]").val(nome);
	$("select[name=escolassets]").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});

	var map = new google.maps.Map(document.getElementById('map'),{
		zoom: 14,
		zoomControl: true,
		mapTypeControl: true,
		scaleControl: false,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: false,
		center: {lat: -23.317591, lng: -46.221609}
	});

	var directionsService = new google.maps.DirectionsService;
	var directionsDisplay = new google.maps.DirectionsRenderer({
		draggable: true,
		map: map,
		panel: document.getElementById('right-panel')
	});

	// latitudedestino = -23.477488;
	// longitudedestino = -46.326744;

	console.log(displayRoute(latitudedestino, longitudedestino, directionsService, directionsDisplay));

	var posicaoScroll = $("#tittle").offset().top;

	var body = $("html, body");
	body.stop().animate({scrollTop: posicaoScroll}, 500, 'swing', function(){
	
	});
	
	
}//end function selescola

function displayRoute(latitudedestino, longitudedestino, service, display){
	
	// origem Latitude
	var originLat = $('#latitude').val();
	// Origem Longitude
	var originLng = $('#longitude').val();

	service.route({
	  origin: {lat: parseFloat(originLat), lng:  parseFloat(originLng)},
	  destination: {lat: parseFloat(latitudedestino), lng: parseFloat(longitudedestino)}, // {lat: 59.327, lng: 18.067},
	  //waypoints: [{location: 'Adelaide, SA'}, {location: 'Broken Hill, NSW'}],
	  travelMode: 'WALKING',
	  avoidTolls: true
	}, function(response, status) {
	  if (status === 'OK') {
		display.setDirections(response);
	  } else {
		alert('Could not display directions due to: ' + status);
	  }
	});
  }// end function display route

  // Função para comparar e ordenar a array de escolas no ranking
  function comparer(a, b){
	if (a.distancia_caminhada < b.distancia_caminhada)
		return -1;

	if (a.distancia_caminhada > b.distancia_caminhada)
		return 1;

	return 0;
}// end function comparer

function openmodaldetails(id){
	
	$('#table-escola-resultados').html('');

	console.log('id -->', id);

	$.ajax({
		url: baseUrl + '/admin/censonovosalunos/detalhesvagasescolas',
		type: 'POST',
		data: {
				idescola: id
			  },
	beforeSend: function () {
		// $('#table-ranking').css("display", "none");
		// $('#loading-map').css("display", "block");
		// $('#map').css("display", "none");
	}}).done(function (data){
		
		$('#exampleModalLongTitle').html('cheguei ' + id);
		
		data = JSON.parse(data);

		console.log('cheguei', data);

		jQuery.each(data,function(key, value){

			$('#table-escola-resultados').append('<tr>');

				$('#table-escola-resultados').append('<td>' + (key + 1) + '</td>');
				$('#table-escola-resultados').append('<td>' + value.descricao + '</td>');
				$('#table-escola-resultados').append('<td>' + value.series + '</td>');
				$('#table-escola-resultados').append('<td>' + value.turma + '</td>');
				$('#table-escola-resultados').append('<td>' + value.periodo + '</td>');
				$('#table-escola-resultados').append('<td>' + value.totalvagas + '</td>');
			
			$('#table-escola-resultados').append('</tr>');

		});

		$('#modaldetalhes').modal('show');

	});

	$('#modaldetalhes').modal('show');

}//end function openmodaldetails

  $("select[name=escolassets]").change(function() {

	var selecionado = $("select[name=escolassets] :selected").val();


	$('[name=distanciaset]').val(selecionado);


		
  });//end function 

  function gerarhanking(){

	// Localizando posição do elemento para aplicar scroll
	var local = $('#table-ranking').offset().top;
	$('html, body').animate({scrollTop: (local - 40)}, 3000); //slow, medium, fast

	// origem Latitude
	var latitude = $('#latitude').val();
	// Origem Longitude
	var longitude = $('#longitude').val();

	$.ajax({
			url: baseUrl + '/admin/censonovosalunos/rankinggoogleescolas',
			type: 'POST',
			data: {
					latitude: latitude,
					longitude: longitude
				  },
		beforeSend: function () {
			//$('#table-ranking').css("display", "none");
			//$('#loading-map').css("display", "block");
			//$('#map').css("display", "none");
		}}).done(function (data) {
			$('#loading-map').css("display", "none");	
			$('#table-ranking').css("display", "block");
			//$('#map').css("display", "block");
			//$('[name=table-ranking-content]').html('');

			// id="table-ranking" class="table table-striped table-hover"
			$('#table-ranking').addClass("table table-striped table-hover");
			
			var retornoData = JSON.parse(data);

			retornoData.sort(comparer);

			var objRetorno = new Array();
			var contador = 0;
			
			// alert(retornoData);
			jQuery.each(retornoData,function(key, value){
				contador++;
				// Criando JSON para alimentar os elementos
				
				var objLinhaRetorno = new Array();

				objLinhaRetorno['escola'] = value.distancia;
				objLinhaRetorno['distancia'] = $('#auxiliar').val();
				objLinhaRetorno['matriculas'] = 'teste';

				objRetorno[value.escola] = objLinhaRetorno; 
				
				$('[name=table-ranking-content]').append('<tr id="linha-' + contador + '">');
				/* 1 */	$('[name=table-ranking-content]').append('<td><strong>' + value.escola + '</strong> </td>');
				/* 2 */	$('[name=table-ranking-content]').append('<td style="text-align: center"><strong>00</strong></td>');
				/* 3 */	$('[name=table-ranking-content]').append('<td style="text-align: center"><strong>01</strong></td>');
				/* 4 */	$('[name=table-ranking-content]').append('<td style="text-align: center"><strong>02</strong></td>');
				/* 5 */	$('[name=table-ranking-content]').append('<td style="text-align: center"><strong>03</strong></td>');
				/* 6 */	$('[name=table-ranking-content]').append('<td><h6 id="distancia' + contador + '">05 </h6></td>');
				/* 7 */	$('[name=table-ranking-content]').append('<td style="width: 10%"><strong id="duracao' + contador + '">05 </strong></td>');
				/* 8 */	$('[name=table-ranking-content]').append('<td style="width: 15%"><div class="btn btn-default btn-block" style="cursor: pointer" onclick="openmodaldetails(' + value.id + ')"> Detalhes </div></td>');
				/* 9 */	$('[name=table-ranking-content]').append('<td style="width: 15%"><div class="btn btn-info btn-block" style="cursor:pointer"  onclick="selescola(\'' + value.escola  + '\',' + value.id + ',' + value.distancia_caminhada + ',' + value.latitude + ',' + value.longitude + ')"> Selecionar </div></td>');
				/* 10 *///$('[name=table-ranking-content]').append('<td></td>');
				/* 11 *///$('[name=table-ranking-content]').append('<td></td>');
				/* 12 *///$('[name=table-ranking-content]').append('<td></td>');
				$('[name=table-ranking-content]').append('</tr>');

				var origem = latitude + ',' + longitude;
				var destino = value.latitude + ',' + value.longitude;
				
				CalculaDistancia(origem, destino, contador);

				/* ----------------------------------------  
					Setando os Valores das Escolas no MAPS 
				-------------------------------------------*/
				// var marker = new google.maps.Marker({
				// 	position: {lat: parseFloat(value.latitude), lng: parseFloat(value.longitude)},
				// 	animation: google.maps.Animation.DROP,
				// 	icon: {
				// 		icon: null,
				// 		scale: 3
				// 	},
				// 	draggable: false,
				// 	map: map,
				// 	label: { text: value.escola },
				// });
			});
			//objRet= JSON.parse(objRetorno);
			console.log('Rerotno objeto',objRetorno);
			
		});
  }//end function 

  function demandastotaldevagas(){

	var map = new google.maps.Map(document.getElementById('map'),{
		zoom: 14,
		zoomControl: true,
		mapTypeControl: true,
		scaleControl: false,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: true,
		center: {lat: -23.317591, lng: -46.221609}
	});
	   
	$.ajax({
		url: baseUrl + '/admin/censonovosalunos/rankinggoogleescolas',
		type: 'POST',
		data: {

		}
	  }).done(function(message) {

		var escolas = JSON.parse(message);

		jQuery.each(escolas,function(key, value){

			var marker = new google.maps.Marker({
					position: {lat: parseFloat(value.latitude), lng: parseFloat(value.longitude)},
					animation: google.maps.Animation.DROP,
					icon: {
						icon: null,
						scale: 3
					},
					draggable: false,
					map: map,
					label: { text: value.escola },
				});

			
			var circulo = new google.maps.Circle(
                {
                  map: map,
                  center: new google.maps.LatLng(parseFloat(value.latitude), parseFloat(value.longitude)),
                  radius: (1,5 * Math.floor((Math.random() * 80) + 1)), // 1000 metros = 1k.
                  //strokeColor: "#818c99",
                  fillColor: '#31b2ae',
                  fillOpacity: 0.60
			});

		});
		
	  });// end Ajax Jquery
	


	
  }//end function demandastotaldevagas

  function CalculaDistancia(origem, destino, id){

    
	console.log('origem -> ',origem);
	console.log('destino ->', destino);
	var service = new google.maps.DistanceMatrixService();
	
	service.getDistanceMatrix(
	{
		origins: [origem],
		destinations: [destino],
		travelMode: google.maps.TravelMode.WALKING
	},function (response, status){
		
		var distancia = response.rows[0].elements[0].distance.text;
		var duration = response.rows[0].elements[0].duration.text;
		console.log('response',response);
		$('#distancia' + id).html(distancia);
		$('#duracao' + id).html(duration);
		//$('#' + id).attr('id', 'definido');
		//$('linha-' +  id).toggleClass(s);
	});
}

$( "[name=portadorespecialidade]" ).change(function() {
	
	var str = '';

    $( "select[name=portadorespecialidade] option:selected" ).each(function() {
	  str = $(this).val();
    });
	
	srt = String(str);
	if(str == 'sim'){
		$("#tipo-deficiencia").css('display', 'block');
		$("#obs-deficiencia").css('display', 'block');
		//console.log('sim ',str);
	}else{
		$("#tipo-deficiencia").css('display', 'none');
		$("#obs-deficiencia").css('display', 'none');
		//console.log('nao ',str);
	}//end if	
});

$( "select[name=temnomesocial]" ).change(function(){
	var str = '';
    $( "select[name=temnomesocial] option:selected" ).each(function() {
	  str = $(this).val();
    });
	if(str == 'sim'){
		$("#box-nomesocial").css('display', 'block');
	}else{
		$("#box-nomesocial").css('display', 'none');
	}//end if
	
});

function localpreinscrito(){

	var latitude = parseFloat($('#latitude').val());
	var longitude = parseFloat($('#longitude').val());
	var nome = $('#nomealuno').val();

	if($('#nomealuno').val() == ''){
		nome = 'Pre Inscrito';
	}

	$('#loading-map').css("display", "none");
	
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 14,
		zoomControl: true,
		mapTypeControl: true,
		scaleControl: false,
		streetViewControl: false,
		rotateControl: false,
		fullscreenControl: true,
		center: {lat: latitude, lng: longitude},
		
	  });// end map google

	  var marker = new google.maps.Marker({
			position: map.getCenter(),
			animation: google.maps.Animation.DROP,
			icon: {
				icon: null,
				scale: 3
			},
			draggable: true,
			map: map,
			label: { text: nome },
		});

		var infoWindow = new google.maps.InfoWindow;
            
		marker.addListener('mouseout', function() {
			//alert(infoWindow);
			Lat = marker.position.lat();
			Lgn = marker.position.lng() ;
			$("#latitude").val(Lat);
			$("#longitude").val(Lgn);
			//alert(Lat + " - " + Lgn);
		});
		
		
} //end function localpreinscrito


/* =====================================================================

				Elementos Animate para Hide de formulário

========================================================================= */

$('#controller-01').click(function (){
	$("#line-01").animate({
		height: 'toggle'
	});
});

$('#controller-02').click(function (){
	$("#line-02").animate({
		height: 'toggle'
	});
});

$('#controller-03').click(function (){
	$("#line-03").animate({
		height: 'toggle'
	});
});

$('#controller-04').click(function (){
	$("#line-04").animate({
		height: 'toggle'
	});
});

$('#controller-05').click(function (){
	$("#line-05").animate({
		height: 'toggle'
	});
});


$('#controller-06').click(function (){
	$("#line-06").animate({
		height: 'toggle'
	});
});
  /* --------------------------------------------------------------
				  Evento Change ao selecionar Tipo de Ensino
	---------------------------------------------------------------*/
/*
	
$("[name=ensinoset]").change(function() {

    var latitude = $('#latitude').val();
    var longitude = $('#longitude').val();
	var nome = $('#nomealuno').val();
	
	

    $("select[name=escolasset] option:selected").append('<option>Aguarde...</option>');

    $( "select[name=escolasset]" ).prop( "disabled", false );

    $( "select[name=ensinoset] option:selected" ).each(function() {

        var text = $(this).text();
		var value = $(this).val();
		
	

        $.ajax({
                url: baseUrl + '/admin/censonovosalunos/setescola/',
                type: 'POST',
                data: {
                        idescola:value
                    },
        beforeSend: function () {
                    
                $("#map").css("display","none");
                $('#table-ranking').css("display", "none");
                $('#loading-map').css("display", "block");

        }}).done(function (data){

            // Setando Displays após o encerramento do loading
            $('#loading-map').css("display", "none");
            $("#map").css("display","block");
            $('#table-ranking').css("display", "block");

            data = JSON.parse(data);

            data.sort(comparer);
           
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                zoomControl: true,
                mapTypeControl: true,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: false,
                center: {lat: parseFloat(latitude), lng: parseFloat(longitude)}
            });

            var marker = new google.maps.Marker({
                position: map.getCenter(),
                animation: google.maps.Animation.DROP,
                icon: {
                    icon: null,
                    scale: 3
                },
                draggable: false,
                map: map,
                label: { text: "Casa do Fulaniho"},
				});
				
				
				
               jQuery.each(data,function(key, value){

					var marker = new google.maps.Marker({
						position: {lat: parseFloat(value.latitude), lng: parseFloat(value.longitude)},
						animation: google.maps.Animation.DROP,
						icon: {
							icon: null,
							scale: 3
						},
						draggable: false,
						map: map,
						label: { text: value.escola },
					});

			   });

        // $("select[name=escolasset]").html('');
            
        // $('[name=table-ranking-content]').html('');
			
		

            // jQuery.each(data,function(key, value){


			// 	console.log("=>" + key);
            //     $("select[name=escolasset]").append('<option>' + value.escola + '</option>');

			// 	$('[name=table-ranking-content]').append('<tr>');
			// 	$('[name=table-ranking-content]').append('<td colspapn="3">' + value.escola + ' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td>');
			// 	$('[name=table-ranking-content]').append('<td colspan="3"><h4>' + value.distancia_caminhada + ' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </h4></td>');
			// 	$('[name=table-ranking-content]').append('<td ><div class="btn btn-default btn-block" style="cursor: pointer" onclick="openmodaldetails(' + value.id + ')"> Detalhes </div></td>');
			// 	$('[name=table-ranking-content]').append('<td><div class="btn btn-info btn-block" style="cursor:pointer"  onclick="selescola(\'' + value.escola  + '\',' + value.id + ',' + value.latitude + ',' + value.longitude + ')"> Selecionar Escola </div></td>');
			// 	$('[name=table-ranking-content]').append('</tr>');
                
            //     $("#select-" + value.id).addClass( "select" );

            //     carregatiposensino(value.id);
               
			// 	var marker = new google.maps.Marker({
			// 		position: {lat: parseFloat(value.latitude), lng: parseFloat(value.longitude)},
			// 		animation: google.maps.Animation.DROP,
			// 		icon: {
			// 			icon: null,
			// 			scale: 3
			// 		},
			// 		draggable: false,
			// 		map: map,
			// 		label: { text: value.escola },
			// 	});

            // });
            

            // var posicaoScroll = $("#tittle").offset().top;

            // var body = $("html, body");
            // body.stop().animate({scrollTop: posicaoScroll}, 500, 'swing', function() {

            // });
        });//end done jquery ajax

    });

    
 
});// end change
	*/