var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=tipoprocesso]").val(form_values.tipoprocesso);
		$("select[name=tipochamado]").val(form_values.tipochamado);
		$("select[name=idtiposchamado]").val(form_values.idtiposchamado);
		$("select[name=posicao]").val(form_values.posicao);
		$("select[name=status1]").val(form_values.status1);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}

	rules = {};
	rules.tipoprocesso = "required";
	rules.tipochamado = "required";
	rules.posicao = "required";
	rules.status1 = "required";
	
	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
	}		
});

function encaminhar(id) {

	jConfirm('Confirmar o encaminhamento?', 'Encaminhar', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/encaminhar",
		   	{id: id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller + "/editar/id/"+id;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}