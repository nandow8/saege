function confirmDelete($id, $texto) {
    jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
        if (r) {
            $.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
            {id: $id}, function(data) {
                if (data=="OK") {
                    window.location = baseUrl + "/" + module + '/' + controller;
                } else {
                    jAlert(data, "Erro!");
                }
            });
        }
    }); 
}

function statusChange($id, $obj) {
    $obj = $($obj);
 
    $.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
    {id: $id, op: controller}, function(data) {
        if ((data=="Ativo") || (data=="Bloqueado")) {
            $obj.html(data);
        } else {
            jAlert(data, "Alerta!");
        }
    });
} 