$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
$("select[name=idescola]").val(form_values.idescola);
$("select[name=idtipoedificacao]").val(form_values.idtipoedificacao);
$("select[name=e_idenderecoidestado]").val(form_values.e_idenderecoidestado);	
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});		
	}
	


	$('input[name=idendereco]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});

	$("input[name=dataconstrucao_i],input[name=dataconstrucao_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataconstrucao_i],input[name=dataconstrucao_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});



		
	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
 	$.post(baseUrl + "/"+controller+"/changestatusxml",
 	 	 	{id: $id, op: controller}, function(data) {
			if ((data=="Ativo") || (data=="Bloqueado")) {
				
				$.get(baseUrl + '/' + controller, {}, function(r) {
					$("table.table-content tbody").html($(r).find('table.table-content tbody').html());
					$.unblockUI();
				});
				
				
			} else {
				$.unblockUI();
				jAlert(data, "Alerta!");
			}
 	});
} 