var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	/*window.onbeforeunload = function() {
		if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }
	};*/


	if(typeof(form_values)!='undefined'){
		console.log(form_values);

		$("select[name=status1]").val(form_values.status1);
		$("textarea[name=observacoes]").val(form_values.observacoes);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
	}

	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		

	if (editar) {
		
		$("textarea[name=observacoes], input[name=numeroboletim]").each(function() {
			val = $(this).val();
			
			val = $.trim(val);
			val = (val=='') ? '' : val;

			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});

		$("select[name=idpatrimonio], select[name=idescola], select[name=motivobaixa]").each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '' : val;				
			
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');
			
		});
		
		$("label span.required").remove();	
	}

});