jQuery(document).ready(function() {
    var idveiculo = form_values.idveiculo;
    setVinculo(idveiculo);
});

function setEstudantes(obj) {
    var identidade = $(obj).val();
    // identidade = identidade.substr(1);
    var tipo = $(obj).find('option:selected').data('tipo');

    data = '';
    $.post(baseUrl + '/admin/lteveiculosestudantes/setestudantes/', {identidade: identidade, tipo: tipo}, function (data) {
        $(".box-estudantes").html(data);
    });
}

function setVinculo(idveiculo) {

    data = '';
    $.post(baseUrl + '/admin/lteveiculosestudantes/setvinculos/', {idveiculo: idveiculo}, function (data) {
        $(".box-veiculo").html(data);
        contador();
    });
}

function setEntidades(obj) {
    var tipo = $(obj).val();

    data = '';
    $.post(baseUrl + '/admin/lteveiculosestudantes/setentidades/', {tipo: tipo}, function (data) {
        $("select[name=identidade]").html(data);
    });
}

function ExcluirAluno(obj, idaluno) {
    var iditem    = $(obj).parent().parent().find("input[name='iditem']").val();
    var idveiculo = $(obj).parent().parent().find("input[name='idveiculo']").val();
    // var path_box = $(obj).parent().parent().parent();
    jConfirm('Confirmar a exclusão do estudante deste veículo?', 'Excluir aluno', function (r) {
        if (r) {
            $.post(baseUrl + '/admin/lteveiculosestudantes/excluiritem/', {iditem: iditem}, function (data) {
                var item = $('.aluno_' + idaluno);
                item.removeClass("off");
                item.addClass("on");
                $(obj).parent().parent().remove();
                contador();
            });
        }
    });
}

function contador() {
    var capacidade = form_values.capacidade;
    $("#capacidademaxima").text(capacidade);

    total = $("#aba0").find(".linha").size();
    $("#total").text(total);
}
