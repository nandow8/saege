var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {

	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=tipo]").val(form_values.tipo);
		$("select[name=idusuario]").val(form_values.idusuario);
		$("select[name=iddepartamento]").val(form_values.iddepartamento);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=tipocontato]").val(form_values.tipocontato);
		$("select[name=idtipo]").val(form_values.idtipo);
		$("select[name=posicao]").val(form_values.posicao);
		$("select[name=encaminhar]").val(form_values.encaminhar);
		$("select[name=status1]").val(form_values.status1);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}

	rules = {};
	rules.sequencial = "required";
	rules.tipo = "required";
	rules.idescola = "required";
	rules.reclamante = "required";
	rules.telefone1 = "required";
	rules.reclamado = "required";
	rules.idtipo = "required";
	rules.reclamacaoassunto = "required";
	rules.posicao = "required";
	rules.status1 = "required";
	
	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
	}
});