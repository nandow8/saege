$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
$("select[name=idescolavinculo]").val(form_values.idescolavinculo);
$("select[name=idprofessor]").val(form_values.idprofessor);
$("select[name=idmateria]").val(form_values.idmateria);
$("select[name=status1]").val(form_values.status1);
	
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});		
	}
	



	$('input[name=quantidadeaulas]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});

		
	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
 	$.post(baseUrl + "/"+controller+"/changestatusxml",
 	 	 	{id: $id, op: controller}, function(data) {
			if ((data=="Ativo") || (data=="Bloqueado")) {
				
				$.get(baseUrl + '/' + controller, {}, function(r) {
					$("table.table-content tbody").html($(r).find('table.table-content tbody').html());
					$.unblockUI();
				});
				
				
			} else {
				$.unblockUI();
				jAlert(data, "Alerta!");
			}
 	});
} 