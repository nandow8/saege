var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=iddepartamento]").val(form_values.iddepartamento);
		$("select[name=idlocais]").val(form_values.idlocais); 
		$("select[name=idsprofessores]").val(form_values.idsprofessores);
$("select[name=status1]").val(form_values.status1);
setDados($("select[name=idusuario]").val(form_values.idusuario));

		if ((typeof form_values.idsprofessores) != 'undefined') {
			setProfessores($("[name=idlocais]").get(0), form_values.idsprofessores);
		} 

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	$("input[name=telefone]").mask("(99)9?9999-9999", {placeholder:" "});
	$("input[name=celular]").mask("(99)9?9999-9999", {placeholder:" "});
	





	
	
	rules = {};
rules.idusuario = "required";
rules.telefone = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setDados(obj)
{
	var idusuario = $(obj).val();

	if(idusuario > 0)
	{
		$("#departamento").val('');
		$.post(baseUrl + "/" + module + "/usuarios/getusuario/", {idusuario:idusuario}, function(data) {
			data = JSON.parse(data.trim());

			if(data.status == "OK")
			{
				$('#box-info').show();
				$("#departamento").val(data.response.departamento);
			}
			else
			{
				$('#box-info').hide();
				jAlert(data.response, 'Alerta!');
			}
		});
	}
}

function setProfessores(obj, values) {
	obj = $(obj);
	val = obj.val();
	if (val != '') {
		$("#professores-container").html("Aguarde!");
		$.post(baseUrl + '/admin/comunicacaocomunicados/getprofessores', { idlocal: val }, function (professores) {
			professores = JSON.parse(professores);
			if (professores.length == 0) {
				$("#professores-container").html("Nenhum professor foi encontrado");
			} else {
				$("#professores-container").html("");
				for (var i = 0; i < professores.length; i++) {

					$("#professores-container").append('<input name="idsprofessores[]" type="checkbox" value="' + professores[i].id + '">' + professores[i].nome + ' <br />');
				}
				if (values) {
					$("[name='idsprofessores[]']").val(values);
				}
			}
		});
	} else {
		$("#professores-container").html("Selecione um local!");
	}
}