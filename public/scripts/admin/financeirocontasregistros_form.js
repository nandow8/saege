var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=origem]").val(form_values.origem);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idtipoconvenio]").val(form_values.idtipoconvenio);
		$("select[name=idconvenio]").val(form_values.idconvenio);
		$("select[name=idbanco]").val(form_values.idbanco);
		$("select[name=natureza]").val(form_values.natureza);
		$("select[name=tipo]").val(form_values.tipo);
		$("select[name=idtiporeceita]").val(form_values.idtiporeceita);
		$("select[name=idtipodespesa]").val(form_values.idtipodespesa);
		$("select[name=faturado]").val(form_values.faturado);
		$("select[name=status1]").val(form_values.status1);

		setDados($("select[name=idescola]"));
		setTipo($("select[name=tipo]"));
		if ((typeof form_values.idtipoconvenio) != 'undefined') {
			setConvenios($("[name=idtipoconvenio]"), form_values.idconvenio);
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	$("input[name=cpfcnpj]").mask("99.999.999/9999-99", {placeholder:" "});
	$("input[name=data], input[name=datapagamento]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data], input[name=datapagamento]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	$('input[name=valor]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '',
	    centsLimit: 2
	});

	rules = {};
	rules.idescola = "required";
	rules.lancamento = "required";
	rules.idbanco = "required";
	rules.titulo = "required";
	rules.tipo = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
});

$(window).load(function(){
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();

			val = $.trim(val);
			val = (val=='') ? '--' : val;
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		$("label span.required").remove();
	}
});

function settipobanco(obj, values) {
	obj = $(obj);
	val = obj.val();
	if (val != '') {
		$("#escolasapmscontas-container").html("Aguarde!");
		$.post(baseUrl + '/admin/financeirocontasregistros/gettipobanco', { idtipoconvenio: val }, function (escolasapmscontas) {
			escolasapmscontas = JSON.parse(escolasapmscontas);
			
			if (escolasapmscontas.length == 0) {
				$("#escolasapmscontas-container").html("Nenhum banco foi encontrado");
			} else {
				$("#escolasapmscontas-container").html("");
				for (var i = 0; i < escolasapmscontas.length; i++) {

					$("#escolasapmscontas-container").append('<option  value="' + escolasapmscontas[i].id + '">' + "Banco:"+ escolasapmscontas[i].banco + "  Agencia:" + escolasapmscontas[i].agencia + "  Conta:" + escolasapmscontas[i].conta + "  Digito:" + escolasapmscontas[i].digito +'</option>');
				}
				if (values) {
					$("[name='idbanco']").val(values);
					val = $("select[name=idbanco]").find('option:selected').html();
					$("select[name=idbanco]").closest('div').find('span').html(val);

					$("select[name=idbanco]").trigger("chosen:updated");
				}
			}
		});
	} else {
		$("#escolasapmscontas-container").html("Selecione um banco!");
	}
}

function setTipo(obj)
{
	var tipo = $(obj).val();

	if(tipo == 'Receita')
	{
		$('.box-receita').show();
		$('.box-despesa').hide();
		$('#boxdescricao').show();
		$('input[name=datapagamento]').val('');
		$('.box-datapagamento').hide();
	}
	else if(tipo == 'Despesa')
	{	
		$('.box-despesa').show();
		$('.box-receita').hide();
		$('#boxdescricao').show();
		$('.box-datapagamento').show();
	}
	else
	{
		$('.box-despesa').hide();
		$('.box-receita').hide();
	}
}

function setDados(obj, _value)
{
	var value = (_value > 0) ? _value : $(obj).val();

	if(value != '')
	{ $('.box-convenios').show(); }
	else
	{ $('.box-convenios').hide(); }
}

function setConvenios(obj, idconvenio){
	var idtipo = $(obj).val();

	$.post(baseUrl + "/" + module + "/"+controller+"/setconvenios",{idtipoconvenio: idtipo}, function(data) {
		$('select[name=idconvenio]').html(data);
		$("select[name=idconvenio]").val(idconvenio);
		val = $("select[name=idconvenio]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
			
		if((idconvenio!=0) && (idconvenio!="")){
			$("select[name=idconvenio]").val(idconvenio);
			setBancos($("select[name=idconvenio]"), form_values.idbanco);
		
			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
		}
	});
}

function setBancos(obj, idbanco){
	var idconvenio = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setbancos",{idconvenio: idconvenio}, function(data) {
		$('select[name=idbanco]').html(data);
		$("select[name=idbanco]").val(idbanco);
		val = $("select[name=idbanco]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

			
		if((idbanco!=0) && (idbanco!="")){
			$("select[name=idbanco]").val(idbanco);
		
			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
		}
	});
}