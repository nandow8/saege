var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {

	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=destino]").val(form_values.destino);
		setTipo($("select[name=destino]").val(form_values.destino));
$("select[name=iddepartamento]").val(form_values.iddepartamento);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=idprioridade]").val(form_values.idprioridade);
$("select[name=idstatus]").val(form_values.idstatus);
$("select[name=idresponsavel]").val(form_values.idresponsavel);
//$("select[name=finalizacao]").val(form_values.finalizacao);
setVisualizaTipo($("select[name=finalizacao]").val(form_values.finalizacao));

//$("select[name=atendido]").val(form_values.atendido);
$("select[name=idtipo]").val(form_values.idtipo);
$("select[name=idtipodaequipe]").val(form_values.idtipodaequipe);
if ((typeof form_values.idsfuncionarios)!='undefined')  $("[name='idsfuncionarios[]']").val(form_values.idsfuncionarios);
$("select[name=status1]").val(form_values.status1);
//setAtendido($("[name=finalizacao]").val(form_values.finalizacao), 0);
//setCamposAtendido($("select[name=atendido]").val(form_values.atendido));

		if ($("#atendido_nao").is(':checked')){
			$('.content-campos-nao-atendido').css('display', 'block');	
		}
		if ($("#atendido_sim").is(':checked')) {
			$('.content-campos-atentido').css('display', 'block');
			$('.content-campos-nao-atendido').css('display', 'block');
		}
			

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	



	$("input[name=dataagendada]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataagendada]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});









	
	
	rules = {};
	rules.sequencial = "required";
	rules.idescola = "required";
	rules.descricoes = "required";
	rules.idprioridade = "required";
	rules.idstatus = "required";
	rules.dataagendada = "required";
	rules.atendido = "required";
	rules.idsfuncionarios = "required";
	rules.servicoesdescricoes = "required";
	rules.servicosmateriais = "required";
	rules.observacoes = "required";
	//rules.idtipo = "required";
	rules.idtipodaequipe = "required";
	rules.idresponsavel = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	 
	
});

function setTipo(obj){
	var destino = $(obj).val();

	if(destino=='Escola'){
		$('.box-departmento').css('display', 'none');
		$('.box-escola').css('display', 'block');
	}else {
		$('.box-departmento').css('display', 'block');
		$('.box-escola').css('display', 'none');
	}
	
}

function setVisualizaTipo(obj){
	var finalizacao = $(obj).val();

	if(finalizacao=='sim'){
		$('.box-tipo-avaliacao').css('display', 'block');
	}else {
		$('.box-tipo-avaliacao').css('display', 'none');
	}
}

function setFinalizar(){
	jConfirm('Finalizar Chamado ?', 'Finalizar Chamado', function(r) {
		if (r) {
			$('input[name=finalizar]').val('sim');
			$('select[finalizacao]').val('sim');
			$( "#form" ).submit();
		}
 	});	

}

function setAtendido(obj, atendido){

	var v_atendido = $(obj).val();
	var vv_atendido = $(obj).attr("checked");
	//alert(v_atendido);
	//alert(vv_atendido);
	if((v_atendido=="sim") && (vv_atendido=="checked")){
		$('.content-atentido').css('display', 'block');
	}else{
		$('.content-atentido').css('display', 'none');	
	}
	
}

function setCamposAtendido(obj, atendido_c){
	var c_atendido = $(obj).val();
	var cc_atendido = $(obj).attr("checked");
//alert(c_atendido);
//alert(cc_atendido);
	if((c_atendido=="sim") && (cc_atendido=="checked")){
		$('.content-campos-atentido').css('display', 'block');
		$('.content-campos-nao-atendido').css('display', 'block');	
	}else{
		$('.content-campos-atentido').css('display', 'none');	
		$('.content-campos-nao-atendido').css('display', 'block');	
	}
	
}
