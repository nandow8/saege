﻿var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

jQuery(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};
	
	if ((typeof form_values)!='undefined') {
		$("select[name=status1]").val(form_values.status1);
		if ((typeof form_values.aprovacao)!='undefined') jQuery("select[name=aprovacao]").val(form_values.aprovacao);
		if ((typeof form_values.confirmaaprovacao)!='undefined') jQuery("select[name=confirmaaprovacao]").val(form_values.confirmaaprovacao);
		if ((typeof form_values.baixalocal)!='undefined') jQuery("select[name=baixalocal]").val(form_values.baixalocal);
		
		if ((typeof form_values.origem)!='undefined') {
			jQuery("select[name=origem]").val(form_values.origem);
			setOrigem(jQuery("select[name=origem]").val(form_values.origem));
		}
		if ((typeof form_values.iddepartamentosecretaria)!='undefined') jQuery("select[name=iddepartamentosecretaria]").val(form_values.iddepartamentosecretaria);
		if ((typeof form_values.idescola)!='undefined') jQuery("select[name=idescola]").val(form_values.idescola);
		if ((typeof form_values.iddepartamentoescola)!='undefined') jQuery("select[name=iddepartamentoescola]").val(form_values.iddepartamentoescola);
		if ((typeof form_values.idescola)!='undefined') setDepartamentos($("select[name=idescola]").val(form_values.idescola), form_values.iddepartamentoescola, form_values.idescolausuarioresponsavel);
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}
		
	jQuery('select, input, textarea').bind('blur', function() { });

	$( "input[name='quantidades[]']" ).blur(function() {
		v_idproduto = $(this).parent().parent().find("select[name='idsprodutos[]']").val();
		$(this).addClass("prod_" + v_idproduto);
		validaItens();
	});
	
	rules = {};
	rules.status1 = "required";
	rules.titulo = "required";
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
		    
		},		
		submitHandler: function(form) {
			unsaved = false;
			var v_quantidade = validaItens();
			if(v_quantidade=="OK") form.submit();
		}
	});

	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});	
});

function iteAddTr(item) {
	
	table = $(".itens table tbody#raiz");
	
	tr = $('<tr />');
	
	var classe_qtd="form-control";
	if (item===false) {
		item = {};
		item.id=0;
		item.idproduto = '';
		item.estoque = '';
		item.quantidade = '';
		item.observacoes = '';
		item.status = '';			
	}else{
		classe_qtd = "form-control prod_" + item.idproduto;
	}
	
	select_produtos = $('<select style="width: 178px!Important" class="form-control" name="idsprodutos[]" onchange="setEstoque(this)" />');
	select_produtos.append('<option value="">Selecione...</option>');
	for (var i=0; i<produtos.length; i++) {
		p = produtos[i];
		select_produtos.append('<option value="'+p.id+'">'+p.produto+'</option>');
	}
	select_produtos.val(item.idproduto);
	$(select_produtos).trigger("chosen:updated");
	td_produtos = $('<td />');
	td_produtos.append(select_produtos);
		
	tr.append('<td><input style="width: 50px!Important" type="text" maxlength="255" name="cod[]" class="form-control"/></td>');
	tr.append(td_produtos);
	tr.append('<td><input style="width: 50px!Important" type="text" maxlength="255" name="unid[]" class="form-control"/></td>');
	tr.append('<td><input style="width: 50px!Important" type="text" maxlength="255" name="marca[]" class="form-control"/></td>');
	tr.append('<td><input style="width: 50px!Important" value="'+item.estoque+'" type="text" maxlength="255" name="estoque[]" class="estoque form-control" readonly="readonly" /></td>');	
	tr.append('<td><input name="idssaidas[]" type="hidden" value="'+item.id+'" /><input style="width: 50px!Important" value="'+item.quantidade+'" type="text" maxlength="255" class="'+classe_qtd+'" name="quantidades[]" /><label class="error_qtd" style="display:none; color: #b94a48; margin-top: 2px;">N° Inválido.</label></td>');
	tr.append('<td><textarea name="itensobservacoes[]" class="form-control">'+item.observacoes+'</textarea></td>');
	
	tr.append('<td style="text-align:center!Important" align="center"><a href="javascript:;" onclick="iteExcluirFromTable(this)"><i class="icon-remove"></i></a></a></td>');
	qtdMask(tr);
	table.append(tr);
	setBlur();
}

function iteExcluirFromTable(obj) {
	$(obj).parent().parent().remove();
}

function qtdMask(tr) {
   $("input[name='quantidades[]']").priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',	    
	    centsLimit: 0    
	});	
}

function validaItens(){
	var retorno = "OK";
	$("input[name='quantidades[]']").each(function() {
		quantidade = $(this).val();
		estoque = $(this).parent().parent().find("input[name='estoque[]']").val();

		if (parseInt(quantidade) > parseInt(estoque)){
			retorno = "erro";
			$(this).parent().parent().find(".error_qtd").css("display", "block");
		}else{			
			$(this).parent().parent().find(".error_qtd").css("display", "none");
			if(retorno != "erro") retorno = "OK";
		}
		
		/*MAIS DE UMA LINHA COM MESMO PRODUTO*/
		var qtdtotal = 0;
		mp_idproduto = $(this).parent().parent().find("select[name='idsprodutos[]']").val();

		$(".prod_" + mp_idproduto).each(function() {
			qtdtotal = qtdtotal + parseInt($(this).val());
		});

		if(qtdtotal > estoque){
			$(".prod_" + mp_idproduto).each(function() {
				$(this).parent().find(".error_qtd").css("display", "block");
			});
			retorno = "erro";
		}
	});

	return retorno;
}

function setBlur(){
	v_idproduto = 0;
	$( "input[name='quantidades[]']" ).blur(function() {
		v_idproduto = $(this).parent().parent().find("select[name='idsprodutos[]']").val();
		$(this).addClass("prod_" + v_idproduto);
		validaItens();
	});
}

function setEstoque(obj){
	$idproduto = $(obj).val();
	$idsaida = $("input[name=id]").val();
	$.post(baseUrl + "/admin/"+controller+"/quantidadeestoque",	{idproduto: $idproduto, idsaida: $idsaida}, function(data) {	
		$(obj).parent().parent().find("input[name='estoque[]']").val(parseInt(data));
		$(obj).parent().parent().find("input[name='quantidades[]']").attr('class','form-control');
	});
	
	
}

function setOrigem(obj) {
	var val = $(obj).val();	
	if(val=="escola"){
		$(".box-escola").css("display", "block");
		$(".box-secretaria").css("display", "none");
		$("select[name=iddepartamentosecretaria]").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select[name=iddepartamentosecretaria]").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
		//$("select[name=iddepartamentosecretaria]").closest('div').find('span').html('Selecione...');
		
	}else{
		$(".box-secretaria").css("display", "block");
		$(".box-escola").css("display", "none");	
		$("select[name=idescola]").closest('div').find('span').html('Selecione...');
		$("select[name=iddepartamentoescola]").closest('div').find('span').html('Selecione...');
	}
	
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
}


function setDepartamentos(obj, iddepartamentoescola){
	var idescola = $(obj).val();
	data = '';
	$("select[name=iddepartamentoescola]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/almoxarifadosaidas/setdepartamentosescolas/', {idescola:idescola}, function(data) {
		$("select[name=iddepartamentoescola]").html(data);

		$("select[name=iddepartamentoescola]").val(iddepartamentoescola);
		val = $("select[name=iddepartamentoescola]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(iddepartamentoescola==""){
			$("select[name=iddepartamentoescola]").closest('div').find('span').html('Selecione...');
		}
	});
}