
$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		if ((typeof form_values.tipo)!='undefined') {
			if (form_values.tipo=="INTEGER") {
				$('input[name=valor]').priceFormat({
				    prefix: '',
				    centsSeparator: '',
				    thousandsSeparator: '',
				    centsLimit: 0
				});				
			}
		}
	}
	
	$('select, input, textarea').bind('blur', function() {
		verificaErros($('#form').get(), false);
	});
	
});

function formSubmit(obj) {
	erros = verificaErros(obj, true);
	
	if (!erros) {
		scroll(0, 0);
		return false;
	}
	blockPage();
}

function verificaErros(obj, display) {
	
	erros = getFormErros(obj, display);
	setErrosBox(erros, display);
	
	if (erros.length>0) {
		return false;
	}
	return true;
}	

