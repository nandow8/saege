$(document).ready(function() {
    if ((typeof form_values) != 'undefined') {        
        $("select[name=idsecretaria]").val(form_values.idsecretaria);
        $("select[name=idlocais]").val(form_values.idlocais);
        $("select[name=idsdepartamentossecrtaria]").val(form_values.idsdepartamentossecrtaria);
        $("select[name=idsprofessores]").val(form_values.idsprofessores);
        $("select[name=status1]").val(form_values.status1);
        $('input[name=datacriacao]').val(form_values.datacriacao);
        $('input[name=titulo]').val(form_values.titulo);
        $('select[name=idperfilorigem]').val(form_values.idperfilorigem);
        $('select[name=idescolaorigem]').val(form_values.idescolaorigem);

        $('select[name=idperfildestino]').val(form_values.idperfildestino);
        $('select[name=idescoladestino]').val(form_values.idescoladestino);
       
        if (form_values.idperfilorigem == '28' || form_values.idperfilorigem == '29'){
            setEscolas('select[name=idperfilorigem]', form_values.idescolaorigem, 'origem');
        }else if (form_values.idperfildestino == '28' || form_values.idperfildestino == '29')
            setEscolas('select[name=idperfildestino]', form_values.idescoladestino, 'destino');

        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        tmp_idperfil = $('input[name=tmp_idperfil]').val();
        tmp_idescola = $('input[name=tmp_idescola]').val();
        
        if (form_values.idperfilorigem && form_values.idperfilorigem !== tmp_idperfil) {            
            setEscolas('select[name=idperfilorigem]', form_values.idescolaorigem, 'origem');
        } else if (form_values.idperfildestino && form_values.idperfildestino !== tmp_idperfil) {
            setEscolas('select[name=idperfildestino]', form_values.idescoladestino, 'destino');
        }
    }

    $("input[name=datacriacao]").mask("99/99/9999", {placeholder: " "});
    $("input[name=datacriacao]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });
});

function verificaEscola(obj, item){
    idescola = $(obj).val();
    path = (item === 'destino') ? 'origem' : 'destino';
    idperfil = $('select[name=idperfil'+item+']').val();
    
    if(idperfil == '29' && idperfil == form_values.perfilAtivo && 
        idescola != form_values.escolaAtiva){
            $('select[name=idperfil' + path + ']').val(form_values.perfilAtivo);
            $('select[name=idperfil' + path + ']').prop("disabled", true);
          
            setEscolas('select[name=idperfil'+item+']', idescola, item);
        }
}
function setSelect(obj) {
    $("select").each(function() {
        val = $(this).find('option:selected').html();
        $(this).closest('div').find('span').html(val);
    });
}
function setEscolas(obj, idescola, item) {

    idperfil = $(obj).val();
    
    tmp_idperfil = $('input[name=tmp_idperfil]').val();//id perfil logado
    tmp_idescola = $('input[name=tmp_idescola]').val();//id escola logada

    path = (item === 'destino') ? 'origem' : 'destino';

    //se perfil de SUPER ADMIN, não entrar nessa função
    if (tmp_idperfil != 30 && tmp_idperfil != 1) {       
        if (idperfil != "" && idperfil > 0) {
            
            if (tmp_idperfil !== idperfil) {
                //PERFIL
                $('select[name=idperfil' + path + ']').val(tmp_idperfil);
                setSelect($('select[name=idperfil' + path + ']'));

                $('select[name=idperfil' + path + ']').attr("disabled", "disabled");
                $('select[name=idperfil' + path + ']').removeAttr('name');
                $('#helper_idperfil' + path).attr('name', 'idperfil' + path);

                //ESCOLA
                if (tmp_idescola) {
                    $('.box-escolas-' + path).css('display', 'block');
                    $('select[name=idescola' + path + ']').val(tmp_idescola);
                    setSelect($('select[name=idescola' + path + ']'));

                    $('select[name=idescola' + path + ']').attr("disabled", "disabled");
                    $('select[name=idescola' + path + ']').removeAttr('name');
                    $('#helper_idescola' + path).attr('name', 'idescola' + path);
                }
            } else {
                $('select[name=idperfil' + path + ']').attr("readonly", false);
                $('select[name=idescola' + path + ']').css('disabled', false);
            }
        } else {
            $('.box-escolas-' + path).fadeOut('fast');
            $('#idperfil' + path).val('');
            $('#idescola' + path).val('');

            $('#idescola' + path).attr('name', 'idescola' + path).prop('disabled', false);
            $('#idperfil' + path).attr('name', 'idperfil' + path).prop('disabled', false);
            setSelect($('select[name=idescola' + path + ']'));
        }
    }

    if (idperfil == 28 || idperfil == 29) {
        $('.box-escolas-' + item).show();
        if (idescola > 0)
            $('select[name=idescola' + item + ']').val(idescola);
    } else {
        $('.box-escolas-' + item).hide();
        $('select[name=idescola' + item + ']').val('');
        setSelect('select[name=idescola' + item + ']');
    }
}

function confirmDelete($id, $texto) {
    jConfirm('Confirmar a exclusão de "' + $texto + '"?', 'Excluir registro', function(r) {
        if (r) {
            $.post(baseUrl + "/" + module + "/" + controller + "/excluirxml",
                    {id: $id}, function(data) {
                if (data == "OK") {
                    window.location = baseUrl + "/" + module + '/' + controller;
                } else {
                    jAlert(data, "Erro!");
                }
            });
        }
    });
}

function statusChange($id, $obj) {
    $obj = $($obj);

    $.post(baseUrl + "/" + module + "/" + controller + "/changestatusxml",
            {id: $id, op: controller}, function(data) {
        if ((data == "Ativo") || (data == "Bloqueado")) {
            $obj.html(data);
        } else {
            jAlert(data, "Alerta!");
        }
    });
}