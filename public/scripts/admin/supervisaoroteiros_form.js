var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescolafuncionario]").val(form_values.idescolafuncionario);
		$("select[name=secretaria]").val(form_values.secretaria);
		$("select[name=periodo]").val(form_values.periodo);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idtipoensino]").val(form_values.idtipoensino);
		$("select[name=idequipesupervisao]").val(form_values.idequipesupervisao);
		$("select[name=idperiodosupervisao]").val(form_values.idperiodosupervisao);
		//$("select[name=idveiculo]").val(form_values.idveiculo);
		$("select[name=status1]").val(form_values.status1);
		
		if ((typeof form_values.idsescolas)!='undefined')
		{
			$("[name='idsescolas[]']").val(form_values.idsescolas);
		}

		if ((typeof form_values.tipo)!='undefined') {
			if(form_values.idequipesupervisao>0) $("#equipe").attr("checked", true);
			if(form_values.idescolafuncionario>0) $("#individual").attr("checked", true);
			setTipo($("[name=tipo]"), form_values.tipo);
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}
	
	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	
	rules = {};
	rules.data = "required";
	rules.secretaria = "required";
	rules.periodo = "required";
	rules.status1 = "required";
	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {

		$("div.control-group input, div.control-group textarea").not('[type=hidden], [name=tipo], .ind-escola').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();		
		
		//campo radio - tipo de funcionário
		$("input[name=tipo]::checked").each(function(){
			val = $(this).val();

			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			$('#destinatario').html('');
			parent.append(val.toUpperCase());
			parent.addClass('visualizar');
		});

		if(typeof form_values.idsescolas!="undefined" && form_values.idsescolas.length>0){
			$('.ind-escola').each(function(){
				if($(this).is(':checked')){
					parent = $(this).parent();
					
					parent.css('font-weight', 'bold');
					$(this).remove();
				}else{
					$(this).parent().remove();
				}
			});
		}

		//console.log(form_values.idsescolas);
	}		
	
});


/*function getVeiculo(obj)
{
	var idveiculo = $(obj).val();

	data = '';
	$(".box-veiculo").html('');
	$.post(baseUrl + '/admin/'+controller+'/getveiculo/', {idveiculo:idveiculo}, function(data) {
		$(".box-veiculo").html(data);
	});
}*/
function resetSelect(obj){
	$(obj).each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});
}

function setTipo(obj, tipo)
{
	var obj = (tipo) ? tipo : $(obj).val();

	if((obj == 'equipe' || tipo == 'equipe'))
	{
		$('.box-equipe').show();
		$('.box-individual').hide();
		$("select[name=idescolafuncionario]").attr("required", false);
		$("select[name=idequipesupervisao]").attr("required", true);		
		$("select[name=idescolafuncionario]").val('');
		resetSelect("select[name=idescolafuncionario]");
	}
	else if((obj == 'individual' || tipo == 'individual'))
	{
		$('.box-individual').show();
		$('.box-equipe').hide();
		$("select[name=idequipesupervisao]").attr("required", false);
		$("select[name=idescolafuncionario]").attr("required", true);
		$("select[name=idequipesupervisao]").val('');
		resetSelect("select[name=idequipesupervisao]");		
	}
	else
	{
		$('.box-equipe').hide();
		$('.box-individual').hide();
		//$("select[name=idescolafuncionario]").val('');
		//$("select[name=idequipesupervisao]").val('');
	}
}