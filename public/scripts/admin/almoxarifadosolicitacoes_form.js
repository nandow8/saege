﻿var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

jQuery(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};
	
	if ((typeof form_values)!='undefined') {
		if ((typeof form_values.status)!='undefined') jQuery("select[name=status]").val(form_values.status);
		if ((typeof form_values.aprovacao)!='undefined') jQuery("select[name=aprovacao]").val(form_values.aprovacao);
		if ((typeof form_values.confirmaaprovacao)!='undefined') jQuery("select[name=confirmaaprovacao]").val(form_values.confirmaaprovacao);
		if ((typeof form_values.confirmasolicitacao)!='undefined') jQuery("select[name=confirmasolicitacao]").val(form_values.confirmasolicitacao);
		
		if ((typeof form_values.origem)!='undefined') {
			jQuery("select[name=origem]").val(form_values.origem);
			setOrigem(jQuery("select[name=origem]").val(form_values.origem));
		}
		if ((typeof form_values.idusuariosecretaria)!='undefined') jQuery("select[name=idusuariosecretaria]").val(form_values.idusuariosecretaria);
		if ((typeof form_values.idescola)!='undefined') jQuery("select[name=idescola]").val(form_values.idescola);
		if ((typeof form_values.idusuarioescola)!='undefined') jQuery("select[name=idusuarioescola]").val(form_values.idusuarioescola);
		$("select").trigger("chosen:updated");
	}
		
	jQuery('select, input, textarea').bind('blur', function() { });
	
	$("input[name=quantidade]").priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',	    
	    centsLimit: 0    
	});	
	
	rules = {};
	rules.status = "required";
	rules.titulo = "required";
	rules.quantidade = "required";
	rules.origem = "required";
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
		    
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});	
	
	
});
function setOrigem(obj) {
	var val = $(obj).val();	
	if(val=="escola"){
		$(".box-escola").css("display", "block");
		$("select[name=idescola]").closest('div').find('span').html('Selecione...');
		
	}else{
		$(".box-escola").css("display", "none");	
	}
	
	$("select").each(function (){
		div = $(this).closest('div').find('.chosen-container').css('width', '100%');
	});
}
