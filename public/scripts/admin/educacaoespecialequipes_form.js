var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idfuncionario]").val(form_values.idfuncionario);
$("select[name=idfuncao]").val(form_values.idfuncao);
$("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	



	
	
	rules = {};
	rules.nome = "required";
rules.idfuncionario = "required";
rules.idfuncao = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});


function iteAddTr(item) {
	
	table = $(".itens table tbody#raiz");
	
	tr = $('<tr />');
	
	var classe_qtd="form-control";
	if (item===false) {
		item = {};
		item.id=0;
		item.idfuncionario = '';
		item.idfuncao = '';		
	}else{
		classe_qtd = "form-control prod_" + item.idfuncionario;
	}
	
	select_funcionarios = $('<input type="hidden" value="'+item.id+'" name="idmembro[]" /><select style="width: 178px!Important" class="form-control" name="idfuncionario[]" />');
	select_funcionarios.append('<option value="">Selecione...</option>');
	for (var i=0; i<funcionarios.length; i++) {
		d = funcionarios[i];
		select_funcionarios.append('<option value="'+d.k+'">'+d.v+'</option>');
	}
	//select_funcionarios.append('<option value="1">Funcionário 1</option>');
	//select_funcionarios.append('<option value="2">Funcionário 2</option>');

	select_funcionarios.val(item.idfuncionario);
	$(select_funcionarios).trigger("chosen:updated");
	td_funcionarios = $('<td />');
	td_funcionarios.append(select_funcionarios);
	tr.append(td_funcionarios);


	select_funcoes = $('<select style="width: 178px!Important" class="form-control" name="idfuncao[]" />');
	select_funcoes.append('<option value="">Selecione...</option>');

	select_funcoes.val(item.idfuncionario);
	$(select_funcoes).trigger("chosen:updated");
	td_funcoes = $('<td />');
	td_funcoes.append(select_funcoes);
	tr.append(td_funcoes);

	tr.append('<td style="text-align:center!Important" align="center"><a href="javascript:;" onclick="iteExcluirFromTable(this)"><i class="icon-remove"></i></a></a></td>');
	qtdMask(tr);
	table.append(tr);
	setBlur();
}


function iteExcluirFromTable(obj) {
	$(obj).parent().parent().remove();
}

function qtdMask(tr) {
   $("input[name='quantidades[]']").priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',	    
	    centsLimit: 0    
	});	
}

function validaItens(){
	var retorno = "OK";
	$("input[name='quantidades[]']").each(function() {
		quantidade = $(this).val();
		estoque = $(this).parent().parent().find("input[name='estoque[]']").val();

		if (parseInt(quantidade) > parseInt(estoque)){
			retorno = "erro";
			$(this).parent().parent().find(".error_qtd").css("display", "block");
		}else{			
			$(this).parent().parent().find(".error_qtd").css("display", "none");
			if(retorno != "erro") retorno = "OK";
		}
		
		/*MAIS DE UMA LINHA COM MESMO PRODUTO*/
		var qtdtotal = 0;
		mp_idproduto = $(this).parent().parent().find("select[name='idsprodutos[]']").val();

		$(".prod_" + mp_idproduto).each(function() {
			qtdtotal = qtdtotal + parseInt($(this).val());
		});

		if(qtdtotal > estoque){
			$(".prod_" + mp_idproduto).each(function() {
				$(this).parent().find(".error_qtd").css("display", "block");
			});
			retorno = "erro";
		}
	});

	return retorno;
}

function setBlur(){
	v_idproduto = 0;
	$( "input[name='quantidades[]']" ).blur(function() {
		v_idproduto = $(this).parent().parent().find("select[name='idsprodutos[]']").val();
		$(this).addClass("prod_" + v_idproduto);
		validaItens();
	});
}