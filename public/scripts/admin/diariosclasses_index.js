$(document).ready(function() {
    if ((typeof form_values) != 'undefined') {
        cmp = '';
        cmp = (idperfil === '29') ? 'input' : 'select';
        campo = cmp + '[name=idescola]';

        if (idperfil !== '29')
            $('select[name=idescola]').val(form_values.idescola);
        $("select[name=idescolavinculo]").val(form_values.idescolavinculo);
        $("select[name=idmateria]").val(form_values.idmateria);
        $("select[name=idprofessor]").val(form_values.idprofessor);
        $("select[name=idaluno]").val(form_values.idaluno);
        $("select[name=status1]").val(form_values.status1);


        setSeries(campo, 0);
        setTurmas(campo, 0);
        if (form_values.idserie)
            setSeries(campo, form_values.idserie);

        if (form_values.idturma)
            setTurmas(campo, form_values.idturma);



        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
    }
    $("input[name=data_i],input[name=data_f]").mask("99/99/9999", {placeholder: " "});
    $("input[name=data_i],input[name=data_f]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });
});

function confirmDelete($id, $texto) {
    jConfirm('Confirmar a exclusão de "' + $texto + '"?', 'Excluir registro', function(r) {
        if (r) {
            $.post(baseUrl + "/admin/" + module + "/" + controller + "/excluirxml",
                    {id: $id}, function(data) {
                if (data == "OK") {
                    window.location = baseUrl + "/" + module + '/' + controller;
                } else {
                    jAlert(data, "Erro!");
                }
            });
        }
    });
}

function statusChange($id, $obj) {
    $obj = $($obj);

    $.post(baseUrl + "/" + module + "/" + controller + "/changestatusxml",
            {id: $id, op: controller}, function(data) {
        if ((data == "Ativo") || (data == "Bloqueado")) {
            $obj.html(data);
        } else {
            jAlert(data, "Alerta!");
        }
    });
}

function setSeries(obj, serie) {
    var val = $(obj).val();

    data = '';
    $("select[name=serie]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/diariosclasses/setseries/', {idescola: val}, function(data) {

        $("select[name=idserie]").html(data);
        $("select[name=idserie]").val(serie);
        val = $("select[name=idserie]").find('option:selected').html();
        $("select[name=idserie]").closest('div').find('span').html(val);

        $("select[name=idserie]").trigger("chosen:updated");

        $("input[name=idtipoensino]").val($.trim($("#idserie").children("option:selected").text()).split(".")[0]);

        if (serie === "") {
            $("select[name=serie]").closest('div').find('span').html('Todos');
            $("select[name=idturma]").closest('div').find('span').html('Todos');
        }
    });
    setTurmas(obj, 0);
}
function setTurmas(obj, idturma) {
    var val = $(obj).val();

    data = '';
    $("select[name=idturma]").html('<option value="">Aguarde...</option>');
    $.post(baseUrl + '/admin/diariosclasses/setturmas/', {idescola: val}, function(data) {
        $("select[name=idturma]").html(data);
        $("select[name=idturma]").val(idturma);
        val = $("select[name=idturma]").find('option:selected').html();
        $("select[name=idturma]").closest('div').find('span').html(val);

        $("select[name=idturma]").trigger("chosen:updated");

        if (idturma === "") {
            $("select[name=idturma]").closest('div').find('span').html('Todos');
        }
    });
}
/*
 * Função: Redimensionar o select da classe 'SELECT'
 * @param min - tamanho mínimo do select
 * @param max - tamanho máximo do select
 */
$(function() {
    $(".select2-container").css({
        'min-width': '140px',
        'max-width': '300px'
    });
});