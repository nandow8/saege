$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=status1]").val(form_values.status1);	
		$("select[name=idperfil]").val(form_values.idperfil);
		
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});		
	}
	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/admin/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/admin/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
 	$.post(baseUrl + "/admin/"+controller+"/changestatusxml",
 	 	 	{id: $id, op: controller}, function(data) {
			if ((data=="Ativo") || (data=="Bloqueado")) {
				
				$.get(baseUrl + '/admin/' + controller, {}, function(r) {
					$("table.table-content tbody").html($(r).find('table.table-content tbody').html());
					$.unblockUI();
				});
				
				
			} else {
				$.unblockUI();
				jAlert(data, "Alerta!");
			}
 	});
} 