var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {		
		$("select[name=idescola]").val(form_values.idescola);
		//$("select[name=idserie").val(form_values.idsseries);
		$("select[name=idmateria]").val(form_values.idmateria);
		$("select[name=idtitulonota]").val(form_values.idstitulos);
		$("select[name=status1]").val(form_values.status1);
		
		if(typeof form_values.idsseries !='undefined'){ 			
			$.each(form_values.idsseries.split(","), function(i,e){			 
				$("#series option[value='" + e + "']").prop("selected", true);
			});
		}
		var seriePreLoad = [];//array
		var serieNamePreLoad = [];
		$("#series option:selected").each(function() {			
			valor = $(this).val();
			texto = $(this).text(); // Pegando as opções selecionada
			serieNamePreLoad.push(texto);//Add valores no array
			seriePreLoad.push(valor);//Add valores no array
		});
		if(visualizar && serieNamePreLoad){
			$(".group-serie").remove();
			$.each(serieNamePreLoad, function(index, palavra){
				textReceptor = "<li class='select2-search-choice'><b>"+palavra+"</b></li>"
				$("#series-selected").append(textReceptor);
			});
			
		}
		$('#series').val(seriePreLoad).change();//setar valores no select
			
		if(typeof form_values.idstitulos !='undefined' && typeof form_values.percentualnotas !='undefined'){
			// transformar string em array
			var arrayPercentual = form_values.percentualnotas.split(",");
			//setar títulos e notas
			$.each(form_values.idstitulos.split(","), function(i,e){							
				$("#field_"+e).prop("disabled", false).css("border-color", "#02a302").val(arrayPercentual[i]);				
				
				$("#titulo_"+e).prop("checked", "checked");
			});
		}

		if ((typeof form_values.idserie)!='undefined') {
			setSeries($("select[name=idescola]"), form_values.idserie);
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}

	$('.notasmask').priceFormat({
	    prefix: '',
	    centsSeparator: '.',
	    thousandsSeparator: '.',
	    centsLimit: 2
	});

	rules = {};
	rules.idescola = "required";
	rules.idserie = "required";
	rules.idmateria = "required";
	rules.idtitulonota = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	$(window).load(function(){
		if (visualizar) {
			$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
				val = $(this).val();
				val = $.trim(val);
				val = (val=='') ? '--' : val;			
				parent = $(this).parent();
				parent.append(val);
				parent.addClass('visualizar');

				$(this).remove();
			});
			$("div.control-group select").not('[type=hidden]').each(function() {
				val = $(this).find('option:selected').html();
				val = $.trim(val);
				val = (val=='') ? '--' : val;				
				parent = $(this).parent();
				parent.html(val);
				parent.addClass('visualizar');

			});		
			
			$("label span.required").remove();
		}
	});
	
});

function setSeries(obj, idserie){
	var val = $(obj).val();
	
	data = '';
	$("#series" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/'+controller+'/setseries/', {idescola:val}, function(data) {
		$("#series").html(data);
		$("#series").val(idserie);
		
		val = $("#series").find('option:selected').html();
		$("#series").closest('div').find('span').html(val);
	
		$("#series").trigger("chosen:updated");
		
		if(idserie==""){
			$("#series").closest('div').find('span').html('Selecione...');
		}
	});
}

function setNotas(obj, id){
	if($(obj).is(":checked") == true){
		$("#field_"+id).css("border-color", "#02a302");
		$("#field_"+id).prop("disabled", false).focus();
	}else{
		$("#field_"+id).css("border-color", "#ccc").val("");
		$("#field_"+id).prop("disabled", true);
	}
}