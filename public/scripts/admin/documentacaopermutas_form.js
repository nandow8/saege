var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idtipoensino]").val(form_values.idtipoensino);
		$("select[name=idperiodo]").val(form_values.idperiodo);
		$("select[name=status1]").val(form_values.status1);
		$("select[name=posicao]").val(form_values.posicao);
		setProfessores($("select[name=idescola]"),form_values.idprofessor);

		if ((typeof form_values.idperiodo)!='undefined') {
			setPeriodos($("select[name=idescola]"), form_values.idperiodo);
		}
		if ((typeof form_values.idsescolas)!='undefined') {
			$("input[name='idsescolas[]']").val(form_values.idsescolas)	
		}
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	}
	$("input[name=horario]").mask("99:99", {placeholder:" "});

	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	rules = {};
	//rules.data = "required";
	//rules.idprofessor = "required";
	rules.idescola = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
	}
});

function setProfessores(obj , idprofessor){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/documentacaoavaliacoesprofissionais/setprofessores",{idescola: idescola}, function(data) {
		$('select[name=idprofessor]').html(data);
		
		$("select[name=idprofessor]").val(idprofessor);
		val = $("select[name=idprofessor]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idprofessor!=0) && (idprofessor!="")){
			$("select[name=idprofessor]").closest('div').find('span').html('Selecione...');
		
			$("select").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
		}

	});
}

function setPeriodos(obj, idperiodo){
	var val = $(obj).val();
	
	data = '';
	$("select[name=idperiodo]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/documentacaosolicitacoesremocao/setperiodos/', {idescola:val}, function(data) {
		$("select[name=idperiodo]").html(data);
		$("select[name=idperiodo]").val(idperiodo);
		val = $("select[name=idperiodo]").find('option:selected').html();
		$("select[name=idperiodo]").closest('div').find('span').html(val);
	
		$("select[name=idperiodo]").trigger("chosen:updated");
		
		if(idperiodo==""){
			$("select[name=idperiodo]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setProfessor(obj){
	var val = $('input[name=idprofessor]').val();
	
		//console.log(val)
	if(val == '')
		return false;
	
	$.post(baseUrl + '/admin/'+controller+'/setprofessor/', {idprofessor:val}, function(data) {
		console.log(data)
	});
}



function setAprovar($id, $idfuncionario, $texto) {
	jConfirm('Confirma a permuta entre os professores?', 'Aceitar Solicitação', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/aprovarpermuta",
		   	{id: $id, idfuncionario: $idfuncionario}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}



function setRejeitar($id, $idfuncionario, $texto) {
	jConfirm('Confirma o cancelamento dessa solicitação de permuta?', 'Rejeitar Solicitação', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/rejeitarpermuta",
		   	{id: $id, idfuncionario: $idfuncionario}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}