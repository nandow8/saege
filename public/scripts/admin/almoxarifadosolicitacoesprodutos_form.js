$(document).ready(function() {
    $('#find-produtos').click(function() {

        var idproduto = $('#idproduto').val();
        var codigoproduto = $('#codigoproduto').val();
        var categoria = $('#categoria').find('option:selected').val();
        var unidademedida = $('#unidademedida').find('option:selected').val();
        var produto = $('#produto').val();
        var descricao = $('#descricao').val();

        $.post(baseUrl + '/admin/almoxarifadosolicitacoesprodutos/findprodutos/', {idproduto: idproduto, codigoproduto: codigoproduto, categoria: categoria, unidademedida: unidademedida, produto: produto, descricao: descricao}, function(data) {
            if (data == 0) {
                $('#modal-produtos').html('Nenhum produto encontrado');
            } else if (data == -1) {
                $('#modal-produtos').html('Utilize mais critérios de busca');
            } else {
                $('#produtos').css('height', '550px');
                $('#modal-produtos').html(data);
            } 
        });
    });

    $('#add-produto').click(function() {

        itens = new Array();
        $("input[type=checkbox][name='item[]']:checked").each(function(){
            itens.push($(this).val());
        });

        itens.forEach(function (iditem) {

            var codigoproduto = $('#codigoproduto_'+iditem).val();
            var produto = $('#produto_'+iditem).val();
            var unidademedida = $('#unidademedida_'+iditem).val();

            var verifica = findduplicados(iditem);
            if (!verifica) {
                var tr = '<tr><td>'+codigoproduto+'</td><td>'+produto+'</td><td>'+unidademedida+'</td><td>0</td><td>0</td><td>0</td><td><input type="number" name="qtd_selecionada[]" data-item="'+iditem+'" min="0" required ><input type="hidden" name="id_selecionado[]" value="'+iditem+'"></td></tr>';
                $('#solicitacao').append(tr);
            }
        });
    });

    rules = {};
    messages = {};
    
    $("#form").validate({
        ignore: [],
        rules: rules,       
        messages: messages,
        submitHandler: function(form) {
            unsaved = false;
            $.blockUI({ message: '<h1> Aguarde...</h1>' }); 
            form.submit();
        }
    });
});

function ckbox() {
    if($('#all-checkbox').is(':checked')) {
        $('.ckbox').prop('checked', true);
    } else {
        $('.ckbox').prop('checked', false);
    }
}

function findduplicados(item) {

    itensduplicados = new Array();
    $("input[type=number][name='qtd_selecionada[]']").each(function() {
        itensduplicados.push($(this).data('item'));
    });

    var i = 0;

    while (i < itensduplicados.length) {
        if (itensduplicados[i] == item) {
            return true;
        } else {
            i++;
        }
    }

    return false;
}