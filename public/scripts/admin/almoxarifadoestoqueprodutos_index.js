$(document).ready(function() {
    if ((typeof form_values)!='undefined') {
        if ((typeof form_values.categoria)!='undefined') $("select[name=categoria]").val(form_values.categoria);
        if ((typeof form_values.zerados)!='undefined' && form_values.zerados == 'on') $("input[name=zerados]").attr('checked', 'true');
    }

    $("[name=codigo]").mask("999.999.9999");
});