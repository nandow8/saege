var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

function setSeries( idserie, idescolavinculo, idaluno, media){
	var idescola = $("select[name=idescola]").val();
	console.log(idescola);
	$.post(baseUrl + "/" + module + "/"+controller+"/setseries",{idescola: idescola}, function(data) {
		console.log(data);
		$('select[name=idserie]').html(data);
		$("select[name=idserie]").val(idserie);
		val = $("select[name=idserie]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idserie!=0) && (idserie!="")){
			$("select[name=idserie]").closest('div').find('span').html('Selecione...');
			
			if((idescolavinculo!=0) && (idescolavinculo!="")){
				$("select[name=idserie]").val(idserie);
				setTurmas(idescolavinculo, idaluno, media);
			}
		}
	});
}

function setTurmas( idturma, idaluno, media){

	var idserie = $("select[name=idserie]").val();
	var idescola = $("select[name=idescola]").val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setturmas",{idserie: idserie, idescola:idescola}, function(data) {
console.log(idserie);
console.log(idescola);
console.log(data);
		$('select[name=idturma]').html(data); 	
		
		$("select[name=idturma]").val(idturma);
		val = $("select[name=idturma]").find('option:selected').html();

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idturma!=0) && (idturma!="")){
			$("select[name=idturma]").closest('div').find('span').html('Selecione...');

			$("select[name=idturma]").val(idturma);
			setAlunos(idaluno, media);
		}
	});
}

function setAlunos( idaluno , media ){

	var idescolavinculo = $("select[name=idturma]").val();
	var idescola = $("select[name=idescola]").val();
	var idserie = $("select[name=idserie]").val();
	var idtipoavaliacao = $("select[name=idtipoavaliacao]").val();
	console.log(idescolavinculo);
	console.log(idtipoavaliacao);
	if( idescolavinculo != ""  ) {
		console.log('aqui');
		$.post(baseUrl + "/" + module + "/"+controller+"/setalunos",{idvinculo: idescolavinculo, idserie: idserie, idescola:idescola, idtipoavaliacao:idtipoavaliacao}, function(data) {
			console.log(data);
			$('#tablehelp').html(data);
			$('#tablehelp').show();

			if( media != undefined) {
				var i=0;
				var medias = (media).split(",");
				medias = medias.slice(0).reverse();
				$("input[type=hidden][name='acertos[]']").each(function (){
					$(this).val(medias[i]);
					$(this).parent().find("td[class=acertos]").html(medias[i] + "%");

					i++;
				});
			}

			if(idaluno!=undefined && idaluno != ""){
				var alunos = idaluno.split(',');
				
				$("[name='idaluno[]']").val(alunos);

				var i = 0;
				$("[name='idaluno[]']").each(function (){
					setCampos(this);

					if($(this).attr("checked") == "checked") {
						i++;
					}
				});
			}
		});

		$("#tablehelp").show();
	}
}

function setCoordenadores(obj , idcoordenador){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setcoordenadores",{idescola: idescola}, function(data) {
		$('select[name=idcoordenador]').html(data);
		
		$("select[name=idcoordenador]").val(idcoordenador);
		val = $("select[name=idcoordenador]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idcoordenador!=0) && (idcoordenador!="")){
			$("select[name=idcoordenador]").closest('div').find('span').html('Selecione...');
		}

	});
}

function setDiretores(obj , iddiretor){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setdiretores",{idescola: idescola}, function(data) {
		$('select[name=iddiretor]').html(data);
		
		$("select[name=iddiretor]").val(iddiretor);
		val = $("select[name=iddiretor]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((iddiretor!=0) && (iddiretor!="")){
			$("select[name=iddiretor]").closest('div').find('span').html('Selecione...');
		}

	});
}

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};		

	if ((typeof form_values)!='undefined') {
		$("select[name=idtipoavaliacao]").val(form_values.idtipoavaliacao);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idescola]").val(form_values.idescola);
		setSeries(form_values.idserie, form_values.idturma, form_values.idaluno, form_values.media);
		setDiretores($("select[name=idescola]").val(form_values.idescola) , form_values.iddiretor);
		setCoordenadores($("select[name=idescola]").val(form_values.idescola) , form_values.idcoordenador);
		$("select[name=status1]").val(form_values.status1);

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});	
	}
	
	rules = {};
	rules.idescola = "required";
	rules.sequencial = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	

	//visualizar = false;
	if (visualizar) {
		setTimeout( function() {
		$("input[type=radio]").each(function() {
			$(this).attr("disabled","disabled");
		});

		$("div.control-group input, div.control-group textarea").not('[type=hidden]').not('[type=checkbox]').not('[type=radio]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;		
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});

		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='Selecione...') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');
		});	
		
		$("label span.required").remove();
		$("[type=checkbox]").attr("disabled",true);},750);
	}

	// $("input[name=sequencial]").mask("99/9999", {placeholder:" "});

	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});	
});