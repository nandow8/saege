    $(document).ready(function() {

    // // cria array com os cookie atuais
    // var colunas = $.cookie();

    // // percorre o array em busca dos cookie correspondentes as colunas
    // // oculta a coluna quando o valor do cookie for falso e desmarca
    // // o checkbox correspondente
    // $.each( colunas, function( key, value ) {
    //     if (key.substring(0, 4) == 'col-') {

    //         if (value == 'false') {
    //             $('.'+key).animate({
    //                 width: 'toggle', duration: 0
    //             });
    //             $('#'+key).prop('checked', false);
    //         }
    //     }
    // });

    // // esconde a div com os checkboxs
    // $("#colunas").animate({ height: 'toggle' });

    // // quando um checkbox é marcado ou desmarcado esconde ou mostra 
    // // a coluna correspondente e cria um cookie com o nome do checkbox
    // // e valor true ou false
    // $('.colunas').change(function() {
    //     var nome = $(this).attr('name');
    //     $('.'+nome).animate({
    //         width: 'toggle', duration: 1
    //     });

    //     if($(this).is(':checked')) {
    //         $.cookie(nome, 'true');
    //     } else {
    //         $.cookie(nome, 'false');
    //     }
    // });

    // $("#label-colunas").click(function () {
    //     $("#colunas").animate({
    //         height: 'toggle'
    //     });  
    // });

    $("input[name=datanascimento]").mask("99/99/9999", {placeholder:" "});
    $("input[name=datanascimento]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function(date) {
        }
    });
});

    jQuery(document).ready(function() {

        if ((typeof form_values)!='undefined') {
            if ((typeof form_values.status)!='undefined') $("select[name=status]").val(form_values.status);
            if ((typeof form_values.tipoestudante)!='undefined') {
                $("select[name=tipoestudante]").val(form_values.tipoestudante);
                if (form_values.tipoestudante!='universidade') $('.col-univ').css('display', 'none');
            }
            if ((typeof form_values.tipotransporte)!='undefined') $("select[name=tipotransporte]").val(form_values.tipotransporte);
            if ((typeof form_values.subsidio)!='undefined') $("select[name=subsidio]").val(form_values.subsidio);
            if ((typeof form_values.identidade)!='undefined') $("select[name=identidade]").val(form_values.identidade);

            if ((typeof form_values.curso)!='undefined') $("select[name=curso]").val(form_values.curso);

            $("select").trigger("chosen:updated");
            $("select").each(function() {
                val = $(this).find('option:selected').html();
                $(this).closest('div').find('span').html(val);
            });
            
            $("select").each(function (){
                div = $(this).closest('div').find('.chosen-container').css('width', '100%');
            });

        }
    });

    function confirmDelete($id, $texto) {
        jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
            if (r) {
                $.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
                    {id: $id}, function(data) {
                        if (data=="OK") {
                            window.location = baseUrl + "/" + module + '/' + controller;
                        } else {
                            jAlert(data, "Erro!");
                        }
                    });
            }
        }); 
    }

    function statusChange($id, $obj) {
        $obj = $($obj);

        $.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
            {id: $id, op: controller}, function(data) {
                if ((data=="Ativo") || (data=="Bloqueado")) {
                    $obj.html(data);
                } else {
                    jAlert(data, "Alerta!");
                }
            });
    }

    function setTipo(obj) {
        var tipo = $(obj).val();

        if (tipo == 'universidade') {
            $('.col-univ').css('display', 'block');
        } else {
            $('.col-univ').css('display', 'none');
        }
    }