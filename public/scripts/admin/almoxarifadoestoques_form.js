var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
   unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
   unsaved = true;
});

jQuery(document).ready(function() {
    window.onbeforeunload = function() {
        console.log(unsaved);
        if(unsaved){
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }       
    };

    if ((typeof form_values)!='undefined') {
        if ((typeof form_values.idlocal)!='undefined') $("select[name=idlocal]").val(form_values.idlocal);
        if ((typeof form_values.idresponsavel)!='undefined') $("select[name=idresponsavel]").val(form_values.idresponsavel);
        if ((typeof form_values.idtipoestoque)!='undefined') $("select[name=idtipoestoque]").val(form_values.idtipoestoque);
        if ((typeof form_values.status)!='undefined') $("select[name=status]").val(form_values.status);

        if ((typeof form_values.idescola)!='undefined' && form_values.idescola > 0) {
            setescolas();
            $("select[name=idescola]").val(form_values.idescola);
            $("select[name=idescola]").trigger('change');
            $("#idescolas").css('display', 'block');
        }

        $("select").trigger("chosen:updated");
        $("select").each(function() {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });
        
        $("select").each(function (){
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });       
    }

    jQuery('select, input, textarea').bind('blur', function() { });
    
    $('.click_acc').each(function() {
        $( this ).click();
    });
    
    rules = {};
    rules.idlocal = "required";
    rules.idresponsavel = "required";
    rules.idtipoestoque = "required";
    rules.status = "required";

    messages = {};
    
    $("#form").validate({
        ignore: [],
        rules: rules,         
        messages: messages,
        submitHandler: function (form) {
            unsaved = false;
            $.blockUI({ message: '<h1> Aguarde...</h1>' });
            form.submit();
        }
    });

    $('#idlocal').change(function () {
        var idlocal = $(this).val();

        if (idlocal == 29) {
            $("#idescolas").css('display', 'block');
            // setresponsaveis(0);
            setescolas();
        } else {
            $("#idescolas").css('display', 'none');
            // setresponsaveis(idlocal,0);
        }
    });

    // $('#idescola').change(function () {
    //     var idescola = $(this).val();
    //     setresponsaveis(29, idescola);
    // });
});

// function setresponsaveis(idlocal, idescola = 0) {

//     data = '';
//     $.post(baseUrl + '/admin/almoxarifadoestoques/setresponsaveis/', {idlocal: idlocal, idescola: idescola}, function (data) {
//         $("select[name=idresponsavel]").val(null).trigger("change");
//         $("select[name=idresponsavel]").html(data);
//     });
// }

function setescolas() {

    data = '';
    $.post(baseUrl + '/admin/almoxarifadoestoques/setescolas/', function (data) {
        // $("select[name=idescola]").val(null).trigger("change");
        $("select[name=idescola]").html(data);
    });
}