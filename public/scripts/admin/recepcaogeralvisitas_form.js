var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	$("#form-visitas").hide();
	$("#novasvisita").click(function () {
		$("#form-visitas").show();
	});

	$("#tipo").click(function () {
		if ($("#tipo").val() == 'Outros'){
			$(".cpfoutros").show();
			$('.cpfoutros').attr('disabled', false);
			$('.cpfoutros').css('display', true);
		}
	});

	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=tipo]").val(form_values.tipo);

		setTipo($("select[name=tipo]").val(form_values.tipo), $('input[name=id]').val());
$("select[name=iddepartamento]").val(form_values.iddepartamento);
$("select[name=idfuncionario]").val(form_values.idfuncionario);
$("select[name=idfuncionarios]").val(form_values.idfuncionarios);
$("select[name=idlocais]").val(form_values.idlocais);
		setProfessorCpf($("select[name=idfuncionario]").val(form_values.idfuncionario));
		setProfessorLocal($("select[name=idfuncionario]").val(form_values.idfuncionario));
		setProfessoresLocais($("select[name=idfuncionario]").val(form_values.idfuncionario));
		
		

		 

		if(form_values.tipo=="Funcionário da secretaria"){
			setFuncionarios($("select[name=iddepartamento]").val(form_values.iddepartamento), form_values.idfuncionario);
		}
$("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
		$("input[name=dataentrada]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataentrada]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});


$("input[name=cpf]").mask("999.999.999-99", {placeholder:" "});

	
		
	 


	$("input[name=datasaida]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datasaida]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});


	
	
	rules = {};
	rules.tipo = "required";
//rules.cpf = "required";
// rules.assunto = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setTipo(obj, id){
	var tipo = $(obj).val();

	if(tipo=="Funcionário da secretaria"){
		$('.box-secretaria').css('display', 'block');	
		$('.box-outros').css('display', 'block');
		$('.box-nome').css('display', 'none');	
	}else{
		$('.box-secretaria').css('display', 'none');	
		$('.box-nome').css('display', 'block');	
		// $("input[name=cpf]").removeAttr('readonly');
		// $("input[name=nome]").removeAttr('readonly');
		$("input[name=setor]").removeAttr('readonly');
		/*if(id > 0){
			$('input[name=cpf]').val('');
			$('input[name=nome]').val('');
			$('input[name=setor]').val('');
		}*/
		$('.box-outros').css('display', 'block');
	}
}

function setFuncionarios(obj, idfuncionario){
	var iddepartamento = $(obj).val();
	$.post(baseUrl + "/" + module + "/recepcaogeralvisitas/setfuncionarios/", {iddepartamento:iddepartamento}, function(data) {
		$('select[name=idfuncionario]').html(data);
		
		$("select[name=idfuncionario]").val(idfuncionario);
		val = $("select[name=idfuncionario]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if(idfuncionario==""){
			$("select[name=idfuncionario]").closest('div').find('span').html('Selecione...');
		}else{
			setDados($('select[name=idfuncionario]').val(idfuncionario));
		}

		//$("input[name=telefone]").mask("(99)9?9999-9999", {placeholder:" "});
	});

}

function setDados(obj)
{
	var idfuncionario = $(obj).val();

	if(idfuncionario > 0){
		$.post(baseUrl + "/" + module + "/recepcaogeralvisitas/getdados/", {idfuncionario:idfuncionario}, function(data) {
			data = JSON.parse(data.trim());

			if(data.status == "OK"){
				// $("input[name=cpf]").val(data.response.cpf);
				$("input[name=nome]").val(data.response.nome);
				$("input[name=setor]").val(data.response.setor);

				// $("input[name=cpf]").attr('readonly', 'readonly');
				// $("input[name=nome]").attr('readonly', 'readonly');
				// $("input[name=setor]").attr('readonly', 'readonly');
			}
			else{
				jAlert(data.response, 'Alerta!');
			}
		});
	}
}

function setProfessoresLocais(obj, values) {
	obj = $(obj);
	val = obj.val();
	if (val != '') {
		$("#professores-container").html("Aguarde!");
		$.post(baseUrl + '/admin/recepcaogeralvisitas/getprofessores', { idlocal: val }, function (professores) {
			professores = JSON.parse(professores);
			if (professores.length == 0) {
				$("#professores-container").html("Nenhum professores foi encontrado");
			} else {
				$("#professores-container").html("");
				for (var i = 0; i < professores.length; i++) {

					$("#professores-container").append('<option  value="' + professores[i].id + '">' + professores[i].nome + '</option>');
				}
				if (values) {
					$("[name='idfuncionario']").val(values);
					val = $("select[name=idfuncionario]").find('option:selected').html();
					$("select[name=idfuncionario]").closest('div').find('span').html(val);

					$("select[name=idfuncionario]").trigger("chosen:updated");
				}
			}
		});
	} else {
		$("#professores-container").html("Selecione um local!");
	}
}


function setProfessorCpf(obj, values) {
	obj = $(obj);
	val = obj.val();
	var elemento;
	$.post(baseUrl + '/admin/recepcaogeralvisitas/getprofessordados', { idfuncionario: val }, function (professors) {
		professors = JSON.parse(professors);  
		elemento = '<input maxlength="500" placeholder="CPF" name="cpf" value="' + professors.cpf + '" type="text"  class="span3"   />';
		$('.cpfoutros').attr('disabled', 'disabled');
		$('.cpfoutros').css('display', 'none');
		$('#professores-cpf').html(elemento);
	});
} 

function setProfessorLocal(obj, values) {
	obj = $(obj);
	val = obj.val();
	var elemento;
	$.post(baseUrl + '/admin/recepcaogeralvisitas/getprofessorlocal', { idfuncionario: val }, function (professors) {
		professors = JSON.parse(professors); 
		 if(professors){
			 elemento = '<input maxlength="500" placeholder="CPF" name="idlocais" value="' + professors.id + '" type="hidden"  class="span3"   /> "' + professors.titulo +'"';
		 }
		$('.cpfoutros').attr('disabled', 'disabled');
		$('.cpfoutros').css('display', 'none');
		$('#professores-locais').html(elemento);
		if (values) {
			$("[name='idlocais']").val(values);
			val = $("select[name=idlocais]").find('option:selected').html();
			$("select[name=idlocais]").closest('div').find('span').html(val);

			$("select[name=idlocais]").trigger("chosen:updated");
		}
	});
} 

/* não consegui  T-T   ;/  :(
function setProfessorFoto(obj, values) {
	obj = $(obj);
	val = obj.val();
	var elemento;
	$.post(baseUrl + '/admin/recepcaogeralvisitas/getprofessorfoto', { idfuncionario: val }, function (professors) {
		professors = JSON.parse(professors); 
		console.log(professors);
		elemento = '';
		$('.cpfoutros').attr('disabled', 'disabled');
		$('.cpfoutros').css('display', 'none');
		$('#professores-foto').html(elemento);
		 
	});
} 
*/