var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idtipoconvenio]").val(form_values.idtipoconvenio);
		$("select[name=idescolasapms]").val(form_values.idescolasapms);
		$("select[name=idprogramasgov]").val(form_values.idprogramasgov);
		$("select[name=status1]").val(form_values.status1);

		$("select").trigger("chosen:updated");
		if ((typeof form_values.idescola) != 'undefined') {
			setConvenios($("[name=idescola]"), form_values.idescola, form_values.idtipoconvenio);
		}
		if ((typeof form_values.idtipoconvenio) != 'undefined') {
			settipoconvenio($("[name=idtipoconvenio]"), form_values.idprogramasgov);
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
		console.log(form_values);
	}
	$('input[name=saldoaplicacaofinanceira]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 2
	});
	rules = {};
	rules.idescola = "required";
	rules.idprogramasgov = "required";
	rules.status1 = "required";

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
});

$(window).load(function(){
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			
			val = (val=='') ? '--' : val;
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}	
});

function setConvenios(obj, value = 0, tipo = 0)
{
	idescola = (value > 0) ? value : Number($(obj).val());

	
	if(idescola > 0)
	{
		$('.box-convenios').show();
		if(tipo)
		{
			$("[name='idtipoconvenio']").val(tipo);
			val = $("select[name=idtipoconvenio]").find('option:selected').html();
			$("select[name=idtipoconvenio]").closest('div').find('span').html(val);

			$("select[name=idtipoconvenio]").trigger("chosen:updated");
		}
	}
	else
	{
		$('.box-convenios').hide();
	}
	$("select[name=idprogramasgov]").closest('div').find('span').html('Selecione...');
	$('select[name=idprogramasgov]').html('<option value="">Selecione...</option>');
	$("select[name=idprogramasgov]").trigger("chosen:updated");

}


function settipoconvenio(obj, values) {
	obj = $(obj);
	val = obj.val();
	idescola = (idescola == '' || idescola == 0) ? Number($('select[name=idescola]').val()) : idescola;

	//console.log(idescola)
	
	$('.dados-convenio').hide();
	$('#convenio_datainicio').val('');
	$('#convenio_datafim').val('');

	if (val != '') {
		$("select[name=idprogramasgov]").closest('div').find('span').html('Selecione...');
		$('select[name=idprogramasgov]').html('<option value="">Selecione...</option>');
		$("select[name=idprogramasgov]").trigger("chosen:updated");
		$.post(baseUrl + '/admin/financeiroregistrosfechamentos/getconvenios', { idtipoconvenio: val, idescola: idescola }, function (data) {
			//console.log(data);
			if (data == 'ERROR') {
				$('select[name=idprogramasgov]').html('<option value="">Selecione...</option>');
			}
			else {
				$('select[name=idprogramasgov]').html(data);
				if(values)
				{
					$("[name='idprogramasgov']").val(values);
					val = $("select[name=idprogramasgov]").find('option:selected').html();
					$("select[name=idprogramasgov]").closest('div').find('span').html(val);

					$("select[name=idprogramasgov]").trigger("chosen:updated");
					setConvenio($("select[name=idprogramasgov]"), form_values.idprogramasgov);
				}
			}
		});
	}
}


function setConvenio(obj, values) {
	val = (values > 0) ? values : $(obj).val();
	

	if (val != '') {
		$.post(baseUrl + '/admin/financeiroregistrosfechamentos/getconvenio', { idconvenio: val }, function (data) {
			//console.log(data);
			data = JSON.parse(data);
			if (data.status == 'ERROR') {
				$('.dados-convenio').hide();
				$('#convenio_datainicio').val('');
				$('#convenio_datafim').val('');
			}
			else 
			{
				$('.dados-convenio').show();

				$('#convenio_datainicio').val(data.dados.datainicio);
				$('#convenio_datafim').val(data.dados.datafim);
			}
		});
	}
	else
	{
		$('.dados-convenio').hide();
		$('#convenio_datainicio').val('');
		$('#convenio_datafim').val('');
	}
}

function setFechamento()
{
	$('input[name=finalizado]').val('sim');
}