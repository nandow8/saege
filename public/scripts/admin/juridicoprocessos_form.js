var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		//$("select[name=idsadvogados]").val(form_values.idsadvogados);
		if ((typeof form_values.idsadvogados)!='undefined')  $("[name='idsadvogados[]']").val(form_values.idsadvogados);
$("select[name=idorigem]").val(form_values.idorigem);
setOrigemdados($("select[name=idorigem]").val(form_values.idorigem), 0);
$("select[name=movimentolancar]").val(form_values.movimentolancar);
setLancamento($("select[name=movimentolancar]").val(form_values.movimentolancar), 0);
$("select[name=idstatus]").val(form_values.idstatus);
$("select[name=status1]").val(form_values.status1);


		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	





	$("input[name=dataproximaaudiencia]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataproximaaudiencia]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$('input[name=valorcausa]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '.',
	    centsLimit: 2
	});

	$("input[name=datacitacao]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datacitacao]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=datapenhora]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datapenhora]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=datacontestacao]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datacontestacao]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=dataembargos]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataembargos]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=datasentenca]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datasentenca]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});


	$("input[name=movimentodata]").mask("99/99/9999", {placeholder:" "});
	$("input[name=movimentodata]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});



	
	
	rules = {};
	rules.processonumero = "required";
rules.processooriginario = "required";
rules.idorigem = "required";
rules.representantelegal = "required";
rules.idstatus = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

function setLancamento(obj, txtlancamento){
	var lancamento = $(obj).val();
	if(lancamento=="Sim"){
		$('.box-lancamentos').css('display', 'block');
	}else{
		$('.box-lancamentos').css('display', 'none');
	}
}


function setOrigemdados(obj, idorigemp){
	var idorigem = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/getdadosorigem", {idorigem: idorigem}, function(data) {
		$('.dadosorigens').html(data); 
	});
}
