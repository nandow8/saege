

	$("input[name=datainicio]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datainicio]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=datafim]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datafim	]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

function geraRelatorio($obj)
{
	var idescola = $($obj).val();
	var tipo = $('select[name=tipo]').val();
	var datainicio = $('input[name=datainicio]').val();
	var datafim = $('input[name=datafim]').val();

	if(idescola != '' && tipo != '')
	{
		$('.box-gerar').show();

		console.log(idescola)

		if(tipo == 'apm')
			$('.button').html('<a href="' + baseUrl + '/admin/relatoriosfinanceiros/geraapm/idescola/' + idescola + '/datainicio/'+datainicio+'/datafim/'+datafim+'" class="btn btn-primary btn-block" target="_blank">Gerar Relatorio</a>');
		else if(tipo == 'pdde')
			$('.button').html('<a href="' + baseUrl + '/admin/relatoriosfinanceiros/gerapdde/idescola/' + idescola + '/datainicio/'+datainicio+'/datafim/'+datafim+'" class="btn btn-primary btn-block" target="_blank">Gerar Relatorio</a>');
	}
	else
	{
		$('.box-gerar').hide();
		jAlert("Selecione uma escola e o tipo de relatório");
	}
}