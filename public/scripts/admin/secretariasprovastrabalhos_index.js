$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=idusuariologado]").val(form_values.idusuariologado);
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idserie]").val(form_values.idserie);
		$("select[name=idescolavinculo]").val(form_values.idescolavinculo);
		$("select[name=idperiodo]").val(form_values.idperiodo);
		$("select[name=idaluno]").val(form_values.idaluno);
		$("select[name=material]").val(form_values.material);
		$("select[name=status1]").val(form_values.status1);
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});	
	}
	
	
	$("input[name=datalancamento_i],input[name=datalancamento_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datalancamento_i],input[name=datalancamento_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});











	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Ativo") || (data=="Bloqueado")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 