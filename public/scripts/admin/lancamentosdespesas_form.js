var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {

	

	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=origem]").val(form_values.origem);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=idtipoconvenio]").val(form_values.idtipoconvenio);
$("select[name=idtipodespesa]").val(form_values.idtipodespesa);
$("select[name=idbanco]").val(form_values.idbanco);
$("select[name=status1]").val(form_values.status1);

		if ((typeof form_values.idbanco) != 'undefined') {
			settipobanco($("[name=idtipoconvenio]").get(0), form_values.idbanco);
		}
		 
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	




	$("input[name=data]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});





	
$("input[name=cnpj]").mask("99.999.999/9999-99", {placeholder:" "});



	$('input[name=valor]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '.',
	    centsLimit: 2
	});


	
	
	rules = {};

rules.idbanco = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});

 

function settipobanco(obj, values) {
	obj = $(obj);
	val = obj.val();
	if (val != '') {
		$("#escolasapmscontas-container").html("Aguarde!");
		$.post(baseUrl + '/admin/lancamentosdespesas/gettipobanco', { idtipoconvenio: val }, function (escolasapmscontas) {
			escolasapmscontas = JSON.parse(escolasapmscontas);
			console.log(escolasapmscontas);
			if (escolasapmscontas.length == 0) {
				$("#escolasapmscontas-container").html("Nenhum banco foi encontrado");
			} else {
				$("#escolasapmscontas-container").html("");
				for (var i = 0; i < escolasapmscontas.length; i++) {

					$("#escolasapmscontas-container").append('<option  value="' + escolasapmscontas[i].id + '">' + "Banco:" + escolasapmscontas[i].banco + "  Agencia:" + escolasapmscontas[i].agencia + "  Conta:" + escolasapmscontas[i].conta + "  Digito:" + escolasapmscontas[i].digito + '</option>');
				}
				if (values) {
					$("[name='idbanco']").val(values);
					val = $("select[name=idbanco]").find('option:selected').html();
					$("select[name=idbanco]").closest('div').find('span').html(val);

					$("select[name=idbanco]").trigger("chosen:updated");
				}
			}
		});
	} else {
		$("#escolasapmscontas-container").html("Selecione um banco!");
	}
}