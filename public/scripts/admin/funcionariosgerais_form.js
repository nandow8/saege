var unsaved = false;

$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function() {
	window.onbeforeunload = function() {
		console.log(unsaved);
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};	
	
	if ((typeof form_values)!='undefined') {
		$("select[name=idsecretaria]").val(form_values.idsecretaria);
$("select[name=tipo]").val(form_values.tipo);
$("select[name=e_idenderecoidestado]").val(form_values.e_idenderecoidestado);$("select[name=iddepartamentosecretaria]").val(form_values.iddepartamentosecretaria);
$("select[name=cnh]").val(form_values.cnh);
$("select[name=estrangeiro]").val(form_values.estrangeiro);
$("select[name=grauinstrucao]").val(form_values.grauinstrucao);
$("select[name=estadocivil]").val(form_values.estadocivil);
$("select[name=filhos]").val(form_values.filhos);
$("select[name=regime]").val(form_values.regime);
$("select[name=bolsista]").val(form_values.bolsista);
$("select[name=status1]").val(form_values.status1);
$("select[name=idfuncao]").val(form_values.idfuncao);
$("select[name=idcargo]").val(form_values.idcargo);
/*
		if(form_values.idescola){			
			$("select[name=idescola]").val(form_values.idescola);
			$("select[name=recebesolicitacoes]").val(form_values.recebesolicitacoes);
			$("select[name=idprefeitura]").val(form_values.idprefeitura);
			$("select[name=enviaemails]").val(form_values.enviaemails);
			if ((typeof form_values.idescolas)!='undefined') $("[name='idescolas[]']").val(form_values.idescolas);
			setDepartamentos($("select[name=enviaemails]").val(form_values.enviaemails), form_values.idsdepartamentos);
			if ((typeof form_values.idsdepartamentos)!='undefined') $("[name='idsdepartamentos[]']").val(form_values.idsdepartamentos);
			if ((typeof form_values.professor)!='undefined') setCamposUser($("select[name=professor]").val(form_values.professor), form_values.professor);
			setPerfil($("select[name=idescola]").val(form_values.idescola), form_values.idperfil);
			setCargo($("select[name=iddepartamento]").val(form_values.iddepartamento), form_values.idcargo);

			//$("select[name=idperfil]").val(form_values.idperfil);
		}
*/
		if ((typeof form_values.cnh)!='undefined') {
			jQuery("select[name=cnh]").val(form_values.cnh);
			setCampos(jQuery("select[name=cnh]").val(form_values.cnh), 'cnh', 0);
		}
		if ((typeof form_values.estrangeiro)!='undefined') {
			jQuery("select[name=estrangeiro]").val(form_values.estrangeiro);
			setCampos(jQuery("select[name=estrangeiro]").val(form_values.estrangeiro), 'estrangeiro', 0);
		}
		if ((typeof form_values.filhos)!='undefined'){ 
			jQuery("select[name=filhos]").val(form_values.filhos);
			setCampos(jQuery("select[name=filhos]").val(form_values.filhos), 'filhos', 0);
		}
		
		if ((typeof form_values.bolsista)!='undefined'){ 
			jQuery("select[name=bolsista]").val(form_values.bolsista);
			setCampos(jQuery("select[name=bolsista]").val(form_values.bolsista), 'bolsista', 0);
		}
                
		if ((typeof form_values.regime)!='undefined'){ 
			jQuery("select[name=regime]").val(form_values.regime);
			setCampos(jQuery("select[name=regime]").val(form_values.regime), 'regime', 0);
		}

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});
	
	}
	
	




	$('input[name=idendereco]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});








	$("input[name=datanascimento]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datanascimento]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=emissaocarteira]").mask("99/99/9999", {placeholder:" "});
	$("input[name=emissaocarteira]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});



$("input[name=cpf]").mask("999.999.999-99", {placeholder:" "});
	$("input[name=filhosquantidade]").priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',	    
	    centsLimit: 0    
	});	
	
	$("input[name=salarioinicial]").priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '.',	    
	    centsLimit: 2    
	});	


	$("input[name=cnh_validade]").mask("99/99/9999", {placeholder:" "});
	$("input[name=cnh_validade]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});










	$("input[name=dataadmissao]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataadmissao]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});





	$("input[name=dataregistroctps]").mask("99/99/9999", {placeholder:" "});
	$("input[name=dataregistroctps]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});



	$("input[name=datainiciobolsa]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datainiciobolsa]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
	$("input[name=datafimbolsa]").mask("99/99/9999", {placeholder:" "});
	$("input[name=datafimbolsa]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});

	
	
	rules = {};
	//rules.tipo = "required";
rules.status1 = "required";
	

	messages = {};
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		messages: messages,
		submitHandler: function(form) {
			unsaved = false;
			$.blockUI({ message: '<h1> Aguarde...</h1>' }); 
			form.submit();
		}
	});	
	
	//visualizar = false;
	if (visualizar) {
		$("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function() {
			val = $(this).val();
			val = $.trim(val);
			val = (val=='') ? '--' : val;			
			parent = $(this).parent();
			parent.append(val);
			parent.addClass('visualizar');

			$(this).remove();
		});
		$("div.control-group select").not('[type=hidden]').each(function() {
			val = $(this).find('option:selected').html();
			val = $.trim(val);
			val = (val=='') ? '--' : val;				
			parent = $(this).parent();
			parent.html(val);
			parent.addClass('visualizar');

		});		
		
		$("label span.required").remove();
		
	}		
	
});



function setCampos(obj, tipo, value){
	var val = $(obj).val();	
	if((tipo=="cnh") && (val=="Sim")){
		$(".box-cnh").css("display", "block");
	}else if((tipo=="filhos") && (val=="Sim")){
		$(".box-filhos").css("display", "block");
	}else if((tipo=="estrangeiro") && (val=="Sim")){
		$(".box-estrangeiro").css("display", "block");
	}else if((tipo=="bolsista") && (val=="Sim")){
		$(".box-bolsista").css("display", "block");
	}else if((tipo=="cnh") && (val!="Sim")){
		$(".box-cnh").css("display", "none");
	}else if((tipo=="filhos") && (val!="Sim")){
		$(".box-filhos").css("display", "none");
	}else if((tipo=="estrangeiro") && (val!="Sim")){
		$(".box-estrangeiro").css("display", "none");
	}else if((tipo=="bolsista") && (val!="Sim")){
		$(".box-bolsista").css("display", "none");
	}else if((tipo=="regime") && (val=="outros"))
		$(".box-regime").css("display", "block");
}

function setRegime(obj)
{
    var valor = $(obj).val();
    
    if(valor == "outros")
    {
        $(".box-regime").css("display", "block");
    }
    else
        $(".box-regime").css("display", "none");
}



function setPerfil(obj, idperfil){
	var val = $(obj).val();
	data = '';
	$("select[name=idperfil]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasusuarios/setperfil/', {idescola:val}, function(data) {
		$("select[name=idperfil]").html(data);
		$("select[name=idperfil]").val(idperfil);
		val = $("select[name=idperfil]").find('option:selected').html();
		$("select[name=idperfil]").closest('div').find('span').html(val);
	
		$("select[name=idperfil]").trigger("chosen:updated");
		
		if(idperfil==""){
			$("select[name=idperfil]").each(function() {
				val = $(this).find('option:selected').html();
				$(this).closest('div').find('span').html(val);
			});
			
			$("select[name=idperfil]").each(function (){
				div = $(this).closest('div').find('.chosen-container').css('width', '100%');
			});
			//$("select[name=idperfil]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setCargo(obj, idcargo){
	var val = $(obj).val();
	data = '';
	$("select[name=idcargo]" ).html('<option value="">Aguarde...</option>');
	$.post(baseUrl + '/admin/escolasusuarios/setcargos/', {iddepartamento:val}, function(data) {
		$("select[name=idcargo]").html(data);
		$("select[name=idcargo]").val(idcargo);
		val = $("select[name=idcargo]").find('option:selected').html();
		$("select[name=idcargo]").closest('div').find('span').html(val);
	
		$("select[name=idcargo]").trigger("chosen:updated");
		
		if(idcargo==""){
			$("select[name=idcargo]").closest('div').find('span').html('Selecione...');
		}
	});
}

function setDepartamentos(obj, idsdepartamentos){
	var enviaemails = $(obj).val();
	var professor = $('select[name=professor]').val();
	var idescola = $('select[name=idescola]').val();
	data = '';
	if((enviaemails=="Departamentos") && (professor!="Sim")){
		$('.campos_departamentos').css('display', 'block');
		$.post(baseUrl + '/admin/escolasusuarios/setdepartamentos/', {enviaemails:enviaemails, idescola:idescola, professor:professor}, function(data) {
			$(".content_departamentos").html(data);
			if ((typeof idsdepartamentos)!='undefined') $("[name='idsdepartamentos[]']").val(idsdepartamentos);
		});
	}else{
		$('.campos_departamentos').css('display', 'none');

	}

}

function setCamposUser(obj, professor){	
	/*rules = {};
	rules.nomerazao = "required";
	rules.sobrenomefantasia = "required";
	rules.rgf = "required";
	rules.professor = "required";
	rules.email = {required: true,email: true};
	rules.senha = {minlength: 6};
	rules.resenha = {
		  equalTo: "[name=senha]"
	};	
	*/
	var val = $(obj).val();
	if(val=="Sim"){
		$('.campos_professores').css('display', 'block');
		$('.campos_nao_professores').css('display', 'none');
/*
		rules = {};
		rules.nomerazao = "required";
		rules.sobrenomefantasia = "required";
		rules.rgf = "required";
		rules.professor = "required";
		rules.email = {required: true,email: true};
		rules.senha = {minlength: 6};
		rules.resenha = {
			  equalTo: "[name=senha]"
		};	*/
		
	}else {
		$('.campos_professores').css('display', 'none');
		$('.campos_nao_professores').css('display', 'block');
/*
		rules.idperfil = "required";
		rules.iddepartamento = "required";
		rules.idcargo = "required";
		rules.idescola = "required"; */
	}
	/*rules.status1 = "required";
	if (action=='adicionar') {
		rules.senha = "required";
	}*/
	/*
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				//$(element).parent().find('div').append(error);
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
			
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});	
	*/
	if(professor){
		$("select[name=professor]").val(professor);
	}
}
