$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=idtipoprotocolo]").val(form_values.idtipoprotocolo);
$("select[name=tipo]").val(form_values.tipo);
$("select[name=idusuariologado]").val(form_values.idusuariologado);
$("select[name=origem]").val(form_values.origem);
$("select[name=idescola]").val(form_values.idescola);
$("select[name=iddepartamento]").val(form_values.iddepartamento);
$("select[name=tiposolicitacao]").val(form_values.tiposolicitacao);
$("select[name=encaminhar]").val(form_values.encaminhar);
$("select[name=posicao]").val(form_values.posicao);
$("select[name=status1]").val(form_values.status1);
	

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});	
	}
	
	

















	
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Ativo") || (data=="Bloqueado")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 