var unsaved = false;

$("input,select,textarea").change(function () { //trigers change in all input fields including text type
    unsaved = true;
});

$("input,select,textarea").click(function () { //trigers change in all input fields including text type
    unsaved = true;
});

$(document).ready(function () {
    window.onbeforeunload = function() {
        console.log(unsaved);
        if(unsaved){
            return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
        }		
    };

    if ($("#tipo").is(':checked')) { //Retornar true ou false
        esconderCamposExternos();
    }

    if ((typeof form_values) != 'undefined') {
        $("select[name=idtipoprotocolo]").val(form_values.idtipoprotocolo);
        $("select[name=tipo]").val(form_values.tipo);
        $("select[name=idusuariologado]").val(form_values.idusuariologado);
        $("select[name=origem]").val(form_values.origem);
        $("select[name=idescola]").val(form_values.idescola);
        $("select[name=iddepartamento]").val(form_values.iddepartamento);
        $("select[name=idtiposolicitacao]").val(form_values.idtiposolicitacao);
        $("select[name=posicao]").val(form_values.posicao);
        $("select[name=idperfilencaminhar]").val(form_values.idperfilencaminhar);
        $("select[name=idescolaencaminhar]").val(form_values.idescolaencaminhar);
        $("select[name=status1]").val(form_values.status1);


        $("select").each(function () {
            val = $(this).find('option:selected').html();
            $(this).closest('div').find('span').html(val);
        });

        $("select").each(function () {
            div = $(this).closest('div').find('.chosen-container').css('width', '100%');
        });

    }


    $("input[name=datalancamento]").mask("99/99/9999", {placeholder: " "});
    $("input[name=datalancamento]").datepicker({
        dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        currentText: 'Hoje',
        prevText: 'Antes',
        nextText: 'Depois',
        onSelect: function (date) {
        }
    });

    rules = {};
    rules.idtipoprotocolo = "required";
    rules.tipo = "required";
    rules.idusuariologado = "required";
    rules.idescola = "required";
    rules.titulo = "required";
    rules.idtiposolicitacao = "required";
    rules.posicao = "required";
    rules.status1 = "required";

    messages = {};

    $("#form").validate({
        ignore: [],
        rules: rules,
        messages: messages,
        submitHandler: function (form) {
            unsaved = false;
            $.blockUI({message: '<h1> Aguarde...</h1>'});
            form.submit();
        }
    });

    esconderCamposExternos($("input[name=tipo]"))

    //visualizar = false;
    if (visualizar) {
        $("div.control-group input, div.control-group textarea").not('[type=hidden]').each(function () {
            val = $(this).val();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.append(val);
            parent.addClass('visualizar');

            $(this).remove();
        });
        $("div.control-group select").not('[type=hidden]').each(function () {
            val = $(this).find('option:selected').html();
            val = $.trim(val);
            val = (val == '') ? '--' : val;
            parent = $(this).parent();
            parent.html(val);
            parent.addClass('visualizar');

        });

        $("label span.required").remove();

    }

});

function esconderCamposExternos(obj) {
    
    let val = $(obj).val();
 
    if (val == "Interno"){
        document.getElementById('tipo-externo').style.display = 'none'; 
    }else{
        document.getElementById('tipo-externo').style.display = 'block';
    }
}

function exibeExtras(obj) {
    var Valor = $(obj).val();

    //Tratamento para perfil secretaria Escolar
    if (Valor == 29) {
        $(".box-escolas").css("display", "block");
    } else {
        $(".idescolaencaminhar").value = "";
        $(".box-escolas").css("display", "none");
    }
}

function setHistoricos(obj, opcao = '') {

    if ($(obj).context['name'] === "idperfilencaminharhistoricoV") {
        document.getElementById("idperfilencaminharhistorico").value = $(obj).val();
    } else if ($(obj).context['name'] === "idescolaencaminharhistoricoV") {
        document.getElementById("idescolaencaminharhistorico").value = $(obj).val();
    } else if ($(obj).context['name'] === "observacoeshistoricoV") {
        document.getElementById("observacoeshistorico").value = $(obj).val();
    } else {

        if ($(obj).context['name'] === "btnSalvaHistorico") {

            if (document.getElementById("idperfilencaminharhistorico").value === "") {
                jAlert('Preencha a informação do campo Encaminhar', 'Atenção!');
                return;
            }

            if ((document.getElementById("idperfilencaminharhistorico").value === "29") && (document.getElementById("idescolaencaminharhistorico").value === "")) {
                jAlert('Preencha a informação do campo Escola', 'Atenção!');
                return;
            } else {
                $(".idescolaencaminharhistorico").value = "";
            }

        }

        if ($("textarea[name=observacoeshistoricoV]").val() == "") {
            jAlert('Preencha a informação do campo Observações', 'Atenção!');
            return;
        }

        if (opcao !== "") {
            jConfirm("Este Protocolo será finalizado e marcado como: " + opcao + ". Deseja continuar?", "Atenção!", function (r) {
                if (r) {
                    document.getElementById("posicao").value = opcao;
                    document.getElementById("form").submit();
                } else {
                    document.getElementById("posicao").value = 'Em Andamento';
                }
            });
        } else {
            document.getElementById("posicao").value = 'Em Andamento';
            document.getElementById("form").submit();
        }
    }
}

function JanelaArquivos(obj, tipo = 0, destino = 0, id = "") {

    let NomeJanela = '#' + obj.rel;

    if ($(obj).context['name'] === "btnEncaminhar") {
        $(".box-encaminhar").css("display", "block");
        $(".box-encaminharbotao").css("display", "block");
        $(".box-finalizarbotao").css("display", "none");
        $(".modal-title").html('Encaminhar Protocolo.');
    } else {
        $(".box-encaminhar").css("display", "none");
        $(".box-encaminharbotao").css("display", "none");
        $(".box-finalizarbotao").css("display", "block");
        $(".modal-title").html('Finalizar Protocolo.');
    }

    destino = (destino == 1) ? 'settramitacaoarquivos' : 'settramitacao';

    $.fancybox({
        type: (tipo == 0) ? 'inline' : 'iframe',
        href: (tipo == 0) ? NomeJanela : baseUrl + "/admin/protocolosprocessos/" + destino + "/id/" + id,
        width: $('.' + ($(NomeJanela).attr("class"))).width() + 22,
        height: $('.' + ($(NomeJanela).attr("class"))).height() + 22,
        autoSize: false,
        helpers: {
            title: {
                type: 'float'
            }
        }
    });

}
