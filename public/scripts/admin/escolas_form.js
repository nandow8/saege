﻿var unsaved = false;
$("input,select,textarea").change(function(){ //trigers change in all input fields including text type
    unsaved = true;
});
$("input,select,textarea").click(function(){ //trigers change in all input fields including text type
    unsaved = true;
});

jQuery(document).ready(function() {

	window.onbeforeunload = function() {
	    if(unsaved){
	        return "Você tem dados não salvos nesta página. Deseja sair e perder os dados?";
	    }		
	};
	
	if ((typeof form_values)!='undefined') {
		if ((typeof form_values.status)!='undefined') jQuery("select[name=status]").val(form_values.status);
		
		if ((typeof form_values.tipozoneamento)!='undefined') jQuery("select[name=tipozoneamento]").val(form_values.tipozoneamento);
		if ((typeof form_values.tipoimovel)!='undefined') jQuery("select[name=tipoimovel]").val(form_values.tipoimovel);
		
		if ((typeof form_values.idtiporedeagua)!='undefined') jQuery("select[name=idtiporedeagua]").val(form_values.idtiporedeagua);
		
		if ((typeof form_values.idtipoterreno)!='undefined') jQuery("select[name=idtipoterreno]").val(form_values.idtipoterreno);
		if ((typeof form_values.idtipodelimitacao)!='undefined') jQuery("select[name=idtipodelimitacao]").val(form_values.idtipodelimitacao);
		
		if ((typeof form_values.idtipolocalizacao)!='undefined') jQuery("select[name=idtipolocalizacao]").val(form_values.idtipolocalizacao);
		if ((typeof form_values.idtiporedes)!='undefined') jQuery("select[name=idtiporedes]").val(form_values.idtiporedes);
		if ((typeof form_values.idtipoesgoto)!='undefined') jQuery("select[name=idtipoesgoto]").val(form_values.idtipoesgoto);
		if ((typeof form_values.idtipoeletrica)!='undefined') jQuery("select[name=idtipoeletrica]").val(form_values.idtipoeletrica);
		if ((typeof form_values.idtipodocumentacao)!='undefined') jQuery("select[name=idtipodocumentacao]").val(form_values.idtipodocumentacao);
		
		
		if ((typeof form_values.coletalixo)!='undefined') jQuery("select[name=coletalixo]").val(form_values.coletalixo);
		if ((typeof form_values.transporteescolar)!='undefined') jQuery("select[name=transporteescolar]").val(form_values.transporteescolar);
		if ((typeof form_values.transportefuncional)!='undefined') jQuery("select[name=transportefuncional]").val(form_values.transportefuncional);
		if ((typeof form_values.servicosclinicos)!='undefined') jQuery("select[name=servicosclinicos]").val(form_values.servicosclinicos);
		if ((typeof form_values.servicosodontologicos)!='undefined') jQuery("select[name=servicosodontologicos]").val(form_values.servicosodontologicos);
		if ((typeof form_values.atividadescomunitarias)!='undefined') jQuery("select[name=atividadescomunitarias]").val(form_values.atividadescomunitarias);
		
		
		if ((typeof form_values.caixaresiduos)!='undefined') jQuery("select[name=caixaresiduos]").val(form_values.caixaresiduos);
		if ((typeof form_values.tratamentoresiduos)!='undefined') jQuery("select[name=tratamentoresiduos]").val(form_values.tratamentoresiduos);
		if ((typeof form_values.caixagorduras)!='undefined') jQuery("select[name=caixagorduras]").val(form_values.caixagorduras);
		
		
		if ((typeof form_values.reservatorio)!='undefined') jQuery("select[name=reservatorio]").val(form_values.reservatorio);
		if ((typeof form_values.energiasolar)!='undefined') jQuery("select[name=energiasolar]").val(form_values.energiasolar);
		if ((typeof form_values.existenciaesportivo)!='undefined') jQuery("select[name=existenciaesportivo]").val(form_values.existenciaesportivo);
		if ((typeof form_values.compartilhados)!='undefined') jQuery("select[name=compartilhados]").val(form_values.compartilhados);
		if ((typeof form_values.idtipoedificacao)!='undefined') jQuery("select[name=idtipoedificacao]").val(form_values.idtipoedificacao);
		if ((typeof form_values.idcobertura)!='undefined') jQuery("select[name=idcobertura]").val(form_values.idcobertura);
		if ((typeof form_values.idtipoconstrucao)!='undefined') jQuery("select[name=idtipoconstrucao]").val(form_values.idtipoconstrucao);
		
		if ((typeof form_values.codsituacao)!='undefined') jQuery("select[name=codsituacao]").val(form_values.codsituacao);
		if ((typeof form_values.codredeensino)!='undefined') jQuery("select[name=codredeensino]").val(form_values.codredeensino);
		if ((typeof form_values.codidentificador)!='undefined') jQuery("select[name=codidentificador]").val(form_values.codidentificador);
		
		if ((typeof form_values.idsensinostipos)!='undefined') $("[name='idsensinostipos[]']").val(form_values.idsensinostipos);
		if ((typeof form_values.idsensinossubtipos)!='undefined') $("[name='idsensinossubtipos[]']").val(form_values.idsensinossubtipos);
		if ((typeof form_values.idsmaterias)!='undefined') $("[name='idsmaterias[]']").val(form_values.idsmaterias);
		if ((typeof form_values.diassemana)!='undefined')  $("select[name=diassemana]").val(form_values.diassemana);
		if ((typeof form_values.acessosdeficiente)!='undefined') {
			setObservacaoAcessibilidade(jQuery("select[name=acessosdeficiente]").val(form_values.acessosdeficiente), form_values.acessosdeficiente);
			jQuery("select[name=acessosdeficiente]").val(form_values.acessosdeficiente);
		}
		
		if (form_values.idsensinossubtipos.indexOf(105) > -1) {

			carregaAtividadeAEE();
			$('.boxAtividades').show();
		}

		if ((typeof form_values.elevador)!='undefined') jQuery("select[name=elevador]").val(form_values.elevador);
            if ((typeof form_values.gestorescolarnome)!='undefined') jQuery("select[name=gestorescolarnome]").val(form_values.gestorescolarnome);
            if ((typeof form_values.gestorescolarcpf)!='undefined') jQuery("select[name=gestorescolarcpf]").val(form_values.gestorescolarcpf);
            if ((typeof form_values.gestorescolarcargo)!='undefined') jQuery("select[name=gestorescolarcargo]").val(form_values.gestorescolarcargo);
            if ((typeof form_values.gestorescolaremail)!='undefined') jQuery("select[name=gestorescolaremail]").val(form_values.gestorescolaremail);
            if ((typeof form_values.segunda)!='undefined') jQuery("select[name=segunda]").val(form_values.segunda);
            if ((typeof form_values.terca)!='undefined') jQuery("select[name=terca]").val(form_values.terca);
            if ((typeof form_values.quarta)!='undefined') jQuery("select[name=quarta]").val(form_values.quarta);
            if ((typeof form_values.quinta)!='undefined') jQuery("select[name=quinta]").val(form_values.quinta);
            if ((typeof form_values.sexta)!='undefined') jQuery("select[name=sexta]").val(form_values.sexta);
            if ((typeof form_values.sabado)!='undefined') jQuery("select[name=sabado]").val(form_values.sabado);
            if ((typeof form_values.domingo)!='undefined') jQuery("select[name=domingo]").val(form_values.domingo);
            
		$("select").trigger("chosen:updated");
                
	}
		

	$('input[name=quantidadesaulas]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: 0
	});

        $( "input[name=pavimentos]" ).blur(function() {
            var pavimentos = $(this).val();
            pavimentos = parseInt(pavimentos);

            if(pavimentos==1){
                $('.v_elevador').css('display','block');
                $("select[name=elevador]").trigger("chosen:updated");
            }else{
                $('.v_elevador').css('display','none');
                $("select[name=elevador]").trigger("chosen:updated");
            }
        });
		
        $( "input[name=cep], input[name=endereco], input[name=numero], input[name=bairro], input[name=cidade]" ).blur(function() {
                setMapa();
        });
		
		
        $('input[name=dimensao], input[name=areaverde], input[name=areaconstruida], input[name=arealivre], input[name=areatotal]').priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '',	    
                centsLimit: 2    
        });	

        $('input[name=banheirosdeficientesmasculinos], input[name=banheirosdeficientesfemininos], input[name=pontosentradas], input[name=pontossaidas]').priceFormat({
                prefix: '',
                centsSeparator: '',
                thousandsSeparator: '.',	    
                centsLimit: 0    
        });

        $('input[name=quantidade], input[name=quantidadeperfil], input[name=capacidademedia], input[name=salasdeapoio], input[name=docentes], input[name=apoioadministrativo], input[name=apoiotecnico], input[name=apoiooperacional]').priceFormat({
                prefix: '',
                centsSeparator: '',
                thousandsSeparator: '.',	    
                centsLimit: 0    
        });	

        $('input[name=laboratoriosinformatica], input[name=laboratoriosfisica], input[name=laboratoriosquimica], input[name=laboratoriosapoio], input[name=bibliotecas], input[name=salasleitura], input[name=estacionamentos]').priceFormat({
                prefix: '',
                centsSeparator: '',
                thousandsSeparator: '.',	    
                centsLimit: 0    
        });	

        $('input[name=quadrasabertas], input[name=capacidademediaesportivo], input[name=quantidadeesportivo], input[name=instalacoesalunosespeciais], input[name=anexos]').priceFormat({
                prefix: '',
                centsSeparator: '',
                thousandsSeparator: '.',	    
                centsLimit: 0    
        });	

        $('input[name=quadrasfechadas], input[name=piscinaspadroes], input[name=piscinassemiolimpicas], input[name=piscinasolimipicas], input[name=saltoornamental], input[name=camposdefutebol], input[name=pistasatletismo]').priceFormat({
                prefix: '',
                centsSeparator: '',
                thousandsSeparator: '.',	    
                centsLimit: 0    
        });	

        $('input[name=valorpatrimonial]').priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '.',	    
                centsLimit: 2    
        });

        $('input[name=pavimentos], input[name=fracoes], input[name=pontosacessos], input[name=saidasemergencia], input[name=sistemaseguranca], input[name=saidasemergenciadeficiencia]').priceFormat({
                prefix: '',
                centsSeparator: '',
                thousandsSeparator: '.',	    
                centsLimit: 0    
        });	

        $('input[name=areaconstruidainformacoes]').priceFormat({
                prefix: '',
                centsSeparator: ',',
                thousandsSeparator: '',	    
                centsLimit: 2    
        });
		
        $("input[name=dataregistro]").mask("99/99/9999", {placeholder:" "});
        $("input[name=dataregistro]").datepicker({ 
                dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                dateFormat: 'dd/mm/yy',
                dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                currentText: 'Hoje',
                prevText: 'Antes',
                nextText: 'Depois'
        });
        
        $("input[name=dataconstrucao]").mask("99/99/9999", {placeholder:" "});
        $("input[name=dataconstrucao]").datepicker({ 
                dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
                dateFormat: 'dd/mm/yy',
                dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                currentText: 'Hoje',
                prevText: 'Antes',
                nextText: 'Depois'
        });
		

	//$('#accordion').accordion({ collapsible: true });  

        //initialize();	
	//setMapa();
	
	$('.click_acc').each(function() {
		$( this ).click();
	});
	
	$("select[name=idtiporede]").trigger("chosen:updated");

	jQuery('select, input, textarea').bind('blur', function() { });
        
        $("input[name=gestorescolarcpf]").mask("999.999.999-99", {placeholder:" "});

	rules = {};
	rules.status = "required";
	rules.escola = "required";
	rules.cep = "required";
	
	$("#form").validate({
		ignore: [],
		rules: rules,		
		errorPlacement: function(error, element) {
			if (element.is('select')) {
				error.insertAfter($(element).parent().find('div.chosen-container'));	
			} else {
				error.insertAfter($(element));
			}
		    
		},		
		submitHandler: function(form) {
			unsaved = false;
			form.submit();
		}
	});
        
});

function setObservacaoAcessibilidade(obj, acessibilidade){
	var val = $(obj).val();
	
	if(val=="Sim"){
		$('.box-acesso-deficiente').css('display','block');
	}else{
		$('.box-acesso-deficiente').css('display','none');
	}
}

var geocoder;
var map;
var marker;

function initialize() {
	var latlng = new google.maps.LatLng(-18.8800397, -47.05878999999999);
	var options = {
		zoom: 5,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
	map = new google.maps.Map(document.getElementById("mapa"), options);
	
	geocoder = new google.maps.Geocoder();
	
	marker = new google.maps.Marker({
		map: map,
		draggable: true,
	});
	
	marker.setPosition(latlng);
}

function setMapa(){
	var cep = $('input[name=cep]').val();
	var endereco = $('input[name=endereco]').val();
	var numero = $('input[name=numero]').val();
	var bairro = $('input[name=bairro]').val();
	var cidade = $('input[name=cidade]').val();
	var estado = $('.idestado_chosen .chosen-single span').text();
	
	var count_endereco = 0;
	var monta_endereco = "";
	
	if(cep){
		monta_endereco = cep;
		count_endereco = 1;
	}
	
	if(endereco){
		if(count_endereco == 1){
			monta_endereco = monta_endereco + "," + endereco;
		}else{
			count_endereco = 1;
			monta_endereco = monta_endereco + endereco;
		}				
	}
	
	if(numero){
		if(count_endereco == 1){
			monta_endereco = monta_endereco + "," + numero;
		}else{
			count_endereco = 1;
			monta_endereco = monta_endereco + numero;
		}				
	}
	
	if(bairro){
		if(count_endereco == 1){
			monta_endereco = monta_endereco + "," + bairro;
		}else{
			count_endereco = 1;
			monta_endereco = monta_endereco + bairro;
		}				
	}
	
	if(cidade){
		if(count_endereco == 1){
			monta_endereco = monta_endereco + "," + cidade;
		}else{
			count_endereco = 1;
			monta_endereco = monta_endereco + cidade;
		}				
	}
	
	if(estado){
		if(count_endereco == 1){
			monta_endereco = monta_endereco + "," + estado;
		}else{
			count_endereco = 1;
			monta_endereco = monta_endereco + estado;
		}				
	}	
	carregarNoMapa(monta_endereco);
}

function carregarNoMapa(endereco) {
	if(endereco!=""){
		geocoder.geocode({ 'address': endereco + ', Brasil', 'region': 'BR' }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
					var latitude = results[0].geometry.location.lat();
					var longitude = results[0].geometry.location.lng();
		
					$('#txtEndereco').val(results[0].formatted_address);
					$('#txtLatitude').val(latitude);
					$('#txtLongitude').val(longitude);
		
					var location = new google.maps.LatLng(latitude, longitude);
					marker.setPosition(location);
					map.setCenter(location);
					map.setZoom(16);
				}
			}
		})
	}else{
		initialize();
	}
}
	/*
	$("#btnEndereco").click(function() {
		if($(this).val() != "")
			carregarNoMapa($("#txtEndereco").val());
	})
	
	$("#txtEndereco").blur(function() {
		if($(this).val() != "")
			carregarNoMapa($(this).val());
	})
	
	google.maps.event.addListener(marker, 'drag', function () {
		geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {  
					$('#txtEndereco').val(results[0].formatted_address);
					$('#txtLatitude').val(marker.getPosition().lat());
					$('#txtLongitude').val(marker.getPosition().lng());
				}
			}
		});
	});
	
	$("#txtEndereco").autocomplete({
		source: function (request, response) {
			geocoder.geocode({ 'address': request.term + ', Brasil', 'region': 'BR' }, function (results, status) {
				response($.map(results, function (item) {
					return {
						label: item.formatted_address,
						value: item.formatted_address,
						latitude: item.geometry.location.lat(),
          				longitude: item.geometry.location.lng()
					}
				}));
			})
		},
		select: function (event, ui) {
			$("#txtLatitude").val(ui.item.latitude);
    		$("#txtLongitude").val(ui.item.longitude);
			var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
			marker.setPosition(location);
			map.setCenter(location);
			map.setZoom(16);
		}
	});*/

function selectAllmaterias(obj){
	
	if($(obj).is(":checked")){
		$('.idsmaterias').each(function() {
			$( this ).attr('checked', true);
		})
	}else{
		$('.idsmaterias').each(function() {
			$( this ).attr('checked', false);
		})
	}
}

function colocaInputNoSelect(id, value){
	if(id!=""){
    	option = "<option value='"+id+"' >"+texto+"</option>";
    	$('#groupatividades').append(option);
    }
}
function removerAtividade(btn){
	jConfirm("Deseja remover o tipo de ensino?", 'Confirmar', function(r) {
		
		if(r){
			idname = $(btn).attr("name");
			parts = idname.split('_');

			texto = $(btn).next('.tipoatividade').text();
			id = parts[1];
			
			colocaInputNoSelect(id, texto);

			if(idname!="" && idname!="undefined") {
				obj = $('.container input#'+idname);
				obj.parent().remove();
			}
		}
		
	});
}

function addAtividade(valor, texto){
	if(valor!=""){
		input = "<div class='container'>";                     
		input += "<input type='checkbox' name='idsatividadesaee[]' value='"+valor+"' style='display:none' id='ativ_"+valor+"' checked>";
        input += "	<span class='fechar' name='ativ_"+valor+"' onclick='removerAtividade(this)'>X</span>";
        input += "  <label class='tipoatividade'>"+texto+"</label>";
       	input += "</div>";
		
		$("#atividadesselecionadas").append(input);
	}
}
function setAtividades(obj){
	option = $(obj);//select
	valor = option.val();//valor selecionado

	texto = $("#groupatividades ::selected").text();//texto do valor selecionado
	addAtividade(valor, texto);//criar opção
	$('#groupatividades option').each(function(){
		key = $(this).val();
		if(key!=""){
			if(key == valor){
				$(this).remove();
			}
		}			
	});	
}
function showTipoAtividades(obj){	
	if($(obj).is(":checked")){
		$('.boxAtividades').show();
	}else{
		$('.boxAtividades').hide();
	}

	id = $(obj).val();
	//AEE
	if(id==105){
		$('#atividadesselecionadas .container input').each(function(){
			pegaID = $(this).val();
			texto = $(this).next().next().text();
			
			colocaInputNoSelect(pegaID, texto);
			$('#atividadesselecionadas').html('');
			$('.boxAtividades').hide();
			$('.boxAtividades').closest('div').find('span').html('Selecione');
		});
	}
}
function showSubtiposEnsinoGdae(obj, id){	
    if($(obj).is(":checked")){

        $('.idsensinostipos').each(function(){
        	eachContainer = $('.boxSubtipo'+id);
            eachContainer.show();
        });
    }else {
        $('.idsensinostipos').each(function(){        	
            $('.boxSubtipo'+id).hide();
        });
        $('.boxSubtipo'+id+ ' input').each(function(){    		
    		$(this).attr("checked", false);
    	});
    }

    key = $(obj).val();
    if(key==32){
    	$('#atividadesselecionadas .container input').each(function(){
			pegaID = $(this).val();
			texto = $(this).next().next().text();
			
			colocaInputNoSelect(pegaID, texto);
			$('#atividadesselecionadas').html('');
			$('.boxAtividades').hide();
			$('.boxAtividades').closest('div').find('span').html('Selecione');
		});
    }
}

function CarregaCamposChecados(){
    var check = document.getElementsByName("idsensinostipos[]"); 
    for (var i=0;i<check.length;i++){ 
        if (check[i].checked == true){ 
            box = '.boxSubtipo'+(i+1); 
            $(box).show();
        }else{
            box = '.boxSubtipo'+(i+1); 
            $(box).hide();
        }      
    }
}
/*Carregar as opções da Educação Especial*/
function carregaAtividadeAEE(){
	valores = form_values.idsatividadesaee;

	for(var i=0; i<valores.length; i++){
		$('#groupatividades option').each(function(){
			key = $(this).val();
			if(key!=""){
				if(key == valores[i]){
					texto = $(this).text();
					addAtividade(key, texto);					
					$(this).remove();
				}
			}			
		});		
	}
	
}

function setCampo(obj){
    if ($(obj).is(":checked")){
        $(obj).val(1);
    }else{
        $(obj).val(0);
    }
}


//SALAS
function salaAddTr(_values) {
	html = "";
	
	if (_values===false) {
		_values = {};
	} else {
		_values = jQuery.parseJSON(_values);
		itens = _values;
	}
	
	modelo = $("#modelo-sala").html();
	modelo = '<tr>' + modelo + '</tr>';
	modelo = $(modelo);
	table = $("table.table-sala");
	salainer = table.find('tbody');
	
	modelo.find('p').html(html);
	modelo.find('input,select,textarea').each(function() {
		name = $(this).attr('name');
		name = name.replace("_", "");
		if(_values && _values.var_db == true)name = name.replace("[]", "");
		
    
		for (var property in _values) {
			if (property==name) {
				
				$(this).attr('name', name + '[]');
				if(_values.var_db){
				}
				
				$(this).attr('value', _values[property]);
				
				if ($(this).is("select")) {
					$(this).parent().find('a').remove();
				}
			}
		}
	});
	salainer.append(modelo);
	
	$('.numbervalue').priceFormat({
		prefix: '',
		limit: 3,
        centsSeparator: '',
        thousandsSeparator: ''	    
	});

	$('.numbervalue').blur(function(){
		if($(this).val() <= 0){
			$(this).val('');	
		}
			
	});
}

function salaExcluirFromTable(obj) {
	jConfirm("Deseja excluir o registro?", 'Confirmar', function(r) {
		
       if(r){
			obj = $(obj);
			content = obj.closest('tr');
			content.find('input').remove();
			content.hide();
    	}
	});
}
