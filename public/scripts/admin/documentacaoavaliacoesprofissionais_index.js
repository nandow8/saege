$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=idescola]").val(form_values.idescola);
		$("select[name=idprofessor]").val(form_values.idescola);
		$("select[name=idperiodo]").val(form_values.idperiodo);
		$("select[name=idsala]").val(form_values.idsala);
		$("select[name=idserie]").val(form_values.idserie);
		$("select[name=idturma]").val(form_values.idturma);
		$("select[name=status1]").val(form_values.status1);
	
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});	
	}
	
	$("input[name=data_i],input[name=data_f]").mask("99/99/9999", {placeholder:" "});
	$("input[name=data_i],input[name=data_f]").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});
});

function setProfessores(obj , idprofessor){
	var idescola = $(obj).val();
	$.post(baseUrl + "/" + module + "/"+controller+"/setprofessores",{idescola: idescola}, function(data) {
		$('select[name=idprofessor]').html(data);
		
		$("select[name=idprofessor]").val(idprofessor);
		val = $("select[name=idprofessor]").find('option:selected').html();
		
		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});
		
		$("select").each(function (){
			div = $(this).closest('div').find('.chosen-container').css('width', '100%');
		});

		if((idprofessor!=0) && (idcoordenador!="")){
			$("select[name=idcoordenador]").closest('div').find('span').html('Selecione...');
		}

	});
}

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/" + module + "/"+controller+"/excluirxml",
		   	{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + "/" + module + '/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}

function statusChange($id, $obj) {
 	$obj = $($obj);
 
 	$.post(baseUrl + "/" + module + "/"+controller+"/changestatusxml",
 	{id: $id, op: controller}, function(data) {
		if ((data=="Ativo") || (data=="Bloqueado")) {
			$obj.html(data);
		} else {
			jAlert(data, "Alerta!");
		}
 	});
} 