﻿
$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		if ((typeof form_values.perguntasprofessores)!='perguntasprofessores') $("select[name=perguntasprofessores]").val(form_values.perguntasprofessores);
		if ((typeof form_values.perguntassecretaria)!='perguntassecretaria') $("select[name=perguntassecretaria]").val(form_values.perguntassecretaria);
	}
	
	$('select, input, textarea').bind('blur', function() {
		verificaErros($('#form').get(), false);
	});
	
	$("select").each(function() {
		val = $(this).find('option:selected').html();
		$(this).closest('div').find('span').html(val);
	});
	
	$("select").each(function (){
		div = $(this).closest('div').find('.chosen-container').css('width', '100%');
	});

});

function formSubmit(obj) {
	erros = verificaErros(obj, true);
	
	if (!erros) {
		scroll(0, 0);
		return false;
	}
	blockPage();
}

function verificaErros(obj, display) {
	
	erros = getFormErros(obj, display);
	setErrosBox(erros, display);
	
	if (erros.length>0) {
		return false;
	}
	return true;
}	