﻿$(document).ready(function() {
	if ((typeof form_values)!='undefined') {
		$("select[name=status]").val(form_values.status);	

		$("select").each(function() {
			val = $(this).find('option:selected').html();
			$(this).closest('div').find('span').html(val);
		});		
	}
	
	$(".line").draggable(
		{cursor: 'pointer', 
		helper: 'clone', 
		opacity: 0.75});
	$(".line").droppable(
		{accept: '.line',
      	drop: function(event, ui) { 
	      	$idTo = $(this).find("input[name='id[]']").attr("value");
	      	$idFrom = ui.draggable.find("input[name='id[]']").attr("value");
	      	
	      	if ($idTo!=$idFrom) {
				
		      	$.post(baseUrl + "/alunos/" + controller + "/changeorderxml",
				   	{to: $idTo, from: $idFrom, op: 'change'}, function(data) {
					$message = $(data).find("result").text();
					if ($message=="OK") {
						window.location.reload();
					} else {
						jAlert($message, "Alerta!", function(r) {
							window.location.reload();
						});
					}					      	
			    });		      		
	      	}
	    }
    });
});

function confirmDelete($id, $texto) {
	jConfirm('Confirmar a exclusão de "'+$texto+'"?', 'Excluir registro', function(r) {
		if (r) {
	   		$.post(baseUrl + "/alunos/"+controller+"/excluirxml",
		   		{id: $id}, function(data) {
				if (data=="OK") {
					window.location = baseUrl + '/alunos/' + controller;
				} else {
					jAlert(data, "Erro!");
				}
			});
		}
 	}); 
}



function setGrafico(tipo) {

	$.fancybox({
		'padding'		: 0,
		'autoScale'		: false,
		'transitionIn'	: 'none',
		'transitionOut'	: 'none',
		'title'			: 'Gráfico',
		'width'		: 800,
		'height'		: 600,
		'href'			: baseUrl + '/alunos/'+controller+'/setgraficos/tipo/' + tipo,
		'type'			: 'iframe'
	});

	return false;	
}
