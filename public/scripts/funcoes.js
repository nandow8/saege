jQuery.validator.addMethod("mn_cnpj", function(value, element) { 
	return isCpfCnpj(value); 
});	

$(document).ready(function() {
	if ((typeof sorting)!='undefined') {
		
		for (var i=0; i<sorting.length; i++) {
			sort = sorting[i];
			th = $('th.s-' + sort);
			
			th.on('click', function() {
				
				find = '';
				for (var i=0; i<sorting.length; i++) {
					if ($(this).hasClass('s-' + sorting[i])) {
						find = sorting[i];
						break;
					}
				}
				
				i_sorting = $('[name=sorting]').val();
				i_sorting = i_sorting.split('_');
				if ((i_sorting.length==2) && (i_sorting[1]=='asc')) {
					$('[name=sorting]').val(find + '_desc');
				}  else {
					$('[name=sorting]').val(find + '_asc');
				}
				setSortingView();
				$.blockUI({ message: '<h1> Aguarde...</h1>' });
				form = th.closest('form')
				form.submit();
				
				
			});
		}
		setSortingView();
	}
	
	$('div.alert').delay(7000).slideUp(1500, function() {
	    // Animation complete.
	  });
	
});

function setSortingView() {
	
	for (var i=0; i<sorting.length; i++) {
		sort = sorting[i];
		th = $('th.s-' + sort);
		th.removeClass('sorting_asc');
		th.removeClass('sorting_desc');
		th.addClass('sorting');
	}
	
	i_sorting = $('[name=sorting]').val();
	i_sorting = i_sorting.split('_');	
	if (i_sorting.length>0) {
		th = $('th.s-' + i_sorting[0]);
		th.addClass('sorting_' + i_sorting[1]); 
	}	
	
}

function buscaCEP(cep) {
	$.post(baseUrl + '/index/buscacep', {cep:cep}, function(cep) {
		if (cep=='false') {
			jAlert('CEP não encontrado!','Erro');
			return;
		}
		
		cep = jQuery.parseJSON(cep);
		$("[name=uf]").val(cep.uf);
		
		$("[name=uf]").select2("destroy");
		$("[name=uf]").select2();
		
		$("[name=cidade]").val(cep.cidade);
		$("[name=bairro]").val(cep.bairro);
		$("[name=tipo]").val(cep.tipo_logradouro);
		$("[name=logradouro]").val(cep.logradouro);
	});
}