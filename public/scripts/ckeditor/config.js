﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/*
CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};
*/

CKEDITOR.editorConfig = function( config ) {	
	config.toolbar = 'Custom';
	config.format_tags = 'h2';
	config.toolbar_Custom =
	[
		{ name: 'document',		items : [ 'Source' ] },
		{ name: 'tools',		items : [ 'Maximize' ] },	
		{ name: 'paragraph', 	items : [ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
		'/',
		{ name: 'editing',		items : [ 'SelectAll','-','RemoveFormat' ] },
		{ name: 'basicstyles',	items : [ 'Bold','Italic','Underline', ] },
		{ name: 'links',		items : [ 'Link','Unlink' ] }
	];
};

