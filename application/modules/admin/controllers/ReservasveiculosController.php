<?php

/**
* Controle da classe recepcaogeralvisitas do sistema
*
* @author		Fernando Alves		
* @uses        Zend_Controller_Action
* @copyright   Copyright (c) 2018 FJMX. 
* @version     1.0
*/
class Admin_ReservasveiculosController extends Zend_Controller_Action {
	
	/**
    * Propriedade protegida que contem os dados do usário logado
    * @var Reservasveiculos
    */
	protected $_usuario = null;	
	
	
	/**
    * Verificação de permissao de acesso
    */
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
		Mn_Util::blockAccess("reservasveiculos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$reservasveiculos = new Reservasveiculos();
		$queries = array();	
		$ultimo = $reservasveiculos->getUltimoReservasveiculos($queries);
		
		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial; 
		
	}
    
	/**
    * Action para modificar o status via Ajax
    */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="reservasveiculos") $objs = new Reservasveiculos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Reservasveiculos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Reserva de veiculo excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}	
	
	
	/**
    *
    * Action de edição de perfil de acesso
    */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'reservasveiculos', 'name' => 'Controle de visitas'),
			array('url' => null,'name' => 'Visualizar Controle de visita')
		);
		
		$id = (int)$this->_request->getParam("id");
		$reservasveiculos = new Reservasveiculos();
		$reservasveiculos = $reservasveiculos->getReservasveiculoById($id, array());
		
		if (!$reservasveiculos) 
        $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $reservasveiculos;
		$this->preForm();
        
		$this->view->visualizar = true;
		return true;
	} 
	
	/**
    * Listagem
    */
	public function indexAction() {   
        $this->view->bread_crumb = array(
			array('url' => false,'name' => 'Controle de visitas')
		);
		
		$ns = new Zend_Session_Namespace('default_reservasveiculos');
		$reservasveiculos = new Reservasveiculos();
		$queries = array();	
        
		//PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();	
        }
        
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
            
			
            
            if ($this->view->post_var["periodo"]!="") $queries["periodo"] = $this->view->post_var["periodo"];
            if ($this->view->post_var["localdestino"]!="") $queries["localdestino"] = $this->view->post_var["localdestino"];
            if ($this->view->post_var["idveiculo"]!="") $queries["idveiculo"] = $this->view->post_var["idveiculo"];
            if ($this->view->post_var["datareserva"]!="") $queries["datareserva"] = $this->view->post_var["datareserva"];
            if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"]; 
            
            if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
        }		
		
		//PAGINACAO
        $maxpp = 20;
		
        $paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $reservasveiculos->getReservasveiculos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
        
		$this->view->rows = $reservasveiculos->getReservasveiculos($queries, $paginaAtual, $maxpp);	
	}

	/**
	 * 
	 * Action de edição de rematriculas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'reservasveiculos', 'name' => 'Reservasveiculos'),
			array('url' => null,'name' => 'Editar Reservasveiculo')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$reservasveiculos = new Reservasveiculos();
		$reservasveiculo = $reservasveiculos->getReservasveiculoById($id);
		
		if (!$reservasveiculo) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $reservasveiculo;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($reservasveiculo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Reserva do veiculo editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de rematriculas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'rematriculas', 'name' => 'Rematrículas'),
			array('url' => null,'name' => 'Adicionar Rematrícula')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Rematrícula adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
	}	
	
	public function getdadosveiculoAction() {
		$this -> _helper -> layout -> disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");		

		$id = (int)$this -> getRequest() -> getPost('idveiculo');
		$veiculo = new Logisticafrotas();
		$rows = $veiculo -> getLogisticafrotaByIdHelper($id);

		echo json_encode($rows);
		die();
	}

	public function getprofessorlocalAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$id = (int)$this->getRequest()->getPost('idfuncionario');
		$professores = new Funcionariosgeraisescolas();
		$rows = $professores->getFuncionariogeralescolaByIdHelper($id);
 
		$local = Locais::getLocalByIdHelper($rows['idlocal']);
		 
		echo json_encode($local);
		die();

	}
    
    /**
    * Atribui valores ao view
    * @param int $idreservasveiculos
    */    
    private function preForm($idreservasveiculos = 0) {
	}    
	
	 private function getPost($_financeirolancamentoreceita = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idveiculo = (int)$this->getRequest()->getPost("idveiculo");
		$idusuario = (int)$this->getRequest()->getPost("idusuario"); 
		$datareserva = Mn_Util::stringToTime($this->getRequest()->getPost("datareserva"));
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$periodo = trim($this->getRequest()->getPost("periodo")); 
		// $solicitarreserva = trim($this->getRequest()->getPost("solicitarreserva"));
		$observacao = trim($this->getRequest()->getPost("observacao"));
		$localdestino =  trim($this->getRequest()->getPost("localdestino")); 
		// $status = trim($this->getRequest()->getPost("status1"));
		$idfuncionario = trim($this->getRequest()->getPost("idfuncionario"));
	  
		$erros = array();
		
		if (""==$sequencial) array_push($erros, "Informe a Sequencial."); 
 
		$reservasveiculos = new Reservasveiculos();
		// não permite alugar veiculo no mesmo dia e periodo(mas se foi alugado de manhã, pode alugar de tarde.) 
		$verificalocacaoveiculo = 1; 
		$databanco = date('Y-m-d', $datareserva);  
		$verificareservaveiculo = "SELECT idfuncionario, idveiculo, periodo, datareserva FROM reservasveiculos WHERE idveiculo = $idveiculo AND datareserva ='$databanco' ";
		$db = Zend_Registry::get('db');
		$veiculoalugado = $db->fetchAll($verificareservaveiculo); 

		if(!empty($veiculoalugado)){
			$veiculoalugado[1]['periodo'] = '';
			$veiculoalugado[1]['idveiculo'] = '';
			$verificalocacaoveiculo = 0;
		    
			if($veiculoalugado[0]['periodo'] == 'tarde' AND $periodo == 'manha' AND $veiculoalugado[0]['idveiculo'] == $idveiculo OR $veiculoalugado[1]['idveiculo'] == $idveiculo){
					$verificalocacaoveiculo = 1; 
			}

			if($veiculoalugado[0]['periodo'] == 'manha' AND $periodo == 'tarde' AND $veiculoalugado[0]['idveiculo'] == $idveiculoo OR $veiculoalugado[1]['idveiculo'] == $idveiculo){
					$verificalocacaoveiculo = 1; 
			}

			if($veiculoalugado[0]['periodo'] == 'manha' AND  $veiculoalugado[1]['periodo'] == 'tarde' AND $veiculoalugado[0]['idveiculo'] == $idveiculoo OR $veiculoalugado[1]['idveiculo'] == $idveiculo){
					$verificalocacaoveiculo = 0; 
			} 
			
			if($veiculoalugado[0]['periodo'] == 'tarde' AND  $veiculoalugado[1]['periodo'] == 'manha' AND $veiculoalugado[0]['idveiculo'] == $idveiculoo OR $veiculoalugado[1]['idveiculo'] == $idveiculo){
					$verificalocacaoveiculo = 0; 
			} 
		    
			$funcionariogeral = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($veiculoalugado[0]['idfuncionario']);
			$local = Locais::getLocalByIdHelper($funcionariogeral['idlocal']);

			$nomefuncionario = $funcionariogeral['nome'];
			$localfuncionario = $local['titulo'] ;
			//if (0==$verificalocacaoveiculo) array_push($erros, "Este veículo já está reservado para $nomefuncionario da $localfuncionario.");
                        if (0==$verificalocacaoveiculo) array_push($erros, "Este veículo já está reservado.");

		}	
		
		if (0==$idveiculo) array_push($erros, "Selecione um Veículo.");
		//if (0==$periodo) array_push($erros, "Selecione um Período.");
		if (0==$datareserva) array_push($erros, "Selecione uma data.");
		//if (0==$localdestino) array_push($erros, "Selecione uma Destino.");
		 
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idveiculo"] = $idveiculo;
			$dados["idusuario"] = $idusuario;
			$dados["datareserva"] = date("Y-m-d", $datareserva); 
			$dados["sequencial"] = $sequencial;
			$dados["periodo"] = $periodo; 
			// $dados["solicitarreserva"] = $solicitarreserva;
			$dados["observacao"] = $observacao;
			$dados["localdestino"] =  $localdestino;
			$dados["idfuncionario"] =  $idfuncionario;
			
			// $dados["status"] = $status; 
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
				 
			$row = $reservasveiculos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();
        
        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
            
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros,$e->getMessage());
        }
        
        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
        return $idimagem;
    }
    
}