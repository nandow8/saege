<?php

/**
 * Controle da classe logisticassegurorevisoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_LogisticassegurorevisoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Logisticassegurorevisao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("logisticassegurorevisoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$logisticassegurorevisoes = new Logisticassegurorevisoes();
		$queries = array();	
		$ultimo = $logisticassegurorevisoes->getUltimoLogisticasegurorevisao($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Logisticassegurorevisoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Seguro e Revisão excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="logisticassegurorevisoes") $objs = new Logisticassegurorevisoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticassegurorevisoes', 'name' => 'Seguros e Revisões'),
			array('url' => null,'name' => 'Visualizar Seguro e Revisão')
		);
		
		$id = (int)$this->_request->getParam("id");
		$logisticassegurorevisoes = new Logisticassegurorevisoes();
		$logisticassegurorevisao = $logisticassegurorevisoes->getLogisticassegurorevisaoById($id, array());
		
		if (!$logisticassegurorevisao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticassegurorevisao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Seguros e Revisões')
		);
		
		$ns = new Zend_Session_Namespace('default_logisticassegurorevisoes');
		$logisticassegurorevisoes = new Logisticassegurorevisoes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idveiculo"]!="") $queries["idveiculo"] = $this->view->post_var["idveiculo"];
// if ($this->view->post_var["seguro"]!="") $queries["seguro"] = $this->view->post_var["seguro"];
if ($this->view->post_var["seguradora"]!="") $queries["seguradora"] = $this->view->post_var["seguradora"];
// if ($this->view->post_var["numero_apolice"]!="") $queries["numero_apolice"] = $this->view->post_var["numero_apolice"];
// if ($this->view->post_var["revisao"]!="") $queries["revisao"] = $this->view->post_var["revisao"];
if ($this->view->post_var["meslicenciamento_vencimento"]!="") $queries["meslicenciamento_vencimento"] = $this->view->post_var["meslicenciamento_vencimento"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $logisticassegurorevisoes->getLogisticassegurorevisoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $logisticassegurorevisoes->getLogisticassegurorevisoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de logisticassegurorevisoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticassegurorevisoes', 'name' => 'Seguros e Revisões'),
			array('url' => null,'name' => 'Editar Seguro e Revisão')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$logisticassegurorevisoes = new Logisticassegurorevisoes();
		$logisticassegurorevisao = $logisticassegurorevisoes->getLogisticassegurorevisaoById($id);
		
		if (!$logisticassegurorevisao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticassegurorevisao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($logisticassegurorevisao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Seguro e Revisão editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de logisticassegurorevisoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticassegurorevisoes', 'name' => 'Seguros e Revisões'),
			array('url' => null,'name' => 'Adicionar Seguro e Revisão')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Seguro e Revisão adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
	}	
	
	public function getdadosveiculoAction() {
		$this -> _helper -> layout -> disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");		

		$id = (int)$this -> getRequest() -> getPost('idveiculo');
		$veiculo = new Logisticafrotas();
		$rows = $veiculo -> getLogisticafrotaByIdHelper($id);

		echo json_encode($rows);
		die();
	}

	public function getvalidadecnhAction(){
		$this -> _helper -> layout -> disableLayout(); 
		$id = (int)$this -> getRequest() -> getPost('idmotorista');
		$cnhVencimento = Logisticamotoristas::getLogisticamotoristaByIdHelper($id);
		$datavalidadecnh  = new DateTime($cnhVencimento['datavalidadecnh']);
		$datahoje = new DateTime(date('Y-m-d G:i:s'));  
		 if($datavalidadecnh < $datahoje){
			 $validadeCNH = 'vencida'; 
		 }else{
			 $validadeCNH = $datavalidadecnh->diff($datahoje);
		 }
		
		echo json_encode($validadeCNH);
		die();
	}
    
    /**
     * Atribui valores ao view
     * @param int $idlogisticassegurorevisao
     */    
    private function preForm($idlogisticassegurorevisao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_logisticassegurorevisao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idveiculo = (int)trim($this->getRequest()->getPost("idveiculo"));
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		$meslicenciamento_vencimento = (int)$this->getRequest()->getPost("meslicenciamento_vencimento");
		$diasaviso_vencimento = trim($this->getRequest()->getPost("diasaviso_vencimento"));
		$seguro = trim($this->getRequest()->getPost("seguro"));
		$seguradora = trim($this->getRequest()->getPost("seguradora"));
		$numero_apolice = trim($this->getRequest()->getPost("numero_apolice"));
		$corretora = trim($this->getRequest()->getPost("corretora"));
		$telefone_seguradora = trim($this->getRequest()->getPost("telefone_seguradora"));
		$periodo_vigenciainicial = Mn_Util::stringToTime($this->getRequest()->getPost("periodo_vigenciainicial"));
		$periodo_vigenciafinal = Mn_Util::stringToTime($this->getRequest()->getPost("periodo_vigenciafinal"));
		$diasaviso_vencimentoseguro = (int)$this->getRequest()->getPost("diasaviso_vencimentoseguro");
		$revisao = trim($this->getRequest()->getPost("revisao"));
		$concessionaria = trim($this->getRequest()->getPost("concessionaria"));
		$telefone_concessionaria = trim($this->getRequest()->getPost("telefone_concessionaria"));
		$km_atual =  (int)$this->getRequest()->getPost("km_atual");
		$revisao1 = (int)trim($this->getRequest()->getPost("revisao1"));
		$avisar_vencimentorevisao1 = (int)trim($this->getRequest()->getPost("avisar_vencimentorevisao1"));
		$revisao2 = (int)trim($this->getRequest()->getPost("revisao2"));
		$avisar_vencimentorevisao2 = (int)trim($this->getRequest()->getPost("avisar_vencimentorevisao2"));
		$revisao3 = (int)trim($this->getRequest()->getPost("revisao3"));
		$avisar_vencimentorevisao3 = (int)trim($this->getRequest()->getPost("avisar_vencimentorevisao3"));
		$revisao4 = (int)trim($this->getRequest()->getPost("revisao4"));
		$avisar_vencimentorevisao4 = (int)trim($this->getRequest()->getPost("avisar_vencimentorevisao4"));
		$status = trim($this->getRequest()->getPost("status1")); 
		
		$erros = array();
		
		if (0==$idveiculo) array_push($erros, "Informe a Veículo.");
		if (""==$sequencial) array_push($erros, "Informe a Sequencial/Ano.");
		if (""==$seguro) array_push($erros, "Informe a Possui Seguro?.");
		if (""==$revisao) array_push($erros, "Informe a Veículo possui revisões?.");
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$logisticassegurorevisoes = new Logisticassegurorevisoes();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idveiculo"] = $idveiculo;
			if((int)$id <= 0 ) $dados["sequencial"] = $sequencial;
			$dados["data"] = date("Y-m-d", $data);
			$dados["meslicenciamento_vencimento"] = $meslicenciamento_vencimento;
			$dados["diasaviso_vencimento"] = $diasaviso_vencimento;
			$dados["seguro"] = $seguro;
			$dados["seguradora"] = $seguradora;
			$dados["numero_apolice"] = $numero_apolice;
			$dados["corretora"] = $corretora;
			$dados["telefone_seguradora"] = $telefone_seguradora;
			$dados["periodo_vigenciainicial"] = date("Y-m-d", $periodo_vigenciainicial);
			$dados["periodo_vigenciafinal"] = date("Y-m-d", $periodo_vigenciafinal);
			$dados["diasaviso_vencimentoseguro"] = $diasaviso_vencimentoseguro;
			$dados["revisao"] = $revisao;
			$dados["concessionaria"] = $concessionaria;
			$dados["telefone_concessionaria"] = $telefone_concessionaria;
			$dados["km_atual"] = $km_atual;
			$dados["revisao1"] = $revisao1;
			$dados["avisar_vencimentorevisao1"] = $avisar_vencimentorevisao1;
			$dados["revisao2"] = $revisao2;
			$dados["avisar_vencimentorevisao2"] = $avisar_vencimentorevisao2;
			$dados["revisao3"] = $revisao3;
			$dados["avisar_vencimentorevisao3"] = $avisar_vencimentorevisao3;
			$dados["revisao4"] = $revisao4;
			$dados["avisar_vencimentorevisao4"] = $avisar_vencimentorevisao4;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
		 		
			$row = $logisticassegurorevisoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}