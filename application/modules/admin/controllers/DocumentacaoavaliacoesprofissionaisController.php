<?php

/**
 * Controle da classe documentacaoavaliacoesprofissionais do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_DocumentacaoavaliacoesprofissionaisController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Documentacaoavaliacaoprofissional
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("documentacaoavaliacoesprofissionais", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Documentacaoavaliacoesprofissionais();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Avaliação de Desempenho Profissional excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="documentacaoavaliacoesprofissionais") $objs = new Documentacaoavaliacoesprofissionais();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaoavaliacoesprofissionais', 'name' => 'Avaliações de Desempenho Profissional'),
			array('url' => null,'name' => 'Visualizar Avaliação de Desempenho Profissional')
		);
		
		$id = (int)$this->_request->getParam("id");
		$documentacaoavaliacoesprofissionais = new Documentacaoavaliacoesprofissionais();
		$documentacaoavaliacaoprofissional = $documentacaoavaliacoesprofissionais->getDocumentacaoavaliacaoprofissionalById($id, array());
		
		if (!$documentacaoavaliacaoprofissional) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $documentacaoavaliacaoprofissional;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Avaliações de Desempenho Profissional')
		);
		
		$ns = new Zend_Session_Namespace('default_documentacaoavaliacoesprofissionais');
		$documentacaoavaliacoesprofissionais = new Documentacaoavaliacoesprofissionais();
		$queries = array();	
		$queries['idescola'] = Usuarios::getUsuario('idescola');
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
			if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["idprofessor"]!="") $queries["idprofessor"] = $this->view->post_var["idprofessor"];
			if ($this->view->post_var["rgf"]!="") $queries["rgf"] = $this->view->post_var["rgf"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $documentacaoavaliacoesprofissionais->getDocumentacaoavaliacoesprofissionais($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $documentacaoavaliacoesprofissionais->getDocumentacaoavaliacoesprofissionais($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de documentacaoavaliacoesprofissionais
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaoavaliacoesprofissionais', 'name' => 'Avaliações de Desempenho Profissional'),
			array('url' => null,'name' => 'Editar Avaliação de Desempenho Profissional')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$documentacaoavaliacoesprofissionais = new Documentacaoavaliacoesprofissionais();
		$documentacaoavaliacaoprofissional = $documentacaoavaliacoesprofissionais->getDocumentacaoavaliacaoprofissionalById($id);
		
		if (!$documentacaoavaliacaoprofissional) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$avaliacao = Documentacaoavaliacoesperguntas::getDocumentacaoavaliacaoperguntaByIdParentHelper($documentacaoavaliacaoprofissional['id']);
		if($avaliacao)
		{
			unset($avaliacao['id']);
			unset($avaliacao['status']);
			unset($avaliacao['excluido']);
			unset($avaliacao['datacriacao']);
			$documentacaoavaliacaoprofissional = array_merge($documentacaoavaliacaoprofissional, $avaliacao);
		}
		
		$this->view->post_var = $documentacaoavaliacaoprofissional;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($documentacaoavaliacaoprofissional);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Avaliação de Desempenho Profissional editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de documentacaoavaliacoesprofissionais 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaoavaliacoesprofissionais', 'name' => 'Avaliações de Desempenho Profissional'),
			array('url' => null,'name' => 'Adicionar Avaliação de Desempenho Profissional')
		);	
				
		$this->preForm();
		$this->view->adicionar = true;
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Avaliação de Desempenho Profissional adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $iddocumentacaoavaliacaoprofissional
     */    
    private function preForm($iddocumentacaoavaliacaoprofissional = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_documentacaoavaliacaoprofissional = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);

		$id = (int)$this->getRequest()->getPost("id");
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
		$rgf = trim($this->getRequest()->getPost("rgf"));
		$idperiodo = (int)trim($this->getRequest()->getPost("idperiodo"));
		$idsala = (int)trim($this->getRequest()->getPost("idsala"));
		$idserie = (int)trim($this->getRequest()->getPost("idserie"));
		$idturma = (int)trim($this->getRequest()->getPost("idturma"));
		$atualizarpont = trim($this->getRequest()->getPost("atualizarpont"));
		$status = trim($this->getRequest()->getPost("status1"));
		$aprovado = trim($this->getRequest()->getPost("aprovado"));
		
		$avaliacao_1 = trim($this->getRequest()->getPost("avaliacao_1"));
		$avaliacao_2 = trim($this->getRequest()->getPost("avaliacao_2"));
		$avaliacao_3 = trim($this->getRequest()->getPost("avaliacao_3"));
		$avaliacao_4 = trim($this->getRequest()->getPost("avaliacao_4"));
		$avaliacao_5 = trim($this->getRequest()->getPost("avaliacao_5"));
		$avaliacao_6 = trim($this->getRequest()->getPost("avaliacao_6"));
		$avaliacao_7 = trim($this->getRequest()->getPost("avaliacao_7"));
		$avaliacao_8 = trim($this->getRequest()->getPost("avaliacao_8"));
		$avaliacao_9 = trim($this->getRequest()->getPost("avaliacao_9"));
		$avaliacao_10 = trim($this->getRequest()->getPost("avaliacao_10"));
		$avaliacao_11 = trim($this->getRequest()->getPost("avaliacao_11"));
		$avaliacao_12 = trim($this->getRequest()->getPost("avaliacao_12"));
		$avaliacao_13 = trim($this->getRequest()->getPost("avaliacao_13"));
		$avaliacao_14 = trim($this->getRequest()->getPost("avaliacao_14"));
		$avaliacao_15 = trim($this->getRequest()->getPost("avaliacao_15"));
		$avaliacao_16 = trim($this->getRequest()->getPost("avaliacao_16"));

		$avaliacao = trim($this->getRequest()->getPost("avaliacao"));

		$erros = array();
		
		//if (""==$data) array_push($erros, "Informe a Data.");
		if (0==$idescola) array_push($erros, "Informe a Unidade Escolas.");
		if (0==$idprofessor) array_push($erros, "Informe a Professor.");
		if (""==$rgf) array_push($erros, "Informe a RGF.");
		if (""==$status) array_push($erros, "Informe a Status.");

		if (""==$avaliacao_1) array_push($erros, "Responda a questão 1.");
		if (""==$avaliacao_2) array_push($erros, "Responda a questão 2.");
		if (""==$avaliacao_3) array_push($erros, "Responda a questão 3.");
		if (""==$avaliacao_4) array_push($erros, "Responda a questão 4.");
		if (""==$avaliacao_5) array_push($erros, "Responda a questão 5.");
		if (""==$avaliacao_6) array_push($erros, "Responda a questão 6.");
		if (""==$avaliacao_7) array_push($erros, "Responda a questão 7.");
		if (""==$avaliacao_8) array_push($erros, "Responda a questão 8.");
		if (""==$avaliacao_9) array_push($erros, "Responda a questão 9.");
		if (""==$avaliacao_10) array_push($erros, "Responda a questão 10.");
		if (""==$avaliacao_11) array_push($erros, "Responda a questão 11.");
		if (""==$avaliacao_12) array_push($erros, "Responda a questão 12.");
		if (""==$avaliacao_13) array_push($erros, "Responda a questão 13.");
		if (""==$avaliacao_14) array_push($erros, "Responda a questão 14.");
		if (""==$avaliacao_15) array_push($erros, "Responda a questão 15.");
		if (""==$avaliacao_16) array_push($erros, "Responda a questão 16.");

		$documentacaoavaliacoesprofissionais = new Documentacaoavaliacoesprofissionais();

		$row = $documentacaoavaliacoesprofissionais->fetchRow("excluido='nao' AND idprofessor=$idprofessor AND id<>$id AND YEAR(datacriacao) = YEAR(NOW())");
		if($row && $row['rgf'] == $rgf) array_push($erros, "Este professor já foi avaliado neste ano letivo.");
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["data"] = date("Y-m-d");
			$dados["idescola"] = $idescola;
			$dados["idprofessor"] = $idprofessor;
			$dados["rgf"] = $rgf;
			$dados["idperiodo"] = $idperiodo;
			$dados["idsala"] = $idsala;
			$dados["idserie"] = $idserie;
			$dados["idturma"] = $idturma;
			$dados["avaliacao"] = $avaliacao;
			$dados["atualizarpont"] = $atualizarpont;
			$dados["status"] = $status;
			$dados["aprovado"] = $aprovado;

			$dados_avaliacao = array();
			$dados_avaliacao['avaliacao_1'] = $avaliacao_1;
			$dados_avaliacao['avaliacao_2'] = $avaliacao_2;
			$dados_avaliacao['avaliacao_3'] = $avaliacao_3;
			$dados_avaliacao['avaliacao_4'] = $avaliacao_4;
			$dados_avaliacao['avaliacao_5'] = $avaliacao_5;
			$dados_avaliacao['avaliacao_6'] = $avaliacao_6;
			$dados_avaliacao['avaliacao_7'] = $avaliacao_7;
			$dados_avaliacao['avaliacao_8'] = $avaliacao_8;
			$dados_avaliacao['avaliacao_9'] = $avaliacao_9;
			$dados_avaliacao['avaliacao_10'] = $avaliacao_10;
			$dados_avaliacao['avaliacao_11'] = $avaliacao_11;
			$dados_avaliacao['avaliacao_12'] = $avaliacao_12;
			$dados_avaliacao['avaliacao_13'] = $avaliacao_13;
			$dados_avaliacao['avaliacao_14'] = $avaliacao_14;
			$dados_avaliacao['avaliacao_15'] = $avaliacao_15;
			$dados_avaliacao['avaliacao_16'] = $avaliacao_16;

			$dados['perguntas'] = $dados_avaliacao;

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $documentacaoavaliacoesprofissionais->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}
		
		return "";    	
    }

    public function setprofessoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idescola"] = $idescola;
		$queries["professor"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
    }

    public function getrgfAction() {
    	$this->_helper->layout->disableLayout();

    	$idprofessor = (int)$this->_request->getPost("idprofessor");
    	
    	$rows = new Funcionariosgeraisescolas();
    	$professor = $rows->getFuncionariogeralescolaById($idprofessor);

    	die($professor['rgf']);
    }


	public function setperiodosAction() {
		$this->_helper->layout->disableLayout();
		
		$idprofessor = (int)$this->getRequest()->getPost('idprofessor');
		//var_dump($idprofessor);die();

		$vinculos = Escolasvinculosprofessoresmaterias::getEscolasvinculosprofessoresmaterias(array('idprofessor' => (int)$idprofessor));
    	
    	//var_dump($vinculos);die();
    	$this->view->rows = $vinculos;
	}

	public function setsalasAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
    	
		$salas = new Escolassalas();
    	$this->view->rows = $salas->getEscolassalas(array('idescola'=>$idescola, 'status'=>'Ativo')); 
	}

    public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries['idescola'] = $idescola;

		$rows = new Escolasseries();
		$this->view->rows = $rows->getEscolasseries($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setturmasAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		
		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculos($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0)) $this->view->rows = null;		
	}
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}