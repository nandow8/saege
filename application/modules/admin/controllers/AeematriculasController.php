<?php

class MYPDF extends TCPDF {

	//Page header
	public function Header(){
		$image_file = "public/admin/imagens/logosantaisabel.jpg";
		$this->Image($image_file, 18, 5, 20, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', '', 5);
        $this->SetXY(16, 26);
        $this->Cell(0, 0, 'Paraíso da Grande São Paulo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
       
        // cabeçalho Endereço 
        $this->SetFont('helvetica', '', 12);
        $this->SetXY(84, 6);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Municipio de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', 'B', 10);
        $this->SetXY(80, 10);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Secretaria Municipal de Educação', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(75, 14);
        $this->SetTextColor(130, 130, 130);
		$this->Cell(0, 0, 'Tel. 4656-2440 e-mail: sec.educacao@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 8);
        $this->SetXY(75, 18);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297-Centro - CEP: 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 12);
        $this->SetXY(90, 30);
        $this->SetTextColor(0, 0, 0);
        $this->Cell(0, 0, 'Matrículas Aee', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$image_file = "public/admin/imagens/aeelogo.jpeg";
		$this->Image($image_file, 175, 5, 20, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', '', 5);
        $this->SetXY(16, 26);
        $this->Cell(0, 0, 'Paraíso da Grande São Paulo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
 
        //informações Saege
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(60,20);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

       	$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();

        $this->SetFont('helvetica', '', 7);
        $this->SetXY(60,23);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: '.$data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 7);
        $this->SetXY(40,26);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        	

	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
        $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Position at 15 mm from bottom
		$this->SetXY(150,-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

/**
 * Controle da classe aeematriculas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_AeematriculasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Aeematricula
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("aeematriculas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Aeematriculas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Matrícula no Polo AEE excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="aeematriculas") $objs = new Aeematriculas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'aeematriculas', 'name' => 'Matrículas no Polo AEE'),
			array('url' => null,'name' => 'Visualizar Matrícula no Polo AEE')
		);
		
		$id = (int)$this->_request->getParam("id");
		$aeematriculas = new Aeematriculas();
		$aeematricula = $aeematriculas->getAeematriculaById($id, array());
		
		if (!$aeematricula) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $aeematricula;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Matrículas no Polo AEE')
		);
		
		$ns = new Zend_Session_Namespace('default_aeematriculas');
		$aeematriculas = new Aeematriculas();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idaluno"]!="") $queries["idaluno"] = $this->view->post_var["idaluno"];
if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $aeematriculas->getAeematriculas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $aeematriculas->getAeematriculas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de aeematriculas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'aeematriculas', 'name' => 'Matrículas no Polo AEE'),
			array('url' => null,'name' => 'Editar Matrícula no Polo AEE')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$aeematriculas = new Aeematriculas();
		$aeematricula = $aeematriculas->getAeematriculaById($id);
		
		if (!$aeematricula) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $aeematricula;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($aeematricula);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Matrícula no Polo AEE editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
		$this->view->editar = true;	
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de aeematriculas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'aeematriculas', 'name' => 'Matrículas no Polo AEE'),
			array('url' => null,'name' => 'Adicionar Matrícula no Polo AEE')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Matrícula no Polo AEE adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idaeematricula
     */    
    private function preForm($idaeematricula = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_aeematricula = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$escolaregular = trim($this->getRequest()->getPost("escolaregular"));
		$idperiodo = (int)trim($this->getRequest()->getPost("idperiodo"));
		// $idauxiliar = (int)trim($this->getRequest()->getPost("idauxiliar"));
		$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
		$idprofessor2 = (int)trim($this->getRequest()->getPost("idprofessor2"));
		$aulas = trim($this->getRequest()->getPost("aulas"));
		$iddeficiencia = (int)trim($this->getRequest()->getPost("iddeficiencia"));
		$laudo = trim($this->getRequest()->getPost("laudo"));
		$cid = trim($this->getRequest()->getPost("cid"));
		$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo")); 
		$idarquivotermocompromisso = (int)trim($this->getRequest()->getPost("idarquivotermocompromisso")); 
		$idarquivotermoautorizacaopasseio = (int)trim($this->getRequest()->getPost("idarquivotermoautorizacaopasseio")); 
		$idarquivotermodesistencia = (int)trim($this->getRequest()->getPost("idarquivotermodesistencia")); 
		$apoio = trim($this->getRequest()->getPost("apoio"));
		$necessidades = trim($this->getRequest()->getPost("necessidades"));
		$descricoes = trim($this->getRequest()->getPost("descricoes"));
		$transporte = trim($this->getRequest()->getPost("transporte"));
		$hipotesedeescrita = trim($this->getRequest()->getPost("hipotesedeescrita"));
		$transporteopcoes = $this->getRequest()->getPost("transporteopcoes");
		$transporteopcoes = implode(',', $transporteopcoes);
		$tipodeficiencia = $this->getRequest()->getPost("tipodeficiencia");
		$tipodeficiencia = implode(',', $tipodeficiencia);
		$atendimentoespecializado = $this->getRequest()->getPost("atendimentoespecializado");
		$atendimentoespecializado = implode(',', $atendimentoespecializado);
		$atendimentoespecializadosimnao = trim($this->getRequest()->getPost("atendimentoespecializadosimnao"));
		$atendimentoespecializadoObs = trim($this->getRequest()->getPost("atendimentoespecializadoObs"));
		$observacoes = trim($this->getRequest()->getPost("observacoes"));
		$responsavel = trim($this->getRequest()->getPost("responsavel"));
		$medicacao = trim($this->getRequest()->getPost("medicacao"));
		$qualmedicacao = trim($this->getRequest()->getPost("qualmedicacao"));
		$horarioMedicacao = trim($this->getRequest()->getPost("horarioMedicacao"));
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		// ====== Lista de matérias (salva em outra tabela)
			$segundafeira = $this->getRequest()->getPost("segundafeira");
			$tercafeira = $this->getRequest()->getPost("tercafeira");
			$quartafeira = $this->getRequest()->getPost("quartafeira"); 
			$quintafeira = $this->getRequest()->getPost("quintafeira");
			$sextafeira = $this->getRequest()->getPost("sextafeira"); 
		// =======

		$erros = array();
		
		//if (0==$idaluno) array_push($erros, "Informe a Selecionar Aluno.");
 

		
		$aeematriculas = new Aeematriculas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idaluno"] = $idaluno;
			$dados["idescola"] = $idescola;
			$dados["escolaregular"] = $escolaregular;
			$dados["idperiodo"] = $idperiodo;
			$dados["idauxiliar"] = $idauxiliar;
			$dados["idprofessor"] = $idprofessor;
			$dados["idprofessor2"] = $idprofessor2;
			$dados["aulas"] = $aulas;
			$dados["iddeficiencia"] = $iddeficiencia;
			$dados["laudo"] = $laudo;
			$dados["cid"] = $cid;

			$idarquivo = $this->getArquivo('idarquivo');
						if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo; 

			$idarquivotermocompromisso = $this->getArquivo('idarquivotermocompromisso');
						if ($idarquivotermocompromisso!=0) $dados['idarquivotermocompromisso'] = $idarquivotermocompromisso; 

			$idarquivotermoautorizacaopasseio = $this->getArquivo('idarquivotermoautorizacaopasseio');
						if ($idarquivotermoautorizacaopasseio!=0) $dados['idarquivotermoautorizacaopasseio'] = $idarquivotermoautorizacaopasseio; 

			$idarquivotermodesistencia = $this->getArquivo('idarquivotermodesistencia');
						if ($idarquivotermodesistencia!=0) $dados['idarquivotermodesistencia'] = $idarquivotermodesistencia; 
						
			$dados["apoio"] = $apoio;
			$dados["necessidades"] = $necessidades;
			$dados["descricoes"] = $descricoes;
			$dados["transporte"] = $transporte;
			$dados["hipotesedeescrita"] = $hipotesedeescrita;
			$dados["transporteopcoes"] = $transporteopcoes;
			$dados["tipodeficiencia"] = $tipodeficiencia;
			$dados["atendimentoespecializado"] = $atendimentoespecializado;
			$dados["atendimentoespecializadosimnao"] = $atendimentoespecializadosimnao;
			$dados["atendimentoespecializadoObs"] = $atendimentoespecializadoObs;
			$dados["observacoes"] = $observacoes;
			$dados["responsavel"] = $responsavel;
			$dados["medicacao"] = $medicacao;
			$dados["qualmedicacao"] = $qualmedicacao;
			$dados["horarioMedicacao"] = $horarioMedicacao;
			$dados["data"] = date("Y-m-d", $data);
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

				$materias = array();
				$materias['segundafeira'] = $segundafeira;
				$materias['tercafeira'] = $tercafeira;
				$materias['quartafeira'] = $quartafeira;
				$materias['quintafeira'] = $quintafeira; 
				$materias['sextafeira'] = $sextafeira; 

				$materias['excluido'] = 'nao';
				$materias['logusuario'] = Usuarios::getUsuario('id');
				$materias['logdata'] = date('Y-m-d G:i:s');

				$dados['materias'] = $materias;	
				
			$row = $aeematriculas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), $e, 'E');
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
	
	public function obterdadosalunoAction() {
		$this->_helper->layout->disableLayout();     
		$id = (int)$this->getRequest()->getPost('idaluno');
	 
		$rows = Escolasalunos::getEscolaalunoByIdHelper($id);

		$endereco = Enderecos::getEnderecoById($rows['idendereco']);
  
		$rows['endereco'] = $endereco ;

		echo json_encode($rows);
		die();
	
	}

	public function relatorioaeematriculasAction(){   
         
		$id = (int)$this->_request->getParam("id");  
		
		$this->view->aeealuno = Aeematriculas::getAeematriculaByIdHelper($id);

		$this->view->escolaaluno = Escolasalunos::getEscolaalunoByIdHelper($this->view->aeealuno['idaluno']);

		$this->view->enderecoaluno = Enderecos::getEnderecoById($this->view->escolaaluno['idendereco']);
		 
		$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();
		$local = 'Santa Isabel';

		$localData = $local.' , '.$data;
 
		$this->view->localdata = $localData;
 

		$db = Zend_Registry::get('db');

		 // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('e');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Matrículas Aee');
        // $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 7);

        // add a page        
        $pdf->AddPage();
        
        $html = $this->view->render('aeematriculas/pdf/relatorioaeematriculas.phtml');
        $image_file = "public/admin/imagens/logosantaisabel.jpg";

        $pdf->writeHTML($html, true, 0, true, 0);

        $filename = 'produtos_estoque.pdf';
        $pdf->Output($filename, 'I');
        
        Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Solicitou a impressão do relatório.");
        
        die();
        return true;
    	
    }
}