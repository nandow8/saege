<?php

/**
 * Controle da classe atividadescomplementares do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_AtividadescomplementaresController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Coordenacaofrequenciaatividadescomplementar
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("atividadescomplementaresCord", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Atividadescomplementares();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Atividade complementar excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="atividadescomplementares") $objs = new Atividadescomplementares();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'atividadescomplementares', 'name' => 'Atividades Complementares'),
			array('url' => null,'name' => 'Visualizar Atividade Complementar')
		);
		
		$id = (int)$this->_request->getParam("id");
		$atividadescomplementares = new Atividadescomplementares();
		$atividadescomplementares = $atividadescomplementares->getAtividadescomplementaresById($id, array());
		
		if (!$atividadescomplementares) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $atividadescomplementares;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Atividadescomplementares')
		);
		
		$ns = new Zend_Session_Namespace('default_contas');
		$atividadescomplementares = new Atividadescomplementares();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ($this->view->post_var["idtipoatividade"]!="") $queries["idtipoatividade"] = $this->view->post_var["idtipoatividade"];
    		if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
    		if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];

			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $atividadescomplementares->getAtividadescomplementares($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $atividadescomplementares->getAtividadescomplementares($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de atividadescomplementares
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'atividadescomplementares', 'name' => 'Atividades complementares'),
			array('url' => null,'name' => 'Editar Atividade complementar')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$atividadescomplementares = new Atividadescomplementares();
		$atividadescomplementares = $atividadescomplementares->getAtividadescomplementaresById($id);
		
		if (!$atividadescomplementares) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $atividadescomplementares;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($atividadescomplementares);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Atividade complementar editada com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de atividadescomplementares 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'atividadescomplementares', 'name' => 'Atividades complementares'),
			array('url' => null,'name' => 'Adicionar Atividade Complementar')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Atividade complementar adicionada com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idtreinamento
     */    
    private function preForm($idatividadescomplementares = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_coordenacaoatividadesgrequencia = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idtipoatividade = (int)trim($this->getRequest()->getPost("idtipoatividade"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
		$datafim = Mn_Util::stringToTime($this->getRequest()->getPost("datafim"));
		$horainicio = trim($this->getRequest()->getPost("horainicio"));
		$horafim = trim($this->getRequest()->getPost("horafim"));
		$titulo = trim($this->getRequest()->getPost("titulo"));
		$observacoes = trim($this->getRequest()->getPost("observacoes"));
		$diasemana = $this->getRequest()->getPost("diassemana");
		$qtdvagas = $this->getRequest()->getPost("qtdvagas");
		$status = trim($this->getRequest()->getPost("status1"));
		
		$erros = array();
		
		if (0==$idtipoatividade) array_push($erros, "Informe a Atividade.");
		if (0==$idescola) array_push($erros, "Informe a Escola.");
		if (""==$datainicio) array_push($erros, "Informe a Data de Início.");
		if (""==$status) array_push($erros, "Informe a Status.");

		$atividadescomplementares = new Atividadescomplementares();		

		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idtipoatividade"] = $idtipoatividade;
			$dados["idescola"] = $idescola;
			$dados["titulo"] = $titulo;
			$dados["observacoes"] = $observacoes;
			$dados["datainicio"] = date("Y-m-d", $datainicio);
			$dados["datafim"] = date("Y-m-d", $datafim);
			$dados["horainicio"] = $horainicio;
			$dados["horafim"] = $horafim;
			$dados["diasemana"] = implode(',', $diasemana);
			$dados["qtdvagas"] = $qtdvagas;
			$dados["status"] = $status;

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $atividadescomplementares->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }

    public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		$queries['idescola'] = $idescola;
		
		$rows = new Escolasalunos();
		$this->view->rows = $rows->getEscolasalunos($queries);

		if(((int)$idescola <= 0)) $this->view->rows = null;		
	}  
}