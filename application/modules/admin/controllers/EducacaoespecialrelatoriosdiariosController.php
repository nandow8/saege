<?php

/**
 * Controle da classe educacaoespecialrelatoriosdiarios do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EducacaoespecialrelatoriosdiariosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Educacaoespecialrelatoriodiario
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("educacaoespecialrelatoriosdiarios", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Educacaoespecialrelatoriosdiarios();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Relatório Diário excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="educacaoespecialrelatoriosdiarios") $objs = new Educacaoespecialrelatoriosdiarios();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialrelatoriosdiarios', 'name' => 'Relatórios Diários'),
			array('url' => null,'name' => 'Visualizar Relatório Diário')
		);
		
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialrelatoriosdiarios = new Educacaoespecialrelatoriosdiarios();
		$educacaoespecialrelatoriodiario = $educacaoespecialrelatoriosdiarios->getEducacaoespecialrelatoriodiarioById($id, array());
		
		if (!$educacaoespecialrelatoriodiario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialrelatoriodiario;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Relatórios Diários')
		);
		
		$ns = new Zend_Session_Namespace('default_educacaoespecialrelatoriosdiarios');
		$educacaoespecialrelatoriosdiarios = new Educacaoespecialrelatoriosdiarios();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
			if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
				
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $educacaoespecialrelatoriosdiarios->getEducacaoespecialrelatoriosdiarios($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $educacaoespecialrelatoriosdiarios->getEducacaoespecialrelatoriosdiarios($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de educacaoespecialrelatoriosdiarios
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialrelatoriosdiarios', 'name' => 'Relatórios Diários'),
			array('url' => null,'name' => 'Editar Relatório Diário')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialrelatoriosdiarios = new Educacaoespecialrelatoriosdiarios();
		$educacaoespecialrelatoriodiario = $educacaoespecialrelatoriosdiarios->getEducacaoespecialrelatoriodiarioById($id);
		
		if (!$educacaoespecialrelatoriodiario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialrelatoriodiario;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($educacaoespecialrelatoriodiario);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Relatório Diário editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
		$this->view->editar = true;
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de educacaoespecialrelatoriosdiarios 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialrelatoriosdiarios', 'name' => 'Relatórios Diários'),
			array('url' => null,'name' => 'Adicionar Relatório Diário')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Relatório Diário adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $ideducacaoespecialrelatoriodiario
     */    
    private function preForm($ideducacaoespecialrelatoriodiario = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_educacaoespecialrelatoriodiario = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idaluno = (int)$this->getRequest()->getPost("idaluno");
		// $data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		// $titulo = trim($this->getRequest()->getPost("titulo"));
		// $descricoes = trim($this->getRequest()->getPost("descricoes"));
		$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
		$status = trim($this->getRequest()->getPost("status1"));

		// ====== Lista de matérias (salva em outra tabela)
			$datas = $this->getRequest()->getPost("datas");
			$titulos = $this->getRequest()->getPost("titulos");
			$evolucoes = $this->getRequest()->getPost("evolucoes"); 
		// =======
		
		
		$erros = array();
		
		// if (""==$data) array_push($erros, "Informe a Data.");
		// if (""==$titulo) array_push($erros, "Informe a Título.");
		// if (""==$status) array_push($erros, "Informe a Status.");

		
		$educacaoespecialrelatoriosdiarios = new Educacaoespecialrelatoriosdiarios();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados['idaluno'] = $idaluno; 
			$dados["data"] = date("Y-m-d", $data);
			$dados["titulo"] = $titulo;
			$dados["descricoes"] = $descricoes;

			$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
				$materias = array();
				$materias['datas'] = $datas;
				$materias['titulos'] = $titulos;
				$materias['evolucoes'] = $evolucoes;
				  
				$materias['excluido'] = 'nao';
				$materias['logusuario'] = Usuarios::getUsuario('id');
				$materias['logdata'] = date('Y-m-d G:i:s');

				$dados['materias'] = $materias;	

			$row = $educacaoespecialrelatoriosdiarios->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	}
	
	public function obterprofessoresdosalunosAction() {
		$this->_helper->layout->disableLayout();     
		$id = (int)$this->getRequest()->getPost('idaluno');
	 
		$rows['aee'] = Aeematriculas::getAeematriculaByAlunoIdHelper($id);

		$rows['nomealuno'] = Escolasalunos::getEscolaalunoByIdHelper($rows['aee']['id'])['nomerazao']; 
		$rows['professor'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($rows['aee']['idprofessor']);
		$rows['professorAee'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($rows['aee']['idprofessor2']);
		$rows['escola'] = Escolas::getEscolaByIdHelper($rows['aee']['idescola'])['escola'];

		echo json_encode($rows);
		die();
	
	}
    
}