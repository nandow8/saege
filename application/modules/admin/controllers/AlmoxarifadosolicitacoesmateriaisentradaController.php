<?php

class MYPDF extends TCPDF {

	//Page header
	public function Header(){
		$image_file = "public/admin/imagens/logosantaisabel.jpg";
		$this->Image($image_file, 45, 5, 20, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', '', 5);
        $this->SetXY(43, 26);
        $this->Cell(0, 0, 'Paraíso da Grande São Paulo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
       
        // cabeçalho Endereço 
        $this->SetFont('helvetica', '', 12);
        $this->SetXY(124, 6);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Municipio de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', 'B', 10);
        $this->SetXY(120, 10);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Secretaria Municipal de Educação', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(115, 14);
        $this->SetTextColor(130, 130, 130);
		$this->Cell(0, 0, 'Tel. 4656-2440 e-mail: sec.educacao@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 8);
        $this->SetXY(115, 18);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297-Centro - CEP: 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');


        //informações Saege
        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

       	$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();

        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,18);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: '.$data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 10);
        $this->SetXY(40,21);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        	

	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
        $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Position at 15 mm from bottom
		$this->SetXY(150,-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

/**
 * Controle da classe almoxarifadosolicitacoesmateriaisentrada do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_AlmoxarifadosolicitacoesmateriaisentradaController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Almoxarifadosolicitacaomaterialentrada
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("almoxarifadosolicitacoesmateriaisentrada", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		$codigosolicitacao = $this->_request->getPost("codigosolicitacao"); 
		 
		$rows = new Almoxarifadosolicitacoesmateriaisentrada();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			
			$rows->save($row); 
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Material de Entrada excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="almoxarifadosolicitacoesmateriaisentrada") $objs = new Almoxarifadosolicitacoesmateriaisentrada();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosolicitacoesmateriaisentrada', 'name' => 'Solicitações de Materias de Entrada'),
			array('url' => null,'name' => 'Visualizar Solicitação de Material de Entrada')
		);
		
		$id = (int)$this->_request->getParam("id");
		$almoxarifadosolicitacoesmateriaisentrada = new Almoxarifadosolicitacoesmateriaisentrada();
		$almoxarifadosolicitacaomaterialentrada = $almoxarifadosolicitacoesmateriaisentrada->getAlmoxarifadosolicitacaomaterialentradaById($id, array());
		
		if (!$almoxarifadosolicitacaomaterialentrada) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $almoxarifadosolicitacaomaterialentrada;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Solicitações de Materias de Entrada')
		);
		
		$ns = new Zend_Session_Namespace('default_almoxarifadosolicitacoesmateriaisentrada');
		$almoxarifadosolicitacoesmateriaisentrada = new Almoxarifadosolicitacoesmateriaisentrada();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		// foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if (isset($this->view->post_var["idescola"])) $queries["idescola"] = $this->view->post_var["idescola"];
			if (isset($this->view->post_var["idperfilbusca"])) $queries["idperfilbusca"] = $this->view->post_var["idperfilbusca"];
			if (isset($this->view->post_var["idusuario"])) $queries["idusuario"] = $this->view->post_var["idusuario"];
			if (isset($this->view->post_var["status1"])) $queries["status"] = $this->view->post_var["status1"];
    		
    		// if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$idperfil = Usuarios::getUsuario();
		$buscaidescola = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idescola'];
		
		if($idperfil['idperfil'] != 32 AND $idperfil['idperfil'] != 25 AND $idperfil['idperfil'] != 30) 
			$queries['idescola'] = $buscaidescola; 
		 
		if( $buscaidescola == 0 AND $idperfil['idperfil'] != 32 AND $idperfil['idperfil'] != 25 AND $idperfil['idperfil'] != 30){
			$queries['idperfil'] = $idperfil['idperfil'];
			$queries['idescola'] = "";
		} 


		$queries['total'] = true;
		$totalRegistros = $almoxarifadosolicitacoesmateriaisentrada->getAlmoxarifadosolicitacoesmateriaisentrada($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $almoxarifadosolicitacoesmateriaisentrada->getAlmoxarifadosolicitacoesmateriaisentrada($queries, $paginaAtual, $maxpp);	
	}

	/**
     * Exibe produtos disponiveis no estoque
     */
	public function estoquedisponivelAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Produtos disponiveis no estoque')
		);
		
		$ns = new Zend_Session_Namespace('default_almoxarifadosolicitacoesmateriaisentrada');
		$almoxarifadoprodutos = new Almoxarifadoprodutossaidas();
		$queries = array();	
		//PESQUISA
		if ($this->getRequest()->isPost()) {
			$ns->pesquisa = serialize($_POST);
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName() . '/estoquedisponivel');
			die();	
		}

		if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);

		if (isset($this->view->post_var)) {
			// foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);

			if (isset($this->view->post_var["idescola"])) $queries["idescola"] = $this->view->post_var["idescola"];
			if (isset($this->view->post_var["idperfilbusca"])) $queries["idperfilbusca"] = $this->view->post_var["idperfilbusca"];
			if (isset($this->view->post_var["old_unidademedida"])) $queries["old_unidademedida"] = $this->view->post_var["old_unidademedida"];
			if (isset($this->view->post_var["idcategoria"])) $queries["idcategoria"] = $this->view->post_var["idcategoria"];
			if (isset($this->view->post_var["produto"])) $queries["produto"] = $this->view->post_var["produto"];
			// if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
			if (isset($this->view->post_var["quantidade"])) $queries["quantidade"] = $this->view->post_var["quantidade"];
			// if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];

			// if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
		}	

		$buscaidescola = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idescola'];   // departamento não pode ter idescola(sempre 0)
		
		$idperfil = Usuarios::getUsuario();
		if($idperfil['idperfil'] != 32 AND $idperfil['idperfil'] != 25 AND $idperfil['idperfil'] != 30) 
			$queries['idescola'] = $buscaidescola; 
		 
		if($buscaidescola == 0 AND $idperfil['idperfil'] != 32 AND $idperfil['idperfil'] != 25 AND $idperfil['idperfil'] != 30){
			$queries['idperfil'] = $idperfil['idperfil'];
			$queries['idescola'] = null;
		}
		
		//PAGINACAO
		$maxpp = 200;
		
		$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1; 
		
		$queries['total'] = true;
		$totalRegistros = $almoxarifadoprodutos->getAlmoxarifadoprodutossaidasHelper($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $almoxarifadoprodutos->getAlmoxarifadoprodutossaidasHelper($queries, $paginaAtual, $maxpp);		
	}
	
	/**
	 * 
	 * Action de edição de almoxarifadosolicitacoesmateriaisentrada
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosolicitacoesmateriaisentrada', 'name' => 'Solicitações de Materias de Entrada'),
			array('url' => null,'name' => 'Editar Solicitação de Material de Entrada')
		);	
			 
		$id = (int)$this->_request->getParam("id"); 
        $idusuario = (int)$this->_request->getParam("idusuario"); 
        $codigosolicitacao = (int)$this->_request->getParam("codigosolicitacao"); 
        $almoxarifadosolicitacoes = new Almoxarifadosolicitacoesentradalistadeprodutos();
        $almoxarifadosolicitacaoprodutos = $almoxarifadosolicitacoes->getAlmoxarifadosolicitacoesentradalistadeprodutosById(
			$idusuario, array(
				'idusuario' => $idusuario,
                'codigosolicitacao' => $codigosolicitacao
                )
            ); 
        
        $entrada = new Almoxarifadosolicitacoesmateriaisentrada();
        $entradaproduto = $entrada->getAlmoxarifadosolicitacoesmateriaisentrada(array('id' => $id));
            
            if (!$almoxarifadosolicitacaoprodutos) 
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            
            $this->view->post_var = $almoxarifadosolicitacaoprodutos;
            
            array_push( $this->view->post_var , $entradaproduto);
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($almoxarifadosolicitacaoprodutos);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Solicitação editada com sucesso.";
            
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }   

        return true; 		
    }  		
	
	/**
	 * 
	 * Action de adição de almoxarifadosolicitacoesmateriaisentrada 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosolicitacoesmateriaisentrada', 'name' => 'Solicitações de Materias de Entrada'),
			array('url' => null,'name' => 'Adicionar Solicitação de Material de Entrada')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Material de Entrada adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idalmoxarifadosolicitacaomaterialentrada
     */    
    private function preForm($idalmoxarifadosolicitacaomaterialentrada = 0) {
	}    
	
	public function findprodutosAction() {
        $this->_helper->layout->disableLayout();

        $array = array();

        $idproduto = (int)$this->getRequest()->getPost('idproduto');
        $codigoproduto = strip_tags(trim($this->getRequest()->getPost('codigoproduto')));
        $categoria = (int)$this->getRequest()->getPost('categoria');
        $unidademedida = (int)$this->getRequest()->getPost('unidademedida');
        
        // $idlocalestoque = (int)$this->getRequest()->getPost('idlocalestoque');
        $idlocal = (int)$this->getRequest()->getPost('idlocal');
        $idsolicitante = (int)$this->getRequest()->getPost('idsolicitante');
        $produto = strip_tags(trim($this->getRequest()->getPost('produto')));
        $descricao = strip_tags(trim($this->getRequest()->getPost('descricao')));

        if ($idproduto <> 0 && !is_null($idproduto)) $array['id'] = $idproduto;
        if ($codigoproduto != '' && !is_null($codigoproduto)) $array['codigoproduto'] = $codigoproduto;
        if ($produto != '' && !is_null($produto)) $array['produto'] = $produto;
        if ($descricao != '' && !is_null($descricao)) $array['descricoes'] = $descricao;
        if ($categoria <> 0 && !is_null($categoria)) $array['idcategoria'] = $categoria;

        $produtos = new Almoxarifadoprodutos();

        if (sizeof($array) == 0) {
            $this->view->rows = array("erro" => "erro");
        } else {
            if ($unidademedida <> 0 && !is_null($unidademedida)) $array['idunidademedida'] = $unidademedida;
            $this->view->rows = $produtos->getAlmoxarifadoprodutos($array);
        }
    }
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_almoxarifadosolicitacaomaterialentrada = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);

		$id = (int)$this->getRequest()->getPost("id"); 
		$data = MN_Util::stringToTime($this->getRequest()->getPost("data"));
		$observacao = trim($this->getRequest()->getPost("observacao"));
		

		$ids = $this->getRequest()->getPost('id_selecionado');
        $qtds = $this->getRequest()->getPost('qtd_selecionada'); 
        $idlocal = $this->getRequest()->getPost('idlocal');
        $idperfilform = $this->getRequest()->getPost('idperfilform');
        $idusuario = $this->getRequest()->getPost('idusuario');
		$idescola = $this->getRequest()->getPost('idescola');
		if($idescola == null) $idescola = null;
        $idsolicitante = $this->getRequest()->getPost('idusuario');
        $idsolicitanteescola = $this->getRequest()->getPost('idsolicitanteescola');
        
        $id_produtos = $this->getRequest()->getPost('id_produtos'); 
        $produto = $this->getRequest()->getPost('produto');
        $qtd_selecionada = $this->getRequest()->getPost('qtd_selecionada');
        $qtd_liberado = $this->getRequest()->getPost('qtd_liberado');
        $unidademedida = $this->getRequest()->getPost('unidademedida');
        $codigoproduto = $this->getRequest()->getPost('codigoproduto');
        $motivoliberacao = $this->getRequest()->getPost('motivoliberacao');
        $codigosolicitacaoEdit = $this->getRequest()->getPost('codigosolicitacao');
		$itens = array_combine($ids, $qtds);
		$erros = array(); 

		$almoxarifadosolicitacoesmateriaisentrada = new Almoxarifadosolicitacoesmateriaisentrada();
		$almoxarifadosolicitacoesentradalistadeprodutos = new Almoxarifadosolicitacoesentradalistadeprodutos();
		$almoxarifadoprodutossaidas = new Almoxarifadoprodutossaidas();

		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dadosProdutos = array();
			$dadosSalvaEntradaParaSaidaProdutos = array();
			$dados['id'] = $id;

			$idperfil = Usuarios::getUsuario('idperfil');

			if($idperfil != 32 OR $idperfil != 25){
				$dados['idusuario'] = $idusuario;
				$dados['idperfil'] = $idperfilform;
				if(empty(Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper( $idsolicitante)['idescola']))
                    $dados['idperfilbusca'] = $idperfilform; 
			}
				
// echo '<pre>'; print_r($dados); die;
			$dados['data'] =  date("Y-m-d", $data);
			$dados["observacao"] = $observacao;
			$dados["status"] = 'Ativo';
			$dados['idlocal'] = $idusuario; 
            $dados['idescola'] = $idescola; 

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			$codigosolicitacao =  date('sidmy') . $idusuario;
			
			$dados['codigosolicitacao'] = $codigosolicitacao;
  			
			$dadosProdutos['tiposolicitacao'] = 'entrada';

			$db = Zend_Registry::get('db');
			for ($i=0; $i < count( $itens ) ;$i++) { 
				$dados['codigosolicitacao'] = $codigosolicitacao;
				$dadosProdutos['id'] = $id_produtos[$i];
				$dadosProdutos['idfuncionario'] = $dados['idusuario'];
				$dadosProdutos['idescola'] = $dados['idescola'];
				$dadosProdutos['idperfil'] = $idperfilform;
				$dadosProdutos['produto'] = $produto[$i];
				$dadosProdutos['unidademedida'] = $unidademedida[$i];
				$dadosProdutos['qtd_selecionada'] = $qtd_selecionada[$i];
				$dadosProdutos['qtd_liberado'] = $qtd_selecionada[$i];
				$dadosProdutos['codigoproduto'] = $codigoproduto[$i];
				$dadosProdutos['codigosolicitacao'] = $codigosolicitacao;
			
				$dadosProdutos['excluido'] = 'nao';
				$dadosProdutos['logusuario'] = $this->_usuario['id'];
				$dadosProdutos['logdata'] = date('Y-m-d G:i:s');

				$row = $almoxarifadosolicitacoesentradalistadeprodutos->save($dadosProdutos);

				$strsql = "select id,quantidade from almoxarifadoprodutossaidas where idcodigoalmoxarifadoproduto = '$dadosProdutos[codigoproduto]' AND idescola = '$dados[idescola]'";
					
				$idprodutosaida = $db->fetchRow($strsql);
				
				if($idprodutosaida){
					$dadosSalvaEntradaParaSaidaProdutos['id'] = $idprodutosaida['id'];
					$dadosSalvaEntradaParaSaidaProdutos['quantidade'] = $idprodutosaida['quantidade'] + $qtd_selecionada[$i];	
				}else{
					$dadosSalvaEntradaParaSaidaProdutos['id'] = null;
					$dadosSalvaEntradaParaSaidaProdutos['quantidade'] = $qtd_selecionada[$i];
				}
				
				$dadosSalvaEntradaParaSaidaProdutos['ultimaquantidadeinserida'] = $qtd_selecionada[$i];
				$dadosSalvaEntradaParaSaidaProdutos['idescola'] = $dados['idescola'];
				$dadosSalvaEntradaParaSaidaProdutos['idperfil'] = $idperfilform;
				$dadosSalvaEntradaParaSaidaProdutos['idperfilbusca'] = $idperfilform;
				$dadosSalvaEntradaParaSaidaProdutos['idcodigoalmoxarifadoproduto'] = $codigoproduto[$i];
				$dadosSalvaEntradaParaSaidaProdutos['codigosolicitacao'] = $dadosProdutos['codigosolicitacao'];
				$dadosSalvaEntradaParaSaidaProdutos["status"] = 'Ativo';
				$dadosSalvaEntradaParaSaidaProdutos['excluido'] = 'nao';
				$dadosSalvaEntradaParaSaidaProdutos['logusuario'] = $this->_usuario['id'];
				$dadosSalvaEntradaParaSaidaProdutos['logdata'] = date('Y-m-d G:i:s');
			
				$row = $almoxarifadoprodutossaidas->save($dadosSalvaEntradaParaSaidaProdutos);
			}   
			
			for ($i=0; $i < count( $id_produtos ) ;$i++) { 
				$dadosProdutos['id'] = $id_produtos[$i];
				$dadosProdutos['idfuncionario'] = $dados['idusuario'];
				$dadosProdutos['produto'] = $produto[$i];
				$dadosProdutos['unidademedida'] = $unidademedida[$i];
				$dadosProdutos['qtd_selecionada'] = $qtd_selecionada[$i];
				$dadosProdutos['qtd_liberado'] = $qtd_selecionada[$i];
				$dadosProdutos['codigoproduto'] = $codigoproduto[$i];
				$dadosProdutos['codigosolicitacao'] = $codigosolicitacao;
				 
				$dadosProdutos['excluido'] = 'nao';
				$dadosProdutos['logusuario'] = $this->_usuario['id'];
				$dadosProdutos['logdata'] = date('Y-m-d G:i:s');
				
				$row = $almoxarifadosolicitacoesentradalistadeprodutos->save($dadosProdutos);

				$strsql = "select id,quantidade,ultimaquantidadeinserida,quantidade from almoxarifadoprodutossaidas where idcodigoalmoxarifadoproduto = '$dadosProdutos[codigoproduto]' AND idescola = '$dados[idescola]'";
					
				$idprodutosaida = $db->fetchRow($strsql);
				
				if($idprodutosaida){
					$dadosSalvaEntradaParaSaidaProdutos['id'] = $idprodutosaida['id'];
					$quantidadeCorrecao = $idprodutosaida['quantidade'] - $idprodutosaida['ultimaquantidadeinserida'];
					$dadosSalvaEntradaParaSaidaProdutos['quantidade'] = $quantidadeCorrecao + $qtd_selecionada[$i];	
				}
				
				$dadosSalvaEntradaParaSaidaProdutos['ultimaquantidadeinserida'] = $qtd_selecionada[$i];
				$dadosSalvaEntradaParaSaidaProdutos['idcodigoalmoxarifadoproduto'] = $codigoproduto[$i];
				$dadosSalvaEntradaParaSaidaProdutos["status"] = 'Ativo';
				$dadosSalvaEntradaParaSaidaProdutos['excluido'] = 'nao';
				$dadosSalvaEntradaParaSaidaProdutos['logusuario'] = $this->_usuario['id'];
				$dadosSalvaEntradaParaSaidaProdutos['logdata'] = date('Y-m-d G:i:s');
				
				$row = $almoxarifadoprodutossaidas->save($dadosSalvaEntradaParaSaidaProdutos);
			}  

			$row = $almoxarifadosolicitacoesmateriaisentrada->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	} 
	
	public function produtosdisponiveisAction(){  
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Solicitações de Materias de Entrada')
		);
		
		$ns = new Zend_Session_Namespace('default_almoxarifadosolicitacoesmateriaisentrada');
		
		$queries = array();	
		if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
		if (isset($this->view->post_var)) { 
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["old_unidademedida"]!="") $queries["old_unidademedida"] = $this->view->post_var["old_unidademedida"];
			if ($this->view->post_var["idcategoria"]!="") $queries["idcategoria"] = $this->view->post_var["idcategoria"];
			if ($this->view->post_var["produto"]!="") $queries["produto"] = $this->view->post_var["produto"];
			if ($this->view->post_var["quantidade"]!="") $queries["quantidade"] = $this->view->post_var["quantidade"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}	

		$almoxarifadoprodutos = new Almoxarifadoprodutossaidas();
		$_row = $this->view->rows = $almoxarifadoprodutos->getAlmoxarifadoprodutossaidasHelper($queries);

		$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();
		$local = 'Santa Isabel';

		$localData = $local.' , '.$data;

		$this->view->rows = $_row; 
		$this->view->localdata = $localData;

		$this->preForm();

		$db = Zend_Registry::get('db');

		 // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('l');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Produtos disponiveis no estoque');
        // $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 7);

        // add a page        
        $pdf->AddPage();
        
        $html = $this->view->render('almoxarifadosolicitacoesmateriaisentrada/pdf/relatorioprodutosdisponiveisestoque.phtml');
        $image_file = "public/admin/imagens/logosantaisabel.jpg";

        $pdf->writeHTML($html, true, 0, true, 0);

        $filename = 'produtos_estoque.pdf';
        $pdf->Output($filename, 'I');
        
        die();
        return true;
    	
    }
}