<?php

/**
 * Controle da classe eventos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EventosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Evento
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("eventos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Eventos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$eventossecretarias = new Eventossecretarias();
			$_evento = $eventossecretarias->getEventosecretariaByIdevento($id);

			$id_evento = 0;
			if(isset($_evento['id'])){
				$id_evento = $_evento['id'];
			}
			$_evento['excluido'] = 'sim';
			$_evento['logusuario'] = $this->_usuario['id'];
			$_evento['logdata'] = date('Y-m-d G:i:s');			
			
			$eventossecretarias->save($_evento);

			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Evento excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="eventos") $objs = new Eventos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			$eventossecretarias = new Eventossecretarias();
			$_evento = $eventossecretarias->getEventosecretariaByIdevento($id);

			$id_evento = 0;
			if(isset($_evento['id'])){
				$id_evento = $_evento['id'];
			}
			$_evento['status'] = ($_evento['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$_evento['logusuario'] = $this->_usuario['id'];
			$_evento['logdata'] = date('Y-m-d G:i:s');			
			
			$eventossecretarias->save($_evento);

			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'eventos', 'name' => 'Eventos'),
			array('url' => null,'name' => 'Visualizar Evento')
		);
		
		$id = (int)$this->_request->getParam("id");
		$eventos = new Eventos();
		$evento = $eventos->getEventoById($id, array());
		
		if (!$evento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $evento;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Eventos')
		);
		
		$ns = new Zend_Session_Namespace('default_eventos');
		$eventos = new Eventos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ((isset($this->view->post_var["idescola"])) && ($this->view->post_var["idescola"]!="")) $queries["idescola"] = $this->view->post_var["idescola"];
if ((isset($this->view->post_var["idusuario"])) && ($this->view->post_var["idusuario"]!="")) $queries["idusuario"] = $this->view->post_var["idusuario"];
if ((isset($this->view->post_var["titulo"])) && ($this->view->post_var["titulo"]!="")) $queries["titulo"] = $this->view->post_var["titulo"];
if ((isset($this->view->post_var["datainicio_i"])) && ($this->view->post_var["datainicio_i"]!="")) $queries["datainicio_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_i"]));
if ((isset($this->view->post_var["datainicio_f"])) && ($this->view->post_var["datainicio_f"]!="")) $queries["datainicio_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_f"]));
if ((isset($this->view->post_var["datafim_i"])) && ($this->view->post_var["datafim_i"]!="")) $queries["datafim_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datafim_i"]));
if ((isset($this->view->post_var["datafim_f"])) && ($this->view->post_var["datafim_f"]!="")) $queries["datafim_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datafim_f"]));
if ((isset($this->view->post_var["descricoes"])) && ($this->view->post_var["descricoes"]!="")) $queries["descricoes"] = $this->view->post_var["descricoes"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		//if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $eventos->getEventos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $eventos->getEventos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de eventos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'eventos', 'name' => 'Eventos'),
			array('url' => null,'name' => 'Editar Evento')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$eventos = new Eventos();
		$evento = $eventos->getEventoById($id);
		
		if (!$evento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $evento;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($evento);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Evento editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de eventos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'eventos', 'name' => 'Eventos'),
			array('url' => null,'name' => 'Adicionar Evento')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Evento adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idevento
     */    
    private function preForm($idevento = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_evento = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		
		$idperfil = (int)Usuarios::getUsuario('idperfil');
		$idescola = (int)Usuarios::getUsuario('idescola');
		$idusuario = Usuarios::getUsuario('id');

$titulo = trim($this->getRequest()->getPost("titulo"));
$datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
$datafim = Mn_Util::stringToTime($this->getRequest()->getPost("datafim"));
$horainicio = trim($this->getRequest()->getPost("horainicio"));
$horafimevento = trim($this->getRequest()->getPost("horafimevento"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
if (0==$idusuario) array_push($erros, "Informe a Usuários.");
if (""==$titulo) array_push($erros, "Informe a Título.");
if (""==$datainicio) array_push($erros, "Informe a Data início.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$eventos = new Eventos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
$dados["idperfil"] = $idperfil;
$dados["idusuario"] = $idusuario;
$dados["titulo"] = $titulo;
$dados["datainicio"] = date("Y-m-d", $datainicio);
$dados["datafim"] = date("Y-m-d", $datafim);
$dados["horainicio"] = $horainicio;
$dados["horafimevento"] = $horafimevento;
$dados["descricoes"] = $descricoes;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $eventos->save($dados);
			
			$eventossecretarias = new Eventossecretarias();
			$_evento = $eventossecretarias->getEventosecretariaByIdevento($row['id']);

			$id_evento = 0;
			if(isset($_evento['id'])){
				$id_evento = $_evento['id'];
			}

			$dados = array();
			$dados['id'] = $id_evento;
			$dados['idevento'] = $row['id'];
			$dados["idsecretaria"] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
			$dados["idusuariointerno"] = $row['idusuario'];
			$dados["idusuario"] = $row['idusuario'];
			$dados["data"] = date("Y-m-d", $datainicio);
			$dados["datainicio"] = date("Y-m-d", $datainicio);
			$dados["horainicio"] = $horainicio;
			$dados["datafim"] = date("Y-m-d", $datafim);
			$dados["horafim"] = $horafimevento;
			$dados["hora"] = $horainicio;
			$dados["titulo"] = $titulo;
			$dados["descricoes"] = $descricoes;
			$dados["origem"] = "Comunicação";
			$dados["status"] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			$row = $eventossecretarias->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}