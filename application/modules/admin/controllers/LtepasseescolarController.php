<?php

class Admin_LtepasseescolarController extends Zend_Controller_Action {
    
    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null; 
    
    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
        Mn_Util::blockAccess("ltepasseescolar", $this->_request->getActionName());    
        
        $this->_usuario = unserialize($loginNameSpace->usuario);
        
        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }               
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir a passe
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->_request->getPost("id");
        
        $rows = new Ltepasseescolar();
        $row = $rows->fetchRow("id=".$id);
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = Usuarios::getUsuario('id');
            $row['logdata'] = date('Y-m-d G:i:s');          
            
            $rows->save($row);
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Passe Escolar excluído com sucesso.";
            
            die("OK");
        }
        
        die("Não encontrado!"); 
    }       
    
    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");
        
        if ($op=="ltepasseescolar") $objs = new Ltepasseescolar();
        $obj = $objs->fetchRow("excluido='nao' AND id=".$id);
        if ($obj) {
            
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = Usuarios::getUsuario('id');
            $obj['logdata'] = date('Y-m-d G:i:s');
            
            $objs->save($obj);
            
            die($obj['status']);
        }
        
        die("Não encontrado!");
    }       
    
    public function changeorderxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $idfrom = (int)$this->getRequest()->getPost("from");
        $idto = (int)$this->getRequest()->getPost("to");
        $op = $this->getRequest()->getPost("op");
        
        if ($op=="change") $objs = new Ltepasseescolar();
        $from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
        $to = $objs->fetchRow("excluido='nao' AND id=".$idto);
        
        if (($from) && ($to)) {
            $from = $from->toArray();
            $to = $to->toArray();
            $ordemFrom = $from["ordem"];
            $orderTo = $to["ordem"];

            $from['ordem'] = $orderTo;
            $to['ordem'] = $ordemFrom;

            $objs->save($from);
            $objs->save($to);
        }
        
        $this->view->message = "OK"; 
        $this->render("xml");
    }   
    
    public function ordemAction() {
        $ordem = (int) $this->getRequest()->getParam('ordem', 0);
        $d = $this->getRequest()->getParam('d', 0);
        
        $rows = new Ltepasseescolar();
        $rows->swapOrdem($ordem, $d, false);

        $this->_redirect("/admin/".$this->_request->getControllerName()."/index");
        die();  
    }   
    
    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'PasseEscolar')
        );
      
        $ns = new Zend_Session_Namespace('logistica_passeescolar');
        $passeescolar = new Ltepasseescolar();
        $queries = array('fechado' => 'nao'); 

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) { 
            
            foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = $v;

            if ((isset($this->view->post_var['passe'])) && ($this->view->post_var['passe']!='')) $queries['passe'] = $this->view->post_var['passe'];
            if ((isset($this->view->post_var['cidade'])) && ($this->view->post_var['cidade']!='')) $queries['cidade'] = $this->view->post_var['cidade'];
        }       
        
        //PAGINACAO
        $maxpp = 15;
        
        $paginaAtual = (int)$this->getRequest()->getParam('p');
        if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
        if ($paginaAtual==0) $paginaAtual = 1;
        
        $queries['total'] = true;
        $totalRegistros = $passeescolar->getPeriodos($queries);
        $paginaTotal = ceil($totalRegistros/$maxpp);
        $queries['total'] = false;
        if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual<0) $paginaAtual = 0;
        
        $this->view->pagina_atual = $paginaAtual+1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;           
        
        $this->view->rows = $passeescolar->getPeriodos($queries, $paginaAtual, $maxpp);
    }
    
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'ltepasseescolar', 'name' => 'Passeescolar'),
            array('url' => null,'name' => 'Visualizar Passe Escolar')
        );
    
        $id = (int)$this->_request->getParam("id");
        $passeescolar = new Ltepasseescolar();
        $passe = $passeescolar->getEstudantesPasse($id);

        if (!$passe) $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->visualizar = true;
        $this->view->post_var = $passe;
    }
    
    /**
     * 
     * Action de edição de passe
     */ 
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'ltepasseescolar', 'name' => 'Passeescolar'),
            array('url' => null,'name' => 'Editar Passe Escolar')
        );  
                
        $id = (int)$this->_request->getParam("id");
        $passeescolar = new Ltepasseescolar();
        $periodo = $passeescolar->getPeriodosById($id);
        $estudantes = $passeescolar->getEstudantesPasseByTipo(array('idperiodo' => $periodo['id'], 'tipo' => $periodo['tipo'], 'tipotransporte' => $periodo['tipotransporte']));
        // var_dump($estudantes); die();

        if (!$periodo || !$estudantes)
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->post_var = array('periodo' => $periodo, 'estudantes' => $estudantes);
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($periodo);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "periodo editada com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }       
        return true;        
    }

    /**
     *
     *Action de lançamento de faltas
     */
    public function setfaltasAction() {
        $this->_helper->layout->disableLayout();

        $idpasse = (int)$this->getRequest()->getPost('idpasse');
        $faltas = (int)$this->getRequest()->getPost('faltas');

        $passe = new Ltepasseescolar();

        $row = $passe->save(array('id' => $idpasse, 'faltas' => $faltas));

        die($row['total']);
    }

    public function setentidadesAction() {
        $this->_helper->layout->disableLayout();

        $identidade = (int)$this->getRequest()->getPost('identidade');
        $idperiodo = (int)$this->getRequest()->getPost('idperiodo');

        $passe = new Ltepasseescolar();
        $this->view->rows = $passe->getEstudantesPasse(array('identidade' => $identidade, 'idperiodo' => $idperiodo));
    }
    
    /**
     * 
     * Action de adição de passe 
     */
    public function adicionarAction() {

        $this->view->bread_crumb = array(
            array('url' => 'ltepasseescolar', 'name' => 'Passeescolar'),
            array('url' => null,'name' => 'Adicionar Passe Escolar')
        );

        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "passe adicionado com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }

        return true;        
    }   
    
    /**
     * Atribui valores ao view
     * @param int $id
     */    
    private function preForm($id = 0) {
    }    
    
    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_registro = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);
        
        $id = (int)$this->getRequest()->getPost("id");
        $passe = strip_tags(trim($this->getRequest()->getPost("passe")));
        //$cidade = strip_tags(trim($this->getRequest()->getPost("cidade")));

        $status = strip_tags(trim($this->getRequest()->getPost("status")));

        $erros = array();
        if (""==$status) array_push($erros, 'Selecione um STATUS.');
                
        $passeescolar = new Ltepasseescolar();

        if (sizeof($erros)>0) return $erros; 
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
               
            $dados = array();           
            $dados['id'] = $id;
            $dados['passe'] = $passe;
            // $dados['cidade'] = $cidade;
            $dados['cidade'] = 'Santa Isabel';

            $dados['status'] = $status;

            $dados['excluido'] = 'nao';

            $row = $passeescolar->save($dados);
            
            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die();
        }       

        return "";      
    }
}
