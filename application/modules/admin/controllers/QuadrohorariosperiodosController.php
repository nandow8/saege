<?php

/**
 * Controle da classe quadrohorariosperiodos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_QuadrohorariosperiodosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Quadrohorariosperiodo
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("quadrohorariosperiodos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Quadrohorariosperiodos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Quadro de horário excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="quadrohorariosperiodos") $objs = new Quadrohorariosperiodos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'quadrohorariosperiodos', 'name' => 'Quadro de horários'),
			array('url' => null,'name' => 'Visualizar Quadro de horário')
		);
		
		$id = (int)$this->_request->getParam("id");
		$quadrohorarios = new Quadrohorarios();
		$quadrohorario = $quadrohorarios->getQuadrohorarioById($id, array());
		
		if (!$quadrohorario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $quadrohorario;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
		/*

		$this->view->bread_crumb = array(
			array('url' => 'quadrohorariosperiodos', 'name' => 'Quadro de horários'),
			array('url' => null,'name' => 'Visualizar Quadro de horário')
		);
		
		$id = (int)$this->_request->getParam("id");
		$quadrohorariosperiodos = new Quadrohorariosperiodos();
		$quadrohorariosperiodo = $quadrohorariosperiodos->getQuadrohorariosperiodoById($id, array());
		
		if (!$quadrohorariosperiodo) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $quadrohorariosperiodo;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;*/
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Quadro de horários')
		);
		
		$ns = new Zend_Session_Namespace('admin_quadrohorarios');
		$escolas = new Escolas();
		$queries = array();	
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
        
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	

    		foreach ($this->view->post_var as $k=>$v){
    			if(!is_array($v)) $this->view->post_var[$k] = trim($v);
    		}
    		if ($this->view->post_var['status']!='') $queries['status'] = $this->view->post_var['status'];
    		if ($this->view->post_var['chave']!='') $queries['chave'] = $this->view->post_var['chave'];
    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $escolas->getEscolas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $escolas->getEscolas($queries, $paginaAtual, $maxpp);			

		/*
		$ns = new Zend_Session_Namespace('default_quadrohorariosperiodos');
		$quadrohorariosperiodos = new Quadrohorariosperiodos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["diassemana"]!="") $queries["diassemana"] = $this->view->post_var["diassemana"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $quadrohorariosperiodos->getQuadrohorariosperiodos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $quadrohorariosperiodos->getQuadrohorariosperiodos($queries, $paginaAtual, $maxpp);
		*/	
	}
	
	/**
	 * 
	 * Action de edição de quadrohorariosperiodos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'quadrohorariosperiodos', 'name' => 'Quadro de horários'),
			array('url' => null,'name' => 'Editar Quadro de horário')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$escolas = new Escolas();
		$escola = $escolas->getEscolaById($id);
		
		if (!$escola) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		//var_dump($escola['quantidadesaulas']);die();
		$this->view->visualizar = false;
		$this->view->post_var = $escola;
		$this->view->post_var['idescola'] = $escola['id'];
                
		$this->preForm(0, $escola['id']);	
		if ($this->_request->isPost()) {
			$erros = $this->getPost($quadrohorariosperiodo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Quadro de horário editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;	
      				
		/*$this->view->bread_crumb = array(
			array('url' => 'quadrohorariosperiodos', 'name' => 'Quadro de horários'),
			array('url' => null,'name' => 'Editar Quadro de horário')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$quadrohorariosperiodos = new Quadrohorariosperiodos();
		$quadrohorariosperiodo = $quadrohorariosperiodos->getQuadrohorariosperiodoById($id);
		
		if (!$quadrohorariosperiodo) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $quadrohorariosperiodo;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($quadrohorariosperiodo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Quadro de horário editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;	*/	
    }  		
	
	/**
	 * 
	 * Action de adição de quadrohorariosperiodos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'quadrohorariosperiodos', 'name' => 'Quadro de horários'),
			array('url' => null,'name' => 'Adicionar Quadro de horário')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Quadro de horário adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }

	public function setvinculosAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getParam('idescola');
		//$idescola = 1;
		$quadrohorariosperiodos = new Quadrohorariosperiodos();

		$vinculos = new Escolasvinculos();
    	$escolasvinculos = $vinculos->getEscolasvinculos(array('idescola'=>$idescola));

    	$escola = Escolas::getEscolaByIdHelper($idescola);
    	//var_dump($escola);die();

    	$diassemanas = array();
		$diassemanas[0] = "2º Feira";
		$diassemanas[1] = "3º Feira";
		$diassemanas[2] = "4º Feira";
		$diassemanas[3] = "5º Feira";
		$diassemanas[4] = "6º Feira";
		//$diassemanas[5] = "Sábado";


		$condicao_saida = false;
		$condicao_saida_periodo = false;
		$totalaula = 0;
		$verifica_dupla = false;

		$quadrohorarios = new Quadrohorarios();

    	foreach ($escolasvinculos as $i => $row) {

			$totalaulas = 1;
			if(isset($row['quantidadesaulas'])) $totalaulas = $row['quantidadesaulas'];
			
			$qtdaulas = 0;
			if((int)$escola['quantidadesaulas'] > 0) $qtdaulas = $escola['quantidadesaulas'];
	    	$periodos = array();
	    	for ($ip=0; $ip < $qtdaulas; $ip++) { 
				$periodos[$ip] = $ip+1;
			}

			if(sizeof($periodos) <= 0) continue;

			//var_dump($row);die('OK');

			$apaga_anteriores = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('idescola'=>$idescola, 'idescolavinculo'=>$row['id']));

			foreach ($apaga_anteriores as $indicde => $row_apaga) {
				$row_apaga['excluido'] = 'sim';
				$quadrohorariosperiodos->save($row_apaga);
			}

			/*$periodos[1] = "2";
			$periodos[2] = "3";
			$periodos[3] = "4";
			$periodos[4] = "5";
			$periodos[5] = "6";*/

			$condicao_saida = false;
			$condicao_saida_periodo = false;
			$totalaula = 0;
    		$rows = $quadrohorarios->getQuadrohorarios(array('fieldsmaterias'=>true ,'idescolavinculo'=>$row['id'], 'order'=>'ORDER BY quantidadesaulas DESC, q1.idmateria ASC')); 
		
    		
			$___rows = Escolasseries::getEscolasseriesHelper(array('id'=>$row['idserie']));
			$___row = (sizeof($___rows)>0) ? 	$___rows[0] : false;	
			
			echo ($___row) ? $___row['serie'] : '--';	

			$___rows = Escolasturmas::getEscolasturmasHelper(array('id'=>$row['idturma']));
			$___row = (sizeof($___rows)>0) ? 	$___rows[0] : false;	
			
			echo ($___row) ? $___row['turma'] . '<br />' : '--';		
    		
    		//echo '<br /><br /><br /><br /><br />';	
    		foreach ($rows as $iq => $row_quadro) {
    			//var_dump($row_quadro);die('adasd');
    			echo 'idquadro: '.$row_quadro['id'].'<br/>';
    			$verifica_quantidade = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('idescola'=>$idescola, 'idmateria'=>$row_quadro['idmateria'], 'idquadrohorario'=>$row_quadro['id'],'total'=>true));
    			$verifica_quantidade = (int)$verifica_quantidade;
    			//echo '	verifica_quantidade: '.$verifica_quantidade.'<br/>';
    			//echo '	quantidadesaulas: '.$row_quadro['quantidadesaulas'].'<br/>';
    			//VERIFICA SE JA FORAM ADICIONADA TODAS AS AULAS PARA ESSA TURMA
    			if($verifica_quantidade<$row_quadro['quantidadesaulas']){
    				$totalaula = $verifica_quantidade;
    			//	echo '		totalaula: '.$totalaula.'<br/>';
    	    		foreach ($diassemanas as $ds => $diasemana) {
    	    			$condicao_saida_periodo = false;
		    			foreach ($periodos as $p => $periodo) {
		    				//1º VERIFICA SE JA POSSUI UMA AULA PARA ESSE DIA
				    		$verifica = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('diassemana'=>$diasemana, 'idescola'=>$idescola, 'periodo'=>$periodo, 'idquadrohorario'=>$row_quadro['id']));

		    		//1º VERIFICA SE JA POSSUI UMA AULA PARA ESSE DIA
	    			if(!$verifica){
	    			//	echo 'verifica<br/>';
	    			//	echo 'condicao_saida_periodo='; var_dump($condicao_saida_periodo); echo '<br />';
	    				//CONDICAO SAIDA PERIODO
	    				//CONDICAO QUE AVANCA PERIODO
	    				if (!$condicao_saida_periodo) {
    						//echo 'totalaula_quantidadesaulas<br/>';
    						$verifica_dupla = false;
							$force_unica = false;
    						$verifica_aula = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('diassemana'=>$diasemana, 'idescola'=>$idescola, 'periodo'=>$periodo, 'idescolavinculo'=>$row['id']));
    						//var_dump(($row_quadro['quantidadesaulas'] % 2));die('oko');


	    					//VERIFICAR SE E DUPLA $row_quadro['quadroperiodoaula']
	    					if($row_quadro['quadroperiodoaula']=="Dupla"){
								if(($p+1) > 5) {
									$verifica_dupla = true;
								}
								else if(isset($periodos[$p+1])){
		    						$auladupla = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('diassemana'=>$diasemana, 'idescola'=>$idescola, 'periodo'=>$periodos[$p+1], 'idescolavinculo'=>$row['id']));
		    					//	echo 'periodo='.$periodo; echo '<br />';
		    					//	echo 'periodos[p+1]='.$periodos[$p+1]; echo '<br />';
		    					//	echo 'auladupla='; var_dump($auladupla); echo '<br />';
		    						if(sizeof($auladupla)>0) $verifica_dupla = true;

	    							echo "<br>";
	    							echo "TESTA DUPLA - ".$row_quadro['id'];
	    							echo "<br>";
									echo '			diasemana: '.$diasemana.'<br/>';
								}
								else if(($row_quadro['quantidadesaulas'] % 2) == 1)
	    						{
	    							echo "<br>";
	    							echo "FORÇA DUPLA P/ UNICA - ".$row_quadro['id'];
	    							echo "<br>";
									echo '			diasemana: '.$diasemana.'<br/>';
	    							echo "QTD AULAS - ".$totalaula;
	    							echo "<br>";
	    							$row_quadro['quadroperiodoaula']=="Unica";
    								$verifica_dupla = false;
    								$force_unica = true;
	    						}
	    						else if($row_quadro['quantidadesaulas'] <= 2)
	    						{
	    							$row_quadro['quadroperiodoaula']=="Unica";
    								$verifica_dupla = false;
    								$force_unica = true;
	    						}

	    					}




    						//echo 'verifica_aula='; var_dump($verifica_aula); echo '<br />';
	    					//echo 'verifica_dupla='; var_dump($verifica_dupla); echo '<br />';
    						//VERIFICA SE JA POSSUI UMA AULA PARA ESSE PERIODO
    						if ((!$verifica_aula) && (!$verifica_dupla)) {
    							//VERIFICA SE JA TEM TODAS AS AULAS PARA ESSA MATERIA
		    					if ($totalaula<$row_quadro['quantidadesaulas']){
		    						//VERIFICA SE O PROFESSOR JÁ POSSUI UMA AULA PARA ESSE PERIODO E DIA
						    		$verifica_professsor = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('die'=>true, 'diassemana'=>$diasemana, 'idescola'=>$idescola, 'periodo'=>$periodo, 'idprofessor'=>$row_quadro['idprofessor']));
						    		echo 'verifica_professsor='; var_dump(sizeof($verifica_professsor)); echo '<br />';
									if(sizeof($verifica_professsor) <= 0){
										//VERIFICA SE A MATERIA JÁ POSSUI UMA AULA PARA ESSE PERIODO E DIA
										$verifica_materia = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('diassemana'=>$diasemana, 'idescola'=>$idescola, 'periodo'=>$periodo, 'idmateria'=>$row_quadro['idmateria']));
										echo 'verifica_materia='; var_dump(sizeof($verifica_materia)); echo '<br />';
										if(sizeof($verifica_materia) <= 0){
											//SE PASSAR GRAVA
											if(($row_quadro['quadroperiodoaula']=="Dupla")){
												echo '			DUPLA_______________________________________<br/>';
 												echo '			diasemana: '.$diasemana.'<br/>';
									    		echo '			periodo: '.$periodos[$p+1].'<br/>';
									    		echo '			idprofessor: '.$row_quadro['idprofessor'].'<br/>';
									    		echo '			idmateria: '.$row_quadro['idmateria'].'<br/><br/>';
												$dados = array();
												$dados["idescolavinculo"] = $row['id'];
												$dados["idquadrohorario"] = $row_quadro['id'];
												$dados["diassemana"] = $diasemana;
												$dados["periodo"] = $periodo;
												$dados["status"] = 'Ativo';
												$dados['excluido'] = 'nao';
												$dados['logusuario'] = $this->_usuario['id'];;
												$dados['logdata'] = date('Y-m-d G:i:s');												
												$_row = $quadrohorariosperiodos->save($dados);
												$totalaula = $totalaula+1;

												if(!$force_unica && ($totalaula < $row_quadro['quantidadesaulas']))
												{
													echo '			DUPLA NORMAL_______________________________________<br/>';
 													echo '			diasemana: '.$diasemana.'<br/>';
													$dados = array();
													$dados["idescolavinculo"] = $row['id'];
													$dados["idquadrohorario"] = $row_quadro['id'];
													$dados["diassemana"] = $diasemana;
													$dados["periodo"] = $periodos[$p+1];
													$dados["status"] = 'Ativo';
													$dados['excluido'] = 'nao';
													$dados['logusuario'] = $this->_usuario['id'];;
													$dados['logdata'] = date('Y-m-d G:i:s');												
													$_row = $quadrohorariosperiodos->save($dados);
													$totalaula = $totalaula+1;
												}
											}else{
									    		echo '			UNICA: ----<br/>';
									    		echo '			diasemana: '.$diasemana.'<br/>';
									    		echo '			periodo: '.$periodo.'<br/>';
									    		echo '			idprofessor: '.$row_quadro['idprofessor'].'<br/>';
									    		echo '			idmateria: '.$row_quadro['idmateria'].'<br/><br/>';
												$dados = array();
												$dados["idescolavinculo"] = $row['id'];
												$dados["idquadrohorario"] = $row_quadro['id'];
												$dados["diassemana"] = $diasemana;
												$dados["periodo"] = $periodo;
												$dados["status"] = 'Ativo';
												$dados['excluido'] = 'nao';
												$dados['logusuario'] = $this->_usuario['id'];;
												$dados['logdata'] = date('Y-m-d G:i:s');
												
												$_row = $quadrohorariosperiodos->save($dados);
												$totalaula = $totalaula+1;
											}
											
											$condicao_saida_periodo = true;
											//if($row_quadro['id']=="4") die();
											if($totalaula>=$row_quadro['quantidadesaulas']){

												echo '				totalaula: '.$totalaula.'<br/><br/><br/>';
												//$totalaula = 0;
												$condicao_saida = true;

												continue;
											}
										}
									}
					    		}
					    	}
				    	}
				    }

							if($condicao_saida)	{

								continue;
							}
						}
						
						if($condicao_saida)	{
							continue;
						}
						
					}
						
    			}
    			//$totalaula = 0;
				//die('depois do if');
				$totalaula = 0;
			}
    		//die('saida geral');
    		echo '				____________________<br/><br/><br/><br/><br/><br/>';
    	
    	}
			//die('aqui');
		$this->_redirect('admin/' . $this->getRequest()->getControllerName() . '/editar/id/' . $idlink);

	}	
    
	public function verificadisponibilidadeAction() {
		$obj = new stdClass();
		$obj->data = "ERRO";
		$obj->mensagem = "";
		$obj->titulo = "";

		$obj->idquadrohorarioperiodosorigem = "";
		$obj->idquadrohorarioorigem = "";
		$obj->idescolavinculoorigem = "";
		$obj->idprofessororigem = "";
		$obj->idmateriaorigem = "";
		$obj->periodoorigem = "";
		$obj->diasemanaorigem = "";
		$obj->idquadrohorarioperiodosdestino = "";
		$obj->idquadrohorariodestino = "";
		$obj->idescolavinculodestino = "";
		$obj->idprofessordestino = "";
		$obj->idmateriadestino = "";
		$obj->periododestino = "";
		$obj->diasemanadestino = "";

		$this->_helper->layout->disableLayout();
		$idescola = $this->getRequest()->getPost('idescola');
		$idquadrohorarioperiodosorigem = $this->getRequest()->getPost('idquadrohorarioperiodosorigem');
		$idquadrohorarioorigem = $this->getRequest()->getPost('idquadrohorarioorigem');
		$idescolavinculoorigem = $this->getRequest()->getPost('idescolavinculoorigem');
		$idprofessororigem = $this->getRequest()->getPost('idprofessororigem');
		$idmateriaorigem = $this->getRequest()->getPost('idmateriaorigem');
		$periodoorigem = $this->getRequest()->getPost('periodoorigem');
		$diasemanaorigem = $this->getRequest()->getPost('diasemanaorigem');

		$idquadrohorarioperiodosdestino = $this->getRequest()->getPost('idquadrohorarioperiodosdestino');
		$idquadrohorariodestino = $this->getRequest()->getPost('idquadrohorariodestino');
		$idescolavinculodestino = $this->getRequest()->getPost('idescolavinculodestino');
		$idprofessordestino = $this->getRequest()->getPost('idprofessordestino');
		$idmateriadestino = $this->getRequest()->getPost('idmateriadestino');
		$periododestino = $this->getRequest()->getPost('periododestino');
		$diasemanadestino = $this->getRequest()->getPost('diasemanadestino');


		$quadrohorarios = new Quadrohorarios();
		$quadrohorariosperiodos = new Quadrohorariosperiodos();
		//VERIFICACAO PARA SABER SE EXISTE UMA AULA NO HORARIO DE DESTINO
				$verifica_periodo_destino = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('idescola'=>$idescola, 'diassemana'=>$diasemanadestino, 'periodo'=>$periododestino, 'idescolavinculo'=>$idescolavinculodestino));
		if($verifica_periodo_destino){

			$verifica_periodo_origem = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('idescola'=>$idescola, 'diassemana'=>$diasemanaorigem, 'periodo'=>$periodoorigem, 'idescolavinculo'=>$idescolavinculodestino));
			//SE EXISTIR AULA NO DESTINO, VERIFICO SE O PROFESSOR DESTINO ESTA DISPONIVEL
			if(($verifica_periodo_origem) && ((int)$idescolavinculodestino > 0)){
				$verifica_professsor_origem = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('idescola'=>$idescola, 'diassemana'=>$diasemanadestino, 'periodo'=>$periododestino, 'idprofessor'=>$idprofessororigem, 'idmateria'=>$idmateriaorigem));

				if(sizeof($verifica_professsor_origem)>0){

					//var_dump($idquadrohorarioperiodosdestino); die('origem');
					//CORRIGIR aqui, jogar o 6 do 1A na quarta de primeiro B
					$obj->mensagem = "O professor destino já possui uma outra aula nessde dia e horário.";
					$obj->titulo = "CONFLITO!";
					echo json_encode($obj);
					die();
					die('CONFLITO: O professor destino já possui uma outra aula nessde dia e horário.');
				}else {
					$verifica_professsor_destino = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('idescola'=>$idescola, 'diassemana'=>$diasemanaorigem, 'periodo'=>$periodoorigem, 'idprofessor'=>$idprofessordestino, 'idmateria'=>$idmateriadestino));
					if((sizeof($verifica_professsor_destino)>0) && ((int)$idprofessordestino>0)){
						//var_dump($verifica_professsor_destino); die('destino');
						$obj->mensagem = "O professor origem já possui uma outra aula nesse dia e horário.";
						$obj->titulo = "CONFLITO!";
						echo json_encode($obj);
						die();
						die('CONFLITO: O professor origem já possui uma outra aula nesse dia e horário.');
					}
				}				
			}else{
				//die('aqui');
			}

			//VERIFICAR SE O PROFESSOR DA AULA PARA ESSA SALA DESTINO			
			if($idquadrohorariodestino <= 0){
				$rows_destino = $quadrohorarios->getQuadrohorarios(array('idescolavinculo'=>$idescolavinculodestino, 'idprofessor'=>$idprofessororigem));
				if(sizeof($rows_destino) <= 0) {
					$obj->mensagem = "Esse professor de destino não está vinculado a essa turma.";
					$obj->titulo = "Erro!";
					echo json_encode($obj);
					die();
					die('CONFLITO: Esse professor de destino não está vinculado a essa turma.');
				}
			}else{
				//VERIFICAR DE COLOCAR MATERIA
				//VERIFICAR SE O PROFESSOR DA AULA PARA ESSA SALA ORIGEM
				$rows_destino = $quadrohorarios->getQuadrohorarios(array('idescolavinculo'=>$idescolavinculodestino, 'idprofessor'=>$idprofessororigem)); 
				if(sizeof($rows_destino) <= 0){
					$obj->mensagem = "O professor de origem não está vinculado a essa turma.";
					$obj->titulo = "Erro!";
					echo json_encode($obj);
					die();
					die('CONFLITO: O professor de origem não está vinculado a essa turma.');
				}

				//VERIFICAR SE O PROFESSOR DA AULA PARA ESSA SALA ORIGEM
				$rows_origem = $quadrohorarios->getQuadrohorarios(array('idescolavinculo'=>$idescolavinculoorigem, 'idprofessor'=>$idprofessordestino)); 
				if(sizeof($rows_destino) <= 0){
					$obj->mensagem = "O professor de destino não está vinculado a essa turma.";
					$obj->titulo = "Erro!";
					echo json_encode($obj);
					die();
					die('CONFLITO: O professor de destino não está vinculado a essa turma.');
				}

			}
			//$verifica_aula = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('diassemana'=>$diasemana, 'idescola'=>$idescola, 'periodo'=>$periodo, 'idescolavinculo'=>$row['id']));
			
			
			//verifica professororigem, diasemanadestino, perdiododestino
			//'diferenteidescolavinculo'=>$idescolavinculodestino, 
			$rows_professor_dia_periodo = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('diferenteidescolavinculo'=>$idescolavinculodestino, 'idprofessor'=>$idprofessororigem, 'diassemana'=>$diasemanadestino, 'periodo'=>$periododestino)); 
/*			
, 'idmateria'=>$idmateriaorigem
, 'idmateria'=>$idmateriadestino

*/
			if(sizeof($rows_professor_dia_periodo) > 0){
				$obj->mensagem = "O professor de destino está vinculado a outra aula no mesmo horário.";
				$obj->titulo = "Erro!";
				echo json_encode($obj);
				die();
				die('CONFLITO: O professor de destino está vinculado a outra aula no mesmo horário.');
			}
			
		}else{
			//verifica professororigem, diasemanadestino, perdiododestino
			$rows_professor_dia_periodo = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('idprofessor'=>$idprofessororigem, 'diassemana'=>$diasemanadestino, 'periodo'=>$periododestino)); 
			
			if(sizeof($rows_professor_dia_periodo) > 0){
				$obj->mensagem = "O professor de destino está vinculado a outra aula no mesmo horário.";
				$obj->titulo = "Erro!";
				echo json_encode($obj);
				die();
				die('CONFLITO: O professor de destino está vinculado a outra aula no mesmo horário.');
			}	
		}
 

		if($idescolavinculoorigem!=$idescolavinculodestino){
			//verificar se já tem aula na semana qtd por semana

			$quadrohorario = $quadrohorarios->getQuadrohorarioById($idquadrohorarioorigem);
			$totalmateria = (isset($quadrohorario['quantidadesaulas'])) ? $quadrohorario['quantidadesaulas'] : 0;
			$rows_qtdaulas_destino = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('idescolavinculo'=>$idescolavinculoorigem ,'idmateria'=>$idmateriadestino, 'total'=>true)); 
			$rows_qtdaulas_destino = (int)$rows_qtdaulas_destino;
			if($totalmateria < $rows_qtdaulas_destino){
				$obj->mensagem = "Essa turma(origem) ultrapassou a quantidade máxima de aula de matéria.";
				$obj->titulo = "Erro!";
				echo json_encode($obj);
				die();
				die('CONFLITO: Essa turma(origem) ultrapassou a quantidade máxima de aula de matéria');
			}


			$quadrohorario_destino = $quadrohorarios->getQuadrohorarioById($idquadrohorariodestino);
			$totalmateria = (isset($quadrohorario_destino['quantidadesaulas'])) ? $quadrohorario_destino['quantidadesaulas'] : 0;
			$rows_qtdaulas_destino = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('idescolavinculo'=>$idescolavinculodestino ,'idmateria'=>$idmateriaorigem, 'total'=>true)); 
			$rows_qtdaulas_destino = (int)$rows_qtdaulas_destino;
			if($totalmateria < $rows_qtdaulas_destino){
				$obj->mensagem = "Essa turma(destino) ultrapassou a quantidade máxima de aula de matéria";
				$obj->titulo = "Erro!";
				echo json_encode($obj);
				die();
				die('CONFLITO: Essa turma(destino) ultrapassou a quantidade máxima de aula de matéria');
			}
		}
		



		$totalmateria_destino = 0;
		$totalquadroperiodoaula_destino = "";
/*
		if((int)$idquadrohorariodestino > 0){
die('aqui22222');
			$quadrohorario_destino = $quadrohorarios->getQuadrohorarioById($idquadrohorariodestino);
			$totalmateria_destino = (isset($quadrohorario_destino['quantidadesaulas'])) ? $quadrohorario_destino['quantidadesaulas'] : 0;
			$totalquadroperiodoaula_destino = (isset($quadrohorario_destino['quadroperiodoaula'])) ? $quadrohorario_destino['quadroperiodoaula'] : "";

			$quadrohorario_origem = $quadrohorarios->getQuadrohorarioById($idquadrohorarioorigem);
			$totalmateria_origem = (isset($quadrohorario_origem['quantidadesaulas'])) ? $quadrohorario_origem['quantidadesaulas'] : 0;
			$totalquadroperiodoaula_origem = (isset($quadrohorario_origem['quadroperiodoaula'])) ? $quadrohorario_origem['quadroperiodoaula'] : "";

			$dados_horarios_origem = array();
			$dados_horarios_origem['id'] = $idquadrohorariodestino;
			$dados_horarios_origem['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
			$dados_horarios_origem['idescolavinculo'] = $idescolavinculoorigem;
			$dados_horarios_origem['idmateria'] = $idmateriaorigem;

			$dados_horarios_origem['quantidadesaulas'] = $totalmateria_origem;
			$dados_horarios_origem['quadroperiodoaula'] = $totalquadroperiodoaula_origem;

			$dados_horarios_origem['idprofessor'] = $idprofessororigem;
			$dados_horarios_origem['status'] = 'Ativo';
			$dados_horarios_origem['logusuario'] = $this->_usuario['id'];;
			$dados_horarios_origem['logdata'] = date('Y-m-d G:i:s');
			
			$row_quadrohorarios_origem = $quadrohorarios->save($dados_horarios_origem);

			$dados_origem = array();
			$dados_origem['id'] = $idquadrohorarioperiodosdestino;
			$dados_origem["idescolavinculo"] = $idescolavinculoorigem;
			$dados_origem["idquadrohorario"] = $idescolavinculoorigem;
			$dados_origem["diassemana"] = $diasemanaorigem;
			$dados_origem["periodo"] = $periodoorigem;
			$dados_origem["status"] = 'Ativo';
			$dados_origem['excluido'] = 'nao';
			$dados_origem['logusuario'] = $this->_usuario['id'];;
			$dados_origem['logdata'] = date('Y-m-d G:i:s');
			$row_origem = $quadrohorariosperiodos->save($dados_origem);
		}
*/





		$totalmateria_origem = 0;
		$totalquadroperiodoaula_origem = "";

		if((int)$idquadrohorarioorigem > 0){
			$quadrohorario_origem = $quadrohorarios->getQuadrohorarioById($idquadrohorarioorigem);
			$totalmateria_origem = (isset($quadrohorario_origem['quantidadesaulas'])) ? $quadrohorario_origem['quantidadesaulas'] : 0;
			$totalquadroperiodoaula_origem = (isset($quadrohorario_origem['quadroperiodoaula'])) ? $quadrohorario_origem['quadroperiodoaula'] : "";
		}


if(((int)$idquadrohorarioorigem > 0) && ((int)$idquadrohorariodestino > 0)){
		$quadrohorario_destino = $quadrohorarios->getQuadrohorarioById($idquadrohorariodestino);
		$totalmateria_destino = (isset($quadrohorario_destino['quantidadesaulas'])) ? $quadrohorario_destino['quantidadesaulas'] : 0;
		$totalquadroperiodoaula_destino = (isset($quadrohorario_destino['quadroperiodoaula'])) ? $quadrohorario_destino['quadroperiodoaula'] : "";

		$quadrohorario_origem = $quadrohorarios->getQuadrohorarioById($idquadrohorarioorigem);
		$totalmateria_origem = (isset($quadrohorario_origem['quantidadesaulas'])) ? $quadrohorario_origem['quantidadesaulas'] : 0;
		$totalquadroperiodoaula_origem = (isset($quadrohorario_origem['quadroperiodoaula'])) ? $quadrohorario_origem['quadroperiodoaula'] : "";

		$dados_horarios_destino = array();
		$dados_horarios_destino['id'] = $idquadrohorarioorigem;
		$dados_horarios_destino['idsecretaria'] =  Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' ); 
		$dados_horarios_destino['idescolavinculo'] = $idescolavinculodestino;
		$dados_horarios_destino['idmateria'] = $idmateriadestino;

		$dados_horarios_destino['quantidadesaulas'] = $totalmateria_destino;
		$dados_horarios_destino['quadroperiodoaula'] = $totalquadroperiodoaula_destino;

		$dados_horarios_destino['idprofessor'] = $idprofessordestino;
		$dados_horarios_destino['status'] = 'Ativo';
		$dados_horarios_destino['logusuario'] = $this->_usuario['id'];;
		$dados_horarios_destino['logdata'] = date('Y-m-d G:i:s');

		//$row_quadrohorarios_destino = $quadrohorarios->save($dados_horarios_destino);

		$dados_destino = array();
		$dados_destino['id'] = (int)$idquadrohorarioperiodosorigem;
		$dados_destino["idescolavinculo"] = $idescolavinculodestino;
		$dados_destino["idquadrohorario"] = $idquadrohorariodestino;
		$dados_destino["diassemana"] = $diasemanadestino;
		$dados_destino["periodo"] = $periododestino;
		$dados_destino["status"] = 'Ativo';
		$dados_destino['excluido'] = 'nao';
		$dados_destino['logusuario'] = $this->_usuario['id'];;
		$dados_destino['logdata'] = date('Y-m-d G:i:s');

		$row_destino = $quadrohorariosperiodos->save($dados_destino);









		$dados_horarios_origem = array();
		$dados_horarios_origem['id'] = $idquadrohorariodestino;
		$dados_horarios_origem['idsecretaria'] =  Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );; 
		$dados_horarios_origem['idescolavinculo'] = $idescolavinculoorigem;
		$dados_horarios_origem['idmateria'] = $idmateriaorigem;

		$dados_horarios_origem['quantidadesaulas'] = $totalmateria_origem;
		$dados_horarios_origem['quadroperiodoaula'] = $totalquadroperiodoaula_origem;

		$dados_horarios_origem['idprofessor'] = $idprofessororigem;
		$dados_horarios_origem['status'] = 'Ativo';
		$dados_horarios_origem['logusuario'] = $this->_usuario['id'];;
		$dados_horarios_origem['logdata'] = date('Y-m-d G:i:s');

		$row_quadrohorarios_origem = $quadrohorarios->save($dados_horarios_origem);

		$dados_origem = array();
		$dados_origem['id'] = (int)$idquadrohorarioperiodosdestino;
		$dados_origem["idescolavinculo"] = $idescolavinculoorigem;
		$dados_origem["idquadrohorario"] = $idquadrohorarioorigem;
		$dados_origem["diassemana"] = $diasemanaorigem;
		$dados_origem["periodo"] = $periodoorigem;
		$dados_origem["status"] = 'Ativo';
		$dados_origem['excluido'] = 'nao';
		$dados_origem['logusuario'] = $this->_usuario['id'];;
		$dados_origem['logdata'] = date('Y-m-d G:i:s');

		$row_origem = $quadrohorariosperiodos->save($dados_origem);











//TA AQUI
		/*
		$obj->idquadrohorarioperiodosorigem = $row_quadrohorarios_origem['id'];
		$obj->idquadrohorarioorigem = $row_quadrohorarios_origem['id'];
		$obj->idescolavinculoorigem = $row_quadrohorarios_destino['idescolavinculo'];
		$obj->idprofessororigem = $row_quadrohorarios_destino['idprofessor'];
		$obj->idmateriaorigem = $row_quadrohorarios_destino['idmateria'];
		$obj->periodoorigem = $row_quadrohorarios_destino['periodo'];
		$obj->diasemanaorigem = $row_quadrohorarios_destino['diassemana'];

		$obj->idquadrohorarioperiodosdestino = $row_destino['id'];
		$obj->idquadrohorariodestino = $row_quadrohorarios_destino['id'];
		$obj->idescolavinculodestino = $row_origem['idescolavinculo'];
		$obj->idprofessordestino = $row_quadrohorarios_origem['idprofessor'];
		$obj->idmateriadestino = $row_quadrohorarios_origem['idmateria'];
		$obj->periododestino = $row_origem['periodo'];
		$obj->diasemanadestino = $row_origem['diassemana'];*/
}else{
	//QUANDO É DESTINO É VAZIO
		$dados_horarios_destino = array();
		$dados_horarios_destino['id'] = $idquadrohorarioorigem;
		$dados_horarios_destino['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$dados_horarios_destino['idescolavinculo'] = $idescolavinculoorigem;
		$dados_horarios_destino['idmateria'] = $idmateriaorigem;

		$dados_horarios_destino['quantidadesaulas'] = $totalmateria_origem;
		$dados_horarios_destino['quadroperiodoaula'] = $totalquadroperiodoaula_origem;

		$dados_horarios_destino['idprofessor'] = $idprofessororigem;
		$dados_horarios_destino['status'] = 'Ativo';
		$dados_horarios_destino['logusuario'] = $this->_usuario['id'];;
		$dados_horarios_destino['logdata'] = date('Y-m-d G:i:s');

		$row_quadrohorarios_destino = $quadrohorarios->save($dados_horarios_destino);

		$dados_destino = array();
		$dados_destino['id'] = (int)$idquadrohorarioperiodosorigem;
		$dados_destino["idescolavinculo"] = $idescolavinculodestino;
		$dados_destino["idquadrohorario"] = $row_quadrohorarios_destino['id'];
		$dados_destino["diassemana"] = $diasemanadestino;
		$dados_destino["periodo"] = $periododestino;
		$dados_destino["status"] = 'Ativo';
		$dados_destino['excluido'] = 'nao';
		$dados_destino['logusuario'] = $this->_usuario['id'];;
		$dados_destino['logdata'] = date('Y-m-d G:i:s');
		//var_dump($dados_destino);
		//die('aqui3');
		$row_destino = $quadrohorariosperiodos->save($dados_destino);

		$obj->idquadrohorarioperiodosdestino = $row_destino['id'];
		$obj->idquadrohorariodestino = $row_quadrohorarios_destino['id'];
		$obj->idescolavinculodestino = $row_destino['idescolavinculo'];
		$obj->idprofessordestino = $row_quadrohorarios_destino['idprofessor'];
		$obj->idmateriadestino = $row_quadrohorarios_destino['idmateria'];
		$obj->periododestino = $row_destino['periodo'];
		$obj->diasemanadestino = $row_destino['diassemana'];
}

		$obj->data = "OK";
		$obj->mensagem = "Horário alterado com Sucesso!";
		$obj->titulo = "Sucesso!";



		echo json_encode($obj);
		die();
	}

    /**
     * Atribui valores ao view
     * @param int $idquadrohorariosperiodo
     */    
    private function preForm($idquadrohorariosperiodo = 0, $idescola = 0) {
		$vinculos = new Escolasvinculos();
    	$this->view->escolasvinculos = $vinculos->getEscolasvinculos(array('idescola'=>$idescola));

    	$quadrohorariosperiodos = new Quadrohorariosperiodos();
    	$this->view->quadrohorariosperiodos = $quadrohorariosperiodos->getQuadrohorariosperiodos(array('idescola'=>$idescola));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_quadrohorariosperiodo = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescolavinculo = (int)trim($this->getRequest()->getPost("idescolavinculo"));
$idquadrohorario = (int)trim($this->getRequest()->getPost("idquadrohorario"));
$diassemana = trim($this->getRequest()->getPost("diassemana"));
$periodo = trim($this->getRequest()->getPost("periodo"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$diassemana) array_push($erros, "Informe a Dias da semana.");
if (""==$periodo) array_push($erros, "Informe a Aula.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$quadrohorariosperiodos = new Quadrohorariosperiodos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescolavinculo"] = $idescolavinculo;
$dados["idquadrohorario"] = $idquadrohorario;
$dados["diassemana"] = $diassemana;
$dados["periodo"] = $periodo;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $quadrohorariosperiodos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}