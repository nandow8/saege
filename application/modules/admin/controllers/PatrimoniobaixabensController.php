<?php
// class MYPDF extends TCPDF {

// 	//Page header
// 	public function Header(){
		
//         $image_file = "public/admin/imagens/logosantaisabel.jpg";
// 		$this->Image($image_file, 16, 15, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
//         //Set font
// 		$this->SetFont('helvetica', 'B', 20);

//         $this->SetFont('helvetica', 'B', 10);
       
//         // cabeçalho Endereço
//         $this->SetXY(16, 12);
//         $this->Cell(0, 0, 'Prefeitura Municipal de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 9);
//         $this->SetXY(35, 16);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Patrimônios - Baixa de bens', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 19);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 22);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetXY(35, 25);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');


//         //informações Saege
//         $this->SetFont('helvetica', '', 8);
//         $this->SetXY(60,20);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

//        	$patrimoniosbaixabem = new Patrimoniobaixabens();
//        	$data = $patrimoniosbaixabem->getCurrentData();

//         $this->SetFont('helvetica', '', 8);
//         $this->SetXY(60,25);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Data: '.$data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

//         $this->SetFont('helvetica', '', 10);
//         $this->SetXY(60,30);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');

// 	}

// 	// Page footer
// 	public function Footer() {
// 		// Position at 15 mm from bottom
// 		$this->SetY(-15);
// 		// Set font
// 		$this->SetFont('helvetica', 'I', 8);
// 		// Page number
//         $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
//         // Position at 15 mm from bottom
// 		$this->SetXY(150,-15);
// 		// Set font
// 		$this->SetFont('helvetica', 'I', 8);
// 		// Page number
// 		$this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
// 	}
// }
/**
 * Controle da classe patrimoniobaixabens do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PatrimoniobaixabensController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usuário logado
	 * @var Patrimoniobaixabem
	 */
	protected $_usuario = null;		
	
	/**
     * Verificação de permissão de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("patrimoniobaixabens", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$patrimoniobaixabens = new Patrimoniobaixabens();
		$queries = array();	
		$ultimo = $patrimoniobaixabens->getUltimoPatrimoniobaixabens($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Patrimoniobaixabens();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Patrimônio excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="patrimoniobaixabens") $objs = new Patrimoniobaixabens();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {

		$this->view->bread_crumb = array(
			array('url' => 'patrimoniobaixabens', 'name' => 'Patrimônios baixa de bens'),
			array('url' => null,'name' => 'Visualizar Patrimônio')
		);
		
		$id = (int)$this->_request->getParam("id");
		$patrimoniobaixabens = new Patrimoniobaixabens();
		$patrimoniobaixabem = $patrimoniobaixabens->getPatrimoniobaixabemById($id, array());
		
		if (!$patrimoniobaixabem) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimoniobaixabem;

		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Patrimônios baixa de bens')
		);
		
		$ns = new Zend_Session_Namespace('default_patrimoniobaixabens');
		$patrimoniobaixabens = new Patrimoniobaixabens();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}

		$dados = Usuarios::getUsuarios(array('id'=>Usuarios::getUsuario('id')));
		$_data = $dados[0];
		$perfil = $_data['idperfil'];

		if($perfil == 29):
			$escola = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'));
			$queries['idescola'] = $escola['id'];
 		endif;

 		if(!UsuariosPerfis::isAllowedCrudPatrimonio($perfil)) $queries['iddepartamento'] = $perfil;
 		
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
			if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
			if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
			if ($this->view->post_var["idpatrimonio"]!="") $queries["idpatrimonio"] = $this->view->post_var["idpatrimonio"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $patrimoniobaixabens->getPatrimoniobaixabens($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $patrimoniobaixabens->getPatrimoniobaixabens($queries, $paginaAtual, $maxpp);
	}
	
	/**
	 * 
	 * Action de edição de patrimoniobaixabens
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniobaixabens', 'name' => 'Patrimônios baixa de bens'),
			array('url' => null,'name' => 'Editar Patrimônio')
		);
				
		$id = (int)$this->_request->getParam("id");
		$patrimoniobaixabens = new Patrimoniobaixabens();
		$patrimoniobaixabem = $patrimoniobaixabens->getPatrimoniobaixabemById($id);
		
		if (!$patrimoniobaixabem) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimoniobaixabem;
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($patrimoniobaixabem);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Patrimônio editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}

		return true;
    }
	
	/**
	 * 
	 * Action de adição de patrimoniobaixabens 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniobaixabens', 'name' => 'Patrimônios baixa de bens'),
			array('url' => null,'name' => 'Adicionar Patrimônio')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Patrimônio adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		$this->view->adicionar = true;
		return true;		
    }	
    
    
	public function getdadosAction() {
		$this->_helper->layout->disableLayout();
		
		$idpatrimonio = (int)$this->_request->getParam("idpatrimonio");
		$patrimonios = new Patrimonios();
		$patrimonio = $patrimonios->getPatrimonioById($idpatrimonio, array());
		
		if (!isset($patrimonio['id'])) die('erro');
		
		$this->view->post_var = $patrimonio;
		
	}



    /**
     * Atribui valores ao view
     * @param int $idpatrimoniobaixabem
     */    
    private function preForm($idpatrimoniobaixabem = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_patrimoniobaixabem = false) {

		$iddepartamentotemp = (isset($this->view->post_var['iddepartamento']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamento'])) ? (int)$this->view->post_var['iddepartamento'] : (int)trim($this->getRequest()->getPost("iddepartamento"));
		
		$idescolatemp = (isset($this->view->post_var['iddepartamento']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamento'])) ? (int)$this->view->post_var['idescola'] : (int)trim($this->getRequest()->getPost("idescola"));

		$idpatrimoniotemp = (isset($this->view->post_var['idpatrimonio']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamento'])) ? (int)$this->view->post_var['idpatrimonio'] : (int)$this->getRequest()->getPost('idpatrimonio');
		
		$motivobaixatemp = (isset($this->view->post_var['motivobaixa']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamento'])) ? $this->view->post_var['motivobaixa'] : trim($this->getRequest()->getPost("motivobaixa"));

		$observacoestemp = (isset($this->view->post_var['observacoes']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamento'])) ? $this->view->post_var['observacoes'] : trim($this->getRequest()->getPost("observacoes"));

		$numeroboletimtemp = (isset($this->view->post_var['numeroboletim']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamento'])) ? $this->view->post_var['numeroboletim'] : trim($this->getRequest()->getPost("numeroboletim"));

		$statustemp = (isset($this->view->post_var['status1']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamento'])) ? $this->view->post_var['status1'] : trim($this->getRequest()->getPost("status1"));

		if (!isset($this->view->post_var))  $this->view->post_var = $_POST;
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
		$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
		$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
		$origem = trim($this->getRequest()->getPost("origem"));
		$idescola = $idescolatemp;
		$iddepartamento =  $iddepartamentotemp;
		
		$idpatrimonio = $idpatrimoniotemp;
		
		$motivobaixa = $motivobaixatemp;
		$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
		$numeroboletim = $numeroboletimtemp;
		$observacoes = $observacoestemp;
		$baixabem = trim($this->getRequest()->getPost("baixabem"));
		$status = $statustemp;
		$iddocumento = (int)trim($this->getRequest()->getPost("iddocumento"));
		$motivonaoaceito = trim($this->getRequest()->getPost("motivonaoaceito"));
		

		$erros = array();
		
		if (0==$idpatrimonio) array_push($erros, "Informe o Patrimônio.");
		if (""==$status) array_push($erros, "Informe a Status.");

		$patrimoniobaixabens = new Patrimoniobaixabens();

		if (sizeof($erros)>0) return $erros;
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			
			$dados['id'] = $id;
			
			$dados["sequencial"] = $sequencial;
			$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			$dados["horalancamento"] = $horalancamento;
			$dados["idusuariologado"] = $idusuariologado;
			$dados["origem"] = $origem;
			$dados["idescola"] = $idescola;
			
			$dados["iddepartamento"] = $iddepartamento;
			$dados["idpatrimonio"] = $idpatrimonio;
			$dados["motivobaixa"] = $motivobaixa;

			$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;

			$iddocumento = $this->getArquivo('iddocumento');
			if($iddocumento!=0): 
				$dados['iddocumento'] = $iddocumento; 
				$dados['status'] = "Aguardando Aceite";
			else:
				$dados["status"] = $status;
			endif;

			$dados["numeroboletim"] = $numeroboletim;
			$dados["observacoes"] = $observacoes;
			$dados["motivonaoaceito"] = $motivonaoaceito;
			$dados["baixabem"] = $baixabem;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');

			$row = $patrimoniobaixabens->save($dados);

				if($row):
					if(isset($dados['status']) && $dados['status'] == 'Aceito'):
						$idpatrimonio = $dados['idpatrimonio'];
						
						$info['id'] = $idpatrimonio;
						$info['statusbaixa'] = 'Baixado';
						$info['idperfil'] = null;
						$info['codescola'] = null;

						$patrimonio = new Patrimonios();
						$patrimonio->save($info);
					endif;
					
				endif;

			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }
   
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }


    public function gerardocumentopatrimonioAction(){

    	$arraymotivos = array(
    			'1'=>'O bem a ser baixado se apresenta irrecuperável', 
    			'2'=>'O conserto do bem se mostra antieconômico', 
    			'3'=>'Roubo ou Furto'
    	);

    	$id = $this->_request->getParam('id');

    	/**INFORMAÇÕES DO BAIXA**/
    	$baixa = Patrimoniobaixabens::getPatrimoniobaixabemByIdHelper($id);

    	//pegar número do patrimônio
    	$___rows = Patrimonios::getPatrimoniosHelper(array('id'=>$baixa['idpatrimonio']));

    	$numeropatrimonio = (isset($___rows[0]['numeropatrimonio'])) ? $___rows[0]['numeropatrimonio'] : '';

    	$idescola = (isset($baixa['idescola']) && $baixa['idescola'] > 0 ) ? $baixa['idescola'] : '';
    	$idperfil = (isset($baixa['iddepartamento']) && $baixa['iddepartamento'] > 0 ) ? $baixa['iddepartamento'] : '';
    	$motivo = (isset($baixa['motivobaixa'])) ? $baixa['motivobaixa'] : '';
    	$observacoes = (isset($baixa['observacoes'])) ? $baixa['observacoes'] : '';
    	
    	$numeroboletim = (isset($baixa['numeroboletim'])) ? $baixa['numeroboletim'] : '';
    	
    	if($motivo) $razao = $arraymotivos[$motivo];

    	if($idescola) :
    		$_rowEscola = Escolas::getEscolaByIdHelper($idescola);
    		$escola = $_rowEscola['escola'];
    	endif;
    	if($idperfil):
    		$_rowPerfil = UsuariosPerfis::getPerfilByIdHelper($idperfil);
    		$perfil = $_rowPerfil['perfil'];
    	endif;    	

    	/**INFORMAÇÕES DO PATRIMÔNIO**/
    	$bem = Patrimonios::getPatrimonioByIdHelper($baixa['idpatrimonio']);

    	$patrimonio = (isset($bem['patrimonio'])) ? $bem['patrimonio'] : '';
    	$descricoesdobem = (isset($bem['descricoesdobem'])) ? $bem['descricoesdobem'] : '';
    	
    	$patrimoniosbaixabem = new Patrimoniobaixabens();
       	$data = $patrimoniosbaixabem->getCurrentData();
       	
		$local = 'Santa Isabel';

		$localData = $local.' , '.$data;

        /*=========================    Setando valores na View ==========================*/
        $this->view->perfil = (isset($perfil)) ? $perfil : '';
        if(isset($escola)) $this->view->escola = $escola;
        $this->view->razao = $razao;
        $this->view->motivo = $motivo;
        if($motivo && $motivo==3) $this->view->numeroboletim = $numeroboletim;
        $this->view->observacoes =  (isset($observacoes)) ? $observacoes : '';
        $this->view->numeropatrimonio = $numeropatrimonio;
        $this->view->patrimonio = $patrimonio;
        $this->view->descricoesdobem = $descricoesdobem;
        $this->view->localData = $localData;

        /* =====================    END Setando valores na View ==========================*/    

        $this->preForm();
        

        $db = Zend_Registry::get('db');

        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Requerimento de baixa de bem');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 9);
        // add a page
        $pdf->AddPage();

        $html = $this->view->render('patrimoniobaixabens/pdf/patrimoniobaixabensdocumento.phtml');

        $pdf->writeHTML($html, true, 0, true, 0);

        
        $filename = 'requisicaobaixabem.pdf';
        $pdf->Output($filename, 'I');
        die();
        return true;
    }
    
}
