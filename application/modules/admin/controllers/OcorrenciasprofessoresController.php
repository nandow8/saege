<?php

/**
 * Controle da classe ocorrenciasprofessores do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_OcorrenciasprofessoresController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Ocorrenciasprofessor
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("ocorrenciasprofessores", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Ocorrenciasprofessores();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ocorrência excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="ocorrenciasprofessores") $objs = new Ocorrenciasprofessores();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ocorrenciasprofessores', 'name' => 'Ocorrências'),
			array('url' => null,'name' => 'Visualizar Ocorrência')
		);
		
		$id = (int)$this->_request->getParam("id");
		$ocorrenciasprofessores = new Ocorrenciasprofessores();
		$ocorrenciasprofessor = $ocorrenciasprofessores->getOcorrenciasprofessorById($id, array());
		
		if (!$ocorrenciasprofessor) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $ocorrenciasprofessor;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Ocorrências')
		);
		
		$ns = new Zend_Session_Namespace('default_ocorrenciasprofessores');
		$ocorrenciasprofessores = new Ocorrenciasprofessores();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["idaluno"]!="") $queries["idaluno"] = $this->view->post_var["idaluno"];
			if ($this->view->post_var["ocorrencia"]!="") $queries["ocorrencia"] = $this->view->post_var["ocorrencia"];
			if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $ocorrenciasprofessores->getOcorrenciasprofessores($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $ocorrenciasprofessores->getOcorrenciasprofessores($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de ocorrenciasprofessores
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ocorrenciasprofessores', 'name' => 'Ocorrências'),
			array('url' => null,'name' => 'Editar Ocorrência')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$ocorrenciasprofessores = new Ocorrenciasprofessores();
		$ocorrenciasprofessor = $ocorrenciasprofessores->getOcorrenciasprofessorById($id);
		
		if (!$ocorrenciasprofessor) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $ocorrenciasprofessor;
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($ocorrenciasprofessor);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ocorrência editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de ocorrenciasprofessores 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ocorrenciasprofessores', 'name' => 'Ocorrências'),
			array('url' => null,'name' => 'Adicionar Ocorrência')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ocorrência adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		$idescolavinculo = (int)$this->_request->getPost("idescolavinculo");
		$idsprofessores = Usuarios::getUsuario('idfuncionario');
		$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>$idsprofessores, 'idescolavinculo'=>$idescolavinculo));
	}

    /**
     * Atribui valores ao view
     * @param int $idocorrenciasprofessor
     */    
    private function preForm($idocorrenciasprofessor = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_ocorrenciasprofessor = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
		$idescolavinculo = (int)trim($this->getRequest()->getPost("idescolavinculo"));
		$ocorrencia = trim($this->getRequest()->getPost("ocorrencia"));
		$descricoes = trim($this->getRequest()->getPost("descricoes"));
		$status = trim($this->getRequest()->getPost("status1"));
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		
		$erros = array();
		
		if (0==$idaluno) array_push($erros, "Informe a Alunos.");
		if (""==$ocorrencia) array_push($erros, "Informe a Ocorrências.");
		if (""==$data) array_push($erros, "Informe a Data da ocorrência.");
		if (""==$status) array_push($erros, "Informe a Status.");

		$ocorrenciasprofessores = new Ocorrenciasprofessores();

		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
			$dados["idescolavinculo"] = $idescolavinculo;
			$dados["idaluno"] = $idaluno;
			$dados["ocorrencia"] = $ocorrencia;
			$dados["descricoes"] = $descricoes;
			$dados["data"] = date('Y-m-d', $data);
			$dados["status"] = $status;

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

			if($idaluno == -1)
			{
				$escolas = new Escolassalasatribuicoes();
				$alunos = $escolas->getSalasatribuicoes(array('idvinculo' => $idescolavinculo));

				foreach($alunos as $v)
				{
					$dados['idaluno'] = $v['idaluno'];
					$row = $ocorrenciasprofessores->save($dados);
				}
			}
			else
			{	
				$row = $ocorrenciasprofessores->save($dados);
			}
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function setescolasalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$id = (int)$this->_request->getPost("id");
		$idescolavinculo = (int)$this->_request->getPost("idescolavinculo");
		if($idescolavinculo <= 0) return $this->view->rows = false;

		if($id > 0)
		{
			$this->view->editar = true;
		}

		$escolas = new Escolassalasatribuicoes();
		$this->view->rows = $escolas->getSalasatribuicoes(array('idvinculo' => $idescolavinculo));
	}
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}