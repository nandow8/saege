<?php

/**
 * Controle da classe avaliacoescomplementares do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_AvaliacoescomplementaresController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Avaliacoescomplementares
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("avaliacoescomplementares", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$avaliacoescomplementares = new Avaliacoescomplementares();
		$queries = array();	
		$ultimo = $avaliacoescomplementares->getUltimoAvaliacaocomplementar($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}

		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Avaliacoescomplementares();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mapa de acompanhamento excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="avaliacoescomplementares") $objs = new Avaliacoescomplementares();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'avaliacoescomplementares', 'name' => 'Avaliações Complementares'),
			array('url' => null,'name' => 'Visualizar Avaliações Complementares')
		);
		
		$id = (int)$this->_request->getParam("id");
		$avaliacoescomplementares = new Avaliacoescomplementares();
		$avaliacoescomplementares = $avaliacoescomplementares->getAvaliacoescomplementaresById($id, array());
		
		if (!$avaliacoescomplementares) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $avaliacoescomplementares;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Avaliações Complementares')
		);
		
		$ns = new Zend_Session_Namespace('default_avaliacoescomplementares');
		$avaliacoescomplementares = new Avaliacoescomplementares();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
    		if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $avaliacoescomplementares->getAvaliacoescomplementares($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $avaliacoescomplementares->getAvaliacoescomplementares($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de avaliacoescomplementares
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'avaliacoescomplementares', 'name' => 'Avaliações Complementares'),
			array('url' => null,'name' => 'Editar Avaliações Complementares')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$avaliacoescomplementares = new Avaliacoescomplementares();
		$avaliacoescomplementares = $avaliacoescomplementares->getAvaliacoescomplementaresById($id);
		
		if (!$avaliacoescomplementares) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $avaliacoescomplementares;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($avaliacoescomplementares);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mapa de acompanhamento de classe e sondagem editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de avaliacoescomplementares 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'avaliacoescomplementares', 'name' => 'Avaliações Complementares'),
			array('url' => null,'name' => 'Adicionar Avaliações Complementares')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mapa de acompanhamento de classe e sondagem adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idavaliacoescomplementares
     */    
    private function preForm($idavaliacoescomplementares = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_avaliacoescomplementares = false) {

		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		// die(var_dump($this->view->post_var));
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
		$data = Mn_Util::stringToTime(trim($this->getRequest()->getPost("data")));
		$idescola = trim($this->getRequest()->getPost("idescola"));
		$idserie = trim($this->getRequest()->getPost("idserie"));
		$idturma = trim($this->getRequest()->getPost("idturma"));
		$idprofessor = trim($this->getRequest()->getPost("idprofessor"));
		$iddiretor = trim($this->getRequest()->getPost("iddiretor"));
		$idcoordenador = trim($this->getRequest()->getPost("idcoordenador"));
		$idtipoavaliacao = trim($this->getRequest()->getPost("idtipoavaliacao"));
		$status = trim($this->getRequest()->getPost("status1"));

		$idaluno = ($this->getRequest()->getPost("idaluno") != NULL) ? $this->getRequest()->getPost("idaluno") : NULL;
		$notas = array();

		$__qtdquestoes = (Tipoavaliacoes::getTipoavaliacoes(array('id'=>$idtipoavaliacao)));
		$_qtdquestoes = ($__qtdquestoes != NULL) ? $__qtdquestoes[0] : NULL;
		$qtdquestoes = ($_qtdquestoes != NULL) ? $_qtdquestoes['qtdquestoes'] : NULL;

		if(sizeof($idaluno) > 0){
			foreach ($idaluno as $k => $v) {
				for ( $i=0 ; $i < $qtdquestoes ; $i++ ) { 
					$_notas = ($this->getRequest()->getPost("questoes".$v.$i));
					$notas[$k][] = $_notas[0];
				}
			}
		}

		$acertos = $this->getRequest()->getPost("acertos");

		$erros = array();
		
		if (""==$sequencial) array_push($erros, "Informe o sequencial.");
		if (""==$idescola) array_push($erros, "Informe a escola.");
		if(""==$status) array_push($erros, "Informe o status.");
		
		$avaliacoescomplementares = new Avaliacoescomplementares();
		$avaliacoesalunosrelacao = new Avaliacoesalunosrelacoes();		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();

			$dados['id'] = $id;
			if((int)$id <= 0)$dados["sequencial"] = $sequencial;
			if((int)$id <= 0)$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			if((int)$id <= 0)$dados["horalancamento"] = $horalancamento;
			if((int)$id <= 0)$dados["idusuariologado"] = (int)$idusuariologado;
			$dados["data"] = date("Y-m-d", $data);
			$dados["idescola"] = (int)$idescola;
			$dados["iddiretor"] = (int)$iddiretor;
			$dados["idcoordenador"] = (int)$idcoordenador;
			$dados["idserie"] = (int)$idserie;
			$dados["idturma"] = (int)$idturma;
			$dados["idtipoavaliacao"] = (int)$idtipoavaliacao;
			$dados["status"] = $status;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');
		
			$row = $avaliacoescomplementares->save($dados);

			$avaliacoesalunosrelacao->setExcluido($row['id']);

			$i = 0;
			foreach ($idaluno as $key => $aluno) {
				$_dados = array();
				$_dados['idavaliacao'] = $row['id'];
				$_dados['idaluno'] = $aluno;
				$_dados['notas'] = implode(',', $notas[$key]);
				$_dados['media'] = (float)$acertos[$i];
				$_dados['excluido'] = 'nao';

				$_row = $avaliacoesalunosrelacao->save($_dados);

				$i++;
			}

			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}
		
		return "";    	
    }  

    public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries['idescola'] = $idescola;
		$queries['order'] = " GROUP BY ev1.idserie";
		$escolasvinculos = new Escolasvinculos();
		$this->view->rows = $escolasvinculos->getEscolasvinculos($queries);
		//var_dump($this->view->rows); die();
		//$rows = new Escolasseries();
		//$this->view->rows = $rows->getEscolasseries($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setturmasAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;

		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculos($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0)) $this->view->rows = null;		
	}

    public function setcoordenadoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idescola"] = $idescola;
		$queries["coordenador"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setdiretoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idescola"] = $idescola;
		$queries["diretor"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$idserie = (int)$this->_request->getPost("idserie");
		$idvinculo = (int)$this->_request->getPost("idvinculo");
		$idtipoavaliacao = (int)$this->_request->getPost("idtipoavaliacao");


		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		$queries['idvinculo'] = $idvinculo;
		
		$rows = new Escolassalasatribuicoes();
		$this->view->rows = $rows->getSalasatribuicoes($queries);

		$_queries = array();
		$_queries['id'] = $idtipoavaliacao;

		$_rows = new Tipoavaliacoes();
		$this->view->rows = array_merge($_rows->getTipoavaliacoes($_queries),$rows->getSalasatribuicoes($queries));

		if(((int)$idescola <= 0) || ((int)$idserie <= 0) || ((int)$idvinculo <= 0) || ((int)$idtipoavaliacao <= 0) ) $this->view->rows = null;		
	}    

	public function toexcelAction(){
			$c = 0;

			$objPHPExcel = new PHPExcel();
			// Set Orientation, size and scaling
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);	

			$border_style= array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN,'color' => array('argb' => '000'),)));

			//$objPHPExcel->getActiveSheet()->SetCellValue("A11", 'IMG2');
			//$objPHPExcel->getActiveSheet()->mergeCells('A11:D14');				

			$objPHPExcel->getActiveSheet()->SetCellValue("A1", 'Relatório de Atividades')->getStyle("A1")->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

			$objPHPExcel->getActiveSheet()->mergeCells('A1:O1')->getStyle('A1:O1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->SetCellValue("A2", "Escola");
			$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->SetCellValue("B2", "Série");
			$objPHPExcel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->SetCellValue("C2", "Turma");
			$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->SetCellValue("D2", "Diretor");
			$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->getActiveSheet()->SetCellValue("E2", "Coordenador");
			$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

			header('Content-type: application/vnd.ms-excel');
			header('Content-Disposition: attachment; filename="file.xlsx"');
			$objWriter->save('php://output');
			die();
			
			/*
			$cell = ''.chr($c).''.$rowCount;
			$objPHPExcel->getActiveSheet()->SetCellValue($cell, 'Item');
			$objPHPExcel->getActiveSheet()->getStyle(chr($c).$rowCount)->getFont()->setBold(true);	*/
		die('aqui');
	}
}