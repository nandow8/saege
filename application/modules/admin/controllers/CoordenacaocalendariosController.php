<?php
class MYPDF extends TCPDF {

	//Page header
	public function Header(){
		$image_file = "public/admin/imagens/logosantaisabel.jpg";
		$this->Image($image_file, 45, 5, 20, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', '', 5);
        $this->SetXY(43, 26);
        $this->Cell(0, 0, 'Paraíso da Grande São Paulo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
       
        // cabeçalho Endereço 
        $this->SetFont('helvetica', '', 12);
        $this->SetXY(124, 6);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Municipio de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', 'B', 10);
        $this->SetXY(120, 10);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Secretaria Municipal de Educação', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(115, 14);
        $this->SetTextColor(130, 130, 130);
		$this->Cell(0, 0, 'Tel. 4656-2440 e-mail: sec.educacao@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 8);
        $this->SetXY(115, 18);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297-Centro - CEP: 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
         


        //informações Saege
        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

       	$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();

        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,18);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: '.$data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 10);
        $this->SetXY(40,21);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        	

	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
        $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Position at 15 mm from bottom
		$this->SetXY(150,-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
/**
 * Controle da classe coordenacaocalendarios do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_CoordenacaocalendariosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Coordenacaocalendario
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("coordenacaocalendarios", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Coordenacaocalendarios();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Calendário escolar excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="coordenacaocalendarios") $objs = new Coordenacaocalendarios();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaocalendarios', 'name' => 'Calendários escolares'),
			array('url' => null,'name' => 'Visualizar Calendário escolar')
		);
		
		$id = (int)$this->_request->getParam("id");
		$coordenacaocalendarios = new Coordenacaocalendarios();
		$coordenacaocalendario = $coordenacaocalendarios->getCoordenacaocalendarioById($id, array());
		
		if (!$coordenacaocalendario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaocalendario;
		$this->preForm();
		
		$queriestiposdias = array();
		$queriestiposdias['idcalendario'] = $id;
		$queriestiposdias['order'] = " GROUP BY c1.data DESC";
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();
		//$queriestiposdias['die'] = true;
		$this->view->eventosdias = $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queriestiposdias);
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Calendários escolares')
		);
		
		$ns = new Zend_Session_Namespace('default_coordenacaocalendarios');
		$coordenacaocalendarios = new Coordenacaocalendarios();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		 
			if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"]; 
			if ($this->view->post_var["ano"]!="") $queries["ano"] = $this->view->post_var["ano"]; 
			if ($this->view->post_var["status"]!="") $queries["status"] = $this->view->post_var["status"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$idescolaFuncionario = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario()['idfuncionario'])['idescola'];
		$idperfilFuncionario = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario()['idfuncionario'])['idperfil'];
		  

		if((!empty($idescolaFuncionario) AND $idescolaFuncionario != 0) OR $idperfilFuncionario != 17){  // 17 Coordenação
			$queries['idescola'] = $idescolaFuncionario;
			$queries['idperfil'] = 17;
		} 

		$queries['total'] = true;
		$totalRegistros = $coordenacaocalendarios->getCoordenacaocalendarios($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $coordenacaocalendarios->getCoordenacaocalendarios($queries, $paginaAtual, $maxpp);	
	}
	
	
	/**
	 * 
	 * Action de adição de afastamentos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaocalendarios', 'name' => 'Calendários escolares'),
			array('url' => null,'name' => 'Adicionar Calendário escolar')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Afastamento e Licença adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }

	/**
	 * 
	 * Action de edição de coordenacaocalendarios
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaocalendarios', 'name' => 'Calendários escolares'),
			array('url' => null,'name' => 'Editar Calendário escolar')
		);
				
		$id = (int)$this->_request->getParam("id");
		$coordenacaocalendarios = new Coordenacaocalendarios();
		$coordenacaocalendario = $coordenacaocalendarios->getCoordenacaocalendarioById($id);
		
		if (!$coordenacaocalendario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaocalendario;
		$this->preForm();
		
		$queriestiposdias = array();
		$queriestiposdias['idcalendario'] = $id;
		$queriestiposdias['order'] = " GROUP BY c1.data DESC";
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();
		//$queriestiposdias['die'] = true;
		$this->view->eventosdias = $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queriestiposdias);

		$this->view->visualizar = false;
		if ($this->_request->isPost()) {
			$erros = $this->getPost($coordenacaocalendario);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Calendário escolar editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
	}  		
	
	/**
	 * preenche os campos de data de acordo com calendario base selecionado 
	 */
	public function getcalendardatesAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      
	
		$id = (int)$this->getRequest()->getPost('idcalendariobase');
	
		$coordenacaocalendarios = new Coordenacaocalendarios();
		$rows = $coordenacaocalendarios->getCoordenacaocalendarioById($id);
	 
	
		echo json_encode($rows);
		die();
	
	}
	
	/**
	 * 
	 * Action de adição de coordenacaocalendarios 
	 */
	public function settipoAction() {
		$this->_helper->layout->disableLayout();	

		$idtipo = (int)$this->_request->getPost("idtipo");
		$coordenacaocalendariostipos = new Coordenacaocalendariostipos();
		$coordenacaocalendariotipo = $coordenacaocalendariostipos->getCoordenacaocalendariotipoById($idtipo, array());
		
		if (!$coordenacaocalendariotipo) die('Não encontrado!');

		$this->view->post_var = $coordenacaocalendariotipo;
    }

	/**
	 * 
	 * Action de adição de coordenacaocalendarios 
	 */
	public function setdiaAction() {
		$this->_helper->layout->disableLayout();	

		$idcalendario = (int)$this->_request->getPost("id");
		$idtiposelecionado = (int)$this->_request->getPost("idtiposelecionado");
		$mes = (int)$this->_request->getPost("mes");
		$dia = (int)$this->_request->getPost("dia");

		$coordenacaocalendariostipos = new Coordenacaocalendariostipos();
		$coordenacaocalendariotipo = $coordenacaocalendariostipos->getCoordenacaocalendariotipoById($idtiposelecionado, array());
		
		if (!$coordenacaocalendariotipo) die('Não encontrado!');

		$this->view->post_var = $coordenacaocalendariotipo;

		if($mes<10) '0'.$mes;
		if($dia<10) '0'.$dia;

		$data = date('Y') . '-' . $mes . '-' . $dia;
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();

		$queries['data_i'] = date("Y-m-d", strtotime($data));
		$queries['data_f'] = date("Y-m-d", strtotime($data));
		$queries['idcalendario'] = $idcalendario;
		$queries['idtipo'] = $idtiposelecionado;
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();
		$coordenacaocalendariodia = $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queries);

		if(sizeof($coordenacaocalendariodia) <= 0){
			$dados = array();		
			$dados["idcalendario"] = $idcalendario;
			$dados["idtipo"] = $idtiposelecionado;
			$dados["data"] = date("Y-m-d", strtotime($data));
			$dados["status"] = 'Ativo';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			$row = $coordenacaocalendariosdias->save($dados);
		}else{
			$_row = Coordenacaocalendariosdias::getCoordenacaocalendariodiaByIdTipoDataHelper($queries);
			if(isset($_row['id'])){
				$_row['excluido'] = "sim";
				$_row['logusuario'] = $this->_usuario['id'];;
				$_row['logdata'] = date('Y-m-d G:i:s');
				$coordenacaocalendariosdias->save($_row);
			}
		}


		$queriestiposdias = array();
		$queriestiposdias['data_i'] = date("Y-m-d", strtotime($data));
		$queriestiposdias['data_f'] = date("Y-m-d", strtotime($data));
		$queriestiposdias['idcalendario'] = $idcalendario;
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();
		$coordenacaocalendariodia = $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queriestiposdias);
		$this->view->rows = $coordenacaocalendariodia;

		$queriestiposdias['total'] = true;
		$this->view->total = $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queriestiposdias);
    }

    /**
     * Atribui valores ao view
     * @param int $idcoordenacaocalendario
     */    
    private function preForm($idcoordenacaocalendario = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_coordenacaocalendario = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = 1;//(int)trim($this->getRequest()->getPost("idsecretaria"));
		$ano = $this->getRequest()->getPost("ano");
		$titulo = trim($this->getRequest()->getPost("titulo"));
		$status = trim($this->getRequest()->getPost("status1"));
		$idescolaForm =  $this->getRequest()->getPost("idescolaForm");
		$idcalendariobase =  (int)$this->getRequest()->getPost("idcalendariobase");
		$salvaidcalendariobase =  (int)$this->getRequest()->getPost("salvaidcalendariobase");
	 	
		$primeirobimestreinicio = Mn_Util::stringToTime($this->getRequest()->getPost("primeirobimestreinicio"));
		$primeirobimestrefim = Mn_Util::stringToTime($this->getRequest()->getPost("primeirobimestrefim"));
		$segundobimestreinicio = Mn_Util::stringToTime($this->getRequest()->getPost("segundobimestreinicio"));
		$segundobimestrefim = Mn_Util::stringToTime($this->getRequest()->getPost("segundobimestrefim"));
		$terceirobimestreinicio = Mn_Util::stringToTime($this->getRequest()->getPost("terceirobimestreinicio"));
		$terceirobimestrefim = Mn_Util::stringToTime($this->getRequest()->getPost("terceirobimestrefim"));
		$quartobimestreinicio = Mn_Util::stringToTime($this->getRequest()->getPost("quartobimestreinicio"));
		$quartobimestrefim = Mn_Util::stringToTime($this->getRequest()->getPost("quartobimestrefim"));
 
		$erros = array();
		
		if (0==$idsecretaria) array_push($erros, "Informe a Secretaria.");
		if (""==$ano) array_push($erros, "Informe a Ano.");
		if (""==$status) array_push($erros, "Informe a Status.");
 
		$coordenacaocalendarios = new Coordenacaocalendarios();
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dadosCalendarioDias = array();
			$dados['id'] = $id;

			if(empty($id)){				
				$dados['idescola'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario()['idfuncionario'])['idescola'];
				$dados['idperfil'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario()['idfuncionario'])['idperfil'];
			}

			$dados["idsecretaria"] = $idsecretaria;
			$dados["ano"] = $ano;
			$dados["titulo"] = $titulo;
			$dados["idescolaForm"] = $idescolaForm;
			$dados["idcalendariobase"] = $idcalendariobase;

			$dados["primeirobimestreinicio"] = ($primeirobimestreinicio) ? date("Y-m-d", $primeirobimestreinicio) :  NULL;		
			$dados["primeirobimestrefim"] = ($primeirobimestreinicio) ? date("Y-m-d", $primeirobimestrefim) :  NULL;		
			$dados["segundobimestreinicio"] = ($primeirobimestreinicio) ? date("Y-m-d", $segundobimestreinicio) :  NULL;		
			$dados["segundobimestrefim"] = ($primeirobimestreinicio) ? date("Y-m-d", $segundobimestrefim) :  NULL;		
			$dados["terceirobimestreinicio"] = ($primeirobimestreinicio) ? date("Y-m-d", $terceirobimestreinicio) :  NULL;		
			$dados["terceirobimestrefim"] = ($primeirobimestreinicio) ? date("Y-m-d", $terceirobimestrefim) :  NULL;		
			$dados["quartobimestreinicio"] = ($primeirobimestreinicio) ? date("Y-m-d", $quartobimestreinicio) :  NULL;		
			$dados["quartobimestrefim"] = ($primeirobimestreinicio) ? date("Y-m-d", $quartobimestrefim) :  NULL;
 
			$dados["status"] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
 

			$row = $coordenacaocalendarios->save($dados);
			 
			if( $salvaidcalendariobase > 0 ){
				foreach (Coordenacaocalendariosdias::getCoordenacaocalendariosdiasHelper(array('idcalendario' => $idcalendariobase)) as $key => $value) {
					$data = explode("-", $value['data']);
					$dataCriacao = explode("-", $value['datacriacao']);
					$logData = explode("-", $value['logdata']);
					
					$data[0] = $dados["ano"]; 
					$dataCriacao[0] = $dados["ano"]; 
					$logData[0] = $dados["ano"]; 

					$dataImpoded = implode("-", $data);
					$dataCriacaoImpoded = implode("-", $dataCriacao);
					$logDataImpoded = implode("-", $logData); 

					$dadosCalendarioDias['idcalendario'] = $id;
					$dadosCalendarioDias['idtipo'] = $value['idtipo'];
					$dadosCalendarioDias['data'] = $dataImpoded;
					$dadosCalendarioDias['status'] = $value['status'];
					$dadosCalendarioDias['datacriacao'] = $dataCriacaoImpoded;
					$dadosCalendarioDias['excluido'] = $value['excluido'];
					$dadosCalendarioDias['logusuario'] = $value['logusuario'];
					$dadosCalendarioDias['logdata'] = $logDataImpoded;
					$dadosCalendarioDias['cor'] = $value['cor'];
					$dadosCalendarioDias['sigla'] = $value['sigla'];
					$dadosCalendarioDias['dialetivo'] = $value['dialetivo'];
					$dadosCalendarioDias['ano'] = $value['ano']; 
					
					$row = $coordenacaocalendariosdias->save($dadosCalendarioDias);
				}
			}
		
			$db->commit();

			if($id<=0){
				$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName().'/editar/id/' . $row['id']);
			}
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	}
	
	public function calendariosescolarespdfAction(){  
		
		$id = (int)$this->_request->getParam("id");
		$coordenacaocalendarios = new Coordenacaocalendarios();
		$coordenacaocalendario = $coordenacaocalendarios->getCoordenacaocalendarioById($id, array());
		
		if (!$coordenacaocalendario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaocalendario;
		$this->preForm();
		
		$queriestiposdias = array();
		$queriestiposdias['idcalendario'] = $id;
		$queriestiposdias['order'] = " GROUP BY c1.data DESC";
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();
		//$queriestiposdias['die'] = true;
		$this->view->eventosdias = $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queriestiposdias);
	 
		$teste = [];
		foreach ($this->view->eventosdias as $evento) {
			$teste[$evento['data']] = $evento;
		}

		$this->view->eventosNovo = $teste;
//  echo '<pre>'; print_r($this->view->eventosdias );die;
		$this->preForm();

		$db = Zend_Registry::get('db');

		 // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('l');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Calendario Escolar');
        // $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 7);

        // add a page        
        $pdf->AddPage();
        
        $html = $this->view->render('coordenacaocalendarios/pdf/calendariosescolarespdf.phtml');
        $image_file = "public/admin/imagens/logosantaisabel.jpg";

        $pdf->writeHTML($html, true, 0, true, 0);

        $filename = 'produtos_estoque.pdf';
        $pdf->Output($filename, 'I');
        
        die();
        return true;
    	
    }
    
}