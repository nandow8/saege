<?php

/**
 * Controle da classe escolasconfiguracoesdiferenciadas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EscolasconfiguracoesdiferenciadasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Escolaconfiguracaodiferenciada
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("escolasconfiguracoesdiferenciadas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Escolasconfiguracoesdiferenciadas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
            Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Configuração diferenciada - ID -> ".$id." excluído com sucesso!");
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Configuração Diferenciada excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="escolasconfiguracoesdiferenciadas") $objs = new Escolasconfiguracoesdiferenciadas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
            Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Configuração diferenciada -> ".$id." atualizada para ".$obj['status']);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasconfiguracoesdiferenciadas', 'name' => 'Configurações Diferenciadas'),
			array('url' => null,'name' => 'Visualizar Configuração Diferenciada')
		);
		
		$id = (int)$this->_request->getParam("id");
		$escolasconfiguracoesdiferenciadas = new Escolasconfiguracoesdiferenciadas();
		$escolaconfiguracaodiferenciada = $escolasconfiguracoesdiferenciadas->getEscolaconfiguracaodiferenciadaById($id, array());
		
		if (!$escolaconfiguracaodiferenciada) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolaconfiguracaodiferenciada;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Configurações Diferenciadas')
		);
		
		$ns = new Zend_Session_Namespace('default_escolasconfiguracoesdiferenciadas');
		$escolasconfiguracoesdiferenciadas = new Escolasconfiguracoesdiferenciadas();
		
		$queries = array();	
		$queries['group_by'] = ' GROUP BY e1.id ';

		$perfil = Usuarios::getUsuario('idperfil');
		$iduser = Usuarios::getUsuario('id');
		if($perfil==29) {			
			$escola = Usuarios::getEscolaAtiva($iduser);			
			$queries["idescola"] = $escola["id"];
		}
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if($perfil==29) {
				$escola = Usuarios::getEscolaAtiva($iduser);			
				$queries["idescola"] = $escola["id"];
			}else{
				if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			}
			
			if ($this->view->post_var["idserie"]!="") $queries["idserie"] = $this->view->post_var["idserie"];
			if ($this->view->post_var["idmateria"]!="") $queries["idmateria"] = $this->view->post_var["idmateria"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $escolasconfiguracoesdiferenciadas->getEscolasconfiguracoesdiferenciadas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $escolasconfiguracoesdiferenciadas->getEscolasconfiguracoesdiferenciadas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de escolasconfiguracoesdiferenciadas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasconfiguracoesdiferenciadas', 'name' => 'Configurações Diferenciadas'),
			array('url' => null,'name' => 'Editar Configuração Diferenciada')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$escolasconfiguracoesdiferenciadas = new Escolasconfiguracoesdiferenciadas();
		$escolaconfiguracaodiferenciada = $escolasconfiguracoesdiferenciadas->getEscolaconfiguracaodiferenciadaById($id);
		
		if (!$escolaconfiguracaodiferenciada) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolaconfiguracaodiferenciada;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($escolaconfiguracaodiferenciada);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Configuração Diferenciada editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de escolasconfiguracoesdiferenciadas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasconfiguracoesdiferenciadas', 'name' => 'Configurações Diferenciadas'),
			array('url' => null,'name' => 'Adicionar Configuração Diferenciada')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Configuração Diferenciada adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idescolaconfiguracaodiferenciada
     */    
    private function preForm($idescolaconfiguracaodiferenciada = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_escolaconfiguracaodiferenciada = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idserie = $this->getRequest()->getPost("idserie");
		$idmateria = (int)trim($this->getRequest()->getPost("idmateria"));
		$status = trim($this->getRequest()->getPost("status1"));
		$idtitulos = $this->getRequest()->getPost("idstitulos");
		$percentualnota = $this->getRequest()->getPost("percentualnotas");
		
		
		//pegar apenas os campos preenchidos
		$novopercentualnotas = array();
		foreach($percentualnota as $k=> $v){
			if($v != 0) array_push($novopercentualnotas, $v);			
		}
		if(is_array($idserie)) $idsseries = implode($idserie, ",");


		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
		if (empty($idsseries)) array_push($erros, "Informe a Série.");
		if (0==$idmateria) array_push($erros, "Informe a Matéria.");
		if (""==$status) array_push($erros, "Informe a Status.");
		if(count($novopercentualnotas) != count($idtitulos)) array_push($erros, "Inserir notas para todos os títulos marcados");

		//transformar array em string
		if(is_array($idtitulos)) $idstitulos = implode($idtitulos, ",");
		if(is_array($novopercentualnotas)) $novopercentualnotas = implode($novopercentualnotas, ",");

		$escolasconfiguracoesdiferenciadas = new Escolasconfiguracoesdiferenciadas();
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
			$dados["idsseries"] = $idsseries;
			$dados["idmateria"] = $idmateria;
			$dados["status"] = $status;

			$dados['idstitulos'] = $idstitulos;
			$dados['percentualnotas'] = $novopercentualnotas;		

			/*$notas = array();
			$notas['idstitulos'] = $idstitulos;
			$notas['percentualnotas'] = $percentualnotas;
			$notas['logusuario'] = $this->_usuario['id'];
			$dados['notas'] = $notas;*/
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $escolasconfiguracoesdiferenciadas->save($dados);
			if($id===0){
                Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Configuração diferenciada adicionada com sucesso!");
            }else Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Configuração diferenciada - ID -> ".$id." atualizada com sucesso!");
						
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    
	public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
    	
		//$series = new Escolasseries();
    	$this->view->rows = Escolasseries::getEscolasseriesHelper(array('idescola'=>$idescola, 'status'=>'Ativo', 'order'=>'ORDER BY id', 'idsecretaria'=> Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' ))); 
    	//var_dump($this->view->rows);die();
	}
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}