<?php

class Admin_PrefeiturasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacoteprefeitura
	 */
	protected $_prefeitura = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("prefeituras", $this->_request->getActionName());	
		
		$this->_prefeitura = unserialize($loginNameSpace->prefeitura);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}	
	

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Prefeituras();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Usuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Cliente excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="prefeituras") $objs = new Prefeituras();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Usuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Clientes')
		);
		
		$ns = new Zend_Session_Namespace('admin_prefeituras');
		$prefeituras = new Prefeituras();
		$queries = array();	
		//$queries['idssecretarias'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ((isset($this->view->post_var['status'])) && ($this->view->post_var['status']!='')) $queries['status'] = $this->view->post_var['status'];
    		if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $prefeituras->getPrefeituras($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $prefeituras->getPrefeituras($queries, $paginaAtual, $maxpp);	
	}
	
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'prefeituras',	'name' => 'Clientes'),
			array('url' => null,'name' => 'Editar Cliente')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$prefeituras = new Prefeituras();
		$prefeitura = $prefeituras->getPrefeituraById($id);
		
		if (!$prefeitura) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $prefeitura;
			
		$endereco = Enderecos::getEnderecoById($prefeitura['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['idestado']);
			//unset($endereco['idcidade']);
			unset($endereco['excluido']);
			$this->view->post_var = array_merge($this->view->post_var, $endereco);
		}
		$this->preForm();
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de prefeitura
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'prefeituras',	'name' => 'Clientes'),
			array('url' => null,'name' => 'Editar Cliente')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$prefeituras = new Prefeituras();
		$prefeitura = $prefeituras->getPrefeituraById($id);
		
		if (!$prefeitura) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $prefeitura;
		
		$endereco = Enderecos::getEnderecoById($prefeitura['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['idestado']);
			unset($endereco['idcidade']);
			unset($endereco['excluido']);
			$this->view->post_var = array_merge($this->view->post_var, $endereco);
		}	
		//var_dump($endereco); die();
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($prefeitura);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Cliente editado com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de prefeituras 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'prefeituras',	'name' => 'Clientes'),
			array('url' => null,'name' => 'Adicionar Cliente')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Cliente adicionado com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
	public function setcidadesAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idestado = $this->getRequest()->getPost('idestado');
    	
        $cidades= new Cidades();
        $row_cidade = $cidades->fetchAll("idestado=" . $idestado . " ");
    	$this->view->rows = $row_cidade;
	}
    
    /**
     * Atribui valores ao view
     * @param int $idprefeitura
     */    
    private function preForm($idprefeitura = 0) {
		$estados = new Estados();
		$this->view->estados = $estados->getEstados();
		
		$secretarias = new Secretarias();
		$this->view->secretarias = $secretarias->getSecretarias(array('status'=>'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idestado = strip_tags(trim($this->getRequest()->getPost("idestado")));
		$idcidade = strip_tags(trim($this->getRequest()->getPost("idcidade")));
		$idendereco = strip_tags((int)$this->getRequest()->getPost("idendereco"));
		$idssecretarias = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		//var_dump($idssecretarias); die();
		//if (!is_array($idssecretarias)) $idssecretarias = array();
		//$idssecretarias = implode(',', $idssecretarias);
		//var_dump($idssecretarias);die();
		
		$cpfcnpj = strip_tags(trim($this->getRequest()->getPost("cpfcnpj")));
		$cidade = strip_tags(trim($this->getRequest()->getPost("cidade")));
		$telefone = strip_tags(trim($this->getRequest()->getPost("telefone")));
		$contato = strip_tags(trim($this->getRequest()->getPost("contato")));
		
		$datacontrato = strip_tags(trim($this->getRequest()->getPost("datacontrato")));
		$datacontrato = Mn_Util::stringToTime($datacontrato);		
		$datacontrato = date('Y-m-d G:i:s', $datacontrato);	
		
		$databloqueio = strip_tags(trim($this->getRequest()->getPost("databloqueio")));
		$databloqueio = Mn_Util::stringToTime($databloqueio);		
		$databloqueio = date('Y-m-d G:i:s', $databloqueio);	
		
		$prefeito = strip_tags(trim($this->getRequest()->getPost("prefeito")));
		$secretario = strip_tags(trim($this->getRequest()->getPost("secretario")));
		$partido = strip_tags(trim($this->getRequest()->getPost("partido")));
		$texto = strip_tags(trim($this->getRequest()->getPost("texto")));	
		$status = strip_tags(trim($this->getRequest()->getPost("status")));
		
		$erros = array();

		if (""==$prefeito) array_push($erros, 'Preencha o campo PREFEITURA.');
		if (0>=$idestado) array_push($erros, 'Selecione um ESTADO.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$prefeituras = new Prefeituras();
		//$row = $prefeituras->fetchRow("excluido='nao' AND prefeitura='$prefeitura' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe registro com esse nome de E-MAIL.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {	
			$idendereco = ($_registro) ? (int)$_registro['idendereco'] : 0;
			$enderecosModel = new EnderecoModel($this);
			$endereco = $enderecosModel->getPost($idendereco);
			$idendereco = ($endereco) ? $endereco['id'] : 0;
			
			if($id<=0):
				//cria secretaria 

				$secretarias = new Secretarias();
				$dadossecretarias = array();
				$dadossecretarias['idendereco'] = $idendereco;
				$dadossecretarias['secretaria'] = ((isset($texto)) && ($texto) && ($texto!="")) ? $texto : $cidade;
				$dadossecretarias['telefone'] = $telefone;                     
				$dadossecretarias['status'] = $status;
				$dadossecretarias['excluido'] = 'nao';
				$dadossecretarias['logusuario'] = Usuarios::getUsuario('id');
				$dadossecretarias['logdata'] = date('Y-m-d G:i:s');
				$_row = $secretarias->save($dadossecretarias);
				$idssecretarias = $_row['id'];

				$usuarios = new Usuarios();
				$dadosusuarios = array();
				$dadosusuarios['id'] = Usuarios::getUsuario('id');			
				$dadosusuarios['idsecretarias'] = (Usuarios::getUsuario('idsecretarias')) ? Usuarios::getUsuario('idsecretarias') . ',' . $idssecretarias : $idssecretarias;
				$dadosusuarios['logusuario'] = Usuarios::getUsuario('id');
				$dadosusuarios['logdata'] = date('Y-m-d G:i:s');

				$usuarios->save($dadosusuarios);
			endif;


			$idimagem = $this->getImagem();
			//var_dump($idimagem); die();
			$dados = array();
			$dados['id'] = $id;
			$dados['idimagem'] = $idimagem;
			$dados['idestado'] = $idestado;
			$dados['idcidade'] = $idcidade;
			$dados['idendereco'] = $idendereco;
			$dados['idssecretarias'] = $idssecretarias;
			$dados['cpfcnpj'] = $cpfcnpj;
			$dados['telefone'] = $telefone;
			$dados['contato'] = $contato;
			$dados['datacontrato'] = $datacontrato;
			$dados['databloqueio'] = $databloqueio;
			$dados['prefeito'] = $prefeito;
			$dados['secretario'] = $secretario;
			$dados['partido'] = $partido;
			$dados['texto'] = $texto;		
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Usuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			$row = $prefeituras->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }

    private function getImagem($apenas_copia = false) {
		$idimagem = false;
		$imagens = new Imagens();
		try {
			ini_set('memory_limit', '-1');
			$idimagem = $imagens->getImagemFromForm('imagem', NULL, NULL, $apenas_copia);
		} catch (Exception $e) {
			$idimagem = false;
			array_push($erros,$e->getMessage());
		}
		$excluir_imagem = trim($this->getRequest()->getPost("excluir_imagem"));
		if ($excluir_imagem=='excluir_imagem') $idimagem = -1;
		return $idimagem;		    	
    }
	
	
}