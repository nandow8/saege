<?php

/**
 * Controle da classe tihistoricosequipamentos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_TihistoricosequipamentosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Tihistoricosequipamento
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("tihistoricosequipamentos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Tihistoricosequipamentos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Histórico excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="tihistoricosequipamentos") $objs = new Tihistoricosequipamentos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tihistoricosequipamentos', 'name' => 'Históricos'),
			array('url' => null,'name' => 'Visualizar Histórico')
		);
		
		$id = (int)$this->_request->getParam("id");
		$tihistoricosequipamentos = new Tihistoricosequipamentos();
		$tihistoricosequipamento = $tihistoricosequipamentos->getTihistoricosequipamentoById($id, array());
		
		if (!$tihistoricosequipamento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $tihistoricosequipamento;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Históricos')
		);
		
		$ns = new Zend_Session_Namespace('default_tihistoricosequipamentos');
		$tihistoricosequipamentos = new Tihistoricosequipamentos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
if ($this->view->post_var["idtecnico"]!="") $queries["idtecnico"] = $this->view->post_var["idtecnico"];
if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["idpatrimonio"]!="") $queries["idpatrimonio"] = $this->view->post_var["idpatrimonio"];
if ($this->view->post_var["trocapecas"]!="") $queries["trocapecas"] = $this->view->post_var["trocapecas"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $tihistoricosequipamentos->getTihistoricosequipamentos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $tihistoricosequipamentos->getTihistoricosequipamentos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de tihistoricosequipamentos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tihistoricosequipamentos', 'name' => 'Históricos'),
			array('url' => null,'name' => 'Editar Histórico')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$tihistoricosequipamentos = new Tihistoricosequipamentos();
		$tihistoricosequipamento = $tihistoricosequipamentos->getTihistoricosequipamentoById($id);
		
		if (!$tihistoricosequipamento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $tihistoricosequipamento;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($tihistoricosequipamento);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Histórico editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de tihistoricosequipamentos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tihistoricosequipamentos', 'name' => 'Históricos'),
			array('url' => null,'name' => 'Adicionar Histórico')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Histórico adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idtihistoricosequipamento
     */    
    private function preForm($idtihistoricosequipamento = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_tihistoricosequipamento = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
$idtecnico = (int)trim($this->getRequest()->getPost("idtecnico"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$idpatrimonio = (int)trim($this->getRequest()->getPost("idpatrimonio"));
$trocapecas = trim($this->getRequest()->getPost("trocapecas"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$data) array_push($erros, "Informe a Data.");
if (0==$idtecnico) array_push($erros, "Informe a Técnicos.");
if (0==$idescola) array_push($erros, "Informe a Escola.");
if (0==$idpatrimonio) array_push($erros, "Informe a Patrimônio.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$tihistoricosequipamentos = new Tihistoricosequipamentos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["data"] = date("Y-m-d", $data);
$dados["idtecnico"] = $idtecnico;
$dados["idescola"] = $idescola;
$dados["idpatrimonio"] = $idpatrimonio;
$dados["trocapecas"] = $trocapecas;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $tihistoricosequipamentos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__ ."::incluir", Usuarios::getUsuario("id"), "Tihistoricosequipamentos " . $e->getMessage(), "E");
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}