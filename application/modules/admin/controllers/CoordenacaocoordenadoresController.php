<?php

/**
 * Controle da classe coordenacaocoordenadores do sistema
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_CoordenacaocoordenadoresController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Coordenacaocoordenador
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("coordenacaocoordenadores", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Coordenacaocoordenadores();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Coordenador excluído com sucesso.";

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "coordenacaocoordenadores")
            $objs = new Coordenacaocoordenadores();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'coordenacaocoordenadores', 'name' => 'Coordenadores'),
            array('url' => null, 'name' => 'Visualizar Coordenador')
        );

        $id = (int) $this->_request->getParam("id");
        $coordenacaocoordenadores = new Coordenacaocoordenadores();
        $coordenacaocoordenador = $coordenacaocoordenadores->getCoordenacaocoordenadorById($id, array());

        if (!$coordenacaocoordenador)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $coordenacaocoordenador;
        $this->preForm();


        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Coordenadores')
        );

        $ns = new Zend_Session_Namespace('default_coordenacaocoordenadores');
        $coordenacaocoordenadores = new Coordenacaocoordenadores();
        $queries = array();
        //$queries['idfuncionario'] = (int)Usuarios::getUsuario('idfuncionario');
        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ((isset($this->view->post_var["idfuncionario"])) && ($this->view->post_var["idfuncionario"] != ""))
                $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
            if ((isset($this->view->post_var["email"])) && ($this->view->post_var["email"] != ""))
                $queries["email"] = $this->view->post_var["email"];
            if ($this->view->post_var["status1"] != "")
                $queries["status"] = $this->view->post_var["status1"];

            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $coordenacaocoordenadores->getCoordenacaocoordenadores($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $coordenacaocoordenadores->getCoordenacaocoordenadores($queries, $paginaAtual, $maxpp);
    }

    /**
     *
     * Action de edição de coordenacaocoordenadores
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'coordenacaocoordenadores', 'name' => 'Coordenadores'),
            array('url' => null, 'name' => 'Editar Coordenador')
        );

        $id = (int) $this->_request->getParam("id");
        $coordenacaocoordenadores = new Coordenacaocoordenadores();
        $coordenacaocoordenador = $coordenacaocoordenadores->getCoordenacaocoordenadorById($id);

        if (!$coordenacaocoordenador)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $coordenacaocoordenador;
        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($coordenacaocoordenador);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Coordenador editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     *
     * Action de adição de coordenacaocoordenadores
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'coordenacaocoordenadores', 'name' => 'Coordenadores'),
            array('url' => null, 'name' => 'Adicionar Coordenador')
        );

        $this->view->adicionar = true;
        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Coordenador adicionado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idcoordenacaocoordenador
     */
    private function preForm($idcoordenacaocoordenador = 0) {

    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_coordenacaocoordenador = false) {

        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $idsecretaria = (int) trim($this->getRequest()->getPost("idsecretaria"));
        $idfuncionario = (int) trim($this->getRequest()->getPost("idfuncionario"));
        $nomerazao = trim($this->getRequest()->getPost("nomerazao"));
        $sobrenomefantasia = trim($this->getRequest()->getPost("sobrenomefantasia"));
        $telefone = trim($this->getRequest()->getPost("telefone"));
        $email = trim($this->getRequest()->getPost("email"));
        $status = trim($this->getRequest()->getPost("status1"));
        $idsescolas = $this->getRequest()->getPost("idsescolas");
        if (!is_array($idsescolas))
            $idsescolas = array();
        $idsescolas = implode(',', $idsescolas);

        $erros = array();

        //  if (0==$idfuncionario) array_push($erros, "Informe a Funcionário.");
        if ("" == $email)
            array_push($erros, "Informe a E-mail.");
        if ("" == $status)
            array_push($erros, "Informe a Status.");

        $coordenacaocoordenadores = new Coordenacaocoordenadores();

        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;

            $dados["idsecretaria"] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
            $dados['idsescolas'] = $idsescolas;
            $dados["idfuncionario"] = $idfuncionario;
            $dados["nomerazao"] = $nomerazao;
            $dados["sobrenomefantasia"] = $sobrenomefantasia;
            $dados["telefone"] = $telefone;
            $dados["email"] = $email;
            $dados["status"] = $status;


            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            ;
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $coordenacaocoordenadores->save($dados);

            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();

            $db->rollBack();
            die();
        }

        return "";
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

}
