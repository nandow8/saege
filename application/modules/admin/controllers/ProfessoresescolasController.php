<?php

/**
 * Controle da classe funcionariosgeraisescolas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ProfessoresescolasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Funcionariogeralescola
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("professoresescolas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Funcionariosgeraisescolas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="professoresescolas") $objs = new Funcionariosgeraisescolas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'professoresescolas', 'name' => 'Funcionários'),
			array('url' => null,'name' => 'Visualizar Funcionário')
		);
		
		$id = (int)$this->_request->getParam("id");
		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
		$funcionariogeralescola = $funcionariosgeraisescolas->getFuncionariogeralescolaById($id, array());
		
		if (!$funcionariogeralescola) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $funcionariogeralescola;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Funcionários')
		);
		
		$ns = new Zend_Session_Namespace('default_professoresescolas');
		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
		$queries = array();	
		$queries['professor'] = "Sim";	
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ((isset($this->view->post_var["nome"])) && ($this->view->post_var["nome"]!="")) $queries["nome"] = $this->view->post_var["nome"];
if ((isset($this->view->post_var["rgf"])) && ($this->view->post_var["rgf"]!="")) $queries["rgf"] = $this->view->post_var["rgf"];
if ((isset($this->view->post_var["idescola"])) && ($this->view->post_var["idescola"]!="")) $queries["idescola"] = $this->view->post_var["idescola"];
if ((isset($this->view->post_var["tipo"])) && ($this->view->post_var["tipo"]!="")) $queries["tipo"] = $this->view->post_var["tipo"];
if ((isset($this->view->post_var["status1"])) && ($this->view->post_var["status1"]!="")) $queries["status"] = $this->view->post_var["status1"];
if ((isset($this->view->post_var["tipoensino"])) && ($this->view->post_var["tipoensino"]!="")) $queries["tipoensino"] = $this->view->post_var["tipoensino"];    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $funcionariosgeraisescolas->getFuncionariosgeraisescolas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $funcionariosgeraisescolas->getFuncionariosgeraisescolas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de professoresescolas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'professoresescolas', 'name' => 'Funcionários'),
			array('url' => null,'name' => 'Editar Funcionário')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
		$funcionariogeralescola = $funcionariosgeraisescolas->getFuncionariogeralescolaById($id);
		
		if (!$funcionariogeralescola) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $funcionariogeralescola;
		$escolasusuarios = new Escolasusuarios();
		$escolausuario = $escolasusuarios->getEscolausuarioByIdfuncionario($id);
		
		if($escolausuario){
			unset($escolausuario['id']);
			unset($escolausuario['excluido']);
			unset($escolausuario['status']);
			unset($escolausuario['logdata']);
			unset($escolausuario['logusuario']);	
			unset($escolausuario['datacriacao']);
			$this->view->post_var = array_merge($this->view->post_var, $escolausuario);
		}
		//var_dump($this->view->post_var); die();
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($funcionariogeralescola);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de professoresescolas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'professoresescolas', 'name' => 'Funcionários'),
			array('url' => null,'name' => 'Adicionar Funcionário')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function setmateriasAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
    	if((int)$idescola <=0) return $this->view->rows = array();
		$materias = new Controlealunosmaterias();
    	$this->view->rows = $materias->getControlealunosmaterias(array('idescola'=>$idescola, 'status'=>'Ativo')); 
	}

	public function setcargosescolasAction() {

		$this->_helper->layout->disableLayout();
		
		$iddepartamentoescola = (int)$this->_request->getPost("iddepartamentoescola");
		
		$idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );

		$cargos = new Escolascargos();
		$this->view->rows = $cargos->getEscolascargos(array('iddepartamentoescola'=>$iddepartamentoescola));


	}

	public function setfuncoesescolasAction() {

		$this->_helper->layout->disableLayout();
		
		$idcargo = (int)$this->_request->getPost("idcargo");

		$idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );

		$funcoes = new Escolasfuncoes();
		$this->view->rows = $funcoes->getEscolasfuncoes(array('idcargo'=>$idcargo));


	}

	public function setdepartamentosescolasAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$escolas = Escolas::getEscolaByIdHelper($idescola);
		if(!$idescola) die('<option value="">Não encontrado.</option>');

		$departamentos = new Departamentosescolas();
		$this->view->rows = $departamentos->getDepartamentosescolas(array('idescola'=>$idescola));

	}

    /**
     * Atribui valores ao view
     * @param int $idfuncionariogeralescola
     */    
    private function preForm($idfuncionariogeralescola = 0) {
    	       $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        
    	$perfis = new Escolasusuariosperfis();
    	$this->view->perfis = $perfis->getPerfis(false, array('idsecretaria' => $this->view->idsecretaria));  
            
		$escolas = new Escolas();
		$this->view->escolas = $escolas->getEscolas(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
        $cargos = new Escolascargos();
		$this->view->cargos = $cargos->getEscolascargos(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		$departamentos = new Departamentosescolas();
		$this->view->departamentos = $departamentos->getDepartamentosescolas(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		$cargos_sys = $cargos->getCargoSys('SYS');
		$perfis_sys = $perfis->getPerfilByIdSecretaria(Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' ), array('origem'=>'SYS'));
		
		$dados_professores = array();
		$dados_professores['idperfil'] = $perfis_sys['id'];
		$dados_professores['iddepartamento'] = $cargos_sys['iddepartamentoescola'];

		$dados_professores['idcargo'] = $cargos_sys['id'];
		
		$this->view->post_var_professor = $dados_professores;
		
		
		$prefeituras = new Prefeituras();
		$this->view->prefeituras = $prefeituras->getPrefeituras(array('status'=>'Ativo', 'idssecretarias'=>Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' )));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_funcionariogeralescola = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$iddepartamentoescola = (int)trim($this->getRequest()->getPost("iddepartamentoescola"));
$idfuncao = (int)trim($this->getRequest()->getPost("idfuncao"));
$idcargo = (int)trim($this->getRequest()->getPost("idcargo"));
$idimagem = (int)trim($this->getRequest()->getPost("idimagem"));
$datafimbolsa = trim($this->getRequest()->getPost("datafimbolsa"));
$datainiciobolsa = trim($this->getRequest()->getPost("datainiciobolsa"));
$bolsista = trim($this->getRequest()->getPost("bolsista"));
$regime_outros = trim($this->getRequest()->getPost("regime_outros"));
$regime = trim($this->getRequest()->getPost("regime"));
$conta = trim($this->getRequest()->getPost("conta"));
$dataregistroctps = trim($this->getRequest()->getPost("dataregistroctps"));
$ctps = trim($this->getRequest()->getPost("ctps"));
$filiacaopai = trim($this->getRequest()->getPost("filiacaopai"));
$filiacaomae = trim($this->getRequest()->getPost("filiacaomae"));
$carteiraserie = trim($this->getRequest()->getPost("carteiraserie"));
$carteiranumero = trim($this->getRequest()->getPost("carteiranumero"));
$celular = trim($this->getRequest()->getPost("celular"));
$agencia = trim($this->getRequest()->getPost("agencia"));
$banco = trim($this->getRequest()->getPost("banco"));
$formapagamento = trim($this->getRequest()->getPost("formapagamento"));
$salarioinicial = MN_Util::trataNum(trim($this->getRequest()->getPost("salarioinicial")));
$riscofuncao = trim($this->getRequest()->getPost("riscofuncao"));
$descricaofuncao = trim($this->getRequest()->getPost("descricaofuncao"));
$funcao = trim($this->getRequest()->getPost("funcao"));
$setor = trim($this->getRequest()->getPost("setor"));
$secretaria = trim($this->getRequest()->getPost("secretaria"));
$matricula = trim($this->getRequest()->getPost("matricula"));
$dataadmissaocomissionado = trim($this->getRequest()->getPost("dataadmissaocomissionado"));
$cargocomissionado = trim($this->getRequest()->getPost("cargocomissionado"));
$cargo = trim($this->getRequest()->getPost("cargo"));
$cbo = trim($this->getRequest()->getPost("cbo"));
$dataadmissao = trim($this->getRequest()->getPost("dataadmissao"));
$filhosquantidade = trim($this->getRequest()->getPost("filhosquantidade"));
$filhos = trim($this->getRequest()->getPost("filhos"));
$conjugenome = trim($this->getRequest()->getPost("conjugenome"));
$estadocivil = trim($this->getRequest()->getPost("estadocivil"));
$grauinstrucao = trim($this->getRequest()->getPost("grauinstrucao"));
$localnascimento = trim($this->getRequest()->getPost("localnascimento"));
$nacionalidade = trim($this->getRequest()->getPost("nacionalidade"));
$obs_estrangeiro = trim($this->getRequest()->getPost("obs_estrangeiro"));
$estrangeiro = trim($this->getRequest()->getPost("estrangeiro"));
$cnh_validade = trim($this->getRequest()->getPost("cnh_validade"));
$cnh_categoria = trim($this->getRequest()->getPost("cnh_categoria"));
$cnh_numero = trim($this->getRequest()->getPost("cnh_numero"));
$cnh = trim($this->getRequest()->getPost("cnh"));
$pispasep = trim($this->getRequest()->getPost("pispasep"));
$cpf = trim($this->getRequest()->getPost("cpf"));
$rg = trim($this->getRequest()->getPost("rg"));
$secaoeleitoral = trim($this->getRequest()->getPost("secaoeleitoral"));
$zonaeleitoral = trim($this->getRequest()->getPost("zonaeleitoral"));
$tituloeleitor = trim($this->getRequest()->getPost("tituloeleitor"));
$reservista_ra = trim($this->getRequest()->getPost("reservista_ra"));
$reservista = trim($this->getRequest()->getPost("reservista"));
$emissaocarteira = trim($this->getRequest()->getPost("emissaocarteira"));
$datanascimento = trim($this->getRequest()->getPost("datanascimento"));
$email = trim($this->getRequest()->getPost("email"));
$sobrenomefantasia = trim($this->getRequest()->getPost("sobrenomefantasia"));
$nomerazao = trim($this->getRequest()->getPost("nomerazao"));
$professor = "Sim";
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$nome = trim($this->getRequest()->getPost("nome"));
$telefone = trim($this->getRequest()->getPost("telefone"));
$modulo = trim($this->getRequest()->getPost("modulo"));
$tipo = trim($this->getRequest()->getPost("tipo"));
$idendereco = (int)trim($this->getRequest()->getPost("idendereco"));
$idprefeitura = (int)trim($this->getRequest()->getPost("idprefeitura"));
$tipoensino = trim($this->getRequest()->getPost("tipoensino"));
$status = trim($this->getRequest()->getPost("status1"));
		$idsmaterias = $this->getRequest()->getPost("idsmaterias");   
		$idsmaterias = implode(',',$idsmaterias);

			$idescola = strip_tags((int)$this->getRequest()->getPost("idescola"));
		$idcargo = strip_tags((int)$this->getRequest()->getPost("idcargo"));
		$idperfil = strip_tags((int)trim($this->getRequest()->getPost("idperfil")));
		$idprefeitura = strip_tags((int)trim($this->getRequest()->getPost("idprefeitura")));
		$rgf = strip_tags(trim($this->getRequest()->getPost("rgf")));
		$email = strip_tags(trim($this->getRequest()->getPost("email")));
		$senha = strip_tags(trim($this->getRequest()->getPost("senha")));		
		$professor = "Sim";
		
		/**/
		$idperfilprofessor = strip_tags((int)$this->getRequest()->getPost("idperfilprofessor"));
		$iddepartamentoprofessor = strip_tags((int)$this->getRequest()->getPost("iddepartamentoprofessor"));
		$idcargoprofessor = strip_tags((int)trim($this->getRequest()->getPost("idcargoprofessor")));
		/**/
		
		$funcoes = strip_tags(trim($this->getRequest()->getPost("funcoes")));
		$unidadesdesenvolvimento = strip_tags(trim($this->getRequest()->getPost("unidadesdesenvolvimento")));
		$observacoes = strip_tags(trim($this->getRequest()->getPost("observacoes")));
		$periodoatividades = strip_tags(trim($this->getRequest()->getPost("periodoatividades")));
		$situacaogestao = strip_tags(trim($this->getRequest()->getPost("situacaogestao")));
		$recebesolicitacoes = strip_tags(trim($this->getRequest()->getPost("recebesolicitacoes")));		
$enviaemails = strip_tags(trim($this->getRequest()->getPost("enviaemails")));
$idsdepartamentos = $this->getRequest()->getPost("idsdepartamentos");
if (!is_array($idsdepartamentos)) $idsdepartamentos = array();
$idsdepartamentos = implode(',',$idsdepartamentos);

				$idescolas = $this->getRequest()->getPost("idescolas");
		if (!is_array($idescolas)) $idescolas = array();
		$idescolas = implode(',',$idescolas);		
		$erros = array();
		
		//if (""==$tipo) array_push($erros, "Informe a Tipo.");
if (""==$status) array_push($erros, "Informe a Status.");
		$__rows = new Escolasusuarios();
		if($email){
		//	$row = $rows->fetchRow("excluido='nao' AND email='$email' AND id<>".$id);
		//	if ($row) array_push($erros, 'Já existe usuário com esse EMAIL DE ACESSO.');
		}
		//if (""==$rgf) array_push($erros, 'Preencha o campo RGF.');
		$_row = $__rows->fetchRow("excluido='nao' AND rgf='$rgf' AND idfuncionario<>".$id);
		if ($_row) array_push($erros, 'Já existe usuário com esse RGF.');	
		
		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			$dados["nome"] = $nome;	
			$dados["telefone"] = $telefone;	
			$dados["idsecretaria"] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
	$dados["idescola"] = $idescola;		
$dados["idfuncao"] = $idfuncao;		
$dados["idcargo"] = $idcargo;			
			$enderecosModel = new EnderecoModel($this);
			$endereco = $enderecosModel->getPost( ($_funcionariogeralescola) ? (int)$_funcionariogeralescola['idendereco'] : 0, "e_idendereco");
			$idendereco = ($endereco) ? $endereco['id'] : 0;	
			$dados['idendereco'] = $idendereco;

$idimagem = $this->getImagem('idimagem');
			if ($idimagem!=0) $dados['idimagem'] = $idimagem;
$dados["tipoensino"] = $tipoensino;
$dados["matricula"] = $matricula;
$dados["secretaria"] = $secretaria;
$dados["iddepartamentoescola"] = $iddepartamentoescola;
$dados["setor"] = $setor;
$dados["funcao"] = $funcao;
$dados["descricaofuncao"] = $descricaofuncao;
$dados["riscofuncao"] = $riscofuncao;
$dados["email"] = $email;
$dados["datanascimento"] = date("Y-m-d", $datanascimento);
$dados["emissaocarteira"] = date("Y-m-d", $emissaocarteira);
$dados["reservista"] = $reservista;
$dados["rg"] = $rg;
$dados["cpf"] = $cpf;
$dados["pispasep"] = $pispasep;
$dados["cnh"] = $cnh;
$dados["cnh_numero"] = $cnh_numero;
$dados["cnh_categoria"] = $cnh_categoria;
$dados["cnh_validade"] = date("Y-m-d", $cnh_validade);
$dados["estrangeiro"] = $estrangeiro;
$dados["obs_estrangeiro"] = $obs_estrangeiro;
$dados["nacionalidade"] = $nacionalidade;
$dados["localnascimento"] = $localnascimento;
$dados["grauinstrucao"] = $grauinstrucao;
$dados["estadocivil"] = $estadocivil;
$dados["conjugenome"] = $conjugenome;
$dados["filhos"] = $filhos;
$dados["filhosquantidade"] = $filhosquantidade;
$dados["celular"] = $celular;
$dados["dataadmissao"] = date("Y-m-d", $dataadmissao);
$dados["carteiranumero"] = $carteiranumero;
$dados["carteiraserie"] = $carteiraserie;
$dados["filiacaomae"] = $filiacaomae;
$dados["filiacaopai"] = $filiacaopai;
$dados["ctps"] = $ctps;
$dados["dataregistroctps"] = date("Y-m-d", $dataregistroctps);
$dados["regime"] = $regime;
$dados["regime_outros"] = $regime_outros;
$dados["bolsista"] = $bolsista;
$dados['professor'] = $professor;
$dados["datainiciobolsa"] = date("Y-m-d", $datainiciobolsa);
$dados["datafimbolsa"] = date("Y-m-d", $datafimbolsa);
$dados["status"] = $status;
$dados['idprefeitura'] = $idprefeitura;

			$dados["idsmaterias"] = $idsmaterias;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
				
			$row = $funcionariosgeraisescolas->save($dados);
			


		$escolasusuarios = new Escolasusuarios();
		$escolausuario = $escolasusuarios->getEscolausuarioByIdfuncionario($row['id']);
			if($professor=="Sim"){
				
			    $perfis = new Escolasusuariosperfis();
			    $perfisprofessor = $perfis->getPerfilOrigem('SYS');
				
				
				$_idescolas = explode(',', $idescolas);
				//$idescola = $_idescolas[0];
				$idperfil = $perfisprofessor['id'];
				$idcargo = $idcargoprofessor;
				//
			}


			$dados_usuarios = array();
			if(isset($escolausuario['id'])) $dados_usuarios['id'] = $escolausuario['id'];
			$dados_usuarios['idfuncionario'] = $row['id'];
			$dados_usuarios['idescola'] = $idescola;
			$dados_usuarios['idescolas'] = $idescola;
			$dados_usuarios['idcargo'] = $idcargo;
			$dados_usuarios['idperfil'] = $idperfil;
			$dados_usuarios['idprefeitura'] = $idprefeitura;
			$dados_usuarios['nomerazao'] = $nome;
			$dados_usuarios['rgf'] = $rgf;
			$dados_usuarios['email'] = $email;
			$dados_usuarios['senha'] = $senha;
			$dados_usuarios['status'] = $status;	
			$dados_usuarios['professor'] = $professor;			
			$dados_usuarios['funcoes'] = $funcoes;
			$dados_usuarios['unidadesdesenvolvimento'] = $unidadesdesenvolvimento;
			$dados_usuarios['observacoes'] = $observacoes;
			$dados_usuarios['periodoatividades'] = $periodoatividades;
			$dados_usuarios['situacaogestao'] = $situacaogestao;			
			$dados_usuarios['enviaemails'] = $enviaemails;
			$dados_usuarios['idsdepartamentos'] = $idsdepartamentos;
			$dados_usuarios['recebesolicitacoes'] = $recebesolicitacoes;		
			$dados_usuarios['excluido'] = 'nao';
			$dados_usuarios['logusuario'] = Usuarios::getUsuario('id');
			$dados_usuarios['logdata'] = date('Y-m-d G:i:s');
			if((int)$idescola <= 0) {
				var_dump($_POST); die();
			}

			//$escolasusuarios->save($dados_usuarios);

//var_dump($row); die('222');	

			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}