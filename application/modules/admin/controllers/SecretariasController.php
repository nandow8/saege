<?php

class Admin_SecretariasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacotesecretaria
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("secretarias", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}	
	

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Secretarias();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Usuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Secretaria excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="secretarias") $objs = new Secretarias();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Usuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Secretarias();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Secretarias();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/admin/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Secretarias')
		);
		
		
		$ns = new Zend_Session_Namespace('admin_secretarias');
		$secretarias = new Secretarias();
		$queries = array();	
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ($this->view->post_var['status']!='') $queries['status'] = $this->view->post_var['status'];
    		if ($this->view->post_var['chave']!='') $queries['chave'] = $this->view->post_var['chave'];
    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $secretarias->getSecretarias($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $secretarias->getSecretarias($queries, $paginaAtual, $maxpp);	
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretarias',	'name' => 'Secretarias'),
			array('url' => null,'name' => 'Visualizar secretaria')
		);
	
		$id = (int)$this->_request->getParam("id");
		$secretarias = new Secretarias();
		$secretaria = $secretarias->getSecretariaById($id);
		
		if (!$secretaria) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		$this->view->visualizar = true;
		$this->view->post_var = $secretaria;
		$endereco = Enderecos::getEnderecoById((int)$secretaria['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->post_var = array_merge($this->view->post_var, $endereco);
		}
                
		$this->preForm();
	
		
	}
	
	/**
	 * 
	 * Action de edição de secretaria
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretarias',	'name' => 'Secretarias'),
			array('url' => null,'name' => 'Editar secretaria')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$secretarias = new Secretarias();
		$secretaria = $secretarias->getSecretariaById($id);
		
		if (!$secretaria) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $secretaria;
		$endereco = Enderecos::getEnderecoById((int)$secretaria['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->post_var = array_merge($this->view->post_var, $endereco);
		}
                
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($secretaria);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Secretaria editada com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de secretarias 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretarias',	'name' => 'Secretarias'),
			array('url' => null,'name' => 'Adicionar secretaria')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Secretaria adicionada com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idsecretaria
     */    
    private function preForm($idsecretaria = 0) {

    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$secretaria = strip_tags(trim($this->getRequest()->getPost("secretaria")));
		$responsavel = strip_tags(trim($this->getRequest()->getPost("responsavel")));
		$telefone = strip_tags(trim($this->getRequest()->getPost("telefone")));		
                
		$endereco = strip_tags(trim($this->getRequest()->getPost("endereco")));		
		$cep = strip_tags(trim($this->getRequest()->getPost("cep")));		
		$numero = strip_tags(trim($this->getRequest()->getPost("numero")));	
		$complemento = strip_tags(trim($this->getRequest()->getPost("complemento")));		
		$idestado = strip_tags(trim($this->getRequest()->getPost("idestado")));			
		$bairro = strip_tags(trim($this->getRequest()->getPost("bairro")));		
		$idestado = strip_tags(trim($this->getRequest()->getPost("idestado")));		
		$cidade = strip_tags(trim($this->getRequest()->getPost("cidade")));	
                
		$status = strip_tags(trim($this->getRequest()->getPost("status")));
		
		$erros = array();

		if (""==$secretaria) array_push($erros, 'Preencha o campo SECRETARIA.');
		if (""==$responsavel) array_push($erros, 'Preencha o campo RESPONSÁVEL LEGAL.');
		if (""==$telefone) array_push($erros, 'Preencha o campo TELEFONE.');
		if (""==$endereco) array_push($erros, 'Preencha o campo ENDERECO.');
		if (""==$cep) array_push($erros, 'Preencha o campo CEP.');
		if (""==$numero) array_push($erros, 'Preencha o campo NUMERO.');
		if (""==$bairro) array_push($erros, 'Preencha o campo BAIRRO.');
		if (""==$idestado) array_push($erros, 'Selecione um ESTADO.');
		if (""==$cidade) array_push($erros, 'Preencha o campo CIDADE.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$secretarias = new Secretarias();
		$row = $secretarias->fetchRow("excluido='nao' AND secretaria='$secretaria' AND id<>".$id);
		if ($row) array_push($erros, 'Já existe uma SECRETARIA com esse NOME.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {		
			$idendereco = ($_registro) ? (int)$_registro['idendereco'] : 0;
			$enderecosModel = new EnderecoModel($this);
			$endereco = $enderecosModel->getPost($idendereco);
			$idendereco = ($endereco) ? $endereco['id'] : 0;
                        
			$dados = array();
			$dados['id'] = $id;
			$dados['idendereco'] = $idendereco;
			$dados['secretaria'] = $secretaria;
			$dados['telefone'] = $telefone;
			$dados['responsavel'] = $responsavel;
                        
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Usuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			$row = $secretarias->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }
	
	
}