<?php

/**
 * Controle da classe importacoesprofessores do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ImportacoesprofessoresController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Importacaoprofessores
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		// Mn_Util::blockAccess("importacoesprofessores", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Importacoesprofessores();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Importação de Professores excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="importacoesprofessores") $objs = new Importacoesprofessores();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'importacoesprofessores', 'name' => 'Importações de Professores'),
			array('url' => null,'name' => 'Visualizar Importação de Professores')
		);
		
		$id = (int)$this->_request->getParam("id");
		$importacoesprofessores = new Importacoesprofessores();
		$importacaoprofessor = $importacoesprofessores->getImportacaoprofessoresById($id, array());
		
		if (!$importacaoprofessor) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $importacaoprofessor;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Importações de Professores')
		);
		
		$ns = new Zend_Session_Namespace('default_importacoesprofessores');
		$importacoesprofessores = new Importacoesprofessores();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $importacoesprofessores->getImportacoesprofessores($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $importacoesprofessores->getImportacoesprofessores($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de importacoesprofessores
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'importacoesprofessores', 'name' => 'Importações de Professores'),
			array('url' => null,'name' => 'Editar Importação de Professores')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$importacoesprofessores = new Importacoesprofessores();
		$importacaoprofessor = $importacoesprofessores->getImportacaoprofessoresById($id);
		
		if (!$importacaoprofessor) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

		$erroplan = $this->leExcel($importacaoprofessor);
		
		$this->view->post_var = $importacaoprofessor;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($importacaoprofessor);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Importação de Professores editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    } 		
	
	/**
	 * 
	 * Action de adição de importacoesprofessores 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'importacoesprofessores', 'name' => 'Importações de Professores'),
			array('url' => null,'name' => 'Adicionar Importação de Professores')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Importação de Professores adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idimportacaoprofessor
     */    
    private function preForm($idimportacaoprofessor = 0) {
    } 

	private function isValidXls($idarquivo) {
    	PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

    	$arquivos = new Arquivos();
    	$arquivo = $arquivos->downloadArquivo($idarquivo);

		$sourceFilename = $arquivo['sourceFilename'];
		$valid = false;
		$types = array('Excel2007', 'Excel5');
		foreach ($types as $type) {

		    $reader = PHPExcel_IOFactory::createReader($type);

		    if ($reader->canRead($sourceFilename)) {

		        $valid = true;
		        break;
		    }
		}

		return $valid;
    }

    private function leExcel($pedidoimportar) {

    	if (!$this->isValidXls($pedidoimportar['idarquivo'])) {
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		return;
    	}

		$arquivos = new Arquivos();
		$arquivo = $arquivos->downloadArquivo($pedidoimportar['idarquivo']);
		$sourceFilename = $arquivo['sourceFilename'];
		$objPHPExcel = PHPExcel_IOFactory::load($sourceFilename);
		$aba = 0;
		
		try {			
		    $sheet = $objPHPExcel->getSheet($aba);
		} catch (Exception $e) {
			$erros = array();
			
			array_push($erros, "Aba não encontrada. Digite uma aba Válida.");
			if (sizeof($erros)>0) return $erros; 
		}

		$sheet = $objPHPExcel->getSheet($aba);
		
		$dados = $sheet->toArray();

		$fields = $dados[0];

		$ids = array();
	
		$linhas = $sheet->toArray();

		$usuarios = new Usuarios();
		$escolas = new Escolas();
		$funcionarios = new Funcionariosgeraisescolas();
		
		foreach ($linhas as $linha => $values) {
			if ($linha<=0) continue;

			$rgf = (int)$values[0];
			$nomefunc = $values[1];
			$escola = $values[7];

			$db = Zend_Registry::get('db');
			$db->beginTransaction();

			try{

				//CADASTRA ESCOLA
				$escola_ex = $escolas->getEscolas(array('escola'=>$escola));
				if(sizeof($escola_ex)>0) {
					$escola_ = $escola_ex[0];
					$idescola = $escola_['id'];
				} else {
					$idescola = 0;
				}

				$queriesescola = array(
					'id'=>$idescola,
					'idsecretaria'=>1,
					'escola'=>$escola,
					'status'=>'Ativo',
					'excluido'=>'nao',
					'logdata'=>date('Y-m-d G:i:s'),
					'logusuario'=>Usuarios::getUsuario('id')
				);

				$row_escola = $escolas->save($queriesescola);
				$idescola = $row_escola['id'];

				//CADASTRA EM FUNCIONARIOS GERAIS ESCOLAS
				$funcionario_ex = $funcionarios->getFuncionariosgeraisescolas(array('rgf'=>$rgf));
				if(sizeof($funcionario_ex)>0) {
					$funcionario_ = $funcionario_ex[0];
					$idfunc = $funcionario_['id'];
				} else {
					$idfunc = 0;
				}

				$queriesfunc = array(
					'id'=>$idfunc,
					'idescola'=>$idescola,
					'nome'=> $nomefunc,
					'rgf' => $rgf,
					'professor'=>'sim',
					'status'=>'Ativo',
					'excluido'=>'nao',
					'logdata'=>date("Y-m-d G:i:s"),
					'logusuario'=>Usuarios::getUsuario('id')
				);

				$row_func = $funcionarios->save($queriesfunc);
				$idfunc = $row_func['id'];

				//CADASTRA EM USUÁRIOS
				$usuario_ex = $usuarios->getUsuarios(array('rgf'=>$rgf));
				if(sizeof($usuario_ex)>0) {
					$usuario_ = $usuario_ex[0];
					$iduser = $usuario_['id'];
				} else {
					$iduser = 0;
				}

				$perfis = new Escolasusuariosperfis();
				$perfisprofessor = $perfis->getPerfilOrigem('SYS');
				$idperfil = $perfisprofessor['id'];

				$queriesuser = array(
					'id'=>$iduser,
					'idfuncionario'=>$idfunc,
					'idsecretarias'=>1,
					'idprefeitura'=>1,
					'idperfil'=>$idperfil,
					'nomerazao'=>$nomefunc,
					'rgf'=>$rgf,
					'senha'=>$rgf,
					'excluido'=>'nao',
					'logdata'=>date("Y-m-d G:i:s"),
					'logusuario'=>Usuarios::getUsuario('id')
				);

				$row_user = $usuarios->save($queriesuser);
				
				$db->commit();
			} catch (Exception $e) {
				echo $e->getMessage();
				
				$db->rollBack();
				die();
			}
		}

		return "";
    } 
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_importacaoprofessor = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$importacoesprofessores = new Importacoesprofessores();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			
$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $importacoesprofessores->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}