<?php

class MYPDF extends TCPDF {

	//Page header
	public function Header(){
		$image_file = "public/admin/imagens/logosantaisabel.jpg";
		$this->Image($image_file, 23, 5, 20, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', '', 5);
        $this->SetXY(21, 26);
        $this->Cell(0, 0, 'Paraíso da Grande São Paulo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
       
        // cabeçalho Endereço 
        $this->SetFont('helvetica', '', 12);
        $this->SetXY(79, 6);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Municipio de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', 'B', 10);
        $this->SetXY(75, 10);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Secretaria Municipal de Educação', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(70, 14);
        $this->SetTextColor(130, 130, 130);
		$this->Cell(0, 0, 'Tel. 4656-2440 e-mail: sec.educacao@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 8);
        $this->SetXY(70, 18);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297-Centro - CEP: 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 12);
        $this->SetXY(82, 35);
        $this->SetTextColor(0, 0, 0);
        $this->Cell(0, 0, 'Secretaria declaracoes', 0, false, 'L', 0, '', 0, false, 'M', 'M');


        //informações Saege
        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

       	$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();

        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,18);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: '.$data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 10);
        $this->SetXY(40,21);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        	

	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
        $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Position at 15 mm from bottom
		$this->SetXY(150,-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

/**
 * Controle da classe secretariasdeclaracoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_SecretariasdeclaracoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Secretariasdeclaracao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("secretariasdeclaracoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$secretariasdeclaracao = new Secretariasdeclaracoes();
		$queries = array();	
		$ultimo = $secretariasdeclaracao->getUltimoSecretariasdeclaracao($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Secretariasdeclaracoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Declaração excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="secretariasdeclaracoes") $objs = new Secretariasdeclaracoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasdeclaracoes', 'name' => 'Declarações'),
			array('url' => null,'name' => 'Visualizar Declaração')
		);
		
		$id = (int)$this->_request->getParam("id");
		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		$secretariasdeclaracao = $secretariasdeclaracoes->getSecretariasdeclaracaoById($id, array());
		
		if (!$secretariasdeclaracao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $secretariasdeclaracao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
				
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Declarações')
		);
		
		$ns = new Zend_Session_Namespace('default_secretariasdeclaracoes');
		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		$queries = array();	

		$queries['idescola'] = Usuarios::getUsuario('idescola');

		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
if (isset($this->view->post_var["idescola"]) && $this->view->post_var["idescola"] !="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["idaluno"]!="") $queries["idaluno"] = $this->view->post_var["idaluno"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $secretariasdeclaracoes->getSecretariasdeclaracoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $secretariasdeclaracoes->getSecretariasdeclaracoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de secretariasdeclaracoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasdeclaracoes', 'name' => 'Declarações'),
			array('url' => null,'name' => 'Editar Declaração')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		$secretariasdeclaracao = $secretariasdeclaracoes->getSecretariasdeclaracaoById($id);
		
		if (!$secretariasdeclaracao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $secretariasdeclaracao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($secretariasdeclaracao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Declaração editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	public function setalunosAction(){
    	$this->_helper->layout->disableLayout();
    	
    }
	/**
	 * 
	 * Action de adição de secretariasdeclaracoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasdeclaracoes', 'name' => 'Declarações'),
			array('url' => null,'name' => 'Adicionar Declaração')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Declaração adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }


    
    /**
     * Atribui valores ao view
     * @param int $idsecretariasdeclaracao
     */    
    private function preForm($idsecretariasdeclaracao = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_secretariasdeclaracao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
		$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
		$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idlocal = (int)trim($this->getRequest()->getPost("idlocal"));
		$idsecretaria = (int)trim($this->getRequest()->getPost("idsecretaria"));
		$tipo = trim($this->getRequest()->getPost("tipo"));
		$serie = trim($this->getRequest()->getPost("serie"));
		$horainicio = trim($this->getRequest()->getPost("horainicio"));
		$horafim = trim($this->getRequest()->getPost("horafim"));
		$ano = trim($this->getRequest()->getPost("ano"));
		$considerado = trim($this->getRequest()->getPost("considerado"));
		$proximoano = trim($this->getRequest()->getPost("proximoano"));
		$solicitacaovaga = trim($this->getRequest()->getPost("solicitacaovaga"));
		$validapor = trim($this->getRequest()->getPost("validapor"));
		$vagaano = trim($this->getRequest()->getPost("vagaano"));
		$vagadias = trim($this->getRequest()->getPost("vagadias"));
		$anoregulamentado = trim($this->getRequest()->getPost("anoregulamentado"));
		$tituloregulamentado = trim($this->getRequest()->getPost("tituloregulamentado"));
		$proximoanoregulamentado = trim($this->getRequest()->getPost("proximoanoregulamentado"));
		$diasregulamentado = trim($this->getRequest()->getPost("diasregulamentado"));

		$anosegundoitem = trim($this->getRequest()->getPost("anosegundoitem"));
		$anoterceiroitem = trim($this->getRequest()->getPost("anoterceiroitem"));

		$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
		$iddiretora = (int)trim($this->getRequest()->getPost("iddiretora"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$datalancamento) array_push($erros, "Informe a Data de lançamento.");
if (0==$idescola) array_push($erros, "Informe a Escola.");
if (0==$idsecretaria) array_push($erros, "Informe a Secretaria.");
if (0==$idaluno) array_push($erros, "Informe a Alunos.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["sequencial"] = $sequencial;
			$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			$dados["horalancamento"] = $horalancamento;
			$dados["idusuariologado"] = Usuarios::getUsuario('id');
			$dados["idescola"] = $idescola;
			$dados["idlocal"] = $idlocal;
			$dados["idsecretaria"] = $idsecretaria;
			$dados["tipo"] = $tipo;
			$dados["serie"] = $serie;
			$dados["horainicio"] = $horainicio;
			$dados["horafim"] = $horafim;
			$dados["ano"] = $ano;
			$dados["considerado"] = $considerado;
			$dados["proximoano"] = $proximoano;
			$dados["solicitacaovaga"] = $solicitacaovaga;
			$dados["validapor"] = $validapor;
			$dados["vagaano"] = $vagaano;
			$dados["vagadias"] = $vagadias;
			$dados["anoregulamentado"] = $anoregulamentado;
			$dados["tituloregulamentado"] = $tituloregulamentado;
			$dados["proximoanoregulamentado"] = $proximoanoregulamentado;
			$dados["diasregulamentado"] = $diasregulamentado;
			$dados["idaluno"] = $idaluno;
			$dados["iddiretora"] = $iddiretora;
			$dados["anosegundoitem"] = $anosegundoitem;
			$dados["anoterceiroitem"] = $anoterceiroitem;
			$dados["status"] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $secretariasdeclaracoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
	public function relatorioAction(){     
		$this->view->bread_crumb = array(
			array('url' => 'mapaacompanhamentoclasse', 'name' => 'Mapa de acompanhamento de classe e sondagem'),
			array('url' => null,'name' => 'Editar Mapa de acompanhamento de classe e sondagem')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		$secretariasdeclaracao = $secretariasdeclaracoes->getSecretariasdeclaracaoByIdHelper($id);
		
		if (!$secretariasdeclaracao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->secretariasdeclaracao = $secretariasdeclaracao;  

		$patrimonios = new Patrimonios();
			$data = $patrimonios->getCurrentData();
		$local = 'Santa Isabel';

		$localData = $local.' , '.$data;
 
		$this->view->localdata = $localData;

		$this->preForm();

		$db = Zend_Registry::get('db');

			// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$this->view->pdf = $pdf;

		$pdf->setPageOrientation('e');
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
		$pdf->SetTitle('Secretaria declaracoes');
		// $pdf->SetSubject('Assinatura do Funcionário');

		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->SetFont('helvetica', '', 7);

		// add a page        
		$pdf->AddPage();
		
		$html = $this->view->render('secretariasdeclaracoes/pdf/relatorio.phtml');
		$image_file = "public/admin/imagens/logosantaisabel.jpg";

		$pdf->writeHTML($html, true, 0, true, 0);

		$filename = 'produtos_estoque.pdf';
		$pdf->Output($filename, 'I');
		
		die();
		return true; 
	}
}