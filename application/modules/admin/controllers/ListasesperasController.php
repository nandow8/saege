<?php

/**
 * Controle da classe listasesperas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ListasesperasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Listasespera
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("listasesperas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$listasesperas = new Listasesperas();
		$queries = array();	
		$ultimo = $listasesperas->getUltimaListacontrole($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Listasesperas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Lista de Espera excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="listasesperas") $objs = new Listasesperas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'listasesperas', 'name' => 'Listas de Esperas'),
			array('url' => null,'name' => 'Visualizar Lista de Espera')
		);
		
		$id = (int)$this->_request->getParam("id");
		$listasesperas = new Listasesperas();
		$listasespera = $listasesperas->getListasesperaById($id, array());
		
		if (!$listasespera) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $listasespera;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Listas de Esperas')
		);
		
		$ns = new Zend_Session_Namespace('default_listasesperas');
		$listasesperas = new Listasesperas();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idaluno"]!="") $queries["idaluno"] = $this->view->post_var["idaluno"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $listasesperas->getListasesperas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $listasesperas->getListasesperas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de listasesperas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'listasesperas', 'name' => 'Listas de Esperas'),
			array('url' => null,'name' => 'Editar Lista de Espera')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$listasesperas = new Listasesperas();
		$listasespera = $listasesperas->getListasesperaById($id);
		
		if (!$listasespera) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $listasespera;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($listasespera);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Lista de Espera editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de listasesperas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'listasesperas', 'name' => 'Listas de Esperas'),
			array('url' => null,'name' => 'Adicionar Lista de Espera')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Lista de Espera adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	

	/**
	 * 
	 * Action que pega os dados do aluno
	 */
	public function getdadosalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$idaluno = (int)$this->_request->getPost("idaluno");
		
        $alunos = new Escolasalunos();
        $aluno = $alunos->getEscolaalunoById($idaluno);

        if (!$aluno) die('');

        $this->view->post_var = $aluno;
	}
    
    /**
     * Atribui valores ao view
     * @param int $idlistasespera
     */    
    private function preForm($idlistasespera = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_listasespera = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
		$idusuarioaceite = (Usuarios::getUsuario("idperfil")==29) ? Usuarios::getUsuario("id"): '';
		$dataaceite = (Usuarios::getUsuario("idperfil")==29) ? date('Y-m-d') : null;
		$horalancamento = trim($this->getRequest()->getPost("horalancamento"));

		$idusuarioaceite = (Usuarios::getUsuario("idperfil")==29) ? Usuarios::getUsuario("id"): '';
		$fone1 = trim($this->getRequest()->getPost("fone1"));
		$fone2 = trim($this->getRequest()->getPost("fone2"));
		$iddeficiencia = (int)trim($this->getRequest()->getPost("iddeficiencia"));
		$transporte = trim($this->getRequest()->getPost("transporte"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idaluno) array_push($erros, "Informe a Nome do Aluno.");
if (""==$fone1) array_push($erros, "Informe a Fone1.");
// if (0==$iddeficiencia) array_push($erros, "Informe a Deficiência.");
if (""==$transporte) array_push($erros, "Informe a Utilização do Transporte para Deficiência.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$listasesperas = new Listasesperas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			if((int)$id <= 0)$dados["sequencial"] = $sequencial;
			if((int)$id <= 0)$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			if((int)$id <= 0)$dados["horalancamento"] = $horalancamento;
			if((int)$id <= 0)$dados["idusuariocriacao"] = $this->_usuario['id'];
			$dados["idaluno"] = $idaluno;
$dados["fone1"] = $fone1;
$dados["fone2"] = $fone2;
$dados["iddeficiencia"] = $iddeficiencia;
$dados["transporte"] = $transporte;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $listasesperas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}