<?php

/**
 * Controle da classe recepcaogabinetes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_RecepcaogabinetesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Recepcaogabinete
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("recepcaogabinetes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Recepcaogabinetes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Recepção de gabinete excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="recepcaogabinetes") $objs = new Recepcaogabinetes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'recepcaogabinetes', 'name' => 'Recepção de gabinetes'),
			array('url' => null,'name' => 'Visualizar Recepção de gabinete')
		);
		
		$id = (int)$this->_request->getParam("id");
		$recepcaogabinetes = new Recepcaogabinetes();
		$recepcaogabinete = $recepcaogabinetes->getRecepcaogabineteById($id, array());
		
		if (!$recepcaogabinete) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $recepcaogabinete;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Recepção de gabinetes')
		);
		
		$ns = new Zend_Session_Namespace('default_recepcaogabinetes');
		$recepcaogabinetes = new Recepcaogabinetes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
if ($this->view->post_var["numeroprotocolo"]!="") $queries["numeroprotocolo"] = $this->view->post_var["numeroprotocolo"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $recepcaogabinetes->getRecepcaogabinetes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $recepcaogabinetes->getRecepcaogabinetes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de recepcaogabinetes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'recepcaogabinetes', 'name' => 'Recepção de gabinetes'),
			array('url' => null,'name' => 'Editar Recepção de gabinete')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$recepcaogabinetes = new Recepcaogabinetes();
		$recepcaogabinete = $recepcaogabinetes->getRecepcaogabineteById($id);
		
		if (!$recepcaogabinete) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $recepcaogabinete;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($recepcaogabinete);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Recepção de gabinete editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de recepcaogabinetes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'recepcaogabinetes', 'name' => 'Recepção de gabinetes'),
			array('url' => null,'name' => 'Adicionar Recepção de gabinete')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Recepção de gabinete adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idrecepcaogabinete
     */    
    private function preForm($idrecepcaogabinete = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_recepcaogabinete = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
$titulo = trim($this->getRequest()->getPost("titulo"));
$numeroprotocolo = trim($this->getRequest()->getPost("numeroprotocolo"));
$interessado = trim($this->getRequest()->getPost("interessado"));
$numof = trim($this->getRequest()->getPost("numof"));
$dataof = Mn_Util::stringToTime($this->getRequest()->getPost("dataof"));
$proc = trim($this->getRequest()->getPost("proc"));
$procdata = Mn_Util::stringToTime($this->getRequest()->getPost("procdata"));
$assunto = trim($this->getRequest()->getPost("assunto"));
$despacho = trim($this->getRequest()->getPost("despacho"));
$iddocumento = (int)trim($this->getRequest()->getPost("iddocumento"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$datalancamento) array_push($erros, "Informe a Data de Lançamento.");
if (""==$titulo) array_push($erros, "Informe a Título.");
if (""==$numeroprotocolo) array_push($erros, "Informe a Nº Protocolo.");
if (""==$assunto) array_push($erros, "Informe a Assunto.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$recepcaogabinetes = new Recepcaogabinetes();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["datalancamento"] = date("Y-m-d", $datalancamento);
$dados["titulo"] = $titulo;
$dados["numeroprotocolo"] = $numeroprotocolo;
$dados["interessado"] = $interessado;
$dados["numof"] = $numof;
$dados["dataof"] = date("Y-m-d", $dataof);
$dados["proc"] = $proc;
$dados["procdata"] = date("Y-m-d", $procdata);
$dados["assunto"] = $assunto;
$dados["despacho"] = $despacho;

$iddocumento = $this->getArquivo('iddocumento');
			if ($iddocumento!=0) $dados['iddocumento'] = $iddocumento;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $recepcaogabinetes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}