<?php
 
/**
 * Controle da classe Educacaoespecialplanodetrabalhoindividual do sistema
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 JFMX.  
 * @version     1.0
 */
class Admin_EducacaoespecialplanodetrabalhoindividualController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Educacaoespecialplanodetrabalhoindividual
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("educacaoespecialplanodetrabalhoindividual", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Educacaoespecialplanodetrabalhoindividual();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Patrimônio excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		 
		if ($op=="educacaoespecialplanodetrabalhoindividual") $objs = new Educacaoespecialplanodetrabalhoindividual();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialplanodetrabalhoindividual', 'name' => 'Educação Especial'),
			array('url' => null,'name' => 'Visualizar Plano de trabalho individual')
		);
		
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialplanodetrabalhoindividual = new Educacaoespecialplanodetrabalhoindividual();
		$educacaoespecialplanodetrabalhoindividua = $educacaoespecialplanodetrabalhoindividual->getEducacaoespecialplanodetrabalhoindividualById($id, array());
		
		if (!$educacaoespecialplanodetrabalhoindividua) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialplanodetrabalhoindividua;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
		
	/**
     * Listagem
     */
	public function indexAction() { 
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Educacao Especial Plano de trabalho individual')
		);
		
		$ns = new Zend_Session_Namespace('default_educacaoespecialplanodetrabalhoindividual');
		$educacaoespecialplanodetrabalhoindividual = new Educacaoespecialplanodetrabalhoindividual();
		$queries = array(); 
		 
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v); 
				if (isset($this->view->post_var["idaluno"]) && $this->view->post_var["idaluno"]!="") $queries["idaluno"] = $this->view->post_var["idaluno"];
				if (isset($this->view->post_var["conteudo"]) && $this->view->post_var["conteudo"]!="") $queries["conteudo"] = $this->view->post_var["conteudo"];
				if (isset($this->view->post_var["linguagem_oral_portugues"]) && $this->view->post_var["linguagem_oral_portugues"]!="") $queries["linguagem_oral_portugues"] = $this->view->post_var["linguagem_oral_portugues"];
				if (isset($this->view->post_var["raciocinio_logico_matematica"]) && $this->view->post_var["raciocinio_logico_matematica"]!="") $queries["raciocinio_logico_matematica"] = $this->view->post_var["raciocinio_logico_matematica"];
				if (isset($this->view->post_var["coord_motora_fina_grossa"]) && $this->view->post_var["coord_motora_fina_grossa"]!="") $queries["coord_motora_fina_grossa"] = $this->view->post_var["coord_motora_fina_grossa"];
				if (isset($this->view->post_var["independencia_organizacao"]) && $this->view->post_var["independencia_organizacao"]!="") $queries["independencia_organizacao"] = $this->view->post_var["independencia_organizacao"];
				if (isset($this->view->post_var["boaconduta"]) && $this->view->post_var["boaconduta"]!="") $queries["boaconduta"] = $this->view->post_var["boaconduta"];
				if (isset($this->view->post_var["objetivosgerais"]) && $this->view->post_var["objetivosgerais"]!="") $queries["objetivosgerais"] = $this->view->post_var["objetivosgerais"];
				if (isset($this->view->post_var["datacriacao"]) && $this->view->post_var["datacriacao"]!="") $queries["datacriacao"] = $this->view->post_var["datacriacao"];
				if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
				if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
				if (isset($this->view->post_var["status1"]) && $this->view->post_var["status1"]!="") $queries["status1"] = $this->view->post_var["status1"];


    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}
	/*	
		$idperfil = Usuarios::getUsuario('idperfil');
		$idescola = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idescola'];
		
		if($idperfil != 17 AND $idperfil != 30) {  // 30 superadmin que ve tudo   -  17 coordenação
			$queries['idescola'] = $idescola;
		}
	*/		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $educacaoespecialplanodetrabalhoindividual->getEducacaoespecialplanodetrabalhoindividual($queries); 
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $educacaoespecialplanodetrabalhoindividual->getEducacaoespecialplanodetrabalhoindividual($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de educacaoespecialplanodetrabalhoindividual
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialplanodetrabalhoindividual', 'name' => 'Educação Especial'),
			array('url' => null,'name' => 'Editar Plano de trabalho individual')
		);	
		

		$id = (int)$this->_request->getParam("id");
		$educacaoespecialplanodetrabalhoindividual = new Educacaoespecialplanodetrabalhoindividual();
		$educacaoespecialplanodetrabalhoindividua = $educacaoespecialplanodetrabalhoindividual->getEducacaoespecialplanodetrabalhoindividualById($id);

		if (!$educacaoespecialplanodetrabalhoindividua) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialplanodetrabalhoindividua;
 
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($educacaoespecialplanodetrabalhoindividua);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
		$this->view->editar = true;
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de Educacaoespecialplanodetrabalhoindividual 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialplanodetrabalhoindividual', 'name' => 'Educacao Especial'),
			array('url' => null,'name' => 'Adicionar Plano de trabalho individual')
		);

		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }
    
    /**
     * Atribui valores ao view
     * @param int $ideducacaoespecialplanodetrabalhoindividual
     */    
    private function preForm($ideducacaoespecialplanodetrabalhoindividual = 0) {
    }
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_educacaoespecialplanodetrabalhoindividual = false) {
 
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		 
		$id = (int)$this->getRequest()->getPost("id"); 
		$idaluno = trim($this->getRequest()->getPost("idaluno"));
		$conteudo = trim($this->getRequest()->getPost("conteudo"));
		$linguagem_oral_portugues = trim($this->getRequest()->getPost("linguagem_oral_portugues"));
		$raciocinio_logico_matematica = trim($this->getRequest()->getPost("raciocinio_logico_matematica"));
		$coord_motora_fina_grossa = trim($this->getRequest()->getPost("coord_motora_fina_grossa"));
		$independencia_organizacao = trim($this->getRequest()->getPost("independencia_organizacao"));
		$boaconduta = trim($this->getRequest()->getPost("boaconduta"));
		$objetivosgerais = trim($this->getRequest()->getPost("objetivosgerais"));

		$status = trim($this->getRequest()->getPost("status1"));
		  		
		$erros = array(); 
		/*	
		if (0==$idsecretaria) array_push($erros, "Informe a Secretaria."); */
		 		
		$educacaoespecialplanodetrabalhoindividual = new Educacaoespecialplanodetrabalhoindividual();
					
		if (sizeof($erros)>0) return $erros;

		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idaluno"] = $idaluno; 
			$dados["conteudo"] = $conteudo; 
			$dados["linguagem_oral_portugues"] = $linguagem_oral_portugues;
			$dados["raciocinio_logico_matematica"] = $raciocinio_logico_matematica;
			$dados["coord_motora_fina_grossa"] = $coord_motora_fina_grossa;
			$dados["independencia_organizacao"] = $independencia_organizacao;
			$dados["boaconduta"] = $boaconduta;
			$dados["objetivosgerais"] = $objetivosgerais; 
			
			$dados["status"] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');
 
			$row = $educacaoespecialplanodetrabalhoindividual->save($dados);
			
			$db->commit(); 

		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}
		
		return "";
    }
    private function excluirImagem($idfoto){
    	$imagem = new Imagens();
    	try{
    		$imagem->excluir($idfoto);
    	}catch(Exception $e){
    		array_push($erros, "Falha ao excluir imagem");
    	}
    }


    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
 
}
