<?php

class Admin_NutricaoagendacalendarioController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacoteescola
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("nutricaoagendacalendario", 'index');	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}

	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => null,'name' => 'Calendário')
		);	
				
		$this->preForm();
		$comunicacaoagenda = new Agendamentosvisitas();
		$queries = array();	
		$queries['iddepartamento'] = '14';
		$queries['idescola'] = Usuarios::getUsuario('idescola');
		$queries['order'] = "ORDER BY a1.datainicio DESC";		
		$this->view->rows = $comunicacaoagenda->getAgendamentosvisitas($queries, '0', '5');	
	}

	public function setcalendarioAction() {
		$this->_helper->layout->disableLayout();
		$this->preForm();
		
		$page = (int)$this->getRequest()->getPost("page");
		
		$this->view->page = $page;
    }
    
    public function seteventoAction() {
    	$this->_helper->layout->disableLayout();
    	
    	$id = $this->getRequest()->getParam('id');
    	
		$comunicacaoagenda = new Agendamentosvisitas();
		$evento = $comunicacaoagenda->getAgendamentosvisitaById($id);
		//var_dump($evento);die();
		
		if (!$evento) die('Não Encontrado'); 
			
		$this->view->post_var = $evento;
    }
    
    public function seteventosdiaAction() {
    	$this->_helper->layout->disableLayout();
    	
    	$dia = $this->getRequest()->getParam('dia');
		
    	$queries = array();
		$queries['iddepartamento'] = '14';
		$queries['idescola'] = Usuarios::getUsuario('idescola');
		$queries['order'] = "ORDER BY a1.datainicio DESC";	
		$queries['data'] = date('Y-m-d', $dia);
		
		$comunicacaoagenda = new Agendamentosvisitas();
		$rows = $comunicacaoagenda->getAgendamentosvisitas($queries);
		
		if (!$rows) die('Não Encontrado'); 
		$this->view->dia = $dia;
		$this->view->rows = $rows;
		
    }
    
    /**
     * Atribui valores ao view
     * @param int $idescola
     */    
    private function preForm($idescola = 0) {
        $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
                
		$usuarios = new Usuarios();
		$this->view->usuarios = $usuarios->getUsuarios(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		$departamentos = new Departamentossecretarias();
		$this->view->departamentos = $departamentos->getDepartamentossecretarias(array('idsecretaria' => $this->view->idsecretaria));
    } 
}
