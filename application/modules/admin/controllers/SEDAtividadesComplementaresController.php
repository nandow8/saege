<?php

/**
 * Controle da classe sedatividadescomplementares do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_SEDAtividadesComplementaresController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var SEDAtividadesComplementares
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("sedatividadescomplementares", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'sedatividadescomplementares', 'name' => 'Atividades Complementares'),
            array('url' => null, 'name' => 'Visualizar Atividade Complementar')
        );

        $id = (int) $this->_request->getParam("id");
        $sedatividadescomplementares = new SEDAtividadesComplementares();
        $sedatividadescomplementares = $sedatividadescomplementares->getSEDAtividadesComplementaresById($id, array());

        if (!$sedatividadescomplementares)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $sedatividadescomplementares;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'SEDAtividadesComplementares')
        );

        $ns = new Zend_Session_Namespace('default_contas');
        $sedatividadescomplementares = new SEDAtividadesComplementares();
        $queries = array();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ($this->view->post_var["idtipoatividade"] != "")
                $queries["idtipoatividade"] = $this->view->post_var["idtipoatividade"];
            if ($this->view->post_var["titulo"] != "")
                $queries["titulo"] = $this->view->post_var["titulo"];
            if ($this->view->post_var["idescola"] != "")
                $queries["idescola"] = $this->view->post_var["idescola"];

            if ($this->view->post_var["status1"] != "")
                $queries["status"] = $this->view->post_var["status1"];
            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $sedatividadescomplementares->getSEDAtividadesComplementares($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $sedatividadescomplementares->getSEDAtividadesComplementares($queries, $paginaAtual, $maxpp);
    }

    /**
     * Atribui valores ao view
     * @param int $idtreinamento
     */
    private function preForm($idsedatividadescomplementares = 0) {
        $escolas = new Escolas();
        $this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }


}
