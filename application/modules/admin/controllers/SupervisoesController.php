<?php

class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        $image_file = "public/admin/imagens/logosantaisabel.jpg";
        $this->Image($image_file, 16, 15, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

        //Set font
        $this->SetFont('helvetica', 'B', 20);
        $this->SetFont('helvetica', 'B', 10);

        // cabeçalho Endereço
        $this->SetXY(16, 12);
        $this->Cell(0, 0, 'Prefeitura Municipal de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 9);
        $this->SetXY(35, 16);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Supervisão - Termo de visita', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(35, 19);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(35, 22);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetXY(35, 25);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');

        //informações Saege
        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60, 20);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $supervisao = new Supervisoestermovisitas();
        $datafinal = $supervisao->getCurrentData();

        $this->SetFont('helvetica', '', 8);
        $this->SetXY(50, 25);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: ' . $datafinal, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 10);
        $this->SetXY(60, 30);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

}

/**
 * Controle da classe termovisitas do sistema
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_SupervisoesController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var termovisitas
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("supervisoes", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);


        $this->_departamento = false;
        $this->view->departamento = array();
        $this->_urldepartamento = "";

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Supervisoestermovisitas();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Termo de visitas excluída com sucesso.";

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "supervisoes")
            $objs = new termovisitas();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'supervisoes' . "/index" . $this->_urldepartamento, 'name' => 'Termo de visitas'),
            array('url' => null, 'name' => 'Visualizar Visitas')
        );

        $id = (int) $this->_request->getParam("id");
        $termovisitas = new Supervisoestermovisitas();
        $termovisitas = $termovisitas->getTermoVisitaById($id, array());

        if (!$termovisitas)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName() . "/index" . $this->_urldepartamento);

        $this->view->post_var = $termovisitas;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Termo de visitas')
        );
        $idd = (int) $this->_request->getParam("idd");

        $ns = new Zend_Session_Namespace('admin_supervisoes');
        $termovisita = new Supervisoestermovisitas();
        $queries = array();

        $usuario = new Usuarios;

        $dados = $usuario->getUsuarioById(Usuarios::getUsuario('id'));
        $perfil = $dados["idperfil"];

        /* é secretaria */
        if ($perfil == 29):
            $escola = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'));
            $queries['idescola'] = $escola['id'];
        endif;

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName() . "/index" . $this->_urldepartamento);
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if (isset($this->view->post_var["datacriacao"]) AND $this->view->post_var["datacriacao"] != "")
                $queries["datacriacao"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datacriacao"]));

            if (isset($this->view->post_var["idusuariocriacao"]) AND $this->view->post_var["idusuariocriacao"] != "")
                $queries["idusuariocriacao"] = $this->view->post_var["idusuariocriacao"];

            if (isset($this->view->post_var["iddepartamentosecretaria"]) AND $this->view->post_var["iddepartamentosecretaria"] != "")
                $queries["iddepartamentosecretaria"] = $this->view->post_var["iddepartamentosecretaria"];
            if (isset($this->view->post_var["idescola"]) AND $this->view->post_var["idescola"] != "")
                $queries["idescola"] = $this->view->post_var["idescola"];
            if (isset($this->view->post_var["numerotermovisitas"]) AND $this->view->post_var["numerotermovisitas"] != "")
                $queries["numerotermovisitas"] = $this->view->post_var["numerotermovisitas"];
            if (isset($this->view->post_var["titulo"]) AND $this->view->post_var["titulo"] != "")
                $queries["titulo"] = $this->view->post_var["titulo"];
            if (isset($this->view->post_var["descricao"]) AND $this->view->post_var["descricao"] != "")
                $queries["descricao"] = $this->view->post_var["descricao"];
            if ($this->view->post_var["status1"] != "")
                $queries["status"] = $this->view->post_var["status1"];

            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $termovisita->getTermovisita($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $termovisita->getTermoVisita($queries, $paginaAtual, $maxpp);
    }

    /**
     *
     * Action de edição de termovisitas
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'supervisoes' . "/index" . $this->_urldepartamento, 'name' => 'Termo de visitas'),
            array('url' => null, 'name' => 'Editar termo de visitas')
        );

        $id = (int) $this->_request->getParam("id");
        $termovisitas = new Supervisoestermovisitas();
        $termovisitas = $termovisitas->getTermoVisitaById($id);

        if (!$termovisitas)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName() . "/index" . $this->_urldepartamento);

        $this->view->post_var = $termovisitas;
        $this->view->editar = true;

        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($termovisitas);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Termo de visitas editada com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName() . "/index" . $this->_urldepartamento);
        }

        return true;
    }

    /**
     *
     * Action de adição de termovisitas
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'supervisoes' . "/index" . $this->_urldepartamento, 'name' => 'Termo de visitas'),
            array('url' => null, 'name' => 'Adicionar Termo de visitas')
        );
        $this->view->adicionar = true;
        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Termo de visitas adicionada com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName() . "/index" . $this->_urldepartamento);
        }

        return true;
    }

    /**
     * Action para gerar o documento PDF
     * */
    public function gerardocumentoAction() {
        $id = $this->_request->getParam('id');

        if (empty($id)) {
            $messageNameSpace = new Zend_Session_Namespace("message");
            $messageNameSpace->crudmessage = "Falha ao gerar documento. Tente novamente";
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        }

        $termovisita = new Supervisoestermovisitas();
        $visita = $termovisita->getTermoVisitaById($id);

        $idescola = $visita['idescola'];

        $escola = Escolas::getEscolaByIdHelper($idescola);
        $receptor = Usuarios::getUsuarioByIdHelper($visita['idusuarioaceite']);
        unset($receptor['id']);
        unset($receptor['senha']);
        unset($receptor['excluido']);

        $supervisor = Usuarios::getUsuarioByIdHelper($visita['idusuariocriacao']);

        $datafinal = $termovisita->getCurrentData();

        $local = 'Santa Isabel';
        $localData = $local . ' , ' . $datafinal;

        $this->view->visita = $visita;
        $this->view->escola = $escola;
        $this->view->receptor = $receptor;
        $this->view->supervisor = $supervisor;
        $this->view->localdata = $localData;
        $this->preForm();

        $db = Zend_Registry::get('db');

        // criar documento PDF
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(), true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');

        // Setar informações do documento
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Relatório do Termo de visita');
        //$pdf->SetSubject('Registros Financeiros de Programas das Escola');

        $this->view->setScriptPath(APPLICATION_PATH . '/modules/admin/views/scripts/');

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //margins
        $pdf->SetMargins(10, 15, 10);
        //fator de escala de imagem
        $pdf->SetFont('helvetica', '', 10, 'false');
        //adiciona página
        $pdf->AddPage();

        $html = $this->view->render('supervisoes/pdf/geradorpdf.phtml');
        $pdf->writeHTML($html, true, 0, true, 0);

        $filename = 'termo_visita_' . date('d') . '_' . date('m') . '.pdf';

        $pdf->Output($filename, 'I');

        die();
        return true;
    }

    /*
      Criar sequencial após registro ser salvo no banco
      @param $id - int
     */

    public function setSequencial($id) {

        $rows = new Supervisoestermovisitas();
        $linha = $rows->fetchRow('id=' . $id);

        $row = $linha->toArray();
        $row['sequencial'] = $row['id'] . '/' . substr($row['datacriacao'], 0, 4);

        $rows->save($row);

        return true;
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function setdepartamentosescolasAction() {
        $this->_helper->layout->disableLayout();

        $idescola = (int) $this->_request->getPost("idescola");
        $escolas = Escolas::getEscolaByIdHelper($idescola);
        if (!$idescola)
            return $this->view->rows = false;

        $departamentos = new Departamentosescolas();
        $this->view->rows = $departamentos->getDepartamentosescolas(array('idescola' => $idescola));
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function setescolausuarioresponsavelAction() {
        $this->_helper->layout->disableLayout();

        $idescola = (int) $this->_request->getPost("idescola");
        $iddepartamentoescola = (int) $this->_request->getPost("iddepartamento");

        if ((int) $idescola <= 0)
            return $this->view->rows = false;

        $rows = new Escolasusuarios();
        $queries = array();
        $queries['idescola'] = $idescola;
        $queries['iddepartamentoescola'] = $iddepartamentoescola;
        $this->view->rows = $rows->getEscolasusuarios($queries);
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function setescolausuarioprofessoresAction() {
        $this->_helper->layout->disableLayout();

        $idescola = (int) $this->_request->getPost("idescola");
        $iddepartamentoescola = (int) $this->_request->getPost("iddepartamento");

        if ((int) $idescola <= 0)
            return $this->view->rows = false;

        $rows = new Escolasusuarios();
        $queries = array();
        $queries['idescola'] = $idescola;
        $queries['professor'] = "Sim";
        $this->view->rows = $rows->getEscolasusuarios($queries);
    }

    /*
     * Action executado via Ajax para buscar a diretora da escola
     */

    public function setdiretorasdaescolaAction() {
        $this->_helper->layout->disableLayout();

        $idescola = (int) $this->_request->getPost("idescola");

        if ((int) $idescola <= 0)
            return $this->view->rows = false;

        $row = Escolas::getEscolaByIdHelper($idescola);
        echo json_encode($row);
        die();
    }

    /**
     * Atribui valores ao view
     * @param int $idtermovisitas
     */
    private function preForm($idtermovisitas = 0) {

    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_termovisitas = false) {

        $idusuariocriacaotemp = (isset($this->view->post_var["idusuariocriacao"])) ? $this->view->post_var["idusuariocriacao"] : Usuarios::getUsuario("id");
        $datacriacaotemp = (isset($this->view->post_var["datacriacao"])) ? $this->view->post_var["datacriacao"] : date("Y-m-d");
        if (Usuarios::getUsuario('idperfil') == 29):
            $idescolatemp = (isset($this->view->post_var["idescola"])) ? $this->view->post_var["idescola"] : (int) trim($this->getRequest()->getPost("idescola"));
        endif;
        //$idusuarioaceitetemp = (isset($this->view->post_var["idusuarioaceite"])) ? $this->view->post_var["idusuarioaceite"] : $this->getRequest()->getPost("idusuarioaceite");
        //$dataaceitetemp = (isset($this->view->post_var["dataaceite"])) ? $this->view->post_var["dataaceite"] : $this->getRequest()->getPost("datacriacao");

        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = $this->getRequest()->getPost("id");
        $idusuariocriacao = (int) trim($idusuariocriacaotemp);

        $idescola = (isset($idescolatemp)) ? $idescolatemp : (int) trim($this->getRequest()->getPost("idescola"));
        $datacriacao = trim($datacriacaotemp);

        $idusuarioaceite = (Usuarios::getUsuario("idperfil") == 29) ? Usuarios::getUsuario("id") : '';
        $dataaceite = (Usuarios::getUsuario("idperfil") == 29) ? date('Y-m-d') : null;
        $titulo = trim($this->getRequest()->getPost("titulo"));
        $descricao = trim($this->getRequest()->getPost("descricao"));
        $diretor = trim($this->getRequest()->getPost("diretor"));
        $status = trim($this->getRequest()->getPost("status1"));
        $sequencial = trim($this->getRequest()->getPost("sequencial"));

        $erros = array();

        if ("" == $titulo)
            array_push($erros, "Informe a Título.");
        if ("" == $status)
            array_push($erros, "Informe a Status.");

        $termovisita = new Supervisoestermovisitas();

        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            $dados = array();
            $dados['id'] = $id;
            //$dados['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
            $dados["idusuariocriacao"] = (int) $idusuariocriacao;
            $dados["datacriacao"] = $datacriacao;
            $dados["idescola"] = $idescola;
            $dados["titulo"] = $titulo;
            $dados["descricao"] = $descricao;
            $dados['diretor'] = $diretor;
            $dados["status"] = $status;
            $dados["idusuarioaceite"] = (int) $idusuarioaceite;
            $dados["dataaceite"] = $dataaceite;
            $dados["sequencial"] = $sequencial;
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $termovisita->save($dados);
            if ($sequencial == "") {
                $this->setSequencial($row['id']);
            }
            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();

            $db->rollBack();
            die();
        }

        return "";
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

}
