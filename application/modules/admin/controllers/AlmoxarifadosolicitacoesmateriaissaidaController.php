<?php

/**
 * Controle da classe almoxarifadosolicitacoesmateriaissaida do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_AlmoxarifadosolicitacoesmateriaissaidaController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Almoxarifadosolicitacaomaterialsaida
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("almoxarifadosolicitacoesmateriaissaida", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Almoxarifadosolicitacoesmateriaissaida();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Material de Saída excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="almoxarifadosolicitacoesmateriaissaida") $objs = new Almoxarifadosolicitacoesmateriaissaida();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosolicitacoesmateriaissaida', 'name' => 'Solicitações de Materiais de Saída'),
			array('url' => null,'name' => 'Visualizar Solicitação de Material de Saída')
		);
		
		$id = (int)$this->_request->getParam("id"); 
        $idusuario = (int)$this->_request->getParam("idusuario"); 
        $codigosolicitacao = (int)$this->_request->getParam("codigosolicitacao"); 
        $almoxarifadosolicitacoes = new Almoxarifadosolicitacoesentradalistadeprodutossaida();
        $almoxarifadosolicitacaoprodutos = $almoxarifadosolicitacoes->getAlmoxarifadosolicitacoesentradalistadeprodutossaidaById(
			$idusuario, array(
				'idusuario' => $idusuario,
                'codigosolicitacao' => $codigosolicitacao
                )
			); 
		$entrada = new Almoxarifadosolicitacoesmateriaissaida();
		$entradaproduto = $entrada->getAlmoxarifadosolicitacoesmateriaissaida(array('id' => $id));
		
		if (!$almoxarifadosolicitacaoprodutos) 
		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $almoxarifadosolicitacaoprodutos;
		
		array_push( $this->view->post_var , $entradaproduto);
 
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Solicitações de Materiais de Saída')
		);
		
		$ns = new Zend_Session_Namespace('default_almoxarifadosolicitacoesmateriaissaida');
		$almoxarifadosolicitacoesmateriaissaida = new Almoxarifadosolicitacoesmateriaissaida();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		// foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			// if ($this->view->post_var["departamentoalmoxarifado"]!="") $queries["departamentoalmoxarifado"] = $this->view->post_var["departamentoalmoxarifado"];
			if (isset($this->view->post_var["idescola"])) $queries["idescola"] = $this->view->post_var["idescola"];
			if (isset($this->view->post_var["idusuario"])) $queries["idusuario"] = $this->view->post_var["idusuario"];
			if (isset($this->view->post_var["idperfilbusca"])) $queries["idperfilbusca"] = $this->view->post_var["idperfilbusca"];
			if (isset($this->view->post_var["status1"])) $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$idperfil = Usuarios::getUsuario('idperfil');
		$idescola = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idescola'];
		
		if($idperfil != 32 AND $idperfil != 25) {
			$queries['idescola'] = $idescola;
		}
			
		if($idescola == null AND ($idperfil != 32 AND $idperfil != 25))
			$queries['idperfil'] = Usuarios::getUsuario('idperfil');


		$queries['total'] = true;
		$totalRegistros = $almoxarifadosolicitacoesmateriaissaida->getAlmoxarifadosolicitacoesmateriaissaida($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $almoxarifadosolicitacoesmateriaissaida->getAlmoxarifadosolicitacoesmateriaissaida($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de almoxarifadosolicitacoesmateriaissaida
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosolicitacoesmateriaissaida', 'name' => 'Solicitações de Materias de Saida'),
			array('url' => null,'name' => 'Editar Solicitação de Material de Saida')
		);	
			 
		$id = (int)$this->_request->getParam("id"); 
        $idusuario = (int)$this->_request->getParam("idusuario"); 
        $codigosolicitacao = (int)$this->_request->getParam("codigosolicitacao"); 
        $almoxarifadosolicitacoes = new Almoxarifadosolicitacoesentradalistadeprodutossaida();
        $almoxarifadosolicitacaoprodutos = $almoxarifadosolicitacoes->getAlmoxarifadosolicitacoesentradalistadeprodutossaidaById(
			$idusuario, array(
				'idusuario' => $idusuario,
                'codigosolicitacao' => $codigosolicitacao
                )
			); 
			$entrada = new Almoxarifadosolicitacoesmateriaissaida();
			$entradaproduto = $entrada->getAlmoxarifadosolicitacoesmateriaissaida(array('id' => $id));
            
            if (!$almoxarifadosolicitacaoprodutos) 
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            
            $this->view->post_var = $almoxarifadosolicitacaoprodutos;
            
            array_push( $this->view->post_var , $entradaproduto);
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($almoxarifadosolicitacaoprodutos);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Solicitação editada com sucesso.";
            
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }   

        return true; 		
    }	
	
	/**
	 * 
	 * Action de adição de almoxarifadosolicitacoesmateriaissaida 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosolicitacoesmateriaissaida', 'name' => 'Solicitações de Materiais de Saída'),
			array('url' => null,'name' => 'Adicionar Solicitação de Material de Saída')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Material de Saída adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idalmoxarifadosolicitacaomaterialsaida
     */    
    private function preForm($idalmoxarifadosolicitacaomaterialsaida = 0) {
	}   
	
	public function findprodutosAction() {
        $this->_helper->layout->disableLayout();

        $array = array();

        $idproduto = (int)$this->getRequest()->getPost('idproduto');
        $id_produtos = (int)$this->getRequest()->getPost('id_produtos');
        $codigoproduto = strip_tags(trim($this->getRequest()->getPost('codigoproduto')));
        $categoria = (int)$this->getRequest()->getPost('categoria');
        $unidademedida = (int)$this->getRequest()->getPost('unidademedida');
        
        // $idlocalestoque = (int)$this->getRequest()->getPost('idlocalestoque');
        $idlocal = (int)$this->getRequest()->getPost('idlocal');
        $idsolicitante = (int)$this->getRequest()->getPost('idsolicitante');
        $produto = strip_tags(trim($this->getRequest()->getPost('produto')));
		$descricao = strip_tags(trim($this->getRequest()->getPost('descricao')));
		$categoriaprodutos = strip_tags(trim($this->getRequest()->getPost('categoriaprodutos')));

        if ($idproduto <> 0 && !is_null($idproduto)) $array['id'] = $idproduto;
        if ($codigoproduto != '' && !is_null($codigoproduto)) $array['codigoproduto'] = $codigoproduto;
        if ($produto != '' && !is_null($produto)) $array['produto'] = $produto;
        if ($descricao != '' && !is_null($descricao)) $array['descricoes'] = $descricao;
		if ($categoria <> 0 && !is_null($categoria)) $array['idcategoria'] = $categoria;
		if ($categoriaprodutos <> 0 && !is_null($categoriaprodutos)) $array['idcategoria'] = $categoriaprodutos;

		$produtos = new Almoxarifadoprodutossaidas();
		
		$idperfil = Usuarios::getUsuario();
		if($idperfil['idperfil'] != 32 AND $idperfil['idperfil'] != 25) 
			$queries['idescola'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idescola']; 
		 
		if($queries['idescola'] == 0 AND $idperfil['idperfil'] != 32 AND $idperfil['idperfil'] != 25){
			$queries['idperfil'] = $idperfil['idperfil'];
			$queries['idescola'] = null;
		}
		
        if (sizeof($array) == 0) {
            $this->view->rows = $produtos->getAlmoxarifadoprodutossaidas($queries);
        } else {
            if ($unidademedida <> 0 && !is_null($unidademedida)) $array['idunidademedida'] = $unidademedida;
            $this->view->rows = $produtos->getAlmoxarifadoprodutossaidas($array);
        }
    }
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_almoxarifadosolicitacaomaterialsaida = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);

		$ids = $this->getRequest()->getPost('id_selecionado');
        $qtds = $this->getRequest()->getPost('qtd_selecionada'); 
 
		$idlocal = $this->getRequest()->getPost('idlocal');
        $idusuario = $this->getRequest()->getPost('idusuario');
		$idescola = $this->getRequest()->getPost('idescola');
		if($idescola == null) $idescola = null;
        $idperfil = $this->getRequest()->getPost('idperfil');
	
		$id = (int)$this->getRequest()->getPost("id");
		$codigoproduto = $this->getRequest()->getPost("codigoproduto");
		$produto = $this->getRequest()->getPost("produto");
		$unidademedida = $this->getRequest()->getPost("unidademedida");
		$quantidade = $this->getRequest()->getPost("quantidade");
		$qtd_selecionada = $this->getRequest()->getPost("qtd_selecionada");
		$idproduto = $this->getRequest()->getPost("idproduto");
		$id_produtos = $this->getRequest()->getPost("id_produtos");
		$codigosolicitacaopost = $this->getRequest()->getPost("codigosolicitacaopost");

		$itens = array_combine($ids, $qtds);
		$erros = array();
		if($quantidade){
			$erroQtd = 0;
			for ($i=0; $i < count($codigoproduto) ; $i++) {  
				if ($quantidade[$i] < $qtd_selecionada[$i])
					$erroQtd = 1;
			} 
			if ($erroQtd != 0) array_push($erros, "Erro ao Salvar, a entrada não pode ser maior que a quantidade no estoque."); 
		}
		
		if (sizeof($erros)>0) return $erros; 

		$almoxarifadosolicitacoesmateriaissaida = new Almoxarifadosolicitacoesmateriaissaida();
		$almoxarifadosolicitacoesentradalistadeprodutossaida = new Almoxarifadosolicitacoesentradalistadeprodutossaida();
		$almoxarifadoprodutossaidas = new Almoxarifadoprodutossaidas();
		$codigosolicitacao =  date('sidmy') . $idusuario;
		$idperfilUsuarioLogado = Usuarios::getUsuario('idperfil');
		$verificaIDescola = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper( $idsolicitante)['idescola'];

		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dadosListaSaidas = array();
			$dadosSalvaEntradaParaSaidaProdutos = array();
			$dados['id'] = $id;
			
			$dados["idlocal"] = $idlocal;
			$dados["idperfil"] = $idperfil;
			$dados["idescola"] = $idescola;
			$dados["idusuario"] = $idusuario;
			$dados["codigosolicitacao"] = $codigosolicitacao;

			
			if(($idperfilUsuarioLogado != 32 OR $idperfilUsuarioLogado != 25) AND empty($verificaIDescola)){
			    $dados['idperfilbusca'] = $idperfil;  
			}
			
			// echo '<pre>'; print_r($dados); die;
			$dados["status"] = 'Ativo';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			if(empty($codigosolicitacaopost)){
				for ($i=0; $i < count( $itens ) ;$i++) {  
					$dadosListaSaidas['idusuario'] = $idusuario;
					$dadosListaSaidas['codigoproduto'] = $codigoproduto[$i];
					$dadosListaSaidas['idperfil'] = $idperfil;
					$dadosListaSaidas['codigosolicitacao'] = $dados["codigosolicitacao"] ;
					$dadosListaSaidas['qtd_selecionada'] = $qtd_selecionada[$i]; 
					$dadosListaSaidas['produto'] = $produto[$i]; 
					$dadosListaSaidas['unidademedida'] = $unidademedida[$i]; 
					
					$dadosListaSaidas['status'] = 'Ativo';
					$dadosListaSaidas['excluido'] = 'nao';
					$dadosListaSaidas['logusuario'] = $this->_usuario['id'];
					$dadosListaSaidas['logdata'] = date('Y-m-d G:i:s');
					
					$row = $almoxarifadosolicitacoesentradalistadeprodutossaida->save($dadosListaSaidas);

					$strsql = "select id,quantidade from almoxarifadoprodutossaidas where idcodigoalmoxarifadoproduto = '$dadosListaSaidas[codigoproduto]' ";
						
					$idprodutosaida = $db->fetchRow($strsql);
					
					if($idprodutosaida){
						$dadosSalvaEntradaParaSaidaProdutos['id'] = $idprodutosaida['id'];
						$dadosSalvaEntradaParaSaidaProdutos['quantidade'] = $idprodutosaida['quantidade'] - $qtd_selecionada[$i];	
					}
					
					$dadosSalvaEntradaParaSaidaProdutos['ultimaquantidaderetirada'] = $qtd_selecionada[$i];
					$dadosSalvaEntradaParaSaidaProdutos['idperfil'] = $idperfil;
					$dadosSalvaEntradaParaSaidaProdutos['idcodigoalmoxarifadoproduto'] = $codigoproduto[$i];
					$dadosSalvaEntradaParaSaidaProdutos["status"] = 'Ativo';
					$dadosSalvaEntradaParaSaidaProdutos['excluido'] = 'nao';
					$dadosSalvaEntradaParaSaidaProdutos['logusuario'] = $this->_usuario['id'];
					$dadosSalvaEntradaParaSaidaProdutos['logdata'] = date('Y-m-d G:i:s');
					
					$row = $almoxarifadoprodutossaidas->save($dadosSalvaEntradaParaSaidaProdutos);
				}
			}
			
			if(!empty($codigosolicitacaopost)){

				$dados["codigosolicitacao"] = $codigosolicitacaopost;

				for ($i=0; $i < count( $qtd_selecionada ) ;$i++) { 
					$dadosListaSaidas['id'] = $id_produtos[$i];
					$dadosListaSaidas['idusuario'] = $idusuario;
					$dadosListaSaidas['codigoproduto'] = $codigoproduto[$i];
					$dadosListaSaidas['codigosolicitacao'] = $dados["codigosolicitacao"] ;
					$dadosListaSaidas['qtd_selecionada'] = $qtd_selecionada[$i]; 
					$dadosListaSaidas['produto'] = $produto[$i]; 
					$dadosListaSaidas['unidademedida'] = $unidademedida[$i]; 
					
					$dadosListaSaidas['status'] = 'Ativo';
					$dadosListaSaidas['excluido'] = 'nao';
					$dadosListaSaidas['logusuario'] = $this->_usuario['id'];
					$dadosListaSaidas['logdata'] = date('Y-m-d G:i:s');					
					
					$row = $almoxarifadosolicitacoesentradalistadeprodutossaida->save($dadosListaSaidas);

					$strsql = "select id,quantidade,ultimaquantidaderetirada,quantidade from almoxarifadoprodutossaidas where idcodigoalmoxarifadoproduto = '$dadosListaSaidas[codigoproduto]' ";
						
					$idprodutosaida = $db->fetchRow($strsql);
					
					if($idprodutosaida){
						$dadosSalvaEntradaParaSaidaProdutos['id'] = $idprodutosaida['id'];
						$quantidadeCorrecao = $idprodutosaida['quantidade'] + $idprodutosaida['ultimaquantidaderetirada'];
						$dadosSalvaEntradaParaSaidaProdutos['quantidade'] = $quantidadeCorrecao - $qtd_selecionada[$i];	

						if($dadosSalvaEntradaParaSaidaProdutos['quantidade'] <= 0){
							$message = new Zend_Session_Namespace("message");
							$message->crudmessage = "O estoque não possui a quantidade solicitada.";
							
							$this->_redirect('admin' . "/almoxarifadosolicitacoesmateriaissaida/editar/id/$id/idusuario/$idusuario/codigosolicitacao/$codigosolicitacaopost");
						}

					}
					
					$dadosSalvaEntradaParaSaidaProdutos['ultimaquantidaderetirada'] = $qtd_selecionada[$i];
					$dadosSalvaEntradaParaSaidaProdutos['idcodigoalmoxarifadoproduto'] = $codigoproduto[$i];
					$dadosSalvaEntradaParaSaidaProdutos["status"] = 'Ativo';
					$dadosSalvaEntradaParaSaidaProdutos['excluido'] = 'nao';
					$dadosSalvaEntradaParaSaidaProdutos['logusuario'] = $this->_usuario['id'];
					$dadosSalvaEntradaParaSaidaProdutos['logdata'] = date('Y-m-d G:i:s');
					
					$row = $almoxarifadoprodutossaidas->save($dadosSalvaEntradaParaSaidaProdutos);
				} 
				
			} 
			$row = $almoxarifadosolicitacoesmateriaissaida->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}