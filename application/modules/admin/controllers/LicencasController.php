<?php

/**
 * Controle da classe licenças do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_LicencasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Licenca
	 */
	protected $_modal = null;	
	protected $_usuario = null;	
	protected $_isAdmin = false;
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("licencas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->_usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}		

		$modal = $this->getRequest()->getParam('modal');
		$this->_modal = ($modal=='true');
		if ($modal) {
			$this->view->modal = $this->_modal;
		
			$this->_redirect_modal = '/modal/true';
			$this->view->redirect_modal = $this->_redirect_modal;
		}
	}	
	
	public function logarAction() {
		$id = (int)$this->_request->getParam("id");
		$licencas = new Licencas();
		$licenca = $licencas->getLicencaById($id);
		
		
		$cadastro = Cadastros::getCadastroByIdHelper($licenca['idcadastro']);

 		$cadastros = new Cadastros();
 		$cadastros->autentica($cadastro['email'], $cadastro['senha'], $licenca['id'], 'default_mn');
 			
		$this->_redirect('/' . $licenca['alias']);
		
		die();
	}
	
	public function clonarlicencaAction() {
		//ID da licenca que vai receber o clone
		$to = (int)$this->getRequest()->getPost('to');
		//array com os dados da licença a ser clonada
		$parent = $this->getRequest()->getPost('parent');
		$parent = json_decode($parent, true);

		if ($to == '' && sizeof($parent) > 0) {
			die('Você deve escolher uma licença.');
		}
			
		
		/*
		*	LOCAIS UTEIS
		*/
		$locaisuteis = new Locaisuteis();

		$locais = $locaisuteis->fetchAll("excluido='nao' AND idlicenca=".$parent['id']);

		if(sizeof($locais) > 0)
		{
			foreach($locais as $v)
			{
				$dados = array();
				$dados['idlicenca'] = $to;
				$dados['local'] = $v['local'];
				$dados['idendereco'] = $v['idendereco'];
				$dados['telefone1'] = $v['telefone1'];
				$dados['telefone2'] = $v['telefone2'];
				$dados['telefone3'] = $v['telefone3'];
				$dados['contato1'] = $v['contato1'];
				$dados['contato2'] = $v['contato2'];
				$dados['email'] = $v['email'];
				$dados['observacao'] = $v['observacao'];

				$dados['status'] = 'Ativo';
				$dados['excluido'] = 'nao';
				$dados['logcadastro'] = Usuarios::getUsuario('id');
				$dados['logdata'] = date('Y-m-d G:i:s');	

				$locaisuteis->save($dados);
			}	
		}
		
		/*
		*	REQUERIMENTOS ASSUNTOS
		*/
		$requerimentosassuntos = new Requerimentosassuntos();

		$requerimentos = $requerimentosassuntos->fetchAll("excluido='nao' AND idlicenca=".$parent['id']);

		if(sizeof($requerimentos) > 0)
		{
			foreach($requerimentos as $v)
			{
				$dados = array();
				$dados['idlicenca'] = $to;
				$dados['requerimentoassunto'] = $v['requerimentoassunto'];

				$dados['status'] = $v['status'];
				$dados['excluido'] = 'nao';
				$dados['logcadastro'] = Usuarios::getUsuario('id');
				$dados['logdata'] = date('Y-m-d G:i:s');	

				$requerimentosassuntos->save($dados);
			}	
		}

		
		/*
		*	REQUERIMENTOS TIPOS
		*/
		$requerimentostipos = new Requerimentostipos();

		$requerimentostipo = $requerimentostipos->fetchAll("excluido='nao' AND idlicenca=".$parent['id']);

		if(sizeof($requerimentostipo) > 0)
		{
			foreach($requerimentostipo as $v)
			{
				$dados = array();
				$dados['idlicenca'] = $to;
				$dados['requerimentotipo'] = $v['requerimentotipo'];

				$dados['status'] = $v['status'];
				$dados['excluido'] = 'nao';
				$dados['logcadastro'] = Usuarios::getUsuario('id');
				$dados['logdata'] = date('Y-m-d G:i:s');	

				$requerimentostipos->save($dados);	
			}
		}
		
		/*
		*	REQUERIMENTOS SETORES
		*/
		$requerimentossetores = new Requerimentossetores();

		$requerimentos_setores = $requerimentossetores->fetchAll("excluido='nao' AND idlicenca=".$parent['id']);

		if(sizeof($requerimentos_setores) > 0)
		{
			foreach($requerimentos_setores as $v)
			{
				$dados = array();
				$dados['idlicenca'] = $to;
				$dados['requerimentosetor'] = $v['requerimentosetor'];

				$dados['status'] = $v['status'];
				$dados['excluido'] = 'nao';
				$dados['logcadastro'] = Usuarios::getUsuario('id');
				$dados['logdata'] = date('Y-m-d G:i:s');	

				$requerimentossetores->save($dados);
			}	
		}
		
		/*
		*	REQUERIMENTOS SETORES
		*/
		$requerimentosubsetores = new Requerimentossubsetores();

		$requerimentos_subsetores = $requerimentosubsetores->fetchAll("excluido='nao' AND idlicenca=".$parent['id']);

		if(sizeof($requerimentos_subsetores) > 0)
		{
			foreach($requerimentos_subsetores as $v)
			{
				$dados = array();
				$dados['idlicenca'] = $to;
				$dados['requerimentosubsetor'] = $v['requerimentosubsetor'];
				$dados['idrequerimentosetor'] = $v['idrequerimentosetor'];

				$dados['status'] = $v['status'];
				$dados['excluido'] = 'nao';
				$dados['logcadastro'] = Usuarios::getUsuario('id');
				$dados['logdata'] = date('Y-m-d G:i:s');	

				$requerimentosubsetores->save($dados);
			}	
		}
		
		/*
		*	ATENDIMENTOS TIPOS
		*/
		$atendimentostipos = new Atendimentostipos();

		$atendimentos = $atendimentostipos->fetchAll("excluido='nao' AND idlicenca=".$parent['id']);

		if(sizeof($atendimentos) > 0)
		{
			foreach($atendimentos as $v)
			{
				$dados = array();
				$dados['idlicenca'] = $to;
				$dados['atendimentotipo'] = $v['atendimentotipo'];

				$dados['status'] = $v['status'];
				$dados['excluido'] = 'nao';
				$dados['logcadastro'] = Usuarios::getUsuario('id');
				$dados['logdata'] = date('Y-m-d G:i:s');	

				$atendimentostipos->save($dados);
			}	
		}
		
		
		die('OK');
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Licencas();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['loglicenca'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$cadastros = new Cadastros();
			$cadastro = $cadastros->fetchRow("id=" . $row['idcadastro']);
			$row['cadastro'] = $cadastro->toArray();			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Licença excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="licencas") $objs = new Licencas();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['loglicenca'] = Usuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$cadastros = new Cadastros();
			$cadastro = $cadastros->fetchRow("id=" . $obj['idcadastro']);
			$obj['cadastro'] = $cadastro->toArray();
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'licencas',	'name' => 'Licenças'),
			array('url' => null,'name' => 'Visualizar licença')
		);	
				
		
		$id = (int)$this->_request->getParam("id");
		$licencas = new Licencas();
		$licenca = $licencas->getLicencaById($id);
		
		if (!$licenca) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $licenca;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Licenças')
		);
		
		//var_dump($this->_modal); die('aqui');
		$ns = new Zend_Session_Namespace('admin_licencas');
		$licencas = new Licencas();
		$queries = array();	
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ($this->view->post_var['licenca']!='') $queries['licenca'] = $this->view->post_var['licenca'];
    		if ($this->view->post_var['alias']!='') $queries['alias'] = $this->view->post_var['alias'];
    		if ($this->view->post_var['chave']!='') $queries['chave'] = $this->view->post_var['chave'];
    		if ($this->view->post_var['email']!='') $queries['email'] = $this->view->post_var['email'];
    		if ($this->view->post_var['status1']!='') $queries['status'] = $this->view->post_var['status1'];
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		if($this->_modal){
			$id = (int)$this->_request->getParam("id");
			$queries['diferent_id'] = $id;
		}
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $licencas->getLicencas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $licencas->getLicencas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de licenças
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'licencas',	'name' => 'Licenças'),
			array('url' => null,'name' => 'Editar licença')
		);	
				
		
		$id = (int)$this->_request->getParam("id");
		$licencas = new Licencas();
		$licenca = $licencas->getLicencaById($id);
		
		if (!$licenca) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $licenca;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$licenca = $licencas->fetchRow("id=".$id);
			$erros = $this->getPost($licenca->toArray());
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Licença editado com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de licenças 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'licencas',	'name' => 'Licenças'),
			array('url' => null,'name' => 'Adicionar licença')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Licença adicionado com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idlicenca
     */    
    private function preForm($idlicenca = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_licenca) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		
		$licenca = trim($this->getRequest()->getPost("licenca"));
		$alias = trim($this->getRequest()->getPost("alias"));
		$nomerazao = trim($this->getRequest()->getPost("nomerazao"));
		$sobrenomefantasia = trim($this->getRequest()->getPost("sobrenomefantasia"));
		$email = trim($this->getRequest()->getPost("email"));
		$senha = trim($this->getRequest()->getPost("senha"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		$erros = array();
			
		if (""==$licenca) array_push($erros, 'Preencha o campo LICENÇA.');
		if (""==$alias) array_push($erros, 'Preencha o campo ALIAS.');
		if (""==$nomerazao) array_push($erros, 'Preencha o campo NOME DO USUÁRIO.');
		if (""==$sobrenomefantasia) array_push($erros, 'Preencha o campo SOBRENOME DO USUÁRIO.');
		if (!Mn_Util::isValidEmail($email)) array_push($erros, 'O E-MAIL DE ACESSO é inválido.');
		if (($id==0) && (""==$senha)) array_push($erros, 'Preencha o campo SENHA.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$licencas = new Licencas();
		$row = $licencas->fetchRow("excluido='nao' AND alias='$alias' AND id<>".$id);
		if ($row) array_push($erros, 'Já existe licença com esse ALIAS.');
		
		$idcadastro = (($_licenca) && ($_licenca['idcadastro']>0)) ? $_licenca['idcadastro'] : 0;
		$cadastros = new Cadastros();
		$cadastro = $cadastros->fetchRow("excluido='nao' AND email='$email' AND id<>".$idcadastro);
		if ($cadastro) array_push($erros, 'Já existe licença com esse E-MAIL.');
		
		if (sizeof($erros)>0) return $erros; 
		
		
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			
			$dados['idcadastro'] = ($_licenca) ? $_licenca['idcadastro'] : 0;
			$dados['licenca'] = $licenca;
			$dados['alias'] = $alias;
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Usuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			$cadastro = array();
			$cadastro['idperfil'] = 0;
			$cadastro['idparent'] = 0;
			$cadastro['nomerazao'] = $nomerazao;
			$cadastro['sobrenomefantasia'] = $sobrenomefantasia;
			$cadastro['email'] = $email;
			if ($cadastro!='') $cadastro['senha'] = $senha;
			$cadastro['excluido'] = 'nao';
			$cadastro['logusuario'] = $dados['logusuario'];
			$cadastro['logdata'] = $dados['logdata'];
			
			$dados['cadastro'] = $cadastro;
			$row = $licencas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }

	
	
}