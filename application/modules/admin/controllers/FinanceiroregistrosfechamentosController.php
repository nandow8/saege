<?php

/**
 * Controle da classe financeiroregistrosfechamentos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_FinanceiroregistrosfechamentosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Financeiroregistrofechamento
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("financeiroregistrosfechamentos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Financeiroregistrosfechamentos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Fechamento Financeiro excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="financeiroregistrosfechamentos") $objs = new Financeiroregistrosfechamentos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeiroregistrosfechamentos', 'name' => 'Fechamentos Financeiros'),
			array('url' => null,'name' => 'Visualizar Fechamento Financeiro')
		);
		
		$id = (int)$this->_request->getParam("id");
		$financeiroregistrosfechamentos = new Financeiroregistrosfechamentos();
		$financeiroregistrofechamento = $financeiroregistrosfechamentos->getFinanceiroregistrofechamentoById($id, array());
		
		if (!$financeiroregistrofechamento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $financeiroregistrofechamento;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	public function getconveniosAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$id = (int)$this->getRequest()->getPost('idtipoconvenio');
		$idescola = (int)$this->getRequest()->getPost('idescola');
		//var_dump($idescola);die();
		if($id == 0 || $idescola == 0) die('ERRO');
		//$tipoconvenio = Financeirotiposconvenios::getFinanceirotipoconvenioByIdHelper($id);
		
		$queries = array();
		$queries['idtipoconvenio'] = $id;
		$queries['idescola'] = $idescola;
		 
		$escolasprogramas = new Escolasprogramasgov();
		$rows = $escolasprogramas->getEscolasprogramasgovHelper($queries);


		$this->view->rows = $rows;

	}

	public function getconvenioAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$idconvenio = (int)$this->getRequest()->getPost('idconvenio');
		//var_dump($idescola);die();
		if($idconvenio == 0)
		{
			echo json_encode(array('status' => 'ERROR', 'dados' => null));
			die();
		}
		//$tipoconvenio = Financeirotiposconvenios::getFinanceirotipoconvenioByIdHelper($id);
		
		 
		$escolasprogramas = new Escolasprogramasgov();
		$row = $escolasprogramas->getEscolaprogramagovById($idconvenio);
		if($row)
		{
			$row['datainicio'] = date('d/m/Y', strtotime($row['datainicio']));
			$row['datafim'] = date('d/m/Y', strtotime($row['datafim']));
		}


		echo json_encode(array('status' => 'OK', 'dados' => $row));
		die();

	}
	public function setconveniosAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$id = (int)$this->getRequest()->getPost('idtipoconvenio');
		$idescola = (int)Usuarios::getUsuario('idescola');
		//$tipoconvenio = Financeirotiposconvenios::getFinanceirotipoconvenioByIdHelper($id);
		
		$queries = array();
		//$queries['tipo'] = $tipoconvenio['titulo'];
		$queries['idescola'] = $idescola;
		$queries['idtipoconvenio'] = $id;

		if($id) $queries['idtipoconvenio'] = $id;
		if($idescola) $queries['idescola'] = $idescola;
		 
		$escolasprogramascontas = new Escolasprogramasgov();
		$rows = $escolasprogramascontas->getEscolasprogramasgov($queries);
		//var_dump($rows);die();

		$this->view->rows = $rows;
		//die('OK');
	}
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Fechamentos Financeiros')
		);
		
		$ns = new Zend_Session_Namespace('default_financeiroregistrosfechamentos');
		$financeiroregistrosfechamentos = new Financeiroregistrosfechamentos();
		$queries = array();	
		$queries['idescola'] = Usuarios::getUsuario('idescola');
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
		if(Usuarios::getUsuario('idescola') == 0){
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
		}else{
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = Usuarios::getUsuario('idescola');
		}
		if ($this->view->post_var["idtipoconvenio"]!="") $queries["idtipoconvenio"] = $this->view->post_var["idtipoconvenio"];
		if ($this->view->post_var["idprogramasgov"]!="") $queries["idprogramasgov"] = $this->view->post_var["idprogramasgov"];
		if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $financeiroregistrosfechamentos->getFinanceiroregistrosfechamentos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $financeiroregistrosfechamentos->getFinanceiroregistrosfechamentos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de financeiroregistrosfechamentos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeiroregistrosfechamentos', 'name' => 'Fechamentos Financeiros'),
			array('url' => null,'name' => 'Editar Fechamento Financeiro')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$financeiroregistrosfechamentos = new Financeiroregistrosfechamentos();
		$financeiroregistrofechamento = $financeiroregistrosfechamentos->getFinanceiroregistrofechamentoById($id);
		
		if (!$financeiroregistrofechamento || $financeiroregistrofechamento['finalizado'] == 'sim') 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $financeiroregistrofechamento;
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($financeiroregistrofechamento);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Fechamento Financeiro editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de financeiroregistrosfechamentos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeiroregistrosfechamentos', 'name' => 'Fechamentos Financeiros'),
			array('url' => null,'name' => 'Adicionar Fechamento Financeiro')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Fechamento Financeiro adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idfinanceiroregistrofechamento
     */    
    private function preForm($idfinanceiroregistrofechamento = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_financeiroregistrofechamento = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idtipoconvenio = (int)trim($this->getRequest()->getPost("idtipoconvenio"));
		$idescolasapms = (int)trim($this->getRequest()->getPost("idescolasapms"));
		$idprogramasgov = (int)trim($this->getRequest()->getPost("idprogramasgov"));
		$saldoaplicacaofinanceira = MN_Util::trataNum(trim($this->getRequest()->getPost("saldoaplicacaofinanceira")));
		$status = trim($this->getRequest()->getPost("status1"));

		$finalizado = trim($this->getRequest()->getPost("finalizado"));
		//var_dump($finalizado);die();
				
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
		if (0==$idtipoconvenio) array_push($erros, "Informe a Tipo de Convênio.");
		//if (0==$idarquivo) array_push($erros, "Informe a Convênio.");
		if (0==$idprogramasgov) array_push($erros, "Informe a Programas Sociais.");
		if (""==$status) array_push($erros, "Informe a Status.");

		$idarquivo = $this->getArquivo('idarquivo');
		if ("sim"==$finalizado && $idarquivo == 0) array_push($erros, "Você deve anexar um relatório.");
		
		$financeiroregistrosfechamentos = new Financeiroregistrosfechamentos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;

			$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;

			
			$dados["idescola"] = $idescola;
			$dados["idtipoconvenio"] = $idtipoconvenio;
			$dados["idescolasapms"] = $idescolasapms;
			$dados["idprogramasgov"] = $idprogramasgov;
			$dados["saldoaplicacaofinanceira"] = $saldoaplicacaofinanceira;
			$dados["status"] = $status;
			$dados["finalizado"] = ($finalizado == 'sim') ? 'sim' : 'nao';

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			//var_dump($dados);die();
			$row = $financeiroregistrosfechamentos->save($dados);
			
			$db->commit();


		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}