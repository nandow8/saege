<?php

class MYPDF extends TCPDF {

	//Page header
	public function Header(){
		$image_file = "public/admin/imagens/logosantaisabel.jpg";
		$this->Image($image_file, 23, 5, 20, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', '', 5);
        $this->SetXY(21, 26);
        $this->Cell(0, 0, 'Paraíso da Grande São Paulo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
       
        // cabeçalho Endereço 
        $this->SetFont('helvetica', '', 12);
        $this->SetXY(79, 6);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Municipio de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', 'B', 10);
        $this->SetXY(75, 10);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Secretaria Municipal de Educação', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(70, 14);
        $this->SetTextColor(130, 130, 130);
		$this->Cell(0, 0, 'Tel. 4656-2440 e-mail: sec.educacao@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 8);
        $this->SetXY(70, 18);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297-Centro - CEP: 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 12);
        $this->SetXY(68, 25);
        $this->SetTextColor(0, 0, 0);
        $this->Cell(0, 0, 'Mapa de acompanhamento da classe', 0, false, 'L', 0, '', 0, false, 'M', 'M');


        //informações Saege
        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

       	$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();

        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,18);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: '.$data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 10);
        $this->SetXY(40,21);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        	

	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
        $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Position at 15 mm from bottom
		$this->SetXY(150,-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

/**
 * Controle da classe mapaacompanhamentoclasse do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_MapaacompanhamentoclasseController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Mapaacompanhamentoclasse
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("mapaacompanhamentoclasse", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}


		$mapaacompanhamentoclasse = new Mapaacompanhamentoclasse();
		$queries = array();	
		$ultimo = $mapaacompanhamentoclasse->getMapaacompanhamentoclasse($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Mapaacompanhamentoclasse();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mapa de acompanhamento excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	// public function changestatusxmlAction() {
	// 	$this->_helper->layout->disableLayout();
	// 	$this->_response->setHeader("content-type", "text/xml");
		
	// 	$id = (int)$this->getRequest()->getPost("id");
	// 	$op = $this->getRequest()->getPost("op");
		
	// 	if ($op=="mapaacompanhamentoclasse") $objs = new Mapaacompanhamentoclasse();
	// 	$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
	// 	if ($obj) {
	// 		$obj = $obj->toArray();
	// 		$obj['status'] = ($obj['status']=="Em Aberto") ? "Fechado" : "Em Aberto";
	// 		$obj['logusuario'] = $this->_usuario['id'];
	// 		$obj['logdata'] = date('Y-m-d G:i:s');
			
	// 		$objs->save($obj);
			
	// 		die($obj['status']);
	// 	}
		
	// 	die("Não encontrado!");
	// }		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'mapaacompanhamentoclasse', 'name' => 'Mapa de acompanhamento de classe e sondagem'),
			array('url' => null,'name' => 'Visualizar Mapa de acompanhamento de classe e sondagem')
		);
		
		$id = (int)$this->_request->getParam("id");
		$mapaacompanhamentoclasse = new Mapaacompanhamentoclasse();
		$mapaacompanhamentoclasse = $mapaacompanhamentoclasse->getMapaacompanhamentoclasseById($id, array());
		
		if (!$mapaacompanhamentoclasse) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $mapaacompanhamentoclasse;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Mapa de acompanhamento de classe e sondagem')
		);
		
		$ns = new Zend_Session_Namespace('default_contas');
		$mapaacompanhamentoclasse = new Mapaacompanhamentoclasse();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
    		if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];

			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
		}		
		
		$idperfil = Usuarios::getUsuario('idperfil');
		$idescola = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idescola'];
		
		if($idperfil != 30) {  // 30 superadmin que ve tudo 
			$queries['idescola'] = $idescola;
		}
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $mapaacompanhamentoclasse->getMapaacompanhamentoclasse($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $mapaacompanhamentoclasse->getMapaacompanhamentoclasse($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de mapaacompanhamentoclasse
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'mapaacompanhamentoclasse', 'name' => 'Mapa de acompanhamento de classe e sondagem'),
			array('url' => null,'name' => 'Editar Mapa de acompanhamento de classe e sondagem')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$mapaacompanhamentoclasse = new Mapaacompanhamentoclasse();
		$mapaacompanhamentoclasse = $mapaacompanhamentoclasse->getMapaacompanhamentoclasseById($id);
		
		if (!$mapaacompanhamentoclasse) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $mapaacompanhamentoclasse;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($mapaacompanhamentoclasse);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mapa de acompanhamento de classe e sondagem editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de mapaacompanhamentoclasse 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'mapaacompanhamentoclasse', 'name' => 'Mapa de acompanhamento de classe e sondagem'),
			array('url' => null,'name' => 'Adicionar Mapa de acompanhamento de classe e sondagem')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mapa de acompanhamento de classe e sondagem adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idtreinamento
     */    
    private function preForm($idmapaacompanhamentoclasse = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_mapaacompanhamentoclasse = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idusuariologado = trim($this->getRequest()->getPost("idusuariologado"));
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$datalancamento = Mn_Util::stringToTime(trim($this->getRequest()->getPost("datalancamento")));
		$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
		$idescola = trim($this->getRequest()->getPost("idescola"));
		$idserie = trim($this->getRequest()->getPost("idserie"));
		$idturma = trim($this->getRequest()->getPost("idturma"));
		$idperiodo = trim($this->getRequest()->getPost("idperiodo"));
		$iddiretor = trim($this->getRequest()->getPost("iddiretor"));
		$idcoordenador = trim($this->getRequest()->getPost("idcoordenador"));
		$periodonotas = trim($this->getRequest()->getPost("periodonotas"));

		$idaluno = $this->getRequest()->getPost("idaluno");
		$idhipotese = $this->getRequest()->getPost("idhipotese");
		$idescrita = $this->getRequest()->getPost("idescrita");
		$idleitura = $this->getRequest()->getPost("idleitura");
		$idaspectonormativo = $this->getRequest()->getPost("idaspectonormativo");
		$inicial = $this->getRequest()->getPost("inicial");
		$observacoe = $this->getRequest()->getPost("observacoe");
		
		$extraiObservacoes = array();
		foreach ($observacoe as $key => $value) {
			if($value != null)
			$extraiObservacoes[] = $value;
		}
		
		$status = trim($this->getRequest()->getPost("status1"));	
		
		$erros = array();
		
		if (""==$sequencial) array_push($erros, "Informe o sequencial.");
		if (""==$idescola) array_push($erros, "Informe a escola.");
		if (""==$idserie) array_push($erros, "Informe a série.");
		if (""==$idturma) array_push($erros, "Informe a turma.");
		if (""==$idperiodo) array_push($erros, "Informe o período.");
		
		$mapaacompanhamentoclasse = new Mapaacompanhamentoclasse();
		$mapaescolasalunosrelacao = new Mapaescolasalunosrelacao();		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idusuariologado"] = $idusuariologado;
			$dados["sequencial"] = $sequencial;
			$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			$dados["horalancamento"] = $horalancamento;
			$dados["idescola"] = $idescola;
			$dados["idserie"] = $idserie;
			$dados["idturma"] = $idturma;
			$dados["idperiodo"] = $idperiodo;
			$dados["iddiretor"] = $iddiretor;
			$dados["idcoordenador"] = $idcoordenador;
			$dados["periodonotas"] = $periodonotas; 
			
			$dados["status"] = $status;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
		
			$row = $mapaacompanhamentoclasse->save($dados);

			$mapaescolasalunosrelacao->setExcluido($row['id']);

			$i=0;
			foreach ($idaluno as $key => $aluno) {
				$_dados = array();
				$_dados['idmapa'] = $row['id'];
				$_dados['idaluno'] = $aluno;
				$_dados['idtipoleitura'] = $idleitura[$i];
				$_dados['idtipoescrita'] = $idescrita[$i];
				$_dados['idtipohipotese'] = $idhipotese[$i];
				$_dados['idtipoaspectonormativo'] = $idaspectonormativo[$i];
				$_dados['inicial'] = $inicial[$i];
				$_dados['observacoes'] = $extraiObservacoes[$i];
				$_dados['excluido'] = 'nao';

				$_row = $mapaescolasalunosrelacao->save($_dados);
				$i++;
				
			}

			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}
		
		return "";    	
    }

    public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$idusuariologado = (int)$this->_request->getPost("idusuariologado");
		
		$rows = new Escolasseries();
 
		$queries = array();
		$queries['idescola'] = $idescola;
		$queries['vinculos'] = $rows->getVinculosprofessor($idusuariologado)[0]['ids'];;
		
		$this->view->rows = $rows->getEscolasseriesdoprofessor($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setturmasAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");
		
		$queries = array(); 
		$queries['idescola'] = $idescola;
		$queries['idserie'] = $idserie;
		$queries['idprofessor'] = Usuarios::getUsuario('idfuncionario');
		
		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculosprofessor($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0)) $this->view->rows = null;
		
	}
	public function setperiodoAction() {
		$this->_helper->layout->disableLayout();
		
		$idvinculo = (int)$this->_request->getPost("idvinculo");

		$queries = array();
		$queries['id'] = $idvinculo;
		
		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculos($queries);

		if((int)$idvinculo <= 0) $this->view->rows = null;
		
	}

	public function setperiodonotasAction() {
		$this->_helper->layout->disableLayout();

		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		// $queries['idescola'] = $idescola;

		$rows = new Escolasconfiguracoes();
		$this->view->rows = $rows->getEscolasconfiguracoes($queries);

		if((int)$idescola <= 0) $this->view->rows = null;
	}

    public function setcoordenadoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idescola"] = $idescola;
		$queries["coordenador"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setdiretoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idescola"] = $idescola;
		$queries["diretor"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");
		$idvinculo = (int)$this->_request->getPost("idvinculo");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		$queries['idvinculo'] = $idvinculo;
		
		$rows = new Escolassalasatribuicoes();
		$this->view->rows = $rows->getSalasatribuicoes($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0) || ((int)$idvinculo <= 0)) $this->view->rows = null;		
	}

	public function setcoresAction() {
		$this->_helper->layout->disableLayout(); 

		$id = (int)$this->getRequest()->getPost('id');
		$tipo = $this->getRequest()->getPost('tipo');
	 
		if($tipo == 'hipotese')
			$rows = Coordenacaotipohipoteses::GetCoordenacaotipohipotesesHelper(array('status'=>'Ativo', 'id' => $id));
		
		if($tipo == 'leitura')
			$rows = Coordenacaotipoleituras::GetCoordenacaotipoleiturasHelper(array('status'=>'Ativo', 'id' => $id));
		
		if($tipo == 'escrita')
			$rows = Coordenacaotipoescritas::GetCoordenacaotipoescritasHelper(array('status'=>'Ativo', 'id' => $id)); 
		
		if($tipo == 'aspectonormativo')
			$rows = Coordenacaoaspectosnormativos::getCoordenacaoaspectosnormativosHelper(array('status'=>'Ativo', 'id' => $id));
		 

		echo json_encode($rows);
		die();
	
	}

	public function relatorioAction(){     
		$this->view->bread_crumb = array(
			array('url' => 'mapaacompanhamentoclasse', 'name' => 'Mapa de acompanhamento de classe e sondagem'),
			array('url' => null,'name' => 'Editar Mapa de acompanhamento de classe e sondagem')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$mapaacompanhamentoclasse = new Mapaacompanhamentoclasse();
		$mapaacompanhamentoclasse = $mapaacompanhamentoclasse->getMapaacompanhamentoclasseById($id);
		
		if (!$mapaacompanhamentoclasse) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		$this->view->mapaacompanhamentoclasse = $mapaacompanhamentoclasse;
			
		$queries = array();
		$queries['idserie'] = $mapaacompanhamentoclasse['idserie'];
		$queries['idescola'] = $mapaacompanhamentoclasse['idescola'];
		$queries['idvinculo'] = $mapaacompanhamentoclasse['idturma'];
		
		$escolasatribuicoes = new Escolassalasatribuicoes();
		$this->view->escolasatribuicoes = $escolasatribuicoes->getSalasatribuicoes($queries); 
 

		$patrimonios = new Patrimonios();
			$data = $patrimonios->getCurrentData();
		$local = 'Santa Isabel';

		$localData = $local.' , '.$data;
 
		$this->view->localdata = $localData;

		$this->preForm();

		$db = Zend_Registry::get('db');

			// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$this->view->pdf = $pdf;

		$pdf->setPageOrientation('e');
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
		$pdf->SetTitle('Mapa de acompanhamento da classe');
		// $pdf->SetSubject('Assinatura do Funcionário');

		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->SetFont('helvetica', '', 7);

		// add a page        
		$pdf->AddPage();
		
		$html = $this->view->render('mapaacompanhamentoclasse/pdf/relatorio.phtml');
		$image_file = "public/admin/imagens/logosantaisabel.jpg";

		$pdf->writeHTML($html, true, 0, true, 0);

		$filename = 'produtos_estoque.pdf';
		$pdf->Output($filename, 'I');
		
		die();
		return true; 
	}
    
}