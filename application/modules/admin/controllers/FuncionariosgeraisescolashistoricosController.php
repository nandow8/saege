<?php

/**
 * Controle da classe Funcionariosgeraisescolashistoricos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_FuncionariosgeraisescolashistoricosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Funcionariogeralescolahistorico
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("Funcionariosgeraisescolashistoricos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Funcionariosgeraisescolashistoricos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Histórico excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="Funcionariosgeraisescolashistoricos") $objs = new Funcionariosgeraisescolashistoricos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'Funcionariosgeraisescolashistoricos', 'name' => 'Históricos de Funcionários das escolas'),
			array('url' => null,'name' => 'Visualizar Histórico')
		);
		
		$id = (int)$this->_request->getParam("id");
		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
		$funcionariogeralescola = $funcionariosgeraisescolas->getFuncionariogeralescolaById($id, array());
		
		if (!$funcionariogeralescola) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $funcionariogeralescola;

		$queries = array();
		$queries['idparent'] = $funcionariogeralescola['id'];
		$Funcionariosgeraisescolashistoricos = new Funcionariosgeraisescolashistoricos();
		$this->view->rows = $Funcionariosgeraisescolashistoricos->getFuncionariosgeraisescolashistoricos($queries);
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Históricos de Funcionários das escolas')
		);
		
		$ns = new Zend_Session_Namespace('default_Funcionariosgeraisescolashistoricos');
		$funcionariosgerais = new Funcionariosgeraisescolas();
		$queries = array();	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['modulo']= "Escola";
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["nome"]!="") $queries["nome"] = $this->view->post_var["nome"];
if ($this->view->post_var["telefone"]!="") $queries["telefone"] = $this->view->post_var["telefone"];
if ($this->view->post_var["tipo"]!="") $queries["tipo"] = $this->view->post_var["tipo"];
if ($this->view->post_var["iddepartamentosecretaria"]!="") $queries["iddepartamentosecretaria"] = $this->view->post_var["iddepartamentosecretaria"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $funcionariosgerais->getFuncionariosgeraisescolas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $funcionariosgerais->getFuncionariosgeraisescolas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de Funcionariosgeraisescolashistoricos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'Funcionariosgeraisescolashistoricos', 'name' => 'Históricos de Funcionários das escolas'),
			array('url' => null,'name' => 'Editar Histórico')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
		$funcionariogeralescola = $funcionariosgeraisescolas->getFuncionariogeralescolaById($id, array());
		
		if (!$funcionariogeralescola) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $funcionariogeralescola;

		$queries = array();
		$queries['idparent'] = $funcionariogeralescola['id'];
		$Funcionariosgeraisescolashistoricos = new Funcionariosgeraisescolashistoricos();
		$this->view->rows = $Funcionariosgeraisescolashistoricos->getFuncionariosgeraisescolashistoricos($queries);
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($funcionariogeralescola);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Histórico editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de Funcionariosgeraisescolashistoricos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'Funcionariosgeraisescolashistoricos', 'name' => 'Históricos de Funcionários das escolas'),
			array('url' => null,'name' => 'Adicionar Histórico')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Histórico adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idfuncionariogeralescolahistorico
     */    
    private function preForm($idfuncionariogeralescolahistorico = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_funcionariogeralescolahistorico = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idparent = (int)trim($this->getRequest()->getPost("idparent"));
$idsecretaria = (int)trim($this->getRequest()->getPost("idsecretaria"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$iddepartamentoescola = (int)trim($this->getRequest()->getPost("iddepartamentoescola"));
$idimagem = (int)trim($this->getRequest()->getPost("idimagem"));
$idfuncao = (int)trim($this->getRequest()->getPost("idfuncao"));
$idcargo = (int)trim($this->getRequest()->getPost("idcargo"));
$datafimbolsa = trim($this->getRequest()->getPost("datafimbolsa"));
$datainiciobolsa = trim($this->getRequest()->getPost("datainiciobolsa"));
$bolsista = trim($this->getRequest()->getPost("bolsista"));
$regime_outros = trim($this->getRequest()->getPost("regime_outros"));
$regime = trim($this->getRequest()->getPost("regime"));
$conta = trim($this->getRequest()->getPost("conta"));
$dataregistroctps = trim($this->getRequest()->getPost("dataregistroctps"));
$ctps = trim($this->getRequest()->getPost("ctps"));
$filiacaopai = trim($this->getRequest()->getPost("filiacaopai"));
$filiacaomae = trim($this->getRequest()->getPost("filiacaomae"));
$carteiraserie = trim($this->getRequest()->getPost("carteiraserie"));
$carteiranumero = trim($this->getRequest()->getPost("carteiranumero"));
$celular = trim($this->getRequest()->getPost("celular"));
$agencia = trim($this->getRequest()->getPost("agencia"));
$banco = trim($this->getRequest()->getPost("banco"));
$formapagamento = trim($this->getRequest()->getPost("formapagamento"));
$salarioinicial = MN_Util::trataNum(trim($this->getRequest()->getPost("salarioinicial")));
$riscofuncao = trim($this->getRequest()->getPost("riscofuncao"));
$descricaofuncao = trim($this->getRequest()->getPost("descricaofuncao"));
$funcao = trim($this->getRequest()->getPost("funcao"));
$setor = trim($this->getRequest()->getPost("setor"));
$secretaria = trim($this->getRequest()->getPost("secretaria"));
$matricula = trim($this->getRequest()->getPost("matricula"));
$dataadmissaocomissionado = trim($this->getRequest()->getPost("dataadmissaocomissionado"));
$cargocomissionado = trim($this->getRequest()->getPost("cargocomissionado"));
$cargo = trim($this->getRequest()->getPost("cargo"));
$cbo = trim($this->getRequest()->getPost("cbo"));
$dataadmissao = trim($this->getRequest()->getPost("dataadmissao"));
$filhosquantidade = trim($this->getRequest()->getPost("filhosquantidade"));
$filhos = trim($this->getRequest()->getPost("filhos"));
$conjugenome = trim($this->getRequest()->getPost("conjugenome"));
$estadocivil = trim($this->getRequest()->getPost("estadocivil"));
$grauinstrucao = trim($this->getRequest()->getPost("grauinstrucao"));
$localnascimento = trim($this->getRequest()->getPost("localnascimento"));
$nacionalidade = trim($this->getRequest()->getPost("nacionalidade"));
$obs_estrangeiro = trim($this->getRequest()->getPost("obs_estrangeiro"));
$estrangeiro = trim($this->getRequest()->getPost("estrangeiro"));
$cnh_validade = trim($this->getRequest()->getPost("cnh_validade"));
$cnh_categoria = trim($this->getRequest()->getPost("cnh_categoria"));
$cnh_numero = trim($this->getRequest()->getPost("cnh_numero"));
$cnh = trim($this->getRequest()->getPost("cnh"));
$pispasep = trim($this->getRequest()->getPost("pispasep"));
$cpf = trim($this->getRequest()->getPost("cpf"));
$rg = trim($this->getRequest()->getPost("rg"));
$secaoeleitoral = trim($this->getRequest()->getPost("secaoeleitoral"));
$zonaeleitoral = trim($this->getRequest()->getPost("zonaeleitoral"));
$tituloeleitor = trim($this->getRequest()->getPost("tituloeleitor"));
$reservista_ra = trim($this->getRequest()->getPost("reservista_ra"));
$reservista = trim($this->getRequest()->getPost("reservista"));
$emissaocarteira = trim($this->getRequest()->getPost("emissaocarteira"));
$datanascimento = trim($this->getRequest()->getPost("datanascimento"));
$email = trim($this->getRequest()->getPost("email"));
$sobrenomefantasia = trim($this->getRequest()->getPost("sobrenomefantasia"));
$nomerazao = trim($this->getRequest()->getPost("nomerazao"));
$professor = trim($this->getRequest()->getPost("professor"));
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$nome = trim($this->getRequest()->getPost("nome"));
$telefone = trim($this->getRequest()->getPost("telefone"));
$modulo = trim($this->getRequest()->getPost("modulo"));
$tipo = trim($this->getRequest()->getPost("tipo"));
$idendereco = (int)trim($this->getRequest()->getPost("idendereco"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$tipo) array_push($erros, "Informe a Tipo.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$Funcionariosgeraisescolashistoricos = new Funcionariosgeraisescolashistoricos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$enderecosModel = new EnderecoModel($this);
			$endereco = $enderecosModel->getPost( ($_funcionariogeralescolahistorico) ? (int)$_funcionariogeralescolahistorico['idendereco'] : 0, "e_idendereco");
			$idendereco = ($endereco) ? $endereco['id'] : 0;	
			$dados['idendereco'] = $idendereco;

$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $Funcionariosgeraisescolashistoricos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}