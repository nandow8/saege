<?php

class Admin_LtecalendarioController extends Zend_Controller_Action {
    
    /**
     * Propriedade protegida que contem os dados do usário logado
     */
    protected $_usuario = null;

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Calendario')
        );

        $ns = new Zend_Session_Namespace('logistica_calendario');
        $calendario = new Ltecalendario();
        $queries = array(); 

        $this->preForm();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) {
        }           
        // var_dump($calendario->getEventos($queries)); die();
        $this->view->rows = $calendario->getEventos($queries);
    }

    /**
     * Listagem
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Calendario')
        );

        $ns = new Zend_Session_Namespace('logistica_calendario');
        $calendario = new Ltecalendario();
        $queries = array(); 

        $this->preForm();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) {
        }           
        // var_dump($calendario->getEventos($queries)); die();
        $this->view->rows = $calendario->getEventos($queries);
    }

    /**
     * 
     * Action de adição de calendario 
     */
    public function adicionarAction() {
        $this->_helper->layout->disableLayout();

        $title = strip_tags(trim($this->getRequest()->getPost("title")));
        $color = strip_tags(trim($this->getRequest()->getPost("color")));
        $start = strip_tags(trim($this->getRequest()->getPost("start")));
        $a = explode('/', $start);
        $start = $a[2].'-'.$a[1].'-'.$a[0].' 00:00:00';
        $end = strip_tags(trim($this->getRequest()->getPost("end")));
        $a = explode('/', $end);
        $end = $a[2].'-'.$a[1].'-'.$a[0].' 23:59:59';
        $municipal = strip_tags(trim($this->getRequest()->getPost("municipal")));
        $municipal = $municipal ? 'sim' : 'nao';
        $estadual = strip_tags(trim($this->getRequest()->getPost("estadual")));
        $estadual = $estadual ? 'sim' : 'nao';
        
        $calendario = new Ltecalendario();

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            $dados['title'] = $title;
            $dados['color'] = $color;
            $dados['start'] = $start;
            $dados['end'] = $end;
            $dados['municipal'] = $municipal;
            $dados['estadual'] = $estadual;

            $dados['excluido'] = 'nao';
            $dados['logusuario'] = Usuarios::getUsuario('id');
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $calendario->save($dados);
            
            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die(false);
        }

        die(true);        
    }

    public function excluirAction() {
        $this->_helper->layout->disableLayout();

        $id = (int)$this->getRequest()->getPost("id");

        $calendario = new Ltecalendario();

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            $dados['id'] = $id;

            $dados['excluido'] = 'sim';
            $dados['logusuario'] = Usuarios::getUsuario('id');
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $calendario->save($dados);
            
            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die(false);
        }

        die(true);
    }

    public function diasuteisAction() {
        header('Content-Type: application/json');
        $this->_helper->layout->disableLayout();

        $start = $this->getRequest()->getPost("start");
        $end = $this->getRequest()->getPost("end");
        $tipo = $this->getRequest()->getPost("tipo");
        $tipotransporte = $this->getRequest()->getPost("tipotransporte");
        $municipal = 'nao';
        $estadual = 'nao';

        $dias = $this->date2date($start, $end);

        if ($tipo == 'municipal' || $tipo == 'estadual') {
            $query = array("start" => $start, "end" => $end, "tipo" => $tipo);

            if ($tipo == 'municipal') {
                $query['municipal'] = true;
            } elseif ($tipo == 'estadual') {
                $query['estadual'] = true;
            }

            $calendario = new Ltecalendario();

            $eventos = $calendario->getEventos($query);

            foreach ($eventos as $key => $evento) {
                $dias = array_diff($dias, $this->date2date($evento['dt_start'], $evento['dt_end']));
            }
        }

        $diasuteis = $this->diasuteis($dias);

        echo $diasuteis;
    }

    function date2date($start, $end) {
        $dias = array();
        $i = 0;

        do {
            $dias[$i] = $start;
            $start = $this->Soma1dia($start);
            $i++;
        } while ($this->dataToTimestamp($start) <= $this->dataToTimestamp($end));

        return $dias;
    }


    function diasdasemana($start, $end) {
        $cont = 0;
        while ($start <= $end) {
            $diaSemana = date("w", $this->dataToTimestamp($start));
            if ($diaSemana != 0 && $diaSemana != 6) {
                $cont++;
            }
            $start = $this->Soma1dia($start);
        }

        return $cont;
    }

    function diasuteis($dias = array()) {
        
        $cont = 0;
        foreach ($dias as $dia) {
            $diaSemana = date("w", $this->dataToTimestamp($dia));
            if ($diaSemana != 0 && $diaSemana != 6) {
                $cont++;
            }
        }

        return $cont;
    }

    //SOMA 01 DIA   
    function Soma1dia($data){   
        $ano = substr($data, 0,4);
        $mes = substr($data, 5,2);
        $dia = substr($data, 8,2);
        return date("Y-m-d", mktime(0, 0, 0, $mes, $dia+1, $ano));
    }

    //FORMATA COMO TIMESTAMP
    function dataToTimestamp($data){
        $ano = substr($data, 0,4);
        $mes = substr($data, 5,2);
        $dia = substr($data, 8,2);
        return mktime(0, 0, 0, $mes, $dia, $ano);  
    } 

    /**
     * Atribui valores ao view
     * @param int $id
     */    
    private function preForm($id = 0) {
    }

    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_registro = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);
        
        $id = (int)$this->getRequest()->getPost("id");
        $title = strip_tags(trim($this->getRequest()->getPost("title")));
        $color = strip_tags(trim($this->getRequest()->getPost("color")));
        $start = strip_tags(trim($this->getRequest()->getPost("start")));
        $end = strip_tags(trim($this->getRequest()->getPost("end")));
        $municipal = strip_tags(trim($this->getRequest()->getPost("municipal")));
        $estadual = strip_tags(trim($this->getRequest()->getPost("estadual")));

        $calendario = new Ltecalendario();

        if (sizeof($erros)>0) return $erros; 
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
         
            $dados = array();           
            $dados['id'] = $id;
            $dados['title'] = $title;
            $dados['color'] = $color;
            $dados['start'] = $start;
            $dados['end'] = $end;
            $dados['municipal'] = $municipal;
            $dados['estadual'] = $estadual;

            $dados['excluido'] = 'nao';

            $row = $calendario->save($dados);
            
            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die();
        }       

        return "";      
    }
}