<?php

/**
 * Controle da classe logisticamultas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_LogisticamultasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Logisticamulta
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("logisticamultas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
		
		$logisticamultas = new Logisticamultas();
		$queries = array();	
		$ultimo = $logisticamultas->getUltimoLogisticamulta($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;

	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Logisticamultas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Multa excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="logisticamultas") $objs = new Logisticamultas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticamultas', 'name' => 'Multas'),
			array('url' => null,'name' => 'Visualizar Multa')
		);
		
		$id = (int)$this->_request->getParam("id");
		$logisticamultas = new Logisticamultas();
		$logisticamulta = $logisticamultas->getLogisticamultaById($id, array());
		
		if (!$logisticamulta) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticamulta;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Multas')
		);
		
		$ns = new Zend_Session_Namespace('default_logisticamultas');
		$logisticamultas = new Logisticamultas();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idmotorista"]!="") $queries["idmotorista"] = $this->view->post_var["idmotorista"];
if ($this->view->post_var["idfrota"]!="") $queries["idfrota"] = $this->view->post_var["idfrota"];
if ($this->view->post_var["idtipomulta"]!="") $queries["idtipomulta"] = $this->view->post_var["idtipomulta"];
if ($this->view->post_var["datamulta_i"]!="") $queries["datamulta_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datamulta_i"]));
if ($this->view->post_var["datamulta_f"]!="") $queries["datamulta_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datamulta_f"]));
if ($this->view->post_var["horamulta"]!="") $queries["horamulta"] = $this->view->post_var["horamulta"];
if ($this->view->post_var["pago"]!="") $queries["pago"] = $this->view->post_var["pago"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $logisticamultas->getLogisticamultas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $logisticamultas->getLogisticamultas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de logisticamultas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticamultas', 'name' => 'Multas'),
			array('url' => null,'name' => 'Editar Multa')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$logisticamultas = new Logisticamultas();
		$logisticamulta = $logisticamultas->getLogisticamultaById($id);
		
		if (!$logisticamulta) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticamulta;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($logisticamulta);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Multa editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de logisticamultas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticamultas', 'name' => 'Multas'),
			array('url' => null,'name' => 'Adicionar Multa')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Multa adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
	}	
	
	public function getdadosveiculoAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");		

		$id = (int)$this->getRequest()->getPost('idfrota');
	    $veiculo = new Logisticafrotas();
		$rows = $veiculo->getLogisticafrotaByIdHelper($id);
  
		echo json_encode($rows);
		die(); 
	}

	public function getdadosmultaAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");		

		$id = (int)$this->getRequest()->getPost('idmotivoinfracao');
	    $tipomulta = new Logisticatiposmultas();
		$rows = $tipomulta->getLogisticatiposmultaByIdHelper($id);
  
		echo json_encode($rows);
		die();

	}
    
    /**
     * Atribui valores ao view
     * @param int $idlogisticamulta
     */    
    private function preForm($idlogisticamulta = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_logisticamulta = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id"); 
		$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
$idmotorista = (int)trim($this->getRequest()->getPost("idmotorista"));
$idfrota = (int)trim($this->getRequest()->getPost("idfrota"));
// $idtipomulta = (int)trim($this->getRequest()->getPost("idtipomulta"));
$idcidade = (int)trim($this->getRequest()->getPost("idcidade"));
$sequencial = trim($this->getRequest()->getPost("sequencial"));
$ocorrenciamulta = trim($this->getRequest()->getPost("ocorrenciamulta"));
// $pontuacaomulta = trim($this->getRequest()->getPost("pontuacaomulta"));
// $motivomulta = trim($this->getRequest()->getPost("motivomulta"));
$numeroait = trim($this->getRequest()->getPost("numeroait"));
$localmulta = trim($this->getRequest()->getPost("localmulta"));
$datamulta = Mn_Util::stringToTime($this->getRequest()->getPost("datamulta"));
$horamulta = trim($this->getRequest()->getPost("horamulta"));
$orgaomulta = trim($this->getRequest()->getPost("orgaomulta"));
$enquadramento = trim($this->getRequest()->getPost("enquadramento"));
$datavencimentomulta = Mn_Util::stringToTime($this->getRequest()->getPost("datavencimentomulta"));
$valormulta = MN_Util::trataNum(trim($this->getRequest()->getPost("valormulta")));
$pago = trim($this->getRequest()->getPost("pago"));
$status = trim($this->getRequest()->getPost("status1"));
$idmotivoinfracao = (int)$this->getRequest()->getPost("idmotivoinfracao");
		
		
		$erros = array();
		
		if (0==$idmotorista) array_push($erros, "Informe a Motorista.");
if (0==$idfrota) array_push($erros, "Informe a Frota.");
if (0==$idmotivoinfracao) array_push($erros, "Informe o motivo da infração.");
// if (0==$idtipomulta) array_push($erros, "Informe a Tipo.");
if (0==$idcidade) array_push($erros, "Informe a Município.");
// if (""==$ocorrenciamulta) array_push($erros, "Informe a Ocorrencia.");
// if (""==$motivomulta) array_push($erros, "Informe a Motivo da Infração.");
if (""==$datamulta) array_push($erros, "Informe a Data da multa.");
if (""==$horamulta) array_push($erros, "Informe a Hora da multa.");
if (""==$datavencimentomulta) array_push($erros, "Informe a Data de vencimento.");
if (""==$pago) array_push($erros, "Informe a Pago.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$logisticamultas = new Logisticamultas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			
			$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;
			
			$dados["idmotorista"] = $idmotorista;
			$dados["idfrota"] = $idfrota;
			$dados["idtipomulta"] = $idmotivoinfracao;
			$dados["idcidade"] = $idcidade;
			$dados["sequencial"] = $sequencial;
			$dados["ocorrenciamulta"] = $ocorrenciamulta;
			// $dados["pontuacaomulta"] = $pontuacaomulta;
			// $dados["motivomulta"] = $motivomulta;
			$dados["numeroait"] = $numeroait;
			$dados["localmulta"] = $localmulta;
			$dados["datamulta"] = date("Y-m-d", $datamulta);
			$dados["horamulta"] = $horamulta;
			$dados["orgaomulta"] = $orgaomulta;
			$dados["enquadramento"] = $enquadramento;
			$dados["datavencimentomulta"] = date("Y-m-d", $datavencimentomulta);
			$dados["valormulta"] = $valormulta;
			$dados["pago"] = $pago;
			$dados["idmotivoinfracao"] = $idmotivoinfracao;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $logisticamultas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}