<?php

/**
 * Controle da classe pontosmensais do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PontosmensaisController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pontosmensal
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("pontosmensais", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Pontosmensais();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ponto mensal excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
                
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
                
                
		if ($op=="pontosmensais") $objs = new Pontosmensais();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'pontosmensais', 'name' => 'Pontos mensais'),
			array('url' => null,'name' => 'Visualizar Ponto mensal')
		);
		
		$id = (int)$this->_request->getParam("id");
		$pontosmensais = new Pontosmensais();
		$pontosmensal = $pontosmensais->getPontosmensalById($id, array());
		
		if (!$pontosmensal) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $pontosmensal;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Pontos mensais')
		);
                
		$ns = new Zend_Session_Namespace('default_pontosmensais');
		$pontosmensais = new Funcionariosgeraispontogrupos();
		$queries = array();	
                
                
		//$queries['origem'] = "Secretaria";
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
       
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
         
        
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
                
                if ($this->view->post_var["idlocal"]!="") $queries["idlocal"] = $this->view->post_var["idlocal"];
    		
//			if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
//if ($this->view->post_var["dia_i"]!="") $queries["dia_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["dia_i"]));
//if ($this->view->post_var["dia_f"]!="") $queries["dia_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["dia_f"]));
//if ($this->view->post_var["diasemana"]!="") $queries["diasemana"] = $this->view->post_var["diasemana"];
//if ($this->view->post_var["observacoes"]!="") $queries["observacoes"] = $this->view->post_var["observacoes"];
//if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
	
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
                
		$queries['total'] = true;
                $queries['sorting'] = "";
                
		$totalRegistros = $pontosmensais->getAtas($queries);
                
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
                 
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
                //print_r($pontosmensais->getAtas($queries, $paginaAtual, $maxpp));
                //die();
                $this->view->idPost = 
                        $this->getRequest()->getPost("id");
               
		$this->view->rows = $pontosmensais->getAtas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de pontosmensais
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'pontosmensais', 'name' => 'Pontos mensais'),
			array('url' => null,'name' => 'Editar Ponto mensal')
		);
                
                
		$id = (int)$this->_request->getParam("id");
		$pontosmensais = new Funcionariosgeraispontogrupos();
		$pontosmensal = $pontosmensais->getAtaById($id);
                
                $funcionario = new Funcionariosgerais();
                $informacoesFuncionario = $funcionario->getFuncionariogeralById($pontosmensal['idfuncionario']);
                
                //print_r($informacoesFuncionario);
                //die();
                
		if (!$pontosmensal) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		$this->preForm();
		//$this->view->post_var["nomex"] = "aaa";
		if ($this->_request->isPost()) {
			$erros = $this->getPost($pontosmensal);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ponto mensal editado com sucesso.";
                        
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
                
                
                $this->view->idPost = $this->_request->getParam("id");
                $this->view->id = $pontosmensal['idfuncionario'];
                $this->view->nomefuncionario = $pontosmensal['idfuncionario'];
                $this->view->funcionarioGeral = $funcionario->getFuncionariogeralById($pontosmensal['idfuncionario']);
                
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de pontosmensais 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'pontosmensais', 'name' => 'Pontos mensais'),
			array('url' => null,'name' => 'Adicionar Ponto mensal')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
                        
                        
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ponto mensal adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idpontosmensal
     */    
    private function preForm($idpontosmensal = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_pontosmensal = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
$dia = Mn_Util::stringToTime($this->getRequest()->getPost("dia"));
$diasemana = trim($this->getRequest()->getPost("diasemana"));
$horaentrada = trim($this->getRequest()->getPost("horaentrada"));
$horasaida = trim($this->getRequest()->getPost("horasaida"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idfuncionario) array_push($erros, "Informe a Funcionário.");
if (""==$dia) array_push($erros, "Informe a Dia.");
if (""==$diasemana) array_push($erros, "Informe a Dia da semana.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$pontosmensais = new Pontosmensais();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			$dados['origem'] = "Secretaria";
			$dados["idfuncionario"] = $idfuncionario;
$dados["dia"] = date("Y-m-d", $dia);
$dados["diasemana"] = $diasemana;
$dados["horaentrada"] = $horaentrada;
$dados["horasaida"] = $horasaida;
$dados["observacoes"] = $observacoes;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $pontosmensais->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
         /**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function envioajaxAction(){
                $this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
                
                if(isset($_POST["valor"])){
                    $valor = $_POST["valor"];
                }
                
                if(isset($_POST["dia_i"])){
                    $dia_i = $_POST["dia_i"];
                }
                
                if(isset($_POST["dia_f"])){
                    $dia_f = $_POST["dia_f"];
                }
                
                if(isset($_POST["id"])){
                    $id = $_POST["id"];
                }
                
                //$inicio = "30-04-2018";
                //$fim = "2018-05-30";
                
                $dateObjInicio =  date("d-m-Y", strtotime($dia_i));
                $brDateInicio = new DateTime($dateObjInicio);
                
                
                
                $dateObjFim =  date('d-m-Y', strtotime($dia_f));
                $brDateFim = new DateTime($dateObjFim);
                
                
                $interval = new DateInterval('P1D');

                $endOne = new DateTime($dateObjFim);
                $endOne->modify('+1 day');

                $period = new DatePeriod($brDateInicio, $interval, $endOne);
                
                $contador = 0;
                
                
                /*--------------------------------------------------------------
                 *      Setando Valores no Select de Ocorrências
                 --------------------------------------------------------------*/
                $ocorrenciasList = new Funcionariosgeraispontoocorrencias();
		$queries = array();
                $queries['status'] = "Ativo";
                $paginaAtual = 0;
                $maxpp = 20;
                	
                $row = $ocorrenciasList->getAtas($queries);
                
                // Bloco descinado à montagem das ocorrências no select para a escolha do usuário
                $strOcorrenciasSelectBlock = "";
                
                foreach($row as $index):
                    $strOcorrenciasSelectBlock .= "<option value=''>" . $index['ocorrencia'] . "</option>";
                endforeach;
                
                //$strOcorrenciasSelectBlock .= "</select>";
                
                
                
                
                /* -------------------------------------------------------------
                 *            Cabeçalho da table que irá retornar no ajax
                 *-------------------------------------------------------------*/
                
                echo "<tr>";
                echo "<th></th>";
                echo "<th>Data</th>";
                echo "<th>Entrada</th>";
                echo "<th>Saida</th>";
                echo "<th>Entrada Pausa</th>";
                echo "<th>Saida Pausa</th>";
                echo "<th>Ocorrencia</th>";
                echo "<th><div class='btn btn-info' style='cursor: pointer' onclick='confirmarTodos()'>Confirmar Todos</div></th>";
               
                echo "<tr>";
                
                /* -------------------------------------------------------------
                 *            Conteúdo da table que irá retornar no ajax
                  -------------------------------------------------------------*/
                
                
                /*
                 *  Linhas terão nomeclaturas diferenciadas para cada caso
                 *  LinhaS-  -> Definido para finais de semana encontrados
                 *  LinhaF-  -> Definido para Feriados Cadastrados no Banco de Dados
                 *  LinhaC-  -> Definido para Lançamentos já cadastrados no Banco de Dados
                 */
                
                $counter = 0; // Controle de Ids para requisição ajax
                
                
                //Funcionariosgeraispontoferiado::getVerificaFeriado($date->format("Y-m-d"))
                //echo "<br>foi";
                //die();
                
                echo "contexto de linhas <br><br>";
                
                foreach($period as $date):
                    
                
                if($date->format("w") == "0" || $date->format('w') == "6"){
                    
                    // verificar se é sábado ou domingo  
                    echo "<tr id='linhaS-$counter' class='warning' style='color: #e2e3f0'><td>" . Funcionariosgeraispontoferiado::getVerificaFeriado($date->format("Y-m-d")) .  " - " . Mn_Util::getDiaSemana($date->format('w')) . "</td><td>" . $date->format('d-m-Y') . "</td><td>--:--</td><td>--:--</td><td>--:--</td><td>--:--</td><td>----</td><td></td></tr>";
                     
                }else{
                    //$date->format("Y-m-d") == "2018-05-10"
                    //Funcionariosgeraispontoferiado::getVerificaFeriado($date->format("Y-m-d")  > 0
                    if(Funcionariosgeraispontoferiado::getVerificaFeriado($date->format("Y-m-d")) > 0){
                        $row = Funcionariosgeraispontoferiado::getInformacoesFeriadoData($date->format("Y-m-d"));
                        // verificar se é um feriado cadastrado na tabela [funcionariosgeraispontohorarios]   
                        echo "<tr id='linhaF-$counter' class='info'><td>" . Mn_Util::getDiaSemana($date->format('w')) . "</td><td>" . $date->format('d-m-Y') . "</td><td colspan='3'> <i class='icon-flag'></i> &nbsp; &nbsp; " . $row["feriado_pontoferiado"] . "</td><td></td><td></td><td></td></tr>"; 
                        
                    }else{
                        
                        if(Funcionariosgeraispontohorarios::getVerificaRegistro($date->format("Y-m-d"),$id) > 0){
                            Funcionariosgeraispontohorarios::getRetornaRegistro($id);
                            // verificar se o dia já consta cadastrado no banco de dados
                            echo "<tr id='linhaC-$counter' class='success'><td id='day-$counter'>" . Funcionariosgeraispontohorarios::getVerificaRegistro($date->format("Y-m-d"),(int)$id) . "-" .  Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td><td ><input id='entrada-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='08:00' disabled></td><td><input id='saida-$counter' type='text' class='span3' value='17:00' disabled></td><td><input id='entradapausa-$counter' type='text' class='span3' value='11:30'disabled></td><td><input id='saidapausa-$counter' type='text' class='span3' value='13:00' disabled></td><td> Confirmado </td><td><ul class='table-controls'><li><div class='btn tip' style='cursor: pointer' onclick='editarponto($counter)' data-original-title='Confirmar'> &nbsp; <i class='icon-edit'></i>&nbsp;</div></li></ul></td></tr>";
                            
                        }else{
                             $id = $this->_request->getParam("id");
                            echo "<tr id='linha-$counter'><td id='day-$counter'>&nbsp;" .  Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td><td ><input id='entrada-$counter' type='time' class='span3'  required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$' value='08:00'></td><td><input id='saida-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$' value='17:00'></td><td><input id='entradapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$' value='11:30'></td><td><input id='saidapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='13:00'></td><td><select id='obs-$counter' class='select'>" . $strOcorrenciasSelectBlock . "</select></td><td><ul class='table-controls'><li><div class='btn tip' style='cursor: pointer' onclick='cadastrarponto($counter)' data-original-title='Confirmar'> &nbsp; <i class='icon-ok'></i>&nbsp;</div></li></ul></td></tr>";
                        }//end if / else
                        
                    }//end if / else
                    
                }//end if / else
                    
                $counter++; // Insertando valores para o próximo ID
                
                endforeach;
                
                echo "<input id='total_linhas' type=hidden value='$counter'>"; // Inclusão de index para ser reutlizado na função para percorrer todas as linhas
                
		die();
		
	}//envioajaxAction
        
        
    /* -------------------------------------------------------------
    *            Inclusão de Lançamentos no Banco de Dados
    *            Autor : Thiago Torres Migliorati
    *            OBS: Esta função irá incluir os lançamentos no BD
    *            na tabela funcionariosgeraispontosgerais
     -------------------------------------------------------------*/
    public function incluipontoAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        // Criando model para inserir dados no Banco de dados
        
        $meu = new Funcionariosgeraispontohorarios();
        
                
        if(isset($_POST['entrada_json'])){
            $horaEntrada = $_POST['entrada_json'];
        }
        
        if(isset($_POST['saida_json'])){
            $horaSaida = $_POST['saida_json'];
        }
        
        if(isset($_POST['entradapausa_json'])){
            $horaEntrada_pausa = $_POST['entradapausa_json'];
        }
        
        if(isset($_POST['saidapausa_json'])){
            $horaSaida_pausa = $_POST['saidapausa_json'];
        }
        
        if(isset($_POST['observacao'])){
            $obs = $_POST['observacao'];
        }
        
        if(isset($_POST['data_json'])){
            $data = $_POST['data_json'];
        }
        
        if(isset($_POST['id_rhfuncionario'])){
            $id_rhfuncionario = $_POST['id_rhfuncionario'];
        }
        
        if(isset($_POST['id_funcionario'])){
            $id_funcionario = $_POST['id_funcionario'];
        }
       
        //Formatando a data para padrão internacional
         $dateObj =  date("d-m-Y", strtotime($data));
         $dateClass = new DateTime($dateObj);
                
        
        $dataEntrada =  $dateClass->format("Y-m-d") . " " . $horaEntrada . ":00";
        $dataSaida =  $dateClass->format("Y-m-d") . " " . $horaSaida . ":00";
        $dataEntrada_Pausa  =  $dateClass->format("Y-m-d") . " " . $horaEntrada_pausa . ":00";
        $dataSaida_Pausa = $dateClass->format("Y-m-d") . " " . $horaSaida_pausa . ":00";
        
        $date = date_create('2017-10-05 10:20:00');
        date_format($date, 'Y-m-d H:i:s');
        
        $ativao = "ativo";
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        
        
        try {
                $dados = array();
                //$dados['id'] = (int)"";
                $dados['titulo_pontohorarios'] = $obs;
               
                $dados['idfuncionario'] = $id_funcionario;
                $dados['idrhfuncionario'] = $id_rhfuncionario;
                
                $dados['data_pontohorarios'] = $dateClass->format("Y-m-d");
                $dados['entrada_pontohorarios'] = $dataEntrada;
                $dados['saida_pontohorarios'] =  $dataSaida;
                $dados['entradapausa_pontohorarios'] = $dataEntrada_Pausa;
                $dados['saidapausa_pontohorarios'] = $dataSaida_Pausa;
                $dados['status_pontohorarios'] = $ativao;
                $dados['logusuario_pontohorarios'] = $this->_usuario['id'];
                $dados['logdata_pontohorarios'] = date('Y-m-d G:i:s');
              
                /*
                $dados['origem'] = "Secretaria";
                $dados["idfuncionario"] = $idfuncionario;
                $dados["dia"] = date("Y-m-d", $dia);
                $dados["diasemana"] = $diasemana;
                $dados["horaentrada"] = $horaentrada;
                $dados["horasaida"] = $horasaida;
                $dados["observacoes"] = $observacoes;
                $dados["status"] = $status;


                $dados['excluido'] = 'nao';
                $dados['logusuario'] = $this->_usuario['id'];;
                $dados['logdata'] = date('Y-m-d G:i:s');
                */
                $row = $meu->save($dados);

                $db->commit();
        } catch (Exception $e) {
                echo $e->getMessage();

                $db->rollBack();
                die();
        }//end try/catch		
        
        echo "<td id='day-'>" .  Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-'>" . $date->format('d-m-Y') . "</td><td ><input id='entrada-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='08:00' disabled></td><td><input id='saida-$counter' type='text' class='span3' value='17:00' disabled></td><td><input id='entradapausa-$counter' type='text' class='span3' value='11:30'disabled></td><td><input id='saidapausa-$counter' type='text' class='span3' value='13:00' disabled></td><td><select id='obs-$counter' class='select' disabled><option value='presenca normal'>Presença Normal</option><option value='atraso'>Atraso</option><option>Falta</option></select></td><td><ul class='table-controls'><li><div class='btn tip' style='cursor: pointer' onclick='editarponto($counter)' data-original-title='Confirmar'> &nbsp; <i class='icon-edit'></i>&nbsp;</div></li></ul></td></tr>";
        
        die();
        
    }//end function incluiLancamentoPonto
	
	
    
}