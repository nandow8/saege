<?php

/**
 * Controle da classe documentacaovagasampliacao do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_DocumentacaovagasampliacaoController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Documentacaovagaampliacao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("documentacaovagasampliacao", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Documentacaovagasampliacao();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Vaga para Ampliação excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="documentacaovagasampliacao") $objs = new Documentacaovagasampliacao();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function aceitarampliacaoAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Documentacaovagasampliacao();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {


			$ids_vinculos = Escolasvinculosprofessoresmaterias::getIdsEscolasvinculosprofessorByIdHelper((int)Usuarios::getUsuario('idfuncionario'));
			if(!$ids_vinculos) die('Você deve estar atribuído em alguma sala/turma.');

			$row = $row->toArray();

			//var_dump($row);die();
			$aceites = new Documentacaovagasampliacaoaceites();

			$dados = array();
			$dados['idparent'] = $row['id'];
			$dados['idprofessor'] = (int)Usuarios::getUsuario('idfuncionario');
			$dados['status'] = 'Aguardando';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');			
			
			//var_dump($dados);die();
			$aceites->save($dados);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Ampliação registrada com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}	

	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaovagasampliacao', 'name' => 'Vagas para Ampliação'),
			array('url' => null,'name' => 'Visualizar Vaga para Ampliação')
		);
		
		$id = (int)$this->_request->getParam("id");
		$documentacaovagasampliacao = new Documentacaovagasampliacao();
		$documentacaovagaampliacao = $documentacaovagasampliacao->getDocumentacaovagaampliacaoById($id, array());
		
		if (!$documentacaovagaampliacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $documentacaovagaampliacao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Vagas para Ampliação')
		);
		
		$ns = new Zend_Session_Namespace('default_documentacaovagasampliacao');
		$documentacaovagasampliacao = new Documentacaovagasampliacao();
		$queries = array();	
		if(Usuarios::getUsuario('professor') == 'Sim')
		{
			$queries['pendente'] = true;
		}
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["idsala"]!="") $queries["idsala"] = $this->view->post_var["idsala"];
if ($this->view->post_var["observacao"]!="") $queries["observacao"] = $this->view->post_var["observacao"];
if ($this->view->post_var["posicao"]!="") $queries["posicao"] = $this->view->post_var["posicao"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $documentacaovagasampliacao->getDocumentacaovagasampliacao($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $documentacaovagasampliacao->getDocumentacaovagasampliacao($queries, $paginaAtual, $maxpp);
		$this->preForm();	
	}
	
	/**
	 * 
	 * Action de edição de documentacaovagasampliacao
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaovagasampliacao', 'name' => 'Vagas para Ampliação'),
			array('url' => null,'name' => 'Editar Vaga para Ampliação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$documentacaovagasampliacao = new Documentacaovagasampliacao();
		$documentacaovagaampliacao = $documentacaovagasampliacao->getDocumentacaovagaampliacaoById($id);
		
		if (!$documentacaovagaampliacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$solicitacoes = Documentacaovagasampliacaoaceites::getDocumentacaovagasampliacaoaceitesHelper(array('idparent' => $documentacaovagaampliacao['id']));
		//var_dump($solicitacoes);die();

		$this->view->post_var = $documentacaovagaampliacao;
		$this->view->ampliacoes = $solicitacoes;
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($documentacaovagaampliacao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Vaga para Ampliação editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de documentacaovagasampliacao 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaovagasampliacao', 'name' => 'Vagas para Ampliação'),
			array('url' => null,'name' => 'Adicionar Vaga para Ampliação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Vaga para Ampliação adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $iddocumentacaovagaampliacao
     */    
    private function preForm($iddocumentacaovagaampliacao = 0) {
		$salas = new Escolasvinculos();
    	$this->view->salas = $salas->getEscolasvinculos(array('idescola'=>Usuarios::getEscolaAtiva((int)Usuarios::getUsuario('id'), 'id'), 'status'=>'Ativo')); 
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_documentacaovagaampliacao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idsala = (int)trim($this->getRequest()->getPost("idsala"));
		$idtipoensino = (int)trim($this->getRequest()->getPost("idtipoensino"));
		$observacao = trim($this->getRequest()->getPost("observacao"));
		$status = trim($this->getRequest()->getPost("status1"));
		$posicao = trim($this->getRequest()->getPost("posicao"));
		
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Unidade Escolar.");
		if (0==$idsala) array_push($erros, "Informe a Sala.");
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$documentacaovagasampliacao = new Documentacaovagasampliacao();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
			$dados["idtipoensino"] = $idtipoensino;
			$dados["idsala"] = $idsala;
			$dados["observacao"] = $observacao;
			$dados["status"] = $status;
			$dados["posicao"] = ($posicao) ? $posicao : 'Aguardando';

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $documentacaovagasampliacao->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }

	public function setsalasAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = (int)$this->getRequest()->getPost('idescola');
    	
		$salas = new Escolasvinculos();
    	$this->view->rows = $salas->getEscolasvinculos(array('idescola'=>$idescola, 'status'=>'Ativo')); 
    	//var_dump($this->view->rows);die();
	}

	/**
	 * 
	 * Action para APROVAR PERMUTA via ajax e excluir a entidade
	 */
	public function aprovarampliacaoAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		$idfuncionario = (int)$this->_request->getPost("idfuncionario");
		if($id <= 0 && $idfuncionario <= 0) die('Solicitação não encontrada.');
		
		$ampliacoes = new Documentacaovagasampliacao();
		$rows = new Documentacaovagasampliacaoaceites();
		$row = $rows->fetchRow("status='Aguardando' AND idparent=$id AND idprofessor=$idfuncionario");
		
		if ($row) {

			$row = $row->toArray();

			$dados = array();
			$dados['id'] = $row['id'];
			$dados['status'] = 'Aceito';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');			
			
			$row = $rows->save($dados);

			$parent = $ampliacoes->fetchRow("excluido='nao' AND id=$id");
			if($parent)
			{
				$parent = $parent->toArray();
				$parent['posicao'] = 'Aprovado';
				$parent['logusuario'] = $this->_usuario['id'];
				$parent['logdata'] = date('Y-m-d G:i:s');	
				$ampliacoes->save($parent);
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Ampliação aprovada com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}

	/**
	 * 
	 * Action para ser REJEITAR PERMUTA via ajax e excluir a entidade
	 */
	public function rejeitarampliacaoAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		$idfuncionario = (int)$this->_request->getPost("idfuncionario");
		if($id <= 0 && $idfuncionario <= 0) die('Solicitação não encontrada.');
		
		$rows = new Documentacaovagasampliacaoaceites();
		$row = $rows->fetchRow("status='Aguardando' AND idparent=$id AND idprofessor=$idfuncionario");
		
		if ($row) {

			$row = $row->toArray();

			$dados = array();
			$dados['id'] = $row['id'];
			$dados['status'] = 'Reprovado';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($dados);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Ampliação rejeitada com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}
	
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}