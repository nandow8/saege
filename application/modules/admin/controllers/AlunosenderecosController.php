<?php

class Admin_AlunosenderecosController {
	
	/**
	 * Controller
	 * @var Zend_Controller_Action
	 */
	protected $_usuario = null;
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("atas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}
	
	/**
	 * Redireciona a página
	 * @param unknown_type $url
	 * @param array $options
	 */
    /*protected function _redirect($url, array $options = array())
    {
    	$helper = new Zend_Controller_Action_HelperBroker($this->_action);		
        $helper->redirector->gotoUrl($url, $options);
    }*/
    
	/**
	 * Valida e grava os dados do formulário
	 * @return Enderecos
	 */    
    public function getPost($idendereco = 0, $prefix = '') {
		$cep = trim($this->_action->getRequest()->getPost($prefix."cep"));
		$endereco = trim($this->_action->getRequest()->getPost($prefix."endereco"));
		$numero = trim($this->_action->getRequest()->getPost($prefix."numero"));
		$complemento = trim($this->_action->getRequest()->getPost($prefix."complemento"));
		$bairro = trim($this->_action->getRequest()->getPost($prefix."bairro"));
		$idaluno = trim((int)$this->_action->getRequest()->getPost($prefix."idaluno"));
		$cidade = trim($this->_action->getRequest()->getPost($prefix."cidade"));		
		
		$enderecos = new Escolasenderecos();
		$endereco = $enderecos->save($dados);		
		
		return $endereco;
    }

}
