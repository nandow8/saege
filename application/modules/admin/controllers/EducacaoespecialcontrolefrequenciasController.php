<?php

/**
 * Controle da classe educacaoespecialcontrolefrequencias do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EducacaoespecialcontrolefrequenciasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Educacaoespecialcontrolefrequencias
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("educacaoespecialcontrolefrequencias", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Educacaoespecialcontrolefrequencias();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Excluído com sucesso.";

			Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Educação especial Calendario Excluido"); 
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="educacaoespecialcontrolefrequencias") $objs = new Educacaoespecialcontrolefrequencias();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialcontrolefrequencias', 'name' => 'Educação Especial Controle de Frequencia'),
			array('url' => null,'name' => 'Visualizar Educação Especial Controle de Frequencia')
		);
		
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialcontrolefrequencias = new Educacaoespecialcontrolefrequencias();
		$educacaoespecialcontrolefrequencia = $educacaoespecialcontrolefrequencias->getEducacaoespecialcontrolefrequenciasById($id, array());
		
		if (!$educacaoespecialcontrolefrequencia) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialcontrolefrequencia;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Educação Especial Controle de Frequencia')
		);
		
		$ns = new Zend_Session_Namespace('default_educacaoespecialcontrolefrequencias');
		$educacaoespecialcontrolefrequencias = new Educacaoespecialcontrolefrequencias();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ((isset($this->view->post_var['idaluno'])) && ($this->view->post_var['idaluno']!='')) $queries['idaluno'] = $this->view->post_var['idaluno'];
			if ((isset($this->view->post_var['idescola'])) && ($this->view->post_var['idescola']!='')) $queries['idescola'] = $this->view->post_var['idescola'];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
			// if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
			// if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
			if ((isset($this->view->post_var['dataAno'])) && ($this->view->post_var['dataAno']!='')) $queries['dataAno'] = $this->view->post_var['dataAno'];
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $educacaoespecialcontrolefrequencias->getEducacaoespecialcontrolefrequencias($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $educacaoespecialcontrolefrequencias->getEducacaoespecialcontrolefrequencias($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de educacaoespecialcontrolefrequencias
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialcontrolefrequencias', 'name' => 'Educação Especial Controle de Frequencia'),
			array('url' => null,'name' => 'Editar Educação Especial Controle de Frequencia')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialcontrolefrequencias = new Educacaoespecialcontrolefrequencias();
		$educacaoespecialcontrolefrequencia = $educacaoespecialcontrolefrequencias->getEducacaoespecialcontrolefrequenciasById($id);
		
		if (!$educacaoespecialcontrolefrequencia) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialcontrolefrequencia;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($educacaoespecialcontrolefrequencia);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
		$this->view->editar = true;
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de educacaoespecialcontrolefrequencias 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialcontrolefrequencias', 'name' => 'Educação Especial Controle de Frequencia'),
			array('url' => null,'name' => 'Adicionar Educação Controle de Frequencia')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		$this->view->adicionar = true;
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $ideducacaoespecialcontrolefrequencia
     */    
    private function preForm($ideducacaoespecialcontrolefrequencia = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_educacaoespecialcontrolefrequencia = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idaluno = (int)$this->getRequest()->getPost("idaluno");   
		$idescola = (int)$this->getRequest()->getPost("idescola");   
		$dialetivo = $this->getRequest()->getPost("dialetivo");
		$dialetivo = implode(',', $dialetivo); 
		$dataAno = (int)$this->getRequest()->getPost("dataAno");   

		$status = trim($this->getRequest()->getPost("status1")); 
		
		$erros = array();
		 
		$educacaoespecialcontrolefrequencias = new Educacaoespecialcontrolefrequencias();
		 
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			$dados['idaluno'] = $idaluno;
			$dados['idescola'] = $idescola;
			$dados['dialetivo'] = $dialetivo; 
			$dados['dataAno'] = $dataAno; 
			 
			$dados["datacriacao"] = $datacriacao; 
			$dados["status"] = $status; 
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $educacaoespecialcontrolefrequencias->save($dados);
			Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Educação especial Calendario criado ou editado");
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Educação especial Calendario Error: $e" , "E");
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	}
	
	public function obterprofessoresdosalunosAction() {
		$this->_helper->layout->disableLayout();     
		$id = (int)$this->getRequest()->getPost('idaluno');
	 
		$rows['aee'] = Aeematriculas::getAeematriculaByIdHelper($id);

		$rows['nomealuno'] = Escolasalunos::getEscolaalunoByIdHelper($rows['aee']['idaluno'])['nomerazao']; 
		$rows['professor'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($rows['aee']['idprofessor']);
		$rows['professorAee'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($rows['aee']['idprofessor2']);
		$rows['escola'] = Escolas::getEscolaByIdHelper($rows['aee']['idescola'])['escola'];

		echo json_encode($rows);
		die();
	
	}
	
	public function aeelistamateriasAction() {
		$this->_helper->layout->disableLayout();     
		$id = (int)$this->getRequest()->getPost('id');
	 
		$rows['aee'] = Aeematriculas::getAeematriculaByIdHelper($id);
		$rows['aeelistamaterias'] = Aeelistamaterias::getAeelistamateriasHelper(array('idaee' => $id));
		  
		echo json_encode($rows);
		die();
	
	}

	public function somenteumcalendarioporalunoAction(){
		$this->_helper->layout->disableLayout();     
		$id = $this->getRequest()->getPost('idaluno');

		$rows = Educacaoespecialcontrolefrequencias::getEducacaoespecialcontrolefrequenciasByIdAlunoHelper($id);
		$rows['aee'] = Aeematriculas::getAeematriculaByIdHelper($id);
		$rows['nomealuno'] = Escolasalunos::getEscolaalunoByIdHelper($rows['aee']['idaluno'])['nomerazao'];
   
		echo json_encode($rows);
		die();
	}

	public function adicionarcalendarioAction() {
		$this->_helper->layout->disableLayout();
		 
		$dataAno = $this->getRequest()->getPost('dataAno'); 
		  
		$array = array(
			['ano' => $dataAno],		
		);
  
    	$this->view->rows = $array;
	}
    
}