<?php

/**
 * Controle da classe educacaoespecialsalasauxiliares do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EducacaoespecialsalasauxiliaresController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Educacaoespecialsalasauxiliar
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("educacaoespecialsalasauxiliares", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Educacaoespecialsalasauxiliares();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Salas excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="educacaoespecialsalasauxiliares") $objs = new Educacaoespecialsalasauxiliares();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialsalasauxiliares', 'name' => 'Salas Recursos Multifuncionais Auxilires'),
			array('url' => null,'name' => 'Visualizar Salas')
		);
		
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialsalasauxiliares = new Educacaoespecialsalasauxiliares();
		$educacaoespecialsalasauxiliar = $educacaoespecialsalasauxiliares->getEducacaoespecialsalasauxiliarById($id, array());
		
		if (!$educacaoespecialsalasauxiliar) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialsalasauxiliar;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Salas Recursos Multifuncionais Auxilires')
		);
		
		$ns = new Zend_Session_Namespace('default_educacaoespecialsalasauxiliares');
		$educacaoespecialsalasauxiliares = new Educacaoespecialsalasauxiliares();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["periodo"]!="") $queries["periodo"] = $this->view->post_var["periodo"];
if ($this->view->post_var["diassemana"]!="") $queries["diassemana"] = $this->view->post_var["diassemana"];
if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $educacaoespecialsalasauxiliares->getEducacaoespecialsalasauxiliares($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $educacaoespecialsalasauxiliares->getEducacaoespecialsalasauxiliares($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de educacaoespecialsalasauxiliares
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialsalasauxiliares', 'name' => 'Salas Recursos Multifuncionais Auxilires'),
			array('url' => null,'name' => 'Editar Salas')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialsalasauxiliares = new Educacaoespecialsalasauxiliares();
		$educacaoespecialsalasauxiliar = $educacaoespecialsalasauxiliares->getEducacaoespecialsalasauxiliarById($id);
		
		if (!$educacaoespecialsalasauxiliar) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialsalasauxiliar;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($educacaoespecialsalasauxiliar);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Salas editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de educacaoespecialsalasauxiliares 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialsalasauxiliares', 'name' => 'Salas Recursos Multifuncionais Auxilires'),
			array('url' => null,'name' => 'Adicionar Salas')
		);
		$funcionariosgerais = new Funcionariosgeraisescolas();

		$queries = array();
		$idperfil = (int)Usuarios::getUsuario('idperfil');
		$idescola = (int)Usuarios::getUsuario('idescola');

		$_usuarios = new Usuarios();
		

		$_funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById(Usuarios::getUsuario('idfuncionario'));

		if($idescola>0){
			$verificausuario = $_usuarios->getUsuarioById(Usuarios::getUsuario('id'));

			if((isset($verificausuario['idperfil'])) && ($verificausuario['idperfil']) && ((int)$verificausuario['idperfil'] > 0)){

				$idperfil = $verificausuario['idperfil'];	
			}
			
			
			if((!isset($_funcionariogeral['idperfil'])) || (!isset($_funcionariogeral['idsperfis'])) || ($_funcionariogeral['idperfil']=="") || ($_funcionariogeral['idsperfis']=="") || ($_funcionariogeral['idperfil']<=0) ){
				$idperfil = 0;
			}

			if((isset($_funcionariogeral['idsfuncionarios'])) && ($_funcionariogeral['idsfuncionarios']!="") ){
				$queries['idsfuncionariossel'] = $_funcionariogeral['idsfuncionarios'];
				if((!isset($queries['idsfuncionariossel'])) || ($queries['idsfuncionariossel']=="")){
					$queries['idsfuncionariossel'] = "-1";
				}
			}else{
				$queries['idsfuncionariossel'] = "-1";
			}
			
		}elseif($idperfil>0) {
			
			$queries['ids'] = $_funcionariogeral['idsfuncionarios'];
			$queries['idperfil'] = Usuarios::getUsuario('idperfil');
			if(($_funcionariogeral['idsfuncionarios']=="") || (!$_funcionariogeral['idsfuncionarios'])){
				$queries['ids'] = -1;
			}
			
		}elseif((isset($_funcionariogeral['idsfuncionarios'])) && ($_funcionariogeral['idsfuncionarios']!="") ){
			$queries['idsfuncionariossel'] = $_funcionariogeral['idsfuncionarios'];
			if((!isset($queries['idsfuncionariossel'])) || ($queries['idsfuncionariossel']=="")){
				$queries['idsfuncionariossel'] = "-1";
			}
		}else{
			$queries['idsfuncionariossel'] = "-1";
			//$queries['idescola'] = Usuarios::getUsuario('idescola');
		}
		
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$this->view->rows = $funcionariosgerais->getFuncionariosgeraisescolas($queries);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Salas adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $ideducacaoespecialsalasauxiliar
     */    
    private function preForm($ideducacaoespecialsalasauxiliar = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_educacaoespecialsalasauxiliar = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));

		$periodo = $this->getRequest()->getPost("periodo");
		$periodo = implode(',', $periodo);
		
		$diassemana = $this->getRequest()->getPost("diassemana");
		$diassemana = implode(',', $diassemana);
		
		$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
		$status = trim($this->getRequest()->getPost("status1"));
		//var_dump($diassemana);die();
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Polo AEE.");
		if (NULL==$periodo) array_push($erros, "Informe a Período.");
		if (NULL==$diassemana) array_push($erros, "Informe a Semana.");
		if (0==$idfuncionario) array_push($erros, "Informe a Auxiliar.");
		if (""==$status) array_push($erros, "Informe a Status.");

		$educacaoespecialsalasauxiliares = new Educacaoespecialsalasauxiliares();

		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
$dados["periodo"] = $periodo;
$dados["diassemana"] = $diassemana;
$dados["idfuncionario"] = $idfuncionario;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $educacaoespecialsalasauxiliares->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}