<?php

/**
 * Controle da classe nutricaotacoalimentos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_NutricaotacoalimentosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Nutricaotacoalimentos
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("nutricaotacoalimentos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Nutricaotacoalimentos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Alimento excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="nutricaotacoalimentos") $objs = new Nutricaotacoalimentos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaotacoalimentos', 'name' => 'Alimentos'),
			array('url' => null,'name' => 'Visualizar Alimento')
		);
		
		$id = (int)$this->_request->getParam("id");
		$nutricaotacoalimentos = new Nutricaotacoalimentos();
		$nutricaotacoalimento = $nutricaotacoalimentos->getNutricaotacoalimentoById($id, array());
		
		if (!$nutricaotacoalimento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaotacoalimento;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Alimentos')
		);
		
		$ns = new Zend_Session_Namespace('default_nutricaotacoalimentos');
		$nutricaotacoalimentos = new Nutricaotacoalimentos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["descricao"]!="") $queries["descricao"] = $this->view->post_var["descricao"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
                        if ($this->view->post_var["ds_fonte"]!="") $queries["ds_fonte"] = $this->view->post_var["ds_fonte"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $nutricaotacoalimentos->getNutricaotacoalimentos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
                $this->view->rows = $nutricaotacoalimentos->getNutricaotacoalimentos($queries, $paginaAtual, $maxpp);	
                
	}
	
	/**
	 * 
	 * Action de edição de nutricaotacoalimentos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaotacoalimentos', 'name' => 'Alimentos'),
			array('url' => null,'name' => 'Editar Alimento')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$nutricaotacoalimentos = new Nutricaotacoalimentos();
		$nutricaotacoalimento = $nutricaotacoalimentos->getNutricaotacoalimentoById($id);
		
		if (!$nutricaotacoalimento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaotacoalimento;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($nutricaotacoalimento);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Alimento editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * Action de adição de nutricaotacoalimentos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaotacoalimentos', 'name' => 'Alimentos'),
			array('url' => null,'name' => 'Adicionar Alimento')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Alimento adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idnutricaotacoalimento
     */    
    private function preForm($idnutricaotacoalimento = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_nutricaotacoalimento = false) {
        
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
                $id = (int)trim($this->getRequest()->getPost('id'));
                $idtipoalimento = (int)trim($this->getRequest()->getPost('idtipoalimento'));
                $numero_alimento = (int)trim($this->getRequest()->getPost('numero_alimento'));
                $descricao = trim($this->getRequest()->getPost('descricao'));
                $vl_g_poli = trim($this->getRequest()->getPost('vl_g_poli'));
                $vl_g_mon = trim($this->getRequest()->getPost('vl_g_mon'));
                $vl_g_sat = trim($this->getRequest()->getPost('vl_g_sat'));
                $vl_colest = trim($this->getRequest()->getPost('vl_colest'));
                $vl_b1 = trim($this->getRequest()->getPost('vl_b1'));
                $vl_b2 = trim($this->getRequest()->getPost('vl_b2'));
                $vl_b6 = trim($this->getRequest()->getPost('vl_b6'));
                $vl_b12 = trim($this->getRequest()->getPost('vl_b12'));
                $vl_nia = trim($this->getRequest()->getPost('vl_nia'));
                $vl_folic = trim($this->getRequest()->getPost('vl_folic'));
                $vl_pant = trim($this->getRequest()->getPost('vl_pant'));
                $vl_iodo = trim($this->getRequest()->getPost('vl_iodo'));
                $vl_mg = trim($this->getRequest()->getPost('vl_mg'));
                $vl_zinco = trim($this->getRequest()->getPost('vl_zinco'));
                $vl_mn = trim($this->getRequest()->getPost('vl_mn'));
                $vl_cobre = trim($this->getRequest()->getPost('vl_cobre'));
                $vl_selen = trim($this->getRequest()->getPost('vl_selen'));
                $vl_carb = trim($this->getRequest()->getPost('vl_carb'));
                $vl_g_tot = trim($this->getRequest()->getPost('vl_g_tot'));
                $vl_prot = trim($this->getRequest()->getPost('vl_prot'));
                $vl_fibra = trim($this->getRequest()->getPost('vl_fibra'));
                $vl_vit_a = trim($this->getRequest()->getPost('vl_vit_a'));
                $vl_vit_c = trim($this->getRequest()->getPost('vl_vit_c'));
                $vl_vit_d = trim($this->getRequest()->getPost('vl_vit_d'));
                $vl_vit_e = trim($this->getRequest()->getPost('vl_vit_e'));
                $vl_na = trim($this->getRequest()->getPost('vl_na'));
                $vl_ca = trim($this->getRequest()->getPost('vl_ca'));
                $vl_k = trim($this->getRequest()->getPost('vl_k'));
                $vl_p = trim($this->getRequest()->getPost('vl_p'));
                $vl_fe = trim($this->getRequest()->getPost('vl_fe'));
                $umidade_perc = trim($this->getRequest()->getPost('umidade_perc'));
                $energia_kcal = trim($this->getRequest()->getPost('energia_kcal'));
                $energia_kj = trim($this->getRequest()->getPost('energia_kj'));
                $proteina_g = trim($this->getRequest()->getPost('proteina_g'));
                $lipideos_g = trim($this->getRequest()->getPost('lipideos_g'));
                $colesterol_mg = trim($this->getRequest()->getPost('colesterol_mg'));
                $carboidrato_g = trim($this->getRequest()->getPost('carboidrato_g'));
                $fibra_alimentar_g = trim($this->getRequest()->getPost('fibra_alimentar_g'));
                $cinzas_g = trim($this->getRequest()->getPost('cinzas_g'));
                $calcio_mg = trim($this->getRequest()->getPost('calcio_mg'));
                $magnesio_mg = trim($this->getRequest()->getPost('magnesio_mg'));
                $manganes_mg = trim($this->getRequest()->getPost('manganes_mg'));
                $fosforo_mg = trim($this->getRequest()->getPost('fosforo_mg'));
                $ferro_mg = trim($this->getRequest()->getPost('ferro_mg'));
                $sodio_mg = trim($this->getRequest()->getPost('sodio_mg'));
                $potassio_mg = trim($this->getRequest()->getPost('potassio_mg'));
                $cobre_mg = trim($this->getRequest()->getPost('cobre_mg'));
                $zinco_mg = trim($this->getRequest()->getPost('zinco_mg'));
                $retinol_mcg = trim($this->getRequest()->getPost('retinol_mcg'));
                $re_mcg = trim($this->getRequest()->getPost('re_mcg'));
                $rae_mcg = trim($this->getRequest()->getPost('rae_mcg'));
                $tiamina_mg = trim($this->getRequest()->getPost('tiamina_mg'));
                $riboflavina_mg = trim($this->getRequest()->getPost('riboflavina_mg'));
                $piridoxina_mg = trim($this->getRequest()->getPost('piridoxina_mg'));
                $niacina_mg = trim($this->getRequest()->getPost('niacina_mg'));
                $vitamina_c_mg = trim($this->getRequest()->getPost('vitamina_c_mg'));
                $saturados_g = trim($this->getRequest()->getPost('saturados_g'));
                $mono_insaturados_g = trim($this->getRequest()->getPost('mono_insaturados_g'));
                $poli_insaturados_g = trim($this->getRequest()->getPost('poli_insaturados_g'));
                $c12_0_g = trim($this->getRequest()->getPost('c12_0_g'));
                $c14_0_g = trim($this->getRequest()->getPost('c14_0_g'));
                $c16_0_g = trim($this->getRequest()->getPost('c16_0_g'));
                $c18_0_g = trim($this->getRequest()->getPost('c18_0_g'));
                $c20_0_g = trim($this->getRequest()->getPost('c20_0_g'));
                $c22_0_g = trim($this->getRequest()->getPost('c22_0_g'));
                $c24_0_g = trim($this->getRequest()->getPost('c24_0_g'));
                $c14_1_g = trim($this->getRequest()->getPost('c14_1_g'));
                $c16_1_g = trim($this->getRequest()->getPost('c16_1_g'));
                $c18_1_g = trim($this->getRequest()->getPost('c18_1_g'));
                $c20_1_g = trim($this->getRequest()->getPost('c20_1_g'));
                $c18_2_n_6_g = trim($this->getRequest()->getPost('c18_2_n_6_g'));
                $c18_3_n_3_g = trim($this->getRequest()->getPost('c18_3_n_3_g'));
                $c20_4_g = trim($this->getRequest()->getPost('c20_4_g'));
                $c20_5_g = trim($this->getRequest()->getPost('c20_5_g'));
                $c22_5_g = trim($this->getRequest()->getPost('c22_5_g'));
                $c22_6_g = trim($this->getRequest()->getPost('c22_6_g'));
                $c18_1t_g = trim($this->getRequest()->getPost('c18_1t_g'));
                $c18_2t_g = trim($this->getRequest()->getPost('c18_2t_g'));
                $triptofano_g = trim($this->getRequest()->getPost('triptofano_g'));
                $treonina_g = trim($this->getRequest()->getPost('treonina_g'));
                $isoleucina_g = trim($this->getRequest()->getPost('isoleucina_g'));
                $leucina_g = trim($this->getRequest()->getPost('leucina_g'));
                $lisina_g = trim($this->getRequest()->getPost('lisina_g'));
                $metionina_g = trim($this->getRequest()->getPost('metionina_g'));
                $cistina_g = trim($this->getRequest()->getPost('cistina_g'));
                $fenilalanina_g = trim($this->getRequest()->getPost('fenilalanina_g'));
                $tirosina_g = trim($this->getRequest()->getPost('tirosina_g'));
                $valina_g = trim($this->getRequest()->getPost('valina_g'));
                $arginina_g = trim($this->getRequest()->getPost('arginina_g'));
                $histidina_g = trim($this->getRequest()->getPost('histidina_g'));
                $alanina_g = trim($this->getRequest()->getPost('alanina_g'));
                $acido_aspartico_g = trim($this->getRequest()->getPost('acido_aspartico_g'));
                $acido_glutamico_g = trim($this->getRequest()->getPost('acido_glutamico_g'));
                $glicina_g = trim($this->getRequest()->getPost('glicina_g'));
                $prolina_g = trim($this->getRequest()->getPost('prolina_g'));
                $serina_g = trim($this->getRequest()->getPost('serina_g'));
                $ds_fonte = trim($this->getRequest()->getPost('ds_fonte'));
                $status = trim($this->getRequest()->getPost('status1'));

                $opcao = $this->getRequest()->getPost('opcao');

               

		$erros = array();
		
		//if ("0"==$idtipoalimento) array_push($erros, "Informe o Tipo de Alimento.");
                if ("0"==$descricao) array_push($erros, "Informe a Descrição.");        
		if (""==$status) array_push($erros, "Informe o Status.");

		$nutricaotacoalimentos = new Nutricaotacoalimentos();
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();

                        $dados['id'] = (int)$id;
                        $dados['idtipoalimento'] = (int)$idtipoalimento;
                        $dados['idsmedidas'] = implode(',',$opcao);
                        $dados['numero_alimento'] = (int)$numero_alimento;
                        $dados['descricao'] = $descricao;
                        $dados['vl_g_poli'] = $vl_g_poli;
                        $dados['vl_g_mon'] = $vl_g_mon;
                        $dados['vl_g_sat'] = $vl_g_sat;
                        $dados['vl_colest'] = $vl_colest;
                        $dados['vl_b1'] = $vl_b1;
                        $dados['vl_b2'] = $vl_b2;
                        $dados['vl_b6'] = $vl_b6;
                        $dados['vl_b12'] = $vl_b12;
                        $dados['vl_nia'] = $vl_nia;
                        $dados['vl_folic'] = $vl_folic;
                        $dados['vl_pant'] = $vl_pant;
                        $dados['vl_iodo'] = $vl_iodo;
                        $dados['vl_mg'] = $vl_mg;
                        $dados['vl_zinco'] = $vl_zinco;
                        $dados['vl_mn'] = $vl_mn;
                        $dados['vl_cobre'] = $vl_cobre;
                        $dados['vl_selen'] = $vl_selen;
                        $dados['vl_carb'] = $vl_carb;
                        $dados['vl_g_tot'] = $vl_g_tot;
                        $dados['vl_prot'] = $vl_prot;
                        $dados['vl_fibra'] = $vl_fibra;
                        $dados['vl_vit_a'] = $vl_vit_a;
                        $dados['vl_vit_c'] = $vl_vit_c;
                        $dados['vl_vit_d'] = $vl_vit_d;
                        $dados['vl_vit_e'] = $vl_vit_e;
                        $dados['vl_na'] = $vl_na;
                        $dados['vl_ca'] = $vl_ca;
                        $dados['vl_k'] = $vl_k;
                        $dados['vl_p'] = $vl_p;
                        $dados['vl_fe'] = $vl_fe;
                        $dados['umidade_perc'] = $umidade_perc;
                        $dados['energia_kcal'] = $energia_kcal;
                        $dados['energia_kj'] = $energia_kj;
                        $dados['proteina_g'] = $proteina_g;
                        $dados['lipideos_g'] = $lipideos_g;
                        $dados['colesterol_mg'] = $colesterol_mg;
                        $dados['carboidrato_g'] = $carboidrato_g;
                        $dados['fibra_alimentar_g'] = $fibra_alimentar_g;
                        $dados['cinzas_g'] = $cinzas_g;
                        $dados['calcio_mg'] = $calcio_mg;
                        $dados['magnesio_mg'] = $magnesio_mg;
                        $dados['manganes_mg'] = $manganes_mg;
                        $dados['fosforo_mg'] = $fosforo_mg;
                        $dados['ferro_mg'] = $ferro_mg;
                        $dados['sodio_mg'] = $sodio_mg;
                        $dados['potassio_mg'] = $potassio_mg;
                        $dados['cobre_mg'] = $cobre_mg;
                        $dados['zinco_mg'] = $zinco_mg;
                        $dados['retinol_mcg'] = $retinol_mcg;
                        $dados['re_mcg'] = $re_mcg;
                        $dados['rae_mcg'] = $rae_mcg;
                        $dados['tiamina_mg'] = $tiamina_mg;
                        $dados['riboflavina_mg'] = $riboflavina_mg;
                        $dados['piridoxina_mg'] = $piridoxina_mg;
                        $dados['niacina_mg'] = $niacina_mg;
                        $dados['vitamina_c_mg'] = $vitamina_c_mg;
                        $dados['saturados_g'] = $saturados_g;
                        $dados['mono_insaturados_g'] = $mono_insaturados_g;
                        $dados['poli_insaturados_g'] = $poli_insaturados_g;
                        $dados['c12_0_g'] = $c12_0_g;
                        $dados['c14_0_g'] = $c14_0_g;
                        $dados['c16_0_g'] = $c16_0_g;
                        $dados['c18_0_g'] = $c18_0_g;
                        $dados['c20_0_g'] = $c20_0_g;
                        $dados['c22_0_g'] = $c22_0_g;
                        $dados['c24_0_g'] = $c24_0_g;
                        $dados['c14_1_g'] = $c14_1_g;
                        $dados['c16_1_g'] = $c16_1_g;
                        $dados['c18_1_g'] = $c18_1_g;
                        $dados['c20_1_g'] = $c20_1_g;
                        $dados['c18_2_n_6_g'] = $c18_2_n_6_g;
                        $dados['c18_3_n_3_g'] = $c18_3_n_3_g;
                        $dados['c20_4_g'] = $c20_4_g;
                        $dados['c20_5_g'] = $c20_5_g;
                        $dados['c22_5_g'] = $c22_5_g;
                        $dados['c22_6_g'] = $c22_6_g;
                        $dados['c18_1t_g'] = $c18_1t_g;
                        $dados['c18_2t_g'] = $c18_2t_g;
                        $dados['triptofano_g'] = $triptofano_g;
                        $dados['treonina_g'] = $treonina_g;
                        $dados['isoleucina_g'] = $isoleucina_g;
                        $dados['leucina_g'] = $leucina_g;
                        $dados['lisina_g'] = $lisina_g;
                        $dados['metionina_g'] = $metionina_g;
                        $dados['cistina_g'] = $cistina_g;
                        $dados['fenilalanina_g'] = $fenilalanina_g;
                        $dados['tirosina_g'] = $tirosina_g;
                        $dados['valina_g'] = $valina_g;
                        $dados['arginina_g'] = $arginina_g;
                        $dados['histidina_g'] = $histidina_g;
                        $dados['alanina_g'] = $alanina_g;
                        $dados['acido_aspartico_g'] = $acido_aspartico_g;
                        $dados['acido_glutamico_g'] = $acido_glutamico_g;
                        $dados['glicina_g'] = $glicina_g;
                        $dados['prolina_g'] = $prolina_g;
                        $dados['serina_g'] = $serina_g;
                        $dados['ds_fonte'] = $ds_fonte;
                        $dados['status'] = $status;

                        $dados['excluido'] = 'nao';
                        $dados['logdata'] = date('Y-m-d G:i:s');
                        $dados['logusuario'] = $this->_usuario['id'];
                        
			$row = $nutricaotacoalimentos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}
