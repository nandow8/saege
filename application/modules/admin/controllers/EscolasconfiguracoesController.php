<?php

/**
 * Controle da classe escolasconfiguracoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EscolasconfiguracoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Escolasconfiguracao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("escolasconfiguracoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Escolasconfiguracoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Configuração de nota excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="escolasconfiguracoes") $objs = new Escolasconfiguracoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasconfiguracoes', 'name' => 'Configurações de notas'),
			array('url' => null,'name' => 'Visualizar Configuração de nota')
		);
		
		$id = (int)$this->_request->getParam("id");
		$escolasconfiguracoes = new Escolasconfiguracoes();
		$escolasconfiguracao = $escolasconfiguracoes->getEscolasconfiguracaoById($id, array());
		
		if (!$escolasconfiguracao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolasconfiguracao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Configurações de notas')
		);
		
		$ns = new Zend_Session_Namespace('default_escolasconfiguracoes');
		$escolasconfiguracoes = new Escolasconfiguracoes();
		$queries = array();	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["periodos"]!="") $queries["periodos"] = $this->view->post_var["periodos"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $escolasconfiguracoes->getEscolasconfiguracoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $escolasconfiguracoes->getEscolasconfiguracoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de escolasconfiguracoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasconfiguracoes', 'name' => 'Configurações de notas'),
			array('url' => null,'name' => 'Editar Configuração de nota')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$escolasconfiguracoes = new Escolasconfiguracoes();
		$escolasconfiguracao = $escolasconfiguracoes->getEscolasconfiguracaoById($id);
		
		if (!$escolasconfiguracao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolasconfiguracao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($escolasconfiguracao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Configuração de nota editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de escolasconfiguracoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasconfiguracoes', 'name' => 'Configurações de notas'),
			array('url' => null,'name' => 'Adicionar Configuração de nota')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Configuração de nota adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idescolasconfiguracao
     */    
    private function preForm($idescolasconfiguracao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_escolasconfiguracao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$quantidadenotas = trim($this->getRequest()->getPost("quantidadenotas"));
$quantidadetrabalhos = trim($this->getRequest()->getPost("quantidadetrabalhos"));
$quantidadetarefas = trim($this->getRequest()->getPost("quantidadetarefas"));

$percentualtrabalhos = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("percentualtrabalhos"))));
$percentualprovas = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("percentualprovas"))));
$percentualparticipacao = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("percentualparticipacao"))));
$percentualtarefas = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("percentualtarefas"))));

$periodos = trim($this->getRequest()->getPost("periodos"));
$datainicio1 = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio1"));
$datafim1 = Mn_Util::stringToTime($this->getRequest()->getPost("datafim1"));
$datainicio2 = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio2"));
$datafim2 = Mn_Util::stringToTime($this->getRequest()->getPost("datafim2"));
$datainicio3 = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio3"));
$datafim3 = Mn_Util::stringToTime($this->getRequest()->getPost("datafim3"));
$datainicio4 = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio4"));
$datafim4 = Mn_Util::stringToTime($this->getRequest()->getPost("datafim4"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idsecretaria) array_push($erros, "Informe a Secretaria.");
if (0==$idescola) array_push($erros, "Informe a Escola.");
if (""==$periodos) array_push($erros, "Informe a Periodo de notas.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$escolasconfiguracoes = new Escolasconfiguracoes();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = $idsecretaria;
$dados["idescola"] = $idescola;
$dados["quantidadenotas"] = $quantidadenotas;
$dados["quantidadetrabalhos"] = $quantidadetrabalhos;
$dados["quantidadetarefas"] = $quantidadetarefas;
$dados["percentualtrabalhos"] = $percentualtrabalhos;
$dados["percentualprovas"] = $percentualprovas;
$dados["percentualparticipacao"] = $percentualparticipacao;
$dados["percentualtarefas"] = $percentualtarefas;
$dados["periodos"] = $periodos;
$dados["datainicio1"] = date("Y-m-d", $datainicio1);
$dados["datafim1"] = date("Y-m-d", $datafim1);
$dados["datainicio2"] = date("Y-m-d", $datainicio2);
$dados["datafim2"] = date("Y-m-d", $datafim2);
$dados["datainicio3"] = date("Y-m-d", $datainicio3);
$dados["datafim3"] = date("Y-m-d", $datafim3);
$dados["datainicio4"] = date("Y-m-d", $datainicio4);
$dados["datafim4"] = date("Y-m-d", $datafim4);
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $escolasconfiguracoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}