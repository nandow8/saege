<?php

/**
 * Controle da classe treinamento do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_TreinamentoController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Coordenacaofrequenciaatividadescomplementar
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("treinamento", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$treinamentos = new Treinamento();
		$queries = array();	
		$ultimo = $treinamentos->getUltimoTreinamento($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Treinamento();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Frequencia excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="treinamento") $objs = new Treinamento();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'treinamento', 'name' => 'Treinamento'),
			array('url' => null,'name' => 'Visualizar Treinamento')
		);
		
		$id = (int)$this->_request->getParam("id");
		$treinamento = new Treinamento();
		$treinamento = $treinamento->getTreinamentoById($id, array());
		
		if (!$treinamento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $treinamento;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Treinamento')
		);
		
		$ns = new Zend_Session_Namespace('default_treinamento');
		$treinamento = new Treinamento();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ($this->view->post_var["ano"]!="") $queries["ano"] = $this->view->post_var["ano"];
    		if ($this->view->post_var["cursoid"]!="") $queries["cursoid"] = $this->view->post_var["cursoid"];
			if ($this->view->post_var["data"]!="") $queries["data"] = $this->view->post_var["data"];
			if ($this->view->post_var["informacoes"]!="") $queries["informacoes"] = $this->view->post_var["informacoes"];

			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $treinamento->getTreinamentos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $treinamento->getTreinamentos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de treinamento
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'treinamento', 'name' => 'Treinamento'),
			array('url' => null,'name' => 'Editar Treinamento')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$treinamento = new Treinamento();
		$treinamento = $treinamento->getTreinamentoById($id);
		
		if (!$treinamento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $treinamento;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($treinamento);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Treinamento editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de treinamento 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'treinamento', 'name' => 'Treinamento'),
			array('url' => null,'name' => 'Adicionar Treinamento')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Treinamento adicionada com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	/**
	 * 
	 * Action de edição de cursos
	 */	
	public function getcursoAction() {
		$this->_helper->layout->disableLayout();
		$idcurso = (int)$this->_request->getPost("idcurso");

		$cursos = new Cursos();
		$curso = $cursos->getCursoById($idcurso);
		
		if (!$curso) {
			die();
		}

		$this->view->post_var = $curso;
    }  

    /**
     * Atribui valores ao view
     * @param int $idtreinamento
     */    
    private function preForm($idtreinamento = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_treinamento = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		
		$data = Mn_Util::stringToTime(trim($this->getRequest()->getPost("data")));
		$usuarioid = trim($this->getRequest()->getPost("usuarioid"));
		$cursoid = trim($this->getRequest()->getPost("cursoid"));
		$datainicio = Mn_Util::stringToTime(trim($this->getRequest()->getPost("datainicio")));
		$datatermino = Mn_Util::stringToTime(trim($this->getRequest()->getPost("datatermino")));
		$horarioinicio = trim($this->getRequest()->getPost("horarioinicio"));
		$horariofinal = trim($this->getRequest()->getPost("horariofinal"));
		$qtdvagas = trim($this->getRequest()->getPost("qtdvagas"));
		$local = trim($this->getRequest()->getPost("local"));
		$informacoes = trim($this->getRequest()->getPost("informacoes"));
		$datalimite = Mn_Util::stringToTime(trim($this->getRequest()->getPost("datalimite")));
		$status = trim($this->getRequest()->getPost("status1"));
		$diassemana = $this->getRequest()->getPost("diassemana");
		$diassemana = implode(',', $diassemana);		
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		
		$erros = array();
		
		if (""==$data) array_push($erros, "Informe a data do treinamento.");
		if (""==$cursoid) array_push($erros, "Informe a curso.");
		if (""==$datainicio) array_push($erros, "Informe a data de início do treinamento.");
		if (""==$datatermino) array_push($erros, "Informe a data de término do treinamento.");
		if (""==$horarioinicio) array_push($erros, "Informe o horário de início do treinamento.");
		if (""==$horariofinal) array_push($erros, "Informe o horário final do treinamento.");
		if (""==$diassemana) array_push($erros, "Informe os dias da semana do treinamento.");
		if (""==$qtdvagas) array_push($erros, "Informe a quantidade de vagas do treinamento.");
		if (""==$local) array_push($erros, "Informe o local do treinamento.");
		if (""==$informacoes) array_push($erros, "Informe as informações do treinamento.");
		if (""==$datalimite) array_push($erros, "Informe a data limite do treinamento.");
		if (""==$status) array_push($erros, "Informe o Status.");

		
		$treinamento = new Treinamento();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["usuarioid"] = $usuarioid;
			$dados["sequencial"] = $sequencial;
			$dados["cursoid"] = $cursoid;
			$dados["data"] = date("Y-m-d", $data);
			$dados["datainicio"] = date("Y-m-d", $datainicio);
			$dados["datatermino"] = date("Y-m-d", $datatermino);
			$dados["horarioinicio"] = $horarioinicio;
			$dados["horariofinal"] = $horariofinal;
			$dados["diassemana"] = $diassemana;
			$dados["qtdvagas"] = $qtdvagas;
			$dados["informacoes"] = $informacoes;
			$dados["local"] = $local;
			$dados["datalimite"] = date("Y-m-d", $datalimite);
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
				
			$row = $treinamento->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}