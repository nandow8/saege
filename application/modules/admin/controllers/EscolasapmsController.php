<?php

/**
 * Controle da classe escolasapms do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EscolasapmsController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Escolaapm
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("escolasapms", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Escolasapms();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Convênio excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="escolasapms") $objs = new Escolasapms();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasapms', 'name' => 'Apms'),
			array('url' => null,'name' => 'Visualizar Apm')
		);
		
		$id = (int)$this->_request->getParam("id");
		$escolasapms = new Escolasapms();
		$escolaapm = $escolasapms->getEscolaapmById($id, array());
		
		if (!$escolaapm) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolaapm;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Convênios')
		);
		
		$ns = new Zend_Session_Namespace('default_escolasapms');
		$escolasapms = new Escolasapms();
		$queries = array();	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['idescola'] = Usuarios::getUsuario('idescola');

		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["cpfcnpj"]!="") $queries["cpfcnpj"] = $this->view->post_var["cpfcnpj"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $escolasapms->getEscolasapms($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $escolasapms->getEscolasapms($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de escolasapms
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasapms', 'name' => 'Convênios'),
			array('url' => null,'name' => 'Editar Convênio')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$escolasapms = new Escolasapms();
		$escolaapm = $escolasapms->getEscolaapmById($id);
		
		if (!$escolaapm) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolaapm;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($escolaapm);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Convênio editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de escolasapms 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasapms', 'name' => 'Convênios'),
			array('url' => null,'name' => 'Adicionar Convênio')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Convênio adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idescolaapm
     */    
    private function preForm($idescolaapm = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_escolaapm = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idlocal = (int)trim($this->getRequest()->getPost("idlocal"));
$cpfcnpj = trim($this->getRequest()->getPost("cpfcnpj"));
		$banco = $this->getRequest()->getPost("banco");
		$agencia = $this->getRequest()->getPost("agencia");
		$conta = $this->getRequest()->getPost("conta");
		$digito = $this->getRequest()->getPost("digito");	
		$tipo = $this->getRequest()->getPost("tipo");

$status = trim($this->getRequest()->getPost("status1"));

		$saldoexercicioanterior = MN_Util::trataNum(trim($this->getRequest()->getPost("saldoexercicioanterior")));
		$rendimentoaplicacaofinanceira = MN_Util::trataNum(trim($this->getRequest()->getPost("rendimentoaplicacaofinanceira")));
		
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
if (""==$cpfcnpj) array_push($erros, "Informe a CNPJ.");
		if (sizeof($banco) <= 0) array_push($erros, 'Preencha o campo BANCO.');
		if (sizeof($agencia) <= 0) array_push($erros, 'Preencha o campo AGÊNCIA.');
		if (sizeof($conta) <= 0) array_push($erros, 'Preencha o campo CONTA.');
		if (sizeof($digito) <= 0) array_push($erros, 'Preencha o campo DÍGITO.');
if (""==$status) array_push($erros, "Informe a Status.");

		
		$escolasapms = new Escolasapms();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
			$dados["idlocal"] = $idlocal;
			$dados["cpfcnpj"] = $cpfcnpj;
			$dados["status"] = $status;
			$dados["saldoexercicioanterior"] = $saldoexercicioanterior;
			$dados["rendimentoaplicacaofinanceira"] = $rendimentoaplicacaofinanceira;


			$dados['tipo'] = $tipo;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

            $contas = array();
            $contas['banco'] = array_filter($banco);
            $contas['agencia'] = array_filter($agencia);
            $contas['conta'] = array_filter($conta);
            $contas['digito'] = array_filter($digito);
            $contas['tipo'] = $tipo;

            $contas['logusuario'] = Usuarios::getUsuario('id');
            $contas['logdata'] = date('Y-m-d G:i:s');
            
            $dados['contas'] = $contas;

			$row = $escolasapms->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}