<?php

/**
 * Controle da classe coordenacaoacademico do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_CoordenacaoacademicoController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Coordenacaoacademico
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("coordenacaoacademico", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Coordenação Acadêmico')
		);
	}

    public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		// $queries['idescola'] = $idescola;

		$rows = new Escolasseries();
		$this->view->rows = $rows->getEscolasseries($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setturmasAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		// $queries['idserie'] = $idserie;
		// $queries['idescola'] = $idescola;
		
		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculos($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0)) $this->view->rows = null;
		
	}

	public function setperiodoAction() {
		$this->_helper->layout->disableLayout();

		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		// $queries['idescola'] = $idescola;

		$rows = new Escolasconfiguracoes();
		$this->view->rows = $rows->getEscolasconfiguracoes($queries);

		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");
		$idturma = (int)$this->_request->getPost("idvinculo");

		$queries = array();
		// $queries['idserie'] = $idserie;
		// $queries['idescola'] = $idescola;
		// $queries['idvinculo'] = $idturma;
		
		$rows = new Escolassalasatribuicoes();
		$this->view->rows = $rows->getSalasatribuicoes($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0) || ((int)$idturma <= 0)) $this->view->rows = null;
		
	}

	public function compararAction() {
		$this->_helper->layout->disableLayout();
		
		$base = $this->_request->getPost("base");
		$tipo = $this->_request->getPost("tipo");

		$idserie1 = (int)$this->_request->getPost("idserie_1");
		$idescola1 = (int)$this->_request->getPost("idescola_1");
		$idvinculo1 = (int)$this->_request->getPost("idturma_1");
		$idaluno1 = (int)$this->_request->getPost("idaluno_1");

		$idserie2 = (int)$this->_request->getPost("idserie_2");
		$idescola2 = (int)$this->_request->getPost("idescola_2");
		$idvinculo2 = (int)$this->_request->getPost("idturma_2");
		$idaluno2 = (int)$this->_request->getPost("idaluno_2");

		$queries1 = array();
		// $queries1['idserie'] = $idserie1;
		// $queries1['idescola'] = $idescola1;
		// $queries1['idvinculo'] = $idvinculo1;
		// $queries1['idaluno'] = $idaluno1;
		
		$queries2 = array();
		// $queries2['idserie'] = $idserie2;
		// $queries2['idescola'] = $idescola2;
		// $queries2['idvinculo'] = $idvinculo2;
		// $queries2['idaluno'] = $idaluno2;
		
		$rows = new Escolassalasatribuicoes();
		$_rows = new Escolassalasatribuicoes();
		$this->view->rows = array();

		$this->view->rows[] = $base;
		$this->view->rows[] = $tipo;

		if( $idaluno1 != 0 )
			$this->view->rows[] = $rows->getSalasatribuicoes($queries1);
		else {
			array_pop($queries1);
			$this->view->rows[] = $_rows->getSalasatribuicoes($queries1);
		}

		if( $idaluno2 != 0 )
			$this->view->rows[] = $rows->getSalasatribuicoes($queries2);
		else {
			array_pop($queries2);
			$this->view->rows[] = $_rows->getSalasatribuicoes($queries2);
		}

		if(((int)$idescola2 <= 0 || (int)$idescola1 <= 0) || ((int)$idserie1 <= 0 || (int)$idserie2 <= 0) || ((int)$idvinculo1 <= 0 || (int)$idvinculo2 <= 0)) $this->view->rows = null;		
	}
    
}