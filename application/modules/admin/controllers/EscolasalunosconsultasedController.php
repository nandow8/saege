<?php

class Admin_EscolasalunosconsultasedController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null;
    protected $_soapstatus = '';
    protected $_soaptipo = '';

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("escolasalunos", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }


    public function changeorderxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $idfrom = (int) $this->getRequest()->getPost("from");
        $idto = (int) $this->getRequest()->getPost("to");
        $op = $this->getRequest()->getPost("op");

        if ($op == "change")
            $objs = new Escolasalunosconsultased();
        $from = $objs->fetchRow("excluido='nao' AND id=" . $idfrom);
        $to = $objs->fetchRow("excluido='nao' AND id=" . $idto);

        if (($from) && ($to)) {
            $from = $from->toArray();
            $to = $to->toArray();
            $ordemFrom = $from["ordem"];
            $orderTo = $to["ordem"];

            $from['ordem'] = $orderTo;
            $to['ordem'] = $ordemFrom;

            $objs->save($from);
            $objs->save($to);
        }

        $this->view->message = "OK";
        $this->render("xml");
    }

    public function ordemAction() {
        $ordem = (int) $this->getRequest()->getParam('ordem', 0);
        $d = $this->getRequest()->getParam('d', 0);

        $rows = new Escolasalunosconsultased();
        $rows->swapOrdem($ordem, $d, false);

        $this->_redirect("/admin/" . $this->_request->getControllerName() . "/index");
        die();
    }

    /**
     * Listagem
     */
    public function indexAction() {
        
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Aluno')
        );

        $ns = new Zend_Session_Namespace('admin_alunos');
        $alunos = new Escolasalunosconsultased();
        $queries = array();

        $this->preForm();
        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {

            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = $v;


            if ((isset($this->view->post_var['sorting'])) && ($this->view->post_var['sorting'] != ''))
                $queries['sorting'] = $this->view->post_var['sorting'];
            
        }

        $operacao = '';
        $dados    = [];
        
        if (isset($this->view->post_var['tabnav']) && ($this->view->post_var['tabnav'] == '1')){
            $operacao = 'ConsultaRA';

            $dados = 
                [
                    'ra'        => $this->view->post_var['inRA'],
                    'digra'  => $this->view->post_var['inDigitoRA'],
                    'ufra'        => $this->view->post_var['inUF']
                ];
        }else if (isset($this->view->post_var['tabnav']) && ($this->view->post_var['tabnav'] == '2')){
            $operacao = 'ConsultaFonetica';

            $dados = 
                [
                    'NomeAluno'     => $this->view->post_var['inNomeAluno'],     
                    'NomeSocial'    => $this->view->post_var['inNomeSocial'],    
                    'NomeMae'       => $this->view->post_var['inNomeMae'],       
                    'Nascimento'    => $this->view->post_var['inNascimento'],  
                ];
        }else if (isset($this->view->post_var['tabnav']) && ($this->view->post_var['tabnav'] == '3')){
            $operacao = 'ConsultaNomeCompleto';

            $dados = 
                [
                    'NomeComplAluno'  => $this->view->post_var['inNomeComplAluno'],     
                    'NomeComplMae'    => $this->view->post_var['inNomeComplMae'],    
                    'NomeComplPai'    => $this->view->post_var['inNomeComplPai']
                ];
        }else if (isset($this->view->post_var['tabnav']) && ($this->view->post_var['tabnav'] == '4')){
            $operacao = 'ConsultaDocumentos';

            $dados = 
                [
                    'NumRG'               => $this->view->post_var['inNumRG'],                
                    'DigitoRG'            => $this->view->post_var['inDigitoRG'],          
                    'UFRG'                => $this->view->post_var['inUFRG'],             
                    'CPF'                 => $this->view->post_var['inCPF'],              
                    'NumINEP'             => $this->view->post_var['inNumINEP'],          
                    'NumNIS'              => $this->view->post_var['inNumNIS'],           
                    'NumCertidaoNova'     => $this->view->post_var['inNumCertidaoNova'],  
                    'NumCertidaoNasc'     => $this->view->post_var['inNumCertidaoNasc'],  
                    'LivroCertidaoNasc'   => $this->view->post_var['inLivroCertidaoNasc'],
                    'FolhaCertidaoNasc'   => $this->view->post_var['inFolhaCertidaoNasc']
                ];
        }
        
        if(isset($this->view->post_var)){
            $this->view->rows = (SED::ConsultarFichaAluno($dados, $operacao));
        }
        
    }

    /**
     * Atribui valores ao view
     * @param int $idaluno
     */
    private function preForm($idaluno = 0) {

        $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $this->view->escolas = Escolas::getEscolasHelper(array('status' => 'Ativo', 'idsecretaria' => $this->view->idsecretaria));

        $instituicoes = new Escolasinstituicoesnecessidades();
        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['status'] = 'Ativo';
        $this->view->instituicoesapoio = $instituicoes->getInstituicoes($queries);

        $programas = new Escolasprogramassociais();
        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['status'] = 'Ativo';
        $this->view->programassociais = $programas->getProgramas($queries);
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_registro = false) {

        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");

        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            $dados = array();
            $dados['id'] = $id;

            $dados['status'] = $status;
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = Usuarios::getUsuario('id');
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $alunos->save($dados);
            
            $db->commit();

            // Realiza a operação no SED
            if ($id === 0) {
                SED::SoapIncluirDadosPessoaisFichaAluno($dados, 'Inc');
            } else {
                SED::SoapIncluirDadosPessoaisFichaAluno($dados, 'Alt');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die();
        }

        return "";
    }

}
