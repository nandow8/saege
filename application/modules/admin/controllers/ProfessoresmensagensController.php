<?php

/**
 * Controle da classe professoresmensagens do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ProfessoresmensagensController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Mensagensaluno
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("professoresmensagens", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Mensagensalunos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mensagem excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="professoresmensagens") $objs = new Mensagensalunos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'professoresmensagens', 'name' => 'Mensagens para aluno'),
			array('url' => null,'name' => 'Visualizar Mensagem')
		);
		
		$id = (int)$this->_request->getParam("id");
		$professoresmensagens = new Mensagensalunos();
		$mensagensaluno = $professoresmensagens->getMensagensalunoById($id, array());
		
		if (!$mensagensaluno) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $mensagensaluno;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idprofessor =  Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['id'];
		$idserieturma =  $this->getRequest()->getPost('serieturmaid');
		$serie = substr($idserieturma, 0,1);
		$turma = substr($idserieturma, 1,1);
		$idserie = substr($idserieturma, 2,5);
		
		$idescola =  (int)$this->getRequest()->getPost('idescola');

		$escolasvinculosprofessoresmaterias = Escolasvinculosprofessoresmaterias::getEscolasvinculosprofessoresmateriasHelper(
			array('idprofessor' => $idprofessor, 'serie' => $serie, 'turma' => $turma, 'idescolavinculo' => $idserie)
		);
		
		$escolassalasatribuicoes = Escolassalasatribuicoes::getSalasatribuicoesHelper(array('idvinculo' => $escolasvinculosprofessoresmaterias[0]['idescolavinculo'] ));
		$this->view->rows = $escolassalasatribuicoes;
		/*
    	$escolasalunosnotas = new Escolassalasalunos();

    	$alunos = array();
		$salasalunos = $escolasalunosnotas->getSalasalunos(array('idescola'=>(int)Usuarios::getUsuario('idescola'), 'idserie'=>14));
		if(sizeof($salasalunos) > 0)
		{
			foreach($salasalunos as $sala)
			{
				$rows = explode(',', $sala['idsalunos']);
				foreach($rows as $id)
				{
					$aluno = Escolasalunos::getEscolaalunoByIdHelper($id, array('idescola' => (int)Usuarios::getUsuario('idescola')));

					if($aluno)
					{
						array_push($alunos, $aluno);
					}
				}
			}
		}
		//var_dump($alunos);die();	

		$this->view->rows = $alunos;
		*/
 
	}
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Mensagens para aluno')
		);
		
		$ns = new Zend_Session_Namespace('default_professoresmensagens');
		$professoresmensagens = new Mensagensalunos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
			if ($this->view->post_var["mensagem"]!="") $queries["mensagem"] = $this->view->post_var["mensagem"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $professoresmensagens->getMensagensalunos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $professoresmensagens->getMensagensalunos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de professoresmensagens
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'professoresmensagens', 'name' => 'Mensagens para aluno'),
			array('url' => null,'name' => 'Editar Mensagem')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$professoresmensagens = new Mensagensalunos();
		$mensagensaluno = $professoresmensagens->getMensagensalunoById($id);
		
		if (!$mensagensaluno) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $mensagensaluno;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($mensagensaluno);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mensagem editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
		$this->view->editar = true;
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de professoresmensagens 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'professoresmensagens', 'name' => 'Mensagens para aluno'),
			array('url' => null,'name' => 'Adicionar Mensagem')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mensagem adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		$this->view->adicionar = true;
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idmensagensaluno
     */    
    private function preForm($idmensagensaluno = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_mensagensaluno = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$qtdaluno = $this->getRequest()->getPost("idaluno");
		$idaluno = $this->getRequest()->getPost("idaluno");
        $idaluno = implode(',', $idaluno);
		$idserie = (int)trim($this->getRequest()->getPost("serieturmaid"));
		$serieturmaid = trim($this->getRequest()->getPost("serieturmaid"));
		$titulo = trim($this->getRequest()->getPost("titulo"));
		$mensagem = trim($this->getRequest()->getPost("mensagem"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		$erros = array();
		
		if (0==$idserie) array_push($erros, "Informe a Série.");
		if (0==$idaluno) array_push($erros, "Informe a Alunos.");
		if (""==$titulo) array_push($erros, "Informe a Título.");
		if (""==$status) array_push($erros, "Informe a Status.");


		
		$professoresmensagens = new Mensagensalunos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = (int)Usuarios::getUsuario('idescola');
			$dados["idaluno"] = $idaluno;
			$dados["idserie"] = $idserie;
			$dados["serieturmaid"] = $serieturmaid;
			$dados["titulo"] = $titulo;
			$dados["mensagem"] = $mensagem;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $professoresmensagens->save($dados);

			$idsdoalunoparaenviaremail = explode(',', $idaluno );
			for ($i=0; $i < count($qtdaluno) ; $i++) { // envia email para os alunos 
				$alunoId = Escolassalasatribuicoes::getSalaatribuicaoByIdHelper($idsdoalunoparaenviaremail[$i]); 
				$email = Escolasalunos::getEscolaalunoByIdHelper($alunoId['id'])['email'];
				Mn_Util:: sendMailProfessoresmensagens($email, $dados["titulo"], $dados["mensagem"], null);
			}
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}