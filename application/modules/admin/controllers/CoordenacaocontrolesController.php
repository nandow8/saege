<?php
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
    	 $image_file = "public/admin/imagens/logosantaisabel.jpg";
		$this->Image($image_file, 16, 15, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
        //Set font
		$this->SetFont('helvetica', 'B', 20);
        $this->SetFont('helvetica', 'B', 10);
       
        // cabeçalho Endereço
        $this->SetXY(16, 12);
        $this->Cell(0, 0, 'Prefeitura Municipal de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 9);
        $this->SetXY(35, 16);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Coordenação - Controle Diário', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(35, 19);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(35, 22);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetXY(35, 25);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');

        //informações Saege
        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,20);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

       	$controle = new Coordenacaocontroles();
       	$datafinal = $controle->getCurrentData();

        $this->SetFont('helvetica', '', 8);
        $this->SetXY(50,25);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: '.$datafinal, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 10);
        $this->SetXY(60,30);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');    	
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

/**
 * Controle da classe coordenacaocontroles do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_CoordenacaocontrolesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Coordenacaocontrole
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("coordenacaocontroles", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}


		$coordenacaocontroles = new Coordenacaocontroles();
		$queries = array();	
		$ultimo = $coordenacaocontroles->getUltimoCoordenacaocontrole($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Coordenacaocontroles();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle do dia excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="coordenacaocontroles") $objs = new Coordenacaocontroles();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaocontroles', 'name' => 'Coordenador - Controle Diário'),
			array('url' => null,'name' => 'Visualizar Controle do dia')
		);
		
		$id = (int)$this->_request->getParam("id");
		$coordenacaocontroles = new Coordenacaocontroles();
		$coordenacaocontrole = $coordenacaocontroles->getCoordenacaocontroleById($id, array());
		
		if (!$coordenacaocontrole) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaocontrole;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Coordenador - Controle Diário')
		);
		
		$ns = new Zend_Session_Namespace('default_coordenacaocontroles');
		$coordenacaocontroles = new Coordenacaocontroles();
		$queries = array();	

		$idperfil = Usuarios::getUsuario('idperfil');
		$perfil = UsuariosPerfis::getPerfilByIdHelper($idperfil);
		$coordenacao = isset($perfil['coordenacao']) ? $perfil['coordenacao'] : false;
		if($idperfil==29){
			$queries['idescola'] = (int)Usuarios::getUsuario('idescola');
		}

		/*if($coordenacao == 'sim')
		{
			$queries['idfuncionario'] = (int)Usuarios::getUsuario('id');
		}*/
		//$queries['idsescolas'] = (int)Usuarios::getUsuario('idescola');
		
		//var_dump($queries);die();
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);    	
		
		if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
		if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
		if ($this->view->post_var["idcoordenador"]!="") $queries["idcoordenador"] = $this->view->post_var["idcoordenador"];

		if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
		if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}	
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;

		$queries['total'] = true;
		$totalRegistros = $coordenacaocontroles->getCoordenacaocontroles($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		

		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $coordenacaocontroles->getCoordenacaocontroles($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de coordenacaocontroles
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaocontroles', 'name' => 'Coordenador - Controle Diário'),
			array('url' => null,'name' => 'Editar Controle do dia')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$coordenacaocontroles = new Coordenacaocontroles();
		$coordenacaocontrole = $coordenacaocontroles->getCoordenacaocontroleById($id);
		
		if (!$coordenacaocontrole) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaocontrole;
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($coordenacaocontrole);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle do dia editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de coordenacaocontroles 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaocontroles', 'name' => 'Coordenador - Controle Diário'),
			array('url' => null,'name' => 'Adicionar Controle do dia')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle do dia adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
     /**
    * Action para gerar o documento PDF
    **/
    public function gerardocumentoAction(){
    	$id = $this->_request->getParam('id');
    	
    	if(empty($id)){
			$messageNameSpace = new Zend_Session_Namespace("message");
			$messageNameSpace->crudmessage = "Falha ao gerar documento. Tente novamente";
    		$this->_redirect('admin/' . $this->getRequest()->getControllerName());
    	}

    	$controles = new Coordenacaocontroles();
    	$controle = $controles->getCoordenacaocontroleById($id);

    	$idescola = $controle['idescola'];
    	
    	$escola = Escolas::getEscolaByIdHelper($idescola);
    	$receptor = Usuarios::getUsuarioByIdHelper($controle['idusuarioaceite']);
    	unset($receptor['id']);
    	unset($receptor['senha']);
    	unset($receptor['excluido']);
    	
    	$coordenador = Usuarios::getUsuarioByIdHelper($controle['idusuariocriacao']);
    	unset($coordenador['id']);
    	unset($coordenador['senha']);
    	unset($coordenador['excluido']);

       	$datafinal = $controles->getCurrentData();

		$local = 'Santa Isabel';
		$localData = $local.' , '.$datafinal;
		
    	$this->view->controle = $controle;
    	$this->view->escola = $escola;
    	$this->view->receptor = $receptor;
    	$this->view->coordenador = $coordenador;
    	$this->view->localdata = $localData;
    	$this->preForm();

    	$db = Zend_Registry::get('db');
		
		// criar documento PDF
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('p');

		// Setar informações do documento
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
		$pdf->SetTitle('Controle Diário');
				
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		//margins
		$pdf->SetMargins(10,15,10);
		//fator de escala de imagem
		$pdf->SetFont('helvetica', '', 10, 'false');
		//adiciona página
		$pdf->AddPage();

		$html = $this->view->render('coordenacaocontroles/pdf/geradorpdf.phtml');
		$pdf->writeHTML($html, true, 0, true, 0);

		$filename = 'controle_diario_'.date('d').'_'.date('m').'.pdf';

		$pdf->Output($filename, 'I');

    	die();
    	return true;
    }


    /**
     * Atribui valores ao view
     * @param int $idcoordenacaocontrole
     */    
    private function preForm($idcoordenacaocontrole = 0) {
    }
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_coordenacaocontrole = false) {
    	$idusuariocriacaotemp = (isset($this->view->post_var["idusuariocriacao"])) ? $this->view->post_var["idusuariocriacao"] : Usuarios::getUsuario("id"); 
    	$datacriacaotemp = (isset($this->view->post_var["datacriacao"])) ? $this->view->post_var["datacriacao"] : date("Y-m-d");
    	if(Usuarios::getUsuario('idperfil')==29):
    		$idescolatemp = (isset($this->view->post_var["idescola"])) ? $this->view->post_var["idescola"] : (int)trim($this->getRequest()->getPost("idescola"));
    	endif;

		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
		$idusuarioaceite = (Usuarios::getUsuario("idperfil")==29) ? Usuarios::getUsuario("id"): '';
		$dataaceite = (Usuarios::getUsuario("idperfil")==29) ? date('Y-m-d') : null;
		$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
		$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
		//$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		$periodo = trim($this->getRequest()->getPost("periodo"));
		$assunto = trim($this->getRequest()->getPost("assunto"));
		$descricoes = trim($this->getRequest()->getPost("descricoes"));
		$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		$idusuariocriacao = (int)trim($idusuariocriacaotemp);

		$idescola = (isset($idescolatemp)) ? $idescolatemp : (int)trim($this->getRequest()->getPost("idescola"));

		
		$erros = array();
		
		if (""==$data) array_push($erros, "Informe a Data.");
		if (0==$idescola) array_push($erros, "Informe a Escola.");
		if (""==$periodo) array_push($erros, "Informe a Período.");
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$coordenacaocontroles = new Coordenacaocontroles();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			if((int)$id <= 0)$dados["sequencial"] = $sequencial;
			if((int)$id <= 0)$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			if((int)$id <= 0)$dados["horalancamento"] = $horalancamento;
			if((int)$id <= 0)$dados["idusuariologado"] = $idusuariologado;
			if((int)$id <= 0)$dados["idusuariocriacao"] = $idusuariocriacao;
			$dados["idusuarioaceite"] = $idusuarioaceite;
			$dados["dataaceite"] = $dataaceite;
			//$dados["idfuncionario"] = $idfuncionario;
			$dados["data"] = date("Y-m-d", $data);
			$dados["idescola"] = $idescola;
			$dados["periodo"] = $periodo;
			$dados["assunto"] = $assunto;
			$dados["descricoes"] = $descricoes;
			

			$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $coordenacaocontroles->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}