<?php

// class MYPDF extends TCPDF {

//     //Page header
//     public function Header() {

//         $image_file = "public/admin/imagens/logosantaisabel.jpg";
//         $this->Image($image_file, 16, 15, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

//         //Set font
//         $this->SetFont('helvetica', 'B', 20);
//         $this->SetFont('helvetica', 'B', 10);

//         // cabeçalho Endereço
//         $this->SetXY(16, 12);
//         $this->Cell(0, 0, 'Prefeitura Municipal de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 9);
//         $this->SetXY(35, 16);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Patrimônios - Baixa de bens', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 19);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 22);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetXY(35, 25);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');

//         //informações Saege
//         $this->SetFont('helvetica', '', 8);
//         $this->SetXY(60, 20);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

//         $patrimoniostransferencia = new Patrimoniostrasnferencias();
//         $datafinal = $patrimoniostransferencia->getCurrentData();

//         $this->SetFont('helvetica', '', 8);
//         $this->SetXY(60, 25);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Data: ' . $datafinal, 0, false, 'R', 0, '', 0, false, 'M', 'M');

//         $this->SetFont('helvetica', '', 10);
//         $this->SetXY(60, 30);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
//     }

//     // Page footer
//     public function Footer() {
//         // Position at 15 mm from bottom
//         $this->SetY(-15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

//         // Position at 15 mm from bottom
//         $this->SetXY(150, -15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
//     }

// }

/**
 * Controle da classe patrimoniostrasnferencias do sistema
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PatrimoniostrasnferenciasController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Patrimoniostrasnferencia
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("patrimoniostrasnferencias", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }

        $patrimonioscomprasbens = new Patrimoniostrasnferencias();
        $queries = array();
        $ultimo = $patrimonioscomprasbens->getUltimoPatrimoniotrasnferencias($queries);

        $sequencial = '1/' . date('Y');
        if (isset($ultimo['id'])) {
            $sequencial = ($ultimo['id'] + 1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
        }
        $this->view->sequencial = $sequencial;
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Patrimoniostrasnferencias();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Transferência excluído com sucesso.";

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "patrimoniostrasnferencias")
            $objs = new Patrimoniostrasnferencias();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'patrimoniostrasnferencias', 'name' => 'Transferências'),
            array('url' => null, 'name' => 'Visualizar Transferência')
        );

        $id = (int) $this->_request->getParam("id");
        $patrimoniostrasnferencias = new Patrimoniostrasnferencias();
        $patrimoniostrasnferencia = $patrimoniostrasnferencias->getPatrimoniostrasnferenciaById($id, array());

        if (!$patrimoniostrasnferencia)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());


        $this->view->post_var = $patrimoniostrasnferencia;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Transferências')
        );

        $ns = new Zend_Session_Namespace('default_patrimoniostrasnferencias');
        $patrimoniostrasnferencias = new Patrimoniostrasnferencias();
        $queries = array();

        $dados = Usuarios::getUsuarios(array('id' => Usuarios::getUsuario('id')));
        $_data = $dados[0];
        $perfil = $_data['idperfil'];

        if ($perfil == 29):
            $escola = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'));
            $queries['idescola'] = $escola['id'];
        endif;

        if (!UsuariosPerfis::isAllowedCrudPatrimonio($perfil))
            $queries['iddepartamentoorigem'] = $perfil;

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ($this->view->post_var["sequencial"] != "")
                $queries["sequencial"] = $this->view->post_var["sequencial"];
            if ($this->view->post_var["datalancamento_i"] != "")
                $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
            if ($this->view->post_var["datalancamento_f"] != "")
                $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
            if ($this->view->post_var["idpatrimonio"] != "")
                $queries["idpatrimonio"] = $this->view->post_var["idpatrimonio"];
            if ($this->view->post_var["status1"] != "")
                $queries["status"] = $this->view->post_var["status1"];

            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $patrimoniostrasnferencias->getPatrimoniostrasnferencias($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $patrimoniostrasnferencias->getPatrimoniostrasnferencias($queries, $paginaAtual, $maxpp);
    }

    /**
     *
     * Action de edição de patrimoniostrasnferencias
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'patrimoniostrasnferencias', 'name' => 'Transferências'),
            array('url' => null, 'name' => 'Editar Transferência')
        );

        $id = (int) $this->_request->getParam("id");
        $patrimoniostrasnferencias = new Patrimoniostrasnferencias();
        $patrimoniostrasnferencia = $patrimoniostrasnferencias->getPatrimoniostrasnferenciaById($id);

        if (!$patrimoniostrasnferencia)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $patrimoniostrasnferencia;

        $this->view->editar = true;

        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($patrimoniostrasnferencia);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Transferência editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     *
     * Action de adição de patrimoniostrasnferencias
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'patrimoniostrasnferencias', 'name' => 'Transferências'),
            array('url' => null, 'name' => 'Adicionar Transferência')
        );
        $this->view->adicionar = true;
        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Transferência adicionado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idpatrimoniostrasnferencia
     */
    private function preForm($idpatrimoniostrasnferencia = 0) {

    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_patrimoniostrasnferencia = false) {
        $iddepartamentoorigemtemp = (isset($this->view->post_var['iddepartamentoorigem']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamentoorigem'])) ? (int) $this->view->post_var['iddepartamentoorigem'] : (int) trim($this->getRequest()->getPost("iddepartamentoorigem"));

        $idescolatemp = (isset($this->view->post_var['idescola']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamentoorigem'])) ? (int) $this->view->post_var['idescola'] : (int) trim($this->getRequest()->getPost("idescola"));

        $iddepartamentodestinotemp = (isset($this->view->post_var['iddepartamentodestino']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamentoorigem'])) ? (int) $this->view->post_var['iddepartamentodestino'] : (int) trim($this->getRequest()->getPost("iddepartamentodestino"));

        $idescolatransferenciatemp = (isset($this->view->post_var['idescolatransferencia']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamentoorigem'])) ? (int) $this->view->post_var['idescolatransferencia'] : (int) trim($this->getRequest()->getPost("idescolatransferencia"));

        $idpatrimoniotemp = (isset($this->view->post_var['idpatrimonio']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamentoorigem'])) ? (int) $this->view->post_var['idpatrimonio'] : (int) $this->getRequest()->getPost('idpatrimonio');


        $observacoestemp = (isset($this->view->post_var['observacoes']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamentoorigem'])) ? $this->view->post_var['observacoes'] : trim($this->getRequest()->getPost("observacoes"));

        $statustemp = (isset($this->view->post_var['status1']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamentoorigem'])) ? $this->view->post_var['status1'] : trim($this->getRequest()->getPost("status1"));


        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $sequencial = trim($this->getRequest()->getPost("sequencial"));
        $datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
        $horalancamento = trim($this->getRequest()->getPost("horalancamento"));
        $idusuariologado = (int) trim($this->getRequest()->getPost("idusuariologado"));
        $origem = trim($this->getRequest()->getPost("origem"));
        $idescola = $idescolatemp;
        $iddepartamentoorigem = $iddepartamentoorigemtemp;

        $idpatrimonio = $idpatrimoniotemp;
        $transferencia = trim($this->getRequest()->getPost("transferencia"));
        $origemtransferencia = trim($this->getRequest()->getPost("origemtransferencia"));
        $iddepartamentodestino = $iddepartamentodestinotemp;

        $idescolatransferencia = $idescolatransferenciatemp;

        $observacoes = $observacoestemp;
        $baixatransferencia = trim($this->getRequest()->getPost("baixatransferencia"));

        $iddocumento = (int) trim($this->getRequest()->getPost("iddocumento"));
        $status = $statustemp;
        $motivonegacao = trim($this->getRequest()->getPost("motivonegacao"));

        $erros = array();

        if ("" == $status)
            array_push($erros, "Informe a Status.");


        $patrimoniostrasnferencias = new Patrimoniostrasnferencias();

        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;

            $dados["sequencial"] = $sequencial;
            $dados["datalancamento"] = date("Y-m-d", $datalancamento);
            $dados["horalancamento"] = $horalancamento;
            $dados["idusuariologado"] = $idusuariologado;
            $dados["origem"] = $origem;
            $dados["idescola"] = $idescola;
            $dados["iddepartamentoorigem"] = $iddepartamentoorigem;
            $dados["idpatrimonio"] = $idpatrimonio;
            $dados["transferencia"] = $transferencia;
            $dados["origemtransferencia"] = $origemtransferencia;
            $dados["idescolatransferencia"] = $idescolatransferencia;
            $dados["iddepartamentodestino"] = $iddepartamentodestino;
            $dados["observacoes"] = $observacoes;
            $dados["baixatransferencia"] = $baixatransferencia;
            $iddocumento = $this->getArquivo('iddocumento');
            if ($iddocumento != 0):
                $dados['iddocumento'] = $iddocumento;
                $dados['status'] = "Aguardando Aceite";
            else:
                $dados["status"] = $status;
            endif;
            $dados['motivonegacao'] = $motivonegacao;
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            ;
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $patrimoniostrasnferencias->save($dados);

            if ($row):
                if (isset($dados['status']) && $dados['status'] == 'Aceito'):
                    $idpatrimonio = $dados['idpatrimonio'];

                    $info['id'] = $idpatrimonio;
                    $info['idperfil'] = $iddepartamentodestino;
                    if ($iddepartamentodestino == 29):
                        $info['codescola'] = $idescolatransferencia;
                    else:
                        $info['codescola'] = null;
                    endif;

                    $patrimonio = new Patrimonios();
                    $patrimonio->save($info);
                endif;
            endif;

            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();

            $db->rollBack();
            die();
        }

        return "";
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

    public function gerardocumentotransferenciaAction() {

        $id = $this->_request->getParam('id');

        /*         * INFORMAÇÕES DO BAIXA* */
        $transfer = Patrimoniostrasnferencias::getPatrimoniostrasnferenciaByIdHelper($id);
        $observacoes = (isset($transfer['observacoes'])) ? $transfer['observacoes'] : '';
        //origem
        $idescola = (isset($transfer['idescola']) && $transfer['idescola'] > 0 ) ? $transfer['idescola'] : '';
        $departamentoorigem = (isset($transfer['iddepartamentoorigem']) && $transfer['iddepartamentoorigem'] > 0 ) ? $transfer['iddepartamentoorigem'] : '';

        //destino
        $idescolatransferencia = (isset($transfer['idescolatransferencia']) && $transfer['idescolatransferencia'] > 0 ) ? $transfer['idescolatransferencia'] : '';
        $departamentodestino = (isset($transfer['iddepartamentodestino']) && $transfer['iddepartamentodestino'] > 0 ) ? $transfer['iddepartamentodestino'] : '';

        if ($idescola) :
            $_rowEscola = Escolas::getEscolaByIdHelper($idescola);
            $escola = $_rowEscola['escola'];
        endif;
        if ($departamentoorigem):
            $_rowPerfil = UsuariosPerfis::getPerfilByIdHelper($departamentoorigem);
            $perfil = $_rowPerfil['perfil'];
        endif;

        if ($idescolatransferencia) :
            $_rowEscola = Escolas::getEscolaByIdHelper($idescolatransferencia);
            $escoladestino = $_rowEscola['escola'];
        endif;
        if ($departamentodestino):
            $_rowPerfil = UsuariosPerfis::getPerfilByIdHelper($departamentodestino);
            $perfildestino = $_rowPerfil['perfil'];
        endif;


        /*         * INFORMAÇÕES DA TRANSFERÊNCIA* */
        $___rows = Patrimonios::getPatrimoniosHelper(array('id' => $transfer['idpatrimonio']));

        $numeropatrimonio = (isset($___rows[0]['numeropatrimonio'])) ? $___rows[0]['numeropatrimonio'] : '';
        $patrimonio = (isset($___rows[0]['patrimonio'])) ? $___rows[0]['patrimonio'] : '';
        $descricoesdobem = (isset($___rows[0]['descricoesdobem'])) ? $___rows[0]['descricoesdobem'] : '';

        $patrimoniostransferencia = new Patrimoniostrasnferencias();
        $datafinal = $patrimoniostransferencia->getCurrentData();

        $local = 'Santa Isabel';
        $localData = $local . ' , ' . $datafinal;

        /* =========================    Setando valores na View ========================== */
        $this->view->observacoes = $observacoes;
        if (isset($escola))
            $this->view->escola = $escola;
        if (isset($perfil))
            $this->view->perfil = $perfil;
        if (isset($escoladestino))
            $this->view->escoladestino = $escoladestino;
        if (isset($perfildestino))
            $this->view->perfildestino = $perfildestino;
        $this->view->numeropatrimonio = $numeropatrimonio;
        $this->view->patrimonio = $patrimonio;
        $this->view->descricoesdobem = $descricoesdobem;
        $this->view->localdata = $localData;
        /* =====================    END Setando valores na View ========================== */

        $this->preForm();


        $db = Zend_Registry::get('db');

        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Requerimento de transferência de patrimônio');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 9);
        // add a page
        $pdf->AddPage();

        $html = $this->view->render('patrimoniostrasnferencias/pdf/patrimoniostransferenciadocumento.phtml');

        $pdf->writeHTML($html, true, 0, true, 0);


        $filename = 'transferencia_de_bem.pdf';
        $pdf->Output($filename, 'I');
        die();
        return true;
    }

}
