<?php

/**
 * Controle da classe almoxarifadoprodutos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_AlmoxarifadoprodutosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Almoxarifadoproduto
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("almoxarifadoprodutos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Almoxarifadoprodutos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Produto excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="almoxarifadoprodutos") $objs = new Almoxarifadoprodutos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadoprodutos', 'name' => 'Produtos'),
			array('url' => null,'name' => 'Visualizar Produto')
		);
		
		$id = (int)$this->_request->getParam("id");
		$almoxarifadoprodutos = new Almoxarifadoprodutos();
		$almoxarifadoproduto = $almoxarifadoprodutos->getAlmoxarifadoprodutoById($id, array());
		
		if (!$almoxarifadoproduto) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $almoxarifadoproduto;
		$this->preForm();

		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Produtos')
		);
		
		$ns = new Zend_Session_Namespace('default_almoxarifadoprodutos');
		$almoxarifadoprodutos = new Almoxarifadoprodutos();
		$queries = array();	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		//PESQUISA
		if ($this->getRequest()->isPost()) {
			$ns->pesquisa = serialize($_POST);
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
			die();	
		}

		if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);

		if (isset($this->view->post_var)) {
			foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);

			if ($this->view->post_var["produto"]!="") $queries["produto"] = $this->view->post_var["produto"];
			if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];

			if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
		}		
		
		//PAGINACAO
		$maxpp = 20;
		
		$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $almoxarifadoprodutos->getAlmoxarifadoprodutos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $almoxarifadoprodutos->getAlmoxarifadoprodutos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de almoxarifadoprodutos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadoprodutos', 'name' => 'Produtos'),
			array('url' => null,'name' => 'Editar Produto')
		);	

		$id = (int)$this->_request->getParam("id");
		$almoxarifadoprodutos = new Almoxarifadoprodutos();
		$almoxarifadoproduto = $almoxarifadoprodutos->getAlmoxarifadoprodutoById($id);
		
		if (!$almoxarifadoproduto) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $almoxarifadoproduto;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($almoxarifadoproduto);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Produto editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	

		return true;		
	}  		
	
	/**
	 * 
	 * Action de adição de almoxarifadoprodutos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadoprodutos', 'name' => 'Produtos'),
			array('url' => null,'name' => 'Adicionar Produto')
		);	

		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Produto adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
	}	

    /**
     * Atribui valores ao view
     * @param int $idalmoxarifadoproduto
     */    
    private function preForm($idalmoxarifadoproduto = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
	private function getPost($_almoxarifadoproduto = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)trim($this->getRequest()->getPost("idsecretaria"));
		$produto = trim($this->getRequest()->getPost("produto"));
		$descricoes = trim($this->getRequest()->getPost("descricoes"));
		$codigoproduto = trim($this->getRequest()->getPost("codigoproduto"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		$idcategoria = (int)trim($this->getRequest()->getPost("idcategoria"));
		$categoria_id = (int)trim($this->getRequest()->getPost("categoria_id"));
		$old_unidademedida = trim($this->getRequest()->getPost("old_unidademedida"));
		$qtdmin = (int)trim($this->getRequest()->getPost("qtdmin"));

		$idimagem = (int)trim($this->getRequest()->getPost("idimagem"));
		
		$erros = array();
		
		if (""==$produto) array_push($erros, "Informe a Produto.");
		if (""==$status) array_push($erros, "Informe a Status.");
		
		$almoxarifadoprodutos = new Almoxarifadoprodutos();

		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
			$dados["produto"] = $produto;
			$dados["descricoes"] = $descricoes;
			$dados["codigoproduto"] = $codigoproduto;
			$dados["status"] = $status;

			$dados["idcategoria"] = $idcategoria;
			$dados["categoria_id"] = $categoria_id;
			$dados["old_unidademedida"] = $old_unidademedida;
			$dados["qtdmin"] = $qtdmin;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

			$idimagem = $this->getImagem('idimagem');
			if ($idimagem!=0) $dados['idimagem'] = $idimagem;

			$row = $almoxarifadoprodutos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
	}

	private function getArquivo($filename) {
		$idarquivo = false;
		$arquivos = new Arquivos();

		try {
			$idarquivo = $arquivos->getArquivoFromForm($filename);
		} catch (Exception $e) {
			$idarquivo = false;
			array_push($erros,$e->getMessage());
		}

		$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));

		if ($excluir_arquivo=='excluir') $idarquivo = -1;

		return $idarquivo;
	}

	private function getImagem($nome = false, $apenas_copia = false) {

		$idimagem = false;
		$imagens = new Imagens();

		try {

			ini_set('memory_limit', '-1');
			$idimagem = $imagens->getImagemFromForm($nome, NULL, NULL, $apenas_copia);
			//var_dump($idimagem); die();
		} catch (Exception $e) {

			$idimagem = false;
			array_push($erros,$e->getMessage());

		}

		$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $nome));

		if ($excluir_imagem=='excluir_' . $nome) $idimagem = -1;

		return $idimagem;
	}

}