<?php

/**
 * Controle da classe patrimoniosdoacoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PatrimoniosdoacoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Patrimoniosdoacao
	 */
	protected $_cadastro = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->cadastro)) $this->_redirect(Licencas::getLicenca('alias') . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockCadastroAccess("patrimoniosdoacoes", $this->_request->getActionName());	
		
		$this->_cadastro = unserialize($loginNameSpace->cadastro);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}		

	}	


	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Patrimoniosdoacoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_cadastro['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Doação excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="patrimoniosdoacoes") $objs = new Patrimoniosdoacoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_cadastro['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosdoacoes', 'name' => 'Doações'),
			array('url' => null,'name' => 'Visualizar Doação')
		);	
				
		
		$id = (int)$this->_request->getParam("id");
		$patrimoniosdoacoes = new Patrimoniosdoacoes();
		$patrimoniosdoacao = $patrimoniosdoacoes->getPatrimoniosdoacaoById($id, array());
		
		if (!$patrimoniosdoacao) 
			$this->_redirect(Licencas::getLicenca('alias') . '/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $patrimoniosdoacao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Doações')
		);
		
		
		$ns = new Zend_Session_Namespace('default_patrimoniosdoacoes');
		$patrimoniosdoacoes = new Patrimoniosdoacoes();
		$queries = array();	
		$queries['idlicenca'] = Licencas::getLicenca('id');
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect(Licencas::getLicenca('alias') . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ($this->view->post_var["idsecretaria"]!="") $queries["idsecretaria"] = $this->view->post_var["idsecretaria"];
    		if ($this->view->post_var["doacao"]!="") $queries["doacao"] = $this->view->post_var["doacao"];
    		if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
    		if ($this->view->post_var["idusuariocriacao"]!="") $queries["idusuariocriacao"] = $this->view->post_var["idusuariocriacao"];
    		if ($this->view->post_var["aprovacao"]!="") $queries["aprovacao"] = $this->view->post_var["aprovacao"];
    		if ($this->view->post_var["descricoesaprovacao"]!="") $queries["descricoesaprovacao"] = $this->view->post_var["descricoesaprovacao"];
    		if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $patrimoniosdoacoes->getPatrimoniosdoacoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $patrimoniosdoacoes->getPatrimoniosdoacoes($queries, $paginaAtual, $maxpp);	
		
		
			
	}
	
	/**
	 * 
	 * Action de edição de patrimoniosdoacoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosdoacoes', 'name' => 'Doações'),
			array('url' => null,'name' => 'Editar Doação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$patrimoniosdoacoes = new Patrimoniosdoacoes();
		$patrimoniosdoacao = $patrimoniosdoacoes->getPatrimoniosdoacaoById($id);
		
		if (!$patrimoniosdoacao) 
			$this->_redirect(Licencas::getLicenca('alias') . '/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $patrimoniosdoacao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($patrimoniosdoacao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Doação editado com sucesso.";
			
			$this->_redirect(Licencas::getLicenca('alias') . '/' . $this->getRequest()->getControllerName());
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de patrimoniosdoacoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosdoacoes', 'name' => 'Doações'),
			array('url' => null,'name' => 'Adicionar Doação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Doação adicionado com sucesso.";
			
			$this->_redirect(Licencas::getLicenca('alias') . '/' . $this->getRequest()->getControllerName());
		}
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idpatrimoniosdoacao
     */    
    private function preForm($idpatrimoniosdoacao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_patrimoniosdoacao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)trim($this->getRequest()->getPost("idsecretaria"));
		$doacao = trim($this->getRequest()->getPost("doacao"));
		$descricoes = trim($this->getRequest()->getPost("descricoes"));
		$idusuariocriacao = (int)trim($this->getRequest()->getPost("idusuariocriacao"));
		$aprovacao = trim($this->getRequest()->getPost("aprovacao"));
		$idusuarioaprovacao = (int)trim($this->getRequest()->getPost("idusuarioaprovacao"));
		$logdataaprovacao = trim($this->getRequest()->getPost("logdataaprovacao"));
		$descricoesaprovacao = trim($this->getRequest()->getPost("descricoesaprovacao"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idsecretaria) array_push($erros, "Informe a Secretaria.");
		if (""==$doacao) array_push($erros, "Informe a Doação.");
		if (0==$idusuariocriacao) array_push($erros, "Informe a Funcionário.");
		if (""==$aprovacao) array_push($erros, "Informe a Aprovação.");
		if (0==$idusuarioaprovacao) array_push($erros, "Informe a Usuários de aprovação.");
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$patrimoniosdoacoes = new Patrimoniosdoacoes();
		

		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			$dados['idlicenca'] = Licencas::getLicenca('id');
			
			$dados["idsecretaria"] = $idsecretaria;
			$dados["doacao"] = $doacao;
			$dados["descricoes"] = $descricoes;
			$dados["idusuariocriacao"] = $idusuariocriacao;
			$dados["aprovacao"] = $aprovacao;
			$dados["idusuarioaprovacao"] = $idusuarioaprovacao;
			$dados["logdataaprovacao"] = $logdataaprovacao;
			$dados["descricoesaprovacao"] = $descricoesaprovacao;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_cadastro['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $patrimoniosdoacoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	
    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }    

	
	
}