<?php

class Admin_CalendarioexternoController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacoteescola
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("calendarioexterno", 'index');	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}

	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => null,'name' => 'Calendário')
		);	
				
		$this->preForm();
		$comunicacaoagenda = new Eventossecretarias();
		$queries = array();	
		$queries["origem"] = "Comunicação";	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['tipo'] = "Externa";
		$queries['order'] = "ORDER BY e1.datainicio DESC";		
		$this->view->rows = $comunicacaoagenda->getEventossecretarias($queries, '0', '5');	
    }

	public function index2Action() {
		$this->view->bread_crumb = array(
			array('url' => null,'name' => 'Calendário')
		);	
				
		$this->preForm();
		$comunicacaoagenda = new Eventossecretarias();
		$queries = array();	
		$queries["tipo"] = "Externa";
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['order'] = "ORDER BY e1.datainicio DESC";		
		$this->view->rows = $comunicacaoagenda->getEventossecretarias($queries, '0', '5');	
    }    
    
	public function setcalendarioAction() {
		$this->_helper->layout->disableLayout();
		$this->preForm();
		
		$page = (int)$this->getRequest()->getPost("page");
		
		$this->view->page = $page;
    }
    
    public function seteventoAction() {
    	$this->_helper->layout->disableLayout();
    	
    	$id = $this->getRequest()->getParam('id');
    	
		$comunicacaoagenda = new Eventossecretarias();
		$evento = $comunicacaoagenda->getEventosecretariaById($id);
		
		if (!$evento) die('Não Encontrado'); 
			
		$this->view->post_var = $evento;
    }
    
    public function seteventosdiaAction() {
    	$this->_helper->layout->disableLayout();
    	
    	$dia = $this->getRequest()->getParam('dia');
		
    	$queries = array();
    	$queries["origem"] = "Comunicação";	
    	$queries['tipo'] = "Externa";
		$queries['data'] = date('Y-m-d', $dia);
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['order'] = "ORDER BY e1.datainicio DESC";
		
		$comunicacaoagenda = new Eventossecretarias();
		$rows = $comunicacaoagenda->getEventossecretarias($queries);
		
		if (!$rows) die('Não Encontrado'); 
		$this->view->dia = $dia;
		$this->view->rows = $rows;
		
    }
    
    /**
     * Atribui valores ao view
     * @param int $idescola
     */    
    private function preForm($idescola = 0) {
                $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
                
		$usuarios = new Usuarios();
		$this->view->usuarios = $usuarios->getUsuarios(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		$departamentos = new Departamentossecretarias();
		$this->view->departamentos = $departamentos->getDepartamentossecretarias(array('idsecretaria' => $this->view->idsecretaria));
    } 
}
