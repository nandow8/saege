<?php

/**
 * Controle da classe diplomas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_DiplomasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Diploma
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("diplomas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Diplomas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Diploma excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="diplomas") $objs = new Diplomas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'diplomas', 'name' => 'Diplomas'),
			array('url' => null,'name' => 'Visualizar Diploma')
		);
		
		$id = (int)$this->_request->getParam("id");
		$diplomas = new Diplomas();
		$diploma = $diplomas->getDiplomaById($id, array());
		
		if (!$diploma) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $diploma;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Diplomas')
		);
		
		$ns = new Zend_Session_Namespace('default_diplomas');
		$diplomas = new Diplomas();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["curso"]!="") $queries["curso"] = $this->view->post_var["curso"];
if ($this->view->post_var["duracao"]!="") $queries["duracao"] = $this->view->post_var["duracao"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $diplomas->getDiplomas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $diplomas->getDiplomas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de diplomas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'diplomas', 'name' => 'Diplomas'),
			array('url' => null,'name' => 'Editar Diploma')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$diplomas = new Diplomas();
		$diploma = $diplomas->getDiplomaById($id);
		
		if (!$diploma) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $diploma;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($diploma);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Diploma editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de diplomas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'diplomas', 'name' => 'Diplomas'),
			array('url' => null,'name' => 'Adicionar Diploma')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Diploma adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    

public function geradiplomaAction() {
		$id = (int)$this->getRequest()->getParam('id');
		
		$rows = new Diplomas();
		$row = $rows->getDiplomaById($id);
		$this->view->post_var = $row;
		$idsalunos = explode(',', $row['idsalunos']);
		//var_dump($idsalunos); die();
		
		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('l');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('Diploma');
		$pdf->SetSubject('Diploma');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');
	
			
		//$header= $this->view->render('diplomas/pdf/header.phtml');
		//$pdf->setHeaderHtml($header);
		//$pdf->setPrintHeader(true);
		//$pdf->SetHeaderMargin(5);
	
	/*
		$footer= $this->view->render('diplomas/pdf/footer.phtml');
		$pdf->setFooterHtml($footer);
		$pdf->setPrintFooter(true);
		$pdf->SetFooterMargin(0);
	*/
	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		foreach ($idsalunos as $i=>$aluno) :
		$this->view->post_var['nomes'] = Diplomasalunos::getNomeDiplomaalunoByIdHelper($aluno);
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('diplomas/pdf/index.phtml');
		
		//echo $html;
		//die();
		$pdf->writeHTML($html, true, 0, true, 0);
		endforeach;	
		$filename = 'diploma_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}

    /**
     * Atribui valores ao view
     * @param int $iddiploma
     */    
    private function preForm($iddiploma = 0) {
        $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
                
		$alunos = new Diplomasalunos();
		$this->view->alunos = $alunos->getDiplomasalunos(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_diploma = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
    	$idsalunos = $this->getRequest()->getPost("idsalunos");
        if (!is_array($idsalunos)){ 
			$idsalunos = array();		
		}
		$idsalunos = implode(',',$idsalunos);
$idimagemassinatura = (int)trim($this->getRequest()->getPost("idimagemassinatura"));
$curso = trim($this->getRequest()->getPost("curso"));
$duracao = trim($this->getRequest()->getPost("duracao"));
$diretor = trim($this->getRequest()->getPost("diretor"));
$instituicao = trim($this->getRequest()->getPost("instituicao"));
$cidade = trim($this->getRequest()->getPost("cidade"));
$uf = trim($this->getRequest()->getPost("uf"));
$datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
$datafim = Mn_Util::stringToTime($this->getRequest()->getPost("datafim"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$curso) array_push($erros, "Informe a Curso.");
if (""==$duracao) array_push($erros, "Informe a Duração.");
if (""==$diretor) array_push($erros, "Informe a Diretor.");
if (""==$instituicao) array_push($erros, "Informe a Instituição.");
if (""==$cidade) array_push($erros, "Informe a Cidade.");
if (""==$uf) array_push($erros, "Informe a UF.");
if (""==$datainicio) array_push($erros, "Informe a Data Início.");
if (""==$datafim) array_push($erros, "Informe a Data Fim.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$diplomas = new Diplomas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = $idsecretaria;
$dados["idsalunos"] = $idsalunos;

$idimagemassinatura = $this->getImagem('idimagemassinatura');
			if ($idimagemassinatura!=0) $dados['idimagemassinatura'] = $idimagemassinatura;
$dados["curso"] = $curso;
$dados["duracao"] = $duracao;
$dados["diretor"] = $diretor;
$dados["instituicao"] = $instituicao;
$dados["cidade"] = $cidade;
$dados["uf"] = $uf;
$dados["datainicio"] = date("Y-m-d", $datainicio);
$dados["datafim"] = date("Y-m-d", $datafim);
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $diplomas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}