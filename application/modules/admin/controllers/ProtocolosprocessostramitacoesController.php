<?php

/**
 * Controle da classe protocolosprocessos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ProtocolosprocessostramitacoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Protocolosprocessotramitacoes
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("protocolosprocessos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);

                $perfis = new UsuariosPerfis();
		$perfil = $perfis->getPerfilById($this->_usuario['idperfil']);
                $isprotocolo = ((isset($perfil['id'])) && (($perfil['id'] == 39) || ($perfil['id'] == '26'))) ? true : false;
		$this->view->isprotocolo = $isprotocolo;
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$protocolosprocessostramitacoes = new Protocolosprocessostramitacoes();
		$queries = array();	
		$ultimo = $protocolosprocessostramitacoes->getUltimoProtocoloprocessotramitacoes($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;

	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Protocolosprocessostramitacoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			//$row['excluido'] = 'sim';
			//$row['logusuario'] = $this->_usuario['id'];
			$row['datacriacao'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Protocolo processos excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="protocolosprocessostramitacoes") $objs = new Protocolosprocessostramitacoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
//			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
//			$obj['logusuario'] = $this->_usuario['id'];
			$obj['datacriacao'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'protocolosprocessos', 'name' => 'Protocolos Processos'),
			array('url' => null,'name' => 'Visualizar Protocolo processos')
		);
		
		$id = (int)$this->_request->getParam("id");
		$protocolosprocessostramitacoes = new Protocolosprocessostramitacoes();
		$protocolosprocessotramitacao = $protocolosprocessostramitacoes->getProtocolosprocessotramitacoesById($id, array());
		
		if (!$protocolosprocessotramitacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $protocolosprocessotramitacao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Protocolos Processos Tramitacoes')
		);
		
		$ns = new Zend_Session_Namespace('default_protocolosprocessostramitacoes');
		$protocolosprocessostramitacoes = new Protocolosprocessostramitacoes();
		$queries = array();	
				
		//PESQUISA
                if ($this->getRequest()->isPost()) {
                        $ns->pesquisa = serialize($_POST);
                        $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
                        die();	
                }

                if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);

                if (isset($this->view->post_var)) {
                        foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);

                                if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
                                if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
                                if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
                                if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
                                if ($this->view->post_var["observacoes"]!="") $queries["observacoes"] = $this->view->post_var["observacoes"];
                                if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];

                                if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
                }		
		
		//PAGINACAO
                $maxpp = 20;
		
                $paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $protocolosprocessos->getProtocolosprocessos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $protocolosprocessos->getProtocolosprocessos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de protocolosprocessos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'protocolosprocessos', 'name' => 'Protocolos Processos'),
			array('url' => null,'name' => 'Editar Protocolo processos')
		);	
                
		$id = (int)$this->_request->getParam("id");
		$protocolosprocessostramitacoes = new Protocolosprocessostramitacoes();
		$protocolosprocessotramitacao = $protocolosprocessostramitacoes->getProtocolosprocessotramitacoesById($id);
		
		if (!$protocolosprocessotramitacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $protocolosprocessotramitacao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($protocolosprocessotramitacao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Protocolo processos editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de protocolosprocessos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'protocolosprocessostramitacoes', 'name' => 'Protocolos Processos Tramitacoes'),
			array('url' => null,'name' => 'Adicionar Protocolo processos Tramitações')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Protocolo processos tramitações adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idprotocolosprocessotramitacao
     */    
    private function preForm($idprotocolosprocessotramitacao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_protocolosprocessotramitacao = false) {
            if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
            else $this->view->post_var = array_merge($this->view->post_var, $_POST);

            $id = (int)$this->getRequest()->getPost("id");
            $idprotocoloprocesso = (int)$this->getRequest()->getPost("idprotocoloprocesso");
            $idusuario = (int)$this->getRequest()->getPost("idusuario");
            $idfuncionario = (int)$this->getRequest()->getPost("idfuncionario");
            $idperfilorigem = (int)$this->getRequest()->getPost("idperfilorigem");
            $idlocalorigem = (int)$this->getRequest()->getPost("idlocalorigem");
            $idescolaorigem = (int)$this->getRequest()->getPost("idescolaorigem");
            $idusuarioorigem = (int)$this->getRequest()->getPost("idusuarioorigem");
            $idperfildestino = (int)$this->getRequest()->getPost("idperfildestino");
            $idlocaldestino = (int)$this->getRequest()->getPost("idlocaldestino");
            $idescoladestino = (int)$this->getRequest()->getPost("idescoladestino");
            $idusuariodestino = (int)$this->getRequest()->getPost("idusuariodestino");
            $datacriacao = Mn_Util::stringToTime($this->getRequest()->getPost("datacriacao"));
            $observacao = trim($this->getRequest()->getPost("observacao")); 
            $excluido = trim($this->getRequest()->getPost("excluido")); 
            $status = trim($this->getRequest()->getPost("status")); 

            $iddocumentohistorico = (int)trim($this->getArquivo('iddocumentohistorico'));

            
            $erros = array();

            $protocolosprocessostramitacoes = new Protocolosprocessostramitacoes();
		
            if (sizeof($erros)>0) return $erros; 

            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            try {
                    $dados = array();
                    $dados["id"] = $id;
                    $dados["idprotocoloprocesso"] = $idprotocoloprocesso;
                    $dados["idusuario"] = $this->_usuario['id'];
                    $dados["idfuncionario"] = $idfuncionario;
                    $dados["idperfilorigem"] = $idperfilorigem;
                    $dados["idlocalorigem"] = $idlocalorigem;
                    $dados["idescolaorigem"] = $idescolaorigem;
                    $dados["idusuarioorigem"] = $idusuarioorigem;
                    $dados["idperfildestino"] = $idperfildestino;
                    $dados["idlocaldestino"] = $idlocaldestino;
                    $dados["idescoladestino"] = $idescoladestino;
                    $dados["idusuariodestino"] = $idusuariodestino;
                    $dados["datacriacao"] = date("Y-m-d G:i:s", $datacriacao);
                    $dados["observacao"] = $observacao;
                    $dados["excluido"] = $excluido;
                    $dados["status"] = $status;
                    
                    $iddocumentohistorico = $this->getArquivo('iddocumentohistorico');
                    if ($iddocumentohistorico != 0) {
                        $dados['iddocumentohistorico'] = $iddocumentohistorico;
                    }

                    $row = $protocolosprocessostramitacoes->save($dados);

                    $db->commit();
            } catch (Exception $e) {
                    echo $e->getMessage();

                    $db->rollBack();
                    die();
            }		

            return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}