<?php

/**
 * Controle da classe financeirocontrolesdespesas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
error_reporting(0);
class Admin_FinanceirocontrolesdespesasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Financeirocontrolesdespesa
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("financeirocontrolesdespesas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Financeirocontrolesdespesas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Despesa excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="financeirocontrolesdespesas") $objs = new Financeirocontrolesdespesas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeirocontrolesdespesas', 'name' => 'Controle de despesas, Água, Luz e Telefone'),
			array('url' => null,'name' => 'Visualizar Despesa')
		);
		
		$id = (int)$this->_request->getParam("id");
		$financeirocontrolesdespesas = new Financeirocontrolesdespesas();
		$financeirocontrolesdespesa = $financeirocontrolesdespesas->getFinanceirocontrolesdespesaById($id, array());
		
		if (!$financeirocontrolesdespesa) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $financeirocontrolesdespesa;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	private function gerapdf($queries) {

		$financeirocontrolesdespesas = new Financeirocontrolesdespesas();

		$this->view->rows = $financeirocontrolesdespesas->getFinanceirocontrolesdespesas($queries);	
		//var_dump($queries);die();

		$idescola = Usuarios::getEscolaAtiva('id');

		if((int)$idescola > 0)
		{
			$escola = Escolas::getEscolaByIdHelper($idescola);
			$this->view->escola = $escola;

			//$this->view->post_var = $declaracao;
			$endereco = Enderecos::getEnderecoById((int)$escola['idendereco']);
			if($endereco){
				unset($endereco['id']);
				unset($endereco['excluido']);
				$this->view->endereco = $endereco;
			}
		}
		
		$this->preForm();
		//var_dump($this->view->despesas); die();
		

		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('p');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('Diploma');
		$pdf->SetSubject('Diploma');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		
		
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('financeirocontrolesdespesas/pdf/index.phtml');
		//echo $html; die();

		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'declaracao_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}

	public function getdadosidentificacaoAction() {
		$this->_helper->layout->disableLayout();
		
		$ididentificacao = (int)$this->_request->getParam("ididentificacao");
		$financeiros = new Financeirotipos();
		$financeiro = $financeiros->getFinanceirotipoByIdHelper($ididentificacao, array());
		 
		if (!isset($financeiro['id'])) die('erro');
		
		$this->view->post_var = $financeiro;
		
	}
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Controle de despesas, Água, Luz e Telefone')
		);
		
		$ns = new Zend_Session_Namespace('default_financeirocontrolesdespesas');
		$financeirocontrolesdespesas = new Financeirocontrolesdespesas();
		$queries = array();	
		$queries['idescola'] = Usuarios::getUsuario('idescola');
		//var_dump(Usuarios::getUsuario('idescola'));die();
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
if ($this->view->post_var["idtipo"]!="") $queries["idtipo"] = $this->view->post_var["idtipo"];
if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["numeroidentificacao"]!="") $queries["numeroidentificacao"] = $this->view->post_var["numeroidentificacao"];
if ($this->view->post_var["datavencimento_i"]!="") $queries["datavencimento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datavencimento_i"]));
if ($this->view->post_var["datavencimento_f"]!="") $queries["datavencimento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datavencimento_f"]));
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $financeirocontrolesdespesas->getFinanceirocontrolesdespesas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $financeirocontrolesdespesas->getFinanceirocontrolesdespesas($queries, $paginaAtual, $maxpp);	

		if($this->_request->getParam("to") == 'pdf')
		{
			$this->gerapdf($queries);
		}
	}
	
	/**
	 * 
	 * Action de edição de financeirocontrolesdespesas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeirocontrolesdespesas', 'name' => 'Controle de despesas, Água, Luz e Telefone'),
			array('url' => null,'name' => 'Editar Despesa')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$financeirocontrolesdespesas = new Financeirocontrolesdespesas();
		$financeirocontrolesdespesa = $financeirocontrolesdespesas->getFinanceirocontrolesdespesaById($id);
		
		if (!$financeirocontrolesdespesa) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $financeirocontrolesdespesa;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($financeirocontrolesdespesa);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Despesa editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de financeirocontrolesdespesas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeirocontrolesdespesas', 'name' => 'Controle de despesas, Água, Luz e Telefone'),
			array('url' => null,'name' => 'Adicionar Despesa')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Despesa adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idfinanceirocontrolesdespesa
     */    
    private function preForm($idfinanceirocontrolesdespesa = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_financeirocontrolesdespesa = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
$idtipo = (int)trim($this->getRequest()->getPost("idtipo"));
$idlocal = (int)trim($this->getRequest()->getPost("idlocal"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$valor = MN_Util::trataNum(trim($this->getRequest()->getPost("valor")));
$consumo = trim($this->getRequest()->getPost("consumo"));
$datavencimento = Mn_Util::stringToTime($this->getRequest()->getPost("datavencimento"));
		
		
		$erros = array();
		
		if (""==$data) array_push($erros, "Informe a Data.");
if (0==$idtipo) array_push($erros, "Informe a Identificação.");
if (""==$datavencimento) array_push($erros, "Informe a Data de vencimento.");

		
		$financeirocontrolesdespesas = new Financeirocontrolesdespesas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["data"] = date("Y-m-d", $data);
			$dados["idtipo"] = $idtipo;
			$dados["idlocal"] = $idlocal;
			$dados["idescola"] = $idescola;
			$dados["valor"] = $valor;
			$dados["consumo"] = $consumo;
			$dados["datavencimento"] = date("Y-m-d", $datavencimento);

						
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $financeirocontrolesdespesas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}