<?php

/**
 * Controle da classe coordenacaosisteses do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_CoordenacaosistesesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Coordenacaosistese
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("coordenacaosisteses", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Coordenacaosisteses();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Síntese de Comportamento e Desempenho excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="coordenacaosisteses") $objs = new Coordenacaosisteses();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaosisteses', 'name' => 'Sínteses de Comportamentos e Desempenhos'),
			array('url' => null,'name' => 'Visualizar Síntese de Comportamento e Desempenho')
		);
		
		$id = (int)$this->_request->getParam("id");
		$coordenacaosisteses = new Coordenacaosisteses();
		$coordenacaosistese = $coordenacaosisteses->getCoordenacaosisteseById($id, array());
		
		if (!$coordenacaosistese) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaosistese;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Sínteses de Comportamentos e Desempenhos')
		);
		
		$ns = new Zend_Session_Namespace('default_coordenacaosisteses');
		$coordenacaosisteses = new Coordenacaosisteses();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
if ($this->view->post_var["iddiretor"]!="") $queries["iddiretor"] = $this->view->post_var["iddiretor"];
if ($this->view->post_var["semestre"]!="") $queries["semestre"] = $this->view->post_var["semestre"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $coordenacaosisteses->getCoordenacaosisteses($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $coordenacaosisteses->getCoordenacaosisteses($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de coordenacaosisteses
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaosisteses', 'name' => 'Sínteses de Comportamentos e Desempenhos'),
			array('url' => null,'name' => 'Editar Síntese de Comportamento e Desempenho')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$coordenacaosisteses = new Coordenacaosisteses();
		$coordenacaosistese = $coordenacaosisteses->getCoordenacaosisteseById($id);
		
		if (!$coordenacaosistese) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaosistese;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($coordenacaosistese);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Síntese de Comportamento e Desempenho editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de coordenacaosisteses 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaosisteses', 'name' => 'Sínteses de Comportamentos e Desempenhos'),
			array('url' => null,'name' => 'Adicionar Síntese de Comportamento e Desempenho')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Síntese de Comportamento e Desempenho adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idcoordenacaosistese
     */    
    private function preForm($idcoordenacaosistese = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_coordenacaosistese = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$idserie = (int)trim($this->getRequest()->getPost("idserie"));
$idescolavinculo = (int)trim($this->getRequest()->getPost("idescolavinculo"));
$iddiretor = (int)trim($this->getRequest()->getPost("iddiretor"));
$idcoordenador = (int)trim($this->getRequest()->getPost("idcoordenador"));
$semestre = trim($this->getRequest()->getPost("semestre"));
$idtipocomportamento = (int)trim($this->getRequest()->getPost("idtipocomportamento"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
if (0==$idserie) array_push($erros, "Informe a Série.");
if (0==$idescolavinculo) array_push($erros, "Informe a Turma.");
if (0==$iddiretor) array_push($erros, "Informe a Diretor.");
if (0==$idcoordenador) array_push($erros, "Informe a Coordenador.");
if (""==$semestre) array_push($erros, "Informe a Semestre.");
if (0==$idtipocomportamento) array_push($erros, "Informe a Tipo de comportamento e Desempenho.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$coordenacaosisteses = new Coordenacaosisteses();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["sequencial"] = $sequencial;
$dados["datalancamento"] = date("Y-m-d", $datalancamento);
$dados["horalancamento"] = $horalancamento;
$dados["idusuariologado"] = $idusuariologado;
$dados["idescola"] = $idescola;
$dados["idserie"] = $idserie;
$dados["idescolavinculo"] = $idescolavinculo;
$dados["iddiretor"] = $iddiretor;
$dados["idcoordenador"] = $idcoordenador;
$dados["semestre"] = $semestre;
$dados["idtipocomportamento"] = $idtipocomportamento;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $coordenacaosisteses->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}