<?php

/**
 * Controle da classe logisticasfornecedores do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_LogisticasfornecedoresController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Logisticafornecedor
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("logisticasfornecedores", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Logisticasfornecedores();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Fornecedor excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="logisticasfornecedores") $objs = new Logisticasfornecedores();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticasfornecedores', 'name' => 'Fornecedores'),
			array('url' => null,'name' => 'Visualizar Fornecedor')
		);
		
		$id = (int)$this->_request->getParam("id");
		$logisticasfornecedores = new Logisticasfornecedores();
		$logisticafornecedor = $logisticasfornecedores->getLogisticafornecedorById($id, array());
		
		if (!$logisticafornecedor) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticafornecedor;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Fornecedores')
		);
		
		$ns = new Zend_Session_Namespace('default_logisticasfornecedores');
		$logisticasfornecedores = new Logisticasfornecedores();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["razaosocial"]!="") $queries["razaosocial"] = $this->view->post_var["razaosocial"];
if ($this->view->post_var["sobrenomefantasia"]!="") $queries["sobrenomefantasia"] = $this->view->post_var["sobrenomefantasia"];
if ($this->view->post_var["cnpj"]!="") $queries["cnpj"] = $this->view->post_var["cnpj"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $logisticasfornecedores->getLogisticasfornecedores($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $logisticasfornecedores->getLogisticasfornecedores($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de logisticasfornecedores
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticasfornecedores', 'name' => 'Fornecedores'),
			array('url' => null,'name' => 'Editar Fornecedor')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$logisticasfornecedores = new Logisticasfornecedores();
		$logisticafornecedor = $logisticasfornecedores->getLogisticafornecedorById($id);
		
		if (!$logisticafornecedor) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticafornecedor;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($logisticafornecedor);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Fornecedor editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de logisticasfornecedores 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticasfornecedores', 'name' => 'Fornecedores'),
			array('url' => null,'name' => 'Adicionar Fornecedor')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Fornecedor adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idlogisticafornecedor
     */    
    private function preForm($idlogisticafornecedor = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_logisticafornecedor = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$razaosocial = trim($this->getRequest()->getPost("razaosocial"));
		$sobrenomefantasia = trim($this->getRequest()->getPost("sobrenomefantasia"));
		$cnpj = trim($this->getRequest()->getPost("cnpj"));
		$telefone = trim($this->getRequest()->getPost("telefone"));
	 

$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$razaosocial) array_push($erros, "Informe a Razão Social.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$logisticasfornecedores = new Logisticasfornecedores(); 
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try { 
			
			$enderecosModel = new EnderecoModel($this);
			$endereco = $enderecosModel->getPost( ($_logisticafornecedor) ? (int)$_logisticafornecedor['idendereco'] : 0, "e_idendereco");
			$idendereco = ($endereco) ? $endereco['id'] : 0;	
			$dadosendereco['idendereco'] = $idendereco;
	 
			$dadoslogisticasfornecedores = array();
			$dadoslogisticasfornecedores['id'] = $id;
			
			$dadoslogisticasfornecedores['razaosocial'] = $razaosocial;
			$dadoslogisticasfornecedores['sobrenomefantasia'] = $sobrenomefantasia;
			$dadoslogisticasfornecedores['cnpj'] = $cnpj;
			$dadoslogisticasfornecedores['telefone'] = $telefone;
			$dadoslogisticasfornecedores['idendereco'] = $idendereco;

			$dadoslogisticasfornecedores["status"] = $status; 
			$dadoslogisticasfornecedores['excluido'] = 'nao';
			$dadoslogisticasfornecedores['logusuario'] = $this->_usuario['id'];;
			$dadoslogisticasfornecedores['logdata'] = date('Y-m-d G:i:s');
		 
			$row = $logisticasfornecedores->save($dadoslogisticasfornecedores);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}