<?php

class Admin_UsuarioController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Usuario
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}		
	
	public function indexAction() {
		$this->_redirect('/admin/usuario/meusdados');
	}
	
	
	/**
	 * 
	 * Action de edição de usuários
	 */	
	public function meusdadosAction() {
		$this->view->bread_crumb = array(
			array('url' => null,'name' => 'Meus Dados')
		);	
				
		$id = Usuarios::getUsuario('id');
		$usuarios = new Usuarios();
		$usuario = $usuarios->fetchRow("id=".$id." AND excluido='nao'");
		
		if (!$usuario) 
			$this->_redirect($this->getRequest()->getControllerName());	
		
		
		$this->view->post_var = $usuario->toArray();
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost();
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Usuário editado com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName() . '/meusdados');	
		}		
		return true;		
    }  		
    
    /**
     * Atribui valores ao view
     * @param int $idusuario
     */    
    private function preForm($idusuario = 0) {
    }       
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost() {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = Usuarios::getUsuario('id');
		$nomerazao = strip_tags(trim($this->getRequest()->getPost("nomerazao")));
		$sobrenomefantasia = strip_tags(trim($this->getRequest()->getPost("sobrenomefantasia")));
		$email = strip_tags(trim($this->getRequest()->getPost("email")));
		$senha = strip_tags(trim($this->getRequest()->getPost("senha")));
		
		$erros = array();
			
		if (""==$nomerazao) array_push($erros, 'Preencha o campo NOME DO USUÁRIO.');
		if (!Mn_Util::isValidEmail($email)) array_push($erros, 'O E-MAIL DE ACESSO é inválido.');
		if (($id==0) && (""==$senha)) array_push($erros, 'Preencha o campo SENHA.');
		
		$usuarios = new Usuarios();
		$row = $usuarios->fetchRow("excluido='nao' AND email='$email' AND id<>".$id);
		if ($row) array_push($erros, 'Já existe usuário com esse EMAIL DE ACESSO.');
			
		if (sizeof($erros)>0) return $erros; 
		
		$dados = array(
			'id' => $id,
			'pfpj' => 'pf',
			'nomerazao' => $nomerazao,
			'sobrenomefantasia' => $sobrenomefantasia,
			'email' => $email,
			'excluido' => 'nao',
			'logusuario' => $this->_usuario['id'],
			'logdata' => date('Y-m-d G:i:s'),
		);
		if ($senha!='') $dados['senha'] = $senha;
					
		$usuarios->save($dados);
				
		return "";
    }        

}