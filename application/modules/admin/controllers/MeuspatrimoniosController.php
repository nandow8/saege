<?php

/**
 * Controle da classe patrimonios do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_MeuspatrimoniosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Patrimonio
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("patrimonios", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Patrimonios();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Patrimônio excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="patrimonios") $objs = new Patrimonios();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'meuspatrimonios', 'name' => 'Patrimônios'),
			array('url' => null,'name' => 'Visualizar Patrimônio')
		);
		
		$id = (int)$this->_request->getParam("id");
		$patrimonios = new Patrimonios();
		$patrimonio = $patrimonios->getPatrimonioById($id, array());
		
		if (!$patrimonio) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimonio;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Patrimônios')
		);
		
		$ns = new Zend_Session_Namespace('default_meuspatrimonios');
		$patrimonios = new Patrimonios();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["numeropatrimonio"]!="") $queries["numeropatrimonio"] = $this->view->post_var["numeropatrimonio"];
if ($this->view->post_var["patrimonio"]!="") $queries["patrimonio"] = $this->view->post_var["patrimonio"];
if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $patrimonios->getPatrimonios($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $patrimonios->getPatrimonios($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de patrimonios
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'meuspatrimonios', 'name' => 'Patrimônios'),
			array('url' => null,'name' => 'Editar Patrimônio')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$patrimonios = new Patrimonios();
		$patrimonio = $patrimonios->getPatrimonioById($id);
		
		if (!$patrimonio) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimonio;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($patrimonio);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Patrimônio editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de patrimonios 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'meuspatrimonios', 'name' => 'Patrimônios'),
			array('url' => null,'name' => 'Adicionar Patrimônio')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Patrimônio adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idpatrimonio
     */    
    private function preForm($idpatrimonio = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_patrimonio = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)trim($this->getRequest()->getPost("idsecretaria"));
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$idsetor = (int)trim($this->getRequest()->getPost("idsetor"));
$idtipo = (int)trim($this->getRequest()->getPost("idtipo"));
$idmarca = (int)trim($this->getRequest()->getPost("idmarca"));
$idfornecedor = (int)trim($this->getRequest()->getPost("idfornecedor"));
$origem = trim($this->getRequest()->getPost("origem"));
$iddepartamentosecretaria = (int)trim($this->getRequest()->getPost("iddepartamentosecretaria"));
$idresponsavelsecretaria = (int)trim($this->getRequest()->getPost("idresponsavelsecretaria"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$idresponsavelescola = (int)trim($this->getRequest()->getPost("idresponsavelescola"));
$numeropatrimonio = trim($this->getRequest()->getPost("numeropatrimonio"));
$patrimonio = trim($this->getRequest()->getPost("patrimonio"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$cor = trim($this->getRequest()->getPost("cor"));
$aprovado = trim($this->getRequest()->getPost("aprovado"));
$modelo = trim($this->getRequest()->getPost("modelo"));
$serie = trim($this->getRequest()->getPost("serie"));
$numerolote = trim($this->getRequest()->getPost("numerolote"));
$anofabricacao = trim($this->getRequest()->getPost("anofabricacao"));
$vidautil = trim($this->getRequest()->getPost("vidautil"));
$quantidade = trim($this->getRequest()->getPost("quantidade"));
$aprovacao = trim($this->getRequest()->getPost("aprovacao"));
$statusaprovacao = trim($this->getRequest()->getPost("statusaprovacao"));
$observacoesaprovacoes = trim($this->getRequest()->getPost("observacoesaprovacoes"));
$baixa = trim($this->getRequest()->getPost("baixa"));
$statusbaixa = trim($this->getRequest()->getPost("statusbaixa"));
$observacoesbaixa = trim($this->getRequest()->getPost("observacoesbaixa"));
$roubofurto = trim($this->getRequest()->getPost("roubofurto"));
$statusroubofurto = trim($this->getRequest()->getPost("statusroubofurto"));
$observacoesroubofurto = trim($this->getRequest()->getPost("observacoesroubofurto"));
$dataroubofurto = Mn_Util::stringToTime($this->getRequest()->getPost("dataroubofurto"));
$idarquivoroubofurto = (int)trim($this->getRequest()->getPost("idarquivoroubofurto"));
$depreciacao = trim($this->getRequest()->getPost("depreciacao"));
$descricoesdepreciacao = trim($this->getRequest()->getPost("descricoesdepreciacao"));
$valordepreciacao = MN_Util::trataNum(trim($this->getRequest()->getPost("valordepreciacao")));
$percentualdepreciacao = MN_Util::trataNum(trim($this->getRequest()->getPost("percentualdepreciacao")));
$tipodepreciacao = trim($this->getRequest()->getPost("tipodepreciacao"));
$idfoto = (int)trim($this->getRequest()->getPost("idfoto"));
$caracteristicas = trim($this->getRequest()->getPost("caracteristicas"));
$dimensoes = trim($this->getRequest()->getPost("dimensoes"));
$datacompra = Mn_Util::stringToTime($this->getRequest()->getPost("datacompra"));
$descricoesdobem = trim($this->getRequest()->getPost("descricoesdobem"));
$codigosbarras = trim($this->getRequest()->getPost("codigosbarras"));
$qrcode = trim($this->getRequest()->getPost("qrcode"));
$status = trim($this->getRequest()->getPost("status1"));
$origemaprovacao = trim($this->getRequest()->getPost("origemaprovacao"));
$logdataescola = trim($this->getRequest()->getPost("logdataescola"));
$logusuarioescola = (int)trim($this->getRequest()->getPost("logusuarioescola"));
		
		
		$erros = array();
		
/*		if (0==$idsecretaria) array_push($erros, "Informe a Secretaria.");
if (0==$idsetor) array_push($erros, "Informe a Setor.");
if (0==$idtipo) array_push($erros, "Informe a Tipo.");
if (0==$idescola) array_push($erros, "Informe a Escolas.");
if (0==$idresponsavelescola) array_push($erros, "Informe a Responsável da escola.");*/
if (""==$numeropatrimonio) array_push($erros, "Informe a Nº do patrimônio.");
if (""==$patrimonio) array_push($erros, "Informe a Patrimônio.");
/*if (""==$aprovado) array_push($erros, "Informe a Aprovado.");
if (""==$aprovacao) array_push($erros, "Informe a Aprovação.");
if (""==$statusaprovacao) array_push($erros, "Informe a Status de Aprovação.");*/
if (""==$status) array_push($erros, "Informe a Status.");

		
		$patrimonios = new Patrimonios();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = $idsecretaria;
$dados["iddepartamento"] = $iddepartamento;
$dados["idsetor"] = $idsetor;
$dados["idtipo"] = $idtipo;
$dados["idmarca"] = $idmarca;
$dados["idfornecedor"] = $idfornecedor;
$dados["origem"] = $origem;
$dados["iddepartamentosecretaria"] = $iddepartamentosecretaria;
$dados["idresponsavelsecretaria"] = $idresponsavelsecretaria;
$dados["idescola"] = $idescola;
$dados["idresponsavelescola"] = $idresponsavelescola;
$dados["numeropatrimonio"] = $numeropatrimonio;
$dados["patrimonio"] = $patrimonio;
$dados["descricoes"] = $descricoes;
$dados["cor"] = $cor;
$dados["aprovado"] = $aprovado;
$dados["modelo"] = $modelo;
$dados["serie"] = $serie;
$dados["numerolote"] = $numerolote;
$dados["anofabricacao"] = $anofabricacao;
$dados["vidautil"] = $vidautil;
$dados["quantidade"] = $quantidade;
$dados["aprovacao"] = $aprovacao;
$dados["statusaprovacao"] = $statusaprovacao;
$dados["observacoesaprovacoes"] = $observacoesaprovacoes;
$dados["baixa"] = $baixa;
$dados["statusbaixa"] = $statusbaixa;
$dados["observacoesbaixa"] = $observacoesbaixa;
$dados["roubofurto"] = $roubofurto;
$dados["statusroubofurto"] = $statusroubofurto;
$dados["observacoesroubofurto"] = $observacoesroubofurto;
$dados["dataroubofurto"] = date("Y-m-d", $dataroubofurto);

$idarquivoroubofurto = $this->getArquivo('idarquivoroubofurto');
			if ($idarquivoroubofurto!=0) $dados['idarquivoroubofurto'] = $idarquivoroubofurto;
$dados["depreciacao"] = $depreciacao;
$dados["descricoesdepreciacao"] = $descricoesdepreciacao;
$dados["valordepreciacao"] = $valordepreciacao;
$dados["percentualdepreciacao"] = $percentualdepreciacao;
$dados["tipodepreciacao"] = $tipodepreciacao;

$idfoto = $this->getImagem('idfoto');
			if ($idfoto!=0) $dados['idfoto'] = $idfoto;
$dados["caracteristicas"] = $caracteristicas;
$dados["dimensoes"] = $dimensoes;
$dados["datacompra"] = date("Y-m-d", $datacompra);
$dados["descricoesdobem"] = $descricoesdobem;
$dados["codigosbarras"] = $codigosbarras;
$dados["qrcode"] = $qrcode;
$dados["status"] = $status;
$dados["origemaprovacao"] = $origemaprovacao;
$dados["logdataescola"] = $logdataescola;
$dados["logusuarioescola"] = $logusuarioescola;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $patrimonios->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}