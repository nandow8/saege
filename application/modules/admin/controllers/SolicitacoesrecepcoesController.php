<?php

/**
 * Controle da classe protocolos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_SolicitacoesrecepcoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Protocolo
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("solicitacoesrecepcoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
	
	
		$this->_departamento = false;
		$this->view->departamento = array();
		$this->_urldepartamento = "";

		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Protocolos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Protocolo excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="solicitacoesrecepcoes") $objs = new Protocolos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'solicitacoesrecepcoes'."/index" . $this->_urldepartamento, 'name' => 'Protocolos'),
			array('url' => null,'name' => 'Visualizar Protocolo')
		);
		
		$id = (int)$this->_request->getParam("id");
		$protocolos = new Protocolos();
		$protocolo = $protocolos->getProtocoloById($id, array());
		
		if (!$protocolo) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName()."/index" . $this->_urldepartamento);
		
		$this->view->post_var = $protocolo;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Protocolos')
		);
		$idd = (int)$this->_request->getParam("idd");

		$ns = new Zend_Session_Namespace('admin_solicitacoesrecepcoes');
		$protocolos = new Protocolos();
		$queries = array();	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );	
		$queries["origem_destino"] = "Secretaria";
		$queries["modulo"] = "Recepção";

		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName()."/index" . $this->_urldepartamento);
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idusuariocriacao"]!="") $queries["idusuariocriacao"] = $this->view->post_var["idusuariocriacao"];
if ($this->view->post_var["iddepartamentosecretaria"]!="") $queries["iddepartamentosecretaria"] = $this->view->post_var["iddepartamentosecretaria"];
if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["numeroprotocolo"]!="") $queries["numeroprotocolo"] = $this->view->post_var["numeroprotocolo"];
if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $protocolos->getProtocolos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $protocolos->getProtocolos($queries, $paginaAtual, $maxpp);	
		
	}
	
	/**
	 * 
	 * Action de edição de protocolos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'solicitacoesrecepcoes'."/index" . $this->_urldepartamento, 'name' => 'Protocolos'),
			array('url' => null,'name' => 'Editar Protocolo')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$protocolos = new Protocolos();
		$protocolo = $protocolos->getProtocoloById($id);
		
		if (!$protocolo) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName()."/index" . $this->_urldepartamento);

		$solicitacoes = new Solicitacoes();
		$this->view->historicos = $solicitacoes->getSolicitacoes(array('idprotocolo'=>$id, 'order'=>'ORDER BY s1.id ASC'));

		$ultimasolicitacao = $solicitacoes->getUltimaSolicitacaoByIdprotocolo($id);
		if(isset($ultimasolicitacao['iddepartamentosecretaria'])){
			$protocolo['iddepartamentosecretaria'] = $ultimasolicitacao['iddepartamentosecretaria'];
			$protocolo['idusuarioresponsavel'] = $ultimasolicitacao['idusuarioresponsavel'];
			$protocolo['status'] = $ultimasolicitacao['status'];			
		}

		$this->view->post_var = $protocolo;


		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($protocolo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Protocolo editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName()."/index" . $this->_urldepartamento);
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de protocolos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'solicitacoesrecepcoes'."/index" . $this->_urldepartamento, 'name' => 'Protocolos'),
			array('url' => null,'name' => 'Adicionar Protocolo')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Protocolo adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName()."/index" . $this->_urldepartamento);
		}
		
		return true;		
    }	
    
	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function setdepartamentosescolasAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$escolas = Escolas::getEscolaByIdHelper($idescola);
		if(!$idescola)  return $this->view->rows = false;

		$departamentos = new Departamentosescolas();
		$this->view->rows = $departamentos->getDepartamentosescolas(array('idescola'=>$idescola));
		
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function setescolausuarioresponsavelAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$iddepartamentoescola = (int)$this->_request->getPost("iddepartamento");

		if((int)$idescola <= 0) return $this->view->rows = false;

		$rows = new Escolasusuarios();
		$queries = array();	
		$queries['idescola'] = $idescola;
		$queries['iddepartamentoescola'] = $iddepartamentoescola;
		$this->view->rows = $rows->getEscolasusuarios($queries);	
		
	}

    /**
     * Atribui valores ao view
     * @param int $idprotocolo
     */    
    private function preForm($idprotocolo = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_protocolo = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idusuariocriacao = (int)trim($this->getRequest()->getPost("idusuariocriacao"));
$iddepartamentosecretaria = (int)trim($this->getRequest()->getPost("iddepartamentosecretaria"));
$idusuarioresponsavel = (int)trim($this->getRequest()->getPost("idusuarioresponsavel"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$iddepartamentoescola = (int)trim($this->getRequest()->getPost("iddepartamentoescola"));
$idescolausuariocriacao = (int)trim($this->getRequest()->getPost("idescolausuariocriacao"));
$idescolausuarioresponsavel = (int)trim($this->getRequest()->getPost("idescolausuarioresponsavel"));
$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
$numeroprotocolo = trim($this->getRequest()->getPost("numeroprotocolo"));


$titulo = trim($this->getRequest()->getPost("titulo"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$nome = trim($this->getRequest()->getPost("nome"));
$rg = trim($this->getRequest()->getPost("rg"));
$origem = trim($this->getRequest()->getPost("origem"));
$destinatario = trim($this->getRequest()->getPost("destinatario"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
//		if (""==$numeroprotocolo) array_push($erros, "Informe a N° do Protocolo.");
if (""==$titulo) array_push($erros, "Informe a Título.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$protocolos = new Protocolos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {

			if((int)$id > 0){
				$numeroprotocolo = "PROTOC" . str_pad($id, 4, "0", STR_PAD_LEFT);
			}

			$dados = array();
			$dados['id'] = $id;
			$dados['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
			$dados["idusuariocriacao"] = Usuarios::getUsuario('id');
$dados["iddepartamentosecretaria"] = $iddepartamentosecretaria;
$dados["idusuarioresponsavel"] = $idusuarioresponsavel;
$dados["idescola"] = $idescola;
$dados["iddepartamentoescola"] = $iddepartamentoescola;
$dados["idescolausuariocriacao"] = $idescolausuariocriacao;
$dados["idescolausuarioresponsavel"] = $idescolausuarioresponsavel;
$dados["idaluno"] = $idaluno;
$dados["idarquivo"] = $idarquivo;
$dados["numeroprotocolo"] = $numeroprotocolo;
$dados["titulo"] = $titulo;
$dados["descricoes"] = $descricoes;
$dados["observacoes"] = $observacoes;
$dados["nome"] = $nome;
$dados["rg"] = $rg;
$dados["origem"] = "Secretaria";
$dados["destinatario"] = "Escola";
$dados["modulo"] = "Recepção";
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $protocolos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}