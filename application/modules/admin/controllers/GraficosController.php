<?php

class Admin_GraficosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var funcionarios
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("graficos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}	
	
   public function setgraficoslocacoesAction(){
   		$this->_helper->layout->disableLayout();
		/*LOCALIZACOES*/
		$rows = new Unidades();	
		$this->view->rows = $rows->getUnidades();
			
    }
		
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Gráficos')
		);
		
		$solicitacoes = new Protocolos();
		$queries_solicitacoes = array();	
		$queries_solicitacoes['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );	
		$queries_solicitacoes["origem_destino"] = "Secretaria";
		$queries_solicitacoes["modulo"] = "Recepção";
		$queries_solicitacoes['total'] = true;;
		
		$queries_solicitacoes['status'] = "Aberto";
		$this->view->solicitacoespendente = $solicitacoes->getProtocolos($queries_solicitacoes);

		$queries_solicitacoes['status'] = "Analisando";
		$this->view->solicitacoesexecucao = $solicitacoes->getProtocolos($queries_solicitacoes);
		
		$queries_solicitacoes['status'] = "Encerrado";
		$this->view->solicitacoesencerrado = $solicitacoes->getProtocolos($queries_solicitacoes);
		
		$queries_solicitacoes['status'] = "Cancelado";
		$this->view->solicitacoescancelado = $solicitacoes->getProtocolos($queries_solicitacoes);

    	$ouvidorias = new Ouvidorias();
    	$queriesouvidoria = array();	
		
		if(!Mn_Util::isAccess("ouvidoriatodos", "visualizar")){
			$iddepartamento = (int)Usuarios::getUsuario("iddepartamento");
			if((Usuarios::getUsuario("master")!="sim") && ($iddepartamento > 0)):
				$queriesouvidoria['iddepartamento'] = (int)$iddepartamento;
			elseif (Usuarios::getUsuario("master")!="sim"):
				$queriesouvidoria['iddepartamento'] = -1;
			endif;			
		}
		
		$queriesouvidoria['total'] = true;
		$queriesouvidoria['status'] = "Aberto";
		$this->view->ouvidoriaabertos = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		$queriesouvidoria['status'] = "Pendente";
		$this->view->ouvidoriapendente = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		$queriesouvidoria['status'] = "Encerrado";
		$this->view->ouvidoriaencerrado = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		$queriesouvidoria['status'] = "Cancelado";
		$this->view->ouvidoriacancelado = $ouvidorias->getOuvidorias($queriesouvidoria);


		$solicitacoes = new Protocolos();
		$querieschamados = array();	
		$querieschamados['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );	
		$querieschamados["origem_destino"] = "Secretaria";
		$querieschamados["modulo"] = "Ti Chamados";

		$querieschamados['status'] = "Aberto";
		$this->view->chamadostecnicosabertos = $solicitacoes->getProtocolos($querieschamados);

		$querieschamados['status'] = "Analisando";
		$this->view->chamadostecnicospendente = $solicitacoes->getProtocolos($querieschamados);
		
		$querieschamados['status'] = "Cancelado";
		$this->view->chamadostecnicoscancelado = $solicitacoes->getProtocolos($querieschamados);
		
		$querieschamados['status'] = "Encerrado";
		$this->view->chamadostecnicosencerrado = $solicitacoes->getProtocolos($querieschamados);

		$solicitacoes = new Protocolos();
		$queriesengenharias = array();	
		$queriesengenharias['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );	
		$queriesengenharias["origem_destino"] = "Secretaria";
		$queriesengenharias["modulo"] = "Engenharia Chamados";

		$queriesengenharias['status'] = "Aberto";
		$this->view->chamadosengenhariasabertos = $solicitacoes->getProtocolos($queriesengenharias);

		$queriesengenharias['status'] = "Analisando";
		$this->view->chamadosengenhariaspendente = $solicitacoes->getProtocolos($queriesengenharias);
		
		$queriesengenharias['status'] = "Cancelado";
		$this->view->chamadosengenhariascancelado = $solicitacoes->getProtocolos($queriesengenharias);
		
		$queriesengenharias['status'] = "Encerrado";
		$this->view->chamadosengenhariasencerrado = $solicitacoes->getProtocolos($queriesengenharias);


		$cursos = new Cursos();
		$queriescursos = array();	
		$queriescursos['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queriescursos['origem'] = "Coordenacao";
		$this->view->cursos = $cursos->getCursos($queriescursos, 0, 5);

		$comunicacaocomunicados = new Comunicacaocomunicados();
		$queriescomunicacaocomunicados = array();	
		$queriescomunicacaocomunicados['status'] = "Ativo";
		$queriescomunicacaocomunicados['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$this->view->comunicados = $comunicacaocomunicados->getComunicacaocomunicados($queriescomunicacaocomunicados, 0, 5);	

		/*
		$ns = new Zend_Session_Namespace('admin_funcionarios');
		$funcionarios = new Funcionarios();
		$queries = array();	
                $queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		//if ((isset($this->view->post_var['bolsista'])) && ($this->view->post_var['bolsista']!='')) $queries['bolsista'] = $this->view->post_var['bolsista'];
    		//if ((isset($this->view->post_var['status'])) && ($this->view->post_var['status']!='')) $queries['status'] = $this->view->post_var['status'];
    		//if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		
    	}		
		$solicitacoes = new Solicitacoes();
		$queries_solicitacoes = array();	
		if($this->_usuario['secretaria']=="Sim"):
			$queries_solicitacoes['idusuarioretorno'] = $this->_usuario['id'];
		else: 
			//var_dump($this->_usuario); die();
			if($this->_usuario['master']!="sim"):
				$queries_solicitacoes['idusuarioretorno_todos'] = $this->_usuario['id'];
			endif;
		endif;
		
		$queries_solicitacoes['total'] = true;;
		
		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_PENDENTE;
		$this->view->solicitacoespendente = $solicitacoes->getSolicitacoes($queries_solicitacoes);

		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_EXECUCAO;
		$this->view->solicitacoesexecucao = $solicitacoes->getSolicitacoes($queries_solicitacoes);
		
		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_FINALIZADO;
		$this->view->solicitacoesencerrado = $solicitacoes->getSolicitacoes($queries_solicitacoes);
		
		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_CANCELADO;
		$this->view->solicitacoescancelado = $solicitacoes->getSolicitacoes($queries_solicitacoes);
		//var_dump($this->view->solicitacoescancelado); die();
		
		
    	$ouvidorias = new Ouvidoria();
    	$queriesouvidoria = array();	
		
		if(!Mn_Util::isAccess("ouvidoriatodos", "visualizar")){
			$iddepartamento = (int)Usuarios::getUsuario("iddepartamento");
			if((Usuarios::getUsuario("master")!="sim") && ($iddepartamento > 0)):
				$queriesouvidoria['iddepartamento'] = (int)$iddepartamento;
			elseif (Usuarios::getUsuario("master")!="sim"):
				$queriesouvidoria['iddepartamento'] = -1;
			endif;			
		}
		
		$queriesouvidoria['total'] = true;
		$queriesouvidoria['status'] = Ouvidoria::$_STATUS_ABERTO;
		$this->view->ouvidoriaabertos = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		$queriesouvidoria['status'] = Ouvidoria::$_STATUS_PENDENTE;
		$this->view->ouvidoriapendente = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		$queriesouvidoria['status'] = Ouvidoria::$_STATUS_ENCERRADO;
		$this->view->ouvidoriaencerrado = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		$queriesouvidoria['status'] = Ouvidoria::$_STATUS_CANCELADO;
		$this->view->ouvidoriacancelado = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		
		$patrimonios = new Patrimonios_Patrimonios();
		$queriespatrimonios = array();	
		
		$queriespatrimonios['total'] = true;
		$queriespatrimonios['aprovacao'] = "sim";
		$this->view->patrimoniosaprovados = $patrimonios->getPatrimonios($queriespatrimonios);;
		
		$queriespatrimonios['aprovacao'] = "nao";
		$this->view->patrimoniosnaoaprovados = $patrimonios->getPatrimonios($queriespatrimonios);
		
		$solicitacoes = new Solicitacoes();
		$queries_solicitacoes = array();	
		if($this->_usuario['secretaria']=="Sim"):
			$queries_solicitacoes['idusuarioretorno'] = $this->_usuario['id'];
		else: 
			//var_dump($this->_usuario); die();
			if($this->_usuario['master']!="sim"):
				$queries_solicitacoes['idusuarioretorno_todos'] = $this->_usuario['id'];
			endif;
		endif;
		
		$queries_solicitacoes['total'] = true;;
		
		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_PENDENTE;
		$this->view->solicitacoespendente = $solicitacoes->getSolicitacoes($queries_solicitacoes);

		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_EXECUCAO;
		$this->view->solicitacoesexecucao = $solicitacoes->getSolicitacoes($queries_solicitacoes);
		
		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_FINALIZADO;
		$this->view->solicitacoesencerrado = $solicitacoes->getSolicitacoes($queries_solicitacoes);
		
		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_CANCELADO;
		$this->view->solicitacoescancelado = $solicitacoes->getSolicitacoes($queries_solicitacoes);
		//var_dump($this->view->solicitacoescancelado); die();
		
		
    	$ouvidorias = new Ouvidoria();
    	$queriesouvidoria = array();	
		
		if(!Mn_Util::isAccess("ouvidoriatodos", "visualizar")){
			$iddepartamento = (int)Usuarios::getUsuario("iddepartamento");
			if((Usuarios::getUsuario("master")!="sim") && ($iddepartamento > 0)):
				$queriesouvidoria['iddepartamento'] = (int)$iddepartamento;
			elseif (Usuarios::getUsuario("master")!="sim"):
				$queriesouvidoria['iddepartamento'] = -1;
			endif;			
		}
		
		$queriesouvidoria['total'] = true;
		$queriesouvidoria['status'] = Ouvidoria::$_STATUS_ABERTO;
		$this->view->ouvidoriaabertos = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		$queriesouvidoria['status'] = Ouvidoria::$_STATUS_PENDENTE;
		$this->view->ouvidoriapendente = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		$queriesouvidoria['status'] = Ouvidoria::$_STATUS_ENCERRADO;
		$this->view->ouvidoriaencerrado = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		$queriesouvidoria['status'] = Ouvidoria::$_STATUS_CANCELADO;
		$this->view->ouvidoriacancelado = $ouvidorias->getOuvidorias($queriesouvidoria);
		
		
		$patrimonios = new Patrimonios_Patrimonios();
		$queriespatrimonios = array();	
		
		$queriespatrimonios['total'] = true;
		$queriespatrimonios['aprovacao'] = "sim";
		$this->view->patrimoniosaprovados = $patrimonios->getPatrimonios($queriespatrimonios);;
		
		$queriespatrimonios['aprovacao'] = "nao";
		$this->view->patrimoniosnaoaprovados = $patrimonios->getPatrimonios($queriespatrimonios);
		
        $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
		$queriesalmoxarifadoprodutos = array('idsecretaria' => $this->view->idsecretaria);
		$produtos = new Almoxarifado_Produtos();	
		$this->view->almoxarifadoprodutos = $produtos->getProdutos($queriesalmoxarifadoprodutos);
                    
		
		$chamadostecnicos = new Ti_Chamados();
		$querieschamadostecnicos = array();	

		$visualizar_todos = Mn_Util::isAccess("chamadosrespostas", 'visualizar');
		if(!$visualizar_todos) $querieschamadostecnicos['idusuario'] = $this->_usuario['id'];
		
		$querieschamadostecnicos['total'] = true;
		$querieschamadostecnicos['status'] = Ti_Chamados::$_STATUS_ABERTO;
		$this->view->chamadostecnicosabertos = $chamadostecnicos->getChamados($querieschamadostecnicos);;
		
		$querieschamadostecnicos['status'] = Ti_Chamados::$_STATUS_PENDENTE;
		$this->view->chamadostecnicospendente = $chamadostecnicos->getChamados($querieschamadostecnicos);
		
		$querieschamadostecnicos['status'] = Ti_Chamados::$_STATUS_ENCERRADO;
		$this->view->chamadostecnicosencerrado = $chamadostecnicos->getChamados($querieschamadostecnicos);
		
		$querieschamadostecnicos['status'] = Ti_Chamados::$_STATUS_CANCELADO;
		$this->view->chamadostecnicoscancelado = $chamadostecnicos->getChamados($querieschamadostecnicos);
		
		
		$rows = new Unidades();	
		$this->view->unidades = $rows->getUnidades();*/
	}
	
	public function setgraficoslogisticafrotasAction(){
		//$this->_helper->layout->setLayout('modal');	
		$this->_helper->layout->disableLayout();
		$tipo = $this->_request->getParam("tipo");
		$this->view->tipo = $tipo;	

        $ns = new Zend_Session_Namespace('admin_frotas');
        $frotas = new Logisticafrotas();
        $queries = array();
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();
        }
        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {

            foreach ($this->view->post_var as $k => $v) $this->view->post_var[$k] = trim($v);
            if ((isset($this->view->post_var['status'])) && ($this->view->post_var['status'] != '')) $queries['status'] = $this->view->post_var['status'];
            if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave'] != '')) $queries['chave'] = $this->view->post_var['chave'];
        }

		$rows = new Logisticafabricantes();	
		$this->view->rows = $rows->getFabricantes($queries);
		
	}
}