<?php

/**
 * Controle da classe usuários do sistema
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PerfisController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Usuario
     */
    protected $_usuario = null;

    /**
     * Verifica a permissão de acessos, inicia o usuário e verifica mensagens de transação
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("perfis", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();

        $id = (int) $this->_request->getPost("id");

        $usuarios = new Usuarios();
        $perfis = $usuarios->fetchAll("excluido='nao' AND idperfil=$id");

        if (sizeof($perfis) > 0) {
            die("O Perfil não pode ser excluído pois está sendo usado por " . sizeof($perfis) . " usuário(s)");
        }

        $rows = new UsuariosPerfis();
        $row = $rows->fetchRow("id=" . $id);
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Perfil excluído com sucesso.";

            die('OK');
        }

        die('Não encontrado!');
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        if ($id == Usuarios::getUsuario('idperfil')) {
            die('Você não pode mudar seu próprio perfil');
        }
        $op = $this->getRequest()->getPost("op");

        if ($op == "perfis")
            $objs = new UsuariosPerfis();
        $obj = $objs->fetchRow("excluido='nao' AND id=" . $id);
        if ($obj) {

            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = Usuarios::getUsuario('id');
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Lista dos perfis de acesso
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => null, 'name' => 'Perfis de acesso')
        );



        $ns = new Zend_Session_Namespace('admin_perfis');
        $rows = new UsuariosPerfis();
        $queries = array();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
        }
        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {

            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);
            if ($this->view->post_var['status1'] != '')
                $queries['status'] = $this->view->post_var['status1'];
            if ($this->view->post_var['chave'] != '')
                $queries['chave'] = $this->view->post_var['chave'];
            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $rows->getPerfis($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;


        $this->view->rows = $rows->getPerfis($queries, $paginaAtual, $maxpp);
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'perfis', 'name' => 'Perfis de acesso'),
            array('url' => null, 'name' => 'Visualizar perfil')
        );

        $id = (int) $this->_request->getParam("id");
        $perfis = new UsuariosPerfis();
        $perfil = $perfis->fetchRow("id=" . $id . " AND excluido='nao'");

        if (!$perfil)
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());


        $this->view->post_var = $perfil->toArray();
        $this->preForm($id);

        $this->view->visualizar = true;
        return true;
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'perfis', 'name' => 'Perfis de acesso'),
            array('url' => null, 'name' => 'Editar perfil')
        );

        $id = (int) $this->_request->getParam("id");
        $perfis = new UsuariosPerfis();
        $perfil = $perfis->fetchRow("id=" . $id . " AND excluido='nao'");

        if (!$perfil)
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());


        $this->view->post_var = $perfil->toArray();
        $this->preForm($id);

        if ($this->_request->isPost()) {
            $erros = $this->getPost();
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Perfil editado com sucesso.";

            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        }
        return true;
    }

    /**
     *
     * Action de adição de pefis de acesso.
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'perfis', 'name' => 'Perfis de acesso'),
            array('url' => null, 'name' => 'Adicionar perfil')
        );


        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost();
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Perfil adicionado com sucesso.";

            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        }
        return true;
    }

    /**
     *
     * Atribui valores ao view
     * @param int $idperfil
     */
    private function preForm($idperfil = 0) {
        $perfis = new UsuariosPerfis();
        $this->view->limites = $perfis->getLimitesByIdperfil($idperfil);
    }

    /**
     *
     * Valida e grava os dados do formulário
     */
    private function getPost() {
        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $perfil = trim($this->getRequest()->getPost("perfil"));
        $individualgeral = trim($this->getRequest()->getPost("individualgeral"));
        $status = trim($this->getRequest()->getPost("status1"));
        $visualizar = $this->getRequest()->getPost("visualizar");
        $adicionar = $this->getRequest()->getPost("adicionar");
        $editar = $this->getRequest()->getPost("editar");
        $excluir = $this->getRequest()->getPost("excluir");
        $recebeprotocolo = trim($this->getRequest()->getPost("recebeprotocolo"));

        $erros = array();

        if ("" == $perfil)
            array_push($erros, 'Preencha o campo NOME DO PERFIL.');
        if ("" == $status)
            array_push($erros, 'Selecione um STATUS.');
        if ("" == $recebeprotocolo)
            array_push($erros, 'Preencha o campo Recebimento de Protocolo.');

        $perfis = new UsuariosPerfis();
        $row = $perfis->fetchRow("excluido='nao' AND perfil='$perfil' AND id<>" . $id);
        if ($row)
            array_push($erros, 'Já existe perfil com esse NOME.');

        if (sizeof($erros) > 0)
            return $erros;

        $dados = array(
            'id' => $id,
            'perfil' => $perfil,
            'status' => $status,
            'individualgeral' => $individualgeral,
            'excluido' => 'nao',
            'recebeprotocolo' => $recebeprotocolo,
            'logusuario' => $this->_usuario['id'],
            'logdata' => date('Y-m-d G:i:s'),
            'limites' => array(
                'visualizar' => $visualizar,
                'adicionar' => $adicionar,
                'editar' => $editar,
                'excluir' => $excluir,
            )
        );



        $perfis = $perfis->save($dados);

        return "";
    }

}