<?php

/**
 * Controle da classe pontosmensais do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PontodefinehorarioController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pontosmensal
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("pontosmensais", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Funcionariosgeraispontodefinicaohorarios();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Bloco de Horário excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
                
                
		if ($op=="pontodefinehorario") $objs = new Funcionariosgeraispontodefinicaohorarios;
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'pontosmensais', 'name' => 'Pontos mensais'),
			array('url' => null,'name' => 'Visualizar Ponto mensal')
		);
		
		$id = (int)$this->_request->getParam("id");
		$pontosmensais = new Pontosmensais();
		$pontosmensal = $pontosmensais->getPontosmensalById($id, array());
		
		if (!$pontosmensal) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $pontosmensal;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction(){
           
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Pontos mensais')
		);
                
		$ns = new Zend_Session_Namespace('default_pontosmensais');
		$horariosDefinidos = new Funcionariosgeraispontodefinicaohorarios();
		$queries = array();
                $queries['excluido'] = "nao";
		//$queries['origem'] = "Secretaria";
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
       
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
         
        
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
                
                if ($this->view->post_var["idlocal"]!="") $queries["idlocal"] = $this->view->post_var["idlocal"];
    		
//			if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
//if ($this->view->post_var["dia_i"]!="") $queries["dia_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["dia_i"]));
//if ($this->view->post_var["dia_f"]!="") $queries["dia_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["dia_f"]));
//if ($this->view->post_var["diasemana"]!="") $queries["diasemana"] = $this->view->post_var["diasemana"];
//if ($this->view->post_var["observacoes"]!="") $queries["observacoes"] = $this->view->post_var["observacoes"];
//if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 10;
	
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
                
		$queries['total'] = true;
                $queries['sorting'] = "";
                
		$totalRegistros = $horariosDefinidos->getFuncionariosgeraispontodefinicaohorarios($queries);
                
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
                
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
                //print_r($pontosmensais->getAtas($queries, $paginaAtual, $maxpp));
                //die();
                $this->view->idPost = $this->getRequest()->getPost("id");
               
		$this->view->rows = $horariosDefinidos->getFuncionariosgeraispontodefinicaohorarios($queries, $paginaAtual, $maxpp);
                
               
        }
	
	/**
	 * 
	 * Action de edição de pontosmensais
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'pontodefinehorario', 'name' => 'Pontos definição de horários'),
			array('url' => null,'name' => 'Editar Ponto mensal')
		);
               
		$id = (int)$this->_request->getParam("id");
		$horariosDefinidos = new Funcionariosgeraispontodefinicaohorarios();
		$rowHorarios = $horariosDefinidos->getfuncionariosgeraispontodefinicaohorariosById($id);
				
                
		if (!$rowHorarios) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		$this->preForm();
		//$this->view->post_var["nomex"] = "aaa";
		if ($this->_request->isPost()) {
			$erros = $this->getPost($rowHorarios);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ponto mensal editado com sucesso.";
                        
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
                
                $this->view->idPost = $this->_request->getParam("id");
                //$this->view->id = $pontosmensal['idfuncionario'];
                //$this->view->nomefuncionario = $pontosmensal['idfuncionario'];
                //$this->view->funcionarioGeral = $rowHorarios->getFuncionariosgeraispontodefinicaohorariosById($id);
                $this->view->blocoHorario = $rowHorarios;
                
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de pontosmensais 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'pontodefinehorario', 'name' => 'Pontos mensais'),
			array('url' => null,'name' => 'Adicionar Ponto mensal')
		);
                
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
                        
                        
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ponto mensal adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idpontosmensal
     */    
    private function preForm($idpontosmensal = 0){
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_pontodefinehorario = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = $this->getRequest()->getPost("id");
                
 /*               
    $idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
    $dia = Mn_Util::stringToTime($this->getRequest()->getPost("dia"));
    $diasemana = trim($this->getRequest()->getPost("diasemana"));
    $horaentrada = trim($this->getRequest()->getPost("horaentrada"));
    $horasaida = trim($this->getRequest()->getPost("horasaida"));
    $observacoes = trim($this->getRequest()->getPost("observacoes"));
    $status = trim($this->getRequest()->getPost("status1"));
*/
$idOrigins = $this->getRequest()->getPost("idOrigins");                
$rotulohorario = $this->getRequest()->getPost("rotulohorario");
$horarioentrada = $this->getRequest()->getPost("horarioentrada");
$pausa = $this->getRequest()->getPost("pausa");
$horariosaida = $this->getRequest()->getPost("horariosaida");
$horariopausaentrada = $this->getRequest()->getPost("horariopausaentrada");
$horariopausasaida = $this->getRequest()->getPost("horariopausasaida");
$status = trim($this->getRequest()->getPost("status1"));
		$erros = array();
/*		
		if (0==$idfuncionario) array_push($erros, "Informe a Funcionário.");
                if (""==$dia) array_push($erros, "Informe a Dia.");
                if (""==$diasemana) array_push($erros, "Informe a Dia da semana.");
                if (""==$status) array_push($erros, "Informe a Status.");
*/
		
		 
                
		$pontosHorariosDefinidos  = new Funcionariosgeraispontodefinicaohorarios();
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
                        
                        $dados['id'] = $idOrigins;
                        //rotulohorario
                        $dados['rotulohorario'] = $rotulohorario;
                        //hora_entrada
                        $dados['hora_entrada'] = $horarioentrada;
                        //hora_saida
						$dados['hora_saida'] = $horariosaida;
						if($pausa == 'on'):
							$dados['pausa'] = 'sim';
							//hora_pausa_entrada
							$dados['hora_pausa_entrada'] = $horariopausaentrada;
							//hora_pausa_saida
							$dados['hora_pausa_saida'] = $horariopausasaida;
						else: 
							$dados['pausa'] = 'nao';
							//hora_pausa_entrada
							$dados['hora_pausa_entrada'] = $horarioentrada;
							//hora_pausa_saida
							$dados['hora_pausa_saida'] = $horarioentrada;
						endif;
                        
                        // Bloco Padrão
                        $dados["status"] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

			$row = $pontosHorariosDefinidos->save($dados);
                        
			$db->commit();
                        
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
    
	
	
    
}