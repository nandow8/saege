<?php

/**
 * Controle da index (Admin)
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_IndexController extends Zend_Controller_Action {

    protected $_usuario = null;

    /**
     * Verificação de Permissao de Acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        $this->_usuario = unserialize($loginNameSpace->usuario);
    }

    public function importarAction() {
        $sourceFilename = BASE_PATH . "/_apoio/cep aruja 08_08_16.xlsx";
        $objPHPExcel = PHPExcel_IOFactory::load($sourceFilename);

        if ($objPHPExcel->getSheetCount() == 0)
            return 'Arquivo inválido (deve pelo menos um WorkSheet).';

        $sheet = $objPHPExcel->getSheet(0);
        $dados = $sheet->toArray();

        $ceps = new Ceps();
        $regioes = new Geograficoregioes();
        $zonas = new Geograficozonas();

        foreach ($dados as $k => $v) {
            if ($k == 0)
                continue;

            $v[3] = str_pad($v[3], 8, "0", STR_PAD_LEFT);

            $obj = new stdClass;
            $obj->CODIGO = '';
            $obj->CEP = trim((strlen($v[3]) == 8) ? substr($v[3], 0, 5) . '-' . substr($v[3], 5, 3) : $v[3]);
            $obj->ENDERECO = trim($v[0]);
            $obj->BAIRRO = trim($v[1]);
            $obj->CIDADE = trim($v[2]);
            $obj->UF = 'SP';
            $obj->REGIAO = trim(trim($v[4]));
            $obj->ZONA = trim(trim($v[5]));


            if ($obj->REGIAO == '') {
                $idregiao = 0;
            } else {
                //REGIAO
                $regiao = $regioes->fetchRow("regiao='$obj->REGIAO' AND cidade='$obj->CIDADE' AND excluido='nao'");
                if ($regiao)
                    $regiao = $regiao->toArray();
                else
                    $regiao = array();

                $regiao['idlicenca'] = 7;
                $regiao['regiao'] = $obj->REGIAO;
                $regiao['cidade'] = $obj->CIDADE;
                $regiao['descricao'] = $this->addCEP(isset($regiao['descricao']) ? $regiao['descricao'] : '', $obj->CEP);
                $regiao['status'] = 'Ativo';
                $regiao['aplicado'] = 'Sim';
                $regiao['aplicadodata'] = date('Y-m-d G:i:s');
                $regiao['aplicadousuario'] = 0;
                $regiao['excluido'] = 'nao';
                $regiao['logcadastro'] = 0;
                $regiao['logdata'] = date('Y-m-d G:i:s');

                $regiao = $regioes->save($regiao);
                $idregiao = $regiao['id'];
            }



            //ZONA
            if ($obj->ZONA == '') {
                $idzona = 0;
            } else {
                $zona = $zonas->fetchRow("zona='$obj->ZONA' AND cidade='$obj->CIDADE' AND excluido='nao'");
                if ($zona)
                    $zona = $zona->toArray();
                else
                    $zona = array();

                $zona['idlicenca'] = 7;
                $zona['zona'] = $obj->ZONA;
                $zona['cidade'] = $obj->CIDADE;
                $zona['descricao'] = $this->addCEP(isset($zona['descricao']) ? $zona['descricao'] : '', $obj->CEP);
                $zona['status'] = 'Ativo';
                $zona['excluido'] = 'nao';
                $zona['logcadastro'] = 0;
                $zona['logdata'] = date('Y-m-d G:i:s');
                $zona = $zonas->save($zona);
                $idzona = $zona['id'];
            }



            //CEP
            $cep = $ceps->fetchRow("cep='$obj->CEP' AND excluido='nao'");
            if ($cep)
                $cep = $cep->toArray();
            else
                $cep = array();

            $tipo = explode(' ', $obj->ENDERECO);

            $cep['idlicenca'] = 7;
            $cep['cep'] = $obj->CEP;
            $cep['cidade'] = $obj->CIDADE;
            $cep['uf'] = $obj->UF;
            $cep['bairro'] = $obj->BAIRRO;
            $cep['tipo'] = trim($tipo[0]);
            $cep['logradouro'] = $obj->ENDERECO;
            $cep['idregiao'] = $idregiao;
            $cep['idzona'] = $idzona;
            $cep['excluido'] = 'nao';
            $cep['logcadastro'] = 0;
            $cep['logdata'] = date('Y-m-d G:i:s');

            $ceps->save($cep);
        }

        die('FIM');
    }

    private function addCEP($descricao, $cep) {
        $_ceps = explode(PHP_EOL, $descricao);
        foreach ($_ceps as $_cep) {
            if ((trim($_cep) == $cep) && (trim($_cep) != ''))
                return $descricao;
        }

        return $descricao . PHP_EOL . $cep;
    }

    public function indexAction() {
        /* $perfis = array();

          $perfis[0] = 'administrativoconfiguracoesgerais';
          $perfis[1] = 'administrativostipos';
          $perfis[2] = 'almoxarifadotipos';
          $perfis[3] = 'alunostipos';
          $perfis[4] = 'censoescolas';
          $perfis[5] = 'censostiposestruturas';
          $perfis[6] = 'censostiposedificacoes';
          $perfis[7] = 'comunicacaotipos';
          $perfis[8] = 'coordenacaotiposprovas';
          $perfis[9] = 'coordenacaoidebs';
          $perfis[10] = 'documentacoespontuacoes';
          $perfis[11] = 'documentacoespontuacoesimportacoes';
          $perfis[12] = 'educacaoespecialtipo';
          $perfis[13] = 'engenhariatipos';
          $perfis[14] = 'financeirotipos';
          $perfis[15] = 'juridicotipos';
          $perfis[16] = 'locacoestipos';
          $perfis[17] = 'logisticatipos';
          $perfis[18] = 'logisticaskmconfiguracos';
          $perfis[19] = 'nutricoestipos';
          $perfis[22] = 'ouvidoriatipos';
          $perfis[21] = 'pabxtipos';
          $perfis[22] = 'patrimoniostipos';
          $perfis[23] = 'professoresmetodosavaliacoes';
          $perfis[24] = 'professoresconfiguracoesnotas';
          $perfis[25] = 'professoreslancarnotas';
          $perfis[26] = 'professoresparticipacoesalunos';
          $perfis[27] = 'protocolostipos';
          $perfis[28] = 'recepcaogabinetetipos';
          $perfis[29] = 'rhtipos';
          $perfis[30] = 'secretariaescolartipos';
          $perfis[31] = 'supervisaotipos';
          $perfis[32] = 'titipos';

          $labels = array();
          $labels[0] = 'Configurações Gerais';
          $labels[1] = 'Cadastrar Tipos';
          $labels[2] = 'Cadastrar Tipos';
          $labels[3] = 'Cadastrar Tipos';
          $labels[4] = 'Cadastrar Tipos';
          $labels[5] = 'Tipos de estruturas';
          $labels[6] = 'Tipos de edificações';
          $labels[7] = 'Cadastrar Tipos';
          $labels[8] = 'Tipos de provas';
          $labels[9] = 'Região IDEB';
          $labels[10] = 'Tipos de pontuações';
          $labels[11] = 'Importar pontuações';
          $labels[12] = 'Cadastrar Tipos';
          $labels[13] = 'Tipos de equipe';
          $labels[14] = 'Tipos de idenficação';
          $labels[15] = 'Cadastrar Tipos';
          $labels[16] = 'Parametrizar tempo de vencimento';
          $labels[17] = 'Cadastrar Tipos';
          $labels[18] = 'Configurar KM escolar';
          $labels[19] = 'Cadastrar Tipos';
          $labels[22] = 'Cadastrar Tipos';
          $labels[21] = 'Cadastrar Tipos';
          $labels[22] = 'Cadastrar Tipos';
          $labels[23] = 'Método de avaliação';
          $labels[24] = 'Configuração de notas';
          $labels[25] = 'Lançar notas de trabalho';
          $labels[26] = 'Participação do aluno';
          $labels[27] = 'Tipos de processos';
          $labels[28] = 'Cadastrar Tipos';
          $labels[29] = 'Cadastrar Tipos';
          $labels[30] = 'Cadastrar Tipos';
          $labels[31] = 'Equipe técnica';
          $labels[32] = 'Equipe técnica';

          $html = "";
          foreach ($perfis as $ap => $perfil) :
          $html .='< ? php if (Mn_Util::isAccess("'.$perfil.'", "visualizar")) : ? >
          <li class="'.$perfil.'"><a href="javascript:;" title="">'.$labels[$ap].'</a></li>
          < ? php endif; ? >


          ';
          endforeach;
          die($html); */


        /* $solicitacoes = new Solicitacoes();
          $queries_solicitacoes = array();
          if($this->_usuario['secretaria']=="Sim"):
          $queries_solicitacoes['idusuarioretorno'] = $this->_usuario['id'];
          else:
          //var_dump($this->_usuario); die();
          if($this->_usuario['master']!="sim"):
          $queries_solicitacoes['idusuarioretorno_todos'] = $this->_usuario['id'];
          endif;
          endif; */
        $solicitacoes = new Protocolos();
        $queries_solicitacoes = array();
        $queries_solicitacoes['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries_solicitacoes["origem_destino"] = "Secretaria";
        $queries_solicitacoes["modulo"] = "Recepção";
        $queries_solicitacoes['total'] = true;

        $queries_solicitacoes['status'] = "Aberto";
        $this->view->solicitacoespendente = $solicitacoes->getProtocolos($queries_solicitacoes);

        $queries_solicitacoes['status'] = "Analisando";
        $this->view->solicitacoesexecucao = $solicitacoes->getProtocolos($queries_solicitacoes);

        $queries_solicitacoes['status'] = "Encerrado";
        $this->view->solicitacoesencerrado = $solicitacoes->getProtocolos($queries_solicitacoes);

        $queries_solicitacoes['status'] = "Cancelado";
        $this->view->solicitacoescancelado = $solicitacoes->getProtocolos($queries_solicitacoes);

        $ouvidorias = new Ouvidorias();
        $queriesouvidoria = array();

        if (!Mn_Util::isAccess("ouvidoriatodos", "visualizar")) {
        $iddepartamento = (int) Usuarios::getUsuario("iddepartamento");
        if ( (Usuarios::getUsuario("master") != "sim") && ($iddepartamento > 0)):
            $queriesouvidoria['iddepartamento'] = (int) $iddepartamento;
        elseif (Usuarios::getUsuario("master") != "sim"):
            $queriesouvidoria['iddepartamento'] = -1;
        endif;
    }

    $queriesouvidoria['total'] = true;
    $queriesouvidoria['status'] = "Aberto";
    $this->view->ouvidoriaabertos = $ouvidorias->getOuvidorias($queriesouvidoria);

    $queriesouvidoria['status'] = "Pendente";
    $this->view->ouvidoriapendente = $ouvidorias->getOuvidorias($queriesouvidoria);

    $queriesouvidoria['status'] = "Encerrado";
    $this->view->ouvidoriaencerrado = $ouvidorias->getOuvidorias($queriesouvidoria);

    $queriesouvidoria['status'] = "Cancelado";
    $this->view->ouvidoriacancelado = $ouvidorias->getOuvidorias($queriesouvidoria);


    $solicitacoes = new Protocolos();
    $querieschamados = array();
    $querieschamados['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'),  'id');
    $querieschamados["origem_destino"] = "Secretaria";
    $querieschamados["modulo"] = "Ti Chamados";

    $querieschamados['status'] = "Aberto";
    $this->view->chamadostecnicosabertos = $solicitacoes->getProtocolos($querieschamados);

    $querieschamados['status'] = "Analisando";
    $this->view->chamadostecnicospendente = $solicitacoes->getProtocolos($querieschamados);

    $querieschamados['status'] = "Cancelado";
    $this->view->chamadostecnicoscancelado = $solicitacoes->getProtocolos($querieschamados);

    $querieschamados['status'] = "Encerrado";
    $this->view->chamadostecnicosencerrado = $solicitacoes->getProtocolos($querieschamados);

    $solicitacoes = new Protocolos();
    $queriesengenharias = array();
    $queriesengenharias['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'),  'id');
    $queriesengenharias["origem_destino"] = "Secretaria";
    $queriesengenharias["modulo"] = "Engenharia Chamados";

    $queriesengenharias['status'] = "Aberto";
    $this->view->chamadosengenhariasabertos = $solicitacoes->getProtocolos($queriesengenharias);

    $queriesengenharias['status'] = "Analisando";
    $this->view->chamadosengenhariaspendente = $solicitacoes->getProtocolos($queriesengenharias);

    $queriesengenharias['status'] = "Cancelado";
    $this->view->chamadosengenhariascancelado = $solicitacoes->getProtocolos($queriesengenharias);

    $queriesengenharias['status'] = "Encerrado";
    $this->view->chamadosengenhariasencerrado = $solicitacoes->getProtocolos($queriesengenharias);


    $comunicacaoagenda = new Eventossecretarias();
    $queries = array();
    $queries['iddepartamento'] = Usuarios::getUsuario('idperfil');
    $queries["origem"] = "Comunicação";
    $queries["tipo"] = "Interna";
    $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'),  'id');
    $queries['order'] = "ORDER BY e1.datainicio DESC";
    $this->view->eventosagenda = $comunicacaoagenda->getEventossecretarias($queries, '0', '5');

    $cursos = new Cursos();
    $queriescursos = array();
    $queriescursos['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'),  'id');
    $queriescursos['origem'] = "Coordenacao";
    $this->view->cursos = $cursos->getCursos($queriescursos, 0, 5);

    $comunicacaocomunicados = new Comunicacaocomunicados();


    $queriescomunicacaocomunicados = array();
    $idescola = Usuarios::getUsuario('idescola');
    $idperfil = Usuarios::getUsuario('idperfil');
    $iduser = Usuarios::getUsuario('id');

    $queriescomunicacaocomunicados['idescola'] = (int) $idescola;
    if ($idescola <= 0) {
    $queriescomunicacaocomunicados['idperfil'] = Usuarios::getUsuario('idperfil');
    $queriescomunicacaocomunicados['verificaperfil'] = true;
} elseif (Usuarios::getUsuario('professor') == "Sim") {
$queriescomunicacaocomunicados['verificaescola'] = true;
$queriescomunicacaocomunicados['idsprofessores'] = Usuarios::getUsuario('idfuncionario');
} else {
$queriescomunicacaocomunicados['verificaescola'] = true;
}

//$queries['idperfil'] = Usuarios::getUsuario('idperfil');
$queriescomunicacaocomunicados['idusuariocriacao'] = Usuarios::getUsuario('id');
//$queriescomunicacaocomunicados = array();
//$queriescomunicacaocomunicados['status'] = "Ativo";
//$queriescomunicacaocomunicados['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
//$queriescomunicacaocomunicados['idperfil'] = (int)Usuarios::getUsuario('idperfil');
//$queriescomunicacaocomunicados['idsprofessores'] = (int)Usuarios::getUsuario('idfuncionario');
$this->view->comunicados = $comunicacaocomunicados->getComunicacaocomunicados($queriescomunicacaocomunicados);

$documentacaopermutasaceites = new Documentacaopermutasaceites();
$queries = array();
$queries['idprofessor'] = (int) Usuarios::getUsuario('idfuncionario');
$this->view->permutasaceitas = $documentacaopermutasaceites->getDocumentacaopermutasaceites($queries, 0, 5);

$documentacaovagasampliacao = new Documentacaovagasampliacaoaceites();
$queries = array();
$queries['idprofessor'] = (int) Usuarios::getUsuario('idfuncionario');
$this->view->ampliacoesaceitas = $documentacaovagasampliacao->getDocumentacaovagasampliacaoaceites($queries, 0, 5);

$protocoloprocessos = new Protocolosprocessostramitacoes();
$usuario = Usuarios::getUsuario();

/* Nortificação do protocolo */
$queriesprotocoloprocessosaberto = array();
$queriesprotocoloprocessosaberto['idperfildestino'] = $usuario['idperfil'];
if ($usuario['idperfil'] == '29') {
$queriesprotocoloprocessosaberto['idescoladestino'] = $usuario['idescola'];
}
$queriesprotocoloprocessosaberto['status'] = 'aberto';
$queriesprotocoloprocessosaberto['excluido'] = 'nao';
$this->view->protocoloprocessos_aberto = $protocoloprocessos->getProtocolosprocessostramitacoesHelper($queriesprotocoloprocessosaberto);

$queriesprotocoloprocessoslido = array();
$queriesprotocoloprocessoslido['idperfilorigem'] = $usuario['idperfil'];
if ($usuario['idperfil'] == '29') {
$queriesprotocoloprocessoslido['idescolaorigem'] = $usuario['idescola'];
}
$queriesprotocoloprocessoslido['status'] = 'lido';
$queriesprotocoloprocessoslido['excluido'] = 'nao';
$this->view->protocoloprocessos_lido = $protocoloprocessos->getProtocolosprocessostramitacoesHelper($queriesprotocoloprocessoslido);
}

public function ajaxAction() {
$action = trim($this->getRequest()->getParam('acao'));

if ($action == 'copiar_tipo') {

$idtipo = (int) trim($this->getRequest()->getParam('idtipo'));

$financas = new Financas();
$tipo = $financas->fetchRow("excluido='nao' AND IFNULL(idfranquiatipo,0)=$idtipo");

if ($tipo) {
$tipo = $tipo->toArray();
echo json_encode($tipo);
die();
}
die(json_encode(array('erro' => 'Tipo não encontrado')));
}

die(json_encode(array('erro' => 'Função não encontrada')));
}

public function setminimizeAction() {
$val = $this->getRequest()->getParam('val');
$ns = new Zend_Session_Namespace('minimize');
$ns->val = ($val == 'sim') ? 'sim' : 'nao';
die($ns->val);
}

public function buscacepAction() {
$_cep = $this->getRequest()->getPost('cep');
$modo = $this->getRequest()->getPost('modo');

$url = "http://cep.republicavirtual.com.br/web_cep.php?cep=" . $_cep . "&formato=json";

$contents = @file_get_contents($url);

$decode = json_decode($contents);
$resultado = ((isset($decode->resultado)) && ((int) $decode->resultado == 1));

if (!$resultado) {
die('false');
}

$uf = $decode->uf;
$idestado = Estados::getIdEstadoByUF($uf);
$decode->idestado = $idestado;

die(json_encode($decode));
}

public function buscacepxmlAction() {
$_cep = $this->getRequest()->getPost('cep');
$modo = $this->getRequest()->getPost('modo');

$url = "http://cep.republicavirtual.com.br/web_cep.php?cep=" . $_cep . "&formato=json";

$contents = @file_get_contents($url);

$decode = json_decode($contents);
$resultado = ((isset($decode->resultado)) && ((int) $decode->resultado == 1));

if (!$resultado) {
die('false');
}

$uf = $decode->uf;
$idestado = Estados::getIdEstadoByUF($uf);
$decode->idestado = $idestado;

die(json_encode($decode));
}

public function setcalendarioAction() {
$this->_helper->layout->disableLayout();

$page = (int) $this->getRequest()->getPost("page");

$this->view->page = $page;
}

public function seteventoAction() {
$this->_helper->layout->disableLayout();

$id = $this->getRequest()->getParam('id');

$comunicacaoagenda = new Eventossecretarias();
$evento = $comunicacaoagenda->getEventosecretariaById($id);

if (!$evento)
die('Não Encontrado');

$this->view->post_var = $evento;
}

public function seteventosdiaAction() {
$this->_helper->layout->disableLayout();

$dia = $this->getRequest()->getParam('dia');

$queries["origem"] = "Comunicação";
$queries["tipo"] = "Interna";
$queries['iddepartamento'] = Usuarios::getUsuario('idperfil');
$queries['data'] = date('Y-m-d', $dia);
$queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'),  'id');
$queries['order'] = "ORDER BY e1.datainicio DESC";

$comunicacaoagenda = new Eventossecretarias();
$rows = $comunicacaoagenda->getEventossecretarias($queries);

if (!$rows)
die('Não Encontrado');
$this->view->dia = $dia;
$this->view->rows = $rows;
}

public function setgraficossolicitacoesAction() {
$this->_helper->layout->disableLayout();

$solicitacoes = new Solicitacoes();
$queries_solicitacoes = array();
if ($this->_usuario['secretaria'] == "Sim"):
$queries_solicitacoes['idusuarioretorno'] = $this->_usuario['id'];
else:
//var_dump($this->_usuario); die();
if ($this->_usuario['master'] != "sim"):
$queries_solicitacoes['idusuarioretorno_todos'] = $this->_usuario['id'];
endif;
endif;

$queries_solicitacoes['total'] = true;
;

$queries_solicitacoes['status'] = "Pendente";
$this->view->solicitacoespendente = $solicitacoes->getSolicitacoes($queries_solicitacoes);

$queries_solicitacoes['status'] = "Analisando";
$this->view->solicitacoesexecucao = $solicitacoes->getSolicitacoes($queries_solicitacoes);

$queries_solicitacoes['status'] = "Encerrado";
$this->view->solicitacoesencerrado = $solicitacoes->getSolicitacoes($queries_solicitacoes);

$queries_solicitacoes['status'] = "Cancelado";
$this->view->solicitacoescancelado = $solicitacoes->getSolicitacoes($queries_solicitacoes);

$solicitacoes = new Protocolos();
$queries_solicitacoes = array();
$queries_solicitacoes['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'),  'id');
$queries_solicitacoes["origem_destino"] = "Secretaria";
$queries_solicitacoes["modulo"] = "Recepção";
$queries_solicitacoes['total'] = true;
;

$queries_solicitacoes['status'] = "Aberto";
$this->view->solicitacoespendente = $solicitacoes->getProtocolos($queries_solicitacoes);

$queries_solicitacoes['status'] = "Analisando";
$this->view->solicitacoesexecucao = $solicitacoes->getProtocolos($queries_solicitacoes);

$queries_solicitacoes['status'] = "Encerrado";
$this->view->solicitacoesencerrado = $solicitacoes->getProtocolos($queries_solicitacoes);

$queries_solicitacoes['status'] = "Cancelado";
$this->view->solicitacoescancelado = $solicitacoes->getProtocolos($queries_solicitacoes);
}

public function setgraficosouvidoriaAction() {
$this->_helper->layout->disableLayout();

$ouvidorias = new Ouvidorias();
$queriesouvidoria = array();

if (!Mn_Util::isAccess("ouvidoriatodos", "visualizar")) {
$iddepartamento = (int) Usuarios::getUsuario("iddepartamento");
if ( (Usuarios::getUsuario("master") != "sim") && ($iddepartamento > 0)):
$queriesouvidoria['iddepartamento'] = (int) $iddepartamento;
elseif (Usuarios::getUsuario("master") != "sim"):
$queriesouvidoria['iddepartamento'] = -1;
endif;
}

$queriesouvidoria['total'] = true;
$queriesouvidoria['status'] = "Aberto";
$this->view->ouvidoriaabertos = $ouvidorias->getOuvidorias($queriesouvidoria);

$queriesouvidoria['status'] = "Pendente";
$this->view->ouvidoriapendente = $ouvidorias->getOuvidorias($queriesouvidoria);

$queriesouvidoria['status'] = "Encerrado";
$this->view->ouvidoriaencerrado = $ouvidorias->getOuvidorias($queriesouvidoria);

$queriesouvidoria['status'] = "Cancelado";
$this->view->ouvidoriacancelado = $ouvidorias->getOuvidorias($queriesouvidoria);
}

public function setgraficospatrimoniosAction() {
$this->_helper->layout->disableLayout();
$patrimonios = new Patrimonios_Patrimonios();
$queriespatrimonios = array();

$queriespatrimonios['total'] = true;
$queriespatrimonios['aprovacao'] = "sim";
$this->view->patrimoniosaprovados = $patrimonios->getPatrimonios($queriespatrimonios);
;

$queriespatrimonios['aprovacao'] = "nao";
$this->view->patrimoniosnaoaprovados = $patrimonios->getPatrimonios($queriespatrimonios);
}

public function setgraficoschamadostecnicosAction() {
$this->_helper->layout->disableLayout();

$solicitacoes = new Protocolos();
$querieschamados = array();
$querieschamados['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'),  'id');
$querieschamados["origem_destino"] = "Secretaria";
$querieschamados["modulo"] = "Ti Chamados";

$querieschamados['status'] = "Aberto";
$this->view->chamadostecnicosabertos = $solicitacoes->getProtocolos($querieschamados);

$querieschamados['status'] = "Analisando";
$this->view->chamadostecnicospendente = $solicitacoes->getProtocolos($querieschamados);

$querieschamados['status'] = "Cancelado";
$this->view->chamadostecnicoscancelado = $solicitacoes->getProtocolos($querieschamados);

$querieschamados['status'] = "Encerrado";
$this->view->chamadostecnicosencerrado = $solicitacoes->getProtocolos($querieschamados);
}

public function setgraficoschamadosengenhariasAction() {
$this->_helper->layout->disableLayout();
$solicitacoes = new Protocolos();
$queriesengenharias = array();
$queriesengenharias['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'),  'id');
$queriesengenharias["origem_destino"] = "Secretaria";
$queriesengenharias["modulo"] = "Engenharia Chamados";

$queriesengenharias['status'] = "Aberto";
$this->view->chamadosengenhariasabertos = $solicitacoes->getProtocolos($queriesengenharias);

$queriesengenharias['status'] = "Analisando";
$this->view->chamadosengenhariaspendente = $solicitacoes->getProtocolos($queriesengenharias);

$queriesengenharias['status'] = "Cancelado";
$this->view->chamadosengenhariascancelado = $solicitacoes->getProtocolos($queriesengenharias);

$queriesengenharias['status'] = "Encerrado";
$this->view->chamadosengenhariasencerrado = $solicitacoes->getProtocolos($queriesengenharias);
}

public function getimoveisAction() {
$this->_helper->layout->disableLayout();

$id = trim($this->getRequest()->getParam('id'));

$db = Zend_Registry::get('db');
$strsql = "SELECT * FROM locacoesimoveis WHERE idlocador='$id' AND excluido='nao' ORDER BY id DESC";

$this->view->imoveis = $db->fetchAll($strsql);
}

public function getveiculosAction() {
$this->_helper->layout->disableLayout();

$id = trim($this->getRequest()->getParam('id'));

$frotas = new Logisticafrotas();

$this->view->frotas = $frotas->getFrotas();
}

public function getmotoristasveiculosAction() {
$this->_helper->layout->disableLayout();

$id = trim($this->getRequest()->getParam('id'));

$motoristas_veiculos = new Logisticamotoristasveiculos();
$queries['idmotorista'] = $id;

$this->view->frotas = $motoristas_veiculos->getMotoristasVeiculos($queries);
}

public function getmarcasAction() {
$this->_helper->layout->disableLayout();

$id = trim($this->getRequest()->getParam('id'));

$marcas = new Logisticamarcas();
$queries['idfabricante'] = $id;

$this->view->marcas = $marcas->getMarcas($queries);
}

public function changesecretariaAction() {
$id = (int) $this->getRequest()->getPost('id');

$usuarios = new Usuarios();
$usuario = $usuarios->fetchRow("id=" . Usuarios::getUsuario('id'));

$usuario = $usuario->toArray();

$usuario['idsecretariaativa'] = $id;
$usuarios->save($usuario);

die('OK');
}

public function changeescolaAction() {
$id = (int) $this->getRequest()->getPost('id');

$usuarios = new Usuarios();
$usuario = $usuarios->fetchRow("id=" . Usuarios::getUsuario('id'));

$usuario = $usuario->toArray();

$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
$funcionariogeralescola = $funcionariosgeraisescolas->getFuncionariogeralescolaById($usuario['idfuncionario']);

$funcionariogeralescola['idescola'] = $id;
$funcionariosgeraisescolas->save($funcionariogeralescola);

$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
$dados_session = unserialize($loginNameSpace->usuario);
$dados_session['idescola'] = $id;

$loginNameSpace->usuario = serialize($dados_session);

die('OK');
}

public function fileuploadAction() {
error_reporting(E_ALL | E_STRICT);

$upload_handler = new UploadHandler();

header('Pragma: no-cache');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Content-Disposition: inline; filename="files.json"');
header('X-Content-Type-Options: nosniff');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, DELETE');
header('Access-Control-Allow-Headers: X-File-Name, X-File-Type, X-File-Size');

switch ($_SERVER['REQUEST_METHOD']) {
case 'OPTIONS':
break;
case 'HEAD':
case 'GET':
$upload_handler->get();
break;
case 'POST':
if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
$upload_handler->delete();
} else {
// $upload_handler->post();


$arquivos = new Arquivos();
$idarquivo = $arquivos->getArquivoFromFormsByPos('files', 0);

$result = array();
$result['idarquivo'] = $idarquivo;
$result['filename'] = $_FILES['files']["name"][0];
$result['idx'] = (int) $_POST['idx'];

echo '[' . json_encode($result) . ']';
}
break;
case 'DELETE':
$upload_handler->delete();
break;
default:
header('HTTP/1.1 405 Method Not Allowed');
}
die();
}

public function geradorAction() {
if ($this->getRequest()->isPost()) {
if (!isset($this->view->post_var))
$this->view->post_var = $_POST;
else

$this->view->post_var = array_merge($this->view->post_var, $_POST);

$tabela = trim($_POST['tabela']);

$matriz_sistema = explode('|', $_POST['matriz_sistema']);
foreach ($matriz_sistema as $k => $v)
$matriz_sistema[$k] = trim($v);
$matriz_humano = explode('|', $_POST['matriz_humano']);


foreach ($matriz_humano as $k => $v)
$matriz_humano[$k] = trim($v);

$modulo = trim($_POST['modulo']);
$prefixo = trim($_POST['prefixo']);
$sobrescrever = trim($_POST['sobrescrever']);
$sources = array();
$sources['controller'] = APPLICATION_PATH . '/modules/admin/views/scripts/index/gerador/default/TemplatesController.php';
$sources['db'] = APPLICATION_PATH . '/modules/admin/views/scripts/index/gerador/default/Templates.php';
$sources['views_index'] = APPLICATION_PATH . '/modules/admin/views/scripts/index/gerador/default/views/index.phtml';
$sources['views_adicionar'] = APPLICATION_PATH . '/modules/admin/views/scripts/index/gerador/default/views/adicionar.phtml';
$sources['views_editar'] = APPLICATION_PATH . '/modules/admin/views/scripts/index/gerador/default/views/editar.phtml';
$sources['views_form'] = APPLICATION_PATH . '/modules/admin/views/scripts/index/gerador/default/views/form.phtml';
$sources['views_visualizar'] = APPLICATION_PATH . '/modules/admin/views/scripts/index/gerador/default/views/visualizar.phtml';
$sources['scripts_index'] = APPLICATION_PATH . '/modules/admin/views/scripts/index/gerador/default/scripts/templates_index.js';
$sources['scripts_form'] = APPLICATION_PATH . '/modules/admin/views/scripts/index/gerador/default/scripts/templates_form.js';


$targets = array();
$targets['controller'] = APPLICATION_PATH . '/modules/' . $modulo . '/controllers/' . ucfirst($tabela) . 'Controller.php';
$targets['db'] = APPLICATION_PATH . '/modules/default/models/db/' . ucfirst($tabela) . '.php';
$targets['views_index'] = APPLICATION_PATH . '/modules/' . $modulo . '/views/scripts/' . $tabela . '/index.phtml';
$targets['views_adicionar'] = APPLICATION_PATH . '/modules/' . $modulo . '/views/scripts/' . $tabela . '/adicionar.phtml';
$targets['views_editar'] = APPLICATION_PATH . '/modules/' . $modulo . '/views/scripts/' . $tabela . '/editar.phtml';
$targets['views_form'] = APPLICATION_PATH . '/modules/' . $modulo . '/views/scripts/' . $tabela . '/form.phtml';
$targets['views_visualizar'] = APPLICATION_PATH . '/modules/' . $modulo . '/views/scripts/' . $tabela . '/visualizar.phtml';
$targets['scripts_index'] = BASE_PATH . '/public/scripts/' . $modulo . '/' . $tabela . '_index.js';
$targets['scripts_form'] = BASE_PATH . '/public/scripts/' . $modulo . '/' . $tabela . '_form.js';

//verifica se arquivos existem
if ($sobrescrever != 'sim') {
$erros = array();
foreach ($targets as $target) {
if (file_exists($target))
array_push($erros, 'Já existe arquivo em "' . $target . '".');
}
if (sizeof($erros) > 0) {
$this->view->erros = $erros;
return false;
}
}

//columns schema
$schema = $this->getColumnsSchema($tabela);


//cria o diretorio da view se não existir
if (!file_exists(dirname($targets['views_adicionar']))) {
mkdir(dirname($targets['views_adicionar']),  0777, true);
}

//sript index
copy($sources['scripts_index'], $targets['scripts_index']);
$contents = file_get_contents($targets['scripts_index']);

$contents = $this->setScriptFormJsRepopula($contents, $schema);
$contents = $this->setScriptFormJsMaskFormat($contents, $schema, 'index');
file_put_contents($targets['scripts_index'], $contents);

//sript form
copy($sources['scripts_form'], $targets['scripts_form']);
$contents = file_get_contents($targets['scripts_form']);

$contents = $this->setScriptFormJsRepopula($contents, $schema);
$contents = $this->setScriptFormJsRequired($contents, $schema);
$contents = $this->setScriptFormJsMaskFormat($contents, $schema);
file_put_contents($targets['scripts_form'], $contents);


//view form
copy($sources['views_form'], $targets['views_form']);
$contents = file_get_contents($targets['views_form']);

$contents = str_replace('MSYSTemplates', $matriz_sistema[0], $contents);
$contents = str_replace('MSYSTemplate', $matriz_sistema[1], $contents);
$contents = str_replace('MSYStemplates', $matriz_sistema[2], $contents);
$contents = str_replace('MSYStemplate', $matriz_sistema[3], $contents);
$contents = str_replace('MHUMTemplates', $matriz_humano[0], $contents);
$contents = str_replace('MHUMTemplate', $matriz_humano[1], $contents);
$contents = str_replace('MHUMtemplates', $matriz_humano[2], $contents);
$contents = str_replace('MHUMtemplate', $matriz_humano[3], $contents);

$contents = $this->setViewFormFields($contents, $schema);
$contents = $this->setViewIndexPopulaCombos($contents, $schema);

file_put_contents($targets['views_form'], $contents);

//view index
copy($sources['views_index'], $targets['views_index']);
$contents = file_get_contents($targets['views_index']);

$contents = str_replace('MSYSTemplates', $matriz_sistema[0], $contents);
$contents = str_replace('MSYSTemplate', $matriz_sistema[1], $contents);
$contents = str_replace('MSYStemplates', $matriz_sistema[2], $contents);
$contents = str_replace('MSYStemplate', $matriz_sistema[3], $contents);
$contents = str_replace('MHUMTemplates', $matriz_humano[0], $contents);
$contents = str_replace('MHUMTemplate', $matriz_humano[1], $contents);
$contents = str_replace('MHUMtemplates', $matriz_humano[2], $contents);
$contents = str_replace('MHUMtemplate', $matriz_humano[3], $contents);

$grids = 2;
foreach ($schema as $k => $v)
if ($v->grid)
$grids++;
$contents = str_replace('#COLSPAN_MENOS_1', $grids - 1, $contents);
$contents = str_replace('#COLSPAN_MENOS', $grids, $contents);

$contents = $this->setViewIndexTheadTitle($contents, $schema);
$contents = $this->setViewIndexTheadSearch($contents, $schema);
$contents = $this->setViewIndexBodyValues($contents, $schema);
$contents = $this->setViewIndexPopulaCombos($contents, $schema);
$contents = $this->setViewIndexJsSorting($contents, $schema);

file_put_contents($targets['views_index'], $contents);


//views adicionar, editar e visualizar
foreach (array('views_adicionar', 'views_editar', 'views_visualizar') as $k => $v) {
copy($sources[$v], $targets[$v]);
$contents = file_get_contents($targets[$v]);
$contents = str_replace('MSYStemplates', $matriz_sistema[2], $contents);
file_put_contents($targets[$v], $contents);
}

//db
copy($sources['db'], $targets['db']);
$contents = file_get_contents($targets['db']);

$contents = str_replace('MSYSTemplates', $matriz_sistema[0], $contents);
$contents = str_replace('MSYSTemplate', $matriz_sistema[1], $contents);
$contents = str_replace('MSYStemplates', $matriz_sistema[2], $contents);
$contents = str_replace('MSYStemplate', $matriz_sistema[3], $contents);

$contents = $this->setTableAddress($contents, $schema);
$contents = str_replace('PREFIX_T1', substr($tabela, 0, 1) . '1', $contents); //importante estar embaixo de setTableAddress

$contents = $this->setGravaDadosDb($contents, $schema);
$contents = $this->setDbAttrFilters($contents, $schema, $tabela);

file_put_contents($targets['db'], $contents);
//fim db
//controller
copy($sources['controller'], $targets['controller']);
$contents = file_get_contents($targets['controller']);

$contents = str_replace('Prefixo_', $prefixo, $contents);
$contents = str_replace('MSYSTemplates', $matriz_sistema[0], $contents);
$contents = str_replace('MSYSTemplate', $matriz_sistema[1], $contents);
$contents = str_replace('MSYStemplates', $matriz_sistema[2], $contents);
$contents = str_replace('MSYStemplate', $matriz_sistema[3], $contents);
$contents = str_replace('MHUMTemplates', $matriz_humano[0], $contents);
$contents = str_replace('MHUMTemplate', $matriz_humano[1], $contents);
$contents = str_replace('MHUMtemplates', $matriz_humano[2], $contents);
$contents = str_replace('MHUMtemplate', $matriz_humano[3], $contents);

$contents = $this->setControllerGetFieldsFront($contents, $schema);
$contents = $this->setControllerRequired($contents, $schema);
$contents = $this->setControllerFilters($contents, $schema);
$contents = $this->setControllerUnique($contents, $schema, $tabela);

$contents = $this->setAttrValuesToDb($contents, $schema);
$contents = str_replace('MSYStemplate', $matriz_sistema[3], $contents); //importante repassar

file_put_contents($targets['controller'], $contents);
////fim controller



die();
}
}

private function setScriptFormJsMaskFormat($contents, $schema, $source = 'form') {
$JS_MASK_FORMAT = '';
foreach ($schema as $k => $v) {
if (!$v->system) {

if ( ($v->decimal !== false) && (!$v->combo) && ($v->address)) {

if ((int) $v->decimal > 0) {
$JS_MASK_FORMAT .= "	$('input[name=" . $v->name . "]').priceFormat({
	    prefix: '',
	    centsSeparator: ',',
	    thousandsSeparator: '.',
	    centsLimit: " . $v->decimal . "
	});";
} else {
$JS_MASK_FORMAT .= "	$('input[name=" . $v->name . "]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: " . $v->decimal . "
	});";
}
} else if ($v->number !== false) {

$JS_MASK_FORMAT .= "	$('input[name=" . $v->name . "]').priceFormat({
	    prefix: '',
	    centsSeparator: '',
	    thousandsSeparator: '',
	    centsLimit: " . $v->number . "
	});";
} else if ($v->type == 'date') {


if ($source == 'form') {

$JS_MASK_FORMAT .= "	$(\"input[name=" . $v->name . "]\").mask(\"99/99/9999\", {placeholder:\" \"});
	$(\"input[name=" . $v->name . "]\").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});";
} else {

$JS_MASK_FORMAT .= "	$(\"input[name=" . $v->name . "_i],input[name=" . $v->name . "_f]\").mask(\"99/99/9999\", {placeholder:\" \"});
	$(\"input[name=" . $v->name . "_i],input[name=" . $v->name . "_f]\").datepicker({
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
		dateFormat: 'dd/mm/yy',
		dayNames:  ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		currentText: 'Hoje',
		prevText: 'Antes',
		nextText: 'Depois',
		onSelect: function(date) {
	    }
	});";
}
}


$JS_MASK_FORMAT .= '' . chr(13) . chr(10);
}
}

$contents = str_replace('//#JS_MASK_FORMAT', $JS_MASK_FORMAT, $contents);
return $contents;
}

private function setScriptFormJsRequired($contents, $schema) {
$JS_REQUIRED = '';
foreach ($schema as $k => $v) {
if ( (!$v->system) && ($v->required)) {

if ($v->name == 'status') {
$JS_REQUIRED .= "rules.status1 = \"required\";";
} else {
$JS_REQUIRED .= "rules." . $v->name . " = \"required\";";
}

$JS_REQUIRED .= '' . chr(13) . chr(10);
}
}

$contents = str_replace('//#JS_REQUIRED', $JS_REQUIRED, $contents);
return $contents;
}

private function setScriptFormJsRepopula($contents, $schema) {
$JS_REPOPULA = '';
foreach ($schema as $k => $v) {
if ($v->combo) {

if ($v->name == 'status') {
$JS_REPOPULA .= "$(\"select[name=status1]\").val(form_values.status1);";
} else {
$JS_REPOPULA .= "$(\"select[name=" . $v->name . "]\").val(form_values." . $v->name . ");";
}

$JS_REPOPULA .= '' . chr(13) . chr(10);
} else if ($v->address) {

$__idestado = 'e_' . $v->name . 'idestado';
$JS_REPOPULA .= "$(\"select[name=" . $__idestado . "]\").val(form_values." . $__idestado . ");";
}
}

$contents = str_replace('//JS_REPOPULA', $JS_REPOPULA, $contents);
return $contents;
}

private function setViewFormFields($contents, $schema) {
$FIELDS = '';

foreach ($schema as $k => $v) {
if (!$v->system) {
$required = "";
if ($v->required)
$required = "<span class=\"required\">*</span>";

$maxlength = "";
if ((int) $v->maxlength > 0)
$maxlength = 'maxlength="' . $v->maxlength . '"';

$tip = "";
if ($v->tip) {
$tip = "<span class=\"help-block\">" . $v->tip . "</span>";
}

if ($v->combo) {

$name = $v->name;
if ($name == 'status')
$name = "status1";

if ($v->fk) {

$fk = explode(',', $v->fk);
$fk_classe = (isset($fk[0])) ? trim($fk[0]) : 'Classe';
$fk_helper = (isset($fk[1])) ? trim($fk[1]) : 'Helper';
$fk_field = (isset($fk[2])) ? trim($fk[2]) : 'field';


$FIELDS .= "						        <div class=\"control-group\">
						            <label class=\"control-label\">" . $v->label . " " . $required . "</label>
						            <div class=\"controls\">
						                <select name=\"" . $name . "\" data-placeholder=\"" . $v->label . "\" class=\"select\">
						                    <option value=\"\">Selecione...</option>
						                	<?php foreach (" . $fk_classe . "::" . $fk_helper . "(array('status'=>'Ativo','idlicenca'=>Licencas::getLicenca('id'))) as XcifraoXk=>XcifraoXv) : ?>
						                	<option value=\"<?php echo XcifraoXv['id'] ?>\"><?php echo XcifraoXv['" . $fk_field . "'] ?></option>
						                	<?php endforeach;?>
						                </select>
						                " . $tip . "
						            </div>
						        </div>" . chr(13) . chr(10) . chr(13) . chr(10);
} else {

$options = '';
foreach ($v->combo_values as $chave => $valor) {
$options .= "<option value=\"" . $valor . "\">" . $valor . "</option>";
}

$FIELDS .= "						        <div class=\"control-group\">
						            <label class=\"control-label\">" . $v->label . " " . $required . "</label>
						            <div class=\"controls\">
						                <select name=\"" . $name . "\" data-placeholder=\"" . $v->label . "\" class=\"select\">
						                    <option value=\"\">Selecione...</option>
						                	" . $options . "
						                </select>
						                " . $tip . "
						            </div>
						        </div>" . chr(13) . chr(10) . chr(13) . chr(10);
}
} else if ($v->type == 'text') {

$FIELDS .= "						        <div class=\"control-group\">
						            <label class=\"control-label\">" . $v->label . " " . $required . "</label>
						            <div class=\"controls\"><textarea class=\"span6\" name=\"" . $v->name . "\" cols=\"5\" rows=\"5\"><?php echo (isset(xthis->post_var)) ? xthis->post_var[\"" . $v->name . "\"] : ''; x?></textarea>" . $tip . "</div>
						        </div>" . chr(13) . chr(10) . chr(13) . chr(10);
} else if ($v->imagem) {

$FIELDS .= "						        <div class=\"control-group\">
						            <label class=\"control-label\">" . $v->label . " " . $required . "</label>
						            <div class=\"controls\">
									<?php if ((isset(xthis->post_var['" . $v->name . "'])) && ((int)xthis->post_var['" . $v->name . "']>0)) : ?>
										<?php echo xthis->getImagem(xthis->post_var['" . $v->name . "'], 80, 80, '', Imagens::XcifraoXINDISPONIVEL) ?>

										<?php if (!xthis->visualizar) : ?>
										<div class=\"checkbox block\">
											<label><input type=\"checkbox\" name=\"excluir_" . $v->name . "\" value=\"excluir_" . $v->name . "\" /> Desejo excluir a imagem</label>
										</div>
										<?php endif; x?>

									<xphp endif; ?>

						            	<input placeholder=\"" . $v->label . "\" name=\"" . $v->name . "\" type=\"file\"  class=\"span3\" />" . $tip . "
						            </div>
						        </div>	" . chr(13) . chr(10) . chr(13) . chr(10);
} else if ($v->arquivo) {

$FIELDS .= "						        <div class=\"control-group\">
					            <label class=\"control-label\">" . $v->label . " " . $required . "</label>
					            <div class=\"controls\">
								<?php if ((isset(xthis->post_var['" . $v->name . "'])) && ((int)xthis->post_var['" . $v->name . "']>0)) : ?>
									<?php echo xthis->getArquivo(xthis->post_var['" . $v->name . "']) ?>

									<?php if (!xthis->visualizar) : ?>
									<div class=\"checkbox block\">
										<label><input type=\"checkbox\" name=\"excluir_" . $v->name . "\" value=\"excluir\" /> Desejo excluir o arquivo</label>
									</div>
									<?php endif; x?>

								<xphp endif; ?>

					            	<input placeholder=\"" . $v->label . "\" name=\"" . $v->name . "\" type=\"file\"  class=\"span3\" />" . $tip . "
					            </div>
					        </div>	" . chr(13) . chr(10) . chr(13) . chr(10);
} else if ($v->type == 'date') {

$FIELDS .= "						        <div class=\"control-group\">
						            <label class=\"control-label\">" . $v->label . "</label>
						            <div class=\"controls\"><input placeholder=\"" . $v->label . "\" name=\"" . $v->name . "\" value=\"<?php echo (isset(xthis->post_var['" . $v->name . "'])) ? Mn_Util::formPostVarData(xthis->post_var['" . $v->name . "']) : '' ?>\" type=\"text\"  class=\"span3\" /></div>
						        </div>" . chr(13) . chr(10) . chr(13) . chr(10);
} else if ($v->address) {

$prefix = 'e_' . $v->name;

$FIELDS .= "<?php
							xthis->endereco_prefix = \"" . $prefix . "\";
							echo xthis->render('/helpers/forms/endereco.phtml') ?>";
} else {
$FIELDS .= "						        <div class=\"control-group\">
						            <label class=\"control-label\">" . $v->label . " " . $required . "</label>
						            <div class=\"controls\"><input " . $maxlength . " placeholder=\"" . $v->label . "\" name=\"" . $v->name . "\" value=\"<?php echo (isset(xthis->post_var['" . $v->name . "'])) ? xthis->post_var['" . $v->name . "'] : '' ?>\" type=\"text\"  class=\"span3\" />" . $tip . "</div>
						        </div>" . chr(13) . chr(10) . chr(13) . chr(10);
}
}
}


$FIELDS = str_replace('xthis', '$this', $FIELDS);
$FIELDS = str_replace('x?>', '?>', $FIELDS);
$FIELDS = str_replace('<xphp', '<?php', $FIELDS);
$FIELDS = str_replace('XcifraoX', '$', $FIELDS);


$contents = str_replace('#FIELDS', $FIELDS, $contents);
return $contents;
}

private function setViewIndexJsSorting($contents, $schema) {
$JS_SORTING = '';
$arr = array();
foreach ($schema as $k => $v) {
if ($v->filter)
array_push($arr, $v->name . '"');
}
$JS_SORTING = trim(implode(', "', $arr));
if ($JS_SORTING != "")
$JS_SORTING = '"' . $JS_SORTING;

$contents = str_replace('$JS_SORTING', $JS_SORTING, $contents);
return $contents;
}

private function setViewIndexPopulaCombos($contents, $schema) {
$POPULA_COMBOS = '';
foreach ($schema as $k => $v) {
if ($v->combo) {

if ($v->name == 'status') {
$POPULA_COMBOS .= "form_values.status1 = '<?php echo (isset(xthis->post_var['status1'])) ? xthis->post_var['status1'] : xthis->post_var['status'] ?>';" . chr(13) . chr(10);
} else {
$POPULA_COMBOS .= "form_values." . $v->name . " = '<?php echo (isset(xthis->post_var['" . $v->name . "'])) ? xthis->post_var['" . $v->name . "'] : '' ?>';" . chr(13) . chr(10);
}

$POPULA_COMBOS .= '' . chr(13) . chr(10);
} else if ($v->address) {
$__idestado = 'e_' . $v->name . 'idestado';
$POPULA_COMBOS .= "form_values." . $__idestado . " = '<?php echo (isset(xthis->post_var['" . $__idestado . "'])) ? xthis->post_var['" . $__idestado . "'] : '' ?>';" . chr(13) . chr(10);
}
}

$POPULA_COMBOS = str_replace('xthis', '$this', $POPULA_COMBOS);

$contents = str_replace('//#POPULA_COMBOS', $POPULA_COMBOS, $contents);
return $contents;
}

private function setViewIndexBodyValues($contents, $schema) {
$BODY_VALUES = '';
foreach ($schema as $k => $v) {
if ($v->grid) {

if ($v->name == 'status') {
$BODY_VALUES .= " <td>
	                                    	<?php if (Mn_Util::isCadastroAccess(xthis->getController(), 'editar')) : ?>
	                                    	<a onclick=\"javascript:statusChange(<?php echo xrow[\"id\"]?>, this);\" href=\"javascript:;\"><span class=\"label <?php echo (xrow['status']=='Ativo') ? 'label-success' : 'label-important' ?>\"><?php echo xrow['status'] ?></span></a>
	                                    	<xphp else : ?>
	                                    	<?php echo xrow['status'] ?>
	                                    	<?php endif; ?>
	                                    </td>";
} else {
if ($v->type == 'decimal') {
$BODY_VALUES .= "<td><?php echo number_format(xrow['" . $v->name . "'],2,',','.') ?></td>";
} else if ($v->imagem) {
$BODY_VALUES .= "<td><?php echo xthis->getImagem(xrow['" . $v->name . "'], 80) ?></td>";
} else if ($v->arquivo) {
$BODY_VALUES .= "<td>
								<?php if ((int)xrow['" . $v->name . "']>0) : ?>
								<?php echo xthis->getArquivo((int)xrow['" . $v->name . "']) ?>
								<?php else : ?>
								Nenhum arquivo
								<?php endif ?>
										</td>";
} else if ($v->type == 'date') {
$BODY_VALUES .= "<td><?php echo date('d/m/Y', strtotime(xrow['" . $v->name . "']))?></td>";
} else if ($v->fk) {

$fk = explode(',', $v->fk);
$fk_classe = (isset($fk[0])) ? trim($fk[0]) : 'Classe';
$fk_helper = (isset($fk[1])) ? trim($fk[1]) : 'Helper';
$fk_field = (isset($fk[2])) ? trim($fk[2]) : 'field';

$BODY_VALUES .= "<td><?php
								XcifraoX___rows = " . $fk_classe . "::" . $fk_helper . "(array('id'=>xrow['" . $v->name . "']));
								XcifraoX___row = (sizeof(XcifraoX___rows)>0) ? 	XcifraoX___rows[0] : false;

								echo (XcifraoX___row) ? XcifraoXthis->formatTextoBranco(XcifraoX___row['" . $fk_field . "']) : '--';

								?></td>";
} else if ($v->address) {
$BODY_VALUES .= "<td><?php echo XcifraoXthis->formatEndereco(XcifraoXrow,false,true,true,true,', ', 'e_" . $v->name . "') ?></td>";
} else {
$BODY_VALUES .= "<td><?php echo xrow['" . $v->name . "'] ?></td>";
}
}

$BODY_VALUES .= chr(13) . chr(10);
}
}

$BODY_VALUES = str_replace('xrow', '$row', $BODY_VALUES);
$BODY_VALUES = str_replace('xphp', '?php', $BODY_VALUES);
$BODY_VALUES = str_replace('xthis', '$this', $BODY_VALUES);
$BODY_VALUES = str_replace('XcifraoX', '$', $BODY_VALUES);

$contents = str_replace('<td>#BODY_VALUES</td>', $BODY_VALUES, $contents);
return $contents;
}

private function setViewIndexTheadSearch($contents, $schema) {
$THEAD_SEARCH = '';
foreach ($schema as $k => $v) {
if ($v->grid) {
if ($v->filter) {
if ($v->combo) {

if ($v->fk) {
$fk = explode(',', $v->fk);
$fk_classe = (isset($fk[0])) ? trim($fk[0]) : 'Classe';
$fk_helper = (isset($fk[1])) ? trim($fk[1]) : 'Helper';
$fk_field = (isset($fk[2])) ? trim($fk[2]) : 'field';


$THEAD_SEARCH .= "<th>
	                                    	<div>
                                            	<select name=\"" . $v->name . "\" data-placeholder=\"Todos\" class=\"select\" tabindex=\"2\">
	                                                <option value=\"\">Todos</option>
								                	<?php foreach (" . $fk_classe . "::" . $fk_helper . "(array('status'=>'Ativo','idlicenca'=>Licencas::getLicenca('id'))) as XcifraoXk=>XcifraoXv) : ?>
								                	<option value=\"<?php echo XcifraoXv['id'] ?>\"><?php echo XcifraoXv['" . $fk_field . "'] ?></option>
								                	<?php endforeach;?>
	                                            </select>
	                                    	</div>
	                                    </th>" . chr(13) . chr(10);
;
} else {

$v_name = $v->name;

if ($v_name == 'status')
$v_name = 'status1';

$options = '';
foreach ($v->combo_values as $chave => $valor) {
$options .= "<option value=\"" . $valor . "\">" . $valor . "</option>";
}

$THEAD_SEARCH .= "<th>
	                                    	<div>
                                            	<select name=\"" . $v_name . "\" data-placeholder=\"Todos\" class=\"select\" tabindex=\"2\">
	                                                <option value=\"\">Todos</option>
	                                                " . $options . "
	                                            </select>
	                                    	</div>
	                                    </th>" . chr(13) . chr(10);
;
}
} else if ($v->type == 'date') {

$THEAD_SEARCH .= "<th>
								<input name=\"" . $v->name . "_i\" value=\"<?php echo (isset(xthis->post_var['" . $v->name . "_i'])) ? Mn_Util::formPostVarData(xthis->post_var['" . $v->name . "_i']) : '' ?>\" type=\"text\" style=\"display: block; margin-top: 6px;\" placeholder=\"" . $v->label . " inicial\" class=\"input-block-level\">
								<input name=\"" . $v->name . "_f\" value=\"<?php echo (isset(xthis->post_var['" . $v->name . "_f'])) ? Mn_Util::formPostVarData(xthis->post_var['" . $v->name . "_f']) : '' ?>\" type=\"text\" style=\"display: block; margin-top: 6px;\" placeholder=\"" . $v->label . " final\" class=\"input-block-level\">

										</th>" . chr(13) . chr(10);
} else if ($v->address) {

$__idestado = 'e_' . $v->name . 'idestado';
$__chave = 'e_chave' . $v->name . 'chave';

$THEAD_SEARCH .= "<th>
	                                    	<div>
                                            	<select name=\"" . $__idestado . "\" data-placeholder=\"Estado\" class=\"select\" tabindex=\"2\">
	                                                <option value=\"\">Estado</option>
	                                                <?php foreach(Estados::getEstados() as XcifraoXk=>XcifraoXv) : ?>
                                            		<option value=\"<?php echo XcifraoXv['id'] ?>\"><?php echo XcifraoXv['nome'] ?></option>
                                            		<?php endforeach ?>
	                                            </select>
                                            	<input name=\"" . $__chave . "\" value=\"<?php echo (isset(xthis->post_var['" . $__chave . "'])) ? xthis->post_var['" . $__chave . "'] : '' ?>\" type=\"text\" style=\"display: block; margin-top: 6px;\" placeholder=\"Rua, Bairro ou Cidade\" class=\"input-block-level\">
	                                    	</div>
	                                    </th>" . chr(13) . chr(10);
;
} else {
$THEAD_SEARCH .= "<th><input name=\"" . $v->name . "\" value=\"<?php echo (isset(xthis->post_var['" . $v->name . "'])) ? xthis->post_var['" . $v->name . "'] : '' ?>\" type=\"text\" style=\"display: block; margin-top: 6px;\" placeholder=\"" . $v->label . "\" class=\"input-block-level\"></th>" . chr(13) . chr(10);
}
} else {
$THEAD_SEARCH .= '<th></th>' . chr(13) . chr(10);
}
}
}

$THEAD_SEARCH = str_replace('XcifraoX', '$', $THEAD_SEARCH);
$THEAD_SEARCH = str_replace('xthis', '$this', $THEAD_SEARCH);

$contents = str_replace('<th>#THEAD_SEARCH</th>', $THEAD_SEARCH, $contents);
return $contents;
}

private function setViewIndexTheadTitle($contents, $schema) {
$THEAD_TITLE = '';
foreach ($schema as $k => $v) {
if ($v->grid) {
if ($v->filter) {
$THEAD_TITLE .= '<th class="s-' . $v->name . '">' . $v->label . '</th>' . chr(13) . chr(10);
} else {
$THEAD_TITLE .= '<th>' . $v->label . '</th>' . chr(13) . chr(10);
}
}
}

$contents = str_replace('<th>#THEAD_TITLE</th>', $THEAD_TITLE, $contents);
return $contents;
}

private function setTableAddress($contents, $schema) {
$TABLE_ADDRESS = '';
$TABLE_ADDRESS_FIELDS = '';
foreach ($schema as $k => $v) {
if ($v->address) {

$prefix = 'e_' . $v->name;
$prefix_es = 'es_' . $v->name;


$TABLE_ADDRESS_FIELDS .= "XcifraoXfields .= \", $prefix.cep as " . $prefix . "cep, $prefix.endereco as " . $prefix . "endereco, $prefix.numero as " . $prefix . "numero, $prefix.complemento as " . $prefix . "complemento, $prefix.bairro as " . $prefix . "bairro, $prefix.idestado as " . $prefix . "idestado,$prefix.cidade as " . $prefix . "cidade,$prefix_es.uf as " . $prefix . "uf,$prefix_es.uf as " . $prefix . "estado \"; ";


$TABLE_ADDRESS .= '						LEFT JOIN enderecos ' . $prefix . ' ON ' . $prefix . '.id=PREFIX_T1.' . $v->name . '
						LEFT JOIN estados ' . $prefix_es . ' ON ' . $prefix_es . '.id=' . $prefix . '.idestado	';
}
}

$TABLE_ADDRESS_FIELDS = str_replace('XcifraoX', '$', $TABLE_ADDRESS_FIELDS);

$contents = str_replace('$TABLE_ADDRESS_FIELDS', $TABLE_ADDRESS_FIELDS, $contents);
$contents = str_replace('TABLE_ADDRESS', $TABLE_ADDRESS, $contents);

return $contents;
}

private function setGravaDadosDb($contents, $schema) {
$GRAVA_DADOS_DB = '';
foreach ($schema as $k => $v) {
if ($v->name == 'id')
continue;

if ($v->name == 'datacriacao') {
$GRAVA_DADOS_DB .= '		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						' . chr(13) . chr(10);
} else {
$GRAVA_DADOS_DB .= '		$row->' . $v->name . ' = (array_key_exists("' . $v->name . '",$dados)) ? $dados["' . $v->name . '"] : $row->' . $v->name . ';' . chr(13) . chr(10);
}
}

$contents = str_replace('$GRAVA_DADOS_DB;', $GRAVA_DADOS_DB, $contents);
return $contents;
}

private function setDbAttrFilters($contents, $schema, $tabela) {
$t = substr($tabela, 0, 1);


$DB_ATTR_FILTERS = '';
foreach ($schema as $k => $v) {
if ($v->filter) {


if ($v->address) {

$__idestado = 'e_' . $v->name . 'idestado';
$__chave = 'e_chave' . $v->name . 'chave';

$DB_ATTR_FILTERS .= '		$' . $__idestado . ' = (isset($queries["' . $__idestado . '"])) ? $queries["' . $__idestado . '"] : false;
		if ($' . $__idestado . ') array_push($where, " e_' . $v->name . '.idestado=$' . $__idestado . ' ");' . chr(13) . chr(10) . chr(13) . chr(10);

$DB_ATTR_FILTERS .= '		$' . $__chave . ' = (isset($queries["' . $__chave . '"])) ? $queries["' . $__chave . '"] : false;
		if ($' . $__chave . ') array_push($where, " (e_' . $v->name . '.endereco LIKE \'%$' . $__chave . '%\' OR e_' . $v->name . '.bairro LIKE \'%$' . $__chave . '%\' OR e_' . $v->name . '.complemento LIKE \'%$' . $__chave . '%\')   ");' . chr(13) . chr(10) . chr(13) . chr(10);
} else if ($v->type == 'varchar') {
$DB_ATTR_FILTERS .= '		$' . $v->name . ' = (isset($queries["' . $v->name . '"])) ? $queries["' . $v->name . '"] : false;
		if ($' . $v->name . ') array_push($where, " ' . $t . '1.' . $v->name . ' LIKE \'%$' . $v->name . '%\' ");' . chr(13) . chr(10) . chr(13) . chr(10);
} else if ( ($v->type == 'decimal') || ($v->type == 'bigint')) {
$DB_ATTR_FILTERS .= '		$' . $v->name . ' = (isset($queries["' . $v->name . '"])) ? $queries["' . $v->name . '"] : false;
		if ($' . $v->name . ') array_push($where, " ' . $t . '1.' . $v->name . ' = $' . $v->name . ' ");' . chr(13) . chr(10) . chr(13) . chr(10);
} else if ($v->type == 'date') {

$DB_ATTR_FILTERS .= '		$' . $v->name . '_i = (isset($queries["' . $v->name . '_i"])) ? $queries["' . $v->name . '_i"] : false;
		if ($' . $v->name . '_i) array_push($where, " ' . $t . '1.' . $v->name . ' >= \'$' . $v->name . '_i\' ");' . chr(13) . chr(10) . chr(13) . chr(10);

$DB_ATTR_FILTERS .= '		$' . $v->name . '_f = (isset($queries["' . $v->name . '_f"])) ? $queries["' . $v->name . '_f"] : false;
		if ($' . $v->name . '_f) array_push($where, " ' . $t . '1.' . $v->name . ' <= \'$' . $v->name . '_f\' ");' . chr(13) . chr(10) . chr(13) . chr(10);
} else {


$DB_ATTR_FILTERS .= '		$' . $v->name . ' = (isset($queries["' . $v->name . '"])) ? $queries["' . $v->name . '"] : false;
		if ($' . $v->name . ') array_push($where, " ' . $t . '1.' . $v->name . ' = \'$' . $v->name . '\' ");' . chr(13) . chr(10) . chr(13) . chr(10);
}
}
}

$contents = str_replace('$DB_ATTR_FILTERS;', $DB_ATTR_FILTERS, $contents);
return $contents;
}

private function setControllerGetFieldsFront($contents, $schema) {
$GET_FIELDS_FRONT = '';
foreach ($schema as $k => $v) {
if (!$v->system) {
if ($v->name == 'status') {
$GET_FIELDS_FRONT .= '		$status = trim($this->getRequest()->getPost("status1"));' . chr(13) . chr(10);
} else {

if ($v->type == 'decimal') {
$GET_FIELDS_FRONT .= '		$' . $v->name . ' = MN_Util::trataNum(trim($this->getRequest()->getPost("' . $v->name . '")));' . chr(13) . chr(10);
} else if ($v->type == 'bigint') {
$GET_FIELDS_FRONT .= '		$' . $v->name . ' = (int)trim($this->getRequest()->getPost("' . $v->name . '"));' . chr(13) . chr(10);
} else if ($v->type == 'date') {
$GET_FIELDS_FRONT .= '		$' . $v->name . ' = Mn_Util::stringToTime($this->getRequest()->getPost("' . $v->name . '"));' . chr(13) . chr(10);
} else {
$GET_FIELDS_FRONT .= '		$' . $v->name . ' = trim($this->getRequest()->getPost("' . $v->name . '"));' . chr(13) . chr(10);
}
}
}
}

$contents = str_replace('$GET_FIELDS_FRONT;', $GET_FIELDS_FRONT, $contents);
return $contents;
}

private function setControllerRequired($contents, $schema) {
$REQUIRED = '';
foreach ($schema as $k => $v) {
if ($v->required) {

if ( ($v->type == 'decimal') || ($v->type == 'bigint')) {
$REQUIRED .= '		if (0==$' . $v->name . ') array_push($erros, "Informe a ' . ($v->label) . '.");' . chr(13) . chr(10);
} else {
$REQUIRED .= '		if (""==$' . $v->name . ') array_push($erros, "Informe a ' . ($v->label) . '.");' . chr(13) . chr(10);
}
}
}

$contents = str_replace('$REQUIRED;', $REQUIRED, $contents);
return $contents;
}

private function setControllerFilters($contents, $schema) {
$FILTER_HERE = '';
foreach ($schema as $k => $v) {
if ( ($v->grid) && ($v->filter)) {
if ($v->name == 'status') {
$FILTER_HERE .= '    		if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];' . chr(13) . chr(10);
} else if ($v->type == 'date') {

$FILTER_HERE .= '    		if ($this->view->post_var["' . $v->name . '_i"]!="") $queries["' . $v->name . '_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["' . $v->name . '_i"]));' . chr(13) . chr(10);
$FILTER_HERE .= '    		if ($this->view->post_var["' . $v->name . '_f"]!="") $queries["' . $v->name . '_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["' . $v->name . '_f"]));' . chr(13) . chr(10);
} else if ($v->address) {

$__idestado = 'e_' . $v->name . 'idestado';
$__chave = 'e_chave' . $v->name . 'chave';

$FILTER_HERE .= '    		if ($this->view->post_var["' . $__idestado . '"]!="") $queries["' . $__idestado . '"] = (int)$this->view->post_var["' . $__idestado . '"];' . chr(13) . chr(10);
$FILTER_HERE .= '    		if ($this->view->post_var["' . $__chave . '"]!="") $queries["' . $__chave . '"] = $this->view->post_var["' . $__chave . '"];' . chr(13) . chr(10);
} else {
$FILTER_HERE .= '    		if ($this->view->post_var["' . $v->name . '"]!="") $queries["' . $v->name . '"] = $this->view->post_var["' . $v->name . '"];' . chr(13) . chr(10);
}
}
}

$contents = str_replace('$FILTER_HERE;', $FILTER_HERE, $contents);
return $contents;
}

private function setControllerUnique($contents, $schema, $tabela) {
$UNIQUE = '';
foreach ($schema as $k => $v) {
if ($v->unique) {
$UNIQUE .= '		$row = $' . $tabela . '->fetchRow("excluido=\'nao\' AND idlicenca=".Licencas::getLicenca(\'id\')." AND ' . $v->name . '=\'$' . $v->name . '\' AND id<>".$id);
		if ($row) array_push($erros, "Já existe ' . ($v->label) . ' com esse valor");' . chr(13) . chr(10) . chr(13) . chr(10);
;
}
}

$contents = str_replace('$UNIQUE;', $UNIQUE, $contents);
return $contents;
}

private function setAttrValuesToDb($contents, $schema) {
$ATTR_VALUES_TO_DB = '';
foreach ($schema as $k => $v) {
if (!$v->system) {

if ($v->imagem) {
$ATTR_VALUES_TO_DB .= chr(13) . chr(10) . "			XCIFRAOX" . $v->name . " = XCIFRAOXthis->getImagem('" . $v->name . "');
			if (XCIFRAOX" . $v->name . "!=0) XCIFRAOXdados['" . $v->name . "'] = XCIFRAOX" . $v->name . ";" . chr(13) . chr(10);
} else if ($v->arquivo) {
$ATTR_VALUES_TO_DB .= chr(13) . chr(10) . "			XCIFRAOX" . $v->name . " = XCIFRAOXthis->getArquivo('" . $v->name . "');
			if (XCIFRAOX" . $v->name . "!=0) XCIFRAOXdados['" . $v->name . "'] = XCIFRAOX" . $v->name . ";" . chr(13) . chr(10);
} elseif ($v->type == 'date') {
$ATTR_VALUES_TO_DB .= '			$dados["' . $v->name . '"] = date("Y-m-d", $' . $v->name . ');' . chr(13) . chr(10);
;
} elseif ($v->address) {

$prefix = "e_" . $v->name;

$ATTR_VALUES_TO_DB = "			XCIFRAOXenderecosModel = new EnderecoModel(XCIFRAOXthis);
			XCIFRAOXendereco = XCIFRAOXenderecosModel->getPost( (XCIFRAOX_MSYStemplate) ? (int)XCIFRAOX_MSYStemplate['" . $v->name . "'] : 0, \"" . $prefix . "\");
			XCIFRAOX" . $v->name . " = (XCIFRAOXendereco) ? XCIFRAOXendereco['id'] : 0;
			XCIFRAOXdados['" . $v->name . "'] = XCIFRAOX" . $v->name . ";" . chr(13) . chr(10) . chr(13) . chr(10);
} else {
$ATTR_VALUES_TO_DB .= '			$dados["' . $v->name . '"] = $' . $v->name . ';' . chr(13) . chr(10);
;
}
}
}

$ATTR_VALUES_TO_DB = str_replace('XCIFRAOX', '$', $ATTR_VALUES_TO_DB);

$contents = str_replace('$ATTR_VALUES_TO_DB;', $ATTR_VALUES_TO_DB, $contents);
return $contents;
}

private function getColumnsSchema($tabela) {
$config = Zend_Controller_Front::getInstance()->getParam('bootstrap');
$resources = $config->getOption('resources');
$db = $resources['db']['params']['dbname'];

$strsql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$db' AND TABLE_NAME = '$tabela';";
$db = Zend_Registry::get('db');
$_rows = $db->fetchAll($strsql);

$system = array('id', 'datacriacao', 'idlicenca', 'excluido', 'logdata', 'logusuario');

$rows = array();
foreach ($_rows as $k => $v) {
$obj = new stdClass;
$obj->name = $v['COLUMN_NAME'];
$obj->type = $v['DATA_TYPE'];
$obj->maxlength = (is_null($v['CHARACTER_MAXIMUM_LENGTH'])) ? false : (int) $v['CHARACTER_MAXIMUM_LENGTH'];
$obj->decimal = (is_null($v['NUMERIC_SCALE'])) ? false : (int) $v['NUMERIC_SCALE'];

$comments = explode('|', $v['COLUMN_COMMENT']);
foreach ($comments as $chave => $valor)
$comments[$chave] = trim($valor);

$obj->required = (false !== array_search('required', $comments));
$obj->filter = (false !== array_search('filter', $comments));
$obj->system = (false !== array_search($obj->name, $system));
$obj->unique = (false !== array_search('unique', $comments));
$obj->grid = (false !== array_search('grid', $comments));
$obj->imagem = (false !== array_search('imagem', $comments));
$obj->arquivo = (false !== array_search('arquivo', $comments));
$obj->address = (false !== array_search('address', $comments));

$obj->label = $obj->name;
foreach ($comments as $chave => $valor) {
if (strpos($valor, 'label') !== false) {
$valor = explode(':', $valor);
$obj->label = ((isset($valor[1])) && (trim($valor[1]) != '')) ? trim($valor[1]) : $obj->name;
}
}

$obj->combo = false;
foreach ($comments as $chave => $valor) {
//p. ex.: combo:Ativo,Bloqueado
if (strpos($valor, 'combo') !== false) {
$obj->combo = true;
$valor = explode(':', $valor);
$valor = (isset($valor[1])) ? $valor[1] : false;
if ($valor) {
$valor = explode(',', $valor);
foreach ($valor as $_chave => $_valor)
$valor[$_chave] = trim($_valor);
$obj->combo_values = $valor;
} else {
$obj->combo_values = array();
}
}
}

$obj->tip = false;
foreach ($comments as $chave => $valor) {
if (strpos($valor, 'tip') !== false) {
$valor = explode(':', $valor);
$obj->tip = ((isset($valor[1])) && (trim($valor[1]) != '')) ? trim($valor[1]) : false;
}
}

$obj->fk = false;
foreach ($comments as $chave => $valor) {
if (strpos($valor, 'fk') !== false) {
$valor = explode(':', $valor);
$obj->fk = ((isset($valor[1])) && (trim($valor[1]) != '')) ? trim($valor[1]) : false;
}
}

$obj->number = false;
foreach ($comments as $chave => $valor) {
if (strpos($valor, 'number') !== false) {
$valor = explode(':', $valor);
$obj->number = ((isset($valor[1])) && (trim($valor[1]) != '')) ? trim($valor[1]) : false;
}
}

array_push($rows, $obj);
}

return $rows;
}

}
