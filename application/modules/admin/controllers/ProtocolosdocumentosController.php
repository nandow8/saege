<?php

/**
 * Controle da classe protocolosdocumentos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ProtocolosdocumentosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Protocolosdocumento
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("protocolosdocumentos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$protocolosdocumentos = new Protocolosdocumentos();
		$queries = array();	
		$ultimo = $protocolosdocumentos->getUltimoProtocolodocumento($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;

	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Protocolosdocumentos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Protocolo documento excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="protocolosdocumentos") $objs = new Protocolosdocumentos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'protocolosdocumentos', 'name' => 'Protocolos Documentos'),
			array('url' => null,'name' => 'Visualizar Protocolo documento')
		);
		
		$id = (int)$this->_request->getParam("id");
		$protocolosdocumentos = new Protocolosdocumentos();
		$protocolosdocumento = $protocolosdocumentos->getProtocolosdocumentoById($id, array());
		
		if (!$protocolosdocumento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $protocolosdocumento;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Protocolos Documentos')
		);
		
		$ns = new Zend_Session_Namespace('default_protocolosdocumentos');
		$protocolosdocumentos = new Protocolosdocumentos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
if ($this->view->post_var["datalancamento"]!="") $queries["datalancamento"] = $this->view->post_var["datalancamento"];
if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
if ($this->view->post_var["observacoes"]!="") $queries["observacoes"] = $this->view->post_var["observacoes"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $protocolosdocumentos->getProtocolosdocumentos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $protocolosdocumentos->getProtocolosdocumentos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de protocolosdocumentos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'protocolosdocumentos', 'name' => 'Protocolos Documentos'),
			array('url' => null,'name' => 'Editar Protocolo documento')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$protocolosdocumentos = new Protocolosdocumentos();
		$protocolosdocumento = $protocolosdocumentos->getProtocolosdocumentoById($id);
		
		if (!$protocolosdocumento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $protocolosdocumento;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($protocolosdocumento);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Protocolo documento editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de protocolosdocumentos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'protocolosdocumentos', 'name' => 'Protocolos Documentos'),
			array('url' => null,'name' => 'Adicionar Protocolo documento')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Protocolo documento adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idprotocolosdocumento
     */    
    private function preForm($idprotocolosdocumento = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_protocolosdocumento = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
$datalancamento = trim($this->getRequest()->getPost("datalancamento"));
$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
$idtipoprotocolo = (int)trim($this->getRequest()->getPost("idtipoprotocolo"));
$tipo = trim($this->getRequest()->getPost("tipo"));
$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
$origem = trim($this->getRequest()->getPost("origem"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$titulo = trim($this->getRequest()->getPost("titulo"));
$tiposolicitacao = trim($this->getRequest()->getPost("tiposolicitacao"));
$solicitacao = trim($this->getRequest()->getPost("solicitacao"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
$encaminhar = (int)trim($this->getRequest()->getPost("encaminhar"));
$posicao = trim($this->getRequest()->getPost("posicao"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idtipoprotocolo) array_push($erros, "Informe a Tipo Processo.");
if (""==$tipo) array_push($erros, "Informe a Tipo.");
if (0==$idusuariologado) array_push($erros, "Informe a Usuários.");
if (0==$idescola) array_push($erros, "Informe a Escola.");
if (""==$titulo) array_push($erros, "Informe a Título.");
if (""==$posicao) array_push($erros, "Informe a Posição.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$protocolosdocumentos = new Protocolosdocumentos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["sequencial"] = $sequencial;
$dados["datalancamento"] = $datalancamento;
$dados["horalancamento"] = $horalancamento;
$dados["idtipoprotocolo"] = $idtipoprotocolo;
$dados["tipo"] = $tipo;
$dados["idusuariologado"] = $idusuariologado;
$dados["origem"] = $origem;
$dados["idescola"] = $idescola;
$dados["iddepartamento"] = $iddepartamento;
$dados["titulo"] = $titulo;
$dados["tiposolicitacao"] = $tiposolicitacao;
$dados["solicitacao"] = $solicitacao;
$dados["observacoes"] = $observacoes;

$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;
$dados["encaminhar"] = $encaminhar;
$dados["posicao"] = $posicao;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $protocolosdocumentos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}