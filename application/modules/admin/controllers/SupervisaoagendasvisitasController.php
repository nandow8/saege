<?php

/**
 * Controle da classe supervisaoagendasvisitas do sistema
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_SupervisaoagendasvisitasController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Agendamentosvisita
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("supervisaoagendasvisitas", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    public function calendarioAction() {
        $this->view->bread_crumb = array(
            array('url' => 'supervisaoagendasvisitas', 'name' => 'Visitas Agendadas'),
            array('url' => null, 'name' => 'Calendário de supervisão.')
        );

        $supervisaoagendasvisitas = new Agendamentosvisitas();
        $queries = array();
        $_data = Usuarios::getUsuarios(array('id' => Usuarios::getUsuario('id')));
        $_data = $_data[0];
        //if($_data) $queries['iddepartamento'] = $_data['idperfil'];

        $this->view->rows = $supervisaoagendasvisitas->getAgendamentosvisitas($queries, '0', '5');
    }

    public function setcalendarioAction() {
        $this->_helper->layout->disableLayout();
        $this->preForm();

        $page = (int) $this->getRequest()->getPost("page");

        $this->view->page = $page;
    }

    public function seteventoAction() {
        $this->_helper->layout->disableLayout();

        $id = $this->getRequest()->getParam('id');

        $comunicacaoagenda = new Agendamentosvisitas();
        $evento = $comunicacaoagenda->getAgendamentosvisitaById($id);

        if (!$evento)
            die('Não Encontrado');

        $this->view->post_var = $evento;
    }

    public function seteventosdiaAction() {
        $this->_helper->layout->disableLayout();

        $dia = $this->getRequest()->getParam('dia');

        $queries = array();
        $queries['datainicio_i'] = date('Y-m-d', $dia);
        $queries['datainicio_f'] = date('Y-m-d', $dia);
        $queries['order'] = "ORDER BY a1.datainicio DESC";

        $comunicacaoagenda = new Agendamentosvisitas();
        $rows = $comunicacaoagenda->getAgendamentosvisitas($queries);

        if (!$rows)
            die('Não Encontrado');
        $this->view->dia = $dia;
        $this->view->rows = $rows;
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Agendamentosvisitas();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Visita Agendada excluído com sucesso.";

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "supervisaoagendasvisitas")
            $objs = new Agendamentosvisitas();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'supervisaoagendasvisitas', 'name' => 'Visitas Agendadas'),
            array('url' => null, 'name' => 'Visualizar Visita Agendada')
        );

        $id = (int) $this->_request->getParam("id");
        $supervisaoagendasvisitas = new Agendamentosvisitas();
        $agendamentosvisita = $supervisaoagendasvisitas->getAgendamentosvisitaById($id, array());

        if (!$agendamentosvisita)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $agendamentosvisita;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Visitas Agendadas')
        );

        $ns = new Zend_Session_Namespace('default_supervisaoagendasvisitas');
        $supervisaoagendasvisitas = new Agendamentosvisitas();
        $queries = array();

        $_data = Usuarios::getUsuarios(array('id' => Usuarios::getUsuario('id')));
        $_data = $_data[0];

        //if($_data) $queries['iddepartamento'] = $_data['idperfil'];
        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ($this->view->post_var["datainicio_i"] != "")
                $queries["datainicio_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_i"]));
            if ($this->view->post_var["datainicio_f"] != "")
                $queries["datainicio_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_f"]));
            if ($this->view->post_var["titulo"] != "")
                $queries["titulo"] = $this->view->post_var["titulo"];
            if ($this->view->post_var["descricoes"] != "")
                $queries["descricoes"] = $this->view->post_var["descricoes"];
            if ($this->view->post_var["status1"] != "")
                $queries["status"] = $this->view->post_var["status1"];

            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $supervisaoagendasvisitas->getAgendamentosvisitas($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $supervisaoagendasvisitas->getAgendamentosvisitas($queries, $paginaAtual, $maxpp);
    }

    /**
     *
     * Action de edição de supervisaoagendasvisitas
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'supervisaoagendasvisitas', 'name' => 'Visitas Agendadas'),
            array('url' => null, 'name' => 'Editar Visita Agendada')
        );

        $id = (int) $this->_request->getParam("id");
        $supervisaoagendasvisitas = new Agendamentosvisitas();
        $agendamentosvisita = $supervisaoagendasvisitas->getAgendamentosvisitaById($id);

        if (!$agendamentosvisita)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $agendamentosvisita;
        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($agendamentosvisita);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Visita Agendada editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     *
     * Action de adição de supervisaoagendasvisitas
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'supervisaoagendasvisitas', 'name' => 'Visitas Agendadas'),
            array('url' => null, 'name' => 'Adicionar Visita Agendada')
        );

        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Visita Agendada adicionado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idagendamentosvisita
     */
    private function preForm($idagendamentosvisita = 0) {

    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_agendamentosvisita = false) {
        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $iddepartamento = (int) trim($this->getRequest()->getPost("iddepartamento"));
        $datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
        $horainicio = trim($this->getRequest()->getPost("horainicio"));
        $datafim = Mn_Util::stringToTime($this->getRequest()->getPost("datafim"));
        $horafim = trim($this->getRequest()->getPost("horafim"));
        $titulo = trim($this->getRequest()->getPost("titulo"));
        $descricoes = trim($this->getRequest()->getPost("descricoes"));
        $observações = trim($this->getRequest()->getPost("observações"));
        $status = trim($this->getRequest()->getPost("status1"));


        $erros = array();

        if ("" == $datainicio)
            array_push($erros, "Informe a Data.");
        if ("" == $titulo)
            array_push($erros, "Informe a Título.");
        if ("" == $status)
            array_push($erros, "Informe a Status.");


        $supervisaoagendasvisitas = new Agendamentosvisitas();



        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;

            $dados["iddepartamento"] = $iddepartamento;
            $dados["datainicio"] = date("Y-m-d", $datainicio);
            $dados["horainicio"] = $horainicio;
            $dados["datafim"] = date("Y-m-d", $datafim);
            $dados["horafim"] = $horafim;
            $dados["titulo"] = $titulo;
            $dados["descricoes"] = $descricoes;
            $dados["observações"] = $observações;
            $dados["status"] = $status;


            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            ;
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $supervisaoagendasvisitas->save($dados);

            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();

            $db->rollBack();
            die();
        }

        return "";
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

}
