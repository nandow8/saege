<?php

/**
 * Controle da classe comunicacaocomunicados do sistema
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ComunicacaocomunicadosController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Comunicacaocomunicado
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("comunicacaocomunicados", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Comunicacaocomunicados();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Comunicado excluído com sucesso.";
            
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Comunicado excluído com sucesso!");

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "comunicacaocomunicados")
            $objs = new Comunicacaocomunicados();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);
            
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Status do comunicado ".($obj['status'] == "Ativo") ? "Bloqueado" : "Ativado"." com sucesso!");

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    public function getusuariosAction() {
        $this->_helper->layout->disableLayout();
//$this->_response->setHeader("content-type", "text/xml");
//$id = (int)$this->getRequest()->getPost('idlocal');
        $id = (int) $this->getRequest()->getPost('id');
        $idescola = (int) $this->getRequest()->getPost('idescola');
        $dpt = (int) $this->getRequest()->getPost('departamento');
//var_dump($idescola);die();
//$professores = new Funcionariosgeraisescolas();
        $usuarios = new Usuarios();

        $queries = array();
        $queries['status'] = 'Ativo';
        if ($id == 29 AND empty($idescola))
            $idescola = -1;

        if ($id)
            $queries['idperfil'] = $id;

        if (($id == 28 || $id == 29) && $idescola) {
            $queries['idescola'] = $idescola;

            if ($idescola == -1) {
                $queries['idescola'] = false;
            }
            $rows = $usuarios->getUsuariosEscola($queries);
        } else {
            $rows = $usuarios->getUsuarios($queries);
        }

//$queries['idsecretaria'] = $this->view->idsecretaria;
//var_dump($queries); die();
//$rows = $professores->getFuncionariosgeraisescolas($queries);

        if ($id == 29 && $idescola == 0)
            $rows = array();

        echo json_encode($rows);
        die();
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'comunicacaocomunicados', 'name' => 'Comunicados'),
            array('url' => null, 'name' => 'Visualizar Comunicado')
        );

        $id = (int) $this->_request->getParam("id");
        $comunicacaocomunicados = new Comunicacaocomunicados();
        $comunicacaocomunicado = $comunicacaocomunicados->getComunicacaocomunicadoById($id, array());

        if ($comunicacaocomunicado['idperfil'] == '28')
            $comunicacaocomunicado['sendTo'] = 'professor';
        if ($comunicacaocomunicado['idperfil'] == '29')
            $comunicacaocomunicado['sendTo'] = 'diretoria';

        if (!$comunicacaocomunicado)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $comunicacaocomunicado;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Comunicados')
        );

        $ns = new Zend_Session_Namespace('default_comunicacaocomunicados');
        $comunicacaocomunicados = new Comunicacaocomunicados();
        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $idescola = Usuarios::getUsuario('idescola');
        $queries['idescola'] = (int) $idescola;
        
        if ($idescola <= 0) {
            $queries['idperfil'] = Usuarios::getUsuario('idperfil');
            $queries['verificaperfil'] = true;
        } elseif (Usuarios::getUsuario('professor') == "Sim") {
            $queries['idsprofessores'] = Usuarios::getUsuario('idfuncionario');
            $queries['verificaescola'] = true;
        } else {
            $queries['verificaescola'] = true;
        }
        $idperfil = Usuarios::getUsuario('idperfil');
        
        if ($idperfil != 30) {
            $queries['idusuariocriacao'] = Usuarios::getUsuario('id');
        }

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ($this->view->post_var["titulo"] != "")
                $queries["titulo"] = $this->view->post_var["titulo"];

            if ($this->view->post_var["idperfilorigem"] != "")
                $queries["perfilorigem"] = $this->view->post_var["idperfilorigem"];

            if ($this->view->post_var["idescolaorigem"] != "")
                $queries["escolaorigem"] = $this->view->post_var["idescolaorigem"];

            if (isset($this->view->post_var["idperfildestino"]) AND $this->view->post_var["idperfildestino"] != "")
                $queries["perfildestino"] = $this->view->post_var["idperfildestino"];

            if ($this->view->post_var["idescoladestino"] != "")
                $queries["escoladestino"] = $this->view->post_var["idescoladestino"];

            //if ($this->view->post_var["status1"] != "")
            //$queries["status"] = $this->view->post_var["status1"];

            if ($this->view->post_var["datacriacao"] != "")
                $queries["datacriacao"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datacriacao"]));

            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $comunicacaocomunicados->getComunicacaocomunicados($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $comunicacaocomunicados->getComunicacaocomunicados($queries, $paginaAtual, $maxpp);
    }

    /**
     *
     * Action de edição de comunicacaocomunicados
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'comunicacaocomunicados', 'name' => 'Comunicados'),
            array('url' => null, 'name' => 'Editar Comunicado')
        );

        $id = (int) $this->_request->getParam("id");
        $comunicacaocomunicados = new Comunicacaocomunicados();
        $comunicacaocomunicado = $comunicacaocomunicados->getComunicacaocomunicadoById($id);

        if (!$comunicacaocomunicado)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $comunicacaocomunicado;
        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($comunicacaocomunicado);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Comunicado editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     *
     * Action de adição de comunicacaocomunicados
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'comunicacaocomunicados', 'name' => 'Comunicados'),
            array('url' => null, 'name' => 'Adicionar Comunicado')
        );

        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Comunicado adicionado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idcomunicacaocomunicado
     */
    private function preForm($idcomunicacaocomunicado = 0) {
        $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');

        $professores = new Funcionariosgeraisescolas();
        $this->view->professores = $professores->getFuncionariosgeraisescolas(array('status' => 'Ativo', 'idsecretaria' => $this->view->idsecretaria));
        $escolas = new Escolas();
        $this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_comunicacaocomunicado = false) {
        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $idsecretaria = (int) Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');

        $idimagem = (int) trim($this->getRequest()->getPost("idimagem"));
        //$idlocal = (int)trim($this->getRequest()->getPost("idlocal"));
        $idescola = (int) trim($this->getRequest()->getPost("idescola"));
        //$idsescolas = (int) trim($this->getRequest()->getPost("idsescolas"));
        $idlocais = trim($this->getRequest()->getPost("idlocais"));
        $idperfil = (int) trim($this->getRequest()->getPost("idperfil"));
        $sendTo = trim($this->getRequest()->getPost("sendTo"));
        $idsdepartamentossecretaria = trim($this->getRequest()->getPost("idsdepartamentossecrtaria"));
        $idsprofessores = $this->getRequest()->getPost("idsprofessores");

        if (!is_array($idsprofessores)) {
            $idsprofessores = array();
        }
        $idsprofessores = implode(',', $idsprofessores);
        $titulo = trim($this->getRequest()->getPost("titulo"));
        $descricoes = trim($this->getRequest()->getPost("descricoes"));
        $observacoes = trim($this->getRequest()->getPost("observacoes"));
        $status = trim($this->getRequest()->getPost("status1"));
        $setescola = trim($this->getRequest()->getPost("setescola"));

        if ($sendTo AND $sendTo === 'professor')
            $idperfil = 28;

        //pegar os ids das escolas
        if ($idescola == '' || $idescola == '0' || $idescola == '-1') {
            $idsescolas = array();
            $idsAtivos = explode(',', $idsprofessores);

            if (sizeof($idsAtivos) > 0) {

                foreach ($idsAtivos as $k => $v) {
                    $escola = Usuarios::getEscolaAtiva($v);
                    if ($escola)
                        array_push($idsescolas, $escola['id']);
                }
            }
            $idsescolas = implode(',', $idsescolas);
        }

        $erros = array();

        if ("" == $titulo)
            array_push($erros, "Informe a Título.");
        if ("" == $idperfil)
            array_push($erros, "Informe o Departamento.");


        $comunicacaocomunicados = new Comunicacaocomunicados();


        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;

            $dados["idsecretaria"] = $idsecretaria;
            $dados["idimagem"] = $idimagem;
            //$dados["idlocal"] = $idlocal;
            $dados["idperfil"] = $idperfil;
            $dados["sendTo"] = $sendTo;

            $dados["idescola"] = (($idescola > 0 && $setescola == 'sim') || $idescola == -1) ? $idescola : 0;
            //$dados["idlocais"] = $idlocais;
            $dados["idsdepartamentossecretaria"] = $idsdepartamentossecretaria;
            $dados["idsprofessores"] = $idsprofessores;
            $dados["idsescolas"] = $idsescolas;
            $dados["titulo"] = $titulo;
            $dados["descricoes"] = $descricoes;
            $dados["observacoes"] = $observacoes;
            $dados["status"] = 'Ativo';
            if ($this->getRequest()->getActionName() == 'adicionar') {
                $dados["idusuariocriacao"] = Usuarios::getUsuario('id');
                $_idedscola = Usuarios::getUsuario('idescola');
                if ((int) $_idedscola > 0) {
                    $dados["idescolacriacao"] = Usuarios::getUsuario('idescola');
                }

                $_idperfil = Usuarios::getUsuario('idperfil');
                if ((int) $_idperfil > 0) {
                    $dados['idperfilcriacao'] = Usuarios::getUsuario('idperfil');
                }
            }

            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $comunicacaocomunicados->save($dados);
            //$idteste = $idsprofessores;
            $idsprof = explode(", ", $idsprofessores);


            foreach ($idsprof as $ids):

                $professores = new Funcionariosgeraisescolas();
                $rows = $professores->getFuncionariogeralescolaById($ids);

                //$email = strip_tags(trim($this->getRequest()->getPost("email")));

                Usuarios::sendComunicacao($rows["nome"], $rows["email"], $titulo, $descricoes, $observacoes);

            endforeach;

            /*
              //SE enviar comunicado para todas as escolas
              if($idescola == -1)
              {
              foreach (Escolas::getEscolasHelper(array('status'=>'Ativo')) as $k=>$v) :

              Usuarios::sendComunicacao($v["escola"], $v["email"], $titulo, $descricoes, $observacoes);

              endforeach;
              }
             */

            //$message = new Zend_Session_Namespace("message");
            //$message->crudmessage = "Envios realizados com sucesso.";

            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();

            $db->rollBack();
            die();
        }

        return "";
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

}
