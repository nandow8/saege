<?php

/**
 * Controle da classe lancamentosdespesas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
error_reporting(0);
class Admin_RelatoriosfinanceirosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Lancamentosdespesa
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("lancamentosdespesas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}	
	
	
	public function geraapmAction() {
		$id = (int)$this->_request->getParam("id");
		$idescola = (int)$this->_request->getParam("idescola");

		$apm = Escolasapms::getEscolaapmByIdEscolaHelper($idescola, array('tipo' => 'APM'));
		$composicao_apm = Escolasapmsgrupos::getEscolasapmsgrupoByIdHelper($idescola, array('status' => 'Ativo'));
		
		if (!$composicao_apm) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());

		$this->view->apm = $apm;
		$this->view->composicao_apm = $composicao_apm;

		$escola = Escolas::getEscolaByIdHelper($apm['idescola']);
		$this->view->escola = $escola;


		$conta = Escolasapmscontas::getApmByIdParentHelper($apm['id'], array('idescola' =>$escola['id']));
		if($conta){
			unset($conta['id']);
			unset($conta['excluido']);
			$this->view->apm = array_merge($apm, $conta);
		}
		
		//$this->view->post_var = $declaracao;
		$endereco = Enderecos::getEnderecoById((int)$escola['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->endereco = $endereco;
		}
		
		$this->view->receitas = Financeirolancamentosreceitas::getFinanceirolancamentosreceitasHelper(array('idescola'=>$escola['id']));
		$this->view->despesas = Lancamentosdespesas::getLancamentosdespesasHelper(array('idescola'=>$escola['id'],'idtipoconvenio' => 1));
		$this->preForm();
		//var_dump($this->view->despesas); die();
		

		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('p');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('Diploma');
		$pdf->SetSubject('Diploma');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		
		
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('relatoriosfinanceiros/pdf/index.phtml');
		//echo $html; die();

		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'relatorio_apm_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}


	public function gerapddeAction() {
		$id = (int)$this->_request->getParam("id");
		$idescola = (int)$this->_request->getParam("idescola");

		$apm = Escolasapms::getEscolaapmByIdEscolaHelper($idescola, array('tipo' => 'PDDE'));
		$composicao_apm = Escolasapmsgrupos::getEscolasapmsgrupoByIdHelper($idescola, array('status' => 'Ativo'));
		
		if (!$composicao_apm)
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());

		$this->view->apm = $apm;
		$this->view->composicao_apm = $composicao_apm;

		$escola = Escolas::getEscolaByIdHelper($apm['idescola']);
		$this->view->escola = $escola;


		$conta = Escolasapmscontas::getApmByIdParentHelper($apm['id'], array('idescola' =>$escola['id']));
		if($conta){
			unset($conta['id']);
			unset($conta['excluido']);
			$this->view->apm = array_merge($apm, $conta);
		}
		
		//$this->view->post_var = $declaracao;
		$endereco = Enderecos::getEnderecoById((int)$escola['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->endereco = $endereco;
		}
		
		$this->view->receitas = Financeirolancamentosreceitas::getFinanceirolancamentosreceitasHelper(array('idescola'=>$escola['id']));
		$this->view->despesas = Lancamentosdespesas::getLancamentosdespesasHelper(array('idescola'=>$escola['id'], 'idtipoconvenio' => 2));
		$this->preForm();
		//var_dump($this->view->despesas); die();
		

		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('p');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('Diploma');
		$pdf->SetSubject('Diploma');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		
		
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('relatoriosfinanceiros/pdf/pdde.phtml');
		//echo $html; die();

		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'relatorio_pdde_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}

	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Relatorios APMs e PDDE')
		);
		
		$this->preForm();
		$ns = new Zend_Session_Namespace('default_relatoriosfinanceiros');
		$lancamentosdespesas = new Lancamentosdespesas();
		$queries = array();	
		$queries['idescola'] = Usuarios::getUsuario('idescola');
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idtipoconvenio"]!="") $queries["idtipoconvenio"] = $this->view->post_var["idtipoconvenio"];
//if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $lancamentosdespesas->getLancamentosdespesas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $lancamentosdespesas->getLancamentosdespesas($queries, $paginaAtual, $maxpp);	

		if($this->getRequest()->getParam('to') == 'pdf')
		{
			$tipo = $this->getRequest()->getParam('tipo');
			$this->geraexcel($queries, $tipo);
		}
	}


	private function geraexcel($queries, $tipo = 'APM') 
	{
		$idescola = (int)Usuarios::getUsuario('idescola');
		$idescola = ($idescola == 0) ? $queries['idescola'] : $idescola;

		$apm = Escolasapms::getEscolaapmByIdEscolaHelper($idescola, array('tipo' => $tipo));
		$composicao_apm = Escolasapmsgrupos::getEscolasapmsgrupoByIdHelper($idescola, array('status' => 'Ativo'));
		
		if (!$composicao_apm) 
		{
			$messageNameSpace = new Zend_Session_Namespace("message");
			$messageNameSpace->crudmessage = "Nenhuma APM encontrada. Selecione uma UNIDADE ESCOLAR.";
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		}

		$this->view->datainicio = isset($queries['data_i']) ? date('d/m/Y', strtotime($queries['data_i'])) : '--';
		$this->view->datafim = isset($queries['data_f']) ? date('d/m/Y', strtotime($queries['data_f'])) : '--';

		$this->view->apm = $apm;
		$this->view->composicao_apm = $composicao_apm;

		$escola = Escolas::getEscolaByIdHelper($apm['idescola']);
		$this->view->escola = $escola;

		$conta = Escolasapmscontas::getApmByIdParentHelper($apm['id'], array('idescola' =>$escola['id']));
		if($conta){
			unset($conta['id']);
			unset($conta['excluido']);
			$this->view->apm = array_merge($apm, $conta);
		}
		
		//$this->view->post_var = $declaracao;
		$endereco = Enderecos::getEnderecoById((int)$escola['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->endereco = $endereco;
		}

		$financeirotiposconvenios = new Financeirotiposconvenios();
		$tipoconvenio = $financeirotiposconvenios->fetchRow("titulo LIKE '%$tipo%'");
		$idtipoconvenio = ($tipoconvenio) ? $tipoconvenio['id'] : 0;
		$idtipoconvenio = (isset($queries['idtipoconvenio']) && (int)$queries['idtipoconvenio'] > 0) ? (int)$queries['idtipoconvenio'] : $idtipoconvenio;
		//var_dump($idtipoconvenio); die();
		
		$this->view->receitas = Financeirolancamentosreceitas::getFinanceirolancamentosreceitasHelper(array('idescola'=>$idescola, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'idtipoconvenio' => $idtipoconvenio));
		$this->view->total_receitas = Financeirolancamentosreceitas::getFinanceirolancamentosreceitasHelper(array('idescola'=>$idescola, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'sum' => true, 'idtipoconvenio' => $idtipoconvenio));

		//var_dump($this->view->total_receitas); die();
		$this->view->despesas = Lancamentosdespesas::getLancamentosdespesasHelper(array('idescola'=>$idescola, 'idtipoconvenio' => $idtipoconvenio, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f']));

		$this->view->total_despesas = Lancamentosdespesas::getLancamentosdespesasHelper(array('idescola'=>$idescola, 'idtipoconvenio' => $idtipoconvenio, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'sum' => true));
		//var_dump($this->view->total_despesas); die();

		$this->preForm();
		//var_dump($this->view->despesas); die();
		

		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('p');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('Diploma');
		$pdf->SetSubject('Diploma');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		
		
		// add a page
		$pdf->AddPage();
		
		$html = $this->view->render('relatoriosfinanceiros/pdf/index.phtml');
		if($tipo == 'PDDE')
		{
			$html = $this->view->render('relatoriosfinanceiros/pdf/pdde.phtml');
		}
		//echo $html; die();

		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'relatorio_financeiro_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}
	
    
    /**
     * Atribui valores ao view
     * @param int $idlancamentosdespesa
     */    
    private function preForm($idlancamentosdespesa = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
    
}