<?php

// class MYPDF extends TCPDF {

//     //Page header
//     public function Header() {
//         $image_file = "public/admin/imagens/logosantaisabel.jpg";
//         $this->Image($image_file, 20, 15, 10, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

//         //Set font
//         $this->SetFont('helvetica', 'B', 20);

//         $this->SetFont('helvetica', 'B', 10);

//         // cabeçalho Endereço
//         $this->SetXY(16, 12);
//         $this->Cell(0, 0, 'Prefeitura Municipal de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 9);
//         $this->SetXY(35, 16);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Patrimônios - Termo de responsabilidade', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 19);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 22);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetXY(35, 25);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');


//         //informações Saege
//         $this->SetFont('helvetica', '', 8);
//         $this->SetXY(60, 15);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

//         $patrimonios = new Patrimonios();
//         $data = $patrimonios->getCurrentData();

//         $this->SetFont('helvetica', '', 8);
//         $this->SetXY(60, 18);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Data: ' . $data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

//         $this->SetFont('helvetica', '', 10);
//         $this->SetXY(40, 21);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
//     }

//     // Page footer
//     public function Footer() {
//         // Position at 15 mm from bottom
//         $this->SetY(-15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

//         // Position at 15 mm from bottom
//         $this->SetXY(150, -15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
//     }

// }

/**
 * Controle da classe patrimonios do sistema
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PatrimoniosController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Patrimonio
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("patrimonios", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Patrimonios();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Patrimônio excluído com sucesso.";

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "patrimonios")
            $objs = new Patrimonios();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'patrimonios', 'name' => 'Patrimônios'),
            array('url' => null, 'name' => 'Visualizar Patrimônio')
        );

        $id = (int) $this->_request->getParam("id");
        $patrimonios = new Patrimonios();
        $patrimonio = $patrimonios->getPatrimonioById($id, array());

        if (!$patrimonio)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $patrimonio;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {

        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Patrimônios')
        );

        $ns = new Zend_Session_Namespace('default_patrimonios');
        $patrimonios = new Patrimonios();
        $queries = array();

        $idOn = Usuarios::getUsuario('id');
        $dados = Usuarios::getUsuarios(array('id' => $idOn));
        $idPerfil = $dados[0]['idperfil'];

        //se master ou patrimônio, ver tudo / senão filtra por perfil
        if ($idPerfil == 1 || $idPerfil == 30) :
        elseif ($idPerfil == 29 || UsuariosPerfis::isAllowedCrudPatrimonio($idPerfil)):
            $_data = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'));
            if ($_data):
                $queries['idperfil'] = $dados[0]['idperfil'];
                $queries['codescola'] = $_data['id'];
            endif;
        else:
            $queries['idperfil'] = $dados[0]['idperfil'];
        endif;

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {

            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ($this->view->post_var["numeropatrimonio"] != "")
                $queries["numeropatrimonio"] = $this->view->post_var["numeropatrimonio"];
            if ($this->view->post_var["patrimonio"] != "")
                $queries["patrimonio"] = $this->view->post_var["patrimonio"];

            if (isset($this->view->post_var["idperfil"]) && $this->view->post_var["idperfil"] != "")
                $queries["idperfil"] = $this->view->post_var["idperfil"];

            if (isset($this->view->post_var["codescola"]) && $this->view->post_var["codescola"] != "")
                $queries["codescola"] = $this->view->post_var["codescola"];

            //if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
            if (isset($this->view->post_var["atribuido"]) && $this->view->post_var["atribuido"] != "")
                $queries["atribuido"] = $this->view->post_var["atribuido"];

            if (isset($this->view->post_var["status1"]) && $this->view->post_var["status1"] != "")
                $queries["status"] = $this->view->post_var["status1"];

            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $patrimonios->getPatrimonios($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $patrimonios->getPatrimonios($queries, $paginaAtual, $maxpp);
    }

    /**
     *
     * Action de edição de patrimonios
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'patrimonios', 'name' => 'Patrimônios'),
            array('url' => null, 'name' => 'Editar Patrimônio')
        );


        $id = (int) $this->_request->getParam("id");
        $patrimonios = new Patrimonios();
        $patrimonio = $patrimonios->getPatrimonioById($id);

        if (!$patrimonio)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $patrimonio;



        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($patrimonio);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Patrimônio editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     *
     * Action de adição de patrimonios
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'patrimonios', 'name' => 'Patrimônios'),
            array('url' => null, 'name' => 'Adicionar Patrimônio')
        );

        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Patrimônio adicionado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idpatrimonio
     */
    private function preForm($idpatrimonio = 0) {

    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_patrimonio = false) {


        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);


        $id = (int) $this->getRequest()->getPost("id");
        $idsecretaria = (int) trim($this->getRequest()->getPost("idsecretaria"));
        $iddepartamento = (int) trim($this->getRequest()->getPost("iddepartamento"));
        $idsetor = (int) trim($this->getRequest()->getPost("idsetor"));
        $idtipo = (int) trim($this->getRequest()->getPost("idtipo"));
        $idmarca = (int) trim($this->getRequest()->getPost("idmarca"));
        $idfornecedor = (int) trim($this->getRequest()->getPost("idfornecedor"));
        $origem = trim($this->getRequest()->getPost("origem"));
        $iddepartamentosecretaria = (int) trim($this->getRequest()->getPost("iddepartamentosecretaria"));
        $idresponsavelsecretaria = (int) trim($this->getRequest()->getPost("idresponsavelsecretaria"));
        $idescola = (int) trim($this->getRequest()->getPost("idescola"));
        $idresponsavelescola = (int) trim($this->getRequest()->getPost("idresponsavelescola"));
        $numeropatrimonio = trim($this->getRequest()->getPost("numeropatrimonio"));
        $patrimonio = trim($this->getRequest()->getPost("patrimonio"));
        $descricoes = trim($this->getRequest()->getPost("descricoes"));
        $cor = trim($this->getRequest()->getPost("cor"));
        $aprovado = trim($this->getRequest()->getPost("aprovado"));
        $modelo = trim($this->getRequest()->getPost("modelo"));
        $serie = trim($this->getRequest()->getPost("serie"));
        $numerolote = trim($this->getRequest()->getPost("numerolote"));
        $anofabricacao = trim($this->getRequest()->getPost("anofabricacao"));
        $vidautil = trim($this->getRequest()->getPost("vidautil"));
        $quantidade = trim($this->getRequest()->getPost("quantidade"));
        $aprovacao = trim($this->getRequest()->getPost("aprovacao"));
        $statusaprovacao = trim($this->getRequest()->getPost("statusaprovacao"));
        $observacoesaprovacoes = trim($this->getRequest()->getPost("observacoesaprovacoes"));
        $baixa = trim($this->getRequest()->getPost("baixa"));
        $statusbaixa = trim($this->getRequest()->getPost("statusbaixa"));
        $observacoesbaixa = trim($this->getRequest()->getPost("observacoesbaixa"));
        $roubofurto = trim($this->getRequest()->getPost("roubofurto"));
        $statusroubofurto = trim($this->getRequest()->getPost("statusroubofurto"));
        $observacoesroubofurto = trim($this->getRequest()->getPost("observacoesroubofurto"));
        $dataroubofurto = Mn_Util::stringToTime($this->getRequest()->getPost("dataroubofurto"));
        $idarquivoroubofurto = (int) trim($this->getRequest()->getPost("idarquivoroubofurto"));
        $depreciacao = trim($this->getRequest()->getPost("depreciacao"));
        $descricoesdepreciacao = trim($this->getRequest()->getPost("descricoesdepreciacao"));
        $valordepreciacao = MN_Util::trataNum(trim($this->getRequest()->getPost("valordepreciacao")));
        $percentualdepreciacao = MN_Util::trataNum(trim($this->getRequest()->getPost("percentualdepreciacao")));
        $tipodepreciacao = trim($this->getRequest()->getPost("tipodepreciacao"));
        $idfoto = (int) trim($this->getRequest()->getPost("idfoto"));
        $oldfoto = (int) trim($this->getRequest()->getPost("oldfoto"));
        $caracteristicas = trim($this->getRequest()->getPost("caracteristicas"));
        $dimensoes = trim($this->getRequest()->getPost("dimensoes"));
        $datacompra = Mn_Util::stringToTime($this->getRequest()->getPost("datacompra"));
        $descricoesdobem = trim($this->getRequest()->getPost("descricoesdobem"));
        $codigosbarras = trim($this->getRequest()->getPost("codigosbarras"));
        $qrcode = trim($this->getRequest()->getPost("qrcode"));
        $idperfil = trim($this->getRequest()->getPost("departamento"));
        $codescola = trim($this->getRequest()->getPost("codescola"));
        $status = trim($this->getRequest()->getPost("status1"));
        $origemaprovacao = trim($this->getRequest()->getPost("origemaprovacao"));
        $logdataescola = trim($this->getRequest()->getPost("logdataescola"));
        $logusuarioescola = (int) trim($this->getRequest()->getPost("logusuarioescola"));
        $excluir_idfoto = $this->getRequest()->getPost("excluir_idfoto");

        //die();

        $erros = array();

        $allowNumber = Patrimonios::approvePatrimonioNumberHelper($id, $numeropatrimonio);

        if ($allowNumber['total'] >= 1)
            array_push($erros, "Número do patrimônio já existe! Informe outro");
        /*
          if (0==$idsecretaria) array_push($erros, "Informe a Secretaria.");
          if (0==$idsetor) array_push($erros, "Informe a Setor.");
          if (0==$idtipo) array_push($erros, "Informe a Tipo.");
          if (0==$idescola) array_push($erros, "Informe a Escolas.");
          if (0==$idresponsavelescola) array_push($erros, "Informe a Responsável da escola."); */
        if ("" == $numeropatrimonio)
            array_push($erros, "Informe a Nº do patrimônio");
        if ("" == $patrimonio)
            array_push($erros, "Informe a Patrimônio");
        /* if (""==$aprovado) array_push($erros, "Informe a Aprovado.");
          if (""==$aprovacao) array_push($erros, "Informe a Aprovação.");
          if (""==$statusaprovacao) array_push($erros, "Informe a Status de Aprovação."); */
        if ("" == $status)
            array_push($erros, "Informe a Status");


        $patrimonios = new Patrimonios();

        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;

            $dados["idsecretaria"] = $idsecretaria;
            $dados["iddepartamento"] = $iddepartamento;
            $dados["idsetor"] = $idsetor;
            $dados["idtipo"] = $idtipo;
            $dados["idmarca"] = $idmarca;
            $dados["idfornecedor"] = $idfornecedor;
            $dados["origem"] = $origem;
            $dados["iddepartamentosecretaria"] = $iddepartamentosecretaria;
            $dados["idresponsavelsecretaria"] = $idresponsavelsecretaria;
            $dados["idescola"] = $idescola;
            $dados["codescola"] = (int) $codescola;
            $dados["idresponsavelescola"] = $idresponsavelescola;
            $dados["numeropatrimonio"] = $numeropatrimonio;
            $dados["patrimonio"] = $patrimonio;
            $dados["descricoes"] = $descricoes;
            $dados["cor"] = $cor;
            $dados["aprovado"] = $aprovado;
            $dados["modelo"] = $modelo;
            $dados["serie"] = $serie;
            $dados["numerolote"] = $numerolote;
            $dados["anofabricacao"] = (int) $anofabricacao;
            $dados["vidautil"] = (int) $vidautil;
            $dados["quantidade"] = (int) $quantidade;
            $dados["aprovacao"] = $aprovacao;
            $dados["statusaprovacao"] = $statusaprovacao;
            $dados["observacoesaprovacoes"] = $observacoesaprovacoes;
            $dados["baixa"] = $baixa;
            $dados["statusbaixa"] = $statusbaixa;
            $dados["observacoesbaixa"] = $observacoesbaixa;
            $dados["roubofurto"] = $roubofurto;
            $dados["statusroubofurto"] = $statusroubofurto;
            $dados["observacoesroubofurto"] = $observacoesroubofurto;
            $dados["dataroubofurto"] = date("Y-m-d", $dataroubofurto);

            $idarquivoroubofurto = $this->getArquivo('idarquivoroubofurto');
            if ($idarquivoroubofurto != 0)
                $dados['idarquivoroubofurto'] = $idarquivoroubofurto;
            $dados["depreciacao"] = $depreciacao;
            $dados["descricoesdepreciacao"] = $descricoesdepreciacao;
            $dados["valordepreciacao"] = $valordepreciacao;
            $dados["percentualdepreciacao"] = $percentualdepreciacao;
            $dados["tipodepreciacao"] = $tipodepreciacao;


            if ($excluir_idfoto == "sim"):
                $dados['idfoto'] = 0;
            else:
                $idfoto = $this->getImagem('idfoto');
                if ($idfoto != 0)
                    $dados['idfoto'] = $idfoto;
            endif;

            $dados["caracteristicas"] = $caracteristicas;
            $dados["dimensoes"] = $dimensoes;
            $dados["datacompra"] = date("Y-m-d", $datacompra);
            $dados["descricoesdobem"] = $descricoesdobem;
            $dados["codigosbarras"] = $codigosbarras;
            $dados["qrcode"] = $qrcode;
            $dados["idperfil"] = (int) $idperfil;
            $dados["status"] = $status;
            //$dados["origemaprovacao"] = $origemaprovacao;
            //$dados["logdataescola"] = $logdataescola;
            //$dados["logusuarioescola"] = $logusuarioescola;

            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            $dados['logdata'] = date('Y-m-d G:i:s');


            $row = $patrimonios->save($dados);

            $db->commit();

            //excluir imagem da pasta depois que salvar as alterações
            if ($excluir_idfoto == "sim")
                $this->excluirImagem($oldfoto);
        } catch (Exception $e) {
            echo $e->getMessage();

            $db->rollBack();
            die();
        }

        return "";
    }

    private function excluirImagem($idfoto) {
        $imagem = new Imagens();
        try {
            $imagem->excluir($idfoto);
        } catch (Exception $e) {
            array_push($erros, "Falha ao excluir imagem");
        }
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

    public function documentopatrimonioAction() {

        $patrimonios = new Patrimonios();
        $queries = array();

        $id = Usuarios::getUsuario('id');
        $dados = Usuarios::getUsuarios(array('id' => $id));
        $record = $dados[0];

        //se master ou patrimônio, ver tudo / senão filtra por perfil
        $idperfil = $record['idperfil'];
        $perfil = $record['perfil'];

        if ($idperfil == 1 || UsuariosPerfis::isAllowedCrudPatrimonio($idperfil)):
            $_rowPerfil = UsuariosPerfis::getPerfilByIdHelper($idperfil);
            $perfil = $_rowPerfil['perfil'];

        elseif ($idperfil == 29):
            $_data = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'));
            if ($_data):
                $queries['idperfil'] = $idperfil;
                $queries['codescola'] = $_data['id'];
                $escola = (isset($_data['escola'])) ? $_data['escola'] : '0';
            endif;
        else:
            $queries['idperfil'] = $idperfil;
        endif;

        $queries['status'] = "Ativo";

        $_row = $patrimonios->getPatrimonios($queries);

        $patrimonios = new Patrimonios();
        $data = $patrimonios->getCurrentData();
        $local = 'Santa Isabel';

        $localData = $local . ' , ' . $data;

        $this->view->rows = $_row;
        if (isset($escola))
            $this->view->escola = $escola;
        if (isset($perfil))
            $this->view->perfil = $perfil;
        $this->view->localdata = $localData;

        $this->preForm();

        $db = Zend_Registry::get('db');

        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Termos de aceite de patrimônios');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage();

        $html = $this->view->render('patrimonios/pdf/patrimoniosdocumento.phtml');
        $image_file = "public/admin/imagens/logosantaisabel.jpg";

        $pdf->writeHTML($html, true, 0, true, 0);

        $filename = 'meus_patrimonios.pdf';
        $pdf->Output($filename, 'I');

        die();
        return true;
    }

}
