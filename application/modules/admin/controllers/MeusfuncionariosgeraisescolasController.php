<?php

/**
 * Controle da classe meusfuncionariosgeraisescolas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_MeusfuncionariosgeraisescolasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Meusfuncionariosgeraisescola
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("meusfuncionariosgeraisescolas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Meusfuncionariosgeraisescolas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="meusfuncionariosgeraisescolas") $objs = new Meusfuncionariosgeraisescolas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'meusfuncionariosgeraisescolas', 'name' => 'Meus Funcionários'),
			array('url' => null,'name' => 'Visualizar Funcionário')
		);
		
		$id = (int)$this->_request->getParam("id");
		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		$meusfuncionariosgeraisescola = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolaById($id, array());
		
		if (!$meusfuncionariosgeraisescola) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $meusfuncionariosgeraisescola;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Meus Funcionários')
		);
		
		$ns = new Zend_Session_Namespace('default_meusfuncionariosgeraisescolas');
		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idfuncionarioemissor"]!="") $queries["idfuncionarioemissor"] = $this->view->post_var["idfuncionarioemissor"];
if ($this->view->post_var["idfuncionarioadicionado"]!="") $queries["idfuncionarioadicionado"] = $this->view->post_var["idfuncionarioadicionado"];
if ($this->view->post_var["idescolaemissor"]!="") $queries["idescolaemissor"] = $this->view->post_var["idescolaemissor"];
if ($this->view->post_var["idescolaadicionado"]!="") $queries["idescolaadicionado"] = $this->view->post_var["idescolaadicionado"];
if ($this->view->post_var["idescolaselecionado"]!="") $queries["idescolaselecionado"] = $this->view->post_var["idescolaselecionado"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de meusfuncionariosgeraisescolas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'meusfuncionariosgeraisescolas', 'name' => 'Meus Funcionários'),
			array('url' => null,'name' => 'Editar Funcionário')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		$meusfuncionariosgeraisescola = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolaById($id);
		
		if (!$meusfuncionariosgeraisescola) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $meusfuncionariosgeraisescola;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($meusfuncionariosgeraisescola);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de meusfuncionariosgeraisescolas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'meusfuncionariosgeraisescolas', 'name' => 'Meus Funcionários'),
			array('url' => null,'name' => 'Adicionar Funcionário')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idmeusfuncionariosgeraisescola
     */    
    private function preForm($idmeusfuncionariosgeraisescola = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_meusfuncionariosgeraisescola = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idfuncionarioemissor = (int)trim($this->getRequest()->getPost("idfuncionarioemissor"));
$idfuncionarioadicionado = (int)trim($this->getRequest()->getPost("idfuncionarioadicionado"));
$idescolaemissor = (int)trim($this->getRequest()->getPost("idescolaemissor"));
$idescolaadicionado = (int)trim($this->getRequest()->getPost("idescolaadicionado"));
$idescolaselecionado = (int)trim($this->getRequest()->getPost("idescolaselecionado"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idfuncionarioemissor) array_push($erros, "Informe a Funcionário.");
if (0==$idfuncionarioadicionado) array_push($erros, "Informe a Funcionário.");
if (0==$idescolaemissor) array_push($erros, "Informe a Escola.");
if (0==$idescolaadicionado) array_push($erros, "Informe a Escola.");
if (0==$idescolaselecionado) array_push($erros, "Informe a Escola.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idfuncionarioemissor"] = $idfuncionarioemissor;
$dados["idfuncionarioadicionado"] = $idfuncionarioadicionado;
$dados["idescolaemissor"] = $idescolaemissor;
$dados["idescolaadicionado"] = $idescolaadicionado;
$dados["idescolaselecionado"] = $idescolaselecionado;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $meusfuncionariosgeraisescolas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}