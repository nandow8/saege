<?php
/**
 * Controle da classe materiaisdidaticosprofessores do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_MateriaisdidaticosprofessoresController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Materiaisdidaticosprofessores
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("materiaisdidaticosprofessores", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$mapaacompanhamentoclasse = new Materiaisdidaticosprofessores();
		$queries = array();	
		$ultimo = $mapaacompanhamentoclasse->getMateriaisdidaticosprofessores($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Materiaisdidaticosprofessores();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Material didático do professor excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="materiaisdidaticosprofessores") $objs = new Materiaisdidaticosprofessores();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Em Aberto") ? "Fechado" : "Em Aberto";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'materiaisdidaticosprofessores', 'name' => 'Material didático de professor'),
			array('url' => null,'name' => 'Visualizar Material didático de professor')
		);
		
		$id = (int)$this->_request->getParam("id");
		$materiaisdidaticosprofessores = new Materiaisdidaticosprofessores();
		$materiaisdidaticosprofessores = $materiaisdidaticosprofessores->getMateriaisdidaticosprofessoresById($id, array());
		
		if (!$materiaisdidaticosprofessores) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $materiaisdidaticosprofessores;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Material didático de professor')
		);

		$ns = new Zend_Session_Namespace('default_materiaisdidaticosprofessores');
		$materiaisdidaticosprofessores = new Materiaisdidaticosprofessores();
		$queries = array();	
		$queries['idescolas'] = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'), 'id');
		
			
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
    		if ($this->view->post_var["iddisciplina"]!="") $queries["iddisciplina"] = $this->view->post_var["iddisciplina"];

			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $materiaisdidaticosprofessores->getMateriaisdidaticosprofessores($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $materiaisdidaticosprofessores->getMateriaisdidaticosprofessores($queries, $paginaAtual, $maxpp);
	}
	
	/**
	 * 
	 * Action de edição de materiaisdidaticosprofessores
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'materiaisdidaticosprofessores', 'name' => 'Material Didático de Professor'),
			array('url' => null,'name' => 'Editar Material Didático de Professor')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$materiaisdidaticosprofessores = new Materiaisdidaticosprofessores();
		$materiaisdidaticosprofessores = $materiaisdidaticosprofessores->getMateriaisdidaticosprofessoresById($id);
		
		if (!$materiaisdidaticosprofessores) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $materiaisdidaticosprofessores;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($materiaisdidaticosprofessores);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Material didático de professor editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de materiaisdidaticosprofessores 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'materiaisdidaticosprofessores', 'name' => 'Material didático de professor'),
			array('url' => null,'name' => 'Adicionar Material didático de professor')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Material didático de professor adicionado com sucesso.";

			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
	
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idtreinamento
     */    
    private function preForm($idmateriaisdidaticosprofessores = 0) {
    	$funcionarios = new Funcionariosgeraisescolas();

    	$this->view->escolas = Escolas::getEscolasByUsuario(Usuarios::getUsuario('id'));
    	//var_dump(Usuarios::getUsuario());die();
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_materiaisdidaticosprofessores = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idusuariologado = trim($this->getRequest()->getPost("idusuariologado"));
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$data = Mn_Util::stringToTime(trim($this->getRequest()->getPost("data")));
		$iddisciplina = trim($this->getRequest()->getPost("iddisciplina"));
		$idescolas = $this->getRequest()->getPost("idescolas");
		$idseries = $this->getRequest()->getPost("idseries");		
		$idtipoensino = trim($this->getRequest()->getPost("idtipoensino"));
		$idmaterial = (int)trim($this->getRequest()->getPost("idmaterial"));

		$status = trim($this->getRequest()->getPost("status1"));

		//ORDENA VETOR DE SERIES DE ACORDO COM ORDEM DE ESCOLAS

		$idseriesOrd = array();
		foreach ($idescolas as $key => $value) {
			$idseriesOrd[$key] = -1;
		}

		foreach ($idseries as $k => $v) {
			$a_escola = (explode('-', $v));
			$escolaSerie = $a_escola[1];

			foreach ($idescolas as $ind => $escola) {
				if( $escolaSerie == $escola ) {
					$s_idseriesOrd = (explode('-', $v));
					$idseriesOrd[$ind] = $s_idseriesOrd[0];
					break;
				}
			}
		}

		$idseries = ($idseries != NULL) ? implode(',', $idseriesOrd) : NULL;
		$idescolas = ($idescolas != NULL) ? implode(',', $idescolas) : NULL;

		$erros = array();
		
		//if (""==$sequencial) array_push($erros, "Informe o sequencial.");
		
		$materiaisdidaticosprofessores = new Materiaisdidaticosprofessores();		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idusuariologado"] = (int)$idusuariologado;
			$dados["sequencial"] = $sequencial;
			$dados["data"] = date("Y-m-d", $data);
			$dados["idescolas"] = $idescolas;
			$dados["idseries"] = $idseries;
			$dados["iddisciplina"] = (int)$iddisciplina;
			$dados["idtipoensino"] = (int)$idtipoensino;
			$dados["idseries"] =(int) $idseries;

			$idmaterial = $this->getArquivo('idmaterial');
			if ($idmaterial!=0) $dados['idmaterial'] = $idmaterial;

			$dados["status"] = $status;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
		
			$row = $materiaisdidaticosprofessores->save($dados);

			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}
		
		return "";    	
    }

    public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries['idescola'] = $idescola;

		$rows = new Escolasseries();
		$this->view->rows = $rows->getEscolasseries($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }     
}