<?php

/**
 * Controle da classe protocolos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_TichamadosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Protocolo
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("tichamados", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
	
		$iddepartamento = Usuarios::getUsuario('iddepartamento');
		$departamento = false;
		if($iddepartamento > 0){
			$departamento = Departamentossecretarias::getDepartamentosecretariaByIdHelper($iddepartamento);
		}
		
		//var_dump(Usuarios::getUsuario('iddepartamento')); die('aqui');
		$this->view->departamento = $departamento;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Chamados();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="tichamados") $objs = new Chamados();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tichamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Visualizar Chamado')
		);
		
		$id = (int)$this->_request->getParam("id");
		$chamados = new Chamados();
		$chamado = $chamados->getChamadoById($id, array());
		
		if (!$chamado) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $chamado;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Chamados')
		);
		
		/* Adicionando comentário */
		$ns = new Zend_Session_Namespace('admin_tichamados');
		$chamados = new Chamados();
		$queries = array();	

		 
		$queries['destino'] = "TI";
	 
		$queries['idescola'] = Usuarios::getUsuario('idescola');
		// Setando o local do usuário
		
	
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["idprioridade"]!="") $queries["idprioridade"] = $this->view->post_var["idprioridade"];
			if ($this->view->post_var["idstatus"]!="") $queries["idstatus"] = $this->view->post_var["idstatus"];
			if ($this->view->post_var["atendido"]!="") $queries["atendido"] = $this->view->post_var["atendido"];
			if ($this->view->post_var["idtipo"]!="") $queries["idtipo"] = $this->view->post_var["idtipo"];

			if ($this->view->post_var["dataagendada_i"]!="") $queries["dataagendada_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["dataagendada_i"]));
			if ($this->view->post_var["dataagendada_f"]!="") $queries["dataagendada_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["dataagendada_f"]));
						
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
		$maxpp = 20;
		
		if($queries['idescola'] == 0){
			
			if(Usuarios::getUsuario('idperfil') == "10" OR Usuarios::getUsuario('idperfil') == "30"):  // 30 é um super admin que visualiza tudo
				$totalRegistros = $chamados->getChamados($queries);	
			else:
				$queries['idescola'] = 0;
				$queries['iddepartamentocriacao'] = Usuarios::getUsuario('idperfil');
				$totalRegistros = $chamados->getChamados($queries);	
			endif;
			
		}else{
			$totalRegistros = $chamados->getChamados($queries);	
		}
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $chamados->getChamados($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal; 

		if($queries['idescola'] == 0){
			
			if(Usuarios::getUsuario('idperfil') == "10" OR Usuarios::getUsuario('idperfil') == "30"):  // 30 é um super admin que visualiza tudo
				$this->view->rows = $chamados->getChamados($queries, $paginaAtual, $maxpp);	
			else:
				$queries['idescola'] = 0;
				$queries['iddepartamentocriacao'] = Usuarios::getUsuario('idperfil');
				$this->view->rows = $chamados->getChamados($queries, $paginaAtual, $maxpp);	
			endif;
			
		}else{
			$this->view->rows = $chamados->getChamados($queries, $paginaAtual, $maxpp);	
		}
		
	}
	
	/**
	 * 
	 * Action de edição de chamados
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tichamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Editar Chamado')
		);	
		
		$id = (int)$this->_request->getParam("id");
		$chamados = new Chamados();
		$chamado = $chamados->getChamadoById($id);
		
		if (!$chamado) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $chamado;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($chamado);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de chamados 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tichamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Adicionar Chamado')
		);	
			
		$chamados = new Chamados();
		$queries = array();	
		$ultimo = $chamados->getUltimoChamadoByDestino("TI", $queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
		
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function getdadosAction(){
		$this->_helper->layout->disableLayout();
		
		$idpatrimonio = (int)$this->_request->getParam("idpatrimonio");
		$patrimonios = new Patrimonios();
		$patrimonio = $patrimonios->getPatrimonioById($idpatrimonio, array());
		
		if (!isset($patrimonio['id'])) die('erro');
		
		$this->view->post_id = $id;
		$this->view->post_var = $patrimonio;
		
	}
    /**
     * Atribui valores ao view
     * @param int $idchamado
     */    
    private function preForm($idchamado = 0) {
    }    
	/**
	 * Valida e grava os dados do formulário
	 */    
	private function getPost($_chamado = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$tipoPerfil = Usuarios::getUsuario('idperfil'); 
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
$destino = "TI";
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$descricoes = trim($this->getRequest()->getPost("descricoes")); 
$idprioridade = (int)trim($this->getRequest()->getPost("idprioridade"));
$idstatus = (int)trim($this->getRequest()->getPost("idstatus"));
$dataagendada = Mn_Util::stringToTime($this->getRequest()->getPost("dataagendada"));
$finalizacao = trim($this->getRequest()->getPost("finalizacao"));
$finalizar = trim($this->getRequest()->getPost("finalizar"));
$idtipoequipamento = (int)trim($this->getRequest()->getPost("idtipoequipamento"));
$finalizacaodata = date('Y-m-d G:i:s');
$atendido = trim($this->getRequest()->getPost("atendido"));
$idtipo = (int)trim($this->getRequest()->getPost("idtipo"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$servicoesdescricoes = trim($this->getRequest()->getPost("servicoesdescricoes"));
$servicosmateriais = trim($this->getRequest()->getPost("servicosmateriais"));
$idsfuncionarios = $this->getRequest()->getPost("idsfuncionarios");
$obsDataTi = $this->getRequest()->getPost("obsDataTi");
// $idsfuncionarios = implode(',', $idsfuncionarios);

$idspatrimonios = $this->getRequest()->getPost("idspatrimonios");
$observacaosubstituicao = $this->getRequest()->getPost("observacaosubstituicao");
$servicosexecutados = $this->getRequest()->getPost("servicosexecutados");
$defeitoconstatado = $this->getRequest()->getPost("defeitoconstatado");
$idresponsavel = (int)trim($this->getRequest()->getPost("idresponsavel"));
$patrimoniosobservacoes = $this->getRequest()->getPost("patrimoniosobservacoes");
$substituicaopecas = $this->getRequest()->getPost("substituicaopecas");
$pecaid = (int)trim($this->getRequest()->getPost("pecaid"));

/* ===================================================================================
			Campos Hidden criados para inserir dados que seriam perdidos 
   ===================================================================================*/

$hidden_patrimonio_id = $this->getRequest()->getPost("hidden-patrimonio-id");
$hidden_tipoequipamento_id = $this->getRequest()->getPost("hidden-tipoequipamento-id");
$hidden_patrimonioobservacoes_id = $this->getRequest()->getPost("hiddenpatrimonioobservacoesid");
$hidden_descricoes_id = $this->getRequest()->getPost("hidden-descricoes-id");
$hidden_idprioridade = $this->getRequest()->getPost("hidden-idprioridade-id");
$hidden_dataagendada = $this->getRequest()->getPost("hidden-dataagendada");
$hidden_idresponsavel = $this->getRequest()->getPost("hidden-idresponsavel");
$hidden_idstatus = $this->getRequest()->getPost("hidden-idstatus");
$hidden_observacoes = $this->getRequest()->getPost("hidden-observacoes");
$hidden_idtipo = $this->getRequest()->getPost("hidden-idtipo");
 
$status = 'Ativo';

if($idstatus == 0) $idstatus = 2;


// print_r($idprioridade);
// echo '<br>';
// print_r($atendido);
// die();

// echo $idtipoequipamento;
// echo '<br>' . $hidden_tipoequipamento_id;
// die();
		
// Setando o local onde o funcionário irá trabalhar para futuros filtros 
//$idlocal = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idlocal'];
$idlocal = Usuarios::getUsuario('idperfil');

		$erros = array();
		
		//if (0==$pecaid) array_push($erros, "Informe a peça.");

		if ((""==$sequencial) && ($id<=0)) array_push($erros, "Informe a Sequancial/Ano.");
//if (0==$idescola) array_push($erros, "Informe a Escola.");
 if($tipoPerfil == 10){
	if (""==$descricoes && ($id<=0)) array_push($erros, "Informe a Descrições.");
 
	
//	if (0==$hidden_idstatus) array_push($erros, "Informe a Status.");
	//if (""==$dataagendada) array_push($erros, "Informe a Data.");
}
//if (""==$idsfuncionarios) array_push($erros, "Informe a Funcionário.");

		$chamados = new Chamados();
		
		if (sizeof($erros)>0) return $erros; 
				
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			if((int)$id <= 0) $dados['idusuariocriacao'] = $this->_usuario['id'];
			if((int)$id <= 0) $dados["sequencial"] = $sequencial;
if((int)$id <= 0) $dados["destino"] = $destino;
if((int)$id <= 0) $dados["iddepartamento"] = $iddepartamento;
if((int)$id <= 0) $dados["idescola"] = Usuarios::getUsuario('idescola');

if(isset($descricoes) and ($descricoes != '')){
	$dados["descricoes"] = $descricoes;
}else{
	$dados['descricoes'] = $hidden_descricoes_id;
}
//  or $idprioridade != '0' or $idprioridade


if(isset($idprioridade)){

	if($idprioridade == '0'){
		$dados["idprioridade"] = (int)$hidden_idprioridade;
	}else{
		$dados["idprioridade"] = (int)$idprioridade;
	}
	
}else{
	$dados["idprioridade"] = (int)$hidden_idprioridade;
}
if(isset($idstatus)){
	if($idstatus == '0'){
		$dados["idstatus"] = (int)$hidden_idstatus;
	}else{
		$dados["idstatus"] = (int)$idstatus;
	}
}else{
	$dados["idstatus"] = (int)$hidden_idstatus;
}


if($finalizar=="Sim") $dados["idstatus"] = 2;

if($atendido == 'Sim' or $atendido == 'Não'): 
		$dados["dataagendada"] = $hidden_dataagendada;
else: 

	if(isset($dataagendada) and  (string) date("Y-m-d", $dataagendada) !='1969-12-31'): 
		$dados["dataagendada"] = date("Y-m-d", $dataagendada);
	else: 
		if($hidden_dataagendada == ''){
			$dados['dataagendada'] = date("Y-m-d", '0000-00-00');
		}else{
			$dados["dataagendada"] = $hidden_dataagendada;
		}
		
	endif;
endif;

if($finalizar=="Sim") $dados["finalizacaodata"] = $finalizacaodata;
if($finalizar=="Sim") $dados["finalizacao"] = "Sim";  
if($atendido == 'Não'): 
	$dados["atendido"] = $atendido;
	$dados["finalizacao"] = 'Sim';
	$dados["idstatus"] = 4;
elseif($atendido == 'Sim'):
	$dados["atendido"] = $atendido;
	$dados["finalizacao"] = $finalizacao;
	$dados["idstatus"] = 8;
endif;
 
if(isset($idtipo)){
	if($idtipo == '0'){
		$dados["idtipo"] = (int)$hidden_idtipo;
	}else{
		$dados["idtipo"] = (int)$idtipo;
	}
}else{
	$dados["idtipo"] = (int)$idtipo;
}


if(isset($idtipoequipamento) and $idtipoequipamento != '0'){
	$dados["idtipoequipamento"] = $idtipoequipamento;
}else{
	$dados["idtipoequipamento"] = $hidden_tipoequipamento_id;
}

if(isset($observacoes) and $observacoes != null and $observacoes != ''){
	$dados["observacoes"] = $observacoes;
}else{
	$dados["observacoes"] = $hidden_observacoes;
}



if(Usuarios::getUsuario('idperfil') != "10")
			$dados["iddepartamentocriacao"] = $idlocal;
// $dados["servicoesdescricoes"] = $servicoesdescricoes;
// $dados["servicosmateriais"] = $servicosmateriais;
// $dados["idsfuncionarios"] = $idsfuncionarios;
// $dados["tiobservacoesresposta"] = $tiobservacoesresposta;
$dados["servicosexecutados"] = $servicosexecutados;
$dados["defeitoconstatado"] = $defeitoconstatado;


if(isset($idresponsavel)){
	if($idresponsavel == '0'){
		$dados["idresponsavel"] = (int)$hidden_idresponsavel;
		
	}else{
		$dados["idresponsavel"] = (int)$idresponsavel;
	}
}else{
	$dados["idresponsavel"] = (int)$hidden_idresponsavel;
}




if(isset($patrimoniosobservacoes)){
	$dados["patrimoniosobservacoes"] = $patrimoniosobservacoes;
}else{
	$dados["patrimoniosobservacoes"] = $hidden_patrimonioobservacoes_id;
}


$dados["observacaosubstituicao"] = $observacaosubstituicao;
$dados["obsDataTi"] = $obsDataTi;


if($pecaid == '0'): 
	$dados["pecaid"] = '1';
else: 
	$dados["pecaid"] = $pecaid;	
endif;
// echo 'pecaid ' . $pecaid . '<br>';
// echo 'dados' . $dados["pecaid"];
// die();

$dados["substituicaopecas"] = $substituicaopecas;

if($substituicaopecas == "Sim" or $substituicaopecas == "Não"){
	$dados['finalizacao'] = 'Sim';
	$dados['idstatus'] = '5';
}

if(isset( $idspatrimonios)){
	$dados["idspatrimonios"] = (int)$idspatrimonios;
}else{
	$dados["idspatrimonios"] = (int)$hidden_patrimonio_id;
}

			if($dados['atendido'] == null) $dados['atendido'] = 'Não respondido';
 

			$dados["status"] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			$row = $chamados->save($dados);
			
			$db->commit();
		} catch (Exception $e) {

			echo $e->getMessage();
			Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__ ."::incluir", Usuarios::getUsuario("id"), "TIChamados " . $e->getMessage(), "E");
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename){
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	}
	
	public function detalhestichamadosAction(){
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idpatrimonio = (int)$this->_request->getParam("idpatrimonio");
		$patrimonios = new Patrimonios();
		$patrimonio = $patrimonios->getPatrimonioById($idpatrimonio, array());
		
		if (!isset($patrimonio['id'])) die('erro');
		
		//$this->view->post_var = $patrimonio;
		
		echo json_encode($patrimonio);

		die();
	}//end public functio detalhestichamadosAction
    
}