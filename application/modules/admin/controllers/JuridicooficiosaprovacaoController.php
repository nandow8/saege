<?php

/**
 * Controle da classe juridicooficiosaprovacao do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_JuridicooficiosAprovacaoController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Juridicooficioaprovacao
	 */
	protected $_usuario = null;	
	
	
	/**
        * Verificação de permissao de acesso
        */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("juridicooficiosaprovacao", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Juridicooficios();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Aprovação de Oficio excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="juridicooficiosaprovacao") $objs = new Juridicooficiosaprovacao();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Finalizado") ? "Em aberto" : "Finalizado";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'juridicooficiosaprovacao', 'name' => 'Aprovação de Oficios'),
			array('url' => null,'name' => 'Visualizar Aprovação de Oficio')
		);
		
		$id = (int)$this->_request->getParam("id");
		$juridicooficiosaprovacao = new Juridicooficiosaprovacao();
		$juridicooficioaprovacao = $juridicooficiosaprovacao->getJuridicooficioaprovacaoById($id, array());
		
		if (!$juridicooficioaprovacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $juridicooficioaprovacao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Aprovação de Oficios')
		);
		
		$ns = new Zend_Session_Namespace('default_juridicooficiosaprovacao');
		$juridicooficiosaprovacao = new Juridicooficiosaprovacao();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
                if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
                if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
                if ($this->view->post_var["idtipo"]!="") $queries["idtipo"] = $this->view->post_var["idtipo"];
                if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
                if ($this->view->post_var["posicao"]!="") $queries["posicao"] = $this->view->post_var["posicao"];
                if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
                if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
        }		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $juridicooficiosaprovacao->getJuridicooficiosaprovacao($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $juridicooficiosaprovacao->getJuridicooficiosaprovacao($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de juridicooficios
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'juridicooficiosaprovacao', 'name' => 'Aprovação de Oficios'),
			array('url' => null,'name' => 'Editar Oficio')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$juridicooficiosaprovacao = new Juridicooficiosaprovacao();
		$juridicooficioaprovacao = $juridicooficiosaprovacao->getJuridicooficioaprovacaoById($id);
		
		if (!$juridicooficioaprovacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $juridicooficioaprovacao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($juridicooficioaprovacao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Aprovação de Oficio editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de juridicooficiosaprovacao 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'juridicooficiosaprovacao', 'name' => 'Aprovação de Oficios'),
			array('url' => null,'name' => 'Adicionar Aprovação de Oficio')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Aprovação de Oficio adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idjuridicooficio
     */    
    private function preForm($idjuridicooficioaprovacao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_juridicooficioaprovacao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
                $idtipo = (int)trim($this->getRequest()->getPost("idtipo"));
                $numero = trim($this->getRequest()->getPost("numero"));
                $titulo = trim($this->getRequest()->getPost("titulo"));
                $numeroof = trim($this->getRequest()->getPost("numeroof"));
                $dataof = Mn_Util::stringToTime($this->getRequest()->getPost("dataof"));
                $proc = trim($this->getRequest()->getPost("proc"));
                $dataproc = Mn_Util::stringToTime($this->getRequest()->getPost("dataproc"));
                $numeroreinteracao = trim($this->getRequest()->getPost("numeroreinteracao"));
                $datareinteracao = Mn_Util::stringToTime($this->getRequest()->getPost("datareinteracao"));
                $iddocumentooriginal = (int)trim($this->getRequest()->getPost("iddocumentooriginal"));
                $datadevolucao = Mn_Util::stringToTime($this->getRequest()->getPost("datadevolucao"));
                $posicao = trim($this->getRequest()->getPost("posicao"));
                $iddocumentoenviado = (int)trim($this->getRequest()->getPost("iddocumentoenviado"));
                $status = trim($this->getRequest()->getPost("status1"));
                $cienvioflag = trim($this->getRequest()->getPost("cienvioflag"));
                $cienviodt = Mn_Util::stringToTime($this->getRequest()->getPost("cienviodt"));
                $cienvio = trim($this->getRequest()->getPost("cienvio"));
                $idimgcienvio = (int)trim($this->getRequest()->getPost("idimgcienvio"));
                $ciretornoflag = trim($this->getRequest()->getPost("ciretornoflag"));
                $ciretornodt = Mn_Util::stringToTime($this->getRequest()->getPost("ciretornodt"));
                $ciretorno = trim($this->getRequest()->getPost("ciretorno"));
		$idimgciretorno = (int)trim($this->getRequest()->getPost("idimgciretorno"));
		
		$erros = array();
		
		if (""==$datalancamento) array_push($erros, "Informe a Data de Lançamento.");
                if (0==$idtipo) array_push($erros, "Informe a Tipo.");
                if (""==$numero) array_push($erros, "Informe a Num. de controle do orgão.");
                if (""==$titulo) array_push($erros, "Informe a Título.");
                if (""==$status) array_push($erros, "Informe a Status.");

		
		$juridicooficiosaprovacao = new Juridicooficios();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["datalancamento"] = date("Y-m-d", $datalancamento);
                        $dados["idtipo"] = $idtipo;
                        $dados["numero"] = $numero;
                        $dados["titulo"] = $titulo;
                        $dados["numeroof"] = $numeroof;
                        $dados["dataof"] = date("Y-m-d", $dataof);
                        $dados["proc"] = $proc;
                        $dados["dataproc"] = date("Y-m-d", $dataproc);
                        $dados["numeroreinteracao"] = $numeroreinteracao;
                        $dados["datareinteracao"] = date("Y-m-d", $datareinteracao);

                        $iddocumentooriginal = $this->getArquivo('iddocumentooriginal');
                        if ($iddocumentooriginal!=0) $dados['iddocumentooriginal'] = $iddocumentooriginal;
                        $dados["datadevolucao"] = date("Y-m-d", $datadevolucao);
                        $dados["posicao"] = $posicao;

                        $iddocumentoenviado = $this->getArquivo('iddocumentoenviado');
                        if ($iddocumentoenviado!=0) $dados['iddocumentoenviado'] = $iddocumentoenviado;
                        $dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
                        
                        $dados['cienvioflag'] = $cienvioflag;
                        $dados['cienviodt'] = date("Y-m-d G:i:s", $cienviodt);
                        $dados['cienvio'] = $cienvio;
                        $idimgcienvio = $this->getArquivo('idimgcienvio');
                        if ($idimgcienvio!=0) $dados['idimgcienvio'] = $idimgcienvio;
                        
                        $dados['ciretornoflag'] = $ciretornoflag;
                        $dados['ciretornodt'] = date("Y-m-d G:i:s", $ciretornodt);
                        $dados['ciretorno'] = $ciretorno;
                        $idimgciretorno = $this->getArquivo('idimgciretorno');
                        if ($idimgciretorno!=0) $dados['idimgciretorno'] = $idimgciretorno;                        

			$row = $juridicooficiosaprovacao->save($dados);
			
                        
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}
