<?php

class Admin_LteentidadesController extends Zend_Controller_Action {
    
    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null; 
    
    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
        Mn_Util::blockAccess("lteentidades", $this->_request->getActionName());    
        
        $this->_usuario = unserialize($loginNameSpace->usuario);
        
        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }               
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->_request->getPost("id");
        
        $rows = new Lteentidades();
        $row = $rows->fetchRow("id=".$id);
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = Usuarios::getUsuario('id');
            $row['logdata'] = date('Y-m-d G:i:s');          
            
            $rows->save($row);
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Entidade excluída com sucesso.";
            
            die("OK");
        }
        
        die("Não encontrado!"); 
    }       
    
    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");
        
        if ($op=="lteentidades") $objs = new Lteentidades();
        $obj = $objs->fetchRow("excluido='nao' AND id=".$id);
        if ($obj) {
            
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = Usuarios::getUsuario('id');
            $obj['logdata'] = date('Y-m-d G:i:s');
            
            $objs->save($obj);
                        
            die($obj['status']);
        }
        
        die("Não encontrado!");
    }       
    
    public function changeorderxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $idfrom = (int)$this->getRequest()->getPost("from");
        $idto = (int)$this->getRequest()->getPost("to");
        $op = $this->getRequest()->getPost("op");
        
        if ($op=="change") $objs = new Lteentidades();
        $from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
        $to = $objs->fetchRow("excluido='nao' AND id=".$idto);
        
        if (($from) && ($to)) {
            $from = $from->toArray();
            $to = $to->toArray();
            $ordemFrom = $from["ordem"];
            $orderTo = $to["ordem"];

            $from['ordem'] = $orderTo;
            $to['ordem'] = $ordemFrom;

            $objs->save($from);
            $objs->save($to);
        }
        
        $this->view->message = "OK"; 
        $this->render("xml");
    }   
    
    public function ordemAction() {
        $ordem = (int) $this->getRequest()->getParam('ordem', 0);
        $d = $this->getRequest()->getParam('d', 0);
        
        $rows = new Lteentidades();
        $rows->swapOrdem($ordem, $d, false);

        $this->_redirect("/admin/".$this->_request->getControllerName()."/index");
        die();  
    }   
    
    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Entidade')
        );
      
        $ns = new Zend_Session_Namespace('logistica_entidades');
        $entidades = new Lteentidades();
        $queries = array(); 

        $this->preForm();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) { 
            
            foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = $v;

            if ((isset($this->view->post_var['entidade'])) && ($this->view->post_var['entidade']!='')) $queries['entidade'] = $this->view->post_var['entidade'];
            if ((isset($this->view->post_var['email'])) && ($this->view->post_var['email']!='')) $queries['email'] = $this->view->post_var['email'];
            if ((isset($this->view->post_var['telefone'])) && ($this->view->post_var['telefone']!='')) $queries['telefone'] = $this->view->post_var['telefone'];
        }       
        
        //PAGINACAO
        $maxpp = 15;
        
        $paginaAtual = (int)$this->getRequest()->getParam('p');
        if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
        if ($paginaAtual==0) $paginaAtual = 1;
        
        $queries['total'] = true;
        $totalRegistros = $entidades->getEntidades($queries);
        $paginaTotal = ceil($totalRegistros/$maxpp);
        $queries['total'] = false;
        if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual<0) $paginaAtual = 0;
        
        $this->view->pagina_atual = $paginaAtual+1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;           
        
        $this->view->rows = $entidades->getEntidades($queries, $paginaAtual, $maxpp);
    }
    
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'lteentidades', 'name' => 'Entidades'),
            array('url' => null,'name' => 'Visualizar entidade')
        );
    
        $id = (int)$this->_request->getParam("id");
        $entidades = new Lteentidades();
        $entidade = $entidades->getEntidadeById($id);

        if (!$entidade) $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->visualizar = true;
        $this->view->post_var = $entidade;
                
        $this->preForm();   
    }
    
    /**
     * 
     * Action de edição de entidade
     */ 
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'lteentidades', 'name' => 'Entidades'),
            array('url' => null,'name' => 'Editar entidade')
        );  
                
        $id = (int)$this->_request->getParam("id");
        $entidades = new Lteentidades();
        $entidade = $entidades->getEntidadeById($id);
    
        if (!$entidade) 
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->post_var = $entidade;
            
        $this->preForm();
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($entidade);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Entidade editada com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }       
        return true;        
    }       
    
    /**
     * 
     * Action de adição de entidade 
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'lteentidades', 'name' => 'Entidades'),
            array('url' => null,'name' => 'Adicionar entidade')
        );  
                
        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Entidade adicionada com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }
        return true;        
    }   
    
    /**
     * Atribui valores ao view
     * @param int $id
     */    
    private function preForm($idaluno = 0) {
    }    
    
    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_registro = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);
        
        $id          = (int)$this->getRequest()->getPost("id");
        $entidade    = strip_tags(trim($this->getRequest()->getPost("entidade")));
        $status      = strip_tags(trim($this->getRequest()->getPost("status")));
        $telefone    = strip_tags(trim($this->getRequest()->getPost("telefone")));
        $telefone    = preg_replace("/[^0-9]/", "", $telefone);
        $email       = strip_tags(trim($this->getRequest()->getPost("email")));
        $cep         = strip_tags(trim($this->getRequest()->getPost("cep")));
        $cep         = preg_replace("/[^0-9]/", "", $cep);
        $endereco    = strip_tags(trim($this->getRequest()->getPost("endereco")));
        $numero      = strip_tags(trim($this->getRequest()->getPost("numero")));
        $complemento = strip_tags(trim($this->getRequest()->getPost("complemento")));
        $idestado    = (int)(trim($this->getRequest()->getPost("idestado")));
        
        $tipo        = strip_tags(trim($this->getRequest()->getPost("tipo")));
        $observacoes = strip_tags(trim($this->getRequest()->getPost("observacoes")));
        $idbairro = 0;
        $idcidade = 0;

        if ($tipo == 'universidade') {

            $idcidade = (int)(trim($this->getRequest()->getPost("idcidade")));

        } else if ($tipo == 'estadual') {

            $idbairro = (int)(trim($this->getRequest()->getPost("idbairro")));

        }

        $erros = array();
        if (""==$status) array_push($erros, 'Selecione um STATUS.');
                
        $entidades = new Lteentidades();

        if (sizeof($erros)>0) return $erros; 
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
               
            $dados = array();           
            $dados['id']          = $id;
            $dados['entidade']    = $entidade;
            $dados['status']      = $status;
            $dados['telefone']    = $telefone;
            $dados['email']       = $email;
            $dados['cep']         = $cep;
            $dados['endereco']    = $endereco;
            $dados['numero']      = $numero;
            $dados['complemento'] = $complemento;
            $dados['idbairro']    = $idbairro;
            $dados['idestado']    = $idestado;
            $dados['idcidade']    = $idcidade;
            $dados['tipo']        = $tipo;
            $dados['observacoes'] = $observacoes;
            
            $dados['excluido'] = 'nao';

            $row = $entidades->save($dados);
            
            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die();
        }       

        return "";      
    }
    
    private function getLatitudeLongitude($identidade) {
        $entidades = new Lteentidades();
        $entidade = $entidades->getEntidadeById($identidade);
        if(!$entidade) return false;       

        $endereco = str_replace(" ", "+", $entidade['endereco']);
        
        $address = $endereco . ','.$entidade['numero'].',' . $entidade['cidade'] . ',' . $entidade['uf'];
        $address = str_replace(" ", "+", $address);

        $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.',brasil');
                
        $output= json_decode($geocode);
        
        $lat = $output->results[0]->geometry->location->lat;
        $long = $output->results[0]->geometry->location->lng;
        
        $entidade['latitude'] = $lat;
        $entidade['longitude'] = $long;        
        $entidades->save($entidade);
        return ;    
    }
    
    private function getImagem($apenas_copia = false, $nome = false) {
        $idimagem = false;
        $imagens = new Imagens();
        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($nome, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros,$e->getMessage());
        }
        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $nome));
        if ($excluir_imagem=='excluir_' . $nome) $idimagem = -1;
        return $idimagem;               
    }
}
