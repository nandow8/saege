<?php
class Admin_EscolasalunosController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("escolasalunos", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {

        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Escolasalunos();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);
            
            Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Aluno do ID ".$id." excluído com sucesso!");
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Aluno excluído com sucesso.";

            die("OK");
        }
        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "escolasalunos")
            $objs = new Escolasalunos();
        $obj = $objs->fetchRow("excluido='nao' AND id=" . $id);
        if ($obj) {

            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = Usuarios::getUsuario('id');
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);
            Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Aluno do ID ".$id." - ". $dados['status']." com sucesso!");

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    public function changeorderxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $idfrom = (int) $this->getRequest()->getPost("from");
        $idto = (int) $this->getRequest()->getPost("to");
        $op = $this->getRequest()->getPost("op");

        if ($op == "change")
            $objs = new Escolasalunos();
        $from = $objs->fetchRow("excluido='nao' AND id=" . $idfrom);
        $to = $objs->fetchRow("excluido='nao' AND id=" . $idto);

        if (($from) && ($to)) {
            $from = $from->toArray();
            $to = $to->toArray();
            $ordemFrom = $from["ordem"];
            $orderTo = $to["ordem"];

            $from['ordem'] = $orderTo;
            $to['ordem'] = $ordemFrom;

            $objs->save($from);
            $objs->save($to);
        }

        $this->view->message = "OK";
        $this->render("xml");
    }

    public function ordemAction() {
        $ordem = (int) $this->getRequest()->getParam('ordem', 0);
        $d = $this->getRequest()->getParam('d', 0);

        $rows = new Escolasalunos();
        $rows->swapOrdem($ordem, $d, false);

        $this->_redirect("/admin/" . $this->_request->getControllerName() . "/index");
        die();
    }

    /**
     * Listagem
     */
    public function indexAction() {

        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Aluno')
        );

        $ns = new Zend_Session_Namespace('admin_alunos');
        $alunos = new Escolasalunos();        
        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['verifica_nutricao'] = true;

        //dados da escola
        $dataEscola = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'));
        if ($dataEscola) {
            $queries['idescola'] = $dataEscola['id'];
        } else
            $queries['idescola'] = 0;

        $this->preForm();
        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();
        }
        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {

            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = $v;

            if ((isset($this->view->post_var['idescola'])) && ($this->view->post_var['idescola'] != ''))
                $queries['idescola'] = $this->view->post_var['idescola'];
            if ((isset($this->view->post_var['idsala'])) && ($this->view->post_var['idsala'] != ''))
                $queries['idsala'] = $this->view->post_var['idsala'];
            if ((isset($this->view->post_var['idserie'])) && ($this->view->post_var['idserie'] != ''))
                $queries['idserie'] = $this->view->post_var['idserie'];
            if ((isset($this->view->post_var['idclassificacao'])) && ($this->view->post_var['idclassificacao'] != ''))
                $queries['idclassificacao'] = $this->view->post_var['idclassificacao'];
            if ((isset($this->view->post_var['idperiodo'])) && ($this->view->post_var['idperiodo'] != ''))
                $queries['idperiodo'] = $this->view->post_var['idperiodo'];
            if ((isset($this->view->post_var['status'])) && ($this->view->post_var['status'] != ''))
                $queries['status'] = $this->view->post_var['status'];
            if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave'] != ''))
                $queries['chave'] = $this->view->post_var['chave'];
            if ((isset($this->view->post_var['nome'])) && ($this->view->post_var['nome'] != ''))
                $queries['nomerazao'] = $this->view->post_var['nome'];
            if ((isset($this->view->post_var['ra'])) && ($this->view->post_var['ra'] != ''))
                $queries['ra'] = $this->view->post_var['ra'];
            if ((isset($this->view->post_var['nome_mae'])) && ($this->view->post_var['nome_mae'] != ''))
                $queries['nomemae'] = $this->view->post_var['nome_mae'];
            if ((isset($this->view->post_var['nome_pai'])) && ($this->view->post_var['nome_pai'] != ''))
                $queries['nomepai'] = $this->view->post_var['nome_pai'];
            if ((isset($this->view->post_var['rg'])) && ($this->view->post_var['rg'] != ''))
                $queries['rg'] = $this->view->post_var['rg'];
            if ((isset($this->view->post_var['cpf'])) && ($this->view->post_var['cpf'] != ''))
                $queries['cpf'] = $this->view->post_var['cpf'];
            if ((isset($this->view->post_var['tiposerie'])) && ($this->view->post_var['tiposerie'] != ''))
                $queries['tiposerie'] = $this->view->post_var['tiposerie'];
            if ((isset($this->view->post_var['allseries'])) && ($this->view->post_var['allseries'] != ''))
                $queries['allseries'] = $this->view->post_var['allseries'];
            if ((isset($this->view->post_var['soapretorno'])) && ($this->view->post_var['soapretorno'] != ''))
                $queries['soapretorno'] = $this->view->post_var['soapretorno'];

            if ((isset($this->view->post_var['sorting'])) && ($this->view->post_var['sorting'] != ''))
                $queries['sorting'] = $this->view->post_var['sorting'];
        }
        //PAGINACAO
        $maxpp = 15;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $alunos->getEscolasalunos($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $alunos->getEscolasalunos($queries, $paginaAtual, $maxpp);
    }

    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasalunos', 'name' => 'Alunos'),
            array('url' => null, 'name' => 'Visualizar aluno')
        );

        $id = (int) $this->_request->getParam("id");
        $alunos = new Escolasalunos();
        $aluno = $alunos->getEscolaalunoById($id);
        $endereco = Alunosenderecos::getEnderecoByIdAlunoHelper($aluno['id']);

        $irmaosassociados = Escolasalunosassociairmao::getIrmaosAssociadosByIdAlunoHelper($aluno['id']);
        if($irmaosassociados) $this->view->irmaosassociados = $irmaosassociados;

        if ($endereco)
            $this->view->endereco = $endereco;

        if (!$aluno)
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());

        $this->view->visualizar = true;
        $this->view->post_var = $aluno;
        $this->view->soap = SED::ConsultarFichaAluno($aluno);

        $this->preForm();

    }

    /**
     *
     * Action de edição de aluno
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasalunos', 'name' => 'Alunos'),
            array('url' => null, 'name' => 'Editar aluno')
        );

        $id = (int) $this->_request->getParam("id");
        $alunos = new Escolasalunos();
        $aluno = $alunos->getEscolaalunoById($id);
        if (!$aluno)
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $aluno;
        $endereco = Alunosenderecos::getEnderecoByIdAlunoHelper($aluno['id']);

        $irmaosassociados = Escolasalunosassociairmao::getIrmaosAssociadosByIdAlunoHelper($aluno['id']);

        if($irmaosassociados) $this->view->irmaosassociados = $irmaosassociados;

        if ($endereco) {
            unset($endereco['id']);
            unset($endereco['excluido']);
            unset($endereco['inLatitude']);
            unset($endereco['inLongitude']);
            unset($endereco['endIndicativoinLatitude']);
            unset($endereco['endIndicativoinLongitude']);
            $this->view->endereco = $endereco;
            //$this->view->post_var = array_merge($this->view->post_var, $endereco);
        }

        $this->view->editar = true;
        $repopulaenderecos = array();

        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($aluno);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Aluno editado com sucesso.";

            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        }
        return true;
    }

    /**
     *
     * Action de adição de alunos
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasalunos', 'name' => 'Alunos'),
            array('url' => null, 'name' => 'Adicionar aluno')
        );


        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Aluno adicionado com sucesso.";

            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        }
        return true;
    }

    public function removeSpecialCaracters($str) {

        $notAllowed = array(".", "-");

        foreach ($notAllowed as $k => $v) {
            $newstr = str_replace($v, '', $str);
            $str = $newstr;
        }

        return trim($newstr);
    }

    public function setalunosAction() {
        $this->_helper->layout->disableLayout();
        $idescola = (int) $this->getRequest()->getPost('idescola');
        if ($idescola <= 0)
            die('ERRO');

        $alunos = new Escolasalunos();
        $this->view->rows = $alunos->getEscolasalunos(array('idescola' => $idescola, 'status' => 'Ativo', 'idsecretaria' => Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id')));
    }

    public function setseriesAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = $this->getRequest()->getPost('idescola');

        $series = new Escolasseries();
        $this->view->rows = $series->getEscolasseries(array('idescola' => $idescola, 'status' => 'Ativo', 'idsecretaria' => Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id')));
    }

    public function setperiodosAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = $this->getRequest()->getPost('idescola');

        $periodos = new Escolasperiodos();
        $this->view->rows = $periodos->getEscolasperiodos(array('idescola' => $idescola, 'status' => 'Ativo', 'idsecretaria' => Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id')));
    }

    public function setclassificacoesAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = $this->getRequest()->getPost('idescola');
        ;
        $classificacoes = new Escolasalunosclassificacoes();
        $this->view->rows = $classificacoes->getClassificacoes(array('idescola' => $idescola, 'status' => 'Ativo', 'idsecretaria' => Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id')));
    }
    /**
     * Buscar registro do aluno no SED
     * @return $dados - dados do aluno
     */
    
    public function consultafichaalunoAction(){
        $this->_helper->layout->disableLayout();

        $inRA = $this->getRequest()->getPost('inRA');
        $inDigitoRA = $this->getRequest()->getPost('inDigitoRA');
        $inUF = $this->getRequest()->getPost('inUF');
        
        $dados = array();
        $dados['ra'] = $inRA;
        $dados['digra'] = $inDigitoRA;
        $dados['ufra'] = $inUF;
        
        $registroAluno = SED::ConsultarFichaAluno($dados);
        echo json_encode($registroAluno);
        die();
    }

    /**
     * Associar um aluno como irmão
     * @return string 'Mensagem de resposta'
     */
    public function associairmaoAction(){
        $this->_helper->layout->disableLayout();

        //pegar valores que vêm do formulário
        $idaluno = $this->getRequest()->getPost('idaluno');
        $inRA = $this->getRequest()->getPost('inRA');
        $inDigitoRA = $this->getRequest()->getPost('inDigitoRA');
        $inUFRA = $this->getRequest()->getPost('inUFRA');
        $inIrmaoRA = $this->getRequest()->getPost('inIrmaoRA');
        $inIrmaoDigitoRA = $this->getRequest()->getPost('inIrmaoDigitoRA');
        $inIrmaoUFRA = $this->getRequest()->getPost('inIrmaoUFRA');
        $inGemeo = $this->getRequest()->getPost('inGemeo');
        $inNome = $this->getRequest()->getPost('inNome');
        $inMae = $this->getRequest()->getPost('inMae');
        $inPai = $this->getRequest()->getPost('inPai');
        $inNascimento = $this->getRequest()->getPost('inNascimento');

        //guardar valores em um array
        $dados = array();
        $dados['idaluno'] = $idaluno;
        $dados['ra'] = $inRA;
        $dados['digra'] = $inDigitoRA;
        $dados['ufra'] = $inUFRA;
        $dados['inIrmaoRA'] = $inIrmaoRA;
        $dados['inIrmaoDigitoRA'] = $inIrmaoDigitoRA;
        $dados['inIrmaoUFRA'] = $inIrmaoUFRA;
        $dados['inGemeo'] = $inGemeo;
        $dados['inNome'] = $inNome;
        $dados['inMae'] = $inMae;
        $dados['inPai'] = $inPai;
        $dados['inNascimento'] = date('Y-m-d', MN_Util::stringToTime($inNascimento));
        $dados['logdata'] = date('Y-m-d G:i:s');
        $dados['logusuario'] = Usuarios::getUsuario('id');
        
        //enviar para ser salvo no SED
        $associaIrmao = SED::AssociarIrmao($dados);

        if($associaIrmao){
            //salvar os dados no banco
            $associa = new Escolasalunosassociairmao;
            $queries = array();

            $queries['idaluno'] = $dados['idaluno'];
            $queries['inRA'] = $dados['ra'];
            $queries['inDigitoRA'] = $dados['digra'];
            $queries['inIrmaoRA'] = $dados['inIrmaoRA'];
            $queries['inIrmaoDigitoRA'] = $dados['inIrmaoDigitoRA'];

            $registro = $associa->getIrmaosAssociados($queries);
            if($registro){
                $registro = $registro[0];
                $dados['id'] = $registro['id'];
            }
            
            $associa->save($dados);
            
            $valores = array();
            $rows = $associa->getUltimoEscolasalunosassociairmao($valores);
            $new = (isset($dados['id'])) ? 'NAO' : 'SIM';
                 
            echo json_encode(array('idlast'=>$rows['id'], 'novo'=>$new));            
        }else{
            echo json_encode($associaIrmao);            
        }
        die();        
    }

    /**
     * Excluir irmão
     * @param int $id - id do registro 
     */
    public function excluirirmaoAction(){
        $this->_helper->layout->disableLayout();

        $id = $this->getRequest()->getPost('id');
        if(!$id) return false;

        $rows = Escolasalunosassociairmao::getEscolasalunosassociairmaoByIdHelper($id);
       
        if($rows){
            $dados = array();
            $dados['id'] = $rows['id'];
            $dados['ra'] = $rows['inRA'];
            $dados['digra'] = $rows['inDigitoRA'];
            $dados['ufra'] = $rows['inUFRA'];
            $dados['inIrmaoRA'] = $rows['inIrmaoRA'];
            $dados['inIrmaoDigitoRA'] = $rows['inIrmaoDigitoRA'];
            $dados['inIrmaoUFRA'] = $rows['inIrmaoUFRA'];
            $dados['inGemeo'] = $rows['inGemeo'];

            //excluir no SED
            $excluiIrmao = SED::ExcluirIrmao($dados);
                      
            if($excluiIrmao){                
                //excluir no BANCO DE DADOS
                $row = new Escolasalunosassociairmao();
                $row->excluirEscolasalunosassociairmaoById($id);
                echo json_encode('OK');
               
            }else{
                echo json_encode('FALHA');
            }           
        }                
        die();
    }
    /**
     * @param int $idaluno
     * @param Date dataabandono
     * @return Obj - Status e Mensagem
     */
    public function registrarabandonoAction(){
        error_reporting(0);
        ini_set("display_errors", 0 );
        $this->_helper->layout->disableLayout();

        $id = $this->getRequest()->getPost('idAluno');        
        $data = $this->getRequest()->getPost('dataabandono');
        $inNumClasse = $this->getRequest()->getPost('inNumClasse');

        $data = date("Y-m-d", Mn_Util::stringToTime($data));

        $alunos = new Escolasalunos();
        $aluno = $alunos->getEscolaalunoById($id);

        $inRA = $aluno["ra"];
        $inDigitoRA = $aluno["digra"];
        $inUFRA = $aluno["ufra"];

        //inserir dados no array
        $dados = array();
        $dados['id'] = $id;
        $dados['ra'] = $inRA;  
        $dados['digra'] = $inDigitoRA;
        $dados['ufra'] = $inUFRA;
        $dados['dataabandono'] = $data;
        $dados['classe'] = $inNumClasse;

        $registroAbandono = SED::RegistrarAbandono($dados);
        
        if(isset($registroAbandono["Mensagens"]["BaixarMatricula"]["outErro"])){            
            echo json_encode(array("status"=>"false", "mensagem"=>$registroAbandono["Mensagens"]["BaixarMatricula"]["outErro"]));
        }else{
            $queries = array();
            $queries['id'] = $id;
            $queries['abandono'] = "sim";
            $queries['dataabandono'] = $data;
            $queries['dataestorno'] = date("0000-00-00 00-00-00");
            $queries['status'] = "Abandono";
            
            $escolasaluno = new Escolasalunos();
            $escolasaluno->save($queries);

            //data abandono
            $dateObj =  date('Y-m-d', strtotime(substr($data, 0, 10)));
            $dataabandono = new DateTime($dateObj);                       
            //data estorno
            $dateObj2 =  date('Y-m-d', strtotime(date('Y-m-d')));
            $dataestorno = new DateTime($dateObj2);
            
            $diff = $dataabandono->diff($dataestorno);
            $datadiff = $diff->d;

            echo json_encode(array("status"=>"true", "mensagem"=>"Abandono do aluno registrado com sucesso", "dias" => $datadiff));
        }
        die();
    }

    /**
     * @param int $idaluno
     * @param Date datafalecimento
     * @return Obj - Status e Mensagem
     */
    public function baixamatfalecimentoAction(){
        error_reporting(0);
        ini_set("display_errors", 0 );
        $this->_helper->layout->disableLayout();

        $id = $this->getRequest()->getPost('idAluno');
        $data = $this->getRequest()->getPost('datafalecimento');
        $data = date("Y-m-d", Mn_Util::stringToTime($data));
        
        $alunos = new Escolasalunos();
        $aluno = $alunos->getEscolaalunoById($id);

        $inRA = $aluno["ra"];
        $inDigitoRA = $aluno["digra"];
        $inUFRA = $aluno["ufra"];

        //inserir dados no array
        $dados = array();
        $dados['id'] = $id;
        $dados['ra'] = $inRA;
        $dados['digra'] = $inDigitoRA;
        $dados['ufra'] = $inUFRA;
        $dados['datafalecimento'] = $data;
        
        $baixaFalecimento = SED::BaixarMatrFalecimentoRA($dados);
        
        if(isset($baixaFalecimento["Mensagens"]["BaixarMatricula"]["outErro"])){
            echo json_encode(array("status"=>"false", "mensagem" => $baixaFalecimento["Mensagens"]["BaixarMatricula"]["outErro"]));
        }else{
            $queries = array();
            $queries['id'] = $id;
            $queries['falecimento'] = "sim";
            $queries['datafalecimento'] = $data;
            $queries['status'] = "Falecido";
                       
            $escolasaluno = new Escolasalunos();
            $escolasaluno->save($queries);
          
            echo json_encode(array("status"=>"true", "mensagem"=>"Baixa por falecimento cadastrada com sucesso"));
        }
        
        die();
    }

    /**
     * @param int $idaluno     
     * @return Obj - Status e Mensagem     
     */
    public function estornoabandonoAction(){
        error_reporting(0);
        ini_set("display_errors", 0 );
        $this->_helper->layout->disableLayout();

        $id = $this->getRequest()->getPost('idAluno');
        $alunos = new Escolasalunos();
        $aluno = $alunos->getEscolaalunoById($id);

        $inRA = $aluno["ra"];
        $inDigitoRA = $aluno["digra"];
        $inUFRA = $aluno["ufra"];
        $inNumClasse = $aluno["classe"];

        //inserir dados no array
        $dados = array();
        $dados['id'] = $id;
        $dados['ra'] = $inRA;
        $dados['digra'] = $inDigitoRA;
        $dados['ufra'] = $inUFRA;
        $dados['classe'] = $inNumClasse;

        $registroEstorno = SED::EstornarRegistroAbandono($dados);

        if(isset($registroEstorno["Mensagens"]["EstornarRegAbandono"]["outErro"])){
            echo json_encode(array("status"=>"false", "mensagem" => $registroEstorno["Mensagens"]["EstornarRegAbandono"]["outErro"]));            
        }else{
            $queries = array();
            $queries['id'] = $id;
            $queries['status'] = "Ativo";
            $queries['abandono'] = "nao";
            $queries['dataabandono'] = date("0000-00-00 00-00-00");;
            $queries['dataestorno'] = date('Y-m-d H:g:s');
            
            $escolasaluno = new Escolasalunos();
            $escolasaluno->save($queries);

            echo json_encode(array("status"=>"true", "mensagem" => "Estorno de abandono registrado com sucesso"));
        }
        
        die();
    }

    /**
     * @param int $idaluno     
     * @return Obj - Status e Mensagem
     */
    public function registrarnaocomparecimentoAction(){
        error_reporting(0);
        ini_set("display_errors", 0 );
        $this->_helper->layout->disableLayout();

        $id = $this->getRequest()->getPost('idAluno');
        $alunos = new Escolasalunos();
        $aluno = $alunos->getEscolaalunoById($id);

        $inRA = $aluno["ra"];
        $inDigitoRA = $aluno["digra"];
        $inUFRA = $aluno["ufra"];
        $inNumClasse = $aluno["classe"];

        //inserir dados no array
        $dados = array();
        $dados['id'] = $id;
        $dados['ra'] = $inRA;
        $dados['digra'] = $inDigitoRA;
        $dados['ufra'] = $inUFRA;
        $dados['classe'] = $inNumClasse;

        $registroEstorno = SED::RegistrarNaoComparecimento($dados);

        if(isset($registroEstorno["Mensagens"]["BaixarMatricula"]["outErro"])){
            echo json_encode(array("status"=>"false", "mensagem" => $registroEstorno["Mensagens"]["BaixarMatricula"]["outErro"]));
        }else{
            $queries = array();
            $queries['id'] = $id;           
            $queries['abandono'] = "sim";           
            $queries['dataabandono'] = date('Y-m-d H:g:s');
            $queries['status'] = "Não Compareceu";
            
            $escolasaluno = new Escolasalunos();
            $escolasaluno->save($queries);

            echo json_encode(array("status"=>"true", "mensagem"=>"Não comparecimento do aluno confirmado com sucesso"));
        }
        
        die();
    }

    /**
     * Atribui valores ao view
     * @param int $idaluno
     */
    private function preForm($idaluno = 0) {

        $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $this->view->escolas = Escolas::getEscolasHelper(array('status' => 'Ativo', 'idsecretaria' => $this->view->idsecretaria));

        $instituicoes = new Escolasinstituicoesnecessidades();
        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['status'] = 'Ativo';
        $this->view->instituicoesapoio = $instituicoes->getInstituicoes($queries);

        $programas = new Escolasprogramassociais();
        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['status'] = 'Ativo';
        $this->view->programassociais = $programas->getProgramas($queries);
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_registro = false) {

        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $idescola = strip_tags((int) $this->getRequest()->getPost("idescola"));
        $idendereco = strip_tags((int) $this->getRequest()->getPost("idendereco"));

        $idclassificacao = strip_tags((int) $this->getRequest()->getPost("idclassificacao"));
        $idarquivodatanascimento = (int) $this->getArquivo('arquivodatanascimento');
        $input_webcam = trim($this->getRequest()->getPost("input_webcam"));
        $idimagemfoto = (int) $this->getImagem(false, 'idimagemfoto');

        $nomerazao = strip_tags(trim($this->getRequest()->getPost("nomerazao")));
        $sobrenomefantasia = strip_tags(trim($this->getRequest()->getPost("sobrenomefantasia")));
        $cpfcnpj = strip_tags(trim($this->getRequest()->getPost("cpfcnpj")));
        $nascimento = strip_tags(trim($this->getRequest()->getPost("nascimento")));
        $_senha = strip_tags(trim($this->getRequest()->getPost("nascimento")));
        $nascimento = Mn_Util::stringToTime($nascimento);
        $nascimento = date('Y-m-d G:i:s', $nascimento);

        $email = strip_tags(trim($this->getRequest()->getPost("email")));
        $_senha = explode('/', $_senha);
        $_senha = implode('', $_senha);
        //$senha = strip_tags(trim($this->getRequest()->getPost("senha")));

        $inUF = strip_tags(trim($this->getRequest()->getPost("inUF")));
        $logradouro = strip_tags(trim($this->getRequest()->getPost("logradouro")));
        $tipologradouro = strip_tags(trim($this->getRequest()->getPost("tipoLogradouro")));

        $cep = strip_tags(trim($this->getRequest()->getPost("cep")));
        $numero = strip_tags(trim($this->getRequest()->getPost("numero")));
        $complemento = strip_tags(trim($this->getRequest()->getPost("complemento")));

        $bairro = strip_tags(trim($this->getRequest()->getPost("bairro")));
        $cidade = strip_tags(trim($this->getRequest()->getPost("cidade")));
        $idestado = strip_tags(trim($this->getRequest()->getPost("idestado")));

        $indLogradouro = strip_tags(trim($this->getRequest()->getPost("indLogradouro")));
        $indNumero = strip_tags(trim($this->getRequest()->getPost("indNumero")));
        $indBairro = strip_tags(trim($this->getRequest()->getPost("indBairro")));
        $indCidade = strip_tags(trim($this->getRequest()->getPost("indCidade")));
        $indUF = strip_tags(trim($this->getRequest()->getPost("indUF")));

        //contato
        $foneresidencial = strip_tags(trim($this->getRequest()->getPost("foneresidencial")));
        $inDDD = strip_tags(trim($this->getRequest()->getPost("inDDD")));

        $fonerecado = strip_tags(trim($this->getRequest()->getPost("fonerecado")));
        $nomefonerecado = strip_tags(trim($this->getRequest()->getPost("nomefonerecado")));
        $inDDDCel = strip_tags(trim($this->getRequest()->getPost("inDDDCel")));
        $foneCel = strip_tags(trim($this->getRequest()->getPost("foneCel")));
        $sms = strip_tags(trim($this->getRequest()->getPost("sms")));
        $sms = (isset($sms) AND $sms == 'on') ? "S" : "N";
        $status = strip_tags(trim($this->getRequest()->getPost("status")));

        $nomemae = strip_tags(trim($this->getRequest()->getPost("nomemae")));
        $cpfcnpjmae = strip_tags(trim($this->getRequest()->getPost("cpfcnpjmae")));
        $rgmae = strip_tags(trim($this->getRequest()->getPost("rgmae")));
        $nascimentomae = strip_tags(trim($this->getRequest()->getPost("nascimentomae")));
        $nascimentomae = Mn_Util::stringToTime($nascimentomae);
        $nascimentomae = date('Y-m-d G:i:s', $nascimentomae);

        $nomepai = strip_tags(trim($this->getRequest()->getPost("nomepai")));
        $cpfcnpjpai = strip_tags(trim($this->getRequest()->getPost("cpfcnpjpai")));
        $rgpai = strip_tags(trim($this->getRequest()->getPost("rgpai")));
        $nascimentopai = strip_tags(trim($this->getRequest()->getPost("nascimentopai")));
        $nascimentopai = Mn_Util::stringToTime($nascimentopai);
        $nascimentopai = date('Y-m-d G:i:s', $nascimentopai);

        $naturalidade = strip_tags(trim($this->getRequest()->getPost("naturalidade")));
        //$nacionalidade = strip_tags(trim($this->getRequest()->getPost("nacionalidade")));
        $nacionalidadetipo = strip_tags(trim($this->getRequest()->getPost("inNacionalidade")));
        $nacionalidade = strip_tags(trim($this->getRequest()->getPost("inPaisOrigem")));
        $nacionalidadeentradadata = strip_tags(trim($this->getRequest()->getPost("inDiaEntBrasil")));
        $rg = strip_tags(trim($this->getRequest()->getPost("rg")));
        $inDataEmissaoRGRNE = strip_tags(trim($this->getRequest()->getPost("inDataEmissaoRGRNE")));

        $rgm = strip_tags(trim($this->getRequest()->getPost("rgm")));

        $idserie = strip_tags(trim($this->getRequest()->getPost("idserie")));
        $idperiodo = strip_tags(trim($this->getRequest()->getPost("idperiodo")));

        $matricula = strip_tags(trim($this->getRequest()->getPost("matricula")));
        $numeromatricula = strip_tags(trim($this->getRequest()->getPost("numeromatricula")));
        $ra = strip_tags(trim($this->getRequest()->getPost("ra")));
        $digra = strip_tags(trim($this->getRequest()->getPost("digra")));
        $ufra = strip_tags(trim($this->getRequest()->getPost("ufra")));
        $classificacao = strip_tags(trim($this->getRequest()->getPost("classificacao")));

        $cgm = strip_tags(trim($this->getRequest()->getPost("cgm")));
        $nis = strip_tags(trim($this->getRequest()->getPost("nis")));
        $sexo = strip_tags(trim($this->getRequest()->getPost("sexo")));
        //$iddatanascimento = strip_tags(trim($this->getRequest()->getPost("iddatanascimento")));
        //$idfoto = strip_tags(trim($this->getRequest()->getPost("idfoto")));
        $etnia = strip_tags(trim($this->getRequest()->getPost("etnia")));
        $quilombola = strip_tags(trim($this->getRequest()->getPost("quilombola")));
        $tipocertidao = strip_tags(trim($this->getRequest()->getPost("tipocertidao")));
        
        $digrg = strip_tags(trim($this->getRequest()->getPost("digrg")));
        $ufrg = strip_tags(trim($this->getRequest()->getPost("ufrg")));
        $municipio_de_nascimento = strip_tags(trim($this->getRequest()->getPost("municipio_de_nascimento")));
        $uf_mun_de_nascimento = strip_tags(trim($this->getRequest()->getPost("uf_mun_de_nascimento")));
        //$data_emis_certidao_nasc = strip_tags(trim($this->getRequest()->getPost("data_emis_certidao_nasc")));
        
        $nova_cert_cod_nasc_serv = strip_tags(trim($this->getRequest()->getPost("nova_cert_cod_nasc_serv")));
        $numero_certidao_nasc = strip_tags(trim($this->getRequest()->getPost("numero_certidao_nasc")));
        $nova_cert_cod_acervo = strip_tags(trim($this->getRequest()->getPost("nova_cert_cod_acervo")));
        $nova_cert_ano_reg_nasc = strip_tags(trim($this->getRequest()->getPost("nova_cert_ano_reg_nasc")));
        $nova_cert_num_termo_reg = strip_tags(trim($this->getRequest()->getPost("nova_cert_num_termo_reg")));
        $nova_cert_cod_serv_reg_civil = strip_tags(trim($this->getRequest()->getPost("nova_cert_cod_serv_reg_civil")));
        $nova_cert_tipo_livro_reg = strip_tags(trim($this->getRequest()->getPost("nova_cert_tipo_livro_reg")));
        $nova_cert_num_livro = strip_tags(trim($this->getRequest()->getPost("nova_cert_num_livro")));
        $nova_cert_num_folha = strip_tags(trim($this->getRequest()->getPost("nova_cert_num_folha")));
        $num_folha_certidao_nasc = strip_tags(trim($this->getRequest()->getPost("num_folha_certidao_nasc")));
        $num_livro_certidao_nasc = strip_tags(trim($this->getRequest()->getPost("num_livro_certidao_nasc")));
        $numero_certidao_nasc_nova = strip_tags(trim($this->getRequest()->getPost("numero_certidao_nasc_nova")));
        $digver_certidao_nasc_nova = strip_tags(trim($this->getRequest()->getPost("digver_certidao_nasc_nova")));
        
        $inDistritoCertidao = strip_tags(trim($this->getRequest()->getPost("inDistritoCertidao")));
        $inMunicipioComarca = strip_tags(trim($this->getRequest()->getPost("inMunicipioComarca")));
        $inUFComarca = strip_tags(trim($this->getRequest()->getPost("inUFComarca")));
        $data_emis_certidao_nasc = strip_tags(trim($this->getRequest()->getPost("data_emis_certidao_nasc")));

        $rendafamiliar = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("rendafamiliar"))));
        $passeescolar = strip_tags(trim($this->getRequest()->getPost("passeescolar")));
        $transporteescolar = strip_tags(trim($this->getRequest()->getPost("transporteescolar")));
        $restricaoalimentar = strip_tags(trim($this->getRequest()->getPost("restricaoalimentar")));
        $observacoes = strip_tags(trim($this->getRequest()->getPost("observacoes")));

        /*$data_emis_certidao_nasc = strip_tags(trim($this->getRequest()->getPost("data_emis_certidao_nasc")));
        $data_emis_certidao_nasc = Mn_Util::stringToTime($data_emis_certidao_nasc);
        $data_emis_certidao_nasc = date('Y-m-d G:i:s', $data_emis_certidao_nasc);*/

        $tituloeleitoralmae = strip_tags(trim($this->getRequest()->getPost("tituloeleitoralmae")));
        $idarquivonascimentomae = (int) $this->getArquivo('arquivonascimentomae');
        $tituloeleitoralpai = strip_tags(trim($this->getRequest()->getPost("tituloeleitoralpai")));
        $idarquivonascimentopai = (int) $this->getArquivo('arquivonascimentopai');
        $filiacao = strip_tags(trim($this->getRequest()->getPost("filiacao")));
        $nomeresponsavel = strip_tags(trim($this->getRequest()->getPost("nomeresponsavel")));
        $cpfcnpjresponsavel = strip_tags(trim($this->getRequest()->getPost("cpfcnpjresponsavel")));

        $rgresponsavel = strip_tags(trim($this->getRequest()->getPost("rgresponsavel")));
        $nascimentoresponsavel = strip_tags(trim($this->getRequest()->getPost("nascimentoresponsavel")));
        //$nascimentoresponsavel = Mn_Util::stringToTime($nascimentoresponsavel);
        $nascimentoresponsavel = date('Y-m-d G:i:s', strtotime($nascimentoresponsavel));
        $tituloeleitoralresponsavel = strip_tags(trim($this->getRequest()->getPost("tituloeleitoralresponsavel")));
        $idarquivonascimentoresponsavel = (int) $this->getArquivo('arquivonascimentoresponsavel');

        $necessidadesespeciais = strip_tags(trim($this->getRequest()->getPost("necessidadesespeciais")));
        $descricoesnecessidadesespeciais = strip_tags(trim($this->getRequest()->getPost("descricoesnecessidadesespeciais")));
        $idinstituicaoapoio = strip_tags(trim((int) $this->getRequest()->getPost("idinstituicaoapoio")));

        $observacaodeficiencia = strip_tags(trim((int) $this->getRequest()->getPost("observacaodeficiencia")));
        $tipodeficiencia = strip_tags(trim((int) $this->getRequest()->getPost("tipodeficiencia")));

        $dataefetivacao = strip_tags(trim($this->getRequest()->getPost("dataefetivacao")));
        $usuarioefetivacao = (int) strip_tags(trim($this->getRequest()->getPost("usuarioefetivacao")));

        $motivo = strip_tags(trim($this->getRequest()->getPost("motivo")));
        $situacaoatual = strip_tags(trim($this->getRequest()->getPost("situacaoatual")));
        $redeorigem = strip_tags(trim($this->getRequest()->getPost("redeorigem")));
        $ensinoorigem = strip_tags(trim($this->getRequest()->getPost("ensinoorigem")));
        $situacaoanoanterior = strip_tags(trim($this->getRequest()->getPost("situacaoanoanterior")));
        $formaingresso = strip_tags(trim($this->getRequest()->getPost("formaingresso")));

        $estadocivilmae = strip_tags(trim($this->getRequest()->getPost("estadocivilmae")));
        $estadocivilpai = strip_tags(trim($this->getRequest()->getPost("estadocivilpai")));
        $estadocivilresponsavel = strip_tags(trim($this->getRequest()->getPost("estadocivilresponsavel")));
        $merendadiferenciada = strip_tags(trim($this->getRequest()->getPost("merendadiferenciada")));
        $merendadiferenciadaaprovacao = strip_tags(trim($this->getRequest()->getPost("merendadiferenciadaaprovacao")));


        $idsprogramas = (is_array($this->getRequest()->getPost("idsprogramas"))) ? array_filter($this->getRequest()->getPost("idsprogramas")) : array();
        $situacoes = (is_array($this->getRequest()->getPost("situacoes"))) ? array_filter($this->getRequest()->getPost("situacoes")) : array();
        $idsitens = (is_array($this->getRequest()->getPost("idsitens"))) ? array_filter($this->getRequest()->getPost("idsitens")) : array();

        /* Deficiências */
        $defnecespecial = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defnecespecial"))));
        $defmultipla = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defmultipla"))));
        $defcegueira = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defcegueira"))));
        $defbaixavisao = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defbaixavisao"))));
        $defsurdezseveraprofunda = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defsurdezseveraprofunda"))));
        $defsurdezlevemoderada = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defsurdezlevemoderada"))));
        $defsurdocegueira = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defsurdocegueira"))));
        $deffisicaparalisiacerebral = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("deffisicaparalisiacerebral"))));
        $deffisicacadeirante = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("deffisicacadeirante"))));
        $deffisicaoutros = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("deffisicaoutros"))));
        $defsindromedown = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defsindromedown"))));
        $defintelectual = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defintelectual"))));
        $defautistaclassico = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defautistaclassico"))));
        $defsindromeasperger = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defsindromeasperger"))));
        $defsindromerett = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defsindromerett"))));
        $deftransdesintegrativoinf = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("deftransdesintegrativoinf"))));
        $defaltashabsuperdotacao = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defaltashabsuperdotacao"))));
        $defpermanente = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defpermanente"))));
        $deftemporaria = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("deftemporaria"))));
        $defmobilidadereduzida = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defmobilidadereduzida"))));
        $defauxilioleitor = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defauxilioleitor"))));
        $defauxiliotranscricao = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defauxiliotranscricao"))));
        $defguiainterprete = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defguiainterprete"))));
        $definterpretelibras = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("definterpretelibras"))));
        $defleituralabial = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defleituralabial"))));
        $defnenhum = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defnenhum"))));
        $defprovabraile = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defprovabraile"))));
        $defprovaampliada = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("defprovaampliada"))));
        $deftam16 = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("deftam16"))));
        $deftam20 = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("deftam20"))));
        $deftam24 = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("deftam24"))));
        /* Fim Deficiências */
        $soapstatus = trim($this->getRequest()->getPost("soapstatus"));
        $soaptipo = trim($this->getRequest()->getPost("soaptipo"));
        $soapretorno = trim($this->getRequest()->getPost("soapretorno"));

        $status = strip_tags(trim($this->getRequest()->getPost("status1")));

        /* MULTIPLOS ARQUIVOS */
        $idsarquivos = $this->getRequest()->getPost("idsarquivos");
        $legendasarquivos = $this->getRequest()->getPost("legendasarquivos");


        $erros = array();
        if ("" == $logradouro)
            array_push($erros, 'Preencha o campo ENDERECO.');
        if ("" == $cep)
            array_push($erros, 'Preencha o campo CEP.');
        if ("" == $numero)
            array_push($erros, 'Preencha o campo NUMERO.');
        if ("" == $bairro)
            array_push($erros, 'Preencha o campo BAIRRO.');
        //if (""==$idestado) array_push($erros, 'Selecione um ESTADO.');
        if ("" == $cidade)
            array_push($erros, 'Preencha o campo CIDADE.');
        if ($sms == "S" AND empty($inDDDCel) || empty($foneCel))
            array_push($erros, 'Para receber SMS informe o DDD e NÚMERO do CELULAR');

        if ("" == Mn_Util::formPostVarData($nascimento))
            array_push($erros, 'Preencha o campo DATA DE NASCIMENTO.');
        if ("" == $status)
            array_push($erros, 'Selecione um STATUS.');

        if (!empty($logradouro) AND ! empty($bairro) AND ! empty($cidade) AND $inUF) {
            $inlatlong = $this->getLatitudeLongitudeByAddress($logradouro, $numero, $bairro, $cidade, $inUF);
        }

        if (!empty($indLogradouro) AND ! empty($indBairro) AND ! empty($indCidade) AND $indUF) {
            $inLatLong = $this->getLatitudeLongitudeByAddress($indLogradouro, $indNumero, $indBairro, $indCidade, $indUF);
        }
        
        $alunos = new Escolasalunos();

        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            /* $idendereco = ($_registro) ? (int) $_registro['idendereco'] : 0;
              $enderecosModel = new EnderecoModel($this);
              $endereco = $enderecosModel->getPost($idendereco);
              $idendereco = ($endereco) ? $endereco['id'] : 0; */

            $dados = array();
            $dados['id'] = $id;
            $dados['idescola'] = $idescola;
            // $dados['idendereco'] = $idendereco;
            $dados['idclassificacao'] = $idclassificacao;
            $dados['merendadiferenciada'] = $merendadiferenciada;
            $dados['merendadiferenciadaaprovacao'] = $merendadiferenciadaaprovacao;
            // $dados['idarquivo'] = $idarquivo;
            $dados['nomerazao'] = $nomerazao;
            $dados['sobrenomefantasia'] = $sobrenomefantasia;
            $dados['cpfcnpj'] = Escolasalunos::removeSpecialCaracters($cpfcnpj);
            $dados['nascimento'] = $nascimento;
            $dados['email'] = $email;
            $dados['senha'] = $_senha;
            $dados['nomemae'] = $nomemae;
            $dados['cpfcnpjmae'] = $cpfcnpjmae;
            $dados['rgmae'] = $rgmae;
            $dados['nascimentomae'] = $nascimentomae;
            $dados['nomepai'] = $nomepai;
            $dados['cpfcnpjpai'] = $cpfcnpjpai;
            $dados['rgpai'] = $rgpai;
            $dados['nascimentopai'] = $nascimentopai;
            $dados['naturalidade'] = $naturalidade;
            $dados['nacionalidadetipo'] = $nacionalidadetipo;
            $dados['nacionalidade'] = $nacionalidade;
            //código do BR - 76
            if($nacionalidadetipo == 1) $dados['nacionalidade'] = 76;

            $dados['nacionalidadeentradadata'] = (isset($nacionalidadeentradadata)) ? date("Y-m-d G:i:s", MN_Util::stringToTime($nacionalidadeentradadata)) : '';
           
            $dados['inDataEmissaoRGRNE'] = date("Y-m-d G:i:s", MN_Util::stringToTime($inDataEmissaoRGRNE));
            $dados['rg'] = $rg;
            $dados['rgm'] = $rgm;
            $dados['idserie'] = $idserie;
            $dados['classificacao'] = $classificacao;
            $dados['idperiodo'] = $idperiodo;
            /* $dados['telefoneresidencial'] = $telefoneresidencial;
              $dados['telefonecontato'] = $telefonecontato;
              $dados['nomecontato'] = $nomecontato; */
            $dados['matricula'] = $matricula;
            $dados['numeromatricula'] = $numeromatricula;
            $dados['ra'] = $ra;
            $dados['digra'] = $digra;
            $dados['ufra'] = $ufra;

            $dados['cgm'] = $cgm;
            $dados['nis'] = $nis;
            $dados['sexo'] = $sexo;
            $dados['idarquivodatanascimento'] = $idarquivodatanascimento;

            if ($input_webcam != '') {
                $input_webcam = str_replace("data:image/png;base64,", "", $input_webcam);
                $imagens = new Imagens();
                $__imagem = $imagens->saveBase64($input_webcam);
                $download = $imagens->downloadImagem($__imagem['id']);

                $simple = new SimpleImage();
                $simple->load($download['sourceFilename']);

                $src = $simple->image;
                $height = imagesy($src);
                $width = floor(($height / 4) * 3);
                $pos_x = floor((imagesx($src) - $width) / 2);

                $dest = imagecreatetruecolor($width, $height);
                imagecopy($dest, $src, 0, 0, $pos_x, 0, $width, $height);
                imagejpeg($dest, $download['sourceFilename'], 100);

                $dados['idimagemfoto'] = $__imagem['id'];
            } else {
                $idimagemfoto = $this->getImagem('idimagemfoto');
                if ($idimagemfoto != 0)
                    $dados['idimagemfoto'] = $idimagemfoto;
            }

            $dados['etnia'] = $etnia;
            $dados['quilombola'] = $quilombola;           
            $dados['tipocertidao'] = $tipocertidao;
            
            $dados['digrg'] = $digrg;
            $dados['ufrg'] = $ufrg;
            $dados['nascimentomunicipio'] = $municipio_de_nascimento;
            $dados['nascimentouf'] = $uf_mun_de_nascimento;
            $dados['data_emis_certidao_nasc'] = $data_emis_certidao_nasc;
            $dados['nova_cert_cod_nasc_serv'] = $nova_cert_cod_nasc_serv;
            $dados['nova_cert_cod_acervo']  = $nova_cert_cod_acervo;
            $dados['nova_cert_cod_serv_reg_civil'] = $nova_cert_cod_serv_reg_civil;
            $dados['nova_cert_tipo_livro_reg'] = $nova_cert_tipo_livro_reg;
            $dados['nova_cert_num_livro'] = $nova_cert_num_livro;
            $dados['nova_cert_num_folha'] = $nova_cert_num_folha;
            $dados['numero_certidao_nasc'] = $numero_certidao_nasc;
            $dados['nova_cert_ano_reg_nasc'] = $nova_cert_ano_reg_nasc;
            $dados['nova_cert_num_termo_reg'] = $nova_cert_num_termo_reg;
            $dados['num_folha_certidao_nasc'] = $num_folha_certidao_nasc;
            $dados['num_livro_certidao_nasc'] = $num_livro_certidao_nasc;
            $dados['numero_certidao_nasc_nova'] = $numero_certidao_nasc_nova;
            $dados['digver_certidao_nasc_nova'] = $digver_certidao_nasc_nova;

            $dados['inDistritoCertidao'] = $inDistritoCertidao;
            $dados['inMunicipioComarca'] = $inMunicipioComarca;
            $dados['inUFComarca'] = $inUFComarca;
            $dados['data_emis_certidao_nasc'] = date("Y-m-d G:i:s", Mn_Util::stringToTime($data_emis_certidao_nasc));

            $dados['rendafamiliar'] = $rendafamiliar;
            $dados['passeescolar'] = $passeescolar;
            $dados['transporteescolar'] = $transporteescolar;
            $dados['restricaoalimentar'] = $restricaoalimentar;
            $dados['observacoes'] = $observacoes;

            $dados['tituloeleitoralmae'] = $tituloeleitoralmae;
            $dados['idarquivonascimentomae'] = $idarquivonascimentomae;
            $dados['tituloeleitoralpai'] = $tituloeleitoralpai;
            $dados['idarquivonascimentopai'] = $idarquivonascimentopai;
            $dados['filiacao'] = $filiacao;
            $dados['nomeresponsavel'] = $nomeresponsavel;
            $dados['cpfcnpjresponsavel'] = $cpfcnpjresponsavel;
            $dados['rgresponsavel'] = $rgresponsavel;
            $dados['nascimentoresponsavel'] = $nascimentoresponsavel;
            $dados['tituloeleitoralresponsavel'] = $tituloeleitoralresponsavel;
            $dados['idarquivonascimentoresponsavel'] = $idarquivonascimentoresponsavel;

            $dados['necessidadesespeciais'] = $necessidadesespeciais;
            $dados['descricoesnecessidadesespeciais'] = $descricoesnecessidadesespeciais;
            $dados['idinstituicaoapoio'] = $idinstituicaoapoio;

            $dados['observacaodeficiencia'] = $observacaodeficiencia;
            $dados['tipodeficiencia'] = $tipodeficiencia;

            $dados['dataefetivacao'] = $dataefetivacao;
            $dados['usuarioefetivacao'] = $usuarioefetivacao;
            $dados['ensinoorigem'] = $ensinoorigem;
            $dados['situacaoanoanterior'] = $situacaoanoanterior;
            $dados['formaingresso'] = $formaingresso;
            $dados['motivo'] = $motivo;
            $dados['situacaoatual'] = $situacaoatual;
            $dados['redeorigem'] = $redeorigem;

            $dados['estadocivilmae'] = $estadocivilmae;
            $dados['estadocivilpai'] = $estadocivilpai;
            $dados['estadocivilresponsavel'] = $estadocivilresponsavel;

            /* Deficiências */
            $dados['defnecespecial'] = $defnecespecial;
            $dados['defmultipla'] = $defmultipla;
            $dados['defcegueira'] = $defcegueira;
            $dados['defbaixavisao'] = $defbaixavisao;
            $dados['defsurdezseveraprofunda'] = $defsurdezseveraprofunda;
            $dados['defsurdezlevemoderada'] = $defsurdezlevemoderada;
            $dados['defsurdocegueira'] = $defsurdocegueira;
            $dados['deffisicaparalisiacerebral'] = $deffisicaparalisiacerebral;
            $dados['deffisicacadeirante'] = $deffisicacadeirante;
            $dados['deffisicaoutros'] = $deffisicaoutros;
            $dados['defsindromedown'] = $defsindromedown;
            $dados['defintelectual'] = $defintelectual;
            $dados['defautistaclassico'] = $defautistaclassico;
            $dados['defsindromeasperger'] = $defsindromeasperger;
            $dados['defsindromerett'] = $defsindromerett;
            $dados['deftransdesintegrativoinf'] = $deftransdesintegrativoinf;
            $dados['defaltashabsuperdotacao'] = $defaltashabsuperdotacao;
            $dados['defpermanente'] = $defpermanente;
            $dados['deftemporaria'] = $deftemporaria;
            $dados['defmobilidadereduzida'] = $defmobilidadereduzida;
            $dados['defauxilioleitor'] = $defauxilioleitor;
            $dados['defauxiliotranscricao'] = $defauxiliotranscricao;
            $dados['defguiainterprete'] = $defguiainterprete;
            $dados['definterpretelibras'] = $definterpretelibras;
            $dados['defleituralabial'] = $defleituralabial;
            $dados['defnenhum'] = $defnenhum;
            $dados['defprovabraile'] = $defprovabraile;
            $dados['defprovaampliada'] = $defprovaampliada;
            $dados['deftam16'] = $deftam16;
            $dados['deftam20'] = $deftam20;
            $dados['deftam24'] = $deftam24;
            /* Fim Deficiências */

            $dados["soapstatus"] = $soapstatus;
            $dados["soaptipo"] = $soaptipo;
            $dados["soapretorno"] = $soapretorno;

            $dados['status'] = $status;
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = Usuarios::getUsuario('id');
            $dados['logdata'] = date('Y-m-d G:i:s');

            /* --início endereço-- */
            $dados_endereco = array();
            $dados_endereco['inTipoLogradouro'] = $tipologradouro;
            $dados_endereco['inUF'] = $inUF;
            $dados_endereco['inLogradouro'] = $logradouro;
            $dados_endereco['inCep'] = $this->removeSpecialCaracters($cep);
            $dados_endereco['inNumero'] = $numero;
            $dados_endereco['inComplemento'] = $complemento;
            $dados_endereco['inBairro'] = $bairro;
            $dados_endereco['inCidade'] = $cidade;

            $dados_endereco['inDDD'] = $inDDD;
            $dados_endereco['inFoneResidencial'] = $this->removeSpecialCaracters($foneresidencial);
            $dados_endereco['inFoneRecados'] = $this->removeSpecialCaracters($fonerecado);
            $dados_endereco['inNomeFoneRecado'] = $nomefonerecado;
            $dados_endereco['inSMS'] = $sms;
            $dados_endereco['inDDDCel'] = $inDDDCel;
            $dados_endereco['inFoneCel'] = $this->removeSpecialCaracters($foneCel);

            $dados_endereco['inLatitude'] = (isset($inlatlong)) ? $inlatlong['latitude'] : '';
            $dados_endereco['inLongitude'] = (isset($inlatlong)) ? $inlatlong['longitude'] : '';

            //endereço indicativo
            $dados_endereco['endIndicativoInBairro'] = $indBairro;
            $dados_endereco['endIndicativoinCidade'] = $indCidade;
            $dados_endereco['endIndicativoinLatitude'] = (isset($inLatLong)) ? $inLatLong['latitude'] : '';
            $dados_endereco['endIndicativoinLongitude'] = (isset($inLatLong)) ? $inLatLong['longitude'] : '';
            $dados_endereco['endIndicativoinLogradouro'] = $indLogradouro;
            $dados_endereco['endIndicativoinNumero'] = $indNumero;
            $dados_endereco['endIndicativoinUF'] = $indUF;

            //$dados_endereco['idestado'] = $idestado;
            $dados_endereco['datacriacao'] = date('Y-m-d');
            $dados_endereco['excluido'] = 'nao';
            $dados_endereco['logusuario'] = Usuarios::getUsuario('id');
            $dados_endereco['logdata'] = date('Y-m-d G:i:s');
            $dados['enderecos'] = $dados_endereco;
            /* --FIM ENDEREÇO-- */

            $dados_programas = array();
            $dados_programas['ids'] = $idsitens;
            $dados_programas['idsprogramas'] = $idsprogramas;
            $dados_programas['situacoes'] = $situacoes;
            $dados_programas['excluido'] = 'nao';
            $dados_programas['logusuario'] = Usuarios::getUsuario('id');
            $dados_programas['logdata'] = date('Y-m-d G:i:s');
            $dados['programas'] = $dados_programas;

          
            //SED::DadosPessoaisFichaAluno($dados, (($id === 0) ? 'Inc' : 'Alt'));
          
            $row = $alunos->save($dados);
            
            if($id===0){
                Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Aluno adicionado com sucesso!");
            }else Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Aluno do ID ".$id." atualizado com sucesso!");
            
            if ((isset($merendadifenciada)) && ($merendadifenciada == "Sim")) {

                $rows_diferenciadas = Nutricaomerendasdiferenciadas::getNutricaomerendasdiferenciadasHelper(array('idescola' => $idescola));
                //var_dump($rows_diferenciadas[0]['id']);die();
                if (isset($rows_diferenciadas[0]['id'])) {
                    $dados_merendas = array();
                    $verificaid = Nutricaomerendas::getNutricaomerendasHelper(array('idaluno' => $row['id']));
                    if (isset($verificaid[0]['id'])) {
                        $dados_merendas['id'] = $verificaid[0]['id'];                       
                    }
                    $dados_merendas["idsecretaria"] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
                    $dados_merendas["idescola"] = $idescola;
                    $dados_merendas["idaluno"] = $row['id'];
                    $dados_merendas["titulo"] = $rows_diferenciadas[0]['titulo'];
                    $dados_merendas["descricoes"] = $rows_diferenciadas[0]['descricoes'];

                    $dados_merendas["status"] = $rows_diferenciadas[0]['status'];


                    $dados_merendas['excluido'] = 'nao';
                    $dados_merendas['logusuario'] = Usuarios::getUsuario('id');
                    $dados_merendas['logdata'] = date('Y-m-d G:i:s');
                    //var_dump($dados_merendas); die();
                    $nutricaomerendas = new Nutricaomerendas();
                    $row = $nutricaomerendas->save($dados_merendas);
                }
            }
            
            $multiplosarquivos = new Escolasalunosarquivosmatricula();
            $multiplosarquivos->setArquivos($row['id'], $idsarquivos, $legendasarquivos);

            $db->commit();

            // Realiza a operação no SED
            SED::DadosPessoaisFichaAluno($dados, (($id === 0) ? 'Inc' : 'Alt'));            

        } catch (PDOException $e) {
            Fjmx_Util::geraLog(date('Y-m-d'), "Salvar Aluno", Usuarios::getUsuario('id'), "Erro: " . $e->getMessage());
            $db->rollBack();
            die($e);
        }

        return "";
    }

    private function getLatitudeLongitude($idendereco, $idaluno) {
        if ((int) $idendereco <= 0)
            return false;
        $enderecos = new Enderecos();
        $row = $enderecos->getEnderecoByIdendereco($idendereco);
        if (!$row)
            return false;

        $alunos = new Escolasalunos();
        $aluno = $alunos->getEscolaalunoById($idaluno);
        if (!$aluno)
            return false;

        //$endereco = utf8_decode(str_replace(" ", "+", $row['endereco']));
        $endereco = str_replace(" ", "+", $row['endereco']);

        $address = $endereco . ',' . $row['numero'] . ',' . $row['cidade'] . ',' . $row['uf'];
        $address = str_replace(" ", "+", $address);
        //$address = utf8_decode($address);
        //$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=R.+%C3%89rico+Ver%C3%ADssimo,125,Hortol%C3%A2ndia,SP,brasil&sensor=false');
        $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . $address . ',brasil');

        $output = json_decode($geocode);

        $lat = $output->results[0]->geometry->location->lat;
        $long = $output->results[0]->geometry->location->lng;

        $row['latitude'] = $lat;
        $row['longitude'] = $long;
        $enderecos->save($row);

        $aluno['latitude'] = $lat;
        $aluno['longitude'] = $long;
        $alunos->save($aluno);
        return;
    }

    private function getLatitudeLongitudeByAddress($endereco, $num, $bairro, $cidade, $uf) {
        //$endereco = utf8_decode(str_replace(" ", "+", $row['endereco']));
        //$endereco = str_replace(" ", "+", $row['endereco']);
        $endereco = str_replace(" ", "+", $endereco);

        $address = $endereco . ',' . $num . ',' . $bairro . ',' . $cidade . ',' . $uf;
        $address = str_replace(" ", "+", $address);

        $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $address . ',Brasil&key=AIzaSyAlHzImDCagrOppCrUjRU2FT__KHKsUYhs');

        $output = json_decode($geocode);

        $lat = $output->results[0]->geometry->location->lat;
        $long = $output->results[0]->geometry->location->lng;

        $row = array();
        $row['latitude'] = $lat;
        $row['longitude'] = $long;

        return $row;
    }

    private function getArquivo($filename = 'arquivo') {
        $idarquivo = false;
        $arquivos = new Arquivos();
        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));
        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;
        return $idarquivo;
    }

    private function getImagem($apenas_copia = false, $nome = false) {
        $idimagem = false;
        $imagens = new Imagens();
        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($nome, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $nome));
        if ($excluir_imagem == 'excluir_' . $nome)
            $idimagem = -1;
        return $idimagem;
    }


 }
