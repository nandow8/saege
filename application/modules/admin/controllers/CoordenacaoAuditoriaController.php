<?php

/**
 * Controle da classe coordenacaoauditoria do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_CoordenacaoauditoriaController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Coordenacaoauditoria
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("coordenacaoauditoria", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Coordenação Auditoria')
		);
	}

    public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries['idescola'] = $idescola;

		$rows = new Escolasseries();
		$this->view->rows = $rows->getEscolasseries($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setturmasAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		
		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculos($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0)) $this->view->rows = null;
		
	}

	public function setperiodoAction() {
		$this->_helper->layout->disableLayout();

		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		$queries['idescola'] = $idescola;

		$rows = new Escolasconfiguracoes();
		$this->view->rows = $rows->getEscolasconfiguracoes($queries);

		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");
		$idturma = (int)$this->_request->getPost("idvinculo");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		$queries['idvinculo'] = $idturma;
		
		$rows = new Escolassalasatribuicoes();
		$this->view->rows = $rows->getSalasatribuicoes($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0) || ((int)$idturma <= 0)) $this->view->rows = null;
		
	}

	public function compararAction() {
		$this->_helper->layout->disableLayout();

		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");
		$idvinculo = (int)$this->_request->getPost("idturma");
		$idaluno = (int)$this->_request->getPost("idaluno");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		$queries['idvinculo'] = $idvinculo;
		$queries['idaluno'] = $idaluno;
		
		$_queries = array();
		$_queries['idserie'] = $idserie;
		$_queries['idescola'] = $idescola;
		$_queries['idvinculo'] = $idvinculo;
		
		$rows = new Escolassalasatribuicoes();
		$this->view->rows = array();

		$this->view->rows[] = $rows->getSalasatribuicoes($queries);
		$this->view->rows[] = $rows->getSalasatribuicoes($_queries);
		if(((int)$idescola <= 0) || ((int)$idserie <= 0) || ((int)$idvinculo <= 0)) $this->view->rows = null;

		$idmedia = 1;
		$secretariasmedias = new Secretariasmedias();
		$this->view->secretariasmedia1 = $secretariasmedias->getSecretariasmediaById($idmedia);

		$mediasnacionais = new Mediasnacionais();
		$this->view->mediasnacional1 = $mediasnacionais->getMediasnacionalById($idmedia);

		$escolasmedias = new Escolasmedias();
		$this->view->escolasmedia1 = $escolasmedias->getEscolasmediaById($idmedia);

	}
    
}