<?php

class Admin_LteperiodosuniversitariosController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null; 
    
    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
        Mn_Util::blockAccess("lteperiodos", $this->_request->getActionName());    
        
        $this->_usuario = unserialize($loginNameSpace->usuario);
        
        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }               
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir o periodo
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->_request->getPost("id");
        
        $rows = new Lteperiodosuniversitarios();
        $row = $rows->fetchRow("id=".$id);
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = Usuarios::getUsuario('id');
            $row['logdata'] = date('Y-m-d G:i:s');          
            
            $rows->save($row);
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Período do Universitário excluído com sucesso.";
            
            Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__, Usuarios::getUsuario("id"), "Período Universitário de ID ".$id." excluído com sucesso!");
            
            die("OK");
        }
        
        die("Não encontrado!"); 
    }
    
    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");
        
        if ($op=="lteperiodos") $objs = new Lteperiodosuniversitarios();
        $obj = $objs->fetchRow("excluido='nao' AND id=".$id);
        if ($obj) {

            $obj = $obj->toArray();
            $obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = Usuarios::getUsuario('id');
            $obj['logdata'] = date('Y-m-d G:i:s');
            
            $objs->save($obj);
            
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Status do período do universitário ".($obj['status'] == "Ativo") ? "bloqueado" : "ativado"." com sucesso!");
            
            die($obj['status']);
        }
        
        die("Não encontrado!");
    }  
    
    public function ordemAction() {
        $ordem = (int) $this->getRequest()->getParam('ordem', 0);
        $d = $this->getRequest()->getParam('d', 0);
        
        $rows = new Lteperiodosuniversitarios();
        $rows->swapOrdem($ordem, $d, false);

        $this->_redirect("/admin/".$this->_request->getControllerName()."/index");
        die();  
    }   
    
    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Periodo')
        );

        $ns = new Zend_Session_Namespace('logistica_periodos_universitarios');
        $periodos = new Lteperiodosuniversitarios();
        $queries = array(); 

        $this->preForm();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        // if (isset($this->view->post_var)) { 

        //     foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = $v;

        //     if ((isset($this->view->post_var['periodo'])) && ($this->view->post_var['periodo']!='')) $queries['periodo'] = $this->view->post_var['periodo'];
        //     if ((isset($this->view->post_var['cidade'])) && ($this->view->post_var['cidade']!='')) $queries['cidade'] = $this->view->post_var['cidade'];
        // }       
        
        //PAGINACAO
        $maxpp = 15;
        
        $paginaAtual = (int)$this->getRequest()->getParam('p');
        if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
        if ($paginaAtual==0) $paginaAtual = 1;
        
        $queries['total'] = true;
        $totalRegistros = $periodos->getPeriodos($queries);
        $paginaTotal = ceil($totalRegistros/$maxpp);
        $queries['total'] = false;
        if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual<0) $paginaAtual = 0;
        
        $this->view->pagina_atual = $paginaAtual+1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;           
        
        $this->view->rows = $periodos->getPeriodos($queries, $paginaAtual, $maxpp);
    }
    
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'lteperiodosuniversitarios', 'name' => 'Periodo'),
            array('url' => null,'name' => 'Visualizar Periodo')
        );

        $id = (int)$this->_request->getParam("id");
        $periodos = new Lteperiodosuniversitarios();
        $lte['periodo'] = $periodos->getPeriodosById($id);
        $lte['estudantes'] = $periodos->getEstudantes(array('idperiodo' => $id));

        $lte['total'] = 0;

        foreach ($lte['estudantes'] as $estudante) {
            $lte['total'] += $estudante['subsidio'];
        }

        if (!$lte) $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->visualizar = true;
        $this->view->post_var = $lte;

        $this->preForm();   
    }
    
    /**
     * 
     * Action de edição do período
     */ 
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'lteperiodosuniversitarios', 'name' => 'Periodo'),
            array('url' => null,'name' => 'Editar Periodo')
        );  

        $id = (int)$this->_request->getParam("id");
        $periodos = new Lteperiodosuniversitarios();
        $lte['periodo'] = $periodos->getPeriodosById($id);
        $lte['estudantes'] = $periodos->getEstudantes(array('idperiodo' => $id));

        $lte['total'] = 0;

        foreach ($lte['estudantes'] as $estudante) {
            $lte['total'] += $estudante['subsidio'];
        }

        if (!$lte) 
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->editar = true;
        $this->view->post_var = $lte;

        $this->preForm();
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($lte);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "periodo editada com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }       
        return true;        
    }

    public function setestudantesajaxAction() {
        $this->_helper->layout->disableLayout();
        
        $queries = array();
        $idperiodo = $this->getRequest()->getPost('idperiodo');
        $identidade = $this->getRequest()->getPost('identidade');

        if ($identidade == '') {
            $periodos = new Lteperiodosuniversitarios();
            $periodo['estudantes'] = $periodos->getEstudantes(array('idperiodo'=>$idperiodo));

            $periodo['total'] = 0;

            foreach ($periodo['estudantes'] as $estudante) {
                $periodo['total'] += $estudante['subsidio'];
            }

            $this->view->rows = $periodo;
        } else {
            $periodos = new Lteperiodosuniversitarios();
            $periodo['estudantes'] = $periodos->getEstudantes(array('idperiodo'=>$idperiodo, 'identidade'=>$identidade));

            $periodo['total'] = 0;

            foreach ($periodo['estudantes'] as $estudante) {
                $periodo['total'] += $estudante['subsidio'];
            }

            $this->view->rows = $periodo;
        }
    }
    
    /**
     * 
     * Action de adição de periodo 
     */
    public function adicionarAction() {

        $this->view->bread_crumb = array(
            array('url' => 'lteperiodosuniversitarios', 'name' => 'Periodo'),
            array('url' => null,'name' => 'Adicionar Periodo')
        );

        $this->preForm();

        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "periodo adicionado com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }

        return true;        
    }   
    
    /**
     * Atribui valores ao view
     * @param int $id
     */    
    private function preForm($id = 0) {
    }    
    
    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_registro = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);
        
        $id = (int)$this->getRequest()->getPost("id");
       
        $datainicio = strip_tags(trim($this->getRequest()->getPost("datainicio")));
        $datainicio = explode('/', $datainicio);
        $datainicio = $datainicio[2].'-'.$datainicio[1].'-'.$datainicio[0];
        $datafim = strip_tags(trim($this->getRequest()->getPost("datafim")));
        $datafim = explode('/', $datafim);
        $datafim = $datafim[2].'-'.$datafim[1].'-'.$datafim[0];
        $diasletivos = (int)strip_tags(trim($this->getRequest()->getPost("diasletivos")));

        $observacoes = strip_tags(trim($this->getRequest()->getPost("observacoes")));

        $fechado = strip_tags(trim($this->getRequest()->getPost("fechado")));
        $fechado = $fechado ? 'sim' : 'nao';

        $status = strip_tags(trim($this->getRequest()->getPost("status")));

        $erros = array();
        if (""==$status) array_push($erros, 'Selecione um STATUS.');

        $periodos = new Lteperiodosuniversitarios();

        if (sizeof($erros)>0) return $erros; 
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            $dados = array();           
            $dados['id'] = $id;
            $dados['datainicio'] = $datainicio;
            $dados['datafim'] = $datafim;
            $dados['diasletivos'] = $diasletivos;
            $dados['observacoes'] = $observacoes;

            $dados['status'] = $status;
            $dados['excluido'] = 'nao';
            $dados['fechado'] = $fechado;
            $dados['logusuario'] = Usuarios::getUsuario('id');
            $dados['logdata'] = date('Y-m-d G:i:s');

            if ($fechado == 'sim') {

                $dados['usuariofechamento'] = Usuarios::getUsuario('id');
                $dados['datafechamento'] = date('Y-m-d G:i:s');
                $row = $periodos->save($dados);

            } else {
               
                $row = $periodos->save($dados);
                
                if($id===0){                    
                    Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Período universitários adicionados com sucesso!");
                }else Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Período universitários de ID ".$id." atualizado com sucesso!");
                
                $universitarios = new Lteuniversitarios();

                $excluidos = $this->excluirEstudantes($dados['id']);

                $estudantes = $universitarios->getEstudantes();                

                foreach ($estudantes as $estudante) {
                    $dados2 = array(
                        'idestudante' => $estudante['id'],
                        'idperiodo' => $row->id,
                        'faltas' => 0,
                        'status' => 'Ativo',
                        'excluido' => 'nao'
                    );
                  
                    $row2 = $universitarios->save($dados2);
                }
                
            }
            
            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die();
        }

        return "";
    }

    private function excluirEstudantes($idperiodo = false) {

        if ($idperiodo) {
            $universitarios = new Lteuniversitarios();
            $estudantes = $universitarios->getEstudantesPeriodo(array('idperiodo' => $idperiodo));

            $rows = array();

            foreach ($estudantes as $key => $estudante) {               
                $estudantes[$key]['excluido'] = 'sim';
                // $estudantes[$key]['observacoes'] = '';
                $rows[] = $universitarios->save($estudantes[$key]);
            }

            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Estudante excluído com sucesso!");
            
            return $rows;

        } else {
            return false;
        }
    }
}