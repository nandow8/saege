<?php

/**
 * Controle da classe solicitacoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_SolicitacoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Solicitacao
	 */
	protected $_cadastro = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->cadastro)) $this->_redirect(Licencas::getLicenca('alias') . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockCadastroAccess("solicitacoes", $this->_request->getActionName());	
		
		$this->_cadastro = unserialize($loginNameSpace->cadastro);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}		

	}	


	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Solicitacoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_cadastro['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="solicitacoes") $objs = new Solicitacoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_cadastro['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'solicitacoes', 'name' => 'Solicitações'),
			array('url' => null,'name' => 'Visualizar Solicitação')
		);	
				
		
		$id = (int)$this->_request->getParam("id");
		$solicitacoes = new Solicitacoes();
		$solicitacao = $solicitacoes->getSolicitacaoById($id, array());
		
		if (!$solicitacao) 
			$this->_redirect(Licencas::getLicenca('alias') . '/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $solicitacao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Solicitações')
		);
		
		
		$ns = new Zend_Session_Namespace('default_solicitacoes');
		$solicitacoes = new Solicitacoes();
		$queries = array();	
		$queries['idlicenca'] = Licencas::getLicenca('id');
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect(Licencas::getLicenca('alias') . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ($this->view->post_var["idusuariocriacao"]!="") $queries["idusuariocriacao"] = $this->view->post_var["idusuariocriacao"];
    		if ($this->view->post_var["iddepartamentosecretaria"]!="") $queries["iddepartamentosecretaria"] = $this->view->post_var["iddepartamentosecretaria"];
    		if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
    		if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
    		if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
    		if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $solicitacoes->getSolicitacoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $solicitacoes->getSolicitacoes($queries, $paginaAtual, $maxpp);	
		
		
			
	}
	
	/**
	 * 
	 * Action de edição de solicitacoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'solicitacoes', 'name' => 'Solicitações'),
			array('url' => null,'name' => 'Editar Solicitação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$solicitacoes = new Solicitacoes();
		$solicitacao = $solicitacoes->getSolicitacaoById($id);
		
		if (!$solicitacao) 
			$this->_redirect(Licencas::getLicenca('alias') . '/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $solicitacao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($solicitacao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação editado com sucesso.";
			
			$this->_redirect(Licencas::getLicenca('alias') . '/' . $this->getRequest()->getControllerName());
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de solicitacoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'solicitacoes', 'name' => 'Solicitações'),
			array('url' => null,'name' => 'Adicionar Solicitação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação adicionado com sucesso.";
			
			$this->_redirect(Licencas::getLicenca('alias') . '/' . $this->getRequest()->getControllerName());
		}
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idsolicitacao
     */    
    private function preForm($idsolicitacao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_solicitacao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idprotocolo = (int)trim($this->getRequest()->getPost("idprotocolo"));
		$idusuariocriacao = (int)trim($this->getRequest()->getPost("idusuariocriacao"));
		$iddepartamentosecretaria = (int)trim($this->getRequest()->getPost("iddepartamentosecretaria"));
		$idusuarioresponsavel = (int)trim($this->getRequest()->getPost("idusuarioresponsavel"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idescolausuariocriacao = (int)trim($this->getRequest()->getPost("idescolausuariocriacao"));
		$idescolausuarioresponsavel = (int)trim($this->getRequest()->getPost("idescolausuarioresponsavel"));
		$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
		$titulo = trim($this->getRequest()->getPost("titulo"));
		$descricoes = trim($this->getRequest()->getPost("descricoes"));
		$origem = trim($this->getRequest()->getPost("origem"));
		$destinatario = trim($this->getRequest()->getPost("destinatario"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$titulo) array_push($erros, "Informe a Título.");
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$solicitacoes = new Solicitacoes();
		

		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			$dados['idlicenca'] = Licencas::getLicenca('id');
			
			$dados["idprotocolo"] = $idprotocolo;
			$dados["idusuariocriacao"] = $idusuariocriacao;
			$dados["iddepartamentosecretaria"] = $iddepartamentosecretaria;
			$dados["idusuarioresponsavel"] = $idusuarioresponsavel;
			$dados["idescola"] = $idescola;
			$dados["idescolausuariocriacao"] = $idescolausuariocriacao;
			$dados["idescolausuarioresponsavel"] = $idescolausuarioresponsavel;
			$dados["idaluno"] = $idaluno;
			$dados["titulo"] = $titulo;
			$dados["descricoes"] = $descricoes;
			$dados["origem"] = $origem;
			$dados["destinatario"] = $destinatario;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_cadastro['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $solicitacoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	
    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }    

	
	
}