<?php
 class MYPDF extends TCPDF {

	//Page header
	public function Header(){
		$image_file = "public/admin/imagens/logosantaisabel.jpg";
		$this->Image($image_file, 23, 5, 20, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', '', 5);
        $this->SetXY(21, 26);
        $this->Cell(0, 0, 'Paraíso da Grande São Paulo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
       
        // cabeçalho Endereço 
        $this->SetFont('helvetica', '', 12);
        $this->SetXY(79, 6);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Municipio de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', 'B', 10);
        $this->SetXY(75, 10);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Secretaria Municipal de Educação', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(70, 14);
        $this->SetTextColor(130, 130, 130);
		$this->Cell(0, 0, 'Tel. 4656-2440 e-mail: sec.educacao@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 8);
        $this->SetXY(70, 18);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297-Centro - CEP: 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 12);
        $this->SetXY(75, 25);
        $this->SetTextColor(0, 0, 0);
        $this->Cell(0, 0, 'Desempenho de aprendizagem', 0, false, 'L', 0, '', 0, false, 'M', 'M');


        //informações Saege
        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

       	$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();

        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,18);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: '.$data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 10);
        $this->SetXY(40,21);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        	

	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
        $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Position at 15 mm from bottom
		$this->SetXY(150,-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}
/**
 * Controle da classe Coordenacaoquadrodesempenhoaprendizagem do sistema
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 JFMX.  
 * @version     1.0
 */
class Admin_CoordenacaoquadrodesempenhoaprendizagemController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Patrimonio
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("coordenacaoquadrodesempenhoaprendizagem", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Coordenacaoquadrodesempenhoaprendizagem();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Patrimônio excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		 
		if ($op=="coordenacaoquadrodesempenhoaprendizagem") $objs = new Coordenacaoquadrodesempenhoaprendizagem();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaoquadrodesempenhoaprendizagem', 'name' => 'Coordenacaoquadrodesempenhoaprendizagem'),
			array('url' => null,'name' => 'Visualizar Coordenacaoquadrodesempenhoaprendizagem')
		);
		
		$id = (int)$this->_request->getParam("id");
		$coordenacaoquadrodesempenhoaprendizagem = new Coordenacaoquadrodesempenhoaprendizagem();
		$coordenacaoquadrodesempenhoaprendizage = $coordenacaoquadrodesempenhoaprendizagem->getCoordenacaoquadrodesempenhoaprendizagemById($id, array());
		
		if (!$coordenacaoquadrodesempenhoaprendizage) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaoquadrodesempenhoaprendizage;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
		
	/**
     * Listagem
     */
	public function indexAction() { 
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Coordenacaoquadrodesempenhoaprendizagem')
		);
		
		$ns = new Zend_Session_Namespace('default_coordenacaoquadrodesempenhoaprendizagem');
		$coordenacaoquadrodesempenhoaprendizagem = new Coordenacaoquadrodesempenhoaprendizagem();
		$queries = array(); 
		 
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v); 
			if (isset($this->view->post_var["classe"]) && $this->view->post_var["classe"]!="") $queries["classe"] = $this->view->post_var["classe"];
			if (isset($this->view->post_var["status"]) && $this->view->post_var["status"]!="") $queries["status"] = $this->view->post_var["status"];
			if (isset($this->view->post_var["qtd_matriculados"]) && $this->view->post_var["qtd_matriculados"]!="") $queries["qtd_matriculados"] = $this->view->post_var["qtd_matriculados"];
			if (isset($this->view->post_var["qtd_transf_rem"]) && $this->view->post_var["qtd_transf_rem"]!="") $queries["qtd_transf_rem"] = $this->view->post_var["qtd_transf_rem"];
			if (isset($this->view->post_var["qtd_evadidos_ou_faltosos"]) && $this->view->post_var["qtd_evadidos_ou_faltosos"]!="") $queries["qtd_evadidos_ou_faltosos"] = $this->view->post_var["qtd_evadidos_ou_faltosos"];
			if (isset($this->view->post_var["qtd_conc_satisfatorio_mat"]) && $this->view->post_var["qtd_conc_satisfatorio_mat"]!="") $queries["qtd_conc_satisfatorio_mat"] = $this->view->post_var["qtd_conc_satisfatorio_mat"];
			if (isset($this->view->post_var["qtd_conc_insatisfatorio_mat"]) && $this->view->post_var["qtd_conc_insatisfatorio_mat"]!="") $queries["qtd_conc_insatisfatorio_mat"] = $this->view->post_var["qtd_conc_insatisfatorio_mat"];
			if (isset($this->view->post_var["qtd_conc_satisfatorio_port"]) && $this->view->post_var["qtd_conc_satisfatorio_port"]!="") $queries["qtd_conc_satisfatorio_port"] = $this->view->post_var["qtd_conc_satisfatorio_port"];
			if (isset($this->view->post_var["qtd_conc_insatisfatorio_port"]) && $this->view->post_var["qtd_conc_insatisfatorio_port"]!="") $queries["qtd_conc_insatisfatorio_port"] = $this->view->post_var["qtd_conc_insatisfatorio_port"];
			if (isset($this->view->post_var["idescolaBusca"]) && $this->view->post_var["idescolaBusca"]!="") $queries["idescolaBusca"] = $this->view->post_var["idescolaBusca"];
			if (isset($this->view->post_var["ano"]) && $this->view->post_var["ano"]!="") $queries["ano"] = $this->view->post_var["ano"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}
		
		$idperfil = Usuarios::getUsuario('idperfil');
		$idescola = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idescola'];
		
		if($idperfil != 17 AND $idperfil != 30) {  // 30 superadmin que ve tudo   -  17 coordenação
			$queries['idescola'] = $idescola;
		}
			
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $coordenacaoquadrodesempenhoaprendizagem->getCoordenacaoquadrodesempenhoaprendizagem($queries); 
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $coordenacaoquadrodesempenhoaprendizagem->getCoordenacaoquadrodesempenhoaprendizagem($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de coordenacaoquadrodesempenhoaprendizagem
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaoquadrodesempenhoaprendizagem', 'name' => 'Patrimônios'),
			array('url' => null,'name' => 'Editar Coordenacaoquadrodesempenhoaprendizagem')
		);	
		

		$id = (int)$this->_request->getParam("id");
		$coordenacaoquadrodesempenhoaprendizagem = new Coordenacaoquadrodesempenhoaprendizagem();
		$coordenacaoquadrodesempenhoaprendizage = $coordenacaoquadrodesempenhoaprendizagem->getCoordenacaoquadrodesempenhoaprendizagemById($id);
		// echo '<pre>'; print_r($coordenacaoquadrodesempenhoaprendizage); die;

		if (!$coordenacaoquadrodesempenhoaprendizage) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaoquadrodesempenhoaprendizage;



		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($coordenacaoquadrodesempenhoaprendizage);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Coordenacaoquadrodesempenhoaprendizagem editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de Coordenacaoquadrodesempenhoaprendizage 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaoquadrodesempenhoaprendizagem', 'name' => 'Patrimônios'),
			array('url' => null,'name' => 'Adicionar Coordenacaoquadrodesempenhoaprendizage')
		);

		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Quadro de desempenho de aprendizagem adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }
    
    /**
     * Atribui valores ao view
     * @param int $idcoordenacaoquadrodesempenhoaprendizage
     */    
    private function preForm($idcoordenacaoquadrodesempenhoaprendizagem = 0) {
    }
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_coordenacaoquadrodesempenhoaprendizagem = false) {
 
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		 
		$id = (int)$this->getRequest()->getPost("id");
		 
		$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario")); 
		$ano = (int)trim($this->getRequest()->getPost("ano")); 
		$idperfil = (int)trim($this->getRequest()->getPost("idperfil"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$bimestre = trim($this->getRequest()->getPost("bimestre"));
		$classe = trim($this->getRequest()->getPost("classe"));
		$qtd_matriculados = (int)trim($this->getRequest()->getPost("qtd_matriculados"));
		$qtd_incluidos = (int)trim($this->getRequest()->getPost("qtd_incluidos"));
		$qtd_transf_rem = (int)trim($this->getRequest()->getPost("qtd_transf_rem"));
		$qtd_evadidos_ou_faltosos = (int)trim($this->getRequest()->getPost("qtd_evadidos_ou_faltosos"));
		$qtd_cca_port = (int)trim($this->getRequest()->getPost("qtd_cca_port"));
		$qtd_cca_mat = (int)trim($this->getRequest()->getPost("qtd_cca_mat"));
		$qtd_conc_insatisfatorio_port = (int)trim($this->getRequest()->getPost("qtd_conc_insatisfatorio_port"));
		$qtd_conc_insatisfatorio_mat = (int)trim($this->getRequest()->getPost("qtd_conc_insatisfatorio_mat"));
		$qtd_conc_satisfatorio_port = (int)trim($this->getRequest()->getPost("qtd_conc_satisfatorio_port"));
		$qtd_conc_satisfatorio_mat = (int)trim($this->getRequest()->getPost("qtd_conc_satisfatorio_mat"));
		$status = trim($this->getRequest()->getPost("status"));
		  		
		$erros = array(); 
		/*	
		if (0==$idsecretaria) array_push($erros, "Informe a Secretaria."); */
		 		
		$coordenacaoquadrodesempenhoaprendizagem = new Coordenacaoquadrodesempenhoaprendizagem();
					
		if (sizeof($erros)>0) return $erros;

		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idfuncionario"] = $idfuncionario; 
			$dados["ano"] = $ano; 
			$dados["idperfil"] = $idperfil;
			$dados["idescola"] = $idescola;
			$dados["idescolaBusca"] = $idescola;
			$dados["bimestre"] = $bimestre;
			$dados["classe"] = $classe;
			$dados["qtd_matriculados"] = (int)$qtd_matriculados;
			$dados["qtd_incluidos"] = (int)$qtd_incluidos;
			$dados["qtd_transf_rem"] = (int)$qtd_transf_rem;
			$dados["qtd_evadidos_ou_faltosos"] = (int)$qtd_evadidos_ou_faltosos;
			$dados["qtd_cca_port"] = (int)$qtd_cca_port;
			$dados["qtd_cca_mat"] = (int)$qtd_cca_mat;
			$dados["qtd_conc_insatisfatorio_port"] = (int)$qtd_conc_insatisfatorio_port;
			$dados["qtd_conc_insatisfatorio_mat"] = (int)$qtd_conc_insatisfatorio_mat;
			$dados["qtd_conc_satisfatorio_port"] = (int)$qtd_conc_satisfatorio_port;
			$dados["qtd_conc_satisfatorio_mat"] = (int)$qtd_conc_satisfatorio_mat;
			
			$dados["status"] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');
 
			$row = $coordenacaoquadrodesempenhoaprendizagem->save($dados);
			
			$db->commit(); 

		} catch (Exception $e) {
			echo $e->getMessage();
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Quadro de desempenho aprendizagem $e");
			$db->rollBack();
			die();
		}
		
		return "";
    }
    private function excluirImagem($idfoto){
    	$imagem = new Imagens();
    	try{
    		$imagem->excluir($idfoto);
    	}catch(Exception $e){
    		array_push($erros, "Falha ao excluir imagem");
    	}
    }


    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }

    public function documentopatrimonioAction(){

    	$patrimonios = new Patrimonios();
		$queries = array();

    	$id = Usuarios::getUsuario('id');
		$dados = Usuarios::getUsuarios(array('id'=>$id));
		$record = $dados[0];

		//se master ou patrimônio, ver tudo / senão filtra por perfil
		$idperfil = $record['idperfil'];
		$perfil = $record['perfil'];

		if($idperfil == 1 || UsuariosPerfis::isAllowedCrudPatrimonio($idperfil)):			
	    		$_rowPerfil = UsuariosPerfis::getPerfilByIdHelper($idperfil);
	    		$perfil = $_rowPerfil['perfil'];
    		
		elseif($idperfil == 29):
			$_data = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'));
			if($_data):
				$queries['idperfil'] = $idperfil;
				$queries['codescola'] = $_data['id'];
				$escola = (isset($_data['escola'])) ? $_data['escola'] : '0';
			endif;
		else:
			$queries['idperfil'] = $idperfil;
		endif;

		$queries['status'] = "Ativo";

		$_row = $patrimonios->getPatrimonios($queries);

		$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();
		$local = 'Santa Isabel';

		$localData = $local.' , '.$data;

		$this->view->rows = $_row;
		if(isset($escola)) $this->view->escola = $escola;
		if(isset($perfil)) $this->view->perfil = $perfil;
		$this->view->localdata = $localData;

		$this->preForm();

		$db = Zend_Registry::get('db');

		 // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Termos de aceite de patrimônios');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 9);

        // add a page        
        $pdf->AddPage();
        
        $html = $this->view->render('patrimonios/pdf/patrimoniosdocumento.phtml');
        $image_file = "public/admin/imagens/logosantaisabel.jpg";

        $pdf->writeHTML($html, true, 0, true, 0);

        $filename = 'meus_patrimonios.pdf';
        $pdf->Output($filename, 'I');
        
        die();
        return true;
    	
	}
	
	public function relatorioAction(){     
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Coordenacaoquadrodesempenhoaprendizagem')
		);
		
		$ns = new Zend_Session_Namespace('default_coordenacaoquadrodesempenhoaprendizagem');
		
		$queries = array();	
		if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
		if (isset($this->view->post_var)) { 
			if (isset($this->view->post_var["classe"]) && $this->view->post_var["classe"]!="") $queries["classe"] = $this->view->post_var["classe"];
			if (isset($this->view->post_var["status"]) && $this->view->post_var["status"]!="") $queries["status"] = $this->view->post_var["status"];
			if (isset($this->view->post_var["qtd_matriculados"]) && $this->view->post_var["qtd_matriculados"]!="") $queries["qtd_matriculados"] = $this->view->post_var["qtd_matriculados"];
			if (isset($this->view->post_var["qtd_transf_rem"]) && $this->view->post_var["qtd_transf_rem"]!="") $queries["qtd_transf_rem"] = $this->view->post_var["qtd_transf_rem"];
			if (isset($this->view->post_var["qtd_evadidos_ou_faltosos"]) && $this->view->post_var["qtd_evadidos_ou_faltosos"]!="") $queries["qtd_evadidos_ou_faltosos"] = $this->view->post_var["qtd_evadidos_ou_faltosos"];
			if (isset($this->view->post_var["qtd_conc_satisfatorio_mat"]) && $this->view->post_var["qtd_conc_satisfatorio_mat"]!="") $queries["qtd_conc_satisfatorio_mat"] = $this->view->post_var["qtd_conc_satisfatorio_mat"];
			if (isset($this->view->post_var["qtd_conc_insatisfatorio_mat"]) && $this->view->post_var["qtd_conc_insatisfatorio_mat"]!="") $queries["qtd_conc_insatisfatorio_mat"] = $this->view->post_var["qtd_conc_insatisfatorio_mat"];
			if (isset($this->view->post_var["qtd_conc_satisfatorio_port"]) && $this->view->post_var["qtd_conc_satisfatorio_port"]!="") $queries["qtd_conc_satisfatorio_port"] = $this->view->post_var["qtd_conc_satisfatorio_port"];
			if (isset($this->view->post_var["qtd_conc_insatisfatorio_port"]) && $this->view->post_var["qtd_conc_insatisfatorio_port"]!="") $queries["qtd_conc_insatisfatorio_port"] = $this->view->post_var["qtd_conc_insatisfatorio_port"];
			if (isset($this->view->post_var["idescolaBusca"]) && $this->view->post_var["idescolaBusca"]!="") $queries["idescolaBusca"] = $this->view->post_var["idescolaBusca"];
			if (isset($this->view->post_var["ano"]) && $this->view->post_var["ano"]!="") $queries["ano"] = $this->view->post_var["ano"];
			
			if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
		}	

		$idperfil = Usuarios::getUsuario('idperfil');
		$idescola = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idescola'];
		
		if($idperfil != 17 AND $idperfil != 30) {  // 30 superadmin que ve tudo   -  17 coordenação
			$queries['idescola'] = $idescola;
		}

		$this->view->idescola = $idescola;

		$coordenacaoquadrodesempenhoaprendizagem = new Coordenacaoquadrodesempenhoaprendizagem();
		$this->view->coordenacaoquadrodesempenhoaprendizagem = $coordenacaoquadrodesempenhoaprendizagem->getCoordenacaoquadrodesempenhoaprendizagem($queries); 

		$patrimonios = new Patrimonios();
			$data = $patrimonios->getCurrentData();
		$local = 'Santa Isabel';

		$localData = $local.' , '.$data;
 
		$this->view->localdata = $localData;

		$this->preForm();

		$db = Zend_Registry::get('db');

			// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$this->view->pdf = $pdf;

		$pdf->setPageOrientation('e');
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
		$pdf->SetTitle('Desempenho de aprendizagem');
		// $pdf->SetSubject('Assinatura do Funcionário');

		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		//set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		$pdf->SetFont('helvetica', '', 7);

		// add a page        
		$pdf->AddPage();
		
		$html = $this->view->render('coordenacaoquadrodesempenhoaprendizagem/pdf/relatorio.phtml');
		$image_file = "public/admin/imagens/logosantaisabel.jpg";

		$pdf->writeHTML($html, true, 0, true, 0);

		$filename = 'produtos_estoque.pdf';
		$pdf->Output($filename, 'I');
		
		die();
		return true; 
	}

	public function naorepetirbimestreAction(){
		$this->_helper->layout->disableLayout();     
		$classe = $this->getRequest()->getPost('classe');
		$bimestre = $this->getRequest()->getPost('bimestre');
		$ano = $this->getRequest()->getPost('ano');
		$idescola = $this->getRequest()->getPost('idescola');

		
		$rows = Coordenacaoquadrodesempenhoaprendizagem::getCoordenacaoquadrodesempenhoaprendizagemHelper(
					array(
						'idescola' => $idescola, 'ano' => $ano, 'bimestre' => $bimestre, 'classe' => $classe 
					)
				);
   
		echo json_encode($rows);
		die();
	}
	
}
