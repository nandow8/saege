<?php

class Admin_CalendariosprofessoresController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacoteescola
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("calendariosprofessores", 'index');	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}

	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => null,'name' => 'Calendário')
		);	
				$eventossecretarias = new Eventossecretarias();
		$queries = array();	
		$queries["tipo"] = "Interna";
		$queries["origem"] = "Professores";		
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['idusuariointerno'] = $this->_usuario['id'];
		$queries['order'] = "ORDER BY e1.datainicio DESC";

		$idescola = (int)Usuarios::getUsuario('idescola');
		if($idescola>0){
			$queries['idescola'] = $idescola;	
		}

		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			// if ($this->view->post_var["idusuariointerno"]!="") $queries["idusuariointerno"] = $this->view->post_var["idusuariointerno"];
// if ($this->view->post_var["iddepartamento"]!="") $queries["iddepartamento"] = $this->view->post_var["iddepartamento"];
if ($this->view->post_var["datainicio_i"]!="") $queries["datainicio_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_i"]));
if ($this->view->post_var["datainicio_f"]!="") $queries["datainicio_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_f"]));
if ($this->view->post_var["datafim_i"]!="") $queries["datafim_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datafim_i"]));
if ($this->view->post_var["datafim_f"]!="") $queries["datafim_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datafim_f"]));
if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $eventossecretarias->getEventossecretarias($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $eventossecretarias->getEventossecretarias($queries, $paginaAtual, $maxpp);	
    }

	/**
	 * 
	 * Action de edição de eventossecretarias
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'calendariosprofessores', 'name' => 'Eventos'),
			array('url' => null,'name' => 'Editar Evento')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$eventossecretarias = new Eventossecretarias();
		$eventosecretaria = $eventossecretarias->getEventosecretariaById($id);
		
		if (!$eventosecretaria) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $eventosecretaria;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($eventosecretaria);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Evento editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de eventossecretarias 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'calendariosprofessores', 'name' => 'Eventos'),
			array('url' => null,'name' => 'Adicionar Evento')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Evento adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function calendarioAction() {
		//$this->_helper->layout->disableLayout();
		$this->view->bread_crumb = array(
			array('url' => 'calendariosprofessores',	'name' => 'Eventos'),
			array('url' => null,'name' => 'Calendário')
		);	
				
		$this->preForm();
		$comunicacaoagenda = new Eventossecretarias();
		$queries = array();	
		$queries["origem"] = "Professores";
		$queries["tipo"] = "Interna";
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['idusuariointerno'] = $this->_usuario['id'];
		$queries['order'] = "ORDER BY e1.datainicio DESC";		
		$this->view->rows = $comunicacaoagenda->getEventossecretarias($queries, '0', '5');	
    }
    
	public function setcalendarioAction() {
		$this->_helper->layout->disableLayout();
		$this->preForm();
		
		$page = (int)$this->getRequest()->getPost("page");
		
		$this->view->page = $page;
    }
    
    public function seteventoAction() {
    	$this->_helper->layout->disableLayout();
    	
    	$id = $this->getRequest()->getParam('id');
    	$queries = array();
    	$queries["origem"] = "Professores";
		$comunicacaoagenda = new Eventossecretarias();
		$evento = $comunicacaoagenda->getEventosecretariaById($id, $queries);
		
		if (!$evento) die('Não Encontrado'); 
			
		$this->view->post_var = $evento;
    }
    
    public function seteventosdiaAction() {
    	$this->_helper->layout->disableLayout();
    	
    	$dia = $this->getRequest()->getParam('dia');
		
    	$queries = array();
		$queries['data'] = date('Y-m-d', $dia);
		$queries['order'] = "ORDER BY a1.datainicio DESC";
		$queries["origem"] = "Professores";
		$comunicacaoagenda = new Eventossecretarias();
		$rows = $comunicacaoagenda->getEventossecretarias($queries);
		
		if (!$rows) die('Não Encontrado'); 
		$this->view->dia = $dia;
		$this->view->rows = $rows;
		
    }
    /**
     * Atribui valores ao view
     * @param int $idescola
     */    
    private function preForm($idescola = 0) {
                $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
                
		$usuarios = new Usuarios();
		$this->view->usuarios = $usuarios->getUsuarios(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		$departamentos = new Departamentossecretarias();
		$this->view->departamentos = $departamentos->getDepartamentossecretarias(array('idsecretaria' => $this->view->idsecretaria));
    } 


    private function getPost($_eventosecretaria = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );


		$idescola = (int)Usuarios::getUsuario('idescola');
		if($idescola>0){
			$especificoescola = "Sim";
		}else{
			$especificoescola = trim($this->getRequest()->getPost("especificoescola"));
			$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		}


$idusuariointerno = (int)trim($this->getRequest()->getPost("idusuariointerno"));
$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
$datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
$horainicio = trim($this->getRequest()->getPost("horainicio"));
$datafim = Mn_Util::stringToTime($this->getRequest()->getPost("datafim"));
$horafim = trim($this->getRequest()->getPost("horafim"));
$hora = trim($this->getRequest()->getPost("hora"));
$titulo = trim($this->getRequest()->getPost("titulo"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$tipo = trim($this->getRequest()->getPost("tipo"));
$origem = trim($this->getRequest()->getPost("origem"));
$mostrarambos = trim($this->getRequest()->getPost("mostrarambos"));
$status = trim($this->getRequest()->getPost("status1"));
		if($id<=0) $status = "Aberto";
		
		$erros = array();
		
if (""==$datainicio) array_push($erros, "Informe a Data Início.");
if (""==$datafim) array_push($erros, "Informe a Data Fim.");
if (""==$titulo) array_push($erros, "Informe a Título.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$eventossecretarias = new Eventossecretarias();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );

$dados["especificoescola"] = $especificoescola;
$dados["idescola"] = $idescola;			
$dados["idusuariointerno"] = $this->_usuario['id'];
$dados["idusuario"] = $idusuario;
$dados["iddepartamento"] = $iddepartamento;
$dados["data"] = date("Y-m-d", $data);
$dados["datainicio"] = date("Y-m-d", $datainicio);
$dados["horainicio"] = $horainicio;
$dados["datafim"] = date("Y-m-d", $datafim);
$dados["horafim"] = $horafim;
$dados["hora"] = $hora;
$dados["titulo"] = $titulo;
$dados["descricoes"] = $descricoes;
$dados["observacoes"] = $observacoes;
$dados["mostrarambos"] = $mostrarambos;
$dados["origem"] = "Professores";
$dados["tipo"] = "Interna";
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $eventossecretarias->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
}
