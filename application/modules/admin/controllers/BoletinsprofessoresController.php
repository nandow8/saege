<?php

class Admin_BoletinsprofessoresController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacotealuno
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("boletinsprofessores", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}	
	

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Escolasalunos();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Usuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Boletim excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="boletins") $objs = new Escolasalunos();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Usuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Escolasalunos();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Escolasalunos();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/admin/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Boletins')
		);
		
		
		$ns = new Zend_Session_Namespace('admin_professores');
		
		$escolasvinculos = new Escolasvinculos();
		$queries = array();	
		
		$idescola = Usuarios::getUsuario('idescola');
		$idescola = (int)$idescola;
		if((Usuarios::getUsuario('professor')=="Sim") && ($idescola>0)){

			//ENCONTRA ATRIBUICOS DO PROFESSOR COM AS SALAS
			$ids_vinculos = Escolasvinculosprofessoresmaterias::getIdsEscolasvinculosprofessorByIdHelper(Usuarios::getUsuario('idfuncionario'), array('idescola' => $idescola));
			if($ids_vinculos != '')
			{
				//ENCONTRA AS SERIES AS QUAIS O PROFESSOR ESTA ATRELADO
				$ids_series = Escolasvinculos::getIdsSeriesByIdsVinculosHelper($ids_vinculos);
				$queries['idsseries'] = (isset($ids_series) && $ids_series != '') ? $ids_series : false;
			}
			
			$queries['idescola'] = $idescola;
		}
		
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
		if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
		    		
		if ($this->view->post_var["idserie"]!="") $queries["idserie"] = $this->view->post_var["idserie"];
		if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
		    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $escolasvinculos->getEscolasvinculos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $escolasvinculos->getEscolasvinculos($queries, $paginaAtual, $maxpp);
		
		
	}
	
	/**
	 * 
	 * Action de edição de aluno
	 */	
	public function editarAction() {
				
		$id = (int)$this->_request->getParam("id");
		$escolasvinculos = new Escolasvinculos();
		$escolavinculo = $escolasvinculos->getEscolavinculoById($id);
		
		if (!$escolavinculo) 
			$this->_redirect($this->getRequest()->getControllerName());

		$this->view->bread_crumb = array(
			array('url' => 'boletinsprofessores',	'name' => 'Boletins'),
			array('url' => null,'name' => 'Editar Boletim')
		);	

		$this->view->post_var = $escolavinculo;
		//var_dump($escolavinculo);die();
			
		$this->preForm();
		$alunos = new Escolassalasatribuicoes();
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['idvinculo'] = $id;
		
		$this->view->escolasalunos = $alunos->getSalasatribuicoes($queries);
		//var_dump($this->view->escolasalunos);die();	
		

		$this->view->materias = Controlealunosmaterias::getControlealunosmateriasHelper(array('status'=>'Ativo'));
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($aluno);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Boletim editado com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de alunos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'boletinsprofessores',	'name' => 'Boletins'),
			array('url' => null,'name' => 'Adicionar Boletim')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Boletim adicionado com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		$idescola = (int) $this->getRequest()->getPost('idescola');
    	if($idescola <= 0) die('ERRO');

		$alunos = new Escolasalunos();
    	$this->view->rows = $alunos->getEscolasalunos(array('idescola'=>$idescola, 'status'=>'Ativo', 'idsecretaria'=> Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' )));
    	
	}
    
	public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
    	
		$series = new Escolasseries();
    	$this->view->rows = $series->getEscolasseries(array('idescola'=>$idescola, 'status'=>'Ativo', 'idsecretaria'=> Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' ))); 
	}
		
	public function setperiodosAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
    	
		$periodos = new Escolasperiodos();
    	$this->view->rows = $periodos->getEscolasperiodos(array('idescola'=>$idescola, 'status'=>'Ativo', 'idsecretaria'=> Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' ))); 
	}
	
	public function setclassificacoesAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
    	
		$classificacoes = new Escolasalunosclassificacoes();
    	$this->view->rows = $classificacoes->getClassificacoes(array('idescola'=>$idescola, 'status'=>'Ativo', 'idsecretaria'=> Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' ))); 
	}
    
    /**
     * Atribui valores ao view
     * @param int $idaluno
     */    
    private function preForm($idaluno = 0) {

    	$escolasconfiguracoes = new Escolasconfiguracoes();
    	$queries = array();
    	//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	$escolas = $escolasconfiguracoes->getEscolasconfiguracoes($queries);
    	$_escolas = array();
    	if(sizeof($escolas) > 0) $_escolas = $escolas[0];
    	$this->view->configuracoesnotas = $_escolas;
    	//var_dump($this->view->configuracoesnotas); die();

		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
    	$this->view->professores = $funcionariosgeraisescolas->getFuncionariosgeraisescolas(array('status'=>'Ativo', 'professor'=>'Sim'));
			
    }    
      
    
    private function getArquivo($filename = 'arquivo') {
		$idarquivo = false;
		$arquivos = new Arquivos();
		try {
			$idarquivo = $arquivos->getArquivoFromForm($filename);
		} catch (Exception $e) {
			$idarquivo = false;
			array_push($erros,$e->getMessage());
		}
		
		$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
		if ($excluir_arquivo=='excluir') $idarquivo = -1;
		return $idarquivo;    	
    }
    
    private function getImagem($apenas_copia = false, $nome = false) {
		$idimagem = false;
		$imagens = new Imagens();
		try {
			ini_set('memory_limit', '-1');
			$idimagem = $imagens->getImagemFromForm($nome, NULL, NULL, $apenas_copia);
		} catch (Exception $e) {
			$idimagem = false;
			array_push($erros,$e->getMessage());
		}
		$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $nome));
		if ($excluir_imagem=='excluir_' . $nome) $idimagem = -1;
		return $idimagem;		    	
    }
}
