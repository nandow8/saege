<?php

/**
 * Controle da classe responsável pelas ações de autenticação
 * login e logoff do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_AuthController extends Zend_Controller_Action {
	
	/**
	 * Recebe um post via ajax do front e retorna a mensagem correspondente ao envio ou ao erro
	 */
	public function esqueciAction() {
		$this->_helper->layout->disableLayout();
		$email = strip_tags(trim($this->getRequest()->getPost("email")));
		if ($email) {
			
			$usuarios = new Usuarios();
			$usuario = $usuarios->fetchRow("email='$email' AND excluido='nao'");
			
			if (!$usuario) {
				$this->view->mensagem = "O E-mail informado não foi encontrado!";
				return false;
			}
			
			Usuarios::sendSenha($usuario["email"], $usuario["senha"]);
			
			$this->view->mensagem = "A senha de acesso foi enviada para o e-mail informado!";			
		}		
	}
	
	/**
	 * Mostra o formulário de login e recebendo a requisição POST procede a verificação de autenticidade e retorna um erro ou loga o usuário
	 */
	public function loginAction() {
		$this->_helper->layout->disableLayout();
		
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		//unset($loginNameSpace->prefeituracidade);
		//if(!isset($loginNameSpace->prefeituracidade)) $this->_redirect("/admin/auth/prefeiturascidades?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		//$this->view->prefeitura = $loginNameSpace->prefeituracidade;
		 
		if(isset($loginNameSpace->usuario)) {
			$this->_redirect("/admin");
		}
		
		$this->view->disableMenu = true;
		
		if ($this->getRequest()->isPost()) {
			if (!isset($this->view->post_var)) $this->view->post_var = $_POST;
			else $this->view->post_var = array_merge($this->view->post_var, $_POST);
			
			//$email = strip_tags(trim($this->getRequest()->getPost('email')));
			$rgf = strip_tags(trim($this->getRequest()->getPost('rgf')));
			$senha = strip_tags(trim($this->getRequest()->getPost('senha')));

			$usuarios = new Usuarios();
			//$autentica = $usuarios->autentica($email, $senha);
			$autentica = $usuarios->autenticargf($rgf, $senha);
			
			if ($autentica===true) {
				$redirectUrl = (isset($_GET['redirectUrl'])) ? $_GET['redirectUrl'] :  "/admin";
				$this->_redirect($redirectUrl);	
				die();		
			} 
			
			$this->view->erro = $autentica;
		}
	}
	
	public function prefeiturascidadesAction() {
		$this->_helper->layout->disableLayout();
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		

		//SP
		$idestado = 27;
		//Santa Isabel
		$idcidade = "Santa Isabel";

		$rows = new Prefeituras();
		$row = $rows->getPrefeituraByIdEstadoCidade($idestado, $idcidade, array('leftenderecos'=>true));
		//var_dump($row); die('2');
		if(!$row){
			$row = $rows->getPrefeituraByIdEstadoCidade($idestado, $idcidade, array('leftenderecos'=>true, 'c1nome'=>$idcidade));
		}

		if(!$row) {
			$this->_redirect("/admin/auth/prefeiturascidades?redirectUrl=/admin");			
		}else{
			$redirectUrl = (isset($_GET['redirectUrl'])) ? $_GET['redirectUrl'] :  "/admin";
				
			$redirectUrl = "/admin";
			$loginNameSpace->prefeituracidade = $row;
			$this->_redirect("/admin/auth/login?redirectUrl=" . $redirectUrl);
		}
		/*
		$estados = new Estados();
		$this->view->estados = $estados->getEstados();
		*/

		$prefeituras = new Prefeituras();
		$this->view->estados = $prefeituras->getPrefeituras(array('status'=>'Ativo', 'groupestado'=>true));
		
		if ($this->getRequest()->isPost()) {
			if (!isset($this->view->post_var)) $this->view->post_var = $_POST;
			else $this->view->post_var = array_merge($this->view->post_var, $_POST);
			
			$idestado = strip_tags(trim($this->getRequest()->getPost('idestado')));
			$idcidade = strip_tags(trim($this->getRequest()->getPost('idcidade')));
			$cidade = strip_tags(trim($this->getRequest()->getPost('cidade')));
			//var_dump($idcidade); die();
			$rows = new Prefeituras();
			$row = $rows->getPrefeituraByIdEstadoIdCidade($idestado, $idcidade);
			

			if(!$row){
				$row = $rows->getPrefeituraByIdEstadoCidade($idestado, $idcidade, array('leftenderecos'=>true));
				//var_dump($row); die('2');
				if(!$row){
					$row = $rows->getPrefeituraByIdEstadoCidade($idestado, $idcidade, array('leftenderecos'=>true, 'c1nome'=>$idcidade));
				}
				//var_dump($row); die('aaa');
			}
			if(!$row) {
				$this->_redirect("/admin/auth/prefeiturascidades?redirectUrl=/admin");			
			}else{
				$redirectUrl = (isset($_GET['redirectUrl'])) ? $_GET['redirectUrl'] :  "/admin";
					
				$redirectUrl = "/admin";
				$loginNameSpace->prefeituracidade = $row;
				$this->_redirect("/admin/auth/login?redirectUrl=" . $redirectUrl);
			}
			
		}
	}
	
	public function setcidadesAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idestado = $this->getRequest()->getPost('idestado');
    	
		/*
        $cidades= new Cidades();
        $row_cidade = $cidades->fetchAll("idestado=" . $idestado . " ");
    	$this->view->rows = $row_cidade;
    	*/
		$prefeituras = new Prefeituras();
		$this->view->rows = $prefeituras->getPrefeituras(array('status'=>'Ativo', 'idestado'=>$idestado, array('groupcidade'=>true)));
	}
	
	/**
	 * Procede o logou destruindo a variável de sessão e redireciona para a raiz
	 * do painel
	 */
	public function logoutAction() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		unset($loginNameSpace->usuario);
		unset($loginNameSpace->prefeituracidade);
		$this->_redirect("/admin");
		die();
	}	
	
	
}