<?php

/**
 * Controle da index (Admin)
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_CoordenacaoatasController extends Zend_Controller_Action {
	
		/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Ata
	 */
	protected $_usuario = null;	

	
	/**
     * Verificação de Permissao de Acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("coordenacaoatas", $this->_request->getActionName());	
		
		$this->_cadastro = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a xml_set_unparsed_entity_decl_handler(parser, handler)
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Atas();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Escolasusuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Material excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="coordenacaoatas") $objs = new Atas();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Escolasusuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Atas();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Atas();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Atas')
		);
		
		
		$ns = new Zend_Session_Namespace('default_coordenacaoatas');
		$coordenacaoatas = new Atas();
		$queries = array();	
		 $queries['iddepartamento'] = 15;		
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
			if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
			if ($this->view->post_var["pauta"]!="") $queries["pauta"] = $this->view->post_var["pauta"];
			if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
			
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
			    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $coordenacaoatas->getAtas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $coordenacaoatas->getAtas($queries, $paginaAtual, $maxpp);
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaoatas',	'name' => 'Atas'),
			array('url' => null,'name' => 'Visualizar Ata')
		);	
		//$this->_usuario['idescola']		
		$id = (int)$this->_request->getParam("id");
		$coordenacaoatas = new Atas();
		$coordenacaoata = $coordenacaoatas->getAtaById($id);
		
		if (!$coordenacaoata) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $coordenacaoata;
		$this->preForm();
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de saida
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaoatas',	'name' => 'Atas'),
			array('url' => null,'name' => 'Editar Ata')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$coordenacaoatas = new Atas();
		$coordenacaoata = $coordenacaoatas->getAtaById($id);
		//$this->_usuario['idescola']
		if (!$coordenacaoatas) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $coordenacaoata;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($coordenacaoata);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ata editada com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de saidas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaoatas',	'name' => 'Atas'),
			array('url' => null,'name' => 'Adicionar Ata')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ata adicionada com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
    
    /**
     * Atribui valores ao view
     * @param int $identrada
     */    
    private function preForm($identrada = 0) {
    	
    	// $idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');	
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$iddepartamento = 15;
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		$idperiodo = (int)trim($this->getRequest()->getPost("idperiodo"));
		$idsfuncionarios = $this->getRequest()->getPost("idsfuncionarios");
		$pauta = trim($this->getRequest()->getPost("pauta"));
		$descricoes = trim($this->getRequest()->getPost("descricoes"));
		$idarquivoata = (int)trim($this->getRequest()->getPost("idarquivoata"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$data) array_push($erros, "Informe a Data.");
		if (0==$idperiodo) array_push($erros, "Informe a Período.");
		if (""==$pauta) array_push($erros, "Informe a Pauta.");
		if (""==$descricoes) array_push($erros, "Informe a Descrições.");
		if (""==$status) array_push($erros, "Informe a Status.");
		
		$coordenacaoatas = new Atas();
		//$row = $saidas->fetchRow("excluido='nao' AND saida='$saida' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma ESCOLA com esse NOME.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {		
			$dados = array();
			$dados['id'] = $id;
			$dados["iddepartamento"] = $iddepartamento;
			$dados["data"] = date("Y-m-d", $data);
			$dados["idperiodo"] = $idperiodo;
			$idsfuncionarios = implode(',', $idsfuncionarios);
			$dados["idsfuncionarios"] = $idsfuncionarios;
			$dados["pauta"] = $pauta;
			$dados["descricoes"] = $descricoes;
			$idarquivoata = $this->getArquivo('idarquivoata');
				if ($idarquivoata!=0) $dados['idarquivoata'] = $idarquivoata;
			$dados["status"] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Escolasusuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			$row = $coordenacaoatas->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }

    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }  
}

