<?php

/**
 * Controle da classe chamados do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ChamadosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Chamado
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("chamados", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Chamados();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="chamados") $objs = new Chamados();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'chamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Visualizar Chamado')
		);
		
		$id = (int)$this->_request->getParam("id");
		$chamados = new Chamados();
		$chamado = $chamados->getChamadoById($id, array());
		
		if (!$chamado) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $chamado;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Chamados')
		);
		
		$ns = new Zend_Session_Namespace('default_chamados');
		$chamados = new Chamados();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
if ($this->view->post_var["idprioridade"]!="") $queries["idprioridade"] = $this->view->post_var["idprioridade"];
if ($this->view->post_var["idstatus"]!="") $queries["idstatus"] = $this->view->post_var["idstatus"];
if ($this->view->post_var["dataagendada_i"]!="") $queries["dataagendada_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["dataagendada_i"]));
if ($this->view->post_var["dataagendada_f"]!="") $queries["dataagendada_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["dataagendada_f"]));
if ($this->view->post_var["finalizacao"]!="") $queries["finalizacao"] = $this->view->post_var["finalizacao"];
if ($this->view->post_var["atendido"]!="") $queries["atendido"] = $this->view->post_var["atendido"];
if ($this->view->post_var["idsfuncionarios"]!="") $queries["idsfuncionarios"] = $this->view->post_var["idsfuncionarios"];
if ($this->view->post_var["idusuariocriacao"]!="") $queries["idusuariocriacao"] = $this->view->post_var["idusuariocriacao"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $chamados->getChamados($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $chamados->getChamados($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de chamados
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'chamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Editar Chamado')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$chamados = new Chamados();
		$chamado = $chamados->getChamadoById($id);
		
		if (!$chamado) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $chamado;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($chamado);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de chamados 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'chamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Adicionar Chamado')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idchamado
     */    
    private function preForm($idchamado = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_chamado = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
$destino = trim($this->getRequest()->getPost("destino"));
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$idprioridade = (int)trim($this->getRequest()->getPost("idprioridade"));
$idstatus = (int)trim($this->getRequest()->getPost("idstatus"));
$dataagendada = Mn_Util::stringToTime($this->getRequest()->getPost("dataagendada"));
$finalizacao = trim($this->getRequest()->getPost("finalizacao"));
$finalizacaodata = trim($this->getRequest()->getPost("finalizacaodata"));
$atendido = trim($this->getRequest()->getPost("atendido"));
$idtipo = (int)trim($this->getRequest()->getPost("idtipo"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$servicoesdescricoes = trim($this->getRequest()->getPost("servicoesdescricoes"));
$servicosmateriais = trim($this->getRequest()->getPost("servicosmateriais"));
$idsfuncionarios = trim($this->getRequest()->getPost("idsfuncionarios"));
$idspatrimonios = trim($this->getRequest()->getPost("idspatrimonios"));
$patrimoniosobservacoes = trim($this->getRequest()->getPost("patrimoniosobservacoes"));
$defeitoconstatado = trim($this->getRequest()->getPost("defeitoconstatado"));
$servicosexecutados = trim($this->getRequest()->getPost("servicosexecutados"));
$substituicaopecas = trim($this->getRequest()->getPost("substituicaopecas"));
$idspecas = trim($this->getRequest()->getPost("idspecas"));
$tiobservacoesresposta = trim($this->getRequest()->getPost("tiobservacoesresposta"));
$idresponsavel = (int)trim($this->getRequest()->getPost("idresponsavel"));
$status = trim($this->getRequest()->getPost("status1"));
$idusuariocriacao = (int)trim($this->getRequest()->getPost("idusuariocriacao"));
$iddepartamentocriacao = (int)trim($this->getRequest()->getPost("iddepartamentocriacao"));
		
		
		$erros = array();
		
		if (""==$sequencial) array_push($erros, "Informe a Sequancial/Ano.");
if (0==$idescola) array_push($erros, "Informe a Escola.");
if (""==$descricoes) array_push($erros, "Informe a Descrições.");
if (0==$idprioridade) array_push($erros, "Informe a Prioridadade.");
if (0==$idstatus) array_push($erros, "Informe a Status.");
if (""==$dataagendada) array_push($erros, "Informe a Data.");
if (""==$atendido) array_push($erros, "Informe a Atendido.");
if (""==$idsfuncionarios) array_push($erros, "Informe a Funcionário.");
if (0==$idusuariocriacao) array_push($erros, "Informe a Funcionário.");

		
		$chamados = new Chamados();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["sequencial"] = $sequencial;
$dados["destino"] = $destino;
$dados["iddepartamento"] = $iddepartamento;
$dados["idescola"] = $idescola;
$dados["descricoes"] = $descricoes;
$dados["idprioridade"] = $idprioridade;
$dados["idstatus"] = $idstatus;
$dados["dataagendada"] = date("Y-m-d", $dataagendada);
$dados["finalizacao"] = $finalizacao;
$dados["finalizacaodata"] = $finalizacaodata;
$dados["atendido"] = $atendido;
$dados["idtipo"] = $idtipo;
$dados["observacoes"] = $observacoes;
$dados["servicoesdescricoes"] = $servicoesdescricoes;
$dados["servicosmateriais"] = $servicosmateriais;
$dados["idsfuncionarios"] = $idsfuncionarios;
$dados["idspatrimonios"] = $idspatrimonios;
$dados["patrimoniosobservacoes"] = $patrimoniosobservacoes;
$dados["defeitoconstatado"] = $defeitoconstatado;
$dados["servicosexecutados"] = $servicosexecutados;
$dados["substituicaopecas"] = $substituicaopecas;
$dados["idspecas"] = $idspecas;
$dados["tiobservacoesresposta"] = $tiobservacoesresposta;
$dados["idresponsavel"] = $idresponsavel;
$dados["status"] = $status;
$dados["idusuariocriacao"] = $idusuariocriacao;
$dados["iddepartamentocriacao"] = $iddepartamentocriacao;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $chamados->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}