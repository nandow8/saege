<?php

// class MYPDF extends TCPDF {

//     //Page header
//     public function Header() {
//         $image_file = "public/admin/imagens/logosantaisabel.jpg";
//         $this->Image($image_file, 20, 15, 10, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

//         //Set font
//         $this->SetFont('helvetica', 'B', 20);

//         $this->SetFont('helvetica', 'B', 10);

//         //cabeçalho Endereço
//         $this->SetXY(16, 12);
//         $this->Cell(0, 0, 'Prefeitura Municipal de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 9);
//         $this->SetXY(35, 16);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Nutrição - Cálculo do IMC', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 19);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 22);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetXY(35, 25);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');


//         //informações Saege
//         $this->SetFont('helvetica', '', 8);
//         $this->SetXY(60, 15);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

//         $nutricaoimc = new Nutricoesimcs();
//         $data = $nutricaoimc->getCurrentData();

//         $this->SetFont('helvetica', '', 8);
//         $this->SetXY(60, 18);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Data: ' . $data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

//         $this->SetFont('helvetica', '', 10);
//         $this->SetXY(40, 21);
//         $this->SetFillColor(127);
//         $this->SetTextColor(127);
//         $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
//     }

//     // Page footer
//     public function Footer() {
//         // Position at 15 mm from bottom
//         $this->SetY(-15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

//         // Position at 15 mm from bottom
//         $this->SetXY(150, -15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
//     }

// }

/**
 * Controle da classe nutricoesimcs do sistema
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_NutricoesimcsController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Nutricaoimc
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("nutricoesimcs", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function graficoAction() {
        $this->_helper->layout->disableLayout();

        $id = (int) $this->_request->getParam("id");
        $nutricoesimcs = new Nutricoesimcs();
        $nutricaoimc = $nutricoesimcs->getNutricaoimcById($id);

        $datamedicao = $nutricaoimc['datamedicao'];
        $idaluno = (int) $nutricaoimc['idescolaaluno'];
        $imc = (float) $nutricaoimc['imc'];

        //buscar dados do aluno
        $aluno = Escolasalunos::getEscolaalunoByIdHelper($idaluno);
        $alunogdaei = Escolassubtiposensinosgdae::getSubtiposensinosgdaeHelper(array('idgdae' => $aluno['grau'], 'serie' => $aluno['serie']));
        $alunoserie = Escolasseries::getEscolasseriesHelper(array('id' => $aluno['idserie']));
        $alunoturma = Escolasturmas::getEscolasturmasHelper(array('id' => $aluno['idperiodo']));
        $alunoperiodo = Escolasperiodos::getEscolasperiodosHelper(array('id' => $aluno['idperiodo']));

        //data de nascimento
        $datanascimento = $aluno['nascimento'];
        $sexo = $aluno['sexo'];

        //retirar a hora
        $data = substr($datanascimento, 0, 10);

        $date = new DateTime($data);
        $hoje = date('m') . '/' . date('d') . '/' . date('Y');

        //verificar diferença
        $interval = $date->diff(new DateTime($datamedicao));

        //separar ano, mês e dias
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');

        // quando dias for 16 ou superior, aproxima o mês
        // ao próximo mês a ser completado
        if ($dias > 15):
            if ($meses == 11):
                $anos = $anos + 1;
                $meses = 0;
            else:
                $meses = $meses + 1;
            endif;
            $dias = 0;
        endif;

        if (!$nutricaoimc)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());


        $this->view->nutricao = $nutricaoimc;
        //setar valor
        $this->view->id = $id;
        $this->view->datanascimento = $data;
        $this->view->imc = $imc;
        $this->view->anos = $anos;
        $this->view->meses = $meses;
        $this->view->idaluno = $idaluno;
        $this->view->sexo = $sexo;
        $this->view->alunogdaei = $alunogdaei;
        $this->view->serie = $alunoserie;
        $this->view->turma = $alunoturma;
        $this->view->periodo = $alunoperiodo;
        $this->view->aluno = $aluno;

        $this->preForm();

        $db = Zend_Registry::get('db');

        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Termos de aceite de patrimônios');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage();

        $html = $this->view->render('nutricoesimcs/pdf/documentoimc.phtml');
        $image_file = "public/admin/imagens/logosantaisabel.jpg";

        $pdf->writeHTML($html, true, 0, true, 0);

        $filename = 'nutricao.pdf';
        $pdf->Output($filename, 'I');

        die();
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Nutricoesimcs();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Imc excluído com sucesso.";

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "nutricoesimcs")
            $objs = new Nutricoesimcs();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'nutricoesimcs', 'name' => 'Imcs'),
            array('url' => null, 'name' => 'Visualizar Imc')
        );

        $id = (int) $this->_request->getParam("id");
        $nutricoesimcs = new Nutricoesimcs();
        $nutricaoimc = $nutricoesimcs->getNutricaoimcById($id);

        $datamedicao = $nutricaoimc['datamedicao'];
        $idaluno = (int) $nutricaoimc['idescolaaluno'];
        $imc = (float) $nutricaoimc['imc'];

        //buscar dados do aluno
        $aluno = Escolasalunos::getEscolaalunoByIdHelper($idaluno);

        //data de nascimento
        $datanascimento = $aluno['nascimento'];
        $sexo = $aluno['sexo'];

        //retirar a hora
        $data = substr($datanascimento, 0, 10);

        $date = new DateTime($data);
        $hoje = date('m') . '/' . date('d') . '/' . date('Y');

        //verificar diferença
        $interval = $date->diff(new DateTime($datamedicao));

        //separar ano, mês e dias
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');

        // quando dias for 16 ou superior, aproxima o mês
        // ao próximo mês a ser completado
        if ($dias > 15):
            if ($meses == 11):
                $anos = $anos + 1;
                $meses = 0;
            else:
                $meses = $meses + 1;
            endif;
            $dias = 0;
        endif;

        if (!$nutricaoimc)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());


        $this->view->post_var = $nutricaoimc;
        //setar valor
        $this->view->id = $id;
        $this->view->datanascimento = $data;
        $this->view->imc = $imc;
        $this->view->anos = $anos;
        $this->view->meses = $meses;
        $this->view->idaluno = $idaluno;
        $this->view->sexo = $sexo;
        $this->view->visualizar = true;
        $this->preForm();
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Imcs')
        );
        $ns = new Zend_Session_Namespace('default_nutricoesimcs');
        $nutricoesimcs = new Nutricoesimcs();
        $queries = array();

        $escola = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'));

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {

            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ($escola) {

                $queries['idescola'] = $escola['id'];
                $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
            } else {
                if (isset($this->view->post_var["idescola"]) && $this->view->post_var["idescola"] != "")
                    $queries["idescola"] = $this->view->post_var["idescola"];
            }

            if ($this->view->post_var["idescolaaluno"] != "")
                $queries["idescolaaluno"] = $this->view->post_var["idescolaaluno"];
            if ($this->view->post_var["datamedicao_i"] != "")
                $queries["datamedicao_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datamedicao_i"]));
            if ($this->view->post_var["datamedicao_f"] != "")
                $queries["datamedicao_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datamedicao_f"]));

            if ($this->view->post_var["diagnostico"] != "")
                $queries["diagnostico"] = $this->view->post_var["diagnostico"];
            //if ($this->view->post_var["imc_i"]!="") $queries["imc_i"] = str_replace(',','.', $this->view->post_var["imc_i"]);
            //if ($this->view->post_var["imc_f"]!="") $queries["imc_f"] = str_replace(',','.', $this->view->post_var["imc_f"]);
            //if ($this->view->post_var["peso"]!="") $queries["peso"] = $this->view->post_var["peso"];
            //if ($this->view->post_var["altura"]!="") $queries["altura"] = $this->view->post_var["altura"];
            //if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];

            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }else {
            if ($escola) {
                $queries['idescola'] = $escola['id'];
                $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
            }
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $nutricoesimcs->getNutricoesimcs($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $nutricoesimcs->getNutricoesimcs($queries, $paginaAtual, $maxpp);
    }

    /**
     *
     * Action de edição de nutricoesimcs
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'nutricoesimcs', 'name' => 'Imcs'),
            array('url' => null, 'name' => 'Editar Imc')
        );

        $id = (int) $this->_request->getParam("id");
        $nutricoesimcs = new Nutricoesimcs();
        $nutricaoimc = $nutricoesimcs->getNutricaoimcById($id);

        $idaluno = (int) $nutricaoimc['idescolaaluno'];
        $imc = (float) $nutricaoimc['imc'];

        //buscar dados do aluno
        $aluno = Escolasalunos::getEscolaalunoByIdHelper($idaluno);

        //data de nascimento
        $datanascimento = $aluno['nascimento'];
        $sexo = $aluno['sexo'];

        //retirar a hora
        $data = substr($datanascimento, 0, 10);

        $date = new DateTime($data);
        $hoje = date('m') . '/' . date('d') . '/' . date('Y');

        //verificar diferença
        $interval = $date->diff(new DateTime($hoje));

        //separar ano, mês e dias
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');

        // quando dias for 16 ou superior, arredonda o mês
        // ao próximo mês a ser completado
        if ($dias > 15):
            if ($meses == 11):
                $anos = $anos + 1;
                $meses = 0;
            else:
                $meses = $meses + 1;
            endif;
            $dias = 0;
        endif;

        if (!$nutricaoimc)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());


        $this->view->post_var = $nutricaoimc;
        //setar valor
        $this->view->id = $id;
        $this->view->datanascimento = $data;
        $this->view->imc = $imc;
        $this->view->anos = $anos;
        $this->view->meses = $meses;
        $this->view->idaluno = $idaluno;
        $this->view->sexo = $sexo;
        $this->view->editar = true;
        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($nutricaoimc);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Imc editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     *
     * Action de adição de nutricoesimcs
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'nutricoesimcs', 'name' => 'Imcs'),
            array('url' => null, 'name' => 'Adicionar Imc')
        );
        $this->view->adicionar = true;
        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Imc adicionado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * Action usada para buscar todos os alunos por escola
     */
    public function setescolasalunosAction() {
        $this->_helper->layout->disableLayout();

        $idescola = (int) $this->_request->getPost("idescola");

        if (!$idescola): endif;

        $escolas = Escolas::getEscolaByIdHelper($idescola);

        $escolas = new Escolasalunos();
        if (!$idescola)
            $this->view->rows = $escolas->getEscolasalunos();
        else
            $this->view->rows = $escolas->getEscolasalunos(array('idescola' => $idescola));
    }

    public function getdadosaddAction() {
        $this->_helper->layout->disableLayout();

        $idaluno = $this->_request->getPost("idaluno");
        $imc = $this->_request->getPost("imc");
        $datamedicao = $this->_request->getPost("datamedicao");

        $aluno = Escolasalunos::getEscolaalunoByIdHelper($idaluno);
        $retorno = array();

        if (!isset($aluno['id'])):
            $retorno = array('status' => '0', 'mensagem' => 'Não foi possível gerar o gráfico no momento. Salve os dados e tente novamente');
            echo json_encode($retorno);
            die();
        endif;

        $datanascimento = $aluno['nascimento'];
        $sexo = $aluno['sexo'];

        //pegar data e separá-la por "/"
        $datanova = explode('/', $datamedicao);
        //converter para tipo date
        $mydate = date('Y-m-d', strtotime($datanova[2] . '-' . $datanova[1] . '-' . $datanova[0]));

        //pegar data de nascimento - retirar as horas
        $data = substr($datanascimento, 0, 10);
        $date = new DateTime($data); //garantir tipo data

        if ($mydate < $datanascimento):
            $retorno = array('status' => '0', 'mensagem' => 'A data da medição deve ser maior do que a data de nascimento');
            echo json_encode($retorno);
            die();
        endif;

        //verificar intervalo entre data de nascimento e data de medição
        $interval = $date->diff(new DateTime($mydate));

        //separar ano, mês e dias
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');

        // quando dias = 16 ou superior, arredonda o mês
        // ao próximo mês a ser completado
        if ($dias > 15):
            if ($meses == 11):
                $anos = $anos + 1;
                $meses = 0;
            else:
                $meses = $meses + 1;
            endif;
            $dias = 0;
        endif;

        $retorno = array('status' => '1', 'anos' => (int) $anos, 'meses' => (int) $meses, 'sexo' => $sexo, 'datanascimento' => $datanascimento);
        echo json_encode($retorno);
        die();
    }

    /**
     * Action usada para buscar informações do aluno
     */
    public function getdadosAction() {
        $this->_helper->layout->disableLayout();

        $idaluno = (int) $this->_request->getPost("idaluno");
        $aluno = Escolasalunos::getEscolaalunoByIdHelper($idaluno);
        if (!isset($aluno['id']))
            die('erro');

        //data de nascimento
        $datanascimento = $aluno['nascimento'];
        $sexo = $aluno['sexo'];

        $this->view->post_var = $aluno;
    }

    public function setgraficoAction() {
        $this->_helper->layout->disableLayout();

        $id = (int) $this->_request->getParam("id");
        if ($id == "")
            return false;

        //buscar dados do IMC do aluno
        $nutricaoimc = Nutricoesimcs::getNutricaoimcByIdHelper($id);
        $idaluno = (int) $nutricaoimc['idescolaaluno'];
        $imc = (float) $nutricaoimc['imc'];

        //buscar dados do aluno
        $aluno = Escolasalunos::getEscolaalunoByIdHelper($idaluno);

        //data de nascimento
        $datanascimento = $aluno['nascimento'];
        $sexo = $aluno['sexo'];

        //retirar a hora
        $data = substr($datanascimento, 0, 10);
        $date = new DateTime($data);
        $hoje = date('m') . '/' . date('d') . '/' . date('Y');

        //verificar diferença
        $interval = $date->diff(new DateTime($hoje));

        //separar ano, mês e dias
        $anos = $interval->format('%y');
        $meses = $interval->format('%m');
        $dias = $interval->format('%d');

        // quando 16 ou superior, aproxima o mês
        // ao próximo mês a ser completado
        if ($dias > 15):
            if ($meses == 11):
                $anos = $anos + 1;
                $meses = 0;
            else:
                $meses = $meses + 1;
            endif;
            $dias = 0;
        endif;

        //setar valor
        $this->view->id = $id;
        $this->view->datanascimento = $data;
        $this->view->imc = $imc;
        $this->view->anos = $anos;
        $this->view->meses = $meses;
        $this->view->idaluno = $idaluno;
        $this->view->sexo = $sexo;

        //enviar para a View
        $this->preform();
    }

    public function calculaimcAction() {
        $this->_helper->layout->disableLayout();

        $peso = MN_Util::trataNum(trim($this->_request->getPost("peso")));
        $altura = MN_Util::trataNum(trim($this->_request->getPost("altura")));

        if ((float) $altura <= 0)
            die('0');

        $imc = $peso / ($altura * $altura);
        $imc = number_format($imc, '2', '.', '.');
        die($imc);
    }

    /**
     * Atribui valores ao view
     * @param int $idnutricaoimc
     */
    private function preForm($idnutricaoimc = 0) {
        $escolas = new Escolas();
        $this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_nutricaoimc = false) {
        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $idsecretaria = (int) Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $idescola = (int) trim($this->getRequest()->getPost("idescola"));
        $idlocal = (int) trim($this->getRequest()->getPost("idlocal"));
        $idescolaaluno = (int) trim($this->getRequest()->getPost("idescolaaluno"));
        $peso = MN_Util::trataNum(trim($this->getRequest()->getPost("peso")));
        $altura = MN_Util::trataNum(trim($this->getRequest()->getPost("altura")));
        $imc = MN_Util::trataNum(trim($this->getRequest()->getPost("imc")));
        $diagnostico = $this->getRequest()->getPost("diagnostico");
        $datamedicao = Mn_Util::stringToTime($this->getRequest()->getPost("datamedicao"));
        $status = trim($this->getRequest()->getPost("status1"));
        $idusuario = trim($this->getRequest()->getPost("idusuario"));

        $erros = array();

        if (0 == $idescola)
            array_push($erros, "Informe a Escola.");
        if (0 == $idescolaaluno)
            array_push($erros, "Informe a Alunos.");
        if ("" == $datamedicao)
            array_push($erros, "Informe a Data.");
        if ("" == $status)
            array_push($erros, "Informe a Status.");


        $nutricoesimcs = new Nutricoesimcs();



        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;

            $dados["idsecretaria"] = $idsecretaria;
            $dados["idescola"] = $idescola;
            $dados["idlocal"] = $idlocal;
            $dados["idescolaaluno"] = $idescolaaluno;
            $dados["peso"] = $peso;
            $dados["altura"] = $altura;
            $dados["imc"] = $imc;
            $dados["diagnostico"] = $diagnostico;
            $dados["datamedicao"] = date("Y-m-d", $datamedicao);
            $dados["status"] = $status;
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            ;
            $dados['logdata'] = date('Y-m-d G:i:s');
            $dados["idusuario"] = $idusuario;

            $row = $nutricoesimcs->save($dados);

            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();

            $db->rollBack();
            die();
        }

        return "";
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

}
