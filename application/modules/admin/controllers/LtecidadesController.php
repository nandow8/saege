<?php

class Admin_LtecidadesController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null; 
    
    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
        Mn_Util::blockAccess("lteperiodos", $this->_request->getActionName());    
        
        $this->_usuario = unserialize($loginNameSpace->usuario);
        
        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }               
    }
    
    /**
     * Atribui valores ao view
     * @param int $id
     */    
    private function preForm($id = 0) {
    } 

    /**
     * 
     * Action para ser consultada via ajax e excluir o subsídio/cidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->_request->getPost("id");
        
        $rows = new Ltecidades();
        $row = $rows->fetchRow("id=".$id);
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = Usuarios::getUsuario('id');
            $row['logdata'] = date('Y-m-d G:i:s');          
            
            $rows->save($row);
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Subsídio/Cidade excluído com sucesso.";
            
            die("OK");
        }
        
        die("Não encontrado!"); 
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Cidades')
        );
      
        $ns = new Zend_Session_Namespace('logistica_cidades');
        $cidades = new Ltecidades();
        $queries = array(); 

        $this->preForm();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) { 
            
            foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = $v;

            if ((isset($this->view->post_var['cidades'])) && ($this->view->post_var['cidades']!='')) $queries['cidades'] = $this->view->post_var['cidades'];
            if ((isset($this->view->post_var['cidade'])) && ($this->view->post_var['cidade']!='')) $queries['cidade'] = $this->view->post_var['cidade'];
        }       
        
        //PAGINACAO
        $maxpp = 15;
        
        $paginaAtual = (int)$this->getRequest()->getParam('p');
        if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
        if ($paginaAtual==0) $paginaAtual = 1;
        
        $queries['total'] = true;
        $totalRegistros = $cidades->getcidades($queries);
        $paginaTotal = ceil($totalRegistros/$maxpp);
        $queries['total'] = false;
        if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual<0) $paginaAtual = 0;
        
        $this->view->pagina_atual = $paginaAtual+1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;           
        
        $this->view->rows = $cidades->getcidades($queries, $paginaAtual, $maxpp);
    }

    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'ltecidades', 'name' => 'Cidades'),
            array('url' => null,'name' => 'Visualizar Cidades')
        );
    
        $id = (int)$this->_request->getParam("id");
        $cidades = new Ltecidades();
        $cidades = $cidades->getCidadesById($id);

        if (!$cidades) $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->visualizar = true;
        $this->view->post_var = $cidades;
                
        $this->preForm();   
    }
    
    /**
     * 
     * Action de edição do período
     */ 
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'ltecidades', 'name' => 'Cidades'),
            array('url' => null,'name' => 'Editar Cidades')
        );  
                
        $id = (int)$this->_request->getParam("id");
        $cidades = new Ltecidades();
        $cidades = $cidades->getCidadesById($id);
    
        if (!$cidades) 
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->editar = true;
        $this->view->post_var = $cidades;
            
        $this->preForm();
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($cidades);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "cidades editada com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }       
        return true;        
    }       
    
    /**
     * 
     * Action de adição de cidades 
     */
    public function adicionarAction() {

        $this->view->bread_crumb = array(
            array('url' => 'ltecidades', 'name' => 'Cidades'),
            array('url' => null,'name' => 'Adicionar Cidades')
        );
                
        $this->preForm();

        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "cidades adicionado com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }

        return true;        
    }

    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_registro = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);
        
        $id = (int)$this->getRequest()->getPost("id");
        $cidade = strip_tags(trim($this->getRequest()->getPost("cidade")));
        $valorfretado = strip_tags(trim($this->getRequest()->getPost("valorfretado")));
        $subsidio = strip_tags(trim($this->getRequest()->getPost("subsidio")));
        $subsidio    = preg_replace("/[^0-9]/", "", $subsidio);

        $status = strip_tags(trim($this->getRequest()->getPost("status")));

        $erros = array();
        if (""==$status) array_push($erros, 'Selecione um STATUS.');
                
        $cidades = new Ltecidades();
        $hitorico = new Ltehistoricocidades();

        if (sizeof($erros)>0) return $erros; 
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
               
            $dados = array();           
            $dados['id'] = $id;
            $dados['status'] = $status;
            $dados['excluido'] = 'nao';

            $row = $cidades->save($dados);

            $dados2['idcidade'] = $row->id;
            $dados2['cidade'] = $cidade;
            $dados2['valorfretado'] = $valorfretado;
            $dados2['subsidio'] = $subsidio;
            $dados2['logusuario'] = Usuarios::getUsuario('id');
            $dados2['logdata'] = date('Y-m-d G:i:s');

            $row2 = $hitorico->save($dados2);
            
            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die();
        }

        return "";
    }

}