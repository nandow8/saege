<?php
class MYPDF extends TCPDF {

	//Page header
	public function Header(){
		$image_file = "public/admin/imagens/logosantaisabel.jpg";
		$this->Image($image_file, 18, 5, 20, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', '', 5);
        $this->SetXY(16, 26);
        $this->Cell(0, 0, 'Paraíso da Grande São Paulo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
       
        // cabeçalho Endereço 
        $this->SetFont('helvetica', '', 12);
        $this->SetXY(84, 6);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Municipio de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', 'B', 10);
        $this->SetXY(80, 10);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Secretaria Municipal de Educação', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(75, 14);
        $this->SetTextColor(130, 130, 130);
		$this->Cell(0, 0, 'Tel. 4656-2440 e-mail: sec.educacao@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 8);
        $this->SetXY(75, 18);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297-Centro - CEP: 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 12);
        $this->SetXY(90, 24);
        $this->SetTextColor(0, 0, 0);
        $this->Cell(0, 0, 'Atendimento Aee', 0, false, 'L', 0, '', 0, false, 'M', 'M');

		$image_file = "public/admin/imagens/aeelogo.jpeg";
		$this->Image($image_file, 175, 5, 20, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', '', 5);
        $this->SetXY(16, 26);
        $this->Cell(0, 0, 'Paraíso da Grande São Paulo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
 
        //informações Saege
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(60,20);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

       	$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();

        $this->SetFont('helvetica', '', 7);
        $this->SetXY(60,23);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: '.$data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 7);
        $this->SetXY(40,26);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        	

	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
        $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Position at 15 mm from bottom
		$this->SetXY(150,-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

/**
 * Controle da classe salasrecursosprofessores do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_SalasrecursosprofessoresController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Salasrecursosprofessor
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("salasrecursosprofessores", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Salasrecursosprofessores();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Sala de recurso excluído com sucesso.";
			
			Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "excluída com sucesso.");

			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="salasrecursosprofessores") $objs = new Salasrecursosprofessores();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'salasrecursosprofessores', 'name' => 'Criação de salas de recursos'),
			array('url' => null,'name' => 'Visualizar Sala de recurso')
		);
		
		$id = (int)$this->_request->getParam("id");
		$salasrecursosprofessores = new Salasrecursosprofessores();
		$salasrecursosprofessor = $salasrecursosprofessores->getSalasrecursosprofessorById($id, array());
		
		if (!$salasrecursosprofessor) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $salasrecursosprofessor;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Criação de salas de recursos')
		);
		
		$ns = new Zend_Session_Namespace('default_salasrecursosprofessores');
		$salasrecursosprofessores = new Salasrecursosprofessores();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["periodo"]!="") $queries["periodo"] = $this->view->post_var["periodo"];
if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $salasrecursosprofessores->getSalasrecursosprofessores($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $salasrecursosprofessores->getSalasrecursosprofessores($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de salasrecursosprofessores
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'salasrecursosprofessores', 'name' => 'Criação de salas de recursos'),
			array('url' => null,'name' => 'Editar Sala de recurso')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$salasrecursosprofessores = new Salasrecursosprofessores();
		$salasrecursosprofessor = $salasrecursosprofessores->getSalasrecursosprofessorById($id);
		
		if (!$salasrecursosprofessor) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $salasrecursosprofessor;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($salasrecursosprofessor);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Sala de recurso editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de salasrecursosprofessores 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'salasrecursosprofessores', 'name' => 'Criação de salas de recursos'),
			array('url' => null,'name' => 'Adicionar Sala de recurso')
		);	

		$funcionariosgerais = new Funcionariosgeraisescolas();

		$queries = array();
		$idperfil = (int)Usuarios::getUsuario('idperfil');
		$idescola = (int)Usuarios::getUsuario('idescola');

		$_usuarios = new Usuarios();
		

		$_funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById(Usuarios::getUsuario('idfuncionario'));

		if($idescola>0){
			$verificausuario = $_usuarios->getUsuarioById(Usuarios::getUsuario('id'));

			if((isset($verificausuario['idperfil'])) && ($verificausuario['idperfil']) && ((int)$verificausuario['idperfil'] > 0)){

				$idperfil = $verificausuario['idperfil'];	
			}
			
			
			if((!isset($_funcionariogeral['idperfil'])) || (!isset($_funcionariogeral['idsperfis'])) || ($_funcionariogeral['idperfil']=="") || ($_funcionariogeral['idsperfis']=="") || ($_funcionariogeral['idperfil']<=0) ){
				$idperfil = 0;
			}

			if((isset($_funcionariogeral['idsfuncionarios'])) && ($_funcionariogeral['idsfuncionarios']!="") ){
				$queries['idsfuncionariossel'] = $_funcionariogeral['idsfuncionarios'];
				if((!isset($queries['idsfuncionariossel'])) || ($queries['idsfuncionariossel']=="")){
					$queries['idsfuncionariossel'] = "-1";
				}
			}else{
				$queries['idsfuncionariossel'] = "-1";
			}
			
		}elseif($idperfil>0) {
			
			$queries['ids'] = $_funcionariogeral['idsfuncionarios'];
			$queries['idperfil'] = Usuarios::getUsuario('idperfil');
			if(($_funcionariogeral['idsfuncionarios']=="") || (!$_funcionariogeral['idsfuncionarios'])){
				$queries['ids'] = -1;
			}
			
		}elseif((isset($_funcionariogeral['idsfuncionarios'])) && ($_funcionariogeral['idsfuncionarios']!="") ){
			$queries['idsfuncionariossel'] = $_funcionariogeral['idsfuncionarios'];
			if((!isset($queries['idsfuncionariossel'])) || ($queries['idsfuncionariossel']=="")){
				$queries['idsfuncionariossel'] = "-1";
			}
		}else{
			$queries['idsfuncionariossel'] = "-1";
			//$queries['idescola'] = Usuarios::getUsuario('idescola');
		}
		
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$this->view->rows = $funcionariosgerais->getFuncionariosgeraisescolas($queries);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Sala de recurso adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idsalasrecursosprofessor
     */    
    private function preForm($idsalasrecursosprofessor = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_salasrecursosprofessor = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$status = trim($this->getRequest()->getPost("status1"));

		// ====== Lista (salva em outra tabela) 
			$horario = $this->getRequest()->getPost("horario");
			$horariofim = $this->getRequest()->getPost("horariofim");
			$aluno = $this->getRequest()->getPost("aluno"); 
			$professor = $this->getRequest()->getPost("professor"); 
			$diasemana = $this->getRequest()->getPost("diasemana"); 
			
		// =======
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Polo AEE.");
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$salasrecursosprofessores = new Salasrecursosprofessores();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
			// $dados["periodo"] = $periodo;
			// $dados["idfuncionario"] = $idfuncionario;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

				$segunda = array();
				$segunda['diasemana'] = $diasemana;
				$segunda['horario'] = $horario;
				$segunda['horariofim'] = $horariofim;
				$segunda['aluno'] = $aluno;
				$segunda['professor'] = $professor;
				
				$segunda['excluido'] = 'nao';
				$segunda['logusuario'] = Usuarios::getUsuario('id');
				$segunda['logdata'] = date('Y-m-d G:i:s');

				$dados['segunda'] = $segunda;
				
			$row = $salasrecursosprofessores->save($dados);
			Fjmx_Util::geraLog(date('Y-m-d'), 'salasrecursosprofessores', Usuarios::getUsuario('id'), "Salvo com sucesso");
			$db->commit();
		} catch (Exception $e) {
			Fjmx_Util::geraLog(date('Y-m-d'), 'salasrecursosprofessores', Usuarios::getUsuario('id'), " Exception: '$e->getMessage() '.", "E");
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	}
	
	public function relatoriohorarioaulasAction(){   
         
		$id = (int)$this->_request->getParam("id");  
		
		$salasrecursosprofessores = Salasrecursosprofessores::getSalasrecursosprofessoresHelper(array('id' => $id));
		$this->view->salasrecursosprofessores = $salasrecursosprofessores;

        $professoresaulas = Educacaoespecialsalasrecursosprofessoreslist::getEducacaoespecialsalasrecursosprofessoreslistHelper(array('idrecursosprofessores' => $id,  'order'=>'ORDER BY diasemana, horario'));
		$this->view->professoresaulas = $professoresaulas;
		
		$diassemana = [];
		foreach ($this->view->professoresaulas as $key => $professores) {
			$diassemana[$professores['diasemana']][$key] = $professores;
		}
		$this->view->orderdiassemana = $diassemana;
		
		// echo '<pre>'; print_r($this->view->orderdiassemana); die;

		$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();
		$local = 'Santa Isabel';

		$localData = $local.' , '.$data;
 
		$this->view->localdata = $localData;
 

		$db = Zend_Registry::get('db');

		 // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('e');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Atendimento Aee');
        // $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 7);

        // add a page        
        $pdf->AddPage();
        
        $html = $this->view->render('salasrecursosprofessores/pdf/relatoriohorarioaulas.phtml');
        $image_file = "public/admin/imagens/logosantaisabel.jpg";

        $pdf->writeHTML($html, true, 0, true, 0);

        $filename = 'produtos_estoque.pdf';
        $pdf->Output($filename, 'I');
        
        Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Solicitou a impressão do relatório.");
        
        die();
        return true;
    	
    }
    
}