<?php

/**
 * Controle da classe Imagens
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ImagensController extends Zend_Controller_Action {

	
	/**
	 * Action get
	 */
	public function getAction() {
		$id = (int)$this->_request->getParam("id");
		$w = (int)$this->_request->getParam("w");
		$h = (int)$this->_request->getParam("h");
		$indisponivel = $this->_request->getParam("i");
		
		$imagens = new Imagens(); 
		$imagem = $imagens->downloadImagem($id, $this->getRequest()->getParam('i'));
		
		$sourceFilename = $imagem['sourceFilename'];
		$imgInfo = @getimagesize($sourceFilename);
		if (!$imgInfo) {
			$imagem = $imagens->downloadImagem(0, $this->getRequest()->getParam('i'));
			$sourceFilename = $imagem['sourceFilename'];
			$imgInfo = @getimagesize($sourceFilename);			
		}

		$width = $imgInfo[0];	
		$height = $imgInfo[1];			
		
		$image = new SimpleImage();
		$image->load($sourceFilename);
		
		if (($w>0) || ($h>0)) {
			$toWidth = false;
			$toHeight = false;
			if ($w==0) $toHeight =  true;
			elseif ($h==0) $toWidth = true;
			else {
				if ($width>$height) $toWidth = true;
				else $toHeight =  true;
			}
			
			if (($toWidth) ) {
				if ($w<$width) {
					$image->resizeToWidth($w);
					$nh = $image->getHeight();
					if (($h>0) && ($nh>$h)) $image->resizeToHeight($h);
				}
			}
			if (($toHeight)) {
				if ($h<$height) {
					$image->resizeToHeight($h);	
					$nw = $image->getWidth();
					if (($w>0) && ($nw>$w)) $image->resizeToWidth($w);
				}
			}
		}
		
		$this->_helper->layout->disableLayout();
		header('content-type: image');
   		//header('FILENAME="'.$imagem['filename'].'"', TRUE);
   		//header("Content-Length: " . $imagem['filesize']);
   		header("Content-Transfer-Encoding: binary");		
		$image->output();
	}	
	
}