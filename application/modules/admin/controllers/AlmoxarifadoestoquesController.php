<?php

class Admin_AlmoxarifadoestoquesController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("almoxarifadoestoques", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     * Listagem
     */
    public function indexAction() {

        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'AlmoxarifadoEstoques')
        );

        $ns = new Zend_Session_Namespace('almoxarifadoestoques');
        $estoques = new Almoxarifadoestoques();
        $queries = array();

        $idperfil = Usuarios::getUsuario('idperfil');
        
        if ($idperfil == 29) {
            $queries['idlocal'] = $idperfil;
            $queries['idescola'] = Usuarios::getUsuario('idescola');
        } elseif ($idperfil != 29 && $idperfil != 32) {
            $queries['idlocal'] = $idperfil;
        }

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }
        
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) { 

            foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = $v;

        }

        //PAGINACAO
        $maxpp = 15;
        
        $paginaAtual = (int)$this->getRequest()->getParam('p');
        if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
        if ($paginaAtual==0) $paginaAtual = 1;
        
        $queries['total'] = true;
        $totalRegistros = $estoques->getEstoques($queries);
        $paginaTotal = ceil($totalRegistros/$maxpp);
        $queries['total'] = false;
        if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual<0) $paginaAtual = 0;
        
        $this->view->pagina_atual = $paginaAtual+1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;           
        
        $this->view->rows = $estoques->getEstoques($queries, $paginaAtual, $maxpp);
    }

    /**
     * Visualização
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadoestoques', 'name' => 'AlmoxarifadoEstoques'),
            array('url' => null,'name' => 'Visualizar Estoques')
        );

        $id = (int)$this->_request->getParam("id");
        $estoques = new Almoxarifadoestoques();
        $estoque = $estoques->getEstoquesById($id);

        if (!$estoque) $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->visualizar = true;
        $this->view->post_var = $estoque;   
    }

    /**
     * Edição
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadoestoques', 'name' => 'AlmoxarifadoEstoques'),
            array('url' => null,'name' => 'Editar AlmoxarifadoEstoque')
        );  

        $id = (int)$this->_request->getParam("id");
        $estoques = new Almoxarifadoestoques();
        $estoque = $estoques->getEstoquesById($id);

        if (!$estoque) 
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->editar = true;
        $this->view->post_var = $estoque;
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($estoque);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "estoque editada com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }
        return true;        
    }

    /**
     * Adição
     */
    public function adicionarAction() {

        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadoestoques', 'name' => 'AlmoxarifadoEstoques'),
            array('url' => null,'name' => 'Adicionar Estoque')
        );

        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Estoque adicionado com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }

        return true;        
    }

    public function setresponsaveisAction() {
        $this->_helper->layout->disableLayout();

        $idlocal = (int)$this->getRequest()->getPost('idlocal');
        $idescola = (int)$this->getRequest()->getPost('idescola');

        if ($idlocal == 0 || ($idlocal == 29 && $idescola == 0)) {

           $this->view->rows = array();

        } else {
            $queries = array('idlocal' => $idlocal);

            if ($idescola > 0) $queries['idescola'] = $idescola;

            $Usuarios = new Almoxarifadousuarios();

            $this->view->rows = $Usuarios->getUsuarios($queries);
        }
    }

    public function setescolasAction() {
        $this->_helper->layout->disableLayout();

        $escolas = new Escolas();
        $this->view->rows = $escolas->getEscolas();
    }

    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_registro = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int)$this->getRequest()->getPost("id");
        $estoque = strip_tags(trim($this->getRequest()->getPost("estoque")));
        $idlocal = (int)$this->getRequest()->getPost("idlocal");
        $idescola = (int)$this->getRequest()->getPost("idescola");
        $idtipoestoque = (int)$this->getRequest()->getPost("idtipoestoque");

        $status = strip_tags(trim($this->getRequest()->getPost("status")));

        $erros = array();
        if ($idlocal == '' || $idlocal == 0) array_push($erros, 'Selecione um LOCAL.');
        if ($idlocal == 29 && ($idescola == '' || $idescola == 0)) array_push($erros, 'Selecione uma ESCOLA.');
        if ($idtipoestoque == '' || $idtipoestoque == 0) array_push($erros, 'Selecione um TIPO DE ESTOQUE.');
        if ($status == '') array_push($erros, 'Selecione um STATUS.');

        $almoxarifadoestoque = new Almoxarifadoestoques();

        if (sizeof($erros)>0) return $erros; 
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            $dados = array();           
            $dados['id'] = $id;
            $dados['estoque'] = $estoque;
            $dados['idlocal'] = $idlocal;
            $dados['idescola'] = $idescola;
            $dados['idtipoestoque'] = $idtipoestoque;
            $dados['status'] = $status;
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = Usuarios::getUsuario('id');
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $almoxarifadoestoque->save($dados);
            
            $db->commit();
            
        } catch (Exception $e) {

            $errorInfo = $e->getPrevious()->errorInfo;

            if ($errorInfo[0] == 23000 && $errorInfo[1] == 1062) {
                array_push($erros, 'Estoque já existente.');
                return $erros;
            }

            echo $e->getMessage();
            $db->rollBack();
            die();
        }       

        return "";      
    }
}