<?php

/**
 * Controle da classe recepcaogeralvisitas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_RecepcaogeralvisitasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Recepcaogeralvisita
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("recepcaogeralvisitas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	public function novavisitaAction(){
		$this->view->bread_crumb = array(
			array('url' => 'recepcaogeralvisitas', 'name' => 'Controle de visitas'),
			array('url' => null,'name' => 'Editar Controle de visita')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$recepcaogeralvisitas = new Recepcaogeralvisitas();
		$recepcaogeralvisita = $recepcaogeralvisitas->getRecepcaogeralvisitaById($id);
		
		if (!$recepcaogeralvisita) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $recepcaogeralvisita;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($recepcaogeralvisita);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de visita editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Recepcaogeralvisitas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de visita excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="recepcaogeralvisitas") $objs = new Recepcaogeralvisitas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		

	public function getprofessoresAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");		

		$id = (int)$this->getRequest()->getPost('idlocal');
		$professores = new Funcionariosgeraisescolas();
		$rows = $professores->getFuncionariosgeraisescolas(
			array(
				'status'=>'Ativo',  
				'idlocal' => $id, 'return_sql'=>false)
		);


		echo json_encode($rows);
		die();

	}

	  public function getprofessordadosAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$id = (int)$this->getRequest()->getPost('idfuncionario');
		$professores = new Funcionariosgeraisescolas();
		$rows = $professores->getFuncionariogeralescolaByIdHelper($id);


		echo json_encode($rows);
		die();

	}

	public function getprofessorlocalAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$id = (int)$this->getRequest()->getPost('idfuncionario');
		$professores = new Funcionariosgeraisescolas();
		$rows = $professores->getFuncionariogeralescolaByIdHelper($id);
 
		$local = Locais::getLocalByIdHelper($rows['idlocal']);
		 
		echo json_encode($local);
		die();

	}

/*  T-T
	 public function getprofessorfotoAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$id = (int)$this->getRequest()->getPost('idfuncionario');
		$professores = new Funcionariosgeraisescolas();
		$rows = $professores->getFuncionariogeralescolaByIdHelper($id);


		$imagem = $this->getImagem($rows['idimagem'], 80, 80, '', Imagens::$INDISPONIVEL);
		$idimagemassinatura = $this->getImagem($rows['idimagem']);

		echo json_encode($imagem);
		die();

	}
*/	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'recepcaogeralvisitas', 'name' => 'Controle de visitas'),
			array('url' => null,'name' => 'Visualizar Controle de visita')
		);
		
		$id = (int)$this->_request->getParam("id");
		$recepcaogeralvisitas = new Recepcaogeralvisitas();
		$recepcaogeralvisita = $recepcaogeralvisitas->getRecepcaogeralvisitaById($id, array());
		
		if (!$recepcaogeralvisita) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $recepcaogeralvisita;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Controle de visitas')
		);
		
		$ns = new Zend_Session_Namespace('default_recepcaogeralvisitas');
		$recepcaogeralvisitas = new Recepcaogeralvisitas();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
		
if ($this->view->post_var["cpf"]!="") $queries["cpf"] = $this->view->post_var["cpf"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
if ($this->view->post_var["nome"]!="") $queries["nome"] = $this->view->post_var["nome"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $recepcaogeralvisitas->getRecepcaogeralvisitas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $recepcaogeralvisitas->getRecepcaogeralvisitas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de recepcaogeralvisitas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'recepcaogeralvisitas', 'name' => 'Controle de visitas'),
			array('url' => null,'name' => 'Editar Controle de visita')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$recepcaogeralvisitas = new Recepcaogeralvisitas();
		$recepcaogeralvisita = $recepcaogeralvisitas->getRecepcaogeralvisitaById($id);
		
		if (!$recepcaogeralvisita) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $recepcaogeralvisita;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($recepcaogeralvisita);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de visita editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de recepcaogeralvisitas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'recepcaogeralvisitas', 'name' => 'Controle de visitas'),
			array('url' => null,'name' => 'Adicionar Controle de visita')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de visita adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	

	public function setfuncionariosAction() {
		$this->_helper->layout->disableLayout();
		
		$iddepartamento = (int)$this->_request->getPost("iddepartamento");
		
		$funcionariosgerais = new Funcionariosgeraisescolas();
		$queries = array();	
		$queries["iddepartamento"] = $iddepartamento;	
		$queries['status'] = 'Ativo';
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$this->view->rows = $funcionariosgerais->getFuncionariosgeraisescolas($queries);	

	}

	public function getdadosAction() {
		$this->_helper->layout->disableLayout();
		
		$idfuncionario = (int)$this->_request->getPost("idfuncionario");
		
		$rows = new Funcionariosgeraisescolas();
		$row = $rows->getFuncionariogeralescolaById($idfuncionario);
		
		if ($row) {
			echo json_encode(array('status' => 'OK', 'response' => $row));die();
		}
		
		echo json_encode(array('status' => 'ERROR', 'response' => 'Funcionário não encontrado!'));
		die();
	}	
    
    /**
     * Atribui valores ao view
     * @param int $idrecepcaogeralvisita
     */    
    private function preForm($idrecepcaogeralvisita = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_recepcaogeralvisita = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$dataentrada = Mn_Util::stringToTime($this->getRequest()->getPost("dataentrada"));
$horaentrada = trim($this->getRequest()->getPost("horaentrada"));
$tipo = trim($this->getRequest()->getPost("tipo"));
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
$idlocais = (int)$this->getRequest()->getPost("idlocais"); 
$cpf = trim($this->getRequest()->getPost("cpf"));
$nome = trim($this->getRequest()->getPost("nome"));
$idfoto = (int)trim($this->getRequest()->getPost("idfoto"));
$setor = trim($this->getRequest()->getPost("setor"));
$falarcom = trim($this->getRequest()->getPost("falarcom"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$assunto = trim($this->getRequest()->getPost("assunto"));
$datasaida = Mn_Util::stringToTime($this->getRequest()->getPost("datasaida"));
$horasaida = trim($this->getRequest()->getPost("horasaida"));
$status = trim($this->getRequest()->getPost("status1"));
$input_webcam = trim($this->getRequest()->getPost("input_webcam"));		
$novavisita = trim($this->getRequest()->getPost("novavisita"));

$erros = array();
		
		if (""==$tipo) array_push($erros, "Informe a Tipo do Visitante.");
if($tipo != 'Funcionário da secretaria'){
	if (""==$cpf) array_push($erros, "Informe o CPF.");
}
if($novavisita == 'Sim')
	if (""==$assunto) array_push($erros, "Informe o Assunto.");

if (""==$status) array_push($erros, "Informe o Status.");
 

		
		$recepcaogeralvisitas = new Recepcaogeralvisitas();
		//$verificacpf = $recepcaogeralvisitas->fetchRow(" excluido = 'nao'  ");
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			 
			if($dados['id'] == 0){
				$consultacpfs = "select cpf from recepcaogeralvisitas where cpf = '$cpf'";
				$db = Zend_Registry::get('db');
				$consultacpf = $db->fetchAll($consultacpfs);
				
				if(!empty($consultacpf)){  
					array_push($erros, "CPF já cadastrado no sistema.");
					if (sizeof($erros)>0) return $erros; 
				}
			}
			 

if ($input_webcam!='') {
				$input_webcam = str_replace("data:image/png;base64,", "", $input_webcam);
				$imagens = new Imagens();
				$__imagem = $imagens->saveBase64($input_webcam);
				$download = $imagens->downloadImagem($__imagem['id']);

				$simple = new SimpleImage();
				$simple->load($download['sourceFilename']);

				$src = $simple->image;
				$height = imagesy($src);
				$width = floor(($height/4)*3);
				$pos_x = floor((imagesx($src)-$width)/2);

				$dest = imagecreatetruecolor($width, $height);
				imagecopy($dest, $src, 0, 0, $pos_x, 0, $width, $height);
				imagejpeg($dest,$download['sourceFilename'],100);


				$dados['idfoto'] = $__imagem['id'];		
			} else {
				$idimagem = $this->getImagem('foto');
				if ($idimagem!=0) $dados['idfoto'] = $idimagem;		
			}	
$dados["tipo"] = $tipo;
$dados["iddepartamento"] = $iddepartamento;
$dados["idfuncionario"] = $idfuncionario;
$dados["idlocais"] = $idlocais;	
$dados["cpf"] = $cpf;
$dados["nome"] = $nome;

 
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
	

				
			$row = $recepcaogeralvisitas->save($dados);
  

//  save na tabela Historicorecepcaogeralvisitas

	if($novavisita == 'Sim'){
			$dadoshistoricorecepcaogeralvisitas = array();			
			$historicorecepcaogeralvisitas = new Historicorecepcaogeralvisitas();
		
			$dadoshistoricorecepcaogeralvisitas["idrecepcaogeralvisitas"] =  $row['id'];
			
			
			
			$dadoshistoricorecepcaogeralvisitas["setor"] = $setor; 
			$dadoshistoricorecepcaogeralvisitas["falarcom"] = $falarcom;
			$dadoshistoricorecepcaogeralvisitas["observacoes"] = $observacoes;
			$dadoshistoricorecepcaogeralvisitas["assunto"] = $assunto;
			$dadoshistoricorecepcaogeralvisitas["dataentrada"] = date("Y-m-d", $dataentrada);
			$dadoshistoricorecepcaogeralvisitas["horaentrada"] = $horaentrada;
			$dadoshistoricorecepcaogeralvisitas["datasaida"] = date("Y-m-d");
			$dadoshistoricorecepcaogeralvisitas["horasaida"] = date("G:i:s");
			$dadoshistoricorecepcaogeralvisitas["status"] = $status; 
			$dadoshistoricorecepcaogeralvisitas['excluido'] = 'nao';
			$dadoshistoricorecepcaogeralvisitas['logusuario'] = $this->_usuario['id'];;
			$dadoshistoricorecepcaogeralvisitas['logdata'] = date('Y-m-d G:i:s');
			
			$row = $historicorecepcaogeralvisitas->save($dadoshistoricorecepcaogeralvisitas);
	}		 
		
		
		$db->commit();
	} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}