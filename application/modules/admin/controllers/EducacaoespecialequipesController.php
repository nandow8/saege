<?php

/**
 * Controle da classe educacaoespecialequipes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EducacaoespecialequipesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Educacaoespecialequipe
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("educacaoespecialequipes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Educacaoespecialequipes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Equipe Multidisciplinar excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="educacaoespecialequipes") $objs = new Educacaoespecialequipes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialequipes', 'name' => 'Equipes Multidisciplinares'),
			array('url' => null,'name' => 'Visualizar Equipe Multidisciplinar')
		);
		
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialequipes = new Educacaoespecialequipes();
		$educacaoespecialequipe = $educacaoespecialequipes->getEducacaoespecialequipeById($id, array());
		
		if (!$educacaoespecialequipe) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialequipe;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Equipes Multidisciplinares')
		);
		
		$ns = new Zend_Session_Namespace('default_educacaoespecialequipes');
		$educacaoespecialequipes = new Educacaoespecialequipes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["nome"]!="") $queries["nome"] = $this->view->post_var["nome"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $educacaoespecialequipes->getEducacaoespecialequipes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $educacaoespecialequipes->getEducacaoespecialequipes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de educacaoespecialequipes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialequipes', 'name' => 'Equipes Multidisciplinares'),
			array('url' => null,'name' => 'Editar Equipe Multidisciplinar')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialequipes = new Educacaoespecialequipes();
		$educacaoespecialequipesmembros = new Educacaoespecialequipesmembros();
		$educacaoespecialequipe = $educacaoespecialequipes->getEducacaoespecialequipeById($id);
		$membros = $educacaoespecialequipesmembros->getEducacaoespecialequipeByIdParent($id); 
		
		if (!$educacaoespecialequipe) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialequipe;
		$this->view->membros = $membros;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($educacaoespecialequipe);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Equipe Multidisciplinar editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de educacaoespecialequipes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialequipes', 'name' => 'Equipes Multidisciplinares'),
			array('url' => null,'name' => 'Adicionar Equipe Multidisciplinar')
		);	

		$funcionariosgerais = new Funcionariosgeraisescolas();

		$queries = array();
		$idperfil = (int)Usuarios::getUsuario('idperfil');
		$idescola = (int)Usuarios::getUsuario('idescola');

		$_usuarios = new Usuarios();
		

		$_funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById(Usuarios::getUsuario('idfuncionario'));

		if($idescola>0){
			$verificausuario = $_usuarios->getUsuarioById(Usuarios::getUsuario('id'));

			if((isset($verificausuario['idperfil'])) && ($verificausuario['idperfil']) && ((int)$verificausuario['idperfil'] > 0)){

				$idperfil = $verificausuario['idperfil'];	
			}
			
			
			if((!isset($_funcionariogeral['idperfil'])) || (!isset($_funcionariogeral['idsperfis'])) || ($_funcionariogeral['idperfil']=="") || ($_funcionariogeral['idsperfis']=="") || ($_funcionariogeral['idperfil']<=0) ){
				$idperfil = 0;
			}

			if((isset($_funcionariogeral['idsfuncionarios'])) && ($_funcionariogeral['idsfuncionarios']!="") ){
				$queries['idsfuncionariossel'] = $_funcionariogeral['idsfuncionarios'];
				if((!isset($queries['idsfuncionariossel'])) || ($queries['idsfuncionariossel']=="")){
					$queries['idsfuncionariossel'] = "-1";
				}
			}else{
				$queries['idsfuncionariossel'] = "-1";
			}
			
		}elseif($idperfil>0) {
			
			$queries['ids'] = $_funcionariogeral['idsfuncionarios'];
			$queries['idperfil'] = Usuarios::getUsuario('idperfil');
			if(($_funcionariogeral['idsfuncionarios']=="") || (!$_funcionariogeral['idsfuncionarios'])){
				$queries['ids'] = -1;
			}
			
		}elseif((isset($_funcionariogeral['idsfuncionarios'])) && ($_funcionariogeral['idsfuncionarios']!="") ){
			$queries['idsfuncionariossel'] = $_funcionariogeral['idsfuncionarios'];
			if((!isset($queries['idsfuncionariossel'])) || ($queries['idsfuncionariossel']=="")){
				$queries['idsfuncionariossel'] = "-1";
			}
		}else{
			$queries['idsfuncionariossel'] = "-1";
			//$queries['idescola'] = Usuarios::getUsuario('idescola');
		}
		
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$this->view->rows = $funcionariosgerais->getFuncionariosgeraisescolas($queries);
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Equipe Multidisciplinar adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $ideducacaoespecialequipe
     */    
    private function preForm($ideducacaoespecialequipe = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_educacaoespecialequipe = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$nome = trim($this->getRequest()->getPost("nome"));
		$idmembro = $this->getRequest()->getPost("idmembro");
		$idfuncionario = ($this->getRequest()->getPost("idfuncionario"));
		$idcargo = ($this->getRequest()->getPost("idcargo"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		$erros = array();
		
		if (""==$nome) array_push($erros, "Informe a Nome da Equipe.");
		// if (NULL==$idmembro) array_push($erros, "Informe algum funcionário.");
		if (""==$status) array_push($erros, "Informe a Status.");

		$educacaoespecialequipes = new Educacaoespecialequipes();
		$educacaoespecialequipesmembros = new Educacaoespecialequipesmembros();

		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["nome"] = $nome;
			$dados["status"] = $status;

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $educacaoespecialequipes->save($dados);

			//Checa membros a serem excluidos
			 $membros_old = $educacaoespecialequipesmembros->getEducacaoespecialequipeByIdParent($row['id']);
			 foreach ($membros_old as $k => $v) {
			 	if(!in_array($v['id'], $membros_old)) {
			 		$dados_exc = array(
			 			'id'=>$v['id'],
			 			'excluido'=>'sim'
			 		);

			 		$_rowexc = $educacaoespecialequipesmembros->save($dados_exc);
			 	}
			 }

			//adiciona membros
			 foreach ($idmembro as $k => $_id) {
			 	$dados_membro = array(
			 		'id'=>$_id,
			 		'idparent'=>$row['id'],
			 		'idfuncionario'=>$idfuncionario[$k],
			 		'idcargo'=>$idcargo[$k],
			 		'logdata'=>date("Y-m-d G:i:s"),
			 		'excluido'=>'nao'
			 	);

			 	$_row = $educacaoespecialequipesmembros->save($dados_membro);
			 }
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}