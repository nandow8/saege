<?php
define('MY_PATH', 'http://localhost');
/**
 * Controle da classe rhsolicitacoescontascorrentes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_RhsolicitacoescontascorrentesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Rhsolicitacaocontascorrentes
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("rhsolicitacoescontascorrentes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Rhsolicitacoescontascorrentes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="rhsolicitacoescontascorrentes") $objs = new Rhsolicitacoescontascorrentes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'rhsolicitacoescontascorrentes', 'name' => 'Solicitações'),
			array('url' => null,'name' => 'Visualizar Solicitação')
		);
		
		$id = (int)$this->_request->getParam("id");
		$rhsolicitacoescontascorrentes = new Rhsolicitacoescontascorrentes();
		$rhsolicitacaocontascorrentes = $rhsolicitacoescontascorrentes->getRhsolicitacaocontascorrentesById($id, array());
		
		if (!$rhsolicitacaocontascorrentes) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $rhsolicitacaocontascorrentes;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Solicitações')
		);
		
		$ns = new Zend_Session_Namespace('default_rhsolicitacoescontascorrentes');
		$rhsolicitacoescontascorrentes = new Rhsolicitacoescontascorrentes();
		$queries = array();	
		$queries['origem'] = "Secretaria";
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
if ($this->view->post_var["salario"]!="") $queries["salario"] = $this->view->post_var["salario"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $rhsolicitacoescontascorrentes->getRhsolicitacoescontascorrentes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $rhsolicitacoescontascorrentes->getRhsolicitacoescontascorrentes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de rhsolicitacoescontascorrentes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'rhsolicitacoescontascorrentes', 'name' => 'Solicitações'),
			array('url' => null,'name' => 'Editar Solicitação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$rhsolicitacoescontascorrentes = new Rhsolicitacoescontascorrentes();
		$rhsolicitacaocontascorrentes = $rhsolicitacoescontascorrentes->getRhsolicitacaocontascorrentesById($id);
		
		if (!$rhsolicitacaocontascorrentes) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $rhsolicitacaocontascorrentes;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($rhsolicitacaocontascorrentes);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de rhsolicitacoescontascorrentes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'rhsolicitacoescontascorrentes', 'name' => 'Solicitações'),
			array('url' => null,'name' => 'Adicionar Solicitação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function gerapdfAction() {
		$id = (int)$this->getRequest()->getParam('id');
		
		$solicitacoes = new Rhsolicitacoescontascorrentes();
		$solicitacao = $solicitacoes->getRhsolicitacaocontascorrentesById($id);
		
		if (!$solicitacao) 
			$this->_redirect('rh/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $solicitacao;
		$endereco = Enderecos::getEnderecoById((int)$this->view->post_var['idendereco']);
		//var_dump($endereco); die();
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);

			$this->view->post_var = array_merge($this->view->post_var, $endereco);
		}
		
		$this->preForm();
		//var_dump($this->view->post_var); die();
		
		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('p');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('Diploma');
		$pdf->SetSubject('Diploma');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');
	
			
		//$header= $this->view->render('diplomas/pdf/header.phtml');
		//$pdf->setHeaderHtml($header);
		//$pdf->setPrintHeader(true);
		//$pdf->SetHeaderMargin(5);
	
	/*
		$footer= $this->view->render('diplomas/pdf/footer.phtml');
		$pdf->setFooterHtml($footer);
		$pdf->setPrintFooter(true);
		$pdf->SetFooterMargin(0);
	*/
	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		
		
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('rhsolicitacoescontascorrentes/pdf/index.phtml');
		//echo $html; die();


		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'declaracao_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}

    /**
     * Atribui valores ao view
     * @param int $idrhsolicitacaocontascorrentes
     */    
    private function preForm($idrhsolicitacaocontascorrentes = 0) {
    	$prefeitura = false;
    	if((int)Usuarios::getUsuario('idprefeitura') > 0){
    		$prefeitura = Prefeituras::getPrefeituraByIdHelper(Usuarios::getUsuario('idprefeitura'));
    		
    	}
    	$this->view->prefeitura = $prefeitura;
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_rhsolicitacaocontascorrentes = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
		$idfuncionarioescola = (int)trim($this->getRequest()->getPost("idfuncionarioescola"));
		$salario = MN_Util::trataNum(trim($this->getRequest()->getPost("salario")));
		$cargo = trim($this->getRequest()->getPost("cargo"));
		$nomebanco = trim($this->getRequest()->getPost("nomebanco"));
		$agencia = trim($this->getRequest()->getPost("agencia"));
		$nomeresponsavel = trim($this->getRequest()->getPost("nomeresponsavel"));
		$cargoresponsavel = trim($this->getRequest()->getPost("cargoresponsavel"));
		$nomeempresa = trim($this->getRequest()->getPost("nomeempresa"));
		$cnpjempresa = trim($this->getRequest()->getPost("cnpjempresa"));
		$telefoneempresa = trim($this->getRequest()->getPost("telefoneempresa"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		// if (""==$cargo) array_push($erros, "Informe a Cargo.");
		if (""==$status) array_push($erros, "Informe a Status.");

		$rhsolicitacoescontascorrentes = new Rhsolicitacoescontascorrentes();

		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			$dados['origem'] = "Secretaria";
			
			$dados["idfuncionario"] = $idfuncionario;
			$dados["idfuncionarioescola"] = $idfuncionarioescola;
			$dados["salario"] = $salario;
			$dados["cargo"] = $cargo;
			$dados["nomebanco"] = $nomebanco;
			$dados["agencia"] = $agencia;
			$dados["nomeresponsavel"] = $nomeresponsavel;
			$dados["cargoresponsavel"] = $cargoresponsavel;
			$dados["nomeempresa"] = $nomeempresa;
			$dados["cnpjempresa"] = $cnpjempresa;
			$dados["telefoneempresa"] = $telefoneempresa;
			if($this->getImagem('idassinatura')>0)
			$dados['idassinatura'] = $this->getImagem('idassinatura');
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $rhsolicitacoescontascorrentes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}