<?php

/**
 * Controle da classe patrimoniosrecebimentos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PatrimoniosrecebimentosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Patrimoniosrecebimentos
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("patrimoniosrecebimentos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$patrimoniosrecebimentos = new Patrimoniosrecebimentos();
		$queries = array();	
		$ultimo = $patrimoniosrecebimentos->getUltimoPatrimoniosrecebimento($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Patrimoniosrecebimentos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mapa de acompanhamento excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="patrimoniosrecebimentos") $objs = new Patrimoniosrecebimentos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosrecebimentos', 'name' => 'Patrimônios Recebimentos'),
			array('url' => null,'name' => 'Visualizar Patrimônios Recebimentos')
		);
		
		$id = (int)$this->_request->getParam("id");
		$patrimoniosrecebimentos = new Patrimoniosrecebimentos();
		$patrimoniosrecebimentos = $patrimoniosrecebimentos->getPatrimoniosrecebimentoById($id, array());
		
		if (!$patrimoniosrecebimentos) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimoniosrecebimentos;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Patrimônios Recebimentos')
		);
		
		$ns = new Zend_Session_Namespace('default_patrimoniosrecebimentos');
		$patrimoniosrecebimentos = new Patrimoniosrecebimentos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
    		if ($this->view->post_var["idpatrimonio"]!="") $queries["idpatrimonio"] = $this->view->post_var["idpatrimonio"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $patrimoniosrecebimentos->getPatrimoniosrecebimentos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $patrimoniosrecebimentos->getPatrimoniosrecebimentos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de patrimoniosrecebimentos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosrecebimentos', 'name' => 'Patrimônios Recebimentos'),
			array('url' => null,'name' => 'Editar Patrimônios Recebimentos')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$patrimoniosrecebimentos = new Patrimoniosrecebimentos();
		$patrimoniosrecebimentos = $patrimoniosrecebimentos->getPatrimoniosrecebimentoById($id);
		
		if (!$patrimoniosrecebimentos) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimoniosrecebimentos;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($patrimoniosrecebimentos);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Recebimento editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de patrimoniosrecebimentos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosrecebimentos', 'name' => 'Patrimônios Recebimentos'),
			array('url' => null,'name' => 'Adicionar Patrimônios Recebimentos')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Recebimento adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idpatrimoniosrecebimentos
     */    
    private function preForm($idpatrimoniosrecebimentos = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_patrimoniosrecebimentos = false) {

		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		// die(var_dump($this->view->post_var));
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$datalancamento = Mn_Util::stringToTime(trim($this->getRequest()->getPost("datalancamento")));
		$horalancamento = Mn_Util::stringToTime(trim($this->getRequest()->getPost("horalancamento")));
		$idusuario = trim($this->getRequest()->getPost("idusuario"));
		$idpatrimonio = trim($this->getRequest()->getPost("idpatrimonio"));
		$iddepartamento = trim($this->getRequest()->getPost("iddepartamento"));
		$idsecretaria = trim($this->getRequest()->getPost("idsecretaria"));

		$status = trim($this->getRequest()->getPost("status1"));

		

		$erros = array();
		
		if (""==$idpatrimonio) array_push($erros, "Informe o patrimonio.");
		if(""==$status) array_push($erros, "Informe o status.");
		
		$patrimoniosrecebimentos = new Patrimoniosrecebimentos();	
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();

			$dados['id'] = $id;
			$dados["sequencial"] = $sequencial;
			$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			$dados["horalancamento"] = date("G:i:s", $horalancamento);
			$dados["idusuario"] = $idusuario;
			$dados["idpatrimonio"] = $idpatrimonio;
			$dados["iddepartamento"] = $iddepartamento;
			$dados["idsecretaria"] = $idsecretaria;

			$dados["status"] = $status;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');
		
			$row = $patrimoniosrecebimentos->save($dados);


			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}
		
		return "";    	
    }  

    public function setpatrimoniosAction() {
		$this->_helper->layout->disableLayout();
		$idpatrimonio = (int)$this->_request->getPost("idpatrimonio");
		$queries = array();
		$queries['id'] = $idpatrimonio;

		$rows = new Patrimonios();
		$this->view->post_var = $rows->getPatrimonioById($idpatrimonio);
		//if((int)$idpatrimonio <= 0) $this->view->rows = null;
	}

	private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}