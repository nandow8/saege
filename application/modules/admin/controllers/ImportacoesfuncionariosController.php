<?php

/**
 * Controle da classe importacoesfuncionarios do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ImportacoesfuncionariosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Importacoesfuncionario
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		//Mn_Util::blockAccess("importacoesfuncionarios", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Importacoesfuncionarios();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Importação excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="importacoesfuncionarios") $objs = new Importacoesfuncionarios();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'importacoesfuncionarios', 'name' => 'Importações de Arquivos'),
			array('url' => null,'name' => 'Visualizar Importação')
		);
		
		$id = (int)$this->_request->getParam("id");
		$importacoesfuncionarios = new Importacoesfuncionarios();
		$importacoesfuncionario = $importacoesfuncionarios->getImportacoesfuncionarioById($id, array());
		
		if (!$importacoesfuncionario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $importacoesfuncionario;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Importações de Arquivos')
		);
		
		$ns = new Zend_Session_Namespace('default_importacoesfuncionarios');
		$importacoesfuncionarios = new Importacoesfuncionarios();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $importacoesfuncionarios->getImportacoesfuncionarios($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $importacoesfuncionarios->getImportacoesfuncionarios($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de importacoesfuncionarios
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'importacoesfuncionarios', 'name' => 'Importações de Arquivos'),
			array('url' => null,'name' => 'Editar Importação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$importacoesfuncionarios = new Importacoesfuncionarios();
		$importacoesfuncionario = $importacoesfuncionarios->getImportacoesfuncionarioById($id);
		
		if (!$importacoesfuncionario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

		$erroplan = $this->leExcel($importacoesfuncionario);

		$this->view->post_var = $importacoesfuncionario;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($importacoesfuncionario);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Importação editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	private function isValidXls($idarquivo) {
    	PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

    	$arquivos = new Arquivos();
    	$arquivo = $arquivos->downloadArquivo($idarquivo);

		$sourceFilename = $arquivo['sourceFilename'];
		$valid = false;
		$types = array('Excel2007', 'Excel5');
		foreach ($types as $type) {

		    $reader = PHPExcel_IOFactory::createReader($type);

		    if ($reader->canRead($sourceFilename)) {

		        $valid = true;
		        break;
		    }
		}

		return $valid;
    }

    private function leExcel($pedidoimportar) {

    	if (!$this->isValidXls($pedidoimportar['idarquivo'])) {
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		return;
    	}

		$arquivos = new Arquivos();
		$arquivo = $arquivos->downloadArquivo($pedidoimportar['idarquivo']);
		$sourceFilename = $arquivo['sourceFilename'];
		$objPHPExcel = PHPExcel_IOFactory::load($sourceFilename);
		$aba = 0;
		try {			
		    $sheet = $objPHPExcel->getSheet($aba);
		} catch (Exception $e) {
			$erros = array();
			
			array_push($erros, "Aba não encontrada. Digite uma aba Válida.");
			if (sizeof($erros)>0) return $erros; 
		}

		$sheet = $objPHPExcel->getSheet($aba);
		
		$dados = $sheet->toArray();
		$fields = $dados[0];


		$ids = array();
	
		$linhas = $sheet->toArray();
		
		$departamentossecretarias = new Departamentossecretarias();
		$cargos = new Cargos();
		$departamentosescolas = new Departamentosescolas();
		$cargosescolas = new Escolascargos();
		$funcoesescolas = new Escolasfuncoes();
		$enderecos = new Enderecos();
		//VERIFICA os dados das células
		foreach ($linhas as $linha => $values) {
			if ($linha<=0) continue;

			$iddepartamento = 0;
			$idcargosecretaria = 0;

			$queries = array();
			$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
			
			$verificaexistentefuncionarioescola = Funcionariosgeraisescolas::getFuncionariogeraisescolaByRgfHelper($values[0], $queries);

			$idfuncionarioescola = (isset($verificaexistentefuncionarioescola)) ? $verificaexistentefuncionarioescola['id'] : 0;

			$verificaexistentefuncionario = Funcionariosgerais::getFuncionariosgeraisByRgfHelper($values[0], $queries);
			$idfuncionario = (isset($verificaexistentefuncionario)) ? $verificaexistentefuncionario['id'] : 0;

			$verificaexistenteescolasusuarios = Escolasusuarios::getEscolasusuariosByRgfHelper($values[0], $queries);
			$idescolausuario = (isset($verificaexistenteescolasusuarios)) ? $verificaexistenteescolasusuarios['id'] : 0;

			$verificaexistenteusuarios = Usuarios::getUsuarioByRgfHelper($values[0], $queries);
			$idusuario = (isset($verificaexistenteusuarios)) ? $verificaexistenteusuarios['id'] : 0;

			//if(isset($verificaexistentefuncionarioescola['id'])) continue;


			$isValor = false;
			foreach ($values as $k => $v) {
				if (trim($v)!='') {
					$isValor = true;
					break;
				}
			}
			if (!$isValor) continue;

			foreach ($values as $k => $v) {
				$values[$k] = trim($values[$k]);
			}

			$funcionariosgeraisescolas = new Funcionariosgeraisescolas();

			//$_dataadmissao = date("Y-m-d", strtotime($values[2]));
			$dataadmissao = MN_Util::stringToTime($values[2]);			
			$datanascimento = MN_Util::stringToTime($values[3]);

			$dataadmissao = date("Y-m-d", strtotime($values[2]));
			$datanascimento = date("Y-m-d", strtotime($values[3]));


			/*
				$endereco = $values[4]
				$municipio = $values[5]
				$cep = $values[6]

 
			*/

				$locais = new Locais();

				$_local = strtolower($values[21]);

				$local = $locais->fetchRow("excluido='nao' AND LOWER(titulo) LIKE '%$_local%'");

				$dados_locais = array();
				$dados_locais['id'] = ($local) ? $local['id'] : 0;
				$dados_locais['titulo'] = $values[21];
				$dados_locais['status'] = 'Ativo';
				$dados_locais['excluido'] = 'nao';
				$dados_locais['logusuario'] = Usuarios::getUsuario('id');
				$dados_locais['logdata'] = date('Y-m-d G:i:s');
				$local = $locais->save($dados_locais);


$escolas = new Escolas();
$verificar_escola = Escolas::getEscolaByEscolaNome($values[21]);

$idescola = 0;
if(!isset($verificar_escola['id'])){
	$dadosescolas = array();  
	$dadosescolas['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
	$dadosescolas['escola'] = $values[21];
	$dadosescolas['status'] = 'Ativo';
	$dadosescolas['excluido'] = 'nao';
	$dadosescolas['logusuario'] = Usuarios::getUsuario('id');
	$dadosescolas['logdata'] = date('Y-m-d G:i:s');
	$row_escola = $escolas->save($dadosescolas);
	$idescola = $row_escola['id'];
}else{
	$idescola = $verificar_escola['id'];
}

			$verifica_departamento_secretaria = Departamentossecretarias::getDepartamentosecretariaByNome($values[20]);
			if(!isset($verifica_departamento_secretaria['id'])){
				$dadosdepartamentossecretarias = array();
				$dadosdepartamentossecretarias["idimportacao"] = $pedidoimportar['id'];
				$dadosdepartamentossecretarias["departamento"] = $values[20];
				$dadosdepartamentossecretarias["descricoes"] = $values[20];
				$dadosdepartamentossecretarias["codigoimportacao"] = $values[19];
				$dadosdepartamentossecretarias["importacao"] = "Sim";				
				$dadosdepartamentossecretarias["status"] = 'Ativo';
				$dadosdepartamentossecretarias['excluido'] = 'nao';
				$dadosdepartamentossecretarias['logusuario'] = $this->_usuario['id'];;
				$dadosdepartamentossecretarias['logdata'] = date('Y-m-d G:i:s');

				$_row_departamentosecretaria = $departamentossecretarias->save($dadosdepartamentossecretarias);
				$iddepartamento = $_row_departamentosecretaria['id'];


			}else{
				$iddepartamento = $verifica_departamento_secretaria['id'];
			}

				//idcargosecretaria
				
			$verifica_cargo_secretaria = Cargos::getCargoByNome($values[16]);
			if(!isset($verifica_cargo_secretaria['id'])){
				$dadoscargo = array();
				$dadoscargo["idsecretaria"] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
				$dadoscargo["iddepartamento"] = $iddepartamento;
				$dadoscargo["cargo"] = $values[16];
				$dadoscargo["descricao"] = $values[16];
				$dadoscargo["status"] = 'Ativo';
				$dadoscargo['excluido'] = 'nao';
				$dadoscargo['logusuario'] = $this->_usuario['id'];;
				$dadoscargo['logdata'] = date('Y-m-d G:i:s');
				$row_cargo_secretaria = $cargos->save($dadoscargo);
				$idcargosecretaria = $row_cargo_secretaria['id'];
			}else{
				$idcargosecretaria = $verifica_cargo_secretaria['id'];
			}

			$verifica_departamento = Departamentosescolas::getDepartamentoescolaBydepartamentoHelper($values[20], array());

			$iddepartamentoescola = 0;
			$idfuncao = 0;
			$idcargo = 0;
			if(!isset($verifica_departamento['id'])){
				$dadosdepartamentos = array();  
				$dadosdepartamentos['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
				$dadosdepartamentos['idarquivoimportacao'] = $pedidoimportar['id'];
				$dadosdepartamentos['idescola'] = $idescola;
				$dadosdepartamentos['coddep'] = $values[19];
				$dadosdepartamentos['departamento'] = $values[20];
				$dadosdepartamentos['status'] = 'Ativo';
				$dadosdepartamentos['excluido'] = 'nao';
				$dadosdepartamentos['logusuario'] = Usuarios::getUsuario('id');
				$dadosdepartamentos['logdata'] = date('Y-m-d G:i:s');
				
				$row_departamento = $departamentosescolas->save($dadosdepartamentos);
				$iddepartamentoescola = $row_departamento['id'];
			}else{
				$iddepartamentoescola = $verifica_departamento['id'];
			}

			//$verifica_cargo = Departamentosescolas::getDepartamentoescolaByCoddepHelper($values[19], array('idescola'=>$idescola));
			$verifica_cargo = Escolascargos::getEscolacargoByCargoHelper($values[16], array());
			
			if(!isset($verifica_cargo['id'])){
				$dadoscargos = array();  
				$dadoscargos['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
				$dadoscargos['idarquivoimportacao'] = $pedidoimportar['id'];
				$dadoscargos['idescola'] = $idescola;
				$dadoscargos['iddepartamento'] = $iddepartamento;
				$dadoscargos['iddepartamentoescola'] = $iddepartamentoescola;
				$dadoscargos['cargo'] = $values[16];
				$dadoscargos['status'] = 'Ativo';
				$dadoscargos['excluido'] = 'nao';
				$dadoscargos['logusuario'] = Usuarios::getUsuario('id');
				$dadoscargos['logdata'] = date('Y-m-d G:i:s');
				
				$row_cargo = $cargosescolas->save($dadoscargos);
				$idcargo = $row_cargo['id'];
			}else{
				$idcargo = $verifica_cargo['id'];
			}
			
			$idendereco = 0;
			//if((!isset($verificaexistentefuncionarioescola['idendereco'])) && (isset($values[4])) && ($values[4])) {
				$quebra_endereco = explode(',', $values[4]);
				$_endereco = (isset($quebra_endereco[0])) ? $quebra_endereco[0] : '';
				$_numero = (isset($quebra_endereco[1])) ? $quebra_endereco[1] : '';

				$dados_enderecos = array();
				$dados_enderecos['cep'] = $values[6];
				$dados_enderecos['endereco'] = $_endereco;
				$dados_enderecos['numero'] = $_numero;
				$dados_enderecos['complemento'] = '';
				$dados_enderecos['bairro'] = '';
				$dados_enderecos['idestado'] = 27;
				$dados_enderecos['cidade'] = 'Santa Isabel';
				$dados_enderecos['excluido'] = 'nao';
				$_endereco = $enderecos->save($dados_enderecos);
				$idendereco = $_endereco['id'];
			//}
			//enderecos


			/*$verifica_funcao = Departamentosescolas::getDepartamentoescolaByCoddepHelper($values[19], array('idescola'=>$idescola));
			if(!isset($verifica_funcao['id'])){
				$dadosfuncoes = array();  
				$dadosfuncoes['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
				$dadosfuncoes['idarquivoimportacao'] = $pedidoimportar['id'];
				$dadosfuncoes['idcargo'] = $idcargo;
				$dadosfuncoes['idescola'] = $idescola;
				$dadosfuncoes['funcao'] = $values[16];
				$dadosfuncoes['status'] = 'Ativo';
				$dadosfuncoes['excluido'] = 'nao';
				$dadosfuncoes['logusuario'] = Usuarios::getUsuario('id');
				$dadosfuncoes['logdata'] = date('Y-m-d G:i:s');
				
				$row_funcao = $funcoesescolas->save($dadosfuncoes);
				$idfuncao = $row_funcao['id'];
			}else{
				$idfuncao = $verifica_funcao['id'];
			}*/

			$dados = array();
			$dados['id'] = $idfuncionarioescola;
			$dados['idendereco'] = $idendereco;
			$dados["idsecretaria"] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
			$dados["idlocal"] = $local['id'];
			$dados["idescola"] = $idescola;
			$dados['rgf'] = $values[0];;
			$dados["nome"] = $values[1];;	
			//$dados["dataadmissao"] = date("Y-m-d", $dataadmissao);
			//$dados["datanascimento"] = date("Y-m-d", $datanascimento);
			$dados["dataadmissao"] = $dataadmissao;
			$dados["datanascimento"] = $datanascimento;
			$dados["telefone"] = $values[7] . $values[8];	
			$dados["celular"] = $values[9] . $values[10];
			$dados["filiacaomae"] = $values[12];
			$dados["filiacaopai"] = $values[11];
			$dados["pispasep"] = $values[13];
			$dados["cpf"] = $values[14];
			$dados["rg"] = $values[15];
			$dados["funcao"] = $values[16];
			$dados["codigovinculo"] = $values[17];
			$dados["vinculoempregaticio"] = $values[18];
			$dados["codigodepartamento"] = $values[19];
			$dados["descdepartamento"] = $values[20];
			$dados["desclocal"] = $values[21];
			$dados["regime"] = $values[18];
			$dados["idimportacao"] = $pedidoimportar['id'];

			$dados["iddepartamento"] = $iddepartamento;
			$dados["idcargosecretaria"] = $idcargosecretaria;
	
			$dados["iddepartamentoescola"] = $iddepartamentoescola;
			$dados["idfuncao"] = $idfuncao;
			$dados["idcargo"] = $idcargo;
			$dados["pertenceescola"] = "Sim";

			if($values[22]=="SIM"){
				$dados["professor"] = "Sim";
				$dados['idescolas']	= $idescola;				
			}

			$dados["status"] = 'Ativo';			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

			$row = $funcionariosgeraisescolas->save($dados);
				//var_dump($dados);die();

			if($values[22]=="SIM"){
				$idperfil = 0;
				$escolasusuarios = new Escolasusuarios();
				$escolausuario = $escolasusuarios->getEscolausuarioByIdfuncionario($row['id']);
				

				$perfis = new Escolasusuariosperfis();
				$perfisprofessor = $perfis->getPerfilOrigem('SYS');

				$cargos_sys = $cargosescolas->getCargoSys('SYS');
				
				
				$idescola = $idescola;
				$idperfil = $perfisprofessor['id'];
				$idcargo = $cargos_sys['id'];
					//
				


				if((int)$idperfil > 0):
					$dados_usuarios = array();
					if(isset($escolausuario['id'])) $dados_usuarios['id'] = $escolausuario['id'];
						$dados_usuarios['idfuncionario'] = $row['id'];
						$dados_usuarios['idescola'] = $idescola;
						$dados_usuarios['idescolas'] = $idescola;
						$dados_usuarios['idcargo'] = $idcargo;
						$dados_usuarios['idperfil'] = $idperfil;
						$dados_usuarios['nomerazao'] = $values[1];
						$dados_usuarios['rgf'] = $values[0];
						if(!isset($escolausuario['id'])){
							$dados_usuarios['senha'] = $values[0];
						}else{

					}

					$dados_usuarios['status'] = 'Ativo';	
					$dados_usuarios['professor'] = 'Sim';					
					$dados_usuarios['excluido'] = 'nao';
					$dados_usuarios['logusuario'] = Usuarios::getUsuario('id');
					$dados_usuarios['logdata'] = date('Y-m-d G:i:s');
					//var_dump($dados_usuarios); die();

					$escolasusuarios->save($dados_usuarios);
				endif;				
			}




		$funcionariosgerais = new Funcionariosgerais();
		$dados_funcionario = array();
		$dados_funcionario['id'] = $idfuncionario;
		$dados_funcionario["idsecretaria"] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$dados_funcionario['rgf'] = $values[0];
		$dados_funcionario['nome'] = $values[1];
		$dados_funcionario['telefone'] = $values[7] . $values[8];
		$dados_funcionario["modulo"] = 'Secretaria';
		$dados_funcionario["funcao"] = $values[16];
		$dados_funcionario["datanascimento"] = date("Y-m-d", $datanascimento);
		$dados_funcionario["rg"] = $values[15];
		$dados_funcionario["cpf"] = $values[14];
		$dados_funcionario["pispasep"] = $values[13];
		$dados_funcionario["celular"] = $values[9] . $values[10];
		$dados_funcionario["dataadmissao"] = date("Y-m-d", $dataadmissao);
		$dados_funcionario["filiacaomae"] = $values[12];
		$dados_funcionario["filiacaopai"] = $values[11];
		$dados_funcionario["status"] = 'Ativo';
		$dados_funcionario['excluido'] = 'nao';
		$dados_funcionario['logusuario'] = $this->_usuario['id'];;
		$dados_funcionario['logdata'] = date('Y-m-d G:i:s');
			
			$_row = $funcionariosgerais->save($dados_funcionario);			
$db = Zend_Registry::get('db');
		$db->beginTransaction();
$db->commit();
		}

		return "";
    }

    private function getNameFromNumber($num) {
	    $numeric = $num % 26;
	    $letter = chr(65 + $numeric);
	    $num2 = intval($num / 26);
	    if ($num2 > 0) {
	        return $this->getNameFromNumber($num2 - 1) . $letter;
	    } else {
	        return $letter;
	    }
	}	
	/**
	 * 
	 * Action de adição de importacoesfuncionarios 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'importacoesfuncionarios', 'name' => 'Importações de Arquivos'),
			array('url' => null,'name' => 'Adicionar Importação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Importação adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idimportacoesfuncionario
     */    
    private function preForm($idimportacoesfuncionario = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_importacoesfuncionario = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$importacoesfuncionarios = new Importacoesfuncionarios();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			
$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $importacoesfuncionarios->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}