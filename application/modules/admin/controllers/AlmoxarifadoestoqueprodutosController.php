<?php

class Admin_AlmoxarifadoestoqueprodutosController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("almoxarifadoestoqueprodutos", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     * Listagem
     */
    public function indexAction() {

        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'AlmoxarifadoEstoqueProdutos')
        );

        $idperfil = Usuarios::getUsuario('idperfil');

        $queries0 = array();

        if ($idperfil == 29) {
            $queries0['idlocal'] = $idperfil;
            $queries0['idescola'] = Usuarios::getUsuario('idescola');
        } elseif ($idperfil != 29 && $idperfil != 32) {
            $queries0['idlocal'] = $idperfil;
        }

        $estoques = new Almoxarifadoestoques();

        $estoque = $estoques->getEstoques($queries0)[0];

        $ns = new Zend_Session_Namespace('almoxarifadoestoqueprodutos');
        $estoques = new Almoxarifadoestoqueprodutos();
        $queries = array('idestoque' => $estoque['id']);

        // PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }

        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);

            if ($this->view->post_var["idproduto"]!="") $queries["idproduto"] = $this->view->post_var["idproduto"];
            if ($this->view->post_var["produto"]!="") $queries["produto"] = $this->view->post_var["produto"];
            if ($this->view->post_var["codigo"]!="") $queries["codigo"] = $this->view->post_var["codigo"];
            if ($this->view->post_var["categoria"]!="") $queries["categoria"] = $this->view->post_var["categoria"];
            if (isset($this->view->post_var["zerados"])) $queries["zerados"] = true;
        }

        //PAGINACAO
        $maxpp = 15;

        $paginaAtual = (int)$this->getRequest()->getParam('p');
        if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
        if ($paginaAtual==0) $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $estoques->getEstoqueProdutos($queries);
        $paginaTotal = ceil($totalRegistros/$maxpp);
        $queries['total'] = false;
        if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual<0) $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual+1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $rs = $estoques->getEstoqueProdutos($queries, $paginaAtual, $maxpp);
        $this->view->rows = array('rows' => $rs, 'estoque' => $estoque);
    }

    public function entradaAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadoestoqueprodutos', 'name' => 'AlmoxarifadoEstoqueProdutos'),
            array('url' => null,'name' => 'Entrada Estoque')
        );

        $id = (int)$this->_request->getParam("id");

        $estoques = new Almoxarifadoestoques();

        $estoque = $estoques->getEstoquesById($id);

        $this->view->post_var = $estoque;

        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false;
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Entrada realizada com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());
        }

        return true;
    }

    public function findprodutosAction() {
        $this->_helper->layout->disableLayout();

        $array = array();

        $idproduto = (int)$this->getRequest()->getPost('idproduto');
        $codigoproduto = strip_tags(trim($this->getRequest()->getPost('codigoproduto')));
        $categoria = (int)$this->getRequest()->getPost('categoria');
        $unidademedida = (int)$this->getRequest()->getPost('unidademedida');
        $produto = strip_tags(trim($this->getRequest()->getPost('produto')));
        $descricao = strip_tags(trim($this->getRequest()->getPost('descricao')));
        $categoriaprodutos = strip_tags(trim($this->getRequest()->getPost('categoriaprodutos')));

        if ($idproduto <> 0 && !is_null($idproduto)) $array['id'] = $idproduto;
        if ($codigoproduto != '' && !is_null($codigoproduto)) $array['codigoproduto'] = $codigoproduto;
        if ($produto != '' && !is_null($produto)) $array['produto'] = $produto;
        if ($descricao != '' && !is_null($descricao)) $array['descricoes'] = $descricao;
        if ($categoria <> 0 && !is_null($categoria)) $array['idcategoria'] = $categoria;
        if ($categoriaprodutos <> 0 && !is_null($categoriaprodutos)) $array['idcategoria'] = $categoriaprodutos;

        $produtos = new Almoxarifadoprodutos();
        if (sizeof($array) == 0) {
            $this->view->rows = $produtos->getAlmoxarifadoprodutos(array('status' => 'Ativo'));
        } else {
            if ($unidademedida <> 0 && !is_null($unidademedida)) $array['idunidademedida'] = $unidademedida;
            $this->view->rows = $produtos->getAlmoxarifadoprodutos($array);
            
        }
    }

    /**
     * Visualização
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadoestoqueprodutos', 'name' => 'AlmoxarifadoEstoqueProdutos'),
            array('url' => null,'name' => 'Visualizar Estoques')
        );

        $id = (int)$this->_request->getParam("id");
        $estoques = new Almoxarifadoestoqueprodutos();
        $estoque = $estoques->getEstoqueProdutosById($id);

        if (!$estoque) $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->visualizar = true;
        $this->view->post_var = $estoque;
    }

    /**
     * Edição
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadoestoqueprodutos', 'name' => 'AlmoxarifadoEstoqueProdutos'),
            array('url' => null,'name' => 'Editar AlmoxarifadoEstoque')
        );  

        $id = (int)$this->_request->getParam("id");
        $estoques = new Almoxarifadoestoqueprodutos();
        $estoque = $estoques->getEstoqueProdutosById($id);

        if (!$estoque) 
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->post_var = $estoque;
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($estoque);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "estoque editada com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());
        }
        return true;
    }

    /**
     * Adição
     */
    public function adicionarAction() {

        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadoestoqueprodutos', 'name' => 'AlmoxarifadoEstoqueProdutos'),
            array('url' => null,'name' => 'Adicionar Estoque')
        );

        $idestoque = (int)$this->_request->getParam("idestoque");
        $this->view->post_var = array('idestoque' => $idestoque);

        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false;
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Estoque adicionado com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());
        }

        return true;
    }

    public function setresponsaveisAction() {
        $this->_helper->layout->disableLayout();

        $idlocal = (int)$this->getRequest()->getPost('idlocal');
        if ($idlocal == 0) {
            return false;
        } else {
            $funcionarios = new Funcionariosgeraisescolas();
            $this->view->rows = $funcionarios->getFuncionariosgeraisescolas(array('idlocal' => $idlocal));
        }
    }

    public function getprodutosAction() {
        $this->_helper->layout->disableLayout();

        $array = array();

        $idproduto = (int)$this->getRequest()->getPost('idproduto');
        $codigoproduto = strip_tags(trim($this->getRequest()->getPost('codigoproduto')));
        $categoria = (int)$this->getRequest()->getPost('categoria');
        $unidademedida = (int)$this->getRequest()->getPost('unidademedida');
        $produto = strip_tags(trim($this->getRequest()->getPost('produto')));
        $descricao = strip_tags(trim($this->getRequest()->getPost('descricao')));

        if ($idproduto <> 0 && !is_null($idproduto)) $array['id'] = $idproduto;
        if ($codigoproduto != '' && !is_null($codigoproduto)) $array['codigoproduto'] = $codigoproduto;
        if ($produto != '' && !is_null($produto)) $array['produto'] = $produto;
        if ($descricao != '' && !is_null($descricao)) $array['descricoes'] = $descricao;
        if ($categoria <> 0 && !is_null($categoria)) $array['idcategoria'] = $categoria;

        $produtos = new Almoxarifadoprodutos();

        if (sizeof($array) == 0) {
            $this->view->rows = array("erro" => "erro");
        } else {
            if ($unidademedida <> 0 && !is_null($unidademedida)) $array['idunidademedida'] = $unidademedida;
            $this->view->rows = $produtos->getAlmoxarifadoprodutos($array);
        }
    }

    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_registro = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST;
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);
        
        //estava recebendo ate 1000 itens
        //foi descomentado a linha max_input_vars no php.ini e aumentado
        //o limit de variaveis para 100000, para conseguir receber todos
        //os itens enviados
        //Samuel Serapião 2018/08/16
        $idestoque = $this->getRequest()->getPost('idestoque');
        $ids = $this->getRequest()->getPost('id_selecionado');
        $qtds = $this->getRequest()->getPost('qtd_selecionada');

        $itens = array_combine($ids, $qtds);

        $produtos = new Almoxarifadoestoqueprodutos();
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            foreach ($itens as $id => $qtd) {
                $dados = array();
                $dados['idestoque'] = $idestoque;
                $dados['idproduto'] = $id;
                $dados['estoque'] = $qtd;
                $dados['status'] = 'Ativo';
                $dados['excluido'] = 'nao';
                $dados['logusuario'] = Usuarios::getUsuario('id');
                $dados['logdata'] = date('Y-m-d G:i:s');

                $row = $produtos->save($dados);
            }
            
            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die();
        }       

        return "";
    }
}