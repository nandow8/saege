<?php

class Admin_LtebairrosController extends Zend_Controller_Action {
    
    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null; 
    
    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
        Mn_Util::blockAccess("ltebairros", $this->_request->getActionName());    
        
        $this->_usuario = unserialize($loginNameSpace->usuario);
        
        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }               
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir a bairro
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->_request->getPost("id");
        
        $rows = new Ltebairros();
        $row = $rows->fetchRow("id=".$id);
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = Usuarios::getUsuario('id');
            $row['logdata'] = date('Y-m-d G:i:s');          
            
            $rows->save($row);
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Bairro excluído com sucesso.";
            
            die("OK");
        }
        
        die("Não encontrado!"); 
    }       
    
    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");
        
        if ($op=="ltebairros") $objs = new Ltebairros();
        $obj = $objs->fetchRow("excluido='nao' AND id=".$id);
        if ($obj) {
            
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = Usuarios::getUsuario('id');
            $obj['logdata'] = date('Y-m-d G:i:s');
            
            $objs->save($obj);
            
            die($obj['status']);
        }
        
        die("Não encontrado!");
    }       
    
    public function changeorderxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $idfrom = (int)$this->getRequest()->getPost("from");
        $idto = (int)$this->getRequest()->getPost("to");
        $op = $this->getRequest()->getPost("op");
        
        if ($op=="change") $objs = new Ltebairros();
        $from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
        $to = $objs->fetchRow("excluido='nao' AND id=".$idto);
        
        if (($from) && ($to)) {
            $from = $from->toArray();
            $to = $to->toArray();
            $ordemFrom = $from["ordem"];
            $orderTo = $to["ordem"];

            $from['ordem'] = $orderTo;
            $to['ordem'] = $ordemFrom;

            $objs->save($from);
            $objs->save($to);
        }
        
        $this->view->message = "OK"; 
        $this->render("xml");
    }   
    
    public function ordemAction() {
        $ordem = (int) $this->getRequest()->getParam('ordem', 0);
        $d = $this->getRequest()->getParam('d', 0);
        
        $rows = new Ltebairros();
        $rows->swapOrdem($ordem, $d, false);

        $this->_redirect("/admin/".$this->_request->getControllerName()."/index");
        die();  
    }   
    
    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Bairro')
        );
      
        $ns = new Zend_Session_Namespace('logistica_bairros');
        $bairros = new Ltebairros();
        $queries = array(); 

        $this->preForm();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) { 
            
            foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = $v;

            if ((isset($this->view->post_var['bairro'])) && ($this->view->post_var['bairro']!='')) $queries['bairro'] = $this->view->post_var['bairro'];
            if ((isset($this->view->post_var['cidade'])) && ($this->view->post_var['cidade']!='')) $queries['cidade'] = $this->view->post_var['cidade'];
        }       
        
        //PAGINACAO
        $maxpp = 15;
        
        $paginaAtual = (int)$this->getRequest()->getParam('p');
        if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
        if ($paginaAtual==0) $paginaAtual = 1;
        
        $queries['total'] = true;
        $totalRegistros = $bairros->getBairros($queries);
        $paginaTotal = ceil($totalRegistros/$maxpp);
        $queries['total'] = false;
        if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual<0) $paginaAtual = 0;
        
        $this->view->pagina_atual = $paginaAtual+1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;           
        
        $this->view->rows = $bairros->getBairros($queries, $paginaAtual, $maxpp);
    }
    
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'ltebairros', 'name' => 'Bairros'),
            array('url' => null,'name' => 'Visualizar Bairro')
        );
    
        $id = (int)$this->_request->getParam("id");
        $bairros = new Ltebairros();
        $bairro = $bairros->getBairroById($id);

        if (!$bairro) $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->visualizar = true;
        $this->view->post_var = $bairro;
                
        $this->preForm();   
    }
    
    /**
     * 
     * Action de edição de bairro
     */ 
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'ltebairros', 'name' => 'Bairros'),
            array('url' => null,'name' => 'Editar Bairro')
        );  
                
        $id = (int)$this->_request->getParam("id");
        $bairros = new Ltebairros();
        $bairro = $bairros->getBairroById($id);
    
        if (!$bairro) 
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->post_var = $bairro;
            
        $this->preForm();
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($bairro);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "bairro editada com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }       
        return true;        
    }       
    
    /**
     * 
     * Action de adição de bairro 
     */
    public function adicionarAction() {

        $this->view->bread_crumb = array(
            array('url' => 'ltebairros', 'name' => 'Bairros'),
            array('url' => null,'name' => 'Adicionar Bairro')
        );
                
        $this->preForm();

        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "bairro adicionado com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }

        return true;        
    }   
    
    /**
     * Atribui valores ao view
     * @param int $id
     */    
    private function preForm($id = 0) {
    }    
    
    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_registro = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);
        
        $id = (int)$this->getRequest()->getPost("id");
        $bairro = strip_tags(trim($this->getRequest()->getPost("bairro")));
        //$cidade = strip_tags(trim($this->getRequest()->getPost("cidade")));

        $status = strip_tags(trim($this->getRequest()->getPost("status")));

        $erros = array();
        if (""==$status) array_push($erros, 'Selecione um STATUS.');
                
        $bairros = new Ltebairros();

        if (sizeof($erros)>0) return $erros; 
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
               
            $dados = array();           
            $dados['id'] = $id;
            $dados['bairro'] = $bairro;
            // $dados['cidade'] = $cidade;
            $dados['cidade'] = 'Santa Isabel';

            $dados['status'] = $status;

            $dados['excluido'] = 'nao';

            $row = $bairros->save($dados);

            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die();
        }       

        return "";      
    }
}
