<?php

class MYPDF extends TCPDF {

    // //Page header
    // public function Header(){

    //     // $image_file = 'https://saege.com.br/public/admin/imagens/logosantaisabel.jpg';
    //     // $this->Image($image_file, 18, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

    //     $this->SetFont('helvetica', 'B', 10);
    //     // cabeçalho Endereço
    //     $this->SetXY(35, 12);
    //     $this->Cell(0, 0, 'Prefeitura Municipal de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');

    //     $this->SetFont('helvetica', '', 9);
    //     $this->SetXY(35, 16);
    //     $this->Cell(0, 0, 'Relatório Estudantes Ativos', 0, false, 'L', 0, '', 0, false, 'M', 'M');
    // }

    // Page footer
    public function Footer() {

        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

        // Position at 15 mm from bottom
        $this->SetXY(50,-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'L', 0, '', 0, false, 'T', 'M');

        // Position at 15 mm from bottom
        $this->SetXY(150,-15);
        // Page number
        $this->Cell(0, 10, 'Transporte Escolar', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

class Admin_LterelatoriosController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     */
    protected $_usuario = null;

    /**
     * Inicial
     */
    public function indexAction() {

    }

    public function fretadoAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Relatórios')
        );

        $ns = new Zend_Session_Namespace('logistica_relatorios_fretado');
        $lte = new Lterelatorios();
        $queries = array(); 


        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName().'/fretado');
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var) && (isset($this->view->post_var['periodo'])) && ($this->view->post_var['periodo']!='')) {

            $queries['idperiodo'] = $this->view->post_var['periodo'];

            if ((isset($this->view->post_var['tipoestudante'])) && ($this->view->post_var['tipoestudante']!='')) $queries['tipoestudante'] = $this->view->post_var['tipoestudante'];
            $queries['tipotransporte'] = 'fretado';
            if ((isset($this->view->post_var['entidades'])) && ($this->view->post_var['entidades']!='')) $queries['identidade'] = $this->view->post_var['entidades'];

            // var_dump($queries); die();

            $relatorios['lte'] = $lte->getRelatorio($queries);

            $relatorios['total'] = 0;

            foreach ($relatorios['lte'] as $relatorio) {
                $relatorios['total'] += $relatorio['total'];
            }

            $this->view->rows = $relatorios;
        }
    }

    public function pdffretadoAction() {

        $idperiodo = (int)$this->_request->getParam("idperiodo");
        $tipoestudante = $this->_request->getParam("tipoestudante");
        $identidade = (int)$this->_request->getParam("identidade");

        $ns = new Zend_Session_Namespace('pdf_relatorio_fretado');
        $lte = new Lterelatorios();
        $periodos = new Lteperiodos();
        $queries = array();

        if ($idperiodo <> 0) {
            $queries['idperiodo'] = $idperiodo;
            $queries['tipotransporte'] = 'fretado';

            if ((!is_null($tipoestudante)) && ($tipoestudante!='')) $queries['tipoestudante'] = $tipoestudante;
            if ($identidade <> 0) $queries['identidade'] = $identidade;
        } else {
            return false;
        }

        // var_dump($queries); die();

        $rows = $lte->getRelatorio($queries);

        $total = 0;

        foreach ($rows as $relatorio) {
            $total += $relatorio['total'];
        }

        $this->view->rows = $rows;
        $this->view->total = $total;

        $periodo = $periodos->getPeriodosById($idperiodo);

        $this->view->periodo = $periodo;


        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->setPageOrientation('p');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Relatório Transporte Escolar Fretado');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(5, 4, 5);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


        // //set auto page breaks
        // //$pdf->SetAutoPageBreak(FALSE);
        // $pdf->SetAutoPageBreak(TRUE, 4);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        //$pdf->SetFont('times', '', null , 'false');
        // set font
        $pdf->SetFont('helvetica', '', 9);
        // add a page
        $pdf->AddPage('L', 'A4');

        $html = $this->view->render('lterelatorios/pdf/relatoriofretado.phtml');

        $pdf->writeHTML($html, true, 0, true, 0);

        $inicio = preg_replace("/[^0-9]/", "", $periodo['datainicio']);
        $fim = preg_replace("/[^0-9]/", "", $periodo['datafim']);

        $filename = 'relatoriofretado_'.$inicio.'_'.$fim.'_'.date('YmdHis').'.pdf';
        $pdf->Output($filename, 'D');

        Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Geração de relatório Fretado PDF executado com sucesso!");
        
        die();

        return true;
    }

    public function xlsfretadoAction() {

        $idperiodo = (int)$this->_request->getParam("idperiodo");
        $tipoestudante = $this->_request->getParam("tipoestudante");
        $identidade = (int)$this->_request->getParam("identidade");

        $ns = new Zend_Session_Namespace('xls_relatorio_fretado');
        $lte = new Lterelatorios();
        $periodos = new Lteperiodos();
        $queries = array();

        if ($idperiodo <> 0) {
            $queries['idperiodo'] = $idperiodo;
            $queries['tipotransporte'] = 'fretado';

            if ((!is_null($tipoestudante)) && ($tipoestudante!='')) $queries['tipoestudante'] = $tipoestudante;
            if ($identidade <> 0) $queries['identidade'] = $identidade;
        } else {
            return false;
        }

        // var_dump($queries); die();

        $rows = $lte->getRelatorio($queries);

        $this->view->rows = $rows;

        $periodo = $periodos->getPeriodosById($idperiodo);

        $this->view->periodo = $periodo;


        foreach ($this->view->rows as $k => $row) {
            $row['matricula'] = $this->view->formatTextoBranco($row['matricula']);
            $row['nome'] = $this->view->formatTextoBranco($row['nome']);
            $row['ra'] = $this->view->formatTextoBranco($row['ra']);
            $row['eja'] = $this->view->formatTextoBranco($row['eja']);
            $row['serie'] = $this->view->formatTextoBranco($row['serie']);
            $row['periodo'] = $this->view->formatTextoBranco($row['periodo']);
            $row['entidade'] = $this->view->formatTextoBranco($row['entidade']);
            $row['datanascimento'] = $this->view->formatTextoBranco($row['datanascimento']);
            $row['endereco'] = $this->view->formatTextoBranco($row['endereco']);
            $row['bairro'] = $this->view->formatTextoBranco($row['bairro']);
            $row['faltas'] = $this->view->formatTextoBranco($row['faltas']);
            $row['total'] = $this->view->formatTextoBranco($row['total']);
            $this->view->rows[$k] = $row;
        }

        $fields = array(
            'matricula' => array(
                'label' => 'Matricula',
                'width' => 10),
            'nome' => array(
                'label' => 'Nome',
                'width' => 10),
            'ra' => array(
                'label' => 'R.A',
                'width' => 10),
            'eja' => array(
                'label' => 'EJA',
                'width' => 10),
            'serie' => array(
                'label' => 'Série',
                'width' => 10),
            'periodo' => array(
                'label' => 'Período',
                'width' => 10),
            'entidade' => array(
                'label' => 'Entidade',
                'width' => 10),
            'datanascimento' => array(
                'label' => 'Nascimento',
                'width' => 10),
            'endereco' => array(
                'label' => 'Endereço',
                'width' => 10),
            'bairro' => array(
                'label' => 'Bairro',
                'width' => 10),
            'faltas' => array(
                'label' => 'Faltas',
                'width' => 10),
            'total' => array(
                'label' => 'Total',
                'width' =>100)
        );

        try {
            $inicio = preg_replace("/[^0-9]/", "", $periodo['datainicio']);
            $fim = preg_replace("/[^0-9]/", "", $periodo['datafim']);

            $filename = 'fretado_'.$inicio.'_'.$fim.'_'.date('YmdHis');
            $relatorioExcel = new RelatorioExcel($this, $this->view->rows);
            $relatorioExcel->generate($filename . '.xlsx', $fields);
            
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Geração do arquivo Fretado XLSX realizado com sucesso!");
            
            die();

        } catch (Exception $e) {
            echo $e->getMessage();

            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Houve um erro na geração do arquivo Fretado XLSX. Exception: ".$e, "E");

            die();
        }
    }

    public function passeAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Relatórios')
        );

        $ns = new Zend_Session_Namespace('logistica_relatorios_passe');
        $lte = new Lterelatorios();
        $queries = array(); 


        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName().'/passe');
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var) && (isset($this->view->post_var['periodo'])) && ($this->view->post_var['periodo']!='')) {

            $queries['idperiodo'] = $this->view->post_var['periodo'];

            if ((isset($this->view->post_var['tipoestudante'])) && ($this->view->post_var['tipoestudante']!='')) $queries['tipoestudante'] = $this->view->post_var['tipoestudante'];
            $queries['tipotransporte'] = 'passe';
            if ((isset($this->view->post_var['entidades'])) && ($this->view->post_var['entidades']!='')) $queries['identidade'] = $this->view->post_var['entidades'];

            // var_dump($queries); die();

            $relatorios['lte'] = $lte->getRelatorio($queries);

            $relatorios['total'] = 0;

            foreach ($relatorios['lte'] as $relatorio) {
                $relatorios['total'] += $relatorio['total'];
            }

            $this->view->rows = $relatorios;
        }
    }

    public function pdfpasseAction() {

        $idperiodo = (int)$this->_request->getParam("idperiodo");
        $tipoestudante = $this->_request->getParam("tipoestudante");
        $identidade = (int)$this->_request->getParam("identidade");

        $ns = new Zend_Session_Namespace('pdf_relatorio_passe');
        $lte = new Lterelatorios();
        $periodos = new Lteperiodos();
        $queries = array();

        if ($idperiodo <> 0) {
            $queries['idperiodo'] = $idperiodo;
            $queries['tipotransporte'] = 'passe';

            if ((!is_null($tipoestudante)) && ($tipoestudante!='')) $queries['tipoestudante'] = $tipoestudante;
            if ($identidade <> 0) $queries['identidade'] = $identidade;
        } else {
            return false;
        }

        // var_dump($queries); die();

        $rows = $lte->getRelatorio($queries);

        $total = 0;

        foreach ($rows as $relatorio) {
            $total += $relatorio['total'];
        }

        $this->view->rows = $rows;
        $this->view->total = $total;

        $periodo = $periodos->getPeriodosById($idperiodo);

        $this->view->periodo = $periodo;


        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->setPageOrientation('p');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Relatório Transporte Escolar Passe Escolar');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(5, 4, 5);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


        // //set auto page breaks
        // //$pdf->SetAutoPageBreak(FALSE);
        // $pdf->SetAutoPageBreak(TRUE, 4);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        //$pdf->SetFont('times', '', null , 'false');
        // set font
        $pdf->SetFont('helvetica', '', 9);
        // add a page
        $pdf->AddPage('L', 'A4');

        $html = $this->view->render('lterelatorios/pdf/relatoriopasse.phtml');

        $pdf->writeHTML($html, true, 0, true, 0);

        $inicio = preg_replace("/[^0-9]/", "", $periodo['datainicio']);
        $fim = preg_replace("/[^0-9]/", "", $periodo['datafim']);

        $filename = 'relatoriopasse_'.$inicio.'_'.$fim.'_'.date('YmdHis').'.pdf';
        $pdf->Output($filename, 'D');

        Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Geração de relatório Passe PDF executado com sucesso!");        
        
        die();

        return true;
    }

    public function xlspasseAction() {

        $idperiodo = (int)$this->_request->getParam("idperiodo");
        $tipoestudante = $this->_request->getParam("tipoestudante");
        $identidade = (int)$this->_request->getParam("identidade");

        $ns = new Zend_Session_Namespace('xls_relatorio_passe');
        $lte = new Lterelatorios();
        $periodos = new Lteperiodos();
        $queries = array();

        if ($idperiodo <> 0) {
            $queries['idperiodo'] = $idperiodo;
            $queries['tipotransporte'] = 'passe';

            if ((!is_null($tipoestudante)) && ($tipoestudante!='')) $queries['tipoestudante'] = $tipoestudante;
            if ($identidade <> 0) $queries['identidade'] = $identidade;
        } else {
            return false;
        }

        // var_dump($queries); die();

        $rows = $lte->getRelatorio($queries);

        $this->view->rows = $rows;

        $periodo = $periodos->getPeriodosById($idperiodo);

        $this->view->periodo = $periodo;


        foreach ($this->view->rows as $k => $row) {
            $row['matricula'] = $this->view->formatTextoBranco($row['matricula']);
            $row['nome'] = $this->view->formatTextoBranco($row['nome']);
            $row['ra'] = $this->view->formatTextoBranco($row['ra']);
            $row['eja'] = $this->view->formatTextoBranco($row['eja']);
            $row['serie'] = $this->view->formatTextoBranco($row['serie']);
            $row['periodo'] = $this->view->formatTextoBranco($row['periodo']);
            $row['entidade'] = $this->view->formatTextoBranco($row['entidade']);
            $row['datanascimento'] = $this->view->formatTextoBranco($row['datanascimento']);
            $row['endereco'] = $this->view->formatTextoBranco($row['endereco']);
            $row['bairro'] = $this->view->formatTextoBranco($row['bairro']);
            $row['faltas'] = $this->view->formatTextoBranco($row['faltas']);
            $row['total'] = $this->view->formatTextoBranco($row['total']);
            $this->view->rows[$k] = $row;
        }

        $fields = array(
            'matricula' => array(
                'label' => 'Matricula',
                'width' => 10),
            'nome' => array(
                'label' => 'Nome',
                'width' => 10),
            'ra' => array(
                'label' => 'R.A',
                'width' => 10),
            'eja' => array(
                'label' => 'EJA',
                'width' => 10),
            'serie' => array(
                'label' => 'Série',
                'width' => 10),
            'periodo' => array(
                'label' => 'Período',
                'width' => 10),
            'entidade' => array(
                'label' => 'Entidade',
                'width' => 10),
            'datanascimento' => array(
                'label' => 'Nascimento',
                'width' => 10),
            'endereco' => array(
                'label' => 'Endereço',
                'width' => 10),
            'bairro' => array(
                'label' => 'Bairro',
                'width' => 10),
            'faltas' => array(
                'label' => 'Faltas',
                'width' => 10),
            'total' => array(
                'label' => 'Total',
                'width' =>100)
        );

        try {
            $inicio = preg_replace("/[^0-9]/", "", $periodo['datainicio']);
            $fim = preg_replace("/[^0-9]/", "", $periodo['datafim']);

            $filename = 'passe_'.$inicio.'_'.$fim.'_'.date('YmdHis');
            $relatorioExcel = new RelatorioExcel($this, $this->view->rows);
            $relatorioExcel->generate($filename . '.xlsx', $fields);
            
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Geração do arquivo Passe XLSX executado com sucesso!");
            
            die();

        } catch (Exception $e) {
            echo $e->getMessage();
            
             Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Houve um erro na geração do arquivo Passe XLSX. Exception: ".$e, "E");
            
            die();
        }
    }

    public function universitariosAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Relatórios')
        );

        $ns = new Zend_Session_Namespace('logistica_relatorios_universitarios');
        $lte = new Lterelatorios();
        $queries = array(); 


        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName().'/universitarios');
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var) && (isset($this->view->post_var['periodo'])) && ($this->view->post_var['periodo']!='')) {

            $queries['idperiodo'] = $this->view->post_var['periodo'];

            if ((isset($this->view->post_var['entidades_universidades'])) && ($this->view->post_var['entidades_universidades']!='')) $queries['identidade'] = $this->view->post_var['entidades_universidades'];
            if ((isset($this->view->post_var['subsidio'])) && ($this->view->post_var['subsidio']!='')) $queries['subsidio'] = $this->view->post_var['subsidio'];

            // var_dump($queries); die();

            $relatorios['lte'] = $lte->getUniversitarios($queries);
           
            $relatorios['total'] = 0;

            foreach ($relatorios['lte'] as $relatorio) {
                $relatorios['total'] += $relatorio['subsidio'];
            }

            $this->view->rows = $relatorios;
        }
    }

    public function pdfuniversitariosAction() {

        $idperiodo = (int)$this->_request->getParam("idperiodo");
        $identidade = (int)$this->_request->getParam("identidade");
        $subsidio = $this->_request->getParam("subsidio");

        $ns = new Zend_Session_Namespace('pdf_relatorio_universitarios');
        $lte = new Lterelatorios();
        $periodos = new Lteperiodos();
        $queries = array();

        if ($idperiodo <> 0) {
            $queries['idperiodo'] = $idperiodo;

            if ($identidade <> 0) $queries['identidade'] = $identidade;
            if ((!is_null($subsidio)) && ($subsidio!='')) $queries['subsidio'] = $subsidio;

        } else {
            return false;
        }

        // var_dump($queries); die();

        $rows = $lte->getUniversitarios($queries);

        $total = 0;

        foreach ($rows as $relatorio) {
            $total += $relatorio['subsidio'];
        }

        $this->view->rows = $rows;
        $this->view->total = $total;

        $periodo = $periodos->getPeriodosById($idperiodo);

        $this->view->periodo = $periodo;


        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Relatório Transporte Escolar Fretado');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(5, 4, 5);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


        // //set auto page breaks
        // //$pdf->SetAutoPageBreak(FALSE);
        // $pdf->SetAutoPageBreak(TRUE, 4);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        //$pdf->SetFont('times', '', null , 'false');
        // set font
        $pdf->SetFont('helvetica', '', 9);
        // add a page
        $pdf->AddPage('L', 'A4');

        $html = $this->view->render('lterelatorios/pdf/relatoriouniversitarios.phtml');

        $pdf->writeHTML($html, true, 0, true, 0);

        $inicio = preg_replace("/[^0-9]/", "", $periodo['datainicio']);
        $fim = preg_replace("/[^0-9]/", "", $periodo['datafim']);

        $filename = 'relatoriouniversitarios_'.$inicio.'_'.$fim.'_'.date('YmdHis').'.pdf';
        $pdf->Output($filename, 'D');

        Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Geração do relatório Universitário PFF executado com sucesso.");
        
        die();

        return true;
    }

    public function xlsuniversitariosAction() {

        $idperiodo = (int)$this->_request->getParam("idperiodo");
        $identidade = (int)$this->_request->getParam("identidade");
        $subsidio = $this->_request->getParam("subsidio");

        $ns = new Zend_Session_Namespace('xls_relatorio_universitarios');
        $lte = new Lterelatorios();
        $periodos = new Lteperiodos();
        $queries = array();

        if ($idperiodo <> 0) {
            $queries['idperiodo'] = $idperiodo;

            if ($identidade <> 0) $queries['identidade'] = $identidade;
            if ((!is_null($subsidio)) && ($subsidio!='')) $queries['subsidio'] = $subsidio;

        } else {
            return false;
        }

        // var_dump($queries); die();

        $rows = $lte->getUniversitarios($queries);

        $this->view->rows = $rows;

        $periodo = $periodos->getPeriodosById($idperiodo);

        $this->view->periodo = $periodo;


        foreach ($this->view->rows as $k => $row) {
            // $row['nome'] = ($nome) ? $this->view->formatTextoBranco($nome['nome']) : '--';
            // $row['filiacao'] = ($filiacao) ? $this->view->formatTextoBranco($filiacao['filiacao']) : '--';
            // $row['entidade'] = ($entidade) ? $this->view->formatTextoBranco($entidade['entidade']) : '--';
            // $row['cidade'] = ($cidade) ? $this->view->formatTextoBranco($cidade['cidade']) : '--';
            $row['valorfretado'] = 'R$ '.number_format($row['valorfretado'],2,',','');
            $row['subsidio'] = 'R$ '.number_format($row['subsidio'],2,',','');
            $this->view->rows[$k] = $row;
        }

        $fields = array(
            'nome' => array(
                'label' => 'Nome',
                'width' => 70),
            'cpf' => array(
                'label' => 'CPF',
                'width' => 70),
            'filiacao' => array(
                'label' => 'Filiação',
                'width' => 10),
            'entidade' => array(
                'label' => 'Entidade',
                'width' => 30),
            'cidade' => array(
                'label' => 'Cidade',
                'width' => 20),
            'valorfretado' => array(
                'label' => 'Valor Fretado',
                'width' => 20),
            'subsidio' => array(
                'label' => 'Subsídio',
                'width' => 10)
        );

        try {
            $inicio = preg_replace("/[^0-9]/", "", $periodo['datainicio']);
            $fim = preg_replace("/[^0-9]/", "", $periodo['datafim']);

            $filename = 'universitarios_'.$inicio.'_'.$fim.'_'.date('YmdHis');
            $relatorioExcel = new RelatorioExcel($this, $this->view->rows);
            $relatorioExcel->generate($filename . '.xlsx', $fields);
            
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Geração do arquivo Universitário XLSX executado com sucesso!");

            die();

        } catch (Exception $e) {
            echo $e->getMessage();

            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Houve um erro na geração do arquivo Universitário XLSX. Exception: ".$e, "E");

            die();
        }
    }

    public function setentidadesAction() {
        $this->_helper->layout->disableLayout();
        
        $queries = array();
        $tipo = $this->getRequest()->getPost('tipo');

        if ($tipo == '') {
            $entidades = new Lteveiculosestudantes();
            $this->view->rows = $entidades->getEntidadesTransporte(array('ntipo'=>'universidade'));
        } else {
            $entidades = new Lteveiculosestudantes();
            $this->view->rows = $entidades->getEntidadesTransporte(array('tipo'=>$tipo));
        }
    }
}