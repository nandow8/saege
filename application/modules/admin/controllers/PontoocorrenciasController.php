<?php

/**
 * Controle da classe atas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PontoocorrenciasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pontoocorrencias
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("pontoocorrencias", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Funcionariosgeraispontoocorrencias();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Registro de ID $id excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction(){
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="pontoocorrencias") $objs = new Funcionariosgeraispontoocorrencias();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'atas', 'name' => 'Atas'),
			array('url' => null,'name' => 'Visualizar Ata')
		);
		
		$id = (int)$this->_request->getParam("id");
		$atas = new Funcionariosgeraispontoocorrencias();
		$ata = $atas->getAtaById($id, array());
		
		if (!$ata) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $ata;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
        * Listagem
        */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Ocorrências de Ponto')
		);
		
                
                
		$ns = new Zend_Session_Namespace('default_atas');
		$atas = new Funcionariosgeraispontoocorrencias();
		$queries = array();	
		//$queries['iddepartamento'] = '13';			
		//PESQUISA
                
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	/*
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			//if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
//if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
if ($this->view->post_var["ocorrencia"]!="") $queries["ocorrencia"] = $this->view->post_var["ocorrencia"];
if ($this->view->post_var["descricao"]!="") $queries["descricao"] = $this->view->post_var["descricao"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		*/
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $atas->getAtas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
                
		$this->view->rows = $atas->getAtas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de atas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'atas', 'name' => 'Atas'),
			array('url' => null,'name' => 'Editar Ata')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$atas = new Funcionariosgeraispontoocorrencias();
		$ata = $atas->getAtaById($id);
		
		if (!$ata) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $ata;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($ata);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ata editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de atas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'pontoocorrencias', 'name' => 'Ocorrencias'),
			array('url' => null,'name' => 'Adicionar Ocorrência')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ata adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idata
     */    
    private function preForm($idata = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_ata = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		//$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
		$iddepartamento = 13;
                
                $codigo = $this->getRequest()->getPost("codigo");
                $ocorrencia = $this->getRequest()->getPost("ocorrencia");
                $descricao = $this->getRequest()->getPost("descricao");
				$status = trim($this->getRequest()->getPost("status1"));
				$periodo = $this->getRequest()->getPost("periodo");
				$divergencia = $this->getRequest()->getPost("divergencia");
				$extra = $this->getRequest()->getPost("extra");
				$atraso = $this->getRequest()->getPost("atraso");

				($periodo) ? $periodo = 'sim' : $periodo = 'nao';
                
                if($divergencia){

					$divergencia = 'sim';
					($extra) ? $extra = 'sim' : $extra = 'nao';
					($atraso) ? $atraso = 'sim' : $atraso = 'nao';
						
                }else{

					$divergencia = 'nao';
					$extra = 'nao';
					$atraso = 'nao';

				}//end if else
               
		
		
		$erros = array();
/*		
		if (""==$data) array_push($erros, "Informe a Data.");
if (0==$idperiodo) array_push($erros, "Informe a Período.");
if (""==$pauta) array_push($erros, "Informe a Pauta.");
if (""==$descricoes) array_push($erros, "Informe a Descrições.");
if (""==$status) array_push($erros, "Informe a Status.");
*/
		
		$atas = new Funcionariosgeraispontoocorrencias();
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
                       
                        $dados['ocorrencia'] = $ocorrencia;
						$dados['descricao'] = $descricao;
						$dados['periodo'] = $periodo;
						$dados['divergencia'] = $divergencia;
						$dados['extra'] = $extra;
						$dados['atraso'] = $atraso;
                        
						$dados["status"] = $status;

			
						$dados['excluido'] = 'nao';
						$dados['logusuario'] = $this->_usuario['id'];;
						$dados['logdata'] = date('Y-m-d G:i:s');
						
						$row = $atas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}