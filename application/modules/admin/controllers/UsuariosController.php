<?php

/**
 * Controle da classe usuários do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_UsuariosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Usuario
	 */
	protected $_usuario = null;	
	protected $_isAdmin = false;
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("usuarios", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}		

		$this->_isAdmin = Usuarios::isAdmin(Usuarios::getUsuario('id'));
		$this->view->isAdmin = $this->_isAdmin;
	}	
	

	public function funcionariosAction() {

		$this->_helper->layout->disableLayout();
		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
		$rows = $funcionariosgeraisescolas->getFuncionariosgeraisescolas(array('professor'=>'Sim'));

		$usuarios = new Usuarios();
		foreach ($rows as $i => $row) {
			if((!isset($row['rgf'])) || ($row['rgf']=="")){
				continue;
			}
			$usuario = Usuarios::getUsuarioByRgfHelper($row['rgf']);
			if((!$usuario['id']) || ((int)$usuario['id']<=0)) {
				$dados = array();
				$dados['idperfil'] = '28';
				$dados['idprefeitura'] = '1';
				$dados['secretaria'] = '1';
				$dados['idsecretarias'] = '1';
				$dados['nomerazao'] = $row['nome'];
				$dados['rgf'] = $row['rgf'];
				$dados['email'] = $row['email'];
				$dados['senha'] = $row['rgf'];
				$dados['idfuncionario'] = $row['id'];
				$dados['status'] = 'Ativo';	
				$dados['excluido'] = 'nao';
				$dados['logusuario'] = Usuarios::getUsuario('id');
				$dados['logdata'] = date('Y-m-d G:i:s');

				$usuarios->save($dados);
			}
			if((!isset($usuario['idfuncionario'])) || (!$usuario['idfuncionario']) || ((int)$usuario['idfuncionario']<=0)){

				if($usuario['rgf']==$row['rgf']){
					$usuario['idperfil'] = 28;
					$usuario['idfuncionario'] = $row['id'];

					$usuarios->save($usuario);
				}
			}
			echo 'passou_'	. $usuario['id'] . '<br />';
		}
		
		die("OK");
		
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		if ($id==Usuarios::getUsuario('id')) {
			die('Você não pode excluir a si mesmo!');
		}
		
		$rows = new Usuarios();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			/*if ((!$this->_isAdmin) && (Usuarios::podeVisualizar($id, Usuarios::getUsuario('id')))) {
				die('Sem permissão');
			}*/
			
			
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Usuário excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		if ($id==Usuarios::getUsuario('id')) {
			die('Você não pode mudar seu próprio status');
		}
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="usuarios") $objs = new Usuarios();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			if ((!$this->_isAdmin) && (Usuarios::podeVisualizar($id, Usuarios::getUsuario('id')))) {
				die('Sem permissão');
			}			
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Usuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}	


	public function getusuarioAction() {
		$this->_helper->layout->disableLayout();
		
		$idusuario = (int)$this->_request->getPost("idusuario");
		
		$rows = new Usuarios();
		$row = $rows->getUsuarioById($idusuario);
		
		if ($row) {
			if(isset($row['iddepartamento']))
			{
				$departamento = Departamentossecretarias::getDepartamentosecretariaByIdHelper($row['iddepartamento']);
				if($departamento) $row['departamento'] = $departamento['departamento'];
			}
			echo json_encode(array('status' => 'OK', 'response' => $row));die();
		}
		
		echo json_encode(array('status' => 'ERROR', 'response' => 'Funcionário não encontrado!'));die();
	}		
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Usuários')
		);
		
		
		$ns = new Zend_Session_Namespace('admin_usuarios');
		$usuarios = new Usuarios();
		$queries = array();	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		if(Usuarios::getUsuario('master')!="sim"):			
			$queries['master'] = 'nao';
		endif;

		//restricao
		if (!$this->_isAdmin) {
			//$queries['onlyid'] = Usuarios::getUsuario('id');
		}
		//fim restricao
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		//echo '<pre>';
    		//print_r($this->view->post_var);die();
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ((isset($this->view->post_var['status1'])) && ($this->view->post_var['status1']!='')) $queries['status'] = $this->view->post_var['status1'];
    		if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		if ((isset($this->view->post_var['idperfil'])) && ($this->view->post_var['idperfil']!='')) $queries['idperfil'] = $this->view->post_var['idperfil'];
    		if ((isset($this->view->post_var['rgf'])) && ($this->view->post_var['rgf']!='')) $queries['rgf'] = $this->view->post_var['rgf'];
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;

		$totalRegistros = $usuarios->getUsuarios($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;

		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		$queries['die'] = true;
		$this->view->rows = $usuarios->getUsuarios($queries, $paginaAtual, $maxpp);


	}
	
	/**
	 * 
	 * Action de edição de usuários
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'usuarios',	'name' => 'Usuários'),
			array('url' => null,'name' => 'Editar usuário')
		);	
				
		
		$id = (int)$this->_request->getParam("id");
		$usuarios = new Usuarios();
		$usuario = $usuarios->fetchRow("id=".$id." AND excluido='nao'");
		
		/*
		if ((!$this->_isAdmin) && (Usuarios::podeVisualizar($id, Usuarios::getUsuario('id')))) {
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		}
		*/
		
		if (!$usuario) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $usuario->toArray();
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost();
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Usuário editado com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	

	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'treinamento', 'name' => 'Treinamento'),
			array('url' => null,'name' => 'Visualizar Treinamento')
		);
		
		$id = (int)$this->_request->getParam("id");
		$usuarios = new Usuarios();
		$usuario = $usuarios->fetchRow("id=".$id." AND excluido='nao'");
		
		/*
		if ((!$this->_isAdmin) && (Usuarios::podeVisualizar($id, Usuarios::getUsuario('id')))) {
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		}
		*/
		
		if (!$usuario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

		$this->view->post_var = $usuario;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	/**
	 * 
	 * Action de adição de usuários 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'usuarios',	'name' => 'Usuários'),
			array('url' => null,'name' => 'Adicionar usuário')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost();
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Usuário adicionado com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idusuario
     */    
    private function preForm($idusuario = 0) {
    	$perfis = new UsuariosPerfis();
    	$this->view->perfis = $perfis->getPerfis(); 
    	
    	$departamentos = new Departamentossecretarias();
    	$this->view->departamentos = $departamentos->getDepartamentossecretarias(array('status'=>'Ativo'));
    	
		$prefeituras = new Prefeituras();
		$this->view->prefeituras = $prefeituras->getPrefeituras(array('status'=>'Ativo', 'idssecretarias'=>Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' )));
		/*
		if(Usuarios::getUsuario('master')!="sim"):
			
		else:
			$this->view->prefeituras = $prefeituras->getPrefeituras(array('status'=>'Ativo'));
		endif;
		*/
		
		
		//$queries['idssecretarias'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		//var_dump(Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' )); die();
		$secretarias = new Secretarias();
		$this->view->secretarias = $secretarias->getSecretarias(array('status'=>'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost() {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idprefeitura = strip_tags((int)trim($this->getRequest()->getPost("idprefeitura")));
		$nomerazao = trim($this->getRequest()->getPost("nomerazao"));
		$sobrenomefantasia = trim($this->getRequest()->getPost("sobrenomefantasia"));
		$rgf = trim($this->getRequest()->getPost("rgf"));
		$email = trim($this->getRequest()->getPost("email"));
		$senha = trim($this->getRequest()->getPost("senha"));
		$idperfil = (int)trim($this->getRequest()->getPost("idperfil"));
		$status = trim($this->getRequest()->getPost("status1"));
		$secretaria = trim($this->getRequest()->getPost("secretaria"));
		$iddepartamento = trim((int)$this->getRequest()->getPost("iddepartamento"));
                
		$idsecretarias = $this->getRequest()->getPost("idsecretarias");
		//if (!is_array($idsecretarias)) $idsecretarias = array();
		//$idsecretarias = implode(',',$idsecretarias);
          $idsecretarias = 1;      
		$erros = array();
			
		if (""==$nomerazao) array_push($erros, 'Preencha o campo NOME DO USUÁRIO.');
		if (""==$sobrenomefantasia) array_push($erros, 'Preencha o campo SOBRENOME DO USUÁRIO.');
		if (0>=$idprefeitura) array_push($erros, 'Selecione UMA PREFEITURA.');
		if (""==$rgf) array_push($erros, 'Preencha o campo RGF.');
		if (!Mn_Util::isValidEmail($email)) array_push($erros, 'O E-MAIL DE ACESSO é inválido.');
		if (($id==0) && (""==$senha)) array_push($erros, 'Preencha o campo SENHA.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$usuarios = new Usuarios();
		$row = $usuarios->fetchRow("excluido='nao' AND email='$email' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe usuário com esse EMAIL DE ACESSO.');
			
		if (sizeof($erros)>0) return $erros; 
		
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			/*
			if ($this->_isAdmin) {
				$dados = array();
				$dados['id'] = $id;
				$dados['idperfil'] = $idperfil;
				$dados['secretaria'] = $secretaria;
				$dados['iddepartamento'] = $iddepartamento;
				$dados['nomerazao'] = $nomerazao;
				$dados['sobrenomefantasia'] = $sobrenomefantasia;
				$dados['email'] = $email;
				if ($senha!='') $dados['senha'] = $senha;
				$dados['status'] = $status;	
			} else {
				$dados = array();
				$dados['id'] = $id;
				$dados['idparent'] = ($id==0) ? Usuarios::getUsuario('id') : 0;
				$dados['idperfil'] = Usuarios::getUsuario('idperfil');
				$dados['secretaria'] = $secretaria;
				$dados['iddepartamento'] = $iddepartamento;
				$dados['nomerazao'] = $nomerazao;
				$dados['sobrenomefantasia'] = $sobrenomefantasia;
				$dados['email'] = $email;
				if ($senha!='') $dados['senha'] = $senha;
				$dados['status'] = $status;
				$dados['tipo'] = Usuarios::getUsuario('tipo');		
			}*/
			$dados = array();
			$dados['id'] = $id;
			$dados['idperfil'] = $idperfil;
			$dados['idprefeitura'] = $idprefeitura;
			$dados['secretaria'] = $secretaria;
			$dados['iddepartamento'] = $iddepartamento;
			$dados['idsecretarias'] = $idsecretarias;
			$dados['nomerazao'] = $nomerazao;
			$dados['sobrenomefantasia'] = $sobrenomefantasia;
			$dados['rgf'] = $rgf;
			$dados['email'] = $email;
			if ($senha!='') $dados['senha'] = $senha;
			$dados['status'] = $status;	
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Usuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');

			$row = $usuarios->save($dados);

			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }

	
	
}