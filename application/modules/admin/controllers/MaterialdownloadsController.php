<?php

/**
 * Controle da classe coordenacaomateriais do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_MaterialdownloadsController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Coordenacaomaterial
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("materialdownloads", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}


		$secretariasprovastrabalhos = new Coordenacaomateriais();
		$queries = array();	
		$ultimo = $secretariasprovastrabalhos->getUltimoCoordenacaomateria($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Coordenacaomateriais();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Material disponivel excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="materialdownloads") $objs = new Coordenacaomateriais();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'materialdownloads', 'name' => 'Materiais disponpiniveis'),
			array('url' => null,'name' => 'Visualizar Material disponivel')
		);
		
		$id = (int)$this->_request->getParam("id");
		$coordenacaomateriais = new Coordenacaomateriais();
		$coordenacaomaterial = $coordenacaomateriais->getCoordenacaomaterialById($id, array());
		
		if (!$coordenacaomaterial) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaomaterial;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Materiais disponpiniveis')
		);
		
		$ns = new Zend_Session_Namespace('default_materialdownloads');
		$coordenacaomateriais = new Coordenacaomateriais();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $coordenacaomateriais->getCoordenacaomateriais($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $coordenacaomateriais->getCoordenacaomateriais($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de coordenacaomateriais
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'materialdownloads', 'name' => 'Materiais disponpiniveis'),
			array('url' => null,'name' => 'Editar Material disponivel')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$coordenacaomateriais = new Coordenacaomateriais();
		$coordenacaomaterial = $coordenacaomateriais->getCoordenacaomaterialById($id);
		
		if (!$coordenacaomaterial) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaomaterial;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($coordenacaomaterial);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Material disponivel editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de coordenacaomateriais 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'materialdownloads', 'name' => 'Materiais disponpiniveis'),
			array('url' => null,'name' => 'Adicionar Material disponivel')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Material disponivel adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idcoordenacaomaterial
     */    
    private function preForm($idcoordenacaomaterial = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_coordenacaomaterial = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
$idmateria = (int)trim($this->getRequest()->getPost("idmateria"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
$confirmacaocoordenacao = trim($this->getRequest()->getPost("confirmacaocoordenacao"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idprofessor) array_push($erros, "Informe a Professor.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$coordenacaomateriais = new Coordenacaomateriais();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
if((int)$id <= 0 )			$dados["sequencial"] = $sequencial;
if((int)$id <= 0 )$dados["datalancamento"] = date("Y-m-d", $datalancamento);
if((int)$id <= 0 )$dados["horalancamento"] = $horalancamento;
$dados["idprofessor"] = $idprofessor;
$dados["idmateria"] = $idmateria;
$dados["descricoes"] = $descricoes;

$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;
$dados["confirmacaocoordenacao"] = $confirmacaocoordenacao;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $coordenacaomateriais->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}