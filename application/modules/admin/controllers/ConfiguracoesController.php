<?php
class Admin_ConfiguracoesController extends Zend_Controller_Action {
	
	public function preDispatch() {
		//TODO Coloar isAccess na view
		
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("configuracoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}			
	}	

	public function indexAction() {
		$rows = new Configuracoes();
		$this->view->rows = $rows->getConfiguracoes();
	}	
	
	public function editarAction() {
		$id = (int)$this->_request->getParam("id");
		$configuracoes = new Configuracoes();
		$configuracao = $configuracoes->fetchRow("id=".$id." AND excluido='nao'");
		
		if (!$configuracao) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());	
		
		
		$this->view->post_var = $configuracao->toArray();
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost();
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Configuração editada com sucesso.";
			
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
    
	private function preForm() { }	    
	
    private function getPost() {
		if (!isset($this->view->post_var)) {
			$this->view->post_var = $_POST;
		} else {
			$this->view->post_var = array_merge($this->view->post_var, $_POST);
		}
		
		$id = (int)$this->getRequest()->getPost("id");
		$valor = trim($this->getRequest()->getPost("valor"));
		
		$erros = array();
			
		if ($id==0) array_push($erros, 'Falha crítica.');
		if (""==$valor) array_push($erros, 'Preencha o campo VALOR.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$configuracoes = new Configuracoes();
		$dados = array(
			'id' => $id,
			'valor' => $valor
		);
		$configuracoes->save($dados);
				
		return "";    	
    }   	
	
}