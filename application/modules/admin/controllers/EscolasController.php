<?php

class Admin_EscolasController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacoteescola
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("escolas", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     *
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Escolas();
        $row = $rows->fetchRow("id=" . $id);
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = Usuarios::getUsuario('id');
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Escola excluída com sucesso.";

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "escolas")
            $objs = new Escolas();
        $obj = $objs->fetchRow("excluido='nao' AND id=" . $id);
        if ($obj) {

            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = Usuarios::getUsuario('id');
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    public function changeorderxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $idfrom = (int) $this->getRequest()->getPost("from");
        $idto = (int) $this->getRequest()->getPost("to");
        $op = $this->getRequest()->getPost("op");

        if ($op == "change")
            $objs = new Escolas();
        $from = $objs->fetchRow("excluido='nao' AND id=" . $idfrom);
        $to = $objs->fetchRow("excluido='nao' AND id=" . $idto);

        if (($from) && ($to)) {
            $from = $from->toArray();
            $to = $to->toArray();
            $ordemFrom = $from["ordem"];
            $orderTo = $to["ordem"];

            $from['ordem'] = $orderTo;
            $to['ordem'] = $ordemFrom;

            $objs->save($from);
            $objs->save($to);
        }

        $this->view->message = "OK";
        $this->render("xml");
    }

    public function ordemAction() {
        $ordem = (int) $this->getRequest()->getParam('ordem', 0);
        $d = $this->getRequest()->getParam('d', 0);

        $rows = new Escolas();
        $rows->swapOrdem($ordem, $d, false);

        $this->_redirect("/admin/" . $this->_request->getControllerName() . "/index");
        die();
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Escolas')
        );


        $ns = new Zend_Session_Namespace('admin_escolas');
        $escolas = new Escolas();
        $queries = array();

        $dataEscola = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'));
        if ($dataEscola):
            $queries['id'] = $dataEscola['id'];
        endif;

        //$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();
        }
        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {

            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = $v;
            if ($this->view->post_var['status1'] != '')
                $queries['status'] = $this->view->post_var['status1'];
            if (isset($this->view->post_var['chave']) && $this->view->post_var['chave'] != '')
                $queries['chave'] = $this->view->post_var['chave'];
        }

        //PAGINACAO
        $maxpp = 15;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $escolas->getEscolas($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $escolas->getEscolas($queries, $paginaAtual, $maxpp);
    }

    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolas', 'name' => 'Escolas'),
            array('url' => null, 'name' => 'Visualizar escola')
        );

        $id = (int) $this->_request->getParam("id");
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($id);

        if (!$escola)
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());

        $this->view->visualizar = true;
        $this->view->post_var = $escola;
        $endereco = Enderecos::getEnderecoById((int) $escola['idendereco']);
        if ($endereco) {
            unset($endereco['id']);
            unset($endereco['excluido']);
            $this->view->post_var = array_merge($this->view->post_var, $endereco);
        }

        $this->preForm();
    }

    /**
     *
     * Action de edição de escola
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolas', 'name' => 'Escolas'),
            array('url' => null, 'name' => 'Editar escola')
        );

        $id = (int) $this->_request->getParam("id");
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($id);

        if (!$escola)
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());


        $this->view->post_var = $escola;

        $endereco = Escolasenderecos::getEnderecoByIdEscolaHelper((int) $escola['idendereco']);

        if ($endereco) {
            unset($endereco['id']);
            unset($endereco['excluido']);
            unset($endereco['latitude']);
            unset($endereco['longitude']);
            $this->view->post_var = array_merge($this->view->post_var, $endereco);
        }

        $tipos = new Estabelecimentos_Escolasabastecimentos();
        $tipo_esgoto = $tipos->getAbastecimentoByIdEscola($id);


        if ($tipo_esgoto) {
            unset($tipo_esgoto['id']);
            unset($tipo_esgoto['excluido']);
            unset($tipo_esgoto['status']);
            unset($tipo_esgoto['logdata']);
            unset($tipo_esgoto['logusuario']);
            unset($tipo_esgoto['idsecretaria']);
            $this->view->post_var = array_merge($this->view->post_var, $tipo_esgoto);
        }

        $redes = new Estabelecimentos_Escolasredes();
        $tipo_rede = $redes->getRedeByIdEscola($id);

        if ($tipo_rede) {
            unset($tipo_rede['id']);
            unset($tipo_rede['excluido']);
            unset($tipo_rede['status']);
            unset($tipo_rede['logdata']);
            unset($tipo_rede['logusuario']);
            $tipo_rede['idtiporedeagua'] = $tipo_rede['idtipo'];
            unset($tipo_rede['idtipo']);
            $this->view->post_var = array_merge($this->view->post_var, $tipo_rede);
        }

        $infraestrutura = new Estabelecimentos_Escolasinfraestruturas();
        $tipo_infraestrutura = $infraestrutura->getInfraestruturaByIdEscola($id);

        if ($tipo_infraestrutura) {
            unset($tipo_infraestrutura['id']);
            unset($tipo_infraestrutura['excluido']);
            unset($tipo_infraestrutura['status']);
            unset($tipo_infraestrutura['logdata']);
            unset($tipo_infraestrutura['logusuario']);
            $this->view->post_var = array_merge($this->view->post_var, $tipo_infraestrutura);
        }

        $terrenos = new Estabelecimentos_Escolasterrenos();
        $terreno = $terrenos->getTerrenoByIdEscola($id);

        if ($terreno) {
            unset($terreno['id']);
            unset($terreno['excluido']);
            unset($terreno['status']);
            unset($terreno['logdata']);
            unset($terreno['logusuario']);
            $this->view->post_var = array_merge($this->view->post_var, $terreno);
        }

        $perfistecnicos = new Estabelecimentos_Escolasperfistecnicos();
        $perfil = $perfistecnicos->getPerfilTecnicoByIdEscola($id);

        if ($perfil) {
            unset($perfil['id']);
            unset($perfil['excluido']);
            unset($perfil['status']);
            unset($perfil['logdata']);
            unset($perfil['logusuario']);
            $perfil['quantidadeperfil'] = $perfil['quantidade'];
            unset($perfil['quantidade']);
            $this->view->post_var = array_merge($this->view->post_var, $perfil);
        }

        $esportivos = new Estabelecimentos_Escolasestruturasesportivas();
        $esportivo = $esportivos->getEstruturaesportivaByIdEscola($id);

        if ($esportivo) {
            unset($esportivo['id']);
            unset($esportivo['excluido']);
            unset($esportivo['status']);
            unset($esportivo['logdata']);
            unset($esportivo['logusuario']);
            $esportivo['existenciaesportivo'] = $esportivo['existencia'];
            unset($esportivo['existencia']);
            $esportivo['quantidadeesportivo'] = $esportivo['quantidade'];
            unset($esportivo['quantidade']);
            $esportivo['capacidademediaesportivo'] = $esportivo['capacidademedia'];
            unset($esportivo['capacidademedia']);
            $this->view->post_var = array_merge($this->view->post_var, $esportivo);
        }

        $instalacoes = new Estabelecimentos_Escolasinstalacoes();
        $instalacao = $instalacoes->getInstalacaoByIdEscola($id);

        if ($instalacao) {
            unset($instalacao['id']);
            unset($instalacao['excluido']);
            unset($instalacao['status']);
            unset($instalacao['logdata']);
            unset($instalacao['logusuario']);

            $this->view->post_var = array_merge($this->view->post_var, $instalacao);
        }

        $documentacoes = new Estabelecimentos_Escolasdocumentacoes();
        $documentacao = $documentacoes->getDocumentacaoByIdEscola($id);

        if ($documentacao) {
            unset($documentacao['id']);
            unset($documentacao['excluido']);
            unset($documentacao['status']);
            unset($documentacao['logdata']);
            unset($documentacao['logusuario']);
            $documentacao['idtipodocumentacao'] = $documentacao['idtipo'];
            unset($documentacao['idtipo']);
            $this->view->post_var = array_merge($this->view->post_var, $documentacao);
        }

        $servicos = new Estabelecimentos_Escolasservicos();
        $servico = $servicos->getServicoByIdEscola($id);

        if ($servico) {
            unset($servico['id']);
            unset($servico['excluido']);
            unset($servico['status']);
            unset($servico['logdata']);
            unset($servico['logusuario']);

            $this->view->post_var = array_merge($this->view->post_var, $servico);
        }

        $edificacoes = new Escolasedificacoes();
        $edificacao = $edificacoes->getEdificacaoByIdEscola($id);

        if ($edificacao) {
            $edificacao['idedificacao'] = $edificacao['id'];
            unset($edificacao['id']);
            unset($edificacao['idendereco']);
            unset($edificacao['excluido']);
            unset($edificacao['status']);
            unset($edificacao['logdata']);
            unset($edificacao['logusuario']);

            $this->view->post_var = array_merge($this->view->post_var, $edificacao);
        }

        $informacoestecnicas = new Edificacoes_Informacoestecnicas();
        $informacao = $informacoestecnicas->getInformacaoByIdEscola($id);

        if ($informacao) {
            unset($informacao['id']);
            unset($informacao['excluido']);
            unset($informacao['status']);
            unset($informacao['logdata']);
            unset($informacao['logusuario']);
            $informacao['areaconstruidainformacoes'] = $informacao['areaconstruida'];
            unset($informacao['idtipo']);
            $this->view->post_var = array_merge($this->view->post_var, $informacao);
        }

        $infraestruturasdeficiencias = new Edificacoes_Infraestruturadeficiencia();
        $infraestruturadeficiencia = $infraestruturasdeficiencias->getInfraestruturaByIdEscola($id);

        if ($infraestruturadeficiencia) {
            unset($infraestruturadeficiencia['id']);
            unset($infraestruturadeficiencia['excluido']);
            unset($infraestruturadeficiencia['status']);
            unset($infraestruturadeficiencia['logdata']);
            unset($infraestruturadeficiencia['logusuario']);
            $infraestruturadeficiencia['observacoesinstalacoes'] = $infraestruturadeficiencia['observacoes'];
            unset($infraestruturadeficiencia['observacoes']);
            $infraestruturadeficiencia['saidasemergenciadeficiencia'] = $infraestruturadeficiencia['saidasemergencia'];
            unset($infraestruturadeficiencia['idtipo']);
            $this->view->post_var = array_merge($this->view->post_var, $infraestruturadeficiencia);
        }
        //var_dump($this->view->post_var['latitude']); die();
        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($escola);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Escola editada com sucesso.";

            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        }
        return true;
    }

    /**
     *
     * Action de adição de escolas
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolas', 'name' => 'Escolas'),
            array('url' => null, 'name' => 'Adicionar escola')
        );

        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Escola adicionada com sucesso.";

            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        }
        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idescola
     */
    private function preForm($idescola = 0) {
        $secretarias = new Secretarias();
        $this->view->secretarias = $secretarias->getSecretarias();

        $tipos = new Estabelecimentos_Escolasestruturastipos();

        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Estabelecimentos_Escolasestruturastipos::$_ORIGEM_ABASTECIMENTO_ESGOTO;
        $this->view->tiposredesesgoto = $tipos->getEscolasestruturastipos($queries);

        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Estabelecimentos_Escolasestruturastipos::$_ORIGEM_ABASTECIMENTO_TUBULACAO_ESGOTO;
        $this->view->tipostibulacoesesgoto = $tipos->getEscolasestruturastipos($queries);

        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Estabelecimentos_Escolasestruturastipos::$_ORIGEM_ABASTECIMENTO_REDE_AGUA;
        $this->view->tiposredesaguas = $tipos->getEscolasestruturastipos($queries);

        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Estabelecimentos_Escolasestruturastipos::$_ORIGEM_ABASTECIMENTO_TIPO_TERRENO;
        $this->view->tiposterrenos = $tipos->getEscolasestruturastipos($queries);

        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Estabelecimentos_Escolasestruturastipos::$_ORIGEM_ABASTECIMENTO_DELIMITACAO;
        $this->view->tiposdelimitacoes = $tipos->getEscolasestruturastipos($queries);
        //tiposredesesgoto
        /*
          $queries = array();
          $queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
          $queries['origem'] = Estabelecimentos_Escolasestruturastipos::$_ORIGEM_PERFIL_LOCALIZACAO_UNIDADE;
          $this->view->tiposinstalacoeslocalizacao = $tipos->getTipos($queries); */

        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Estabelecimentos_Escolasestruturastipos::$_ORIGEM_PERFIL_LOCALIZACAO_REDES_AGUA;
        $this->view->tiposinstalacoesagua = $tipos->getEscolasestruturastipos($queries);

        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Estabelecimentos_Escolasestruturastipos::$_ORIGEM_PERFIL_LOCALIZACAO_REDES_ESGOTO;
        $this->view->tiposinstalacoesesgotos = $tipos->getEscolasestruturastipos($queries);

        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Estabelecimentos_Escolasestruturastipos::$_ORIGEM_PERFIL_LOCALIZACAO_REDES_ELETRICA;
        $this->view->tiposinstalacoeseletricas = $tipos->getEscolasestruturastipos($queries);
        /*
          $queries = array();
          $queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
          $queries['origem'] = Estabelecimentos_Escolasestruturastipos::$_ORIGEM_PERFIL_LOCALIZACAO_DOCUMENTO;
          $this->view->tiposdocumentacoes = $tipos->getTipos($queries); */

        $tiposedificacoes = new Edificacoes_Tipos();
        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Edificacoes_Tipos::$_ORIGEM_EDIFICACOES;
        $this->view->tiposedificacoes = $tiposedificacoes->getTipos($queries);

        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Edificacoes_Tipos::$_ORIGEM_ESTRUTURA_COBERTURA;
        $this->view->tiposcoberturas = $tiposedificacoes->getTipos($queries);

        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['origem'] = Edificacoes_Tipos::$_ORIGEM_CONSTRUCAO;
        $this->view->tiposconstrucoes = $tiposedificacoes->getTipos($queries);

        $tiposensinos = new Escolastiposensinos();
        $queries = array();
        $queries['status'] = 'Ativo';
        $this->view->tiposensinos = $tiposensinos->getTiposensinos($queries);

        $identificadores = new Gdae_Identificadoresescolas();
        $this->view->identificadores = $identificadores->getIdentificadores(array('status' => 'Ativo'));
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_registro = false) {
        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $idendereco = strip_tags((int) $this->getRequest()->getPost("idendereco"));
        $idsecretaria = strip_tags((int) $this->getRequest()->getPost("idsecretaria"));

        $idsensinostipos = $this->getRequest()->getPost("idsensinostipos");
        if (!is_array($idsensinostipos))
            $idsensinostipos = array();
        $idsensinostipos = implode(',', $idsensinostipos);

        $idsensinossubtipos = $this->getRequest()->getPost("idsensinossubtipos");
        if (!is_array($idsensinossubtipos))
            $idsensinossubtipos = array();
        $idsensinossubtipos = implode(',', $idsensinossubtipos);

        $idsatividadesaee = $this->getRequest()->getPost("idsatividadesaee");
        if (!is_array($idsatividadesaee))
            $idsatividadesaee = array();
        $idsatividadesaee = implode(',', $idsatividadesaee);

        $idsmaterias = $this->getRequest()->getPost("idsmaterias");
        if (!is_array($idsmaterias))
            $idsmaterias = array();
        $idsmaterias = implode(',', $idsmaterias);
        //
        $escola = strip_tags(trim($this->getRequest()->getPost("escola")));
        $idescola = strip_tags(trim($this->getRequest()->getPost("idescola")));
        $telefone = strip_tags(trim($this->getRequest()->getPost("telefone")));

        $tipozoneamento = strip_tags(trim($this->getRequest()->getPost("tipozoneamento")));
        $tipoimovel = strip_tags(trim($this->getRequest()->getPost("tipoimovel")));

        $endereco = strip_tags(trim($this->getRequest()->getPost("endereco")));
        $cep = strip_tags(trim($this->getRequest()->getPost("cep")));
        $numero = strip_tags(trim($this->getRequest()->getPost("numero")));
        $complemento = strip_tags(trim($this->getRequest()->getPost("complemento")));
        $idestado = strip_tags(trim($this->getRequest()->getPost("idestado")));
        $bairro = strip_tags(trim($this->getRequest()->getPost("bairro")));
        $cidade = strip_tags(trim($this->getRequest()->getPost("cidade")));

        $status = strip_tags(trim($this->getRequest()->getPost("status")));

        /* CAMPOS DOS HELPERS DE TIPO */
        $idtiporede = strip_tags((int) $this->getRequest()->getPost("idtiporede"));
        $idtipotubulacao = strip_tags((int) $this->getRequest()->getPost("idtipotubulacao"));
        $caixaresiduos = strip_tags($this->getRequest()->getPost("caixaresiduos"));
        $tratamentoresiduos = strip_tags($this->getRequest()->getPost("tratamentoresiduos"));
        $caixagorduras = strip_tags($this->getRequest()->getPost("caixagorduras"));


        /* REDE AGUA */
        $idtiporedeagua = strip_tags((int) $this->getRequest()->getPost("idtiporedeagua"));
        $origem = strip_tags($this->getRequest()->getPost("origem"));
        $reservatorio = strip_tags($this->getRequest()->getPost("reservatorio"));
        $capacidade = strip_tags($this->getRequest()->getPost("capacidade"));
        $energiasolar = strip_tags($this->getRequest()->getPost("energiasolar"));

        /* INFRAESTRUTURA */
        $banheirosdeficientesmasculinos = strip_tags((int) $this->getRequest()->getPost("banheirosdeficientesmasculinos"));
        $banheirosdeficientesfemininos = strip_tags((int) $this->getRequest()->getPost("banheirosdeficientesfemininos"));
        $pontosentradas = strip_tags((int) $this->getRequest()->getPost("pontosentradas"));
        $pontossaidas = strip_tags((int) $this->getRequest()->getPost("pontossaidas"));

        /* TERRENOS */
        $idtipoterreno = strip_tags((int) $this->getRequest()->getPost("idtipoterreno"));
        $idtipodelimitacao = strip_tags((int) $this->getRequest()->getPost("idtipodelimitacao"));
        $dimensao = strip_tags(Mn_Util::trataNum($this->getRequest()->getPost("dimensao")));
        $areaverde = strip_tags(Mn_Util::trataNum($this->getRequest()->getPost("areaverde")));
        $areaconstruida = strip_tags(Mn_Util::trataNum($this->getRequest()->getPost("areaconstruida")));
        $arealivre = strip_tags(Mn_Util::trataNum($this->getRequest()->getPost("arealivre")));
        $areatotal = strip_tags(Mn_Util::trataNum($this->getRequest()->getPost("areatotal")));
        $quantidade = strip_tags((int) $this->getRequest()->getPost("quantidade"));
        $avaliacaoresumida = strip_tags($this->getRequest()->getPost("avaliacaoresumida"));
        $confrontacao = strip_tags($this->getRequest()->getPost("confrontacao"));

        /* PERFIL DO TERRENO */
        $existencia = strip_tags($this->getRequest()->getPost("existencia"));
        $quantidadeperfil = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("quantidadeperfil")));
        $capacidademedia = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("capacidademedia")));
        $salasdeapoio = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("salasdeapoio")));
        $docentes = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("docentes")));
        $apoioadministrativo = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("apoioadministrativo")));
        $apoiotecnico = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("apoiotecnico")));
        $apoiooperacional = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("apoiooperacional")));
        $laboratoriosinformatica = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("laboratoriosinformatica")));
        $laboratoriosfisica = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("laboratoriosfisica")));
        $laboratoriosquimica = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("laboratoriosquimica")));
        $laboratoriosapoio = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("laboratoriosapoio")));
        $bibliotecas = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("bibliotecas")));
        $salasleitura = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("salasleitura")));
        $estacionamentos = strip_tags((int) Mn_Util::trataNum($this->getRequest()->getPost("estacionamentos")));

        /* ESPORTIVO */
        $existenciaesportivo = strip_tags($this->getRequest()->getPost("existenciaesportivo"));
        $quantidadeesportivo = strip_tags((int) $this->getRequest()->getPost("quantidadeesportivo"));
        $capacidademediaesportivo = strip_tags((int) $this->getRequest()->getPost("capacidademediaesportivo"));
        $quadrasabertas = strip_tags((int) $this->getRequest()->getPost("quadrasabertas"));
        $quadrasfechadas = strip_tags((int) $this->getRequest()->getPost("quadrasfechadas"));
        $piscinaspadroes = strip_tags((int) $this->getRequest()->getPost("piscinaspadroes"));
        $piscinassemiolimpicas = strip_tags((int) $this->getRequest()->getPost("piscinassemiolimpicas"));
        $piscinasolimipicas = strip_tags((int) $this->getRequest()->getPost("piscinasolimipicas"));
        $saltoornamental = strip_tags((int) $this->getRequest()->getPost("saltoornamental"));
        $camposdefutebol = strip_tags((int) $this->getRequest()->getPost("camposdefutebol"));
        $pistasatletismo = strip_tags((int) $this->getRequest()->getPost("pistasatletismo"));

        /* INSTALACOES */
        $idtipolocalizacao = strip_tags((int) $this->getRequest()->getPost("idtipolocalizacao"));
        $idtiporedes = strip_tags((int) $this->getRequest()->getPost("idtiporedes"));
        $idtipoesgoto = strip_tags((int) $this->getRequest()->getPost("idtipoesgoto"));
        $idtipoeletrica = strip_tags((int) $this->getRequest()->getPost("idtipoeletrica"));
        $anexos = strip_tags((int) $this->getRequest()->getPost("anexos"));
        $compartilhados = strip_tags($this->getRequest()->getPost("compartilhados"));
        $instalacoesalunosespeciais = strip_tags((int) $this->getRequest()->getPost("instalacoesalunosespeciais"));

        /* DOCUMENTACOES */
        $idtipodocumentacao = strip_tags((int) $this->getRequest()->getPost("idtipodocumentacao"));
        $numerodocumento = strip_tags((int) $this->getRequest()->getPost("numerodocumento"));
        $localregistro = strip_tags((int) $this->getRequest()->getPost("localregistro"));
        $dataregistro = strip_tags($this->getRequest()->getPost("dataregistro"));
        $dataregistro = Mn_Util::stringToTime($dataregistro);
        $dataregistro = date('Y-m-d G:i:s', $dataregistro);

        $valorpatrimonial = strip_tags(Mn_Util::trataNum($this->getRequest()->getPost("valorpatrimonial")));

        /* SERVICOS */
        $coletalixo = strip_tags($this->getRequest()->getPost("coletalixo"));
        $transporteescolar = strip_tags($this->getRequest()->getPost("transporteescolar"));
        $transportefuncional = strip_tags($this->getRequest()->getPost("transportefuncional"));
        $servicosclinicos = strip_tags($this->getRequest()->getPost("servicosclinicos"));
        $servicosodontologicos = strip_tags($this->getRequest()->getPost("servicosodontologicos"));
        $atividadescomunitarias = strip_tags($this->getRequest()->getPost("atividadescomunitarias"));

        /* MULTIPLOS ARQUIVOS */
        $idsarquivos = $this->getRequest()->getPost("idsarquivos");
        $legendasarquivos = $this->getRequest()->getPost("legendasarquivos");

        /* EDIFICAÇÕES */
        $idtipoedificacao = strip_tags(trim($this->getRequest()->getPost("idtipoedificacao")));
        $descricoesedificacoes = strip_tags(trim($this->getRequest()->getPost("descricoesedificacoes")));
        $dataconstrucao = strip_tags(trim($this->getRequest()->getPost("dataconstrucao")));
        $dataconstrucao = Mn_Util::stringToTime($dataconstrucao);
        $dataconstrucao = date('Y-m-d G:i:s', $dataconstrucao);
        $localizacao = strip_tags(trim($this->getRequest()->getPost("localizacao")));
        $referencia = strip_tags(trim($this->getRequest()->getPost("referencia")));

        /* EDIFICACOES INFORMACOES TECNICAS */
        $idcobertura = strip_tags(trim($this->getRequest()->getPost("idcobertura")));
        $idtipoconstrucao = strip_tags(trim($this->getRequest()->getPost("idtipoconstrucao")));
        $pavimentos = strip_tags(trim($this->getRequest()->getPost("pavimentos")));
        $areaconstruidainformacoes = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("areaconstruidainformacoes"))));
        $fracoes = strip_tags(trim($this->getRequest()->getPost("fracoes")));
        $pontosacessos = strip_tags(trim($this->getRequest()->getPost("pontosacessos")));
        $saidasemergencia = strip_tags(trim($this->getRequest()->getPost("saidasemergencia")));

        /* EDIFICACOES DEFICIENTES */
        $sistemaseguranca = strip_tags(trim($this->getRequest()->getPost("sistemaseguranca")));
        $saidasemergenciadeficiencia = strip_tags(trim($this->getRequest()->getPost("saidasemergenciadeficiencia")));
        $redeeletrica = strip_tags(trim($this->getRequest()->getPost("redeeletrica")));
        $manutencoesperiodicas = strip_tags(trim($this->getRequest()->getPost("manutencoesperiodicas")));

        $acessosdeficiente = strip_tags(trim($this->getRequest()->getPost("acessosdeficiente")));
        $observacoesinstalacoes = strip_tags(trim($this->getRequest()->getPost("observacoesinstalacoes")));
        $elevador = strip_tags(trim($this->getRequest()->getPost("elevador")));

        $idmultiplas_imagens = $this->getRequest()->getPost("idmultiplas_imagens");
        $legendas_multiplas_imagens = $this->getRequest()->getPost("legendas_multiplas_imagens");
        $quantidadesaulas = (int) trim($this->getRequest()->getPost("quantidadesaulas"));
        $diassemana = trim($this->getRequest()->getPost("diassemana"));
        /* LATITUDE */
        $latitude = trim($this->getRequest()->getPost("latitude"));
        $longitude = trim($this->getRequest()->getPost("longitude"));

        $nomeabreviado = trim($this->getRequest()->getPost("nomeabreviado"));
        $codsituacao = trim($this->getRequest()->getPost("codsituacao"));
        $codredeensino = trim($this->getRequest()->getPost("codredeensino"));
        $codidentificador = trim($this->getRequest()->getPost("codidentificador"));

        /* Gestor Escolar */
        $gestorescolarnome = trim($this->getRequest()->getPost("gestorescolarnome"));
        $gestorescolarcpf = trim($this->getRequest()->getPost("gestorescolarcpf"));
        $gestorescolarcargo = trim($this->getRequest()->getPost("gestorescolarcargo"));
        $gestorescolaremail = trim($this->getRequest()->getPost("gestorescolaremail"));

        /* Dias da Semana */
        $segunda = trim($this->getRequest()->getPost("segunda"));
        $terca = trim($this->getRequest()->getPost("terca"));
        $quarta = trim($this->getRequest()->getPost("quarta"));
        $quinta = trim($this->getRequest()->getPost("quinta"));
        $sexta = trim($this->getRequest()->getPost("sexta"));
        $sabado = trim($this->getRequest()->getPost("sabado"));
        $domingo = trim($this->getRequest()->getPost("domingo"));

        $erros = array();


        $temporario = explode(',', $idsensinossubtipos);

        if (in_array(105, $temporario) AND empty($idsatividadesaee)) {
            array_push($erros, 'Preencha pelo menos uma atividade na Educação Especial');
        }

        if ("" == $escola)
            array_push($erros, 'Preencha o campo ESCOLA.');
        if ("" == $telefone)
            array_push($erros, 'Preencha o campo TELEFONE.');
        if ("" == $endereco)
            array_push($erros, 'Preencha o campo ENDERECO.');
        if ("" == $cep)
            array_push($erros, 'Preencha o campo CEP.');
        if ("" == $numero)
            array_push($erros, 'Preencha o campo NUMERO.');
        if ("" == $bairro)
            array_push($erros, 'Preencha o campo BAIRRO.');
        if ("" == $idestado)
            array_push($erros, 'Selecione um ESTADO.');
        if ("" == $cidade)
            array_push($erros, 'Preencha o campo CIDADE.');
        if ("" == $status)
            array_push($erros, 'Selecione um STATUS.');
        if ("" == $gestorescolarnome)
            array_push($erros, 'Preencha o campo Nome do Gestor Escolar.');
        if ("" == $gestorescolarcpf)
            array_push($erros, 'Preencha o campo CPF do Gestor Escolar.');
        if ("" == $gestorescolarcargo)
            array_push($erros, 'Preencha o campo Cargo do Gestor Escolar.');

        $escolas = new Escolas();
        //$row = $escolas->fetchRow("excluido='nao' AND escola='$escola' AND id<>".$id);
        //if ($row) array_push($erros, 'Já existe uma ESCOLA com esse NOME.');

        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {

            /* $idendereco = ($_registro) ? (int)$_registro['idendereco'] : 0;
              $enderecosModel = new EscolasenderecosModel($this);
              $endereco = $enderecosModel->getPost($idendereco);
              $idendereco = ($endereco) ? $endereco['id'] : 0; */

            $dados = array();
            $dados['id'] = $id;
            $dados['idendereco'] = (int) $idendereco;
            $dados['idsecretaria'] = (int) $idsecretaria;
            $dados['idsensinostipos'] = $idsensinostipos;
            $dados['idsensinossubtipos'] = $idsensinossubtipos;
            $dados['idsatividadesaee'] = $idsatividadesaee;
            $dados['idsmaterias'] = $idsmaterias;
            $dados['escola'] = $escola;
            $dados['telefone'] = $telefone;
            $dados['tipozoneamento'] = $tipozoneamento;
            $dados['tipoimovel'] = $tipoimovel;
            $dados['latitude'] = $latitude;
            $dados['longitude'] = $longitude;
            $dados["diassemana"] = $diassemana;
            $dados["quantidadesaulas"] = $quantidadesaulas;
            $dados['nomeabreviado'] = $nomeabreviado;
            $dados['codsituacao'] = $codsituacao;
            $dados['codredeensino'] = $codredeensino;
            $dados['codidentificador'] = (int) $codidentificador;

            $dados['status'] = $status;
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = Usuarios::getUsuario('id');
            $dados['logdata'] = date('Y-m-d G:i:s');

            $dados['gestorescolarnome'] = $gestorescolarnome;
            $dados['gestorescolarcpf'] = $gestorescolarcpf;
            $dados['gestorescolarcargo'] = $gestorescolarcargo;
            $dados['gestorescolaremail'] = $gestorescolaremail;

            $dados['segunda'] = $segunda;
            $dados['terca'] = $terca;
            $dados['quarta'] = $quarta;
            $dados['quinta'] = $quinta;
            $dados['sexta'] = $sexta;
            $dados['sabado'] = $sabado;
            $dados['domingo'] = $domingo;

            $dados_endereco = array();
            $dados_endereco['idescola'] = $idescola;
            $dados_endereco['cep'] = $cep;
            $dados_endereco['endereco'] = $endereco;
            $dados_endereco['numero'] = $numero;
            $dados_endereco['complemento'] = $complemento;
            $dados_endereco['bairro'] = $bairro;
            $dados_endereco['cidade'] = $cidade;
            $dados_endereco['idestado'] = $idestado;
            $dados_endereco['datacriacao'] = date('Y-m-d');
            $dados_endereco['excluido'] = 'nao';
            $dados_endereco['logusuario'] = Usuarios::getUsuario('id');
            $dados_endereco['logdata'] = date('Y-m-d G:i:s');
            $dados['endereco'] = $dados_endereco;

            $dados_tipo_esgoto = array();
            $dados_tipo_esgoto['idtiporede'] = (int) $idtiporede;
            $dados_tipo_esgoto['idtipotubulacao'] = (int) $idtipotubulacao;
            $dados_tipo_esgoto['caixaresiduos'] = $caixaresiduos;
            $dados_tipo_esgoto['tratamentoresiduos'] = $tratamentoresiduos;
            $dados_tipo_esgoto['caixagorduras'] = $caixagorduras;
            $dados_tipo_esgoto['status'] = 'Ativo';
            $dados_tipo_esgoto['excluido'] = 'nao';
            $dados_tipo_esgoto['logusuario'] = Usuarios::getUsuario('id');
            $dados_tipo_esgoto['logdata'] = date('Y-m-d G:i:s');
            $dados['tiposesgotos'] = $dados_tipo_esgoto;

            $dados_tipo_redeagua = array();
            $dados_tipo_redeagua['idtipo'] = (int) $idtiporedeagua;
            $dados_tipo_redeagua['origem'] = $origem;
            $dados_tipo_redeagua['reservatorio'] = $reservatorio;
            $dados_tipo_redeagua['capacidade'] = $capacidade;
            $dados_tipo_redeagua['energiasolar'] = $energiasolar;
            $dados_tipo_redeagua['status'] = 'Ativo';
            $dados_tipo_redeagua['excluido'] = 'nao';
            $dados_tipo_redeagua['logusuario'] = Usuarios::getUsuario('id');
            $dados_tipo_redeagua['logdata'] = date('Y-m-d G:i:s');
            $dados['redeagua'] = $dados_tipo_redeagua;

            $dados_tipo_infraestrutura = array();
            $dados_tipo_infraestrutura['banheirosdeficientesmasculinos'] = $banheirosdeficientesmasculinos;
            $dados_tipo_infraestrutura['banheirosdeficientesfemininos'] = $banheirosdeficientesfemininos;
            $dados_tipo_infraestrutura['pontosentradas'] = $pontosentradas;
            $dados_tipo_infraestrutura['pontossaidas'] = $pontossaidas;
            $dados_tipo_infraestrutura['status'] = 'Ativo';
            $dados_tipo_infraestrutura['excluido'] = 'nao';
            $dados_tipo_infraestrutura['logusuario'] = Usuarios::getUsuario('id');
            $dados_tipo_infraestrutura['logdata'] = date('Y-m-d G:i:s');
            $dados['infraestrutura'] = $dados_tipo_infraestrutura;

            $dados_tipo_terrenos = array();
            $dados_tipo_terrenos['idtipoterreno'] = (int) $idtipoterreno;
            $dados_tipo_terrenos['idtipodelimitacao'] = (int) $idtipodelimitacao;
            $dados_tipo_terrenos['dimensao'] = $dimensao;
            $dados_tipo_terrenos['areaverde'] = $areaverde;
            $dados_tipo_terrenos['areaconstruida'] = $areaconstruida;
            $dados_tipo_terrenos['arealivre'] = $arealivre;
            $dados_tipo_terrenos['areatotal'] = $areatotal;
            $dados_tipo_terrenos['quantidade'] = $quantidade;
            $dados_tipo_terrenos['avaliacaoresumida'] = $avaliacaoresumida;
            $dados_tipo_terrenos['confrontacao'] = $confrontacao;
            $dados_tipo_terrenos['status'] = 'Ativo';
            $dados_tipo_terrenos['excluido'] = 'nao';
            $dados_tipo_terrenos['logusuario'] = Usuarios::getUsuario('id');
            $dados_tipo_terrenos['logdata'] = date('Y-m-d G:i:s');
            $dados['terrenos'] = $dados_tipo_terrenos;


            $dados_perfis_terrenos = array();
            $dados_perfis_terrenos['existencia'] = $existencia;
            $dados_perfis_terrenos['quantidade'] = (int) $quantidadeperfil;
            $dados_perfis_terrenos['capacidademedia'] = $capacidademedia;
            $dados_perfis_terrenos['salasdeapoio'] = $salasdeapoio;
            $dados_perfis_terrenos['docentes'] = $docentes;
            $dados_perfis_terrenos['apoioadministrativo'] = $apoioadministrativo;
            $dados_perfis_terrenos['apoiotecnico'] = $apoiotecnico;
            $dados_perfis_terrenos['apoiooperacional'] = $apoiooperacional;
            $dados_perfis_terrenos['laboratoriosinformatica'] = $laboratoriosinformatica;
            $dados_perfis_terrenos['laboratoriosfisica'] = $laboratoriosfisica;
            $dados_perfis_terrenos['laboratoriosquimica'] = $laboratoriosquimica;
            $dados_perfis_terrenos['laboratoriosapoio'] = $laboratoriosapoio;
            $dados_perfis_terrenos['bibliotecas'] = $bibliotecas;
            $dados_perfis_terrenos['salasleitura'] = $salasleitura;
            $dados_perfis_terrenos['estacionamentos'] = $estacionamentos;
            $dados_perfis_terrenos['status'] = 'Ativo';
            $dados_perfis_terrenos['excluido'] = 'nao';
            $dados_perfis_terrenos['logusuario'] = Usuarios::getUsuario('id');
            $dados_perfis_terrenos['logdata'] = date('Y-m-d G:i:s');
            $dados['perfistecnicos'] = $dados_perfis_terrenos;

            $dados_esportivos = array();
            $dados_esportivos['existencia'] = $existenciaesportivo;
            $dados_esportivos['quantidade'] = (int) $quantidadeesportivo;
            $dados_esportivos['capacidademedia'] = $capacidademediaesportivo;
            $dados_esportivos['quadrasabertas'] = $quadrasabertas;
            $dados_esportivos['quadrasfechadas'] = $quadrasfechadas;
            $dados_esportivos['piscinaspadroes'] = $piscinaspadroes;
            $dados_esportivos['piscinassemiolimpicas'] = $piscinassemiolimpicas;
            $dados_esportivos['piscinasolimipicas'] = $piscinasolimipicas;
            $dados_esportivos['saltoornamental'] = $saltoornamental;
            $dados_esportivos['camposdefutebol'] = $camposdefutebol;
            $dados_esportivos['pistasatletismo'] = $pistasatletismo;
            $dados_esportivos['status'] = 'Ativo';
            $dados_esportivos['excluido'] = 'nao';
            $dados_esportivos['logusuario'] = Usuarios::getUsuario('id');
            $dados_esportivos['logdata'] = date('Y-m-d G:i:s');
            $dados['esportivos'] = $dados_esportivos;

            $dados_instalacoes = array();
            $dados_instalacoes['idtipolocalizacao'] = (int) $idtipolocalizacao;
            $dados_instalacoes['idtiporedes'] = (int) $idtiporedes;
            $dados_instalacoes['idtipoesgoto'] = (int) $idtipoesgoto;
            $dados_instalacoes['idtipoeletrica'] = (int) $idtipoeletrica;
            $dados_instalacoes['anexos'] = $anexos;
            $dados_instalacoes['compartilhados'] = $compartilhados;
            $dados_instalacoes['instalacoesalunosespeciais'] = $instalacoesalunosespeciais;
            $dados_instalacoes['status'] = 'Ativo';
            $dados_instalacoes['excluido'] = 'nao';
            $dados_instalacoes['logusuario'] = Usuarios::getUsuario('id');
            $dados_instalacoes['logdata'] = date('Y-m-d G:i:s');
            $dados['instalacoes'] = $dados_instalacoes;

            $dados_documentacoes = array();
            $dados_documentacoes['idtipo'] = (int) $idtipodocumentacao;
            $dados_documentacoes['numerodocumento'] = $numerodocumento;
            $dados_documentacoes['localregistro'] = $localregistro;
            $dados_documentacoes['dataregistro'] = $dataregistro;
            $dados_documentacoes['valorpatrimonial'] = $valorpatrimonial;
            $dados_documentacoes['status'] = 'Ativo';
            $dados_documentacoes['excluido'] = 'nao';
            $dados_documentacoes['logusuario'] = Usuarios::getUsuario('id');
            $dados_documentacoes['logdata'] = date('Y-m-d G:i:s');
            $dados['documentacoes'] = $dados_documentacoes;


            $dados_servicos = array();
            $dados_servicos['coletalixo'] = $coletalixo;
            $dados_servicos['transporteescolar'] = $transporteescolar;
            $dados_servicos['transportefuncional'] = $transportefuncional;
            $dados_servicos['servicosclinicos'] = $servicosclinicos;
            $dados_servicos['servicosodontologicos'] = $servicosodontologicos;
            $dados_servicos['atividadescomunitarias'] = $atividadescomunitarias;
            $dados_servicos['status'] = 'Ativo';
            $dados_servicos['excluido'] = 'nao';
            $dados_servicos['logusuario'] = Usuarios::getUsuario('id');
            $dados_servicos['logdata'] = date('Y-m-d G:i:s');
            $dados['servicos'] = $dados_servicos;


            $dados_edificacoes = array();
            $dados_edificacoes['idtipoedificacao'] = (int) $idtipoedificacao;
            $dados_edificacoes['idendereco'] = (int) $idendereco;
            $dados_edificacoes['descricoes'] = $descricoesedificacoes;
            $dados_edificacoes['dataconstrucao'] = $dataconstrucao;
            $dados_edificacoes['localizacao'] = $localizacao;
            $dados_edificacoes['referencia'] = $referencia;

            $dados_edificacoes['idsarquivos'] = $idsarquivos;
            $dados_edificacoes['legendasarquivos'] = $legendasarquivos;

            $dados_edificacoes['status'] = 'Ativo';
            $dados_edificacoes['excluido'] = 'nao';
            $dados_edificacoes['logusuario'] = Usuarios::getUsuario('id');
            $dados_edificacoes['logdata'] = date('Y-m-d G:i:s');
            $dados['edificacoes'] = $dados_edificacoes;

            $dados_edificacoes_informacoes = array();
            $dados_edificacoes_informacoes['idcobertura'] = (int) $idcobertura;
            $dados_edificacoes_informacoes['idtipoconstrucao'] = (int) $idtipoconstrucao;
            $dados_edificacoes_informacoes['pavimentos'] = $pavimentos;
            $dados_edificacoes_informacoes['areaconstruida'] = $areaconstruidainformacoes;
            $dados_edificacoes_informacoes['fracoes'] = $fracoes;
            $dados_edificacoes_informacoes['pontosacessos'] = $pontosacessos;
            $dados_edificacoes_informacoes['saidasemergencia'] = $saidasemergencia;
            $dados_edificacoes_informacoes['status'] = 'Ativo';
            $dados_edificacoes_informacoes['excluido'] = 'nao';
            $dados_edificacoes_informacoes['logusuario'] = Usuarios::getUsuario('id');
            $dados_edificacoes_informacoes['logdata'] = date('Y-m-d G:i:s');
            $dados['edificacoesinformacoes'] = $dados_edificacoes_informacoes;

            $dados_edificacoes_deficiencientes = array();
            $dados_edificacoes_deficiencientes['sistemaseguranca'] = $sistemaseguranca;
            $dados_edificacoes_deficiencientes['saidasemergencia'] = $saidasemergenciadeficiencia;
            $dados_edificacoes_deficiencientes['redeeletrica'] = $redeeletrica;
            $dados_edificacoes_deficiencientes['manutencoesperiodicas'] = $manutencoesperiodicas;

            $dados_edificacoes_deficiencientes['acessosdeficiente'] = $acessosdeficiente;
            $dados_edificacoes_deficiencientes['observacoes'] = $observacoesinstalacoes;
            $dados_edificacoes_deficiencientes['elevador'] = $elevador;
            $dados_edificacoes_deficiencientes['status'] = 'Ativo';
            $dados_edificacoes_deficiencientes['excluido'] = 'nao';
            $dados_edificacoes_deficiencientes['logusuario'] = Usuarios::getUsuario('id');
            $dados_edificacoes_deficiencientes['logdata'] = date('Y-m-d G:i:s');
            $dados['deficiencientesinformacoes'] = $dados_edificacoes_deficiencientes;

            $row = $escolas->save($dados);
            $dados_lt_lg = $this->getLatitudeLongitude($idendereco, $row['id']);


            $multiplasimagens = new Escolasimagens();
            $multiplasimagens->setImagens($row['id'], $idmultiplas_imagens, $legendas_multiplas_imagens);
            $multiplosarquivos = new Escolasedificacoesarquivos();
            $multiplosarquivos->setArquivos($row['id'], $idsarquivos, $legendasarquivos);

            $controlealunosmaterias = new Controlealunosmaterias();
            $_idsmaterias = explode(',', $idsmaterias);
            foreach ($_idsmaterias as $im => $materia) {
                if ((isset($materia)) && ((int) $materia > 0)) {
                    $_materia = Escolasmaterias::getEscolasmateriaByIdHelper($materia);
                    if (!isset($_materia['id']))
                        continue;
                    $_idmateria = 0;
                    $_controlemateria = Controlealunosmaterias::getControlealunosmateriasHelper(array('idescola' => $row['id'], 'idmateria' => $materia));
                    if (sizeof($_controlemateria) > 0) {
                        $_controlemateria_achado = $_controlemateria[0];
                        if (isset($_controlemateria_achado['id'])) {
                            $_idmateria = $_controlemateria_achado['id'];
                        }
                    }

                    $dadosmateria = array();
                    $dadosmateria['id'] = (int) $_idmateria;
                    $dadosmateria["idescola"] = (int) $row['id'];
                    $dadosmateria["materia"] = $_materia['materia'];
                    $dadosmateria["descricoes"] = $_materia['descricoes'];
                    $dadosmateria["status"] = 'Ativo';
                    $dadosmateria['excluido'] = 'nao';
                    $dadosmateria['logusuario'] = $this->_usuario['id'];
                    ;
                    $dadosmateria['logdata'] = date('Y-m-d G:i:s');
//						var_dump($dadosmateria); die();
                    $row = $controlealunosmaterias->save($dadosmateria);
                }
            }

            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die();
        }





        return "";
    }

    private function getLatitudeLongitude($idendereco, $idescola) {
        if ((int) $idendereco <= 0)
            return false;
        $enderecos = new Enderecos();
        $row = Enderecos::getEnderecoById($idendereco);
        if (!$row)
            return false;

        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($idescola);
        if (!$escola)
            return false;

        //$endereco = utf8_decode(str_replace(" ", "+", $row['endereco']));
        $endereco = str_replace(" ", "+", $row['endereco']);

        $address = $endereco . ',' . $row['numero'] . ',' . $row['cidade'] . ',' . $row['uf'];
        $address = str_replace(" ", "+", $address);
        //$address = urldecode($address);
        //$address = strtolower($address);
        //$address = 'http://maps.google.com/maps/api/geocode/json?address=rua+graciliano+ramos,197,aguas+de+lindoia,SP,brasil&sensor=false';
        //$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=R.+%C3%89rico+Ver%C3%ADssimo,125,Hortol%C3%A2ndia,SP,brasil&sensor=false');
        $geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address=' . $address . ',brasil&sensor=false');

        $output = json_decode($geocode);

        $lat = $output->results[0]->geometry->location->lat;
        $long = $output->results[0]->geometry->location->lng;

        $row['latitude'] = $lat;
        $row['longitude'] = $long;
        //var_dump($lat); die();
        $enderecos->save($row);

        $escola['latitude'] = $lat;
        $escola['longitude'] = $long;

        $escolas->save($escola);
        return;
    }

    private function getImagem($apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();
        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm('imagem', NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }
        $excluir_imagem = trim($this->getRequest()->getPost("excluir_imagem"));
        if ($excluir_imagem == 'excluir_imagem')
            $idimagem = -1;
        return $idimagem;
    }

    public function fileuploadAction() {

        error_reporting(E_ALL | E_STRICT);

        $upload_handler = new UploadHandler();

        header('Pragma: no-cache');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Content-Disposition: inline; filename="files.json"');
        header('X-Content-Type-Options: nosniff');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, DELETE');
        header('Access-Control-Allow-Headers: X-File-Name, X-File-Type, X-File-Size');

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'OPTIONS':
                break;
            case 'HEAD':
            case 'GET':
                $upload_handler->get();
                break;
            case 'POST':
                if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
                    $upload_handler->delete();
                } else {
                    // $upload_handler->post();


                    $arquivos = new Arquivos();
                    $idarquivo = $arquivos->getArquivoFromFormsByPos('files', 0);

                    $result = array();
                    $result['idarquivo'] = $idarquivo;
                    $result['filename'] = $_FILES['files']["name"][0];
                    $result['idx'] = (int) $_POST['idx'];

                    echo '[' . json_encode($result) . ']';
                }
                break;
            case 'DELETE':
                $upload_handler->delete();
                break;
            default:
                header('HTTP/1.1 405 Method Not Allowed');
        }
        die();
    }

    public function fileuploadimagensAction() {

        error_reporting(E_ALL | E_STRICT);

        $upload_handler = new UploadHandler();

        header('Pragma: no-cache');
        header('Cache-Control: no-store, no-cache, must-revalidate');
        header('Content-Disposition: inline; filename="files.json"');
        header('X-Content-Type-Options: nosniff');
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, DELETE');
        header('Access-Control-Allow-Headers: X-File-Name, X-File-Type, X-File-Size');

        switch ($_SERVER['REQUEST_METHOD']) {
            case 'OPTIONS':
                break;
            case 'HEAD':
            case 'GET':
                $upload_handler->get();
                break;
            case 'POST':
                if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
                    $upload_handler->delete();
                } else {
                    // $upload_handler->post();


                    $imagens = new Imagens();
                    $idimagem = $imagens->getImagemFromFormsByPos('files', 0);

                    $result = array();
                    $result['idimagem'] = $idimagem;
                    $result['filename'] = $_FILES['files']["name"][0];
                    $result['idx'] = (int) $_POST['idx'];

                    echo '[' . json_encode($result) . ']';
                }
                break;
            case 'DELETE':
                $upload_handler->delete();
                break;
            default:
                header('HTTP/1.1 405 Method Not Allowed');
        }
        die();
    }

}
