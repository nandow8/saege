<?php

/**
 * Controle da classe ouvidoriareclamacoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_OuvidoriareclamacoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Ouvidoriareclamacao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("ouvidoriareclamacoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Ouvidoriareclamacoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Reclamação excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="ouvidoriareclamacoes") $objs = new Ouvidoriareclamacoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ouvidoriareclamacoes', 'name' => 'Ouvidoria Reclamações'),
			array('url' => null,'name' => 'Visualizar Reclamação')
		);
		
		$id = (int)$this->_request->getParam("id");
		$ouvidoriareclamacoes = new Ouvidoriareclamacoes();
		$ouvidoriareclamacao = $ouvidoriareclamacoes->getOuvidoriareclamacaoById($id, array());
		
		if (!$ouvidoriareclamacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $ouvidoriareclamacao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Ouvidoria Reclamações')
		);
		
		$ns = new Zend_Session_Namespace('default_ouvidoriareclamacoes');
		$ouvidoriareclamacoes = new Ouvidoriareclamacoes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
if ($this->view->post_var["reclamante"]!="") $queries["reclamante"] = $this->view->post_var["reclamante"];
if ($this->view->post_var["reclamado"]!="") $queries["reclamado"] = $this->view->post_var["reclamado"];
if ($this->view->post_var["reclamacaoassunto"]!="") $queries["reclamacaoassunto"] = $this->view->post_var["reclamacaoassunto"];
if ($this->view->post_var["posicao"]!="") $queries["posicao"] = $this->view->post_var["posicao"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $ouvidoriareclamacoes->getOuvidoriareclamacoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $ouvidoriareclamacoes->getOuvidoriareclamacoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de ouvidoriareclamacoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ouvidoriareclamacoes', 'name' => 'Ouvidoria Reclamações'),
			array('url' => null,'name' => 'Editar Reclamação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$ouvidoriareclamacoes = new Ouvidoriareclamacoes();
		$ouvidoriareclamacao = $ouvidoriareclamacoes->getOuvidoriareclamacaoById($id);
		
		if (!$ouvidoriareclamacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $ouvidoriareclamacao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($ouvidoriareclamacao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Reclamação editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de ouvidoriareclamacoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ouvidoriareclamacoes', 'name' => 'Ouvidoria Reclamações'),
			array('url' => null,'name' => 'Adicionar Reclamação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Reclamação adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idouvidoriareclamacao
     */    
    private function preForm($idouvidoriareclamacao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_ouvidoriareclamacao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
$tipo = trim($this->getRequest()->getPost("tipo"));
$datalancamento = trim($this->getRequest()->getPost("datalancamento"));
$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$tipocontato = trim($this->getRequest()->getPost("tipocontato"));
$reclamante = trim($this->getRequest()->getPost("reclamante"));
$telefone1 = trim($this->getRequest()->getPost("telefone1"));
$telefone2 = trim($this->getRequest()->getPost("telefone2"));
$contato = trim($this->getRequest()->getPost("contato"));
$reclamado = trim($this->getRequest()->getPost("reclamado"));
$idtipo = (int)trim($this->getRequest()->getPost("idtipo"));
$reclamacaoassunto = trim($this->getRequest()->getPost("reclamacaoassunto"));
$reclamacaodescricoes = trim($this->getRequest()->getPost("reclamacaodescricoes"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$posicao = trim($this->getRequest()->getPost("posicao"));
$encaminhar = trim($this->getRequest()->getPost("encaminhar"));
$status = trim($this->getRequest()->getPost("status1"));
$processoposicao = trim($this->getRequest()->getPost("processoposicao"));
$processoobservacoes = trim($this->getRequest()->getPost("processoobservacoes"));
		/*MULTIPLOS ARQUIVOS*/
		$idsarquivos = $this->getRequest()->getPost("idsarquivos");
		$legendasarquivos =  $this->getRequest()->getPost("legendasarquivos");	
		
		$erros = array();
		
		if (""==$sequencial) array_push($erros, "Informe a Seq..");
if (""==$tipo) array_push($erros, "Informe a Tipo.");
if (0==$idescola) array_push($erros, "Informe a Escolas.");
if (""==$reclamante) array_push($erros, "Informe a Reclamante.");
if (""==$telefone1) array_push($erros, "Informe a Telefone 1.");
if (""==$reclamado) array_push($erros, "Informe a Reclamado.");
if (0==$idtipo) array_push($erros, "Informe a Tipo de reclamação.");
if (""==$reclamacaoassunto) array_push($erros, "Informe a Assunto da reclamação.");
if (""==$posicao) array_push($erros, "Informe a Posição.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$ouvidoriareclamacoes = new Ouvidoriareclamacoes();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["sequencial"] = $sequencial;
$dados["tipo"] = $tipo;
$dados["datalancamento"] = date('Y-m-d G:i:s');
$dados["idusuario"] = $idusuario;
$dados["iddepartamento"] = $iddepartamento;
$dados["idescola"] = $idescola;
$dados["tipocontato"] = $tipocontato;
$dados["reclamante"] = $reclamante;
$dados["telefone1"] = $telefone1;
$dados["telefone2"] = $telefone2;
$dados["contato"] = $contato;
$dados["reclamado"] = $reclamado;
$dados["idtipo"] = $idtipo;
$dados["reclamacaoassunto"] = $reclamacaoassunto;
$dados["reclamacaodescricoes"] = $reclamacaodescricoes;
$dados["observacoes"] = $observacoes;
$dados["posicao"] = $posicao;
$dados["encaminhar"] = $encaminhar;
$dados["status"] = $status;
$dados["processoposicao"] = $processoposicao;
$dados["processoobservacoes"] = $processoobservacoes;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $ouvidoriareclamacoes->save($dados);
			
			$multiplosarquivos = new Ouvidoriareclamacoesarquivos(); 
			$multiplosarquivos->setArquivos($row['id'], $idsarquivos, $legendasarquivos);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}