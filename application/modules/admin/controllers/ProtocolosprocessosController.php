<?php

/**
 * Controle da classe protocolosprocessos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
// class MyTCPDF extends TCPDF {

//     //Page header
//     public function Header() {

//         $image_file = getcwd() . '/public/admin/imagens/logosantaisabel.jpg';
//         $this->Image($image_file, 18, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

//         //Set font
//         $this->SetFont('helvetica', 'B', 20);
//         //Title
//         //$this->Cell(0, 15, '<< TCPDF Example 003 São Paulo >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', 'B', 10);

//         // cabeçalho Endereço
//         $this->SetXY(35, 12);
//         $this->Cell(0, 0, 'Prefeitura Municipal de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 9);
//         $this->SetXY(35, 16);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Secretaria de Educação - Protocolo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 19);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 22);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetXY(35, 25);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//     }

//     // Page footer
//     public function Footer() {
//         // Position at 15 mm from bottom
//         $this->SetY(-15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

//         // Position at 15 mm from bottom
//         $this->SetXY(150, -15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
//     }

// }

class Admin_ProtocolosprocessosController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Protocolosprocesso
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("protocolosprocessos", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $perfis = new UsuariosPerfis();
        $perfil = $perfis->getPerfilById($this->_usuario['idperfil']);
        $isprotocolo = ((isset($perfil['id'])) && (($perfil['id'] == 39) || ($perfil['id'] == '26'))) ? true : false;
        $this->view->isprotocolo = $isprotocolo;

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }

        $protocolosprocessos = new Protocolosprocessos();
        $queries = array();
//        $ultimo = $protocolosprocessos->getUltimoProtocoloprocesso($queries);
//
//        $sequencial = '1/' . date('Y');
//        if (isset($ultimo['id'])) {
//            $sequencial = ($ultimo['id'] + 1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
//        }
//        $this->view->sequencial = $sequencial;
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Protocolosprocessos();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            Fjmx_Util::geraLog($row['logdata'], "Protocolo Processos", Usuarios::getUsuario('id'), "Exclusão do registro: $id realizada com sucesso.");
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Protocolo processos excluído com sucesso.";
            
            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "protocolosprocessos")
            $objs = new Protocolosprocessos();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'protocolosprocessos', 'name' => 'Protocolos Processos'),
            array('url' => null, 'name' => 'Visualizar Protocolo processos')
        );

        $id = (int) $this->_request->getParam("id");

        $protocolosprocessos = new Protocolosprocessos();
        $protocolosprocesso = $protocolosprocessos->getProtocoloprocessoById($id, array());

        if (!$protocolosprocesso)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $protocolosprocesso;
        $this->preForm();

        $this->view->visualizar = true;

        if ($this->view->post_var['posicao'] == 'Em Andamento') {
            $this->setHistoricoComoLido($id);
        }

        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Protocolos Processos')
        );

        $ns = new Zend_Session_Namespace('default_protocolosprocessos');
        $protocolosprocessos = new Protocolosprocessos();
        $queries = array();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ($this->view->post_var["sequencial"] != "")
                $queries["sequencial"] = $this->view->post_var["sequencial"];
            if ($this->view->post_var["datalancamento_i"] != "")
                $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
            if ($this->view->post_var["datalancamento_f"] != "")
                $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
            if ($this->view->post_var["titulo"] != "")
                $queries["titulo"] = $this->view->post_var["titulo"];
            if ($this->view->post_var["idorigem"] != "")
                $queries["idorigem"] = $this->view->post_var["idorigem"];
            if ($this->view->post_var["iddestino"] != "")
                $queries["iddestino"] = $this->view->post_var["iddestino"];
            if ($this->view->post_var["posicao"] != "")
                $queries["posicao"] = $this->view->post_var["posicao"];
            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $protocolosprocessos->getProtocolosprocessos($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $protocolosprocessos->getProtocolosprocessos($queries, $paginaAtual, $maxpp);
    }

    public function imprimirAction() {

        $id = (int) $this->_request->getParam("id");
        $tipo = (int) $this->_request->getParam("tipo");

        $processos = new Protocolosprocessos();
        //$tramitacoes = new Protocolosprocessostramitacoes();
        //$arquivos = new Protocolosprocessostramitacoesarquivos();

        $queries = array();

        if ((int) $id > 0) {
            $queries['id'] = $id;

            $docProtocolos = $processos->getProtocoloprocessoById($id);
            //$docTramitacoes = $tramitacoes->getProtocoloprocessotramitacaoById($id);
            //$docArquivos = $arquivos->getProtocoloprocessotramitacaoarquivoById($id);

            if (!$docProtocolos)
                $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

            $this->view->post_var = $docProtocolos;
            //$this->view->post_var = $docTramitacoes;
            //$this->view->post_var = $docArquivos;
        }

        // Verifica se o processo está finalizado
//        if ($docProtocolos['progresso'] != 'andamento') {
//            die("Desculpe, a solicitação de férias não está mais em andamento.");
//        }

        $this->view->processo = $processos->getProtocolosprocessos($queries);
//        $this->view->tramitacoes = $tramitacoes->getProtocolosprocessostramitacoes(array('idprotocoloprocesso'=>$id));
        //$this->view->arquivos = $arquivos->getProtocolosprocessostramitacoesArquivos($queries);

        $this->preForm();
        //var_dump($this->view->post_var); die();

        $db = Zend_Registry::get('db');

        // create new PDF document
        $pdf = new MyTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Protocolo e Processos');
        $pdf->SetSubject('Impressão do Protocolo');

        $this->view->setScriptPath(APPLICATION_PATH . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(4, 4, 4, true);

        //set auto page breaks
        //$pdf->SetAutoPageBreak(FALSE);
        $pdf->SetAutoPageBreak(TRUE, 4);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        $pdf->SetFont('times', '', null, 'false');

        // add a page
        $pdf->AddPage();

        if ($tipo == 0) {
            //Analítico
            $html = $this->view->render('protocolosprocessos/pdf/analitico.phtml');
        } else {
            //Sintético
            $html = $this->view->render('protocolosprocessos/pdf/sintetico.phtml');
        }

        
//        echo $html;
//        die();

        $pdf->writeHTML($html, true, 0, true, 0);
        if ((int) $id > 0) {
            $filename = 'Protocolo_' . $id . '_' . date('d-m-Y') . '.pdf';
        } else {
            $filename = 'Protocolo_' . date('d-m-Y') . '.pdf';
        }
        $pdf->Output($filename, 'D');

        die();

        return true;
    }

    /**
     * 
     * Action de edição de protocolosprocessos
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'protocolosprocessos', 'name' => 'Protocolos Processos'),
            array('url' => null, 'name' => 'Editar Protocolo processos')
        );

        $id = (int) $this->_request->getParam("id");

        $protocolosprocessos = new Protocolosprocessos();
        $protocolosprocesso = $protocolosprocessos->getProtocoloprocessoById($id);

        if (!$protocolosprocesso)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $protocolosprocesso;
        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($protocolosprocesso);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Protocolo processos editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }


        if ($this->view->post_var['posicao'] == 'Em Andamento') {
            $this->setHistoricoComoLido($id);
        }
        return true;
    }

    /**
     * 
     * Action de adição de protocolosprocessos 
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'protocolosprocessos', 'name' => 'Protocolos Processos'),
            array('url' => null, 'name' => 'Adicionar Protocolo processos')
        );

        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Protocolo processos adicionado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idprotocolosprocesso
     */
    private function preForm($idprotocolosprocesso = 0) {
        
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_protocolosprocesso = false) {
        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $sequencial = trim($this->getRequest()->getPost("sequencial"));
        $datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
        $horalancamento = trim($this->getRequest()->getPost("horalancamento"));
        $idtipoprotocolo = (int) trim($this->getRequest()->getPost("idtipoprotocolo"));
        $tipo = trim($this->getRequest()->getPost("tipo"));
        $idusuariologado = (int) trim($this->getRequest()->getPost("idusuariologado"));
        //$origem = trim($this->getRequest()->getPost("origem"));
        $idescola = (int) trim($this->getRequest()->getPost("idescola"));
        //$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
        $nomesolicitante = trim($this->getRequest()->getPost("nomesolicitante"));
        $telefone1 = trim($this->getRequest()->getPost("telefone1"));
        $telefone2 = trim($this->getRequest()->getPost("telefone2"));
        $contato = trim($this->getRequest()->getPost("contato"));
        $email = trim($this->getRequest()->getPost("email"));
        $titulo = trim($this->getRequest()->getPost("titulo"));
        $idorigem = (int) trim($this->getRequest()->getPost("idorigem"));
        $iddestino = (int) trim($this->getRequest()->getPost("iddestino"));
        $idtiposolicitacao = (int) trim($this->getRequest()->getPost("idtiposolicitacao"));
        $solicitacao = trim($this->getRequest()->getPost("solicitacao"));
        $observacoes = trim($this->getRequest()->getPost("observacoes"));
        $iddocumento = (int) trim($this->getRequest()->getPost("iddocumento"));
        $posicao = trim($this->getRequest()->getPost("posicao"));
        $encaminhar = trim($this->getRequest()->getPost("encaminhar"));
        $idperfilencaminhar = (int) trim($this->getRequest()->getPost("idperfilencaminhar"));
        $idescolaencaminhar = (int) trim($this->getRequest()->getPost("idescolaencaminhar"));
        $idlocalencaminhar = (int) trim($this->getRequest()->getPost("idlocalencaminhar"));
        $idusuarioencaminhar = (int) trim($this->getRequest()->getPost("idusuarioencaminhar"));
        $status = trim($this->getRequest()->getPost("status1"));
        $idperfilencaminharhistorico = (int) trim($this->getRequest()->getPost('idperfilencaminharhistorico'));
        $idescolaencaminharhistorico = (int) trim($this->getRequest()->getPost('idescolaencaminharhistorico'));
        $titulohistorico = trim($this->getRequest()->getPost('titulohistorico'));
        $observacoeshistorico = trim($this->getRequest()->getPost('observacoeshistorico'));
        //$idsarquivos = $this->getRequest()->getPost('idsarquivos');
        $iddocumentoencaminhar = (int) trim($this->getRequest()->getPost('iddocumentoencaminhar'));

        $erros = array();

        if (0 == $idtipoprotocolo)
            array_push($erros, "Informe a Tipo Processo.");
        if ("" == $tipo)
            array_push($erros, "Informe a Tipo.");
        if (0 == $idusuariologado)
            array_push($erros, "Informe a Usuários.");
        if ("" == $titulo)
            array_push($erros, "Informe a Título.");
        //if (0==$idtiposolicitacao) array_push($erros, "Informe a Tipo de solicitação.");
        if ("" == $posicao)
            array_push($erros, "Informe a Posição.");
        if ("" == $status)
            array_push($erros, "Informe a Status.");
        if($id == 0){
            if (0 == $idperfilencaminhar)
                array_push($erros, "Informe o campo Encaminhar Para.");
            if (29 == $idperfilencaminhar)
                if (0 == $idescolaencaminhar)
                array_push($erros, "Informe o campo Escola.");    
        }

     
        $protocolosprocessos = new Protocolosprocessos();

        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;

            $dados["sequencial"] = $sequencial;
            $dados["datalancamento"] = date("Y-m-d", $datalancamento);
            $dados["horalancamento"] = $horalancamento;
            $dados["idtipoprotocolo"] = $idtipoprotocolo;
            $dados["tipo"] = $tipo;
            $dados["idusuariologado"] = $idusuariologado;
            $dados["idescola"] = $idescola;
            //$dados["iddepartamento"] = $iddepartamento;
            $dados["nomesolicitante"] = $nomesolicitante;
            $dados["telefone1"] = $telefone1;
            $dados["telefone2"] = $telefone2;
            $dados["contato"] = $contato;
            $dados["email"] = $email;
            $dados["titulo"] = $titulo;
            $dados["idorigem"] = $idorigem;
            $dados["iddestino"] = $iddestino;
            $dados["idtiposolicitacao"] = $idtiposolicitacao;
            $dados["solicitacao"] = $solicitacao;
            $dados["observacoes"] = $observacoes;

            $iddocumento = $this->getArquivo('iddocumento');
            if ($iddocumento != 0)
                $dados['iddocumento'] = $iddocumento;

            $dados["posicao"] = $posicao;
            if (in_array($posicao, array('Concluido', 'Cancelado'))) {
                $this->setFinalizaHistoricos($id, $posicao);
            }

            $dados["encaminhar"] = $encaminhar;
            $dados["idperfilencaminhar"] = $idperfilencaminhar;
            $dados["idescolaencaminhar"] = $idescolaencaminhar;
            $dados["idlocalencaminhar"] = $idlocalencaminhar;
            $dados["idusuarioencaminhar"] = $idusuarioencaminhar;

            $dados["idperfilencaminharhistorico"] = $idperfilencaminharhistorico;
            $dados["idescolaencaminharhistorico"] = $idescolaencaminharhistorico;
            $dados["observacoeshistorico"] = $observacoeshistorico;

            $dados["status"] = $status;

            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            $dados['logdata'] = date('Y-m-d G:i:s');

            $iddocumentoencaminhar = $this->getArquivo('iddocumentoencaminhar');
            if ($iddocumentoencaminhar != 0)
                $dados['iddocumentoencaminhar'] = $iddocumentoencaminhar;

            $row = $protocolosprocessos->save($dados);

            $msg = ($id === 0 ? "Inclusão do registro: " : "A atualização do registro: ") .  $row['id'] . ", foi realizada com sucesso.";
            Fjmx_Util::geraLog(date('Y-m-d'), "Protocolo Processos", Usuarios::getUsuario('id'), $msg);

            if($sequencial == ""){
                $this->setSequencial($row['id']);
            }
            
            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();

            Fjmx_Util::geraLog(date('Y-m-d'), "Protocolo Processos", Usuarios::getUsuario('id'), "Erro: ".$e->getMessage(), "E");

            $db->rollBack();
            die();
        }

        return "";
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }


    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

    /*
     * Verifica se histórico atual está 'Aberto' e muda status para 'Lido'."
     */

    private function setHistoricoComoLido($id) {

        //usuário logado
        $usuariodados = Usuarios::getUsuarioByIdHelper(Usuarios::getUsuario('id'));
        $idescolaencaminhar = 0;

        if ($usuariodados['idperfil'] == '29') {
            $idescolaencaminhar = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($usuariodados['idfuncionario'])['idescola'];
        }

        $verificahistoricos = Protocolosprocessostramitacoes::getProtocolosprocessostramitacoesHelper
                        (array('idprotocoloprocesso' => $id,
                    'idperfildestino' => $usuariodados['idperfil'],
                    'idescoladestino' => $idescolaencaminhar,
                    'status' => 'aberto',
                    'order' => 'ORDER BY p1.id DESC'));

        if ($verificahistoricos) {

            foreach ($verificahistoricos as $key => $r) {

                $rows = new Protocolosprocessostramitacoes();
                $linha = $rows->fetchRow("id=" . $r['id']);

                $row = $linha->toArray();
                $row['status'] = 'lido';

                $rows->save($row);

                $message = new Zend_Session_Namespace("message");
                $message->crudmessage = "Protocolo visualizado!";
            }
        }
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function settramitacaoAction() {
        $this->view->bread_crumb = array(
            array('url' => 'protocolosprocessos', 'name' => 'Protocolos Processos'),
            array('url' => null, 'name' => 'Editar Protocolo processos')
        );

        $this->_helper->layout->disableLayout();

        $idTramitacao = (int) $this->getRequest()->getParam("id");

        $this->view->idTramitacao = $idTramitacao;
    }

    /*
     * Finaliza todos os históricos"
     */

    private function setFinalizaHistoricos($id, $situacao) {


        $finalizahistoricos = Protocolosprocessostramitacoes::getProtocolosprocessostramitacoesHelper
                        (array('idprotocoloprocesso' => $id,
                    'order' => 'ORDER BY p1.id DESC'));

        if ($finalizahistoricos) {

            foreach ($finalizahistoricos as $key => $r) {

                $rows = new Protocolosprocessostramitacoes();
                $linha = $rows->fetchRow("id=" . $r['id']);

                $row = $linha->toArray();
                $row['status'] = strtolower($situacao);

                $rows->save($row);

                $message = new Zend_Session_Namespace("message");
                $message->crudmessage = "Protocolo " . $situacao;
            }
        }
    }

    /**
     * 
     * Atualiza sequencial
     */
    public function setSequencial($id) {

        $rows = new Protocolosprocessos();
        $linha = $rows->fetchRow('id=' . $id);

        $row = $linha->toArray(); 
        $row['sequencial'] = $row['id'] . '/' . substr($row['datacriacao'],0,4);
        
        $rows->save($row);

        return true;
    }    
    
}
