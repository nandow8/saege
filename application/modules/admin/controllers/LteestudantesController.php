<?php

class Admin_LteestudantesController extends Zend_Controller_Action {

	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacotealuno
	 */
	protected $_usuario = null;

	/**
     * Verificação de permissao de acesso
     */
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

		Mn_Util::blockAccess("lteestudantes", $this->_request->getActionName());

		$this->_usuario = unserialize($loginNameSpace->usuario);

		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 *
	 * Action para ser consultada via ajax e excluir o estudante
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = (int)$this->_request->getPost("id");

		$rows = new Lteestudantes();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Usuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');

			$rows->save($row);

			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Estudante excluído com sucesso.";

			die("OK");
		}

		die("Não encontrado!");
	}

	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");

		if ($op=="lteestudantes") $objs = new Lteestudantes();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {

			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Usuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');

			$objs->save($obj);

			die($obj['status']);
		}

		die("Não encontrado!");
	}

	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");

		if ($op=="change") $objs = new Lteestudantes();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);

		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}

		$this->view->message = "OK";
		$this->render("xml");
	}

	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);

		$rows = new Lteestudantes();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/admin/".$this->_request->getControllerName()."/index");
		die();
	}

	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Estudante')
		);

		$ns = new Zend_Session_Namespace('logistica_estudantes');
		$estudantes = new Lteestudantes();
		$queries = array();


		//PESQUISA
		if ($this->getRequest()->isPost()) {
			$ns->pesquisa = serialize($_POST);
			$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
			die();
		}
		if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);

		if (isset($this->view->post_var)) {

			if ((isset($this->view->post_var['nome'])) && ($this->view->post_var['nome']!='')) $queries['nome'] = $this->view->post_var['nome'];
			if ((isset($this->view->post_var['filiacao'])) && ($this->view->post_var['filiacao']!='')) $queries['filiacao'] = $this->view->post_var['filiacao'];
			if ((isset($this->view->post_var['matricula'])) && ($this->view->post_var['matricula']!='')) $queries['matricula'] = $this->view->post_var['matricula'];
			if ((isset($this->view->post_var['curso'])) && ($this->view->post_var['curso']!='')) $queries['curso'] = $this->view->post_var['curso'];
			if ((isset($this->view->post_var['subsidio'])) && ($this->view->post_var['subsidio']!='')) $queries['subsidio'] = $this->view->post_var['subsidio'];
			if ((isset($this->view->post_var['cpf'])) && ($this->view->post_var['cpf']!='')) $queries['cpf'] = $this->view->post_var['cpf'];
			if ((isset($this->view->post_var['ra'])) && ($this->view->post_var['ra']!='')) $queries['ra'] = $this->view->post_var['ra'];
			if ((isset($this->view->post_var['tipoestudante'])) && ($this->view->post_var['tipoestudante']!='')) $queries['tipoestudante'] = $this->view->post_var['tipoestudante'];
			if ((isset($this->view->post_var['status'])) && ($this->view->post_var['status']!='')) $queries['status'] = $this->view->post_var['status'];
			if ((isset($this->view->post_var['tipoestudante'])) && ($this->view->post_var['tipoestudante']!='')) $queries['tipoestudante'] = $this->view->post_var['tipoestudante'];
			if ((isset($this->view->post_var['tipotransporte'])) && ($this->view->post_var['tipotransporte']!='')) $queries['tipotransporte'] = $this->view->post_var['tipotransporte'];
			if ((isset($this->view->post_var['identidade'])) && ($this->view->post_var['identidade']!='')) $queries['identidade'] = $this->view->post_var['identidade'];
			if ((isset($this->view->post_var['valorfretado'])) && ($this->view->post_var['valorfretado']!='')) $queries['valorfretado'] = $this->view->post_var['valorfretado'];
			if ((isset($this->view->post_var['eja'])) && ($this->view->post_var['eja']!='')) $queries['eja'] = $this->view->post_var['eja'];
		}

		//PAGINACAO
		$maxpp = 15;

		$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;

		$queries['total'] = true;
		$totalRegistros = $estudantes->getEstudantes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;

		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;

		$this->view->rows = $estudantes->getEstudantes($queries, $paginaAtual, $maxpp);
	}

	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'lteestudantes',	'name' => 'Estudantes'),
			array('url' => null,'name' => 'Visualizar estudante')
		);

		$id = (int)$this->_request->getParam("id");
		$estudantes = new Lteestudantes();
		$estudante = $estudantes->getEstudanteById($id);

		if (!$estudante) $this->_redirect('admin/' . $this->getRequest()->getControllerName());

		$this->view->visualizar = true;
		$this->view->post_var = $estudante;

	}

	/**
	 *
	 * Action de edição de estudante
	 */
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'lteestudantes',	'name' => 'Estudantes'),
			array('url' => null,'name' => 'Editar estudante')
		);

		$id = (int)$this->_request->getParam("id");
		$estudantes = new Lteestudantes();
		$estudante = $estudantes->getEstudanteById($id);

		if (!$estudante)
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());

		$this->view->post_var = $estudante;


		if ($this->_request->isPost()) {
			$erros = $this->getPost($estudante);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false;
			}

			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Estudante editado com sucesso.";

			$this->_redirect('admin/'.$this->getRequest()->getControllerName());
		}
		return true;
	}

	/**
	 *
	 * Action de adição de estudante
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'lteestudantes',	'name' => 'Estudantes'),
			array('url' => null,'name' => 'Adicionar estudante')
		);

		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false;
			}

			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Estudante adicionado com sucesso.";

			$this->_redirect('admin/'.$this->getRequest()->getControllerName());
		}
		return true;
	}

	public function setescolaAction() {
		$this->_helper->layout->disableLayout();

		$tipo = $this->getRequest()->getPost('tipo');
		$entidades = new Lteentidades();
		$this->view->rows = $entidades->getEntidades(array('tipo' => $tipo,0,0));
	}

	/**
	 * Valida e grava os dados do formulário
	 */
	private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST;
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);

		$id 			= (int)$this->getRequest()->getPost("id");
		$nome 			= strip_tags(trim($this->getRequest()->getPost("nome")));
		$nomesocial 	= strip_tags(trim($this->getRequest()->getPost("nomesocial")));
		$nomemae		= strip_tags(trim($this->getRequest()->getPost("nomemae")));
		$sexo 			= strip_tags(trim($this->getRequest()->getPost("sexo")));
		$datanascimento = strip_tags(trim($this->getRequest()->getPost("nascimento")));
		$datanascimento = Mn_Util::stringToTime($datanascimento);
		$datanascimento = date('Y-m-d G:i:s', $datanascimento);
		$status 		= strip_tags(trim($this->getRequest()->getPost("status")));
		$cpf 			= strip_tags(trim($this->getRequest()->getPost("cpf")));
		$cpf 			= preg_replace("/[^0-9]/", "", $cpf);
		$rg 			= strip_tags(trim($this->getRequest()->getPost("rg")));
		$ufrg 			= strip_tags(trim($this->getRequest()->getPost("ufrg")));
		$ra 			= strip_tags(trim($this->getRequest()->getPost("ra")));
		$tituloeleitor 	= strip_tags(trim($this->getRequest()->getPost("tituloeleitor")));
		$tituloeleitor 	= preg_replace("/[^0-9]/", "", $tituloeleitor);
		$zonaeleitor 	= strip_tags(trim($this->getRequest()->getPost("zonaeleitor")));
		$zonaeleitor 	= preg_replace("/[^0-9]/", "", $zonaeleitor);
		$secaoeleitor 	= strip_tags(trim($this->getRequest()->getPost("secaoeleitor")));
		$secaoeleitor 	= preg_replace("/[^0-9]/", "", $secaoeleitor);
		$nis 			= strip_tags(trim($this->getRequest()->getPost("nis")));
		$nis 			= preg_replace("/[^0-9]/", "", $nis);
		$email 			= strip_tags(trim($this->getRequest()->getPost("email")));
		$telefone 		= strip_tags(trim($this->getRequest()->getPost("telefone")));
		$telefone 		= preg_replace("/[^0-9]/", "", $telefone);
		$celular 		= strip_tags(trim($this->getRequest()->getPost("celular")));
		$celular 		= preg_replace("/[^0-9]/", "", $celular);
		$facebook 		= strip_tags(trim($this->getRequest()->getPost("facebook")));
		$cep 			= strip_tags(trim($this->getRequest()->getPost("cep")));
		$cep 			= preg_replace("/[^0-9]/", "", $cep);
		$endereco 		= strip_tags(trim($this->getRequest()->getPost("endereco")));
		$numero 		= strip_tags(trim($this->getRequest()->getPost("numero")));
		$complemento 	= strip_tags(trim($this->getRequest()->getPost("complemento")));
		$bairro 		= strip_tags(trim($this->getRequest()->getPost("bairro")));
		$idestado 		= strip_tags(trim($this->getRequest()->getPost("idestado")));
		$cidade 		= strip_tags(trim($this->getRequest()->getPost("cidade")));
		$observacoes 	= strip_tags(trim($this->getRequest()->getPost("observacoes")));
		$valorfretado	= strip_tags(trim($this->getRequest()->getPost("valorfretado")));
		$valorfretado	= str_replace(",", ".", $valorfretado);
		$semestre 		= strip_tags((int)trim($this->getRequest()->getPost("semestre")));
		$periodo 		= strip_tags(trim($this->getRequest()->getPost("periodo")));

		$tipoestudante	= strip_tags(trim($this->getRequest()->getPost("tipoestudante")));
		$tipotransporte	= strip_tags(trim($this->getRequest()->getPost("tipotransporte")));
		$serie  		= strip_tags(trim($this->getRequest()->getPost("serie")));
		$filiacao		= strip_tags(trim($this->getRequest()->getPost("filiacao")));
		$matricula		= strip_tags(trim($this->getRequest()->getPost("matricula")));
		$eja			= strip_tags(trim($this->getRequest()->getPost("eja")));
		$eja 			= ($eja) ? 'sim' : 'nao';
		$bolsista		= strip_tags(trim($this->getRequest()->getPost("bolsista")));
		$bolsista		= ($bolsista) ? 'sim' : 'nao';

		if ($tipoestudante == 'universidade') {
			$identidade = (int)(trim($this->getRequest()->getPost("identidade")));

			$identidade = $identidade == 0 ? (int)(trim($this->getRequest()->getPost("_identidade"))) : $identidade;

			$idcurso = strip_tags((int)trim($this->getRequest()->getPost("idcurso")));
		} else if ($tipoestudante == 'estadual' || $tipoestudante == 'municipal') {
			$identidade = (int)(trim($this->getRequest()->getPost("idescola")));

			$identidade = $identidade == 0 ? (int)(trim($this->getRequest()->getPost("_idescola"))) : $identidade;

			$curso = strip_tags(trim($this->getRequest()->getPost("cursoescola")));
			$valorfretado = 0;
			$idcurso = 0;
		} else if ($tipoestudante == '') {
			array_push($erros, 'Selecione um tipo de estudante.');
		}

		if ($tipotransporte == '') {
			array_push($erros, 'Selecione um tipo de transporte.');
		}

		$entidade = strip_tags(trim($this->getRequest()->getPost("entidade")));

		$idimagem = (int)trim($this->getRequest()->getPost("idimagem"));

		$input_webcam = trim($this->getRequest()->getPost("input_webcam"));

		$idanxrg = (int)$this->getArquivo('anxrg');
		$idanxcpf = (int)$this->getArquivo('anxcpf');
		$idanxtituloeleitor = (int)$this->getArquivo('anxtituloeleitor');
		$idanxcomprovantevotacao = (int)$this->getArquivo('anxcomprovantevotacao');
		$idanxcomprovanteendereco = (int)$this->getArquivo('anxcomprovanteendereco');
		$idanxdeclaracaoendereco = (int)$this->getArquivo('anxdeclaracaoendereco');
		$idanxaetusi = (int)$this->getArquivo('anxaetusi');
		$idanxmatricula = (int)$this->getArquivo('anxmatricula');
		$idanxestudo = (int)$this->getArquivo('anxestudo');
		$idanxcancelamento = (int)$this->getArquivo('anxcancelamento');

		$erros = array();
		if (""==Mn_Util::formPostVarData($datanascimento)) array_push($erros, 'Preencha o campo DATA DE NASCIMENTO.');
		if ($status == "Bloqueado" && ($idanxcancelamento == '' || $idanxcancelamento == 0) && $tipoestudante == 'universidade') array_push($erros, 'Anexe o pedido de cancelamento');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');

		$estudantes = new Lteestudantes();

		if (sizeof($erros)>0) return $erros;

		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {

			$dados = array();
			$dados['id'] 			 = $id;
			$dados['nome'] 			 = $nome;
			$dados['nomesocial'] 	 = $nomesocial;
			$dados['nomemae']		 = $nomemae;
			$dados['sexo'] 			 = $sexo;
			$dados['datanascimento'] = $datanascimento;
			$dados['status'] 		 = $status;
			$dados['cpf'] 			 = $cpf;
			$dados['rg'] 			 = $rg;
			$dados['ufrg'] 			 = $ufrg;
			$dados['ra'] 			 = $ra;
			$dados['tituloeleitor']  = $tituloeleitor;
			$dados['zonaeleitor']    = $zonaeleitor;
			$dados['secaoeleitor']   = $secaoeleitor;
			$dados['nis']			 = $nis;
			$dados['email'] 		 = $email;
			$dados['telefone'] 		 = $telefone;
			$dados['celular'] 		 = $celular;
			$dados['facebook']		 = $facebook;
			$dados['cep'] 			 = $cep;
			$dados['endereco'] 		 = $endereco;
			$dados['numero'] 		 = $numero;
			$dados['complemento'] 	 = $complemento;
			$dados['bairro'] 		 = $bairro;
			$dados['idestado'] 		 = $idestado;
			$dados['cidade'] 		 = $cidade;
			$dados['observacoes'] 	 = $observacoes;

			$dados['valorfretado']	 = $valorfretado;
			$dados['identidade'] 	 = $identidade;
			$dados['curso'] 		 = $curso;
			$dados['idcurso'] 		 = $idcurso;
			$dados['semestre'] 		 = $semestre;
			$dados['periodo'] 		 = $periodo;

			$dados['entidade'] 	 	 = $entidade;

			$dados['tipoestudante']	 = $tipoestudante;
			$dados['tipotransporte'] = $tipotransporte;
			$dados['serie']			 = $serie;

			$dados['filiacao']		 = $filiacao;
			$dados['matricula']		 = $matricula;
			$dados['eja']			 = $eja;
			$dados['bolsista']		 = $bolsista;

			$dados['idanxrg']  = $idanxrg;
			$dados['idanxcpf'] = $idanxcpf;
			$dados['idanxtituloeleitor']		= $idanxtituloeleitor;
			$dados['idanxcomprovantevotacao']	= $idanxcomprovantevotacao;
			$dados['idanxcomprovanteendereco']	= $idanxcomprovanteendereco;
			$dados['idanxdeclaracaoendereco']	= $idanxdeclaracaoendereco;
			$dados['idanxaetusi']	 = $idanxaetusi;
			$dados['idanxmatricula'] = $idanxmatricula;
			$dados['idanxestudo'] 	 = $idanxestudo;
			$dados['idanxcancelamento'] = $idanxcancelamento;

			$dados['excluido'] 		 = 'nao';
                        $dados['logusuario'] = $this->_usuario['id'];
                        $dados['logdata'] = date('Y-m-d G:i:s');

                        if ($input_webcam!='') {
				$input_webcam = str_replace("data:image/png;base64,", "", $input_webcam);
				$imagens = new Imagens();
				$__imagem = $imagens->saveBase64($input_webcam);
				$download = $imagens->downloadImagem($__imagem['id']);

				$simple = new SimpleImage();
				$simple->load($download['sourceFilename']);

				$src = $simple->image;
				$height = imagesy($src);
				$width = floor(($height/4)*3);
				$pos_x = floor((imagesx($src)-$width)/2);

				$dest = imagecreatetruecolor($width, $height);
				imagecopy($dest, $src, 0, 0, $pos_x, 0, $width, $height);
				imagejpeg($dest,$download['sourceFilename'],100);


				$dados['idimagem'] = $__imagem['id'];
			} else {
				$idimagem = $this->getImagem('idimagem');
				if ($idimagem!=0) $dados['idimagem'] = $idimagem;
			}

			// var_dump($dados); die();

			$row = $estudantes->save($dados);

			$db->commit();

		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}

		return "";
	}

	private function getLatitudeLongitude($idestudante) {
		$estudantes = new Lteestudantes();
		$estudante = $estudantes->getEstudanteById($idestudante);
		if(!$estudante) return false;

		$endereco = str_replace(" ", "+", $estudante['endereco']);

		$address = $endereco . ','.$estudante['numero'].',' . $estudante['cidade'] . ',' . $estudante['uf'];
		$address = str_replace(" ", "+", $address);

		$geocode = file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.',brasil');

		$output= json_decode($geocode);

		$lat = $output->results[0]->geometry->location->lat;
		$long = $output->results[0]->geometry->location->lng;

		$estudante['latitude'] = $lat;
		$estudante['longitude'] = $long;
		$estudantes->save($estudante);
		return ;
	}

	private function getImagem($nome = false, $apenas_copia = false) {

		$idimagem = false;
		$imagens = new Imagens();

		try {

			ini_set('memory_limit', '-1');
			$idimagem = $imagens->getImagemFromForm($nome, NULL, NULL, $apenas_copia);
			//var_dump($idimagem); die();
		} catch (Exception $e) {

			$idimagem = false;
			array_push($erros,$e->getMessage());

		}

		$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $nome));

		if ($excluir_imagem=='excluir_' . $nome) $idimagem = -1;

		return $idimagem;
	}

	private function getArquivo($filename = 'arquivo') {
		$idarquivo = false;
		$arquivos = new Arquivos();
		try {
			$idarquivo = $arquivos->getArquivoFromForm($filename);
		} catch (Exception $e) {
			$idarquivo = false;
			array_push($erros,$e->getMessage());
		}

		$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
		if ($excluir_arquivo=='excluir') $idarquivo = -1;
		return $idarquivo;
	}
}
