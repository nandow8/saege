<?php

/**
 * Controle da classe censonovosalunos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_CensonovosalunosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Xensonovosaluno
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("censonovosalunos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Censonovosalunos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "aluno excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="censonovosalunos") $objs = new Censonovosalunos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Pré Matrícula") ? "Pré Matrícula" : ($obj['status']=="Pré Matrícula") ? "Fila" : "Matriculado";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'censonovosalunos', 'name' => 'Novos alunos'),
			array('url' => null,'name' => 'Visualizar aluno')
		);
		
		$id = (int)$this->_request->getParam("id");
		$censonovosalunos = new Censonovosalunos();
		$censonovosaluno = $censonovosalunos->getXensonovosalunoById($id, array());
		
		if (!$censonovosaluno) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $censonovosaluno;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Novos alunos')
		);
		

		$ns = new Zend_Session_Namespace('default_censonovosalunos');
		$censonovosalunos = new Censonovosalunos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
		
		
    	if (isset($this->view->post_var)) {
			foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
			
			//if ($this->view->post_var["escola"]!="") $queries["escola"] = $this->view->post_var["escola"];
			if ($this->view->post_var["id_tipo_ensino"]!="") $queries["id_tipo_ensino"] = $this->view->post_var["id_tipo_ensino"];
			if ($this->view->post_var["id_escola"]!="") $queries["id_escola"] = $this->view->post_var["id_escola"];
			if ($this->view->post_var["aluno"]!="") $queries["aluno"] = $this->view->post_var["aluno"];
			if ($this->view->post_var["codigoinscricao"]!="") $queries["codigoinscricao"] = $this->view->post_var["codigoinscricao"];
			if ($this->view->post_var["email"]!="") $queries["email"] = $this->view->post_var["email"];
			if ($this->view->post_var["datanascimento_i"]!="") $queries["datanascimento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datanascimento_i"]));
			if ($this->view->post_var["datanascimento_f"]!="") $queries["datanascimento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datanascimento_f"]));

			if ($this->view->post_var["nomefiliacao1"]!="") $queries["nomefiliacao1"] = $this->view->post_var["nomefiliacao1"];
			if ($this->view->post_var["id_tipo_ensino"]!="") $queries["id_tipo_ensino"] = $this->view->post_var["id_tipo_ensino"];
			if ($this->view->post_var["filtro-etnia"]!="") $queries["filtro-etnia"] = $this->view->post_var["filtro-etnia"];
			// if ($this->view->post_var["filtro-escola"]!="") $queries["filtro-escola"] = $this->view->post_var["filtro-escola"];
			// if ($this->view->post_var["filtro-escola"]!="") $queries["filtro-escola"] = $this->view->post_var["filtro-escola"];

			if ($this->view->post_var["filtro-nis"]!="") $queries["filtro-nis"] = $this->view->post_var["filtro-nis"];
			if ($this->view->post_var["aluguel"]!="") $queries["aluguel"] = $this->view->post_var["aluguel"];
			if ($this->view->post_var["portadorespecialidade"]!="") $queries["portadorespecialidade"] = $this->view->post_var["portadorespecialidade"];
			if ($this->view->post_var["statusprofissional"]!="") $queries["statusprofissional"] = $this->view->post_var["statusprofissional"];

			if ($this->view->post_var["filtro-rendainicial"]!="") $queries["filtro-rendainicial"] = $this->view->post_var["filtro-rendainicial"];
			if ($this->view->post_var["filtro-rendafinal"]!="") $queries["filtro-rendafinal"] = $this->view->post_var["filtro-rendafinal"];



// 			if ($this->view->post_var["aluno"]!="") $queries["aluno"] = $this->view->post_var["aluno"];
// if ($this->view->post_var["datanascimento_i"]!="") $queries["datanascimento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datanascimento_i"]));
// if ($this->view->post_var["datanascimento_f"]!="") $queries["datanascimento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datanascimento_f"]));
// if ($this->view->post_var["sexo"]!="") $queries["sexo"] = $this->view->post_var["sexo"];
// // if ($this->view->post_var["escola"]!="") $queries["escola"] = $this->view->post_var["escola"];
// // if ($this->view->post_var["estado"]!="") $queries["estado"] = $this->view->post_var["estado"];
// if ($this->view->post_var["matricula"]!="") $queries["matricula"] = $this->view->post_var["matricula"];
// if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
 
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $censonovosalunos->getCensonovosalunos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->alunofiltro = $queries;
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;  
		
		$this->view->rows = $censonovosalunos->getCensonovosalunos($queries, $paginaAtual, $maxpp);	
	}

	public function listaDataCorte(){
		$censodatacortes = new Censodatacorte();
		$censodatacorte = $censodatacortes->getCensodatacorteHelper(); 
		$this->view->censocortes = $censodatacorte;	
	}
	
	/**
	 * 
	 * Action de edição de censonovosalunos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'censonovosalunos', 'name' => 'Novos alunos'),
			array('url' => null,'name' => 'Editar aluno')
		);	
		
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
	    $series = new Escolasseries(); 	
		$listseries = $series->getEscolasseries(array('status'=>'Ativo', 'idsecretaria'=> Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' )));
		
	
		
		$id = (int)$this->_request->getParam("id");
		$censonovosalunos = new Censonovosalunos();
		$censonovosaluno = $censonovosalunos->getXensonovosalunoById($id);
		$this->view->alunomatricula = $censonovosaluno;

		$escolasseries = new Escolasseries();
		$escolasserie = $escolasseries->getEscolasseriesHelper();
		$this->view->escolasserie = $escolasserie; 

		if (!$censonovosaluno) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

		//lista select datacorte
		$this->listaDataCorte();	
		
		$this->view->post_var = $censonovosaluno;
		$this->preForm();
		
		if ($this->_request->isPost()) {
				$erros = $this->getPost($censonovosaluno);
				if ($erros!="") {
					$this->view->erros = $erros;
				return false; 
			}
			 
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Pré Matrícula realizada com sucesso!";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de censonovosalunos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'censonovosalunos', 'name' => 'Novos alunos'),
			array('url' => null,'name' => 'Adicionar aluno')
		);	
		
		$idc = (int)$this->_request->getParam("idcentralvaga");
		if($idc > 0){
			$this->view->post_var = array();
			$this->view->post_var['idcentralvaga'] = $idc;
		}

		$this->listaDataCorte();

		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "aluno adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
	}	
	
	public function filademandacrecheAction(){
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Novos alunos')
		);
		
		$ns = new Zend_Session_Namespace('default_censonovosalunos');
		$censonovosalunos = new Censonovosalunos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["aluno"]!="") $queries["aluno"] = $this->view->post_var["aluno"];
if ($this->view->post_var["datanascimento_i"]!="") $queries["datanascimento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datanascimento_i"]));
if ($this->view->post_var["datanascimento_f"]!="") $queries["datanascimento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datanascimento_f"]));
if ($this->view->post_var["sexo"]!="") $queries["sexo"] = $this->view->post_var["sexo"];
// if ($this->view->post_var["escola"]!="") $queries["escola"] = $this->view->post_var["escola"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $censonovosalunos->getFilademandacreche($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $censonovosalunos->getFilademandacreche($queries, $paginaAtual, $maxpp);
	}

	public function filademandaalunoAction(){
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Novos alunos')
		);
		
		$ns = new Zend_Session_Namespace('default_censonovosalunos');
		$censonovosalunos = new Censonovosalunos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["aluno"]!="") $queries["aluno"] = $this->view->post_var["aluno"];
if ($this->view->post_var["datanascimento_i"]!="") $queries["datanascimento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datanascimento_i"]));
if ($this->view->post_var["datanascimento_f"]!="") $queries["datanascimento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datanascimento_f"]));
if ($this->view->post_var["sexo"]!="") $queries["sexo"] = $this->view->post_var["sexo"];
// if ($this->view->post_var["escola"]!="") $queries["escola"] = $this->view->post_var["escola"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $censonovosalunos->getfilademandaaluno($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $censonovosalunos->getfilademandaaluno($queries, $paginaAtual, $maxpp);
	}
    
    /**
     * Atribui valores ao view
     * @param int $idcensonovosaluno
     */    
    private function preForm($idcensonovosaluno = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_censonovosaluno = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		
$aluno = trim($this->getRequest()->getPost("aluno"));
$datanascimento = Mn_Util::stringToTime($this->getRequest()->getPost("datanascimento"));
$sexo = trim($this->getRequest()->getPost("sexo"));
$filiacao1 = trim($this->getRequest()->getPost("filiacao1"));
$nomefiliacao1 = trim($this->getRequest()->getPost("nomefiliacao1"));
$filiacao2 = trim($this->getRequest()->getPost("filiacao2"));
$nomefiliacao2 = trim($this->getRequest()->getPost("nomefiliacao2"));
$painaodeclarado = trim($this->getRequest()->getPost("painaodeclarado"));
$temnomesocial = trim($this->getRequest()->getPost("temnomesocial"));
$nomesocial = trim($this->getRequest()->getPost("nomesocial"));
$status = trim($this->getRequest()->getPost("status1"));

$cpffiliacao = trim($this->getRequest()->getPost("cpffiliacao"));
$cpfaluno = trim($this->getRequest()->getPost("cpfaluno"));
$datanascimentoresponsavel = Mn_Util::stringToTime($this->getRequest()->getPost("datanascimentoresponsavel"));
$cep = trim($this->getRequest()->getPost("cep"));
$endereco = trim($this->getRequest()->getPost("endereco"));
$numero = trim($this->getRequest()->getPost("numero"));
$complemento = trim($this->getRequest()->getPost("complemento"));
$bairro = trim($this->getRequest()->getPost("bairro"));
$estado = trim($this->getRequest()->getPost("estado"));
$cidade = trim($this->getRequest()->getPost("cidade"));
$tipotelefone1 = trim($this->getRequest()->getPost("tipotelefone1"));
$numerotelefone1 = trim($this->getRequest()->getPost("numerotelefone1"));
$tipotelefone2 = trim($this->getRequest()->getPost("tipotelefone2"));
$numerotelefone2 = trim($this->getRequest()->getPost("numerotelefone2"));
$email = trim($this->getRequest()->getPost("email"));
$codigoinscricao = trim($this->getRequest()->getPost("codigoinscricao"));
$portadorespecialidade = trim($this->getRequest()->getPost("portadorespecialidade"));
$auxiliosocial = trim($this->getRequest()->getPost("auxiliosocial"));
$nis = trim($this->getRequest()->getPost("nis"));
$rendafamiliar = MN_Util::trataNum(trim($this->getRequest()->getPost("rendafamiliar"))); 
$moradoresresidencia = MN_Util::trataNum(trim($this->getRequest()->getPost("moradoresresidencia"))); 
$statusprofissional = trim($this->getRequest()->getPost("statusprofissional"));
$escola = trim($this->getRequest()->getPost("escola"));
$serie = trim($this->getRequest()->getPost("serie"));
$etapa_ensino = trim($this->getRequest()->getPost("etapa")); 
$datadecorte = trim($this->getRequest()->getPost("datadecorte")); 
$matricula = trim($this->getRequest()->getPost("matricula")); 
$tipodeficiencia = trim($this->getRequest()->getPost("tipodeficiencia")); 
$observacaodeficiencia = trim($this->getRequest()->getPost("observacaodeficiencia"));  
$etnia = trim($this->getRequest()->getPost("etnia"));  
$quilombola = trim($this->getRequest()->getPost("quilombola"));  
$nova_cert_tipo_livro_reg = trim($this->getRequest()->getPost("nova_cert_tipo_livro_reg"));
$latitude = trim($this->getRequest()->getPost("latitude"));  
$longitude = trim($this->getRequest()->getPost("longitude")); 
$idescola = trim($this->getRequest()->getPost("idselectescola")); 

$distancia_calculo = $this->getRequest()->getPost("distanciaset");

$idEscola = $this->getRequest()->getPost("idselectescola");
$escolasset = $this->getRequest()->getPost("escolassets");  
$ensinoset =$this->getRequest()->getPost("ensinoset"); 

/* -----------		Novos Campos Inseridos --------------------------*/

$escola = Escolas::getEscolaByIdHelper($idEscola)['escola']; 

$id_tipo_ensino =$this->getRequest()->getPost("ensinoset"); 


$nome_tipo_ensino = Escolastiposensinosgdae::getTipoensinoByIdHelper($id_tipo_ensino)['descricoes'];

		$erros = array();
		
		
if (""==$aluno) array_push($erros, "Informe a Nome do aluno.");

if (""==$status) array_push($erros, "Informe a Status.");

		
		$censonovosalunos = new Censonovosalunos();
		 
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
	
			
$dados["aluno"] = $aluno;
$dados["datanascimento"] = date("Y-m-d", $datanascimento);
$dados["sexo"] = $sexo;
$dados["filiacao1"] = $filiacao1;
$dados["nomefiliacao1"] = $nomefiliacao1;
$dados["filiacao2"] = $filiacao2;
$dados["nomefiliacao2"] = $nomefiliacao2;
$dados["painaodeclarado"] = $painaodeclarado;
$dados["temnomesocial"] = $temnomesocial;
$dados["nomesocial"] = $nomesocial;
$dados["status"] = $status;

$dados["cpffiliacao"] =  $cpffiliacao; 
$dados["cpfaluno"] =  $cpfaluno; 
$dados["datanascimentoresponsavel"] = date("Y-m-d",$datanascimentoresponsavel);
$dados["cep"] = $cep;
$dados["endereco"] = $endereco;
$dados["numero"] = $numero;
$dados["complemento"] = $complemento;
$dados["bairro"] = $bairro;
$dados["estado"] = $estado;
$dados["cidade"] = $cidade;
$dados["tipotelefone1"] = $tipotelefone1;
$dados["numerotelefone1"] =  $numerotelefone1;
$dados["tipotelefone2"] = $tipotelefone2;
$dados["numerotelefone2"] = $numerotelefone2;
$dados["email"] = $email;
$dados["codigoinscricao"] = $codigoinscricao;
$dados["portadorespecialidade"] = $portadorespecialidade;
$dados["auxiliosocial"] = $auxiliosocial;
$dados["nis"] = $nis;
$dados["rendafamiliar"] = $rendafamiliar;
$dados["moradoresresidencia"] = $moradoresresidencia;
$dados["statusprofissional"] = $statusprofissional;
$dados["id_escola"] = $idescola;

$dados["id_tipo_ensino"] = $id_tipo_ensino;
$dados["nome_tipo_ensino"] = $nome_tipo_ensino;
$dados["escola"] = $escola;
$dados["distancia"] = $distancia_calculo;

$dados["etapa"] = $serie;
$dados["etapa_ensino"] = $etapa_ensino;
$dados["datadecorte"] = $datadecorte;
$dados["matricula"] = $matricula;
$dados["tipodeficiencia"] = $tipodeficiencia;
$dados["observacaodeficiencia"] = $observacaodeficiencia; 
$dados["etnia"] = $etnia; 
$dados["quilombola"] = $quilombola; 
$dados["nova_cert_tipo_livro_reg"] = $nova_cert_tipo_livro_reg; 
$dados["latitude"] = $latitude; 
$dados["longitude"] = $longitude; 
  
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

			$row = $censonovosalunos->save($dados);

		if($matricula == "matricula"){

			$dadosEndereco = new Enderecos();
			$dadosEnderecos = array();

			$dadosEnderecos["cep"] = $cep;
			$dadosEnderecos["endereco"] = $endereco;
			$dadosEnderecos["numero"] = $numero;
			$dadosEnderecos["complemento"] = $complemento;
			$dadosEnderecos["bairro"] = $bairro; 
			$dadosEnderecos["cidade"] = $cidade;
			$dadosEnderecos["idestado"] = $estado; 
			$dadosEnderecos["excluido"] = "nao";
			 
			$row = $dadosEndereco->save($dadosEnderecos); 
 
			$escolasalunos = new Escolasalunos(); 
			
			$dadosescolasalunos = array();
			  	 
		    $dadosescolasalunos['idescola'] = $escola;
				
			$dadosescolasalunos["nomerazao"] = $aluno;
			$dadosescolasalunos["nascimento"] = date("Y-m-d", $datanascimento); 
			$dadosescolasalunos["sexo"] = $sexo;
			$dadosescolasalunos["email"] = $email;
			$dadosescolasalunos["telefoneresidencial"] = $numerotelefone2;
			$dadosescolasalunos["telefonecontato"] = $numerotelefone1;
			$dadosescolasalunos["rendafamiliar"] = $rendafamiliar;
			$dadosescolasalunos["etnia"] = $etnia;
			$dadosescolasalunos["quilombola"] = $quilombola;
                        $nova_cert_tipo_livro_reg["nova_cert_tipo_livro_reg"] = $nova_cert_tipo_livro_reg;
			$dadosescolasalunos["descricoesnecessidadesespeciais"] = $observacaodeficiencia;
			$dadosescolasalunos["necessidadesespeciais"] = $tipodeficiencia;
			
			if($filiacao1 == "Mãe"){
				$dadosescolasalunos['nomemae'] = $nomefiliacao1;
				$dadosescolasalunos['cpfcnpjmae'] = $cpffiliacao; 
				$dadosescolasalunos["nomecontato"] = $nomefiliacao1;
				$dadosescolasalunos["nascimentomae"] = date("Y-m-d",$datanascimentoresponsavel);
			}
			if($filiacao1 == "Pai"){
				$dadosescolasalunos['nomepai'] = $nomefiliacao1;
				$dadosescolasalunos['cpfcnpjpai'] = $cpffiliacao;  
				$dadosescolasalunos["nomecontato"] = $nomefiliacao1;
				$dadosescolasalunos["nascimentopai"] = date("Y-m-d",$datanascimentoresponsavel);
			} 
			
			$dadosescolasalunos["idendereco"] = $row['id']; 
			$dadosescolasalunos["matricula"] = $matricula;
			$dadosescolasalunos["nis"] = $nis; 
			$dadosescolasalunos["idserie"] = $serie; 
			$dadosescolasalunos["latitude"] = $latitude; 
			$dadosescolasalunos["longitude"] = $longitude;  
			
		    $dadosescolasalunos["status"] = 'Ativo';
			$dadosescolasalunos['excluido'] = 'nao';
			$dadosescolasalunos['logusuario'] = $this->_usuario['id'];;
			$dadosescolasalunos['logdata'] = date('Y-m-d G:i:s');	

			
			$row = $escolasalunos->save($dadosescolasalunos);   
		   
		 
		 
		}
		
		$db->commit();
		
		if ($id==0){
				//$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName() . '/editar/id/' . $row['id']);
			}
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
	
	

	public function retornarankingajaxAction(){
			$this->_helper->layout->disableLayout();
			$this->_response->setHeader("content-type", "text/xml");

			$escolas = new Escolas();

			$escolasPrepared = array();

			foreach($escolas->getEscolas() as $index):

				$index['distanciawalk'] = '0';
				$index['distanciadrive'] = '0';
				
				array_push($escolasPrepared,$index);

			endforeach;
			
			echo json_encode($escolasPrepared);

			die();
	}//end public function


	public function rankinggoogleescolasAction(){
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
	
		/* ------------   Dados recebidos  ---------------------*/

		//Latitude do futuro aluno
		$latitude = $this->getRequest()->getPost("latitude");
		//Longitude do futuro aluno
		$longitude = $this->getRequest()->getPost("longitude");

		$escolas = new Escolas();

		$rows = $escolas->getEscolas();

		$returnrows = array();

		// $latitude = '-23.318677';
		// $longitude = '-46.217165';

		$origins = $latitude . "," . $longitude;
		//$destinations = "rua luxemburgo, 51 - Jardim Adriane, Itaquaquecetuba";
		$censocentralvagas = new Censocentralvagas();
		//$censocentralvagas->getCensocentralvagasresumo("21")[0]['totalvagas'];

		foreach($rows as $index):

			$destinations = $index['latitude'] . "," . $index['longitude'];

			//$index['distancia_caminhada'] = MN_Util::getGoogleDistance($origins, $destinations, $arr_distance = false);
			$index['distancia_caminhada'] = rand(1,30);
			// 0 => Manhã
			// 1 => Tarde
			// 2 => Noite
			// 3 => Integral
			//$vagas = ViewCensovagasEscolasTotais::getViewCensovagasEscolasTotaisResumo('21');
			
			$index['manha'] = $origins; //$vagas[0][0]['sum(totalvagas)'];
			$index['tarde'] = $destinations; //$vagas[1][0]['sum(totalvagas)'];
			$index['noite'] = (string) rand(2,20); //$vagas[2][0]['sum(totalvagas)'];
			$index['integral'] = (string) rand(2,20);//$vagas[3][0]['sum(totalvagas)'];
			
			//$index['distancia_carro'] = $latitude . "," . $longitude;
			//$index['totalvagas'] = (string) $censocentralvagas->getCensocentralvagasresumo($index['id']);	
			// ViewCensovagasEscolasTotais::getViewCensovagasEscolasTotaisResumo('21')
			array_push($returnrows, $index);

		endforeach;

		echo json_encode($returnrows); // Ascending order

		die();

	}//end public function

	public function setescolaAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$ensinoid = $this->getRequest()->getPost("idescola");


		$escolas = new Escolas();

		$rows = $escolas->getEscolasByEnsino($ensinoid);

		$rowsReturn = array();

		$latitude = '-23.318677';
		$longitude = '-46.217165';

		$origins = $latitude . "," . $longitude;

		$censocentralvagas = new Censocentralvagas();

		foreach($rows as $index):

			$destinations = $index['latitude'] . "," . $index['longitude'];

			$index['distancia_caminhada'] = MN_Util::getGoogleDistance($origins, $destinations, $arr_distance = false);
			$index['distancia_carro'] = $latitude . "," . $longitude;
			$index['totalvagas'] = 'xx';//$censocentralvagas->getCensocentralvagasresumo($index['id']);	
			array_push($rowsReturn, $index);

		endforeach;

		echo json_encode($rowsReturn);

		die();

	}//end public function 

	public function setensinoAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$escola = $this->getRequest()->getPost("idescola");
		$idensino = $this->getRequest()->getPost("idescola");

		$array = array();

		$array['id'] = $escola;

		$escolas = new Escolas();

		$rows = $escolas->getEscolas($array);

		$ensinos = $rows[0]['idsensinostipos'];

		$arrayEnsinos = explode(",",$ensinos);

		$ensinos = new Escolastiposensinosgdae();

		$strRetorno = '';
		
		$censocentralvagas = new Censocentralvagas();
		$queries = array();
		$queries['idescola'] = $escola;
		//$queries['idensino'] = '6';

		$paginaAtual = 0;
		$maxpp = 0;
		$rows = $censocentralvagas->getCensocentralvagas($queries, $paginaAtual, $maxpp);	


		// foreach($rows as $index):
		// 	$strRetorno .= '<option>' .  Escolastiposensinosgdae::getTiposensinosgdaeHelper(array('id'=>$index['idensino']))[0]['descricoes'] . " - " . $index['turno'] .  '</option>';
		// endforeach;
		
		echo $rows;

		die();


	}//end method setensino

	function detalhesvagasescolasAction(){
		
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$idescola = $this->getRequest()->getPost("idescola");
		$rows = Censovagasescolasview::getviewCensovagasescolasviewByIdHelper($idescola);
			
		echo json_encode($rows); 

		die();

	}//end function detalhesvagasescolas

	public function mapsescolasAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$escolas = new Escolas();

		$rows = $escolas->getEscolasByEnsino();

		echo json_encode($rows);

		die();

	}//end function mapsescolasAction

	
}
