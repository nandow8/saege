<?php

/**
* Controle da classe ligacoesefetuadas do sistema
*
* @author		Alexandre Martin Narciso		
* @uses        Zend_Controller_Action
* @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
* @version     1.0
*/
class Admin_LigacoesefetuadasController extends Zend_Controller_Action {
	
	/**
	* Propriedade protegida que contem os dados do usário logado
	* @var Ligacaoefetuada
	*/
	protected $_usuario = null;	
	
	
	/**
	* Verificação de permissao de acesso
	*/	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("ligacoesefetuadas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}
	
	/**
	* 
	* Action para ser consultada via ajax e excluir a entidade
	*/
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Ligacoesefetuadas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ligação efetuada excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	* Action para modificar o status via Ajax
	*/
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="ligacoesefetuadas") $objs = new Ligacoesefetuadas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	*
	* Action de edição de perfil de acesso
	*/
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ligacoesefetuadas', 'name' => 'Ligações efetuadas'),
			array('url' => null,'name' => 'Visualizar Ligação efetuada')
		);
		
		$id = (int)$this->_request->getParam("id");
		$ligacoesefetuadas = new Ligacoesefetuadas();
		$ligacaoefetuada = $ligacoesefetuadas->getLigacaoefetuadaById($id, array());
		
		if (!$ligacaoefetuada) 
		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $ligacaoefetuada;
		$this->preForm();
		
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
	* Listagem
	*/
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Ligações efetuadas')
		);
		
		$ns = new Zend_Session_Namespace('default_ligacoesefetuadas');
		$ligacoesefetuadas = new Ligacoesefetuadas();
		$queries = array();	
		
		//PESQUISA
		if ($this->getRequest()->isPost()) {
			$ns->pesquisa = serialize($_POST);
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
			die();	
		}
		
		if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
		
		if (isset($this->view->post_var)) {
			foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
			
			
			
			if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
		}		
		
		//PAGINACAO
		$maxpp = 20;
		
		$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $ligacoesefetuadas->getLigacoesefetuadas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $ligacoesefetuadas->getLigacoesefetuadas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	* 
	* Action de edição de ligacoesefetuadas
	*/	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ligacoesefetuadas', 'name' => 'Ligações efetuadas'),
			array('url' => null,'name' => 'Editar Ligação efetuada')
		);	
		
		$id = (int)$this->_request->getParam("id");
		$ligacoesefetuadas = new Ligacoesefetuadas();
		$ligacaoefetuada = $ligacoesefetuadas->getLigacaoefetuadaById($id);
		
		if (!$ligacaoefetuada) 
		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $ligacaoefetuada;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($ligacaoefetuada);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ligação efetuada editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
		
		return true;		
	}  		
	
	/**
	* 
	* Action de adição de ligacoesefetuadas 
	*/
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ligacoesefetuadas', 'name' => 'Ligações efetuadas'),
			array('url' => null,'name' => 'Adicionar Ligação efetuada')
		);	
		
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Ligação efetuada adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
	}	
	
	/**
	* Atribui valores ao view
	* @param int $idligacaoefetuada
	*/    
	private function preForm($idligacaoefetuada = 0) {
	}    
	
	/**
	* Valida e grava os dados do formulário
	*/    
	private function getPost($_ligacaoefetuada = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		
		
		$localsolicitante = trim($this->getRequest()->getPost("localsolicitante"));
		$funcionario = trim($this->getRequest()->getPost("funcionario"));
		$falarcom = trim($this->getRequest()->getPost("falarcom"));
		$assunto = trim($this->getRequest()->getPost("assunto"));
		$dataefetuada = trim($this->getRequest()->getPost("dataefetuada"));
		$horaefetuada = trim($this->getRequest()->getPost("horaefetuada"));
		$telefone = trim($this->getRequest()->getPost("telefone"));
		$status = trim($this->getRequest()->getPost("status1"));
		 
		$erros = array(); 
		$ligacoesefetuadas = new Ligacoesefetuadas();
		 
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["localsolicitante"] = $localsolicitante;
			$dados["funcionario"] = $funcionario;
			$dados["falarcom"] = $falarcom;
			$dados["assunto"] = $assunto;
			$dados["dataefetuada"] = $dataefetuada;
			$dados["horaefetuada"] = $horaefetuada;
			$dados["telefone"] = $telefone;
			  
			$dados["status"] = $status;
			 
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			$row = $ligacoesefetuadas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
	}
	
	private function getArquivo($filename) {
		$idarquivo = false;
		$arquivos = new Arquivos();
		
		try {
			$idarquivo = $arquivos->getArquivoFromForm($filename);
		} catch (Exception $e) {
			$idarquivo = false;
			array_push($erros,$e->getMessage());
		}
		
		$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
		
		if ($excluir_arquivo=='excluir') $idarquivo = -1;
		
		return $idarquivo;
	}    
	
	private function getImagem($imagem, $apenas_copia = false) {
		$idimagem = false;
		$imagens = new Imagens();
		
		try {
			ini_set('memory_limit', '-1');
			$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
			
		} catch (Exception $e) {
			$idimagem = false;
			array_push($erros,$e->getMessage());
		}
		
		$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
		if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
		return $idimagem;
	}
	
}