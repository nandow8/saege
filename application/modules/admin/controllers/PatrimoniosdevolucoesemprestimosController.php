<?php

/**
 * Controle da classe patrimoniosdevolucoesemprestimos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PatrimoniosdevolucoesemprestimosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Patrimoniodevolucaoemprestimo
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("patrimoniosdevolucoesemprestimos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Patrimoniosdevolucoesemprestimos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Devolução de Empréstimo excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="patrimoniosdevolucoesemprestimos") $objs = new Patrimoniosdevolucoesemprestimos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosdevolucoesemprestimos', 'name' => 'Devoluções de Empréstimos'),
			array('url' => null,'name' => 'Visualizar Devolução de Empréstimo')
		);
		
		$id = (int)$this->_request->getParam("id");
		$patrimoniosdevolucoesemprestimos = new Patrimoniosdevolucoesemprestimos();
		$patrimoniodevolucaoemprestimo = $patrimoniosdevolucoesemprestimos->getPatrimoniodevolucaoemprestimoById($id, array());
		
		if (!$patrimoniodevolucaoemprestimo) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimoniodevolucaoemprestimo;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Devoluções de Empréstimos')
		);
		
		$ns = new Zend_Session_Namespace('default_patrimoniosdevolucoesemprestimos');
		$patrimoniosdevolucoesemprestimos = new Patrimoniosdevolucoesemprestimos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["iddepartamento"]!="") $queries["iddepartamento"] = $this->view->post_var["iddepartamento"];
if ($this->view->post_var["idpatrimonio"]!="") $queries["idpatrimonio"] = $this->view->post_var["idpatrimonio"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $patrimoniosdevolucoesemprestimos->getPatrimoniosdevolucoesemprestimos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $patrimoniosdevolucoesemprestimos->getPatrimoniosdevolucoesemprestimos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de patrimoniosdevolucoesemprestimos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosdevolucoesemprestimos', 'name' => 'Devoluções de Empréstimos'),
			array('url' => null,'name' => 'Editar Devolução de Empréstimo')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$patrimoniosdevolucoesemprestimos = new Patrimoniosdevolucoesemprestimos();
		$patrimoniodevolucaoemprestimo = $patrimoniosdevolucoesemprestimos->getPatrimoniodevolucaoemprestimoById($id);
		
		if (!$patrimoniodevolucaoemprestimo) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimoniodevolucaoemprestimo;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($patrimoniodevolucaoemprestimo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Devolução de Empréstimo editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de patrimoniosdevolucoesemprestimos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosdevolucoesemprestimos', 'name' => 'Devoluções de Empréstimos'),
			array('url' => null,'name' => 'Adicionar Devolução de Empréstimo')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Devolução de Empréstimo adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idpatrimoniodevolucaoemprestimo
     */    
    private function preForm($idpatrimoniodevolucaoemprestimo = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_patrimoniodevolucaoemprestimo = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
$horaabertura = trim($this->getRequest()->getPost("horaabertura"));
$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$idpatrimonio = (int)trim($this->getRequest()->getPost("idpatrimonio"));
$idfoto = (int)trim($this->getRequest()->getPost("idfoto"));
$nomeproduto = trim($this->getRequest()->getPost("nomeproduto"));
$corproduto = trim($this->getRequest()->getPost("corproduto"));
$marca = trim($this->getRequest()->getPost("marca"));
$tipo = trim($this->getRequest()->getPost("tipo"));
$modelo = trim($this->getRequest()->getPost("modelo"));
$caracteristica = trim($this->getRequest()->getPost("caracteristica"));
$dimensao = trim($this->getRequest()->getPost("dimensao"));
$descricaoproduto = trim($this->getRequest()->getPost("descricaoproduto"));
$datacompra = Mn_Util::stringToTime($this->getRequest()->getPost("datacompra"));
$dimensao_b = trim($this->getRequest()->getPost("dimensao_b"));
$descricaobem = trim($this->getRequest()->getPost("descricaobem"));
$codbarras = trim($this->getRequest()->getPost("codbarras"));
$qrcode = trim($this->getRequest()->getPost("qrcode"));
$idlocal = (int)trim($this->getRequest()->getPost("idlocal"));
$nomeresponsavel = trim($this->getRequest()->getPost("nomeresponsavel"));
$rgf = trim($this->getRequest()->getPost("rgf"));
$estadodevolucao = (int)trim($this->getRequest()->getPost("estadodevolucao"));
$observacao = trim($this->getRequest()->getPost("observacao"));
$responsavelrecebimento = trim($this->getRequest()->getPost("responsavelrecebimento"));
$rgf_b = trim($this->getRequest()->getPost("rgf_b"));
$idfotobem = (int)trim($this->getRequest()->getPost("idfotobem"));
$devolverbem = trim($this->getRequest()->getPost("devolverbem"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$iddepartamento) array_push($erros, "Informe a Departamento do Patrimônio.");
if (0==$idpatrimonio) array_push($erros, "Informe a N. do Patrimônio.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$patrimoniosdevolucoesemprestimos = new Patrimoniosdevolucoesemprestimos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["sequencial"] = $sequencial;
$dados["datalancamento"] = date("Y-m-d", $datalancamento);
$dados["horaabertura"] = $horaabertura;
$dados["idusuario"] = $idusuario;
$dados["iddepartamento"] = $iddepartamento;
$dados["idpatrimonio"] = $idpatrimonio;

$idfoto = $this->getImagem('idfoto');
			if ($idfoto!=0) $dados['idfoto'] = $idfoto;
$dados["nomeproduto"] = $nomeproduto;
$dados["corproduto"] = $corproduto;
$dados["marca"] = $marca;
$dados["tipo"] = $tipo;
$dados["modelo"] = $modelo;
$dados["caracteristica"] = $caracteristica;
$dados["dimensao"] = $dimensao;
$dados["descricaoproduto"] = $descricaoproduto;
$dados["datacompra"] = date("Y-m-d", $datacompra);
$dados["dimensao_b"] = $dimensao_b;
$dados["descricaobem"] = $descricaobem;
$dados["codbarras"] = $codbarras;
$dados["qrcode"] = $qrcode;
$dados["idlocal"] = $idlocal;
$dados["nomeresponsavel"] = $nomeresponsavel;
$dados["rgf"] = $rgf;
$dados["estadodevolucao"] = $estadodevolucao;
$dados["observacao"] = $observacao;
$dados["responsavelrecebimento"] = $responsavelrecebimento;
$dados["rgf_b"] = $rgf_b;

$idfotobem = $this->getImagem('idfotobem');
			if ($idfotobem!=0) $dados['idfotobem'] = $idfotobem;
$dados["devolverbem"] = $devolverbem;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $patrimoniosdevolucoesemprestimos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}