<?php

/**
 * Controle da classe nutricaomerendasocorrencias do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_NutricaomerendasocorrenciasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Nutricaomerendasocorrencia
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("nutricaomerendasocorrencias", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Nutricaomerendasocorrencias();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de Ocorrência excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="nutricaomerendasocorrencias") $objs = new Nutricaomerendasocorrencias();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendasocorrencias', 'name' => 'Controle de Ocorrências'),
			array('url' => null,'name' => 'Visualizar Controle de Ocorrência')
		);
		
		$id = (int)$this->_request->getParam("id");
		$nutricaomerendasocorrencias = new Nutricaomerendasocorrencias();
		$nutricaomerendasocorrencia = $nutricaomerendasocorrencias->getNutricaomerendasocorrenciaById($id, array());
		
		if (!$nutricaomerendasocorrencia) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaomerendasocorrencia;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Controle de Ocorrências')
		);
		
		$ns = new Zend_Session_Namespace('default_nutricaomerendasocorrencias');
		$nutricaomerendasocorrencias = new Nutricaomerendasocorrencias();
		$queries = array();	
		$queries['idescola'] = Usuarios::getUsuario('idescola');
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
if ($this->view->post_var["idusuario"]!="") $queries["idusuario"] = $this->view->post_var["idusuario"];
if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $nutricaomerendasocorrencias->getNutricaomerendasocorrencias($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $nutricaomerendasocorrencias->getNutricaomerendasocorrencias($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de nutricaomerendasocorrencias
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendasocorrencias', 'name' => 'Controle de Ocorrências'),
			array('url' => null,'name' => 'Editar Controle de Ocorrência')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$nutricaomerendasocorrencias = new Nutricaomerendasocorrencias();
		$nutricaomerendasocorrencia = $nutricaomerendasocorrencias->getNutricaomerendasocorrenciaById($id);
		
		if (!$nutricaomerendasocorrencia) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaomerendasocorrencia;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($nutricaomerendasocorrencia);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de Ocorrência editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de nutricaomerendasocorrencias 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendasocorrencias', 'name' => 'Controle de Ocorrências'),
			array('url' => null,'name' => 'Adicionar Controle de Ocorrência')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de Ocorrência adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idnutricaomerendasocorrencia
     */    
    private function preForm($idnutricaomerendasocorrencia = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_nutricaomerendasocorrencia = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
		$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idlocal = (int)trim($this->getRequest()->getPost("idlocal"));
		$idperiodo = (int)trim($this->getRequest()->getPost("idperiodo"));
		$periodos = ($this->getRequest()->getPost("periodos"));
		$motivos = ($this->getRequest()->getPost("motivos"));
		$observacao = trim($this->getRequest()->getPost("observacao"));
		$todosperiodos = trim($this->getRequest()->getPost("todosperiodos"));

		//var_dump($datalancamento);die();



		
		$erros = array();
		
		if (""==$datalancamento) array_push($erros, "Informe a Data do Lançamento.");
		if (""==$idusuario) array_push($erros, "Informe a Nome do Usuário.");
		//if (""==$idlocal) array_push($erros, "Informe o Local.");

		
		$nutricaomerendasocorrencias = new Nutricaomerendasocorrencias();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			$dados["idusuario"] = $idusuario;
			$dados["idescola"] = $idescola;
			$dados["idlocal"] = $idlocal;	
			$dados["idperiodo"] = $idperiodo;		
			$dados["periodos"] =   $periodos ;
			$dados["motivos"] =  $motivos ;
			$dados["observacao"] = $observacao;
			$dados["todosperiodos"] = $todosperiodos;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $nutricaomerendasocorrencias->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}