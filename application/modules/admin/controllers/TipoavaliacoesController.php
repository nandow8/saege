<?php

/**
 * Controle da classe tipoavaliacoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_TipoavaliacoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Tipoavaliacoes
	 */
	protected $_usuario = null;
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("tipoavaliacoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Tipoavaliacoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Tipo excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="Tipoavaliacoes") $objs = new Tipoavaliacoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tipoavaliacoes', 'name' => 'Tipos de Avaliações'),
			array('url' => null,'name' => 'Visualizar Tipos de Avaliações')
		);
		
		$id = (int)$this->_request->getParam("id");
		$tipoavaliacoes = new Tipoavaliacoes();
		$tipoavaliacoes = $tipoavaliacoes->getTipoavaliacoesById($id, array());
		
		if (!$tipoavaliacoes) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $tipoavaliacoes;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Tipos de Avaliações')
		);
		
		$ns = new Zend_Session_Namespace('default_tipoavaliacoes');
		$tipoavaliacoes = new Tipoavaliacoes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["tipoavaliacao"]!="") $queries["tipoavaliacao"] = $this->view->post_var["tipoavaliacao"];
			if ($this->view->post_var["descricao"]!="") $queries["descricao"] = $this->view->post_var["descricao"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $tipoavaliacoes->getTipoavaliacoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $tipoavaliacoes->getTipoavaliacoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de coordenacaotipo
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tipoavaliacoes', 'name' => 'Tipos de Avaliações'),
			array('url' => null,'name' => 'Editar tipos de ãvaliações')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$tipoavaliacoes = new Tipoavaliacoes();
		$tipoavaliacoes = $tipoavaliacoes->getTipoavaliacoesById($id);
		
		if (!$tipoavaliacoes) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $tipoavaliacoes;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($tipoavaliacoes);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Tipo de avaliação editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de coordenacaotipo 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tipoavaliacoes', 'name' => 'Tipos de Avaliações'),
			array('url' => null,'name' => 'Adicionar Tipo de avaliação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Tipo de avaliação adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idcoordenacaotipo
     */    
    private function preForm($idtipoavaliacoes = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_tipoavaliacoes = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$tipoavaliacao = trim($this->getRequest()->getPost("tipoavaliacao"));
		$iddisciplina = trim($this->getRequest()->getPost("iddisciplina"));
		$ano = trim($this->getRequest()->getPost("ano"));
		$qtdquestoes = trim($this->getRequest()->getPost("qtdquestoes"));
		$valorquestoes = $this->getRequest()->getPost("valorquestoes");
		$descricao = trim($this->getRequest()->getPost("descricao"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		$erros = array();
		
		if (""==$tipoavaliacao) array_push($erros, "Informe a tipo da avaliação.");
		if (""==$status) array_push($erros, "Informe a Status.");
		if(count($valorquestoes) != $qtdquestoes) array_push($erros, "Preencha os valores de todas questões");
		
		$tipoavaliacoes = new Tipoavaliacoes();		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["tipoavaliacao"] = $tipoavaliacao;
			$dados['iddisciplina'] = (int)$iddisciplina;
			$dados['ano'] = (int)$ano;
			$dados['qtdquestoes'] = (int)$qtdquestoes;

			foreach ($valorquestoes as $k => $v) {
				$valorquestoes[$k] = str_replace(',', '.', $v);
			}

			$dados['valorquestoes'] =  trim(implode(',', $valorquestoes));

			$dados["descricao"] = $descricao;
			$dados["status"] = $status;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $tipoavaliacoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }    
}