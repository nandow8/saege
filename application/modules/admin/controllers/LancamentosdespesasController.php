<?php

/**
 * Controle da classe lancamentosdespesas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_LancamentosdespesasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Lancamentosdespesa
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("lancamentosdespesas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Lancamentosdespesas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Lançamento excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="lancamentosdespesas") $objs = new Lancamentosdespesas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'lancamentosdespesas', 'name' => 'Lançamentos de despesas'),
			array('url' => null,'name' => 'Visualizar Lançamento')
		);
		
		$id = (int)$this->_request->getParam("id");
		$lancamentosdespesas = new Lancamentosdespesas();
		$lancamentosdespesa = $lancamentosdespesas->getLancamentosdespesaById($id, array());
		
		if (!$lancamentosdespesa) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $lancamentosdespesa;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}

	public function gettipobancoAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$id = (int)$this->getRequest()->getPost('idtipoconvenio');
		$tipoconvenio = Financeirotiposconvenios::getFinanceirotipoconvenioByIdHelper($id);
		
		$queries = array();
		$queries['tipo'] = $tipoconvenio['titulo'];
		 
		$escolasapmscontas = new Escolasapmscontas();
		$rows = $escolasapmscontas->getEscolasapmscontasHelper($queries);


		echo json_encode($rows);
		die();

	}
	
	public function gerapdfAction() {
		$id = (int)$this->_request->getParam("id");
		$idescola = (int)$this->_request->getParam("idescola");
		$declaracoes = new Rhdeclaracoesdependentes();
		$declaracao = $declaracoes->getRhdeclaracaodependenteById(1);

		$apm = Escolasapms::getEscolaapmByIdHelper(1, array('status' => 'Ativo'));
		$composicao_apm = Escolasapmsgrupos::getEscolasapmsgrupoByIdHelper(1, array('status' => 'Ativo'));
		
		if (!$composicao_apm) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());

		$this->view->apm = $apm;
		$this->view->composicao_apm = $composicao_apm;

		$escola = Escolas::getEscolaByIdHelper($apm['idescola']);
		$this->view->escola = $escola;


		$conta = Escolasapmscontas::getApmByIdParentHelper($apm['id'], array('idescola' =>$escola['id']));
		if($conta){
			unset($conta['id']);
			unset($conta['excluido']);
			$this->view->apm = array_merge($apm, $conta);
		}
		
		//$this->view->post_var = $declaracao;
		$endereco = Enderecos::getEnderecoById((int)$escola['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->endereco = $endereco;
		}
		
		$this->view->receitas = Financeirolancamentosreceitas::getFinanceirolancamentosreceitasHelper(array('idescola'=>$escola['id']));
		$this->view->despesas = Lancamentosdespesas::getLancamentosdespesasHelper(array('idescola'=>$escola['id']));
		$this->preForm();
		//var_dump($this->view->despesas); die();
		

		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('p');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('Diploma');
		$pdf->SetSubject('Diploma');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		
		
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('lancamentosdespesas/pdf/index.phtml');
		//echo $html; die();

		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'declaracao_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}


	public function gerapddeAction() {
		$id = (int)$this->_request->getParam("id");
		$idescola = (int)$this->_request->getParam("idescola");
		$declaracoes = new Rhdeclaracoesdependentes();
		$declaracao = $declaracoes->getRhdeclaracaodependenteById(1);

		$apm = Escolasapms::getEscolaapmByIdHelper(1, array('status' => 'Ativo'));
		$composicao_apm = Escolasapmsgrupos::getEscolasapmsgrupoByIdHelper(1, array('status' => 'Ativo'));
		
		if (!$composicao_apm) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());

		$this->view->apm = $apm;
		$this->view->composicao_apm = $composicao_apm;

		$escola = Escolas::getEscolaByIdHelper($apm['idescola']);
		$this->view->escola = $escola;


		$conta = Escolasapmscontas::getApmByIdParentHelper($apm['id'], array('idescola' =>$escola['id']));
		if($conta){
			unset($conta['id']);
			unset($conta['excluido']);
			$this->view->apm = array_merge($apm, $conta);
		}
		
		//$this->view->post_var = $declaracao;
		$endereco = Enderecos::getEnderecoById((int)$escola['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->endereco = $endereco;
		}
		
		$this->view->receitas = Financeirolancamentosreceitas::getFinanceirolancamentosreceitasHelper(array('idescola'=>$escola['id']));
		$this->view->despesas = Lancamentosdespesas::getLancamentosdespesasHelper(array('idescola'=>$escola['id']));
		$this->preForm();
		//var_dump($this->view->despesas); die();
		

		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('p');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('Diploma');
		$pdf->SetSubject('Diploma');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		
		
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('lancamentosdespesas/pdf/pdde.phtml');
		//echo $html; die();

		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'declaracao_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}

	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Lançamentos de despesas')
		);
		
		$ns = new Zend_Session_Namespace('default_lancamentosdespesas');
		$lancamentosdespesas = new Lancamentosdespesas();
		$queries = array();	
		$queries['idescola'] = Usuarios::getUsuario('idescola');
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idtipoconvenio"]!="") $queries["idtipoconvenio"] = $this->view->post_var["idtipoconvenio"];
//if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
if ($this->view->post_var["valor"]!="") $queries["valor"] = $this->view->post_var["valor"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $lancamentosdespesas->getLancamentosdespesas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $lancamentosdespesas->getLancamentosdespesas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de lancamentosdespesas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'lancamentosdespesas', 'name' => 'Lançamentos de despesas'),
			array('url' => null,'name' => 'Editar Lançamento')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$lancamentosdespesas = new Lancamentosdespesas();
		$lancamentosdespesa = $lancamentosdespesas->getLancamentosdespesaById($id);
		
		if (!$lancamentosdespesa) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $lancamentosdespesa;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($lancamentosdespesa);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Lançamento editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de lancamentosdespesas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'lancamentosdespesas', 'name' => 'Lançamentos de despesas'),
			array('url' => null,'name' => 'Adicionar Lançamento')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Lançamento adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idlancamentosdespesa
     */    
    private function preForm($idlancamentosdespesa = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_lancamentosdespesa = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$origem = trim($this->getRequest()->getPost("origem"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$idlocal = (int)trim($this->getRequest()->getPost("idlocal"));
$idtipoconvenio = (int)trim($this->getRequest()->getPost("idtipoconvenio"));
$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
$sequencial = trim($this->getRequest()->getPost("sequencial"));
$banco = trim($this->getRequest()->getPost("banco"));
$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
$idtipodespesa = (int)trim($this->getRequest()->getPost("idtipodespesa"));
$fornecedor = trim($this->getRequest()->getPost("fornecedor"));
$cnpj = trim($this->getRequest()->getPost("cnpj"));
$idbanco = (int)trim($this->getRequest()->getPost("idbanco"));
$descricaodespesa = trim($this->getRequest()->getPost("descricaodespesa"));
$numerocheque = trim($this->getRequest()->getPost("numerocheque"));
$idcheque = (int)trim($this->getRequest()->getPost("idcheque"));
$tipodocumento = trim($this->getRequest()->getPost("tipodocumento"));
$numerodocumento = trim($this->getRequest()->getPost("numerodocumento"));
$iddocumento = (int)trim($this->getRequest()->getPost("iddocumento"));
$valor = MN_Util::trataNum(trim($this->getRequest()->getPost("valor")));
$observacao = trim($this->getRequest()->getPost("observacao"));
$status = trim($this->getRequest()->getPost("status1"));
	
$___rows = Escolasapmscontas::getEscolasapmscontasHelper(array('id'=>$idbanco));
$___row = (sizeof($___rows)>0) ? 	$___rows[0] : false;	

$banco = ($___row) ? $___row['banco'] : '--';		
$agencia = ($___row) ? $___row['agencia'] : '--';		
$conta = ($___row) ? $___row['conta'] : '--';	
		
		$erros = array();
		
if (0==$idbanco) array_push($erros, "Informe a Banco.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$lancamentosdespesas = new Lancamentosdespesas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["origem"] = $origem;
$dados["idescola"] = $idescola;
$dados["idlocal"] = $idlocal;
$dados["idtipoconvenio"] = $idtipoconvenio;
$dados["idusuariologado"] = $idusuariologado;
$dados["sequencial"] = $sequencial;
$dados["banco"] = $banco;
$dados["data"] = date("Y-m-d", $data);
$dados["idtipodespesa"] = $idtipodespesa;
$dados["fornecedor"] = $fornecedor;
$dados["cnpj"] = $cnpj;
$dados["idbanco"] = $idbanco;
$dados["descricaodespesa"] = $descricaodespesa;
$dados["numerocheque"] = $numerocheque;

$idcheque = $this->getArquivo('idcheque');
			if ($idcheque!=0) $dados['idcheque'] = $idcheque;
$dados["tipodocumento"] = $tipodocumento;
$dados["numerodocumento"] = $numerodocumento;

$iddocumento = $this->getArquivo('iddocumento');
			if ($iddocumento!=0) $dados['iddocumento'] = $iddocumento;
$dados["valor"] = $valor;
$dados["observacao"] = $observacao;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $lancamentosdespesas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}