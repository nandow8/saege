<?php

/**
 * Controle da classe patrimoniosdepreciacoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PatrimoniosdepreciacoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Patrimoniosdepreciacao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("patrimoniosdepreciacoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Patrimoniosdepreciacoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Depreciação excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="patrimoniosdepreciacoes") $objs = new Patrimoniosdepreciacoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosdepreciacoes', 'name' => 'Depreciações'),
			array('url' => null,'name' => 'Visualizar Depreciação')
		);
		
		$id = (int)$this->_request->getParam("id");
		$patrimoniosdepreciacoes = new Patrimoniosdepreciacoes();
		$patrimoniosdepreciacao = $patrimoniosdepreciacoes->getPatrimoniosdepreciacaoById($id, array());
		
		if (!$patrimoniosdepreciacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimoniosdepreciacao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Depreciações')
		);
		
		$ns = new Zend_Session_Namespace('default_patrimoniosdepreciacoes');
		$patrimoniosdepreciacoes = new Patrimoniosdepreciacoes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idcodbem"]!="") $queries["idcodbem"] = $this->view->post_var["idcodbem"];
if ($this->view->post_var["nomebem"]!="") $queries["nomebem"] = $this->view->post_var["nomebem"];
if ($this->view->post_var["idpatrimonio"]!="") $queries["idpatrimonio"] = $this->view->post_var["idpatrimonio"];
if ($this->view->post_var["datacompra_i"]!="") $queries["datacompra_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datacompra_i"]));
if ($this->view->post_var["datacompra_f"]!="") $queries["datacompra_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datacompra_f"]));
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $patrimoniosdepreciacoes->getPatrimoniosdepreciacoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $patrimoniosdepreciacoes->getPatrimoniosdepreciacoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de patrimoniosdepreciacoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosdepreciacoes', 'name' => 'Depreciações'),
			array('url' => null,'name' => 'Editar Depreciação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$patrimoniosdepreciacoes = new Patrimoniosdepreciacoes();
		$patrimoniosdepreciacao = $patrimoniosdepreciacoes->getPatrimoniosdepreciacaoById($id);
		
		if (!$patrimoniosdepreciacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimoniosdepreciacao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($patrimoniosdepreciacao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Depreciação editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de patrimoniosdepreciacoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniosdepreciacoes', 'name' => 'Depreciações'),
			array('url' => null,'name' => 'Adicionar Depreciação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Depreciação adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idpatrimoniosdepreciacao
     */    
    private function preForm($idpatrimoniosdepreciacao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_patrimoniosdepreciacao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idcodbem = (int)trim($this->getRequest()->getPost("idcodbem"));
$nomebem = trim($this->getRequest()->getPost("nomebem"));
$idpatrimonio = (int)trim($this->getRequest()->getPost("idpatrimonio"));
$datacompra = Mn_Util::stringToTime($this->getRequest()->getPost("datacompra"));
$valorcompra = MN_Util::trataNum(trim($this->getRequest()->getPost("valorcompra")));
$depreciacaomensal = trim($this->getRequest()->getPost("depreciacaomensal"));
$depreciacaoacumulada = trim($this->getRequest()->getPost("depreciacaoacumulada"));
$valorresidual = MN_Util::trataNum(trim($this->getRequest()->getPost("valorresidual")));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idcodbem) array_push($erros, "Informe a Cod. Bem.");
if (""==$nomebem) array_push($erros, "Informe a Nome do Bem.");
if (0==$idpatrimonio) array_push($erros, "Informe a N. Patrimônio.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$patrimoniosdepreciacoes = new Patrimoniosdepreciacoes();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idcodbem"] = $idcodbem;
$dados["nomebem"] = $nomebem;
$dados["idpatrimonio"] = $idpatrimonio;
$dados["datacompra"] = date("Y-m-d", $datacompra);
$dados["valorcompra"] = $valorcompra;
$dados["depreciacaomensal"] = $depreciacaomensal;
$dados["depreciacaoacumulada"] = $depreciacaoacumulada;
$dados["valorresidual"] = $valorresidual;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $patrimoniosdepreciacoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}