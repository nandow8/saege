<?php

/**
 * Controle da classe agendafuncionariosexternos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_AgendafuncionariosexternosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Agendafuncionarioexterno
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("agendafuncionariosexternos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Agendafuncionariosexternos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Contato excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="agendafuncionariosexternos") $objs = new Agendafuncionariosexternos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'agendafuncionariosexternos', 'name' => 'Contatos'),
			array('url' => null,'name' => 'Visualizar Contato')
		);
		
		$id = (int)$this->_request->getParam("id");
		$agendafuncionariosexternos = new Agendafuncionariosexternos();
		$agendafuncionarioexterno = $agendafuncionariosexternos->getAgendafuncionarioexternoById($id, array());
		
		if (!$agendafuncionarioexterno) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $agendafuncionarioexterno;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Contatos')
		);
		
		$ns = new Zend_Session_Namespace('default_agendafuncionariosexternos');
		$agendafuncionariosexternos = new Agendafuncionariosexternos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["iddepartamento"]!="") $queries["iddepartamento"] = $this->view->post_var["iddepartamento"];
if ($this->view->post_var["nomerazao"]!="") $queries["nomerazao"] = $this->view->post_var["nomerazao"];
if ($this->view->post_var["telefone"]!="") $queries["telefone"] = $this->view->post_var["telefone"];
if ($this->view->post_var["funcionario"]!="") $queries["funcionario"] = $this->view->post_var["funcionario"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $agendafuncionariosexternos->getAgendafuncionariosexternos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $agendafuncionariosexternos->getAgendafuncionariosexternos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de agendafuncionariosexternos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'agendafuncionariosexternos', 'name' => 'Contatos'),
			array('url' => null,'name' => 'Editar Contato')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$agendafuncionariosexternos = new Agendafuncionariosexternos();
		$agendafuncionarioexterno = $agendafuncionariosexternos->getAgendafuncionarioexternoById($id);
		
		if (!$agendafuncionarioexterno) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $agendafuncionarioexterno;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($agendafuncionarioexterno);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Contato editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de agendafuncionariosexternos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'agendafuncionariosexternos', 'name' => 'Contatos'),
			array('url' => null,'name' => 'Adicionar Contato')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Contato adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function setusuariosAction() {
		$this->_helper->layout->disableLayout();
		
		$iddepartamento = (int)$this->_request->getPost("iddepartamento");

		if((int)$iddepartamento <= 0) return $this->view->rows = false;

		$rows = new Usuarios();
		$queries = array();	
		$queries['iddepartamento'] = $iddepartamento;
		 $queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$this->view->rows = $rows->getUsuarios($queries);	
		
	}

    /**
     * Atribui valores ao view
     * @param int $idagendafuncionarioexterno
     */    
    private function preForm($idagendafuncionarioexterno = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_agendafuncionarioexterno = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
		$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
		$nomerazao = trim($this->getRequest()->getPost("nomerazao"));
		$sobrenomefantasia = trim($this->getRequest()->getPost("sobrenomefantasia"));
		$telefone = trim($this->getRequest()->getPost("telefone"));
		$celular = trim($this->getRequest()->getPost("celular"));
		$funcionario = trim($this->getRequest()->getPost("funcionario"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		if($funcionario=="Sim"){
			if (0>=$iddepartamento) array_push($erros, "Selecione um departamento.");
			if (0>=$idusuario) array_push($erros, "Selecione um funcionário.");			
		}else{
			if (""==$nomerazao) array_push($erros, "Informe a Nome.");
			if (""==$sobrenomefantasia) array_push($erros, "Informe a Sobrenome.");			
		}

		if (""==$telefone) array_push($erros, "Informe a Telefone.");

		
		$agendafuncionariosexternos = new Agendafuncionariosexternos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["iddepartamento"] = $iddepartamento;
			$dados["idusuario"] = $idusuario;
			$dados["nomerazao"] = $nomerazao;
			$dados["sobrenomefantasia"] = $sobrenomefantasia;
			$dados["telefone"] = $telefone;
			$dados["celular"] = $celular;
			$dados["funcionario"] = $funcionario;
			$dados["status"] = 'Ativo';

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $agendafuncionariosexternos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}