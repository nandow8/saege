<?php

class MYPDF extends TCPDF {

	//Page header
	public function Header(){
		$image_file = "public/admin/imagens/logosantaisabel.jpg";
		$this->Image($image_file, 45, 5, 20, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        $this->SetFont('helvetica', '', 5);
        $this->SetXY(43, 26);
        $this->Cell(0, 0, 'Paraíso da Grande São Paulo', 0, false, 'L', 0, '', 0, false, 'M', 'M');
       
        // cabeçalho Endereço 
        $this->SetFont('helvetica', '', 12);
        $this->SetXY(124, 6);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Municipio de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', 'B', 10);
        $this->SetXY(120, 10);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Secretaria Municipal de Educação', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(115, 14);
        $this->SetTextColor(130, 130, 130);
		$this->Cell(0, 0, 'Tel. 4656-2440 e-mail: sec.educacao@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 8);
        $this->SetXY(115, 18);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297-Centro - CEP: 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$this->SetFont('helvetica', '', 12);
        $this->SetXY(128, 24);
        $this->SetTextColor(0, 0, 0);
        $this->Cell(0, 0, 'Estoque Solicitações', 0, false, 'L', 0, '', 0, false, 'M', 'M');


        //informações Saege
        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

       	$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();

        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60,18);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: '.$data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 10);
        $this->SetXY(40,21);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
        	

	}

	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
        $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Position at 15 mm from bottom
		$this->SetXY(150,-15);
		// Set font
		$this->SetFont('helvetica', 'I', 8);
		// Page number
		$this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
	}
}

class Admin_AlmoxarifadosolicitacoesController extends Zend_Controller_Action {
    
    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Almoxarifadosolicitacaoprodutos
     */
    protected $_usuario = null; 
    
    
    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
        Mn_Util::blockAccess("almoxarifadosolicitacoes", $this->_request->getActionName()); 
        
        $this->_usuario = unserialize($loginNameSpace->usuario);
        
        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->_request->getPost("id");
        
        $rows = new Almoxarifadosolicitacoes();
        $row = $rows->fetchRow("id=".$id);
        
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');          
            
            $rows->save($row);
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Solicitação excluída com sucesso.";
            
            die("OK");
        }
        
        die("Não encontrado!");
    }       
    
    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");
        
        if ($op=="almoxarifadosolicitacoes") $objs = new Almoxarifadosolicitacoes();
        $obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');
            
            $objs->save($obj);

            die($obj['status']);
        }
        
        die("Não encontrado!");
    }       
    
    
    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadosolicitacoes', 'name' => 'SolicitacoesProdutos'),
            array('url' => null,'name' => 'Visualizar Solicitação')
        );  
        
        $id = (int)$this->_request->getParam("id"); 
        $idfuncionario = (int)$this->_request->getParam("idfuncionario"); 
        $codigosolicitacao = (int)$this->_request->getParam("codigosolicitacao"); 

        $almoxarifadosolicitacoes = new Almoxarifadosolicitacoesentradalistadeprodutos();
        $almoxarifadosolicitacaoprodutos = $almoxarifadosolicitacoes->getAlmoxarifadosolicitacoesentradalistadeprodutosById(
            $idfuncionario, array(
                'idfuncionario' => $idfuncionario,
                'codigosolicitacao' => $codigosolicitacao
                )
            );
       
        $solicitacoes = new Almoxarifadosolicitacoes();
        $solicitacaoproduto = $solicitacoes->getSolicitacoesProduto(array('id' => $id));
        
        if (!$almoxarifadosolicitacaoprodutos) 
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
                 
        $this->view->post_var = $almoxarifadosolicitacaoprodutos;
            
        array_push( $this->view->post_var , $solicitacaoproduto);
        
        $this->view->visualizar = true; 
    }
    
    public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Almoxarifado Solicitacoes')
		);
	
		$ns = new Zend_Session_Namespace('default_almoxarifadosolicitacoes');
		$almoxarifadosolicitacoes = new Almoxarifadosolicitacoes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		// foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
		
        if ($this->view->post_var["idperfilsolicitante"]!="") $queries["idperfilsolicitante"] = $this->view->post_var["idperfilsolicitante"];
        if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
        if ($this->view->post_var["idsolicitante"]!="") $queries["idsolicitante"] = $this->view->post_var["idsolicitante"];
        if ($this->view->post_var["idperfilbusca"]!="") $queries["idperfilbusca"] = $this->view->post_var["idperfilbusca"];
        if ($this->view->post_var["idfuncionarioresposta"]!="") $queries["idfuncionarioresposta"] = $this->view->post_var["idfuncionarioresposta"];
        // if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
        if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
        if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
        if ($this->view->post_var["status1"]!="") $queries["status1"] = $this->view->post_var["status1"];
    		
    		// if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
        
        $idperfil = Usuarios::getUsuario();
        $buscaidescola = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idescola'];   // departamento não pode ter idescola(sempre 0)
		
		if($idperfil['idperfil'] != 32 AND $idperfil['idperfil'] != 25 AND $idperfil['idperfil'] != 30)
            $queries['idescola'] = $buscaidescola;
        
        if($buscaidescola == 0 AND $idperfil['idperfil'] != 32 AND $idperfil['idperfil'] != 25  AND $idperfil['idperfil'] != 30){
            $queries['idperfil'] = $idperfil['idperfil'];
            $queries['idescola'] = null;
        } 
        

		$queries['total'] = true;
		$totalRegistros = $almoxarifadosolicitacoes->getAlmoxarifadosolicitacoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $almoxarifadosolicitacoes->getAlmoxarifadosolicitacoes($queries, $paginaAtual, $maxpp);	
    }
    
    
    /**
     * 
     * Action de edição de almoxarifadosolicitacoes
     */ 
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadosolicitacoes', 'name' => 'SolicitacoesProdutos'),
            array('url' => null,'name' => 'Editar Solicitação')
        );  
        
        $id = (int)$this->_request->getParam("id"); 
        $idfuncionario = (int)$this->_request->getParam("idfuncionario"); 
        $codigosolicitacao = (int)$this->_request->getParam("codigosolicitacao"); 

        $almoxarifadosolicitacoes = new Almoxarifadosolicitacoesentradalistadeprodutos();
        $almoxarifadosolicitacaoprodutos = $almoxarifadosolicitacoes->getAlmoxarifadosolicitacoesentradalistadeprodutosById(
            $idfuncionario, array(
                'idfuncionario' => $idfuncionario,
                'codigosolicitacao' => $codigosolicitacao
                )
            );
        
        $solicitacoes = new Almoxarifadosolicitacoes();
        $solicitacaoproduto = $solicitacoes->getSolicitacoesProduto(array('id' => $id));
            
            if (!$almoxarifadosolicitacaoprodutos) 
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            
            $this->view->post_var = $almoxarifadosolicitacaoprodutos;
            
            array_push( $this->view->post_var , $solicitacaoproduto);
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($almoxarifadosolicitacaoprodutos);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Solicitação editada com sucesso.";
            
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }   

        return true;        
    }       
    
    /**
     * 
     * Action de adição de almoxarifadosolicitacoes 
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadosolicitacoes', 'name' => 'Produtos'),
            array('url' => null,'name' => 'Adicionar Produto')
        );

        $usuario = Usuarios::getUsuario();

        $idfuncionario = $usuario['idfuncionario'];
        $idperfil = $usuario['idperfil'];
        $idescola = $usuario['idescola'];

        $almoxarifadoestoques = new Almoxarifadosolicitacoes();
        $queries = array('idfuncionario' => $idfuncionario, 'idperfil' => $idperfil, 'idescola' => $idescola);
        $estoque = $almoxarifadoestoques->getEstoque($queries);

        $estoqueprodutos = new Almoxarifadoestoqueprodutos();

        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Produto adicionado com sucesso.";
            
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }
        
        return true;        
    }

    public function findprodutosAction() {
        $this->_helper->layout->disableLayout();

        $array = array();

        $idproduto = (int)$this->getRequest()->getPost('idproduto');
        $codigoproduto = strip_tags(trim($this->getRequest()->getPost('codigoproduto')));
        $categoria = (int)$this->getRequest()->getPost('categoria');
        $unidademedida = (int)$this->getRequest()->getPost('unidademedida');
        
        // $idlocalestoque = (int)$this->getRequest()->getPost('idlocalestoque');
        $idlocal = (int)$this->getRequest()->getPost('idlocal');
        $idsolicitante = (int)$this->getRequest()->getPost('idsolicitante');
        $produto = strip_tags(trim($this->getRequest()->getPost('produto')));
        $descricao = strip_tags(trim($this->getRequest()->getPost('descricao')));

        if ($idproduto <> 0 && !is_null($idproduto)) $array['id'] = $idproduto;
        if ($codigoproduto != '' && !is_null($codigoproduto)) $array['codigoproduto'] = $codigoproduto;
        if ($produto != '' && !is_null($produto)) $array['produto'] = $produto;
        if ($descricao != '' && !is_null($descricao)) $array['descricoes'] = $descricao;
        if ($categoria <> 0 && !is_null($categoria)) $array['idcategoria'] = $categoria;

        $produtos = new Almoxarifadoprodutos();

        if (sizeof($array) == 0) {
            $this->view->rows = array("erro" => "erro");
        } else {
            if ($unidademedida <> 0 && !is_null($unidademedida)) $array['idunidademedida'] = $unidademedida;
            $this->view->rows = $produtos->getAlmoxarifadoprodutos($array);
        }
    }
    
    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_almoxarifadosolicitacaoprodutos = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int)$this->getRequest()->getPost("id");
        $ids = $this->getRequest()->getPost('id_selecionado');
        $qtds = $this->getRequest()->getPost('qtd_selecionada'); 
        $idlocal = $this->getRequest()->getPost('idlocal');
        $idperfil = (int)$this->getRequest()->getPost('idperfil');
        $idperfilsolicitante = (int)$this->getRequest()->getPost('idperfilsolicitante');
        $idfuncionarioresposta = $this->getRequest()->getPost('idfuncionarioresposta');
        $idescola = (int)$this->getRequest()->getPost('idescola');
        $idsolicitante = $this->getRequest()->getPost('idsolicitante');
        $idsolicitanteescola = $this->getRequest()->getPost('idsolicitanteescola');
        $autorizacaonumero = $this->getRequest()->getPost('autorizacaonumero'); 
        $observacaoentradaEdit = $this->getRequest()->getPost('observacaoentrada'); 
        $entradaprodutos = $this->getRequest()->getPost('entradaprodutos');    // envia para entrada de produtos diretamente
        $idescolaPostVar = $this->getRequest()->getPost('idescolaPostVar');
        $idperfilPostVar = $this->getRequest()->getPost('idperfilPostVar');
        
        $id_produtos = $this->getRequest()->getPost('id_produtos'); 
        $produto = $this->getRequest()->getPost('produto');
        $qtd_selecionada = $this->getRequest()->getPost('qtd_selecionada');
        $qtd_liberado = $this->getRequest()->getPost('qtd_liberado');
        $unidademedida = $this->getRequest()->getPost('unidademedida');
        $codigoproduto = $this->getRequest()->getPost('codigoproduto');
        $motivoliberacao = $this->getRequest()->getPost('motivoliberacao');
        $codigosolicitacaoEdit = $this->getRequest()->getPost('codigosolicitacao');
        
        
        $checkboxurgencia  = array(); 
        $observacaoentradas = array(); 
        for ($i =0; $i < count($codigoproduto) ; $i++) { 
            $checkboxurgencia[$i] = $this->getRequest()->getPost("urgencia$i");
            $observacaoentradas[$i] = $this->getRequest()->getPost("observacaoentrada$i");
        }
        $urgencia = $checkboxurgencia;
        $observacaoentrada = $observacaoentradas;
// echo '<pre>'; print_r($observacaoentrada); die; 
        $status = $this->getRequest()->getPost('status'); 
        $itens = array_combine($ids, $qtds); 
        $id = (int)$this->getRequest()->getPost("id");
        
        $erros = array();
                
        $almoxarifadosolicitacoes = new Almoxarifadosolicitacoes();
        $almoxarifadosolicitacoesentradalistadeprodutos = new Almoxarifadosolicitacoesentradalistadeprodutos();

        $almoxarifadosolicitacoesmateriaisentrada = new Almoxarifadosolicitacoesmateriaisentrada(); 
		$almoxarifadoprodutossaidas = new Almoxarifadoprodutossaidas();

        if (sizeof($erros)>0) return $erros;
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dadosEnviaEstoque = array();
            $dadosProdutos = array();

            $dadosEntrada = array();
			$dadosProdutosEntrada = array();
            $dadosSalvaEntradaParaSaidaProdutos = array();
            
            $dados['id'] = $id;
            
            $codigosolicitacao =  date('sidmy') . $idsolicitante;
              

            $dados['idlocal'] = $idlocal;

            
            $dados['autorizacaonumero'] = $autorizacaonumero;
            $dados['urgencia'] = 'urgencia';
            
            $dados["status"] = 'Em Análise';
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            $dados['logdata'] = date('Y-m-d G:i:s');

            $idperfilUsuarioLogado = Usuarios::getUsuario('idperfil');
            
            if($idperfilUsuarioLogado == 32 OR $idperfilUsuarioLogado == 25){  //32 Almoxarifado  25 Nutrição
                if( empty($entradaprodutos) ){  
                    $dados['verificasolicitacao'] = 'respondido';   
                    $dados["status"] = 'Respondido'; 
                }
                $dados['idusuario'] = $idsolicitanteescola; 
            } else {
                if(empty($dados['verificasolicitacao']) AND empty($dados['status'])){    
                    $dados['verificasolicitacao'] = 'solicitado';    
                    $dados["status"] = 'Em Análise';
                }
                $dados['idescola'] = $idescola;  
                $dados['idusuario'] = $idsolicitante; 
                $dados['idperfil'] = $idperfil;
                $dados['idperfilsolicitante'] = $idperfilsolicitante;
                if(empty(Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper( $idsolicitante)['idescola']))
                    $dados['idperfilbusca'] = $idperfil; 
                
                if( !empty($entradaprodutos) ){  
                    $dadosEnviaEstoque['id'] =  $id;    
                    $dadosEnviaEstoque["status"] = 'Concluido';
                    $dadosEnviaEstoque['verificasolicitacao'] = 'concluido';   
                }   
            }  
             
            $dados['codigosolicitacao'] = $codigosolicitacao;
            
            $dadosProdutos['tiposolicitacao'] = 'solicitacao';
            if(empty($entradaprodutos)){ // enquando não envia para entrada(BOTAO Entrada Estoque), cai nesse if
                if(count($itens) > 0 AND ($idperfilUsuarioLogado != 32 AND $idperfilUsuarioLogado != 25)){ //32 almoxarifado  25 Nutrição
                    for ($i=0; $i < count( $itens ) ;$i++) {  
                        $dadosProdutos['id'] = $id_produtos[$i];
                        $dadosProdutos['idfuncionario'] = $idsolicitante;
                        $dadosProdutos['produto'] = $produto[$i];
                        $dadosProdutos['unidademedida'] = $unidademedida[$i];
                        $dadosProdutos['qtd_selecionada'] = $qtd_selecionada[$i];
                        $dadosProdutos['qtd_liberado'] = $qtd_selecionada[$i];
                        $dadosProdutos['codigoproduto'] = $codigoproduto[$i];
                        $dadosProdutos['codigosolicitacao'] = $codigosolicitacao;
                        $dadosProdutos['urgencia'] = $urgencia[$i]; 
                        $dadosProdutos['idescola'] = $idescola;
                        $dadosProdutos['idperfil'] = $idperfil;
                        $dadosProdutos['observacaoentrada'] = $observacaoentrada[$i];
                        $dadosProdutos['excluido'] = 'nao';
                        $dadosProdutos['logusuario'] = $this->_usuario['id'];
                        $dadosProdutos['logdata'] = date('Y-m-d G:i:s');
                        // echo '<pre>'; print_r($dadosProdutos); 
                        $row = $almoxarifadosolicitacoesentradalistadeprodutos->save($dadosProdutos);
                    }
                }

                if(count($id_produtos) > 0 AND ($idperfilUsuarioLogado != 32 AND $idperfilUsuarioLogado != 25)){ //32 almoxarifado  25 Nutrição
                    for ($i=0; $i < count( $id_produtos ) ;$i++) { 
                        $dadosProdutos['id'] = $id_produtos[$i];
                        $dadosProdutos['idfuncionario'] = $idsolicitante;
                        $dadosProdutos['produto'] = $produto[$i];
                        $dadosProdutos['unidademedida'] = $unidademedida[$i];
                        $dadosProdutos['qtd_selecionada'] = $qtd_selecionada[$i];
                        $dadosProdutos['qtd_liberado'] = $qtd_selecionada[$i];
                        $dadosProdutos['codigoproduto'] = $codigoproduto[$i];
                        $dadosProdutos['codigosolicitacao'] = $codigosolicitacao;
                        $dadosProdutos['urgencia'] = $urgencia[$i + 1];
                        $dadosProdutos['observacaoentrada'] = $observacaoentradaEdit[$i];
                        
                        $dadosProdutos['excluido'] = 'nao';
                        $dadosProdutos['logusuario'] = $this->_usuario['id'];
                        $dadosProdutos['logdata'] = date('Y-m-d G:i:s');
                        
                        $row = $almoxarifadosolicitacoesentradalistadeprodutos->save($dadosProdutos);
                    }
                }
                
                if($idperfilUsuarioLogado == 32 OR $idperfilUsuarioLogado == 25) { //32 almoxarifado  25 Nutrição
                    $restricao = null;            
                    for ($i=0; $i < count( $id_produtos ) ;$i++) { 

                        if($qtd_selecionada[$i] >= $qtd_liberado[$i])
                            $restricao = "Liberado com Restrição";

                        if($qtd_selecionada[$i] <= $qtd_liberado[$i])
                            $restricao = "Liberado";    
                        
                        if($qtd_liberado[$i] == 0)
                            $restricao = "Reprovado";

                        $dados['codigosolicitacao'] = $codigosolicitacaoEdit;
                        $dadosProdutos['id'] = $id_produtos[$i];
                        $dadosProdutos['idfuncionario'] = $idsolicitante;
                        $dadosProdutos['idfuncionarioresposta'] = $idfuncionarioresposta;
                        $dados['idfuncionarioresposta'] = $idfuncionarioresposta;
                        $dadosProdutos['produto'] = $produto[$i];
                        $dadosProdutos['unidademedida'] = $unidademedida[$i];
                        $dadosProdutos['qtd_selecionada'] = $qtd_selecionada[$i];
                        $dadosProdutos['qtd_liberado'] = $qtd_liberado[$i];
                        $dadosProdutos['codigoproduto'] = $codigoproduto[$i];
                        $dadosProdutos['motivoliberacao'] = $motivoliberacao[$i];
                        $dadosProdutos['codigosolicitacao'] = $codigosolicitacaoEdit;
                        $dadosProdutos['observacaoentrada'] = $observacaoentradaEdit[$i];
                        $dadosProdutos["status"] = $restricao;
                        $dadosProdutos['excluido'] = 'nao';
                        $dadosProdutos['logusuario'] = $this->_usuario['id'];
                        $dadosProdutos['logdata'] = date('Y-m-d G:i:s');
                        
                        $row = $almoxarifadosolicitacoesentradalistadeprodutos->save($dadosProdutos);
                    }     
                }      
                $row = $almoxarifadosolicitacoes->save($dados);

            }else { // envia para entrada de produtos
                 
                $dadosEntrada['idusuario'] = $idsolicitante;
                $dadosEntrada['idperfil'] = $idperfilPostVar;
                if(empty(Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper( $idsolicitante)['idescola']))
                    $dadosEntrada['idperfilbusca'] = $idperfilPostVar; 
               
                     
                $dadosEntrada['data'] =  date("Y-m-d");
                $dadosEntrada["observacao"] = $observacao;
                $dadosEntrada["status"] = 'Ativo';
                $dadosEntrada['idlocal'] = $idsolicitante; 
                $dadosEntrada['idescola'] = $idescolaPostVar;   
                $dadosEntrada['codigosolicitacao'] = $codigosolicitacaoEdit;    
    
                $dadosEntrada['excluido'] = 'nao';
                $dadosEntrada['logusuario'] = $this->_usuario['id'];;
                $dadosEntrada['logdata'] = date('Y-m-d G:i:s');
                 
               
                $dadosProdutos['tiposolicitacao'] = 'entrada';
                $db = Zend_Registry::get('db');
                for ($i=0; $i < count( $id_produtos ) ;$i++) {  
                    $dadosProdutos['id'] = $id_produtos[$i];
                    $dadosProdutos['tiposolicitacao'] = 'entrada'; 
                    $dadosProdutos['codigoproduto'] = $codigoproduto[$i];  
                    $dadosProdutos['qtd_selecionada'] = $qtd_liberado[$i];  
                    $dadosProdutos['excluido'] = 'nao';
                    $dadosProdutos['logusuario'] = $this->_usuario['id'];
                    $dadosProdutos['logdata'] = date('Y-m-d G:i:s');
                     
                    $row = $almoxarifadosolicitacoesentradalistadeprodutos->save($dadosProdutos);
    
                    $strsql = "select id,quantidade from almoxarifadoprodutossaidas where idcodigoalmoxarifadoproduto = '$dadosProdutos[codigoproduto]' AND idescola = '$dadosEntrada[idescola]'";
                        
                    $idprodutosaida = $db->fetchRow($strsql);
                    
                    if($idprodutosaida){
                        $dadosSalvaEntradaParaSaidaProdutos['id'] = $idprodutosaida['id'];
                        $dadosSalvaEntradaParaSaidaProdutos['quantidade'] = $idprodutosaida['quantidade'] + $qtd_liberado[$i];	
                    }else{
                        $dadosSalvaEntradaParaSaidaProdutos['id'] = null;
                        $dadosSalvaEntradaParaSaidaProdutos['quantidade'] = $qtd_liberado[$i];
                    }
                    
                    $dadosSalvaEntradaParaSaidaProdutos['ultimaquantidadeinserida'] = $qtd_liberado[$i];
                    $dadosSalvaEntradaParaSaidaProdutos['idescola'] = $idescolaPostVar;
                    $dadosSalvaEntradaParaSaidaProdutos['idperfil'] = $idperfilPostVar;
                    $dadosSalvaEntradaParaSaidaProdutos['idperfilbusca'] = $idperfilPostVar;
                    $dadosSalvaEntradaParaSaidaProdutos['idcodigoalmoxarifadoproduto'] = $codigoproduto[$i]; 
                    $dadosSalvaEntradaParaSaidaProdutos["status"] = 'Ativo';
                    $dadosSalvaEntradaParaSaidaProdutos['excluido'] = 'nao';
                    $dadosSalvaEntradaParaSaidaProdutos['logusuario'] = $this->_usuario['id'];
                    $dadosSalvaEntradaParaSaidaProdutos['logdata'] = date('Y-m-d G:i:s');
                
                    
                    $row = $almoxarifadoprodutossaidas->save($dadosSalvaEntradaParaSaidaProdutos);
                }   
                
                $row = $almoxarifadosolicitacoes->save($dadosEnviaEstoque);
                $row = $almoxarifadosolicitacoesmateriaisentrada->save($dadosEntrada);
            }
            
            $db->commit();
        } catch (Exception $e) {
            
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Erro ao executar a operação: ".$e->getMessage(), "E");

            echo $e->getMessage();
            
            $db->rollBack();
            die();
        }       
        
        return "";      
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros,$e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));

        if ($excluir_arquivo=='excluir') $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($nome = false, $apenas_copia = false) {

        $idimagem = false;
        $imagens = new Imagens();

        try {

            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($nome, NULL, NULL, $apenas_copia);
            //var_dump($idimagem); die();
        } catch (Exception $e) {

            $idimagem = false;
            array_push($erros,$e->getMessage());

        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $nome));

        if ($excluir_imagem=='excluir_' . $nome) $idimagem = -1;

        return $idimagem;
    }

    public function relatorioprodutossolicitadosestoqueAction(){   
         
        $id = (int)$this->_request->getParam("id"); 
        $codigosolicitacao = (int)$this->_request->getParam("codigosolicitacao"); 
        
        $almoxarifadosolicitacoes = new Almoxarifadosolicitacoesentradalistadeprodutos();
        $_row = $this->view->rows = $almoxarifadosolicitacoes->getAlmoxarifadosolicitacoesentradalistadeprodutosHelper(
             array( 
                'codigosolicitacao' => $codigosolicitacao
                )
            );
        $descricoesproduto = array();

        for ($i=0; $i < count($this->view->rows); $i++) { 
            $descricoesproduto[$i] =  Almoxarifadoprodutos::getAlmoxarifadoprodutoByCodigoProdutoHelper($this->view->rows[$i]['codigoproduto']); 
        } 
            
        $_row['descricoes'] = $this->view->rows = $descricoesproduto;

        $solicitacoes = Almoxarifadosolicitacoes::getAlmoxarifadosolicitacoesHelper(array('id' => $id));
        $_row['solicitacoes'] = $this->view->rows = $solicitacoes;

		$patrimonios = new Patrimonios();
       	$data = $patrimonios->getCurrentData();
		$local = 'Santa Isabel';

		$localData = $local.' , '.$data;

		$this->view->rows = $_row; 
		$this->view->localdata = $localData;
 

		$db = Zend_Registry::get('db');

		 // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('l');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Estoque Solicitacoes');
        // $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 7);

        // add a page        
        $pdf->AddPage();
        
        $html = $this->view->render('almoxarifadosolicitacoes/pdf/relatorioprodutossolicitadosestoque.phtml');
        $image_file = "public/admin/imagens/logosantaisabel.jpg";

        $pdf->writeHTML($html, true, 0, true, 0);

        $filename = 'produtos_estoque.pdf';
        $pdf->Output($filename, 'I');
        
        Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Solicitou a impressão do relatório.");
        
        die();
        return true;
    	
    }

}