<?php

require_once BASE_PATH . '/library/tcpdf/tcpdf.php';
define('MY_PATH', 'http://localhost');

/**
 * Controle da classe secretariasavaliacoesacumulos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_SecretariasavaliacoesacumulosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Secretariasavaliacaoacumulo
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("secretariasavaliacoesacumulos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$secretariasprovastrabalhos = new Secretariasavaliacoesacumulos();
		$queries = array();	
		$ultimo = $secretariasprovastrabalhos->getUltimoSecretariasavaliacoesacumulo($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Secretariasavaliacoesacumulos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Comissão de avaliação de acúmulos de cargo excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="secretariasavaliacoesacumulos") $objs = new Secretariasavaliacoesacumulos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasavaliacoesacumulos', 'name' => 'Comissão de avaliação de acúmulos de cargos'),
			array('url' => null,'name' => 'Visualizar Comissão de avaliação de acúmulos de cargo')
		);
		
		//dados do 1° vínculo
		$idescola = Usuarios::getUsuario('idescola');
		$info = Escolas::getEscolaByIdHelper($idescola);

		$idendereco = $info['idendereco'];
		$endereco = Enderecos::getEnderecoById($idendereco);

		$id = (int)$this->_request->getParam("id");
		$secretariasavaliacoesacumulos = new Secretariasavaliacoesacumulos();
		$secretariasavaliacaoacumulo = $secretariasavaliacoesacumulos->getSecretariasavaliacaoacumuloById($id, array());
		
		if (!$secretariasavaliacaoacumulo) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->endereco = $endereco;
		$this->view->post_var = $secretariasavaliacaoacumulo;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}

	public function pdfAction()
	{
		$id = (int)$this->_request->getParam("id");
		$secretariasavaliacoesacumulos = new Secretariasavaliacoesacumulos();
		$secretariasavaliacaoacumulo = $secretariasavaliacoesacumulos->getSecretariasavaliacaoacumuloById($id, array());
		
		$endereco = Escolasenderecos::getEnderecoByIdHelper($secretariasavaliacaoacumulo['idendereco']);

		$dataAtual = $secretariasavaliacoesacumulos->getCurrentData();

		$local = 'Santa Isabel';
		$localData = $local.' , '.$dataAtual;
		
		if (!$secretariasavaliacaoacumulo) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
					
		$this->view->row = $secretariasavaliacaoacumulo;
		$this->view->vinculo = $endereco;

		$this->view->estado = Estados::getUFestadoById($endereco['idestado']);
		//print_r($this->view->estado); die();
		$this->view->usuario = Usuarios::getUsuarioByIdHelper($secretariasavaliacaoacumulo['idusuariologado']);

		$this->view->escola = Escolas::getEscolaByIdHelper($secretariasavaliacaoacumulo['idescola']);

		$this->view->funcionario = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($secretariasavaliacaoacumulo['idfuncionario']);

		$this->view->cargo = Cargos::getCargoByIdHelper($this->view->funcionario['idcargo']);

		$this->view->cargahoraria1 = Supervisoescargashorarias::getSupervisoescargahorariaByIdHelper($secretariasavaliacaoacumulo['idcargasemanal']);

		$this->view->cargahoraria2 = Supervisoescargashorarias::getSupervisoescargahorariaByIdHelper($secretariasavaliacaoacumulo['v2idcargasemanal']);

		$this->view->dataAtual = $localData;

		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('P');
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
		$pdf->SetTitle('DEPARTAMENTO DE SUPERVISÃO');
		$pdf->SetSubject('COMISSÃO DE AVALIAÇÃO DE ACÚMULOS DE CARGOS');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 10, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 20);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('secretariasavaliacoesacumulos/pdf/index.phtml');

		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'avaliacao_acumulo_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'I');
	
		die();
	
		return true;

	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Comissão de avaliação de acúmulos de cargos')
		);
		
		$ns = new Zend_Session_Namespace('default_secretariasavaliacoesacumulos');
		$secretariasavaliacoesacumulos = new Secretariasavaliacoesacumulos();

		$queries = array();	
		$idescola = Usuarios::getUsuario('idescola');
		if((int)$idescola>0){
			$queries['idescola'] = Usuarios::getUsuario('idescola');
		}

		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
			if (isset($this->view->post_var["idescola"]) AND $this->view->post_var !="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
			if ($this->view->post_var["rgf"]!="") $queries["rgf"] = $this->view->post_var["rgf"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];

    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $secretariasavaliacoesacumulos->getSecretariasavaliacoesacumulos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $secretariasavaliacoesacumulos->getSecretariasavaliacoesacumulos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de secretariasavaliacoesacumulos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasavaliacoesacumulos', 'name' => 'Comissão de avaliação de acúmulos de cargos'),
			array('url' => null,'name' => 'Editar Comissão de avaliação de acúmulos de cargo')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$secretariasavaliacoesacumulos = new Secretariasavaliacoesacumulos();
		$secretariasavaliacaoacumulo = $secretariasavaliacoesacumulos->getSecretariasavaliacaoacumuloById($id);
		
		if (!$secretariasavaliacaoacumulo) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $secretariasavaliacaoacumulo;
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($secretariasavaliacaoacumulo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Comissão de avaliação de acúmulos de cargo editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de secretariasavaliacoesacumulos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasavaliacoesacumulos', 'name' => 'Comissão de avaliação de acúmulos de cargos'),
			array('url' => null,'name' => 'Adicionar Comissão de avaliação de acúmulos de cargo')
		);

		$idescola = Usuarios::getUsuario('idescola');
		$info = Escolas::getEscolaByIdHelper($idescola);

		$idendereco = $info['idendereco'];
		$endereco = Escolasenderecos::getEnderecoByIdHelper($idendereco);
		
		$this->view->endereco = $endereco;

		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Comissão de avaliação de acúmulos de cargo adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	
	public function getfuncionariosAction() {
		$this->_helper->layout->disableLayout();

		$idescola = (int)$this->getRequest()->getPost('idescola');
		
		$professores = new Funcionariosgeraisescolas();
		$queries = array();

		$queries['status'] = 'Ativo';	
		$queries['idescola'] = $idescola;
		
		$rows = $professores->getFuncionariosgeraisescolas($queries);
		if($id == 29 && $idescola == 0) $rows = array();

		echo json_encode($rows);
		die();

	}

	public function setfuncionariosAction() {
		$this->_helper->layout->disableLayout();

		$idescola = (int)$this->getRequest()->getPost('idescola');
		
		$professores = new Funcionariosgeraisescolas();
		$queries = array();

		$queries['status'] = 'Ativo';	
		$queries['idescola'] = $idescola;
		
		$rows = $professores->getFuncionariosgeraisescolas($queries);
		//var_dump($rows);die();

		$this->view->rows = $rows;

	}


	public function setdadosAction() {
		$this->_helper->layout->disableLayout();

		$idfuncionario = (int)$this->getRequest()->getPost('idfuncionario');
		
		$professores = new Funcionariosgeraisescolas();

		
		$row = $professores->getFuncionariogeralescolaByIdHelper($idfuncionario);
		if($row)
		{
			$cargo = Cargos::getCargoByIdHelper($row['idcargo']);
			$row['cargo'] = ($cargo) ? $cargo['cargo'] : '--';
		}
		//var_dump($rows);die();

		echo json_encode($row);
		die();

	}


    /**
     * Atribui valores ao view
     * @param int $idsecretariasavaliacaoacumulo
     */    
    private function preForm($idsecretariasavaliacaoacumulo = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_secretariasavaliacaoacumulo = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idendereco = (int)trim($this->getRequest()->getPost("idendereco"));

		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idlocal = (int)trim($this->getRequest()->getPost("idlocal"));
$v2idendereco = (int)trim($this->getRequest()->getPost("v2idendereco"));
$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
$sequencial = trim($this->getRequest()->getPost("sequencial"));
$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
$nome = trim($this->getRequest()->getPost("nome"));
$cargo = trim($this->getRequest()->getPost("cargo"));
$rf = trim($this->getRequest()->getPost("rf"));
$rg = trim($this->getRequest()->getPost("rg"));
$unidadetrabalho = trim($this->getRequest()->getPost("unidadetrabalho"));
$telefone = trim($this->getRequest()->getPost("telefone"));
$idcargasemanal = (int)trim($this->getRequest()->getPost("idcargasemanal"));
$horariotrabalho = trim($this->getRequest()->getPost("horariotrabalho"));
$segundahorainicio1 = trim($this->getRequest()->getPost("segundahorainicio1"));
$segundahorafim1 = trim($this->getRequest()->getPost("segundahorafim1"));
$segundahorainicio2 = trim($this->getRequest()->getPost("segundahorainicio2"));
$segundahorafim2 = trim($this->getRequest()->getPost("segundahorafim2"));
$tercahorainicio1 = trim($this->getRequest()->getPost("tercahorainicio1"));
$tercahorafim1 = trim($this->getRequest()->getPost("tercahorafim1"));
$tercahorainicio2 = trim($this->getRequest()->getPost("tercahorainicio2"));
$tercahorafim2 = trim($this->getRequest()->getPost("tercahorafim2"));
$quartahorainicio1 = trim($this->getRequest()->getPost("quartahorainicio1"));
$quartahorafim1 = trim($this->getRequest()->getPost("quartahorafim1"));
$quartahorainicio2 = trim($this->getRequest()->getPost("quartahorainicio2"));
$quartahorafim2 = trim($this->getRequest()->getPost("quartahorafim2"));
$quintahorainicio1 = trim($this->getRequest()->getPost("quintahorainicio1"));
$quintahorafim1 = trim($this->getRequest()->getPost("quintahorafim1"));
$quintahorainicio2 = trim($this->getRequest()->getPost("quintahorainicio2"));
$quintahorafim2 = trim($this->getRequest()->getPost("quintahorafim2"));
$sextahorainicio1 = trim($this->getRequest()->getPost("sextahorainicio1"));
$sextahorafim1 = trim($this->getRequest()->getPost("sextahorafim1"));
$sextahorainicio2 = trim($this->getRequest()->getPost("sextahorainicio2"));
$sextahorafim2 = trim($this->getRequest()->getPost("sextahorafim2"));
$htpcsemana1 = trim($this->getRequest()->getPost("htpcsemana1"));
$htpcsemanahorainicio1 = trim($this->getRequest()->getPost("htpcsemanahorainicio1"));
$htpcsemanahorafim1 = trim($this->getRequest()->getPost("htpcsemanahorafim1"));
$htpcsemana2 = trim($this->getRequest()->getPost("htpcsemana2"));
$htpcsemanahorainicio2 = trim($this->getRequest()->getPost("htpcsemanahorainicio2"));
$htpcsemanahorafim2 = trim($this->getRequest()->getPost("htpcsemanahorafim2"));
$local = trim($this->getRequest()->getPost("local"));
$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
$v2unidadetrabalho = trim($this->getRequest()->getPost("v2unidadetrabalho"));
$v2telefone = trim($this->getRequest()->getPost("v2telefone"));
$v2idcargasemanal = (int)trim($this->getRequest()->getPost("v2idcargasemanal"));
$v2horariotrabalho = trim($this->getRequest()->getPost("v2horariotrabalho"));
$v2segundahorainicio1 = trim($this->getRequest()->getPost("v2segundahorainicio1"));
$v2segundahorafim1 = trim($this->getRequest()->getPost("v2segundahorafim1"));
$v2segundahorainicio2 = trim($this->getRequest()->getPost("v2segundahorainicio2"));
$v2segundahorafim2 = trim($this->getRequest()->getPost("v2segundahorafim2"));
$v2tercahorainicio1 = trim($this->getRequest()->getPost("v2tercahorainicio1"));
$v2tercahorafim1 = trim($this->getRequest()->getPost("v2tercahorafim1"));
$v2tercahorainicio2 = trim($this->getRequest()->getPost("v2tercahorainicio2"));
$v2tercahorafim2 = trim($this->getRequest()->getPost("v2tercahorafim2"));
$v2quartahorainicio1 = trim($this->getRequest()->getPost("v2quartahorainicio1"));
$v2quartahorafim1 = trim($this->getRequest()->getPost("v2quartahorafim1"));
$v2quartahorainicio2 = trim($this->getRequest()->getPost("v2quartahorainicio2"));
$v2quartahorafim2 = trim($this->getRequest()->getPost("v2quartahorafim2"));
$v2quintahorainicio1 = trim($this->getRequest()->getPost("v2quintahorainicio1"));
$v2quintahorafim1 = trim($this->getRequest()->getPost("v2quintahorafim1"));
$v2quintahorainicio2 = trim($this->getRequest()->getPost("v2quintahorainicio2"));
$v2quintahorafim2 = trim($this->getRequest()->getPost("v2quintahorafim2"));
$v2sextahorainicio1 = trim($this->getRequest()->getPost("v2sextahorainicio1"));
$v2sextahorafim1 = trim($this->getRequest()->getPost("v2sextahorafim1"));
$v2sextahorainicio2 = trim($this->getRequest()->getPost("v2sextahorainicio2"));
$v2sextahorafim2 = trim($this->getRequest()->getPost("v2sextahorafim2"));
$v2htpcsemana1 = trim($this->getRequest()->getPost("v2htpcsemana1"));
$v2htpcsemanahorainicio1 = trim($this->getRequest()->getPost("v2htpcsemanahorainicio1"));
$v2htpcsemanahorafim1 = trim($this->getRequest()->getPost("v2htpcsemanahorafim1"));
$v2htpcsemana2 = trim($this->getRequest()->getPost("v2htpcsemana2"));
$v2htpcsemanahorainicio2 = trim($this->getRequest()->getPost("v2htpcsemanahorainicio2"));
$v2htpcsemanahorafim2 = trim($this->getRequest()->getPost("v2htpcsemanahorafim2"));
$v2local = trim($this->getRequest()->getPost("v2local"));
$v2data = Mn_Util::stringToTime($this->getRequest()->getPost("v2data"));
$penasleis = $this->getRequest()->getPost("penasleis");
$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
$distancia = trim($this->getRequest()->getPost("kmdistancia"));
$transporte = trim($this->getRequest()->getPost("transporte"));
$horas = (int)trim($this->getRequest()->getPost("horas"));
$minutos = (int)trim($this->getRequest()->getPost("minutos"));
$status = trim($this->getRequest()->getPost("status1"));
$deferido = trim($this->getRequest()->getPost("deferido"));
		
		
		$erros = array();
		
		if (""==$idfuncionario) array_push($erros, "Informe o Funcionario.");
		if (""==$cargo) array_push($erros, "Informe a Cargo.");
		if (""==$penasleis) array_push($erros, "Informe a Declaro sob penas de lei que.");
		if (""==$status && $this->getRequest()->getActionName() == 'editar') array_push($erros, "Informe a Status.");

		$idarquivo = $this->getArquivo('idarquivo');
		if (0==$idarquivo && $status == 'Deferido' || 0==$idarquivo && $status == 'Indeferido') array_push($erros, "Você deve anexar um arquivo.");
		
		$secretariasavaliacoesacumulos = new Secretariasavaliacoesacumulos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$enderecosModel = new EnderecoModel($this);
			//$endereco = $enderecosModel->getPost( ($_secretariasavaliacaoacumulo) ? (int)$_secretariasavaliacaoacumulo['idendereco'] : 0, "e_idendereco");
			//$idendereco = ($endereco) ? $endereco['id'] : 0;
			
			$dados['idendereco'] = $idendereco;

			$_endereco = $enderecosModel->getPost( ($_secretariasavaliacaoacumulo) ? (int)$_secretariasavaliacaoacumulo['v2idendereco'] : 0, "e_v2idendereco");
			$v2idendereco = ($_endereco) ? $_endereco['id'] : 0;	
			$dados['v2idendereco'] = $v2idendereco;

if((int)$id <= 0 )			$dados["sequencial"] = $sequencial;
if((int)$id <= 0 )$dados["datalancamento"] = date("Y-m-d", $datalancamento);
if((int)$id <= 0 )$dados["horalancamento"] = $horalancamento;
$dados["idusuariologado"] = Usuarios::getUsuario('id');
$dados["idescola"] = $idescola;
$dados["idfuncionario"] = $idfuncionario;
$dados["idlocal"] = $idlocal;
$dados["nome"] = $nome;
$dados["cargo"] = $cargo;
$dados["rf"] = $rf;
$dados["rg"] = $rg;
$dados["unidadetrabalho"] = $unidadetrabalho;
$dados["telefone"] = $telefone;
$dados["idcargasemanal"] = $idcargasemanal;
$dados["horariotrabalho"] = $horariotrabalho;
$dados["segundahorainicio1"] = $segundahorainicio1;
$dados["segundahorafim1"] = $segundahorafim1;
$dados["segundahorainicio2"] = $segundahorainicio2;
$dados["segundahorafim2"] = $segundahorafim2;
$dados["tercahorainicio1"] = $tercahorainicio1;
$dados["tercahorafim1"] = $tercahorafim1;
$dados["tercahorainicio2"] = $tercahorainicio2;
$dados["tercahorafim2"] = $tercahorafim2;
$dados["quartahorainicio1"] = $quartahorainicio1;
$dados["quartahorafim1"] = $quartahorafim1;
$dados["quartahorainicio2"] = $quartahorainicio2;
$dados["quartahorafim2"] = $quartahorafim2;
$dados["quintahorainicio1"] = $quintahorainicio1;
$dados["quintahorafim1"] = $quintahorafim1;
$dados["quintahorainicio2"] = $quintahorainicio2;
$dados["quintahorafim2"] = $quintahorafim2;
$dados["sextahorainicio1"] = $sextahorainicio1;
$dados["sextahorafim1"] = $sextahorafim1;
$dados["sextahorainicio2"] = $sextahorainicio2;
$dados["sextahorafim2"] = $sextahorafim2;
$dados["htpcsemana1"] = $htpcsemana1;
$dados["htpcsemanahorainicio1"] = $htpcsemanahorainicio1;
$dados["htpcsemanahorafim1"] = $htpcsemanahorafim1;
$dados["htpcsemana2"] = $htpcsemana2;
$dados["htpcsemanahorainicio2"] = $htpcsemanahorainicio2;
$dados["htpcsemanahorafim2"] = $htpcsemanahorafim2;
$dados["local"] = $local;
$dados["data"] = date("Y-m-d", $data);
$dados["v2unidadetrabalho"] = $v2unidadetrabalho;
$dados["v2telefone"] = $v2telefone;
$dados["v2idcargasemanal"] = $v2idcargasemanal;
$dados["v2horariotrabalho"] = $v2horariotrabalho;
$dados["v2segundahorainicio1"] = $v2segundahorainicio1;
$dados["v2segundahorafim1"] = $v2segundahorafim1;
$dados["v2segundahorainicio2"] = $v2segundahorainicio2;
$dados["v2segundahorafim2"] = $v2segundahorafim2;
$dados["v2tercahorainicio1"] = $v2tercahorainicio1;
$dados["v2tercahorafim1"] = $v2tercahorafim1;
$dados["v2tercahorainicio2"] = $v2tercahorainicio2;
$dados["v2tercahorafim2"] = $v2tercahorafim2;
$dados["v2quartahorainicio1"] = $v2quartahorainicio1;
$dados["v2quartahorafim1"] = $v2quartahorafim1;
$dados["v2quartahorainicio2"] = $v2quartahorainicio2;
$dados["v2quartahorafim2"] = $v2quartahorafim2;
$dados["v2quintahorainicio1"] = $v2quintahorainicio1;
$dados["v2quintahorafim1"] = $v2quintahorafim1;
$dados["v2quintahorainicio2"] = $v2quintahorainicio2;
$dados["v2quintahorafim2"] = $v2quintahorafim2;
$dados["v2sextahorainicio1"] = $v2sextahorainicio1;
$dados["v2sextahorafim1"] = $v2sextahorafim1;
$dados["v2sextahorainicio2"] = $v2sextahorainicio2;
$dados["v2sextahorafim2"] = $v2sextahorafim2;
$dados["v2htpcsemana1"] = $v2htpcsemana1;
$dados["v2htpcsemanahorainicio1"] = $v2htpcsemanahorainicio1;
$dados["v2htpcsemanahorafim1"] = $v2htpcsemanahorafim1;
$dados["v2htpcsemana2"] = $v2htpcsemana2;
$dados["v2htpcsemanahorainicio2"] = $v2htpcsemanahorainicio2;
$dados["v2htpcsemanahorafim2"] = $v2htpcsemanahorafim2;
$dados["v2local"] = $v2local;
$dados["v2data"] = date("Y-m-d", $v2data);
$dados["penasleis"] = $penasleis;

$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;

$dados["distancia"] = $distancia;
$dados["transporte"] = $transporte;
$dados["horas"] = $horas;
$dados["minutos"] = $minutos;
$dados["status"] = ($status == '') ? 'Em andamento' : $status;
$dados["deferido"] = $deferido;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			$row = $secretariasavaliacoesacumulos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}