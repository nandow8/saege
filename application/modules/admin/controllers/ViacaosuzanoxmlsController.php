<?php

/**
 * Controle da classe viacaosuzanoxmls do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ViacaosuzanoxmlsController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Viacaosuzanoxml
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("viacaosuzanoxmls", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'viacaosuzanoxmls', 'name' => 'Central de vagas'),
            array('url' => null, 'name' => 'Visualizar Vaga')
        );

        $id = (int) $this->_request->getParam("id");
        $viacaosuzanoxmls = new Viacaosuzanoxmls();
        $viacaosuzanoxml = $viacaosuzanoxmls->getViacaosuzanoxmlById($id, array());

        if (!$viacaosuzanoxml)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $viacaosuzanoxml;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Central de vagas')
        );

        $ns = new Zend_Session_Namespace('default_viacaosuzanoxmls');
        $viacaosuzanoxmls = new Viacaosuzanoxmls();
        $queries = array();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $viacaosuzanoxmls->getViacaosuzanoxmls($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;


        $this->view->rows = $viacaosuzanoxmls->getViacaosuzanoxmls($queries, $paginaAtual, $maxpp);
    }

    /**
     * 
     * Action de edição de viacaosuzanoxmls
     */
    public function editarAction() {
        return true;
    }

    /**
     * 
     * Action de adição de viacaosuzanoxmls 
     */
    public function adicionarAction() {
        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idviacaosuzanoxml
     */
    private function preForm($idviacaosuzanoxml = 0) {
        return true;
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_viacaosuzanoxml = false) {
        return true;
    }

}
