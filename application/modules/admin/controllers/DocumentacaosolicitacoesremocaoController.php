<?php

/**
 * Controle da classe documentacaosolicitacoesremocao do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_DocumentacaosolicitacoesremocaoController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Documentacaosolicitacaoremocao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("documentacaosolicitacoesremocao", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Documentacaosolicitacoesremocao();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Remoção excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="documentacaosolicitacoesremocao") $objs = new Documentacaosolicitacoesremocao();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaosolicitacoesremocao', 'name' => 'Solicitações de Remoção'),
			array('url' => null,'name' => 'Visualizar Solicitação de Remoção')
		);
		
		$id = (int)$this->_request->getParam("id");
		$documentacaosolicitacoesremocao = new Documentacaosolicitacoesremocao();
		$documentacaosolicitacaoremocao = $documentacaosolicitacoesremocao->getDocumentacaosolicitacaoremocaoById($id, array());
		
		if (!$documentacaosolicitacaoremocao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $documentacaosolicitacaoremocao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Solicitações de Remoção')
		);
		
		$ns = new Zend_Session_Namespace('default_documentacaosolicitacoesremocao');
		$documentacaosolicitacoesremocao = new Documentacaosolicitacoesremocao();
		$queries = array();	
		$queries['idescola'] = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'), 'id');
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["idprofessor"]!="") $queries["idprofessor"] = $this->view->post_var["idprofessor"];
if ($this->view->post_var["idvinculo"]!="") $queries["idvinculo"] = $this->view->post_var["idvinculo"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $documentacaosolicitacoesremocao->getDocumentacaosolicitacoesremocao($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $documentacaosolicitacoesremocao->getDocumentacaosolicitacoesremocao($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de documentacaosolicitacoesremocao
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaosolicitacoesremocao', 'name' => 'Solicitações de Remoção'),
			array('url' => null,'name' => 'Editar Solicitação de Remoção')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$documentacaosolicitacoesremocao = new Documentacaosolicitacoesremocao();
		$documentacaosolicitacaoremocao = $documentacaosolicitacoesremocao->getDocumentacaosolicitacaoremocaoById($id);
		
		if (!$documentacaosolicitacaoremocao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $documentacaosolicitacaoremocao;
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($documentacaosolicitacaoremocao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Remoção editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de documentacaosolicitacoesremocao 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaosolicitacoesremocao', 'name' => 'Solicitações de Remoção'),
			array('url' => null,'name' => 'Adicionar Solicitação de Remoção')
		);	

		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Remoção adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $iddocumentacaosolicitacaoremocao
     */    
    private function preForm($iddocumentacaosolicitacaoremocao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_documentacaosolicitacaoremocao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
		$horario = trim($this->getRequest()->getPost("horario"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
		$idvinculo = (int)trim($this->getRequest()->getPost("idvinculo"));
		$pontuacao = trim($this->getRequest()->getPost("pontuacao"));
		$observacao = trim($this->getRequest()->getPost("observacao"));
		$posicao = trim($this->getRequest()->getPost("posicao"));

		$idscursos = $this->getRequest()->getPost("idscursos");
				
		if (!is_array($idscursos)) $idscursos = array();
		$idscursos = implode(',',$idscursos);
		
		$erros = array();
		
		if (""==$data) array_push($erros, "Informe a Data.");
		if (0==$idescola) array_push($erros, "Informe a Unidade Escolas.");
		//if (""==$status) array_push($erros, "Informe a Status.");

		
		$documentacaosolicitacoesremocao = new Documentacaosolicitacoesremocao();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idusuario"] = $idusuario;
			$dados["data"] = date("Y-m-d", $data);
			$dados["horario"] = $horario;
			$dados["idescola"] = $idescola;
			if(Usuarios::getUsuario('professor') == 'Sim')
			{
				$dados["idprofessor"] = $idprofessor;
			}
			$dados["idvinculo"] = $idvinculo;
			$dados["pontuacao"] = $pontuacao;
			$dados["observacao"] = $observacao;
			$dados["posicao"] = (Usuarios::getUsuario('professor') == 'Sim') ? 'Aguardando' : $posicao;
			$dados["status"] = 'Ativo';

			$dados["idscursos"] = $idscursos;
						
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $documentacaosolicitacoesremocao->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    

    public function setprofessoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idescola"] = $idescola;
		$queries["professor"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
    }

	public function setperiodosAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->getRequest()->getPost('idescola');

		$vinculos = Escolasvinculosprofessoresmaterias::getEscolasvinculosprofessoresmaterias(array('idprofessor' => (int)Usuarios::getUsuario('idfuncionario'), 'idescola' => $idescola));
    	
    	$this->view->rows = $vinculos;
	}

    public function getcursosAction() {
		$this->_helper->layout->disableLayout();
		
		$idprofessor = (int)$this->_request->getPost("idprofessor");
		//var_dump($idprofessor);die();
		if($idprofessor <= 0) die('ERRO');

		$documentacaosolicitacoespontos = new Documentacaosolicitacoespontos();
		$cursos = $documentacaosolicitacoespontos->getDocumentacaosolicitacoespontosHelper(array('idprofessor' => $idprofessor));
		//var_dump($cursos);die();

		$this->view->cursos = $cursos;
    }
}