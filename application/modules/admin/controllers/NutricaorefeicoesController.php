<?php

/**
 * Controle da classe nutricaorefeicoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */  
class Admin_NutricaorefeicoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Nutricaorefeicao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("nutricaorefeicoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Nutricaorefeicoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Refeição excluída com sucesso.";

			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="nutricaorefeicoes") $objs = new Nutricaorefeicoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaorefeicoes', 'name' => 'Refeições'),
			array('url' => null,'name' => 'Visualizar Refeição')
		);
		
		$id = (int)$this->_request->getParam("id");
		$nutricaorefeicoes = new Nutricaorefeicoes();
		$nutricaorefeicao = $nutricaorefeicoes->getNutricaorefeicaoById($id, array());
		
		if (!$nutricaorefeicao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaorefeicao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Refeições')
		);
		
		$ns = new Zend_Session_Namespace('default_nutricaorefeicoes');
		$nutricaorefeicoes = new Nutricaorefeicoes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
		
			
			if ($this->view->post_var["titulo"]!="") $queries["id"] = $this->view->post_var["titulo"];

    		
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $nutricaorefeicoes->getNutricaorefeicoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $nutricaorefeicoes->getNutricaorefeicoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de nutricaorefeicoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaorefeicoes', 'name' => 'Refeição'),
			array('url' => null,'name' => 'Editar Refeição')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$nutricaorefeicoes = new Nutricaorefeicoes();
		$nutricaorefeicao = $nutricaorefeicoes->getNutricaorefeicaoById($id);
		
		if (!$nutricaorefeicao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaorefeicao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($nutricaorefeicao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Refeição editada com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de nutricaorefeicoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaorefeicoes', 'name' => 'Refeição'),
			array('url' => null,'name' => 'Adicionar Refeição')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Refeição adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idnutricaorefeicao
     */    
    private function preForm($idnutricaorefeicao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_nutricaorefeicao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$titulo = trim($this->getRequest()->getPost("titulo"));
                $porcoes = (int)$this->getRequest()->getPost("porcoes");
                $idimagem = (int)trim($this->getRequest()->getPost("idimagem"));
                $favorita = floatval(trim($this->getRequest()->getPost("favorita")));
                $descricao = trim($this->getRequest()->getPost("descricao"));
		$status = trim($this->getRequest()->getPost("status1"));
		
				$idalimento = $this->getRequest()->getPost("id");
                $idnutricaorefeicao= $this->getRequest()->getPost("idnutricaorefeicao");
                $idnutricaotacoalimento = $this->getRequest()->getPost("idnutricaotacoalimento");
                $descricaotacoalimento = $this->getRequest()->getPost("descricaotacoalimento");
                $quantidade = $this->getRequest()->getPost("quantidade");
                $unidade = $this->getRequest()->getPost("unidade");
                $observacao = $this->getRequest()->getPost("observacao");
		
		$erros = array();
		
		if (""==$titulo) array_push($erros, "Informe o titulo.");
		if (""==$status) array_push($erros, "Informe o Status.");

		$nutricaorefeicoes = new Nutricaorefeicoes();
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			if($id == 0)
				$dados['id'] = $id;
			else
				$dados['id'] = $this->_request->getParam("id"); 
			
			$dados['titulo'] = $titulo;
                        $dados['favorita'] = $favorita;
                        
                        $idimagem = $this->getImagem('idimagem');
			if ($idimagem!=0) $dados['idimagem'] = $idimagem;
                        
			$dados['porcoes'] = $porcoes;
                        $dados['descricao'] = $descricao;
			$dados['status'] = $status;

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');

			$row = $nutricaorefeicoes->save($dados);
			

						/* ============================================= 
								Inserindo Lista de Ingredientes   
						================================================*/

						$ingredientes = array();
					
						
						$ingredientes['idnutricaorefeicao'] = array_filter($idnutricaorefeicao);
						$ingredientes['idnutricaotacoalimento'] = array_filter($idnutricaotacoalimento);
						$ingredientes['descricaotacoalimento'] = array_filter($descricaotacoalimento);
						$ingredientes['quantidade'] = array_filter($quantidade);
						$ingredientes['unidade'] = array_filter($unidade);
						$ingredientes['observacao'] = array_filter($observacao);	
						$ingredientes['id'] = array_filter($idalimento);				

						$arrayTotalIngredientes = array();
					
						$dados['ingredientes'] = $ingredientes;

						foreach($ingredientes as $i1=>$index):

							foreach($index as $i2=>$v):

								if($i1 == 'idnutricaotacoalimento'){
									$arrayTotalIngredientes[$i2] = $v;
								}else{
									$arrayTotalIngredientes[$i2] .= ',' . $v;
								}
								
							endforeach;	

						endforeach;
					
						foreach($arrayTotalIngredientes as $index):

							$auxIngrediente = explode(',',$index);

							$dadosIng = array();

							$dadosIng['id'] = $auxIngrediente[5];
							$dadosIng['idnutricaorefeicao'] = (int)$row['id'];
							$dadosIng['idnutricaotacoalimento'] = (int)$auxIngrediente[0];
							$dadosIng['descricaotacoalimento'] = $auxIngrediente[1];
							$dadosIng['quantidade'] =  (int)$auxIngrediente[2];
							$dadosIng['unidade'] = $auxIngrediente[3];
							$dadosIng['observacao'] = $auxIngrediente[4];

							$dadosIng['status'] = 'Ativo';
							$dadosIng['excluido'] = 'nao';
							$dadosIng['logusuario'] = Usuarios::getUsuario('id');
							$dadosIng['logdata'] = date('Y-m-d G:i:s');
						
							$objIngredientes = new Nutricaorefeicoesingredientes();
							
							$objIngredientes->save($dadosIng);

						endforeach;
						
                       

                        $db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);

        } catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	}
	
	public function unidadesajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = (int)$this->_request->getParam("id");

		$alimento = Nutricaotacoalimentos::getNutricaotacoalimentoByIdHelper($id)['idsmedidas'];

		$alimento = explode(',', $alimento);

		$medidas = array();

		if($alimento[0] != ''){
			foreach($alimento as $index):
		
				$medidasAuxiliar = Unidadescomerciais::getUnidadescomerciaisHelper(array('id'=>$index))[0];
				array_push($medidas, $medidasAuxiliar);
	
			endforeach;
		}else{

		}

		echo json_encode($medidas);

		die();

	} // end function unidadesaj

	public function unidadescomerciaisajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = $this->_request->getParam("id");
		$row = Unidadescomerciais::getNutricaounidadeByIdHelper($id);
		echo json_encode($row);
		die();
	}

	public function retiraalimentoajaxAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = $this->_request->getParam("id");

		$nutriIngredientes = new Nutricaorefeicoesingredientes();

		$row = $nutriIngredientes->getNutricaotacoalimentoByIdHelper($id);

		$row['status'] = 'Ativo';
		$row['excluido'] = 'sim';
		$row['logusuario'] = Usuarios::getUsuario('id');
		$row['logdata'] = date('Y-m-d G:i:s');

		$nutriIngredientes->save($row);

		echo json_encode($row);

		die();

	}// end public function

	public function retornaalimentoajaxAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = $this->_request->getParam("id");

		$alimentosTaco = new Nutricaotacoalimentos();

		$row = $alimentosTaco->getNutricaotacoalimentoByIdHelper($id);

		echo json_encode($row);

		die();

	}//end public function

	public function retornaalimentototalajaxAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = $this->_request->getParam("id");

		$retorno = array();
		$total = array();

		$total['energia_kcal'] = (float)00.00;
		$total['vl_carb'] = (float)00.00;
		$total['vl_g_tot'] = (float)(float)00.00;
		$total['vl_prot'] = (float)00.00;
		$total['vl_fibra'] = (float)00.00;
		$total['vl_vit_a'] = (float)00.00;
		$total['vl_vit_c'] = (float)00.00;
		$total['vl_fe'] = (float)00.00;
		$total['vl_mg'] = (float)00.00;
		$total['vl_ca'] = (float)00.00;
		$total['vl_zinco'] = (float)00.00;

		$alimentosTaco = new Nutricaotacoalimentos();

		foreach($id as $index):

			$row = $alimentosTaco->getNutricaotacoalimentoByIdHelper($index[1]);
			
			$row['vl_carb'] = (( (float)str_replace(',','.',$row['vl_carb'])* (float)$index[0]) * ((float)$index[2] / 100) );
			$row['vl_g_tot'] = (( (float)str_replace(',','.',$row['vl_g_tot'])* (float)$index[0]) * ((float)$index[2] / 100) ) ;
			$row['vl_prot'] = (( (float)str_replace(',','.',$row['vl_prot'])* (float)$index[0]) * ((float)$index[2] / 100) ) ;
			$row['vl_fibra'] = (( (float)str_replace(',','.',$row['vl_fibra'])* (float)$index[0]) * ((float)$index[2] / 100) ) ;
			$row['vl_vit_a'] = (( (float)str_replace(',','.',$row['vl_vit_a'])* (float)$index[0]) * ((float)$index[2] / 100) ) ;
			$row['vl_vit_c'] = (( (float)str_replace(',','.',$row['vl_vit_c'])* (float)$index[0]) * ((float)$index[2] / 100) ) ;
			$row['vl_fe'] = (( (float)str_replace(',','.',$row['vl_fe'])* (float)$index[0]) * ((float)$index[2] / 100) ) ;
			$row['vl_mg'] = (( (float)str_replace(',','.',$row['vl_mg'])* (float)$index[0]) * ((float)$index[2] / 100) ) ;
			$row['vl_ca'] = (( (float)str_replace(',','.',$row['vl_ca'])* (float)$index[0]) * ((float)$index[2] / 100) ) ;
			$row['vl_zinco'] = (( (float)str_replace(',','.',$row['vl_zinco'])* (float)$index[0]) * ((float)$index[2] / 100) ) ;
			$row['qtde'] = $index[0];
			$row['grama'] = $index[2];

			// Calculando Energia Kcal
			
			$row['energia_kcal'] = (4 * (float)$row['vl_carb']) + (4 * (float)$row['vl_prot']) + (9 * (float)$row['vl_g_tot']);

			$total['energia_kcal'] += $row['energia_kcal'];
			$total['vl_carb'] += $row['vl_carb'];
			$total['vl_g_tot'] += $row['vl_g_tot'];
			$total['vl_prot'] += $row['vl_prot'];
			$total['vl_fibra'] += $row['vl_fibra'];
			$total['vl_vit_a'] += $row['vl_vit_a'];
			$total['vl_vit_c'] += $row['vl_vit_c'];
			$total['vl_fe'] += $row['vl_fe'];
			$total['vl_mg'] += $row['vl_mg'];
			$total['vl_ca'] += $row['vl_ca'];
			$total['vl_zinco'] += $row['vl_zinco'];

			$retorno[] = $row;
			
		endforeach;

		$retorno[] = $total;

		echo json_encode($retorno);
		die();

	}//end public function
    
}