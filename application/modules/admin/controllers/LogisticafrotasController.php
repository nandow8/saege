<?php

/**
 * Controle da classe logisticafrotas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_LogisticafrotasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Logisticafrota
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("logisticafrotas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Logisticafrotas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de Frota excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="logisticafrotas") $objs = new Logisticafrotas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticafrotas', 'name' => 'Controle de Frotas'),
			array('url' => null,'name' => 'Visualizar Controle de Frota')
		);
		
		$id = (int)$this->_request->getParam("id");
		$logisticafrotas = new Logisticafrotas();
		$logisticafrota = $logisticafrotas->getLogisticafrotaById($id, array());
		
		if (!$logisticafrota) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticafrota;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Controle de Frotas')
		);
		
		$ns = new Zend_Session_Namespace('default_logisticafrotas');
		$logisticafrotas = new Logisticafrotas();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			// if ($this->view->post_var["idfabricante"]!="") $queries["idfabricante"] = $this->view->post_var["idfabricante"];
if ($this->view->post_var["idmarca"]!="") $queries["idmarca"] = $this->view->post_var["idmarca"];
if ($this->view->post_var["placa"]!="") $queries["placa"] = $this->view->post_var["placa"];
if ($this->view->post_var["idproprietario"]!="") $queries["idproprietario"] = $this->view->post_var["idproprietario"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $logisticafrotas->getLogisticafrotas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $logisticafrotas->getLogisticafrotas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de logisticafrotas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticafrotas', 'name' => 'Controle de Frotas'),
			array('url' => null,'name' => 'Editar Controle de Frota')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$logisticafrotas = new Logisticafrotas();
		$logisticafrota = $logisticafrotas->getLogisticafrotaById($id);
		
		if (!$logisticafrota) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticafrota;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($logisticafrota);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de Frota editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de logisticafrotas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticafrotas', 'name' => 'Controle de Frotas'),
			array('url' => null,'name' => 'Adicionar Controle de Frota')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de Frota adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idlogisticafrota
     */    
    private function preForm($idlogisticafrota = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_logisticafrota = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		// $idfabricante = (int)trim($this->getRequest()->getPost("idfabricante"));
	$idmarca = (int)trim($this->getRequest()->getPost("idmarca"));
	$idmodelo = (int)trim($this->getRequest()->getPost("idmodelo"));
	$idvistoriaescolar = (int)trim($this->getRequest()->getPost("idvistoriaescolar"));
	$vencimentovistoriaescolar = Mn_Util::stringToTime($this->getRequest()->getPost("vencimentovistoriaescolar"));
	$deficiente = trim($this->getRequest()->getPost("deficiente"));
	$idvistoriadeficiente = (int)trim($this->getRequest()->getPost("idvistoriadeficiente"));
	$vencimentovistoriadeficiente = Mn_Util::stringToTime($this->getRequest()->getPost("vencimentovistoriadeficiente"));
	$idlaudoescolar = (int)trim($this->getRequest()->getPost("idlaudoescolar"));
	$idtipo = (int)trim($this->getRequest()->getPost("idtipo"));
	$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
	$tipoveiculo = trim($this->getRequest()->getPost("tipoveiculo"));
	$anomodelo = trim($this->getRequest()->getPost("anomodelo"));
	$patrimonio = trim($this->getRequest()->getPost("patrimonio"));
	$situacaoveiculo = trim($this->getRequest()->getPost("situacaoveiculo"));
	$logisticafrotascol = trim($this->getRequest()->getPost("logisticafrotascol"));
	$kminicial = trim($this->getRequest()->getPost("kminicial"));
	$combustivel = trim($this->getRequest()->getPost("combustivel"));
	$renavam = trim($this->getRequest()->getPost("renavam"));
	$chassi = trim($this->getRequest()->getPost("chassi"));
	$placa = trim($this->getRequest()->getPost("placa"));
	$cor = trim($this->getRequest()->getPost("cor"));
	$capacidadepotencia = trim($this->getRequest()->getPost("capacidadepotencia"));
	$idproprietario = (int)trim($this->getRequest()->getPost("idproprietario"));
	$especie = trim($this->getRequest()->getPost("especie"));
	$categoria = trim($this->getRequest()->getPost("categoria"));
	$veiculo = trim($this->getRequest()->getPost("veiculo"));
	$anofabricacao = trim($this->getRequest()->getPost("anofabricacao"));
	$emprestimo = trim($this->getRequest()->getPost("emprestimo"));
	$veiculopublico = trim($this->getRequest()->getPost("veiculopublico"));
	$quantidade =  (int)trim($this->getRequest()->getPost("quantidade"));
	$reservadepartamentos = trim($this->getRequest()->getPost("reservadepartamentos"));
	$escolar = trim($this->getRequest()->getPost("escolar"));
	$veiculodisponiveldepartamento = trim($this->getRequest()->getPost("veiculodisponiveldepartamento"));
	$status = trim($this->getRequest()->getPost("status1"));
			
		
		$erros = array();
		
		// if (0==$idfabricante) array_push($erros, "Informe a Fabricantes.");
		if (""==$veiculo) array_push($erros, "Informe a Veículo.");
		if (""==$renavam) array_push($erros, "Informe a Renavam.");
		if (""==$chassi) array_push($erros, "Informe a Chassi.");
		if (0==$idmarca) array_push($erros, "Informe a Marcas.");
		if (""==$placa) array_push($erros, "Informe a Placa.");
		//if (0==$idproprietario) array_push($erros, "Informe a Funcionário.");
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$logisticafrotas = new Logisticafrotas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			// $dados["idfabricante"] = $idfabricante;
			$dados["idmarca"] = $idmarca;
			$dados["idmodelo"] = $idmodelo;

			$dados["escolar"] = $escolar;
			$idvistoriaescolar = $this->getArquivo('idvistoriaescolar');
			if ($idvistoriaescolar!=0) $dados['idvistoriaescolar'] = $idvistoriaescolar;
			$dados["vencimentovistoriaescolar"] = date("Y-m-d", $vencimentovistoriaescolar);
			$dados["deficiente"] = $deficiente;
			$idvistoriadeficiente = $this->getArquivo('idvistoriadeficiente');
			if ($idvistoriadeficiente!=0) $dados['idvistoriadeficiente'] = $idvistoriadeficiente;
			$dados["vencimentovistoriadeficiente"] = date("Y-m-d", $vencimentovistoriadeficiente);

			$idlaudoescolar = $this->getArquivo('idlaudoescolar');
			if ($idlaudoescolar!=0) $dados['idlaudoescolar'] = $idlaudoescolar;

			$dados["idtipo"] = $idtipo;
			$dados["idarquivo"] = $idarquivo;
			$dados["tipoveiculo"] = $tipoveiculo;
			$dados["anomodelo"] = $anomodelo;
			$dados["patrimonio"] = $patrimonio;
			$dados["situacaoveiculo"] = $situacaoveiculo;
			$dados["logisticafrotascol"] = $logisticafrotascol;
			$dados["kminicial"] = $kminicial;
			$dados["combustivel"] = $combustivel;
			$dados["renavam"] = $renavam;
			$dados["chassi"] = $chassi;
			$dados["placa"] = $placa;
			$dados["cor"] = $cor;
			$dados["capacidadepotencia"] = $capacidadepotencia;
			$dados["idproprietario"] = $idproprietario;
			$dados["especie"] = $especie;
			$dados["categoria"] = $categoria;
			$dados["veiculo"] = $veiculo;
			$dados["anofabricacao"] = $anofabricacao;
			$dados["emprestimo"] = $emprestimo;
			$dados["veiculopublico"] = $veiculopublico;
			$dados["quantidade"] = $quantidade;
			$dados["reservadepartamentos"] = $reservadepartamentos;
			$dados["veiculodisponiveldepartamento"] = $veiculodisponiveldepartamento;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $logisticafrotas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}