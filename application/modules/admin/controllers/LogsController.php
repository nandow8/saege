<?php
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        $image_file = "public/admin/imagens/logosantaisabel.jpg";
        $this->Image($image_file, 20, 15, 10, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);

        //Set font
        $this->SetFont('helvetica', 'B', 20);

        $this->SetFont('helvetica', 'B', 10);

        // cabeçalho Endereço
        $this->SetXY(16, 12);
        $this->Cell(0, 0, 'Prefeitura Municipal de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 9);
        $this->SetXY(35, 16);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Logs do Sistema', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(35, 19);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(35, 22);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetXY(35, 25);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');


        //informações Saege
        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60, 15);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $patrimonios = new Patrimonios();
        $data = $patrimonios->getCurrentData();

        $this->SetFont('helvetica', '', 8);
        $this->SetXY(60, 18);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Data: ' . $data, 0, false, 'R', 0, '', 0, false, 'M', 'M');

        $this->SetFont('helvetica', '', 10);
        $this->SetXY(40, 21);
        $this->SetFillColor(127);
        $this->SetTextColor(127);
        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

        // Position at 15 mm from bottom
        $this->SetXY(150, -15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

}
/**
 * Controle da classe logs do sistema
 *
 * @author	Julio Cesar Santos
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2018 FJMX Sistemas. (http://www.fjmx.com.br)
 * @version     1.0
 */
class Admin_LogsController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Usuario
     */
    protected $_usuario = null;
    protected $_isAdmin = false;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("usuarios", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }

    }


    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Logs')
        );

        $ns = new Zend_Session_Namespace('admin_logs');
        $logs = new Logs();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();
        }
        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
           
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);
            
            if ((isset($this->view->post_var['data_i'])) && ($this->view->post_var['data_i'] != ''))
                $queries['data_i'] = $this->view->post_var['data_i'];
            
            if ((isset($this->view->post_var['data_f'])) && ($this->view->post_var['data_f'] != ''))
                $queries['data_f'] = $this->view->post_var['data_f'];
            
            if ((isset($this->view->post_var['lgnomeusuario'])) && ($this->view->post_var['lgnomeusuario'] != ''))
                $queries['lgnomeusuario'] = $this->view->post_var['lgnomeusuario'];
            
            if ((isset($this->view->post_var['lgmensagem'])) && ($this->view->post_var['lgmensagem'] != ''))
                $queries['lgmensagem'] = $this->view->post_var['lgmensagem'];
            
            if ((isset($this->view->post_var['lgtpmensagem'])) && ($this->view->post_var['lgtpmensagem'] != ''))
                $queries['lgtpmensagem'] = $this->view->post_var['lgtpmensagem'];
            
            if ((isset($this->view->post_var['lgipusuario'])) && ($this->view->post_var['lgipusuario'] != ''))
                $queries['lgipusuario'] = $this->view->post_var['lgipusuario'];
            
            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];

        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;

        $totalRegistros = $logs->getLogs($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;
        $queries['die'] = true;
        $this->view->rows = $logs->getLogs($queries, $paginaAtual, $maxpp);
    }

    /**
     *
     * Action de visualização
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'log', 'name' => 'Log'),
            array('url' => null, 'name' => 'Visualizar Log')
        );

        $lgid = (int) $this->_request->getParam("lgid");
        $logs = new Logs();
        $log = $logs->fetchRow("lgid=" . $lgid);

        if (!$log)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $log;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idusuario
     */
    private function preForm($idusuario = 0) {
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost() {
        return "";
    }

    public function logsdocumentoAction() {
        $ns = new Zend_Session_Namespace('admin_logs');

        $logs = new Logs();
        $queries = array();

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
           
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);
            
            if ((isset($this->view->post_var['data_i'])) && ($this->view->post_var['data_i'] != ''))
                $queries['data_i'] = $this->view->post_var['data_i'];
            
            if ((isset($this->view->post_var['data_f'])) && ($this->view->post_var['data_f'] != ''))
                $queries['data_f'] = $this->view->post_var['data_f'];
            
            if ((isset($this->view->post_var['lgnomeusuario'])) && ($this->view->post_var['lgnomeusuario'] != ''))
                $queries['lgnomeusuario'] = $this->view->post_var['lgnomeusuario'];
            
            if ((isset($this->view->post_var['lgmensagem'])) && ($this->view->post_var['lgmensagem'] != ''))
                $queries['lgmensagem'] = $this->view->post_var['lgmensagem'];
            
            if ((isset($this->view->post_var['lgtpmensagem'])) && ($this->view->post_var['lgtpmensagem'] != ''))
                $queries['lgtpmensagem'] = $this->view->post_var['lgtpmensagem'];
            
            if ((isset($this->view->post_var['lgipusuario'])) && ($this->view->post_var['lgipusuario'] != ''))
                $queries['lgipusuario'] = $this->view->post_var['lgipusuario'];
            
            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];

        }

        $_row = $logs->getLogs($queries);
        
        $data = $logs->getCurrentData();
        $local = 'Santa Isabel';

        $localData = $local . ' , ' . $data;
        $this->view->rows = $_row;
       
        $this->view->localdata = $localData;

        $this->preForm();

        $db = Zend_Registry::get('db');

        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Logs do Sistema');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 9);

        // add a page
        $pdf->AddPage();

        $html = $this->view->render('logs/pdf/logsdocumento.phtml');
        $image_file = "public/admin/imagens/logosantaisabel.jpg";

        $pdf->writeHTML($html, true, 0, true, 0);

        $filename = 'logs.pdf';
        $pdf->Output($filename, 'I');

        die();
        return true;
       
    }
}
