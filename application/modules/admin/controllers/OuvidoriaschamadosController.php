<?php

/**
 * Controle da classe ouvidoriaschamados do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_OuvidoriaschamadosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Ouvidoriachamado
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("ouvidoriaschamados", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Ouvidoriaschamados();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="ouvidoriaschamados") $objs = new Ouvidoriaschamados();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}

	/**
	 * Action para encaminhar via Ajax
	 */
	public function encaminharAction() {
		$this->_helper->layout->disableLayout();
		
		$id = (int)$this->getRequest()->getPost("id");
		
		$objs = new Ouvidoriaschamados();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		
		if ($obj) {
			$obj = $obj->toArray();
			$obj['dataencaminhamento'] = date('Y-m-d G:i:s');
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ouvidoriaschamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Visualizar Chamado')
		);
		
		$id = (int)$this->_request->getParam("id");
		$ouvidoriaschamados = new Ouvidoriaschamados();
		$ouvidoriachamado = $ouvidoriaschamados->getOuvidoriachamadoById($id, array());
		
		if (!$ouvidoriachamado) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $ouvidoriachamado;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Chamados')
		);
		
		$ns = new Zend_Session_Namespace('default_ouvidoriaschamados');
		$ouvidoriaschamados = new Ouvidoriaschamados();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["tipoprocesso"]!="") $queries["tipoprocesso"] = $this->view->post_var["tipoprocesso"];
			if ($this->view->post_var["tipochamado"]!="") $queries["tipochamado"] = $this->view->post_var["tipochamado"];
			if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
			if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
			if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
			    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $ouvidoriaschamados->getOuvidoriaschamados($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $ouvidoriaschamados->getOuvidoriaschamados($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de ouvidoriaschamados
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ouvidoriaschamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Editar Chamado')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$ouvidoriaschamados = new Ouvidoriaschamados();
		$ouvidoriachamado = $ouvidoriaschamados->getOuvidoriachamadoById($id);
		
		if (!$ouvidoriachamado) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $ouvidoriachamado;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($ouvidoriachamado);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de ouvidoriaschamados 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ouvidoriaschamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Adicionar Chamado')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idouvidoriachamado
     */    
    private function preForm($idouvidoriachamado = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_ouvidoriachamado = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$tipoprocesso = trim($this->getRequest()->getPost("tipoprocesso"));
		$tipochamado = trim($this->getRequest()->getPost("tipochamado"));
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
		$horaabertura = trim($this->getRequest()->getPost("horaabertura"));
		$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
		$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
		$tipocontato = trim($this->getRequest()->getPost("tipocontato"));
		$nomesolicitante = trim($this->getRequest()->getPost("nomesolicitante"));
		$telefone1 = trim($this->getRequest()->getPost("telefone1"));
		$telefone2 = trim($this->getRequest()->getPost("telefone2"));
		$contato = trim($this->getRequest()->getPost("contato"));
		$idtiposchamado = (int)trim($this->getRequest()->getPost("idtiposchamado"));
		$assunto = trim($this->getRequest()->getPost("assunto"));
		$descricao = trim($this->getRequest()->getPost("descricao"));
		$observacoes = trim($this->getRequest()->getPost("observacoes"));
		$iddocumento = (int)trim($this->getRequest()->getPost("iddocumento"));
		$posicao = trim($this->getRequest()->getPost("posicao"));
		$status = trim($this->getRequest()->getPost("status1"));

		if (""==$sequencial) $sequencial = str_pad((string)Ouvidoriaschamados::getIdUltimaOuvidoriachamado(), 5, "0", STR_PAD_LEFT) . "/" . (string)date("Y");
		if (""==$datalancamento) $datalancamento = date("Y-m-d");
		if (""==$horaabertura) $horaabertura = date("G:i:s");
		if (""==$idusuario) $idusuario = Usuarios::getUsuario('id');
		if (""==$iddepartamento) $iddepartamento = Usuarios::getUsuario('iddepartamento');
		
		$erros = array();
		
		if (""==$tipoprocesso) array_push($erros, "Informe o Tipo do Processo.");
		if (""==$tipochamado) array_push($erros, "Informe o Tipo do Chamado.");
		if (""==$posicao) array_push($erros, "Informe a Posição.");
		if (""==$status) array_push($erros, "Informe o Status.");

		$ouvidoriaschamados = new Ouvidoriaschamados();

		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["tipoprocesso"] = $tipoprocesso;
			$dados["tipochamado"] = $tipochamado;
			$dados["sequencial"] = $sequencial;
			$dados["datalancamento"] = $datalancamento;
			$dados["horaabertura"] = $horaabertura;
			$dados["idusuario"] = $idusuario;
			$dados["iddepartamento"] = $iddepartamento;
			$dados["tipocontato"] = $tipocontato;
			$dados["nomesolicitante"] = $nomesolicitante;
			$dados["telefone1"] = $telefone1;
			$dados["telefone2"] = $telefone2;
			$dados["contato"] = $contato;
			$dados["idtiposchamado"] = $idtiposchamado;
			$dados["assunto"] = $assunto;
			$dados["descricao"] = $descricao;
			$dados["observacoes"] = $observacoes;

			$iddocumento = $this->getArquivo('iddocumento');
			if ($iddocumento!=0) $dados['iddocumento'] = $iddocumento;
			
			$dados["posicao"] = $posicao;
			$dados["status"] = $status;

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $ouvidoriaschamados->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}