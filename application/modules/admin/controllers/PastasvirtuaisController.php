<?php

/**
 * Controle da classe pastasvirtuais do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PastasvirtuaisController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pastavirtual
	 */
	protected $_usuario = null;	
	
	protected $_caminhopastas = null;
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("pastasvirtuais", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Pastasvirtuais();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Pasta virtual excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="pastasvirtuais") $objs = new Pastasvirtuais();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'pastasvirtuais', 'name' => 'Pastas virtuais'),
			array('url' => null,'name' => 'Visualizar Pasta virtual')
		);
		
		$id = (int)$this->_request->getParam("id");
		$pastasvirtuais = new Pastasvirtuais();
		$pastavirtual = $pastasvirtuais->getPastavirtualById($id, array());
		
		if (!$pastavirtual) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $pastavirtual;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Pastas virtuais')
		);
		
		$ns = new Zend_Session_Namespace('default_pastasvirtuais');
		$pastasvirtuais = new Pastasvirtuais();
		$queries = array();	
		
		$queries['parentzero'] = true;
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
$queries["origem"] = "Comunicação";
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $pastasvirtuais->getPastasvirtuais($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $pastasvirtuais->getPastasvirtuais($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de pastasvirtuais
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'pastasvirtuais', 'name' => 'Pastas virtuais'),
			array('url' => null,'name' => 'Editar Pasta virtual')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$save = $this->getRequest()->getPost("save");
		$pastasvirtuais = new Pastasvirtuais();
		$pastavirtual = $pastasvirtuais->getPastavirtualById($id);
		
		if (!$pastavirtual) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $pastavirtual;
		$this->preForm($id, 'editar');
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($pastavirtual);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Pasta virtual editado com sucesso.";
			
			if(($id > 0) && ($save=="continuar")):
				$this->_redirect('admin/'.$this->getRequest()->getControllerName() . '/editar/id/' . $id);
			else:
				$this->_redirect('admin/'.$this->getRequest()->getControllerName());
			endif;
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de pastasvirtuais 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'pastasvirtuais', 'name' => 'Pastas virtuais'),
			array('url' => null,'name' => 'Adicionar Pasta virtual')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$save = $this->getRequest()->getPost("save");
		
		
		$this->preForm($id, 'adicionar');
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Pasta adicionada com sucesso.";
			
			if(($id > 0) && ($save=="continuar")):
				$this->_redirect('admin/'.$this->getRequest()->getControllerName() . '/editar/id/' . $id);
			else:
				$this->_redirect('admin/'.$this->getRequest()->getControllerName());
			endif;	
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());
		}
		return true;			
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idpastavirtual
     */    
    private function preForm($idpasta = 0, $modulo = false) {
               $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
                
		$usuarios = new Usuarios();
		$this->view->usuarios = $usuarios->getUsuarios(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		if((int)$idpasta > 0) :	
		$pastasvirtuais = new Pastasvirtuais();
		$this->view->pastas = $pastasvirtuais->getPastasvirtuais(array('status'=>'Ativo', 'idparent'=>$idpasta));
		endif;
		$caminhopastasNameSpace = new Zend_Session_Namespace("caminhopastas");
		$caminhosession = $caminhopastasNameSpace->caminhopastas;
		$montalink = true;
		$montalinkparent = false;
		$novocaminho = array();
		
		//var_dump($idpasta); die();
		if((int)$idpasta > 0) :			
			//idparent
			$row = $pastasvirtuais->getPastavirtualById($idpasta);
    		if(isset($caminhosession)){
	    		if(!is_array($caminhosession)){    			
					$caminhosession = $caminhosession->toArray();
				}				
			}else{
				$caminhosession = array();
			}
			
			foreach ($caminhosession as $i=>$_row) :			
				if (in_array($idpasta, $_row)) {
					$montalink = false;
					break; 
				}
				
				array_push($novocaminho, $_row);
				
    			if (in_array($row['idparent'], $_row)) {
					$montalinkparent = true;
					break; 
				}
			endforeach;

			if (($montalink) && (!$montalinkparent)):
				unset($caminhosession);
				$caminhosession = array();
				$caminho = array();
				$caminho['id'] = $row['id'];
				$caminho['titulo'] = $row['titulo'];				
				array_push($caminhosession, $caminho);			
					
				$caminhopastasNameSpace->caminhopastas = $caminhosession;	
			elseif($montalinkparent):
				$caminhosession = $novocaminho;
				$caminho = array();
				$caminho['id'] = $row['id'];
				$caminho['titulo'] = $row['titulo'];				
				array_push($caminhosession, $caminho);				
				$caminhopastasNameSpace->caminhopastas = $caminhosession;
			elseif((int)$row['idparent']==0):
				unset($caminhosession);
				$caminhosession = array();
				
				$caminhosession = $novocaminho;
				$caminho = array();
				$caminho['id'] = $row['id'];
				$caminho['titulo'] = $row['titulo'];				
				array_push($caminhosession, $caminho);				
				$caminhopastasNameSpace->caminhopastas = $caminhosession;
			endif;
		else: 
			unset($caminhopastasNameSpace->caminhopastas);
		endif;
		
		$this->view->breadcrumbpastas = $caminhopastasNameSpace->caminhopastas;
		//var_dump($this->view->breadcrumbpastas); die();
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_pastavirtual = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)trim($this->getRequest()->getPost("idsecretaria"));
$idparent = (int)trim($this->getRequest()->getPost("idparent"));
$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
$titulo = trim($this->getRequest()->getPost("titulo"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$tipo = trim($this->getRequest()->getPost("tipo"));
$idusuariocriacao = (int)trim($this->getRequest()->getPost("idusuariocriacao"));
$origem = trim($this->getRequest()->getPost("origem"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$titulo) array_push($erros, "Informe a Título.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$pastasvirtuais = new Pastasvirtuais();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] =Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
$dados["idparent"] = $idparent;
//$dados["idusuario"] = $idusuario;
$dados["titulo"] = $titulo;
$dados["descricoes"] = $descricoes;
$dados["tipo"] = 'Publica';
if($id <= 0) $dados['idusuariocriacao'] = $this->_usuario['id'];
$dados["origem"] = "Comunicação";
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $pastasvirtuais->save($dados);
			
			$idsarquivos = $this->getRequest()->getPost("idsarquivos");
			$legendasarquivos =  $this->getRequest()->getPost("legendasarquivos");
				
			$multiplosarquivos = new Pastasvirtuaisarquivos(); 
			$multiplosarquivos->setArquivos($row['id'], $idsarquivos, $legendasarquivos);

			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}