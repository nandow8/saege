<?php

/**
 * Controle da classe logisticamotoristas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_LogisticamotoristasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Logisticamotorista
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("logisticamotoristas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Logisticamotoristas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Motorista excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="logisticamotoristas") $objs = new Logisticamotoristas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticamotoristas', 'name' => 'Motoristas'),
			array('url' => null,'name' => 'Visualizar Motorista')
		);
		
		$id = (int)$this->_request->getParam("id");
		$logisticamotoristas = new Logisticamotoristas();
		$logisticamotorista = $logisticamotoristas->getLogisticamotoristaById($id, array());
		
		if (!$logisticamotorista) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticamotorista;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Motoristas')
		);
		
		$ns = new Zend_Session_Namespace('default_logisticamotoristas');
		$logisticamotoristas = new Logisticamotoristas();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
if ($this->view->post_var["rg"]!="") $queries["rg"] = $this->view->post_var["rg"];
if ($this->view->post_var["cpf"]!="") $queries["cpf"] = $this->view->post_var["cpf"];
if ($this->view->post_var["nomerazao"]!="") $queries["nomerazao"] = $this->view->post_var["nomerazao"];
// if ($this->view->post_var["sobrenomefantasia"]!="") $queries["sobrenomefantasia"] = $this->view->post_var["sobrenomefantasia"];
if ($this->view->post_var["email"]!="") $queries["email"] = $this->view->post_var["email"];
if ($this->view->post_var["categoriacnh"]!="") $queries["categoriacnh"] = $this->view->post_var["categoriacnh"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $logisticamotoristas->getLogisticamotoristas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $logisticamotoristas->getLogisticamotoristas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de logisticamotoristas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticamotoristas', 'name' => 'Motoristas'),
			array('url' => null,'name' => 'Editar Motorista')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$logisticamotoristas = new Logisticamotoristas();
		$logisticamotorista = $logisticamotoristas->getLogisticamotoristaById($id);
		
		if (!$logisticamotorista) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticamotorista;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($logisticamotorista);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Motorista editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de logisticamotoristas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticamotoristas', 'name' => 'Motoristas'),
			array('url' => null,'name' => 'Adicionar Motorista')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Motorista adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
	}	
	
    public function getfuncioanrioAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");		

		$id = (int)$this->getRequest()->getPost('idfuncionario');
	    $funcionario = new Funcionariosgerais();
		$rows = $funcionario->getFuncionariogeralByIdHelper($id);
 
		$departamento = Departamentossecretarias::getDepartamentosecretariaByIdHelper($rows['iddepartamentosecretaria']);
 
		echo json_encode($departamento);
		die();

	}

    
    /**
     * Atribui valores ao view
     * @param int $idlogisticamotorista
     */    
    private function preForm($idlogisticamotorista = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_logisticamotorista = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)trim($this->getRequest()->getPost("idsecretaria"));
		$idarquivocnh = (int)trim($this->getRequest()->getPost("idarquivocnh"));
		$idfornecedor = (int)trim($this->getRequest()->getPost("idfornecedor"));
		$idimagem = (int)trim($this->getRequest()->getPost("idimagem"));
		$idcertificadotransporte = (int)trim($this->getRequest()->getPost("idcertificadotransporte"));
		$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
		$rg = trim($this->getRequest()->getPost("rg"));
		$cpf = trim($this->getRequest()->getPost("cpf"));
		$datanascimento = Mn_Util::stringToTime($this->getRequest()->getPost("datanascimento"));
		$nomerazao = trim($this->getRequest()->getPost("nomerazao"));
		$sobrenomefantasia = trim($this->getRequest()->getPost("sobrenomefantasia"));
		$email = trim($this->getRequest()->getPost("email"));
		$telefone = trim($this->getRequest()->getPost("telefone"));
		$celular = trim($this->getRequest()->getPost("celular"));
		$categoriacnh = trim($this->getRequest()->getPost("categoriacnh"));
		$registrocnh = trim($this->getRequest()->getPost("registrocnh"));
		$datavalidadecnh = Mn_Util::stringToTime($this->getRequest()->getPost("datavalidadecnh"));
		$dataprimeiracnh = Mn_Util::stringToTime($this->getRequest()->getPost("dataprimeiracnh"));
		$dataemissaocnh = Mn_Util::stringToTime($this->getRequest()->getPost("dataemissaocnh"));
		$transporteescolar = trim($this->getRequest()->getPost("transporteescolar"));
		$funcionariopublico = trim($this->getRequest()->getPost("funcionariopublico"));
		$terceirizado = trim($this->getRequest()->getPost("terceirizado"));
		$observacao = trim($this->getRequest()->getPost("observacao"));
		$diasaviso_vencimento = trim($this->getRequest()->getPost("diasaviso_vencimento"));
		$datacertificado = Mn_Util::stringToTime($this->getRequest()->getPost("datacertificado"));
		$status = trim($this->getRequest()->getPost("status1"));
		$input_webcam = trim($this->getRequest()->getPost("input_webcam"));	

		$tipoveiculo = $this->getRequest()->getPost("tipoveiculo");
		if(sizeof($tipoveiculo) > 0) $tipoveiculo = implode(',', $tipoveiculo);
		//var_dump($tipoveiculo);die();
		
		
		$erros = array();
		
		//if (0==$idfuncionario) array_push($erros, "Informe a Funcionário.");
if($$transporteescolar == 'nao'){
	if (""==$tipoveiculo) array_push($erros, "Informe a Tipo de veículo Autorizado.");
}
if (""==$email) array_push($erros, "Informe a Email.");
if (""==$celular) array_push($erros, "Informe a Celular.");
if (""==$categoriacnh) array_push($erros, "Informe a Categoria CNH.");
if (""==$registrocnh) array_push($erros, "Informe a Nº Registro CNH.");
if (""==$datavalidadecnh) array_push($erros, "Informe a Data validade CNH.");
if (""==$dataemissaocnh) array_push($erros, "Informe a Data Emissão CNH.");
if (""==$terceirizado) array_push($erros, "Informe a Motorista terceirizado.");
if (""==$status) array_push($erros, "Informe a Status.");
if($terceirizado == 'sim'){
	if (""==$rg) array_push($erros, "Informe o RG.");
	if (""==$cpf) array_push($erros, "Informe o cpf.");
	if (""==$transporteescolar) array_push($erros, "Informe o transporte.");
}

		
		$logisticamotoristas = new Logisticamotoristas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = $idsecretaria;
			$idarquivocnh = $this->getArquivo('idarquivocnh');
			if ($idarquivocnh!=0) $dados['idarquivocnh'] = $idarquivocnh;
			
			if ($input_webcam!='') {
				$input_webcam = str_replace("data:image/png;base64,", "", $input_webcam);
				$imagens = new Imagens();
				$__imagem = $imagens->saveBase64($input_webcam);
				$download = $imagens->downloadImagem($__imagem['id']);

				$simple = new SimpleImage();
				$simple->load($download['sourceFilename']);

				$src = $simple->image;
				$height = imagesy($src);
				$width = floor(($height/4)*3);
				$pos_x = floor((imagesx($src)-$width)/2);

				$dest = imagecreatetruecolor($width, $height);
				imagecopy($dest, $src, 0, 0, $pos_x, 0, $width, $height);
				imagejpeg($dest,$download['sourceFilename'],100);


				$dados['idimagem'] = $__imagem['id'];	
			} else {
				$idimagem = $this->getImagem('idimagem');
				if ($idimagem!=0) $dados['idimagem'] = $idimagem;		
				echo $dados['idimagem'];die;	
			}	
				
			
			$idcertificadotransporte = $this->getArquivo('idcertificadotransporte');
			if ($idcertificadotransporte!=0) $dados['idcertificadotransporte'] = $idcertificadotransporte;

			if( $idfuncionario == 0){
				$dados["idfuncionario"] = NULL;
			}else{
				$dados["idfuncionario"] = $idfuncionario;
			}
			$dados["idfornecedor"] = $idfornecedor;
			$dados["rg"] = $rg;
			$dados["cpf"] = $cpf;
			$dados["tipoveiculo"] = $tipoveiculo;
			$dados["datanascimento"] = date("Y-m-d", $datanascimento);
			$dados["nomerazao"] = $nomerazao;
			$dados["sobrenomefantasia"] = $sobrenomefantasia;
			$dados["email"] = $email;
			$dados["telefone"] = $telefone;
			$dados["celular"] = $celular;
			$dados["categoriacnh"] = $categoriacnh;
			$dados["registrocnh"] = $registrocnh;
			$dados["datavalidadecnh"] = date("Y-m-d", $datavalidadecnh);
			$dados["dataprimeiracnh"] = date("Y-m-d", $dataprimeiracnh);
			$dados["dataemissaocnh"] = date("Y-m-d", $dataemissaocnh);
			$dados["transporteescolar"] = $transporteescolar;
			$dados["funcionariopublico"] = $funcionariopublico;
			$dados["terceirizado"] = $terceirizado;
			$dados["observacao"] = $observacao;
			$dados["diasaviso_vencimento"] = $diasaviso_vencimento;
			$dados["datacertificado"] = date("Y-m-d", $datacertificado);
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $logisticamotoristas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}