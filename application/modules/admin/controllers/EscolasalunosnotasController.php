<?php

/**
 * Controle da classe escolasalunosnotas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EscolasalunosnotasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Escolasalunosnota
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("escolasalunosnotas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Escolasalunosnotas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Nota excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="escolasalunosnotas") $objs = new Escolasalunosnotas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasalunosnotas', 'name' => 'Notas'),
			array('url' => null,'name' => 'Visualizar Nota')
		);
		
		$id = (int)$this->_request->getParam("id");
		$escolasalunosnotas = new Escolasalunosnotas();
		$escolasalunosnota = $escolasalunosnotas->getEscolasalunosnotaById($id, array());
		
		if (!$escolasalunosnota) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolasalunosnota;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Notas')
		);
		

		$ns = new Zend_Session_Namespace('admin_escolasalunosnotas');
		$escolasvinculos = new Escolasvinculos();
		$queries = array();	
		
		$idescola = Usuarios::getUsuario('idescola');
		$idescola = (int)$idescola;
		if((Usuarios::getUsuario('professor')=="Sim") && ($idescola>0)){

			//ENCONTRA ATRIBUICOS DO PROFESSOR COM AS SALAS
			$ids_vinculos = Escolasvinculosprofessoresmaterias::getIdsEscolasvinculosprofessorByIdHelper(Usuarios::getUsuario('idfuncionario'), array('idescola' => $idescola));
			//var_dump($ids_vinculos);die();
			if($ids_vinculos != '')
			{
				$queries['ids'] = (isset($ids_vinculos) && $ids_vinculos != '') ? $ids_vinculos : false;
			}
			
			$queries['idescola'] = $idescola;
		}
		
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
    		
if ($this->view->post_var["idserie"]!="") $queries["idserie"] = $this->view->post_var["idserie"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $escolasvinculos->getEscolasvinculos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $escolasvinculos->getEscolasvinculos($queries, $paginaAtual, $maxpp);
		
	}
	
	/**
	 * 
	 * Action de edição de escolasalunosnotas
	 */	
	public function editarAction() {

				
		$id = (int)$this->_request->getParam("id");
		$escolasvinculos = new Escolasvinculos();
		$escolavinculo = $escolasvinculos->getEscolavinculoById($id);
		
		if (!$escolavinculo) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolavinculo;
		//var_dump($escolavinculo);die();

		$this->view->bread_crumb = array(
			array('url' => 'escolasalunosnotas', 'name' => 'Notas da série ' . $escolavinculo['serie'] . ' ' . $escolavinculo['turma']),
			array('url' => null,'name' => 'Editar Nota')
		);	

		$this->preForm();
		$alunos = new Escolassalasatribuicoes();
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['idvinculo'] = $id;
		
		$this->view->escolasalunos = $alunos->getSalasatribuicoes($queries);	
		

		$this->view->materias = Controlealunosmaterias::getControlealunosmateriasHelper(array('status'=>'Ativo'));
		/*if(Escolasusuarios::getUsuario('professor')=="Sim"):
			$this->view->materias = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>Escolasusuarios::getUsuario('id'), 'idescolavinculo'=>$id));
		endif;*/
			
		if ($this->_request->isPost()) {
			$erros = $this->getPost($escolavinculo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Notas adicionadas com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName() . '/');
		}

		return true;		
    }  	
	/**
	 * 
	 * Action de adição de escolasalunosnotas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasalunosnotas', 'name' => 'Notas'),
			array('url' => null,'name' => 'Adicionar Nota')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Nota adicionado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    		
	

	public function setmateriasAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idprofessor = $this->getRequest()->getPost('idprofessor');
		$idescolavinculo = $this->getRequest()->getPost('idescolavinculo');
    	if((int)$idprofessor <=0) return $this->view->rows = array();
		$professor = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($idprofessor);
		if(!$professor) return $this->view->rows = array();

		//var_dump($professor);die();
		/*
		$controlealunosmaterias = new Controlealunosmaterias();
		$queries_professores = array();	
		$queries_professores['status'] = "Ativo";
		
		$queries_professores['find_in_set'] = ((isset($professor['idsmaterias'])) && ($professor['idsmaterias']!="")) ? $professor['idsmaterias'] : false;
		$this->view->materiasprofessores = $controlealunosmaterias->getControlealunosmaterias($queries_professores);
		*/
		$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo));
		//var_dump($this->view->materiasprofessores);die();
	}

	public function setnotasAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		if(Escolasusuarios::getUsuario('professor')=="Sim"){
			$idprofessor = Escolasusuarios::getUsuario('id');
		}else{
			$idprofessor = $this->getRequest()->getPost('idprofessor');	
		}
		

		$idescolavinculo = $this->getRequest()->getPost('idescolavinculo');
		$idmateria = (int)$this->getRequest()->getPost('idmateria');
		$idserie = (int)$this->getRequest()->getPost('idserie');
		//var_dump($idserie);die();
		$bimestre = $this->getRequest()->getPost('bimestre');

		$materia = Controlealunosmaterias::getControlealunosmateriaByIdHelper($idmateria);

    	//$configuracoesdiferenciadas = Escolasconfiguracoesdiferenciadas::getEscolasconfiguracoesdiferenciadasHelper(array('idescola' => Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'), 'idmateria' => $idmateria));
    	$configuracoesdiferenciadas = Escolasconfiguracoesdiferenciadas::getEscolasconfiguracoesdiferenciadasHelper(array('idescola' => Usuarios::getEscolaAtiva(Usuarios::getUsuario('idescola'), 'id'), 'idmateria' => $idmateria, 'idserie' => $idserie, 'order'=>'order by titulo'));
    	if($idmateria > 0)
    	{
    		$this->view->configuracoesnotasdiferenciadas = $configuracoesdiferenciadas;
    	}

		//var_dump($this->view->configuracoesnotasdiferenciadas);die();

		//$configuracoesnotas = Escolasconfiguracoes
    	
    	$escolasalunosnotas = new Escolasalunosnotas();
    	$this->view->rows = $escolasalunosnotas->getEscolasalunosnotas(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo, 'idmateria'=>$idmateria, 'bimestre'=>$bimestre));

		$alunos = new Escolassalasatribuicoes();
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['idvinculo'] = $idescolavinculo;
		$queries['die'] = true;
		
		$this->view->escolasalunos = $alunos->getSalasatribuicoes($queries);
		$this->view->idprofessor = $idprofessor;
		$this->view->idescolavinculo = $idescolavinculo;
		$this->view->idmateria = $idmateria;
		$this->view->bimestre = $bimestre;
		$this->view->materia = $materia;
		$this->preForm();
		//var_dump($this->view->materiasprofessores); die();
	}
    /**
     * Atribui valores ao view
     * @param int $idescolasalunosnota
     */    
    private function preForm($idescolasalunosnota = 0) {
    	$escolasconfiguracoes = new Escolasconfiguracoes();
    	$queries = array();
    	$queries['idescola'] = 1;//Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	$escolas = $escolasconfiguracoes->getEscolasconfiguracoes($queries);
    	$_escolas = array();
    	if(sizeof($escolas) > 0) $_escolas = $escolas[0];
    	$this->view->configuracoesnotas = $_escolas;
    	//var_dump($this->view->configuracoesnotas); die();

		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
    	$this->view->professores = $funcionariosgeraisescolas->getFuncionariosgeraisescolas(array('status'=>'Ativo', 'professor'=>'Sim'));
    	//var_dump($this->view->professores); die();
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_escolasalunosnota = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		//var_dump($_POST); die();

		$id = (int)$this->getRequest()->getPost("id");
		$bimestre = $this->getRequest()->getPost("bimestre");
//		$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 
		$idescola = 1;
$idsnotas = $this->getRequest()->getPost("idsnotas");
$idstrabalhos = $this->getRequest()->getPost("idstrabalhos");
$notas = $this->getRequest()->getPost("notas");
$_idescolavinculo = $this->getRequest()->getPost("idescolavinculo");
$_idmateria = $this->getRequest()->getPost("idmateria");
$_idprofessor = $this->getRequest()->getPost("idprofessor");
$_idaluno = $this->getRequest()->getPost("idaluno");
$_numeronota = $this->getRequest()->getPost("numeronotas");
$_bimestre = $this->getRequest()->getPost("bimestres");

$numeronotaprova = $this->getRequest()->getPost("numeronotaprova");
$bimestreprova = $this->getRequest()->getPost("bimestreprova");
$tipoprova = $this->getRequest()->getPost("tipoprova");
$tipos = $this->getRequest()->getPost("tipos");

$status = trim($this->getRequest()->getPost("status1"));
$escolasalunosnotas = new Escolasalunosnotas();
foreach ($notas as $i => $nota) {
	$id = $idsnotas[$i];
	$idescolavinculo = $_idescolavinculo[$i];
	$idmateria = $_idmateria[$i];
	$idprofessor = $_idprofessor[$i];
	$idaluno = $_idaluno[$i];
	$numeronota = $_numeronota[$i];
	$_nota = MN_Util::trataNum(trim($nota));
	//$bimestre = $_bimestre[$i];
	$tipo = $tipos[$i];

		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();			
			$dados["id"] = $id;
			$dados["idescola"] = $idescola;
			$dados["idescolavinculo"] = $idescolavinculo;
			$dados["idmateria"] = $idmateria;
			$dados["idprofessor"] = $idprofessor;//$idprofessor;
			$dados["idaluno"] = $idaluno;
			$dados["numeronota"] = $numeronota;
			$dados["nota"] = $_nota;
			$dados["bimestre"] = $bimestre;
			$dados["data"] = date("Y-m-d");
			$dados["status"] = $status;
			$dados["tipo"] = $tipo;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			//var_dump($dados); die()	;
			$row = $escolasalunosnotas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
	}
/*	
$trabalhos = $this->getRequest()->getPost("trabalhos");
$numeronotatrabalho = $this->getRequest()->getPost("numeronotatrabalho");
$bimestretrabalho = $this->getRequest()->getPost("bimestretrabalho");
$tipotrabalho = $this->getRequest()->getPost("tipotrabalho");
	foreach ($trabalhos as $i => $trabalho) {
		$id = $idstrabalhos[$i];
		$idescolavinculo = $_idescolavinculo[$i];
		$idmateria = $_idmateria[$i];
		$idprofessor = $_idprofessor[$i];
		$idaluno = $_idaluno[$i];
		$numeronota = $numeronotatrabalho[$i];
		$_nota = MN_Util::trataNum($trabalhos[$i]);
		$bimestre = $bimestretrabalho[$i];
		$tipo = $tipotrabalho[$i];


			$dados = array();			
			$dados["id"] = $id;
			$dados["idescola"] = $idescola;
			$dados["idescolavinculo"] = $idescolavinculo;
			$dados["idmateria"] = $idmateria;
			$dados["idprofessor"] = $idprofessor;
			$dados["idaluno"] = $idaluno;
			$dados["numeronota"] = $numeronota;
			$dados["nota"] = $nota;
			$dados["bimestre"] = $bimestre;
			$dados["data"] = date("Y-m-d");
			$dados["status"] = $status;
			$dados["tipo"] = $tipo;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			//var_dump($dados); die()	;
			$escolasalunosnotas->save($dados);

	}

$idescolavinculo = (int)trim($this->getRequest()->getPost("idescolavinculo"));
$idmateria = (int)trim($this->getRequest()->getPost("idmateria"));
$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
$numeronota = trim($this->getRequest()->getPost("numeronota"));
//$nota = MN_Util::trataNum(trim($this->getRequest()->getPost("nota")));
$bimestre = trim($this->getRequest()->getPost("bimestre"));
$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
*/

		
		
		$erros = array();
/*		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
if (0==$idescolavinculo) array_push($erros, "Informe a Série.");
if (0==$idmateria) array_push($erros, "Informe a Matéria.");
if (0==$idprofessor) array_push($erros, "Informe a Professor.");
if (0==$idaluno) array_push($erros, "Informe a Aluno.");
if (""==$numeronota) array_push($erros, "Informe a Nº da nota.");
if (0==$nota) array_push($erros, "Informe a Nota.");
if (""==$bimestre) array_push($erros, "Informe a Bimestre.");
if (""==$status) array_push($erros, "Informe a Status.");
*/
		
		return "";
		
		/*
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
$dados["idescolavinculo"] = $idescolavinculo;
$dados["idmateria"] = $idmateria;
$dados["idprofessor"] = $idprofessor;
$dados["idaluno"] = $idaluno;
$dados["numeronota"] = $numeronota;
$dados["nota"] = $nota;
$dados["bimestre"] = $bimestre;
$dados["data"] = date("Y-m-d", $data);
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $escolasalunosnotas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";  */  	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}