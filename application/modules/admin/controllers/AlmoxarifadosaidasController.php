<?php

require_once BASE_PATH . '/library/tcpdf/tcpdf.php';
define('MY_PATH', 'http://mnsolucoes.com');

class Admin_AlmoxarifadosaidasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacotesaida
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("almoxarifadosaidas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}	
	

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Almoxarifado_Saidas();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Usuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Saída excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="almoxarifadosaidas") $objs = new Almoxarifado_Saidas();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Usuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Almoxarifado_Saidas();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Almoxarifado_Saidas();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/admin/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Saídas')
		);
		
		
		$ns = new Zend_Session_Namespace('almoxarifado_almoxarifadosaidas');
		$saidas = new Almoxarifado_Saidas();
		$queries = array();	
                $queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if (($this->view->post_var['status']) && ($this->view->post_var['status']!='')) $queries['status'] = $this->view->post_var['status'];
    		if (($this->view->post_var['chave']) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		if (($this->view->post_var['origem']) && ($this->view->post_var['origem']!='')) $queries['origem'] = $this->view->post_var['origem'];
    		if (($this->view->post_var['iddepartamentosecretaria']) && ($this->view->post_var['iddepartamentosecretaria']!='')) $queries['iddepartamentosecretaria'] = $this->view->post_var['iddepartamentosecretaria'];
    		if (($this->view->post_var['idescola']) && ($this->view->post_var['idescola']!='')) $queries['idescola'] = $this->view->post_var['idescola'];
    		if (($this->view->post_var['iddepartamentoescola']) && ($this->view->post_var['iddepartamentoescola']!='')) $queries['iddepartamentoescola'] = $this->view->post_var['iddepartamentoescola'];
    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $saidas->getSaidas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $saidas->getSaidas($queries, $paginaAtual, $maxpp);	
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosaidas',	'name' => 'Saídas'),
			array('url' => null,'name' => 'Visualizar saída')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$saidas = new Almoxarifado_Saidas();
		$saida = $saidas->getSaidaById($id);
		
		if (!$saida) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $saida;
		$this->preForm();
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de saida
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosaidas',	'name' => 'Saídas'),
			array('url' => null,'name' => 'Editar saída')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$saidas = new Almoxarifado_Saidas();
		$saida = $saidas->getSaidaById($id);
		
		if (!$saida) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $saida;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($saida);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Saída editada com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de saidas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosaidas',	'name' => 'Saídas'),
			array('url' => null,'name' => 'Adicionar Saída')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Saída adicionada com sucesso.";
			
			$this->_redirect('admin/'.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
	public function verificaestoqueAction() {
		$this->_helper->layout->disableLayout();
		
		$idproduto = $this->getRequest()->getPost('idp');
		$quantidade = $this->getRequest()->getPost('quantidade');
		
		$entradas = new Almoxarifado_Entradasitens();
		$quantidadeentrada = $entradas->getQuantidadeByIdProduto($idproduto, array('origem'=>Almoxarifado_Entradas::$_ORIGEM_SECRETARIA));
	}
	
	public function quantidadeestoqueAction() {
		$this->_helper->layout->disableLayout();
		
		$idproduto = $this->getRequest()->getPost('idproduto');
		$idsaida = $this->getRequest()->getPost('idsaida');
		
		$entradas = new Almoxarifado_Entradasitens();
		$quantidadeentrada = $entradas->getQuantidadeByIdProduto($idproduto, array('origem'=>Almoxarifado_Entradas::$_ORIGEM_SECRETARIA));
		
		$saidas = new Almoxarifado_Saidasitens();
		$quantidadesaida = $saidas->getQuantidadeByIdProduto($idproduto, array('diferentidsaida'=>$idsaida));

		$total = (int)$quantidadeentrada - (int)$quantidadesaida;
		echo $total;
		die();
	}

	public function setdepartamentosescolasAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$escolas = Escolas::getEscolaByIdHelper($idescola);
		if(!$idescola)  return $this->view->rows = false;

		$departamentos = new Departamentosescolas();
		$this->view->rows = $departamentos->getDepartamentosescolas(array('idescola'=>$idescola));
		
	}
    
	public function setgraficosAction(){
		//$this->_helper->layout->disableLayout();
		$this->_helper->layout->setLayout('modal');	
		
		$tipo = $this->_request->getParam("tipo");
		$this->view->tipo = $tipo;	

		$ns = new Zend_Session_Namespace('almoxarifado_almoxarifadosaidas');
		
		$queries = array();
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if (($this->view->post_var['status']) && ($this->view->post_var['status']!='')) $queries['status'] = $this->view->post_var['status'];
    		if (($this->view->post_var['chave']) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		if (($this->view->post_var['origem']) && ($this->view->post_var['origem']!='')) $queries['origem'] = $this->view->post_var['origem'];
    		if (($this->view->post_var['iddepartamentosecretaria']) && ($this->view->post_var['iddepartamentosecretaria']!='')) $queries['iddepartamentosecretaria'] = $this->view->post_var['iddepartamentosecretaria'];
    		if (($this->view->post_var['idescola']) && ($this->view->post_var['idescola']!='')) $queries['idescola'] = $this->view->post_var['idescola'];
    		if (($this->view->post_var['iddepartamentoescola']) && ($this->view->post_var['iddepartamentoescola']!='')) $queries['iddepartamentoescola'] = $this->view->post_var['iddepartamentoescola'];
    		
    	}
		
		$rows = new Almoxarifado_Produtos();	
		$this->view->rows = $rows->getProdutos($queries);
		
	}
	
	public function requisicaoAction() {
		$id = (int)$this->getRequest()->getParam('id');
		//SAIDA
		$saidas = new Almoxarifado_Saidas();
		$saida = $saidas->getSaidaById($id);
		
		if (!$saida) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $saida;
		
		$nome = "--";
		$endereco = "--";
		
		$quantidade = 0;
		if((int)$saida['idsolicitacao'] > 0):
			$solicitacao = Almoxarifado_Solicitacoes::getSolicitacaoByIdHelper($saida['idsolicitacao']);
			if($solicitacao):

			
			
			
				if($saida['origem']==Almoxarifado_Solicitacoes::$_ORIGEM_ESCOLA):
					$usuario = Escolasusuarios::getEscolausuarioHelper($solicitacao['idusuarioescola']);
					
					if($usuario):
						$nome = $usuario['escola'];
						$dados_enderecos = Enderecos::getEnderecoById((int)$usuario['e1idendereco']);
						if($dados_enderecos):
							$endereco = $dados_enderecos['endereco'] . ", N°  " . $dados_enderecos['numero'] . ", " . $dados_enderecos['bairro'] . ", " . $dados_enderecos['uf'];		
						endif;
					endif;
				elseif ($saida['origem']==Almoxarifado_Solicitacoes::$_ORIGEM_SECRETARIA):
					$secretaria = Secretarias::getSecretariaByIdHelper($solicitacao['idsecretaria']);
					if($secretaria):
						$nome = "Secretaria:" . $secretaria['secretaria'];		
						$dados_enderecos = Enderecos::getEnderecoById((int)$secretaria['idendereco']);
						if($dados_enderecos):
							$endereco = $dados_enderecos['endereco'] . ", N°  " . $dados_enderecos['numero'] . ", " . $dados_enderecos['bairro'] . ", " . $dados_enderecos['uf'];		
						endif;
					endif;
				endif;
			
			
			
			endif;
		endif;
		
		$dados_origens = array();
		$dados_origens['texto_nome'] = $nome;
		$dados_origens['texto_endereco'] = $endereco;
		$this->view->post_var = array_merge($this->view->post_var, $dados_origens);
		
		$this->preForm();
		//var_dump($this->view->post_var); die();
		
		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('l');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('REQUISIÇÃO DE MATERIAIS AUTORIZADOS');
		$pdf->SetSubject('REQUISIÇÃO DE MATERIAIS AUTORIZADOS');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		
		
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('almoxarifadosaidas/pdf/index.phtml');
		//echo $html; die();

		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'declaracao_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}


    /**
     * Atribui valores ao view
     * @param int $identrada
     */    
    private function preForm($identrada = 0) {
                $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
                
		$departamentossecretaria = new Departamentossecretarias();
		$this->view->departamentossecretaria = $departamentossecretaria->getDepartamentossecretarias(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		$escolas = new Escolas();
		$this->view->escolas = $escolas->getEscolas(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
                
                $produtos = new Almoxarifado_Produtos();
                $this->view->produtos = $produtos->getProdutos(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
                
        $fornecedores = new Fornecedores();
        $this->view->fornecedores = $fornecedores->getFornecedores(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		$rows = new Departamentosescolas();
    	$this->view->departamentosescolas = $rows->getDepartamentosescolas(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria)); 
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idendereco = strip_tags((int)$this->getRequest()->getPost("idendereco"));
		$titulo = strip_tags(trim($this->getRequest()->getPost("titulo")));	
		$descricoes = strip_tags(trim($this->getRequest()->getPost("descricoes")));	
		$status = strip_tags(trim($this->getRequest()->getPost("status1")));
		$aprovacao = strip_tags(trim($this->getRequest()->getPost("aprovacao")));
		$confirmaaprovacao = strip_tags(trim($this->getRequest()->getPost("confirmaaprovacao")));
		$baixalocal = strip_tags(trim($this->getRequest()->getPost("baixalocal")));
		
		$origem = strip_tags(trim($this->getRequest()->getPost("origem")));
		$iddepartamentosecretaria = strip_tags(trim($this->getRequest()->getPost("iddepartamentosecretaria")));
		$idescola = strip_tags(trim($this->getRequest()->getPost("idescola")));
		$iddepartamentoescola = strip_tags(trim($this->getRequest()->getPost("iddepartamentoescola")));
		
		$idssaidas = $this->getRequest()->getPost("idssaidas");
		$idsprodutos = $this->getRequest()->getPost("idsprodutos");
		$idsfornecedores = $this->getRequest()->getPost("idsfornecedores");
		$quantidades = $this->getRequest()->getPost("quantidades");	
		$itensobservacoes = $this->getRequest()->getPost("itensobservacoes");
		
		$erros = array();

		if (""==$titulo) array_push($erros, 'Preencha o campo ENTRADA.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$saidas = new Almoxarifado_Saidas();
		//$row = $saidas->fetchRow("excluido='nao' AND saida='$saida' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma ESCOLA com esse NOME.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {		
			if($id<=0) $confirmaaprovacao = "Não";
			
			$dados = array();
			$dados['id'] = $id;
			$dados['titulo'] = $titulo;
			$dados['descricoes'] = $descricoes;
			
			$dados['origem'] = $origem;
			$dados['iddepartamentosecretaria'] = $iddepartamentosecretaria;
			$dados['idescola'] = $idescola;
			$dados['iddepartamentoescola'] = $iddepartamentoescola;	
			
			$dados['aprovacao'] = $aprovacao;
			$dados['confirmaaprovacao'] = $confirmaaprovacao;
			$dados['baixalocal'] = $baixalocal;
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Usuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			$itenssaidas = array();
			$itenssaidas['idssaidas'] = $idssaidas;
			$itenssaidas['idsprodutos'] = $idsprodutos;
			$itenssaidas['idsfornecedores'] = $idsfornecedores;
			$itenssaidas['quantidades'] = $quantidades;
			$itenssaidas['itensobservacoes'] = $itensobservacoes;	
			$itenssaidas['status'] = $status;
			$dados['itens'] = $itenssaidas;
			
			$row = $saidas->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }
}