<?php

/**
 * Controle da classe logisticasmanutencoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_LogisticasmanutencoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Logisticasmanutencao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("logisticasmanutencoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
		
		$logisticamanutencoes = new Logisticasmanutencoes();
		$queries = array();	
		$ultimo = $logisticamanutencoes->getUltimoLogisticamanutencoes($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;

	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Logisticasmanutencoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Manutenção excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="logisticasmanutencoes") $objs = new Logisticasmanutencoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticasmanutencoes', 'name' => 'Manutenções'),
			array('url' => null,'name' => 'Visualizar Manutenção')
		);
		
		$id = (int)$this->_request->getParam("id");
		$logisticasmanutencoes = new Logisticasmanutencoes();
		$logisticasmanutencao = $logisticasmanutencoes->getLogisticasmanutencaoById($id, array());
		
		if (!$logisticasmanutencao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticasmanutencao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Manutenções')
		);
		
		$ns = new Zend_Session_Namespace('default_logisticasmanutencoes');
		$logisticasmanutencoes = new Logisticasmanutencoes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idfrota"]!="") $queries["idfrota"] = $this->view->post_var["idfrota"];
if ($this->view->post_var["idtipomanutencao"]!="") $queries["idtipomanutencao"] = $this->view->post_var["idtipomanutencao"];
if ($this->view->post_var["idfornecedor"]!="") $queries["idfornecedor"] = $this->view->post_var["idfornecedor"];
if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
if ($this->view->post_var["numeroprocesso"]!="") $queries["numeroprocesso"] = $this->view->post_var["numeroprocesso"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $logisticasmanutencoes->getLogisticasmanutencoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $logisticasmanutencoes->getLogisticasmanutencoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de logisticasmanutencoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticasmanutencoes', 'name' => 'Manutenções'),
			array('url' => null,'name' => 'Editar Manutenção')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$logisticasmanutencoes = new Logisticasmanutencoes();
		$logisticasmanutencao = $logisticasmanutencoes->getLogisticasmanutencaoById($id);
		
		if (!$logisticasmanutencao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticasmanutencao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($logisticasmanutencao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Manutenção editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de logisticasmanutencoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticasmanutencoes', 'name' => 'Manutenções'),
			array('url' => null,'name' => 'Adicionar Manutenção')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Manutenção adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idlogisticasmanutencao
     */    
    private function preForm($idlogisticasmanutencao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_logisticasmanutencao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idfrota = (int)trim($this->getRequest()->getPost("idfrota"));
$idtipomanutencao = (int)trim($this->getRequest()->getPost("idtipomanutencao"));
$idfornecedor = (int)trim($this->getRequest()->getPost("idfornecedor"));
$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
$sequencial = trim($this->getRequest()->getPost("sequencial"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$numeroprocesso = (int)trim($this->getRequest()->getPost("numeroprocesso"));
$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
$hora = trim($this->getRequest()->getPost("hora"));
$aprovacao = trim($this->getRequest()->getPost("aprovacao"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idfrota) array_push($erros, "Informe a Veículo.");
if (0==$idtipomanutencao) array_push($erros, "Informe a Tipo da Manutenção.");
if (0==$idfornecedor) array_push($erros, "Informe a Fornecedor.");
if (0==$idfuncionario) array_push($erros, "Informe a Responsável pela retirada.");
if (""==$sequencial) array_push($erros, "Informe a Sequencial.");
if (0==$numeroprocesso) array_push($erros, "Informe a Nº do Processo.");
if (""==$data) array_push($erros, "Informe a Data.");
if (""==$hora) array_push($erros, "Informe a Hora.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$logisticasmanutencoes = new Logisticasmanutencoes();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idfrota"] = $idfrota;
$dados["idtipomanutencao"] = $idtipomanutencao;
$dados["idfornecedor"] = $idfornecedor;
$dados["idfuncionario"] = $idfuncionario;
$dados["sequencial"] = $sequencial;
$dados["descricoes"] = $descricoes;
$dados["numeroprocesso"] = $numeroprocesso;
$dados["data"] = date("Y-m-d", $data);
$dados["hora"] = $hora;
$dados["aprovacao"] = $aprovacao;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $logisticasmanutencoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}