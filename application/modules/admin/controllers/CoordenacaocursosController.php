<?php

/**
 * Controle da classe cursos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_CoordenacaocursosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Curso
	 */
	protected $_usuario = null;	
	
	
	/** 
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("coordenacaocursos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Cursos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Curso excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="coordenacaocursos") $objs = new Cursos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaocursos', 'name' => 'Cursos'),
			array('url' => null,'name' => 'Visualizar Curso')
		);
		
		$id = (int)$this->_request->getParam("id");
		$cursos = new Cursos();
		$curso = $cursos->getCursoById($id, array());
		
		if (!$curso) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $curso;
		$this->preForm();
		
		$endereco = Enderecos::getEnderecoById($curso['idendereco']);
		if($endereco){
			unset($endereco['id']);
			//unset($endereco['idestado']);
			//unset($endereco['idcidade']);
			unset($endereco['excluido']);
			$this->view->post_var = array_merge($this->view->post_var, $endereco);
		}
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Cursos/Treinamentos')
		);
		
		$ns = new Zend_Session_Namespace('default_cursos');
		$cursos = new Cursos();
		$queries = array();	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['origem'] = "Coordenacao";
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ((isset($this->view->post_var["titulo"])) && ($this->view->post_var["titulo"]!="")) $queries["titulo"] = $this->view->post_var["titulo"];
if ((isset($this->view->post_var["descricoes"])) && ($this->view->post_var["descricoes"]!="")) $queries["descricoes"] = $this->view->post_var["descricoes"];
if ((isset($this->view->post_var["status1"])) && ($this->view->post_var["status1"]!="")) $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $cursos->getCursos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $cursos->getCursos($queries, $paginaAtual, $maxpp);	
	}
	
	public function setinteresseAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		die('OK');
		$idcurso = (int)$this->getRequest()->getPost("id");
		
		$cursosinteresses = new Coordenacao_Cursostreinamentosinteresses();
		$cursos = $cursosinteresses->getCursostreinamentosinteresses(array('idusuario'=>$this->_usuario['id'], 'idcurso'=>$idcurso));
		
		if (sizeof($cursos)>0) die('Já Cadastrado');
		
		$dados = array();
		$dados['idusuario'] = $this->_usuario['id'];
		$dados['idcurso'] = $idcurso;
		$dados['tipo'] = 'admin';
		$dados['data'] = date('Y-m-d G:i:s');
		$dados['status'] = 'Ativo';
		$dados['excluido'] = 'nao';
		$dados['logdata'] = date('Y-m-d G:i:s');
		$dados['logusuario'] = $this->_usuario['id'];
		$cursosinteresses->save($dados);
		
		
		die('OK');
	}
	
	/**
	 * 
	 * Action de edição de cursos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaocursos', 'name' => 'Cursos/Treinamentos'),
			array('url' => null,'name' => 'Editar Curso')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$cursos = new Cursos();
		$curso = $cursos->getCursoById($id);
		
		if (!$curso) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

		$this->view->post_var = $curso;
		$this->preForm();
		
		$endereco = Enderecos::getEnderecoById($curso['idendereco']);
		if($endereco){
			unset($endereco['id']);
			//unset($endereco['idestado']);
			//unset($endereco['idcidade']);
			unset($endereco['excluido']);
			$this->view->post_var = array_merge($this->view->post_var, $endereco);
		}
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($curso);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Curso editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de cursos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'coordenacaocursos', 'name' => 'Cursos/Treinamentos'),
			array('url' => null,'name' => 'Adicionar Curso')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Curso adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    

/**
	 * Action para modificar o status via Ajax
	 */
	public function setstatusAction() {
		$this->_helper->layout->disableLayout();
		
		$id = (int)$this->getRequest()->getPost("id");;
		
		$objs = new Cursos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}

    /**
     * Atribui valores ao view
     * @param int $idcurso
     */    
    private function preForm($idcurso = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_curso = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idendereco = (int)trim($this->getRequest()->getPost("idendereco"));
		$idsecretaria = (int)Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$titulo = trim($this->getRequest()->getPost("titulo"));
		$descricoes = trim($this->getRequest()->getPost("descricoes"));
		$cargahoraria = trim($this->getRequest()->getPost("cargahoraria"));
		$pontuacao = trim(Mn_Util::trataNum($this->getRequest()->getPost("pontuacao")));
		$instituicaoensino = trim($this->getRequest()->getPost("instituicaoensino"));
		$periodo = trim($this->getRequest()->getPost("periodo"));
		$instrutor = trim($this->getRequest()->getPost("instrutor"));
		$quantidadevagas = trim(Mn_Util::trataNum($this->getRequest()->getPost("quantidadevagas")));
		$status = trim($this->getRequest()->getPost("status1"));
		$origem = trim($this->getRequest()->getPost("origem"));
		
		
		$erros = array();
		
		if (""==$titulo) array_push($erros, "Informe a Curso.");
		if (""==$cargahoraria) array_push($erros, "Informe a Carga horária.");
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$cursos = new Cursos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$idendereco = 0;
			/*
			$idendereco = ($_registro) ? (int)$_registro['idendereco'] : 0;
			$enderecosModel = new EnderecoModel($this);
			$endereco = $enderecosModel->getPost($idendereco);
			$idendereco = ($endereco) ? $endereco['id'] : 0;
			*/

			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
			$dados["titulo"] = $titulo;
			$dados["descricoes"] = $descricoes;
			$dados["cargahoraria"] = $cargahoraria;
			$dados["pontuacao"] = $pontuacao;
			$dados["instituicaoensino"] = $instituicaoensino;
			$dados["periodo"] = $periodo;
			$dados["instrutor"] = $instrutor;
			$dados["quantidadevagas"] = $quantidadevagas;
			$dados["status"] = $status;
			$dados["origem"] = "Coordenacao";

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $cursos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}