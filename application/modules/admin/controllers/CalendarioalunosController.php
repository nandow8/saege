<?php

/**
 * Controle da classe coordenacaocalendarios do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_CalendarioalunosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Coordenacaocalendario
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("calendarioalunos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Coordenacaocalendarios();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Calendário escolar excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="calendarioalunos") $objs = new Coordenacaocalendarios();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'calendarioalunos', 'name' => 'Calendários escolares'),
			array('url' => null,'name' => 'Visualizar Calendário escolar')
		);
		
		$id = (int)$this->_request->getParam("id");
		$coordenacaocalendarios = new Coordenacaocalendarios();
		$coordenacaocalendario = $coordenacaocalendarios->getCoordenacaocalendarioById($id, array());
		
		if (!$coordenacaocalendario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaocalendario;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Calendários escolares')
		);
		
		$ns = new Zend_Session_Namespace('default_calendarioalunos');
		$coordenacaocalendarios = new Coordenacaocalendarios();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idsecretaria"]!="") $queries["idsecretaria"] = $this->view->post_var["idsecretaria"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $coordenacaocalendarios->getCoordenacaocalendarios($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $coordenacaocalendarios->getCoordenacaocalendarios($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de coordenacaocalendarios
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'calendarioalunos', 'name' => 'Calendários escolares'),
			array('url' => null,'name' => 'Editar Calendário escolar')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$coordenacaocalendarios = new Coordenacaocalendarios();
		$coordenacaocalendario = $coordenacaocalendarios->getCoordenacaocalendarioById($id);
		
		if (!$coordenacaocalendario) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $coordenacaocalendario;
		$this->preForm();
		
		$queriestiposdias = array();
		$queriestiposdias['idcalendario'] = $id;
		$queriestiposdias['order'] = " GROUP BY c1.data DESC";
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();
		$this->view->eventosdias = $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queriestiposdias);
		$this->view->visualizar = false;
		if ($this->_request->isPost()) {
			$erros = $this->getPost($coordenacaocalendario);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Calendário escolar editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de coordenacaocalendarios 
	 */
	public function settipoAction() {
		$this->_helper->layout->disableLayout();	

		$idtipo = (int)$this->_request->getPost("idtipo");
		$coordenacaocalendariostipos = new Coordenacaocalendariostipos();
		$coordenacaocalendariotipo = $coordenacaocalendariostipos->getCoordenacaocalendariotipoById($idtipo, array());
		
		if (!$coordenacaocalendariotipo) die('Não encontrado!');

		$this->view->post_var = $coordenacaocalendariotipo;
    }

	/**
	 * 
	 * Action de adição de coordenacaocalendarios 
	 */
	public function setdiaAction() {
		$this->_helper->layout->disableLayout();	

		$idcalendario = (int)$this->_request->getPost("id");
		$idtiposelecionado = (int)$this->_request->getPost("idtiposelecionado");
		$mes = (int)$this->_request->getPost("mes");
		$dia = (int)$this->_request->getPost("dia");

		$coordenacaocalendariostipos = new Coordenacaocalendariostipos();
		$coordenacaocalendariotipo = $coordenacaocalendariostipos->getCoordenacaocalendariotipoById($idtiposelecionado, array());
		
		if (!$coordenacaocalendariotipo) die('Não encontrado!');

		$this->view->post_var = $coordenacaocalendariotipo;

		if($mes<10) '0'.$mes;
		if($dia<10) '0'.$dia;

		$data = date('Y') . '-' . $mes . '-' . $dia;
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();

		$queries['data_i'] = date("Y-m-d", strtotime($data));
		$queries['data_f'] = date("Y-m-d", strtotime($data));
		$queries['idcalendario'] = $idcalendario;
		$queries['idtipo'] = $idtiposelecionado;
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();
		$coordenacaocalendariodia = $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queries);

		if(sizeof($coordenacaocalendariodia) <= 0){
			$dados = array();		
			$dados["idcalendario"] = $idcalendario;
			$dados["idtipo"] = $idtiposelecionado;
			$dados["data"] = date("Y-m-d", strtotime($data));
			$dados["status"] = 'Ativo';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			$row = $coordenacaocalendariosdias->save($dados);
		}else{
			$_row = Coordenacaocalendariosdias::getCoordenacaocalendariodiaByIdTipoDataHelper($queries);
			if(isset($_row['id'])){
				$_row['excluido'] = "sim";
				$_row['logusuario'] = $this->_usuario['id'];;
				$_row['logdata'] = date('Y-m-d G:i:s');
				$coordenacaocalendariosdias->save($_row);
			}
		}


		$queriestiposdias = array();
		$queriestiposdias['data_i'] = date("Y-m-d", strtotime($data));
		$queriestiposdias['data_f'] = date("Y-m-d", strtotime($data));
		$queriestiposdias['idcalendario'] = $idcalendario;
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();
		$coordenacaocalendariodia = $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queriestiposdias);
		$this->view->rows = $coordenacaocalendariodia;

		$queriestiposdias['total'] = true;
		$this->view->total = $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queriestiposdias);
    }

    /**
     * Atribui valores ao view
     * @param int $idcoordenacaocalendario
     */    
    private function preForm($idcoordenacaocalendario = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_coordenacaocalendario = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)trim($this->getRequest()->getPost("idsecretaria"));
$ano = trim($this->getRequest()->getPost("ano"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idsecretaria) array_push($erros, "Informe a Secretaria.");
if (""==$ano) array_push($erros, "Informe a Ano.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$coordenacaocalendarios = new Coordenacaocalendarios();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = $idsecretaria;
$dados["ano"] = $ano;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $coordenacaocalendarios->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}