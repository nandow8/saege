<?php

/**
 * Controle da classe locacoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_LocacoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Locacao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("locacoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Locacoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Locação excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="locacoes") $objs = new Locacoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'locacoes', 'name' => 'Locações'),
			array('url' => null,'name' => 'Visualizar Locação')
		);
		
		$id = (int)$this->_request->getParam("id");
		$locacoes = new Locacoes();
		$locacao = $locacoes->getLocacaoById($id, array());
		
		if (!$locacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $locacao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Locações')
		);
		
		$ns = new Zend_Session_Namespace('default_locacoes');
		$locacoes = new Locacoes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idunidade"]!="") $queries["idunidade"] = $this->view->post_var["idunidade"];
if ($this->view->post_var["datainicio_i"]!="") $queries["datainicio_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_i"]));
if ($this->view->post_var["datainicio_f"]!="") $queries["datainicio_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_f"]));
if ($this->view->post_var["datafim_i"]!="") $queries["datafim_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datafim_i"]));
if ($this->view->post_var["datafim_f"]!="") $queries["datafim_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datafim_f"]));
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $locacoes->getLocacoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $locacoes->getLocacoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de locacoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'locacoes', 'name' => 'Locações'),
			array('url' => null,'name' => 'Editar Locação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$locacoes = new Locacoes();
		$locacao = $locacoes->getLocacaoById($id);
		
		if (!$locacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $locacao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($locacao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Locação editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de locacoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'locacoes', 'name' => 'Locações'),
			array('url' => null,'name' => 'Adicionar Locação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Locação adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    //METODO CRON PARA 
    public function setflagsAction()
    {
        $locacoes = new Locacoes();
        
        //busca locacoes a vencer
        $avencer = $locacoes->fetchAll('DATEDIFF(DATE(datafim),DATE(NOW())) <= 30 AND excluido = "nao"');
        //busca locacoes vencidas
        $vencidas = $locacoes->fetchAll('DATEDIFF(DATE(NOW()), DATE(datafim)) >= 1 AND excluido = "nao"');
        
        //atualiza o status das locacoes a vencer
        foreach($avencer as $locacao)
        {
            echo $locacao['id'];
            $queries['id'] = $locacao['id'];
            $queries['flag'] = Locacoes::$_FLAG_ATENCAO;
            
            $row = $locacoes->save($queries);
        }        
            
        //atualiza o status das locacoes vencidas
        foreach($vencidas as $locacao)
        {
            echo $locacao['id'];
            $queries['id'] = $locacao['id'];
            $queries['flag'] = Locacoes::$_FLAG_VENCIDO;
            
            $row = $locacoes->save($queries);
        }
        
        die();
    }

    /**
     * Atribui valores ao view
     * @param int $idlocacao
     */    
    private function preForm($idlocacao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_locacao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idunidade = (int)trim($this->getRequest()->getPost("idunidade"));
		$datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
		$datafim = Mn_Util::stringToTime($this->getRequest()->getPost("datafim"));
		$datasolicitacao = Mn_Util::stringToTime($this->getRequest()->getPost("datasolicitacao"));
		$numeroaditivo = trim($this->getRequest()->getPost("numeroaditivo"));
		$valormensal = MN_Util::trataNum(trim($this->getRequest()->getPost("valormensal")));
		$total = MN_Util::trataNum(trim($this->getRequest()->getPost("total")));
		$siano = trim($this->getRequest()->getPost("siano"));
		$procano = trim($this->getRequest()->getPost("procano"));
		$recursos = trim($this->getRequest()->getPost("recursos"));
		$informacoesadicionais = trim($this->getRequest()->getPost("informacoesadicionais"));
		$flag = trim($this->getRequest()->getPost("flag"));
		$status = trim($this->getRequest()->getPost("status1"));		
		
		$erros = array();
		
		if (0==$idunidade) array_push($erros, "Informe a Unidade.");
		if (""==$datainicio) array_push($erros, "Informe a Data Início.");
		if (""==$datafim) array_push($erros, "Informe a Data Fim.");
		if (""==$datasolicitacao) array_push($erros, "Informe a Data da solicitação.");
		if (""==$recursos) array_push($erros, "Informe a Recursos.");
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$locacoes = new Locacoes();

		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idunidade"] = $idunidade;
			$dados["datainicio"] = date("Y-m-d", $datainicio);
			$dados["datafim"] = date("Y-m-d", $datafim);
			$dados["datasolicitacao"] = date("Y-m-d", $datasolicitacao);
			$dados["numeroaditivo"] = $numeroaditivo;
			$dados["valormensal"] = $valormensal;
			$dados["total"] = $total;
			$dados["siano"] = $siano;
			$dados["procano"] = $procano;
			$dados["recursos"] = $recursos;
			$dados["informacoesadicionais"] = $informacoesadicionais;
			
			$idimagemimovel = $this->getImagem('idimagemimovel');
			if ($idimagemimovel!=0) $dados['idimagemimovel'] = $idimagemimovel;

			$idimagemlaudos = $this->getImagem('idimagemlaudos');
			if ($idimagemlaudos!=0) $dados['idimagemlaudos'] = $idimagemlaudos;

			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $locacoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
}