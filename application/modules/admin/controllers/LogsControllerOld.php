<?php

/**
 * Controle da classe logs do sistema
 *
 * @author	Julio Cesar Santos
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2018 FJMX Sistemas. (http://www.fjmx.com.br)
 * @version     1.0
 */
class Admin_LogsController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Usuario
     */
    protected $_usuario = null;
    protected $_isAdmin = false;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("usuarios", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }

    }


    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Logs')
        );

        $ns = new Zend_Session_Namespace('admin_logs');
        $logs = new Logs();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();
        }
        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);
            
            if ((isset($this->view->post_var['data_i'])) && ($this->view->post_var['data_i'] != ''))
                $queries['data_i'] = $this->view->post_var['data_i'];
            
            if ((isset($this->view->post_var['data_f'])) && ($this->view->post_var['data_f'] != ''))
                $queries['data_f'] = $this->view->post_var['data_f'];
            
            if ((isset($this->view->post_var['lgnomeusuario'])) && ($this->view->post_var['lgnomeusuario'] != ''))
                $queries['lgnomeusuario'] = $this->view->post_var['lgnomeusuario'];
            
            if ((isset($this->view->post_var['lgmensagem'])) && ($this->view->post_var['lgmensagem'] != ''))
                $queries['lgmensagem'] = $this->view->post_var['lgmensagem'];
            
            if ((isset($this->view->post_var['lgtpmensagem'])) && ($this->view->post_var['lgtpmensagem'] != ''))
                $queries['lgtpmensagem'] = $this->view->post_var['lgtpmensagem'];
            
            if ((isset($this->view->post_var['lgipusuario'])) && ($this->view->post_var['lgipusuario'] != ''))
                $queries['lgipusuario'] = $this->view->post_var['lgipusuario'];
            
            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];

        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;

        $totalRegistros = $logs->getLogs($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;
        $queries['die'] = true;
        $this->view->rows = $logs->getLogs($queries, $paginaAtual, $maxpp);
    }

    /**
     *
     * Action de visualização
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'log', 'name' => 'Log'),
            array('url' => null, 'name' => 'Visualizar Log')
        );

        $lgid = (int) $this->_request->getParam("lgid");
        $logs = new Logs();
        $log = $logs->fetchRow("lgid=" . $lgid);

        if (!$log)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $log;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idusuario
     */
    private function preForm($idusuario = 0) {
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost() {
        return "";
    }

}
