<?php

/**
 * Controle da classe Nutricaomerendasdiferenciadascancelamentos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_NutricaomerendasdiferenciadascancelamentosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Nutricaomerendasdiferenciadascancelamentos
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("nutricaomerendasdiferenciadascancelamentos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
		$nutricaomerendasdiferenciadascancelamentos = new Nutricaomerendasdiferenciadascancelamentos();
		$queries = array();	
		$ultimo = $nutricaomerendasdiferenciadascancelamentos->getNutricaomerendasdiferenciadascancelamentosByDestino($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial; 
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Nutricaomerendasdiferenciadascancelamentos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Merenda diferenciada excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="nutricaomerendasdiferenciadascancelamentos") $objs = new Nutricaomerendasdiferenciadascancelamentos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendasdiferenciadascancelamentos', 'name' => 'Merendas diferenciadas'),
			array('url' => null,'name' => 'Visualizar Merenda diferenciada')
		);
		
		$id = (int)$this->_request->getParam("id");
		$nutricaomerendasdiferenciadascancelamentos = new Nutricaomerendasdiferenciadascancelamentos();
		$nutricaomerendasdiferenciadascancelamento = $nutricaomerendasdiferenciadascancelamentos->getNutricaomerendasdiferenciadascancelamentosById($id, array());
		
		if (!$nutricaomerendasdiferenciadascancelamento) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaomerendasdiferenciadascancelamento;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Merendas diferenciadas')
		);
		
		$ns = new Zend_Session_Namespace('default_nutricaomerendasdiferenciadascancelamentos');
		$nutricaomerendasdiferenciadascancelamentos = new Nutricaomerendasdiferenciadascancelamentos();

                $queries = array();	
                $queries['idescola'] = Usuarios::getUsuario('idescola');
		
		//PESQUISA
                if ($this->getRequest()->isPost()) {
                        $ns->pesquisa = serialize($_POST);
                        $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
                        die();	
                }

                if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
                if (isset($this->view->post_var)) {
                        foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);

                                if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
                                if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
                                if ($this->view->post_var["idusuario"]!="") $queries["idusuario"] = $this->view->post_var["idusuario"];
                                if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
                                if ($this->view->post_var["posicao"]!="") $queries["posicao"] = $this->view->post_var["posicao"];

                                if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
                }		
		
		//PAGINACAO
                $maxpp = 20;
		
                $paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $nutricaomerendasdiferenciadascancelamentos->getNutricaomerendasdiferenciadascancelamentos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $nutricaomerendasdiferenciadascancelamentos->getNutricaomerendasdiferenciadascancelamentos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de nutricaomerendasdiferenciadas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendasdiferenciadas', 'name' => 'Merendas diferenciadas'),
			array('url' => null,'name' => 'Editar Merenda diferenciada')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$nutricaomerendasdiferenciadascancelamentos = new Nutricaomerendasdiferenciadascancelamentos();
		$nutricaomerendasdiferenciadascancelamento = $nutricaomerendasdiferenciadascancelamentos->getNutricaomerendasdiferenciadascancelamentosById($id);
		
		if (!$nutricaomerendasdiferenciada) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaomerendasdiferenciadascancelamento;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($nutricaomerendasdiferenciadascancelamento);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Merenda diferenciada editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de nutricaomerendasdiferenciadas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendasdiferenciadascancelamentos', 'name' => 'Merendas diferenciadas'),
			array('url' => null,'name' => 'Adicionar Merenda diferenciada')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Merenda diferenciada adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function setescolasalunosAction() {
		$this->_helper->layout->disableLayout();
		$idescola = (int)$this->_request->getPost("idescola");
                
		$escolas = new Nutricaomerendasdiferenciadas();
                $this->view->rows = $escolas->getNutricaomerendasdiferenciadas(array('posicao'=>'Autorizado','idescola'=>$idescola));
	}
    
	public function getdadosAction() {
		$this->_helper->layout->disableLayout();
		
		$idaluno = (int)$this->_request->getPost("idaluno");
		$aluno = Escolasalunos::getEscolaalunoByIdHelper($idaluno);
		if(!isset($aluno['id'])) die('erro');

		$this->view->post_var = $aluno;

		$vinculo = array();
		$serieturma = Escolassalasatribuicoes::getSalaatribuicaoByIdAlunoHelper($aluno['id']);
		if(isset($serieturma['idvinculo'])){
			$vinculo = Escolasvinculos::getEscolavinculoByIdHelper($serieturma['idvinculo']);
		}
		$this->view->vinculo = $vinculo;
		
	}

    /**
     * Atribui valores ao view
     * @param int $idnutricaomerendasdiferenciada
     */    
    private function preForm($idnutricaomerendasdiferenciada = 0) {
    	$escolas = new Nutricaomerendasdiferenciadas();
        $idescola = (int)$this->_request->getPost("idescola");
        
        $this->view->escolas = $escolas->getNutricaomerendasdiferenciadas(array('posicao'=>'Autorizado','idescola'=>$idescola,'order'=>'GROUP BY n1.idescola'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_nutricaomerendasdiferenciada = false) {
            if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
            else $this->view->post_var = array_merge($this->view->post_var, $_POST);

            $id = (int)$this->getRequest()->getPost("id");
            $datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
            $idusuario = trim($this->getRequest()->getPost("idusuario"));
            $idescola = trim($this->getRequest()->getPost("idescola"));
            $idlocal = trim($this->getRequest()->getPost("idlocal"));
            $idaluno = trim($this->getRequest()->getPost("idaluno")); 
            $aluno = trim($this->getRequest()->getPost("aluno"));
            $medico = trim($this->getRequest()->getPost("medico"));
            $motivo = trim($this->getRequest()->getPost("motivo")); 
            $crmcrn = trim($this->getRequest()->getPost("crmcrn"));
            $observacoes = trim($this->getRequest()->getPost("observacoes"));
            $datalaudo = Mn_Util::stringToTime($this->getRequest()->getPost("datalaudo"));
            $laudo = (int)trim($this->getRequest()->getPost("laudo"));
            $prescricao = trim($this->getRequest()->getPost("prescricao"));
            $justificativa = trim($this->getRequest()->getPost("justificativa"));
            $posicao = trim($this->getRequest()->getPost("posicao"));
		
            /*MULTIPLOS ARQUIVOS*/
            $idsarquivos = $this->getRequest()->getPost("idsarquivos");
            $legendasarquivos =  $this->getRequest()->getPost("legendasarquivos");

            $erros = array();

            if (""==$datalancamento) array_push($erros, "Informe a Data do Lançamento.");
            if (""==$idusuario) array_push($erros, "Informe a Nome do Usuário.");
            if (""==$idescola) array_push($erros, "Informe a Escola.");
            if (""==$posicao) array_push($erros, "Informe a Posição.");
            if (""==$motivo) array_push($erros, "Informe o motivo para o cancelamento.");
		
            $nutricaomerendasdiferenciadascancelamentos = new Nutricaomerendasdiferenciadascancelamentos();
		
		
		
            if (sizeof($erros)>0) return $erros; 

            $db = Zend_Registry::get('db');
            $db->beginTransaction();
            try {
                    $dados = array();
                    $dados['id'] = $id;

                    $dados["datalancamento"] = date("Y-m-d", $datalancamento);
                    $dados["idusuario"] = $idusuario;
                    $dados["idescola"] = $idescola;
                    $dados["idlocal"] = $idlocal;
                    $dados["idaluno"] = $idaluno;
                    $dados["aluno"] = $aluno;
                    $dados["medico"] = $medico;
                    $dados["motivo"] = $motivo; 
                    $dados["crmcrn"] = $crmcrn;
                    $dados["observacoes"] = $observacoes;
                    $dados["datalaudo"] = date("Y-m-d", $datalaudo);

                    $laudo = $this->getArquivo('laudo');
                    if ($laudo!=0) $dados['laudo'] = $laudo;
                    $dados["prescricao"] = $prescricao;
                    $dados["justificativa"] = $justificativa;
                    $dados["posicao"] = $posicao;

			
                    $dados['excluido'] = 'nao';
                    $dados['logusuario'] = $this->_usuario['id'];;
                    $dados['logdata'] = date('Y-m-d G:i:s');
					
                    $row = $nutricaomerendasdiferenciadascancelamentos->save($dados);
			
                    if($dados["posicao"] == 'Cancelado'){ 
                            $rows = new Nutricaomerendasdiferenciadas();
                            $row = $rows->fetchRow("id=" . $idaluno);

                            if ($row) {
                                    $row = $row->toArray();
                                    $row['posicao'] = 'Cancelado'; 

                                    $rows->save($row);  
                            }	         
		    }

                    $db->commit();
            } catch (Exception $e) {
                    echo $e->getMessage();

                    $db->rollBack();
                    die();
            }		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}
