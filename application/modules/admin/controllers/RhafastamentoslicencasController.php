<?php

/**
 * Controle da classe rhafastamentoslicencas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_RhafastamentoslicencasController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Rhafastamentoslicenca
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("rhafastamentoslicencas", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Rhafastamentoslicencas();
        $row = $rows->fetchRow("id=" . $id);


        if ($row) {
            $row = $row->toArray();
            if ($row['progresso'] != 'andamento') {

                $message = new Zend_Session_Namespace("message");
                $message->crudmessage = "O item selecionado não pode ser excluído. Pois não está mais em andamento.";

                die("OK");
            }

            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Afastamento e Licença excluído com sucesso.";

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "rhafastamentoslicencas")
            $objs = new Rhafastamentoslicencas();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'rhafastamentoslicencas', 'name' => 'Afastamentos e Licenças'),
            array('url' => null, 'name' => 'Visualizar Afastamento e Licença')
        );

        $funcionariosgerais = new Funcionariosgeraisescolas();
        $_funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById((int) Usuarios::getUsuario('idfuncionario'));

        $this->view->funcionariogeral = $_funcionariogeral;

        $id = (int) $this->_request->getParam("id");
        $rhafastamentoslicencas = new Rhafastamentoslicencas();
        $rhafastamentoslicenca = $rhafastamentoslicencas->getRhafastamentoslicencaById($id, array());

        if (!$rhafastamentoslicenca)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $rhafastamentoslicenca;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $ns = new Zend_Session_Namespace('default_rhafastamentoslicencas');
        $rhafastamentoslicencas = new Rhafastamentoslicencas();
        $this->preForm();

        $funcionariosgerais = new Funcionariosgeraisescolas();
        $_funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById((int) Usuarios::getUsuario('idfuncionario'));
        
        $this->view->funcionariogeral = $_funcionariogeral;

        $perfis = new UsuariosPerfis();
        $perfil = $perfis->getPerfilById($this->_usuario['idperfil']);
        $isrh = ((isset($perfil['rh'])) && ($perfil['rh'] == 'sim')) ? 'sim' : '';
        $this->view->isrh = $isrh;

        $queries = array();
        $queries['origem'] = "Secretaria";
        $queries['idescola'] = Usuarios::getUsuario('idescola');
        if (!$isrh) {
            $queries['idlocal'] = Usuarios::getUsuario('idlocal');
            $queries['idusuariologado'] = Usuarios::getUsuario('idfuncionario');
        }
        
        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();
        }
        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ($this->view->post_var["idfuncionariosecretaria"] != "")
                $queries["idfuncionarioafastadolicenca"] = $this->view->post_var["idfuncionariosecretaria"];
            if ($this->view->post_var["datainicio_i"] != "")
                $queries["datainicio_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_i"]));
            if ($this->view->post_var["datainicio_f"] != "")
                $queries["datainicio_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_f"]));
            if ($this->view->post_var["progresso"] != "")
                $queries["progresso"] = $this->view->post_var["progresso"];

            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = false;
        $totalRegistros = count($rhafastamentoslicencas->getRhafastamentoslicencas($queries,0,0));
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $rhafastamentoslicencas->getRhafastamentoslicencas($queries, $paginaAtual, $maxpp);
    }

    /**
     * 
     * Action de edição de rhafastamentoslicencas
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'rhafastamentoslicencas', 'name' => 'Afastamentos e Licenças'),
            array('url' => null, 'name' => 'Editar Afastamento e Licença')
        );

        $funcionariosgerais = new Funcionariosgeraisescolas();
        $_funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById((int) Usuarios::getUsuario('idfuncionario'));

        $this->view->funcionariogeral = $_funcionariogeral;

        $id = (int) $this->_request->getParam("id");
        $rhafastamentoslicencas = new Rhafastamentoslicencas();
        $rhafastamentoslicenca = $rhafastamentoslicencas->getRhafastamentoslicencaById($id);

        $perfis = new UsuariosPerfis();
        $perfil = $perfis->getPerfilById($this->_usuario['idperfil']);
        $isrh = $perfil['rh'] == 'sim';
        $this->view->isrh = $isrh;

        if (!$rhafastamentoslicenca)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $rhafastamentoslicenca;
        $this->preForm();
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($rhafastamentoslicenca);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Afastamento e Licença editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * 
     * Action de adição de rhafastamentoslicencas 
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'rhafastamentoslicencas', 'name' => 'Afastamentos e Licenças'),
            array('url' => null, 'name' => 'Adicionar Afastamento e Licença')
        );

        $funcionariosgerais = new Funcionariosgeraisescolas();
        $_funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById((int) Usuarios::getUsuario('idfuncionario'));

        $this->view->funcionariogeral = $_funcionariogeral;

        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Afastamento e Licença adicionado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * Atribui valores ao view
     * @param int $idrhafastamentoslicenca
     */
    private function preForm($idrhafastamentoslicenca = 0) {
        $escolas = new Escolas();
        $this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_rhafastamentoslicenca = false) {
        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $idsecretaria = (int) trim($this->getRequest()->getPost("idsecretaria"));
        $origem = "Secretaria";
        $idfuncionariosecretaria = (int) trim($this->getRequest()->getPost("idfuncionariosecretaria"));
        $idescola = (int) trim($this->getRequest()->getPost("idescola"));
        $idlocal = (int) trim($this->getRequest()->getPost("idlocal"));
        $idfuncionarioescola = (int) trim($this->getRequest()->getPost("idfuncionarioescola"));
        $dias = trim($this->getRequest()->getPost("dias"));
        $motivo = trim($this->getRequest()->getPost("motivo"));
        $datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
        $datafim = Mn_Util::stringToTime($this->getRequest()->getPost("datafim"));
        $descricoes = trim($this->getRequest()->getPost("descricoes"));
        $aprovacao = trim($this->getRequest()->getPost("aprovacao"));
        $confirmacaoaprovacao = trim($this->getRequest()->getPost("confirmacaoaprovacao"));
        $status = trim($this->getRequest()->getPost("status1"));
        $idfuncionarioafastadolicenca = (int) trim($this->getRequest()->getPost("idfuncionarioafastadolicenca"));
        $progresso = trim($this->getRequest()->getPost("progresso"));
        $observacao = trim($this->getRequest()->getPost("observacao"));
        $idarquivo = $this->getArquivo('idarquivo');

        $logusuario =  trim($this->getRequest()->getPost("logusuario"));
        $erros = array();
        if (0 == $idfuncionarioafastadolicenca)
            array_push($erros, "Informe o Funcionário.");
        if ("" == $datainicio)
            array_push($erros, "Informe a Data Inicial.");
        if ("" == $datafim)
            array_push($erros, "Informe a Data Final.");
        if ("" == $motivo)
            array_push($erros, "Informe o Motivo.");
        if ("" == $progresso)
            array_push($erros, "Informe a Aprovação.");
        if (0 == $idarquivo && $id == 0)
            array_push($erros, "Informe o Arquivo.");
        if ($dias < 7)
            array_push($erros, "Informe no mínimo 7 dias de afastamento.");

        $rhafastamentoslicencas = new Rhafastamentoslicencas();



        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;

            $dados["idsecretaria"] = $idsecretaria;
            
            if ($idarquivo != 0)
                $dados['idarquivo'] = $idarquivo;

            $idarquivoaprovacao = $this->getArquivo('idarquivoaprovacao');
            if ($idarquivoaprovacao != 0)
                $dados['idarquivoaprovacao'] = $idarquivoaprovacao;

            $dados["origem"] = $origem;
            $dados["idfuncionariosecretaria"] = $idfuncionariosecretaria;
            $dados["idescola"] = $idescola;
            $dados["idlocal"] = $idlocal;
            $dados["idfuncionarioescola"] = $idfuncionarioescola;
            $dados["dias"] = $dias;
            $dados["motivo"] = $motivo;
            $dados["datainicio"] = date("Y-m-d", $datainicio);
            $dados["datafim"] = date("Y-m-d", $datafim);
            $dados["descricoes"] = $descricoes;
            $dados["aprovacao"] = $aprovacao;
            $dados["confirmacaoaprovacao"] = $confirmacaoaprovacao;
            $dados["status"] = $status;
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            $dados['logdata'] = date('Y-m-d G:i:s');
            $dados["idfuncionarioafastadolicenca"] = $idfuncionarioafastadolicenca;
            $dados["progresso"] = $progresso;
            $dados["observacao"] = $observacao;

          

            $row = $rhafastamentoslicencas->save($dados);

             // Inserindo lançamentos na base de dados das folhas de ponto
             if($dados['progresso'] == 'aprovado'){
                //  Apenas no caso de uma solicitação aprovada pelo departamento de RH
                $data_inicio = new DateTime($dados['datainicio']);
                $data_fim = new DateTime($dados['datafim']);

                $interval = new DateInterval('P1D');

                $periodo = new DatePeriod($data_inicio, $interval, $data_fim);

                foreach($periodo as $index){

                    $cadastraLancamento = new Funcionariosgeraispontohora();

                    // Alocando dados no array para o lancamento se concluido
                    $dadosLancamento = array();
                    //$dados['id'] = (int)"";
                    $dadosLancamento['titulo_pontohorarios'] = 'Afastamento';
                    $dadosLancamento['idfuncionario'] = $logusuario;
                    $dadosLancamento['idrhfuncionario'] =  $dados['idfuncionarioafastadolicenca'];
                    // Usuarios::getUsuario('idfuncionario') 
                    $dadosLancamento['idlocalfuncionario'] = $dados['idlocal'];
                    $dadosLancamento['iddeptfuncionario'] = Usuarios::getUsuarioByIdHelper($logusuario)['idperfil'];
                    $dadosLancamento['data_pontohorarios'] = $index->format('Y-m-d');
                    $dadosLancamento['entrada_pontohorarios'] = $index->format('Y-m-d 00:00:00');
                    $dadosLancamento['saida_pontohorarios'] =  $index->format('Y-m-d 00:00:00');
                    $dadosLancamento['entradapausa_pontohorarios'] = $index->format('Y-m-d 00:00:00');
                    $dadosLancamento['saidapausa_pontohorarios'] = $index->format('Y-m-d 00:00:00');

                    $dadosLancamento['fkhorariorotulo'] = 'Afastado';
                    $dadosLancamento['fkhorarioid'] = '0';

                    $dadosLancamento['status_pontohorarios'] = null;
                    $dadosLancamento['logusuario_pontohorarios'] = $this->_usuario['id'];
                    $dadosLancamento['logdata_pontohorarios'] = date('Y-m-d G:i:s');

                    if(($index->format("w") == "6") || ($index->format("w") == "0")){
                       
                    }else{
                        if(Feriados::getVerificaFeriado($index->format("Y-m-d")) > 0 && (Feriados::getInformacoesFeriadoData($index->format("Y-m-d"))['status'] == 'Ativo') && (Feriados::getInformacoesFeriadoData($index->format("Y-m-d"))['excluido'] == 'nao')):
                        
                        else: 

                            $cadastraLancamento->save($dadosLancamento);
                            
                        endif;
                            
                    }
                };//end foreach

            }//end if

           
            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();

            $db->rollBack();
            die();
        }

        return "";
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

}
