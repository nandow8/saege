<?php

/**
 * Controle da classe importacoesfuncionariospontos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ImportacoesfuncionariospontosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Importacoesfuncionariosponto
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("importacoesfuncionariospontos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Importacoesfuncionariospontos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Arquivo excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="importacoesfuncionariospontos") $objs = new Importacoesfuncionariospontos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'importacoesfuncionariospontos', 'name' => 'Importações de Arquivos'),
			array('url' => null,'name' => 'Visualizar Arquivo')
		);
		
		$id = (int)$this->_request->getParam("id");
		$importacoesfuncionariospontos = new Importacoesfuncionariospontos();
		$importacoesfuncionariosponto = $importacoesfuncionariospontos->getImportacoesfuncionariospontoById($id, array());
		
		if (!$importacoesfuncionariosponto) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $importacoesfuncionariosponto;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Importações de Arquivos')
		);
		
		$ns = new Zend_Session_Namespace('default_importacoesfuncionariospontos');
		$importacoesfuncionariospontos = new Importacoesfuncionariospontos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $importacoesfuncionariospontos->getImportacoesfuncionariospontos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $importacoesfuncionariospontos->getImportacoesfuncionariospontos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de importacoesfuncionariospontos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'importacoesfuncionariospontos', 'name' => 'Importações de Arquivos'),
			array('url' => null,'name' => 'Editar Arquivo')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$importacoesfuncionariospontos = new Importacoesfuncionariospontos();
		$importacoesfuncionariosponto = $importacoesfuncionariospontos->getImportacoesfuncionariospontoById($id);
		
		if (!$importacoesfuncionariosponto) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		

		$erroplan = $this->leExcel($importacoesfuncionariosponto);

		$this->view->post_var = $importacoesfuncionariosponto;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($importacoesfuncionariosponto);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Arquivo editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	private function isValidXls($idarquivo) {
    	PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

    	$arquivos = new Arquivos();
    	$arquivo = $arquivos->downloadArquivo($idarquivo);

		$sourceFilename = $arquivo['sourceFilename'];
		$valid = false;
		$types = array('Excel2007', 'Excel5');
		foreach ($types as $type) {

		    $reader = PHPExcel_IOFactory::createReader($type);

		    if ($reader->canRead($sourceFilename)) {

		        $valid = true;
		        break;
		    }
		}

		return $valid;
    }

    private function leExcel($pedidoimportar) {

    	if (!$this->isValidXls($pedidoimportar['idarquivo'])) {
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		return;
    	}

		$arquivos = new Arquivos();
		$arquivo = $arquivos->downloadArquivo($pedidoimportar['idarquivo']);
		$sourceFilename = $arquivo['sourceFilename'];
		$objPHPExcel = PHPExcel_IOFactory::load($sourceFilename);
		$aba = 0;
		try {			
		    $sheet = $objPHPExcel->getSheet($aba);
		} catch (Exception $e) {
			$erros = array();
			
			array_push($erros, "Aba não encontrada. Digite uma aba Válida.");
			if (sizeof($erros)>0) return $erros; 
		}

		$sheet = $objPHPExcel->getSheet($aba);
		
		
		$dados = $sheet->toArray();
		$fields = $dados[0];


		$ids = array();
	
		$linhas = $sheet->toArray();
		
		$documentacaocursospontuacao = new Documentacaocursospontuacao();
		$documentacaosolicitacoespontos = new Documentacaosolicitacoespontos();
		$usuarios = new Usuarios();
		/*			
			$dados = array();
			$dados['id'] = $id;
			$dados["curso"] = $curso;
			$dados["valorpontos"] = $valorpontos;
			$dados["descricao"] = $descricao;
			$dados["status"] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
					//var_dump($dados);die();
			$row = $documentacaocursospontuacao->save($dados);


		$dados = array();
		$dados['id'] = $id;
		$dados["idusuario"] = $idusuario;
		$dados["idprofessor"] = $idprofessor;
		$dados["idcurso"] = $idcurso;
		if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;
		$dados["posicao"] = (Usuarios::getUsuario('professor') == 'Sim') ? 'Aguardando' : $posicao;
		$dados["pontuacao"] = $pontuacao;
		$dados["status"] = 'Ativo';
		$dados['excluido'] = 'nao';
		$dados['logusuario'] = $this->_usuario['id'];;
		$dados['logdata'] = date('Y-m-d G:i:s');
		$row = $documentacaosolicitacoespontos->save($dados);
		*/
		//VERIFICA os dados das células
		foreach ($linhas as $linha => $values) {
			if ($linha<=2) continue;

			$usuariorgf = Usuarios::getUsuarioByRgfHelper($values[2]);
			if(!$usuariorgf['id']) continue;

			$funcionario = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($usuariorgf['idfuncionario']);
			if(!$funcionario['id']){
				$funcionario = Funcionariosgeraisescolas::getFuncionariogeraisescolaByRgfHelper($values[2]);
			}
			if(!$funcionario['id']) continue;

			$idescola = $funcionario['idescola'];
			$idfuncionario = $funcionario['id'];
			
			$idcurso = 0;
			$_tipo = $documentacaocursospontuacao->getdocumentacaopontuacaoByTitulo($values[10]);
			if((isset($_tipo['id'])) && ((int)$_tipo['id']>0)) {
				$idcurso = $_tipo['id'];
			}else{
				$dadoscursopontuacao = array();
				$dadoscursopontuacao["curso"] = $values[10];
				$dadoscursopontuacao["status"] = 'Ativo';
				$dadoscursopontuacao['excluido'] = 'nao';
				$dadoscursopontuacao['logusuario'] = $this->_usuario['id'];;
				$dadoscursopontuacao['logdata'] = date('Y-m-d G:i:s');
				
				$row_dadoscursopontuacao = $documentacaocursospontuacao->save($dadoscursopontuacao);

				$idcurso = $row_dadoscursopontuacao['id'];
			}
			
			$_rows_v = $documentacaosolicitacoespontos->getDocumentacaosolicitacoespontoByIdUsuarioByIdCurso($usuariorgf['id'], $idcurso);
			if(($_rows_v['id']) && ((int)$_rows_v['id'] > 0)) continue;

			$dadospontos = array();
			$dadospontos["idusuario"] = $usuariorgf['id'];
			$dadospontos["idprofessor"] = $idfuncionario;
			$dadospontos["idcurso"] = $idcurso;
			$dadospontos["idescola"] = $idescola;
			$dadospontos["posicao"] = "Aprovado";
			$dadospontos["pontuacao"] = $values[4];
			$dadospontos["numerofilhos"] = $values[6];
			$dadospontos["status"] = 'Ativo';
			$dadospontos['excluido'] = 'nao';
			$dadospontos['logusuario'] = $this->_usuario['id'];;
			$dadospontos['logdata'] = date('Y-m-d G:i:s');
			
			$rowpontos = $documentacaosolicitacoespontos->save($dadospontos);
			
		}
		die('Sucesso!');
		return "";
    }

	/**
	 * 
	 * Action de adição de importacoesfuncionariospontos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'importacoesfuncionariospontos', 'name' => 'Importações de Arquivos'),
			array('url' => null,'name' => 'Adicionar Arquivo')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Arquivo adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idimportacoesfuncionariosponto
     */    
    private function preForm($idimportacoesfuncionariosponto = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_importacoesfuncionariosponto = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$importacoesfuncionariospontos = new Importacoesfuncionariospontos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			
$idarquivo = $this->getArquivo('idarquivo');
			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $importacoesfuncionariospontos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}