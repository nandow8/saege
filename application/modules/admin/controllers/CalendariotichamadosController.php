<?php

class Admin_CalendariotichamadosController extends Zend_Controller_Action { 
	 
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacoteescola
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		//Mn_Util::blockAccess("calendario", 'index');	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}

	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => null,'name' => 'Calendário')
		);	
				
		$this->preForm();
		$comunicacaoagenda = new Eventossecretarias();
		$queries = array();	
		$queries["idusuario"] = Usuarios::getUsuario('id');	
		$queries["origem"] = "Comunicação";	
		$queries["tipo"] = "Externa";	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['order'] = "ORDER BY e1.datainicio DESC";	

		$this->view->rows = $row = $comunicacaoagenda->getEventossecretarias($queries, '0', '5');	

		
    }

	public function index2Action() {
		$this->view->bread_crumb = array(
			array('url' => null,'name' => 'Calendário')
		);	
				
		$this->preForm();
		$comunicacaoagenda = new Eventossecretarias();
		$queries = array();	
		$queries["tipo"] = "Externa";
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['order'] = "ORDER BY e1.datainicio DESC";		
		$this->view->rows = $comunicacaoagenda->getEventossecretarias($queries, '0', '5');	
    }    
    
	public function setcalendarioAction() {
		$this->_helper->layout->disableLayout();
		$this->preForm();
		
		$page = (int)$this->getRequest()->getPost("page");
		
		$this->view->page = $page;
    }
    
    public function seteventoAction() {
    	$this->_helper->layout->disableLayout();
    	
    	$id = $this->getRequest()->getParam('id');
    	
		$comunicacaoagenda = new Eventossecretarias();
		$evento = $comunicacaoagenda->getEventosecretariaById($id);
		
		if (!$evento) die('Não Encontrado'); 
			
		$this->view->post_var = $evento;
    }
    
    public function seteventosdiaAction() {
    	$this->_helper->layout->disableLayout();
    	
    	$dia = $this->getRequest()->getParam('dia');
		
    	$queries = array();
    	$queries["origem"] = "Comunicação";	
    	$queries["idusuario"] = Usuarios::getUsuario('id');
		$queries['data'] = date('Y-m-d', $dia);
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$queries['order'] = "ORDER BY e1.datainicio DESC";
		
		$comunicacaoagenda = new Eventossecretarias();
		$rows = $comunicacaoagenda->getEventossecretarias($queries);
		
		if (!$rows) die('Não Encontrado'); 
		$this->view->dia = $dia;
		$this->view->rows = $rows;

		
		
	}
	
	function calendariotieventosajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		// $idescola = $this->getRequest()->getPost("idescola");
		// $rows = Censovagasescolasview::getviewCensovagasescolasviewByIdHelper($idescola);
			
		$chamadosAgendados = new Chamados();
		$rows = $chamadosAgendados->getChamadosHelper(array('destino'=>'TI'));
	

		$jsonRetorno = array();
		
		$jsonRetorno[0]['start'] = '2018-07-01';
		$jsonRetorno[0]['end'] = '2018-07-02';

		$jsonRetorno[1]['start'] = '2018-07-05';
		$jsonRetorno[1]['end'] = '2018-07-06';

		$jsonRetorno[2]['start'] = '2018-07-10';
		$jsonRetorno[2]['end'] = '2018-07-10';

		echo json_encode($jsonRetorno);

		die();

	}//end functinon calendariotieventosajaxAction

	public function calendariodetalhestiajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$idEvento = $this->getRequest()->getPost("idevento");

		$corEvento = $this->getRequest()->getPost("corevento");

		
		$teste = array();
		// evento => '#c2c2f0' Para Agenda
		// evento => '#4286f4' Para Visitas 

		if($corEvento == '#c2c2f0'): 

			$comunicacaoagenda = new Agendamentosvisitas();
			$queries = array();	
			$queries["id"] = $idEvento;	
		
			$row = $comunicacaoagenda->getAgendamentosvisitas($queries);	

			$teste['titulosuperior'] = $row[0]['titulo'];
			$teste['descricaobody'] = $row[0]['descricoes'];

			$dateObjInicio =  date("d-m-Y", strtotime($row[0]['datainicio']));
			$brDateInicio = new DateTime($dateObjInicio);

			$teste['datainicio'] = $brDateInicio->format('d / m / Y');

			$dateObjFim =  date("d-m-Y", strtotime($row[0]['datafim']));
			$brDateFim = new DateTime($dateObjFim);

			$teste['datafim'] = $brDateFim->format('d / m / Y');

		elseif($corEvento == '#4286f4'): 

			$chamados = new Chamados();
			$queries = array();	
			$queries["id"] = $idEvento;	
			$row = $chamados->getChamados($queries);

			$teste['titulosuperior'] = 'Chamado Aberto';
			$teste['descricaobody'] = $row[0]['descricoes'];

			$dateObjAgendada =  date("d-m-Y", strtotime($row[0]['dataagendada']));
			$brDateAgendada = new DateTime($dateObjAgendada);	
			$teste['datainicio'] = $brDateAgendada->format('d / m / Y') . " (agendamento)";
			$teste['datafim'] = '------';

		endif;

		//print_r($row);
		echo json_encode($teste);

		die();

	}//end calendariodetalhestiajaxAction
    
    /**
     * Atribui valores ao view
     * @param int $idescola
     */    
    private function preForm($idescola = 0) {
                $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
                
		$usuarios = new Usuarios();
		$this->view->usuarios = $usuarios->getUsuarios(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		$departamentos = new Departamentossecretarias();
		$this->view->departamentos = $departamentos->getDepartamentossecretarias(array('idsecretaria' => $this->view->idsecretaria));
    } 
}

