<?php

/**
 * Controle da classe logisticascontrolessaidas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_LogisticascontrolessaidasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Logisticascontrolessaida
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("logisticascontrolessaidas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Logisticascontrolessaidas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Logística de Saída excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="logisticascontrolessaidas") $objs = new Logisticascontrolessaidas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticascontrolessaidas', 'name' => 'Logísticas de Saída'),
			array('url' => null,'name' => 'Visualizar Logística de Saída')
		);
		
		$id = (int)$this->_request->getParam("id");
		$logisticascontrolessaidas = new Logisticascontrolessaidas();
		$logisticascontrolessaida = $logisticascontrolessaidas->getLogisticascontrolessaidaById($id, array());
		
		if (!$logisticascontrolessaida) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticascontrolessaida;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Logísticas de Saída')
		);
		
		$ns = new Zend_Session_Namespace('default_logisticascontrolessaidas');
		$logisticascontrolessaidas = new Logisticascontrolessaidas();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idfrota"]!="") $queries["idfrota"] = $this->view->post_var["idfrota"];
if ($this->view->post_var["idmotorista"]!="") $queries["idmotorista"] = $this->view->post_var["idmotorista"];
if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
// if ($this->view->post_var["horaentrada"]!="") $queries["horaentrada"] = $this->view->post_var["horaentrada"];
if ($this->view->post_var["datainicio_i"]!="") $queries["datainicio_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_i"]));
if ($this->view->post_var["datainicio_f"]!="") $queries["datainicio_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_f"]));
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $logisticascontrolessaidas->getLogisticascontrolessaidas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $logisticascontrolessaidas->getLogisticascontrolessaidas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de logisticascontrolessaidas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticascontrolessaidas', 'name' => 'Logísticas de Saída'),
			array('url' => null,'name' => 'Editar Logística de Saída')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$logisticascontrolessaidas = new Logisticascontrolessaidas();
		$logisticascontrolessaida = $logisticascontrolessaidas->getLogisticascontrolessaidaById($id);
		
		if (!$logisticascontrolessaida) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $logisticascontrolessaida;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($logisticascontrolessaida);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Logística de Saída editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de logisticascontrolessaidas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'logisticascontrolessaidas', 'name' => 'Logísticas de Saída'),
			array('url' => null,'name' => 'Adicionar Logística de Saída')
		);	

		$saidas = new Logisticascontrolessaidas();
		$queries = array();	
		$ultimo = $saidas->getUltimoLogisticascontrolessaida($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Logística de Saída adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
	}	
	
	public function getdadosveiculoAction() {
		$this -> _helper -> layout -> disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");		

		$id = (int)$this -> getRequest() -> getPost('idfrota');
		$veiculo = new Logisticafrotas();
		$rows = $veiculo -> getLogisticafrotaByIdHelper($id);

		echo json_encode($rows);
		die();
	}

	public function getvalidadecnhAction(){
		$this -> _helper -> layout -> disableLayout(); 
		$id = (int)$this -> getRequest() -> getPost('idmotorista');
		$cnhVencimento = Logisticamotoristas::getLogisticamotoristaByIdHelper($id);
		$datavalidadecnh  = new DateTime($cnhVencimento['datavalidadecnh']);
		$datahoje = new DateTime(date('Y-m-d G:i:s'));  
		 if($datavalidadecnh < $datahoje){
			 $validadeCNH = 'vencida'; 
		 }else{
			 $validadeCNH = $datavalidadecnh->diff($datahoje);
		 }
		
		echo json_encode($validadeCNH);
		die();
	}
    
    /**
     * Atribui valores ao view
     * @param int $idlogisticascontrolessaida
     */    
    private function preForm($idlogisticascontrolessaida = 0) {
    	$this->view->idsveiculos = Logisticascontrolesdentradas::getLogisticascontrolesdentradasHelper(array('diferentstatus'=>'Finalizado', 'ids'=>true));

    	$idsveiculos = Logisticascontrolessaidas::getLogisticascontrolessaidasHelper(array('diferentretorno'=>'Sim', 'ids'=>true));

    	if((isset($this->view->idsveiculos)) && ($this->view->idsveiculos) && ($this->view->idsveiculos!="") && (isset($idsveiculos)) && ($idsveiculos) && ($idsveiculos!="")){
    		$this->view->idsveiculos = $this->view->idsveiculos.','.$idsveiculos;
    	}elseif((isset($idsveiculos)) && ($idsveiculos) && ($idsveiculos!="")){
    		$this->view->idsveiculos =$idsveiculos; 
    	}
    	
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_logisticascontrolessaida = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idfrota = (int)trim($this->getRequest()->getPost("idfrota"));
$idmotorista = (int)trim($this->getRequest()->getPost("idmotorista"));
$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
$sequencial = trim($this->getRequest()->getPost("sequencial"));
$inicialkm = (int)trim($this->getRequest()->getPost("inicialkm"));
$quantidadecombustivel = trim($this->getRequest()->getPost("quantidadecombustivel"));
$mecanica = trim($this->getRequest()->getPost("mecanica"));
$eletrica = trim($this->getRequest()->getPost("eletrica"));
$funilaria = trim($this->getRequest()->getPost("funilaria"));
$pintura = trim($this->getRequest()->getPost("pintura"));
$pneus = trim($this->getRequest()->getPost("pneus"));
$observacao = trim($this->getRequest()->getPost("observacao"));
$macaco = trim($this->getRequest()->getPost("macaco"));
$triangulo = trim($this->getRequest()->getPost("triangulo"));
$estepe = trim($this->getRequest()->getPost("estepe"));
$extintor = trim($this->getRequest()->getPost("extintor"));
$chavederoda = trim($this->getRequest()->getPost("chavederoda"));
$datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
$horaentrada = trim($this->getRequest()->getPost("horaentrada"));
$status = trim($this->getRequest()->getPost("status1"));
		
$cnhVencimento = Logisticamotoristas::getLogisticamotoristaByIdHelper($idmotorista);

$datavalidadecnh  = new DateTime($cnhVencimento['datavalidadecnh']);
$datadasaida = new DateTime(date("Y-m-d", $datainicio));
 
$validadeCNH = $datavalidadecnh->diff($datadasaida);  

 		 

		
		$erros = array();
		
		if (0==$idfrota) array_push($erros, "Informe a Veículo.");
if (0==$idmotorista) array_push($erros, "Informe a Motorista.");
if (0==$idfuncionario) array_push($erros, "Informe a Funcionário.");
if (0==$inicialkm) array_push($erros, "Informe a Km inicial.");
if (""==$datainicio) array_push($erros, "Informe a Data da Saída.");
if (""==$horaentrada) array_push($erros, "Informe a Hora de saída.");
if (""==$status) array_push($erros, "Informe a Status.");
 		if($datavalidadecnh < $datadasaida){ 
			 if($validadeCNH->days > 0) array_push($erros, "Habilitação Vencida.");
		 } 
		
		$logisticascontrolessaidas = new Logisticascontrolessaidas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idfrota"] = $idfrota;
$dados["idmotorista"] = $idmotorista;
$dados["iddepartamento"] = $iddepartamento;
$dados["idfuncionario"] = $idfuncionario;
$dados["sequencial"] = $sequencial;
$dados["inicialkm"] = $inicialkm;
$dados["quantidadecombustivel"] = $quantidadecombustivel;
$dados["mecanica"] = $mecanica;
$dados["eletrica"] = $eletrica;
$dados["funilaria"] = $funilaria;
$dados["pintura"] = $pintura;
$dados["pneus"] = $pneus;
$dados["observacao"] = $observacao;
$dados["macaco"] = $macaco;
$dados["triangulo"] = $triangulo;
$dados["estepe"] = $estepe;
$dados["extintor"] = $extintor;
$dados["chavederoda"] = $chavederoda;
$dados["datainicio"] = date("Y-m-d", $datainicio);
$dados["horaentrada"] = $horaentrada;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s'); 
					
			$row = $logisticascontrolessaidas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}