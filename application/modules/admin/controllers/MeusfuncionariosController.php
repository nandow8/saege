<?php
//error_reporting(0);
//ini_set(“display_errors”, 0 );
/**
 * Controle da classe funcionariosgerais do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_MeusfuncionariosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Funcionariogeral
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("meusfuncionarios", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}



		$idfuncionario = Usuarios::getUsuario('idfuncionario');
		/*if((int)$idfuncionario > 0){
			$v_funcionario = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($idfuncionario);
			if((isset($v_funcionario['id'])) && ((int)$v_funcionario['id']>0)){
				$c_idsfuncionarios = explode(',', $v_funcionario['idsfuncionarios']);
				$salva_idsfuncionarios = array();
				$salva_idsfuncionarios[0] = '-1';
				foreach ($c_idsfuncionarios as $ic => $vf) {
					$key = array_search($vf, $salva_idsfuncionarios); // $key = 2;
					//echo 'vf: '.$vf . '_Key: '.(int)$key.'_idsfuncionarios:'.$v_funcionario['idsfuncionarios'] . '<br />'; 
					$key = (int)$key;
					if($key==0){
						array_push($salva_idsfuncionarios, $vf);
					}
				}
				unset($salva_idsfuncionarios[0]);
				$idsfuncionariosfinal = implode(',', $salva_idsfuncionarios);
				$v_funcionario['idsfuncionarios'] = $idsfuncionariosfinal;

				$_funcionariosgeraisescolas = new Funcionariosgeraisescolas();
				$_funcionariosgeraisescolas->save($v_funcionario);

			}
			
			$v_funcionario = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($idfuncionario);

			if((isset($v_funcionario['id'])) && ((int)$v_funcionario['id']>0)){
				$c_idsfuncionarios = explode(',', $v_funcionario['idsfuncionarios']);
				
				foreach ($c_idsfuncionarios as $ic => $vf) {
					if((int)$vf<=0){
						unset($c_idsfuncionarios[$ic]);
					}
				}
				$v_funcionario['idsfuncionarios'] = implode(',', $c_idsfuncionarios);

				$_funcionariosgeraisescolas = new Funcionariosgeraisescolas();
				$_funcionariosgeraisescolas->save($v_funcionario);
			}

		}*/
		

		//
		//
		/*$usuario = Usuarios::getUsuarioByRgfHelper(Usuarios::getUsuario('rgf'));
		if((!isset($usuario['idfuncionario'])) || ($usuario['idfuncionario']=="") || (!$usuario['idfuncionario'])){

			$fun_rgf = Funcionariosgeraisescolas::getFuncionariogeraisescolaByRgfHelper(Usuarios::getUsuario('rgf'));

			if((!isset($fun_rgf['idfuncionario'])) || ($fun_rgf['idfuncionario']=="") || (!$fun_rgf['idfuncionario'])){
				$usuario['idfuncionario'] = $fun_rgf['id'];	
				$usuarios = new Usuarios();
				$usuarios->save($usuario);

				$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
				$u_session = unserialize($loginNameSpace->usuario);
				$u_session['idfuncionario'] = $fun_rgf['id'];	
				$loginNameSpace->usuario = serialize($u_session);
			}					
			
		}*/

		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		$idescolalogado_pr = (int)Usuarios::getUsuario('idescola');

		/*if($idescolalogado_pr>0):
			$grupos = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas(array('idescolaselecionado'=>$idescolalogado_pr));
			foreach ($grupos as $kg => $vg) {
				foreach ($grupos as $k_add => $g_add) {
					$_meufuncionario = $meusfuncionariosgeraisescolas->fetchAll(' idfuncionarioemissor = '. $g_add['idfuncionarioadicionado'] .' AND idfuncionarioadicionado = ' . $vg['idfuncionarioemissor'] . ' AND idescolaselecionado = ' . $idescolalogado_pr . ' AND excluido = "nao"');

					if(isset($_meufuncionario['id'])) {
						continue;
					}else{
						$dadosnv = array();
						$dadosnv["idfuncionarioemissor"] = $g_add['idfuncionarioadicionado'];
						$dadosnv["idfuncionarioadicionado"] = $vg['idfuncionarioemissor'];
						$dadosnv["idescolaemissor"] = $idescolalogado_pr;
						$dadosnv["idescolaadicionado"] = $idescolalogado_pr;
						$dadosnv["idescolaselecionado"] = $idescolalogado_pr;
						$dadosnv["status"] = 'Ativo';
						$dadosnv['excluido'] = 'nao';
						$dadosnv['logusuario'] = $this->_usuario['id'];;
						$dadosnv['logdata'] = date('Y-m-d G:i:s');					
						$meusfuncionariosgeraisescolas->save($dadosnv);
					}

					
				}
			}
		endif;*/


		$idperfil_pr = (int)Usuarios::getUsuario('idperfil');

		/*if($idperfil_pr>0):
			$grupos = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas(array('idperfilselecionado'=>$idperfil_pr));
			foreach ($grupos as $kg => $vg) {
				foreach ($grupos as $k_add => $g_add) {
					$_meufuncionario = $meusfuncionariosgeraisescolas->fetchAll(' idfuncionarioemissor = '. $g_add['idfuncionarioadicionado'] .' AND idfuncionarioadicionado = ' . $vg['idfuncionarioemissor'] . ' AND idperfilselecionado = ' . $idperfil_pr . ' AND excluido = "nao"');

					if(isset($_meufuncionario['id'])) {
						continue;
					}else{
						$dadosnv = array();
						$dadosnv["idfuncionarioemissor"] = $g_add['idfuncionarioadicionado'];
						$dadosnv["idfuncionarioadicionado"] = $vg['idfuncionarioemissor'];
						$dadosnv["idperfilemissor"] = $idperfil_pr;
						$dadosnv["idperfiladicionado"] = $idperfil_pr;
						$dadosnv["idperfilselecionado"] = $idperfil_pr;
						$dadosnv["status"] = 'Ativo';
						$dadosnv['excluido'] = 'nao';
						$dadosnv['logusuario'] = $this->_usuario['id'];;
						$dadosnv['logdata'] = date('Y-m-d G:i:s');					
						$meusfuncionariosgeraisescolas->save($dadosnv);
					}

					
				}
			}
		endif;*/

		/*
		//GRUPOS
		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		$idescolalogado_pr = (int)Usuarios::getUsuario('idescola');
		if($idescolalogado_pr>0):
			$grupos = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas(array('idescolaselecionado'=>$idescolalogado_pr));
			$idfuncionario = (int)Usuarios::getUsuario('idfuncionario');

		
			foreach ($grupos as $ig => $rowgrupo) {
				foreach ($grupos as $iga => $rowgrupo_add2) {
					$idfuncionario = $rowgrupo_add2['idfuncionarioemissor'];
					$meufuncionario_nv = $meusfuncionariosgeraisescolas->fetchRow(' idfuncionarioemissor = ' . $rowgrupo['idfuncionarioadicionado'] . ' AND idfuncionarioadicionado = ' . $idfuncionario . ' AND idescolaadicionado = ' . $idescolalogado_pr . ' AND excluido = "nao"');
					//var_dump(' idfuncionarioemissor = ' . $rowgrupo['idfuncionarioadicionado'] . ' AND idfuncionarioadicionado = ' . $idfuncionario . ' AND idescolaadicionado = ' . $idescolalogado_pr . ' AND excluido = "nao"'); echo '<br />';


					$dados = array();
					if(isset($meufuncionario_nv['id'])){
						$dados['id'] = $meufuncionario_nv['id'];			
					}

					$funcionarioidgrupo = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($idfuncionario);
					if((isset($funcionarioidgrupo['idescola'])) && ((int)$funcionarioidgrupo['idescola']>0)){
						$idescolaadicionado = $funcionarioidgrupo['idescola'];
					}
					

					$dados["idfuncionarioemissor"] = $rowgrupo['idfuncionarioadicionado'];
					$dados["idfuncionarioadicionado"] = $idfuncionario;
					$dados["idescolaemissor"] = $idescolalogado_pr;
					$dados["idescolaadicionado"] = $idescolalogado_pr;
					$dados["idescolaselecionado"] = $idescolalogado_pr;
					$dados["status"] = 'Ativo';
					$dados['excluido'] = 'nao';
					$dados['logusuario'] = $this->_usuario['id'];;
					$dados['logdata'] = date('Y-m-d G:i:s');
					
					$meusfuncionariosgeraisescolas->save($dados);
				}
			}
		endif;*/
    	//die();
    	return ;

	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id_child");

		$idperfil = (int)Usuarios::getUsuario('idperfil');
		$idescola = (int)Usuarios::getUsuario('idescola');

		$rows = new Funcionariosgeraisescolas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			

			$idfuncionario = (int)Usuarios::getUsuario('idfuncionario');
			$verifica_idfuncionario = $rows->getFuncionariogeralescolaById((int)Usuarios::getUsuario('idfuncionario'));





			if((isset($verifica_idfuncionario['id'])) && ((int)$verifica_idfuncionario>0)){
				$_idsfuncionarios = explode(',', $verifica_idfuncionario['idsfuncionarios']);
				
				foreach ($_idsfuncionarios as $k => $v) {
					if($v==$id) {
						unset($_idsfuncionarios[$k]);
					}

				}				

				if($idescola>0){
					$verifica_rows = $rows->getFuncionariosgeraisescolas(array('idescola' => $verifica_idfuncionario['idescola'], 'idsfuncionarios'=>$id));

					foreach ($verifica_rows as $key => $value) {
						$verifica_idfuncionario_sel = $rows->getFuncionariogeralescolaById((int)$value['id'] );
						$verifica_idfuncionario_sel['idsfuncionarios'] = implode(',', $_idsfuncionarios);
						//$_ro = $rows->save($verifica_idfuncionario_sel);
					}	


					$idfuncionariologado = Usuarios::getUsuario('idfuncionario');
					$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
					$_meufuncionario = $meusfuncionariosgeraisescolas->fetchAll(' idfuncionarioadicionado = ' . $id . ' AND idescolaadicionado = ' . $idescola . ' AND excluido = "nao"');
					//var_dump($_meufuncionario); die();
					foreach($_meufuncionario as $ima=>$row_m){
						$row_m = $row_m->toArray();
						$row_m['excluido'] = "sim";
						//var_dump($row_m); die();
						$meusfuncionariosgeraisescolas->save($row_m);
					}

					$_meufuncionarioe = $meusfuncionariosgeraisescolas->fetchAll(' idfuncionarioemissor = ' . $id . ' AND idescolaadicionado = ' . $idescola . ' AND excluido = "nao"');
					//var_dump($_meufuncionario); die();
					foreach($_meufuncionarioe as $ima=>$row_m){
						$row_m = $row_m->toArray();
						$row_m['excluido'] = "sim";
						//var_dump($row_m); die();
						$meusfuncionariosgeraisescolas->save($row_m);
					}

				}else{

					$verifica_rows = $rows->getFuncionariosgeraisescolas(array('idperfil' => $idperfil, 'idsfuncionarios'=>$id));

					$_id = (int)Usuarios::getUsuario('idfuncionario');
					$idperfil = (int)Usuarios::getUsuario('idperfil');
					$idescola = (int)Usuarios::getUsuario('idescola');
					$idsfuncionariosdiferentidperfil = "";
					$_funsdiferentidperfil = "";
					$usur_funcionario = Usuarios::getUsuarioByIdFuncionarioHelper($id);




					$idfuncionariologado = Usuarios::getUsuario('idfuncionario');
					$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();

					$idperfillogado = Usuarios::getUsuario('idperfil');

					$_meufuncionario = $meusfuncionariosgeraisescolas->fetchAll(' idfuncionarioadicionado = ' . $id . ' AND idperfiladicionado = ' . $idperfillogado . ' AND excluido = "nao"');

			

					foreach($_meufuncionario as $ima=>$row_m){
						$row_m = $row_m->toArray();
						$row_m['excluido'] = "sim";
						$meusfuncionariosgeraisescolas->save($row_m);
					}
					
					$_meufuncionarioe = $meusfuncionariosgeraisescolas->fetchAll(' idfuncionarioemissor = ' . $id . ' AND idperfiladicionado = ' . $idperfillogado . ' AND excluido = "nao"');
					//var_dump($_meufuncionario); die();
					foreach($_meufuncionarioe as $ima=>$row_m){
						$row_m = $row_m->toArray();
						$row_m['excluido'] = "sim";
						//var_dump($row_m); die();
						$meusfuncionariosgeraisescolas->save($row_m);
					}


					$funcionariosgerais = new Funcionariosgeraisescolas();
					$funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById($id);


					if($idperfil!=(int)$usur_funcionario['idperfil']){

						$_funsdiferentidperfil = explode(',', $funcionariogeral['idsfuncionarios']);
						//array_push($_funsdiferentidperfil, $verifica_idfuncionario['id']);
						foreach ($_funsdiferentidperfil as $k => $v) {
							//var_dump('$v: '.$v.'____$_id'.$_id); 
							if((isset($v)) && ($v) && ($v!="") && ($v==$_id)) {
							
								unset($_funsdiferentidperfil[$k]);
							}
						}




						$idsfuncionariosdiferentidperfil = implode(',', $_funsdiferentidperfil);
						$funcionariogeral['idsfuncionarios'] = $idsfuncionariosdiferentidperfil;
						$_ro = $rows->save($funcionariogeral);

						$verifica_rows = $rows->getFuncionariosgeraisescolas(array('idperfil' => $idperfil, 'idsfuncionarios'=>$id));

						foreach ($verifica_rows as $key => $value) {
							$verifica_idfuncionario_sel = $rows->getFuncionariogeralescolaById((int)$value['id'] );
							$verifica_idfuncionario_sel['idsfuncionarios'] = implode(',', $_idsfuncionarios);
							$_ro = $rows->save($verifica_idfuncionario_sel);
						}


						$_idsfuncionarios = explode(',', $verifica_idfuncionario['idsfuncionarios']);
						
						foreach ($_idsfuncionarios as $k => $v) {
							if($v==$id) {
								unset($_idsfuncionarios[$k]);
							}

						}

						$verifica_idfuncionario['idsfuncionarios'] = implode(',', $_idsfuncionarios);
						//$_ro = $rows->save($verifica_idfuncionario);

					}else{
						foreach ($verifica_rows as $key => $value) {
							$verifica_idfuncionario_sel = $rows->getFuncionariogeralescolaById((int)$value['id'] );
							$verifica_idfuncionario_sel['idsfuncionarios'] = implode(',', $_idsfuncionarios);
							$_ro = $rows->save($verifica_idfuncionario_sel);
						}
					}

					
				}



				
				$message = new Zend_Session_Namespace("message");
				$message->crudmessage = "Funcionário excluído com sucesso.";
				
				die("OK");
			}

			/*$idperfil = (int)Usuarios::getUsuario('idperfil');
			$idfuncionario = (int)Usuarios::getUsuario('idfuncionario');

			$_perfis = explode(',', $row['idsperfis']);
			array_push($_perfis, $idperfil);
			foreach ($_perfis as $k => $v) {
				if($v==$idperfil) {
					unset($_perfis[$k]);
				}

			}

			$ff = "";
			$verifica_idfuncionarioa = $rows->getFuncionariogeralescolaById((int)$id);
			
			$_idsfuncionariosa = explode(',', $verifica_idfuncionarioa['idsfuncionarios']);
			//array_push($_idsfuncionarios, $idfuncionario);

			foreach ($_idsfuncionariosa as $k => $v) {
				if($v==(int)Usuarios::getUsuario('idfuncionario')) {
					unset($_idsfuncionariosa[$k]);
				}

			}
			$verifica_idfuncionarioa['idsfuncionarios'] = implode(',', $_idsfuncionariosa);
			$rows->save($verifica_idfuncionarioa);	


			$verifica_idfuncionario = $rows->getFuncionariogeralescolaById((int)Usuarios::getUsuario('idfuncionario'));
			
			$_idsfuncionarios = explode(',', $verifica_idfuncionario['idsfuncionarios']);
			//array_push($_idsfuncionarios, $idfuncionario);

			foreach ($_idsfuncionarios as $k => $v) {
				if($v==$id) {
					unset($_idsfuncionarios[$k]);
				}

			}


			$verifica_idfuncionario['idsfuncionarios'] = implode(',', $_idsfuncionarios);
			$rows->save($verifica_idfuncionario);	


			$ff = $verifica_idfuncionarioa['idsfuncionarios'];

			foreach ($_idsfuncionarios as $k => $v) {
				$verifica_idfuncionario_for = $rows->getFuncionariogeralescolaById((int)$v);
				$verifica_idfuncionario_for['idsfuncionarios'] = $ff;
				$rows->save($verifica_idfuncionario_for);					
			}

			$row['idsperfis'] = implode(',', $_perfis);
			$row['idsfuncionarios'] = implode(',', $_idsfuncionarios);
			$row['idperfil'] = "";
			$row['idsfuncionarios'] = "";
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$_ro = $rows->save($row);
			
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário excluído com sucesso.";
			
			die("OK");*/
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="meusfuncionarios") $objs = new Funcionariosgeraisescolas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'meusfuncionarios', 'name' => 'Funcionários'),
			array('url' => null,'name' => 'Visualizar Funcionário')
		);
		
		$id = (int)$this->_request->getParam("id");
		$funcionariosgerais = new Funcionariosgeraisescolas();
		$funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById($id, array());
		
		if (!$funcionariogeral) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $funcionariogeral;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	

	public function corrigimeusfuncionariosmultiplasescolasAction() {
		$this->_helper->layout->disableLayout();

		$funcionariosgerais = new Funcionariosgeraisescolas();
		$todos = $funcionariosgerais->getFuncionariosgeraisescolas(array('possuimultiplasescolas'=>true,'possuimultiplosfuncionarios'=>true));

		foreach($todos as $t=>$row){
			var_dump($row['idescolas']); 
			var_dump($row['idsfuncionarios']); 
			die();	
		}

		

		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		die();
	}
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Funcionários')
		);
		
		$ns = new Zend_Session_Namespace('admin_meusfuncionarios');
		$funcionariosgerais = new Funcionariosgeraisescolas();
		$queries = array();	
		//$queries["iddepartamento"] = 18;
		$idperfil = (int)Usuarios::getUsuario('idperfil');
		$idescola = (int)Usuarios::getUsuario('idescola');

		$_usuarios = new Usuarios();
		

		$_funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById((int)Usuarios::getUsuario('idfuncionario'));

		$this->view->funcionariogeral = $_funcionariogeral;

		//$_funcionariogeral = Funcionariosgeraisescolas::getFuncionariogeraisescolaByRgfHelper(Usuarios::getUsuario('rgf'));
		/*
		if($idescola>0)
		{
			//$queries['meuid'] = Usuarios::getUsuario('idfuncionario');
			//$queries['meusfuncionarios_or'] = true;
			//$queries['idescola'] = Usuarios::getUsuario('idescola');
			$verificausuario = $_usuarios->getUsuarioById(Usuarios::getUsuario('id'));

			if((isset($verificausuario['idperfil'])) && ($verificausuario['idperfil']) && ((int)$verificausuario['idperfil'] > 0)){

				$idperfil = $verificausuario['idperfil'];	
			}
			
			
			if((!isset($_funcionariogeral['idperfil'])) || (!isset($_funcionariogeral['idsperfis'])) || ($_funcionariogeral['idperfil']=="") || ($_funcionariogeral['idsperfis']=="") || ($_funcionariogeral['idperfil']<=0) ){
				$idperfil = 0;
			}

			if((isset($_funcionariogeral['idsfuncionarios'])) && ($_funcionariogeral['idsfuncionarios']!="") ){
				$queries['idsfuncionariossel'] = $_funcionariogeral['idsfuncionarios'];
				if((!isset($queries['idsfuncionariossel'])) || ($queries['idsfuncionariossel']=="")){
					$queries['idsfuncionariossel'] = "-1";
				}
			}

			$idfuncionariologado = Usuarios::getUsuario('idfuncionario');
			$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
			$meufuncionario = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas(array('idescolaadicionado' => $idescola, 'order'=>' GROUP BY m1.idfuncionarioadicionado'));

			$idsfuncionariosselecionados = array();
			foreach ($meufuncionario as $im => $vm) {
				array_push($idsfuncionariosselecionados, $vm['idfuncionarioadicionado']);
			}
			$idsfuncionariosselecionados = implode(',', $idsfuncionariosselecionados);
			//var_dump($idsfuncionariosselecionados); die();
			if((isset($idsfuncionariosselecionados)) && ($idsfuncionariosselecionados!="")){
				$queries['idsfuncionariossel'] = $idsfuncionariosselecionados;	
			}else{
				$queries['idsfuncionariossel'] = "-1";
			}
			
			
			
		}
		elseif((isset($_funcionariogeral['idsfuncionarios'])) && ($_funcionariogeral['idsfuncionarios']!="") )
		{
			$queries['idsfuncionariossel'] = $_funcionariogeral['idsfuncionarios'];

		}
		elseif($idperfil > 0)
		{
			$queries['idperfil'] = $idperfil;
		}
		else
		{
			$queries['idsfuncionariossel'] = "-1";
		}
		if(!isset($queries['idsfuncionariossel'])){
			$queries['idsfuncionariossel'] = '-1';
		}*/
		//$queries['idescola'] = Usuarios::getUsuario('idescola');
        if((int)$idescola > 0){
        	$idfuncionario = Usuarios::getUsuario('idfuncionario');
            $meusfuncionarios = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaImplodeByIdescolaHelper($idfuncionario, $idescola, array());
            //var_dump($meusfuncionarios); die();
            if((isset($meusfuncionarios)) && ($meusfuncionarios) && ($meusfuncionarios!="")){
                $queries['idsfuncionariossel'] = $meusfuncionarios;
            }else{
                $queries['idsfuncionariossel'] = '-1';
            }

        }elseif((int)$idperfil > 0){
        	$idfuncionario = Usuarios::getUsuario('idfuncionario');
            $meusfuncionarios = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaImplodeByIdperfilHelper($idfuncionario, $idperfil, array());

            if((isset($meusfuncionarios)) && ($meusfuncionarios) && ($meusfuncionarios!="")){
                $queries['idsfuncionariossel'] = $meusfuncionarios;
            }else{
                $queries['idsfuncionariossel'] = '-1';
            }
        }

		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ((isset($this->view->post_var["nome"])) && ($this->view->post_var["nome"]!="")) $queries["nome"] = $this->view->post_var["nome"];
			if ((isset($this->view->post_var["telefone"])) && ($this->view->post_var["telefone"]!="")) $queries["telefone"] = $this->view->post_var["telefone"];
			// if ($this->view->post_var["tipo"]!="") $queries["tipo"] = $this->view->post_var["tipo"];
			if ((isset($this->view->post_var["status1"])) && ($this->view->post_var["status1"]!="")) $queries["status"] = $this->view->post_var["status1"];
			    		
    		//if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}	
			//$queries['die'] = true;	
		
		//unset($queries['idescola']);

		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $funcionariosgerais->getFuncionariosgeraisescolas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;  

		$this->view->rows = $funcionariosgerais->getFuncionariosgeraisescolas($queries, $paginaAtual, $maxpp);
	}
	
	/**
	 * 
	 * Action de edição de funcionariosgerais
	 */	
	public function editarAction(){
		$this->view->bread_crumb = array(
			array('url' => 'meusfuncionarios', 'name' => 'Funcionários'),
			array('url' => null,'name' => 'Editar Funcionário')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$funcionariosgerais = new Funcionariosgeraisescolas();
		$funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById($id);
		
		if (!$funcionariogeral) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $funcionariogeral;
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($funcionariogeral);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;			
    }  		
	
	/**
	 * 
	 * Action de adição de funcionariosgerais 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'meusfuncionarios', 'name' => 'Funcionários'),
			array('url' => null,'name' => 'Adicionar Funcionário')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function getdadosAction() {
		$this->_helper->layout->disableLayout();
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Funcionariosgeraisescolas();
		$row = $rows->getFuncionariogeralescolaById($id);
		$row = (object)$row;
		$row = json_encode($row);
		//var_dump($row); die();
		die($row);
	}

    /**
     * Atribui valores ao view
     * @param int $idfuncionariogeral
     */    
    private function preForm($idfuncionariogeral = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_funcionariogeral = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		//print_r("sfasdf");
		//die();
		
		$id = (int)$this->getRequest()->getPost("id");

		$idsecretaria = (int)Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		$nome = trim($this->getRequest()->getPost("nome"));
		$idtipoequipe = trim((int)$this->getRequest()->getPost("idtipoequipe"));
		$telefone = trim($this->getRequest()->getPost("telefone"));
		$modulo = trim($this->getRequest()->getPost("modulo"));
		$tipo = trim($this->getRequest()->getPost("tipo"));
		$idendereco = (int)trim($this->getRequest()->getPost("idendereco"));
		$status = trim($this->getRequest()->getPost("status1"));
		$idfuncaosecretaria = trim($this->getRequest()->getPost("idfuncaosecretaria"));
		
		// pucando informação nova para o bloco de horários do funcionário 
        $horarioPonto = trim($this->getRequest()->getPost("horarioponto"));

		$erros = array();
		
		if (0>=$id) array_push($erros, "Informe a um funcionário.");
		$funcionariosgerais = new Funcionariosgeraisescolas();


		$funcionario = $funcionariosgerais->getFuncionariogeralescolaById((int)Usuarios::getUsuario('idfuncionario'));
		if(!$funcionario) array_push($erros, "O usuário logado não foi encontrado.");
			
		/*if(isset($id)){
	    	$idfuncionario = $id;
	    	$idescolalogado = Usuarios::getUsuario('idescola');
			$verficaexistente = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaValida($idfuncionario, $idescolalogado);
			if($verficaexistente===true)  array_push($erros, "O funcionário já está vínculado a outra escola, para adionana-lo é necessário retira-lo da outra escola.");
		}*/


		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$enderecosModel = new EnderecoModel($this);
			$endereco = $enderecosModel->getPost( ($_funcionariogeral) ? (int)$_funcionariogeral['idendereco'] : 0, "e_idendereco");
			$idendereco = ($endereco) ? $endereco['id'] : 0;	
			//$dados['idendereco'] = $idendereco;
			$dados['idtipoequipe'] = $idtipoequipe;

			$dados['idperfil'] = (int)Usuarios::getUsuario('idperfil');
			$idperfil = (int)Usuarios::getUsuario('idperfil');
			$idescola = (int)Usuarios::getUsuario('idescola');
			$idsfuncionariosdiferentidperfil = "";
			$_funsdiferentidperfil = "";
			$usur_funcionario = Usuarios::getUsuarioByIdFuncionarioHelper($id);




			/*if(($idperfil>0)&&($idescola>0)) {
				$dados['idescola'] = Usuarios::getUsuario('idescola');
				
			}*/

			$v_f = $funcionariosgerais->getFuncionariogeralescolaById($id);
			if(isset($v_f['idescolas'])){
				$vv_f = explode(',', $v_f['idescolas']);
				if((isset($vv_f[0])) && ($vv_f[0]) && ((int)$vv_f[0] > 0)){
					$dados['idescola'] = $vv_f[0];	
				}
			}

			$_usuarios = new Usuarios();
			$verificausuario = $_usuarios->getUsuarioById($id);
	

			$verifica_fun = $funcionariosgerais->getFuncionariosgeraisescolas(array('id'=>$id, 'idsfuncionarios'=>(int)Usuarios::getUsuario('id')));


			$verifica_idfuncionario = $funcionariosgerais->getFuncionariogeralescolaById((int)Usuarios::getUsuario('idfuncionario'));

			if((isset($verifica_idfuncionario['idescola'])) && ((int)$verifica_idfuncionario['idescola'] > 0) ){
				
			}else{

				if($idperfil>0){
			//$idescola

					$verifica_idperfil = $funcionariosgerais->getFuncionariosgeraisescolas(array('id'=>$id, 'idsperfis'=>$idperfil));
	
					if(sizeof($verifica_idperfil) > 0){				
						
					}else{		
						
						$funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById($id);
						if(isset($funcionariogeral['id'])){
							$_perfis = explode(',', $funcionariogeral['idsperfis']);
							array_push($_perfis, $idperfil);
							foreach ($_perfis as $k => $v) {
								if(($v=="") || (!$v) || (!isset($v))) {
									unset($_perfis[$k]);
								}
							}

							$dados['idsperfis'] = implode(',', $_perfis);
							
						}
						unset($dados['idescola']);
					}





							
					//var_dump($verifica_idperfil); die('aaa');
					
					if(sizeof($verifica_fun) > 0){				
						
					}else{		

						$funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById($id);

						if(isset($funcionariogeral['id'])){
							$_funs = explode(',', $funcionariogeral['idsfuncionarios']);
							array_push($_funs, (int)Usuarios::getUsuario('id'));
							foreach ($_funs as $k => $v) {
								if(($v=="") || (!$v) || (!isset($v))) {
									unset($_funs[$k]);
								}
							}

							//$dados['idsfuncionarios'] = implode(',', $_funs);
										
						}
					
					}
					
				
				}
			}
				//Sergio - é o id do editar
				//var_dump($funcionariogeral);
				//id = 579
				//die('aqui0');

				//Adenir - é o id do cara logado
				//id = 476
				//var_dump($verifica_idfuncionario);
				//die('aqui1');		

			$_ff = "";
			//$_funs = "";

			if(isset($verifica_idfuncionario['id'])){
				$_funs = explode(',', $verifica_idfuncionario['idsfuncionarios']);
				array_push($_funs, $id);

				if($idescola>0){
					foreach ($_funs as $k => $v) {
						if(($v=="") || (!$v) || (!isset($v))) {
							unset($_funs[$k]);
						}
					}
				}else{
					foreach ($_funs as $k => $v) {
						if(($v=="") || (!$v) || (!isset($v))) {
							unset($_funs[$k]);
						}
					}
				}
				
			if($idperfil!=$usur_funcionario['idperfil']){
				$_funsdiferentidperfil = explode(',', $verifica_idfuncionario['idsfuncionarios']);

				array_push($_funsdiferentidperfil, $verifica_idfuncionario['id']);
				foreach ($_funsdiferentidperfil as $k => $v) {
					if(($v=="") || (!$v) || (!isset($v))) {
						unset($_funs[$k]);
					}
				}
				$dados['idperfil'] = (int)$usur_funcionario['idperfil'];
				$idsfuncionariosdiferentidperfil = implode(',', $_funsdiferentidperfil);
				//$dados['idsfuncionarios'] = $idsfuncionariosdiferentidperfil;
				
			}

				$verifica_idfuncionario['idsfuncionarios'] = implode(',', $_funs);				
				$funcionariosgerais->save($verifica_idfuncionario);



				$_ff = 	$_funs;
				$ff = $_funs;
				$_ff = implode(',', $_ff);
				$ff  = implode(',', $ff);

			}else{
				$usuario = Usuarios::getUsuarioByRgfHelper(Usuarios::getUsuario('rgf'));
				$fun_rgf = Funcionariosgeraisescolas::getFuncionariogeraisescolaByRgfHelper(Usuarios::getUsuario('rgf'));
				if((!isset($usuario['idfuncionario'])) || ($usuario['idfuncionario']=="") || (!$usuario['idfuncionario'])){


					if((!isset($fun_rgf['idfuncionario'])) || ($fun_rgf['idfuncionario']=="") || (!$fun_rgf['idfuncionario'])){
						$usuario['idfuncionario'] = $fun_rgf['id'];	
						$usuarios = new Usuarios();
						$usuarios->save($usuario);

						$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
						$u_session = unserialize($loginNameSpace->usuario);
						$u_session['idfuncionario'] = $fun_rgf['id'];	
						$loginNameSpace->usuario = serialize($u_session);
					}					
					
				}
				
				
				$ff = $fun_rgf['id'];
				

			}
				
			if($idescola>0){
				$verifica  = $this->atualizameusfuncioanrios(array(), $id);
				
				if(!$verifica){
					$erros = array();
					array_push($erros, "O funcionário já está vínculado a outra escola, para adionana-lo é necessário retira-lo da outra escola.");
					if(sizeof($erros) > 0) return $erros;
				}
				



			}else{
				$verifica  = $this->atualizameusfuncioanriosdepartamentos(array(), $id);
				$erros = array();

				if(!$verifica){
					array_push($erros, "O funcionário já está vínculado a outro departamento, para adionana-lo é necessário retira-lo do outro departamento.");

					if(sizeof($erros) > 0) return $erros;
				}

			}

				
			foreach ($_funs as $k => $v) {
			
				$iff = "";
				if($idescola>0){
					//die('aaaaa');
					$verifica_idfuncionario_for = $funcionariosgerais->getFuncionariogeralescolaById((int)$v, array('idescola'=>$idescola));
		
					
				//if($verifica_idfuncionario_for['idescola']==$idescola){
					if($iff == ""){
						$iff = $v;
					}else{
						$iff = $v . ",".$v;
					}
					//$verifica_idfuncionario_for['idsfuncionarios'] = $iff;
					$verifica_idfuncionario_for['idsfuncionarios'] = $ff;

					//$this->atualizameusfuncioanrios($verifica_idfuncionario_for, $id);


	
					//$funcionariosgerais->save($verifica_idfuncionario_for);					
				//}
				}else{
					//die('bbbb');
					$verifica_idfuncionario_for = $funcionariosgerais->getFuncionariogeralescolaById((int)$v, array('idperfil'=>$idperfil));
		
					
				//if($verifica_idfuncionario_for['idescola']==$idescola){

					if($iff == ""){
						$iff = $v;
					}else{
						$iff = $v . ",".$v;
					}
					$verifica_idfuncionario_for['idsfuncionarios'] = $iff;
					$_id = Usuarios::getUsuario('idfuncionario');
					$this->atualizameusfuncioanriosdepartamentos($verifica_idfuncionario_for, $id);

					if($_id==$v){
						$verifica_idfuncionario_for['idsfuncionarios'] = $ff;
						$funcionariosgerais->save($verifica_idfuncionario_for);		
						//$dados['idsfuncionarios'] = $ff;
					}

					
									
				}
				
			}

//die('aaa');
			if($idescola>0){
				if($v_f['idescola']==$idescola){
					$dados['idsfuncionarios'] = $_ff;
				}
			}else{
				$fun_rgf = Funcionariosgeraisescolas::getFuncionariosgeraisescolasHelper(array('idperfil'=>$idperfil));
				$funcionariosgeraisescolas_r = new Funcionariosgeraisescolas();
				foreach ($fun_rgf as $key_p => $value_p) {

					$value_p['idsfuncionarios'] = $ff;
					$value_p['idperfil'] = $idperfil;
					$funcionariosgeraisescolas_r->save($value_p);
				}
			}
			if((int)$idperfil > 0){
				$dados['idsfuncionarios'] = $idsfuncionariosdiferentidperfil;	
			}
			unset($dados['idsfuncionarios']);
			$dados["idfuncaosecretaria"] = (int)$idfuncaosecretaria;

			$dados["idsecretaria"] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
			$dados['idhorarioponto'] = null;
			
			$dados["telefone"] = $telefone;
			$dados["modulo"] = $modulo;
			$dados["tipo"] = $tipo;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

			//var_dump($dados);die();
			$row = $funcionariosgerais->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }

    private function atualizameusfuncioanrios($row, $id) {
    	$idsescolas = Usuarios::getUsuario('idescolas');
    	$idsescolas = explode(',', $idsescolas);
    	$idfuncionario = Usuarios::getUsuario('idfuncionario');
    	$idescolalogado = Usuarios::getUsuario('idescola');
    	$idescolaadicionado = $idescolalogado;
    	$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
    	
		$dados = array();

		$meufuncionario = $meusfuncionariosgeraisescolas->fetchRow(' idfuncionarioemissor = ' . $idfuncionario . ' AND idfuncionarioadicionado = ' . $id . ' AND idescolaadicionado = ' . $idescolalogado . ' AND excluido = "nao"');

		
		if(isset($meufuncionario['id'])){
			$dados['id'] = $meufuncionario['id'];			
		}


		$meufuncionariooutraescola = $meusfuncionariosgeraisescolas->fetchRow('idfuncionarioadicionado = ' . $id . ' AND idescolaadicionado <> ' . $idescolalogado . ' AND excluido = "nao"');

		if(isset($meufuncionariooutraescola['id'])){
			return false;
		}

		$funcionarioid = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($id);
		if((isset($funcionarioid['idescola'])) && ((int)$funcionarioid['idescola']>0)){
			$idescolaadicionado = $funcionarioid['idescola'];
		}
		

		$dados["idfuncionarioemissor"] = $idfuncionario;
		$dados["idfuncionarioadicionado"] = $id;
		$dados["idescolaemissor"] = $idescolalogado;
		$dados["idescolaadicionado"] = $idescolalogado;
		$dados["idescolaselecionado"] = $idescolalogado;
		$dados["status"] = 'Ativo';
		$dados['excluido'] = 'nao';
		$dados['logusuario'] = $this->_usuario['id'];;
		$dados['logdata'] = date('Y-m-d G:i:s');
		
		$_row = $meusfuncionariosgeraisescolas->save($dados);

		/* ATUALIZA GRUPOS */
		$grupos = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas(array('idescolaselecionado'=>$idescolalogado));

		foreach ($grupos as $ig => $rowgrupo) {
			$meufuncionario_nv = $meusfuncionariosgeraisescolas->fetchRow(' idfuncionarioemissor = ' . $rowgrupo['idfuncionarioadicionado'] . ' AND idfuncionarioadicionado = ' . $id . ' AND idescolaadicionado = ' . $idescolalogado . ' AND excluido = "nao"');

			$dados = array();
			if(isset($meufuncionario_nv['id'])){
				$dados['id'] = $meufuncionario_nv['id'];			
				//continue;
			}



			$dados["idfuncionarioemissor"] = $rowgrupo['idfuncionarioadicionado'];
			$dados["idfuncionarioadicionado"] = $id;
			$dados["idescolaemissor"] = $idescolalogado;
			$dados["idescolaadicionado"] = $idescolalogado;
			$dados["idescolaselecionado"] = $idescolalogado;
			$dados["status"] = 'Ativo';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			//var_dump($dados);
			$meusfuncionariosgeraisescolas->save($dados);
		}
    	
    	return true;
    }


	private function atualizameusfuncioanriosdepartamentos($row, $id) {

    	$idfuncionario = Usuarios::getUsuario('idfuncionario');
    	$idperfillogado = Usuarios::getUsuario('idperfil');
    	$idperfiladicionado = $idperfillogado;
    	$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
    	
		$dados = array();

		$meufuncionario = $meusfuncionariosgeraisescolas->fetchRow(' idfuncionarioemissor = ' . $idfuncionario . ' AND idfuncionarioadicionado = ' . $id . ' AND idperfiladicionado = ' . $idperfillogado . ' AND excluido = "nao"');

		
		if(isset($meufuncionario['id'])){
			$dados['id'] = $meufuncionario['id'];			
		}

		$meufuncionariooutraescola = $meusfuncionariosgeraisescolas->fetchRow(' idfuncionarioadicionado = ' . $id . ' AND idperfiladicionado <> ' . $idperfillogado . ' AND excluido = "nao"');

		if(isset($meufuncionariooutraescola['id'])){
			return false;
		}

		$funcionarioid = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($id);
		if((isset($funcionarioid['idperfil'])) && ((int)$funcionarioid['idperfil']>0)){
			$idperfiladicionado = $funcionarioid['idperfil'];
		}
		

		$dados["idfuncionarioemissor"] = $idfuncionario;
		$dados["idfuncionarioadicionado"] = $id;
		$dados["idperfilemissor"] = $idperfillogado;
		$dados["idperfiladicionado"] = $idperfillogado;
		$dados["idperfilselecionado"] = $idperfillogado;
		$dados["status"] = 'Ativo';
		$dados['excluido'] = 'nao';
		$dados['logusuario'] = $this->_usuario['id'];;
		$dados['logdata'] = date('Y-m-d G:i:s');
		
		$_row = $meusfuncionariosgeraisescolas->save($dados);

		/* ATUALIZA GRUPOS */

		$grupos = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas(array('idperfilselecionado'=>$idperfillogado));

		foreach ($grupos as $ig => $rowgrupo) {
			$meufuncionario_nv = $meusfuncionariosgeraisescolas->fetchRow(' idfuncionarioemissor = ' . $rowgrupo['idfuncionarioadicionado'] . ' AND idfuncionarioadicionado = ' . $id . ' AND idperfiladicionado = ' . $idperfillogado . ' AND excluido = "nao"');

			$dados = array();
			if(isset($meufuncionario_nv['id'])){
				$dados['id'] = $meufuncionario_nv['id'];			
			}

			$dados["idfuncionarioemissor"] = $rowgrupo['idfuncionarioadicionado'];
			$dados["idfuncionarioadicionado"] = $id;
			$dados["idperfilemissor"] = $idperfillogado;
			$dados["idperfiladicionado"] = $idperfillogado;
			$dados["idperfilselecionado"] = $idperfillogado;
			$dados["status"] = 'Ativo';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
			//var_dump($dados); die();
			$meusfuncionariosgeraisescolas->save($dados);
		}
    	
    	return true;
    }


    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}