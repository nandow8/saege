<?php

/**
 * Controle da classe secretariasprovastrabalhos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_SecretariasprovastrabalhosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Secretariasprovastrabalho
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("secretariasprovastrabalhos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}


		$secretariasprovastrabalhos = new Secretariasprovastrabalhos();
		$queries = array();	
		$ultimo = $secretariasprovastrabalhos->getUltimoSecretariasprovastrabalho($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Secretariasprovastrabalhos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Prova e Trabalho excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="secretariasprovastrabalhos") $objs = new Secretariasprovastrabalhos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasprovastrabalhos', 'name' => 'Provas e Trabalhos'),
			array('url' => null,'name' => 'Visualizar Prova e Trabalho')
		);
		
		$id = (int)$this->_request->getParam("id");
		$secretariasprovastrabalhos = new Secretariasprovastrabalhos();
		$secretariasprovastrabalho = $secretariasprovastrabalhos->getSecretariasprovastrabalhoById($id, array());
		
		if (!$secretariasprovastrabalho) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $secretariasprovastrabalho;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Provas e Trabalhos')
		);
		
		$ns = new Zend_Session_Namespace('default_secretariasprovastrabalhos');
		$secretariasprovastrabalhos = new Secretariasprovastrabalhos();
		$queries = array();	
		$queries['idescola'] = Usuarios::getUsuario('idescola');
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
if ($this->view->post_var["idaluno"]!="") $queries["idaluno"] = $this->view->post_var["idaluno"];
if ($this->view->post_var["material"]!="") $queries["material"] = $this->view->post_var["material"];
if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $secretariasprovastrabalhos->getSecretariasprovastrabalhos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $secretariasprovastrabalhos->getSecretariasprovastrabalhos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de secretariasprovastrabalhos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasprovastrabalhos', 'name' => 'Provas e Trabalhos'),
			array('url' => null,'name' => 'Editar Prova e Trabalho')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$secretariasprovastrabalhos = new Secretariasprovastrabalhos();
		$secretariasprovastrabalho = $secretariasprovastrabalhos->getSecretariasprovastrabalhoById($id);
		
		if (!$secretariasprovastrabalho) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $secretariasprovastrabalho;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($secretariasprovastrabalho);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Prova e Trabalho editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de secretariasprovastrabalhos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasprovastrabalhos', 'name' => 'Provas e Trabalhos'),
			array('url' => null,'name' => 'Adicionar Prova e Trabalho')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Prova e Trabalho adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries['idescola'] = $idescola;

		$rows = new Escolasseries();
		$this->view->rows = $rows->getEscolasseries($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}	

	public function setturmasAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		
		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculos($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0)) $this->view->rows = null;
		
	}

	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");
		$idvinculo = (int)$this->_request->getPost("idvinculo");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		$queries['idvinculo'] = $idvinculo;
		
		$rows = new Escolassalasatribuicoes();
		$this->view->rows = $rows->getSalasatribuicoes($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0) || ((int)$idvinculo <= 0)) $this->view->rows = null;
		
	}

	public function setperiodoAction() {
		$this->_helper->layout->disableLayout();
		
		$idvinculo = (int)$this->_request->getPost("idvinculo");

		$queries = array();
		$queries['id'] = $idvinculo;
		
		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculos($queries);

		if((int)$idvinculo <= 0) $this->view->rows = null;
		
	}

	/*
	public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$idfuncionario = (int)$this->_request->getPost("idfuncionario");
		
		$rows = new Funcionariosgeraisescolas();
		$row = $rows->getFuncionariogeralescolaById($idfuncionario);
		
		if ($row) {
			echo json_encode(array('status' => 'OK', 'response' => $row));die();
		}
		
		echo json_encode(array('status' => 'ERROR', 'response' => 'Funcionário não encontrado!'));
		die();
	}	
	*/

    /**
     * Atribui valores ao view
     * @param int $idsecretariasprovastrabalho
     */    
    private function preForm($idsecretariasprovastrabalho = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_secretariasprovastrabalho = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$idlocal = (int)trim($this->getRequest()->getPost("idlocal"));
$idserie = (int)trim($this->getRequest()->getPost("idserie"));
$idescolavinculo = (int)trim($this->getRequest()->getPost("idescolavinculo"));
$idperiodo = (int)trim($this->getRequest()->getPost("idperiodo"));
$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
$material = trim($this->getRequest()->getPost("material"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$status = trim($this->getRequest()->getPost("status1"));
		
		$idsarquivos = $this->getRequest()->getPost("idsarquivos");
		$legendasarquivos =  $this->getRequest()->getPost("legendasarquivos");		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
if (0==$idserie) array_push($erros, "Informe a Série.");
if (0==$idescolavinculo) array_push($erros, "Informe a Série.");
if (0==$idperiodo) array_push($erros, "Informe a Período.");
if (0==$idaluno) array_push($erros, "Informe a Aluno.");
if (""==$material) array_push($erros, "Informe a Material.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$secretariasprovastrabalhos = new Secretariasprovastrabalhos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
if((int)$id <= 0 )			$dados["sequencial"] = $sequencial;
if((int)$id <= 0 )$dados["datalancamento"] = date("Y-m-d", $datalancamento);
if((int)$id <= 0 )$dados["horalancamento"] = $horalancamento;
$dados["idusuariologado"] = Usuarios::getUsuario('id');
$dados["idescola"] = $idescola;
$dados["idlocal"] = $idlocal;
$dados["idserie"] = $idserie;
$dados["idescolavinculo"] = $idescolavinculo;
$dados["idperiodo"] = $idperiodo;
$dados["idaluno"] = $idaluno;
$dados["material"] = $material;
$dados["descricoes"] = $descricoes;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $secretariasprovastrabalhos->save($dados);
			$multiplosarquivos = new Secretariasprovastrabalhosarquivos(); 
			$multiplosarquivos->setArquivos($row['id'], $idsarquivos, $legendasarquivos);

			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}