<?php

class MYPDF extends TCPDF {

    //Page header
    public function Header(){
        
        $image_file = "public/admin/imagens/logosantaisabel.jpg";
        $this->Image($image_file, 18, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
        //Set font
        $this->SetFont('helvetica', 'B', 20);
        //Title
        //$this->Cell(0, 15, '<< TCPDF Example 003 São Paulo >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', 'B', 10);
       
        // cabeçalho Endereço
        $this->SetXY(35, 12);
        $this->Cell(0, 0, 'Prefeitura Municipal de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 9);
        $this->SetXY(35, 16);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Secretaria de Educação - Setor de Recursos Humanos', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(35, 19);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetFont('helvetica', '', 7);
        $this->SetXY(35, 22);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
        $this->SetXY(35, 25);
        $this->SetTextColor(130, 130, 130);
        $this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
        // Position at 15 mm from bottom
        $this->SetXY(150,-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}
/**
 * Controle da classe pontosmensais do sistema
 *
 * @author      Alexandre Martin Narciso        
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PontosmensaisController extends Zend_Controller_Action {
    
    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pontosmensal
     */
    protected $_usuario = null;
       
        public $arrayStr = array();
        
    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
        Mn_Util::blockAccess("pontosmensais", $this->_request->getActionName());    
        
        $this->_usuario = unserialize($loginNameSpace->usuario);
        
        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->_request->getPost("id");
        
        $rows = new Pontosmensais();
        $row = $rows->fetchRow("id=".$id);
        
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');          
            
            $rows->save($row);
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Ponto mensal excluído com sucesso.";
            
            die("OK");
        }
        
        die("Não encontrado!");
    }       
    
    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction(){
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");
        
        if ($op=="pontosmensais") $objs = new Pontosmensais();
        $obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');
            
            $objs->save($obj);
            
            die($obj['status']);
        }
        
        die("Não encontrado!");
    }       
    
    
    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction(){
        $this->view->bread_crumb = array(
            array('url' => 'pontosmensais', 'name' => 'Pontos mensais'),
            array('url' => null,'name' => 'Visualizar Ponto mensal')
        );
        
        $id = (int)$this->_request->getParam("id");
        $pontosmensais = new Pontosmensais();
        $pontosmensal = $pontosmensais->getPontosmensalById($id, array());
        
        if (!$pontosmensal) 
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        
        $this->view->post_var = $pontosmensal;
        $this->preForm();
    
        $this->view->visualizar = true;
        return true;
    }


    public function indexAction(){

        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Pontos mensais')
        );

        /* =========================================================
                    Código de Meus Funcionários
        ========================================================== */

        $ns = new Zend_Session_Namespace('admin_pontosmensais');
		$funcionariosgerais = new Funcionariosgeraisescolas();
        $queries = array();	

        if ($this->getRequest()->isPost()){
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        $arraySetFuncionario = array();
        
        // Definindo parfil geral utilizado caso for RH ou se for usuário 
        if((Usuarios::getUsuario('idperfil') == '14') || (Usuarios::getUsuario('idperfil') == '1') || (Usuarios::getUsuario('idperfil') == '30')): 
            
            if(isset($this->view->post_var['idlocal'])): 
            // Caso seja um usuário RH ou ADM que esteja setado na variavel idlocal
                
                // Setando o usuário que será utilizado
                $usuario = Usuarios::getUsuario();

                // Setando a array com os valores a serem buscados pelo filtro

                $arr = explode('-', $this->view->post_var['idlocal']);
                
                $arraySetFuncionario['idnovo'] = $arr[3];
                $arraySetFuncionario['idfuncionario'] = $arr[2];
                $arraySetFuncionario['idlocal'] = $arr[1];
                $arraySetFuncionario['idperfil'] = $arr[0];

                $idperfil = (int)$arraySetFuncionario['idperfil'];
                $idescola = (int)$arraySetFuncionario['idlocal'];

            else: 
             // Caso seja um usuário tipo ADM ou RH e o form não esteja com uma escola específica setada mostrar os meusfuncionários próprios

                // Setando o usuário que será utilizado (Usuário ADM ou RH)

                $usuario = Usuarios::getUsuario();

                $idperfil = (int) $usuario['idperfil'];
                $idescola = (int) $usuario['idescola'];

                // echo 'perfil ' . $idperfil;
                // echo 'escola' . $idescola;
                
            endif;
            
        else: 
        // Caso seja um Usuário acessando sua área de folhas de ponto

            // Setando o usuário que será utilizado (Usuários em suas áreas de Pontos mensais)                                                                          
            
            $usuario = Usuarios::getUsuario();

            $idperfil = (int)$usuario['idperfil'];
            $idescola = (int)$usuario['idescola'];
            $arraySetFuncionario['idnovo'] = $usuario['id'];
            
        endif;
		
		$_usuarios = new Usuarios();

		$_funcionariogeral = $funcionariosgerais->getFuncionariogeralescolaById((int)Usuarios::getUsuario('idfuncionario'));

		$this->view->funcionariogeral = $_funcionariogeral;
		
        if((int)$idescola > 0){
            
            /*  
             * Situações para as três fases de filtro
             * [1] Usuário é de RH ou ADM e irá ver os usuários de outra escola
             * [2] Usuário é de RH ou ADM e irá ver apenas os seus funcionários
             * [3] Usuário está acessando sua área e irá verificar seus funcionários na folha de ponto
             */ 
            if((Usuarios::getUsuario('idperfil') == '14') || (Usuarios::getUsuario('idperfil') == '1') || (Usuarios::getUsuario('idperfil') == '30')): 
                if(isset($this->view->post_var['idlocal'])): 
                // Situação [1]
                    $idfuncionario = Usuarios::getUsuarioByIdHelper($arraySetFuncionario['idnovo'])['idfuncionario'];
                else:
                // Situação [2] 
                    $idfuncionario = Usuarios::getUsuario('idfuncionario');
                endif;
            else: 
            // Situação [3]
                $idfuncionario = Usuarios::getUsuario('idfuncionario');
                
            endif;
            //$idescola = '21';
           
            $meusfuncionarios = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaImplodeByIdescolaHelper($idfuncionario, $idescola, array());
            
          
            
            if((isset($meusfuncionarios)) && ($meusfuncionarios) && ($meusfuncionarios!="")){
                $queries['idsfuncionariossel'] = $meusfuncionarios;
                
            }else{
                $queries['idsfuncionariossel'] = '-1';
            }

        }elseif((int)$idperfil > 0){

           
             /*  
             * Situações para as três fases de filtro
             * [1] Usuário é de RH ou ADM e irá ver os usuários de outra escola
             * [2] Usuário é de RH ou ADM e irá ver apenas os seus funcionários
             * [3] Usuário está acessando sua área e irá verificar seus funcionários na folha de ponto
             */ 
            if((Usuarios::getUsuario('idperfil') == '14') || (Usuarios::getUsuario('idperfil') == '1') || (Usuarios::getUsuario('idperfil') == '30')): 
                if(isset($this->view->post_var['idlocal'])): 
                // Situação [1]
                    $idfuncionario =  Usuarios::getUsuarioByIdHelper($arraySetFuncionario['idnovo'])['idfuncionario'];
                else:
                // Situação [2] 
                    $idfuncionario = Usuarios::getUsuario('idfuncionario');
                endif;
            else: 
            // Situação [3]
                $idfuncionario = Usuarios::getUsuario('idfuncionario');
            endif;

            $idescola = '0';
            $meusfuncionarios = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaImplodeByIdperfilHelper($idfuncionario, $idperfil, array());
            //$meusfuncionarios = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaImplodeByIdperfilHelper($idfuncionario, $idescola, array());
            
            if((isset($meusfuncionarios)) && ($meusfuncionarios) && ($meusfuncionarios!="")){
                $queries['idsfuncionariossel'] = $meusfuncionarios;
                // echo 'id funcionario segundo x<br>';
            }else{
                $queries['idsfuncionariossel'] = '-1';
            }
            
        }
       
            /*  
             * Situações para as três fases de filtro
             * [1] Usuário é de RH ou ADM e irá ver os usuários de outra escola
             * [2] Usuário é de RH ou ADM e irá ver apenas os seus funcionários
             * [3] Usuário está acessando sua área e irá verificar seus funcionários na folha de ponto
             */ 
            if((Usuarios::getUsuario('idperfil') == '14') || (Usuarios::getUsuario('idperfil') == '1') || (Usuarios::getUsuario('idperfil') == '30')): 
                if(isset($this->view->post_var['idlocal'])): 
                // Situação [1]
                    $queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
                    
                else:
                // Situação [2] 
                    $queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
                   
                endif;
            else: 
            // Situação [3]
                $queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
                
            endif;


           
		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		//PESQUISA
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ((isset($this->view->post_var["nome"])) && ($this->view->post_var["nome"]!="")) $queries["nome"] = $this->view->post_var["nome"];
			if ((isset($this->view->post_var["telefone"])) && ($this->view->post_var["telefone"]!="")) $queries["telefone"] = $this->view->post_var["telefone"];
			// if ($this->view->post_var["tipo"]!="") $queries["tipo"] = $this->view->post_var["tipo"];
			if ((isset($this->view->post_var["status1"])) && ($this->view->post_var["status1"]!="")) $queries["status"] = $this->view->post_var["status1"];
           
    		//if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}	
			//$queries['die'] = true;	
		
		//unset($queries['idescola']);

		//PAGINACAO
    	$maxpp = 60;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $funcionariosgerais->getFuncionariosgeraisescolas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;  
        
        /* =========================================================
                   End Código de Meus Funcionários
        ========================================================== */

        // print_r($queries);
        
        // Setando as linhas de acordo com o tipo de usuário
        
       
        $this->view->rows = $funcionariosgerais->getFuncionariosgeraisescolas($queries, $paginaAtual, $maxpp);
        
        // Setando informações de bloco de períodos
        $dataInicial = date('Y') . '-' . date('m') . '-16';             // Valor do primeiro dia do mês vigente
        $dataFinal = date('Y') . '-' . ((int)date('m') + 1) . '-' . date('15');     // Valor do ultimo dia do mês vigente

        // Caso o filtro de blocos de períodos seja utilizado
        if($this->view->post_var['selectdados']):
            $datas = explode(' ',$this->view->post_var['selectdados']);
            $this->view->datainicial = $datas[0];
            $this->view->datafinal = $datas[1];
        else:
            $this->view->datainicial = $dataInicial;
            $this->view->datafinal = $dataFinal;
        endif;

        

        // Setando variaveis com as informações do local e do departamento de acordo com o usuário logado
        if((Usuarios::getUsuario('idperfil') == '14') || (Usuarios::getUsuario('idperfil') == '1') || (Usuarios::getUsuario('idperfil') == '30')):

            $user = new Usuarios();

            $idNovo = (isset($arraySetFuncionario['idnovo'])) ? $arraySetFuncionario['idnovo'] : '';

            $idUsuarioFuncionario = $user->getUsuarios(array('id'=>$idNovo))[0]['idfuncionario'];
           
            $local = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($idUsuarioFuncionario)['idlocal'];
            $idperfil = $user->getUsuarios(array('id'=>$idNovo))[0]['idperfil'];
            
            $this->view->idlocal =  $local;
            $this->view->dept =  $idperfil;
        else:
            $this->view->idlocal = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idlocal'];
            $this->view->dept = Usuarios::getUsuario('idperfil');
        endif;

        // Enviando usuário para a view
        $this->view->idusuario = (isset($arraySetFuncionario['idnovo'])) ? $arraySetFuncionario['idnovo'] : Usuarios::getUsuario('id')  ;
        
        // echo '<br>';
        // echo 'end';
        // die();

    }//end public function 

   
    
    /**
     * 
     * Action de edição de pontosmensais
     */ 
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'pontosmensais', 'name' => 'Pontos mensais'),
            array('url' => null,'name' => 'Editar Ponto mensal')
        );
        
        // Recuperando dparametros passados na URL e realizando tratamento
        $id = (int)$this->_request->getParam("id");
        $iduser = (int)$this->_request->getParam("iduser");

        /* ------------------ Buscando o horário setado do funcionário ---------*/
        $tabeladeHorarios = new Funcionariosgeraispontohorariosfuncionario();
        $horariosDescricoes = new Funcionariosgeraispontodefinicaohorarios();


        $queries = array();
       
        $querie['fkfuncionario'] = $id;
        $querie['idlocalfuncionario'] = $this->_request->getParam('local');
        $querie['iddepstfuncionario'] = $this->_request->getParam('dept');
        
        if($tabeladeHorarios->getFuncionariosgeraispontohorariosfuncionarios($querie)){
            $horarioFuncionario = $tabeladeHorarios->getFuncionariosgeraispontohorariosfuncionarios($querie)[0]['fkfuncionariosgeraispontodefinicaohorarios'];
        }else{
            $horarioFuncionario = 1;
        }

        $horariosDescricoes = new Funcionariosgeraispontodefinicaohorarios();

        $horarioDescricao = $horariosDescricoes->getFuncionariosgeraispontodefinicaohorarios($queries);


        /* ------------------ Buscando o horário setado do funcionário ---------*/


        $dateObjInicio =  date("d-m-Y", strtotime($this->_request->getParam("inicio")));
        $urlDateInicio = new DateTime($dateObjInicio); 

       
        $dateObjFim =  date("d-m-Y", strtotime($this->_request->getParam("fim")));
        $urlDateFim = new DateTime($dateObjFim); 

        //$pontosmensais = new Funcionariosgeraisescolas();
        //$pontosmensal = $pontosmensais->getFuncionariogeralescolaById($id);
                
        $funcionario = new Funcionariosgeraisescolas();
        $informacoesFuncionario = $funcionario->getFuncionariogeralescolaById($id);
        
        // $perfis = explode(",",$informacoesFuncionario['idsperfis']);
        // $horarios = explode(",",$informacoesFuncionario['idshorarios']);

       
        
        if (!$funcionario) 
            $this->_redirect('admin'.'/'.$this->getRequest()->getControllerName());
        $this->preForm();
        //$this->view->post_var["nomex"] = "aaa";
        if ($this->_request->isPost()) {
            $erros = $this->getPost($pontosmensal);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Ponto mensal editado com sucesso.";
                        
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }
        
        
              
                $this->view->funcionario = $informacoesFuncionario;

                $this->view->idPost = $this->_request->getParam('id');
                $this->view->idPostUser = $iduser;
                $this->view->idlocal = $this->_request->getParam('local');
                $this->view->departamento = $this->_request->getParam('dept');

               
                $this->view->urlInicio = $urlDateInicio;
                $this->view->urlFim = $urlDateFim;

                $funcionariohorario = new Funcionariosgeraispontohorariosfuncionario();
                $rows = $funcionariohorario->getFuncionariosgeraispontohorariosfuncionarios($querie);
            
                $querie = array();
                $querie['fkfuncionario'] = $id;
                $querie['idlocalfuncionario'] = $this->_request->getParam('local');
                $queries['iddepstfuncionario'] = $this->_request->getParam('dept');

                $rows = $funcionariohorario->getFuncionariosgeraispontohorariosfuncionarios($querie);
                
                $horarios = new Funcionariosgeraispontodefinicaohorarios();

                if($rows){
                    $rowhorario = $horarios->getFuncionariosgeraispontodefinicaohorariosById($rows[0]['fkfuncionariosgeraispontodefinicaohorarios']);
                }else{
                    $rowhorario = $horarios->getFuncionariosgeraispontodefinicaohorariosById(1);
                }
                

                $this->view->descricaohorario = $horarioDescricao[0];
                $this->view->descricaohorario = "oi";
                $this->view->funcionarioHorariosDefinidos = $rowhorario;
                
                return true;        
    }       
    
    /**
     * 
     * Action de adição de pontosmensais 
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'pontosmensais', 'name' => 'Pontos mensais'),
            array('url' => null,'name' => 'Adicionar Ponto mensal')
        );  
                
        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
                        
                        
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Ponto mensal adicionado com sucesso.";
            
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }
        
        return true;        
    }   
    
    /**
     * Atribui valores ao view
     * @param int $idpontosmensal
     */    
    private function preForm($idpontosmensal = 0) {
    }    
    
    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_pontosmensal = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);
        
        $id = (int)$this->getRequest()->getPost("id");
        $idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
$dia = Mn_Util::stringToTime($this->getRequest()->getPost("dia"));
$diasemana = trim($this->getRequest()->getPost("diasemana"));
$horaentrada = trim($this->getRequest()->getPost("horaentrada"));
$horasaida = trim($this->getRequest()->getPost("horasaida"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$status = trim($this->getRequest()->getPost("status1"));
        
        
        $erros = array();
        
        if (0==$idfuncionario) array_push($erros, "Informe a Funcionário.");
if (""==$dia) array_push($erros, "Informe a Dia.");
if (""==$diasemana) array_push($erros, "Informe a Dia da semana.");
if (""==$status) array_push($erros, "Informe a Status.");

        
        $pontosmensais = new Pontosmensais();
        
        
        
        if (sizeof($erros)>0) return $erros; 
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;
            $dados['origem'] = "Secretaria";
            $dados["idfuncionario"] = $idfuncionario;
            $dados["dia"] = date("Y-m-d", $dia);
            $dados["diasemana"] = $diasemana;
            $dados["horaentrada"] = $horaentrada;
            $dados["horasaida"] = $horasaida;
            $dados["observacoes"] = $observacoes;
            $dados["status"] = $status;

            
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            $dados['logdata'] = date('Y-m-d G:i:s');
                    
            $row = $pontosmensais->save($dados);
            
            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            
            $db->rollBack();
            die();
        }       
        
        return "";      
    }
    
   
         /**
     * 
     * Action para ser consultada via  e excluir a entidade
     */
    public function envioajaxAction(){
                $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
                
                if(isset($_POST["valor"])){
                    $valor = $_POST["valor"];
                }
                
                if(isset($_POST["iduser"])){
                    $iduser = $_POST["iduser"];
                }
                if(isset($_POST["idlocalfuncionario"])){
                    $idlocalfuncionario = $_POST["idlocalfuncionario"];
                }
                if(isset($_POST["iddeptfuncionario"])){
                    $iddeptfuncionario = $_POST["iddeptfuncionario"];
                }
                if(isset($_POST["dia_i"])){
                    $dia_i = $_POST["dia_i"];
                }
                
                if(isset($_POST["dia_f"])){
                    $dia_f = $_POST["dia_f"];
                }
                
                if(isset($_POST["id"])){
                    $id = $_POST["id"];
                }

                if(isset($_POST["horariodefinido"])){
                    $horariodefinido = $_POST["horariodefinido"];
                }

                if(isset($_POST["idhorariodefinido"])){
                    $idhorariodefinido = $_POST["idhorariodefinido"];
                }

                $inicio = "30-04-2018";
                $fim = "2018-05-30";
                
                $dateObjInicio =  date("d-m-Y", strtotime($dia_i));
                $brDateInicio = new DateTime($dateObjInicio);
                
                $dateObjFim =  date('d-m-Y', strtotime($dia_f));
                $brDateFim = new DateTime($dateObjFim);
                
                $interval = new DateInterval('P1D');

                $endOne = new DateTime($dateObjFim);
                $endOne->modify('+1 day');

                $period = new DatePeriod($brDateInicio, $interval, $endOne);
                
                $contador = 0;
                
                
                /*--------------------------------------------------------------
                 *      Setando Valores no Select de Ocorrências
                 --------------------------------------------------------------*/
                $ocorrenciasList = new Funcionariosgeraispontoocorrencias();
                $queries = array();
                $queries['status'] = "Ativo";
                $paginaAtual = 0;
                $maxpp = 10;
                    
                $row = $ocorrenciasList->getAtas($queries);
                
                // Bloco destinado à montagem das ocorrências no select para a escolha do usuário
                
                $strOcorrenciasSelectBlock = "<option value='Dia Trabalhado'>Dia Trabalhado</option>";
                
                foreach($row as $index):
                    $strOcorrenciasSelectBlock .= "<option value='" . $index['ocorrencia'] . "'>" . $index['ocorrencia'] . "</option>";
                endforeach;
                
                //$strOcorrenciasSelectBlock .= "</select>";
                
                /* -------------------------------------------------------------
                 *            Setando valores default de entrada saida e pausas
                 *-------------------------------------------------------------*/
                
              $FuncionariosGeraisGrupos = new Funcionariosgeraispontogrupos();
              $queries = array();
              $queries['id'] = $id;
              $rowFuncionariosGerais = $FuncionariosGeraisGrupos->getAtas($queries, $paginaAtual, $maxpp);

               foreach($rowFuncionariosGerais as $row):
                    $linha = $row;
               endforeach;
               
              // Buscando valores na tabela auxiliar contanto os Blocos de entrada e saida 
              $array = array();
              $array['id'] = 0;
              
              $rowHorarios = Funcionariosgeraispontodefinicaohorarios::getFuncionariosgeraispontodefinicaohorariosHelper($array);
              
              $funcionariohorario = new Funcionariosgeraispontohorariosfuncionario();

              $querie = array();
              $querie['fkfuncionario'] = $id;
              $querie['idlocalfuncionario'] = $idlocalfuncionario;
              $queries['iddepstfuncionario'] = $iddeptfuncionario;
              $rows = $funcionariohorario->getFuncionariosgeraispontohorariosfuncionarios($querie);

              $horarios = new Funcionariosgeraispontodefinicaohorarios();

              if($rows):
                $rowhorario = $horarios->getFuncionariosgeraispontodefinicaohorariosById($rows[0]['fkfuncionariosgeraispontodefinicaohorarios']);
              else: 
                // Caso não tenha horário algum atribuido, atribuir o horário padrão de id 1
                $rowhorario = $horarios->getFuncionariosgeraispontodefinicaohorariosById(1);
              endif;
             
              //$rowhorario = $horarios->getFuncionariosgeraispontodefinicaohorariosById('17');
             
              
              /* ============================================================================
                                            Tratamento de Férias
              ============================================================================== */
                
                $queriesFerias = array();
                $queriesFerias['idfuncionarioferias'] = $id;
                $queriesFerias['progresso'] = 'aprovado';

                $objsFerias = Ferias::getFeriasHelper($queriesFerias);

                $contadorFerias = 0;
                
                foreach($objsFerias as $index):

                    if( (((date('Y-m-d' , strtotime($index['datainicio'])) >= date('Y-m-d',strtotime($brDateInicio->format('Y-m-d')))) and  (date('Y-m-d' , strtotime($index['datainicio'])) <= date('Y-m-d',strtotime($brDateFim->format('Y-m-d'))))) or ((date('Y-m-d' , strtotime($index['datafim'])) >= date('Y-m-d',strtotime($brDateInicio->format('Y-m-d')))) and  (date('Y-m-d' , strtotime($index['datafim'])) <= date('Y-m-d',strtotime($brDateFim->format('Y-m-d')))))) and ($index['progresso'] == 'aprovado') ){
                        $objFerias = $index;
                        $contadorFerias++;     
                    }//endif
                   
                endforeach;
                
              /* ============================================================================
                                           End Tratamento de Férias
              ============================================================================== */

               /* ============================================================================
                                            Tratamento de Afastamentos
              ============================================================================== */
                
              $queriesAfastamentos = array();
              $queriesAfastamentos['idfuncionarioafastadolicenca'] = $id;

              $objAfastamentoslicencas = Rhafastamentoslicencas::getRhafastamentoslicencasHelper($queriesAfastamentos);
             
              
              $contadorAfastamentos = 0;
              

              foreach($objAfastamentoslicencas as $index):

                  if( (((date('Y-m-d' , strtotime($index['datainicio'])) >= date('Y-m-d',strtotime($brDateInicio->format('Y-m-d')))) and  (date('Y-m-d' , strtotime($index['datainicio'])) <= date('Y-m-d',strtotime($brDateFim->format('Y-m-d'))))) or ((date('Y-m-d' , strtotime($index['datafim'])) >= date('Y-m-d',strtotime($brDateInicio->format('Y-m-d')))) and  (date('Y-m-d' , strtotime($index['datafim'])) <= date('Y-m-d',strtotime($brDateFim->format('Y-m-d')))))) and ($index['progresso'] == 'aprovado') ){
                      $objAfastamento = $index;
                      $contadorAfastamentos++;
                  }//endif
                 
              endforeach;

              
             

          
            /* ============================================================================
                                         End Tratamento de Afastamentos
            ============================================================================== */

            
              
            // Setando valores do Bloco de horários para apresentação em tabela
              
           
           
           
            if(isset($rowhorario['hora_entrada'])): 
                $dateObj =  date('H:i:s', strtotime($rowhorario['hora_entrada']));
                $Dateinicio = new DateTime($dateObj);
                $valorEntrada = $Dateinicio->format('H:i');
            else: 
                $dateObj =  date('H:i:s', strtotime('00:00:00'));
                $Dateinicio = new DateTime($dateObj);
                $valorEntrada = $Dateinicio->format('H:i');
            endif;
           
            if(isset($rowhorario['hora_saida'])): 
                $dateObj =  date('H:i:s', strtotime($rowhorario['hora_saida']));
                $Datefim = new DateTime($dateObj);
                $valorSaida = $Datefim->format('H:i');
            else: 
                $dateObj =  date('H:i:s', strtotime('00:00:00'));
                $Datefim = new DateTime($dateObj);
                $valorSaida = $Datefim->format('H:i');
            endif;
           
            if(isset($rowhorario['hora_pausa_entrada'])): 
                $dateObj =  date('H:i:s', strtotime($rowhorario['hora_pausa_entrada']));
                $Datepausainicio = new DateTime($dateObj);
                $valorPausaEntrada = $Datepausainicio->format('H:i');
            else: 
                $dateObj =  date('H:i:s', strtotime('00:00:00'));
                $Datepausainicio = new DateTime($dateObj);
                $valorPausaEntrada = $Datepausainicio->format('H:i');
            endif;

            if(isset($rowhorario['hora_pausa_saida'])): 
                $dateObj =  date('H:i:s', strtotime($rowhorario['hora_pausa_saida']));
                $Datepausafim = new DateTime($dateObj);
                $valorPausaSaida = $Datepausafim->format('H:i');
            else: 
                $dateObj =  date('H:i:s', strtotime('00:00:00'));
                $Datepausafim = new DateTime($dateObj);
                $valorPausaSaida = $Datepausafim->format('H:i');
            endif;
              
            
                /* -------------------------------------------------------------
                 *            Cabeçalho da table que irá retornar no ajax
                 *-------------------------------------------------------------*/
                
                $masterUser = '1'; // Recuperando informações do usuário global para liberação de funcionalisades
                $rhUser = '14'; // Reciperando informações dos usuários responsáveis pelo setor de RG para liberação de funcionalidades
                
                echo "<tr>";
                    echo "<th></th>";
                    echo "<th>Data</th>";
                    echo "<th>Entrada</th>";
                    echo "<th>Saida</th>";
                    echo "<th>Entrada Pausa</th>";
                    echo "<th>Saida Pausa</th>";
                    echo "<th></th>";
                    echo "<th></th>";
                echo '<tr>';
                
                // if(Usuarios::getUsuario('idperfil') == $rhUser || Usuarios::getUsuario('idperfil') == $masterUser):
                //     echo "<th></th>";
                // else:
                //     echo "<th><div class='btn btn-info btn-block' style='cursor: pointer' onclick='confirmarTodos()' >Confirmar Todos </div></th>";
                // endif;
                // echo '<th><button onclick="window.history.back()" class="btn btn-danger cancelar" type="button">Voltar</button></th>';
               
                
                /* -------------------------------------------------------------
                 *            Conteúdo da table que irá retornar no ajax
                  -------------------------------------------------------------*/
                
                 
                /*
                 *  Linhas terão nomeclaturas diferenciadas para cada caso
                 *  LinhaS-  -> Definido para finais de semana encontrados
                 *  LinhaF-  -> Definido para Feriados Cadastrados no Banco de Dados
                 *  LinhaC-  -> Definido para Lançamentos já cadastrados no Banco de Dados
                 */


                // print_r($objAfastamento); 
                // echo '<br>'; 
                // echo $contadorAfastamentos . '<br>';

                // echo 'Inicio : ' . $objAfastamento['datainicio'] . '<br>';
                // echo 'Fim : ' . $objAfastamento['datafim'] . '<br>';

                // foreach($period as $date):

                //     echo $date->format('Y-m-d') . ' - ' . $objAfastamento['datainicio'] .  '<br>';

                //     if ( $date->format('Y-m-d') > (date('Y-m-d', strtotime($objAfastamento['datainicio'])))): 
                //         echo 'chegou <br>';
                //     endif;

                // endforeach;


                // die();
                // instanciando classe feriados - Model
                $objFeriado = new Feriados();

                $counter = 0; // Controle de Ids para requisição ajax

               
              
                foreach($period as $date):
                    
                    if($date->format("w") == "0" || $date->format('w') == "6"):

                        $userId = (isset($iduser)) ? $iduser : Usuarios::getUsuario('id');

                        if(Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"), $id, $userId, $idlocalfuncionario, $iddeptfuncionario) > 0){
                            
                            $row = Funcionariosgeraispontohora::getRetornaRegistroData($id, $userId, $idlocalfuncionario, $iddeptfuncionario, $date->format('Y-m-d'));

                            // tratamento horário de entrada
                            $dateObj =  date('Y-m-d H:i:s', strtotime($row['entrada_pontohorarios']));
                            $Dateentrada = new DateTime($dateObj);

                            // tratamento horário de saida
                            $dateObj =  date('Y-m-d H:i:s', strtotime($row['saida_pontohorarios']));
                            $Datesaida = new DateTime($dateObj);

                            // tratamento horário de entrada pausa
                            $dateObj =  date('Y-m-d H:i:s', strtotime($row['entradapausa_pontohorarios']));
                            $Datepausaentrada = new DateTime($dateObj);

                            // tratamento horário de saida pausa
                            $dateObj =  date('Y-m-d H:i:s', strtotime($row['saidapausa_pontohorarios']));
                            $Datepausasaida = new DateTime($dateObj);

                            $strNucleoLinhas = "<td><input id='entrada-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Dateentrada->format('H:i') . "' disabled></td>";
                            $strNucleoLinhas .= "<td><input id='saida-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datesaida->format('H:i') . "' disabled></td>";
                            $strNucleoLinhas .= "<td><input id='entradapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausaentrada->format('H:i') . "'disabled></td>"; 
                            $strNucleoLinhas .= "<td><input id='saidapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausasaida->format('H:i') . "' disabled></td>";
                            $strNucleoLinhas .= "<td><h5 id='obspont-$counter'>" . $row['titulo_pontohorarios'] .  "</h5></td>";

                            // verificar se o dia já consta cadastrado no banco de dados
                            if((Usuarios::getUsuario('idperfil') == $rhUser) || (Usuarios::getUsuario('idperfil') == $masterUser)):

                                echo "<tr id='linhac-$counter' class='success'><td id='day-$counter'>" /*Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"),(int)$id)*/  .  Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td>" . $strNucleoLinhas . "<td> </td></tr>";
                                
                            else:
                               
                                if($row['status_pontohorarios'] == 'ok'):
                                    echo "<tr id='linhac-$counter' class='success'><td id='day-$counter'>" /*Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"),(int)$id)*/  .  Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td>" . $strNucleoLinhas . "<td><div class='btn btn-block btn-success' disabled> Confirmado </div> </td></tr>";
                                else:
                                    $strNucleoLin = "<td><input id='entrada-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" .  $Dateentrada->format('H:i')  . "'></td>";
                                    $strNucleoLin .= "<td><input id='saida-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datesaida->format('H:i') . "'></td>";
                                    $strNucleoLin .= "<td><input id='entradapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausaentrada->format('H:i') . "'></td>"; 
                                    $strNucleoLin .= "<td><input id='saidapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausasaida->format('H:i') . "'></td>";
                                    $strNucleoLin .= "<td><div></div><select id='obspont-$counter' class='select' ><option value='" . $row['titulo_pontohorarios'] . "'>" . $row['titulo_pontohorarios'] . "</option>" . $strOcorrenciasSelectBlock . "</select></td>";
                                    
                                    if((Usuarios::getUsuario('idperfil') == $rhUser) || (Usuarios::getUsuario('idperfil') == $masterUser) || (Usuarios::getUsuario('idperfil') == '30')):
                                        echo "<tr id='linhac-$counter' class='success'><td id='day-$counter'>". /*Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"),(int)$id)*/    Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td>" . $strNucleoLin . "<td  id='editar-$counter' ><div class='btn btn-block btn-info' style='cursor: pointer' onclick='editarponto(" . $counter . ")' disabled> Editar x1</div> </td></tr>";
                                    else:
                                        $strNucleoLinEditar = "<td><input id='entrada-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" .  $Dateentrada->format('H:i')  . "' disabled></td>";
                                        $strNucleoLinEditar .= "<td><input id='saida-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datesaida->format('H:i') . "' disabled></td>";
                                        $strNucleoLinEditar .= "<td><input id='entradapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausaentrada->format('H:i') . "' disabled></td>"; 
                                        $strNucleoLinEditar .= "<td><input id='saidapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausasaida->format('H:i') . "' disabled></td>";
                                        $strNucleoLinEditar .= "<td><div></div><select id='obspont-$counter' class='select' disabled><option value='" . $row['titulo_pontohorarios'] . "'>" . $row['titulo_pontohorarios'] . "</option>" . $strOcorrenciasSelectBlock . "</select></td>";

                                        if($row['status_pontohorarios'] == 'edit'):
                                            echo "<tr id='linhac-$counter' class='success'><td id='day-$counter'> ". /*Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"),(int)$id)*/    Mn_Util::getDiaSemana($date->format('w')) . " <span class='badge badge-primary'>E</span></td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td>" . $strNucleoLinEditar . "<td  id='editar-$counter' ><div class='btn btn-block btn-info' style='cursor: pointer' onclick='habilitaedicao(" . $counter . ")'> Editar </div> </td></tr>";
                                        else: 
                                            echo "<tr id='linhac-$counter' class='success'><td id='day-$counter'>". /*Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"),(int)$id)*/    Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td>" . $strNucleoLinEditar . "<td  id='editar-$counter' ><div class='btn btn-block btn-info' style='cursor: pointer' onclick='habilitaedicao(" . $counter . ")'> Editar x</div> </td></tr>";
                                        endif;

                                    endif;
                                endif;
                            endif;
                            
                            //echo "<tr id='linhaS-$counter' style='color: #e2e3f0'><td>" . Mn_Util::getDiaSemana($date->format('w')) .  "</td><td>" . $date->format('d-m-Y') . "</td><td>--:--</td><td>--:--</td><td>--:--</td><td>--:--</td><td>----</td><td><div class='btn btn-danger btn-block' style='cursor: pointer' onclick='weekendworking(" . $counter .  ")'>Incluir Lançamento </div></td></tr>";
                        }else{

                            if((Usuarios::getUsuario('idperfil') == $rhUser) || (Usuarios::getUsuario('idperfil') == $masterUser || (Usuarios::getUsuario('idperfil') == '30'))){
                                echo "<tr id='linhaS-$counter' class='warning' style='color: #e2e3f0'><td>" . Mn_Util::getDiaSemana($date->format('w')) .  "</td><td>" . $date->format('d-m-Y') . "</td><td>--:--</td><td>--:--</td><td>--:--</td><td>--:--</td><td></td><td></td></tr>";
                            }else{
                                echo "<tr id='linhaS-$counter' class='warning' style='color: #e2e3f0'><td>" . Mn_Util::getDiaSemana($date->format('w')) .  "</td><td>" . $date->format('d-m-Y') . "</td><td>--:--</td><td>--:--</td><td>--:--</td><td>--:--</td><td><select id='obs-" . $counter . "'  class='select'>" . $strOcorrenciasSelectBlock . "</select></td><td><div class='btn btn-danger btn-block' style='cursor: pointer' onclick='weekendworking(" . $counter .  ")'>Incluir Lançamento </div></td></tr>";
                            }//end if else
                            
                        }//end if/else
                        

                    //elseif( (date('Y-m-d',strtotime($date->format('Y-m-d'))) >= date('Y-m-d' , strtotime($date->format('2018-08-24')))) and (date('Y-m-d',strtotime($date->format('Y-m-d'))) <= date('Y-m-d' , strtotime($date->format('2018-09-10')))) ):
                    
                    elseif(($contadorFerias > 0) and (((date('Y-m-d' , strtotime($date->format('Y-m-d')))) >= (date('Y-m-d' , strtotime($objFerias['datainicio'])))) and ((date('Y-m-d' , strtotime($date->format('Y-m-d')))) < (date('Y-m-d' , strtotime($objFerias['datafim']))))) ):
                         echo "<tr id='linhaFerias-$counter' class='info' style='color: #4d79ff'><td>" . Mn_Util::getDiaSemana($date->format('w')) . "</td><td>" . $date->format('d-m-Y') . "</td><td><i class='icon-ok-sign'></i></td><td colspan='4'> Férias Confirmadas</td><td></td></tr>";
                    else:
                        if((Feriados::getVerificaFeriado($date->format("Y-m-d")) > 0 && (Feriados::getInformacoesFeriadoData($date->format("Y-m-d"))['status'] == 'Ativo') && (Feriados::getInformacoesFeriadoData($date->format("Y-m-d"))['excluido'] == 'nao')) and (Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"), $id, $userId, $idlocalfuncionario, $iddeptfuncionario) == 0)):
                            $row = Feriados::getInformacoesFeriadoData($date->format("Y-m-d"));
                            // verificar se é um feriado cadastrado na tabela [funcionariosgeraispontohorarios]   
                            //echo "<tr id='linhaS-$counter' class='info'><td>" . Mn_Util::getDiaSemana($date->format('w')) . "</td><td>" . $date->format('d-m-Y') . "</td><td colspan='3'>  &nbsp; &nbsp; " . $row["nome_pontoferiado"] . "</td><td></td><td>" . $row['status'] . "</td><td><div class='btn btn-danger btn-block' style='cursor: pointer' onclick='weekendworking(" . $counter .  ")'>Incluir Lançamento </div></td></tr>"; 
                            
                        
                            if((Usuarios::getUsuario('idperfil') == $rhUser) || (Usuarios::getUsuario('idperfil') == $masterUser) || (Usuarios::getUsuario('idperfil') == '30')):
                                echo "<tr id='linhaS-$counter' class='info' style='color: gray'><td>" . Mn_Util::getDiaSemana($date->format('w')) .  "</td><td>" . $date->format('d-m-Y') . "</td><td>" .  $row["nome_pontoferiado"]  . "</td><td>" . $row['status'] . "</td><td></td><td></td><td></td><td></td></tr>";
                            else: 
                                echo "<tr id='linhaS-$counter' class='info' style='color: gray'><td>" . Mn_Util::getDiaSemana($date->format('w')) .  "</td><td>" . $date->format('d-m-Y') . "</td><td>" .  $row["nome_pontoferiado"]  . "</td><td>" . $row['status'] . "</td><td></td><td></td><td><select id='obs-" . $counter . "' class='select'>" . $strOcorrenciasSelectBlock . "</select></td><td><div class='btn btn-danger btn-block' style='cursor: pointer' onclick='weekendworking(" . $counter .  ")'>Incluir Lançamento </div></td></tr>";
                            endif;
                        
                        else:
                            
                            $userId = (isset($iduser)) ? $iduser : Usuarios::getUsuario('id');
                        
                        
                            if(Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"), $id, $userId, $idlocalfuncionario, $iddeptfuncionario) > 0): 
                                
                                $row = Funcionariosgeraispontohora::getRetornaRegistroData($id, $userId, $idlocalfuncionario, $iddeptfuncionario, $date->format('Y-m-d'));

                                // tratamento horário de entrada
                                $dateObj =  date('Y-m-d H:i:s', strtotime($row['entrada_pontohorarios']));
                                $Dateentrada = new DateTime($dateObj);

                                // tratamento horário de saida
                                $dateObj =  date('Y-m-d H:i:s', strtotime($row['saida_pontohorarios']));
                                $Datesaida = new DateTime($dateObj);

                                // tratamento horário de entrada pausa
                                $dateObj =  date('Y-m-d H:i:s', strtotime($row['entradapausa_pontohorarios']));
                                $Datepausaentrada = new DateTime($dateObj);

                                // tratamento horário de saida pausa
                                $dateObj =  date('Y-m-d H:i:s', strtotime($row['saidapausa_pontohorarios']));
                                $Datepausasaida = new DateTime($dateObj);

                                $strNucleoLinhas = "<td><input id='entrada-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Dateentrada->format('H:i') . "' disabled></td>";
                                $strNucleoLinhas .= "<td><input id='saida-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datesaida->format('H:i') . "' disabled></td>";
                                $strNucleoLinhas .= "<td><input id='entradapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausaentrada->format('H:i') . "'disabled></td>"; 
                                $strNucleoLinhas .= "<td><input id='saidapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausasaida->format('H:i') . "' disabled></td>";
                                $strNucleoLinhas .= "<td><h5 id='obspont-$counter'>" . $row['titulo_pontohorarios'] .  "</h5></td>";

                                // verificar se o dia já consta cadastrado no banco de dados
                                if((Usuarios::getUsuario('idperfil') == $rhUser) || (Usuarios::getUsuario('idperfil') == $masterUser) || (Usuarios::getUsuario('idperfil') == '30')):

                                    echo "<tr id='linhac-$counter' class='success'><td id='day-$counter'>" /*Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"),(int)$id)*/  .  Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td>" . $strNucleoLinhas . "<td> </td></tr>";
                                    
                                else:
                                   
                                    if($row['status_pontohorarios'] == 'ok'):
                                        echo "<tr id='linhac-$counter' class='success'><td id='day-$counter'>" /*Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"),(int)$id)*/  .  Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td>" . $strNucleoLinhas . "<td><div class='btn btn-block btn-success' disabled> Confirmado </div> </td></tr>";
                                    else:
                                        $strNucleoLin = "<td><input id='entrada-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" .  $Dateentrada->format('H:i')  . "'></td>";
                                        $strNucleoLin .= "<td><input id='saida-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datesaida->format('H:i') . "'></td>";
                                        $strNucleoLin .= "<td><input id='entradapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausaentrada->format('H:i') . "'></td>"; 
                                        $strNucleoLin .= "<td><input id='saidapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausasaida->format('H:i') . "'></td>";
                                        $strNucleoLin .= "<td><div></div><select id='obspont-$counter' class='select' ><option value='" . $row['titulo_pontohorarios'] . "'>" . $row['titulo_pontohorarios'] . "</option>" . $strOcorrenciasSelectBlock . "</select></td>";
                                        
                                        if((Usuarios::getUsuario('idperfil') == $rhUser) || (Usuarios::getUsuario('idperfil') == $masterUser)):
                                            echo "<tr id='linhac-$counter' class='success'><td id='day-$counter'>". /*Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"),(int)$id)*/    Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td>" . $strNucleoLin . "<td  id='editar-$counter' ><div class='btn btn-block btn-info' style='cursor: pointer' onclick='editarponto(" . $counter . ")' disabled> Editar x1</div> </td></tr>";
                                        else:
                                            $strNucleoLinEditar = "<td><input id='entrada-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" .  $Dateentrada->format('H:i')  . "' disabled></td>";
                                            $strNucleoLinEditar .= "<td><input id='saida-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datesaida->format('H:i') . "' disabled></td>";
                                            $strNucleoLinEditar .= "<td><input id='entradapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausaentrada->format('H:i') . "' disabled></td>"; 
                                            $strNucleoLinEditar .= "<td><input id='saidapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='" . $Datepausasaida->format('H:i') . "' disabled></td>";
                                            $strNucleoLinEditar .= "<td><div></div><select id='obspont-$counter' class='select' disabled><option value='" . $row['titulo_pontohorarios'] . "'>" . $row['titulo_pontohorarios'] . "</option>" . $strOcorrenciasSelectBlock . "</select></td>";

                                            if($row['status_pontohorarios'] == 'edit'):
                                                echo "<tr id='linhac-$counter' class='success'><td id='day-$counter'> ". /*Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"),(int)$id)*/    Mn_Util::getDiaSemana($date->format('w')) . " <span class='badge badge-primary'>E</span></td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td>" . $strNucleoLinEditar . "<td  id='editar-$counter' ><div class='btn btn-block btn-info' style='cursor: pointer' onclick='habilitaedicao(" . $counter . ")'> Editar </div> </td></tr>";
                                            else: 
                                                echo "<tr id='linhac-$counter' class='success'><td id='day-$counter'>". /*Funcionariosgeraispontohora::getVerificaRegistro($date->format("Y-m-d"),(int)$id)*/    Mn_Util::getDiaSemana($date->format('w')) . "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td>" . $strNucleoLinEditar . "<td  id='editar-$counter' ><div class='btn btn-block btn-info' style='cursor: pointer' onclick='habilitaedicao(" . $counter . ")'> Editar </div> </td></tr>";
                                            endif;
                                        endif;
                                    endif;

                                endif;
                                
                                
                             else:
                                $id = $this->_request->getParam("id");
                                if((Usuarios::getUsuario('idperfil') == $rhUser) || (Usuarios::getUsuario('idperfil') == $masterUser) || (Usuarios::getUsuario('idperfil') == '30')):
                                    echo "<tr id='linha-$counter'><td id='day-$counter'><input id='idhorario-$counter' type='hidden' value='$idhorariodefinido'><input id='nomehorario-$counter' type='hidden' value='$horariodefinido'>" .  Mn_Util::getDiaSemana($date->format('w')) .  "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td><td ><input id='entrada-$counter' type='time' class='span3'  required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$' value='$valorEntrada'></td><td><input id='saida-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$' value='$valorSaida'></td><td><input id='entradapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$' value='$valorPausaEntrada'></td><td><input id='saidapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='$valorPausaSaida'></td><td><div class='controls'><select id='obs-$counter' class='select' >" . $strOcorrenciasSelectBlock . "</select></div></td><td><div class='btn btn-info btn-block' style='cursor: pointer'  disabled>Cadastrar </div></td></tr>";
                                else:
                                    echo "<tr id='linha-$counter'><td id='day-$counter'><input id='idhorario-$counter' type='hidden' value='$idhorariodefinido'><input id='nomehorario-$counter' type='hidden' value='$horariodefinido'>" .  Mn_Util::getDiaSemana($date->format('w')) .  "</td><td id='data-$counter'>" . $date->format('d-m-Y') . "</td><td ><input id='entrada-$counter' type='time' class='span3'  required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$' value='$valorEntrada'></td><td><input id='saida-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$' value='$valorSaida'></td><td><input id='entradapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$' value='$valorPausaEntrada'></td><td><input id='saidapausa-$counter' type='time' class='span3' required='required' maxlength='8' name='hour' pattern='[0-9]{2}:[0-9]{2} [0-9]{2}$'  value='$valorPausaSaida'></td><td><select id='obs-$counter' class='select' >" . $strOcorrenciasSelectBlock . "</select></td><td><div class='btn btn-info btn-block' style='cursor: pointer' onclick='cadastrarpontoindividual(" . $counter . ")' >Cadastrar </div></td></tr>";
                                endif;
                             endif;

                            
                         endif;
                    
                    endif;
                    
                    $counter++; // Insertando valores para o próximo ID
                endforeach;
                
                echo "<input id='total_linhas' type=hidden value='$counter'>"; // Inclusão de index para ser reutlizado na função para percorrer todas as linhas
                
                die();
        
    }//envioajaxAction
        
        
   
    public function incluipontoAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        // Criando model para inserir dados no Banco de dados
        
        $meu = new Funcionariosgeraispontohora();
                
        if(isset($_POST['entrada_json'])){
            $horaEntrada = $_POST['entrada_json'];
        }
        
        if(isset($_POST['saida_json'])){
            $horaSaida = $_POST['saida_json'];
        }
        
        if(isset($_POST['entradapausa_json'])){
            $horaEntrada_pausa = $_POST['entradapausa_json'];
        }
        
        if(isset($_POST['saidapausa_json'])){
            $horaSaida_pausa = $_POST['saidapausa_json'];
        }
        
        if(isset($_POST['observacao'])){
            $obs = $_POST['observacao'];
        }
        
        if(isset($_POST['data_json'])){
            $data = $_POST['data_json'];
        }
        
        if(isset($_POST['id_rhfuncionario'])){
            $id_rhfuncionario = $_POST['id_rhfuncionario'];
        }

        if(isset($_POST['idlocalfuncionario'])){
            $idlocalfuncionario = $_POST['idlocalfuncionario'];
        }

        if(isset($_POST['iddeptfuncionario'])){
            $iddeptfuncionario = $_POST['iddeptfuncionario'];
        }
        
        if(isset($_POST['id_funcionario'])){
            $id_funcionario = $_POST['id_funcionario'];
        }

        if(isset($_POST['nomehorario'])){
            $nome_horario = $_POST['nomehorario'];
        }

        if(isset($_POST['idhorario'])){
            $id_horario = $_POST['idhorario'];
        }
       
        //Formatando a data para padrão internacional
        $dateObj =  date("d-m-Y", strtotime($data));
        $dateClass = new DateTime($dateObj);
       
        $dataEntrada =  $dateClass->format("Y-m-d") . " " . $horaEntrada . ":00";
        $dataSaida =  $dateClass->format("Y-m-d") . " " . $horaSaida . ":00";
        $dataEntrada_Pausa  =  $dateClass->format("Y-m-d") . " " . $horaEntrada_pausa . ":00";
        $dataSaida_Pausa = $dateClass->format("Y-m-d") . " " . $horaSaida_pausa . ":00";
        
        $date = date_create('2017-10-05 10:20:00');
        date_format($date, 'Y-m-d H:i:s');
        
        $ativao = "ativo";
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        
        try {
                $dados = array();
                //$dados['id'] = (int)"";
                $dados['titulo_pontohorarios'] = $obs;
                $dados['idfuncionario'] = $id_funcionario;
                $dados['idrhfuncionario'] = $id_rhfuncionario;
                // Usuarios::getUsuario('idfuncionario') 
                $dados['idlocalfuncionario'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idlocal'] ;
                $dados['iddeptfuncionario'] =  Usuarios::getUsuario('idperfil') ;
                $dados['data_pontohorarios'] = $dateClass->format("Y-m-d");
                $dados['entrada_pontohorarios'] = $dataEntrada;
                $dados['saida_pontohorarios'] =  $dataSaida;
                $dados['entradapausa_pontohorarios'] = $dataEntrada_Pausa;
                $dados['saidapausa_pontohorarios'] = $dataSaida_Pausa;

                $dados['fkhorariorotulo'] = $nome_horario;
                $dados['fkhorarioid'] = $id_horario;

                $dados['status_pontohorarios'] = null;
                $dados['logusuario_pontohorarios'] = $this->_usuario['id'];
                $dados['logdata_pontohorarios'] = date('Y-m-d G:i:s');
                
                $row = $meu->save($dados);

                $db->commit();
        } catch (Exception $e) {
                echo $e->getMessage();

                $db->rollBack();
                die();
        }//end try/catch        
        
        
            $return = array(
                'diaSemana'=>'data',
                'data'=>'entrada',
                'horaEntrada'=>$horaEntrada,
                'horaSaida'=>$horaSaida,
                'horaEntradaPausa'=>$horaEntrada_pausa,
                'horaSaidaPausa'=>$horaSaida_pausa,
                'ocorrencia'=>$obs
            );
        

        
        
        echo json_encode($return);
        
        die();
        
    }//end function incluiLancamentoPonto
    
    
    function verificaocorrenciaajaxAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        if(isset($_POST['id'])){
            $id = $_POST['id'];
        }//end if
        
        //echo $id;
        //retorno  de função
        $ocorrenciasList = new Funcionariosgeraispontoocorrencias();
        $queries = array();
        $queries['id'] = $id;
        $queriew['status'] = 'Ativo';
            
        $row = $ocorrenciasList->getAtas($queries);

        $strJsonRetorno;
        
        //Montanto Json para retorno de valores
        
        foreach($row as $index):
            $return[] = array(
                'id'=>$index['id'],
                'ocorrencia'=>$index['ocorrencia'],
                'descricao'=>$index['descricao'],
                'permite_divergencia'=>$index['permite_divergencia'],
                'status'=>$index['status'],
            );
        
        endforeach;
        
        echo json_encode($return);
        
        die();
        
    }//end function verificaOcorrenciaajaxAction
    
    public function cartoesAction(){
        $this->view->bread_crumb = array(
                array('url' => 'pontosmensais', 'name' => 'Pontos mensais'),
                array('url' => null,'name' => 'Blocos de Período')
        );  
        
        
        // Setando valores inseridos na URL
        $urlMaster =  $this->_request->getParam("id");
        $urlMaster = explode("-", $urlMaster);
        $usuarioId = $urlMaster[0];
        $funcionarioId = $urlMaster[1];
      
        //Puxando a lista de funcionarios
        
        $array = Array();
        $array['status'] = "Ativo";

    
        $cartoesList = new Funcionariosgeraispontocartaoblocosdatas();
        $cartao = $cartoesList->getFuncionariosgeraispontocartaoblocosdatas($array);
        $totalRegistros = $cartoesList->getFuncionariosgeraispontocartaoblocosdatas($array);
        
        
         /* ----------------------------------------------------------------------------
                                        Retornos da View
        ------------------------------------------------------------------------------*/
        $this->view->usuarioId = $usuarioId;
        $this->view->funcionarioId = $funcionarioId;
        $this->view->rows = $cartao;
        
    }// end cartoes Action
    
    public function chargetableindexajaxAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $funcionariosGerais = new Funcionariosgeraispontogrupos();

        $queries = array(); 
        $queries['status'] = 'Ativo'; 
        $queries['idhorario'] = '19';
        
        $this->view->total_registros = 3;
        $maxpp = 0;
        
        $paginaAtual = 0;
        
        echo json_encode($funcionariosGerais->getAtas($queries,0,4));
        die();
        
    }//end function chargeTableIndex
    
    
    /*
     * Função criada para apenas carregar os valores selecionados ou não da Table do CRUD
     * 
     */
    public function ajaxAction(){
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $_SESSION['teste']['99'] = false;
        
        echo '<pre>';
            var_dump($_SESSION['teste']);
        echo '</pre>';
        
        die();
        
    }//end public function
    
    public function confirmacaolancamentorhajaxAction(){
        
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        
        
        
        //carregando Valores
        
        /* 01 */  $id = $this->getRequest()->getPost("id");
        /* 02 */  $dataInicio = $this->getRequest()->getPost("inicio");
        /* 03 */  $dataFim = $this->getRequest()->getPost("fim");
        /* 04 */  $nome = $this->getRequest()->getPost("nome");
        /* 05 */  $local = $this->getRequest()->getPost("local");
        /* 06 */  $rgf = $this->getRequest()->getPost("rgf");
        /* 07 */  $lancamentos = $this->getRequest()->getPost("lancamentos");
        /* 08 */  $confirmados = $this->getRequest()->getPost("confirmados");
        /* 09 */  $usuarioSetado =  $this->getRequest()->getPost("usuarioSetado");

        
        $userId = explode("-", $usuarioSetado);
        
        // Trabalhando Datas
         
        $date_i =  date("d-m-Y", strtotime($dataInicio));
        $objDate_i = new DateTime($date_i);
        
        $data =  date("d-m-Y", strtotime($dataFim));
        $objDateFinal = new DateTime($data);
         
        $interval = new DateInterval('P1D');
        
        $endOne = new DateTime($objDateFinal->format('Y-m-d'));
        $endOne->modify('+1 day');
        
        $period = new DatePeriod($objDate_i, $interval, $endOne);
        
        $pontosFuncionarios = new Funcionariosgeraispontohora();
        $contador = 0;   

        $idFuncionarioUser =  Usuarios::getUsuarioByIdHelper($userId[3])['idfuncionario'];

        $funcionariosgerais = new Funcionariosgeraisescolas();

        $funcionario = $funcionariosgerais->getFuncionariogeralescolaById((int)$idFuncionarioUser);

        $idlocal = $funcionario['idlocal'];

        $strRetornos = array();

        $contador = 0;
        
        foreach($period as $data): 
          
            /*
             * Pegando os registros da linha a ser modificada e setada como confirmada pelo RH
             * $id -> [idrhfuncionario]
             * $userId -> Refere-se ao campo 
             * $idlocalfuncionario -> [idlocalfuncionario]
             * $iddeptfuncionario -> [iddeptfuncionario]
             * $data -> [data_pontohorarios]
             */
            
            $row = $pontosFuncionarios->getRetornaRegistroData($id, $userId[3], $idlocal, $userId[0], $data->format('Y-m-d'));

            if($row): 

                $row['status_pontohorarios'] = "ok";
                $row['confirmacao_status'] = date('Y-m-d G:i:s');
                $row['status'] = 'Ativo';
                $row['logusuario'] = $this->_usuario['id'];

                $pontosFuncionarios->save($row);

                $contador++;

            endif;

            array_push($strRetornos, $row);

        endforeach;

        $return = array(
            'id'=>$id,
            'local'=>$local,
            'nome'=>$nome,
            'rgf'=>$rgf,
            'contador'=>$contador,
            'totaldias'=>'20',
            'lancamentos'=>'00 / 00',
            'lancamentosConfirmados'=>'00 / 00',
            'lancamentosConfirmadosRH'=>$contador,
        );

        echo json_encode($return);

        die();

        foreach($period as $data):
            
            //$row = $pontosFuncionarios->getRetornaRegistroData($id, $userId[3],$data->format('Y-m-d'));
           
            if($row){
                    
                    $row['status_pontohorarios'] = "ok";
                    $row['confirmacao_status'] = date('Y-m-d G:i:s');
                    $row['status'] = 'Ativo';
                    $row['logusuario'] = $this->_usuario['id'];
                    $pontosFuncionarios->save($row);
                    $contador++;

            }//end if
            
        endforeach;
        
     
       

        //nome do funcionário e lançamentos
        
        $queries = array();
        $queries['id'] = $id;
        $queries['idsperfis'] = $userId[0];
        $queries['total'] = false;
        $queries['sorting'] = "";
        $queries['status'] = 'Ativo';
        $queries['ocorrenciafiltro'] = '';
       
        //$funcionariosgerais->FuncionariosGeraisPontosResumo($queries, $paginaAtual, $maxpp, $dataInicial, $dataFinal);
        $row = Funcionariosgeraisescolas::FuncionariosGeraisPontosResumo($queries, 0, 0, $objDate_i->format('Y-m-d'), $objDateFinal->format('Y-m-d'),$userId[3],"");
        
        $return = array(
            'id'=>$id,
            'local'=>$local,
            'nome'=>$nome,
            'rgf'=>$rgf,
            'totaldias'=>'20',
            'lancamentos'=>$lancamentos,
            'lancamentosConfirmados'=>$confirmados,
            'lancamentosConfirmadosRH'=>$row[0]['confirmados'],
        );

        echo json_encode($return);
        die();
        
    }//end public function confirmacaoLancamentorhajaxAction
    
    public function editalancamentoajaxAction(){

        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        // id
        $id =  $this->getRequest()->getPost("id");
        // iduser
        $userId = $this->getRequest()->getPost("iduser");
        // idlocalfuncionario
        $idlocalfuncionario = $this->getRequest()->getPost("idlocalfuncionario");
        // iddeptfuncionario
        $iddeptfuncionario = $this->getRequest()->getPost("iddeptfuncionario");
        // data: data,
        $data = $this->getRequest()->getPost("data");
        // entrada: entrada,
        $entrada = $this->getRequest()->getPost("entrada");
        // saida: saida,
        $saida = $this->getRequest()->getPost("saida");
        // entradapausa: entradapausa,
        $entradapausa = $this->getRequest()->getPost("entradapausa");
        // saidapausa: saidapausa,
        $saidapausa = $this->getRequest()->getPost("saidapausa");
        // observacao: observacao
        $observacao = $this->getRequest()->getPost("observacao");

        $data_convert =  date("d-m-Y", strtotime($data));
        $date = new DateTime($data_convert);

        $dataEntrada =  $date->format("Y-m-d") . " " . $entrada . ":00";
        $dataSaida =  $date->format("Y-m-d") . " " . $saida . ":00";
        $dataEntrada_Pausa  =  $date->format("Y-m-d") . " " . $entradapausa . ":00";
        $dataSaida_Pausa = $date->format("Y-m-d") . " " . $saidapausa . ":00";
        
        $funcionariopontohora = new Funcionariosgeraispontohora();
       
        $dados = $funcionariopontohora->getRetornaRegistroData($id, $userId, $idlocalfuncionario, $iddeptfuncionario, $date->format('Y-m-d'));

        /* ------------------  Inclusão dos dados UPDATE --------------------------------------------*/
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        
        try {

                $dados['titulo_pontohorarios'] = $observacao;
                $dados['entrada_pontohorarios'] = $dataEntrada;
                $dados['saida_pontohorarios'] =  $dataSaida;
                $dados['entradapausa_pontohorarios'] = $dataEntrada_Pausa;
                $dados['saidapausa_pontohorarios'] = $dataSaida_Pausa;
                $dados['status_pontohorarios'] = "edit";
                $dados['logusuario_pontohorarios'] = $this->_usuario['id'];
                $dados['logdata_pontohorarios'] = date('Y-m-d G:i:s');
            
                $row = $funcionariopontohora->save($dados);

                $db->commit();
        } catch (Exception $e) {
                echo $e->getMessage();

                $db->rollBack();
                die();
        }//end try/catch        
        

        /* ------------------ End  Inclusão dos dados UPDATE ----------------------------------------*/

        echo json_encode($dados);
        
        die();
        
    }//end public function editalancamentoajaxAction


    public function anexafolhadepontoAction(){

        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        // $dataInicio =  $this->getRequest()->getPost("dataInicio_ajax");
        // $dataFim =  $this->getRequest()->getPost("dataFim_ajax");
        // $usuarioId =  $this->getRequest()->getPost("usuarioId_ajax");
        // $funcionarioId =  $this->getRequest()->getPost("funcionarioId_ajax");
        // $arquivoId = (int)trim($this->getRequest()->getPost("idarquivo"));
        // $arquivofile = $_FILES['idarquivo'];
        // $arquivo = 'C:/Users/Thiago/Documents/teste.txt';
        //$arquivo = 'C:/fakepath/teste.txt'; 
        //$retorno =$this->getArquivo($arquivo);

        // $arquivos = new Arquivos();
        //print_r($_FILES['idarquivo-1']);
        // $idarquivo = $arquivos->getArquivoFromForm($arquivo);
        if($this->getRequest()->isPost()): 
            echo "tem";
            var_dump($this->getRequest()->isPost());
        else: 
            echo "não tem";
        endif;

        
        //echo $retorno;
        die();
        /*
        $cartoes = new Funcionariosgeraispontocartaoblocoanexos();
        $arqui = $this->getRequest()->getPost("idarquivo");
        
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        
        try {
                $dados = array();
                $dados['datainiciobloco'] = $dataInicio;
                $dados['datafimbloco'] = $dataFim;
                $dados['usuarioid'] =  $usuarioId;
                $dados['funcionarioid'] = $funcionarioId;
                $dados['rgf'] = '007';

                $arquivoId = (int) $this->getArquivo("C:\Users\Thiago\Documents\RH 08_06\teste.txt");
            
                if ($arquivoId!=0) 
                    $dados['idarquivofolhaponto'] = $arquivoId;

                $dados["status"] = 'Ativo';
                $dados['excluido'] = 'nao';
                $dados['logusuario'] = $this->_usuario['id'];
                $dados['logdata'] = date('Y-m-d G:i:s');
            
                $row = $cartoes->save($dados);

                $db->commit();
        } catch (Exception $e) {
                echo $e->getMessage();

                $db->rollBack();
                die();
        }//end try/catch
       
        die();
            */
    }//end function 
    


    private function getArquivo($filename) {
        
        $idarquivo = false;
        $arquivos = new Arquivos();
         
        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros,$e->getMessage());
        }
    
        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
         
        if ($excluir_arquivo=='excluir') $idarquivo = -1;

        return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();
        
        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros,$e->getMessage());
        }
        
        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
        return $idimagem;
    }

    public function anexospontosAction(){

        $this->view->bread_crumb = array(
                array('url' => 'pontosmensais', 'name' => 'Pontos mensais'),
                array('url' => null,'name' => 'Anexar Arquivo')
        );  
    
        // Setando valores inseridos na URL
        $urlMaster =  $this->_request->getParam("id");
        $urlMaster = explode("-", $urlMaster);
        $usuarioId = $urlMaster[0];
        $funcionarioId = $urlMaster[1];
  
        
        /* ----------------------------------------------------------------------------
                                        Retornos da View
        ------------------------------------------------------------------------------*/
        $this->view->usuarioId = $usuarioId;
        $this->view->funcionarioId = $funcionarioId;
        

    }//end public function anexar

    public function settimefuncionarioajaxAction(){
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $usuario = $this->getRequest()->getPost("user");
        $funcionario =  $this->getRequest()->getPost("funcionario");
        $horario =  $this->getRequest()->getPost("horario");
        $idlocal =  $this->getRequest()->getPost("idlocal");
        $iddept =  $this->getRequest()->getPost("iddept");

        $funcionariohorario = new Funcionariosgeraispontohorariosfuncionario();

        $querie = array();
        $querie['fkfuncionario'] = $funcionario;
        $querie['idlocalfuncionario'] = $idlocal;
        $queries['iddepstfuncionario'] = $iddept;
        $rows = $funcionariohorario->getFuncionariosgeraispontohorariosfuncionarios($querie);

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        
        try {
                $dados = array();

                // 1    id  bigint(20)          Não None        
               
                if(count($rows) > 0):
                    $dados['id'] = (int)$rows[0]['id'];
                endif;
                // 2    fkfuncionariosgeraispontodefinicaohorarios  bigint(20)          Sim None        
                $dados['fkfuncionariosgeraispontodefinicaohorarios'] = (int)$horario; 
                // 3    fkusuario   bigint(20)          Sim None        
                $dados['fkusuario'] = (int)$usuario;  
                // 4    fkfuncionario
                $dados['fkfuncionario'] = (int)$funcionario;
                // 5 
                $dados['idlocalfuncionario'] = (int)$idlocal;
                // 6 
                $dados['iddepstfuncionario'] = (int)$iddept;

                $dados["status"] = 'Ativo';
                $dados['excluido'] = 'nao';
                $dados['logusuario'] = $this->_usuario['id'];
                $dados['logdata'] = date('Y-m-d G:i:s');
            
                $row = $funcionariohorario->save($dados);

                $horarios = new Funcionariosgeraispontodefinicaohorarios();
                $rowhorario = $horarios->getFuncionariosgeraispontodefinicaohorariosById($horario);

                echo json_encode($rowhorario);

                $db->commit();
        } catch (Exception $e) {
                echo $e->getMessage();

                $db->rollBack();
                die();
        }//end try/catch
      
        die();
    }//end public function anexar


     public function gettimefuncionarioajaxAction(){
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $funcionariohorario = new Funcionariosgeraispontohorariosfuncionario();

        $querie = array();
        $querie['fkusuario'] = $this->getRequest()->getPost("usuario");
        $querie['fkfuncionario'] = $this->getRequest()->getPost("funcionario");
        $rows = $funcionariohorario->getFuncionariosgeraispontohorariosfuncionarios($querie);

        $horario = new Funcionariosgeraispontodefinicaohorarios();
        $row = $horario->getFuncionariosgeraispontodefinicaohorariosById($rows[0]['fkfuncionariosgeraispontodefinicaohorarios']);

        $strRetorno = array();

        echo json_encode($row);
       
        die();

     }//end public function 

    public function gerardocumentoferiasAction(){

        
        $id = $this->_request->getParam("id");
        $datainicio = $this->_request->getParam("inicio");
        $datafim = $this->_request->getParam("fim");

        $usuario = Usuarios::getUsuarioByIdHelper($id);

        $queries = array();
        $queries['id'] = $usuario['idfuncionario'];

        $funcionarios = new Funcionariosgeraisescolas();
        $grupofuncionarios = $funcionarios->getFuncionariosgeraisescolas($queries);

        $querie = array();
        $querie['idsfuncionariossel'] = $grupofuncionarios[0]['idsfuncionarios'];
        $querie['idsecretaria'] = '1';
        $querie['die'] = '0';

        $funcionariogeral = new Funcionariosgeraisescolas();
      
        $paginaAtual = 0; 
        $maxpp = 0;
        
        $idusuario = $id; 
        $estado = '';
       
        $dateObjInicio =  date("Y-m-d", strtotime($datainicio));
        $brDateInicio = new DateTime($dateObjInicio);

        $dateObjFim =  date("Y-m-d", strtotime($datafim));
        $brDateFim = new DateTime($dateObjFim);

        $this->view->nomeEscola = Escolas::getEscolasHelper(array('id'=>$grupofuncionarios[0]['idescola']))[0]['escola'];
        $this->view->datainicio = $brDateInicio->format("Y-m-d");
        $this->view->datafim = $brDateFim->format("Y-m-d");
        $this->view->idusuario = $id;
        $this->view->rows = $funcionariogeral->FuncionariosGeraisPontosResumo($querie, $paginaAtual, $maxpp, $datainicio, $datafim,$id, $estado);

        $this->preForm();
        //var_dump($this->view->post_var); die();

        $db = Zend_Registry::get('db');


        //$var = TCPDF::getTeste();
        // var_dump($var); die();
        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Requerimento de Férias');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);


        // //set auto page breaks
        // //$pdf->SetAutoPageBreak(FALSE);
        // $pdf->SetAutoPageBreak(TRUE, 4);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        //$pdf->SetFont('times', '', null , 'false');
        // set font
        $pdf->SetFont('helvetica', '', 9);
        // add a page
        $pdf->AddPage();

        $html = $this->view->render('pontosmensais/pdf/index.phtml');

        $pdf->writeHTML($html, true, 0, true, 0);


        if((int)$id > 0) {          
                $filename = 'Folhasdeponto'.$funcionario['rgf'].'-'.$funcionario['nome'].'_'.date('d-m-Y').'.pdf';
        }else{
                $filename = 'Folhasdeponto'.date('d-m-Y').'.pdf';
        }           
        $pdf->Output($filename, 'I');

        die();

        return true;        
    }// end documentos

    public function gerarpdfpontosmensaissimplificadoAction(){

        $id = $this->_request->getParam("id");
        $datainicio = $this->_request->getParam("inicio"); 
        $datafim = $this->_request->getParam("fim");

        $funcionariosgerais = new Funcionariosgeraisescolas();

        $usuario = Usuarios::getUsuarioByIdHelper($id);   
       
        $usuariofuncionario = $funcionariosgerais->getFuncionariogeralescolaById($usuario['idfuncionario']);
        $queries = array();

        $idperfil = (int)$usuario['idperfil'];
        $idescola = (int)$usuariofuncionario['idescola'];

        $arraySetFuncionario['idnovo'] = $usuario['id'];

         /* ===========================================================================
                    Inclusão do bloco para busca dos funcionarios do local indicado
        ============================================================================== */

           

        if((int)$idescola > 0){

            // Caso seja uma escola
         
            $idfuncionario = $usuario['idfuncionario'];
            
            // $meusfuncionarios = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaImplodeByIdescolaHelper($idfuncionario, $idescola, array());
            
            // if((isset($meusfuncionarios)) && ($meusfuncionarios) && ($meusfuncionarios!="")){
            //     $queries['idsfuncionariossel'] = $meusfuncionarios;
                
            // }else{
            //     $queries['idsfuncionariossel'] = '-1';
            // }

            $meusfuncionarios = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaImplodeByIdescolaHelper($idfuncionario, $idescola, array());
            if((isset($meusfuncionarios)) && ($meusfuncionarios) && ($meusfuncionarios!="")){
                $queries['idsfuncionariosgerais'] = $meusfuncionarios;
            }else{
                $queries['idsfuncionariosgerais'] = '-1';
            }

        }elseif((int)$idperfil > 0){
          
            
            $idfuncionario = $usuario['idfuncionario'];
            $meusfuncionarios = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaImplodeByIdperfilHelper($idfuncionario, $idperfil, array());
            unset($queries['idsfuncionariosgerais']);
            if((isset($meusfuncionarios)) && ($meusfuncionarios) && ($meusfuncionarios!="")){
                $queries['idsfuncionariossel'] = $meusfuncionarios;
            }else{
                $queries['idsfuncionariossel'] = '-1';
            }
        }// end if else for meusfuncionarios block

       

        /* ===========================================================================
                End Inclusão do bloco para busca dos funcionarios do local indicado
        ============================================================================== */

        // trabalhando data de inicio
        $dateObjInicio =  date("Y-m-d", strtotime($datainicio));
        $brDateInicio = new DateTime($dateObjInicio);

        // trabalhando data de fim
        $dateObjFim =  date("Y-m-d", strtotime($datafim));
        $brDateFim = new DateTime($dateObjFim);

        // ===================  Setando valores a serem repassados para a view

        // Setando o nome da escola
        $nomeImpressao = '';

        if($idescola > 0){
            $this->view->nomeescola = 'Pólo: ' . $usuariofuncionario['desclocal'];
            $nomeImpressao = $usuariofuncionario['desclocal'];
        }else{
            $this->view->nomeescola = 'Departamento ' . UsuariosPerfis::getPerfilById($usuario['idperfil'])['perfil'];
            $nomeImpressao = UsuariosPerfis::getPerfilById($usuario['idperfil'])['perfil'];
        }// end if else

       

        $this->view->usuario = $usuariofuncionario;
        $this->view->userid = $id;
        $this->view->local = '';
        $this->view->departamento = $idperfil;
        $this->view->totalfuncionarios = count($funcionariosgerais->getFuncionariosgeraisescolas($queries));

        $this->view->datainicio = $brDateInicio->format('d/m/Y');
        $this->view->datafim = $brDateFim->format('d/m/Y');

        $this->view->engldatainicio = $brDateInicio->format('Y-m-d');
        $this->view->engldatafim = $brDateFim->format('Y-m-d');

        $this->view->funcionarios = $funcionariosgerais->getFuncionariosgeraisescolas($queries);

        // echo '<pre>';
        // print_r($funcionariosgerais->getFuncionariosgeraisescolas($queries));
        // die();

        /* ===========================================================================
                                            Iniciando o PDF
        ============================================================================== */

        // $this->preForm();
        //var_dump($this->view->post_var); die();

        $db = Zend_Registry::get('db');
        
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('l');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Relatório_Geral_RH');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        //$pdf->SetFont('times', '', null , 'false');
        // set font
        $pdf->SetFont('helvetica', '', 9);
        // add a page
        $pdf->AddPage();
 
        $html = $this->view->render('pontosmensais/pdf/relatoriosimplificado.phtml');
        //$html = 'oi';

        $pdf->writeHTML($html, true, 0, true, 0);
        
        $filename = 'relatoriogeral_' . $nomeImpressao . '.pdf'; 
        $pdf->Output($filename, 'I');

        
        die();

        return true;   

        /* ===========================================================================
                                        End Iniciando o PDF
        ============================================================================== */
    
    }//end gerarpdfpontosmensaissimplificadoAction

    public function gerardocumentofolhadepontoAction(){

            
        $id = $this->_request->getParam("id");
        $user = $this->_request->getParam("user");
        $inicio = $this->_request->getParam("inicio");
        $fim = $this->_request->getParam("fim");

        // informações da escola do usuário
        $usuario = Usuarios::getUsuarioByIdHelper($user);

        $usuariofuncionario = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($usuario['idfuncionario']);

        // local do usuário e usuário responsável pela ficha
        $funcionario = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($usuario['idfuncionario']);

        // nome escola
        $nomeEscolaFolha = $funcionario['descdepartamento'] . " " . $funcionario['desclocal'];

        // Período inicial
        $dataInicio =  date("d-m-Y", strtotime($inicio));
        $date_inicio = new DateTime($dataInicio);

        // Período final
        $dataFim =  date("d-m-Y", strtotime($fim));
        $date_fim = new DateTime($dataFim);
        
        // Recuperando informações do funcionário ao qual a ficha de Folha de ponto pertence
        $funcionarioFolhaPonto = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($id);
       
        // Redundancia para informações para a busca das folhas de ponto
        $this->view->funcionario = $funcionarioFolhaPonto;
        $this->view->usuario = $usuario;
        $this->view->local = $usuariofuncionario['idlocal'];
        $this->view->dept = $usuario['idperfil'];
       
        /* =========================    Setando valores na View ==========================*/

        // print_r($usuario);
        // echo '<br>--------------<br>'; 
        // print_r($usuariofuncionario['idescola']);

        if( !(($usuariofuncionario['idescola'] > 0) and (isset($usuariofuncionario['idescola'] ))) ) {
            $nomeEscolaFolha = 'Departamento : ' . UsuariosPerfis::getPerfilById($usuario['idperfil'])['perfil'];
        }// end if else

        $this->view->nomeescola = $nomeEscolaFolha;
        $this->view->datainiciobr = $date_inicio->format('d/m/Y');
        $this->view->datainicio =  $date_inicio->format('Y-m-d');

        $this->view->datafimbr = $date_fim->format('d/m/Y');
        $this->view->datafim = $date_fim->format('Y-m-d');

        $this->view->funcionarioFolhaPonto = $funcionarioFolhaPonto;
      
        $this->view->idusuario = $user;
        $this->view->idfuncionario = $id;

        /* =====================    END Setando valores na View ==========================*/    

        
        $this->preForm();
        //var_dump($this->view->post_var); die();

        $db = Zend_Registry::get('db');

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Folha_Ponto_Individual');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // //set auto page breaks
        // //$pdf->SetAutoPageBreak(FALSE);
        // $pdf->SetAutoPageBreak(TRUE, 4);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        //$pdf->SetFont('times', '', null , 'false');
        // set font
        $pdf->SetFont('helvetica', '', 9);
        // add a page
        $pdf->AddPage();

        $html = $this->view->render('pontosmensais/pdf/folhadeponto.phtml');

        $pdf->writeHTML($html, true, 0, true, 0);
        
        $filename = 'folhadeponto_' . $funcionarioFolhaPonto['nome'] . '.pdf'; 
        $pdf->Output($filename, 'I');

        die();
    }//end gerardocumentofolhadeponto

    public function verificatipoocorrenciaAction(){
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $ocorrencia = $this->_request->getParam("ocorrencia");
        $rows = Funcionariosgeraispontoocorrencias::getpontoocorrencia($ocorrencia);

        //echo $ocorrencia;
        echo json_encode($rows);

        die();
    }//end public function verificatipoocorrencia


    public function indexgeralAction(){ 

        if(isset($_POST['filtro_index'])){
            $filtros = explode('-',$_POST['filtro_index']);
        }else{
            $filtros = explode('-','0-todos-0');
        }
       
        $escolas = Escolas::getEscolasRH($filtros[1]);
        $escolasFiltro = Escolas::getEscolasRH(' ');

        $departamentos = Escolas::getDepartamentosRH($filtros[1]); 
        $departamentosFiltro = Escolas::getDepartamentosRH(' '); 

        //$escola = Escolas::getEscolaByIdHelper($row['idescola']);

        // Tratamento dos períodos 
        if(isset($_POST['idperiodo'])){
            // Caso exista um período setado no select
            $listadeDatas = new Funcionariosgeraispontocartaoblocosdatas();
            $periodoBloco = $listadeDatas->getFuncionariosgeraispontocartaoblocosdatasById($_POST['idperiodo']);
            $this->view->periodo =  $periodoBloco;
        }else{
            // Caso nao exista, setar o período do mes corrente

        }

        $this->view->filtro =  (isset($_POST["filtro_index"])) ? $_POST["filtro_index"] : ''; 
        $this->view->periodoInicio = '';
        $this->view->periodofim = '';
        $this->view->escolas = $escolas;
        $this->view->departamentos = $departamentos;
        $this->view->departamentosFiltros = $departamentosFiltro;
        $this->view->escolasFiltros = $escolasFiltro;
      

    }
                    
    public function funcionariosrhdetalhesAction(){
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = $this->_request->getParam("iduser");
        $funcionarios = $this->_request->getParam("funcionarios");
        $local = $this->_request->getParam("local");
        $departamento = $this->_request->getParam("departamento");
        $inicio = $this->_request->getParam("inicio");
        $fim = $this->_request->getParam("fim");

        $usuario = Usuarios::getUsuarioByIdHelper($id);

        $usuariofuncionario = Funcionariosgeraisescolas::getFuncionariosgeraisescolasHelper(array('id'=> $usuario['idfuncionario']));

        $departamento = $usuario['idperfil'];

        $strFuncionarios = explode(',',$funcionarios);

        $strTable = array();

        foreach($strFuncionarios as $index):

            $funcionario = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($index);

            $auxiliar['inicio'] = $inicio;
            $auxiliar['fim'] = $fim;
            $auxiliar['idusuario'] = $id;
            $auxiliar['idfuncionario'] = $index;
            $auxiliar['local'] = $local;
            $auxiliar['departamento'] = $departamento;
            $auxiliar['idescola'] = $usuariofuncionario[0]['idescola'];

            $diasUteis = Funcionariosgeraispontohorarios::getDiasUteisPeriodo($inicio, $fim);
            $feriados = Funcionariosgeraispontohorarios::getFeriadosValidos($inicio, $fim);
            $injustificados = Funcionariosgeraispontohorarios::getFGFInjustificadas($index, $local, $departamento, $inicio, $fim);
            $justificados = Funcionariosgeraispontohorarios::getFGFJustificados($index, $local, $departamento, $inicio, $fim);
            $lancados = Funcionariosgeraispontohorarios::getFGLanc($index, $local, $departamento, $inicio, $fim);
            $confirmados = (int)Funcionariosgeraispontohorarios::getFGRhconfirmados($index, $local, $departamento, $inicio, $fim);
            $lancadosFDS = Funcionariosgeraispontohorarios::getFGLancFDS($index, $local, $departamento, $inicio, $fim);
            $feriadosUteis = Funcionariosgeraispontohorarios::getFGLancFeriados($index, $local, $departamento, $inicio, $fim);

            // Pegando os nomes dos funcionários
            $auxiliar['nomefuncionario'] = $funcionario['nome'];

            $auxiliar['rgf'] = $funcionario['rgf'];
            // Setando os lancamentos do funcionario
            $auxiliar['lancamentos'] = $lancados;

            // Setando os dias uteis do funcionario
            $auxiliar['uteis'] = (($diasUteis - $feriados) + ($lancadosFDS + $feriadosUteis));
            // Setando os feriados
            $auxiliar['feriados'] = $feriados;

            $auxiliar['injustificados'] = $injustificados;

            $auxiliar['justificados'] = $justificados;

            $auxiliar['confirmados'] = $confirmados;

            $auxiliar['trabalhados'] = ((int)$lancados - (((int)$justificados) + ((int)$injustificados)));


            // Setando todos os valores dos funcionários
            array_push($strTable, $auxiliar);

        endforeach;


        echo json_encode($strTable);

        die();
    }//end rhusuarios

}