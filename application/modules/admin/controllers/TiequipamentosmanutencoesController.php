<?php

/**
 * Controle da classe tiequipamentosmanutencoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_TiequipamentosmanutencoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Tiequipamentomanutencao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("tiequipamentosmanutencoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Tiequipamentosmanutencoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Equipamento excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="tiequipamentosmanutencoes") $objs = new Tiequipamentosmanutencoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tiequipamentosmanutencoes', 'name' => 'Equipamentos'),
			array('url' => null,'name' => 'Visualizar Equipamento')
		);
		
		$id = (int)$this->_request->getParam("id");
		$tiequipamentosmanutencoes = new Tiequipamentosmanutencoes();
		$tiequipamentomanutencao = $tiequipamentosmanutencoes->getTiequipamentomanutencaoById($id, array());
		
		if (!$tiequipamentomanutencao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $tiequipamentomanutencao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Equipamentos')
		);
		
		$ns = new Zend_Session_Namespace('default_tiequipamentosmanutencoes');
		$tiequipamentosmanutencoes = new Tiequipamentosmanutencoes();
		$queries = array();	
		//$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		//$queries["modulo"] = "TI Manutenção";
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idfornecedor"]!="") $queries["idfornecedor"] = $this->view->post_var["idfornecedor"];
if ($this->view->post_var["idchamado"]!="") $queries["idchamado"] = $this->view->post_var["idchamado"];
if ($this->view->post_var["previsao_i"]!="") $queries["previsao_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["previsao_i"]));
if ($this->view->post_var["previsao_f"]!="") $queries["previsao_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["previsao_f"]));
if ($this->view->post_var["observacoes"]!="") $queries["observacoes"] = $this->view->post_var["observacoes"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $tiequipamentosmanutencoes->getTiequipamentosmanutencoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $tiequipamentosmanutencoes->getTiequipamentosmanutencoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de tiequipamentosmanutencoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tiequipamentosmanutencoes', 'name' => 'Equipamentos'),
			array('url' => null,'name' => 'Editar Equipamento')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$tiequipamentosmanutencoes = new Tiequipamentosmanutencoes();
		$tiequipamentomanutencao = $tiequipamentosmanutencoes->getTiequipamentomanutencaoById($id);
		
		if (!$tiequipamentomanutencao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $tiequipamentomanutencao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($tiequipamentomanutencao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Equipamento editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de tiequipamentosmanutencoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'tiequipamentosmanutencoes', 'name' => 'Equipamentos'),
			array('url' => null,'name' => 'Adicionar Equipamento')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Equipamento adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idtiequipamentomanutencao
     */    
    private function preForm($idtiequipamentomanutencao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_tiequipamentomanutencao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idfornecedor = (int)trim($this->getRequest()->getPost("idfornecedor"));
$idchamado = (int)trim($this->getRequest()->getPost("idchamado"));
$previsao = Mn_Util::stringToTime($this->getRequest()->getPost("previsao"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$previsao) array_push($erros, "Informe a Data.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$tiequipamentosmanutencoes = new Tiequipamentosmanutencoes();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idfornecedor"] = $idfornecedor;
$dados["idchamado"] = $idchamado;
$dados["previsao"] = date("Y-m-d", $previsao);
$dados["observacoes"] = $observacoes;
$dados["status"] = $status;
$dados["modulo"] = "TI Manutenção";

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $tiequipamentosmanutencoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__ ."::incluir", Usuarios::getUsuario("id"), "Tiequipamentosmanutencoes " . $e->getMessage(), "E");
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}