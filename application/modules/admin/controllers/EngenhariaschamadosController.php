<?php

/**
 * Controle da classe chamados do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EngenhariaschamadosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Chamado
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("engenhariaschamados", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$iddepartamento = Usuarios::getUsuario('iddepartamento');
		$departamento = false;
		if($iddepartamento > 0){
			$departamento = Departamentossecretarias::getDepartamentosecretariaByIdHelper($iddepartamento);
		}
		
		//var_dump(Usuarios::getUsuario('iddepartamento')); die('aqui');
		$this->view->departamento = $departamento;

		$chamados = new Chamados();
		$queries = array();	
		$ultimo = $chamados->getUltimoChamadoByDestino("Engenharia", $queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml"); 
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Chamados();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="engenhariaschamados") $objs = new Chamados();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'engenhariaschamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Visualizar Chamado')
		);
		
		$id = (int)$this->_request->getParam("id");
		$chamados = new Chamados();
		$chamado = $chamados->getChamadoById($id, array());
		
		if (!$chamado) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $chamado;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Chamados')
		);
		
		$ns = new Zend_Session_Namespace('admin_engenhariaschamados');
		$chamados = new Chamados();
		$queries = array();	
		$queries['destino'] = "Engenharia";
		$queries['idescola'] = Usuarios::getUsuario('idescola');
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
//			if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
			if ($this->view->post_var["idprioridade"]!="") $queries["idprioridade"] = $this->view->post_var["idprioridade"];
			if ($this->view->post_var["idstatus"]!="") $queries["idstatus"] = $this->view->post_var["idstatus"];
			if ($this->view->post_var["dataagendada_i"]!="") $queries["dataagendada_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["dataagendada_i"]));
			if ($this->view->post_var["dataagendada_f"]!="") $queries["dataagendada_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["dataagendada_f"]));
			if ($this->view->post_var["statusatendimento"]!="") $queries["statusatendimento"] = $this->view->post_var["statusatendimento"]; 
			if (isset($this->view->post_var["tipostatus"]) && $this->view->post_var["tipostatus"]!="") $queries["tipostatus"] = $this->view->post_var["tipostatus"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		


		//PAGINACAO
		$maxpp = 20;
		
		if($queries['idescola'] == '0'): 
			if(Usuarios::getUsuario('idperfil') == "36" OR Usuarios::getUsuario('idperfil') == "30" ):  // 36 engenharia - 30 perfil admin para visualizar tudo
				$totalRegistros = $chamados->getChamados($queries);	
			else: 
				$queries['idescola'] = 0;
				$queries['iddepartamentocriacao'] = Usuarios::getUsuario('idperfil');
				$totalRegistros = $chamados->getChamados($queries);	
			endif;
		else: 
			$totalRegistros = $chamados->getChamados($queries);	
		endif;

    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $chamados->getChamados($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
	
		
		$this->view->queries = $queries;
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;   
		

		if($queries['idescola'] == '0'): 
			if(Usuarios::getUsuario('idperfil') == "36" OR Usuarios::getUsuario('idperfil') == "30" ):  // 36 engenharia - 30 perfil admin para visualizar tudo
				$this->view->rows = $chamados->getChamados($queries, $paginaAtual, $maxpp);	
			else: 
				$queries['idescola'] = 0;
				$queries['iddepartamentocriacao'] = Usuarios::getUsuario('idperfil');
				$this->view->rows = $chamados->getChamados($queries, $paginaAtual, $maxpp);	
			endif;
		else: 
			$this->view->rows = $chamados->getChamados($queries, $paginaAtual, $maxpp);	
		endif;

		
		
		
	}
	
	/**
	 * 
	 * Action de edição de chamados
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'engenhariaschamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Editar Chamado')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$chamados = new Chamados();
		$chamado = $chamados->getChamadoById($id);
		
		if (!$chamado) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $chamado;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($chamado);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de chamados 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'engenhariaschamados', 'name' => 'Chamados'),
			array('url' => null,'name' => 'Adicionar Chamado')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Chamado adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idchamado
     */    
    private function preForm($idchamado = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_chamado = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$destino = "Engenharia";
		$iddepartamento = (int)trim($this->getRequest()->getPost("iddepartamento"));
		$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$descricoes = trim($this->getRequest()->getPost("descricoes"));
		$idprioridade = (int)trim($this->getRequest()->getPost("idprioridade"));
		$idstatus = (int)trim($this->getRequest()->getPost("idstatus"));
		$dataagendada = Mn_Util::stringToTime($this->getRequest()->getPost("dataagendada"));
		$finalizacao = trim($this->getRequest()->getPost("finalizacao"));
		$finalizar = trim($this->getRequest()->getPost("finalizar"));
		$finalizacaodata = date('Y-m-d G:i:s');
		$atendido = trim($this->getRequest()->getPost("atendido"));
		$idtipo = (int)trim($this->getRequest()->getPost("idtipo"));
		$idtipodaequipe = (int)trim($this->getRequest()->getPost("idtipodaequipe"));
		$observacoes = trim($this->getRequest()->getPost("observacoes"));
		$servicoesdescricoes = trim($this->getRequest()->getPost("servicoesdescricoes"));
		$servicosmateriais = trim($this->getRequest()->getPost("servicosmateriais")); 
		$idresponsavel = (int)trim($this->getRequest()->getPost("idresponsavel"));
		$obsEngenheiro =  $this->getRequest()->getPost("obsEngenheiro");
		$status = 'Ativo';
	 
		$idlocal = Usuarios::getUsuario('idperfil');
				
		$erros = array();

		$tipoPerfil = Usuarios::getUsuario('idperfil');
		
		if ((""==$sequencial) && ($id<=0)) array_push($erros, "Informe a Sequancial/Ano."); 
		if (""==$descricoes && ($id<=0)) array_push($erros, "Informe a Descrição.");
		if($tipoPerfil == 36){
		if (0==$idprioridade) array_push($erros, "Informe a Prioridadade.");
		if (0==$idstatus) array_push($erros, "Informe a Status.");
		if (""==$dataagendada) array_push($erros, "Informe a Data.");
		if (""==$idtipodaequipe) array_push($erros, "Informe o tipo de equipe.");
		if (""==$idresponsavel) array_push($erros, "Informe o Responsavel.");
	  
		}
		if($tipoPerfil != 36 AND $idprioridade){ 
			if($atendido == 'sim'){
				if (""==$idtipo) array_push($erros, "Informe o tipo de avaliação.");
			}

		}
		
		$chamados = new Chamados();
		 
		if (sizeof($erros)>0) return $erros; 
				
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			if((int)$id <= 0) $dados['idusuariocriacao'] = $this->_usuario['id'];
			if((int)$id <= 0) $dados["sequencial"] = $sequencial;
			if((int)$id <= 0) $dados["destino"] = $destino;
			if((int)$id <= 0) $dados["iddepartamento"] = $iddepartamento;
			if((int)$id <= 0) $dados["idescola"] = $idescola;
			$dados["descricoes"] = $descricoes;
			($idprioridade) ? $dados["idprioridade"] = $idprioridade : $idprioridade = 5;
			if( !$idprioridade ? $dados["idprioridade"] = NULL : $dados["idprioridade"] = $idprioridade  );
			$dados["idusuariologado"] = $idusuariologado;
			(!$idstatus AND $finalizar == NULL) ? $dados["idstatus"] = 6 : $dados["idstatus"] = $idstatus;
			if($dataagendada) $dados["dataagendada"] = date("Y-m-d", $dataagendada);
			$dados["finalizacao"] = $finalizacao;
			if($finalizacao=='Sim'){  
				$dados["finalizacaodata"] = $finalizacaodata;
				$dados["finalizacao"] = "Sim"; 
				$dados["idstatus"] = 8;
			}
			if ($atendido=='nao') $dados["idstatus"] = 7; 
			if ($atendido=='sim') $dados["idstatus"] = 8;
 
			if(Usuarios::getUsuario('idperfil') != 36){
				if($atendido=='nao'){
					$dados['statusatendimento'] = 'Não Atendido';
				} 

				if($atendido=='sim'){
					$dados['statusatendimento'] = 'Atendido';
				} 

				if(empty($atendido)){
					$dados['statusatendimento'] = 'Novo Chamado';
				} 
			}else{
				if($servicosmateriais != '' and $servicoesdescricoes != ''){
					$dados['statusatendimento'] = 'Finalizado';
					$dados['finalizacao'] = 'Sim';
				}else{
					$dados['statusatendimento'] = 'Chamado Respondido';
				}
			}

			if(Usuarios::getUsuario('idperfil') != "36")
						$dados["iddepartamentocriacao"] = $idlocal;

			$dados["atendido"] = $atendido;

			$dados["idtipo"] = $idtipo;
			$dados["idtipodaequipe"] = $idtipodaequipe;
			$dados["observacoes"] = $observacoes;
			$dados["obsEngenheiro"] = $obsEngenheiro;

			if($atendido == 'nao'): 
				$dados["servicoesdescricoes"] = "Solicitação não atendida!";
				$dados["servicosmateriais"] = "Solicitação não atendida!";
				$dados["finalizacao"] = 'Sim';
			else: 
				$dados["servicoesdescricoes"] = $servicoesdescricoes;
				$dados["servicosmateriais"] = $servicosmateriais;
			endif;

			if(isset($servicosmateriais) and $servicosmateriais != ''): 
				$dados["idstatus"] = 5;
			endif;
 
			$dados["idresponsavel"] = $idresponsavel;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');  
  
			$row = $chamados->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__ ."::incluir", Usuarios::getUsuario("id"), "Engenhariachamados " . $e->getMessage(), "E");
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}