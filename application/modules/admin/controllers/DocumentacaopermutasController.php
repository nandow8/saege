<?php

/**
 * Controle da classe documentacaopermutas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_DocumentacaopermutasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Documentacaopermuta
	 */
	protected $_usuario = null;	
	protected $_filterStatus = false;
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("documentacaopermutas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Documentacaopermutas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$row = $rows->save($row);

			if($row)
			{

				$aceites = new Documentacaopermutasaceites();
				$permutas = $aceites->getDocumentacaopermutasaceitesHelper(array('status' => 'Aguardando', 'idparent' => $id));
				foreach($permutas as $k=>$permuta)
				{
					$dados = array();
					$dados['id'] = $permuta['id'];
					$dados['status'] = 'Reprovado';
					$dados['excluido'] = 'nao';
					$dados['logusuario'] = $this->_usuario['id'];
					$dados['logdata'] = date('Y-m-d G:i:s');			
					
					$aceites->save($dados);
				}
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Permuta excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function aceitarpermutaAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Documentacaopermutas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {


			$ids_vinculos = Escolasvinculosprofessoresmaterias::getIdsEscolasvinculosprofessorByIdHelper((int)Usuarios::getUsuario('idfuncionario'));
			if(!$ids_vinculos) die('Você deve estar atribuído em alguma sala/turma.');

			$row = $row->toArray();

			//TODO CORRIGIR, CADA ESCOLA TEM SEUS VINCULOS, ENTAO NAO TEM COMO VALIDAR PARA UMA ESCOLA DIFERENTE
			//$ids_vinculos = explode(',', $ids_vinculos);
			//if(!in_array($row['idperiodo'], $ids_vinculos)) die('Somente é possível realizar permuta para uma mesma sala/turma que você atua.');


			$permutas = new Documentacaopermutasaceites();

			$dados = array();
			$dados['idparent'] = $row['id'];
			$dados['idprofessor'] = (int)Usuarios::getUsuario('idfuncionario');
			$dados['status'] = 'Aguardando';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');			
			
			//var_dump($dados);die();
			$permutas->save($dados);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Permuta aceita com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}	

	/**
	 * 
	 * Action para APROVAR PERMUTA via ajax e excluir a entidade
	 */
	public function aprovarpermutaAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		$idfuncionario = (int)$this->_request->getPost("idfuncionario");
		if($id <= 0 && $idfuncionario <= 0) die('Nenhum professor indicado para permuta ou solicitação não encontrada.');
		
		$rows = new Documentacaopermutasaceites();
		$row = $rows->fetchRow("status='Aguardando' AND idparent=$id AND idprofessor=$idfuncionario");
		
		if ($row) {

			$row = $row->toArray();

			$dados = array();
			$dados['id'] = $row['id'];
			$dados['status'] = 'Aceito';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');			
			
			$row = $rows->save($dados);

			if($row)
			{
				$permutas = Documentacaopermutasaceites::getDocumentacaopermutasaceitesHelper(array('status' => 'Aguardando', 'idparent' => $id));
				foreach($permutas as $k=>$permuta)
				{
					$dados = array();
					$dados['id'] = $permuta['id'];
					$dados['status'] = 'Reprovado';
					$dados['excluido'] = 'nao';
					$dados['logusuario'] = $this->_usuario['id'];
					$dados['logdata'] = date('Y-m-d G:i:s');			
					
					$rows->save($dados);
				}

				$documentacaopermutas = new Documentacaopermutas();
				$parent = $documentacaopermutas->getDocumentacaopermutaByIdHelper($id);

				if($parent)
				{ 
					$dados = array();
					$dados['id'] = $parent['id'];
					$dados['posicao'] = 'Aceito';
					$dados['excluido'] = 'nao';
					$dados['logusuario'] = $this->_usuario['id'];
					$dados['logdata'] = date('Y-m-d G:i:s');			
					
					$documentacaopermutas->save($dados);
				}
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Permuta aprovada com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}

	/**
	 * 
	 * Action para ser REJEITAR PERMUTA via ajax e excluir a entidade
	 */
	public function rejeitarpermutaAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		$idfuncionario = (int)$this->_request->getPost("idfuncionario");
		if($id <= 0 && $idfuncionario <= 0) die('Nenhum professor indicado para permuta ou solicitação não encontrada.');
		
		$rows = new Documentacaopermutasaceites();
		$row = $rows->fetchRow("status='Aguardando' AND idparent=$id AND idprofessor=$idfuncionario");
		
		if ($row) {

			$row = $row->toArray();

			$dados = array();
			$dados['id'] = $row['id'];
			$dados['status'] = 'Reprovado';
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($dados);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Permuta rejeitada com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}
	
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="documentacaopermutas") $objs = new Documentacaopermutas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaopermutas', 'name' => 'Solicitações de Permutas'),
			array('url' => null,'name' => 'Visualizar Solicitação de Permuta')
		);
		
		$id = (int)$this->_request->getParam("id");
		$documentacaopermutas = new Documentacaopermutas();
		$documentacaopermuta = $documentacaopermutas->getDocumentacaopermutaById($id, array());
		
		if (!$documentacaopermuta) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$solicitacoes = Documentacaopermutasaceites::getDocumentacaopermutasaceitesHelper(array('idparent' => $documentacaopermuta['id']));
		//var_dump($solicitacoes);die();

		$this->view->post_var = $documentacaopermuta;
		$this->view->permutas = $solicitacoes;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}

	public function aprovadasAction() {
 		$this->_helper->viewRenderer->setRender('index');
        $this->_filterStatus = 'Aceito';

        $this->view->filterStatus = $this->_filterStatus;
        $this->view->customTitle = "Solicitações de Permutas <b>Aprovadas</b>";

    	$this->indexAction();
	}

	public function reprovadasAction() {
 		$this->_helper->viewRenderer->setRender('index');
        $this->_filterStatus = 'Reprovado';

        $this->view->filterStatus = $this->_filterStatus;
        $this->view->customTitle = "Solicitações de Permutas <b>Aprovadas</b>";

    	$this->indexAction();
	}

	public function pendentesAction() {
 		$this->_helper->viewRenderer->setRender('index');
        $this->_filterStatus = 'Aguardando';

        $this->view->filterStatus = $this->_filterStatus;
        $this->view->customTitle = "Solicitações de Permutas <b>Pendentes</b>";

    	$this->indexAction();
	}
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Solicitações de Permutas')
		);
		
		$ns = new Zend_Session_Namespace('default_documentacaopermutas');
		$documentacaopermutas = new Documentacaopermutas();
		$queries = array();	
		if(Usuarios::getUsuario('professor') == 'Sim')
		{
			$queries['idescola'] = Usuarios::getEscolaAtiva((int)Usuarios::getUsuario('id'), 'id');
		}

		if ($this->_filterStatus) {
			$queries['idescola'] = null;
			$queries['posicao'] = $this->_filterStatus;
			$queries['idprofessor'] = (int)Usuarios::getUsuario('idfuncionario');
		}
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
			if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
			if ($this->view->post_var["idprofessor"]!="") $queries["idprofessor"] = $this->view->post_var["idprofessor"];
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
			    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $documentacaopermutas->getDocumentacaopermutas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $documentacaopermutas->getDocumentacaopermutas($queries, $paginaAtual, $maxpp);	
		
	}
	
	/**
	 * 
	 * Action de edição de documentacaopermutas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaopermutas', 'name' => 'Solicitações de Permutas'),
			array('url' => null,'name' => 'Editar Solicitação de Permuta')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$documentacaopermutas = new Documentacaopermutas();
		$documentacaopermuta = $documentacaopermutas->getDocumentacaopermutaById($id);
		
		if (!$documentacaopermuta) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$solicitacoes = Documentacaopermutasaceites::getDocumentacaopermutasaceitesHelper(array('idparent' => $documentacaopermuta['id']));
		//var_dump($solicitacoes);die();

		$this->view->post_var = $documentacaopermuta;
		$this->view->permutas = $solicitacoes;
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($documentacaopermuta);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Permuta editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de documentacaopermutas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaopermutas', 'name' => 'Solicitações de Permutas'),
			array('url' => null,'name' => 'Adicionar Solicitação de Permuta')
		);	

		//if (Usuarios::getUsuario('professor') != 'Sim') 
		//	$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Permuta adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $iddocumentacaopermuta
     */    
    private function preForm($iddocumentacaopermuta = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_documentacaopermuta = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		$horario = trim($this->getRequest()->getPost("horario"));
		$idtipoensino = (int)trim($this->getRequest()->getPost("idtipoensino"));
		$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idperiodo = (int)trim($this->getRequest()->getPost("idperiodo"));
		$observacao = trim($this->getRequest()->getPost("observacao"));
		$posicao = trim($this->getRequest()->getPost("posicao"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		$idsescolas = $this->getRequest()->getPost("idsescolas");

		//var_dump($idsescolas);die();				
		if (!is_array($idsescolas)) $idsescolas = array();
		$idsescolas = implode(',',$idsescolas);

		
		$erros = array();
		
		//if (""==$data) array_push($erros, "Informe a Data.");
		//if (0==$idprofessor) array_push($erros, "Informe a Professor.");
		if (sizeof($idsescolas)<=0) array_push($erros, "Informe a Unidade Escolas.");
		//if (""==$status && Usuarios::getUsuario('professor') != 'Sim') array_push($erros, "Informe a Status.");

		
		$documentacaopermutas = new Documentacaopermutas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idusuario"] = $idusuario;
			$dados["data"] = date("Y-m-d", $data);
			$dados["horario"] = $horario;
			$dados["idtipoensino"] = $idtipoensino;
			if(Usuarios::getUsuario('professor') == 'Sim' && $this->getRequest()->getActionName() != "editar")
			{
				$dados["idprofessor"] = $idprofessor;
			}
			$dados["idescola"] = $idescola;
			$dados["idperiodo"] = $idperiodo;
			$dados["observacao"] = $observacao;
			$dados["posicao"] = (Usuarios::getUsuario('professor') == 'Sim') ? 'Aguardando' : $posicao;
			$dados["status"] = 'Ativo';

			$dados["idsescolas"] = $idsescolas;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $documentacaopermutas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }

    public function setprofessorAction()
    {
		$this->_helper->layout->disableLayout();
		
		$idprofessor = (int)$this->getRequest()->getPost("idprofessor");
		if($idprofessor <= 0) die('');

		$funcionario = Usuarios::getUsuarioByIdFuncionarioHelper($idprofessor);
		//var_dump($funcionario);die();
		
		
		die("Não encontrado!");
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}