<?php
/**
 * Controle da classe nutricaovisitastecnicas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
//class MYPDF extends TCPDF {
//
//	//Page header
//	public function Header(){
//
//        $image_file = "public/admin/imagens/logosantaisabel.jpg";
//		$this->Image($image_file, 16, 15, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
//
//        //Set font
//		$this->SetFont('helvetica', 'B', 20);
//
//        $this->SetFont('helvetica', 'B', 10);
//
//        // cabeçalho Endereço
//        $this->SetXY(16, 12);
//        $this->Cell(0, 0, 'Município de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//        $this->SetFont('helvetica', '', 9);
//        $this->SetXY(35, 16);
//        $this->SetTextColor(130, 130, 130);
//        $this->Cell(0, 0, 'Nutrição - Visitas técnicas', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//        $this->SetFont('helvetica', '', 7);
//        $this->SetXY(35, 19);
//        $this->SetTextColor(130, 130, 130);
//        $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//        $this->SetFont('helvetica', '', 7);
//        $this->SetXY(35, 22);
//        $this->SetTextColor(130, 130, 130);
//        $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//        $this->SetXY(35, 25);
//        $this->SetTextColor(130, 130, 130);
//		$this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//
//        //informações Saege
//        $this->SetFont('helvetica', '', 8);
//        $this->SetXY(60,20);
//        $this->SetFillColor(127);
//        $this->SetTextColor(127);
//        $this->Cell(0, 0, 'Sistema SAEGE', 0, false, 'R', 0, '', 0, false, 'M', 'M');
//
//       	$nutricao = new Nutricaovisitastecnicas();
//       	$data = $nutricao->getCurrentData();
//
//        $this->SetFont('helvetica', '', 8);
//        $this->SetXY(60,25);
//        $this->SetFillColor(127);
//        $this->SetTextColor(127);
//        $this->Cell(0, 0, 'Data: '.$data, 0, false, 'R', 0, '', 0, false, 'M', 'M');
//
//        $this->SetFont('helvetica', '', 10);
//        $this->SetXY(60,30);
//        $this->SetFillColor(127);
//        $this->SetTextColor(127);
//        $this->Cell(0, 0, 'Sistema FJMX', 0, false, 'R', 0, '', 0, false, 'M', 'M');
//
//	}
//
//	// Page footer
//	public function Footer() {
//		// Position at 15 mm from bottom
//		$this->SetY(-15);
//		// Set font
//		$this->SetFont('helvetica', 'I', 8);
//		// Page number
//        $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
//
//        // Position at 15 mm from bottom
//		$this->SetXY(150,-15);
//		// Set font
//		$this->SetFont('helvetica', 'I', 8);
//		// Page number
//		$this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
//	}
//}
class Admin_NutricaovisitastecnicasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Nutricaovisitastecnica
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("nutricaovisitastecnicas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
		$nutricaomerendasdiferenciadas = new Nutricaovisitastecnicas();
		$queries = array();	
		$ultimo = $nutricaomerendasdiferenciadas->getNutricaovisitastecnicaByDestino($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Nutricaovisitastecnicas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Visita Técnica excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="nutricaovisitastecnicas") $objs = new Nutricaovisitastecnicas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaovisitastecnicas', 'name' => 'Visitas Técnicas'),
			array('url' => null,'name' => 'Visualizar Visita Técnica')
		);
		
		$id = (int)$this->_request->getParam("id");
		$nutricaovisitastecnicas = new Nutricaovisitastecnicas();
		$nutricaovisitastecnica = $nutricaovisitastecnicas->getNutricaovisitastecnicaById($id, array());
		
		if (!$nutricaovisitastecnica) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaovisitastecnica;
		$this->view->post_var['visualizar'] = 'visualizar';
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Visitas Técnicas')
		);
		
		$ns = new Zend_Session_Namespace('default_nutricaovisitastecnicas');
		$nutricaovisitastecnicas = new Nutricaovisitastecnicas();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
			if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
		}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $nutricaovisitastecnicas->getNutricaovisitastecnicas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $nutricaovisitastecnicas->getNutricaovisitastecnicas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de nutricaovisitastecnicas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaovisitastecnicas', 'name' => 'Visitas Técnicas'),
			array('url' => null,'name' => 'Editar Visita Técnica')
		);	
		
		$id = (int)$this->_request->getParam("id");
		$nutricaovisitastecnicas = new Nutricaovisitastecnicas();
		$nutricaovisitastecnica = $nutricaovisitastecnicas->getNutricaovisitastecnicaById($id);
	
		if (!$nutricaovisitastecnica) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaovisitastecnica;
		$this->view->post_var['editar'] = 'editar';
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($nutricaovisitastecnica);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Visita Técnica editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de nutricaovisitastecnicas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaovisitastecnicas', 'name' => 'Visitas Técnicas'),
			array('url' => null,'name' => 'Adicionar Visita Técnica')
		);	
		$this->view->adicionar = true;	
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Visita Técnica adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idnutricaovisitastecnica
     */    
    private function preForm($idnutricaovisitastecnica = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_nutricaovisitastecnica = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));

		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
		$horavisita = trim($this->getRequest()->getPost("horavisita"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idperfil = (int)trim($this->getRequest()->getPost("idperfil"));
		$nomefuncionario = trim($this->getRequest()->getPost("nomefuncionario"));
		$funcao = trim($this->getRequest()->getPost("funcao"));
		$idusuariovisita = (int)trim($this->getRequest()->getPost("idusuariovisita"));
		$equipeproducaocompleta = trim($this->getRequest()->getPost("equipeproducaocompleta"));
		$producoesdescricoes = trim($this->getRequest()->getPost("producoesdescricoes"));
		$producoesobservacoes = trim($this->getRequest()->getPost("producoesobservacoes"));
		$producoesorganizacoeshigienes = trim($this->getRequest()->getPost("producoesorganizacoeshigienes"));
		$estoqueorganizacoeshigienesexecutado = trim($this->getRequest()->getPost("estoqueorganizacoeshigienesexecutado"));
		$estoqueorganizacoeshigienesdeestoque = trim($this->getRequest()->getPost("estoqueorganizacoeshigienesdeestoque"));
		$estoqueorganizacoeshigienes = trim($this->getRequest()->getPost("estoqueorganizacoeshigienes"));
		$amostrasalimentocoletadas = trim($this->getRequest()->getPost("amostrasalimentocoletadas"));
		$obs_estoqueorganizacoeshigienesexecutado = trim($this->getRequest()->getPost("obs_estoqueorganizacoeshigienesexecutado"));
		$obs_estoqueorganizacoeshigienesdeestoque = trim($this->getRequest()->getPost("obs_estoqueorganizacoeshigienesdeestoque"));
		$obs_producoesorganizacoeshigienes = trim($this->getRequest()->getPost("obs_producoesorganizacoeshigienes"));
		$cardapioprogramado = trim($this->getRequest()->getPost("cardapioprogramado"));
		$cardapioservido = trim($this->getRequest()->getPost("cardapioservido"));
		$cardapioobservacao = trim($this->getRequest()->getPost("cardapioobservacao"));
		$fluxograma = trim($this->getRequest()->getPost("fluxograma"));
		$equipamentosfuncionamentos = trim($this->getRequest()->getPost("equipamentosfuncionamentos"));
		$cardapiodiaexecutado = trim($this->getRequest()->getPost("cardapiodiaexecutado"));
		$equipamentosdesuso = trim($this->getRequest()->getPost("equipamentosdesuso"));
		$episfuncionarios = trim($this->getRequest()->getPost("episfuncionarios"));
		$termometro = trim($this->getRequest()->getPost("termometro"));
		$controlepragas = trim($this->getRequest()->getPost("controlepragas"));
		$caixadagua = trim($this->getRequest()->getPost("caixadagua"));
		$observacoes = trim($this->getRequest()->getPost("observacoes"));
		$status = trim($this->getRequest()->getPost("status1"));
		$iddocumento = (int)trim($this->getRequest()->getPost("iddocumento"));
			
		$erros = array();
		
		if (""==$datalancamento) array_push($erros, "Informe a Data de lançamento.");
		if (0==$idescola) array_push($erros, "Informe a Escola.");
		if (""==$nomefuncionario) array_push($erros, "Informe a Nome do funcionário que o recebeu.");
		if (0==$idusuariovisita) array_push($erros, "Informe a Visita técnica feita por.");
		if (""==$status) array_push($erros, "Informe a Status.");
				
		$nutricaovisitastecnicas = new Nutricaovisitastecnicas();	
		
		
		if (sizeof($erros)>0) return $erros;
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["sequencial"] = $sequencial;
			$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			$dados["horavisita"] = $horavisita;
			$dados["idescola"] = $idescola;
			$dados["idperfil"] = $idperfil;
			$dados["nomefuncionario"] = $nomefuncionario;
			$dados["funcao"] = $funcao;
			$dados["idusuariovisita"] = $idusuariovisita;
			$dados["equipeproducaocompleta"] = $equipeproducaocompleta;
			$dados["producoesdescricoes"] = $producoesdescricoes;
			$dados["producoesobservacoes"] = $producoesobservacoes;
			$dados["producoesorganizacoeshigienes"] = $producoesorganizacoeshigienes;
			$dados["estoqueorganizacoeshigienesexecutado"] = $estoqueorganizacoeshigienesexecutado;
			$dados["estoqueorganizacoeshigienesdeestoque"] = $estoqueorganizacoeshigienesdeestoque;
			$dados["estoqueorganizacoeshigienes"] = $estoqueorganizacoeshigienes;
			$dados["amostrasalimentocoletadas"] = $amostrasalimentocoletadas;
			$dados["cardapiodiaexecutado"] = $cardapiodiaexecutado;
			$dados["cardapioprogramado"] = $cardapioprogramado;
			$dados["cardapioservido"] = $cardapioservido;
			$dados["cardapioobservacao"] = $cardapioobservacao;
			$dados["fluxograma"] = $fluxograma;
			$dados["equipamentosfuncionamentos"] = $equipamentosfuncionamentos;
			$dados["equipamentosdesuso"] = $equipamentosdesuso;
			$dados["episfuncionarios"] = $episfuncionarios;
			$dados["termometro"] = $termometro;
			$dados["controlepragas"] = $controlepragas;
			$dados["caixadagua"] = $caixadagua;
			$dados["observacoes"] = $observacoes;
			$dados["obs_estoqueorganizacoeshigienesexecutado"] = $obs_estoqueorganizacoeshigienesexecutado;
			$dados["obs_estoqueorganizacoeshigienesdeestoque"] = $obs_estoqueorganizacoeshigienesdeestoque;
			$dados["obs_producoesorganizacoeshigienes"] = $obs_producoesorganizacoeshigienes;
			$dados["status"] = $status;

			$iddocumento = $this->getArquivo('iddocumento');
			if($iddocumento!=0)
				$dados['iddocumento'] = $iddocumento;
			

			if($status == 'Ciente'){
				$dados['dataciente'] = date("Y-m-d H:i:s");
				$dados['idusuario'] = Usuarios::getUsuario('id');
			}

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

			$row = $nutricaovisitastecnicas->save($dados);
		   
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	}
	
	public function gerardocumentonutricaovisitasAction(){		
		$id = $this->_request->getParam('id');

		$visitas = Nutricaovisitastecnicas::getNutricaovisitastecnicaByIdHelper($id);

		$idciente = $visitas['idusuario'];
		$func = Usuarios::getUsuarioByIdHelper($idciente);
		
		$escola = Escolas::getEscolaByIdHelper($visitas['idescola']);
		
		$idusuariovisita = $visitas['idusuariovisita'];
		$usuariovisita = Funcionariosgeraisescolas::getFuncionariosgeraisescolas(array('id'=>$idusuariovisita));

		$this->view->usuariovisita = $usuariovisita[0];
		$this->view->escola = $escola['escola'];
		$this->view->nomeciente = $func['nomerazao'].' '.$func['sobrenomefantasia'];
		
		$this->view->visitas = $visitas;

		$this->preForm();
        

        $db = Zend_Registry::get('db');

        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $this->view->pdf = $pdf;

        $pdf->setPageOrientation('p');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Visitas Técnicas');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->SetFont('helvetica', '', 9);
        // add a page
        $pdf->AddPage();

        $html = $this->view->render('nutricaovisitastecnicas/pdf/gerardocumentonutricaovisitas.phtml');

        $pdf->writeHTML($html, true, 0, true, 0);

        
        $filename = 'nutricaovisitas.pdf';
        $pdf->Output($filename, 'I');
        die();
        return true;


	}
    
}