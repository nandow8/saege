<?php
	
/**
 * Controle da classe levantamento de patrimonios do sistema
 *
 * @author		Rafael Alves	
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_PatrimoniostermosController extends Zend_Controller_Action {
	/**
	* Variável protegida contendo os dados do usuário logado
	* var $_usuario
	*/
	protected $_usuario = null;

	public function preDispatch(){
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("patrimoniostermos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$patrimoniostermos = new Patrimoniostermos();
		$queries = array();	
		$ultimo = $patrimoniostermos->getUltimoPatrimoniotermos($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datalancamento']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Patrimoniostermos();
		$row = $rows->fetchRow("id=".$id);
			

		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logdata'] = date('Y-m-d G:i:s');			
			$row['logusuario'] = $this->_usuario['id'];
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Patrimônio excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		

	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {

		$this->view->bread_crumb = array(
			array('url' => 'patrimoniostermos', 'name' => 'Termos de Aceite dos Patrimônios'),
			array('url' => null,'name' => 'Visualizar Termos de Aceite')
		);
		
		$id = (int)$this->_request->getParam("id");
		$patrimoniostermos = new Patrimoniostermos();
		$patrimoniostermo = $patrimoniostermos->getPatrimoniostermosaceiteById($id, array());
		
		if (!$patrimoniostermo)
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $patrimoniostermo;
		$this->view->visualizar = true;
		$this->preForm();
		
		return true;
	}

	public function indexAction(){
		$this->view->bread_crumb = array(
			array('url'=>false, 'name'=>'Termos de aceite dos patrimônios')			
		);

		$ns = new Zend_Session_Namespace('default_patrimoniostermos');
		$patrimoniostermos = new Patrimoniostermos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}

    	$dados = Usuarios::getUsuarios(array('id'=>Usuarios::getUsuario('id')));
		$_data = $dados[0];
		$perfil = $_data['idperfil'];

		if($perfil == 29):
			$escola = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'));
			$queries['idescola'] = $escola['id'];
 		endif;

 		if(!UsuariosPerfis::isAllowedCrudPatrimonio($perfil)) $queries['iddepartamento'] = $perfil;

    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);

    	if(isset($this->view->post_var)){
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);

    		if($this->view->post_var['sequencial']!="") $queries['sequencial'] = $this->view->post_var['sequencial'];
    		//if($this->view->post_var['horalancamento'] != "") $queries['horalancamento'] = $this->view->post_var['horalancamento'];

    		if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
			if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));

			if($this->view->post_var['iddepartamento'] != "") $queries["iddepartamento"] = $this->view->post_var["iddepartamento"];
			
			if(isset($this->view->post_var['idescola']) and $this->view->post_var['idescola'] != "") $queries["idescola"] = $this->view->post_var['idescola'];

			if($this->view->post_var['status1'] != "") $queries["status"] = $this->view->post_var['status1'];

			//if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}
    	//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $patrimoniostermos->getPatrimoniostermosaceite($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $patrimoniostermos->getPatrimoniostermosaceite($queries, $paginaAtual, $maxpp);
	}
	/**
	 * 
	 * Action de adição de termos para aceite de patrimônios
	 */
	public function adicionarAction(){
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniostermos', 'name' => 'Termos de patrimônios'),
			array('url' => null,'name' => 'Adicionar Termos Patrimônio')
		);
		$this->view->adicionar = true;
		$this->preForm();

		if($this->getRequest()->isPost()){
			$erros = $this->getPost(false);

			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Termo de aceito adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;
	}

	/**
	* Controller para edição das informações
	*/
	public function editarAction(){
		$this->view->bread_crumb = array(
			array('url' => 'patrimoniostermos', 'name' => 'Termos de patrimônios'),
			array('url' => null,'name' => 'Editar Termos Patrimônio')
		);

		$id = (int)$this->_request->getParam('id');
		if($id==0) return false;

		$patrimoniostermos = new Patrimoniostermos();
		$queries= array();
		$patrimoniostermo = $patrimoniostermos->getPatrimoniostermosaceiteById($id, $queries);

		if(!$patrimoniostermo)
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

		$this->view->post_var = $patrimoniostermo;
		$this->view->editar = true;
		$this->preForm();

		if ($this->_request->isPost()) {
			$erros = $this->getPost($patrimoniostermo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Termo editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}

		return true;
	}

	private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }
	 /**
     * Atribui valores ao view
     * @param int $idpatrimoniobaixabem
     */    
    private function preForm($_patrimoniostermos = 0) {
    }
    
	/**
	 * Valida e grava os dados do formulário
	 */
	private function getPost($_patrimoniostermos = false){
		$iddepartamentotemp = (isset($this->view->post_var['iddepartamento']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamento'])) ? (int)$this->view->post_var['iddepartamento'] : (int)trim($this->getRequest()->getPost("iddepartamento"));

		$idescolatemp = (isset($this->view->post_var['iddepartamento']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamento'])) ? (int)$this->view->post_var['idescola'] : (int)trim($this->getRequest()->getPost("idescola"));

		$observacoestemp = (isset($this->view->post_var['observacoes']) && !UsuariosPerfis::isAllowedCrudPatrimonio($this->view->post_var['iddepartamento'])) ? $this->view->post_var['observacoes'] : trim($this->getRequest()->getPost("observacoes"));

		if (!isset($this->view->post_var))  $this->view->post_var = $_POST;
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);

		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$datalancamento_i = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento_i"));
		$datalancamento_f = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento_f"));

		$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
		$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
		$iddepartamento = $iddepartamentotemp;
		$idescola = $idescolatemp;
		$iddocumento = $this->getRequest()->getPost("iddocumento");
		$observacoes = $observacoestemp;
		$status = $this->getRequest()->getPost("status1");

		$erros = array();
		
		$iddoc = (isset($iddocumento) && $iddocumento != 0) ? $iddocumento : $this->getArquivo('iddocumento');

		if($iddoc==""):
			array_push($erros, "O documento deve ser anexado");
		else:
			$iddocumento = $iddoc;
		endif;
		
		if($iddepartamento=="") array_push($erros, "Informe o departamento");		
		$patrimoniostermos = new Patrimoniostermos;

		if(sizeof($erros)>0) return $erros;

		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try{

			$dados = array();

			$dados['id'] = $id;
			$dados['sequencial'] = $sequencial;
			$dados["datalancamento_i"] = date("Y-m-d", $datalancamento_i);
			$dados["datalancamento_f"] = date("Y-m-d", $datalancamento_f);
			$dados['horalancamento'] = $horalancamento;
			$dados['idusuario'] = $idusuario;
			$dados['iddepartamento'] = $iddepartamento;
			$dados['iddocumento'] = $iddocumento;				
			$dados['idescola'] = $idescola;
			$dados['observacoes'] = $observacoes;
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');

			$patrimoniostermos->save($dados);

			$db->commit();
		}catch(Exception $e){
			echo $e->getMessage();

			$db->rollBack();
			die();
		}		
	}
}

?>
