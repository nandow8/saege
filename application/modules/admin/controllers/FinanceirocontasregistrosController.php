<?php

// class MYPDF extends TCPDF {

//     //Page header
//     public function Header() {}

//     // Page footer
//     public function Footer() {
//         // Position at 15 mm from bottom
//         $this->SetY(-15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
//     }
// }

/**
 * Controle da classe financeirocontasregistros do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
error_reporting(0);

class Admin_FinanceirocontasregistrosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Financeirocontaregistro
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("financeirocontasregistros", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Financeirocontasregistros();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Registro Financeiro excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="financeirocontasregistros") $objs = new Financeirocontasregistros();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	public function setbancosAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$idconvenio = (int)$this->getRequest()->getPost('idconvenio');
		//$idescola = (int)$this->getRequest()->getPost('idescola');
		//var_dump($idconvenio);die();
		//$tipoconvenio = Financeirotiposconvenios::getFinanceirotipoconvenioByIdHelper($id);
		
		$queries = array();
		$queries['idparent'] = $idconvenio;
		//$queries['idescola'] = $idescola;
		 
		$escolasprogramascontas = new Escolasprogramascontas();
		$rows = $escolasprogramascontas->getEscolasprogramascontasHelper($queries);
		//var_dump($rows);die();


		$this->view->rows = $rows;

	}
	
	public function setconveniosAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$id = (int)$this->getRequest()->getPost('idtipoconvenio');
		$idescola = (int)Usuarios::getUsuario('idescola');
		//$tipoconvenio = Financeirotiposconvenios::getFinanceirotipoconvenioByIdHelper($id);
		
		$queries = array();
		//$queries['tipo'] = $tipoconvenio['titulo'];
		$queries['idescola'] = $idescola;
		$queries['idtipoconvenio'] = $id;

		if($id) $queries['idtipoconvenio'] = $id;
		if($idescola) $queries['idescola'] = $idescola;
		 
		$escolasprogramascontas = new Escolasprogramasgov();
		$rows = $escolasprogramascontas->getEscolasprogramasgov($queries);
		//var_dump($rows);die();

		$this->view->rows = $rows;
		//die('OK');
	}
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeirocontasregistros', 'name' => 'Registros Financeiros'),
			array('url' => null,'name' => 'Visualizar Registro Financeiro')
		);
		
		$id = (int)$this->_request->getParam("id");
		$financeirocontasregistros = new Financeirocontasregistros();
		$financeirocontaregistro = $financeirocontasregistros->getFinanceirocontaregistroById($id, array());
		
		if (!$financeirocontaregistro) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $financeirocontaregistro;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Registros Financeiros')
		);
		
		$ns = new Zend_Session_Namespace('default_financeirocontasregistros');
		$financeirocontasregistros = new Financeirocontasregistros();
		$queries = array();	

		$queries['idescola'] = Usuarios::getUsuario('idescola');
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
		if(Usuarios::getUsuario('idescola') == 0){
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
		}else{
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = Usuarios::getUsuario('idescola');
		}
		
		if ($this->view->post_var["idconvenio"]!="") $queries["idconvenio"] = $this->view->post_var["idconvenio"];
		if ($this->view->post_var["exercicio"]!="") $queries["exercicio"] = $this->view->post_var["exercicio"];
		if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
		if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
		if ($this->view->post_var["natureza"]!="") $queries["natureza"] = $this->view->post_var["natureza"];
		if ($this->view->post_var["tipo"]!="") $queries["tipo"] = $this->view->post_var["tipo"];
		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];

    	}

    	if(($this->view->post_var["idescola"] > 0 || Usuarios::getUsuario('idescola') > 0) && $this->view->post_var["idconvenio"] > 0)
    	{
    		$this->view->idescola = ($this->view->post_var["idescola"] > 0) ? $this->view->post_var["idescola"] : Usuarios::getUsuario('idescola') ;
    		$this->view->idconvenio = $this->view->post_var["idconvenio"];
    	}
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $financeirocontasregistros->getFinanceirocontasregistros($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $financeirocontasregistros->getFinanceirocontasregistros($queries, $paginaAtual, $maxpp);	
	
		if (($this->getRequest()->getParam('to')=='pdf') || ($this->getRequest()->getParam('to')=='xls')) 
		{
			if(($this->getRequest()->getParam('to')=='pdf'))
			{
				$tipo = $this->getRequest()->getParam('tipo');
				$this->gerapdf($queries);
			}
			else
			{

				$fields = array(
						'sequencial' => array(
								'label'=>'Sequencial',
								'width'=> 10,
						),
						'escola' => array(
								'label'=>'Escola',
								'width'=> 20,
						),
						'tipoconvenio' => array(
								'label'=>'Tipo de Convênio',
								'width'=> 10,
						),
						'convenio' => array(
								'label'=>'Convênio / Projeto',
								'width'=> 20,
						),
						'tipo' => array(
								'label'=>'Tipo',
								'width'=> 20,
						),
						'tipolancamento' => array(
								'label'=>'Tipo de Lançamento',
								'width'=> 10,
						),
						'natureza' => array(
								'label'=>'Natureza',
								'width'=> 10,
						),
						'valor' => array(
								'label'=>'Valor',
								'width'=> 10,
						),
						'data' => array(
								'label'=>'Data',
								'width'=> 10,
						),	
						'fornecedor' => array(
								'label'=>'Fornecedor',
								'width'=> 30,
						),
						'cpfcnpj' => array(
								'label'=>'CNPJ do Fornecedor',
								'width'=> 30,
						),
						'descricao' => array(
								'label'=>'Descrição',
								'width'=> 10,
						),	
						'tipodocumento' => array(
								'label'=>'Tipo de Documento',
								'width'=> 10,
						),	
						'ndocumento' => array(
								'label'=>'Nº Documento',
								'width'=> 10,
						),	
						'faturado' => array(
								'label'=>'Faturado',
								'width'=> 10,
						)
				);				
				//var_dump($this->view->rows);die();
				foreach ($this->view->rows as $k=>$row) {

					$escola = Escolas::getEscolaByIdHelper($row['idescola']);
					$row['escola'] = ($escola) ? $this->view->formatTextoBranco($escola['escola']) : '--';	

					$tipoconvenio = Financeirotiposconvenios::getFinanceirotipoconvenioByIdHelper($row['idtipoconvenio']);
					$row['tipoconvenio'] = ($tipoconvenio) ? $this->view->formatTextoBranco($tipoconvenio['titulo']) : '--';

					$convenio = Escolasprogramasgov::getEscolaprogramagovByIdHelper($row['idconvenio']);
					$row['convenio'] = ($convenio) ? $this->view->formatTextoBranco($convenio['titulo']) : '--';

					if($row['tipo'] == 'Receita')
					{						
						$row['tipolancamento'] = $row['tiporeceita'];
					}
					else
					{					
						$row['tipolancamento'] = $row['despesa'];
					}


					$row['fornecedor'] = ($row['fornecedor'] != '') ? $this->view->formatTextoBranco($row['fornecedor']) : '--';
					$row['cpfcnpj'] = ($row['cpfcnpj'] != '') ? $this->view->formatTextoBranco($row['cpfcnpj']) : '--';	

					$row['valor'] = number_format($row['valor'], 2, ',', '.');
					$row['data'] = date('d/m/Y', strtotime($row['data']));
				
					$this->view->rows[$k] = $row;
				}
				
				try {				
					$relatorioExcel = new RelatorioExcel($this, $this->view->rows);
					$relatorioExcel->generate($this->view->getController() . '.xlsx', $fields);
					die();
					
				
				} catch (Exception $e) {
					echo $e->getMessage();
					die();
				}
			}
		}

	}
	
	private function gerapdf($queries) 
	{
		$idescola = (int)Usuarios::getUsuario('idescola');
		$idescola = ($idescola == 0) ? $queries['idescola'] : $idescola;
		$idconvenio = (isset($queries['idconvenio']) && (int)$queries['idconvenio'] > 0) ? (int)$queries['idconvenio'] : 0;

		//$apm = Escolasapms::getEscolaapmByIdEscolaHelper($idescola, array('tipo' => $tipo));
		//$composicao_apm = Escolasapmsgrupos::getEscolasapmsgrupoByIdHelper($idescola, array('status' => 'Ativo'));

		if ($idescola == 0 || $idconvenio == 0)
		{
			$messageNameSpace = new Zend_Session_Namespace("message");
			$messageNameSpace->crudmessage = "Selecione uma UNIDADE ESCOLAR e PROGRAMA.";
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		}

		//$_data = Financeirocontasregistros::getFinanceirocontasregistros(array('idescola'=>$idescola));

		$programa = Escolasprogramasgov::getEscolaprogramagovByIdHelper($idconvenio);		
		$financeirotiposconvenios = new Financeirotiposconvenios();


		$financeirocontasregistros = new Financeirocontasregistros();
		$financeirocontaregistro = $financeirocontasregistros->getFinanceirocontaregistroById($programa['id']);

		$idtipoconvenio = $programa['idtipoconvenio'];
		$tipoconvenio = Financeirotiposconvenios::getFinanceirotipoconvenioByIdHelper($idtipoconvenio);

		$this->view->programa = $programa;
		$this->view->row = $programa;

		$this->view->datainicio = isset($queries['data_i']) ? date('d/m/Y', strtotime($queries['data_i'])) : '--';
		$this->view->datafim = isset($queries['data_f']) ? date('d/m/Y', strtotime($queries['data_f'])) : '--';

		//$this->view->apm = $apm;
		//$this->view->composicao_apm = $composicao_apm;

		$escola = Escolas::getEscolaByIdHelper($idescola);
		$this->view->escola = $escola;
		//var_dump($escola);die();

		//$conta = Escolasapmscontas::getApmByIdParentHelper($apm['id'], array('idescola' =>$escola['id']));
		$conta = Escolasprogramascontas::getEscolasprogramascontasHelper(array('idescola' =>$escola['id'], 'idparent'=>$programa['id']));
		
		if($conta){
			unset($conta['id']);
			unset($conta['excluido']);
			$this->view->conta = $conta;
		}
		
		//$this->view->post_var = $declaracao;
		$endereco = Enderecos::getEnderecoById((int)$escola['idendereco']);

		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->endereco = $endereco;
		}

		$this->view->receitas = Financeirocontasregistros::getFinanceirocontasregistrosHelper(array('idescola'=>$idescola, 'idconvenio' => $idconvenio, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'tipo' => 'Receita'));
		//var_dump($this->view->receitas); die();
		$this->view->despesas = Financeirocontasregistros::getFinanceirocontasregistrosHelper(array('idescola'=>$idescola, 'idconvenio' => $idconvenio, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'tipo' => 'Despesa'));

		$this->view->total_despesas = Financeirocontasregistros::getFinanceirocontasregistrosHelper(array('idescola'=>$idescola, 'idconvenio' => $idconvenio, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'sum' => true, 'tipo' => 'Despesa'));

		$this->view->total_receitas = Financeirocontasregistros::getFinanceirocontasregistrosHelper(array('idescola'=>$idescola, 'idconvenio' => $idconvenio, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'sum' => true, 'tipo' => 'Receita'));
		//var_dump($this->view->receitas); die();
		$this->view->total_despesas_custeio = Financeirocontasregistros::getFinanceirocontasregistrosHelper(array('idescola'=>$idescola, 'idconvenio' => $idconvenio, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'sum' => true, 'tipo' => 'Despesa', 'natureza' => 'Custeio'));
		$this->view->total_despesas_capital = Financeirocontasregistros::getFinanceirocontasregistrosHelper(array('idescola'=>$idescola, 'idconvenio' => $idconvenio, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'sum' => true, 'tipo' => 'Despesa', 'natureza' => 'Capital'));

		$this->view->total_receitas_custeio = Financeirocontasregistros::getFinanceirocontasregistrosHelper(array('idescola'=>$idescola, 'idconvenio' => $idconvenio, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'sum' => true, 'tipo' => 'Receita', 'natureza' => 'Custeio'));

		$this->view->total_receitas_capital = Financeirocontasregistros::getFinanceirocontasregistrosHelper(array('idescola'=>$idescola, 'idconvenio' => $idconvenio, 'data_i' => $queries['data_i'], 'data_f' => $queries['data_f'], 'sum' => true, 'tipo' => 'Receita', 'natureza' => 'Capital'));
		$this->preForm();
		
		$db = Zend_Registry::get('db');
		
		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('l');

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
		$pdf->SetTitle('Relatório Financeiro');
		$pdf->SetSubject('Registros Financeiros de Programas das Escola');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 15, 10);

		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
		// add a page
		$pdf->AddPage();

		similar_text($tipoconvenio['titulo'], "APM", $apm);
		similar_text($tipoconvenio['titulo'], "PDDE", $pdde);

		$html = $this->view->render('financeirocontasregistros/pdf/index.phtml');
		if($pdde > 50)
		{
			$html = $this->view->render('financeirocontasregistros/pdf/pdde.phtml');
		}
		
		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'relatorio_financeiro_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'I');
	
		die();
	
		return true;
	}
	
	/**
	 * 
	 * Action de edição de financeirocontasregistros
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeirocontasregistros', 'name' => 'Registros Financeiros'),
			array('url' => null,'name' => 'Editar Registro Financeiro')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$financeirocontasregistros = new Financeirocontasregistros();
		$financeirocontaregistro = $financeirocontasregistros->getFinanceirocontaregistroById($id);

		if (!$financeirocontaregistro) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $financeirocontaregistro;
		$this->view->editar = true;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($financeirocontaregistro);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Registro Financeiro editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de financeirocontasregistros 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeirocontasregistros', 'name' => 'Registros Financeiros'),
			array('url' => null,'name' => 'Adicionar Registro Financeiro')
		);	
					    	
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Registro Financeiro adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idfinanceirocontaregistro
     */    
    private function preForm($idfinanceirocontaregistro = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_financeirocontaregistro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idlocal = (int)trim($this->getRequest()->getPost("idlocal"));
		$idtipoconvenio = (int)trim($this->getRequest()->getPost("idtipoconvenio"));
		$idconvenio = (int)trim($this->getRequest()->getPost("idconvenio"));
		$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$fornecedor = trim($this->getRequest()->getPost("fornecedor"));
		$cpfcnpj = trim($this->getRequest()->getPost("cpfcnpj"));
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		$lancamento = trim($this->getRequest()->getPost("lancamento"));
		$idbanco = (int)trim($this->getRequest()->getPost("idbanco"));
		$banco = trim($this->getRequest()->getPost("banco"));
		$agencia = trim($this->getRequest()->getPost("agencia"));
		$conta = trim($this->getRequest()->getPost("conta"));
		$titulo = trim($this->getRequest()->getPost("titulo"));
		$valor = MN_Util::trataNum(trim($this->getRequest()->getPost("valor")));
		$datapagamento = Mn_Util::stringToTime($this->getRequest()->getPost("datapagamento"));
		$natureza = trim($this->getRequest()->getPost("natureza"));
		$tipo = trim($this->getRequest()->getPost("tipo"));
		$origem = trim($this->getRequest()->getPost("origem"));
		$tiporeceita = trim($this->getRequest()->getPost("tiporeceita"));
		$tipodespesa = trim($this->getRequest()->getPost("tipodespesa"));
		$descricao = trim($this->getRequest()->getPost("descricao"));
		$observacao = trim($this->getRequest()->getPost("observacao"));
		$tipodocumento = trim($this->getRequest()->getPost("tipodocumento"));
		$ndocumento = trim($this->getRequest()->getPost("ndocumento"));
		$iddocumento = (int)trim($this->getRequest()->getPost("iddocumento"));
		$faturado = trim($this->getRequest()->getPost("faturado"));
		$numerocheque = trim($this->getRequest()->getPost("numerocheque"));

		$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
		if (0==$idtipoconvenio) array_push($erros, "Informe o Tipo de Convenio.");
		if (0==$idconvenio) array_push($erros, "Informe o Convenio.");
		//if (""==$lancamento) array_push($erros, "Informe a Lançamento.");
		if (0==$idbanco) array_push($erros, "Informe a Banco.");
		if (""==$tipo) array_push($erros, "Informe a Tipo.");
		if (""==$natureza) array_push($erros, "Informe a Natureza da Operação.");
		if (""==$status) array_push($erros, "Informe a Status.");

		
		$financeirocontasregistros = new Financeirocontasregistros();
		
		
		
		if (sizeof($erros)>0) return $erros; 

		$___rows = Escolasprogramascontas::getEscolasprogramascontasHelper(array('id'=>$idbanco));
		$___row = (sizeof($___rows)>0) ? 	$___rows[0] : false;	

		$banco = ($___row) ? $___row['banco'] : '--';		
		$agencia = ($___row) ? $___row['agencia'] : '--';		
		$conta = ($___row) ? $___row['conta'] : '--';		

		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;

			$iddocumento = $this->getArquivo('iddocumento');
			if ($iddocumento!=0) $dados['iddocumento'] = $iddocumento;

			$idcheque = $this->getArquivo('idcheque');
			if ($idcheque!=0) $dados['idcheque'] = $idcheque;
			
			$dados["origem"] = $origem;
			$dados["idescola"] = $idescola;
			$dados["idlocal"] = $idlocal;
			$dados["idtipoconvenio"] = $idtipoconvenio;
			$dados["idconvenio"] = $idconvenio;
			$dados["idusuariologado"] = $idusuariologado;
			$dados["sequencial"] = $sequencial;
			$dados["fornecedor"] = $fornecedor;
			$dados["cpfcnpj"] = $cpfcnpj;
			$dados["data"] = date("Y-m-d", $data);
			$dados["lancamento"] = $lancamento;
			$dados["idbanco"] = $idbanco;
			$dados["banco"] = $banco;
			$dados["agencia"] = $agencia;
			$dados["conta"] = $conta;
			$dados["titulo"] = $titulo;
			$dados["valor"] = $valor;
			$dados["datapagamento"] = date("Y-m-d", $datapagamento);
			$dados["natureza"] = $natureza;
			$dados["tipo"] = $tipo;
			$dados["tiporeceita"] = $tiporeceita;
			$dados["tipodespesa"] = $tipodespesa;
			$dados["descricao"] = $descricao;
			$dados["observacao"] = $observacao;
			$dados["tipodocumento"] = $tipodocumento;
			$dados["ndocumento"] = $ndocumento;
			$dados["numerocheque"] = $numerocheque;
			$dados["faturado"] = 'nao';
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
						
			$row = $financeirocontasregistros->save($dados);
			if($sequencial == ""){
                $this->setSequencial($row['id']);
            }
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
    /**     
    * Atualiza sequencial
    */
    public function setSequencial($id) {

        $rows = new Financeirocontasregistros();
        $linha = $rows->fetchRow('id=' . $id);

        $row = $linha->toArray(); 
        $row['sequencial'] = $row['id'] . '/' . substr($row['datacriacao'],0,4);
        
        $rows->save($row);

        return true;
    } 
}