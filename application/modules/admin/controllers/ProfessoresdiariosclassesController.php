<?php

/**
 * Controle da classe professoresdiariosclasses do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_ProfessoresdiariosclassesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Escolasalunosfalta
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("professoresdiariosclasses", $this->_request->getActionName());	
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Escolasalunosfaltas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "falta excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="professoresdiariosclasses") $objs = new Escolasalunosfaltas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'professoresdiariosclasses', 'name' => 'Frequências'),
			array('url' => null,'name' => 'Visualizar Frequência')
		);
		
		$id = (int)$this->_request->getParam("id");
		$professoresdiariosclasses = new Escolasalunosfaltas();
		$escolasalunosfalta = $professoresdiariosclasses->getEscolasalunosfaltaById($id, array());
		
		if (!$escolasalunosfalta) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolasalunosfalta;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Diário de Classes')
		);
		
		$ns = new Zend_Session_Namespace('admin_professoresdiariosclasses');
		$escolasvinculos = new Escolasvinculos();
		$queries = array();	
		$queries['idescola'] = Usuarios::getUsuario('idescola'); 

		if((int)$queries['idescola'] > 0){
			$escolasvinculosprofessoresmaterias = Escolasvinculosprofessoresmaterias::getIdsEscolasvinculosprofessorByIdHelper(Usuarios::getUsuario('idfuncionario'));

			if((isset($escolasvinculosprofessoresmaterias)) && ($escolasvinculosprofessoresmaterias) && ($escolasvinculosprofessoresmaterias!="")){
				$queries['ids']	= $escolasvinculosprofessoresmaterias;
			}
						
		}
		
		//$queries['idprofessor'] = Usuarios::getUsuario('idfuncionario'); 
		//$queries['idsescola'] = Escolasusuarios::getUsuario('idescola'); 
		//var_dump((int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id')); die();
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);

			if ($this->view->post_var["idserie"]!="") $queries["idserie"] = $this->view->post_var["idserie"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
			    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $escolasvinculos->getEscolasvinculos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;
		
		$this->view->rows = $escolasvinculos->getEscolasvinculos($queries, $paginaAtual, $maxpp);
	}
	
	/**
	 * 
	 * Action de edição de professoresdiariosclasses
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'professoresdiariosclasses', 'name' => 'Diário de Classess'),
			array('url' => null,'name' => 'Editar Diário de Classes')
		);	

				
		$id = (int)$this->_request->getParam("id");
		$escolasvinculos = new Escolasvinculos();
		$escolavinculo = $escolasvinculos->getEscolavinculoById($id);
		
		if (!$escolavinculo) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolavinculo;
		$this->preForm();
		$alunos = new Escolassalasatribuicoes();
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['idvinculo'] = $id;
		
		$this->view->escolasalunos = $alunos->getSalasatribuicoes($queries);	
		

		$this->view->materias = Controlealunosmaterias::getControlealunosmateriasHelper(array('status'=>'Ativo'));
		/*if(Escolasusuarios::getUsuario('professor')=="Sim"):
			$this->view->materias = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>Escolasusuarios::getUsuario('id'), 'idescolavinculo'=>$id));
		endif;*/

/*if(Escolasusuarios::getUsuario('professor')=="Sim"):
	/*$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>Escolasusuarios::getUsuario('id'), 'idescolavinculo'=>$id));* /
	$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$id));
	var_dump($id);
	die('aqui');
else:
	$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$id));
endif;*/
$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$id));
	//apagar
	$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('order'=>' GROUP BY q1.idmateria ORDER BY q1.id DESC'));		

		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
    	$this->view->professores = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$id));

		$idsprofessores = Usuarios::getUsuario('idfuncionario');
		$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>$idsprofessores, 'idescolavinculo'=>$id));
//var_dump($this->view->materiasprofessores); die();
		if ($this->_request->isPost()) {
			$erros = $this->getPost($escolavinculo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Diário de Classes adicionadas com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}

		return true;	
    }  		
	
	/**
	 * 
	 * Action de adição de professoresdiariosclasses 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'professoresdiariosclasses', 'name' => 'Diário de Classes'),
			array('url' => null,'name' => 'Adicionar Diário de Classes')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Diário de Classes adicionada com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function setmateriasAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idprofessor = $this->getRequest()->getPost('idprofessor');
		$idescolavinculo = $this->getRequest()->getPost('idescolavinculo');
    	if((int)$idprofessor <=0) return $this->view->rows = array();
		$professor = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($idprofessor);
		if(!$professor) return $this->view->rows = array();

		$controlealunosmaterias = new Controlealunosmaterias();
		$queries_professores = array();	
		$queries_professores['status'] = "Ativo";
		//$queries_professores['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

		$queries_professores['find_in_set'] = ((isset($professor['idsmaterias'])) && ($professor['idsmaterias']!="")) ? $professor['idsmaterias'] : false;
		$this->view->materiasprofessores = $controlealunosmaterias->getControlealunosmaterias($queries_professores);
		//var_dump($this->view->materiasprofessores); die();

		$idsprofessores = Usuarios::getUsuario('idfuncionario');
		$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>$idsprofessores, 'idescolavinculo'=>$idescolavinculo));
	}

	public function setfaltasAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idprofessor = $this->getRequest()->getPost('idprofessor');
		/*if(Escolasusuarios::getUsuario('professor')=="Sim"):
			$idprofessor = Escolasusuarios::getUsuario('id');
		endif;*/

		$idescolavinculo = $this->getRequest()->getPost('idescolavinculo');
		$idmateria = $this->getRequest()->getPost('idmateria');
    	
    	$escolasalunosnotas = new Escolasalunosnotas();
    	$this->view->rows = $escolasalunosnotas->getEscolasalunosnotas(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo, 'idmateria'=>$idmateria));

		$alunos = new Escolassalasatribuicoes();
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['idvinculo'] = $idescolavinculo;
		$queries['order'] = "ORDER BY a1.nomerazao ASC";
		$this->view->escolasalunos = $alunos->getSalasatribuicoes($queries);
		$this->view->idprofessor = $idprofessor;
		$this->view->idescolavinculo = $idescolavinculo;
		$this->view->idmateria = $idmateria;
		$this->preForm();
	}

	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idprofessor = $this->getRequest()->getPost('idprofessor');
		/*if(Escolasusuarios::getUsuario('professor')=="Sim"):
			$idprofessor = Escolasusuarios::getUsuario('id');
		endif;*/

		$idescolavinculo = $this->getRequest()->getPost('idescolavinculo');
		$idmateria = $this->getRequest()->getPost('idmateria');
		$bimestre = $this->getRequest()->getPost('bimestre');

    	$escolasalunosnotas = new Escolasalunosnotas();
    	if($idmateria>0){
    		$this->view->rows = $escolasalunosnotas->getEscolasalunosnotas(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo, 'idmateria'=>$idmateria, 'bimestre'=>$bimestre));	
    	}else{
    		$this->view->rows = $escolasalunosnotas->getEscolasalunosnotas(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo, 'bimestre'=>$bimestre));	
    	}
    	

		$alunos = new Escolassalasatribuicoes();
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['idvinculo'] = $idescolavinculo;
		$queries['order'] = "ORDER BY a1.nomerazao ASC";
		$this->view->escolasalunos = $alunos->getSalasatribuicoes($queries);
		$this->view->idprofessor = $idprofessor;
		$this->view->idescolavinculo = $idescolavinculo;
		$this->view->idmateria = $idmateria;
		$this->view->bimestre = $bimestre;

	
/*if(Escolasusuarios::getUsuario('professor')=="Sim"):
	$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo));
else:
	$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$idescolavinculo));
endif;*/
$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$idescolavinculo));
//apagar
$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('order'=>' GROUP BY q1.idmateria ORDER BY q1.id DESC'));	

		$idsprofessores = Usuarios::getUsuario('idfuncionario');
		$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>$idsprofessores, 'idescolavinculo'=>$idescolavinculo));

		$this->preForm();
	}

	public function addfaltaAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$id = (int)$this->getRequest()->getPost("id");
		//$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');;
		$idescolavinculo = (int)$this->getRequest()->getPost('idescolavinculo');
		$idmateria = (int)$this->getRequest()->getPost('idmateria');
		$idprofessor = (int)$this->getRequest()->getPost('idprofessor');
		$idaluno = (int)$this->getRequest()->getPost("idaluno");
		$dia = $this->getRequest()->getPost('dia');
		$_dia = $dia . '/' . date('m') . '/' . date('Y');
		$data = Mn_Util::stringToTime($_dia);
		$tipo = $this->getRequest()->getPost('tipo');
		$status = "Ativo";
		
		$erros = "";
		
		if (0==$dia) $erros = "Informe o dia.";
		if (0==$idescola) $erros = "Informe a Escola.";
		if (0==$idmateria) $erros = "Informe a Matéria.";
		if (0==$idprofessor) $erros = "Informe a Professor.";
		if (0==$idaluno) $erros = "Informe a Aluno.";
		if (""==$status) $erros = "Informe a Status.";
		$obj = new stdClass();
		$obj->id = 0;
		$obj->data = "ERRO";
		$obj->mensagem = $erros;
		$obj->titulo = "Erro!";
		if($erros!=""){
			echo json_encode($obj);
			die();			
		}

		
		$professoresdiariosclasses = new Escolasalunosfaltas();
		$dados = array();
		$dados['id'] = $id;
		
		$dados["idescola"] = $idescola;
		$dados["idescolavinculo"] = $idescolavinculo;
		$dados["idmateria"] = $idmateria;
		$dados["idprofessor"] = $idprofessor;
		$dados["idaluno"] = $idaluno;
		$dados["data"] = date("Y-m-d", $data);
		$dados["status"] = $status;
		$dados['excluido'] = 'nao';
		$dados['logusuario'] = $this->_usuario['id'];;
		$dados['logdata'] = date('Y-m-d G:i:s');
		//var_dump($dados); die();		
		$row = $professoresdiariosclasses->save($dados);

		$obj->id = $row['id'];
		$obj->data = "OK";
		$obj->mensagem = "Falta adicionada com Sucesso!";
		$obj->titulo = "Sucesso!";

		echo json_encode($obj);
		die();
	}	

	public function removefaltaAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$id = (int)$this->getRequest()->getPost("id");		
		$professoresdiariosclasses = new Escolasalunosfaltas();
		$row = $professoresdiariosclasses->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$professoresdiariosclasses->save($row);
						
			die("OK");
		}	
		die('ERRO');
	}
    /**
     * Atribui valores ao view
     * @param int $idescolasalunosfalta
     */    
    private function preForm($idescolasalunosfalta = 0) {
    	$escolasconfiguracoes = new Escolasconfiguracoes();
    	$queries = array();
    	//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	$escolas = $escolasconfiguracoes->getEscolasconfiguracoes($queries);
    	$_escolas = array();
    	if(sizeof($escolas) > 0) $_escolas = $escolas[0];
    	$this->view->configuracoesnotas = $_escolas;



    	$this->view->ultimadia = date("t");

		$controlealunosmaterias = new Controlealunosmaterias();
		$queries_professores = array();	
		$queries_professores['status'] = "Ativo";
		//$queries_professores['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

		$queries_professores['find_in_set'] = ((isset($professor['idsmaterias'])) && ($professor['idsmaterias']!="")) ? $professor['idsmaterias'] : false;
		//$this->view->materiasprofessores = $controlealunosmaterias->getControlealunosmaterias($queries_professores);
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_escolasalunosfalta = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$idescolavinculo = (int)trim($this->getRequest()->getPost("idescolavinculo"));
$idmateria = (int)trim($this->getRequest()->getPost("idmateria"));
$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
if (0==$idmateria) array_push($erros, "Informe a Matéria.");
if (0==$idprofessor) array_push($erros, "Informe a Professor.");
if (0==$idaluno) array_push($erros, "Informe a Aluno.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$professoresdiariosclasses = new Escolasalunosfaltas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
$dados["idescolavinculo"] = $idescolavinculo;
$dados["idmateria"] = $idmateria;
$dados["idprofessor"] = $idprofessor;
$dados["idaluno"] = $idaluno;
$dados["data"] = date("Y-m-d", $data);
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $professoresdiariosclasses->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	}
	

	/* ===================== Area de Teste de código  ================== */

	public function setalunosquintoconceitoAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idprofessor = $this->getRequest()->getPost('idprofessor');
		/*if(Escolasusuarios::getUsuario('professor')=="Sim"):
			$idprofessor = Escolasusuarios::getUsuario('id');
		endif;*/

		$idescolavinculo = $this->getRequest()->getPost('idescolavinculo');
		$idmateria = $this->getRequest()->getPost('idmateria');
		$bimestre = $this->getRequest()->getPost('bimestre');

    	$escolasalunosnotas = new Escolasalunosnotas();
    	if($idmateria>0){
    		$this->view->rows = $escolasalunosnotas->getEscolasalunosnotas(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo, 'idmateria'=>$idmateria, 'bimestre'=>$bimestre));	
    	}else{
    		$this->view->rows = $escolasalunosnotas->getEscolasalunosnotas(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo, 'bimestre'=>$bimestre));	
    	}
    	

		$alunos = new Escolassalasatribuicoes();
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['idvinculo'] = $idescolavinculo;
		$queries['order'] = "ORDER BY a1.nomerazao ASC";
		$this->view->escolasalunos = $alunos->getSalasatribuicoes($queries);
		$this->view->idprofessor = $idprofessor;
		$this->view->idescolavinculo = $idescolavinculo;
		$this->view->idmateria = $idmateria;
		$this->view->bimestre = $bimestre;

		

	
/*if(Escolasusuarios::getUsuario('professor')=="Sim"):
	$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo));
else:
	$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$idescolavinculo));
endif;*/
$this->view->materiasprofessores  = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$idescolavinculo));
//apagar
$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('order'=>' GROUP BY q1.idmateria ORDER BY q1.id DESC'));	

		$idsprofessores = Usuarios::getUsuario('idfuncionario');
		$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>$idsprofessores, 'idescolavinculo'=>$idescolavinculo));

		
		$this->preForm();
	}//end function
    
}