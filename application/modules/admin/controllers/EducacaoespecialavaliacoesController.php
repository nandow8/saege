<?php

/**
 * Controle da classe educacaoespecialavaliacoes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EducacaoespecialavaliacoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Educacaoespecialavaliacao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("educacaoespecialavaliacoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Educacaoespecialavaliacoes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Avaliação excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="educacaoespecialavaliacoes") $objs = new Educacaoespecialavaliacoes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}

	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		$idescola = (int) $this->getRequest()->getPost('idescola');
    	if($idescola <= 0) die('ERRO');

		$alunos = new Escolasalunos();
    	$this->view->rows = $alunos->getEscolasalunos(array('idescola'=>$idescola, 'status'=>'Ativo', 'idsecretaria'=> Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' )));
	}	
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialavaliacoes', 'name' => 'Ficha integrada'),
			array('url' => null,'name' => 'Visualizar Ficha integrada')
		);
		
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialavaliacoes = new Educacaoespecialavaliacoes();
		$educacaoespecialavaliacao = $educacaoespecialavaliacoes->getEducacaoespecialavaliacaoById($id, array());

		$educacaoespecialrespostas = new Educacaoespecialrespostas();
		$educacaoespecialresposta = $educacaoespecialrespostas->getEducacaoespecialrespostasById($id);
		
		
		if (!$educacaoespecialavaliacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialavaliacao;
		$this->view->post_var['respostas'] = $educacaoespecialresposta;
		
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Ficha integrada')
		);
		
		$ns = new Zend_Session_Namespace('default_educacaoespecialavaliacoes');
		$educacaoespecialavaliacoes = new Educacaoespecialavaliacoes();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["idaluno"]!="") $queries["idaluno"] = $this->view->post_var["idaluno"];
			if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
			if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
			    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $educacaoespecialavaliacoes->getEducacaoespecialavaliacoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $educacaoespecialavaliacoes->getEducacaoespecialavaliacoes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de educacaoespecialavaliacoes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialavaliacoes', 'name' => 'Ficha integrada'),
			array('url' => null,'name' => 'Editar Ficha integrada')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$educacaoespecialavaliacoes = new Educacaoespecialavaliacoes();
		$educacaoespecialavaliacao = $educacaoespecialavaliacoes->getEducacaoespecialavaliacaoById($id);
		
		$educacaoespecialrespostas = new Educacaoespecialrespostas();
		$educacaoespecialresposta = $educacaoespecialrespostas->getEducacaoespecialrespostasById($id);
		
 

		if (!$educacaoespecialavaliacao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $educacaoespecialavaliacao;
		$this->view->post_var['respostas'] = $educacaoespecialresposta;
 

		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($educacaoespecialavaliacao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Avaliação editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
		$this->view->editar = true;
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de educacaoespecialavaliacoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'educacaoespecialavaliacoes', 'name' => 'Ficha integrada'),
			array('url' => null,'name' => 'Adicionar Ficha integrada')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Avaliação adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $ideducacaoespecialavaliacao
     */    
    private function preForm($ideducacaoespecialavaliacao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_educacaoespecialavaliacao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);

		$id = (int)$this->getRequest()->getPost("id");		
		$idlocal = (int)$this->getRequest()->getPost("idlocal");		
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idaluno = (int)trim($this->getRequest()->getPost("idaluno")); 
		$idperguntas = implode(',', $this->getRequest()->getPost("idsperguntas"));
		$objsemestre = trim($this->getRequest()->getPost("objsemestre")); 
		 
		for($i = 0; $i < 55; $i++){
			$perguntasimnao[$i] = trim($this->getRequest()->getPost("perguntasimnao$i"));
			$ca_sa[$i] = trim($this->getRequest()->getPost("ca_sa$i")); 
		} 
		   
		$status = trim($this->getRequest()->getPost("status1"));
 
		$erros = array();
		 
		if (0==$idaluno) array_push($erros, "Informe a Aluno."); 
		if (""==$status) array_push($erros, "Informe a Status.");

		if (sizeof($erros)>0) return $erros;

		$educacaoespecialavaliacoes = new Educacaoespecialavaliacoes();
		$educacaoespecialrespostas = new Educacaoespecialrespostas();
 
		
		if(sizeof($erros)>0) return $erros;
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dadosPerguntasSimNao = array(); 

			$dados['id'] = $id;
			$dados["idescola"] = $idescola;
			$dados["idaluno"] = $idaluno; 
			$dados["objsemestre"] = $objsemestre; 
			  
				$dadosPerguntasSimNao['id'] = $idlocal;
				$dadosPerguntasSimNao["local"] = 'educacaoespecialavaliacoes';

				$ultimaRespostaId = Educacaoespecialrespostas::getUltimoEducacaoespecialrespostas();  

				if($idlocal == 0){
					$dadosPerguntasSimNao["idlocal"] = ($ultimaRespostaId[0]['idlocal'] == 0) ? 1 : $ultimaRespostaId[0]['idlocal'] ;
					$dados['idlocal'] = ($ultimaRespostaId[0]['idlocal'] == 0) ? 1 : $ultimaRespostaId[0]['idlocal'] ;
				} 

				for ($i=0; $i < 55; $i++) { 
					$dadosPerguntasSimNao["perguntasimnao"  . $i] = $perguntasimnao[$i]; 
					$dadosPerguntasSimNao["ca_sa"  . $i] = $ca_sa[$i]; 
				}

				$dadosPerguntasSimNao["excluido"] = 'nao';
				$row = $educacaoespecialrespostas->save($dadosPerguntasSimNao); 
			  
			$dados["idperguntas"] = $idperguntas;
			$dados["status"] = $status;

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');

			$row = $educacaoespecialavaliacoes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	}
	
	public function obterprofessoresdosalunosAction() {
		$this->_helper->layout->disableLayout();     
		$id = (int)$this->getRequest()->getPost('idaluno');
	 
		$rows['aee'] = Aeematriculas::getAeematriculaByIdHelper($id);

		$rows['nomealuno'] = Escolasalunos::getEscolaalunoByIdHelper($rows['aee']['idaluno'])['nomerazao']; 
		$rows['professor'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($rows['aee']['idprofessor']);
		$rows['professorAee'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($rows['aee']['idprofessor2']);
		$rows['escola'] = Escolas::getEscolaByIdHelper($rows['aee']['idescola'])['escola'];

		echo json_encode($rows);
		die();
	
	}
    
}