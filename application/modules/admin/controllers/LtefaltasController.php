<?php

class Admin_LtefaltasController extends Zend_Controller_Action {
    
    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null; 
    
    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
        Mn_Util::blockAccess("ltefaltas", $this->_request->getActionName());    
        
        $this->_usuario = unserialize($loginNameSpace->usuario);
        
        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }               
    }
    
    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Falta')
        );
      
        $ns = new Zend_Session_Namespace('logistica_faltas');
        $faltas = new Ltefaltas();
        $queries = array(); 

        $this->preForm();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) { 
            
            foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = $v;

            if ((isset($this->view->post_var['falta'])) && ($this->view->post_var['falta']!='')) $queries['falta'] = $this->view->post_var['falta'];
        }       
        
        //PAGINACAO
        $maxpp = 15;
        
        $paginaAtual = (int)$this->getRequest()->getParam('p');
        if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
        if ($paginaAtual==0) $paginaAtual = 1;
        
        $queries['total'] = true;
        $totalRegistros = $faltas->getFaltas($queries);
        $paginaTotal = ceil($totalRegistros/$maxpp);
        $queries['total'] = false;
        if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual<0) $paginaAtual = 0;
        
        $this->view->pagina_atual = $paginaAtual+1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;           
        
        $this->view->rows = $faltas->getFaltas($queries, $paginaAtual, $maxpp);
    }
    
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'ltefaltas', 'name' => 'Falta'),
            array('url' => null,'name' => 'Visualizar Falta')
        );
    
        $id = (int)$this->_request->getParam("id");
        $faltas = new Ltefaltas();
        $falta = $faltas->getFaltasById($id);

        if (!$falta) $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->visualizar = true;
        $this->view->post_var = $falta;
                
        $this->preForm();   
    }
    
    /**
     * 
     * Action de edição do período
     */ 
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'ltefaltas', 'name' => 'Falta'),
            array('url' => null,'name' => 'Editar Falta')
        );  
                
        $id = (int)$this->_request->getParam("id");
        $faltas = new Ltefaltas();
        $falta = $faltas->getFaltasById($id);
    
        if (!$falta) 
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->post_var = $falta;
            
        $this->preForm();
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($falta);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Falta editadas com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }       
        return true;        
    }       
    
    /**
     * 
     * Action de adição de falta 
     */
    public function adicionarAction() {

        $this->view->bread_crumb = array(
            array('url' => 'ltefaltas', 'name' => 'Falta'),
            array('url' => null,'name' => 'Adicionar Falta')
        );
                
        $this->preForm();

        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Faltas adicionadas com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }

        return true;        
    }   
    
    /**
     * Atribui valores ao view
     * @param int $id
     */    
    private function preForm($id = 0) {
    }    
    
    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_registro = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);
        
        $id = (int)$this->getRequest()->getPost("id");
        $idestudante = $this->getRequest()->getPost("idestudante");
        $idperiodo = $this->getRequest()->getPost("idperiodo");
        $quantidadefaltas = strip_tags(trim($this->getRequest()->getPost("faltas")));
        $observacoes = strip_tags(trim($this->getRequest()->getPost("observacoes")));

        $status = strip_tags(trim($this->getRequest()->getPost("status")));

        $erros = array();
        if (""==$status) array_push($erros, 'Selecione um STATUS.');
                
        $faltas = new Ltefaltas();

        if (sizeof($erros)>0) return $erros; 
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
               
            $dados = array();           
            $dados['id'] = $id;
            $dados['idestudante'] = $idestudante;
            $dados['idperiodo'] = $idperiodo;
            $dados['faltas'] = $quantidadefaltas;
            $dados['observacoes'] = $observacoes;

            $dados['status'] = $status;

            $dados['excluido'] = 'nao';

            $row = $faltas->save($dados);
            
            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die();
        }       

        return "";      
    }
}