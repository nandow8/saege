<?php

/**
 * Controle da classe escolasvinculos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_EscolasvinculosprofessoresController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Escolavinculo
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->usuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockAccess("escolasvinculosprofessores", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->usuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Escolasvinculos();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Vínculo excluído com sucesso.";

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "escolasvinculosprofessores")
            $objs = new Escolasvinculos();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasvinculosprofessores', 'name' => 'Vínculos'),
            array('url' => null, 'name' => 'Visualizar Vínculo')
        );

        $id = (int) $this->_request->getParam("id");
        $escolasvinculos = new Escolasvinculos();
        $escolavinculo = $escolasvinculos->getEscolavinculoById($id, array());

        if (!$escolavinculo)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $escolavinculo;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Vínculos')
        );

        $ns = new Zend_Session_Namespace('admin_escolasvinculosprofessores');
        $escolasvinculos = new Escolasvinculos();
        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $queries['order'] = "GROUP BY ev1.idserie ASC";

        $idescola = Usuarios::getUsuario('idescola');
        $idescola = (int) $idescola;
        //$idescola = 1;
        if ($idescola > 0) {
            //$queries['idescola'] = Usuarios::getUsuario('idescola');
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName() . '/indexescolas');
        }

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ((isset($this->view->post_var["idescola"])) && ($this->view->post_var["idescola"] != ""))
                $queries["idescola"] = $this->view->post_var["idescola"];
            if ((isset($this->view->post_var["idserie"])) && ($this->view->post_var["idserie"] != ""))
                $queries["idserie"] = $this->view->post_var["idserie"];
            if ((isset($this->view->post_var["idturma"])) && ($this->view->post_var["idturma"] != ""))
                $queries["idturma"] = $this->view->post_var["idturma"];
            if ((isset($this->view->post_var["status1"])) && ($this->view->post_var["status1"] != ""))
                $queries["status"] = $this->view->post_var["status1"];

            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $escolasvinculos->getEscolasvinculos($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $escolasvinculos->getEscolasvinculos($queries, $paginaAtual, $maxpp);
    }

    /**
     * Listagem
     */
    public function indexescolasAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Vínculos')
        );

        $ns = new Zend_Session_Namespace('admin_escolasvinculosprofessores');
        $escolasvinculos = new Escolasvinculos();
        $queries = array();
        $queries['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        //$queries['order'] = "GROUP BY ev1.idserie ASC";

        $idescola = Usuarios::getUsuario('idescola');
        $idescola = (int) $idescola;
        //$idescola = 1;
        if ($idescola > 0) {
            $queries['idescola'] = $idescola;
        } else {
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }
        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);

            if ((isset($this->view->post_var["idserie"])) && ($this->view->post_var["idserie"] != ""))
                $queries["idserie"] = $this->view->post_var["idserie"];
            if ((isset($this->view->post_var["idturma"])) && ($this->view->post_var["idturma"] != ""))
                $queries["idturma"] = $this->view->post_var["idturma"];
            if ((isset($this->view->post_var["status1"])) && ($this->view->post_var["status1"] != ""))
                $queries["status"] = $this->view->post_var["status1"];

            //if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $escolasvinculos->getEscolasvinculos($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $escolasvinculos->getEscolasvinculos($queries, $paginaAtual, $maxpp);
    }

    public function editarescolasAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasvinculosprofessores/indexescolas', 'name' => 'Vínculos'),
            array('url' => null, 'name' => 'Editar Vínculo')
        );

        $id = (int) $this->_request->getParam("id");
        $idescola = (int) $this->_request->getParam("idescola");
        $idserie = (int) $this->_request->getParam("idserie");
        $idsala = (int) $this->_request->getParam("idsala");

        $escolasvinculos = new Escolasvinculos();
        $escolavinculo = $escolasvinculos->getEscolavinculoById($id);

        if (!$escolavinculo)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $escolavinculo;
        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($escolavinculo);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Vínculo editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * 
     * Action de edição de escolasvinculos
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasvinculosprofessores', 'name' => 'Vínculos'),
            array('url' => null, 'name' => 'Editar Vínculo')
        );

        $id = (int) $this->_request->getParam("id");
        $escolasvinculos = new Escolasvinculos();
        $escolavinculo = $escolasvinculos->getEscolavinculoById($id);

        if (!$escolavinculo)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $escolavinculo;
        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($escolavinculo);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Vínculo editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * 
     * Action de adição de escolasvinculos 
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasvinculosprofessores', 'name' => 'Vínculos'),
            array('url' => null, 'name' => 'Adicionar Vínculo')
        );

        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Vínculo adicionado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * 
     * Action de adição de escolasvinculos 
     */
    public function formoldAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasvinculosprofessores', 'name' => 'Vínculos'),
            array('url' => null, 'name' => 'Editar Vínculo')
        );

        $id = (int) $this->_request->getParam("id");
        $escolasvinculos = new Escolasvinculos();
        $escolavinculo = $escolasvinculos->getEscolavinculoById($id);

        if (!$escolavinculo)
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());

        $this->view->post_var = $escolavinculo;
        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($escolavinculo);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Vínculo editado com sucesso.";

            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }

        return true;
    }

    public function setsalasAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = $this->getRequest()->getPost('idescola');

        $salas = new Escolassalas();
        $this->view->rows = $salas->getEscolassalas(array('idescola' => $idescola, 'status' => 'Ativo'));
    }

    public function setseriesAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = $this->getRequest()->getPost('idescola');

        $series = new Escolasseries();
        $this->view->rows = $series->getEscolasseries(array('idescola' => $idescola, 'status' => 'Ativo'));
    }

    public function setturmasAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = $this->getRequest()->getPost('idescola');

        $turmas = new Escolasturmas();
        $this->view->rows = $turmas->getEscolasturmas(array('idescola' => $idescola, 'status' => 'Ativo'));
    }

    public function setperiodosAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = $this->getRequest()->getPost('idescola');

        $periodos = new Escolasperiodos();
        $this->view->rows = $periodos->getEscolasperiodos(array('idescola' => $idescola, 'status' => 'Ativo'));
    }

    public function setmateriasAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idprofessor = $this->getRequest()->getPost('idprofessor');
        if ((int) $idprofessor <= 0)
            return $this->view->rows = array();
        $materias = new Controleprofessoresmaterias();
        $this->view->rows = $materias->getControleprofessoresmaterias(array('idprofessor' => $idprofessor, 'left_materias' => true));

        $this->view->idprofessorselecionado = $idprofessor;
    }

    public function setfuncionariosescolasAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = $this->getRequest()->getPost('idescola');

        $usuarios = new Escolasusuarios();
        $this->view->rows = $usuarios->getEscolasusuarios(array('idescola' => $idescola, 'status' => 'Ativo', 'professor' => 'Sim'));
    }

    public function setvinculosAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = $this->getRequest()->getPost('idescola');
        $idserie = $this->getRequest()->getPost('idserie');
        $idsala = (int) $this->getRequest()->getPost('idsala');
        //$idperiodo = $this->getRequest()->getPost('idperiodo');	

        if (((int) $idescola <= 0) || ((int) $idserie <= 0)) {
            $this->view->rows = array();
            return;
        }
        $vinculos = new Escolasvinculos();
        $this->view->rows = $vinculos->getEscolasvinculos(array('idescola' => $idescola, 'idserie' => $idserie, 'idsala' => $idsala, 'status' => 'Ativo'));

        //$this->view->idescolasalasaluno = $idescolasalasaluno;
    }

    public function setprofessoresAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescolasalasaluno = (int) $this->getRequest()->getPost('idescolasalasaluno');
        $this->view->idescolasalasaluno = $idescolasalasaluno;

        $idescola = $this->getRequest()->getPost('idescola');
        $idserie = $this->getRequest()->getPost('idserie');
        $idperiodo = $this->getRequest()->getPost('idperiodo');
        $professorcomplete = strip_tags(trim($this->getRequest()->getPost('professorcomplete')));
        $notids = strip_tags(trim($this->getRequest()->getPost('notids')));
        $notids = explode(',', $notids);
        $this->view->notids = $notids;
        $this->view->idserie = $idserie;
        $classificacao = $this->getRequest()->getPost('classificacao');
        if (((int) $idescola <= 0) || ((int) $idserie <= 0)) {
            $this->view->rows = array();
            return;
        }

        $funcionariosgeraisescolas = new Funcionariosgeraisescolas();
        $this->view->rows = $funcionariosgeraisescolas->getFuncionariosgeraisescolas(array('idescolas' => $idescola, 'status' => 'Ativo', 'professor' => 'Sim', 'professorcomplete' => $professorcomplete));
    }

    public function verificadisponibilidadeAction() {
        $this->_helper->layout->disableLayout();

        $idvinculo = $this->getRequest()->getPost('idvinculo');
        $idprofessor = $this->getRequest()->getPost('idprofessor');
        //$idperiodo = $this->getRequest()->getPost('idperiodo');

        $vinculos = new Escolasvinculosprofessoresmaterias();
        $row = $vinculos->getEscolasvinculosprofessoresmaterias(array('idescolavinculo' => $idvinculo, 'idprofessor' => $idprofessor));

        if (sizeof($row) > 0)
            die('ESSE PROFESSOR JÁ ESTÁ ATRIBUIDO A ESSA SALA');

        $_row = $vinculos->getEscolasvinculosprofessoresmaterias(array('idprofessor' => $idprofessor));
        if (sizeof($_row) > 0) {
            $v_r = $_row[0];
            /* if($v_r['quantidadesaulas']<=sizeof($_row)) {
              die('ESSE PROFESSOR JÁ ATINGIU O LIMTE DE AULAS');
              } */
        }

        $dados = array();

        $dados["idescolavinculo"] = $idvinculo;
        $dados["idprofessor"] = $idprofessor;
        $dados["status"] = 'Ativo';
        $dados['excluido'] = 'nao';
        $dados['logusuario'] = $this->_usuario['id'];
        ;
        $dados['logdata'] = date('Y-m-d G:i:s');

        $row = $vinculos->save($dados);

        if (!$row)
            die('ERRO AO VINCULAR PROFESSOR À SALA. TENTE NOVAMENTE!');

        die($row['id']);
    }

    public function tipodiarioclasseAction() {
        $this->_helper->layout->disableLayout();
        $diarioclasse = $this->getRequest()->getPost('diarioclasse');
        $idprofessor = $this->getRequest()->getPost('idprofessor');
        $idescolavinculo = $this->getRequest()->getPost('idescolavinculo');
        $idsmaterias = $this->getRequest()->getPost('idsmaterias');


        $verifica = Escolasvinculosdiariosclasses::getEscolasvinculosdiariosclassesHelper(array('idprofessor' => $idprofessor, 'idescolavinculo' => $idescolavinculo));

        $id = 0;
        if (isset($verifica[0]['id'])) {
            $id = $verifica[0]['id'];
        }
        $dados = array();
        $dados['id'] = $id;
        $dados['tipo'] = $diarioclasse;
        $dados['idprofessor'] = $idprofessor;
        $dados['idescolavinculo'] = $idescolavinculo;
        $dados['idsmaterias'] = $idsmaterias;
        $dados['status'] = 'Ativo';
        $dados['excluido'] = 'nao';
        $dados['logusuario'] = Usuarios::getUsuario('id');
        $dados['logdata'] = date('Y-m-d G:i:s');
        $escolasvinculosdiariosclasses = new Escolasvinculosdiariosclasses();
        $escolasvinculosdiariosclasses->save($dados);

        die('OK');
    }

    public function excluiritemAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $iditem = $this->getRequest()->getPost('iditem');
        $idsvinculos = $this->getRequest()->getPost('idsvinculos');
        $idserie = $this->getRequest()->getPost('idserie');
        $idprofessor = $this->getRequest()->getPost('idprofessor');

        $atribuicoes = new Escolasvinculosprofessoresmaterias();
        $row = $atribuicoes->getEscolasvinculosprofessormateriaById($iditem, array('idserie' => $idserie, 'idsvinculos' => $idsvinculos));
        //var_dump($iditem); die('aaa');
        if (!$row)
            die('ERRO!');

        $row['excluido'] = 'sim';
        $row['logusuario'] = Usuarios::getUsuario('id');
        $row['logdata'] = date('Y-m-d G:i:s');

        $r = $atribuicoes->save($row);

        $materiasdadas = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor' => $idprofessor, 'idescolavinculo' => $idsvinculos));
        //var_dump($materiasdadas); die('aaa');

        if ($r && sizeof($materiasdadas) > 0) {

            $quadros = new Quadrohorarios();

            foreach ($materiasdadas as $k => $materia) {
                $materia['excluido'] = 'sim';
                $materia['logusuario'] = $this->_usuario['id'];
                $materia['logdata'] = date('Y-m-d G:i:s');
                //var_dump($materia); die('bbb');

                $quadros->save($materia);
            }
        }

        die('OK');
    }

    public function setmateriasescolasAction() {
        $this->_helper->layout->disableLayout();
        $idescola = $this->getRequest()->getPost('idescola');
        $idsprofessores = $this->getRequest()->getPost('idsprofessores');
        $idescolavinculo = $this->getRequest()->getPost('idescolavinculo');

        $professor = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($idsprofessores);
        if (!$professor)
            die('Professor não encontrado!');
        $this->view->idprofessorselecionado = $idsprofessores;
        $this->view->professor = $professor;

        $controlealunosmaterias = new Controlealunosmaterias();
        $queries_professores = array();
        $queries_professores['status'] = "Ativo";
        $queries_professores['idescola'] = $idescola;
        $queries_professores['find_in_set'] = ((isset($professor['idsmaterias'])) && ($professor['idsmaterias'] != "")) ? $professor['idsmaterias'] : false;
        $this->view->materiasprofessores = $controlealunosmaterias->getControlealunosmaterias($queries_professores);

        $queries_sala = array();
        $queries_sala['status'] = "Ativo";
        $queries_sala['idescolavinculo'] = $idescolavinculo;
        $queries_sala['order'] = " GROUP BY q1.idmateria";
        $quadrohorarios = new Quadrohorarios();
        $this->view->aulassala = $quadrohorarios->getQuadrohorarios($queries_sala);

        $queries = array();
        $queries['status'] = "Ativo";
        $queries['order'] = "GROUP BY c1.materia";
        //$queries['idescola'] = $idescola;
        $this->view->rows = $controlealunosmaterias->getControlealunosmaterias($queries);

        //var_dump($idsprofessores);die();

        $this->view->materiasdadas = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor' => $idsprofessores, 'idescolavinculo' => $idescolavinculo));
    }

    public function removemateriasescolasAction() {
        $id = $this->getRequest()->getPost('id');

        $rows = new Quadrohorarios();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            die("OK");
        }

        die("Não encontrado!");
    }

    public function setprofessornasalaAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasvinculosprofessores', 'name' => 'Associar professor a Sala'),
            array('url' => null, 'name' => 'Professor na sala')
        );
        $this->_helper->layout->disableLayout();

        $arrayids = $this->getRequest()->getParam("arrayids");
        $id = explode(',', $arrayids);

        $idvinculo = (int) $id[0];
        $idprofessor = (int) $id[1];
        $idescola = (int) $id[2];
        $idprofessormateria = (int) $id[3];
        $idsala = (int) $id[4];
        $idperiodo = (int) $id[5];

        $this->view->idvinculo = $idvinculo;
        $this->view->idprofessor = $idprofessor;
        $this->view->idescola = $idescola;
        $this->view->idprofessormateria = $idprofessormateria;
        $this->view->idsala = $idsala;
        $this->view->idperiodo = $idperiodo;
    }

    public function enviadadosAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();

        $quantidadesaulas = $this->getRequest()->getPost('quantidadesaulas');
        $quadroidescolavinculo = (int) $this->getRequest()->getPost('quadroidescolavinculo');
        $quadroidsprofessores = $this->getRequest()->getPost('quadroidsprofessores');
        $quadroidmateria = (int) $this->getRequest()->getPost('quadroidmateria');
        $quadroperiodoaula = $this->getRequest()->getPost('quadroperiodoaula');

        $quadrohorarios = new Quadrohorarios();
//var_dump(" excluido = 'nao' AND idescolavinculo=".$quadroidescolavinculo . " AND idmateria " . $quadroidmateria); die();
        $_verificamateria = $quadrohorarios->getQuadrohorarios(array('idescolavinculo' => $quadroidescolavinculo, 'idmateria' => $quadroidmateria));
        //var_dump($_verificamateria);die();
        if (sizeof($_verificamateria) > 0)
            die('Está matéria já esta adicionada para essa sala.');

        $dados = array();
        $dados["idsecretaria"] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $dados["idescolavinculo"] = $quadroidescolavinculo;
        $dados["idmateria"] = $quadroidmateria;
        $dados["idprofessor"] = $quadroidsprofessores;
        $dados["quantidadesaulas"] = $quantidadesaulas;
        $dados["quadroperiodoaula"] = $quadroperiodoaula;
        $dados["status"] = 'Ativo';


        $dados['excluido'] = 'nao';
        $dados['logusuario'] = $this->_usuario['id'];
        ;
        $dados['logdata'] = date('Y-m-d G:i:s');
        echo '<pre>';
        print_r($dados);
        echo '</pre>';
        die();
        $row = $quadrohorarios->save($dados);
        die('OK');
    }

    public function duploprofessorAction() {
        $this->_helper->layout->disableLayout();

        $idquadrohorario = (int) $this->getRequest()->getPost('idquadrohorario');
        $idprofessor = (int) $this->getRequest()->getPost('idprofessor');

        if ((int) $idprofessor <= 0) {
            die('Professor não encontrado!');
        }

        $quadrohorarios = new Quadrohorarios();
        $quadrohorario = $quadrohorarios->getQuadrohorarioById($idquadrohorario);

        if ((isset($quadrohorario['id'])) && ((int) $quadrohorario['id'] > 0)) {
            unset($quadrohorario['id']);
            $quadrohorario['idprofessor'] = $idprofessor;
            $quadrohorario['logusuario'] = $this->_usuario['id'];
            $quadrohorario['logdata'] = date('Y-m-d G:i:s');
            $row = $quadrohorarios->save($quadrohorario);

            die('OK');
        } else {
            die('Matéria não encontrada !');
        }
    }

    /**
     * Atribui valores ao view
     * @param int $idvinculo
     */
    private function preForm($idvinculo = 0) {

        $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $this->view->escolas = Escolas::getEscolasHelper(array('status' => 'Ativo', 'idsecretaria' => $this->view->idsecretaria));
        $this->view->identificadoresescolas = Gdae_Identificadoresescolas::getIdentificadoresHelper(array('status' => 'Ativo'));
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_escolavinculo = false) {
        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $idsecretaria = (int) trim($this->getRequest()->getPost("idsecretaria"));
        $idescola = (int) trim($this->getRequest()->getPost("idescola"));
        $idsala = (int) trim($this->getRequest()->getPost("idsala"));
        $idserie = (int) trim($this->getRequest()->getPost("idserie"));
        $idturma = (int) trim($this->getRequest()->getPost("idturma"));
        $idperiodo = (int) trim($this->getRequest()->getPost("idperiodo"));

        $idsmaterias = $this->getRequest()->getPost("idsmaterias");

        $idsvinculos = $this->getRequest()->getPost("idsvinculos");
        $idssalas = $this->getRequest()->getPost("idssalas");
        $idsperiodos = $this->getRequest()->getPost("idsperiodos");
        $idsprofessores = $this->getRequest()->getPost("idsprofessores");
        $idsprofessoresmaterias = $this->getRequest()->getPost("idsprofessoresmaterias");
        //$idsmaterias = implode(',',$idsmaterias);
        $idprofessor = (int) trim($this->getRequest()->getPost("idprofessor"));
        $idtipoensino = (int) trim($this->getRequest()->getPost("idtipoensino"));
        $inicioaulas = Mn_Util::stringToTime($this->getRequest()->getPost("inicioaulas"));
        $terminoaulas = Mn_Util::stringToTime($this->getRequest()->getPost("terminoaulas"));
        $turno = trim($this->getRequest()->getPost("turno"));
        $ano = trim($this->getRequest()->getPost("ano"));
        $integracao = trim($this->getRequest()->getPost("integracao"));
        $status = trim($this->getRequest()->getPost("status1"));
        //var_dump($_POST); die();

        $erros = array();

        if (0 == $idescola)
            array_push($erros, "Informe a Escolas.");
//if (0==$idsala) array_push($erros, "Informe a Sala.");
        if (0 == $idserie)
            array_push($erros, "Informe a Série.");
        /* if (0==$idturma) array_push($erros, "Informe a Turma.");
          if (0==$idperiodo) array_push($erros, "Informe a Período.");
          if (0==$idprofessor) array_push($erros, "Informe a Professor.");
          if (0==$idtipoensino) array_push($erros, "Informe a Tipos.");
          if (""==$inicioaulas) array_push($erros, "Informe a Ínicio das aulas.");
          if (""==$terminoaulas) array_push($erros, "Informe a Término das aulas."); */
        if ("" == $status)
            array_push($erros, "Informe a Status.");


        $escolasvinculos = new Escolasvinculos();

        /*
          idsvinculos
          idssalas
          idsperiodos
          idsprofessores
         */

        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $idescolavinculo = 0;
            $idescolavinculoanterior = 0;
            $idsalaanterior = 0;
            $_idsprofessoresmateriasexcludes = array();
            foreach ($idssalas as $is => $sala) :

                if ($idsalaanterior != $sala) {
                    $idsala = $sala;

                    if (isset($idsvinculos[$is])) {
                        $idescolavinculo = $idsvinculos[$is];
                    } else {
                        $idescolavinculo = 0;
                    }
                }


                $dados = array();
                $dados['id'] = $idescolavinculo;

                $dados["idsecretaria"] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
                $dados["idescola"] = $idescola;
                $dados["idsala"] = $idsala;
                $dados["idserie"] = $idserie;

                $dados["integracao"] = "Não";
                $dados["status"] = $status;
                $dados['excluido'] = 'nao';
                $dados['logusuario'] = $this->_usuario['id'];
                ;
                $dados['logdata'] = date('Y-m-d G:i:s');
                //var_dump($dados); die('dados');
                //idvinculo
                $row = $escolasvinculos->save($dados);

                $escolasvinculosprofessoresmaterias = new Escolasvinculosprofessoresmaterias();
                $dados_materias = array();

                $dados_materias["id"] = $idsprofessoresmaterias[$is];
                $dados_materias["idescolavinculo"] = $row['id'];
                $dados_materias["idprofessor"] = $idsprofessores[$is];
                $dados_materias["status"] = $status;
                $dados_materias['excluido'] = 'nao';
                $dados_materias['logusuario'] = $this->_usuario['id'];
                ;
                $dados_materias['logdata'] = date('Y-m-d G:i:s');

                $_row = $escolasvinculosprofessoresmaterias->save($dados_materias);

            endforeach;


            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();

            $db->rollBack();
            die();
        }

        return "";
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

    public function retiramateriaajaxAction() {

        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

//		$id = $this->_request->getParam("id");
//
//		$nutriIngredientes = new Nutricaorefeicoesingredientes();
//
//		$row = $nutriIngredientes->getNutricaotacoalimentoByIdHelper($id);
//
//		$row['status'] = 'Ativo';
//		$row['excluido'] = 'sim';
//		$row['logusuario'] = Usuarios::getUsuario('id');
//		$row['logdata'] = date('Y-m-d G:i:s');
//
//		$nutriIngredientes->save($row);
//
//		echo json_encode($row);

        die();
    }

// end public function

    public function incluimateriaajaxAction() {

        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

		$id = $this->_request->getParam("id");
                
         echo $id;

//
//		$nutriIngredientes = new Nutricaorefeicoesingredientes();
//
//		$row = $nutriIngredientes->getNutricaotacoalimentoByIdHelper($id);
//
//		$row['status'] = 'Ativo';
//		$row['excluido'] = 'sim';
//		$row['logusuario'] = Usuarios::getUsuario('id');
//		$row['logdata'] = date('Y-m-d G:i:s');
//
//		$nutriIngredientes->save($row);
//
//		echo json_encode($row);

        die();
    }

    
    
    public function atribuirmateriaAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();

        $quantidadesaulas = $this->getRequest()->getPost('quantidadesaulas');
        $quadroidescolavinculo = (int) $this->getRequest()->getPost('quadroidescolavinculo');
        $quadroidsprofessores = $this->getRequest()->getPost('quadroidsprofessores');
        $quadroidmateria = (int) $this->getRequest()->getPost('quadroidmateria');
        $quadroperiodoaula = $this->getRequest()->getPost('quadroperiodoaula');
        $aulasseg = (int) $this->getRequest()->getPost('selectseg');
        $aulaster = (int) $this->getRequest()->getPost('selectter');
        $aulasqua = (int) $this->getRequest()->getPost('selectqua');
        $aulasqui = (int) $this->getRequest()->getPost('selectqui');
        $aulassex = (int) $this->getRequest()->getPost('selectsex');
        $aulassab = (int) $this->getRequest()->getPost('selectsab');
        $aulasdom = (int) $this->getRequest()->getPost('selectdom');

        $quadrohorarios = new Quadrohorarios();

        $_verificamateria = $quadrohorarios->getQuadrohorarios(array('idescolavinculo' => $quadroidescolavinculo, 'idmateria' => $quadroidmateria));

        if (sizeof($_verificamateria) > 0)
            die('Está matéria já esta adicionada para essa sala.');

        $dados = array();
        $dados["idsecretaria"] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        $dados["idescolavinculo"] = $quadroidescolavinculo;
        $dados["idmateria"] = $quadroidmateria;
        $dados["idprofessor"] = $quadroidsprofessores;
        $dados["quantidadesaulas"] = $quantidadesaulas;
        $dados["quadroperiodoaula"] = $quadroperiodoaula;
        $dados["status"] = 'Ativo';
        $dados["aulasseg"] = $aulasseg;
        $dados["aulaster"] = $aulaster;
        $dados["aulasqua"] = $aulasqua;
        $dados["aulasqui"] = $aulasqui;
        $dados["aulassex"] = $aulassex;
        $dados["aulassab"] = $aulassab;
        $dados["aulasdom"] = $aulasdom;
        $dados['excluido'] = 'nao';
        $dados['logusuario'] = $this->_usuario['id'];

        $dados['logdata'] = date('Y-m-d G:i:s');

        $row = $quadrohorarios->save($dados);
        die('OK');
    }    
    
// end public function
}
