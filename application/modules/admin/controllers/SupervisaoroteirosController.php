<?php

/**
 * Controle da classe supervisaoroteiros do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_SupervisaoroteirosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Supervisaoroteiro
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("supervisaoroteiros", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Supervisaoroteiros();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Roteiro excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="supervisaoroteiros") $objs = new Supervisaoroteiros();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'supervisaoroteiros', 'name' => 'Roteiros'),
			array('url' => null,'name' => 'Visualizar Roteiro')
		);
		
		$id = (int)$this->_request->getParam("id");
		$supervisaoroteiros = new Supervisaoroteiros();
		$supervisaoroteiro = $supervisaoroteiros->getSupervisaoroteiroById($id, array());
		
		if (!$supervisaoroteiro) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $supervisaoroteiro;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Roteiros')
		);
		
		$ns = new Zend_Session_Namespace('default_supervisaoroteiros');
		$supervisaoroteiros = new Supervisaoroteiros();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
			if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
			if (isset($this->view->post_var["idescolafuncionario"]) and $this->view->post_var["idescolafuncionario"]!="") $queries["idescolafuncionario"] = $this->view->post_var["idescolafuncionario"];
			
			if (isset($this->view->post_var["idequipesupervisao"]) and $this->view->post_var["idequipesupervisao"]!="") $queries["idequipesupervisao"] = $this->view->post_var["idequipesupervisao"];

			if ($this->view->post_var["tipo_filter"]!="") $queries["tipo_filter"] = $this->view->post_var["tipo_filter"];

			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $supervisaoroteiros->getSupervisaoroteiros($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;
		
		$this->view->rows = $supervisaoroteiros->getSupervisaoroteiros($queries, $paginaAtual, $maxpp);
	}
	/**
	 * 
	 * Action de edição de supervisaoroteiros
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'supervisaoroteiros', 'name' => 'Roteiros'),
			array('url' => null,'name' => 'Editar Roteiro')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$supervisaoroteiros = new Supervisaoroteiros();
		$supervisaoroteiro = $supervisaoroteiros->getSupervisaoroteiroById($id);
		
		if (!$supervisaoroteiro) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $supervisaoroteiro;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($supervisaoroteiro);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Roteiro editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }
	
	/**
	 * 
	 * Action de adição de supervisaoroteiros 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'supervisaoroteiros', 'name' => 'Roteiros'),
			array('url' => null,'name' => 'Adicionar Roteiro')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Roteiro adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idsupervisaoroteiro
     */    
    private function preForm($idsupervisaoroteiro = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_supervisaoroteiro = false) {
    	
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		$idequipesupervisao = (int)trim($this->getRequest()->getPost("idequipesupervisao"));
		$idescolafuncionario = (int)trim($this->getRequest()->getPost("idescolafuncionario"));
		$idveiculo = (int)trim($this->getRequest()->getPost("idveiculo"));
		$tipo = trim($this->getRequest()->getPost("tipo"));
		$secretaria = trim($this->getRequest()->getPost("secretaria"));
		$idsescolas = $this->getRequest()->getPost("idsescolas");
		$idtipoensino = (int)trim($this->getRequest()->getPost("idtipoensino"));
		$periodo = trim($this->getRequest()->getPost("periodo"));
		$status = trim($this->getRequest()->getPost("status1"));
				
				
		$erros = array();
		
		if (""==$tipo) array_push($erros, "Informe o Tipo.");
		if (""==$data) array_push($erros, "Informe a Data.");
		//if (0==$idescolafuncionario) array_push($erros, "Informe a Funcionário.");
		//if (""==$secretaria) array_push($erros, "Informe a Secretaria.");
		//if (0==$periodo) array_push($erros, "Informe a Período.");
		// if (""==$periodo) array_push($erros, "Informe a Periodo.");
		if (""==$status) array_push($erros, "Informe a Status.");
		if($tipo=="individual" AND $idescolafuncionario==0) array_push($erros, "Selecione o funcionário");
		
		$supervisaoroteiros = new Supervisaoroteiros();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		$idsescolas = implode(',', $idsescolas);
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["data"] = date("Y-m-d", $data);
			$dados["idequipesupervisao"] = $idequipesupervisao;
			$dados["idescolafuncionario"] = $idescolafuncionario;
			$dados["tipo"] = $tipo;
			$dados["secretaria"] = $secretaria;
			$dados["idsescolas"] = $idsescolas;
			$dados["idtipoensino"] = $idtipoensino;
			$dados["idveiculo"] = $idveiculo;
			$dados["periodo"] = $periodo;
			$dados["status"] = $status;

						
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $supervisaoroteiros->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }

    public function getveiculoAction()
    {
		$this->_helper->layout->disableLayout();
		
		$idveiculo = (int)$this->getRequest()->getPost("idveiculo");
		if($idveiculo <= 0) $this->view->row = array();
		
		$row = Supervisaoveiculos::getSupervisaoveiculoByIdHelper($idveiculo);
		$this->view->row = $row;
		//var_dump($row);die();
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}