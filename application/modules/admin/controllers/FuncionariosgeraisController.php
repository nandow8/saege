<?php

/**
 * Controle da classe funcionariosgerais do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_FuncionariosgeraisController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Funcionariogeral
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("funcionariosgerais", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Funcionariosgerais();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="funcionariosgerais") $objs = new Funcionariosgerais();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'funcionariosgerais', 'name' => 'Funcionários'),
			array('url' => null,'name' => 'Visualizar Funcionário')
		);
		
		$id = (int)$this->_request->getParam("id");
		$funcionariosgerais = new Funcionariosgerais();
		$funcionariogeral = $funcionariosgerais->getFuncionariogeralById($id, array());
		
		if (!$funcionariogeral) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $funcionariogeral;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Funcionários')
		);
		
		$ns = new Zend_Session_Namespace('default_funcionariosgerais');
		$funcionariosgerais = new Funcionariosgerais();
		$queries = array();	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["nome"]!="") $queries["nome"] = $this->view->post_var["nome"];
if ($this->view->post_var["telefone"]!="") $queries["telefone"] = $this->view->post_var["telefone"];
if ($this->view->post_var["tipo"]!="") $queries["tipo"] = $this->view->post_var["tipo"];
if ($this->view->post_var["iddepartamentosecretaria"]!="") $queries["iddepartamentosecretaria"] = $this->view->post_var["iddepartamentosecretaria"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $funcionariosgerais->getFuncionariosgerais($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $funcionariosgerais->getFuncionariosgerais($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de funcionariosgerais
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'funcionariosgerais', 'name' => 'Funcionários'),
			array('url' => null,'name' => 'Editar Funcionário')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$funcionariosgerais = new Funcionariosgerais();
		$funcionariogeral = $funcionariosgerais->getFuncionariogeralById($id);
		
		if (!$funcionariogeral) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $funcionariogeral;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($funcionariogeral);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de funcionariosgerais 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'funcionariosgerais', 'name' => 'Funcionários'),
			array('url' => null,'name' => 'Adicionar Funcionário')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Funcionário adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idfuncionariogeral
     */    
    private function preForm($idfuncionariogeral = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_funcionariogeral = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
$idimagem = (int)trim($this->getRequest()->getPost("idimagem"));
$idcargo = (int)trim($this->getRequest()->getPost("idcargo"));
$nome = trim($this->getRequest()->getPost("nome"));
$telefone = trim($this->getRequest()->getPost("telefone"));
$modulo = trim($this->getRequest()->getPost("modulo"));
$tipo = trim($this->getRequest()->getPost("tipo"));
$idendereco = (int)trim($this->getRequest()->getPost("idendereco"));
$matrícula = trim($this->getRequest()->getPost("matrícula"));
$secretaria = trim($this->getRequest()->getPost("secretaria"));
$iddepartamentosecretaria = (int)trim($this->getRequest()->getPost("iddepartamentosecretaria"));
$setor = trim($this->getRequest()->getPost("setor"));
$funcao = trim($this->getRequest()->getPost("funcao"));
$descricaofuncao = trim($this->getRequest()->getPost("descricaofuncao"));
$riscofuncao = trim($this->getRequest()->getPost("riscofuncao"));
$email = trim($this->getRequest()->getPost("email"));
$datanascimento = Mn_Util::stringToTime($this->getRequest()->getPost("datanascimento"));
$emissaocarteira = Mn_Util::stringToTime($this->getRequest()->getPost("emissaocarteira"));
$reservista = trim($this->getRequest()->getPost("reservista"));
$rg = trim($this->getRequest()->getPost("rg"));
$cpf = trim($this->getRequest()->getPost("cpf"));
$pispasep = trim($this->getRequest()->getPost("pispasep"));
$cnh = trim($this->getRequest()->getPost("cnh"));
$cnh_numero = trim($this->getRequest()->getPost("cnh_numero"));
$cnh_categoria = trim($this->getRequest()->getPost("cnh_categoria"));
$cnh_validade = Mn_Util::stringToTime($this->getRequest()->getPost("cnh_validade"));
$estrangeiro = trim($this->getRequest()->getPost("estrangeiro"));
$obs_estrangeiro = trim($this->getRequest()->getPost("obs_estrangeiro"));
$nacionalidade = trim($this->getRequest()->getPost("nacionalidade"));
$localnascimento = trim($this->getRequest()->getPost("localnascimento"));
$grauinstrucao = trim($this->getRequest()->getPost("grauinstrucao"));
$estadocivil = trim($this->getRequest()->getPost("estadocivil"));
$conjugenome = trim($this->getRequest()->getPost("conjugenome"));
$filhos = trim($this->getRequest()->getPost("filhos"));
$filhosquantidade = trim($this->getRequest()->getPost("filhosquantidade"));
$celular = trim($this->getRequest()->getPost("celular"));
$dataadmissao = Mn_Util::stringToTime($this->getRequest()->getPost("dataadmissao"));
$carteiranumero = trim($this->getRequest()->getPost("carteiranumero"));
$carteiraserie = trim($this->getRequest()->getPost("carteiraserie"));
$filiacaomae = trim($this->getRequest()->getPost("filiacaomae"));
$filiacaopai = trim($this->getRequest()->getPost("filiacaopai"));
$ctps = trim($this->getRequest()->getPost("ctps"));
$dataregistroctps = Mn_Util::stringToTime($this->getRequest()->getPost("dataregistroctps"));
$regime = trim($this->getRequest()->getPost("regime"));
$regime_outros = trim($this->getRequest()->getPost("regime_outros"));
$bolsista = trim($this->getRequest()->getPost("bolsista"));
$datainiciobolsa = Mn_Util::stringToTime($this->getRequest()->getPost("datainiciobolsa"));
$datafimbolsa = Mn_Util::stringToTime($this->getRequest()->getPost("datafimbolsa"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		//if (""==$tipo) array_push($erros, "Informe a Tipo.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$funcionariosgerais = new Funcionariosgerais();
		
	//	var_dump($erros); die();
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$enderecosModel = new EnderecoModel($this);
			$endereco = $enderecosModel->getPost( ($_funcionariogeral) ? (int)$_funcionariogeral['idendereco'] : 0, "e_idendereco");
			$idendereco = ($endereco) ? $endereco['id'] : 0;	

			$idimagem = $this->getImagem('idimagem');
			if ($idimagem!=0) $dados['idimagem'] = $idimagem;

			$dados['idendereco'] = $idendereco;
			$dados['idcargo'] = $idcargo;
			$dados['nome'] = $nome;
			$dados['telefone'] = $telefone;
$dados["modulo"] = 'Secretaria';
$dados["secretaria"] = $secretaria;
$dados["iddepartamentosecretaria"] = $iddepartamentosecretaria;
$dados["setor"] = $setor;
$dados["funcao"] = $funcao;
$dados["descricaofuncao"] = $descricaofuncao;
$dados["riscofuncao"] = $riscofuncao;
$dados["email"] = $email;
$dados["datanascimento"] = date("Y-m-d", $datanascimento);
$dados["emissaocarteira"] = date("Y-m-d", $emissaocarteira);
$dados["reservista"] = $reservista;
$dados["rg"] = $rg;
$dados["cpf"] = $cpf;
$dados["pispasep"] = $pispasep;
$dados["cnh"] = $cnh;
$dados["cnh_numero"] = $cnh_numero;
$dados["cnh_categoria"] = $cnh_categoria;
$dados["cnh_validade"] = date("Y-m-d", $cnh_validade);
$dados["estrangeiro"] = $estrangeiro;
$dados["obs_estrangeiro"] = $obs_estrangeiro;
$dados["nacionalidade"] = $nacionalidade;
$dados["localnascimento"] = $localnascimento;
$dados["grauinstrucao"] = $grauinstrucao;
$dados["estadocivil"] = $estadocivil;
$dados["conjugenome"] = $conjugenome;
$dados["filhos"] = $filhos;
$dados["filhosquantidade"] = $filhosquantidade;
$dados["celular"] = $celular;
$dados["dataadmissao"] = date("Y-m-d", $dataadmissao);
$dados["carteiranumero"] = $carteiranumero;
$dados["carteiraserie"] = $carteiraserie;
$dados["filiacaomae"] = $filiacaomae;
$dados["filiacaopai"] = $filiacaopai;
$dados["ctps"] = $ctps;
$dados["dataregistroctps"] = date("Y-m-d", $dataregistroctps);
$dados["regime"] = $regime;
$dados["regime_outros"] = $regime_outros;
$dados["bolsista"] = $bolsista;
$dados["datainiciobolsa"] = date("Y-m-d", $datainiciobolsa);
$dados["datafimbolsa"] = date("Y-m-d", $datafimbolsa);
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $funcionariosgerais->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}