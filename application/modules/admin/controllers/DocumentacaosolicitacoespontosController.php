<?php

/**
 * Controle da classe documentacaosolicitacoespontos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_DocumentacaosolicitacoespontosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Documentacaosolicitacoesponto
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("documentacaosolicitacoespontos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Documentacaosolicitacoespontos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Ponto excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="documentacaosolicitacoespontos") $objs = new Documentacaosolicitacoespontos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaosolicitacoespontos', 'name' => 'Solicitações de Ponto'),
			array('url' => null,'name' => 'Visualizar Solicitação de Ponto')
		);
		
		$id = (int)$this->_request->getParam("id");
		$documentacaosolicitacoespontos = new Documentacaosolicitacoespontos();
		$documentacaosolicitacoesponto = $documentacaosolicitacoespontos->getDocumentacaosolicitacoespontoById($id, array());
		
		if (!$documentacaosolicitacoesponto) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $documentacaosolicitacoesponto;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
    public function getcursoAction() {
    	$this->_helper->layout->disableLayout();

    	$idcurso = (int)$this->_request->getPost("idcurso");
    	if($idcurso <= 0)
    	{
    		echo json_encode(array('status' => 'ERRO', 'dados' => null));
    	}
    	
    	$rows = new Documentacaocursospontuacao();
    	$curso = $rows->getDocumentacaocursopontuacaoById($idcurso);

		echo json_encode(array('status' => 'OK', 'dados' => $curso));
		die();
    }
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Solicitações de Ponto')
		);
		
		$ns = new Zend_Session_Namespace('default_documentacaosolicitacoespontuacao');
		$documentacaosolicitacoespontos = new Documentacaosolicitacoespontos();
		$queries = array();	
		if(Usuarios::getUsuario('professor') == 'Sim')
		{
			$queries['idprofessor'] = Usuarios::getUsuario('idfuncionario');
		}
		else
		{
			$queries['idescola'] = Usuarios::getEscolaAtiva(Usuarios::getUsuario('id'), 'id');
		}
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idprofessor"]!="") $queries["idprofessor"] = $this->view->post_var["idprofessor"];
			if ($this->view->post_var["idcurso"]!="") $queries["idcurso"] = $this->view->post_var["idcurso"];
			if ($this->view->post_var["posicao"]!="") $queries["posicao"] = $this->view->post_var["posicao"];
			if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
			if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $documentacaosolicitacoespontos->getDocumentacaosolicitacoespontos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $documentacaosolicitacoespontos->getDocumentacaosolicitacoespontos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de documentacaosolicitacoespontos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaosolicitacoespontos', 'name' => 'Solicitações de Ponto'),
			array('url' => null,'name' => 'Editar Solicitação de Ponto')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$documentacaosolicitacoespontos = new Documentacaosolicitacoespontos();
		$documentacaosolicitacoesponto = $documentacaosolicitacoespontos->getDocumentacaosolicitacoespontoById($id);
		
		if (!$documentacaosolicitacoesponto && $documentacaosolicitacoesponto['posicao'] == 'Aguardando') 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $documentacaosolicitacoesponto;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($documentacaosolicitacoesponto);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Ponto editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de documentacaosolicitacoespontos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'documentacaosolicitacoespontos', 'name' => 'Solicitações de Ponto'),
			array('url' => null,'name' => 'Adicionar Solicitação de Ponto')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação de Ponto adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }

    	/**
	 * 
	 * Action de adição de documentacaosolicitacoespontos 
	 */
	public function importarAction() {
		die('aqui');
    }
    
    /**
     * Atribui valores ao view
     * @param int $iddocumentacaosolicitacoesponto
     */    
    private function preForm($iddocumentacaosolicitacoesponto = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_documentacaosolicitacoesponto = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idusuario = (int)trim($this->getRequest()->getPost("idusuario"));
		$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
		$idcurso = (int)trim($this->getRequest()->getPost("idcurso"));
		$idarquivo = (int)trim($this->getRequest()->getPost("idarquivo"));
		$posicao = trim($this->getRequest()->getPost("posicao"));
		$pontuacao = trim($this->getRequest()->getPost("pontuacao"));
		$status = trim($this->getRequest()->getPost("status1"));

		/*MULTIPLOS ARQUIVOS*/
		$idsarquivos = $this->getRequest()->getPost("idsarquivos");
		$legendasarquivos =  $this->getRequest()->getPost("legendasarquivos");
		//var_dump($idsarquivos);die();
				
		
		$erros = array();
		
		if (0==$idprofessor) array_push($erros, "Informe a Professor.");
		if (""==$idcurso) array_push($erros, "Informe a Curso.");
		if (""==$posicao && Usuarios::getUsuario('professor') != 'Sim') array_push($erros, "Informe a Posição.");

		
		$documentacaosolicitacoespontos = new Documentacaosolicitacoespontos();

		if (sizeof($idsarquivos) <= 0 && Usuarios::getUsuario('professor') == 'Sim') array_push($erros, "Selecione um anexo.");
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idusuario"] = $idusuario;
			if(Usuarios::getUsuario('professor') == 'Sim')
			{
				$dados["idprofessor"] = $idprofessor;
			}
			$dados["idcurso"] = $idcurso;

			if ($idarquivo!=0) $dados['idarquivo'] = $idarquivo;

			$dados["posicao"] = (Usuarios::getUsuario('professor') == 'Sim') ? 'Aguardando' : $posicao;
			$dados["pontuacao"] = $pontuacao;
			$dados["status"] = 'Ativo';

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $documentacaosolicitacoespontos->save($dados);

			if($row)
			{
				$multiplosarquivos = new Documentacaosolicitacoespontosarquivos(); 
				$multiplosarquivos->setArquivos($row['id'], $idsarquivos, $legendasarquivos);
			}
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}