<?php

/**
 * Controle da classe rhdeclaracoesdependentes do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_RhdeclaracoesdependentesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Rhdeclaracaodependente
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("rhdeclaracoesdependentes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Rhdeclaracoesdependentes();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Declaração excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="rhdeclaracoesdependentes") $objs = new Rhdeclaracoesdependentes();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'rhdeclaracoesdependentes', 'name' => 'Declarações'),
			array('url' => null,'name' => 'Visualizar Declaração')
		);
		
		$id = (int)$this->_request->getParam("id");
		$rhdeclaracoesdependentes = new Rhdeclaracoesdependentes();
		$rhdeclaracaodependente = $rhdeclaracoesdependentes->getRhdeclaracaodependenteById($id, array());
		
		if (!$rhdeclaracaodependente) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $rhdeclaracaodependente;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Declarações')
		);
		
		$ns = new Zend_Session_Namespace('default_rhdeclaracoesdependentes');
		$rhdeclaracoesdependentes = new Rhdeclaracoesdependentes();
		$queries = array();	
		$queries['idsecretaria'] = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );	
		$queries["origem"] = "Secretaria";
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idsecretaria"]!="") $queries["idsecretaria"] = $this->view->post_var["idsecretaria"];
if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $rhdeclaracoesdependentes->getRhdeclaracoesdependentes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $rhdeclaracoesdependentes->getRhdeclaracoesdependentes($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de rhdeclaracoesdependentes
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'rhdeclaracoesdependentes', 'name' => 'Declarações'),
			array('url' => null,'name' => 'Editar Declaração')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$rhdeclaracoesdependentes = new Rhdeclaracoesdependentes();
		$rhdeclaracaodependente = $rhdeclaracoesdependentes->getRhdeclaracaodependenteById($id);
		
		if (!$rhdeclaracaodependente) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $rhdeclaracaodependente;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($rhdeclaracaodependente);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Declaração editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de rhdeclaracoesdependentes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'rhdeclaracoesdependentes', 'name' => 'Declarações'),
			array('url' => null,'name' => 'Adicionar Declaração')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Declaração adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function gerapdfAction() {
		$id = (int)$this->_request->getParam("id");
		$declaracoes = new Rhdeclaracoesdependentes();
		$declaracao = $declaracoes->getRhdeclaracaodependenteById($id);
		
		if (!$declaracao) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		$cargo = false;

		$_cargo = Cargos::getCargoByIdHelper($declaracao['idcargo']);

		if(isset($_cargo['cargo'])) $declaracao['cargo'] = $_cargo['cargo'];
		
		$this->view->post_var = $declaracao;
		$endereco = Enderecos::getEnderecoById((int)$declaracao['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->post_var = array_merge($this->view->post_var, $endereco);
		}
		
		$this->view->rows = Rhdeclaracoesdependentespessoas::getRhdeclaracoesdependentespessoasHelper(array('iddeclaracao'=>$id));
		$this->preForm();
		//var_dump($this->view->post_var); die();
		
		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('p');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('Diploma');
		$pdf->SetSubject('Diploma');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		
		
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('rhdeclaracoesdependentes/pdf/index.phtml');
		//echo $html; die();

		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'declaracao_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}

    /**
     * Atribui valores ao view
     * @param int $idrhdeclaracaodependente
     */    
    private function preForm($idrhdeclaracaodependente = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_rhdeclaracaodependente = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
$status = trim($this->getRequest()->getPost("status1"));
		
		$idsdependentes = $this->getRequest()->getPost("idsdependentes");
		$nomes = $this->getRequest()->getPost("nome");
		$grauparentesco = $this->getRequest()->getPost("grauparentesco");
		$nascimento = $this->getRequest()->getPost("nascimento");		
		$erros = array();
		
		if (0==$idsecretaria) array_push($erros, "Informe a Secretaria.");
if (0==$idfuncionario) array_push($erros, "Informe a Funcionário.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$rhdeclaracoesdependentes = new Rhdeclaracoesdependentes();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados['origem'] = 'Secretaria';
			$dados["idsecretaria"] = $idsecretaria;
$dados["idfuncionario"] = $idfuncionario;
$dados["status"] = $status;
			$itenssaidas = array();
			$itenssaidas['idsdependentes'] = $idsdependentes;
			$itenssaidas['nomes'] = $nomes;
			$itenssaidas['grauparentesco'] = $grauparentesco;
			$itenssaidas['nascimento'] = $nascimento;
			$dados['itens'] = $itenssaidas;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $rhdeclaracoesdependentes->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}