<?php

class Admin_LteveiculosestudantesController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Pacotealuno
     */
    protected $_usuario = null; 
    
    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect("/admin/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
        Mn_Util::blockAccess("lteveiculosestudantes", $this->_request->getActionName());    
        
        $this->_usuario = unserialize($loginNameSpace->usuario);
        
        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }               
    }   
    

    /**
     * 
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->_request->getPost("id");
        
        $rows = new Lteveiculosestudantes();
        $row = $rows->fetchRow("id=".$id);
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = Usuarios::getUsuario('id');
            $row['logdata'] = date('Y-m-d G:i:s');          
            
            $rows->save($row);
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "VeiculoEstudante excluído com sucesso.";
            
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Veículo excluído com sucesso!");
            
            die("OK");
        }
        
        die("Não encontrado!");
        
    }  
    
    public function ordemAction() {
        $ordem = (int) $this->getRequest()->getParam('ordem', 0);
        $d = $this->getRequest()->getParam('d', 0);
        
        $rows = new Lteveiculosestudantes();
        $rows->swapOrdem($ordem, $d, false);

        $this->_redirect("/admin/".$this->_request->getControllerName()."/index");
        die();  
    }   
    
    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'VeiculoEstudante')
        );

        $ns = new Zend_Session_Namespace('admin_veiculos_estudantes');
        $veiculosestudantes = new Lteveiculosestudantes();
        $queries = array(); 

        $this->preForm();
        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('/admin/' . $this->getRequest()->getControllerName());
            die();  
        }
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) { 

            foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = $v;
            if ((isset($this->view->post_var['veiculo'])) && ($this->view->post_var['veiculo']!='')) $queries['veiculo'] = $this->view->post_var['veiculo'];
            if ((isset($this->view->post_var['placa'])) && ($this->view->post_var['placa']!='')) $queries['placa'] = $this->view->post_var['placa'];
            if ((isset($this->view->post_var['capacidade'])) && ($this->view->post_var['capacidade']!='')) $queries['capacidade'] = $this->view->post_var['capacidade'];
        }       
        
        //PAGINACAO
        $maxpp = 15;
        
        $paginaAtual = (int)$this->getRequest()->getParam('p');
        if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
        if ($paginaAtual==0) $paginaAtual = 1;
        
        $queries['total'] = true;
        $totalRegistros = $veiculosestudantes->getVeiculos($queries);
        $paginaTotal = ceil($totalRegistros/$maxpp);
        $queries['total'] = false;
        if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual<0) $paginaAtual = 0;
        
        $this->view->pagina_atual = $paginaAtual+1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;           
        
        $this->view->rows = $veiculosestudantes->getVeiculos($queries, $paginaAtual, $maxpp);  
    }
    
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'lteveiculosestudantes', 'name' => 'VeiculoEstudantes'),
            array('url' => null,'name' => 'Visualizar VeiculoEstudante')
        );

        $id = (int)$this->_request->getParam("id");
        $veiculosestudantes = new Lteveiculosestudantes();
        $veiculosestudante = $veiculosestudantes->getVeiculosById($id);

        if (!$veiculosestudante) 
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());
        
        $this->view->visualizar = true;
        $this->view->post_var = $veiculosestudante;

        $this->preForm();

        
    }
    
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'lteveiculosestudantes', 'name' => 'VeiculoEstudantes'),
            array('url' => null,'name' => 'Editar VeiculoEstudante')
        );  

        $id = (int)$this->_request->getParam("id");
        $veiculosestudantes = new Lteveiculosestudantes();
        $veiculosestudante = $veiculosestudantes->getVeiculosById($id);

        if (!$veiculosestudante) 
            $this->_redirect('admin/' . $this->getRequest()->getControllerName());

        $this->preForm();

        $this->view->post_var = $veiculosestudante;
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($veiculosestudante);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "VeiculoEstudante editado com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }       
        return true;        
    }       
    
    /**
     * 
     * Action de adição de alunos 
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'lteveiculosestudantes', 'name' => 'VeiculoEstudantes'),
            array('url' => null,'name' => 'Adicionar aluno')
        );  

        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "VeiculoEstudante adicionado com sucesso.";
            
            $this->_redirect('admin/'.$this->getRequest()->getControllerName());    
        }
        return true;        
    }   
    
    public function setestudantesAction() {
        $this->_helper->layout->disableLayout();

        $identidade = (int)$this->getRequest()->getPost('identidade');
        $tipo = $this->getRequest()->getPost('tipo');

        $estudantes = new Lteveiculosestudantes();
        $this->view->rows = $estudantes->getViewAlunos(array('identidade' => $identidade, 'tipo' => $tipo));
    }
    
    public function setvinculosAction() {
        $this->_helper->layout->disableLayout();

        $idveiculo     = (int)$this->getRequest()->getPost('idveiculo');
        $idestudante   = (int)$this->getRequest()->getPost('idestudante');
        $tipoestudante = $this->getRequest()->getPost('tipo');

        if ($idestudante == 0) {

            $veiculosestudantes = new Lteveiculosestudantes();
            $this->view->rows = $veiculosestudantes->getVinculos(array('idveiculo' => $idveiculo));

        } else {

            $vinculos = new Lteveiculosestudantes();
            $row = $vinculos->getVeiculos(array('id'=>$idveiculo));
            
            $total = $vinculos->getVinculos(array('idveiculo'=>$idveiculo, 'total'=>true));

            if(!$row) die('ERRO!');
            if(($total) >= ($row[0]['quantidade'])) {
                die('O veículo já atingiu a sua capacidade MÁXIMA ('.$row[0]['quantidade'].')');
            } else {

                $item = $vinculos->save(array('idveiculo' => $idveiculo, 'idestudante' => $idestudante, 'tipoestudante' => $tipoestudante));
                die($item->id);                
            }
        }
    }

    public function setentidadesAction() {
        $this->_helper->layout->disableLayout();
        
        $queries = array();
        $tipo = $this->getRequest()->getPost('tipo');

        if ($tipo == '') {
            $this->view->rows = '';
        } else {
            $entidades = new Lteveiculosestudantes();
            $this->view->rows = $entidades->getEntidadesTransporte(array('tipo'=>$tipo)); 
        }
    }

    public function excluiritemAction() {
        $this->_helper->layout->disableLayout();
        
        $queries = array();
        $iditem = (int)$this->getRequest()->getPost('iditem');

        $vinculo = new Lteveiculosestudantes();
        $row = $vinculo->getVinculosById($iditem); 
        if(!$row) die('ERRO!');
        
        $row['excluido'] = 'sim';
        $row['logusuario'] = Usuarios::getUsuario('id');
        $row['logdata'] = date('Y-m-d G:i:s');
        
        $vinculo->save($row);
        
        Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Item excluído do veículo com sucesso!");
        
        // die('OK');
    }

    public function vincularAction() {
        $this->_helper->layout->disableLayout();
        
        $idveiculo   = $this->getRequest()->getPost('idveiculo');
        $idestudante = $this->getRequest()->getPost('idestudante');
        
        $vinculos = new Lteveiculosestudantes();
        $row = $vinculos->getVeiculos(array('id'=>$idveiculo));
        
        $total = $vinculos->getVinculosVeiculos(array('idveiculo'=>$idveiculo, 'total'=>true));

        if(!$row) die('ERRO!');
        if(($total) >= ($row[0]['quantidade'])) {
            die('A Sala já atingiu a sua capacidade MÁXIMA ('.$row[0]['quantidade'].')');
        } else {
            $vinculos->save(array('idveiculo' => $idveiculo, 'idestudante' => $idestudante));
            //validar retorno
            die('ok');
        }
    }
    
    /**
     * Atribui valores ao view
     * @param int $idaluno
     */    
    private function preForm($idaluno = 0) {           

    }
}
