<?php

class Admin_AlmoxarifadousuariosController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Marca
     */
    protected $_usuario = null; 
    
    
    /**
     * Verificação de permissao de acesso
     */ 
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
        
        Mn_Util::blockAccess("almoxarifadousuarios", $this->_request->getActionName());   
        
        $this->_usuario = unserialize($loginNameSpace->usuario);
        
        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->_request->getPost("id");
        
        $rows = new Almoxarifadousuarios();
        $row = $rows->fetchRow("id=".$id);
        
        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');          
            
            $rows->save($row);
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Marca excluído com sucesso.";
            
            die("OK");
        }
        
        die("Não encontrado!");
    }       
    
    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");
        
        $id = (int)$this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");
        
        if ($op=="almoxarifadousuarios") $objs = new Almoxarifadousuarios();
        $obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');
            
            $objs->save($obj);
            
            die($obj['status']);
        }
        
        die("Não encontrado!");
    }       
    
    
    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadousuarios', 'name' => 'Almoxarifadousuarios'),
            array('url' => null,'name' => 'Visualizar Marca')
        );
        
        $id = (int)$this->_request->getParam("id");
        $usuarios = new Almoxarifadousuarios();
        $usuario = $usuarios->getAlmoxarifadoUsuariosById($id, array());
        
        if (!$usuario) 
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        
        $this->view->post_var = $usuario;

        $this->view->visualizar = true;
        return true;
    }


    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false,'name' => 'Almoxarifadousuarios')
        );
        
        $ns = new Zend_Session_Namespace('admin_almoxarifadousuarios');
        $usuarios = new AlmoxarifadoUsuarios();
        $queries = array();

        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
            die();  
        }
        
        if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
        
        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);

            if ($this->view->post_var["status"]!="") $queries["status"] = $this->view->post_var["status"];
            
            if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
        }       
        
        //PAGINACAO
        $maxpp = 20;
        
        $paginaAtual = (int)$this->getRequest()->getParam('p');
        if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
        if ($paginaAtual==0) $paginaAtual = 1;
        
        $queries['total'] = true;
        $totalRegistros = $usuarios->getAlmoxarifadoUsuarios($queries);
        $paginaTotal = ceil($totalRegistros/$maxpp);
        $queries['total'] = false;
        if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual<0) $paginaAtual = 0;
        
        $this->view->pagina_atual = $paginaAtual+1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;           
        
        $this->view->rows = $usuarios->getAlmoxarifadoUsuarios($queries, $paginaAtual, $maxpp); 
    }
    
    /**
     * 
     * Action de edição de usuarios
     */ 
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadousuarios', 'name' => 'Almoxarifadousuarios'),
            array('url' => null,'name' => 'Editar Marca')
        );  

        $id = (int)$this->_request->getParam("id");
        $usuarios = new Almoxarifadousuarios();
        $usuario = $usuarios->getAlmoxarifadoUsuariosById($id);
        
        if (!$usuario) 
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        
        $this->view->post_var = $usuario;
        
        if ($this->_request->isPost()) {
            $erros = $this->getPost($usuario);
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Marca editado com sucesso.";
            
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }   

        return true;        
    }       
    
    /**
     * 
     * Action de adição de usuarios 
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'almoxarifadousuarios', 'name' => 'Almoxarifadousuarios'),
            array('url' => null,'name' => 'Adicionar Marca')
        );  

        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);
            
            if ($erros!="") {
                $this->view->erros = $erros;
                return false; 
            }
            
            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Marca adicionado com sucesso.";
            
            $this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
        }
        
        return true;        
    }

    public function setfuncionariosAction() {
        $this->_helper->layout->disableLayout();

        $idestoque = (int)$this->getRequest()->getPost('idestoque');

        if ($idestoque == 0) {

           $this->view->rows = array();

        } else {

            $estoques = new Almoxarifadoestoques();

            $estoque = $estoques->getEstoquesById($idestoque);

            if ($estoque['idlocal'] == 29) {

                $queries = array('idescola' => $estoque['idescola']);

            } else {

                $queries = array('idlocal' => $estoque['idlocal']);

            }
            
            $Usuarios = new Almoxarifadousuarios();

            $this->view->rows = $Usuarios->getFuncionarios($queries);
        }
    }

    public function setperfisAction() {
        $this->_helper->layout->disableLayout();

        $idtipoestoque = (int)$this->getRequest()->getPost('idtipoestoque');

        $perfil = new Almoxarifadoperfis();

        $this->view->rows = $perfil->getAlmoxarifadoPerfis(array('id' => $idtipoestoque));
    }
    
    /**
     * Valida e grava os dados do formulário
     */    
    private function getPost($_usuario = false) {
        if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
        else $this->view->post_var = array_merge($this->view->post_var, $_POST);
        
        $id = (int)$this->getRequest()->getPost("id");
        $idfuncionario = (int)$this->getRequest()->getPost("idfuncionario");
        $idestoque = (int)$this->getRequest()->getPost("idestoque");
        $idperfil = (int)$this->getRequest()->getPost("idperfil");
        $status = trim($this->getRequest()->getPost("status"));

        $erros = array();
        
        if ($idfuncionario == 0) array_push($erros, "Informe um usuário.");
        if ($idestoque == 0) array_push($erros, "Erro ao identificar estoque.");
        if ($status == '') array_push($erros, "Informe a Status.");

        $usuarios = new Almoxarifadousuarios();

        if (sizeof($erros)>0) return $erros; 
        
        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;

            $dados["idfuncionario"] = $idfuncionario;
            $dados["idestoque"] = $idestoque;
            $dados["idperfilalmoxarifado"] = $idperfil;
            $dados["status"] = $status;

            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];;
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $usuarios->save($dados);
            
            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            
            $db->rollBack();
            die();
        }       
        
        return "";      
    }
}