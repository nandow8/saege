<?php  
 
error_reporting(0);
ini_set(“display_errors”, 0 );

// class MYPDF extends TCPDF {

//     //Page header
//     public function Header(){
        
//         $image_file = "public/admin/imagens/logosantaisabel.jpg";
//         $this->Image($image_file, 18, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
//         //Set font
//         $this->SetFont('helvetica', 'B', 20);
//         //Title
//         //$this->Cell(0, 15, '<< TCPDF Example 003 São Paulo >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', 'B', 10);
       
//         // cabeçalho Endereço
//         $this->SetXY(35, 12);
//         $this->Cell(0, 0, 'Município de Santa Isabel', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 9);
//         $this->SetXY(35, 16);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Secretaria de Educação - Setor de Merenda Escolar', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 19);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Avenida da República, 297 – Centro – Santa Isabel - SP – 07500-000', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetFont('helvetica', '', 7);
//         $this->SetXY(35, 22);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Horário de funcionamento: Das 08:00 às 17:00hrs ', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//         $this->SetXY(35, 25);
//         $this->SetTextColor(130, 130, 130);
//         $this->Cell(0, 0, 'Fone:  (11) 4656-1000 - E-mail: prefeituradesantaisabel@santaisabel.sp.gov.br', 0, false, 'L', 0, '', 0, false, 'M', 'M');
//     }

//     // Page footer
//     public function Footer() {
//         // Position at 15 mm from bottom
//         $this->SetY(-15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Página '. $this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        
//         // Position at 15 mm from bottom
//         $this->SetXY(150,-15);
//         // Set font
//         $this->SetFont('helvetica', 'I', 8);
//         // Page number
//         $this->Cell(0, 10, 'Prefeitura de Santa Isabel ', 0, false, 'C', 0, '', 0, false, 'T', 'M');
//     }
// }


 
/** 
 * Controle da classe nutricaocardapiosrefeicao do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_NutricaocardapiosrefeicaoController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Nutricaocardapiorefeicao
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("nutricaocardapiosrefeicao", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Nutricaocardapiosrefeicao();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Excluido com sucesso.";

		
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="nutricaocardapiosrefeicao") $objs = new Nutricaocardapiosrefeicao();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaocardapiosrefeicao', 'name' => 'Tipos'),
			array('url' => null,'name' => 'Visualizar Tipo')
		);
		
		$id = (int)$this->_request->getParam("id");
		$nutricaocardapiosrefeicao = new Nutricaocardapiosrefeicao();
		$nutricaocardapiorefeicao = $nutricaocardapiosrefeicao->getNutricaocardapiorefeicaoById($id, array());
		
		if (!$nutricaocardapiorefeicao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaocardapiorefeicao;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}

    /**
     * 
     * Action de adição de calendario 
     */
    public function adicionareventoAction() {
        $this->_helper->layout->disableLayout();

        $idnutricaocardapiosrefeicao = (int) $this->getRequest()->getPost("id");
        $title = "ssss";
        $color = "#1F1";
        $start = strip_tags(trim($this->getRequest()->getPost("start")));
        $dt = explode('/', $start);
        $start = $dt[2].'-'.$dt[1].'-'.$dt[0].' 00:00:00';
        $end = $start;
        
        $calendario = new Nutricaocardapiosrefeicaocalendario();

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            
            $dados['idnutricaocardapiosrefeicao'] = $idnutricaocardapiosrefeicao;
            $dados['title'] = $title;
            $dados['color'] = $color;
            $dados['start'] = $start;
            $dados['end'] = $end;

            $dados['excluido'] = 'nao';
            $dados['logusuario'] = Usuarios::getUsuario('id');
            $dados['logdata'] = date('Y-m-d G:i:s');



            $row = $calendario->save($dados);
            
            $db->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            die(false);
        }

        die(true);        
    }
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Tipos')
		);
		
		$ns = new Zend_Session_Namespace('default_nutricaocardapiosrefeicao');
		$nutricaocardapiosrefeicao = new Nutricaocardapiosrefeicao();
		$queries = array();	
				
		$filtro = $this->_request->getParam("id");

		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["tipo"]!="") $queries["tipo"] = $this->view->post_var["tipo"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];

			if ($this->view->post_var['ensino']!='') $queries['tipoensino'] = $this->view->post_var['ensino'];
			if ($this->view->post_var['tiporefeicao']!='') $queries['tiporefeicao'] = $this->view->post_var['tiporefeicao'];

    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
		}
		

		
		$this->view->rows = $row = $nutricaocardapiosrefeicao->getNutricaocardapiosrefeicao($queries, $paginaAtual, $maxpp);

		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $nutricaocardapiosrefeicao->getNutricaocardapiosrefeicao($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;

		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    
		
		$this->view->filtro = $filtro;
		$this->view->rows = $row = $nutricaocardapiosrefeicao->getNutricaocardapiosrefeicao($queries, $paginaAtual, $maxpp);


	}
	
	/**
	 * 
	 * Action de edição de nutricaocardapiosrefeicao
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaocardapiosrefeicao', 'name' => 'Tipos'),
			array('url' => null,'name' => 'Editar Tipo')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$nutricaocardapiosrefeicao = new Nutricaocardapiosrefeicao();
		$nutricaocardapiorefeicao = $nutricaocardapiosrefeicao->getNutricaocardapiorefeicaoById($id);
		
		if (!$nutricaocardapiorefeicao) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaocardapiorefeicao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($nutricaocardapiorefeicao);
			if ($erros!=""){
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Tipo editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;	
    }  		
	
	/**
	 * 
	 * Action de adição de nutricaocardapiosrefeicao 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaocardapiosrefeicao', 'name' => 'Tipos'),
			array('url' => null,'name' => 'Adicionar Tipo')
		);	

				
		$this->preForm();

		if ($this->getRequest()->isPost()){
			$erros = $this->getPost(false);
			
			if ($erros!=""){
				$this->view->erros = $erros;
				return false; 
			}

			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Tamanho de Prato adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
			
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idnutricaocardapiosrefeicao
     */    
    private function preForm($idnutricaocardapiosrefeicao = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_nutricaocardapiosrefeicao = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$refeicao = trim($this->getRequest()->getPost("refeicao"));
                $idsrefeicoes = trim($this->getRequest()->getPost("idsrefeicoes"));
                $tipoensino = trim($this->getRequest()->getPost("tipoensino"));
		$tiporefeicao = trim($this->getRequest()->getPost("tiporefeicao"));
		$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$refeicao) array_push($erros, "Informe a Refeicao.");
                if (""==$tipoensino) array_push($erros, "Informe o Tipo do Ensino.");
                if (""==$tiporefeicao) array_push($erros, "Informe o Tipo da Refeição.");
		if (""==$status) array_push($erros, "Informe o Status.");

		$nutricaocardapiosrefeicao = new Nutricaocardapiosrefeicao();
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["refeicao"] = $refeicao;
			$dados["idsrefeicoes"] = $idsrefeicoes;
                        $dados["tipoensino"] = $tipoensino;
                        $dados["tiporefeicao"] = $tiporefeicao;
			$dados["status"] = $status;

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $nutricaocardapiosrefeicao->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
	}
	
	public function retornarefeicoesajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = $this->getRequest()->getPost("id");

		$nutriRef = new Nutricaorefeicoes();

		$row = $nutriRef->getNutricaorefeicoes(array('id'=>$id));

		echo json_encode($row[0]);

		die();

	}//end function retornaRefeicoes

	public function cadastracardapioajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$titulo_cardapio = $this->getRequest()->getPost("titulo_cardapio");
		$tipo_ensino = $this->getRequest()->getPost("tipo_ensino");
		$tipo_refeicao = $this->getRequest()->getPost("tipo_refeicao");
		$lista_refeicoes = $this->getRequest()->getPost("lista_refeicoes");
		
		$nutriCardapioRef = new Nutricaocardapiosrefeicao();

		$dados[] = array();

		$dados['refeicao'] = $titulo_cardapio;//refeicao
		$dados['idsrefeicoes'] = implode(",",$lista_refeicoes);//idrefeicoes
		$dados['status'] = 'Ativo';//status
		$dados['excluido'] = 'nao';//excluido
		$dados['logdata'] =  date('Y-m-d G:i:s');//logdata
		$dados['logusuario'] = $this->_usuario['id'];//logusuario
		$dados['tipoensino'] = $tipo_ensino;//tipoensino
		$dados['tiporefeicao'] = $tipo_refeicao;//tiporefeicao

		$row = $nutriCardapioRef->save($dados);
	

		$dados['id'] = $row->id;

		echo json_encode($dados);

		die();
	}//end public function cadastracardapioajax


	public function updatecardapioajaxAction(){
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id_cardapio = $this->getRequest()->getPost("id_cardapio");
		$titulo_cardapio = $this->getRequest()->getPost("titulo_cardapio");
		$tipo_ensino = $this->getRequest()->getPost("tipo_ensino");
		$tipo_refeicao = $this->getRequest()->getPost("tipo_refeicao");
		$lista_refeicoes = $this->getRequest()->getPost("lista_refeicoes");

		//echo $tipo_ensino . '<br>';
		//echo $tipo_refeicao . '<br>';
		//$lista_refeicoes;

		
		$nutriCardapioRef = new Nutricaocardapiosrefeicao();

		$dados[] = array();

		$dados['id'] = $id_cardapio;
		$dados['refeicao'] = $titulo_cardapio;//refeicao
		$dados['idsrefeicoes'] = implode(",",$lista_refeicoes);//idrefeicoes
		$dados['status'] = 'Ativo';//status
		$dados['excluido'] = 'nao';//excluido
		$dados['logdata'] =  date('Y-m-d G:i:s');//logdata
		$dados['logusuario'] = $this->_usuario['id'];//logusuario
		$dados['tipoensino'] = $tipo_ensino;//tipoensino
		$dados['tiporefeicao'] = $tipo_refeicao;//tiporefeicao

		$row = $nutriCardapioRef->save($dados);

		$dados['id'] = $row->id;

		echo json_encode($dados);

		die();
	}//end public function cadastracardapioajax


	public function retornarefeicaoajaxAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = $this->getRequest()->getPost("id");


		$row = Nutricaocardapiosrefeicao::getNutricaocardapiorefeicaoByIdHelper($id);

		echo json_encode($row);

		die();

	}//end public function retornaferaicaoAjax

	public function cadastrarefeicaoagendaajaxAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = (int)$this->getRequest()->getPost("id");
		$title = $this->getRequest()->getPost("title");
		$start = $this->getRequest()->getPost("start");
		$end = $this->getRequest()->getPost("end");

		$nutricaoCardapioCalendario = new Nutricaocardapiosrefeicaocalendario();

		$data = array();

		$data['idnutricaocardapiosrefeicao'] = (int)$id;
		$data['title'] = $title;
		$data['color'] = '#000000';
		$data['start'] = str_replace('T',' ', $start);
		$data['end'] = str_replace('T',' ', $end);
		$data['status'] = 'Ativo';
		$data['excluido'] = 'nao';
		$data['logusuario'] = (int)$this->_usuario['id'];
		$data['logdata'] = date('Y-m-d G:i:s');//logdata;

		$row = $nutricaoCardapioCalendario->save($data);



		$dataReturn = array();

		$dataReturn['id'] = $row->id;
		$dataReturn['idnutricaocardapiosrefeicao'] = $row->idnutricaocardapiosrefeicao;
		$dataReturn['title'] = $row->title;
		$dataReturn['color'] = $row->color;
		
		echo json_encode($dataReturn);
		die();

	}// end public function cadastrarefeicaoagendaajaxAction

	public function retornacardapiocalendarioajaxAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$row = Nutricaocardapiosrefeicaocalendario::getNutricaocardapiorefeicaocalendarioHelper(array());

		echo json_encode($row);

		die();

	}//end function retornacardapiocalendarioajaxAction
	
	public function excluirefeicaoagendaajaxAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = $this->getRequest()->getPost("id");

		$rows = new Nutricaocardapiosrefeicaocalendario();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Tamanho de Prato excluído com sucesso.";
			die("OK");
		}
		die("Não encontrado!");
		
	}//end public function excluirefeicaoagendaajax

	public function nutricaoingredientesajaxtotaisAction(){

		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");

		$id = $this->getRequest()->getPost("id");

		$alimentosTotal = array();

		$rowNutricaoCardapioRefeicao = Nutricaocardapiosrefeicao::getNutricaocardapiorefeicaoByIdHelper($id);

		$refeicoesArray = explode(',',$rowNutricaoCardapioRefeicao['idsrefeicoes']);

		//echo '++++++++++++++++';
		
		
		foreach($refeicoesArray as $aux):

			//Recuperando as porções de cada refeição
			$porcaoRefeicao = Nutricaorefeicoes::getNutricaorefeicaoByIdHelper($aux);

			$rowIngredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$aux));

			foreach($rowIngredientes as $index):

				$alimento = Nutricaotacoalimentos::getNutricaotacoalimentoByIdHelper($index['idnutricaotacoalimento']);

				$alimento['valor_porcao'] = $porcaoRefeicao['porcoes'];
				$alimento['valor_qtde'] = $index['quantidade'];
				$alimento['valor_unidade'] = $index['unidade'];
				$alimento['valor_unidadevalor'] = $index['unidadevalor'];
				$alimento['valor_observacao'] = $index['observacao'];

				array_push($alimentosTotal, $alimento);

			endforeach;
			
		endforeach;

		echo json_encode($alimentosTotal);

		die();

		$alimentosTotalSoma = array();

		$alimentosTotalSoma['vl_g_poli'] = (double) 0.0;
		print_r('inicio' . $alimentosTotalSoma['vl_g_poli']);
		//$alimentosTotalSoma['vl_g_mon'] = 0.0;
		//$alimentosTotalSoma['vl_g_sat'] = 0.0; 
		//$alimentosTotalSoma['vl_colest'] = 0.0; 
		//$alimentosTotalSoma['vl_b1'] = 0.0;
		//$alimentosTotalSoma['vl_b2'] = 0.0;
		//$alimentosTotalSoma['vl_b6'] = 0.0;
		//$alimentosTotalSoma['vl_b12'] = 0.0;
		//$alimentosTotalSoma['vl_nia'] = 0.0;
		//$alimentosTotalSoma['vl_folic'] = 0.0;
		//$alimentosTotalSoma['vl_pant'] = 0.0; 
		//$alimentosTotalSoma['vl_iodo'] = 0.0; 
		//$alimentosTotalSoma['vl_mg'] = 0.0; 
		//$alimentosTotalSoma['vl_zinco'] = 0.0; 
		//$alimentosTotalSoma['vl_mn'] = 0.0; 
		//$alimentosTotalSoma['vl_cobre'] = 0.0; 
		//$alimentosTotalSoma['vl_selen'] = 0.0;
		//$alimentosTotalSoma['vl_carb'] = 0.0; 
		//$alimentosTotalSoma['vl_g_tot'] = 0.0; 
		//$alimentosTotalSoma['vl_prot'] = 0.0; 
		//$alimentosTotalSoma['vl_fibra'] = 0.0; 
		//$alimentosTotalSoma['vl_vit_a'] = 0.0;
		//$alimentosTotalSoma['vl_vit_c'] = 0.0; 
		//$alimentosTotalSoma['vl_vit_d'] = 0.0; 
		//$alimentosTotalSoma['vl_vit_e'] = 0.0; 
		//$alimentosTotalSoma['vl_na'] = 0.0; 
		//$alimentosTotalSoma['vl_ca'] = 0.0; 
		//$alimentosTotalSoma['vl_k'] = 0.0;  
		//$alimentosTotalSoma['vl_p'] = 0.0;  
		//$alimentosTotalSoma['vl_fe'] = 0.0;

		foreach($alimentosTotal as $index): 

			$alimentosTotalSoma['vl_g_poli'] += str_replace(',', '.',$index['vl_g_poli']);
			print_r('[-->' . $alimentosTotalSoma['vl_g_poli'] . ' - ' . $index['vl_g_poli'] . ']');
            //$alimentosTotalSoma['vl_g_mon'] += $index['vl_g_mon'];
            //$alimentosTotalSoma['vl_g_sat'] += $index['vl_g_sat'];
            //$alimentosTotalSoma['vl_colest'] += $index['vl_colest'];
            //$alimentosTotalSoma['vl_b1'] += $index['vl_b1'];
            //$alimentosTotalSoma['vl_b2'] += $index['vl_b2'];
            //$alimentosTotalSoma['vl_b6'] += $index['vl_b6'];
            //$alimentosTotalSoma['vl_b12'] += $index['vl_b12'];
            //$alimentosTotalSoma['vl_nia'] += $index['vl_nia'];
            //$alimentosTotalSoma['vl_folic'] += $index['vl_folic'];
            //$alimentosTotalSoma['vl_pant'] += $index['vl_pant'];
            //$alimentosTotalSoma['vl_iodo'] += $index['vl_iodo'];
            //$alimentosTotalSoma['vl_mg'] += $index['vl_mg'];
            //$alimentosTotalSoma['vl_zinco'] += $index['vl_zinco'];
            //$alimentosTotalSoma['vl_mn'] += $index['vl_mn'];
            //$alimentosTotalSoma['vl_cobre'] += $index['vl_cobre'];
            //$alimentosTotalSoma['vl_selen'] += $index['vl_selen'];
            //$alimentosTotalSoma['vl_carb'] += $index['vl_carb'];
            //$alimentosTotalSoma['vl_g_tot'] += $index['vl_g_tot'];
            //$alimentosTotalSoma['vl_prot'] += $index['vl_prot'];
            //$alimentosTotalSoma['vl_fibra'] += $index['vl_fibra'];
            //$alimentosTotalSoma['vl_vit_a'] += $index['vl_vit_a'];
            //$alimentosTotalSoma['vl_vit_c'] += $index['vl_vit_c'];
            //$alimentosTotalSoma['vl_vit_d'] += $index['vl_vit_d'];
            //$alimentosTotalSoma['vl_vit_e'] += $index['vl_vit_e'];
            //$alimentosTotalSoma['vl_na']  += $index['vl_na'];
            //$alimentosTotalSoma['vl_ca'] += $index['vl_ca'];
            //$alimentosTotalSoma['vl_k'] += $index['vl_k'];
            //$alimentosTotalSoma['vl_p'] += $index['vl_p'];
			//$alimentosTotalSoma['vl_fe'] += $index['vl_fe'];

		endforeach;
			
		
		print_r($alimentosTotalSoma);
		die();

	}//end public function nutricaoingredientesajax



    public function gerarcardapioAction(){
        
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$this->view->pdf = $pdf;
		
		// valores relaconados a datas

		$id = $this->_request->getParam("id");

		if(($id == 4) or ($id == 7) or ($id == 9) or ($id == 12))
			$id = 10;
		
		$mesCalendario = '2018-' . $id . '-01';	
		$contadorSemanal = 2;
	
		$arrayIngredientesTotaisGeral = '';

		$dataInicio =  date('Y-m-d', strtotime($mesCalendario));
		$_Inicio = new DateTime($dataInicio);

		$dataFim = date('Y-m-d', strtotime($mesCalendario));
		$_Fim = new DateTime($dataFim);
		
		
		$_Fim->modify('last day of this month');
		$ultimoDia = $_Fim->format('d');
		$_Fim->modify('+1 day');
	
		$interval = new DateInterval('P1D');


		$period = new DatePeriod($_Inicio, $interval, $_Fim);


		$semana =  (int) $ultimoDia - (7 - (int)$_Inicio->format('w'));
		$somaMaisUm = 0;
		if((7 - (int)$_Inicio->format('w')) > 0){
			$somaMaisUm = 1;
		}else{
			$somaMaisUm = 0;
		}

		$totalSemanas = ceil($semana/7) + $somaMaisUm;
		
		$pdf->setPageOrientation('l');
		

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Folha_Ponto_Individual');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // //set auto page breaks
        // //$pdf->SetAutoPageBreak(FALSE);
        // $pdf->SetAutoPageBreak(TRUE, 4);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        //$pdf->SetFont('times', '', null , 'false');
        // set font
        $pdf->SetFont('helvetica', '', 8);
        // add a page
		$pdf->AddPage();

			//$html = $this->view->render('nutricaocardapiosrefeicao/helpers/calendario.phtml');
		
		// set cell padding
		$pdf->setCellPaddings(1, 1, 1, 1);

		// set cell margins
		$pdf->setCellMargins(1, 1, 1, 1);

		// set color for background
		$pdf->SetFillColor(242, 242, 242);

		// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

		// set some text for example
		$txt = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

		// Multicell test
		$_line = 50;
		$_col = 66;
		$_cel_width = 40;
		$_cel_height = 30;
		
		$texto2 = 'Vinagrete + Arroz + feijoadinha + couve Refogada + Farova';

		$pdf->SetXY(25,50);
		$pdf->MultiCell(40, 25, " Café da Manhã", 0, 'C', 1, 0, '', '', true);

		$pdf->SetXY(25,76);
		$pdf->MultiCell(40, 25, " Almoço", 0, 'C', 1, 0, '', '', true);

		$pdf->SetXY(25,102);
		$pdf->MultiCell(40, 25, " Lanche", 0, 'C', 1, 0, '', '', true);

		$pdf->SetXY(25,128);
		$pdf->MultiCell(40, 25, " Jantar", 0, 'C', 1, 0, '', '', true);

		/* ================ Segunda Limpa =================================== */

		if(((int)$_Inicio->format('w')) > 1){

			$dataSegunda = date('Y-m-d',strtotime($mesCalendario));
			$_Segunda = new DateTime($dataSegunda);
			$_Segunda->modify('-' . ($_Inicio->format('w') - 1) .  ' day');

			$pdf->SetXY(66,44);
			$pdf->MultiCell(40, 5, "Segunda (" . $_Segunda->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);

			$pdf->SetXY(66, 50);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(66, 76);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(66, 102);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(66, 128);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

		
		}
		if(((int)$_Inicio->format('w')) > 2){
			/* ================ Terça Limpa ===================================*/

			$dataTerca = date('Y-m-d',strtotime($mesCalendario));
			$_Terca = new DateTime($dataTerca);
			$_Terca->modify('-' . ($_Inicio->format('w') - 2) .  ' day');

			$pdf->SetXY(107,44);
			$pdf->MultiCell($_cel_width, 5, "Terça (" . $_Terca->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);


			$pdf->SetXY(107, 50);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(107, 76);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(107, 102);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(107, 128);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
		}
		
		if(((int)$_Inicio->format('w')) > 3){
			/* ================ Quarta Limpa ===================================*/

			$dataQuarta = date('Y-m-d',strtotime($mesCalendario));
			$_Quarta = new DateTime($dataQuarta);
			$_Quarta->modify('-' . ($_Inicio->format('w') - 3) .  ' day');

			$pdf->SetXY(148,44);
			$pdf->MultiCell($_cel_width, 5, "Quarta (" . $_Quarta->format('d/m') . ")" , 0, 'C', 1, 0, '', '', true);

			$pdf->SetXY(148, 50);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(148, 76);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(148, 102);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(148, 128);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
		}
		
		if(((int)$_Inicio->format('w')) > 4){

			$dataQuinta = date('Y-m-d',strtotime($mesCalendario));
			$_Quinta = new DateTime($dataQuinta);
			$_Quinta->modify('-' . ($_Inicio->format('w') - 4) .  ' day');

			
			$pdf->SetXY(189,44);
			$pdf->MultiCell($_cel_width, 5, "Quinta (" . $_Quinta->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);
		

			$pdf->SetXY(189, 50);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(189, 76);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(189, 102);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(189, 128);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
			
		}
		
		if(((int)$_Inicio->format('w')) > 5){

			$dataSexta = date('Y-m-d',strtotime($mesCalendario));
			$_Sexta = new DateTime($dataSexta);
			$_Sexta->modify('-' . ($_Inicio->format('w') - 5) .  ' day');

			$pdf->SetXY(230,44);
			$pdf->MultiCell($_cel_width, 5, "Sexta (" . $_Sexta->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);

			$pdf->SetXY(230, 50);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(230, 76);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(230, 102);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(230, 128);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
			
		}
		
		if(((int)$_Inicio->format('w')) > 6){
			
			$dataSexta = date('Y-m-d',strtotime($mesCalendario));
			$_Sabado = new DateTime($dataSexta);
			$_Sabado->modify('-' . ($_Inicio->format('w') - 5) .  ' day');

			// $pdf->SetXY(230,44);
			// $pdf->MultiCell($_cel_width, 5, "Sabado (" . $_Sexta->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);

			// $pdf->SetXY(230, 50);
			// $pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			// $pdf->SetXY(230, 76);
			// $pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			// $pdf->SetXY(230, 102);
			// $pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			// $pdf->SetXY(230, 128);
			// $pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

		}

		foreach($period as $aux):

			// Definindo a primeira linha da tabela

			$refCafe = '';
			$refAlmoco = '';
			$refLanche = '';
			$refJantar = '';

			$rowCafe = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($aux->format('Y-m-d') . ' 08:00:00', $aux->format('Y-m-d') . ' 10:00:00', 'CM');

			foreach($rowCafe as $index):

				$ingredientes = '';
				$ingredientesIds = '';

				$idsrefeicoes = explode(',', $index['idsrefeicoes']);

				foreach($idsrefeicoes as $au):

					$ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));

					foreach($ingredientes as $ind):
						$ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
					endforeach;

				endforeach;

				$arrayIngredientesTotaisGeral .= ',' . $ingredientesIds;

				$refCafe .=  $index['title'];
			endforeach;

			$rowAlmoco = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($aux->format('Y-m-d') . ' 11:00:00', $aux->format('Y-m-d') . ' 01:00:00', 'CM');

			foreach($rowAlmoco as $index):

				$ingredientes = '';
				$ingredientesIds = '';

				$idsrefeicoes = explode(',', $index['idsrefeicoes']);

				foreach($idsrefeicoes as $au):

					$ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));

					foreach($ingredientes as $ind):
						$ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
					endforeach;

				endforeach;

				$arrayIngredientesTotaisGeral .= ',' . $ingredientesIds;

				$refAlmoco .= $index['title'];
			endforeach;

			$rowLanche = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($aux->format('Y-m-d') . ' 15:00:00', $aux->format('Y-m-d') . ' 17:00:00', 'CM');

			foreach($rowLanche as $index):

				$ingredientes = '';
				$ingredientesIds = '';

				$idsrefeicoes = explode(',', $index['idsrefeicoes']);

				foreach($idsrefeicoes as $au):

					$ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));

					foreach($ingredientes as $ind):
						$ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
					endforeach;

				endforeach;

				$arrayIngredientesTotaisGeral .= ',' . $ingredientesIds;

				$refLanche .= $index['title'];
			endforeach;

			$rowJantar = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($aux->format('Y-m-d') . ' 19:00:00', $aux->format('Y-m-d') . ' 20:00:00', 'CM');

			foreach($rowJantar as $index):

				$ingredientes = '';
				$ingredientesIds = '';

				$idsrefeicoes = explode(',', $index['idsrefeicoes']);

				foreach($idsrefeicoes as $au):

					$ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));

					foreach($ingredientes as $ind):
						$ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
					endforeach;

				endforeach;

				$arrayIngredientesTotaisGeral .= ',' . $ingredientesIds;

				$refJantar .= "[" . $index['title'] . "] \n ";
			endforeach;

			
			
			switch($aux->format('w')){

				case '1':
				
				$pdf->SetXY(66,44);
				$pdf->MultiCell($_cel_width, 5, "Segunda (" . $aux->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);


				$pdf->SetXY(66, 50);
				$pdf->MultiCell(40, 25, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refCafe != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(66, 76);
				$pdf->MultiCell(40, 25, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refAlmoco != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(66, 102);
				$pdf->MultiCell(40, 25, $refLanche,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refLanche != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(66, 128);
				$pdf->MultiCell(40, 25, $refJantar,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refJantar != ''){
					$contadorSemanal++;
				}

				break;

				case '2':

				$pdf->SetXY(107,44);
				$pdf->MultiCell($_cel_width, 5, "Terça (" . $aux->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);


				$pdf->SetXY(107, 50);
				$pdf->MultiCell(40, 25, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refCafe != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(107, 76);
				$pdf->MultiCell(40, 25, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refAlmoco != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(107, 102);
				$pdf->MultiCell(40, 25, $refLanche,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refLanche != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(107, 128);
				$pdf->MultiCell(40, 25, $refJantar,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refJantar != ''){
					$contadorSemanal++;
				}

				break;

				case '3':

				$pdf->SetXY(148,44);
				$pdf->MultiCell($_cel_width, 5, "Quarta (" . $aux->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);

				$pdf->SetXY(148, 50);
				$pdf->MultiCell(40, 25, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refCafe != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(148, 76);
				$pdf->MultiCell(40, 25, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refAlmoco != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(148, 102);
				$pdf->MultiCell(40, 25, $refLanche,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refLanche != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(148, 128);
				$pdf->MultiCell(40, 25, $refJantar,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refJantar != ''){
					$contadorSemanal++;
				}

				break;

				case '4':

				$pdf->SetXY(189,44);
				$pdf->MultiCell($_cel_width, 5, "Quinta (" . $aux->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);

				$pdf->SetXY(189, 50);
				$pdf->MultiCell(40, 25, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refCafe != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(189, 76);
				$pdf->MultiCell(40, 25, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refAlmoco != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(189, 102);
				$pdf->MultiCell(40, 25, $refLanche,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refLanche != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(189, 128);
				$pdf->MultiCell(40, 25, $refJantar,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refJantar != ''){
					$contadorSemanal++;
				}

				break;

				case '5':

				$pdf->SetXY(230,44);
				$pdf->MultiCell($_cel_width, 5, "Sexta (" . $aux->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);

				$pdf->SetXY(230, 50);
				$pdf->MultiCell(40, 25, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refCafe != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(230, 76);
				$pdf->MultiCell(40, 25, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refAlmoco != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(230, 102);
				$pdf->MultiCell(40, 25, $refLanche,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refLanche != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(230, 128);
				$pdf->MultiCell(40, 25, $refJantar,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refJantar != ''){
					$contadorSemanal++;
				}

				$contSemana = $contadorSemanal;
				
				$contadorSemanal = 1;

				break;
				

			}//end switch


			/* =====================================================================================
								  Nutricionistas
		   ===================================================================================== */


		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetXY(80,170);
		$pdf->SetFont('helvetica', '', 9);
		$pdf->MultiCell($_cel_width, 5, "Priscila F. Donomenech ", 0, 'C', 1, 0, '', '', true);

		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetXY(80,174);
		$pdf->MultiCell($_cel_width, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetXY(80,178);
		$pdf->MultiCell($_cel_width, 5, "CRN : 3 23.674 ", 0, 'C', 1, 0, '', '', true);

		$pdf->SetFillColor(242, 242, 242);
		$pdf->SetFont('helvetica', '', 7);


		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetXY(180,170);
		$pdf->SetFont('helvetica', '', 9);
		$pdf->MultiCell($_cel_width, 5, "Sheila C. Souza ", 0, 'C', 1, 0, '', '', true);

		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetXY(180,174);
		$pdf->MultiCell($_cel_width, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetXY(180,178);
		$pdf->MultiCell($_cel_width, 5, "CRN : 3 24.577 ", 0, 'C', 1, 0, '', '', true);

		$pdf->SetFillColor(242, 242, 242);
		$pdf->SetFont('helvetica', '', 7);



		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetXY(130,170);
		$pdf->SetFont('helvetica', '', 9);
		$pdf->MultiCell($_cel_width, 5, "Cristina R. T. Ragazzi ", 0, 'C', 1, 0, '', '', true);

		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetXY(130,174);
		$pdf->MultiCell($_cel_width, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetXY(130,178);
		$pdf->MultiCell($_cel_width, 5, "CRN : 3 10.999 ", 0, 'C', 1, 0, '', '', true);

		$pdf->SetFillColor(242, 242, 242);
		$pdf->SetFont('helvetica', '', 7);


		/* =====================================================================================
								  End Nutricionistas
		   ===================================================================================== */

			if($aux->format('w') == 0){

				/* Ultima linha Titulos */
				$pdf->SetFont('helvetica', 'B', 8);
				$pdf->SetTextColor(255,255,255);
				$pdf->SetXY(25 + ((245/11) * 0), 154);
				$pdf->MultiCell((245/11), 5, 'Kcal',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 1), 154);
				$pdf->MultiCell((245/11), 5, 'CHO (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 2), 154);
				$pdf->MultiCell((245/11), 5, 'LPD (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 3), 154);
				$pdf->MultiCell((245/11), 5, 'PTN (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 4), 154);
				$pdf->MultiCell((245/11), 5, 'Fibra (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 5), 154);
				$pdf->MultiCell((245/11), 5, 'Vit A (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 6), 154);
				$pdf->MultiCell((245/11), 5, 'Vit c (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 7), 154);
				$pdf->MultiCell((245/11), 5, 'Fe (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 8), 154);
				$pdf->MultiCell((245/11), 5, 'Mg (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 9), 154);
				$pdf->MultiCell((245/11), 5, 'Ca (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 10), 154);
				$pdf->MultiCell((245/11), 5, 'Zn (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');


				$soma = $this->valor($arrayIngredientesTotaisGeral);

				/* Ultima linha valores */
				$pdf->SetFont('helvetica', 'B', 8);
				$pdf->SetTextColor(153, 153, 153);

				$total_kcal =  number_format((($soma['val_kcal']) / ($contSemana)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 0), 158);
				//$pdf->MultiCell((245/11), 4, $total_kcal,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_cho =  number_format((($soma['val_cho']) / ($contSemana)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 1), 158);
				//$pdf->MultiCell((245/11), 4, $total_cho,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_lpd = number_format(($soma['val_lpd']) / ($contSemana), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 2), 158);
				//$pdf->MultiCell((245/11), 4, $total_lpd,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_ptn =  number_format((($soma['val_ptn']) / ($contSemana)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 3), 158);
				//$pdf->MultiCell((245/11), 4, $total_ptn,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_fibra =  number_format((($soma['val_fibra']) / ($contSemana)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 4), 158);
				//$pdf->MultiCell((245/11), 4, $soma['val_fibra'],0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_vit_a =  number_format((($soma['val_vit_a']) / ($contSemana)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 5), 158);
				//$pdf->MultiCell((245/11), 4, $total_vit_a,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_vit_c =  number_format((($soma['val_vit_c']) / ($contSemana)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 6), 158);
				//$pdf->MultiCell((245/11), 4, $total_vit_c,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_fe =  number_format((($soma['val_fe']) / ($contSemana)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 7), 158);
				//$pdf->MultiCell((245/11), 4, $total_fe,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_mg =  number_format((($soma['val_mg']) / ($contSemana)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 8), 158);
				//$pdf->MultiCell((245/11), 4, $total_mg,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_ca = number_format((($soma['val_ca']) / ($contSemana)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 9), 158);
				//$pdf->MultiCell((245/11), 4, $total_ca,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_zn = number_format((($soma['val_zn']) / ($contSemana)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 10), 158);
				//$pdf->MultiCell((245/11), 4, $total_zn,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');


				$pdf->SetXY(25  + ((245/11) * 0), 164);
				//$pdf->MultiCell(60, 4, '* Valor total dividido entre ' . ($contSemana) . ' Refeições',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');



				$pdf->SetFont('helvetica', '', 8);
				$pdf->SetTextColor(0,0,0);
				$pdf->AddPage();
				$pdf->SetXY(25,50);
				$pdf->MultiCell(40, 25, " Café da Manhã", 0, 'C', 1, 0, '', '', true);

				$pdf->SetXY(25,76);
				$pdf->MultiCell(40, 25, " Almoço", 0, 'C', 1, 0, '', '', true);

				$pdf->SetXY(25,102);
				$pdf->MultiCell(40, 25, " Lanche", 0, 'C', 1, 0, '', '', true);

				$pdf->SetXY(25,128);
				$pdf->MultiCell(40, 25, " Jantar", 0, 'C', 1, 0, '', '', true);

			}//

			
			
		endforeach;
		
		
		/* ================ Segunda Limpa ===================================*/

		if(((int)$_Fim->format('w')) < 2){

			$pdf->SetXY(66,44);
			$pdf->MultiCell($_cel_width, 5, "Segunda x", 0, 'C', 1, 0, '', '', true);


			$pdf->SetXY(66, 50);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(66, 76);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(66, 102);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(66, 128);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');




		}
		if(((int)$_Fim->format('w')) < 3){
			/* ================ Terça Limpa ===================================*/

			$pdf->SetXY(107,44);
			$pdf->MultiCell($_cel_width, 5, "Terça", 0, 'C', 1, 0, '', '', true);


			$pdf->SetXY(107, 50);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(107, 76);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(107, 102);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(107, 128);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			
		}

		if(((int)$_Fim->format('w')) < 4){
			/* ================ Quarta Limpa ===================================*/

			$pdf->SetXY(148,44);
			$pdf->MultiCell($_cel_width, 5, "Quarta ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetXY(148, 50);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(148, 76);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(148, 102);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(148, 128);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			
		}

		if(((int)$_Fim->format('w')) < 5){

			
			$pdf->SetXY(189,44);
			$pdf->MultiCell($_cel_width, 5, "Quinta ", 0, 'C', 1, 0, '', '', true);


			$pdf->SetXY(189, 50);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(189, 76);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(189, 102);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(189, 128);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			
			
		}

		if(((int)$_Fim->format('w')) < 6){

			$pdf->SetXY(230,44);
			$pdf->MultiCell($_cel_width, 5, "Sexta", 0, 'C', 1, 0, '', '', true);

			$pdf->SetXY(230, 50);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(230, 76);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(230, 102);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(230, 128);
			$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
			
		}

		if(((int)$_Fim->format('w')) < 7){
			
		}


		/* Ultima linha Titulos */
		$pdf->SetFont('helvetica', 'B', 8);
		$pdf->SetTextColor(255,255,255);
		$pdf->SetXY(25 + ((245/11) * 0), 154);
		$pdf->MultiCell((245/11), 5, 'Kcal',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 1), 154);
		$pdf->MultiCell((245/11), 5, 'CHO (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 2), 154);
		$pdf->MultiCell((245/11), 5, 'LPD (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 3), 154);
		$pdf->MultiCell((245/11), 5, 'PTN (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 4), 154);
		$pdf->MultiCell((245/11), 5, 'Fibra (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 5), 154);
		$pdf->MultiCell((245/11), 5, 'Vit A (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 6), 154);
		$pdf->MultiCell((245/11), 5, 'Vit c (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 7), 154);
		$pdf->MultiCell((245/11), 5, 'Fe (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 8), 154);
		$pdf->MultiCell((245/11), 5, 'Mg (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 9), 154);
		$pdf->MultiCell((245/11), 5, 'Ca (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 10), 154);
		$pdf->MultiCell((245/11), 5, 'Zn (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');


		/* Ultima linha valores */
		$pdf->SetFont('helvetica', 'B', 8);
		$pdf->SetTextColor(153, 153, 153);
		$pdf->SetXY(25  + ((245/11) * 0), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 1), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 2), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 3), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 4), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 5), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 6), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 7), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 8), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 9), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 10), 158);
		//$pdf->MultiCell((245/11), 4, '866,7',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');
				


        $filename = 'folhadeponto.pdf'; 
        $pdf->Output($filename, 'I');

		die();
		
	}//end gerardocumentofolhadeponto
	
public function gerarcardapioeeexperimentAction(){

	$id = $this->_request->getParam("id");


	$mesCalendario = '2018-' . $id . '-01';

	$dataInicio =  date('Y-m-d',strtotime($mesCalendario));
	$_Inicio = new DateTime($dataInicio);

	$dataFim = date('Y-m-d', strtotime($mesCalendario));
	$_Fim = new DateTime($dataFim);
	
	$_Fim->modify('last day of this month');

	$ultimoDia = $_Fim->format('d');

	$_Fim->modify('+1 day');

	$interval = new DateInterval('P1D');

	$period = new DatePeriod($_Inicio, $interval, $_Fim);

	/* --------------------------------------------------------------
					Cálculo do total das semanas
	   -------------------------------------------------------------- */

	$semana =  (int) $ultimoDia - (7 - (int)$_Inicio->format('w'));
	$somaMaisUm = 0;
	if((7 - (int)$_Inicio->format('w')) > 0){
		$somaMaisUm = 1;
	}else{
		$somaMaisUm = 0;
	}

	$totalSemanas = ceil($semana/7) + $somaMaisUm;


	/* --------------------------------------------------------------
					Início do PDF
	   -------------------------------------------------------------- */

	$pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	$pdf->setPageOrientation('l');
	// set cell padding
	$pdf->setCellPaddings(1, 1, 1, 1);

	// set cell margins
	$pdf->setCellMargins(1, 1, 1, 1);

	// set color for background
	$pdf->SetFillColor(242, 242, 242);
	$pdf->SetTextColor(0, 0, 0);

	$pdf->SetFont('helvetica', '', 8);
	$pdf->SetTextColor(0,0,0);

	$_box_w = 40;
	$_box_h = 30;
	$_x = 25;
	$_y = 50;

	/* ------------------------------------------------------
					Separando Renges de Meses
	   ------------------------------------------------------ */

	$totalSemanasRange = array();
	$arraySemanasMontadas = array();

	for($int = 0; $int < $totalSemanas; $int++){

		$semanaRange = array();
	
		if($int == 0){
		
			$_aux = date('Y-m-d',strtotime($mesCalendario));
			$_ini = new DateTime($_aux);

			$_ini->modify('-' . $_ini->format('w') . ' day');
			$semanaRange['inicio'] = $_ini->format('Y-m-d');

			$_ini->modify('+6 day');
			$semanaRange['fim'] = $_ini->format('Y-m-d');
			
			$arraySemanasMontadas[$int] = $semanaRange;

		}else{

			$_aux = date('Y-m-d',strtotime($arraySemanasMontadas[$int-1]['fim']));
			$_ini = new DateTime($_aux);

			$_ini->modify('+1 day');
			$semanaRange['inicio'] = $_ini->format('Y-m-d');

			$_ini->modify('+6 day');
			$semanaRange['fim'] = $_ini->format('Y-m-d');

			$arraySemanasMontadas[$int] = $semanaRange;
		}	

	}

	/* ------------------------------------------------------
					End Separando Renges de Meses
	   ------------------------------------------------------ */



	// ==================================================

	foreach($arraySemanasMontadas as $semanas){

		/* =======================================================
							Conteúdo das Semanas
		   ======================================================= */

		   $pdf->AddPage();

		   /*  ---------------- Titulo -------------------------------*/

		   $pdf->SetFillColor(255, 255, 255);
		   $pdf->SetFont('helvetica', 'B', 12);
		   $pdf->SetXY(25,35);
		   $pdf->MultiCell(245, 8, " Cardápio EMEI / EMEF " . MN_Util::getMes($id) . " de " . date('Y'), 0, 'C', 1, 0, '', '', true);

		   /*  ---------------- Primeira coluna -------------------------------*/

		   $pdf->SetFillColor(242, 242, 242);
		   $pdf->SetFont('helvetica', '', 8);
		   $pdf->SetXY(25,50);
		   $pdf->MultiCell($_box_w, $_box_h, " Café da Manhã", 0, 'C', 1, 0, '', '', true);
	   
		   $pdf->SetXY(25,81);
		   $pdf->MultiCell($_box_w, $_box_h, " Almoço", 0, 'C', 1, 0, '', '', true);

		   /*  ---------------- End Primeira coluna ---------------------------*/

		   /* Criando período de datas para a semana */

		   $periodo_Inicio =  date('Y-m-d',strtotime($semanas['inicio']));
		   $_p_Inicio = new DateTime($periodo_Inicio);

		   $periodo_Fim =  date('Y-m-d',strtotime($semanas['fim']));
		   $_p_Fim = new DateTime($periodo_Fim);

		   $interval = new DateInterval('P1D');

		   $periodo = new DatePeriod($_p_Inicio, $interval, $_p_Fim);

		   $arrayIngredientesTotaisGeral = '';
		   $totalArrayIngreds = array();
		   $contadorRefeicoesSemana = 0;

		   foreach($periodo as $dataSemana){

				/* ================================================================================
									Retornando o conteúdo das datas Inseridas
				   ================================================================================ */

				    $refCafe = '';    // Irá conter a array com os detalhes da café do dia
					$refAlmoco = '';  // Irá conter a array com os detalhes do Almoço do dia

					$rowCafe = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($dataSemana->format('Y-m-d') . ' 08:00:00', $dataSemana->format('Y-m-d') . ' 10:00:00', 'EE');

					foreach($rowCafe as $index):

						$rowRefeicaoId = Nutricaocardapiosrefeicao::getNutricaocardapiorefeicaoByIdHelper($index['idnutricaocardapiosrefeicao']);  
						$rowRefeicaoRepet = Nutricaorefeicoes::getNutricaorefeicaoByIdHelper($rowRefeicaoId['idsrefeicoes']);

						$ingredientes = '';
						$ingredientesIds = '';

						$idsrefeicoes = explode(',', $index['idsrefeicoes']);

						foreach($idsrefeicoes as $au):

							$ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));

							foreach($ingredientes as $ind):

								$ingAux = array();

								$ingAux['idnutricaotacoalimento'] = $ind['idnutricaotacoalimento'];
								$ingAux['descricaotacoalimento'] = $ind['descricaotacoalimento'];
								$ingAux['quantidade'] = $ind['quantidade'];
								$ingAux['unidade'] = $ind['unidade'];
								$ingAux['observacao'] = $ind['observacao'];
								$ingAux['repeticoes'] = $rowRefeicaoRepet['porcoes'];
								
								array_push($totalArrayIngreds, $ingAux);


								$ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
							endforeach;

						endforeach;

						$arrayIngredientesTotaisGeral .= $ingredientesIds;

						$refCafe .=  "[" . $index['title'] .  "]";
					endforeach;

					$rowAlmoco = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($dataSemana->format('Y-m-d') . ' 11:30:00', $dataSemana->format('Y-m-d') . ' 13:00:00', 'EE');

					foreach($rowAlmoco as $index):

						$rowRefeicaoId = Nutricaocardapiosrefeicao::getNutricaocardapiorefeicaoByIdHelper($index['idnutricaocardapiosrefeicao']);  
						$rowRefeicaoRepet = Nutricaorefeicoes::getNutricaorefeicaoByIdHelper($rowRefeicaoId['idsrefeicoes']);

						$ingredientes = '';
						$ingredientesIds = '';

						$idsrefeicoes = explode(',', $index['idsrefeicoes']);

						foreach($idsrefeicoes as $au):
							$ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));

							foreach($ingredientes as $ind):

								$ingAux = array();

								$ingAux['idnutricaotacoalimento'] = $ind['idnutricaotacoalimento'];
								$ingAux['descricaotacoalimento'] = $ind['descricaotacoalimento'];
								$ingAux['quantidade'] = $ind['quantidade'];
								$ingAux['unidade'] = $ind['unidade'];
								$ingAux['observacao'] = $ind['observacao'];
								$ingAux['repeticoes'] = $rowRefeicaoRepet['porcoes'];

								
								array_push($totalArrayIngreds, $ingAux);

								$ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
							endforeach;

						endforeach;

						$arrayIngredientesTotaisGeral .= $ingredientesIds;

						$refAlmoco .= "[" . $index['title'] . "]";
					endforeach;

					/* ================================================================================
									    End Retornando o conteúdo das datas Inseridas
				       ================================================================================ */


				switch($dataSemana->format('w')){

					case '1':
					
						// Verifica se há alguma refeição atribuida para a semana
						if(($refCafe != '') || ($refAlmoco != '')){
							$contadorRefeicoesSemana++; 
						}//end if de verificação

						$pdf->SetXY(66,44);
						$pdf->MultiCell(40, 5, "Segunda " . $dataSemana->format('d/m'), 0, 'C', 1, 0, '', '', true);
						
						$pdf->SetXY(66, 50);
						$pdf->MultiCell($_box_w, $_box_h, $refCafe  ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
						
						$pdf->SetXY(66, 81);
						$pdf->MultiCell($_box_w, $_box_h, $refAlmoco ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						
					break;

					case '2':

						// Verifica se há alguma refeição atribuida para a semana
						if(($refCafe != '') || ($refAlmoco != '')){
							$contadorRefeicoesSemana++; 
						}//end if de verificação

						$pdf->SetXY(107,44);
						$pdf->MultiCell(40, 5, "Terça "  . $dataSemana->format('d/m'), 0, 'C', 1, 0, '', '', true);
					
						$pdf->SetXY(107, 50);
						$pdf->MultiCell($_box_w, $_box_h, $refCafe ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
					
						$pdf->SetXY(107, 81);
						$pdf->MultiCell($_box_w, $_box_h, $refAlmoco ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						
					break;

					case '3':

						// Verifica se há alguma refeição atribuida para a semana
						if(($refCafe != '') || ($refAlmoco != '')){
							$contadorRefeicoesSemana++; 
						}//end if de verificação

						$pdf->SetXY(148,44);
						$pdf->MultiCell(40,5, "Quarta " . $dataSemana->format('d/m'), 0, 'C', 1, 0, '', '', true);
					
						$pdf->SetXY(148, 50);
						$pdf->MultiCell($_box_w, $_box_h, $refCafe ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
					
						$pdf->SetXY(148, 81);
						$pdf->MultiCell($_box_w, $_box_h, $refAlmoco ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						
					break;

					case '4':

						// Verifica se há alguma refeição atribuida para a semana
						if(($refCafe != '') || ($refAlmoco != '')){
							$contadorRefeicoesSemana++; 
						}//end if de verificação

						$pdf->SetXY(189,44);
						$pdf->MultiCell(40, 5, "Quinta " . $dataSemana->format('d/m'), 0, 'C', 1, 0, '', '', true);
					
						$pdf->SetXY(189, 50);
						$pdf->MultiCell($_box_w, $_box_h, $refCafe ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
					
						$pdf->SetXY(189, 81);
						$pdf->MultiCell($_box_w, $_box_h, $refAlmoco ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						
					break;

					case '5':

						// Verifica se há alguma refeição atribuida para a semana
						if(($refCafe != '') || ($refAlmoco != '')){
							$contadorRefeicoesSemana++; 
						}//end if de verificação

						$pdf->SetXY(230,44);
						$pdf->MultiCell(40, 5, "Sexta " . $dataSemana->format('d/m'), 0, 'C', 1, 0, '', '', true);
						
						$pdf->SetXY(230, 50);
						$pdf->MultiCell($_box_w, $_box_h, $refCafe ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(230, 81);
		   				$pdf->MultiCell($_box_w, $_box_h, $refAlmoco ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						
					break;

				}//end switch Case

		   }//end foreach for days in week
	   


		   /* =======================================================
							End Conteúdo das Semanas
		   	  ======================================================= */

		   /* =======================================================
		   					Linha de Somatório
			  ======================================================= */

			  	$pdf->SetFont('helvetica', 'B', 8);
				
				$pdf->SetXY(25 + ((245/11) * 0), 124);
				$pdf->MultiCell((245/11), 5, 'Kcal',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 1), 124);
				$pdf->MultiCell((245/11), 5, 'CHO (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 2), 124);
				$pdf->MultiCell((245/11), 5, 'LPD (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 3), 124);
				$pdf->MultiCell((245/11), 5, 'PTN (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 4), 124);
				$pdf->MultiCell((245/11), 5, 'Fibra (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 5), 124);
				$pdf->MultiCell((245/11), 5, 'Vit A (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 6), 124);
				$pdf->MultiCell((245/11), 5, 'Vit c (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 7), 124);
				$pdf->MultiCell((245/11), 5, 'Fe (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 8), 124);
				$pdf->MultiCell((245/11), 5, 'Mg (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 9), 124);
				$pdf->MultiCell((245/11), 5, 'Ca (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 10), 124);
				$pdf->MultiCell((245/11), 5, 'Zn (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				/* ------------------------------------------------------------------------------------------ */

				$soma = $this->valorarray($totalArrayIngreds);

				$mostradorDeQTDERefeicoesSemana = $contadorRefeicoesSemana;

				if($contadorRefeicoesSemana == 0)
							$contadorRefeicoesSemana++; 

				$pdf->SetFont('helvetica', '', 8);
				$soma = $this->valor($arrayIngredientesTotaisGeral);

				$pdf->SetXY(25  + ((245/11) * 0), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_kcal']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 1), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_cho']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 2), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_lpd']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 3), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_ptn']/$contadorRefeicoesSemana), 2, ',', ' '),0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 4), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_fibra']/$contadorRefeicoesSemana), 2, ',', ' '),0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 5), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_vit_a']/$contadorRefeicoesSemana), 2, ',', ' '),0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 6), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_vit_c']/$contadorRefeicoesSemana), 2, ',', ' '),0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 7), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_fe']/$contadorRefeicoesSemana), 2, ',', ' '),0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 8), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_mg']/$contadorRefeicoesSemana), 2, ',', ' '),0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 9), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_ca']/$contadorRefeicoesSemana), 2, ',', ' '),0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 10), 128);
				$pdf->MultiCell((245/11), 4, number_format(($soma['val_zn']/$contadorRefeicoesSemana), 2, ',', ' ')
				,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 0), 134);

				if($mostradorDeQTDERefeicoesSemana == 0){
					$pdf->MultiCell(50, 4, '* Semana sem refeições',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');
				}else if($mostradorDeQTDERefeicoesSemana == 1){
					$pdf->MultiCell(50, 4, '* dividido entre ' . $mostradorDeQTDERefeicoesSemana . ' Dia',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');
				}else{
					$pdf->MultiCell(50, 4, '* dividido entre ' . $mostradorDeQTDERefeicoesSemana . ' Dias',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');
				}

		   /* =======================================================
		   					Elementos Finais da página
			  ======================================================= */
			  

			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetXY(80,160);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->MultiCell(50, 5, "Priscila F. Donomenech ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(80,164);
			$pdf->MultiCell(50, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(80,168);
			$pdf->MultiCell(50, 5, "CRN : 3 23.674 ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFillColor(242, 242, 242);
			$pdf->SetFont('helvetica', '', 7);


			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetXY(180,160);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->MultiCell(50, 5, "Sheila C. Souza ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(180,164);
			$pdf->MultiCell(50, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(180,168);
			$pdf->MultiCell(50, 5, "CRN : 3 24.577 ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFillColor(242, 242, 242);
			$pdf->SetFont('helvetica', '', 7);



			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetXY(130,160);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->MultiCell(50, 5, "Cristina R. T. Ragazzi ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(130,164);
			$pdf->MultiCell(50, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(130,168);
			$pdf->MultiCell(50, 5, "CRN : 3 10.999 ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFillColor(242, 242, 242);
			$pdf->SetFont('helvetica', '', 7);

	}//end for

	

	

	$filename = 'folhadeponto.pdf'; 
	$pdf->Output($filename, 'I');

	die();
		

	

}// end gerarcardapioee


public function gerarcardapioexperimentAction(){

	$id = $this->_request->getParam("id");

	$ano = '2018';

	$mesCalendario = $ano  . '-' . $id . '-01';

	$dataInicio =  date('Y-m-d',strtotime($mesCalendario));
	$_Inicio = new DateTime($dataInicio);

	$dataFim = date('Y-m-d', strtotime($mesCalendario));
	$_Fim = new DateTime($dataFim);
	
	$_Fim->modify('last day of this month');

	$ultimoDia = $_Fim->format('d');

	$_Fim->modify('+1 day');

	$interval = new DateInterval('P1D');

	$period = new DatePeriod($_Inicio, $interval, $_Fim);

	/* --------------------------------------------------------------
					Cálculo do total das semanas
	   -------------------------------------------------------------- */

	$semana =  (int) $ultimoDia - (7 - (int)$_Inicio->format('w'));
	$somaMaisUm = 0;
	if((7 - (int)$_Inicio->format('w')) > 0){
		$somaMaisUm = 1;
	}else{
		$somaMaisUm = 0;
	}

	$totalSemanas = ceil($semana/7) + $somaMaisUm;


	/* --------------------------------------------------------------
					Início do PDF
	   -------------------------------------------------------------- */

	$pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	$pdf->setPageOrientation('l');
	// set cell padding
	$pdf->setCellPaddings(1, 1, 1, 1);

	// set cell margins
	$pdf->setCellMargins(1, 1, 1, 1);

	// set color for background
	$pdf->SetFillColor(242, 242, 242);
	$pdf->SetTextColor(0, 0, 0);

	$pdf->SetFont('helvetica', '', 8);
	$pdf->SetTextColor(0,0,0);

	$_box_w = 40;
	$_box_h = 30;
	$_x = 25;
	$_y = 50;

	/* ------------------------------------------------------
					Separando Renges de Meses
	   ------------------------------------------------------ */

	$totalSemanasRange = array();
	$arraySemanasMontadas = array();

	for($int = 0; $int < $totalSemanas; $int++){

		$semanaRange = array();
	
		if($int == 0){
		
			$_aux = date('Y-m-d',strtotime($mesCalendario));
			$_ini = new DateTime($_aux);

			$_ini->modify('-' . $_ini->format('w') . ' day');
			$semanaRange['inicio'] = $_ini->format('Y-m-d');

			$_ini->modify('+6 day');
			$semanaRange['fim'] = $_ini->format('Y-m-d');
			
			$arraySemanasMontadas[$int] = $semanaRange;

		}else{

			$_aux = date('Y-m-d',strtotime($arraySemanasMontadas[$int-1]['fim']));
			$_ini = new DateTime($_aux);

			$_ini->modify('+1 day');
			$semanaRange['inicio'] = $_ini->format('Y-m-d');

			$_ini->modify('+6 day');
			$semanaRange['fim'] = $_ini->format('Y-m-d');

			$arraySemanasMontadas[$int] = $semanaRange;
		}	

	}

	/* ------------------------------------------------------
					End Separando Renges de Meses
	   ------------------------------------------------------ */



	// ==================================================

	foreach($arraySemanasMontadas as $semanas){

			
		/* =======================================================
							Conteúdo das Semanas
		   ======================================================= */

		   $pdf->AddPage();

		   /*  ---------------- Titulo -------------------------------*/

		   $pdf->SetFillColor(255, 255, 255);
		   $pdf->SetFont('helvetica', 'B', 12);
		   $pdf->SetXY(25,35);
		   $pdf->MultiCell(245, 8, " Cardápio Creche e Maternal " . MN_Util::getMes($id) . " de " . date('Y'), 0, 'C', 1, 0, '', '', true);

		   /*  ---------------- Primeira coluna -------------------------------*/

		   $pdf->SetFillColor(242, 242, 242);
		   $pdf->SetFont('helvetica', '', 8);

		   $pdf->SetXY(25,50);
		   $pdf->MultiCell(40, 25, " Café da Manhã", 0, 'C', 1, 0, '', '', true);

		   $pdf->SetXY(25,76);
		   $pdf->MultiCell(40, 25, " Almoço", 0, 'C', 1, 0, '', '', true);

		   $pdf->SetXY(25,102);
		   $pdf->MultiCell(40, 25, " Lanche", 0, 'C', 1, 0, '', '', true);

		   $pdf->SetXY(25,128);
		   $pdf->MultiCell(40, 25, " Jantar", 0, 'C', 1, 0, '', '', true);

		   /*  ---------------- End Primeira coluna ---------------------------*/

		   /* Criando período de datas para a semana */

		   $periodo_Inicio =  date('Y-m-d',strtotime($semanas['inicio']));
		   $_p_Inicio = new DateTime($periodo_Inicio);

		   $periodo_Fim =  date('Y-m-d',strtotime($semanas['fim']));
		   $_p_Fim = new DateTime($periodo_Fim);

		   $interval = new DateInterval('P1D');

		   $periodo = new DatePeriod($_p_Inicio, $interval, $_p_Fim);

		   $arrayIngredientesTotaisGeral = '';
		   $totalArrayIngreds = array();
		   $contadorRefeicoesSemana = 0;
		   
		   foreach($periodo as $dataSemana){
				
				/* ================================================================================
									Retornando o conteúdo das datas Inseridas
				   ================================================================================ */

				   $refCafe = '';
				   $refAlmoco = '';
				   $refLanche = '';
				   $refJantar = '';
	   
				   $rowCafe = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($dataSemana->format('Y-m-d') . ' 08:00:00', $dataSemana->format('Y-m-d') . ' 10:00:00', 'CM');
				   
				   foreach($rowCafe as $index):

					

						$rowRefeicaoId = Nutricaocardapiosrefeicao::getNutricaocardapiorefeicaoByIdHelper($index['idnutricaocardapiosrefeicao']);  
						$rowRefeicaoRepet = Nutricaorefeicoes::getNutricaorefeicaoByIdHelper($rowRefeicaoId['idsrefeicoes']);
						
					   $ingredientes = '';
					   $ingredientesIds = '';
	   
					   $idsrefeicoes = explode(',', $index['idsrefeicoes']);
	   
					   foreach($idsrefeicoes as $au):
	   
						   $ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));
	   
						   foreach($ingredientes as $ind):

							$ingAux = array();

							$ingAux['idnutricaotacoalimento'] = $ind['idnutricaotacoalimento'];
							$ingAux['descricaotacoalimento'] = $ind['descricaotacoalimento'];
							$ingAux['quantidade'] = $ind['quantidade'];
							$ingAux['unidade'] = $ind['unidade'];
							$ingAux['observacao'] = $ind['observacao'];
							$ingAux['repeticoes'] = $rowRefeicaoRepet['porcoes'];
							
							array_push($totalArrayIngreds, $ingAux);

							   $ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
						   endforeach;
	   
					   endforeach;
	   
					   $arrayIngredientesTotaisGeral .= ',' . $ingredientesIds;
	   
					   $refCafe .=  $index['title'];
					
				   endforeach;
	   
				   $rowAlmoco = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($dataSemana->format('Y-m-d') . ' 11:00:00', $dataSemana->format('Y-m-d') . ' 01:00:00', 'CM');
				   
				   foreach($rowAlmoco as $index):

					$rowRefeicaoId = Nutricaocardapiosrefeicao::getNutricaocardapiorefeicaoByIdHelper($index['idnutricaocardapiosrefeicao']);  
					$rowRefeicaoRepet = Nutricaorefeicoes::getNutricaorefeicaoByIdHelper($rowRefeicaoId['idsrefeicoes']);
	   
					   $ingredientes = '';
					   $ingredientesIds = '';
	   
					   $idsrefeicoes = explode(',', $index['idsrefeicoes']);
	   
					   foreach($idsrefeicoes as $au):
	   
						   $ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));
	   
						   foreach($ingredientes as $ind):

							$ingAux = array();

							$ingAux['idnutricaotacoalimento'] = $ind['idnutricaotacoalimento'];
							$ingAux['descricaotacoalimento'] = $ind['descricaotacoalimento'];
							$ingAux['quantidade'] = $ind['quantidade'];
							$ingAux['unidade'] = $ind['unidade'];
							$ingAux['observacao'] = $ind['observacao'];
							$ingAux['repeticoes'] = $rowRefeicaoRepet['porcoes'];

							
							array_push($totalArrayIngreds, $ingAux);

							   $ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
						   endforeach;
	   
					   endforeach;
	   
					   $arrayIngredientesTotaisGeral .= ',' . $ingredientesIds;
	   
					   $refAlmoco .= $index['title'];
				   endforeach;
	   
				   $rowLanche = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($dataSemana->format('Y-m-d') . ' 15:00:00', $dataSemana->format('Y-m-d') . ' 17:00:00', 'CM');
				   
				   
				   foreach($rowLanche as $index):

					$rowRefeicaoId = Nutricaocardapiosrefeicao::getNutricaocardapiorefeicaoByIdHelper($index['idnutricaocardapiosrefeicao']);  
					$rowRefeicaoRepet = Nutricaorefeicoes::getNutricaorefeicaoByIdHelper($rowRefeicaoId['idsrefeicoes']);
	   
					   $ingredientes = '';
					   $ingredientesIds = '';
	   
					   $idsrefeicoes = explode(',', $index['idsrefeicoes']);
	   
					   foreach($idsrefeicoes as $au):
	   
						   $ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));
	   
						   foreach($ingredientes as $ind):

							$ingAux = array();

							$ingAux['idnutricaotacoalimento'] = $ind['idnutricaotacoalimento'];
							$ingAux['descricaotacoalimento'] = $ind['descricaotacoalimento'];
							$ingAux['quantidade'] = $ind['quantidade'];
							$ingAux['unidade'] = $ind['unidade'];
							$ingAux['observacao'] = $ind['observacao'];
							$ingAux['repeticoes'] = $rowRefeicaoRepet['porcoes'];

							array_push($totalArrayIngreds, $ingAux);

							   $ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
						   endforeach;
	   
					   endforeach;
	   
					   $arrayIngredientesTotaisGeral .= ',' . $ingredientesIds;
	   
					   $refLanche .= $index['title'];
				   endforeach;
	   
				   $rowJantar = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($dataSemana->format('Y-m-d') . ' 19:00:00', $dataSemana->format('Y-m-d') . ' 20:00:00', 'CM');
				   
				   
				   foreach($rowJantar as $index):

					$rowRefeicaoId = Nutricaocardapiosrefeicao::getNutricaocardapiorefeicaoByIdHelper($index['idnutricaocardapiosrefeicao']);  
					$rowRefeicaoRepet = Nutricaorefeicoes::getNutricaorefeicaoByIdHelper($rowRefeicaoId['idsrefeicoes']);
	   
					   $ingredientes = '';
					   $ingredientesIds = '';
					   $ingredArray = array();
	   
					   $idsrefeicoes = explode(',', $index['idsrefeicoes']);
	   
					   foreach($idsrefeicoes as $au):
	   
						   $ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));
	   
						   foreach($ingredientes as $ind):

							   $ingAux = array();

							   $ingAux['idnutricaotacoalimento'] = $ind['idnutricaotacoalimento'];
							   $ingAux['descricaotacoalimento'] = $ind['descricaotacoalimento'];
							   $ingAux['quantidade'] = $ind['quantidade'];
							   $ingAux['unidade'] = $ind['unidade'];
							   $ingAux['observacao'] = $ind['observacao'];
							   $ingAux['repeticoes'] = $rowRefeicaoRepet['porcoes'];

							
							   array_push($totalArrayIngreds, $ingAux);

							   $ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
						   endforeach;
	   
					   endforeach;
	   
					   
					   $arrayIngredientesTotaisGeral .= ',' . $ingredientesIds;
	   
					   $refJantar .= "[" . $index['title'] . "] \n ";
				   endforeach;

					/* ================================================================================
									    End Retornando o conteúdo das datas Inseridas
				       ================================================================================ */


				switch($dataSemana->format('w')){

					case '1':
					
						// Verifica se há alguma refeição atribuida para a semana
						if(($refCafe != '') || ($refAlmoco != '')){
							$contadorRefeicoesSemana++; 
						}//end if de verificação

						$pdf->SetXY(66,44);
						$pdf->MultiCell(40, 5, "Segunda " . $dataSemana->format('d/m'), 0, 'C', 1, 0, '', '', true);
						
						$pdf->SetXY(66, 50);
						$pdf->MultiCell(40, 25, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
						
						$pdf->SetXY(66, 76);
						$pdf->MultiCell(40, 25, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(66, 102);
						$pdf->MultiCell(40, 25, $refLanche,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(66, 128);
						$pdf->MultiCell(40, 25, $refJantar,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						
					break;

					case '2':

						// Verifica se há alguma refeição atribuida para a semana
						if(($refCafe != '') || ($refAlmoco != '')){
							$contadorRefeicoesSemana++; 
						}//end if de verificação

						$pdf->SetXY(107,44);
						$pdf->MultiCell(40, 5, "Terça "  . $dataSemana->format('d/m'), 0, 'C', 1, 0, '', '', true);
					
						$pdf->SetXY(107, 50);
						$pdf->MultiCell(40, 25, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(107, 76);
						$pdf->MultiCell(40, 25, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(107, 102);
						$pdf->MultiCell(40, 25, $refLanche,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(107, 128);
						$pdf->MultiCell(40, 25, $refJantar,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
									
					break;

					case '3':

						// Verifica se há alguma refeição atribuida para a semana
						if(($refCafe != '') || ($refAlmoco != '')){
							$contadorRefeicoesSemana++; 
						}//end if de verificação

						$pdf->SetXY(148,44);
						$pdf->MultiCell(40,5, "Quarta " . $dataSemana->format('d/m'), 0, 'C', 1, 0, '', '', true);
				
						$pdf->SetXY(148, 50);
						$pdf->MultiCell(40, 25, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(148, 76);
						$pdf->MultiCell(40, 25, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(148, 102);
						$pdf->MultiCell(40, 25, $refLanche,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(148, 128);
						$pdf->MultiCell(40, 25, $refJantar,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
						
					break;

					case '4':

						// Verifica se há alguma refeição atribuida para a semana
						if(($refCafe != '') || ($refAlmoco != '')){
							$contadorRefeicoesSemana++; 
						}//end if de verificação

						$pdf->SetXY(189,44);
						$pdf->MultiCell(40, 5, "Quinta " . $dataSemana->format('d/m'), 0, 'C', 1, 0, '', '', true);
					
						$pdf->SetXY(189, 50);
						$pdf->MultiCell(40, 25, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(189, 76);
						$pdf->MultiCell(40, 25, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(189, 102);
						$pdf->MultiCell(40, 25, $refLanche,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(189, 128);
						$pdf->MultiCell(40, 25, $refJantar,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
						
					break;

					case '5':

						// Verifica se há alguma refeição atribuida para a semana
						if(($refCafe != '') || ($refAlmoco != '')){
							$contadorRefeicoesSemana++; 
						}//end if de verificação

						$pdf->SetXY(230,44);
						$pdf->MultiCell(40, 5, "Sexta " . $dataSemana->format('d/m'), 0, 'C', 1, 0, '', '', true);
						
						$pdf->SetXY(230, 50);
						$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(230, 76);
						$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(230, 102);
						$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

						$pdf->SetXY(230, 128);
						$pdf->MultiCell(40, 25, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
			
						
					break;

				}//end switch Case

		   }//end foreach for days in week
	   


		   /* =======================================================
							End Conteúdo das Semanas
		   	  ======================================================= */

		   /* =======================================================
		   					Linha de Somatório
			  ======================================================= */

					   
			 

			  	$pdf->SetFont('helvetica', 'B', 8);
				
				$pdf->SetXY(25 + ((245/11) * 0), 154);
				$pdf->MultiCell((245/11), 5, 'Kcal',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 1), 154);
				$pdf->MultiCell((245/11), 5, 'CHO (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 2), 154);
				$pdf->MultiCell((245/11), 5, 'LPD (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 3), 154);
				$pdf->MultiCell((245/11), 5, 'PTN (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 4), 154);
				$pdf->MultiCell((245/11), 5, 'Fibra (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 5), 154);
				$pdf->MultiCell((245/11), 5, 'Vit A (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 6), 154);
				$pdf->MultiCell((245/11), 5, 'Vit c (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 7), 154);
				$pdf->MultiCell((245/11), 5, 'Fe (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 8), 154);
				$pdf->MultiCell((245/11), 5, 'Mg (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 9), 154);
				$pdf->MultiCell((245/11), 5, 'Ca (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 10), 154);
				$pdf->MultiCell((245/11), 5, 'Zn (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				/* ------------------------------------------------------------------------------------------ */
				
				$somas = $this->valorarray($totalArrayIngreds);

				$mostradorDeQTDERefeicoesSemana = $contadorRefeicoesSemana;

				if($contadorRefeicoesSemana == 0)
							$contadorRefeicoesSemana++; 


				$pdf->SetFont('helvetica', '', 8);
				//$soma = $this->valor($arrayIngredientesTotaisGeral);

				//$pdf->SetXY(25  + ((245/11) * 0), 160);
				//$pdf->MultiCell((245/11), 4, number_format(($soma['val_kcal']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 0), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_kcal']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 1), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_cho']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 2), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_lpd']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 3), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_ptn']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 4), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_fibra']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 5), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_vit_a']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 6), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_vit_c']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 7), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_fe']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 8), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_mg']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 9), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_ca']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 10), 160);
				$pdf->MultiCell((245/11), 4, number_format(($somas['val_zn']/$contadorRefeicoesSemana), 2, ',', ' ') ,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 0), 166);

				if($mostradorDeQTDERefeicoesSemana == 0){
					$pdf->MultiCell(50, 4, '* Semana sem refeições',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');
				}else if($mostradorDeQTDERefeicoesSemana == 1){
					$pdf->MultiCell(50, 4, '* dividido entre ' . $mostradorDeQTDERefeicoesSemana . ' Dia',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');
				}else{
					$pdf->MultiCell(50, 4, '* dividido entre ' . $mostradorDeQTDERefeicoesSemana . ' Dias',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');
				}
				
				


		   /* =======================================================
		   					Elementos Finais da página
			  ======================================================= */

			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetXY(80,168);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->MultiCell(50, 5, "Priscila F. Donomenech ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(80,172);
			$pdf->MultiCell(50, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(80,176);
			$pdf->MultiCell(50, 5, "CRN : 3 23.674 ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFillColor(242, 242, 242);
			$pdf->SetFont('helvetica', '', 7);


			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetXY(180,168);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->MultiCell(50, 5, "Sheila C. Souza ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(180,172);
			$pdf->MultiCell(50, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(180,176);
			$pdf->MultiCell(50, 5, "CRN : 3 24.577 ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFillColor(242, 242, 242);
			$pdf->SetFont('helvetica', '', 7);

			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetXY(130,168);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->MultiCell(50, 5, "Cristina R. T. Ragazzi ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(130,172);
			$pdf->MultiCell(50, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(130,176);
			$pdf->MultiCell(50, 5, "CRN : 3 10.999 ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFillColor(242, 242, 242);
			$pdf->SetFont('helvetica', '', 7);

	}//end for

	$filename = 'folhadeponto.pdf'; 
	$pdf->Output($filename, 'I');

	die();
		

	

}// end gerarcardapio



	public function gerarcardapioeeAction(){
		 
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION , PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$this->view->pdf = $pdf;
		
		// valores relaconados a datas

		$id = $this->_request->getParam("id");

		if(($id == 4) or ($id == 7) or ($id == 9) or ($id == 12))
			$id = 10;

		$mesCalendario = '2018-' . $id . '-01';

		$dataInicio =  date('Y-m-d',strtotime($mesCalendario));
		$_Inicio = new DateTime($dataInicio);

		$dataFim = date('Y-m-d', strtotime($mesCalendario));
		$_Fim = new DateTime($dataFim);
		
		$_Fim->modify('last day of this month');
		$ultimoDia = $_Fim->format('d');
		$_Fim->modify('+1 day');
	
		$interval = new DateInterval('P1D');


		$period = new DatePeriod($_Inicio, $interval, $_Fim);

		
		$semana =  (int) $ultimoDia - (7 - (int)$_Inicio->format('w'));
		$somaMaisUm = 0;
		if((7 - (int)$_Inicio->format('w')) > 0){
			$somaMaisUm = 1;
		}else{
			$somaMaisUm = 0;
		}

		$totalSemanas = ceil($semana/7) + $somaMaisUm;
		
		$pdf->setPageOrientation('l');

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Prefeitura Municipal de Santa Isabel');
        $pdf->SetTitle('Folha_Ponto_Individual');
        $pdf->SetSubject('Assinatura do Funcionário');

        $this->view->setScriptPath(APPLICATION_PATH  . '/modules/admin/views/scripts/');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        //$pdf->SetFont('times', '', null , 'false');
        // set font
        $pdf->SetFont('helvetica', '', 8);
        // add a page
		$pdf->AddPage();

			//$html = $this->view->render('nutricaocardapiosrefeicao/helpers/calendario.phtml');
		
		// set cell padding
		$pdf->setCellPaddings(1, 1, 1, 1);

		// set cell margins
		$pdf->setCellMargins(1, 1, 1, 1);

		// set color for background
		$pdf->SetFillColor(242, 242, 242);
		$pdf->SetTextColor(0, 0, 0);

		// MultiCell($w, $h, $txt, $border=0, $align='J', $fill=0, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0)

		// set some text for example
		$txt = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

		// Multicell test
		$_line = 50;
		$_col = 66;
		$_cel_width = 40;
		$_cel_height = 30;

		$counterSemanas = 2;

		$pdf->SetFillColor(242, 242, 242);
		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(25,50);
		$pdf->MultiCell(40, 30, " Café da Manhã", 0, 'C', 1, 0, '', '', true);

		$pdf->SetXY(25,81);
		$pdf->MultiCell(40, 30, " Almoço", 0, 'C', 1, 0, '', '', true);


		$contadorSemanal = 2;

		

/* ================ Segunda Limpa ===================================*/



if(((int)$_Inicio->format('w')) > 1){

	$dataSegunda = date('Y-m-d',strtotime($mesCalendario));
	$_Segunda = new DateTime($dataSegunda);
	$_Segunda->modify('-' . ($_Inicio->format('w') - 1) .  ' day');

	$pdf->SetXY(66,44);
	$pdf->MultiCell($_cel_width, 5, "Segunda (" . $_Segunda->format('d-m')  . ')', 0, 'C', 1, 0, '', '', true);


	//$contadorSemanal++;
	$pdf->SetXY(66, 50);
	$pdf->MultiCell(40, 30, '' ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
	//$contadorSemanal++;
	$pdf->SetXY(66, 81);
	$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

}


if(((int)$_Inicio->format('w')) > 2){
	/* ================ Terça Limpa ===================================*/

	$dataTerca = date('Y-m-d',strtotime($mesCalendario));
	$_Terca = new DateTime($dataTerca);
	$_Terca->modify('-' . ($_Inicio->format('w') - 2) .  ' day');

	
	$pdf->SetXY(107,44);
	$pdf->MultiCell($_cel_width, 5, "Terça(" . $_Terca->format('d-m')  . ')', 0, 'C', 1, 0, '', '', true);

	//$contadorSemanal++;
	$pdf->SetXY(107, 50);
	$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

	//$contadorSemanal++;
	$pdf->SetXY(107, 81);
	$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

	
}

if(((int)$_Inicio->format('w')) > 3){
	/* ================ Quarta Limpa ===================================*/

	$dataQuarta = date('Y-m-d',strtotime($mesCalendario));
	$_Quarta = new DateTime($dataQuarta);
	$_Quarta->modify('-' . ($_Inicio->format('w') - 3) .  ' day');

	$pdf->SetXY(148,44);
	$pdf->MultiCell($_cel_width, 5, "Quarta" . ($_Inicio->format('w') - 3) . '(' . $_Quarta->format('d-m') . ')', 0, 'C', 1, 0, '', '', true);

	//$contadorSemanal++;
	$pdf->SetXY(148, 50);
	$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

	//$contadorSemanal++;
	$pdf->SetXY(148, 81);
	$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

	
}

if(((int)$_Inicio->format('w')) > 4){

	$dataQuinta = date('Y-m-d',strtotime($mesCalendario));
	$_Quinta = new DateTime($dataQuinta);
	$_Quinta->modify('-' . ($_Inicio->format('w') - 4) .  ' day');

	
	$pdf->SetXY(189,44);
	$pdf->MultiCell($_cel_width, 5, "Quinta (" . $_Quinta->format('d-m') , 0, 'C', 1, 0, '', '', true);


	//$contadorSemanal++;
	$pdf->SetXY(189, 50);
	$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

	//$contadorSemanal++;
	$pdf->SetXY(189, 81);
	$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

	
	
}

if(((int)$_Inicio->format('w')) > 5){

	$dataSexta = date('Y-m-d',strtotime($mesCalendario));
	$_Sexta = new DateTime($dataSexta);
	$_Sexta->modify('-' . ($_Inicio->format('w') - 5) .  ' day');

	$pdf->SetXY(230,44);
	$pdf->MultiCell($_cel_width, 5, "Sexta (" . $_Sexta->format('d-m') . ')', 0, 'C', 1, 0, '', '', true);

	//$contadorSemanal++;
	$pdf->SetXY(230, 50);
	$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

	//$contadorSemanal++;
	$pdf->SetXY(230, 81);
	$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
	
}

if(((int)$_Inicio->format('w')) > 6){
	
}


	$arrayIngredientesTotaisGeral = '';


		// Definindo a primeira linha da tabela
		$pdf->SetTextColor(0, 0, 0);
		$pdf->SetXY(25,44);
		$pdf->MultiCell($_cel_width, 5, "Dia da Semana ", 0, 'C', 1, 0, '', '', true);

		foreach($period as $aux):

			

			$refCafe = '';
			$refAlmoco = '';
			$refLanche = '';
			$refJantar = '';

			$rowCafe = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($aux->format('Y-m-d') . ' 08:00:00', $aux->format('Y-m-d') . ' 10:00:00', 'EE');

			foreach($rowCafe as $index):

				$ingredientes = '';
				$ingredientesIds = '';

				$idsrefeicoes = explode(',', $index['idsrefeicoes']);

				foreach($idsrefeicoes as $au):

					$ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));

					foreach($ingredientes as $ind):
						$ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
					endforeach;

				endforeach;

				$arrayIngredientesTotaisGeral .= ',' . $ingredientesIds;

				$refCafe .=  "[" . $index['title'] .  "]";
			endforeach;

			$rowAlmoco = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($aux->format('Y-m-d') . ' 11:30:00', $aux->format('Y-m-d') . ' 13:00:00', 'EE');

			foreach($rowAlmoco as $index):

				$ingredientes = '';
				$ingredientesIds = '';

				$idsrefeicoes = explode(',', $index['idsrefeicoes']);

				foreach($idsrefeicoes as $au):
					$ingredientes = Nutricaorefeicoesingredientes::getNutricaorefeicoesingredientesHelper(array('idnutricaorefeicao'=>$au));

					foreach($ingredientes as $ind):
						$ingredientesIds .= $ind['idnutricaotacoalimento'] . ',';
					endforeach;

				endforeach;

				$arrayIngredientesTotaisGeral .= $ingredientesIds;

				$refAlmoco .= "[" . $index['title'] . "]";
			endforeach;

			$rowLanche = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($aux->format('Y-m-d') . ' 15:00:00', $aux->format('Y-m-d') . ' 17:00:00', 'EE');

			foreach($rowLanche as $index):
				$refLanche .= $index['title'];
			endforeach;

			$rowJantar = Nutricaocardapiosrefeicaocalendario::verificacardapiosagendados($aux->format('Y-m-d') . ' 19:00:00', $aux->format('Y-m-d') . ' 20:00:00', 'CM');

			foreach($rowJantar as $index):
				$refJantar .= $index['title'];
			endforeach;

			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetFont('helvetica', '', 8);
			
			switch($aux->format('w')){

				case '1':
				
				$pdf->SetXY(66,44);
				$pdf->MultiCell($_cel_width, 5, "Segunda (" . $aux->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);


				$pdf->SetXY(66, 50);
				$pdf->MultiCell(40, 30, $refCafe,0, 'C', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refCafe != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(66, 81);
				$pdf->MultiCell(40, 30, $refAlmoco,0, 'C', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refAlmoco != ''){
					$contadorSemanal++;
				}
				
				break;

				case '2':

				$pdf->SetXY(107,44);
				$pdf->MultiCell($_cel_width, 5, "Terça (" . $aux->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);


				$pdf->SetXY(107, 50);
				$pdf->MultiCell(40, 30, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refCafe != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(107, 81);
				$pdf->MultiCell(40, 30, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			
				if($refAlmoco != ''){
					$contadorSemanal++;
				}
				break;

				case '3':

				$pdf->SetXY(148,44);
				$pdf->MultiCell($_cel_width, 5, "Quarta (" . $aux->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);

				$pdf->SetXY(148, 50);
				$pdf->MultiCell(40, 30, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refCafe != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(148, 81);
				$pdf->MultiCell(40, 30, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refAlmoco != ''){
					$contadorSemanal++;
				}
				break;

				case '4':

				$pdf->SetXY(189,44);
				$pdf->MultiCell($_cel_width, 5, "Quinta (" . $aux->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);
			

				$pdf->SetXY(189, 50);
				$pdf->MultiCell(40, 30, $refCafe,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refCafe != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(189, 81);
				$pdf->MultiCell(40, 30, $refAlmoco,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				
				if($refAlmoco != ''){
					$contadorSemanal++;
				}
				break;

				case '5':

				$pdf->SetXY(230,44);
				$pdf->MultiCell($_cel_width, 5, "Sexta (" . $aux->format('d/m') . ")", 0, 'C', 1, 0, '', '', true);

				$pdf->SetXY(230, 50);
				$pdf->MultiCell(40, 30, $refCafe ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refCafe != ''){
					$contadorSemanal++;
				}

				$pdf->SetXY(230, 81);
				$pdf->MultiCell(40, 30, $refAlmoco ,0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

				if($refAlmoco != ''){
					$contadorSemanal++;
				}

				$contSemana = $contadorSemanal;
				$contadorSemanal = 1;
				
				break;
				
				
			}//end switch

			
			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetXY(80,160);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->MultiCell($_cel_width, 5, "Priscila F. Donomenech ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(80,164);
			$pdf->MultiCell($_cel_width, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(80,168);
			$pdf->MultiCell($_cel_width, 5, "CRN : 3 23.674 ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFillColor(242, 242, 242);
			$pdf->SetFont('helvetica', '', 7);



			



			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetXY(180,160);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->MultiCell($_cel_width, 5, "Sheila C. Souza ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(180,164);
			$pdf->MultiCell($_cel_width, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(180,168);
			$pdf->MultiCell($_cel_width, 5, "CRN : 3 24.577 ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFillColor(242, 242, 242);
			$pdf->SetFont('helvetica', '', 7);



			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetXY(130,160);
			$pdf->SetFont('helvetica', '', 9);
			$pdf->MultiCell($_cel_width, 5, "Cristina R. T. Ragazzi ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(130,164);
			$pdf->MultiCell($_cel_width, 5, "Nutricionista ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFont('helvetica', '', 8);
			$pdf->SetXY(130,168);
			$pdf->MultiCell($_cel_width, 5, "CRN : 3 10.999 ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetFillColor(242, 242, 242);
			$pdf->SetFont('helvetica', '', 7);

			


			if($aux->format('w') == 0){

				/* Ultima linha Titulos */


				$pdf->SetFillColor(255, 255, 255);
				$pdf->SetFont('helvetica', 'B', 12);
				$pdf->SetXY(25,35);
				$pdf->MultiCell(245, 8, " Cardápio EMEI / EMEF " . MN_Util::getMes($id) . " de " . date('Y'), 0, 'C', 1, 0, '', '', true);

				$pdf->SetFillColor(242, 242, 242);
				


				$pdf->SetFont('helvetica', '', 8);
				$pdf->SetXY(25,112);
				$pdf->MultiCell(245, 5, " Lembretes: Saladas (Folhas e Legumes), Sobremesas : Devem ser Servidas no minimo 2 porcoes de Frutas/Semana e 1 Porcao das \n Preparacoes (gelatina, canjica, sagu). No cafe da Manha Diversificar o sabor das Bebidas", 0, 'C', 1, 0, '', '', true);

				$pdf->SetFont('helvetica', 'B', 8);
				$pdf->SetTextColor(255, 255, 255);
				$pdf->SetXY(25 + ((245/11) * 0), 124);
				$pdf->MultiCell((245/11), 5, 'Kcal',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 1), 124);
				$pdf->MultiCell((245/11), 5, 'CHO (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 2), 124);
				$pdf->MultiCell((245/11), 5, 'LPD (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 3), 124);
				$pdf->MultiCell((245/11), 5, 'PTN (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 4), 124);
				$pdf->MultiCell((245/11), 5, 'Fibra (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 5), 124);
				$pdf->MultiCell((245/11), 5, 'Vit A (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 6), 124);
				$pdf->MultiCell((245/11), 5, 'Vit c (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 7), 124);
				$pdf->MultiCell((245/11), 5, 'Fe (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 8), 124);
				$pdf->MultiCell((245/11), 5, 'Mg (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 9), 124);
				$pdf->MultiCell((245/11), 5, 'Ca (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25 + ((245/11) * 10), 124);
				$pdf->MultiCell((245/11), 5, 'Zn (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');


				/* Ultima linha valores */
				$pdf->SetTextColor(153, 153, 153);
				$pdf->SetFont('helvetica', 'B', 8);
				$soma = $this->valor($arrayIngredientesTotaisGeral);

				$total_kcal =  number_format((($soma['val_kcal']) / ($contSemana + 1)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 0), 128);
				$pdf->MultiCell((245/11), 4, $total_kcal,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_cho =  number_format((($soma['val_cho']) / ($contSemana + 1)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 1), 128);
				$pdf->MultiCell((245/11), 4, $total_cho,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_lpd =  number_format((($soma['val_lpd']) / ($contSemana-1)), 2, ',', ' ');
				$total_lpd = number_format(($soma['val_lpd']) / ($contSemana + 1), 2, ',', ' ');
				

				$pdf->SetXY(25  + ((245/11) * 2), 128);
				$pdf->MultiCell((245/11), 4, $total_lpd,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_ptn =  number_format((($soma['val_ptn']) / ($contSemana + 1)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 3), 128);
				$pdf->MultiCell((245/11), 4, $total_ptn,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_fibra =  number_format((($soma['val_fibra']) / ($contSemana + 1)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 4), 128);
				$pdf->MultiCell((245/11), 4, $total_fibra,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_vit_a =  number_format((($soma['val_vit_a']) / ($contSemana + 1)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 5), 128);
				$pdf->MultiCell((245/11), 4, $total_vit_a,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_vit_c =  number_format((($soma['val_vit_c']) / ($contSemana + 1)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 6), 128);
				$pdf->MultiCell((245/11), 4, $total_vit_c,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_fe =  number_format((($soma['val_fe']) / ($contSemana + 1)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 7), 128);
				$pdf->MultiCell((245/11), 4, $total_fe,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_mg =  number_format((($soma['val_mg']) / ($contSemana + 1)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 8), 128);
				$pdf->MultiCell((245/11), 4, $total_mg,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$total_ca = number_format((($soma['val_ca']) / ($contSemana + 1)), 2, ',', ' ');

				$pdf->SetXY(25  + ((245/11) * 9), 128);
				$pdf->MultiCell((245/11), 4, $total_ca,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

				$pdf->SetXY(25  + ((245/11) * 0), 134);
				$pdf->MultiCell(60, 4, '* Valor total dividido entre ' . ($contSemana + 1) . ' Refeições',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');
				

				$pdf->SetXY(25  + ((245/11) * 10), 128);
				$pdf->MultiCell((245/11), 4, $soma['val_zn'],0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');
				$arrayIngredientesTotaisGeral = '';
				$pdf->SetFont('helvetica', '', 8);
				$pdf->SetTextColor(0,0,0);
				$pdf->AddPage();
				
				$pdf->SetXY(25,50);
				$pdf->MultiCell(40, 30, " Café da Manhã ", 0, 'C', 1, 0, '', '', true);

				$pdf->SetXY(25,81);
				$pdf->MultiCell(40, 30, " Almoço ", 0, 'C', 1, 0, '', '', true);
				
			}//
			
			

		endforeach;

		

		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetFont('helvetica', 'B', 12);
		$pdf->SetXY(25,35);
		//$pdf->MultiCell(245, 8, " Cardápio EMEI / EMEF " . $counterSemanas++ . "° Semana 2018", 0, 'C', 1, 0, '', '', true);

		$pdf->MultiCell(245, 8, " Cardápio EMEI / EMEF " . MN_Util::getMes($id) . " de " . date('Y'), 0, 'C', 1, 0, '', '', true);

		$pdf->SetFillColor(242, 242, 242);

		$pdf->SetFillColor(242, 242, 242);
		$pdf->SetFont('helvetica', '', 8);

		/* ================ Segunda Limpa ===================================*/

		if(((int)$_Fim->format('w')) < 2){

			$pdf->SetXY(66,44);
			$pdf->MultiCell($_cel_width, 5, "Segunda x", 0, 'C', 1, 0, '', '', true);


			$pdf->SetXY(66, 50);
			$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(66, 81);
			$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');




		}
		if(((int)$_Fim->format('w')) < 3){
			/* ================ Terça Limpa ===================================*/

			$pdf->SetXY(107,44);
			$pdf->MultiCell($_cel_width, 5, "Terçax", 0, 'C', 1, 0, '', '', true);


			$pdf->SetXY(107, 50);
			$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(107, 81);
			$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');


			
		}

		if(((int)$_Fim->format('w')) < 4){
			/* ================ Quarta Limpa ===================================*/

			$pdf->SetXY(148,44);
			$pdf->MultiCell($_cel_width, 5, "Quarta ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetXY(148, 50);
			$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(148, 81);
			$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');


			
		}

		if(((int)$_Fim->format('w')) < 5){

			
			$pdf->SetXY(189,44);
			$pdf->MultiCell($_cel_width, 5, "Quinta ", 0, 'C', 1, 0, '', '', true);


			$pdf->SetXY(189, 50);
			$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(189, 81);
			$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');
			
		}

		if(((int)$_Fim->format('w')) < 6){

			$pdf->SetXY(230,44);
			$pdf->MultiCell($_cel_width, 5, "Sexta ", 0, 'C', 1, 0, '', '', true);

			$pdf->SetXY(230, 50);
			$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

			$pdf->SetXY(230, 81);
			$pdf->MultiCell(40, 30, '',0, 'J', 1, 1, '', '', true, 0, false, true, 30, 'M');

		
			
		}

		if(((int)$_Fim->format('w')) < 7){
			
		}
		
		
		$pdf->SetFont('helvetica', '', 8);
		$pdf->SetXY(25,112);
		$pdf->MultiCell(245, 5, " Lembretes: Saladas (Folhas e Legumes), Sobremesas : Devem ser Servidas no mńimo 2 porções de Frutas/Semana e 1 Porção das \n Preparações (gelatina, canjica, sagu). No café da Manhã Diversificar o sabor das Bebidas", 0, 'C', 1, 0, '', '', true);


		$pdf->SetFont('helvetica', 'B', 8);
		$pdf->SetTextColor(255, 255, 255);
		$pdf->SetXY(25 + ((245/11) * 0), 124);
		$pdf->MultiCell((245/11), 5, 'Kcal',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 1), 124);
		$pdf->MultiCell((245/11), 5, 'CHO (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 2), 124);
		$pdf->MultiCell((245/11), 5, 'LPD (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 3), 124);
		$pdf->MultiCell((245/11), 5, 'PTN (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 4), 124);
		$pdf->MultiCell((245/11), 5, 'Fibra (g)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 5), 124);
		$pdf->MultiCell((245/11), 5, 'Vit A (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 6), 124);
		$pdf->MultiCell((245/11), 5, 'Vit c (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 7), 124);
		$pdf->MultiCell((245/11), 5, 'Fe (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 8), 124);
		$pdf->MultiCell((245/11), 5, 'Mg (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 9), 124);
		$pdf->MultiCell((245/11), 5, 'Ca (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25 + ((245/11) * 10), 124);
		$pdf->MultiCell((245/11), 5, 'Zn (mg)',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');


		/* Ultima linha valores */
		$pdf->SetTextColor(153, 153, 153);
		$pdf->SetFont('helvetica', 'B', 8);
		$soma = $this->valor($arrayIngredientesTotaisGeral);

		$total_kcal =  number_format((($soma['val_kcal']) / ($contSemana + 1)), 2, ',', ' ');

		$pdf->SetXY(25  + ((245/11) * 0), 128);
		//$total_kcal =  number_format((($soma['val_kcal']) / ($contSemana + 1)), 2, ',', ' ');

		$pdf->SetXY(25  + ((245/11) * 0), 128);
		//$pdf->MultiCell((245/11), 4, $total_kcal,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		//$total_cho =  (($soma['val_cho']) / ($contSemana-1));
		//$total_cho =  number_format((($soma['val_cho']) / ($contSemana + 1)), 2, ',', ' ');

		$pdf->SetXY(25  + ((245/11) * 1), 128);
		//$pdf->MultiCell((245/11), 4, $total_cho,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		//$total_lpd =  number_format((($soma['val_lpd']) / ($contSemana-1)), 2, ',', ' ');
		$total_lpd = number_format(($soma['val_lpd']) / ($contSemana + 1), 2, ',', ' ');

		$pdf->SetXY(25  + ((245/11) * 2), 128);
		//$pdf->MultiCell((245/11), 4, $total_lpd,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$total_ptn =  number_format((($soma['val_ptn']) / ($contSemana + 1)), 2, ',', ' ');

		$pdf->SetXY(25  + ((245/11) * 3), 128);
		//$pdf->MultiCell((245/11), 4, $total_ptn,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$total_fibra =  number_format((($soma['val_fibra']) / ($contSemana + 1)), 2, ',', ' ');

		$pdf->SetXY(25  + ((245/11) * 4), 128);
		//$pdf->MultiCell((245/11), 4, $total_fibra,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$total_vit_a =  number_format((($soma['val_vit_a']) / ($contSemana + 1)), 2, ',', ' ');

		$pdf->SetXY(25  + ((245/11) * 5), 128);
		//$pdf->MultiCell((245/11), 4, $total_vit_a,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$total_vit_c =  number_format((($soma['val_vit_c']) / ($contSemana + 1)), 2, ',', ' ');

		$pdf->SetXY(25  + ((245/11) * 6), 128);
		//$pdf->MultiCell((245/11), 4, $total_vit_c,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$total_fe =  number_format((($soma['val_fe']) / ($contSemana + 1)), 2, ',', ' ');

		$pdf->SetXY(25  + ((245/11) * 7), 128);
		//$pdf->MultiCell((245/11), 4, $total_fe,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$total_mg =  number_format((($soma['val_mg']) / ($contSemana + 1)), 2, ',', ' ');

		$pdf->SetXY(25  + ((245/11) * 8), 128);
		//$pdf->MultiCell((245/11), 4, $total_mg,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$total_ca = number_format((($soma['val_ca']) / ($contSemana + 1)), 2,',',' ');

		$pdf->SetXY(25  + ((245/11) * 9), 128);
		//$pdf->MultiCell((245/11), 4, $total_ca,0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');

		$pdf->SetXY(25  + ((245/11) * 0), 134);
		//$pdf->MultiCell(60, 4, '* Valor total dividido entre ' . ($contSemana + 1) . ' Refeições',0, 'C', 1, 1, '', '', true, 0, false, true, 4, 'M');
		
		
        
        $filename = 'folhadeponto.pdf'; 
        $pdf->Output($filename, 'I');

		die();

	}//end public function gerarpdfEE


	public function valor($variavel){

		$variavel = str_replace(",,", ",", $variavel);

		$array = explode(',', $variavel);

		


		$somaTotal = array();

		$somaTotal['val_kcal'] = 0;
		$somaTotal['val_cho'] = 0;
		$somaTotal['val_lpd'] = 0;
		$somaTotal['val_ptn'] = 0;
		$somaTotal['val_fibra'] = 0;
		$somaTotal['val_vit_a'] = 0;
		$somaTotal['val_vit_c'] = 0;
		$somaTotal['val_fe'] = 0;
		$somaTotal['val_mg'] = 0;
		$somaTotal['val_ca'] = 0;
		$somaTotal['val_zn'] = 0;
		$somaTotal['resposta'] = $variavel;
		$somaTotal['voltas'] = 0;


		foreach($array as $idAlimento):

			$row = Nutricaotacoalimentos::getNutricaotacoalimentoByIdHelper($idAlimento);

			$somaTotal['val_kcal'] += isset($row['energia_kcal'])? (float)$row['energia_kcal'] : 00.00;
			$somaTotal['val_cho'] += isset($row['vl_carb'])? (float)$row['vl_carb'] : 00.00;
			$somaTotal['val_lpd'] += isset($row['vl_g_tot'])? (float)$row['vl_g_tot'] : 00.00; 
			$somaTotal['val_ptn'] += isset($row['vl_prot'])? (float)$row['vl_prot'] : 00.00; 
			$somaTotal['val_fibra'] += isset($row['vl_fibra'])? (float)$row['vl_fibra'] : 00.00; 
			$somaTotal['val_vit_a'] += isset($row['vl_vit_a'])? (float)$row['vl_vit_a'] : 00.00; 
			$somaTotal['val_vit_c'] += isset($row['vl_vit_c'])? (float)$row['vl_vit_c'] : 00.00; 
			$somaTotal['val_fe'] += isset($row['vl_fe'])? (float)$row['vl_fe'] : 00.00; 
			$somaTotal['val_mg'] += isset($row['vl_mg'])? (float)$row['vl_mg'] : 00.00; 
			$somaTotal['val_ca'] += isset($row['vl_ca'])? (float)$row['vl_ca'] : 00.00; 
			$somaTotal['val_zn'] += isset($row['vl_zinco'])? (float)$row['vl_zinco'] : 00.00;
			$somaTotal['voltas'] += 1;
			
		endforeach;

		
		

		return $somaTotal;

		

	}

	public function valorarray($variavel){

		//$variavel = str_replace(",,", ",", $variavel);


		$somaTotal = array();

		$somaTotal['val_kcal'] = 0;
		$somaTotal['val_cho'] = 0;
		$somaTotal['val_lpd'] = 0;
		$somaTotal['val_ptn'] = 0;
		$somaTotal['val_fibra'] = 0;
		$somaTotal['val_vit_a'] = 0;
		$somaTotal['val_vit_c'] = 0;
		$somaTotal['val_fe'] = 0;
		$somaTotal['val_mg'] = 0;
		$somaTotal['val_ca'] = 0;
		$somaTotal['val_zn'] = 0;
		$somaTotal['resposta'] = 0;
		$somaTotal['voltas'] = '';
		$somaTotal['conferencia'] = 'w';

		foreach($variavel as $idAlimento):

			$row = Nutricaotacoalimentos::getNutricaotacoalimentoByIdHelper($idAlimento['idnutricaotacoalimento']);


			$kcal = (4*(float)str_replace(',','.',$row['vl_carb'])) + (4*(float)str_replace(',','.',$row['vl_prot'])) + (9*(float)str_replace(',','.',$row['vl_g_tot']));
			$soma = ((($kcal)*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
			$somaTotal['val_kcal'] += ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));

			//vl_cho
			if(isset($row['vl_carb'])){
				$soma = ((((float)str_replace(',','.',$row['vl_carb']))*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
				$somaTotal['val_cho'] += ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));
			}else{
				$somaTotal['val_cho'] += (float)00.00;
			}
			// $somaTotal['conferencia'] .= 'cc';

			// vl_lpd
			if(isset($row['vl_g_tot'])){
				$soma = ((((float)str_replace(',','.',$row['vl_g_tot']))*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
				$somaTotal['val_lpd'] += ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));
			}else{
				$somaTotal['val_lpd'] += (float)00.00;
			}

			// val_ptn
			if(isset($row['vl_prot'])){
				$soma = ((((float)str_replace(',','.',$row['vl_prot']))*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
				$somaTotal['val_ptn'] += ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));
			}else{
				$somaTotal['val_ptn'] += (float)00.00;
			}
			
			// val_fibra
			if(isset($row['vl_fibra'])){
				$soma = ((((float)str_replace(',','.',$row['vl_fibra']))*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
				$somaTotal['val_fibra'] += ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));
			}else{
				$somaTotal['val_fibra'] += (float)00.00;
			}

			// val_vit_a
			if(isset($row['vl_vit_a'])){
				$soma = ((((float)str_replace(',','.',$row['vl_vit_a']))*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
				$somaTotal['val_vit_a'] += ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));
			}else{
				$somaTotal['val_vit_a'] += (float)00.00;
			}

			// val_vit_c
			if(isset($row['vl_vit_c'])){
				$soma = ((((float)str_replace(',','.',$row['vl_vit_c']))*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
				$somaTotal['val_vit_c'] += ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));
			}else{
				$somaTotal['val_vit_c'] += (float)00.00;
			}

			// val_fe
			if(isset($row['vl_fe'])){
				$soma = ((((float)str_replace(',','.',$row['vl_fe']))*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
				$somaTotal['val_fe'] += ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));
			}else{
				$somaTotal['val_fe'] = (float)00.00;
			}

			// val_mg
			if(isset($row['vl_mg'])){
				$soma = ((((float)str_replace(',','.',$row['vl_mg']))*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
				$somaTotal['val_mg'] += ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));
			}else{
				$somaTotal['val_mg'] += (float)00.00;
			}

			// val_ca
			if(isset($row['vl_ca'])){
				$soma = ((((float)str_replace(',','.',$row['vl_ca']))*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
				$somaTotal['val_ca'] = ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));
			}else{
				$somaTotal['val_ca'] += (float)00.00;
			}

			// val_zn
			if(isset($row['vl_zinco'])){
				$soma = ((((float)str_replace(',','.',$row['vl_zinco']))*(((float)str_replace(',','.',$idAlimento['observacao']))/(float)100)) * ((float)str_replace(',','.',$idAlimento['quantidade'])));
				$somaTotal['val_zn'] += ($soma*((float)str_replace(',','.',$idAlimento['repeticoes'])));
			}else{
				$somaTotal['val_zn'] += (float)00.00;
			}

			// $somaTotal['voltas'] .=  $row['vl_carb'] . ' - ' . $row['observacao'] . ' - ' . $row['quantidade'];
			
		endforeach;

		

		return $somaTotal;

		

	}



}