<?php

/**
 * Controle da classe financeirolancamentosreceitas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Admin_FinanceirolancamentosreceitasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Financeirolancamentoreceita
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->usuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockAccess("financeirolancamentosreceitas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->usuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Financeirolancamentosreceitas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Lançamento de Receita excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="financeirolancamentosreceitas") $objs = new Financeirolancamentosreceitas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeirolancamentosreceitas', 'name' => 'Lançamentos de Receitas'),
			array('url' => null,'name' => 'Visualizar Lançamento de Receita')
		);
		
		$id = (int)$this->_request->getParam("id");
		$financeirolancamentosreceitas = new Financeirolancamentosreceitas();
		$financeirolancamentoreceita = $financeirolancamentosreceitas->getFinanceirolancamentoreceitaById($id, array());
		
		if (!$financeirolancamentoreceita) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $financeirolancamentoreceita;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Lançamentos de Receitas')
		);
		
		$ns = new Zend_Session_Namespace('default_financeirolancamentosreceitas');
		$financeirolancamentosreceitas = new Financeirolancamentosreceitas();
		$queries = array();	
		$queries['idescola'] = Usuarios::getUsuario('idescola');
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idtipoconvenio"]!="") $queries["idtipoconvenio"] = $this->view->post_var["idtipoconvenio"];
// if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
if ($this->view->post_var["data_i"]!="") $queries["data_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_i"]));
if ($this->view->post_var["data_f"]!="") $queries["data_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["data_f"]));
if ($this->view->post_var["valor"]!="") $queries["valor"] = $this->view->post_var["valor"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $financeirolancamentosreceitas->getFinanceirolancamentosreceitas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $financeirolancamentosreceitas->getFinanceirolancamentosreceitas($queries, $paginaAtual, $maxpp);	
	}

	public function getdadosbancoAction() {
		$this->_helper->layout->disableLayout();
		
		$ididentificacao = (string)$this->_request->getParam("ididentificacao");
		$financeiros = new Escolasapmscontas();
		$queries = array();
		$queries['tipo'] = $ididentificacao;
		$financeiro = $financeiros->getEscolasapmscontasHelper($queries);
		
		if (!isset($financeiro['id'])) ;
		echo '<pre>'; print_r($queries);die; 
		
		$this->view->post_var = $financeiro;
		
	}

	public function gettipobancoAction() {
		$this->_helper->layout->disableLayout();
		//$this->_response->setHeader("content-type", "text/xml");      

		$id = (int)$this->getRequest()->getPost('idtipoconvenio');
		$tipoconvenio = Financeirotiposconvenios::getFinanceirotipoconvenioByIdHelper($id);
		
		$queries = array();
		$queries['tipo'] = $tipoconvenio['titulo'];
		 
		$escolasapmscontas = new Escolasapmscontas();
		$rows = $escolasapmscontas->getEscolasapmscontasHelper($queries);


		echo json_encode($rows);
		die();

	}
	
	/**
	 * 
	 * Action de edição de financeirolancamentosreceitas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeirolancamentosreceitas', 'name' => 'Lançamentos de Receitas'),
			array('url' => null,'name' => 'Editar Lançamento de Receita')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$financeirolancamentosreceitas = new Financeirolancamentosreceitas();
		$financeirolancamentoreceita = $financeirolancamentosreceitas->getFinanceirolancamentoreceitaById($id);
		
		if (!$financeirolancamentoreceita) 
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $financeirolancamentoreceita;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($financeirolancamentoreceita);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Lançamento de Receita editado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de financeirolancamentosreceitas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'financeirolancamentosreceitas', 'name' => 'Lançamentos de Receitas'),
			array('url' => null,'name' => 'Adicionar Lançamento de Receita')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Lançamento de Receita adicionado com sucesso.";
			
			$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idfinanceirolancamentoreceita
     */    
    private function preForm($idfinanceirolancamentoreceita = 0) {
    	$escolas = new Escolas();
    	$this->view->escolas = $escolas->getEscolas(array('status' => 'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_financeirolancamentoreceita = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)$this->getRequest()->getPost("idescola");
		$idlocal = (int)$this->getRequest()->getPost("idlocal");
		$idtipoconvenio = (int)trim($this->getRequest()->getPost("idtipoconvenio"));
		$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
$sequencial = trim($this->getRequest()->getPost("sequencial"));
$banco = trim($this->getRequest()->getPost("banco"));
$agencia = trim($this->getRequest()->getPost("agencia"));
$conta = trim($this->getRequest()->getPost("conta"));
$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
$idtiporeceita = (int)trim($this->getRequest()->getPost("idtiporeceita"));
$descricaoreceita = trim($this->getRequest()->getPost("descricaoreceita"));
$ndocumento = trim($this->getRequest()->getPost("ndocumento"));
$valor = MN_Util::trataNum(trim($this->getRequest()->getPost("valor")));
$observacao = trim($this->getRequest()->getPost("observacao"));
$iddocumento = (int)trim($this->getRequest()->getPost("iddocumento"));
$status = trim($this->getRequest()->getPost("status1"));
$idbanco = (int)trim($this->getRequest()->getPost("idbanco"));

$___rows = Escolasapmscontas::getEscolasapmscontasHelper(array('id'=>$idbanco));
$___row = (sizeof($___rows)>0) ? 	$___rows[0] : false;	

$banco = ($___row) ? $___row['banco'] : '--';		
$agencia = ($___row) ? $___row['agencia'] : '--';		
$conta = ($___row) ? $___row['conta'] : '--';		

		
		$erros = array();
		
		if (""==$sequencial) array_push($erros, "Informe a Sequencial.");
if (0==$idbanco) array_push($erros, "Informe a Banco.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$financeirolancamentosreceitas = new Financeirolancamentosreceitas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
			$dados["idlocal"] = $idlocal;
			$dados["idtipoconvenio"] = $idtipoconvenio;
			$dados["idusuariologado"] = $idusuariologado;
			$dados["sequencial"] = $sequencial;
			$dados["banco"] = $banco;
			$dados["agencia"] = $agencia;
			$dados["conta"] = $conta;
			$dados["data"] = date("Y-m-d", $data);
			$dados["idtiporeceita"] = $idtiporeceita;
			$dados["descricaoreceita"] = $descricaoreceita;
			$dados["ndocumento"] = $ndocumento;
			$dados["valor"] = $valor;
			$dados["observacao"] = $observacao;
			$dados["idbanco"] = $idbanco;
			$iddocumento = $this->getArquivo('iddocumento');
						if ($iddocumento!=0) $dados['iddocumento'] = $iddocumento;
			$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $financeirolancamentosreceitas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}