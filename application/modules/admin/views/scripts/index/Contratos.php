<?php

/**
 * Define o modelo Contratos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Contratos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "contratos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	

	public static $_FOLLOW_NA = "na";
	public static $_FOLLOW_APROVADO = "aprovado";
	public static $_FOLLOW_APROVADOCPENDENCIA = "aprovadocpendencia";
	public static $_FOLLOW_REPROVADO = "reprovado";
	public static function getFollows($field = false, $ns = false) {
		if (is_null($field)) $field = self::$_FOLLOW_NA;

		$res = array(
			self::$_FOLLOW_NA => array('label'=>'Não Informado', 'icon'=>'icon-question-sign', 'cor'=>'#555'),
			self::$_FOLLOW_APROVADO => array('label'=>'Aprovado', 'icon'=>'icon-ok-sign', 'cor'=>'#080'),
			self::$_FOLLOW_APROVADOCPENDENCIA => array('label'=>'Aprovado c/ pendência', 'icon'=>'icon-exclamation-sign', 'cor'=>'#EFB035'),
			self::$_FOLLOW_REPROVADO => array('label'=>'Reprovado', 'icon'=>'icon-remove', 'cor'=>'#800'),
		);

		if (!$field) return $res;
		if (!$ns) return $res[$field];

		return $res[$field][$ns];
	}



	/*
	//DEPRECADO
	public static $_TIPO_GENERICO = "generico";

	public static function getTipos($field = false) {
		$res = array(
			self::$_TIPO_GENERICO=>'Genérico'
		);

		if (!$field) return $res;

		return $res[$field];
	}
	*/


	public static function getContratosAgendas() {
		$strsql = "SELECT SUM(cm1.qtd) as qtdtotal, GROUP_CONCAT(cm1.idmodelo) as idsmodelo, c1.*, e1.nomefantasia 
					FROM contratosmodelos cm1 
						INNER JOIN contratos c1 ON c1.id=cm1.idcontrato
						LEFT JOIN empresas e1 ON e1.id=c1.idempresa
					WHERE c1.excluido='nao' 
					GROUP BY c1.id 
					ORDER BY e1.nomefantasia";


		$db = Zend_Registry::get('db');					
		return $db->fetchAll($strsql);
	}

	public static function getContratosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$contratos = new Contratos();
		return $contratos->getContratos($queries, $page, $maxpage);
	}
	
	public function getContratos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$datacriacao = (isset($queries["datacriacao"])) ? $queries["datacriacao"] : false;
		if ($datacriacao) array_push($where, " c1.datacriacao = '$datacriacao' ");

		$idempresa = (isset($queries["idempresa"])) ? $queries["idempresa"] : false;
		if ($idempresa) array_push($where, " c1.idempresa = $idempresa ");

		$idtipo = (isset($queries["idtipo"])) ? $queries["idtipo"] : false;
		if ($idtipo) array_push($where, " c1.idtipo = $idtipo ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");

		$having = "";
		$inspetores = (isset($queries["inspetores"])) ? $queries["inspetores"] : false;
		if ($inspetores=='sem_inspetor') $having = " HAVING (COUNT(ci1.id)=0) ";
		if ($inspetores=='com_inspetor') $having = " HAVING (COUNT(ci1.id)>0) ";


		$dep1 = (isset($queries["dep1"])) ? $queries["dep1"] : false;
		if ($dep1) array_push($where, " IFNULL(c1.dep1,'na') = '$dep1' ");

		$dep2 = (isset($queries["dep2"])) ? $queries["dep2"] : false;
		if ($dep2) array_push($where, " IFNULL(c1.dep2,'na') = '$dep2' ");

		$dep3 = (isset($queries["dep3"])) ? $queries["dep3"] : false;
		if ($dep3) array_push($where, " IFNULL(c1.dep3,'na') = '$dep3' ");

		$dep4 = (isset($queries["dep4"])) ? $queries["dep4"] : false;
		if ($dep4) array_push($where, " IFNULL(c1.dep4,'na') = '$dep4' ");

		$dep5 = (isset($queries["dep5"])) ? $queries["dep5"] : false;
		if ($dep5) array_push($where, " IFNULL(c1.dep5,'na') = '$dep5' ");

		$dep6 = (isset($queries["dep6"])) ? $queries["dep6"] : false;
		if ($dep6) array_push($where, " IFNULL(c1.dep6,'na') = '$dep6' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*, e1.nome as nome_empresa, e1.nomefantasia, COUNT(ci1.id) as totalinspetores, t1.tipo"; 
;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM contratos c1
						LEFT JOIN empresas e1 ON e1.id=c1.idempresa
						LEFT JOIN contratosinspetores ci1 ON (ci1.idcontrato=c1.id AND ci1.situacao='sim')
						LEFT JOIN contratostipos t1 ON t1.id=c1.idtipo
					
					WHERE c1.excluido='nao' 
						$w 
					GROUP BY c1.id	
					$having
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$strsql = "SELECT COUNT(v1.total) as total FROM ($strsql) v1";
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getContratosById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getContratos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getContratosByIdHelper($id, $queries = array()) {
		$rows = new Contratos();
		return $rows->getContratosById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Contratos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		

		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->idempresa = (array_key_exists("idempresa",$dados)) ? $dados["idempresa"] : $row->idempresa;
		$row->idtipo = (array_key_exists("idtipo",$dados)) ? $dados["idtipo"] : $row->idtipo;
		$row->vencimentocontrato = (array_key_exists("vencimentocontrato",$dados)) ? $dados["vencimentocontrato"] : $row->vencimentocontrato;

		$row->iniciocontrato = (array_key_exists("iniciocontrato",$dados)) ? $dados["iniciocontrato"] : $row->iniciocontrato;
		$row->validadepedidomes = (array_key_exists("validadepedidomes",$dados)) ? $dados["validadepedidomes"] : $row->validadepedidomes;
		$row->validadepedidoano = (array_key_exists("validadepedidoano",$dados)) ? $dados["validadepedidoano"] : $row->validadepedidoano;
		$row->numerocontrato = (array_key_exists("numerocontrato",$dados)) ? $dados["numerocontrato"] : $row->numerocontrato;
		$row->obs_pedidofinanceiro = (array_key_exists("obs_pedidofinanceiro",$dados)) ? $dados["obs_pedidofinanceiro"] : $row->obs_pedidofinanceiro;
		
		
		$row->valor = (array_key_exists("valor",$dados)) ? $dados["valor"] : $row->valor;
		$row->pservico = (array_key_exists("pservico",$dados)) ? $dados["pservico"] : $row->pservico;
		
		$row->inspetor_dias = (array_key_exists("inspetor_dias",$dados)) ? $dados["inspetor_dias"] : $row->inspetor_dias;
		$row->obs = (array_key_exists("obs",$dados)) ? $dados["obs"] : $row->obs;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;

		$row->dep1 = (array_key_exists("dep1",$dados)) ? $dados["dep1"] : $row->dep1;
		$row->dep2 = (array_key_exists("dep2",$dados)) ? $dados["dep2"] : $row->dep2;
		$row->dep3 = (array_key_exists("dep3",$dados)) ? $dados["dep3"] : $row->dep3;
		$row->dep4 = (array_key_exists("dep4",$dados)) ? $dados["dep4"] : $row->dep4;
		$row->dep5 = (array_key_exists("dep5",$dados)) ? $dados["dep5"] : $row->dep5;
		$row->dep6 = (array_key_exists("dep6",$dados)) ? $dados["dep6"] : $row->dep6;

		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
	
}