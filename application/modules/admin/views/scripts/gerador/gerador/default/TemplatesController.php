<?php

/**
 * Controle da classe MSYStemplates do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Prefixo_MSYSTemplatesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var MSYSTemplate
	 */
	PROTECTED_USUARIO_NULL	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset(NS_USUARIO)) $this->_redirect(MODULO_NS . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		MODULO_BLOCK_ACCESS("MSYStemplates", $this->_request->getActionName());	
		
		THIS_USUARIO = unserialize(NS_USUARIO);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new MSYSTemplates();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = THIS_USUARIO['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "MHUMTemplate excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="MSYStemplates") $objs = new MSYSTemplates();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = THIS_USUARIO['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'MSYStemplates', 'name' => 'MHUMTemplates'),
			array('url' => null,'name' => 'Visualizar MHUMTemplate')
		);
		
		$id = (int)$this->_request->getParam("id");
		$MSYStemplates = new MSYSTemplates();
		$MSYStemplate = $MSYStemplates->getMSYSTemplateById($id, array());
		
		if (!$MSYStemplate) 
			$this->_redirect(MODULO_NS . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $MSYStemplate;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'MHUMTemplates')
		);
		
		$ns = new Zend_Session_Namespace('default_MSYStemplates');
		$MSYStemplates = new MSYSTemplates();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect(MODULO_NS . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			$FILTER_HERE;    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $MSYStemplates->getMSYSTemplates($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $MSYStemplates->getMSYSTemplates($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de MSYStemplates
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'MSYStemplates', 'name' => 'MHUMTemplates'),
			array('url' => null,'name' => 'Editar MHUMTemplate')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$MSYStemplates = new MSYSTemplates();
		$MSYStemplate = $MSYStemplates->getMSYSTemplateById($id);
		
		if (!$MSYStemplate) 
			$this->_redirect(MODULO_NS . '/' . $this->getRequest()->getControllerName());
		
		$this->view->post_var = $MSYStemplate;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($MSYStemplate);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "MHUMTemplate editado com sucesso.";
			
			$this->_redirect(MODULO_NS . '/' . $this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de MSYStemplates 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'MSYStemplates', 'name' => 'MHUMTemplates'),
			array('url' => null,'name' => 'Adicionar MHUMTemplate')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "MHUMTemplate adicionado com sucesso.";
			
			$this->_redirect(MODULO_NS . '/' . $this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idMSYStemplate
     */    
    private function preForm($idMSYStemplate = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_MSYStemplate = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$GET_FIELDS_FRONT;		
		
		$erros = array();
		
		$REQUIRED;
		
		$MSYStemplates = new MSYSTemplates();
		
		$UNIQUE;
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$ATTR_VALUES_TO_DB;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = THIS_USUARIO['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $MSYStemplates->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}