<?php

/**
 * Define o modelo MSYSTemplates
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class MSYSTemplates extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "MSYStemplates";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getMSYSTemplatesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$MSYStemplates = new MSYSTemplates();
		return $MSYStemplates->getMSYSTemplates($queries, $page, $maxpage);
	}
	
	public function getMSYSTemplates($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " PREFIX_T1.id = $id ");
		
		$idlicenca = (isset($queries['idlicenca'])) ? (int)$queries['idlicenca'] : false;
		if ($idlicenca) array_push($where, " PREFIX_T1.idlicenca = $idlicenca ");
		
$DB_ATTR_FILTERS;

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "PREFIX_T1.*"; 
$TABLE_ADDRESS_FIELDS;
		
		if ($total) $fields = "COUNT(PREFIX_T1.id) as total";
		
		
		$ordem = "ORDER BY PREFIX_T1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM MSYStemplates PREFIX_T1
					TABLE_ADDRESS
					WHERE PREFIX_T1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getMSYSTemplateById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getMSYSTemplates($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getMSYSTemplateByIdHelper($id, $queries = array()) {
		$rows = new MSYSTemplates();
		return $rows->getMSYSTemplateById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return MSYSTemplates
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$idlicenca = (!isset($dados['idlicenca'])) ? 0 : (int)$dados['idlicenca'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		

		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
$GRAVA_DADOS_DB;		
				
		$row->save();
		
		return $row;
	}
	
	
}