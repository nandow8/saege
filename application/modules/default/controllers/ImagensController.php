<?php

/**
 * Controle da classe Imagens
 *
 * @author		Thales Gabriel		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2014 Soluções
 * @version     1.0
 */
class ImagensController extends Zend_Controller_Action {

	
	/**
	 * Action get
	 */
	/*
	public function getAction() {
		$id = (int)$this->_request->getParam("id");
		$w = (int)$this->_request->getParam("w");
		$h = (int)$this->_request->getParam("h");
		$indisponivel = $this->_request->getParam("i");
		
		$imagens = new Imagens(); 
		$imagem = $imagens->downloadImagem($id, $this->getRequest()->getParam('i'));
		
		$sourceFilename = $imagem['sourceFilename'];
		$imgInfo = @getimagesize($sourceFilename);
		if (!$imgInfo) {
			$imagem = $imagens->downloadImagem(0, $this->getRequest()->getParam('i'));
			$sourceFilename = $imagem['sourceFilename'];
			$imgInfo = @getimagesize($sourceFilename);			
		}

		$width = $imgInfo[0];	
		$height = $imgInfo[1];			
		
		$image = new SimpleImage();
		$image->load($sourceFilename);
		
		if (($w>0) || ($h>0)) {
			$toWidth = false;
			$toHeight = false;
			if ($w==0) $toHeight =  true;
			elseif ($h==0) $toWidth = true;
			else {
				if ($width>$height) $toWidth = true;
				else $toHeight =  true;
			}
			
			if (($toWidth) ) {
				if ($w<$width) {
					$image->resizeToWidth($w);
					$nh = $image->getHeight();
					if (($h>0) && ($nh>$h)) $image->resizeToHeight($h);
				}
			}
			if (($toHeight)) {
				if ($h<$height) {
					$image->resizeToHeight($h);	
					$nw = $image->getWidth();
					if (($w>0) && ($nw>$w)) $image->resizeToWidth($w);
				}
			}
		}
		
		$this->_helper->layout->disableLayout();
		header('content-type: image');
   		//header('FILENAME="'.$imagem['filename'].'"', TRUE);
   		header("Content-Transfer-Encoding: binary");	
		$image->output();
	}	*/
	
	
	public function getAction() {
		$id = (int)$this->_request->getParam("id");
		$w = (int)$this->_request->getParam("w");
		$h = (int)$this->_request->getParam("h");
		$marcadagua = $this->_request->getParam("marcadagua");
		$indisponivel = $this->_request->getParam("i");
		
		$imagens = new Imagens(); 
		$imagem = $imagens->downloadImagem($id, $this->getRequest()->getParam('i'));
		//var_dump($marcadagua); die('aqui');
		if((isset($marcadagua)) && ($marcadagua=="true")):
			$session = new Zend_Session_Namespace('admin_mn');
			if (!isset($session->usuario)) return false;
			$usuario = unserialize($session->usuario);
			
			/**/
			$filename = $imagem['sourceFilename'];
			$im = new SimpleImage();
			$im->load($filename);
			
			$white = $this->getRequest()->getParam('w');
			$text = $usuario['nomerazao'] . ' ' . $usuario['sobrenomefantasia'] . ' - ID: ' . $usuario['id'];
			
			$fontesize=30;
			
			$font = BASE_PATH . '/public/default/imagens/arial.ttf';
			
			$_white = imagettfbbox ($fontesize, 0, $font, $white);
			$_white = $_white[0]+$_white[2]; 		
			
			
			//imagettftext($im->image, 50, 0, x, y, 0x000000, $font, $white);
			imagettftext($im->image, 30, 0, 50, 40, 0x000000, $font, $text);	
			
			$this->_helper->layout->disableLayout();
			
			$resize_w = (int)$this->getRequest()->getParam("rw");
			if ($resize_w>0) {
				$im->resizeToWidth($resize_w);
			}
			
			header('content-type: image');
	   		header("Content-Transfer-Encoding: binary");	
			$im->output();
			die();
		endif;
		$this->_helper->layout->disableLayout();
		header('content-type: image');
   		//header('FILENAME="'.$imagem['filename'].'"', TRUE);
   		header("Content-Transfer-Encoding: binary");		
		echo file_get_contents($imagem['sourceFilename']);
		
		die();
		
		var_dump($imagem);
		die();
		
		$sourceFilename = $imagem['sourceFilename'];
		$imgInfo = @getimagesize($sourceFilename);
		if (!$imgInfo) {
			$imagem = $imagens->downloadImagem(0, $this->getRequest()->getParam('i'));
			$sourceFilename = $imagem['sourceFilename'];
			$imgInfo = @getimagesize($sourceFilename);			
		}

		$width = $imgInfo[0];	
		$height = $imgInfo[1];			
		
		$image = new SimpleImage();
		$image->load($sourceFilename);
		
		if (($w>0) || ($h>0)) {
			$toWidth = false;
			$toHeight = false;
			if ($w==0) $toHeight =  true;
			elseif ($h==0) $toWidth = true;
			else {
				if ($width>$height) $toWidth = true;
				else $toHeight =  true;
			}
			
			if (($toWidth) ) {
				if ($w<$width) {
					$image->resizeToWidth($w);
					$nh = $image->getHeight();
					if (($h>0) && ($nh>$h)) $image->resizeToHeight($h);
				}
			}
			if (($toHeight)) {
				if ($h<$height) {
					$image->resizeToHeight($h);	
					$nw = $image->getWidth();
					if (($w>0) && ($nw>$w)) $image->resizeToWidth($w);
				}
			}
		}
		
		$this->_helper->layout->disableLayout();
		header('content-type: image');
   		header('FILENAME="'.$imagem['filename'].'"', TRUE);
   		header("Content-Transfer-Encoding: binary");		
		$image->output();
	}	
}