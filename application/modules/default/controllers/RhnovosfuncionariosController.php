<?php

/**
 * Controle da classe rhnovosfuncionarios do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class RhnovosfuncionariosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Rhnovosfuncionario
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("rhnovosfuncionarios", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Rhnovosfuncionarios();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="rhnovosfuncionarios") $objs = new Rhnovosfuncionarios();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'rhnovosfuncionarios', 'name' => 'Solicitções de funcionário'),
			array('url' => null,'name' => 'Visualizar Solicitação')
		);
		
		$id = (int)$this->_request->getParam("id");
		$rhnovosfuncionarios = new Rhnovosfuncionarios();
		$rhnovosfuncionario = $rhnovosfuncionarios->getRhnovosfuncionarioById($id, array());
		
		if (!$rhnovosfuncionario) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $rhnovosfuncionario;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Solicitções de funcionário')
		);
		
		$ns = new Zend_Session_Namespace('default_rhnovosfuncionarios');
		$rhnovosfuncionarios = new Rhnovosfuncionarios();
		$queries = array();	
		$queries['origem'] = "Escola";
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect($this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["origem"]!="") $queries["origem"] = $this->view->post_var["origem"];
if ($this->view->post_var["idfuncionario"]!="") $queries["idfuncionario"] = $this->view->post_var["idfuncionario"];
if ($this->view->post_var["dias"]!="") $queries["dias"] = $this->view->post_var["dias"];
if ($this->view->post_var["datainicio_i"]!="") $queries["datainicio_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_i"]));
if ($this->view->post_var["datainicio_f"]!="") $queries["datainicio_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_f"]));
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $rhnovosfuncionarios->getRhnovosfuncionarios($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $rhnovosfuncionarios->getRhnovosfuncionarios($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de rhnovosfuncionarios
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'rhnovosfuncionarios', 'name' => 'Solicitções de funcionário'),
			array('url' => null,'name' => 'Editar Solicitação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$rhnovosfuncionarios = new Rhnovosfuncionarios();
		$rhnovosfuncionario = $rhnovosfuncionarios->getRhnovosfuncionarioById($id);
		
		if (!$rhnovosfuncionario) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $rhnovosfuncionario;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($rhnovosfuncionario);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação editado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de rhnovosfuncionarios 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'rhnovosfuncionarios', 'name' => 'Solicitções de funcionário'),
			array('url' => null,'name' => 'Adicionar Solicitação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação adicionado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idrhnovosfuncionario
     */    
    private function preForm($idrhnovosfuncionario = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_rhnovosfuncionario = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$origem = trim($this->getRequest()->getPost("origem"));
$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');;
$idfuncionario = (int)trim($this->getRequest()->getPost("idfuncionario"));
$dias = trim($this->getRequest()->getPost("dias"));
$datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
$datafim = Mn_Util::stringToTime($this->getRequest()->getPost("datafim"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$origem) array_push($erros, "Informe a Origem.");
if (0==$idescola) array_push($erros, "Informe a Escola.");
if (0==$idfuncionario) array_push($erros, "Informe a Funcionário.");
if (""==$datainicio) array_push($erros, "Informe a Data de inicio.");
if (""==$datafim) array_push($erros, "Informe a Data final.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$rhnovosfuncionarios = new Rhnovosfuncionarios();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["origem"] = $origem;
$dados["idescola"] = $idescola;
$dados["idfuncionario"] = $idfuncionario;
$dados["dias"] = $dias;
$dados["datainicio"] = date("Y-m-d", $datainicio);
$dados["datafim"] = date("Y-m-d", $datafim);
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $rhnovosfuncionarios->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}