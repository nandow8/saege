<?php

class RegistrossaidasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacotesaida
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("registrossaidas", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}	
	

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Controlealunos_Registrossaidas();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Escolasusuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Saída excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="registrossaidas") $objs = new Controlealunos_Registrossaidas();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Escolasusuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Controlealunos_Registrossaidas();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Controlealunos_Registrossaidas();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Saídas')
		);
		
		
		$ns = new Zend_Session_Namespace('default_registrossaidas');
		$saidas = new Controlealunos_Registrossaidas();
		$queries = array();	
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
			if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
			if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
    		
    	}				
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $saidas->getRegistrossaidas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $saidas->getRegistrossaidas($queries, $paginaAtual, $maxpp);	
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'registrossaidas',	'name' => 'Saídas'),
			array('url' => null,'name' => 'Visualizar saída')
		);	
		//$this->_usuario['idescola']		
		$id = (int)$this->_request->getParam("id");
		$saidas = new Controlealunos_Registrossaidas();
		$saida = $saidas->getRegistrosaidaById($id);
		
		if (!$saida) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $saida;
		$this->preForm();
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de saida
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'registrossaidas',	'name' => 'Saídas'),
			array('url' => null,'name' => 'Editar saída')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$saidas = new Controlealunos_Registrossaidas();
		$saida = $saidas->getRegistrosaidaById($id);
		//$this->_usuario['idescola']
		if (!$saida) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $saida;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($saida);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Saída editada com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de saidas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'registrossaidas',	'name' => 'Saídas'),
			array('url' => null,'name' => 'Adicionar Saída')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Saída adicionada com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    

    
	public function setdepartamentosescolasAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
    	
		$rows = new Departamentos();
    	$this->view->rows = $rows->getDepartamentos(array('status'=>'Ativo', 'idescola'=>$idescola)); 
	}
    
    /**
     * Atribui valores ao view
     * @param int $identrada
     */    
    private function preForm($identrada = 0) {
    	
    	$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	
		$rows = new Departamentosescolas();
    	$this->view->departamentosescolas = $rows->getDepartamentosescolas(array('status'=>'Ativo', 'idescola'=>$idescola)); 
    	
    	$entradas = new Almoxarifado_Entradasitens();
    	$this->view->entradas = $entradas->getEntradasitens(array('status'=>'Ativo', 'idescola'=>$idescola)); 
    	//var_dump($this->view->entradas); die();
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idaluno = strip_tags((int)$this->getRequest()->getPost("idaluno"));
		$idescola = strip_tags((int)$this->getRequest()->getPost("idescola"));
		$aluno = strip_tags(trim($this->getRequest()->getPost("aluno")));	
		$titulo = strip_tags(trim($this->getRequest()->getPost("titulo")));
		$descricoes = strip_tags(trim($this->getRequest()->getPost("descricoes")));	
		$status = strip_tags(trim($this->getRequest()->getPost("status")));
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		
		$erros = array();

		if (""==$titulo) array_push($erros, 'Preencha o campo ENTRADA.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$saidas = new Controlealunos_Registrossaidas();
		//$row = $saidas->fetchRow("excluido='nao' AND saida='$saida' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma ESCOLA com esse NOME.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {		
			if($id<=0) $confirmaaprovacao = "Não";
			
			$dados = array();
			$dados['id'] = $id;
			$dados['titulo'] = $titulo;
			$dados['descricoes'] = $descricoes;
			
			$dados['idescola'] = $idescola;
			$dados['idaluno'] = $idaluno;	
			
			$dados['aluno'] = $aluno;
			$dados['data'] = date('Y-m-d',$data);
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Escolasusuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			
			
			$row = $saidas->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }
}