<?php

/**
 * Controle da classe eventosescolas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class EventosescolasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Eventoescola
	 */
	protected $_cadastro = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("eventosescolas", $this->_request->getActionName());	
		
		$this->_cadastro = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Eventosescolas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_cadastro['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Evento excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="eventosescolas") $objs = new Eventosescolas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_cadastro['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'eventosescolas', 'name' => 'Eventos'),
			array('url' => null,'name' => 'Visualizar Evento')
		);
		
		$id = (int)$this->_request->getParam("id");
		$eventosescolas = new Eventosescolas();
		$eventoescola = $eventosescolas->getEventoescolaById($id, array());
		
		if (!$eventoescola) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $eventoescola;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Eventos')
		);
		
		$ns = new Zend_Session_Namespace('default_eventosescolas');
		$eventosescolas = new Eventosescolas();
		$queries = array();	
		$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		if(Escolasusuarios::getUsuario('professor')=="Sim"){
			$queries['idescolausuarios'] = Escolasusuarios::getUsuario('id');		
		}else{
			$queries['tipo'] = "Público";		
		}		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect($this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["datainicio_i"]!="") $queries["datainicio_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_i"]));
if ($this->view->post_var["datainicio_f"]!="") $queries["datainicio_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datainicio_f"]));
if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $eventosescolas->getEventosescolas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $eventosescolas->getEventosescolas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de eventosescolas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'eventosescolas', 'name' => 'Eventos'),
			array('url' => null,'name' => 'Editar Evento')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$eventosescolas = new Eventosescolas();
		$eventoescola = $eventosescolas->getEventoescolaById($id);
		
		if (!$eventoescola) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $eventoescola;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($eventoescola);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Evento editado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de eventosescolas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'eventosescolas', 'name' => 'Eventos'),
			array('url' => null,'name' => 'Adicionar Evento')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Evento adicionado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $ideventoescola
     */    
    private function preForm($ideventoescola = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_eventoescola = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
$idescolausuarios = (int)$this->_cadastro['id'];
$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
$datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
$horainicio = trim($this->getRequest()->getPost("horainicio"));
$datafim = Mn_Util::stringToTime($this->getRequest()->getPost("datafim"));
$horafim = trim($this->getRequest()->getPost("horafim"));
$hora = trim($this->getRequest()->getPost("hora"));
$titulo = trim($this->getRequest()->getPost("titulo"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$observacoes = trim($this->getRequest()->getPost("observacoes"));
$tipo = trim($this->getRequest()->getPost("tipo"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
if (0==$idescolausuarios) array_push($erros, "Informe a Usuário da escola.");

if (""==$datainicio) array_push($erros, "Informe a Data início.");
if (""==$horainicio) array_push($erros, "Informe a Hora Início.");
if (""==$datafim) array_push($erros, "Informe a Data fim.");
if (""==$horafim) array_push($erros, "Informe a Hora Fim.");

if (""==$titulo) array_push($erros, "Informe a Título.");
if (""==$descricoes) array_push($erros, "Informe a Descrições.");
if (""==$tipo) array_push($erros, "Informe a Tipo.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$eventosescolas = new Eventosescolas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
$dados["idescolausuarios"] = $idescolausuarios;
$dados["data"] = date("Y-m-d", $data);
$dados["datainicio"] = date("Y-m-d", $datainicio);
$dados["horainicio"] = $horainicio;
$dados["datafim"] = date("Y-m-d", $datafim);
$dados["horafim"] = $horafim;
$dados["hora"] = $hora;
$dados["titulo"] = $titulo;
$dados["descricoes"] = $descricoes;
$dados["observacoes"] = $observacoes;
$dados["tipo"] = $tipo;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_cadastro['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $eventosescolas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}