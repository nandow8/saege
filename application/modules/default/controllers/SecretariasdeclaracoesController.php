<?php

/**
 * Controle da index (Admin)
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class SecretariasdeclaracoesController extends Zend_Controller_Action {
	
	/**
     * Verificação de Permissao de Acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("secretariasdeclaracoes", $this->_request->getActionName());	
		
		$this->_cadastro = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		$queries = array();	
		$ultimo = $secretariasdeclaracoes->getUltimoSecretariasdeclaracao($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;	
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a xml_set_unparsed_entity_decl_handler(parser, handler)
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Secretariasdeclaracoes();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Escolasusuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Material excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="secretariasdeclaracoes") $objs = new Secretariasdeclaracoes();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Escolasusuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Secretariasdeclaracoes();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Secretariasdeclaracoes();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Controle Diário')
		);
		
		
		$ns = new Zend_Session_Namespace('default_Secretariasdeclaracoes');
		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		$queries = array();	
				
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
			if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
			if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["idaluno"]!="") $queries["idaluno"] = $this->view->post_var["idaluno"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
			    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $secretariasdeclaracoes->getSecretariasdeclaracoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $secretariasdeclaracoes->getSecretariasdeclaracoes($queries, $paginaAtual, $maxpp);
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'Secretariasdeclaracoes',	'name' => 'Controle Diário'),
			array('url' => null,'name' => 'Visualizar Controle Diário')
		);	
		//$this->_usuario['idescola']		
		$id = (int)$this->_request->getParam("id");
		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		$secretariasdeclaracoes = $secretariasdeclaracoes->getSecretariasdeclaracaoById($id);
		
		if (!$secretariasdeclaracoes) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $secretariasdeclaracoes;
		$this->preForm();
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de saida
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasdeclaracoes',	'name' => 'Declarações'),
			array('url' => null,'name' => 'Editar Declaração')
		);	
			
		$id = (int)$this->_request->getParam("id");
		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		$secretariasdeclaracao = $secretariasdeclaracoes->getSecretariasdeclaracaoById($id);
		//$this->_usuario['idescola']

		if (!$secretariasdeclaracao) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $secretariasdeclaracao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($secretariasdeclaracao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Declaração editada com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de saidas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'secretariasdeclaracoes',	'name' => 'Declarações'),
			array('url' => null,'name' => 'Adicionar Declaração')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Declaração adicionada com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
    
    /**
     * Atribui valores ao view
     * @param int $identrada
     */    
    private function preForm($identrada = 0) {
    	
    	$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
		$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
		$idusuariologado = (int)trim($this->getRequest()->getPost("idusuariologado"));
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
		$idsecretaria = (int)trim($this->getRequest()->getPost("idsecretaria"));
		$tipo = trim($this->getRequest()->getPost("tipo"));
		$serie = trim($this->getRequest()->getPost("serie"));
		$horainicio = trim($this->getRequest()->getPost("horainicio"));
		$horafim = trim($this->getRequest()->getPost("horafim"));
		$ano = trim($this->getRequest()->getPost("ano"));
		$considerado = trim($this->getRequest()->getPost("considerado"));
		$proximoano = trim($this->getRequest()->getPost("proximoano"));
		$solicitacaovaga = trim($this->getRequest()->getPost("solicitacaovaga"));
		$validapor = trim($this->getRequest()->getPost("validapor"));
		$vagaano = trim($this->getRequest()->getPost("vagaano"));
		$vagadias = trim($this->getRequest()->getPost("vagadias"));
		$anoregulamentado = trim($this->getRequest()->getPost("anoregulamentado"));
		$tituloregulamentado = trim($this->getRequest()->getPost("tituloregulamentado"));
		$proximoanoregulamentado = trim($this->getRequest()->getPost("proximoanoregulamentado"));
		$diasregulamentado = trim($this->getRequest()->getPost("diasregulamentado"));
		$anosegundoitem = trim($this->getRequest()->getPost("anosegundoitem"));
		$anoterceiroitem = trim($this->getRequest()->getPost("anoterceiroitem"));
		$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
		$iddiretora = (int)trim($this->getRequest()->getPost("iddiretora"));
		$status = trim($this->getRequest()->getPost("status1"));
		$erros = array();

		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		//$row = $saidas->fetchRow("excluido='nao' AND saida='$saida' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma ESCOLA com esse NOME.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {		
			$dados = array();
			$dados['id'] = $id;
			$dados['sequencial'] = $sequencial;
			$dados['idusuariologado'] = $idusuariologado;
			$dados['idescola'] = $idescola;
			$dados['datalancamento'] = date('Y-m-d', $datalancamento);
			$dados['idsecretaria'] = $idsecretaria;
			$dados['tipo'] = $tipo;
			$dados['serie'] = $serie;
			$dados['horainicio'] = $horainicio;
			$dados['horafim'] = $horafim;
			$dados['anosegundoitem'] = $anosegundoitem;
			$dados['ano'] = $ano;
			$dados['considerado'] = $considerado;
			$dados['anoterceiroitem'] = $anoterceiroitem;
			$dados['proximoano'] = $proximoano;
			$dados['solicitacaovaga'] = $solicitacaovaga;
			$dados['validapor'] = $validapor;
			$dados['vagaano'] = $vagaano;
			$dados['vagadias'] = $vagadias;
			$dados['anoregulamentado'] = $anoregulamentado;
			$dados['tituloregulamentado'] = $tituloregulamentado;
			$dados['proximoanoregulamentado'] = $proximoanoregulamentado;
			$dados['diasregulamentado'] = $diasregulamentado;
			$dados['idaluno'] = $idaluno;
			$dados['iddiretora'] = $iddiretora;
			$dados['horalancamento'] = $horalancamento;
			$dados['datacriacao'] =date('Y-m-d G:i:s', $datacriacao);
			$dados['descricoes'] = $descricoes;
			$dados['assunto'] = $assunto;
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Escolasusuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			$row = $secretariasdeclaracoes->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }

    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }  
}

