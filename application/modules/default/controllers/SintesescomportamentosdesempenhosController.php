<?php

/**
 * Controle da classe sintesescomportamentosdesempenhos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class SintesescomportamentosdesempenhosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Sintesescomportamentosdesempenhos
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("sintesescomportamentosdesempenhos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Sintesescomportamentosdesempenhos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Síntese de comportamento e desempenho excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="sintesescomportamentosdesempenhos") $objs = new Sintesescomportamentosdesempenhos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'sintesescomportamentosdesempenhos', 'name' => 'Sínteses de Comportamentos e Desempenhos'),
			array('url' => null,'name' => 'Visualizar Sínteses de Comportamentos e Desempenhos')
		);
		
		$id = (int)$this->_request->getParam("id");
		$sintesescomportamentosdesempenhos = new Sintesescomportamentosdesempenhos();
		$sintesescomportamentosdesempenhos = $sintesescomportamentosdesempenhos->getSintesescomportamentosdesempenhosById($id, array());
		
		if (!$sintesescomportamentosdesempenhos) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $sintesescomportamentosdesempenhos;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Sínteses de Comportamentos e Desempenhos')
		);
		
		$ns = new Zend_Session_Namespace('default_sintesescomportamentosdesempenhos');
		$sintesescomportamentosdesempenhos = new Sintesescomportamentosdesempenhos();
		$queries = array();	
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('admin' . '/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
    		if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $sintesescomportamentosdesempenhos->getSintesescomportamentosdesempenhos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $sintesescomportamentosdesempenhos->getSintesescomportamentosdesempenhos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de sintesescomportamentosdesempenhos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'sintesescomportamentosdesempenhos', 'name' => 'Sínteses de Comportamentos e Desempenhos'),
			array('url' => null,'name' => 'Editar Sínteses de Comportamentos e Desempenhos')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$sintesescomportamentosdesempenhos = new Sintesescomportamentosdesempenhos();
		$sintesescomportamentosdesempenhos = $sintesescomportamentosdesempenhos->getSintesescomportamentosdesempenhosById($id);
		
		if (!$sintesescomportamentosdesempenhos) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $sintesescomportamentosdesempenhos;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($sintesescomportamentosdesempenhos);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Sínteses de Comportamentos e Desempenhos editada com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de sintesescomportamentosdesempenhos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'sintesescomportamentosdesempenhos', 'name' => 'Sínteses de Comportamentos e Desempenhos'),
			array('url' => null,'name' => 'Adicionar Sínteses de Comportamentos e Desempenhos')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Sínteses de Comportamentos e Desempenhos adicionada com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idsintesescomportamentosdesempenhos
     */    
    private function preForm($idsintesescomportamentosdesempenhos = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_sintesescomportamentosdesempenhos = false) {

		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		// die(var_dump($this->view->post_var));
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$data = Mn_Util::stringToTime(trim($this->getRequest()->getPost("data")));
		$idescola = trim($this->getRequest()->getPost("idescola"));
		if($idescola==0) $idescola = Escolasusuarios::getUsuario('id');
		$idserie = trim($this->getRequest()->getPost("idserie"));
		$idturma = trim($this->getRequest()->getPost("idturma"));
		$idprofessor = trim($this->getRequest()->getPost("idprofessor"));
		$iddiretor = trim($this->getRequest()->getPost("iddiretor"));
		$idcoordenador = trim($this->getRequest()->getPost("idcoordenador"));
		$periodo = trim($this->getRequest()->getPost("periodo"));
		$status = trim($this->getRequest()->getPost("status1"));

		$idaluno = $this->getRequest()->getPost("idaluno");
		$concentracao = $this->getRequest()->getPost("concentracao");
		$aquicisao = $this->getRequest()->getPost("aquicisao");
		$raciocinio = $this->getRequest()->getPost("raciocinio");
		$motivacao = $this->getRequest()->getPost("motivacao");
		$comportamento = $this->getRequest()->getPost("comportamento");
		$relacaointerpessoal = $this->getRequest()->getPost("relacaointerpessoal");
		$cooperacao = $this->getRequest()->getPost("cooperacao");
		$leitura = $this->getRequest()->getPost("leitura");
		$escrita = $this->getRequest()->getPost("escrita");
		$organizacao = $this->getRequest()->getPost("organizacao");
		$assiduidade = $this->getRequest()->getPost("assiduidade");
		$matematica = $this->getRequest()->getPost("matematica");
		$rendimento = $this->getRequest()->getPost("rendimento");
		$infocomplementar = $this->getRequest()->getPost("infocomplementar");

		$erros = array();
		
		if (""==$idescola) array_push($erros, "Informe a escola.");
		if (""==$status) array_push($erros, "Informe o status.");
		
		$sintesescomportamentosdesempenhos = new Sintesescomportamentosdesempenhos();
		$sintesecomportamentosdesempenhosrelacoes = new Sintesecomportamentosalunosrelacao();		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();

			$dados['id'] = $id;
			if ($id==0) $sequencial = $sintesescomportamentosdesempenhos->getSequencial();
			$dados["sequencial"] = $sequencial;
			$dados["data"] = date("Y-m-d", $data);
			$dados["idescola"] = $idescola;
			$dados["idprofessor"] = $idprofessor;
			$dados["iddiretor"] = $iddiretor;
			$dados["idcoordenador"] = $idcoordenador;
			$dados["idserie"] = $idserie;
			$dados["idturma"] = $idturma;
			$dados["periodo"] = $periodo;
			$dados["qtdalunos"] = $qtdalunos;
			$dados["status"] = $status;
			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];
			$dados['logdata'] = date('Y-m-d G:i:s');
		
			$row = $sintesescomportamentosdesempenhos->save($dados);

			$sintesecomportamentosdesempenhosrelacoes->setExcluido($row['id']);

			$i = 0;
			foreach ($idaluno as $key => $aluno) {
				$_dados = array();
				$_dados['idsintese'] = $row['id'];
				$_dados['idaluno'] = $aluno;
				$_dados['concentracao'] = $concentracao[$i];
				$_dados['aquicisao'] = $aquicisao[$i];
				$_dados['raciocinio'] = $raciocinio[$i];
				$_dados['motivacao'] = $motivacao[$i];
				$_dados['comportamento'] = $comportamento[$i];
				$_dados['relacaointerpessoal'] = $relacaointerpessoal[$i];
				$_dados['cooperacao'] = $cooperacao[$i];
				$_dados['leitura'] = $leitura[$i];
				$_dados['escrita'] = $escrita[$i];
				$_dados['matematica'] = $matematica[$i];
				$_dados['organizacao'] = $organizacao[$i];
				$_dados['assiduidade'] = $assiduidade[$i];
				$_dados['rendimento'] = $rendimento[$i];
				$_dados['infocomplementar'] = $infocomplementar[$i];
				$_dados['excluido'] = 'nao';
				
				$_row = $sintesecomportamentosdesempenhosrelacoes->save($_dados);

				$i++;
			}

			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}
		
		return "";    	
    }  

    public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries['idescola'] = $idescola;

		$rows = new Escolasseries();
		$this->view->rows = $rows->getEscolasseries($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setturmasAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		
		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculos($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0)) $this->view->rows = null;		
	}

    public function setcoordenadoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idsecretaria"] = $idescola;
		$queries["coordenador"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setdiretoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idsecretaria"] = $idescola;
		$queries["diretor"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setprofessoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idsecretaria"] = $idescola;
		$queries["professor"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setperiodoAction() {
		$this->_helper->layout->disableLayout();

		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		$queries['idescola'] = $idescola;

		$rows = new Escolasconfiguracoes();
		$this->view->rows = $rows->getEscolasconfiguracoes($queries);

		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$idserie = (int)$this->_request->getPost("idserie");
		$idvinculo = (int)$this->_request->getPost("idvinculo");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		$queries['idvinculo'] = $idvinculo;
		
		$rows = new Escolassalasatribuicoes();
		$this->view->rows = $rows->getSalasatribuicoes($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0) || ((int)$idvinculo <= 0)) $this->view->rows = null;		
	}    
}