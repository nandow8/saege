<?php

class EscolasusuariosperfisController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Usuario
	 */
	protected $_usuario = null;
	
	/**
     * Verifica a permissão de acessos, inicia o usuário e verifica mensagens de transação
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("escolasusuariosperfis", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}			
	}	
	
	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$usuarios = new Escolasusuarios();
		$livros = $usuarios->fetchAll("excluido='nao' AND idperfil=$id");
		
		if (sizeof($livros)>0) {
			$this->view->message = "O Perfil não pode ser excluído pois está sendo usado por ".sizeof($livros)." usuário(s)";
			$this->render("xml");
			return false;
		}
		
		$rows = new Escolasusuariosperfis();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Perfil excluído com sucesso.";
			die("OK");
		}
		
		die("Não encontrado!");
	}		

	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="escolasusuariosperfis") $objs = new Escolasusuariosperfis();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Usuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}
	
	/**
	 * 
	 * Lista dos perfis de acesso
	 */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => null,'name' => 'Perfil de Usuários das Escolas')
		);	
		if (Mn_Util::isAccess('perfis', 'adicionar'))
			array_push($this->view->bread_crumb, array('url' => 'escolasusuariosperfis/adicionar','name' => '(Adicionar Perfil)'));
		
		
		$this->preForm();
		$ns = new Zend_Session_Namespace('default_escolasusuarios');
		$queries = array();
        $queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');       
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ((isset($this->view->post_var['idescola'])) && ($this->view->post_var['idescola']!='')) $queries['idescola'] = (int)$this->view->post_var['idescola'];
    		if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		if ((isset($this->view->post_var['status1'])) && ($this->view->post_var['status1']!='')) $queries['status'] = $this->view->post_var['status1'];
    	}	
    	$perfis = new Escolasusuariosperfis();
    	
		//PAGINACAO
    	$maxpp = 15;
    	$this->view->maxpp = $maxpp;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $perfis->getperfis("", $queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $perfis->getperfis("", $queries, $paginaAtual, $maxpp);	
		
		
		
	}
	
	/**
	 * 
	 * Action de edição de perfil de acesso
	 */
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasusuariosperfis','name' => 'Perfil de Usuários das Escolas'),
			array('url' => null,'name' => 'Editar Perfil')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$perfis = new Escolasusuariosperfis();
		$perfil = $perfis->fetchRow("id=".$id." AND excluido='nao'");
		
		if (!$perfil) 
			$this->_redirect($this->getRequest()->getControllerName());	
		
		
		$this->view->post_var = $perfil->toArray();
		$this->preForm($id);
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost();
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Perfil editado com sucesso.";
			
			$this->_redirect('' . $this->getRequest()->getControllerName());	
		}		
		return true;		
    }  	
	
	/**
	 * 
	 * Action de adição de pefis de acesso. 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasusuariosperfis','name' => 'Perfil de Usuários das Escolas'),
			array('url' => null,'name' => 'Adicionar Perfil')
		);	
		
			
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost();
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Perfil adicionado com sucesso.";
			
			$this->_redirect('' . $this->getRequest()->getControllerName());	
		}
		return true;		
    }		
    
    /**
     * 
     * Atribui valores ao view
     * @param int $idperfil
     */
	private function preForm($idperfil = 0) {
		$perfis = new Escolasusuariosperfis();
		$this->view->limites = $perfis->getLimitesByIdperfil($idperfil);
		
                $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Escolasusuarios::getUsuario('id'), 'id');
                
		$escolas = new Escolas();
		$this->view->escolas = $escolas->getEscolas(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
	}	    
	
	/**
	 * 
	 * Valida e grava os dados do formulário
	 */
    private function getPost() {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST;
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$perfil = strip_tags(trim($this->getRequest()->getPost("perfil")));
		$status = strip_tags(trim($this->getRequest()->getPost("status1")));
		$multiplasescolas = strip_tags(trim($this->getRequest()->getPost("multiplasescolas")));
		$visualizar = $this->getRequest()->getPost("visualizar");
		$adicionar = $this->getRequest()->getPost("adicionar");
		$editar = $this->getRequest()->getPost("editar");
		$excluir = $this->getRequest()->getPost("excluir");
		
		$erros = array();
			
		if (""==$perfil) array_push($erros, 'Preencha o campo NOME DO PERFIL.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$perfis = new Escolasusuariosperfis();		
		$row = $perfis->fetchRow("excluido='nao' AND perfil='$perfil' AND idescola=".$idescola." AND id<>".$id);
		
		if($id!=22){
			if ($row) array_push($erros, 'Já existe perfil com esse NOME.');	
		}
		
			
		if (sizeof($erros)>0) return $erros; 
		
		if($id!=22){

			$dados = array(
				'id' => $id, 
				'idescola' => $idescola,
				'perfil' => $perfil, 
				'status' => $status,
				'origem' => 'default',
				'excluido' => 'nao',
				'logusuario' => $this->_usuario['id'],
				'logdata' => date('Y-m-d G:i:s'),
				'limites' => array(
					'visualizar' => $visualizar,
					'adicionar' => $adicionar,
					'editar' => $editar,
					'excluir' => $excluir,
				)
			);	
		}else{

			$dados = array(
				'id' => $id, 
				'perfil' => $perfil, 
				'status' => $status,
				'excluido' => 'nao',
				'logusuario' => $this->_usuario['id'],
				'logdata' => date('Y-m-d G:i:s'),
				'limites' => array(
					'visualizar' => $visualizar,
					'adicionar' => $adicionar,
					'editar' => $editar,
					'excluir' => $excluir,
				)
			);
			//var_dump($dados); die();
		}

		$_row = $perfis->save($dados);
		
		return "";    	
    }		
	
}