<?php

/**
 * Controle da classe escolasseries do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class EscolasseriesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Escolaserie
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("escolasseries", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Escolasseries();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Série excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="escolasseries") $objs = new Escolasseries();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasseries', 'name' => 'Séries'),
			array('url' => null,'name' => 'Visualizar Série')
		);
		
		$id = (int)$this->_request->getParam("id");
		$escolasseries = new Escolasseries();
		$escolaserie = $escolasseries->getEscolaserieById($id, array());
		
		if (!$escolaserie) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolaserie;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Séries')
		);
		
		$ns = new Zend_Session_Namespace('default_escolasseries');
		$escolasseries = new Escolasseries();
		$queries = array();	
		//$queries['idsecretaria'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria'); 
		$queries['idescola'] = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect($this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
if ($this->view->post_var["idserietipo"]!="") $queries["idserietipo"] = $this->view->post_var["idserietipo"];
if ($this->view->post_var["serie"]!="") $queries["serie"] = $this->view->post_var["serie"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $escolasseries->getEscolasseries($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $escolasseries->getEscolasseries($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de escolasseries
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasseries', 'name' => 'Séries'),
			array('url' => null,'name' => 'Editar Série')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$escolasseries = new Escolasseries();
		$escolaserie = $escolasseries->getEscolaserieById($id);
		
		if (!$escolaserie) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolaserie;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($escolaserie);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Série editado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de escolasseries 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasseries', 'name' => 'Séries'),
			array('url' => null,'name' => 'Adicionar Série')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Série adicionado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function setmateriasAction() {
		$this->_helper->layout->disableLayout();
		    	
		$queries = array();
		$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

    	if((int)$idescola <=0) return $this->view->rows = array();
		$materias = new Controlealunosmaterias();
    	$this->view->materias = $materias->getControlealunosmaterias(array('left_materias'=>true)); 

		$escolasmaterias = new Escolasmaterias();
		//$this->view->materias = $escolasmaterias->getEscolasmaterias(array('status'=>'Ativo'));
	}	

    /**
     * Atribui valores ao view
     * @param int $idescolaserie
     */    
    private function preForm($idescolaserie = 0) {
		$queries = array();
		$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

    	if((int)$idescola <=0) return $this->view->rows = array();
		$materias = new Controlealunosmaterias();
    	$this->view->materias = $materias->getControlealunosmaterias(array('left_materias'=>true)); 
		
		$escolasmaterias = new Escolasmaterias();
		//$this->view->materias = $escolasmaterias->getEscolasmaterias(array('status'=>'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_escolaserie = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria'); 
$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 
$idserietipo = (int)trim($this->getRequest()->getPost("idserietipo"));
$serie = trim($this->getRequest()->getPost("serie"));
$tipoensino = trim($this->getRequest()->getPost("tipoensino"));
$quantidadesaulas = trim($this->getRequest()->getPost("quantidadesaulas"));
		$idsmaterias = $this->getRequest()->getPost("idsmaterias");   
		if(isset($idsmaterias[0])) {
			$idsmaterias = implode(',',$idsmaterias);
		}else{
			$idsmaterias = array();
			$idsmaterias = implode(',',$idsmaterias);
		}
$ano = trim($this->getRequest()->getPost("ano"));
$termo = trim($this->getRequest()->getPost("termo"));

$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escolas.");
if (0==$idserietipo) array_push($erros, "Informe a Tipos.");
if (""==$serie) array_push($erros, "Informe a Série.");
if (""==$ano) array_push($erros, "Informe a Ano.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$escolasseries = new Escolasseries();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria'); 
$dados["idescola"] = $idescola;
$dados["idserietipo"] = $idserietipo;
$dados["serie"] = $serie;
$dados["tipoensino"] = $tipoensino;
$dados["quantidadesaulas"] = $quantidadesaulas;
$dados["ano"] = $ano;
$dados["termo"] = $termo;
$dados["idsmaterias"] = $idsmaterias;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $escolasseries->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}