<?php

/**
 * Controle da classe quadrohorarios do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class QuadrohorariosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Quadrohorario
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("quadrohorarios", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Quadrohorarios();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Quadro de horário excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="quadrohorarios") $objs = new Quadrohorarios();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'quadrohorarios', 'name' => 'Quadro de horários'),
			array('url' => null,'name' => 'Visualizar Quadro de horário')
		);
		
		$id = (int)$this->_request->getParam("id");
		$quadrohorarios = new Quadrohorarios();
		$quadrohorario = $quadrohorarios->getQuadrohorarioById($id, array());
		
		if (!$quadrohorario) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $quadrohorario;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Quadro de horários')
		);
		
		$ns = new Zend_Session_Namespace('default_escolasvinculosprofessores');
		$escolasvinculos = new Escolasvinculos();
		$queries = array();	
		//$queries['idsecretaria'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria'); 
		$queries['idescola'] = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 	
		$queries['order'] = "GROUP BY ev1.idserie ASC";
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect($this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["idserie"]!="") $queries["idserie"] = $this->view->post_var["idserie"];
if ($this->view->post_var["idturma"]!="") $queries["idturma"] = $this->view->post_var["idturma"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $escolasvinculos->getEscolasvinculos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $escolasvinculos->getEscolasvinculos($queries, $paginaAtual, $maxpp);
	}
	
	/**
	 * 
	 * Action de edição de quadrohorarios
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'quadrohorarios', 'name' => 'Quadro de horários'),
			array('url' => null,'name' => 'Editar Quadro de horário')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$escolasvinculos = new Escolasvinculos();
		$escolavinculo = $escolasvinculos->getEscolavinculoById($id);
		
		if (!$escolavinculo) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolavinculo;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($escolavinculo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Vínculo editado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de quadrohorarios 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'quadrohorarios', 'name' => 'Quadro de horários'),
			array('url' => null,'name' => 'Adicionar Quadro de horário')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Quadro de horário adicionado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idquadrohorario
     */    
    private function preForm($idquadrohorario = 0) {
    	$this->view->escolas = Escolas::getEscolasHelper(array('id'=>Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id') , 'status' => 'Ativo', 'idsecretaria' => Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria')));

    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_quadrohorario = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria'); 
$idescolavinculo = (int)trim($this->getRequest()->getPost("idescolavinculo"));
$diasemana = trim($this->getRequest()->getPost("diasemana"));
$periodo = trim($this->getRequest()->getPost("periodo"));
$idmateria = (int)trim($this->getRequest()->getPost("idmateria"));
$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idsecretaria) array_push($erros, "Informe a Secretaria.");
if (""==$diasemana) array_push($erros, "Informe a Dias da semana.");
if (""==$periodo) array_push($erros, "Informe a Aula.");
if (0==$idmateria) array_push($erros, "Informe a idmateria.");
if (0==$idprofessor) array_push($erros, "Informe a Professor.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$quadrohorarios = new Quadrohorarios();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = $idsecretaria;
$dados["idescolavinculo"] = $idescolavinculo;
$dados["diasemana"] = $diasemana;
$dados["periodo"] = $periodo;
$dados["idmateria"] = $idmateria;
$dados["idprofessor"] = $idprofessor;
$dados["quantidadesaulas"] = $quantidadesaulas;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $quadrohorarios->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}