<?php

/**
 * Controle da index (Admin)
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class MapaacompanhamentoclassesController extends Zend_Controller_Action {
	
	/**
     * Verificação de Permissao de Acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("mapaacompanhamentoclasses", $this->_request->getActionName());	
		
		$this->_cadastro = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}

		
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a xml_set_unparsed_entity_decl_handler(parser, handler)
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Mapaacompanhamentoclasse();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Escolasusuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Material excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="mapaacompanhamentoclasses") $objs = new Mapaacompanhamentoclasse();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Escolasusuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Mapaacompanhamentoclasse();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Mapaacompanhamentoclasse();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Mapaacompanhamentoclasse')
		);
		
		
		$ns = new Zend_Session_Namespace('default_mapaacompanhamentoclasses');
		$mapaacompanhamentoclasses = new Mapaacompanhamentoclasse();
		$queries = array();	
		 $queries['iddepartamento'] = 15;		
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
    		if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];

			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
			
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
			    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $mapaacompanhamentoclasses->getMapaacompanhamentoclasse($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $mapaacompanhamentoclasses->getMapaacompanhamentoclasse($queries, $paginaAtual, $maxpp);
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'mapaacompanhamentoclasses',	'name' => 'Mapa de acompanhamento de classe e sondagem'),
			array('url' => null,'name' => 'Visualizar Mapa de acompanhamento de classe e sondagem')
		);	
		//$this->_usuario['idescola']		
		$id = (int)$this->_request->getParam("id");
		$mapaacompanhamentoclasses = new Mapaacompanhamentoclasse();
		$mapaacompanhamentoclasse = $mapaacompanhamentoclasses->getMapaacompanhamentoclasseById($id);
		
		if (!$mapaacompanhamentoclasse) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $mapaacompanhamentoclasse;
		$this->preForm();
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de saida
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'mapaacompanhamentoclasses',	'name' => 'Mapa de acompanhamento de classe e sondagem'),
			array('url' => null,'name' => 'Editar Mapa de acompanhamento de classe e sondagem')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$mapaacompanhamentoclasses = new Mapaacompanhamentoclasse();
		$mapaacompanhamentoclasse = $mapaacompanhamentoclasses->getMapaacompanhamentoclasseById($id);
		//$this->_usuario['idescola']
		if (!$mapaacompanhamentoclasses) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $mapaacompanhamentoclasse;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($mapaacompanhamentoclasse);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mapa de acompanhamento de classe e sondagem editada com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de saidas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'mapaacompanhamentoclasses',	'name' => 'Mapa de acompanhamento de classe e sondagem'),
			array('url' => null,'name' => 'Adicionar Mapa de acompanhamento de classe e sondagem')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Mapa de acompanhamento de classe e sondagem adicionada com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
    
    /**
     * Atribui valores ao view
     * @param int $identrada
     */    
    private function preForm($identrada = 0) {
    	
    	$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idusuariologado = trim($this->getRequest()->getPost("idusuariologado"));
		$sequencial = trim($this->getRequest()->getPost("sequencial"));
		$datalancamento = Mn_Util::stringToTime(trim($this->getRequest()->getPost("datalancamento")));
		$horalancamento = trim($this->getRequest()->getPost("horalancamento"));
		$idescola = trim($this->getRequest()->getPost("idescola"));
		$idserie = trim($this->getRequest()->getPost("idserie"));
		$idturma = trim($this->getRequest()->getPost("idturma"));
		$idperiodo = trim($this->getRequest()->getPost("idperiodo"));
		$iddiretor = trim($this->getRequest()->getPost("iddiretor"));
		$idcoordenador = trim($this->getRequest()->getPost("idcoordenador"));
		$periodonotas = trim($this->getRequest()->getPost("periodonotas"));
		$idaluno = $this->getRequest()->getPost("idaluno");
		$idhipotese = $this->getRequest()->getPost("idhipotese");
		$idescrita = $this->getRequest()->getPost("idescrita");
		$idleitura = $this->getRequest()->getPost("idleitura");
		$inicial = $this->getRequest()->getPost("inicial");
		$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$sequencial) array_push($erros, "Informe o sequencial.");
		if (""==$idserie) array_push($erros, "Informe a série.");
		if (""==$idturma) array_push($erros, "Informe a turma.");
		if (""==$idperiodo) array_push($erros, "Informe o período.");
		
		$mapaacompanhamentoclasses = new Mapaacompanhamentoclasse();
		//$row = $saidas->fetchRow("excluido='nao' AND saida='$saida' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma ESCOLA com esse NOME.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {		
			$dados = array();
			$dados['id'] = $id;
			$dados["idusuariologado"] = $idusuariologado;
			$dados["sequencial"] = $sequencial;
			$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			$dados["horalancamento"] = $horalancamento;
			$dados["idescola"] = $idescola;
			$dados["idserie"] = $idserie;
			$dados["idturma"] = $idturma;
			$dados["idperiodo"] = $idperiodo;
			$dados["iddiretor"] = $iddiretor;
			$dados["idcoordenador"] = $idcoordenador;
			$dados["periodonotas"] = $periodonotas;
			$dados["status"] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Escolasusuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');

			$i=0;
			foreach ($idaluno as $key => $aluno) {
				$_dados = array();
				$_dados['idmapa'] = $row['id'];
				$_dados['idaluno'] = $aluno;
				$_dados['idtipoleitura'] = $idleitura[$i];
				$_dados['idtipoescrita'] = $idescrita[$i];
				$_dados['idtipohipotese'] = $idhipotese[$i];
				$_dados['inicial'] = $inicial[$i];
				$_dados['excluido'] = 'nao';

				$_row = $mapaescolasalunosrelacao->save($_dados);

				$i++;
			}
			
			$row = $mapaacompanhamentoclasses->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }

    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    } 

    public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries['idescola'] = $idescola;

		$rows = new Escolasseries();
		$this->view->rows = $rows->getEscolasseries($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setturmasAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		
		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculos($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0)) $this->view->rows = null;
		
	}
	public function setperiodoAction() {
		$this->_helper->layout->disableLayout();
		
		$idvinculo = (int)$this->_request->getPost("idvinculo");

		$queries = array();
		$queries['id'] = $idvinculo;
		
		$rows = new Escolasvinculos();
		$this->view->rows = $rows->getEscolasvinculos($queries);

		if((int)$idvinculo <= 0) $this->view->rows = null;
		
	}

	public function setperiodonotasAction() {
		$this->_helper->layout->disableLayout();

		$idescola = (int)$this->_request->getPost("idescola");

		$queries = array();
		$queries['idescola'] = $idescola;

		$rows = new Escolasconfiguracoes();
		$this->view->rows = $rows->getEscolasconfiguracoes($queries);

		if((int)$idescola <= 0) $this->view->rows = null;
	}

    public function setcoordenadoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idescola"] = $idescola;
		$queries["coordenador"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

	public function setdiretoresAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$queries = array();
		$queries["idescola"] = $idescola;
		$queries["diretor"] = "Sim";

		$rows = new Funcionariosgeraisescolas();
		$this->view->rows = $rows->getFuncionariosgeraisescolas($queries);
		if((int)$idescola <= 0) $this->view->rows = null;
	}

    public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$idserie = (int)$this->_request->getPost("idserie");
		$idescola = (int)$this->_request->getPost("idescola");
		$idvinculo = (int)$this->_request->getPost("idvinculo");

		$queries = array();
		$queries['idserie'] = $idserie;
		$queries['idescola'] = $idescola;
		$queries['idvinculo'] = $idvinculo;
		
		$rows = new Escolassalasatribuicoes();
		$this->view->rows = $rows->getSalasatribuicoes($queries);

		if(((int)$idescola <= 0) || ((int)$idserie <= 0) || ((int)$idvinculo <= 0)) $this->view->rows = null;		
	} 
}

