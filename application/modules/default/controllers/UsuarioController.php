<?php

class UsuarioController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Usuario
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("escolasusuarios", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		if($this->getRequest()->getActionName()!="editar")$this->_redirect($this->getRequest()->getControllerName() . '/editar/id/' . Escolasusuarios::getUsuario('id'));	
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}					
	}	
	
	public function setperfilAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
    	
		$perfis = new Escolasusuariosperfis();
    	$this->view->rows = $perfis->getPerfis("", array('idescola'=>$idescola)); 
	}

	public function setcargosAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$iddepartamento = $this->getRequest()->getPost('iddepartamento');
    	
		$cargos = new Escolascargos();
    	$this->view->rows = $cargos->getEscolascargos(array('iddepartamentoescola'=>$iddepartamento)); 
	}
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => null,'name' => 'Usuários das Escolas')
		);
		if (Mn_Util::isAccess('escolasusuarios', 'adicionar'))
			array_push($this->view->bread_crumb, array('url' => 'escolasusuarios/adicionar','name' => '(Adicionar Usuário)'));
		
		$this->preForm();
		$ns = new Zend_Session_Namespace('default_escolasusuarios');
		$rows = new Escolasusuarios();
		$queries = array();	
		$queries['id'] = Escolasusuarios::getUsuario('id');
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		//var_dump($_POST); die();
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
    		if ((isset($this->view->post_var['idescola'])) && ($this->view->post_var['idescola']!='')) $queries['idescola'] = (int)$this->view->post_var['idescola'];
    		if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		if ((isset($this->view->post_var['status1'])) && ($this->view->post_var['status1']!='')) $queries['status'] = $this->view->post_var['status1'];
    		if ((isset($this->view->post_var['escola'])) && ($this->view->post_var['escola']!='')) $queries['escola'] = $this->view->post_var['escola'];
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
    	$this->view->maxpp = $maxpp;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $rows->getEscolasusuarios($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $rows->getEscolasusuarios($queries, $paginaAtual, $maxpp);	
	}
	
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasusuarios', 'name' => 'Usuários das Escolas'),
			array('url' => null,'name' => 'Visualizar Usuário')
		);	
		
		$id = (int)Escolasusuarios::getUsuario('id');
		$rows = new Escolasusuarios();
		$row = $rows->getEscolausuarioById($id);
		
		if (!$row) 
			$this->_redirect($this->getRequest()->getControllerName());	
		
		
		$this->view->post_var = $row;
		$this->preForm();
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de escolasusuarios
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasusuarios', 'name' => 'Usuários das Escolas'),
			array('url' => null,'name' => 'Editar Usuário')
		);	
				
		$id = (int)Escolasusuarios::getUsuario('id');
		$rows = new Escolasusuarios();
		$row = $rows->getEscolausuarioById($id);
		
		if (!$row) 
			$this->_redirect($this->getRequest()->getControllerName());	
		
		
		$this->view->post_var = $row;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($row);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Usuário editado com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de escolasusuarios 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasusuarios',	'name' => 'Usuários das Escolas'),
			array('url' => null,'name' => 'Adicionar Usuário')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost();
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Usuário adicionado com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
	public function setdepartamentosAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
		if((int)$idescola <= 0) return $this->view->rows = false;
    	
		$departamentos = new Departamentosescolas();
    	$this->view->rows = $departamentos->getDepartamentosescolas(array('idescola'=>$idescola, 'status'=>'Ativo')); 
	}

    /**
     * Atribui valores ao view
     * @param int $idescolausuario
     */    
    private function preForm($idescolausuario = 0) {
         $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Escolasusuarios::getUsuario('id'), 'id');
        
    	$perfis = new Escolasusuariosperfis();
    	$this->view->perfis = $perfis->getPerfis(false, array('idsecretaria' => $this->view->idsecretaria));  
            
		$escolas = new Escolas();
		$this->view->escolas = $escolas->getEscolas(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
        $cargos = new Escolascargos();
		$this->view->cargos = $cargos->getEscolascargos(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		$departamentos = new Departamentosescolas();
		$this->view->departamentos = $departamentos->getDepartamentosescolas(array('status'=>'Ativo', 'idsecretaria' => $this->view->idsecretaria));
		
		$cargos_sys = $cargos->getCargoSys('SYS');
		$perfis_sys = $perfis->getPerfilByIdSecretaria(Usuarios::getSecretariaAtiva( Escolasusuarios::getUsuario('id'), 'id' ), array('origem'=>'SYS'));
		
		$dados_professores = array();
		$dados_professores['idperfil'] = $perfis_sys['id'];
		$dados_professores['iddepartamento'] = $cargos_sys['iddepartamentoescola'];

		$dados_professores['idcargo'] = $cargos_sys['id'];
		
		$this->view->post_var_professor = $dados_professores;
		
		
		$prefeituras = new Prefeituras();
		$this->view->prefeituras = $prefeituras->getPrefeituras(array('status'=>'Ativo', 'idssecretarias'=>Usuarios::getSecretariaAtiva( Escolasusuarios::getUsuario('id'), 'id' )));
    }
    
    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_escolausuario = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)Escolasusuarios::getUsuario('id');
		$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$idcargo = strip_tags((int)$this->getRequest()->getPost("idcargo"));
		$idperfil = strip_tags((int)trim($this->getRequest()->getPost("idperfil")));
		$idprefeitura = strip_tags((int)trim($this->getRequest()->getPost("idprefeitura")));
		$nomerazao = strip_tags(trim($this->getRequest()->getPost("nomerazao")));
		$sobrenomefantasia = strip_tags(trim($this->getRequest()->getPost("sobrenomefantasia")));
		$rgf = strip_tags(trim($this->getRequest()->getPost("rgf")));
		$email = strip_tags(trim($this->getRequest()->getPost("email")));
		$senha = strip_tags(trim($this->getRequest()->getPost("senha")));		
		$professor = strip_tags(trim($this->getRequest()->getPost("professor")));
		
		/**/
		$idperfilprofessor = strip_tags((int)$this->getRequest()->getPost("idperfilprofessor"));
		$iddepartamentoprofessor = strip_tags((int)$this->getRequest()->getPost("iddepartamentoprofessor"));
		$idcargoprofessor = strip_tags((int)trim($this->getRequest()->getPost("idcargoprofessor")));
		/**/
		
		$funcoes = strip_tags(trim($this->getRequest()->getPost("funcoes")));
		$unidadesdesenvolvimento = strip_tags(trim($this->getRequest()->getPost("unidadesdesenvolvimento")));
		$observacoes = strip_tags(trim($this->getRequest()->getPost("observacoes")));
		$periodoatividades = strip_tags(trim($this->getRequest()->getPost("periodoatividades")));
		$situacaogestao = strip_tags(trim($this->getRequest()->getPost("situacaogestao")));
		
		$status = strip_tags(trim($this->getRequest()->getPost("status1")));
		$recebesolicitacoes = strip_tags(trim($this->getRequest()->getPost("recebesolicitacoes")));
$enviaemails = strip_tags(trim($this->getRequest()->getPost("enviaemails")));
$idsdepartamentos = $this->getRequest()->getPost("idsdepartamentos");		

if (!is_array($idsdepartamentos)) $idsdepartamentos = array();
$idsdepartamentos = implode(',',$idsdepartamentos);

		$idescolas = $this->getRequest()->getPost("idescolas");
		if (!is_array($idescolas)) $idescolas = array();
		$idescolas = implode(',',$idescolas);
		
		$erros = array();
			
		if (""==$nomerazao) array_push($erros, 'Preencha o campo NOME.');
		if (""==$email) array_push($erros, 'Preencha o campo E-MAIL.');
		if (""==$rgf) array_push($erros, 'Preencha o campo RGF.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		if (0>=$idprefeitura) array_push($erros, 'Selecione UMA PREFEITURA.');
		if (""==$professor) array_push($erros, 'Selecione se o usuário É UM PROFESSOR.');
		if($professor=="Sim"){
			if (sizeof($idescolas) <= 0) array_push($erros, 'Selecione uma ESCOLA.');
			//$professor
		}
		
		$rows = new Escolasusuarios();
		if($email){
			$row = $rows->fetchRow("excluido='nao' AND email='$email' AND id<>".$id);
			if ($row) array_push($erros, 'Já existe usuário com esse EMAIL DE ACESSO.');
		}
		
		$_row = $rows->fetchRow("excluido='nao' AND rgf='$rgf' AND id<>".$id);
		if ($_row) array_push($erros, 'Já existe usuário com esse RGF.');		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
			
		try {		

			if($professor=="Sim"){
				
			    $perfis = new Escolasusuariosperfis();
			    $perfisprofessor = $perfis->getPerfilOrigem('SYS');
				
				
				$_idescolas = explode(',', $idescolas);
				$idescola = $_idescolas[0];
				$idperfil = $perfisprofessor['id'];
				$idcargo = $idcargoprofessor;
				//
			}


			$dados = array();
			$dados['id'] = $id;
			$dados['idescola'] = $idescola;
			$dados['idescolas'] = $idescolas;
			$dados['idcargo'] = $idcargo;
			$dados['idperfil'] = $idperfil;
			$dados['idprefeitura'] = $idprefeitura;
			$dados['nomerazao'] = $nomerazao;
			$dados['sobrenomefantasia'] = $sobrenomefantasia;
			$dados['rgf'] = $rgf;
			$dados['email'] = $email;
			$dados['senha'] = $senha;
			$dados['status'] = $status;	
			$dados['professor'] = $professor;
			
			$dados['funcoes'] = $funcoes;
			$dados['unidadesdesenvolvimento'] = $unidadesdesenvolvimento;
			$dados['observacoes'] = $observacoes;
			$dados['periodoatividades'] = $periodoatividades;
			$dados['situacaogestao'] = $situacaogestao;
			
$dados['enviaemails'] = $enviaemails;
$dados['idsdepartamentos'] = $idsdepartamentos;

			$dados['recebesolicitacoes'] = $recebesolicitacoes;		
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Usuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			//var_dump($dados); die();

			$rows->save($dados);

			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}

			
				
		return "";    	
    }
    
}