<?php

/**
 * Controle da classe nutricaomerendas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class NutricaomerendasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Nutricaomerenda
	 */
	protected $_cadastro = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("nutricaomerendas", $this->_request->getActionName());	
		
		$this->_cadastro = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Nutricaomerendas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_cadastro['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Merenda excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="nutricaomerendas") $objs = new Nutricaomerendas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_cadastro['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendas', 'name' => 'Merendas'),
			array('url' => null,'name' => 'Visualizar Merenda')
		);
		
		$id = (int)$this->_request->getParam("id");
		$nutricaomerendas = new Nutricaomerendas();
		$nutricaomerenda = $nutricaomerendas->getNutricaomerendaById($id, array());
		
		if (!$nutricaomerenda) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaomerenda;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Merendas')
		);
		
		$ns = new Zend_Session_Namespace('default_nutricaomerendas');
		$nutricaomerendas = new Nutricaomerendas();
		$queries = array();	
		$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect($this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $nutricaomerendas->getNutricaomerendas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $nutricaomerendas->getNutricaomerendas($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de nutricaomerendas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendas', 'name' => 'Merendas'),
			array('url' => null,'name' => 'Editar Merenda')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$nutricaomerendas = new Nutricaomerendas();
		$nutricaomerenda = $nutricaomerendas->getNutricaomerendaById($id);
		
		if (!$nutricaomerenda) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaomerenda;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($nutricaomerenda);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Merenda editado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de nutricaomerendas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendas', 'name' => 'Merendas'),
			array('url' => null,'name' => 'Adicionar Merenda')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Merenda adicionado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idnutricaomerenda
     */    
    private function preForm($idnutricaomerenda = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_nutricaomerenda = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
$idescolaanterior = (int)trim($this->getRequest()->getPost("idescolaanterior"));
$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
$aluno = trim($this->getRequest()->getPost("aluno"));
$titulo = trim($this->getRequest()->getPost("titulo"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$aprovacao = trim($this->getRequest()->getPost("aprovacao"));
$dataaprovacao = Mn_Util::stringToTime($this->getRequest()->getPost("dataaprovacao"));
$cancelado = trim($this->getRequest()->getPost("cancelado"));
$datacancelamento = Mn_Util::stringToTime($this->getRequest()->getPost("datacancelamento"));
$descricoescancelamento = trim($this->getRequest()->getPost("descricoescancelamento"));
$transferencia = trim($this->getRequest()->getPost("transferencia"));
$transferenciaaprovacao = trim($this->getRequest()->getPost("transferenciaaprovacao"));
$status = trim($this->getRequest()->getPost("status1"));
$logdataescola = trim($this->getRequest()->getPost("logdataescola"));
$logusuarioescola = (int)trim($this->getRequest()->getPost("logusuarioescola"));
	
$propriacompra = trim($this->getRequest()->getPost("propriacompra"));
		
$tipo = trim($this->getRequest()->getPost("tipo"));
$quantidaderefeicao = trim($this->getRequest()->getPost("quantidaderefeicao"));
$quantidadesobremesa = trim($this->getRequest()->getPost("quantidadesobremesa"));
$dia = Mn_Util::stringToTime($this->getRequest()->getPost("dia"));
$quantidade = trim($this->getRequest()->getPost("quantidade"));		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escolas.");
//if (0==$idescolaanterior) array_push($erros, "Informe a Escolas.");
if (""==$titulo) array_push($erros, "Informe a Título.");
if (""==$dataaprovacao) array_push($erros, "Informe a Data de aprovação.");
if ("Analítico"==$tipo && (int)$idaluno <= 0) array_push($erros, "Informe um Aluno.");
//if (""==$datacancelamento) array_push($erros, "Informe a Data de Cancelamento.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$nutricaomerendas = new Nutricaomerendas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = $idsecretaria;
$dados["idescola"] = $idescola;
$dados["idescolaanterior"] = $idescolaanterior;
$dados["idaluno"] = $idaluno;
$dados["aluno"] = $aluno;
$dados["titulo"] = $titulo;
$dados["descricoes"] = $descricoes;
$dados["aprovacao"] = $aprovacao;
$dados["dataaprovacao"] = date("Y-m-d", $dataaprovacao);
$dados["cancelado"] = $cancelado;
$dados["propriacompra"] = $propriacompra;
$dados["tipo"] = $tipo;
$dados["quantidaderefeicao"] = $quantidaderefeicao;
$dados["quantidadesobremesa"] = $quantidadesobremesa;
$dados["dia"] = date("Y-m-d", $dia);
$dados["quantidade"] = $quantidade;
$dados["descricoescancelamento"] = $descricoescancelamento;
$dados["transferencia"] = $transferencia;
$dados["transferenciaaprovacao"] = $transferenciaaprovacao;
$dados["status"] = $status;
$dados["logdataescola"] = $logdataescola;
$dados["logusuarioescola"] = $logusuarioescola;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_cadastro['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $nutricaomerendas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}