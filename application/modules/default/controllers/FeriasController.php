<?php

/**
 * Controle da classe ferias do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class FeriasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Feria
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("ferias", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Ferias();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Férias excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="ferias") $objs = new Ferias();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ferias', 'name' => 'Férias'),
			array('url' => null,'name' => 'Visualizar Férias')
		);
		
		$id = (int)$this->_request->getParam("id");
		$ferias = new Ferias();
		$feria = $ferias->getFeriaById($id, array());
		
		if (!$feria) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $feria;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$ns = new Zend_Session_Namespace('admin_ferias');
		$ferias = new Ferias();
		$this->preForm();
		$queries = array();	
        $queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
        $queries['origem'] = "Escola";
		//$queries['mesinicio'] = '11';
		//$queries['mesfim'] = '11';
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ((isset($this->view->post_var['status'])) && ($this->view->post_var['status']!='')) $queries['status'] = $this->view->post_var['status'];
    		if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		
    		if ((isset($this->view->post_var['datainicio'])) && ($this->view->post_var['datainicio']!='')) {
				$datainicio = $this->view->post_var["datainicio"];		
				$datainicio = Mn_Util::stringToTime($datainicio);		
				$datainicio = date('Y-m-d G:i:s', $datainicio);
				$queries['datainicio'] = $datainicio;
    		}
    		
    	    if ((isset($this->view->post_var['datafim'])) && ($this->view->post_var['datafim']!='')) {
				$datafim = $this->view->post_var["datafim"];		
				$datafim = Mn_Util::stringToTime($datafim);		
				$datafim = date('Y-m-d G:i:s', $datafim);
				$queries['datafim'] = $datafim;
    		}
    		
    		if ((isset($this->view->post_var['origem'])) && ($this->view->post_var['origem']!='')) $queries['origem'] = $this->view->post_var['origem'];
    		if ((isset($this->view->post_var['idfuncionariosecretaria'])) && ($this->view->post_var['idfuncionariosecretaria']!='')) $queries['idfuncionariosecretaria'] = $this->view->post_var['idfuncionariosecretaria'];
    		if ((isset($this->view->post_var['idescola'])) && ($this->view->post_var['idescola']!='')) $queries['idescola'] = $this->view->post_var['idescola'];
    		if ((isset($this->view->post_var['idfuncionarioescola'])) && ($this->view->post_var['idfuncionarioescola']!='')) $queries['idfuncionarioescola'] = $this->view->post_var['idfuncionarioescola'];
    		if ((isset($this->view->post_var['anobusca'])) && ($this->view->post_var['anobusca']!='')) $queries['anobusca'] = $this->view->post_var['anobusca'];
    	}		
		if(!$this->view->post_var['anobusca']) $queries['anobusca'] = '2016';
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $ferias->getFerias($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $ferias->getFerias($queries, $paginaAtual, $maxpp);
	}
	
	/**
	 * 
	 * Action de edição de ferias
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ferias', 'name' => 'Férias'),
			array('url' => null,'name' => 'Editar Férias')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$ferias = new Ferias();
		$feria = $ferias->getFeriaById($id);
		
		if (!$feria) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $feria;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($feria);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Férias editado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de ferias 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'ferias', 'name' => 'Férias'),
			array('url' => null,'name' => 'Adicionar Férias')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Férias adicionado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function calculadataAction() {
		$this->_helper->layout->disableLayout();

		$dias = (int)$this->_request->getPost("dias");
		$datainicio = Mn_Util::stringToTime($this->_request->getPost("datainicio"));
		
		if((!$datainicio) && ($datainicio=="")){ die(); }
		$datainicio = date("Y-m-d", $datainicio);

		if((!isset($dias)) && (!$dias) && ($dias<=0)){ die(); }


		$datafim = date('d/m/Y', strtotime("+".$dias." days",strtotime($datainicio))); ;


		die($datafim);
	}

    /**
     * Atribui valores ao view
     * @param int $idferia
     */    
    private function preForm($idferia = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_feria = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
$origem = trim($this->getRequest()->getPost("origem"));
$idfuncionariosecretaria = (int)trim($this->getRequest()->getPost("idfuncionariosecretaria"));
$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
$idfuncionarioescola = (int)trim($this->getRequest()->getPost("idfuncionarioescola"));
$dias = trim($this->getRequest()->getPost("dias"));
$datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
$datafim = Mn_Util::stringToTime($this->getRequest()->getPost("datafim"));
$descricoes = trim($this->getRequest()->getPost("descricoes"));
$aprovacao = trim($this->getRequest()->getPost("aprovacao"));
$confirmacaoaprovacao = trim($this->getRequest()->getPost("confirmacaoaprovacao"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (""==$origem) array_push($erros, "Informe a Status.");
		if($origem=="Escola"){
			if (0==$idescola) array_push($erros, "Informe a Escola.");
			if (0==$idfuncionarioescola) array_push($erros, "Informe a Usuário da escola.");			
		}
if (""==$aprovacao) array_push($erros, "Informe a Aprovação.");
if (""==$confirmacaoaprovacao) array_push($erros, "Informe a Confirmar Aprovação.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$ferias = new Ferias();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = $idsecretaria;
$dados["origem"] = $origem;
$dados["idfuncionariosecretaria"] = $idfuncionariosecretaria;
$dados["idescola"] = $idescola;
$dados["idfuncionarioescola"] = $idfuncionarioescola;
$dados["dias"] = $dias;
$dados["datainicio"] = date("Y-m-d", $datainicio);
$dados["datafim"] = date("Y-m-d", $datafim);
$dados["descricoes"] = $descricoes;
$dados["aprovacao"] = $aprovacao;
$dados["confirmacaoaprovacao"] = $confirmacaoaprovacao;
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $ferias->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}