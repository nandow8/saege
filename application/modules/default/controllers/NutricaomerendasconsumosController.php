<?php

/**
 * Controle da classe nutricaomerendasconsumos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class NutricaomerendasconsumosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Nutricaomerenda
	 */
	protected $_cadastro = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("nutricaomerendasconsumos", $this->_request->getActionName());	
		
		$this->_cadastro = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Nutricaomerendasconsumos();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_cadastro['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Merenda excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="nutricaomerendasconsumos") $objs = new Nutricaomerendasconsumos();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_cadastro['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendasconsumos', 'name' => 'Controle de Consumos'),
			array('url' => null,'name' => 'Visualizar Controle de Consumo')
		);
		
		$id = (int)$this->_request->getParam("id");
		$nutricaomerendasconsumos = new Nutricaomerendasconsumos();
		$nutricaomerendasconsumo = $nutricaomerendasconsumos->getNutricaomerendasconsumoById($id, array());
		
		if (!$nutricaomerendasconsumo) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaomerendasconsumo;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Merendas')
		);
		
		$ns = new Zend_Session_Namespace('default_nutricaomerendasconsumos');
		$nutricaomerendasconsumos = new Nutricaomerendasconsumos();
		$queries = array();	
		$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect($this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["datalancamento_i"]!="") $queries["datalancamento_i"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_i"]));
			if ($this->view->post_var["datalancamento_f"]!="") $queries["datalancamento_f"] = date("Y-m-d", MN_Util::stringToTime($this->view->post_var["datalancamento_f"]));
			if ($this->view->post_var["idusuario"]!="") $queries["idusuario"] = $this->view->post_var["idusuario"];
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
			if ($this->view->post_var["tipoensino"]!="") $queries["tipoensino"] = $this->view->post_var["tipoensino"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $nutricaomerendasconsumos->getNutricaomerendasconsumos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $nutricaomerendasconsumos->getNutricaomerendasconsumos($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de nutricaomerendasconsumos
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendasconsumos', 'name' => 'Controle de Consumos'),
			array('url' => null,'name' => 'Editar Controle de Consumo')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$nutricaomerendasconsumos = new Nutricaomerendasconsumos();
		$nutricaomerendasconsumo = $nutricaomerendasconsumos->getNutricaomerendasconsumoById($id);
		
		if (!$nutricaomerendasconsumo) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaomerendasconsumo;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($nutricaomerendasconsumo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de Consumo editado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de nutricaomerendasconsumos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricaomerendasconsumos', 'name' => 'Controle de Consumos'),
			array('url' => null,'name' => 'Adicionar Controle de Consumo')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Controle de Consumo adicionado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idnutricaomerenda
     */    
    private function preForm($idnutricaomerenda = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_nutricaomerenda = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$datalancamento = Mn_Util::stringToTime($this->getRequest()->getPost("datalancamento"));
		$idusuario = trim($this->getRequest()->getPost("idusuario"));
		$idescola = trim($this->getRequest()->getPost("idescola"));
		$periodos = trim($this->getRequest()->getPost("periodos"));
		$tipoensino = trim($this->getRequest()->getPost("tipoensino"));
		$turmas = trim($this->getRequest()->getPost("turmas"));
		$tipo = trim($this->getRequest()->getPost("tipo"));
		$aluno = trim($this->getRequest()->getPost("aluno"));
		$qtdrefeicao = trim($this->getRequest()->getPost("qtdrefeicao"));
		$qtdrefeicaorepeticoes = trim($this->getRequest()->getPost("qtdrefeicaorepeticoes"));
		$qtdsobremesa = trim($this->getRequest()->getPost("qtdsobremesa"));
		$qtdsobremesarepeticoes = trim($this->getRequest()->getPost("qtdsobremesarepeticoes"));
		$qtdcafemanha = trim($this->getRequest()->getPost("qtdcafemanha"));
		$qtdcafemanharepeticoes = trim($this->getRequest()->getPost("qtdcafemanharepeticoes"));
		$qtdalmoco = trim($this->getRequest()->getPost("qtdalmoco"));
		$qtdalmocorepeticoes = trim($this->getRequest()->getPost("qtdalmocorepeticoes"));
		$qtdcafetarde = trim($this->getRequest()->getPost("qtdcafetarde"));
		$qtdcafetarderepeticoes = trim($this->getRequest()->getPost("qtdcafetarderepeticoes"));
		$qtdjantar = trim($this->getRequest()->getPost("qtdjantar"));
		$qtdjantarrepeticoes = trim($this->getRequest()->getPost("qtdjantarrepeticoes"));
		$qtdmamadeira = trim($this->getRequest()->getPost("qtdmamadeira"));
		$qtdmamadeirarepeticoes = trim($this->getRequest()->getPost("qtdmamadeirarepeticoes"));
		$qtddesjejum = trim($this->getRequest()->getPost("qtddesjejum"));
		$qtddesjejumrepeticoes = trim($this->getRequest()->getPost("qtddesjejumrepeticoes"));
		$qtdcolacao = trim($this->getRequest()->getPost("qtdcolacao"));
		$qtdcolacaorepeticoes = trim($this->getRequest()->getPost("qtdcolacaorepeticoes"));
		$qtdlanche = trim($this->getRequest()->getPost("qtdlanche"));
		$qtdlancherepeticoes = trim($this->getRequest()->getPost("qtdlancherepeticoes"));
		
		$erros = array();
		
		if (""==$datalancamento) array_push($erros, "Informe a Data do Lançamento.");
		if (""==$idusuario) array_push($erros, "Informe o Nome do Usuário.");
		if (""==$idescola) array_push($erros, "Informe a Escola.");
		if (""==$tipoensino) array_push($erros, "Informe o Tipo de Ensino.");
		if (""==$tipo) array_push($erros, "Informe o Tipo.");
		

		
		$nutricaomerendasconsumos = new Nutricaomerendasconsumos();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["datalancamento"] = date("Y-m-d", $datalancamento);
			$dados["idusuario"] = $idusuario;
			$dados["idescola"] = $idescola;
			$dados["periodos"] = $periodos;
			$dados["tipoensino"] = $tipoensino;
			$dados["turmas"] = $turmas;
			$dados["tipo"] = $tipo;
			$dados["aluno"] = $aluno;
			$dados["qtdrefeicao"] = $qtdrefeicao;
			$dados["qtdrefeicaorepeticoes"] = $qtdrefeicaorepeticoes;
			$dados["qtdsobremesa"] = $qtdsobremesa;
			$dados["qtdsobremesarepeticoes"] = $qtdsobremesarepeticoes;
			$dados["qtdcafemanha"] = $qtdcafemanha;
			$dados["qtdcafemanharepeticoes"] = $qtdcafemanharepeticoes;
			$dados["qtdalmoco"] = $qtdalmoco;
			$dados["qtdalmocorepeticoes"] = $qtdalmocorepeticoes;
			$dados["qtdcafetarde"] = $qtdcafetarde;
			$dados["qtdcafetarderepeticoes"] = $qtdcafetarderepeticoes;
			$dados["qtdjantar"] = $qtdjantar;
			$dados["qtdjantarrepeticoes"] = $qtdjantarrepeticoes;
			$dados["qtdmamadeira"] = $qtdmamadeira;
			$dados["qtdmamadeirarepeticoes"] = $qtdmamadeirarepeticoes;
			$dados["qtddesjejum"] = $qtddesjejum;
			$dados["qtddesjejumrepeticoes"] = $qtddesjejumrepeticoes;
			$dados["qtdcolacao"] = $qtdcolacao;
			$dados["qtdcolacaorepeticoes"] = $qtdcolacaorepeticoes;
			$dados["qtdlanche"] = $qtdlanche;
			$dados["qtdlancherepeticoes"] = $qtdlancherepeticoes;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_cadastro['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $nutricaomerendasconsumos->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}