<?php

/**
 * Controle da classe dadospessoais do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class DadospessoaisController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Dadopessoal
	 */
	protected $_usuario = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("dadospessoais", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Dadospessoais();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_usuario['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Dado pessoal excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="dadospessoais") $objs = new Dadospessoais();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_usuario['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'dadospessoais', 'name' => 'Dados pessoais'),
			array('url' => null,'name' => 'Visualizar Dado pessoal')
		);
		
		$id = (int)$this->_request->getParam("id");
		$dadospessoais = new Dadospessoais();
		$dadopessoal = $dadospessoais->getDadopessoalById($id, array());
		
		if (!$dadopessoal) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $dadopessoal;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Dados pessoais')
		);
		
		$ns = new Zend_Session_Namespace('default_dadospessoais');
		$dadospessoais = new Dadospessoais();
		$queries = array();	
		$queries['origem']= "Escola";
				
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect($this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idfuncionarioescola"]!="") $queries["idfuncionarioescola"] = $this->view->post_var["idfuncionarioescola"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $dadospessoais->getDadospessoais($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $dadospessoais->getDadospessoais($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de dadospessoais
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'dadospessoais', 'name' => 'Dados pessoais'),
			array('url' => null,'name' => 'Editar Dado pessoal')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$dadospessoais = new Dadospessoais();
		$dadopessoal = $dadospessoais->getDadopessoalById($id);
		
		if (!$dadopessoal) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $dadopessoal;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($dadopessoal);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Dado pessoal editado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de dadospessoais 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'dadospessoais', 'name' => 'Dados pessoais'),
			array('url' => null,'name' => 'Adicionar Dado pessoal')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Dado pessoal adicionado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $iddadopessoal
     */    
    private function preForm($iddadopessoal = 0) {
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_dadopessoal = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idfuncionarioescola = (int)trim($this->getRequest()->getPost("idfuncionarioescola"));
$idfoto3x4 = (int)trim($this->getRequest()->getPost("idfoto3x4"));
$idfoto2x2 = (int)trim($this->getRequest()->getPost("idfoto2x2"));
$idrg = (int)trim($this->getRequest()->getPost("idrg"));
$idcpf = (int)trim($this->getRequest()->getPost("idcpf"));
$idtituloeleitor = (int)trim($this->getRequest()->getPost("idtituloeleitor"));
$idreservista = (int)trim($this->getRequest()->getPost("idreservista"));
$idpispaseb = (int)trim($this->getRequest()->getPost("idpispaseb"));
$idcomprovanteeleicao = (int)trim($this->getRequest()->getPost("idcomprovanteeleicao"));
$idcomprovanteendereco = (int)trim($this->getRequest()->getPost("idcomprovanteendereco"));
$idcertidaonascimento = (int)trim($this->getRequest()->getPost("idcertidaonascimento"));
$idcertidaocasamento = (int)trim($this->getRequest()->getPost("idcertidaocasamento"));
$idantecedentes = (int)trim($this->getRequest()->getPost("idantecedentes"));
$iddgraduacao = (int)trim($this->getRequest()->getPost("iddgraduacao"));
$idcargo = (int)trim($this->getRequest()->getPost("idcargo"));
$idexame = (int)trim($this->getRequest()->getPost("idexame"));
$idcnh = (int)trim($this->getRequest()->getPost("idcnh"));
$idconta = (int)trim($this->getRequest()->getPost("idconta"));
$iddeclaracaobens = (int)trim($this->getRequest()->getPost("iddeclaracaobens"));
$iddeclaracaobensmiposto = (int)trim($this->getRequest()->getPost("iddeclaracaobensmiposto"));
$idacumulo = (int)trim($this->getRequest()->getPost("idacumulo"));
$iddaposentadoria = (int)trim($this->getRequest()->getPost("iddaposentadoria"));
$idfamiliar = (int)trim($this->getRequest()->getPost("idfamiliar"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idfuncionarioescola) array_push($erros, "Informe a Funcionário.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$dadospessoais = new Dadospessoais();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			$dados['origem']= "Escola";
			$dados["idfuncionarioescola"] = $idfuncionarioescola;

$idfoto3x4 = $this->getImagem('idfoto3x4');
			if ($idfoto3x4!=0) $dados['idfoto3x4'] = $idfoto3x4;

$idfoto2x2 = $this->getImagem('idfoto2x2');
			if ($idfoto2x2!=0) $dados['idfoto2x2'] = $idfoto2x2;

$idrg = $this->getArquivo('idrg');
			if ($idrg!=0) $dados['idrg'] = $idrg;

$idcpf = $this->getArquivo('idcpf');
			if ($idcpf!=0) $dados['idcpf'] = $idcpf;

$idtituloeleitor = $this->getArquivo('idtituloeleitor');
			if ($idtituloeleitor!=0) $dados['idtituloeleitor'] = $idtituloeleitor;

$idreservista = $this->getArquivo('idreservista');
			if ($idreservista!=0) $dados['idreservista'] = $idreservista;

$idpispaseb = $this->getArquivo('idpispaseb');
			if ($idpispaseb!=0) $dados['idpispaseb'] = $idpispaseb;

$idcomprovanteeleicao = $this->getArquivo('idcomprovanteeleicao');
			if ($idcomprovanteeleicao!=0) $dados['idcomprovanteeleicao'] = $idcomprovanteeleicao;

$idcomprovanteendereco = $this->getArquivo('idcomprovanteendereco');
			if ($idcomprovanteendereco!=0) $dados['idcomprovanteendereco'] = $idcomprovanteendereco;

$idcertidaonascimento = $this->getArquivo('idcertidaonascimento');
			if ($idcertidaonascimento!=0) $dados['idcertidaonascimento'] = $idcertidaonascimento;

$idcertidaocasamento = $this->getArquivo('idcertidaocasamento');
			if ($idcertidaocasamento!=0) $dados['idcertidaocasamento'] = $idcertidaocasamento;

$idantecedentes = $this->getArquivo('idantecedentes');
			if ($idantecedentes!=0) $dados['idantecedentes'] = $idantecedentes;

$iddgraduacao = $this->getArquivo('iddgraduacao');
			if ($iddgraduacao!=0) $dados['iddgraduacao'] = $iddgraduacao;

$idcargo = $this->getArquivo('idcargo');
			if ($idcargo!=0) $dados['idcargo'] = $idcargo;

$idexame = $this->getArquivo('idexame');
			if ($idexame!=0) $dados['idexame'] = $idexame;

$idcnh = $this->getArquivo('idcnh');
			if ($idcnh!=0) $dados['idcnh'] = $idcnh;

$idconta = $this->getArquivo('idconta');
			if ($idconta!=0) $dados['idconta'] = $idconta;

$iddeclaracaobens = $this->getArquivo('iddeclaracaobens');
			if ($iddeclaracaobens!=0) $dados['iddeclaracaobens'] = $iddeclaracaobens;

$iddeclaracaobensmiposto = $this->getArquivo('iddeclaracaobensmiposto');
			if ($iddeclaracaobensmiposto!=0) $dados['iddeclaracaobensmiposto'] = $iddeclaracaobensmiposto;

$idacumulo = $this->getArquivo('idacumulo');
			if ($idacumulo!=0) $dados['idacumulo'] = $idacumulo;

$iddaposentadoria = $this->getArquivo('iddaposentadoria');
			if ($iddaposentadoria!=0) $dados['iddaposentadoria'] = $iddaposentadoria;

$idfamiliar = $this->getArquivo('idfamiliar');
			if ($idfamiliar!=0) $dados['idfamiliar'] = $idfamiliar;
$dados["status"] = $status;

			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_usuario['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $dadospessoais->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}