<?php

require_once BASE_PATH . '/library/tcpdf/tcpdf.php';
define('MY_PATH', 'http://mnsolucoes.com');

class AlmoxarifadosolicitacoesController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacotesolicitacao
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("almoxarifadosolicitacoes", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}	
	

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Almoxarifado_Solicitacoes();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = (int)Usuarios::getUsuario('id') * -1;
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="almoxarifadosolicitacoes") $objs = new Almoxarifado_Solicitacoes();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = (int)Usuarios::getUsuario('id') * -1;
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Almoxarifado_Solicitacoes();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Almoxarifado_Solicitacoes();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Solicitações')
		);
		
		
		$ns = new Zend_Session_Namespace('default_almoxarifadosolicitacoes');
		$solicitacoes = new Almoxarifado_Solicitacoes();
		$queries = array();	
		$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ($this->view->post_var['status']!='') $queries['status'] = $this->view->post_var['status'];
    		if ($this->view->post_var['chave']!='') $queries['chave'] = $this->view->post_var['chave'];
    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $solicitacoes->getSolicitacoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $solicitacoes->getSolicitacoes($queries, $paginaAtual, $maxpp);	
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosolicitacoes',	'name' => 'Solicitações'),
			array('url' => null,'name' => 'Visualizar Solicitação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$solicitacoes = new Almoxarifado_Solicitacoes();
		$solicitacao = $solicitacoes->getSolicitacaoById($id);
		
		if (!$solicitacao) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $solicitacao;
		$this->preForm();
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de solicitacao
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosolicitacoes',	'name' => 'Solicitações'),
			array('url' => null,'name' => 'Editar Solicitação')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$solicitacoes = new Almoxarifado_Solicitacoes();
		$solicitacao = $solicitacoes->getSolicitacaoById($id);
		
		if (!$solicitacao) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $solicitacao;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($solicitacao);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação editada com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de solicitacoes 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'almoxarifadosolicitacoes',	'name' => 'Solicitações'),
			array('url' => null,'name' => 'Adicionar Solicitação')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Solicitação adicionada com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
	public function requisicaoAction() {
		$id = (int)$this->getRequest()->getParam('id');

		$solicitacoes = new Almoxarifado_Solicitacoes();
		$solicitacao = $solicitacoes->getSolicitacaoById($id);
		
		if (!$solicitacao) 
			$this->_redirect('admin/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $solicitacao;
		
		$nome = "--";
		$endereco = "--";
		
		$usuario = Escolasusuarios::getEscolausuarioHelper($solicitacao['idusuarioescola']);
		
		if($usuario):
			$nome = $usuario['escola'];
			$dados_enderecos = Enderecos::getEnderecoById((int)$usuario['e1idendereco']);
			if($dados_enderecos):
				$endereco = $dados_enderecos['endereco'] . ", N°  " . $dados_enderecos['numero'] . ", " . $dados_enderecos['bairro'] . ", " . $dados_enderecos['uf'];		
			endif;
		endif;
		
		$dados_origens = array();
		$dados_origens['texto_nome'] = $nome;
		$dados_origens['texto_endereco'] = $endereco;
		$this->view->post_var = array_merge($this->view->post_var, $dados_origens);
		
		$this->preForm();
		//var_dump($this->view->post_var); die();
		
		$db = Zend_Registry::get('db');
		
		
		//$var = TCPDF::getTeste();
		//var_dump($var); die();
		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
	
		$pdf->setPageOrientation('l');
	
	
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Prefeituta Municipal de Itaquaquecetuba');
		$pdf->SetTitle('REQUISIÇÃO DE MATERIAIS AUTORIZADOS');
		$pdf->SetSubject('REQUISIÇÃO DE MATERIAIS AUTORIZADOS');
	
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/default/views/scripts/');

	
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
		//set margins
		$pdf->SetMargins(10, 35, 10);
	
	
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
	
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
	
		
		
		// add a page
		$pdf->AddPage();
	
		$html = $this->view->render('almoxarifadosolicitacoes/pdf/index.phtml');
		//echo $html; die();

		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = 'declaracao_'.date('d').'_'.date('m').'.pdf';
			
		$pdf->Output($filename, 'D');
	
		die();
	
		return true;
	}
    /**
     * Atribui valores ao view
     * @param int $idsolicitacao
     */    
    private function preForm($idsolicitacao = 0) {
		$escolas = new Escolas();
		$this->view->escolas = $escolas->getEscolas(array('status'=>'Ativo'));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idendereco = strip_tags((int)$this->getRequest()->getPost("idendereco"));
		$titulo = strip_tags(trim($this->getRequest()->getPost("titulo")));	
		$descricoes = strip_tags(trim($this->getRequest()->getPost("descricoes")));
		$quantidade = strip_tags(trim($this->getRequest()->getPost("quantidade")));
		$idusuariosecretaria = strip_tags(trim($this->getRequest()->getPost("idusuariosecretaria")));
		$idusuarioescola = strip_tags(trim($this->getRequest()->getPost("idusuarioescola")));
		
		$aprovacao = strip_tags(trim($this->getRequest()->getPost("aprovacao")));
		$confirmaaprovacao = strip_tags(trim($this->getRequest()->getPost("confirmaaprovacao")));
		$confirmasolicitacao = strip_tags(trim($this->getRequest()->getPost("confirmasolicitacao")));
		$status = strip_tags(trim($this->getRequest()->getPost("status")));
		$erros = array();

		if (""==$titulo) array_push($erros, 'Preencha o campo ENTRADA.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$solicitacoes = new Almoxarifado_Solicitacoes();
		//$row = $solicitacoes->fetchRow("excluido='nao' AND solicitacao='$solicitacao' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma ESCOLA com esse NOME.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			if($id<=0) $confirmaaprovacao = "Não";
			
			$dados = array();
			$dados['id'] = $id;
			$dados['titulo'] = $titulo;
			$dados['descricoes'] = $descricoes;
			$dados['quantidade'] = $quantidade;
			$dados['origem'] = Almoxarifado_Solicitacoes::$_ORIGEM_ESCOLA;
			//$dados['idusuariosecretaria'] = $this->_usuario['id'];
			$dados['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');;
			$dados['idusuarioescola'] = $this->_usuario['id'];		
			$dados['aprovacao'] = 'Não';
			$dados['confirmaaprovacao'] = $confirmaaprovacao;
			$dados['confirmasolicitacao'] = ($confirmasolicitacao) ? $confirmasolicitacao : 'Não';
			
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = (int)Usuarios::getUsuario('id') * -1;
			$dados['logdata'] = date('Y-m-d G:i:s');

			$dados_saidas = array();
			$dados_saidas['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
			$dados_saidas['titulo'] = $titulo;
			$dados_saidas['descricoes'] = $descricoes;			
			$dados_saidas['origem'] = Almoxarifado_Solicitacoes::$_ORIGEM_ESCOLA;;			
			$dados_saidas['aprovacao'] = "Não";
			$dados_saidas['status'] = $status;
			$dados_saidas['excluido'] = 'nao';
			$dados_saidas['logusuario'] = Usuarios::getUsuario('id');
			$dados_saidas['logdata'] = date('Y-m-d G:i:s');
			$dados['itens'] = $dados_saidas;
			
			
			$row = $solicitacoes->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }
}