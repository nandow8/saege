<?php

/**
 * Controle da classe escolasalunosfaltas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class EscolasalunosfaltasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Escolasalunosfalta
	 */
	protected $_cadastro = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("escolasalunosfaltas", $this->_request->getActionName());	
		$this->_cadastro = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Escolasalunosfaltas();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_cadastro['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "falta excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="escolasalunosfaltas") $objs = new Escolasalunosfaltas();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_cadastro['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasalunosfaltas', 'name' => 'Frequências'),
			array('url' => null,'name' => 'Visualizar Frequência')
		);
		
		$id = (int)$this->_request->getParam("id");
		$escolasalunosfaltas = new Escolasalunosfaltas();
		$escolasalunosfalta = $escolasalunosfaltas->getEscolasalunosfaltaById($id, array());
		
		if (!$escolasalunosfalta) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolasalunosfalta;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Frequências')
		);
		
		$ns = new Zend_Session_Namespace('default_escolasalunosfaltas');
		$escolasvinculos = new Escolasvinculos();
		$queries = array();	
		$queries['idescola'] = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 
		//$queries['idsescola'] = Escolasusuarios::getUsuario('idescola'); 
		//var_dump((int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id')); die();
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect($this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
if ($this->view->post_var["idserie"]!="") $queries["idserie"] = $this->view->post_var["idserie"];
if ($this->view->post_var["idturma"]!="") $queries["idturma"] = $this->view->post_var["idturma"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $escolasvinculos->getEscolasvinculos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $escolasvinculos->getEscolasvinculos($queries, $paginaAtual, $maxpp);
	}
	
	/**
	 * 
	 * Action de edição de escolasalunosfaltas
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasalunosfaltas', 'name' => 'Frequências'),
			array('url' => null,'name' => 'Editar Frequência')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$escolasvinculos = new Escolasvinculos();
		$escolavinculo = $escolasvinculos->getEscolavinculoById($id);
		
		if (!$escolavinculo) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $escolavinculo;
		$this->preForm();
		$alunos = new Escolassalasatribuicoes();
		$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['idvinculo'] = $id;
		
		$this->view->escolasalunos = $alunos->getSalasatribuicoes($queries);	
		

		$this->view->materias = Controlealunosmaterias::getControlealunosmateriasHelper(array('status'=>'Ativo'));
		if(Escolasusuarios::getUsuario('professor')=="Sim"):
			$this->view->materias = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>Escolasusuarios::getUsuario('id'), 'idescolavinculo'=>$id));
		endif;
    	//$this->view->professores = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$id, 'order'=>' GROUP BY q1.idprofessor'));
    	$this->view->professores = Quadrohorarios::getQuadrohorariosHelper(array('order'=>' GROUP BY q1.idprofessor'));
			
		if ($this->_request->isPost()) {
			$erros = $this->getPost($escolavinculo);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Frequências adicionadas com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}

		return true;	
    }  		
	
	/**
	 * 
	 * Action de adição de escolasalunosfaltas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasalunosfaltas', 'name' => 'Frequências'),
			array('url' => null,'name' => 'Adicionar Frequência')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Frequência adicionada com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function setmateriasAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idprofessor = $this->getRequest()->getPost('idprofessor');
		$idescolavinculo = $this->getRequest()->getPost('idescolavinculo');
    	if((int)$idprofessor <=0) return $this->view->rows = array();
		$professor = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($idprofessor);
		if(!$professor) return $this->view->rows = array();

		$controlealunosmaterias = new Controlealunosmaterias();
		$queries_professores = array();	
		$queries_professores['status'] = "Ativo";
		$queries_professores['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

		$queries_professores['find_in_set'] = ((isset($professor['idsmaterias'])) && ($professor['idsmaterias']!="")) ? $professor['idsmaterias'] : false;
		//$this->view->materiasprofessores = $controlealunosmaterias->getControlealunosmaterias($queries_professores);

if(Escolasusuarios::getUsuario('professor')=="Sim"):
	$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo));
else:
	if((int)$idprofessor<=0){
		$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$idescolavinculo));	
	}else{
		$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array('idescolavinculo'=>$idescolavinculo, 'idprofessor'=>$idprofessor));			
	}
	
endif;
//apagar
	$this->view->materiasprofessores = Quadrohorarios::getQuadrohorariosHelper(array());	
		//var_dump($this->view->materiasprofessores); die();
	}

	public function setfaltasAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idprofessor = $this->getRequest()->getPost('idprofessor');
		if(Escolasusuarios::getUsuario('professor')=="Sim"){
			$idprofessor = Escolasusuarios::getUsuario('id');
		}

		$idescolavinculo = $this->getRequest()->getPost('idescolavinculo');
		$idmateria = $this->getRequest()->getPost('idmateria');
    	
    	$escolasalunosnotas = new Escolasalunosnotas();
    	$this->view->rows = $escolasalunosnotas->getEscolasalunosnotas(array('idprofessor'=>$idprofessor, 'idescolavinculo'=>$idescolavinculo, 'idmateria'=>$idmateria));

		$alunos = new Escolassalasatribuicoes();
		$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['idvinculo'] = $idescolavinculo;
		$queries['order'] = "ORDER BY a1.nomerazao ASC";
		$this->view->escolasalunos = $alunos->getSalasatribuicoes($queries);
		$this->view->idprofessor = $idprofessor;
		$this->view->idescolavinculo = $idescolavinculo;
		$this->view->idmateria = $idmateria;
		$this->preForm();
	}

	public function addfaltaAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');;
		$idescolavinculo = (int)$this->getRequest()->getPost('idescolavinculo');
		$idmateria = (int)$this->getRequest()->getPost('idmateria');
		$idprofessor = (int)$this->getRequest()->getPost('idprofessor');
		if(Escolasusuarios::getUsuario('professor')=="Sim"){
			$idprofessor = Escolasusuarios::getUsuario('id');
		}
		$idaluno = (int)$this->getRequest()->getPost("idaluno");
		$dia = $this->getRequest()->getPost('dia');
		$_dia = $dia . '/' . date('m') . '/' . date('Y');
		$data = Mn_Util::stringToTime($_dia);
		$tipo = $this->getRequest()->getPost('tipo');
		$status = "Ativo";
		
		$erros = "";
		
		if (0==$dia) $erros = "Informe o dia.";
		if (0==$idescola) $erros = "Informe a Escola.";
		if (0==$idmateria) $erros = "Informe a Matéria.";
		if (0==$idprofessor) $erros = "Informe a Professor.";
		if (0==$idaluno) $erros = "Informe a Aluno.";
		if (""==$status) $erros = "Informe a Status.";
		$obj = new stdClass();
		$obj->id = 0;
		$obj->data = "ERRO";
		$obj->mensagem = $erros;
		$obj->titulo = "Erro!";
		if($erros!=""){
			echo json_encode($obj);
			die();			
		}

		
		$escolasalunosfaltas = new Escolasalunosfaltas();
		$dados = array();
		$dados['id'] = $id;
		
		$dados["idescola"] = $idescola;
		$dados["idescolavinculo"] = $idescolavinculo;
		$dados["idmateria"] = $idmateria;
		$dados["idprofessor"] = $idprofessor;
		$dados["idaluno"] = $idaluno;
		$dados["data"] = date("Y-m-d", $data);
		$dados["status"] = $status;
		$dados['excluido'] = 'nao';
		$dados['logusuario'] = $this->_cadastro['id'];;
		$dados['logdata'] = date('Y-m-d G:i:s');
		//var_dump($dados); die();		
		$row = $escolasalunosfaltas->save($dados);

		$obj->id = $row['id'];
		$obj->data = "OK";
		$obj->mensagem = "Falta adicionada com Sucesso!";
		$obj->titulo = "Sucesso!";

		echo json_encode($obj);
		die();
	}	

	public function removefaltaAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$id = (int)$this->getRequest()->getPost("id");		
		$escolasalunosfaltas = new Escolasalunosfaltas();
		$row = $escolasalunosfaltas->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_cadastro['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$escolasalunosfaltas->save($row);
						
			die("OK");
		}	
		die('ERRO');
	}

	public function setmesAction() {
		$this->_helper->layout->disableLayout();
		
		$mes = $this->getRequest()->getPost("mes");
    	$ns = new Zend_Session_Namespace('default_datafaltas');
		
		$ns->mesatual = $mes;
		$mesatual = $ns->mesatual;
		die(Mn_Util::getMeses($mesatual));
	}



	public function addplanoAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');;
		$idescolavinculo = (int)$this->getRequest()->getPost('idescolavinculo');
		$idmateria = (int)$this->getRequest()->getPost('idmateria');
		$idprofessor = (int)$this->getRequest()->getPost('idprofessor');

		$titulo = $this->getRequest()->getPost('titulo');
		$texto = $this->getRequest()->getPost('texto');

		if(Escolasusuarios::getUsuario('professor')=="Sim"){
			$idprofessor = Escolasusuarios::getUsuario('id');
		}
		
		$dia = $this->getRequest()->getPost('dia');
		$_dia = $dia . '/' . date('m') . '/' . date('Y');
		$data = Mn_Util::stringToTime($_dia);
		$tipo = $this->getRequest()->getPost('tipo');
		$status = "Ativo";
		
		$erros = "";
		
		if (0==$dia) $erros = "Informe o dia.";
		if (0==$idescola) $erros = "Informe a Escola.";
		if (0==$idmateria) $erros = "Informe a Matéria.";
		if (0==$idprofessor) $erros = "Informe a Professor.";

		if ($titulo == "") $erros = "Informe o Título.";
		if ($texto == "") $erros = "Informe o Texto.";
		
		if (""==$status) $erros = "Informe a Status.";
		$obj = new stdClass();
		$obj->id = 0;
		$obj->data = "ERRO";
		$obj->mensagem = $erros;
		$obj->titulo = "Erro!";
		if($erros!=""){
			echo json_encode($obj);
			die();			
		}

		
		$planosaulas = new Planosaulas();
		$plano = $planosaulas->fetchRow(" excluido = 'nao' AND idescolavinculo = " . $idescolavinculo . " AND idmateria = " . $idmateria . " AND idprofessor = " . $idprofessor);
		$dados = array();
		if(isset($plano['id'])){
		//	$dados['id'] = $plano['id'];	
		}
		//
		
		$dados["idescola"] = $idescola;
		$dados["idescolavinculo"] = $idescolavinculo;
		$dados["idmateria"] = $idmateria;
		$dados["idprofessor"] = $idprofessor;
		$dados["titulo"] = $titulo;
		$dados["texto"] = $texto;
		$dados["data"] = date("Y-m-d", $data);
		$dados["status"] = $status;
		$dados['excluido'] = 'nao';
		$dados['logusuario'] = $this->_cadastro['id'];;
		$dados['logdata'] = date('Y-m-d G:i:s');
				
		$row = $planosaulas->save($dados);

		$obj->id = $row['id'];
		$obj->data = "OK";
		$obj->mensagem = "Plano de aula adicionado com Sucesso!";
		$obj->titulo = "Sucesso!";

		echo json_encode($obj);
		die();
	}

    /**
     * Atribui valores ao view
     * @param int $idescolasalunosfalta
     */    
    private function preForm($idescolasalunosfalta = 0) {

    	$escolasconfiguracoes = new Escolasconfiguracoes();
    	$queries = array();
    	$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	$escolas = $escolasconfiguracoes->getEscolasconfiguracoes($queries);
    	$_escolas = array();
    	if(sizeof($escolas) > 0) $_escolas = $escolas[0];
    	$this->view->configuracoesnotas = $_escolas;

		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
    	//$this->view->professores = $funcionariosgeraisescolas->getFuncionariosgeraisescolas(array('idescola'=>Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'), 'status'=>'Ativo', 'professor'=>'Sim'));


    	
    	$mesatual = date("m");
    	$ns = new Zend_Session_Namespace('default_datafaltas');
		if(isset($ns->mesatual)){
			$mesatual = $ns->mesatual;
		}

		$ano = date("Y"); // Ano atual
    	$this->view->mesatual = $mesatual;
    	$this->view->ultimadia = date("t", mktime(0,0,0,$mesatual,'01',$ano));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_escolasalunosfalta = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)trim($this->getRequest()->getPost("idescola"));
$idescolavinculo = (int)trim($this->getRequest()->getPost("idescolavinculo"));
$idmateria = (int)trim($this->getRequest()->getPost("idmateria"));
$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
$idaluno = (int)trim($this->getRequest()->getPost("idaluno"));
$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
if (0==$idmateria) array_push($erros, "Informe a Matéria.");
if (0==$idprofessor) array_push($erros, "Informe a Professor.");
if (0==$idaluno) array_push($erros, "Informe a Aluno.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$escolasalunosfaltas = new Escolasalunosfaltas();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idescola"] = $idescola;
$dados["idescolavinculo"] = $idescolavinculo;
$dados["idmateria"] = $idmateria;
$dados["idprofessor"] = $idprofessor;
$dados["idaluno"] = $idaluno;
$dados["data"] = date("Y-m-d", $data);
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_cadastro['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $escolasalunosfaltas->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}