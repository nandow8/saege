<?php

class EscolassalasalunosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacotesalaaluno
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("escolassalasalunos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}	
	

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Escolassalasalunos();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Usuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Atribuição excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="escolassalasalunos") $objs = new Escolassalasalunos();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Usuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Escolassalasalunos();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Escolassalasalunos();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect($this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Atribuição')
		);
		
		
		$ns = new Zend_Session_Namespace('default_salasalunos');
		$salasalunos = new Escolassalasalunos();
		$queries = array();	
		$queries['idsecretaria'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
		$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$this->preForm();
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/admin/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		//if ((isset($this->view->post_var['idescola'])) && ($this->view->post_var['idescola']!='')) $queries['idescola'] = $this->view->post_var['idescola'];
    		if ((isset($this->view->post_var['idserie'])) && ($this->view->post_var['idserie']!='')) $queries['idserie'] = $this->view->post_var['idserie'];
    		if ((isset($this->view->post_var['status'])) && ($this->view->post_var['status']!='')) $queries['status'] = $this->view->post_var['status'];
    		if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $salasalunos->getSalasalunos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $salasalunos->getSalasalunos($queries, $paginaAtual, $maxpp);	
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolassalasalunos',	'name' => 'Atribuições'),
			array('url' => null,'name' => 'Visualizar série')
		);
	
		$id = (int)$this->_request->getParam("id");
		$salasalunos = new Escolassalasalunos();
		$salaaluno = $salasalunos->getSalaalunoById($id);
		
		if (!$salaaluno) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->visualizar = true;
		$this->view->post_var = $salaaluno;
                
		$this->preForm();
	
		
	}
	
	/**
	 * 
	 * Action de edição de salaaluno
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolassalasalunos',	'name' => 'Atribuições'),
			array('url' => null,'name' => 'Editar série')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$salasalunos = new Escolassalasalunos();
		$salaaluno = $salasalunos->getSalaalunoById($id);
		
		if (!$salaaluno) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $salaaluno;
                

		
		$queries = array();		
		$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 	;

		$queriessalas = array();	
		$queriessalas['idescola'] = $idescola;
		$salasalunos = new Escolassalasalunos();
		//$ids = $salasalunos->getIdsSeries($idescola);	
    	$ids = false;
		$series = new Escolasseries();
    	$this->view->series = $series->getEscolasseries(array('idescola'=>$idescola, 'status'=>'Ativo', 'not_id' => $ids )); 

		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($salaaluno);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Atribuição editada com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de salasalunos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolassalasalunos',	'name' => 'Atribuições'),
			array('url' => null,'name' => 'Adicionar série')
		);	
				
		$queries = array();		
		$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 	;

		$queriessalas = array();	
		$queriessalas['idescola'] = $idescola;
		$salasalunos = new Escolassalasalunos();
		$ids = $salasalunos->getIdsSeries($idescola);	
    	$ids = false;
		$series = new Escolasseries();
    	$this->view->series = $series->getEscolasseries(array('idescola'=>$idescola, 'status'=>'Ativo', 'not_id' => $ids )); 

		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Atribuição adicionada com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
	public function setalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescolasalasaluno = (int)$this->getRequest()->getPost('idescolasalasaluno');
    	$this->view->idescolasalasaluno = $idescolasalasaluno;
		
    	$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 	
    	$idserie = $this->getRequest()->getPost('idserie');
    	$idperiodo = $this->getRequest()->getPost('idperiodo');
    	$alunocomplete = strip_tags(trim($this->getRequest()->getPost('alunocomplete')));
    	$notids = strip_tags(trim($this->getRequest()->getPost('notids')));
    	$notids = explode(',', $notids);
    	$this->view->notids = $notids;
    	$this->view->idserie = $idserie;
    	$classificacao = $this->getRequest()->getPost('classificacao');
    	if(((int)$idescola <= 0) || ((int)$idserie <= 0)){
    		$this->view->rows = array();	
    		return ;
    	}
		$alunos = new Escolasalunos();
    	$this->view->rows = $alunos->getEscolasalunos(array('idescola'=>$idescola, 'idserie'=>$idserie, 'idperiodo'=>$idperiodo, 'status'=>'Ativo', 'idclassificacao'=>$classificacao, 'alunocomplete'=>$alunocomplete));
    	
	}
	
	public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$action = $this->getRequest()->getPost('action');
		$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 	;

		$queriessalas = array();	
		$queriessalas['idescola'] = $idescola;
		$salasalunos = new Escolassalasalunos();
		$ids = $salasalunos->getIdsSeries($idescola);	
    	if($action=='editar') $ids = false;
		$series = new Escolasseries();
    	$this->view->rows = $series->getEscolasseries(array('idescola'=>$idescola, 'status'=>'Ativo', 'not_id' => $ids )); 
	}
	
	//
	public function setseriesindexAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 	
    	
		$series = new Escolasseries();
    	$this->view->rows = $series->getEscolasseries(array('idescola'=>$idescola, 'status'=>'Ativo')); 
	}
	
	public function setvinculosAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescolasalasaluno = (int)$this->getRequest()->getPost('idescolasalasaluno');
		$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 	
		$idserie = $this->getRequest()->getPost('idserie');	
		$idperiodo = $this->getRequest()->getPost('idperiodo');	
		
    	if(((int)$idescola <= 0) || ((int)$idserie <= 0)){
    		$this->view->rows = array();	
    		return ;
    	}
		$vinculos = new Escolasvinculos();
    	$this->view->rows = $vinculos->getEscolasvinculos(array('idescola'=>$idescola, 'idserie'=>$idserie, 'idperiodo'=>$idperiodo, 'status'=>'Ativo'));

    	$this->view->idescolasalasaluno = $idescolasalasaluno;
    	
    	
	}
	
	public function excluiritemAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$iditem = $this->getRequest()->getPost('iditem');
		$idserie = $this->getRequest()->getPost('idserie');
    	
		$atribuicoes = new Escolassalasatribuicoes();
    	$row = $atribuicoes->getSalaatribuicaoById($iditem, array('idserie'=>$idserie)); 
    	if(!$row) die('ERRO!');
    	
		$row['excluido'] = 'sim';
		$row['logusuario'] = Usuarios::getUsuario('id');
		$row['logdata'] = date('Y-m-d G:i:s');
		
		$atribuicoes->save($row);
    	
    	die('OK');
	}
	
	public function verificadisponibilidadeAction() {
		$this->_helper->layout->disableLayout();
		
		$idvinculo = $this->getRequest()->getPost('idvinculo');
		$idsala = $this->getRequest()->getPost('idsala');
		$idperiodo = $this->getRequest()->getPost('idperiodo');
		
		$vinculos = new Escolasvinculos();
    	$row = $vinculos->getEscolavinculoById($idvinculo, array('idsala'=>$idsala));
    	
    	$escolaalunos = new Escolassalasatribuicoes();
    	$total_alunos = $escolaalunos->getSalasatribuicoes(array('idvinculo'=>$idvinculo, 'idsala'=>$idsala, 'idperiodo'=>$idperiodo, 'total'=>true));
    	
    	if(!$row) die('ERRO!');
    	if(($total_alunos) >= ($row['quantidade'])) die('A Sala já atingiu a sua capacidade MÁXIMA ('.$total_alunos.')');
    	
    	die('disponivel');
	}
	
	public function setclassificacoesAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 	
    	
		$classificacoes = new Escolasalunosclassificacoes();
    	$this->view->rows = $classificacoes->getClassificacoes(array('idescola'=>$idescola, 'status'=>'Ativo')); 
	}
	
	public function setperiodosAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = $this->getRequest()->getPost('idescola');
    	
		$periodos = new Escolasperiodos();
    	$this->view->rows = $periodos->getEscolasperiodos(array('idescola'=>$idescola, 'status'=>'Ativo')); 
	}
    
    /**
     * Atribui valores ao view
     * @param int $idsalaaluno
     */    
    private function preForm($idsalaaluno = 0) {        
		$this->view->idsecretaria = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria'); 
		$this->view->escolas = Escolas::getEscolasHelper(array('status' => 'Ativo', 'idsecretaria' => $this->view->idsecretaria));
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 
		$idserie = trim($this->getRequest()->getPost("idserie"));
		$idperiodo = trim($this->getRequest()->getPost("idperiodo"));
		$status = strip_tags(trim($this->getRequest()->getPost("status1")));
		
		$idsitens = $this->getRequest()->getPost("idsitens");
		$idssalas = $this->getRequest()->getPost("idssalas");
		$idsvinculos = $this->getRequest()->getPost("idsvinculos");
		$idsperiodos = $this->getRequest()->getPost("idsperiodos");
		$idsalunos = $this->getRequest()->getPost("idsalunos");		
		//$idsalunos = implode($idsalunos, ',');
		
		$erros = array();
		//if (""==$idsala) array_push($erros, 'Preencha o campo SÉRIE.');		
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
                
		$salasalunos = new Escolassalasalunos();
		//$row = $salasalunos->fetchRow("excluido='nao' AND idescola='$idescola' AND salaaluno = '$salaaluno' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma SALA para essa ESCOLA.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {		
                        
			$dados = array();           
			$dados['id'] = $id;
            $dados['idescola'] = $idescola;
            $dados['idserie'] = $idserie;
            $dados['idperiodo'] = $idperiodo;          
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Usuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
                        
			$itens = array();
			
			$itens['idsitens'] = $idsitens;
			$itens['idsvinculos'] = $idsvinculos;
			$itens['idssalas'] = $idssalas;
			$itens['idsperiodos'] = $idsperiodos;
			$itens['idsalunos'] = $idsalunos;			
			$dados['itens'] = $itens;
			
			$row = $salasalunos->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }
	
	
}