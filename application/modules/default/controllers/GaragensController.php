<?php

class GaragensController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacotesaida
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("garagens", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}	
	

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Garagens();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Escolasusuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Saída excluída com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="garagens") $objs = new Garagens();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Escolasusuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Garagens();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Garagens();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Garagens')
		);
		
		
		$ns = new Zend_Session_Namespace('default_garagens');
		$garagens = new Garagens();
		$queries = array();	
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	


    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = isset(($v)) ? trim($v) : "";
			if ($this->view->post_var["titulo"]!="") $queries["titulo"] = $this->view->post_var["titulo"];
			if ($this->view->post_var["descricoes"]!="") $queries["descricoes"] = $this->view->post_var["descricoes"];
			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
			
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
			    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $garagens->getGaragens($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $garagens->getGaragens($queries, $paginaAtual, $maxpp);	
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'garagens',	'name' => 'Garagens'),
			array('url' => null,'name' => 'Visualizar garagem')
		);	
		//$this->_usuario['idescola']		
		$id = (int)$this->_request->getParam("id");
		$garagens = new Garagens();
		$garagem = $garagens->getGaragemById($id);
		
		if (!$garagem) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $garagem;
		$this->preForm();
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de garagem
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'garagens',	'name' => 'Garagens'),
			array('url' => null,'name' => 'Editar garagem')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$garagens = new Garagens();
		$garagem = $garagens->getGaragemById($id);

		//$this->_usuario['idescola']
		if (!$garagem) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $garagem;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($garagem);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Garagem editada com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de garagens 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'garagens',	'name' => 'Garagens'),
			array('url' => null,'name' => 'Adicionar garagem')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Garagem adicionada com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    

    
    /**
     * Atribui valores ao view
     * @param int $identrada
     */    
    private function preForm($identrada = 0) {
    	
    	$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	
		$rows = new Departamentosescolas();
    	$this->view->departamentosescolas = $rows->getDepartamentosescolas(array('status'=>'Ativo', 'idescola'=>$idescola)); 
    	
    	$entradas = new Almoxarifado_Entradasitens();
    	$this->view->entradas = $entradas->getEntradasitens(array('status'=>'Ativo', 'idescola'=>$idescola)); 
    	//var_dump($this->view->entradas); die();
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idendereco = (int)trim($this->getRequest()->getPost("idendereco"));	
		$idfrotas = (int)trim($this->getRequest()->getPost("idfrotas"));	
		$titulo = trim($this->getRequest()->getPost("titulo"));
		$descricoes = trim($this->getRequest()->getPost("descricoes"));	
		$quantidade = trim($this->getRequest()->getPost("quantidade"));	
		$latitude = trim($this->getRequest()->getPost("latitude"));	
		$longitude = trim($this->getRequest()->getPost("longitude"));	
		$status = strip_tags(trim($this->getRequest()->getPost("status1")));
		
		$erros = array();

		if (""==$quantidade) array_push($erros, 'Selecione um quantidade.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$garagens = new Garagens();
		//$row = $garagens->fetchRow("excluido='nao' AND garagem='$garagem' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma ESCOLA com esse NOME.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			
			$dados = array();	
			$dados['id'] = $id;
			$enderecosModel = new EnderecoModel($this);
			$endereco = $enderecosModel->getPost( ($_registro) ? (int)$_registro['idendereco'] : 0, "e_idendereco");
			$idendereco = ($endereco) ? $endereco['id'] : 0;

			$dados['idendereco'] = $idendereco;
			$dados['idfrotas'] = $idfrotas;
			$dados['titulo'] = $titulo;
			$dados['descricoes'] = $descricoes;
			$dados['quantidade'] = $quantidade;
			$dados['latitude'] = $latitude;
			$dados['longitude'] = $longitude;

			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Escolasusuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			
			
			$row = $garagens->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }
}