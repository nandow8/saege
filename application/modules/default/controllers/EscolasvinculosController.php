<?php

/**
 * Controle da classe escolasvinculos do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class EscolasvinculosController extends Zend_Controller_Action {

    /**
     * Propriedade protegida que contem os dados do usário logado
     * @var Escolavinculo
     */
    protected $_usuario = null;

    /**
     * Verificação de permissao de acesso
     */
    public function preDispatch() {
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($loginNameSpace->escolausuario))
            $this->_redirect('admin' . "/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));

        Mn_Util::blockDefaultAccess("escolasvinculos", $this->_request->getActionName());

        $this->_usuario = unserialize($loginNameSpace->escolausuario);

        $messageNameSpace = new Zend_Session_Namespace("message");
        if ($messageNameSpace->crudmessage) {
            $this->view->crudMessage = $messageNameSpace->crudmessage;
            unset($messageNameSpace->crudmessage);
        }
    }

    /**
     * 
     * Action para ser consultada via ajax e excluir a entidade
     */
    public function excluirxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->_request->getPost("id");

        $rows = new Escolasvinculos();
        $row = $rows->fetchRow("id=" . $id);

        if ($row) {
            $row = $row->toArray();
            $row['excluido'] = 'sim';
            $row['logusuario'] = $this->_usuario['id'];
            $row['logdata'] = date('Y-m-d G:i:s');

            $rows->save($row);

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Vínculo excluído com sucesso.";

            die("OK");
        }

        die("Não encontrado!");
    }

    /**
     * Action para modificar o status via Ajax
     */
    public function changestatusxmlAction() {
        $this->_helper->layout->disableLayout();
        $this->_response->setHeader("content-type", "text/xml");

        $id = (int) $this->getRequest()->getPost("id");
        $op = $this->getRequest()->getPost("op");

        if ($op == "escolasvinculos")
            $objs = new Escolasvinculos();
        $obj = $objs->fetchRow("excluido='nao' AND  id=" . $id);
        if ($obj) {
            $obj = $obj->toArray();
            $obj['status'] = ($obj['status'] == "Ativo") ? "Bloqueado" : "Ativo";
            $obj['logusuario'] = $this->_usuario['id'];
            $obj['logdata'] = date('Y-m-d G:i:s');

            $objs->save($obj);

            die($obj['status']);
        }

        die("Não encontrado!");
    }

    /**
     *
     * Action de edição de perfil de acesso
     */
    public function visualizarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasvinculos', 'name' => 'Vínculos'),
            array('url' => null, 'name' => 'Visualizar Vínculo')
        );

        $id = (int) $this->_request->getParam("id");
        $escolasvinculos = new Escolasvinculos();
        $escolavinculo = $escolasvinculos->getEscolavinculoById($id, array());

        if (!$escolavinculo)
            $this->_redirect($this->getRequest()->getControllerName());

        $this->view->post_var = $escolavinculo;
        $this->preForm();

        $this->view->visualizar = true;
        return true;
    }

    /**
     * Listagem
     */
    public function indexAction() {
        $this->view->bread_crumb = array(
            array('url' => false, 'name' => 'Vínculos')
        );

        $ns = new Zend_Session_Namespace('default_escolasvinculos');
        $escolasvinculos = new Escolasvinculos();
        $queries = array();
        $queries['idsecretaria'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
        $queries['idescola'] = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
        $queries['not_idprofessor'] = true;
        //PESQUISA
        if ($this->getRequest()->isPost()) {
            $ns->pesquisa = serialize($_POST);
            $this->_redirect($this->getRequest()->getControllerName());
            die();
        }

        if (isset($ns->pesquisa))
            $this->view->post_var = unserialize($ns->pesquisa);

        if (isset($this->view->post_var)) {
            foreach ($this->view->post_var as $k => $v)
                $this->view->post_var[$k] = trim($v);


            if ($this->view->post_var["idserie"] != "")
                $queries["idserie"] = $this->view->post_var["idserie"];
            if ($this->view->post_var["idturma"] != "")
                $queries["idturma"] = $this->view->post_var["idturma"];
            if ($this->view->post_var["status1"] != "")
                $queries["status"] = $this->view->post_var["status1"];
            if ($this->view->post_var["classe1"] != "")
                $queries["classe"] = $this->view->post_var["classe1"];
            if ($this->view->post_var['sorting'] != '')
                $queries['sorting'] = $this->view->post_var['sorting'];
        }

        //PAGINACAO
        $maxpp = 20;

        $paginaAtual = (int) $this->getRequest()->getParam('p');
        if ($paginaAtual > 0)
            $ns->paginaAtual = $paginaAtual;
        $paginaAtual = isset($ns->paginaAtual) ? (int) $ns->paginaAtual : 1;
        if ($paginaAtual == 0)
            $paginaAtual = 1;

        $queries['total'] = true;
        $totalRegistros = $escolasvinculos->getEscolasvinculos($queries);
        $paginaTotal = ceil($totalRegistros / $maxpp);
        $queries['total'] = false;
        if ($paginaAtual > $paginaTotal)
            $paginaAtual = $paginaTotal;
        $paginaAtual--;
        if ($paginaAtual < 0)
            $paginaAtual = 0;

        $this->view->pagina_atual = $paginaAtual + 1;
        $this->view->maxpp = $maxpp;
        $this->view->total_registros = $totalRegistros;
        $this->view->pagina_total = $paginaTotal;

        $this->view->rows = $escolasvinculos->getEscolasvinculos($queries, $paginaAtual, $maxpp);
    }

    /**
     * 
     * Action de edição de escolasvinculos
     */
    public function editarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasvinculos', 'name' => 'Vínculos'),
            array('url' => null, 'name' => 'Editar Vínculo')
        );

        $id = (int) $this->_request->getParam("id");
        $escolasvinculos = new Escolasvinculos();
        $escolavinculo = $escolasvinculos->getEscolavinculoById($id);

        if (!$escolavinculo)
            $this->_redirect($this->getRequest()->getControllerName());

        $this->view->post_var = $escolavinculo;
        $this->preForm();

        if ($this->_request->isPost()) {
            $erros = $this->getPost($escolavinculo);
            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Vínculo editado com sucesso.";

            $this->_redirect($this->getRequest()->getControllerName());
        }

        return true;
    }

    /**
     * 
     * Action de adição de escolasvinculos 
     */
    public function adicionarAction() {
        $this->view->bread_crumb = array(
            array('url' => 'escolasvinculos', 'name' => 'Vínculos'),
            array('url' => null, 'name' => 'Adicionar Vínculo')
        );

        $this->preForm();
        if ($this->getRequest()->isPost()) {
            $erros = $this->getPost(false);

            if ($erros != "") {
                $this->view->erros = $erros;
                return false;
            }

            $message = new Zend_Session_Namespace("message");
            $message->crudmessage = "Vínculo adicionado com sucesso.";

            $this->_redirect($this->getRequest()->getControllerName());
        }

        return true;
    }

    public function setsalasAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

        $salas = new Escolassalas();
        $this->view->rows = $salas->getEscolassalas(array('idescola' => $idescola, 'status' => 'Ativo'));
    }

    public function setseriesAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

        $series = new Escolasseries();
        $this->view->rows = $series->getEscolasseries(array('idescola' => $idescola, 'status' => 'Ativo'));
    }

    public function setturmasAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

        $turmas = new Escolasturmas();
        $this->view->rows = $turmas->getEscolasturmas(array('idescola' => $idescola, 'status' => 'Ativo'));
    }

    public function setperiodosAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

        $periodos = new Escolasperiodos();
        $this->view->rows = $periodos->getEscolasperiodos(array('idescola' => $idescola, 'status' => 'Ativo'));
    }

    public function setmateriasAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idprofessor = $this->getRequest()->getPost('idprofessor');
        if ((int) $idprofessor <= 0)
            return $this->view->rows = array();
        $materias = new Controleprofessoresmaterias();
        $this->view->rows = $materias->getControleprofessoresmaterias(array('idprofessor' => $idprofessor, 'left_materias' => true));
    }

    public function setfuncionariosescolasAction() {
        $this->_helper->layout->disableLayout();

        $queries = array();
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

        $usuarios = new Escolasusuarios();
        $this->view->rows = $usuarios->getEscolasusuarios(array('idescola' => $idescola, 'status' => 'Ativo', 'professor' => 'Sim'));
    }

    /**
     * Atribui valores ao view
     * @param int $idvinculo
     */
    private function preForm($idvinculo = 0) {

        $this->view->idsecretaria = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
        $this->view->escolas = Escolas::getEscolasHelper(array('status' => 'Ativo', 'idsecretaria' => Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria')));
        $this->view->identificadoresescolas = Gdae_Identificadoresescolas::getIdentificadoresHelper(array('status' => 'Ativo'));


        $queries = array();
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

        $salas = new Escolassalas();
        $this->view->salas = $salas->getEscolassalas(array('idescola' => $idescola, 'status' => 'Ativo'));

        $queries = array();
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
        $series = new Escolasseries();
        $this->view->series = $series->getEscolasseries(array('idescola' => $idescola, 'status' => 'Ativo'));

        $queries = array();
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
        $turmas = new Escolasturmas();
        $this->view->turmas = $turmas->getEscolasturmas(array('idescola' => $idescola, 'status' => 'Ativo'));

        $queries = array();
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

        $periodos = new Escolasperiodos();
        $this->view->periodos = $periodos->getEscolasperiodos(array('idescola' => $idescola, 'status' => 'Ativo'));

        $queries = array();
        $idprofessor = $this->getRequest()->getPost('idprofessor');
        if ((int) $idprofessor <= 0)
            return $this->view->rows = array();
        $materias = new Controleprofessoresmaterias();
        $this->view->materias = $materias->getControleprofessoresmaterias(array('idprofessor' => $idprofessor, 'left_materias' => true));

        $queries = array();
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');

        $usuarios = new Escolasusuarios();
        $this->view->rows = $usuarios->getEscolasusuarios(array('idescola' => $idescola, 'status' => 'Ativo', 'professor' => 'Sim'));
    }

    /**
     * Valida e grava os dados do formulário
     */
    private function getPost($_escolavinculo = false) {
        if (!isset($this->view->post_var))
            $this->view->post_var = $_POST;
        else
            $this->view->post_var = array_merge($this->view->post_var, $_POST);

        $id = (int) $this->getRequest()->getPost("id");
        $idsecretaria = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
        $idescola = (int) Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
        $idsala = (int) trim($this->getRequest()->getPost("idsala"));
        $idserie = (int) trim($this->getRequest()->getPost("idserie"));
        $idturma = (int) trim($this->getRequest()->getPost("idturma"));
        $idperiodo = (int) trim($this->getRequest()->getPost("idperiodo"));

        $idsmaterias = $this->getRequest()->getPost("idsmaterias");
        $idsmaterias = implode(',', $idsmaterias);
//$idprofessor = (int)trim($this->getRequest()->getPost("idprofessor"));
        $idtipoensino = (int) trim($this->getRequest()->getPost("idtipoensino"));
        $inicioaulas = Mn_Util::stringToTime($this->getRequest()->getPost("inicioaulas"));
        $terminoaulas = Mn_Util::stringToTime($this->getRequest()->getPost("terminoaulas"));
        $turno = trim($this->getRequest()->getPost("turno"));
        $ano = trim($this->getRequest()->getPost("ano"));
        $integracao = trim($this->getRequest()->getPost("integracao"));
        $status = trim($this->getRequest()->getPost("status1"));
        $classe = (int)trim($this->getRequest()->getPost("classe1"));


        $erros = array();

        if (0 == $idescola)
            array_push($erros, "Informe a Escolas.");
        if (0 == $idsala)
            array_push($erros, "Informe a Sala.");
        if (0 == $idserie)
            array_push($erros, "Informe a Série.");
        if (0 == $idturma)
            array_push($erros, "Informe a Turma.");
        if (0 == $idperiodo)
            array_push($erros, "Informe a Período.");
//if (0==$idprofessor) array_push($erros, "Informe a Professor.");
        if (0 == $idtipoensino)
            array_push($erros, "Informe a Tipos.");
        if ("" == $inicioaulas)
            array_push($erros, "Informe a Ínicio das aulas.");
        if ("" == $terminoaulas)
            array_push($erros, "Informe a Término das aulas.");
        if ("" == $status)
            array_push($erros, "Informe a Status.");


        $escolasvinculos = new Escolasvinculos();



        if (sizeof($erros) > 0)
            return $erros;

        $db = Zend_Registry::get('db');
        $db->beginTransaction();
        try {
            $dados = array();
            $dados['id'] = $id;

            $dados["idsecretaria"] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
            ;
            $dados["idescola"] = $idescola;
            $dados["idsala"] = $idsala;
            $dados["idserie"] = $idserie;
            $dados["idturma"] = $idturma;
            $dados["idperiodo"] = $idperiodo;
            $dados["idsmaterias"] = $idsmaterias;
//$dados["idprofessor"] = $idprofessor;
            $dados["idtipoensino"] = $idtipoensino;
            $dados["inicioaulas"] = date("Y-m-d", $inicioaulas);
            $dados["terminoaulas"] = date("Y-m-d", $terminoaulas);
            $dados["turno"] = $turno;
            $dados["ano"] = $ano;
//$dados["integracao"] = $integracao;
            $dados["integracao"] = "Não";
            $dados["status"] = $status;


            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $this->_usuario['id'];
            ;
            $dados['logdata'] = date('Y-m-d G:i:s');

            $row = $escolasvinculos->save($dados);

            $db->commit();
        } catch (Exception $e) {
            echo $e->getMessage();

            $db->rollBack();
            die();
        }

        return "";
    }

    private function getArquivo($filename) {
        $idarquivo = false;
        $arquivos = new Arquivos();

        try {
            $idarquivo = $arquivos->getArquivoFromForm($filename);
        } catch (Exception $e) {
            $idarquivo = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_arquivo = trim($this->getRequest()->getPost("excluir_" . $filename));

        if ($excluir_arquivo == 'excluir')
            $idarquivo = -1;

        return $idarquivo;
    }

    private function getImagem($imagem, $apenas_copia = false) {
        $idimagem = false;
        $imagens = new Imagens();

        try {
            ini_set('memory_limit', '-1');
            $idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
        } catch (Exception $e) {
            $idimagem = false;
            array_push($erros, $e->getMessage());
        }

        $excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
        if ($excluir_imagem == 'excluir_' . $imagem)
            $idimagem = -1;
        return $idimagem;
    }

}
