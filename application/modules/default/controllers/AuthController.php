<?php

/**
 * Controle da classe responsável pelas ações de autenticação
 * login e logoff do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class AuthController extends Zend_Controller_Action {

	public function preDispatch() {
		$this->_redirect("/admin/");
		$nsAdmin = new Zend_Session_Namespace('admin_mn');
		$nsDefault = new Zend_Session_Namespace('default_mn');
		
		$login = $this->getRequest()->getParam('login');
		$login = ($login=='admin'); 
		
		if ((isset($nsAdmin->usuario)) && ($login)) {
			
			$alias = $this->getRequest()->getParam('alias');			
			$escolas = new Escolas();
			$this->view->escola = $escolas->getEscolaById($alias);		

			if (!$this->view->escola) return;
			
			$admin = unserialize($nsAdmin->usuario);			
			$idperfil = $admin['idperfil'];
			
			$perfis = new UsuariosPerfis();
			$perfil = $perfis->getPerfilById($idperfil);
			
			if((!$perfil) || ($perfil['acessosmodulos']!="sim")) return;
			
			$escolasuarios = new Escolasusuarios();
			unset($loginNameSpace->escolausuario);
			
			$transfer = $escolasuarios->transferAuthFromAdmin(unserialize($nsAdmin->usuario), $this->view->escola);
			$nsDefault->escolausuario = serialize($transfer);
		}
		

	}
	
	public function prefeiturascidadesAction() {
		$this->_helper->layout->disableLayout();
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		
		/*
		$estados = new Estados();
		$this->view->estados = $estados->getEstados();
		*/

		$prefeituras = new Prefeituras();
		$this->view->estados = $prefeituras->getPrefeituras(array('status'=>'Ativo', 'groupestado'=>true));
		
		if ($this->getRequest()->isPost()) {
			if (!isset($this->view->post_var)) $this->view->post_var = $_POST;
			else $this->view->post_var = array_merge($this->view->post_var, $_POST);
			
			$idestado = strip_tags(trim($this->getRequest()->getPost('idestado')));
			$idcidade = strip_tags(trim($this->getRequest()->getPost('idcidade')));
			$cidade = strip_tags(trim($this->getRequest()->getPost('cidade')));
			
			$rows = new Prefeituras();
			$row = $rows->getPrefeituraByIdEstadoIdCidade($idestado, $idcidade);
			

			if(!$row){
				$row = $rows->getPrefeituraByIdEstadoCidade($idestado, $idcidade, array('leftenderecos'=>true));
				//var_dump($row); die();
				if(!$row){
					$row = $rows->getPrefeituraByIdEstadoCidade($idestado, $idcidade, array('leftenderecos'=>true, 'c1nome'=>$idcidade));

				}
			}
			if(!$row) {
				$this->_redirect("/auth/prefeiturascidades?redirectUrl=/");			
			}else{
				//var_dump($row); die();
				$redirectUrl = (isset($_GET['redirectUrl'])) ? $_GET['redirectUrl'] :  "/";
					
				$redirectUrl = "/";
				$loginNameSpace->prefeituracidade_default = $row;
				$this->_redirect("/auth/login?redirectUrl=" . $redirectUrl);
			}
			
		}
	}
	
	/**
	 * Recebe um post via ajax do front e retorna a mensagem correspondente ao envio ou ao erro
	 */
	public function esqueciAction() {
		$this->_helper->layout->disableLayout();
		$email = strip_tags(trim($this->getRequest()->getPost("email")));
		if ($email) {
			
			$usuarios = new Escolasusuarios();
			$usuario = $usuarios->fetchRow("email='$email' AND excluido='nao'");
			
			if (!$usuario) {
				$this->view->mensagem = "O E-mail informado não foi encontrado!";
				return false;
			}
			
			Escolasusuarios::sendSenha($usuario["email"], $usuario["senha"]);
			
			$this->view->mensagem = "A senha de acesso foi enviada para o e-mail informado!";			
		}		
	}
	
	/**
	 * Mostra o formulário de login e recebendo a requisição POST procede a verificação de autenticidade e retorna um erro ou loga o usuário
	 */
	public function loginAction() {
		$this->_helper->layout->disableLayout();
		
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		
		if(!isset($loginNameSpace->prefeituracidade_default)) $this->_redirect("/auth/prefeiturascidades?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		$this->view->prefeitura = $loginNameSpace->prefeituracidade_default;
		
		if(isset($loginNameSpace->escolausuario)) {
			$this->_redirect("/");
		}
		
		$this->view->disableMenu = true;
		
		if ($this->getRequest()->isPost()) {
			if (!isset($this->view->post_var)) $this->view->post_var = $_POST;
			else $this->view->post_var = array_merge($this->view->post_var, $_POST);
			
			//$email = strip_tags(trim($this->getRequest()->getPost('email')));
			$rgf = strip_tags(trim($this->getRequest()->getPost('rgf')));
			$senha = strip_tags(trim($this->getRequest()->getPost('senha')));
			
			$usuarios = new Escolasusuarios();
			//$autentica = $usuarios->autentica($email, $senha);
			$autentica = $usuarios->autenticargf($rgf, $senha);
			
			if ($autentica===true) {
				$redirectUrl = (isset($_GET['redirectUrl'])) ? $_GET['redirectUrl'] :  "/";
				$this->_redirect($redirectUrl);	
				die();		
			} 
			
			$this->view->erro = $autentica;
		}
	}
	
	public function setcidadesAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idestado = $this->getRequest()->getPost('idestado');
    	
		/*
        $cidades= new Cidades();
        $row_cidade = $cidades->fetchAll("idestado=" . $idestado . " ");
    	$this->view->rows = $row_cidade;
    	*/
		$prefeituras = new Prefeituras();
		$this->view->rows = $prefeituras->getPrefeituras(array('status'=>'Ativo', 'idestado'=>$idestado, array('groupcidade'=>true)));
	}
	
	/**
	 * Procede o logou destruindo a variável de sessão e redireciona para a raiz
	 * do painel
	 */
	public function logoutAction() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		unset($loginNameSpace->escolausuario);
		unset($loginNameSpace->prefeituracidade_default);
		$this->_redirect("/");
		die();
	}	
	
	
}