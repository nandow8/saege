<?php

/**
 * Controle da classe nutricoesimcs do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class NutricoesimcsController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Nutricaoimc
	 */
	protected $_cadastro = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("nutricoesimcs", $this->_request->getActionName());	
		
		$this->_cadastro = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Nutricoesimcs();
		$row = $rows->fetchRow("id=".$id);
		
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = $this->_cadastro['id'];
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Imc excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="nutricoesimcs") $objs = new Nutricoesimcs();
		$obj = $objs->fetchRow("excluido='nao' AND  id=".$id);
		if ($obj) {
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = $this->_cadastro['id'];
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	
	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricoesimcs', 'name' => 'Imcs'),
			array('url' => null,'name' => 'Visualizar Imc')
		);
		
		$id = (int)$this->_request->getParam("id");
		$nutricoesimcs = new Nutricoesimcs();
		$nutricaoimc = $nutricoesimcs->getNutricaoimcById($id, array());
		
		if (!$nutricaoimc) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaoimc;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Imcs')
		);
		
		$ns = new Zend_Session_Namespace('default_nutricoesimcs');
		$nutricoesimcs = new Nutricoesimcs();
		$queries = array();	
		$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['filtro_imc'] = "Sim";
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect($this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idescola"]!="") $queries["idescola"] = $this->view->post_var["idescola"];
if ($this->view->post_var["idescolaaluno"]!="") $queries["idescolaaluno"] = $this->view->post_var["idescolaaluno"];
if ($this->view->post_var["peso"]!="") $queries["peso"] = $this->view->post_var["peso"];
if ($this->view->post_var["altura"]!="") $queries["altura"] = $this->view->post_var["altura"];
if ($this->view->post_var["tipoimc"]!="") $queries["tipoimc"] = $this->view->post_var["tipoimc"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $nutricoesimcs->getNutricoesimcs($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $nutricoesimcs->getNutricoesimcs($queries, $paginaAtual, $maxpp);	
	}
	
	/**
	 * 
	 * Action de edição de nutricoesimcs
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricoesimcs', 'name' => 'Imcs'),
			array('url' => null,'name' => 'Editar Imc')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$nutricoesimcs = new Nutricoesimcs();
		$nutricaoimc = $nutricoesimcs->getNutricaoimcById($id);
		
		if (!$nutricaoimc) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $nutricaoimc;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($nutricaoimc);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Imc editado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}	
			
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de nutricoesimcs 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'nutricoesimcs', 'name' => 'Imcs'),
			array('url' => null,'name' => 'Adicionar Imc')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Imc adicionado com sucesso.";
			
			$this->_redirect($this->getRequest()->getControllerName());
		}
		
		return true;		
    }	
    
	public function calculaimcAction() {
		$this->_helper->layout->disableLayout();
		
		$peso = MN_Util::trataNum(trim($this->_request->getPost("peso")));
		$altura = MN_Util::trataNum(trim($this->_request->getPost("altura")));

		if((float)$altura <=0 ) die('0');

		$imc = $peso / ($altura*$altura);
		$imc = number_format($imc, '2', ',', '.');
		die($imc);
	} 


	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function setescolasalunosAction() {
		$this->_helper->layout->disableLayout();
		
		$idescola = (int)$this->_request->getPost("idescola");
		$escolas = Escolas::getEscolaByIdHelper($idescola);
		if(!$idescola)  return $this->view->rows = false;

		$escolas = new Escolasalunos();
		$this->view->rows = $escolas->getEscolasalunos(array('idescola'=>$idescola));
		
	}

    /**
     * Atribui valores ao view
     * @param int $idnutricaoimc
     */    
    private function preForm($idnutricaoimc = 0) {
		$salasalunos = new Escolassalasalunos();
		$queries = array();	
		$queries['idsecretaria'] = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
		$queries['idescola'] = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$this->view->series = $salasalunos->getSalasalunos($queries);
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_nutricaoimc = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idsecretaria = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
$idescola = (int)Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
$idescolaaluno = (int)trim($this->getRequest()->getPost("idescolaaluno"));
$idserie = (int)trim($this->getRequest()->getPost("idserie"));
$peso = MN_Util::trataNum(trim($this->getRequest()->getPost("peso")));
$altura = MN_Util::trataNum(trim($this->getRequest()->getPost("altura")));
$imc = MN_Util::trataNum(trim($this->getRequest()->getPost("imc")));
$datamedicao = Mn_Util::stringToTime($this->getRequest()->getPost("datamedicao"));
$status = trim($this->getRequest()->getPost("status1"));
		
		
		$erros = array();
		
		if (0==$idescola) array_push($erros, "Informe a Escola.");
		if (0==$idserie) array_push($erros, "Informe a Série.");
if (0==$idescolaaluno) array_push($erros, "Informe a Alunos.");
if (""==$datamedicao) array_push($erros, "Informe a Data.");
if (""==$status) array_push($erros, "Informe a Status.");

		
		$nutricoesimcs = new Nutricoesimcs();
		
		
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
			$dados = array();
			$dados['id'] = $id;
			
			$dados["idsecretaria"] = $idsecretaria;
$dados["idescola"] = $idescola;
$dados["idescolaaluno"] = $idescolaaluno;
$dados["idserie"] = $idserie;
$dados["peso"] = $peso;
$dados["altura"] = $altura;
$dados["imc"] = $imc;
$dados["datamedicao"] = date("Y-m-d", $datamedicao);
$dados["status"] = $status;

			
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = $this->_cadastro['id'];;
			$dados['logdata'] = date('Y-m-d G:i:s');
					
			$row = $nutricoesimcs->save($dados);
			
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			
			$db->rollBack();
			die();
		}		
		
		return "";    	
    }
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}