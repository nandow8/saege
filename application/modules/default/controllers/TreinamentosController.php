<?php

/**
 * Controle da index (Admin)
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class TreinamentosController extends Zend_Controller_Action {
	
	/**
     * Verificação de Permissao de Acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("treinamentos", $this->_request->getActionName());	
		
		$this->_cadastro = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}	

		$treinamentos = new Treinamento();
		$queries = array();	
		$ultimo = $treinamentos->getUltimoTreinamento($queries);

		$sequencial = '1/' . date('Y');
		if(isset($ultimo['id'])){
			$sequencial = ($ultimo['id']+1) . '/' . date('Y', strtotime($ultimo['datacriacao']));
		}
		$this->view->sequencial = $sequencial;
	}

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Treinamento();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Escolasusuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Material excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="atividadescomplementares") $objs = new Treinamento();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Escolasusuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Treinamentos();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Treinamento();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Saídas')
		);
		
		
		$ns = new Zend_Session_Namespace('default_treinamentos');
		$treinamentos = new Treinamento();
		$queries = array();	
				
		//$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		//var_dump($_POST); die();
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ($this->view->post_var["sequencial"]!="") $queries["sequencial"] = $this->view->post_var["sequencial"];
    		if ($this->view->post_var["cursoid"]!="") $queries["cursoid"] = $this->view->post_var["cursoid"];
			if ($this->view->post_var["data"]!="") $queries["data"] = $this->view->post_var["data"];
			if ($this->view->post_var["informacoes"]!="") $queries["informacoes"] = $this->view->post_var["informacoes"];

			if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $treinamentos->getTreinamentos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $treinamentos->getTreinamentos($queries, $paginaAtual, $maxpp);
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'treinamentos',	'name' => 'Treinamentos'),
			array('url' => null,'name' => 'Visualizar Treinamento')
		);	
		//$this->_usuario['idescola']		
		$id = (int)$this->_request->getParam("id");
		$treinamentos = new Treinamento();
		$treinamentos = $treinamentos->getTreinamentoById($id);
		
		if (!$treinamentos) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $treinamentos;
		$this->preForm();
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de saida
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'treinamentos',	'name' => 'Treinamentos'),
			array('url' => null,'name' => 'Editar Treinamento')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$treinamentos = new Treinamento();
		$treinamentos = $treinamentos->getTreinamentoById($id);
		//$this->_usuario['idescola']
		if (!$treinamentos) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $treinamentos;
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($treinamentos);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Treinamento editado com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de saidas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'treinamentos',	'name' => 'Treinamentos'),
			array('url' => null,'name' => 'Adicionar Treinamento')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Treinamento adicionado com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
    
    /**
     * Atribui valores ao view
     * @param int $identrada
     */    
    private function preForm($identrada = 0) {
    	
    	$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$sequencial = strip_tags(trim($this->getRequest()->getPost("sequencial")));	
		$data = Mn_Util::stringToTime($this->getRequest()->getPost("data"));
		$status = strip_tags(trim($this->getRequest()->getPost("status1")));
		$usuarioid = (int)strip_tags(trim($this->getRequest()->getPost("usuarioid")));
		$cursoid = (int)strip_tags(trim($this->getRequest()->getPost("cursoid")));
		$local = $this->getRequest()->getPost("local");
		$informacoes = $this->getRequest()->getPost("informacoes");
		$diassemana = $this->getRequest()->getPost("diassemana");
		$diassemana = implode(',', $diassemana);
		$ano = strip_tags(trim($this->getRequest()->getPost("ano")));
		$datainicio = Mn_Util::stringToTime($this->getRequest()->getPost("datainicio"));
		$datatermino = Mn_Util::stringToTime($this->getRequest()->getPost("datatermino"));
		$horarioinicio = (trim($this->getRequest()->getPost("horarioinicio")));
		$horariofinal = (trim($this->getRequest()->getPost("horariofinal")));
		$qtdvagas = (int)strip_tags(trim($this->getRequest()->getPost("qtdvagas")));
		$datalimite = Mn_Util::stringToTime($this->getRequest()->getPost("datalimite"));
		$datacriacao = Mn_Util::stringToTime($this->getRequest()->getPost("datacriacao"));

		$erros = array();

		if (""==$status) array_push($erros, 'Selecione um STATUS.');
		
		$treinamentos = new treinamento();
		//$row = $saidas->fetchRow("excluido='nao' AND saida='$saida' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma ESCOLA com esse NOME.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {		
			$dados = array();
			$dados['id'] = $id;
			$dados['sequencial'] = $sequencial;
			$dados['data'] = date('Y-m-d', $data);	
			$dados['ano'] = $ano;
			$dados['usuarioid'] = $usuarioid;
			$dados['cursoid'] = $cursoid;
			$dados['datainicio'] = date('Y-m-d', $datainicio);
			$dados['datatermino'] = date('Y-m-d', $datatermino);
			$dados['diassemana'] = $diassemana;
			$dados['horarioinicio'] = $horarioinicio;
			$dados['horariofinal'] = $horariofinal;
			$dados['qtdvagas'] = $qtdvagas;
			$dados['local'] = $local;
			$dados['informacoes'] = $informacoes;
			$dados['datalimite'] = date('Y-m-d', $datalimite);
			$dados['datacriacao'] = date('Y-m-d G:i:s', $datacriacao);
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Escolasusuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			$row = $treinamentos->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }

    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }  
}

