<?php

/**
 * Controle da index (Admin)
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class IndexController extends Zend_Controller_Action {
	
	/**
     * Verificação de Permissao de Acesso
     */	
	public function preDispatch() {
			if ($this->getRequest()->getActionName()=='buscacepxml') return true;		
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);	
	}

	public function indexAction() {
			//var_dump(Escolasusuarios::getUsuario()); die();
		$protocolos = new Protocolos();
		$queries = array();	
		$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');	
		//$queries["origem_destino"] = "Escola";
		$this->view->protocolos = $protocolos->getProtocolos($queries, 0, 5);	
	}
	
	public function fileuploadAction() {
		error_reporting(E_ALL | E_STRICT);
	
		$upload_handler = new UploadHandler();
		
		header('Pragma: no-cache');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Content-Disposition: inline; filename="files.json"');
		header('X-Content-Type-Options: nosniff');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, DELETE');
		header('Access-Control-Allow-Headers: X-File-Name, X-File-Type, X-File-Size');
		
		switch ($_SERVER['REQUEST_METHOD']) {
		    case 'OPTIONS':
		        break;
		    case 'HEAD':
		    case 'GET':
		        $upload_handler->get();
		        break;
		    case 'POST':
		        if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
		            $upload_handler->delete();
		        } else {
		           	// $upload_handler->post();

					$arquivos = new Arquivos();
					$idarquivo = $arquivos->getArquivoFromFormsByPos('files', 0);
		        
					$result = array();
					$result['idarquivo'] = $idarquivo;
					$result['filename'] = $_FILES['files']["name"][0];
					$result['idx'] = (int)$_POST['idx'];
					
					echo '[' . json_encode ($result) . ']';		        	
		        }
		        break;
		    case 'DELETE':
		        $upload_handler->delete();
		        break;
		    default:
		        header('HTTP/1.1 405 Method Not Allowed');
		}		
		die();
	}
	
	public function buscacepAction() {
		$_cep = $this->getRequest()->getPost('cep');
		$modo = $this->getRequest()->getPost('modo');
		
		if ($modo=='gravar') {
			$ceps = new Ceps();
			$cep = $ceps->fetchRow("cep='$_cep' AND excluido='nao'");
			if ($cep) {
				$cep = $cep->toArray();
				$cep['idestado'] = Estados::getIdEstadoByUF($cep['uf']);
				die(json_encode($cep));
			}
		}
		
		$url = "http://cep.republicavirtual.com.br/web_cep.php?cep=".$_cep."&formato=json";
		
		$contents = @file_get_contents($url);
		
		$decode = json_decode($contents);
		$resultado = ((isset($decode->resultado)) && ((int)$decode->resultado==1));
		
		if (!$resultado) {
			die('false');
		}
		
		if ($modo=='gravar') {
			$ceps = new Ceps();
			
			$dados = array();
			$dados['idlicenca'] = Licencas::getLicenca('id');
			$dados['cep'] = $_cep;
			$dados['cidade'] = $decode->cidade;
			$dados['uf'] = $decode->uf;
			$dados['bairro'] = $decode->bairro;
			$dados['tipo'] = $decode->tipo_logradouro;
			$dados['logradouro'] = $decode->logradouro;
			$dados['excluido'] = 'nao';
			$dados['logcadastro'] = Cadastros::getCadastro('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			$cep = $ceps->save($dados);
			
			$cep = $cep->toArray();
			$cep['idestado'] = Estados::getIdEstadoByUF($dados['uf']);
			die(json_encode($cep));
		}		
		
		die(json_encode($decode));

	}
	
}