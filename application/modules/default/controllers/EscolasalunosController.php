<?php

class EscolasalunosController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacotealuno
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->escolausuario)) $this->_redirect("/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		Mn_Util::blockDefaultAccess("escolasalunos", $this->_request->getActionName());	
		
		$this->_usuario = unserialize($loginNameSpace->escolausuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}	
	

	/**
	 * 
	 * Action para ser consultada via ajax e excluir a entidade
	 */
	public function excluirxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->_request->getPost("id");
		
		$rows = new Escolasalunos();
		$row = $rows->fetchRow("id=".$id);
		if ($row) {
			$row = $row->toArray();
			$row['excluido'] = 'sim';
			$row['logusuario'] = Escolasusuarios::getUsuario('id');
			$row['logdata'] = date('Y-m-d G:i:s');			
			
			$rows->save($row);
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Aluno excluído com sucesso.";
			
			die("OK");
		}
		
		die("Não encontrado!");
		
	}		
	
	/**
	 * Action para modificar o status via Ajax
	 */
	public function changestatusxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$id = (int)$this->getRequest()->getPost("id");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="escolasalunos") $objs = new Escolasalunos();
		$obj = $objs->fetchRow("excluido='nao' AND id=".$id);
		if ($obj) {
			
			$obj = $obj->toArray();
			$obj['status'] = ($obj['status']=="Ativo") ? "Bloqueado" : "Ativo";
			$obj['logusuario'] = Escolasusuarios::getUsuario('id');
			$obj['logdata'] = date('Y-m-d G:i:s');
			
			$objs->save($obj);
			
			die($obj['status']);
		}
		
		die("Não encontrado!");
	}		
	
	public function changeorderxmlAction() {
		$this->_helper->layout->disableLayout();
		$this->_response->setHeader("content-type", "text/xml");
		
		$idfrom = (int)$this->getRequest()->getPost("from");
		$idto = (int)$this->getRequest()->getPost("to");
		$op = $this->getRequest()->getPost("op");
		
		if ($op=="change") $objs = new Escolasalunos();
		$from = $objs->fetchRow("excluido='nao' AND id=".$idfrom);
		$to = $objs->fetchRow("excluido='nao' AND id=".$idto);
		
		if (($from) && ($to)) {
			$from = $from->toArray();
			$to = $to->toArray();
			$ordemFrom = $from["ordem"];
			$orderTo = $to["ordem"];

			$from['ordem'] = $orderTo;
			$to['ordem'] = $ordemFrom;

			$objs->save($from);
			$objs->save($to);
		}
		
		$this->view->message = "OK"; 
		$this->render("xml");
	}	
	
	public function ordemAction() {
		$ordem = (int) $this->getRequest()->getParam('ordem', 0);
		$d = $this->getRequest()->getParam('d', 0);
		
		$rows = new Escolasalunos();
		$rows->swapOrdem($ordem, $d, false);

		$this->_redirect("/".$this->_request->getControllerName()."/index");
		die();	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Aluno')
		);
		
		
		$ns = new Zend_Session_Namespace('default_alunos');
		$alunos = new Escolasalunos();
		$queries = array();	
		$this->preForm();
		$queries['idescola'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		$queries['verifica_nutricao'] = true;
		//$queries['nutricao'] = "Sim";
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = $v;
			//if ((isset($this->view->post_var['idescola'])) && ($this->view->post_var['idescola']!='')) $queries['idescola'] = $this->view->post_var['idescola'];
    		if ((isset($this->view->post_var['idsala'])) && ($this->view->post_var['idsala']!='')) $queries['idsala'] = $this->view->post_var['idsala'];
    		if ((isset($this->view->post_var['idserie'])) && ($this->view->post_var['idserie']!='')) $queries['idserie'] = $this->view->post_var['idserie'];
    		if ((isset($this->view->post_var['idclassificacao'])) && ($this->view->post_var['idclassificacao']!='')) $queries['idclassificacao'] = $this->view->post_var['idclassificacao'];
    		if ((isset($this->view->post_var['idperiodo'])) && ($this->view->post_var['idperiodo']!='')) $queries['idperiodo'] = $this->view->post_var['idperiodo'];
    		if ((isset($this->view->post_var['status'])) && ($this->view->post_var['status']!='')) $queries['status'] = $this->view->post_var['status'];
    		if ((isset($this->view->post_var['nutricao'])) && ($this->view->post_var['nutricao']!='')) $queries['nutricao'] = $this->view->post_var['nutricao'];
    		if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		
		}		
	
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $alunos->getEscolasalunos($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;  
		
		$this->view->rows = $alunos->getEscolasalunos($queries, $paginaAtual, $maxpp);	
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasalunos',	'name' => 'Alunos'),
			array('url' => null,'name' => 'Visualizar aluno')
		);
	
		$id = (int)$this->_request->getParam("id");
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById($id);
		$endereco = Enderecos::getEnderecoById($aluno['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->post_var = array_merge($this->view->post_var, $endereco);
		}	
		if (!$aluno) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		$this->view->visualizar = true;
		$this->view->post_var = $aluno;
                
		$this->preForm();
	
		
	}
	
	/**
	 * 
	 * Action de edição de aluno
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasalunos',	'name' => 'Alunos'),
			array('url' => null,'name' => 'Editar aluno')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById($id);
		
		if (!$aluno) 
			$this->_redirect('' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $aluno;
		$endereco = Enderecos::getEnderecoById($aluno['idendereco']);
		if($endereco){
			unset($endereco['id']);
			unset($endereco['excluido']);
			$this->view->post_var = array_merge($this->view->post_var, $endereco);
		}	   
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($aluno);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Aluno editado com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de alunos 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'escolasalunos',	'name' => 'Alunos'),
			array('url' => null,'name' => 'Adicionar aluno')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Aluno adicionado com sucesso.";
			
			$this->_redirect(''.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
	public function setseriesAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	
		$series = new Escolasseries();
    	$this->view->rows = $series->getEscolasseries(array('idescola'=>$idescola, 'status'=>'Ativo')); 
	}
		
	public function setperiodosAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	
		$periodos = new Escolasperiodos();
    	$this->view->rows = $periodos->getEscolasperiodos(array('idescola'=>$idescola, 'status'=>'Ativo')); 
	}
	
	public function setclassificacoesAction() {
		$this->_helper->layout->disableLayout();
		
		$queries = array();
		$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
    	
		$classificacoes = new Escolasalunosclassificacoes();
    	$this->view->rows = $classificacoes->getClassificacoes(array('idescola'=>$idescola, 'status'=>'Ativo')); 
	}
    
    /**
     * Atribui valores ao view
     * @param int $idaluno
     */    
    private function preForm($idaluno = 0) {
        
            $this->view->idsecretaria = Usuarios::getSecretariaAtiva(Escolasusuarios::getUsuario('id'), 'id');
            //$this->view->escolas = Escolas::getEscolasHelper(array('status' => 'Ativo', 'idsecretaria' => $this->view->idsecretaria));
            
            $instituicoes = new Escolasinstituicoesnecessidades();
			$queries = array();
			
			$queries['idsecretaria'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');	
			$queries['status'] = 'Ativo';
			$this->view->instituicoesapoio = $instituicoes->getInstituicoes($queries);	
			
            $programas = new Escolasprogramassociais();
			$queries = array();	
			$queries['idsecretaria'] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');	
			$queries['status'] = 'Ativo';
			$this->view->programassociais = $programas->getProgramas($queries);				
			
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id'); 
		$idendereco = strip_tags((int)$this->getRequest()->getPost("idendereco"));  
		$idclassificacao = strip_tags((int)$this->getRequest()->getPost("idclassificacao"));   
		$idarquivodatanascimento = $this->getArquivo('arquivodatanascimento');      
		$idimagemfoto = $this->getImagem(false, 'idimagemfoto');   
		$nomerazao = strip_tags(trim($this->getRequest()->getPost("nomerazao")));
		$sobrenomefantasia = strip_tags(trim($this->getRequest()->getPost("sobrenomefantasia")));
		$cpfcnpj= strip_tags(trim($this->getRequest()->getPost("cpfcnpj")));
		$nascimento = strip_tags(trim($this->getRequest()->getPost("nascimento")));
		$_senha = strip_tags(trim($this->getRequest()->getPost("nascimento")));
		$nascimento = Mn_Util::stringToTime($nascimento);		
		$nascimento = date('Y-m-d G:i:s', $nascimento);
		
		$email = strip_tags(trim($this->getRequest()->getPost("email")));
		$_senha = explode('/', $_senha);
		$senha = implode('', $_senha);
		
		//$senha = strip_tags(trim($this->getRequest()->getPost("senha")));




		$nomemae = strip_tags(trim($this->getRequest()->getPost("nomemae")));
		$cpfcnpjmae = strip_tags(trim($this->getRequest()->getPost("cpfcnpjmae")));
		$rgmae = strip_tags(trim($this->getRequest()->getPost("rgmae")));
		$nascimentomae = strip_tags(trim($this->getRequest()->getPost("nascimentomae")));
		$nascimentomae = Mn_Util::stringToTime($nascimentomae);		
		$nascimentomae = date('Y-m-d G:i:s', $nascimentomae);
		
		$nomepai = strip_tags(trim($this->getRequest()->getPost("nomepai")));
		$cpfcnpjpai = strip_tags(trim($this->getRequest()->getPost("cpfcnpjpai")));
		$rgpai = strip_tags(trim($this->getRequest()->getPost("rgpai")));
		$nascimentopai = strip_tags(trim($this->getRequest()->getPost("nascimentopai")));
		$nascimentopai = Mn_Util::stringToTime($nascimentopai);		
		$nascimentopai = date('Y-m-d G:i:s', $nascimentopai);
		
		$naturalidade = strip_tags(trim($this->getRequest()->getPost("naturalidade")));
		$nacionalidade = strip_tags(trim($this->getRequest()->getPost("nacionalidade")));
		$rg = strip_tags(trim($this->getRequest()->getPost("rg")));
		$rgm = strip_tags(trim($this->getRequest()->getPost("rgm")));

		$idserie = strip_tags(trim($this->getRequest()->getPost("idserie")));
		$idperiodo = strip_tags(trim($this->getRequest()->getPost("idperiodo")));
		$telefoneresidencial = strip_tags(trim($this->getRequest()->getPost("telefoneresidencial")));
		$telefonecontato = strip_tags(trim($this->getRequest()->getPost("telefonecontato")));
		$nomecontato = strip_tags(trim($this->getRequest()->getPost("nomecontato")));
		$matricula = strip_tags(trim($this->getRequest()->getPost("matricula")));
		$numeromatricula = strip_tags(trim($this->getRequest()->getPost("numeromatricula")));
		$ra = strip_tags(trim($this->getRequest()->getPost("ra")));
		$classificacao = strip_tags(trim($this->getRequest()->getPost("classificacao")));
		
		$cgm = strip_tags(trim($this->getRequest()->getPost("cgm")));
		$nis = strip_tags(trim($this->getRequest()->getPost("nis")));
		$sexo = strip_tags(trim($this->getRequest()->getPost("sexo")));
		//$iddatanascimento = strip_tags(trim($this->getRequest()->getPost("iddatanascimento")));
		//$idfoto = strip_tags(trim($this->getRequest()->getPost("idfoto")));
		$etnia = strip_tags(trim($this->getRequest()->getPost("etnia")));
		$quilombola = strip_tags(trim($this->getRequest()->getPost("quilombola")));
		$nova_cert_tipo_livro_reg = strip_tags(trim($this->getRequest()->getPost("nova_cert_tipo_livro_reg")));
		$ufrg = strip_tags(trim($this->getRequest()->getPost("ufrg")));
                $municipio_de_nascimento  = strip_tags(trim($this->getRequest()->getPost("municipio_de_nascimento")));
                $uf_mun_de_nascimento  = strip_tags(trim($this->getRequest()->getPost("uf_mun_de_nascimento")));
                $data_emis_certidao_nasc  = strip_tags(trim($this->getRequest()->getPost("data_emis_certidao_nasc")));
                $numero_certidao_nasc  = strip_tags(trim($this->getRequest()->getPost("numero_certidao_nasc")));
                $num_folha_certidao_nasc  = strip_tags(trim($this->getRequest()->getPost("num_folha_certidao_nasc")));
                $num_livro_certidao_nasc  = strip_tags(trim($this->getRequest()->getPost("num_livro_certidao_nasc")));
                $numero_certidao_nasc_nova  = strip_tags(trim($this->getRequest()->getPost("numero_certidao_nasc_nova")));
                $digver_certidao_nasc_nova  = strip_tags(trim($this->getRequest()->getPost("digver_certidao_nasc_nova")));

                $rendafamiliar = strip_tags(trim(Mn_Util::trataNum($this->getRequest()->getPost("rendafamiliar"))));
		$passeescolar = strip_tags(trim($this->getRequest()->getPost("passeescolar")));
		$transporteescolar = strip_tags(trim($this->getRequest()->getPost("transporteescolar")));
		$restricaoalimentar = strip_tags(trim($this->getRequest()->getPost("restricaoalimentar")));
		$observacoes = strip_tags(trim($this->getRequest()->getPost("observacoes")));

                $data_emis_certidao_nasc = strip_tags(trim($this->getRequest()->getPost("data_emis_certidao_nasc")));
		$data_emis_certidao_nasc = Mn_Util::stringToTime($data_emis_certidao_nasc);		
		$data_emis_certidao_nasc = date('Y-m-d G:i:s', $data_emis_certidao_nasc);
                
		$tituloeleitoralmae = strip_tags(trim($this->getRequest()->getPost("tituloeleitoralmae")));
		$idarquivonascimentomae = $this->getArquivo('arquivonascimentomae');
		$tituloeleitoralpai = strip_tags(trim($this->getRequest()->getPost("tituloeleitoralpai")));
		$idarquivonascimentopai = $this->getArquivo('arquivonascimentopai');
		$filiacao = strip_tags(trim($this->getRequest()->getPost("filiacao")));
		$nomeresponsavel = strip_tags(trim($this->getRequest()->getPost("nomeresponsavel")));
		$cpfcnpjresponsavel = strip_tags(trim($this->getRequest()->getPost("cpfcnpjresponsavel")));
		
		$rgresponsavel = strip_tags(trim($this->getRequest()->getPost("rgresponsavel")));
		$nascimentoresponsavel = strip_tags(trim($this->getRequest()->getPost("nascimentoresponsavel")));
		$nascimentoresponsavel = Mn_Util::stringToTime($nascimentoresponsavel);		
		$nascimentoresponsavel = date('Y-m-d G:i:s', $nascimentoresponsavel);
		$tituloeleitoralresponsavel = strip_tags(trim($this->getRequest()->getPost("tituloeleitoralresponsavel")));
		$idarquivonascimentoresponsavel = $this->getArquivo('arquivonascimentoresponsavel');  
		
		$necessidadesespeciais = strip_tags(trim($this->getRequest()->getPost("necessidadesespeciais")));
		$descricoesnecessidadesespeciais = strip_tags(trim($this->getRequest()->getPost("descricoesnecessidadesespeciais")));
		$idinstituicaoapoio = strip_tags(trim((int)$this->getRequest()->getPost("idinstituicaoapoio")));
		
		$observacaodeficiencia = strip_tags(trim((int)$this->getRequest()->getPost("observacaodeficiencia")));
		$tipodeficiencia = strip_tags(trim((int)$this->getRequest()->getPost("tipodeficiencia")));
		
		$dataefetivacao = strip_tags(trim($this->getRequest()->getPost("dataefetivacao")));
		$usuarioefetivacao = strip_tags(trim($this->getRequest()->getPost("usuarioefetivacao")));
		
		$motivo = strip_tags(trim($this->getRequest()->getPost("motivo")));
		$situacaoatual = strip_tags(trim($this->getRequest()->getPost("situacaoatual")));
		$redeorigem = strip_tags(trim($this->getRequest()->getPost("redeorigem")));
		$ensinoorigem = strip_tags(trim($this->getRequest()->getPost("ensinoorigem")));
		$situacaoanoanterior = strip_tags(trim($this->getRequest()->getPost("situacaoanoanterior")));
		$formaingresso = strip_tags(trim($this->getRequest()->getPost("formaingresso")));
		
		$estadocivilmae = strip_tags(trim($this->getRequest()->getPost("estadocivilmae")));
		$estadocivilpai = strip_tags(trim($this->getRequest()->getPost("estadocivilpai")));
		$estadocivilresponsavel = strip_tags(trim($this->getRequest()->getPost("estadocivilresponsavel")));
		$merendadiferenciada = strip_tags(trim($this->getRequest()->getPost("merendadiferenciada")));
		$merendadiferenciadaaprovacao = strip_tags(trim($this->getRequest()->getPost("merendadiferenciadaaprovacao")));
		

		$idsprogramas = (is_array($this->getRequest()->getPost("idsprogramas"))) ? array_filter($this->getRequest()->getPost("idsprogramas")) : array();
		$situacoes = (is_array($this->getRequest()->getPost("situacoes"))) ? array_filter($this->getRequest()->getPost("situacoes")) : array();
		$idsitens = (is_array($this->getRequest()->getPost("idsitens"))) ? array_filter($this->getRequest()->getPost("idsitens")) : array();

					
		$status = strip_tags(trim($this->getRequest()->getPost("status1")));
		
		/*MULTIPLOS ARQUIVOS*/
		$idsarquivos = $this->getRequest()->getPost("idsarquivos");
		$legendasarquivos =  $this->getRequest()->getPost("legendasarquivos");

		
		$erros = array();
		if (""==Mn_Util::formPostVarData($nascimento)) array_push($erros, 'Preencha o campo DATA DE NASCIMENTO.');
		if (""==$status) array_push($erros, 'Selecione um STATUS.');
                
		$alunos = new Escolasalunos();
		//$row = $alunos->fetchRow("excluido='nao' AND idescola='$idescola' AND aluno = '$aluno' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma SALA para essa ESCOLA.');
		//var_dump($erros); die();
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {		
			$idendereco = ($_registro) ? (int)$_registro['idendereco'] : 0;
			$enderecosModel = new EnderecoModel($this);
			$endereco = $enderecosModel->getPost($idendereco);
			$idendereco = ($endereco) ? $endereco['id'] : 0;
			   
			$dados = array();           
			$dados['id'] = $id;
                        $dados['idescola'] = $idescola;
                        $dados['idendereco'] = $idendereco;
                        $dados['idclassificacao'] = $idclassificacao;
                        $dados['merendadiferenciada'] = $merendadiferenciada;
                        $dados['merendadiferenciadaaprovacao'] = $merendadiferenciadaaprovacao;
                        // $dados['idarquivo'] = $idarquivo;
			$dados['nomerazao'] = $nomerazao;
			$dados['sobrenomefantasia'] = $sobrenomefantasia;
			$dados['cpfcnpj'] = $cpfcnpj;
			$dados['nascimento'] = $nascimento;
			$dados['email'] = $email;
			$dados['senha'] = $senha;
			$dados['nomemae'] = $nomemae;
			$dados['cpfcnpjmae'] = $cpfcnpjmae;
			$dados['rgmae'] = $rgmae;
			$dados['nascimentomae'] = $nascimentomae;
			$dados['nomepai'] = $nomepai;
			$dados['cpfcnpjpai'] = $cpfcnpjpai;
			$dados['rgpai'] = $rgpai;
			$dados['nascimentopai'] = $nascimentopai;
			$dados['naturalidade'] = $naturalidade;
			$dados['nacionalidade'] = $nacionalidade;
			$dados['rg'] = $rg;
			$dados['rgm'] = $rgm;
			$dados['idserie'] = $idserie;
			$dados['classificacao'] = $classificacao;
			$dados['idperiodo'] = $idperiodo;
			$dados['telefoneresidencial'] = $telefoneresidencial;
			$dados['telefonecontato'] = $telefonecontato;
			$dados['nomecontato'] = $nomecontato;
			$dados['matricula'] = $matricula;
			$dados['numeromatricula'] = $numeromatricula;
			$dados['ra'] = $ra;
			
			$dados['cgm'] = $cgm;
			$dados['nis'] = $nis;
			$dados['sexo'] = $sexo;
			$dados['idarquivodatanascimento'] = $idarquivodatanascimento;
			$dados['idimagemfoto'] = $idimagemfoto;
			
			$dados['etnia'] = $etnia;
			$dados['quilombola'] = $quilombola;
                        $dados['nova_cert_tipo_livro_reg'] = $nova_cert_tipo_livro_reg;
                        $dados['ufrg'] = $ufrg;
                        $dados['municipio_de_nascimento'] = $municipio_de_nascimento;
                        $dados['uf_mun_de_nascimento'] = $uf_mun_de_nascimento;
                        $dados['data_emis_certidao_nasc'] = $data_emis_certidao_nasc;
                        $dados['numero_certidao_nasc'] = $numero_certidao_nasc;
                        $dados['num_folha_certidao_nasc'] = $num_folha_certidao_nasc;
                        $dados['num_livro_certidao_nasc'] = $num_livro_certidao_nasc;
                        $dados['numero_certidao_nasc_nova'] = $numero_certidao_nasc_nova;
                        $dados['digver_certidao_nasc_nova'] = $digver_certidao_nasc_nova;

                        $dados['rendafamiliar'] = $rendafamiliar;
			$dados['passeescolar'] = $passeescolar;
			$dados['transporteescolar'] = $transporteescolar;
			$dados['restricaoalimentar'] = $restricaoalimentar;
			$dados['observacoes'] = $observacoes;
			
			$dados['tituloeleitoralmae'] = $tituloeleitoralmae;
			$dados['idarquivonascimentomae'] = $idarquivonascimentomae;
			$dados['tituloeleitoralpai'] = $tituloeleitoralpai;
			$dados['idarquivonascimentopai'] = $idarquivonascimentopai;
			$dados['filiacao'] = $filiacao;
			$dados['nomeresponsavel'] = $nomeresponsavel;
			$dados['cpfcnpjresponsavel'] = $cpfcnpjresponsavel;
			$dados['rgresponsavel'] = $rgresponsavel;
			$dados['nascimentoresponsavel'] = $nascimentoresponsavel;
			$dados['tituloeleitoralresponsavel'] = $tituloeleitoralresponsavel;
			$dados['idarquivonascimentoresponsavel'] = $idarquivonascimentoresponsavel;
			
			$dados['necessidadesespeciais'] = $necessidadesespeciais;
			$dados['descricoesnecessidadesespeciais'] = $descricoesnecessidadesespeciais;
			$dados['idinstituicaoapoio'] = $idinstituicaoapoio;
			
			$dados['observacaodeficiencia'] = $observacaodeficiencia;
			$dados['tipodeficiencia'] = $tipodeficiencia;
			
			$dados['dataefetivacao'] = $dataefetivacao;
			$dados['usuarioefetivacao'] = $usuarioefetivacao;
			$dados['ensinoorigem'] = $ensinoorigem;
			$dados['situacaoanoanterior'] = $situacaoanoanterior;
			$dados['formaingresso'] = $formaingresso;
			$dados['motivo'] = $motivo;
			$dados['situacaoatual'] = $situacaoatual;
			$dados['redeorigem'] = $redeorigem;

			$dados['estadocivilmae'] = $estadocivilmae;
			$dados['estadocivilpai'] = $estadocivilpai;
			$dados['estadocivilresponsavel'] = $estadocivilresponsavel;
			
			$dados['status'] = $status;
			$dados['excluido'] = 'nao';
			$dados['logusuario'] = Escolasusuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			
			$dados_programas = array();
			$dados_programas['ids'] = $idsitens;
			$dados_programas['idsprogramas'] = $idsprogramas;
			$dados_programas['situacoes'] = $situacoes;						
			$dados_programas['excluido'] = 'nao';
			$dados_programas['logusuario'] = Escolasusuarios::getUsuario('id');
			$dados_programas['logdata'] = date('Y-m-d G:i:s');
			$dados['programas'] = $dados_programas;
                        
			$row = $alunos->save($dados);
		if(($merendadiferenciada=="Sim") && ($merendadiferenciada=="Sim")){

			$rows_diferenciadas = Nutricaomerendasdiferenciadas::getNutricaomerendasdiferenciadasHelper(array('idescola'=>$idescola));
			//var_dump($rows_diferenciadas[0]['id']);die();
			if(isset($rows_diferenciadas[0]['id'])){
				$dados_merendas = array();
				$verificaid=Nutricaomerendas::getNutricaomerendasHelper(array('idaluno'=>$row['id']));
				if(isset($verificaid[0]['id'])){
					$dados_merendas['id'] = $verificaid[0]['id'];
					//die('aaa');
				}
				$dados_merendas["idsecretaria"] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
				$dados_merendas["idescola"] = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
				$dados_merendas["idaluno"] = $row['id'];
				$dados_merendas["titulo"] = $rows_diferenciadas[0]['titulo'];
				$dados_merendas["descricoes"] = $rows_diferenciadas[0]['descricoes'];
			
				$dados_merendas["status"] = $rows_diferenciadas[0]['status'];
			
				
				$dados_merendas['excluido'] = 'nao';
				$dados_merendas['logusuario'] = Escolasusuarios::getUsuario('id');
				$dados_merendas['logdata'] = date('Y-m-d G:i:s');
				//var_dump($dados_merendas); die();
				$nutricaomerendas = new Nutricaomerendas();
				$row = $nutricaomerendas->save($dados_merendas);
			}
		}
			$multiplosarquivos = new Escolasalunosarquivosmatricula(); 
			$multiplosarquivos->setArquivos($row['id'], $idsarquivos, $legendasarquivos);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }
	
    private function getArquivo($filename = 'arquivo') {
		$idarquivo = false;
		$arquivos = new Arquivos();
		try {
			$idarquivo = $arquivos->getArquivoFromForm($filename);
		} catch (Exception $e) {
			$idarquivo = false;
			array_push($erros,$e->getMessage());
		}
		
		$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
		if ($excluir_arquivo=='excluir') $idarquivo = -1;
		return $idarquivo;    	
    }
    
    private function getImagem($apenas_copia = false, $nome = false) {
		$idimagem = false;
		$imagens = new Imagens();
		try {
			ini_set('memory_limit', '-1');
			$idimagem = $imagens->getImagemFromForm($nome, NULL, NULL, $apenas_copia);
		} catch (Exception $e) {
			$idimagem = false;
			array_push($erros,$e->getMessage());
		}
		$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $nome));
		if ($excluir_imagem=='excluir_' . $nome) $idimagem = -1;
		return $idimagem;		    	
    }
}
