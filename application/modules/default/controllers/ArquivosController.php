<?php

/**
 * Controle da classe Arquivos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class ArquivosController extends Zend_Controller_Action {

	
	/**
	 * Action get
	 */
	public function getAction() {
		$id = (int)$this->_request->getParam("id");
		
		$arquivos = new Arquivos();
		$arquivo = $arquivos->downloadArquivo($id);
		
		$sourceFilename = $arquivo['sourceFilename'];
		
		$this->_helper->layout->disableLayout();
   		header('Content-Disposition: ATTACHMENT; FILENAME="'.$arquivo['filename'].'"', TRUE);
   		header("Content-Length: " . $arquivo['filesize']);
   		header("Content-Transfer-Encoding: binary");
		
		$fh = fopen($sourceFilename, "r");
		fpassthru($fh);
   		fclose($fh);
   		die();

	}	
	
}