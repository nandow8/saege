<?php

class RelPDF extends TCPDF {

	protected $headerHtml = ''; 
	protected $footerHtml = '';
	
	//Page header
    public function Header() {
        $this->writeHTMLCell('', '', '', '', $this->headerHtml);
    }

    // Page footer
    public function Footer() {
    	$this->SetY(-15);
    	
    	$html = str_replace('xx/xx', $this->getAliasNumPage().'/'.$this->getAliasNbPages(), $this->footerHtml);
    	
    	$this->writeHTMLCell('', '', '', '', $html);
    }	//
    
    
    /**
	 * @return the $headerHtml
	 */
	public function getHeaderHtml() {
		return $this->headerHtml;
	}

	/**
	 * @return the $footerHtml
	 */
	public function getFooterHtml() {
		return $this->footerHtml;
	}

	/**
	 * @param field_type $headerHtml
	 */
	public function setHeaderHtml($headerHtml) {
		$this->headerHtml = $headerHtml;
	}

	/**
	 * @param field_type $footerHtml
	 */
	public function setFooterHtml($footerHtml) {
		$this->footerHtml = $footerHtml;
	}    	
	
}