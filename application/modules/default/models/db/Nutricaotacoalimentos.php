<?php

/**
 * Define o modelo Nutricaotacoalimentos 
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Nutricaotacoalimentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "nutricaotacoalimentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getNutricaotacoalimentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaotacoalimentos = new Nutricaotacoalimentos();
		return $nutricaotacoalimentos->getNutricaotacoalimentos($queries, $page, $maxpage);
	}
	
	public function getNutricaotacoalimentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

                $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
                if ($id) array_push($where, " n1.id = $id ");

                $idtipoalimento = (isset($queries['idtipoalimento'])) ? (int)$queries['idtipoalimento'] : false;
                if ($idtipoalimento) array_push($where, " n1.idtipoalimento = $idtipoalimento ");

                $numero_alimento = (isset($queries['numero_alimento'])) ? (int)$queries['numero_alimento'] : false;
                if ($numero_alimento) array_push($where, " n1.numero_alimento = $numero_alimento ");

                $descricao = (isset($queries['descricao'])) ? $queries['descricao'] : false;
                if ($descricao) array_push($where, " n1.descricao LIKE '%$descricao%' ");

                $vl_g_poli = (isset($queries['vl_g_poli'])) ? $queries['vl_g_poli'] : false;
                if ($vl_g_poli) array_push($where, " n1.vl_g_poli = $vl_g_poli ");

                $vl_g_mon = (isset($queries['vl_g_mon'])) ? $queries['vl_g_mon'] : false;
                if ($vl_g_mon) array_push($where, " n1.vl_g_mon = $vl_g_mon ");

                $vl_g_sat = (isset($queries['vl_g_sat'])) ? $queries['vl_g_sat'] : false;
                if ($vl_g_sat) array_push($where, " n1.vl_g_sat = $vl_g_sat ");

                $vl_colest = (isset($queries['vl_colest'])) ? $queries['vl_colest'] : false;
                if ($vl_colest) array_push($where, " n1.vl_colest = $vl_colest ");

                $vl_b1 = (isset($queries['vl_b1'])) ? $queries['vl_b1'] : false;
                if ($vl_b1) array_push($where, " n1.vl_b1 = $vl_b1 ");

                $vl_b2 = (isset($queries['vl_b2'])) ? $queries['vl_b2'] : false;
                if ($vl_b2) array_push($where, " n1.vl_b2 = $vl_b2 ");

                $vl_b6 = (isset($queries['vl_b6'])) ? $queries['vl_b6'] : false;
                if ($vl_b6) array_push($where, " n1.vl_b6 = $vl_b6 ");

                $vl_b12 = (isset($queries['vl_b12'])) ? $queries['vl_b12'] : false;
                if ($vl_b12) array_push($where, " n1.vl_b12 = $vl_b12 ");

                $vl_nia = (isset($queries['vl_nia'])) ? $queries['vl_nia'] : false;
                if ($vl_nia) array_push($where, " n1.vl_nia = $vl_nia ");

                $vl_folic = (isset($queries['vl_folic'])) ? $queries['vl_folic'] : false;
                if ($vl_folic) array_push($where, " n1.vl_folic = $vl_folic ");

                $vl_pant = (isset($queries['vl_pant'])) ? $queries['vl_pant'] : false;
                if ($vl_pant) array_push($where, " n1.vl_pant = $vl_pant ");

                $vl_iodo = (isset($queries['vl_iodo'])) ? $queries['vl_iodo'] : false;
                if ($vl_iodo) array_push($where, " n1.vl_iodo = $vl_iodo ");

                $vl_mg = (isset($queries['vl_mg'])) ? $queries['vl_mg'] : false;
                if ($vl_mg) array_push($where, " n1.vl_mg = $vl_mg ");

                $vl_zinco = (isset($queries['vl_zinco'])) ? $queries['vl_zinco'] : false;
                if ($vl_zinco) array_push($where, " n1.vl_zinco = $vl_zinco ");

                $vl_mn = (isset($queries['vl_mn'])) ? $queries['vl_mn'] : false;
                if ($vl_mn) array_push($where, " n1.vl_mn = $vl_mn ");

                $vl_cobre = (isset($queries['vl_cobre'])) ? $queries['vl_cobre'] : false;
                if ($vl_cobre) array_push($where, " n1.vl_cobre = $vl_cobre ");

                $vl_selen = (isset($queries['vl_selen'])) ? $queries['vl_selen'] : false;
                if ($vl_selen) array_push($where, " n1.vl_selen = $vl_selen ");

                $vl_carb = (isset($queries['vl_carb'])) ? $queries['vl_carb'] : false;
                if ($vl_carb) array_push($where, " n1.vl_carb = $vl_carb ");

                $vl_g_tot = (isset($queries['vl_g_tot'])) ? $queries['vl_g_tot'] : false;
                if ($vl_g_tot) array_push($where, " n1.vl_g_tot = $vl_g_tot ");

                $vl_prot = (isset($queries['vl_prot'])) ? $queries['vl_prot'] : false;
                if ($vl_prot) array_push($where, " n1.vl_prot = $vl_prot ");

                $vl_fibra = (isset($queries['vl_fibra'])) ? $queries['vl_fibra'] : false;
                if ($vl_fibra) array_push($where, " n1.vl_fibra = $vl_fibra ");

                $vl_vit_a = (isset($queries['vl_vit_a'])) ? $queries['vl_vit_a'] : false;
                if ($vl_vit_a) array_push($where, " n1.vl_vit_a = $vl_vit_a ");

                $vl_vit_c = (isset($queries['vl_vit_c'])) ? $queries['vl_vit_c'] : false;
                if ($vl_vit_c) array_push($where, " n1.vl_vit_c = $vl_vit_c ");

                $vl_vit_d = (isset($queries['vl_vit_d'])) ? $queries['vl_vit_d'] : false;
                if ($vl_vit_d) array_push($where, " n1.vl_vit_d = $vl_vit_d ");

                $vl_vit_e = (isset($queries['vl_vit_e'])) ? $queries['vl_vit_e'] : false;
                if ($vl_vit_e) array_push($where, " n1.vl_vit_e = $vl_vit_e ");

                $vl_na = (isset($queries['vl_na'])) ? $queries['vl_na'] : false;
                if ($vl_na) array_push($where, " n1.vl_na = $vl_na ");

                $vl_ca = (isset($queries['vl_ca'])) ? $queries['vl_ca'] : false;
                if ($vl_ca) array_push($where, " n1.vl_ca = $vl_ca ");

                $vl_k = (isset($queries['vl_k'])) ? $queries['vl_k'] : false;
                if ($vl_k) array_push($where, " n1.vl_k = $vl_k ");

                $vl_p = (isset($queries['vl_p'])) ? $queries['vl_p'] : false;
                if ($vl_p) array_push($where, " n1.vl_p = $vl_p ");

                $vl_fe = (isset($queries['vl_fe'])) ? $queries['vl_fe'] : false;
                if ($vl_fe) array_push($where, " n1.vl_fe = $vl_fe ");

                $umidade_perc = (isset($queries['umidade_perc'])) ? $queries['umidade_perc'] : false;
                if ($umidade_perc) array_push($where, " n1.umidade_perc = $umidade_perc ");

                $energia_kcal = (isset($queries['energia_kcal'])) ? $queries['energia_kcal'] : false;
                if ($energia_kcal) array_push($where, " n1.energia_kcal = $energia_kcal ");

                $energia_kj = (isset($queries['energia_kj'])) ? $queries['energia_kj'] : false;
                if ($energia_kj) array_push($where, " n1.energia_kj = $energia_kj ");

                $proteina_g = (isset($queries['proteina_g'])) ? $queries['proteina_g'] : false;
                if ($proteina_g) array_push($where, " n1.proteina_g = $proteina_g ");

                $lipideos_g = (isset($queries['lipideos_g'])) ? $queries['lipideos_g'] : false;
                if ($lipideos_g) array_push($where, " n1.lipideos_g = $lipideos_g ");

                $colesterol_mg = (isset($queries['colesterol_mg'])) ? $queries['colesterol_mg'] : false;
                if ($colesterol_mg) array_push($where, " n1.colesterol_mg = $colesterol_mg ");

                $carboidrato_g = (isset($queries['carboidrato_g'])) ? $queries['carboidrato_g'] : false;
                if ($carboidrato_g) array_push($where, " n1.carboidrato_g = $carboidrato_g ");

                $fibra_alimentar_g = (isset($queries['fibra_alimentar_g'])) ? $queries['fibra_alimentar_g'] : false;
                if ($fibra_alimentar_g) array_push($where, " n1.fibra_alimentar_g = $fibra_alimentar_g ");

                $cinzas_g = (isset($queries['cinzas_g'])) ? $queries['cinzas_g'] : false;
                if ($cinzas_g) array_push($where, " n1.cinzas_g = $cinzas_g ");

                $calcio_mg = (isset($queries['calcio_mg'])) ? $queries['calcio_mg'] : false;
                if ($calcio_mg) array_push($where, " n1.calcio_mg = $calcio_mg ");

                $magnesio_mg = (isset($queries['magnesio_mg'])) ? $queries['magnesio_mg'] : false;
                if ($magnesio_mg) array_push($where, " n1.magnesio_mg = $magnesio_mg ");

                $manganes_mg = (isset($queries['manganes_mg'])) ? $queries['manganes_mg'] : false;
                if ($manganes_mg) array_push($where, " n1.manganes_mg = $manganes_mg ");

                $fosforo_mg = (isset($queries['fosforo_mg'])) ? $queries['fosforo_mg'] : false;
                if ($fosforo_mg) array_push($where, " n1.fosforo_mg = $fosforo_mg ");

                $ferro_mg = (isset($queries['ferro_mg'])) ? $queries['ferro_mg'] : false;
                if ($ferro_mg) array_push($where, " n1.ferro_mg = $ferro_mg ");

                $sodio_mg = (isset($queries['sodio_mg'])) ? $queries['sodio_mg'] : false;
                if ($sodio_mg) array_push($where, " n1.sodio_mg = $sodio_mg ");

                $potassio_mg = (isset($queries['potassio_mg'])) ? $queries['potassio_mg'] : false;
                if ($potassio_mg) array_push($where, " n1.potassio_mg = $potassio_mg ");

                $cobre_mg = (isset($queries['cobre_mg'])) ? $queries['cobre_mg'] : false;
                if ($cobre_mg) array_push($where, " n1.cobre_mg = $cobre_mg ");

                $zinco_mg = (isset($queries['zinco_mg'])) ? $queries['zinco_mg'] : false;
                if ($zinco_mg) array_push($where, " n1.zinco_mg = $zinco_mg ");

                $retinol_mcg = (isset($queries['retinol_mcg'])) ? $queries['retinol_mcg'] : false;
                if ($retinol_mcg) array_push($where, " n1.retinol_mcg = $retinol_mcg ");

                $re_mcg = (isset($queries['re_mcg'])) ? $queries['re_mcg'] : false;
                if ($re_mcg) array_push($where, " n1.re_mcg = $re_mcg ");

                $rae_mcg = (isset($queries['rae_mcg'])) ? $queries['rae_mcg'] : false;
                if ($rae_mcg) array_push($where, " n1.rae_mcg = $rae_mcg ");

                $tiamina_mg = (isset($queries['tiamina_mg'])) ? $queries['tiamina_mg'] : false;
                if ($tiamina_mg) array_push($where, " n1.tiamina_mg = $tiamina_mg ");

                $riboflavina_mg = (isset($queries['riboflavina_mg'])) ? $queries['riboflavina_mg'] : false;
                if ($riboflavina_mg) array_push($where, " n1.riboflavina_mg = $riboflavina_mg ");

                $piridoxina_mg = (isset($queries['piridoxina_mg'])) ? $queries['piridoxina_mg'] : false;
                if ($piridoxina_mg) array_push($where, " n1.piridoxina_mg = $piridoxina_mg ");

                $niacina_mg = (isset($queries['niacina_mg'])) ? $queries['niacina_mg'] : false;
                if ($niacina_mg) array_push($where, " n1.niacina_mg = $niacina_mg ");

                $vitamina_c_mg = (isset($queries['vitamina_c_mg'])) ? $queries['vitamina_c_mg'] : false;
                if ($vitamina_c_mg) array_push($where, " n1.vitamina_c_mg = $vitamina_c_mg ");

                $saturados_g = (isset($queries['saturados_g'])) ? $queries['saturados_g'] : false;
                if ($saturados_g) array_push($where, " n1.saturados_g = $saturados_g ");

                $mono_insaturados_g = (isset($queries['mono_insaturados_g'])) ? $queries['mono_insaturados_g'] : false;
                if ($mono_insaturados_g) array_push($where, " n1.mono_insaturados_g = $mono_insaturados_g ");

                $poli_insaturados_g = (isset($queries['poli_insaturados_g'])) ? $queries['poli_insaturados_g'] : false;
                if ($poli_insaturados_g) array_push($where, " n1.poli_insaturados_g = $poli_insaturados_g ");

                $c12_0_g = (isset($queries['c12_0_g'])) ? $queries['c12_0_g'] : false;
                if ($c12_0_g) array_push($where, " n1.c12_0_g = $c12_0_g ");

                $c14_0_g = (isset($queries['c14_0_g'])) ? $queries['c14_0_g'] : false;
                if ($c14_0_g) array_push($where, " n1.c14_0_g = $c14_0_g ");

                $c16_0_g = (isset($queries['c16_0_g'])) ? $queries['c16_0_g'] : false;
                if ($c16_0_g) array_push($where, " n1.c16_0_g = $c16_0_g ");

                $c18_0_g = (isset($queries['c18_0_g'])) ? $queries['c18_0_g'] : false;
                if ($c18_0_g) array_push($where, " n1.c18_0_g = $c18_0_g ");

                $c20_0_g = (isset($queries['c20_0_g'])) ? $queries['c20_0_g'] : false;
                if ($c20_0_g) array_push($where, " n1.c20_0_g = $c20_0_g ");

                $c22_0_g = (isset($queries['c22_0_g'])) ? $queries['c22_0_g'] : false;
                if ($c22_0_g) array_push($where, " n1.c22_0_g = $c22_0_g ");

                $c24_0_g = (isset($queries['c24_0_g'])) ? $queries['c24_0_g'] : false;
                if ($c24_0_g) array_push($where, " n1.c24_0_g = $c24_0_g ");

                $c14_1_g = (isset($queries['c14_1_g'])) ? $queries['c14_1_g'] : false;
                if ($c14_1_g) array_push($where, " n1.c14_1_g = $c14_1_g ");

                $c16_1_g = (isset($queries['c16_1_g'])) ? $queries['c16_1_g'] : false;
                if ($c16_1_g) array_push($where, " n1.c16_1_g = $c16_1_g ");

                $c18_1_g = (isset($queries['c18_1_g'])) ? $queries['c18_1_g'] : false;
                if ($c18_1_g) array_push($where, " n1.c18_1_g = $c18_1_g ");

                $c20_1_g = (isset($queries['c20_1_g'])) ? $queries['c20_1_g'] : false;
                if ($c20_1_g) array_push($where, " n1.c20_1_g = $c20_1_g ");

                $c18_2_n_6_g = (isset($queries['c18_2_n_6_g'])) ? $queries['c18_2_n_6_g'] : false;
                if ($c18_2_n_6_g) array_push($where, " n1.c18_2_n_6_g = $c18_2_n_6_g ");

                $c18_3_n_3_g = (isset($queries['c18_3_n_3_g'])) ? $queries['c18_3_n_3_g'] : false;
                if ($c18_3_n_3_g) array_push($where, " n1.c18_3_n_3_g = $c18_3_n_3_g ");

                $c20_4_g = (isset($queries['c20_4_g'])) ? $queries['c20_4_g'] : false;
                if ($c20_4_g) array_push($where, " n1.c20_4_g = $c20_4_g ");

                $c20_5_g = (isset($queries['c20_5_g'])) ? $queries['c20_5_g'] : false;
                if ($c20_5_g) array_push($where, " n1.c20_5_g = $c20_5_g ");

                $c22_5_g = (isset($queries['c22_5_g'])) ? $queries['c22_5_g'] : false;
                if ($c22_5_g) array_push($where, " n1.c22_5_g = $c22_5_g ");

                $c22_6_g = (isset($queries['c22_6_g'])) ? $queries['c22_6_g'] : false;
                if ($c22_6_g) array_push($where, " n1.c22_6_g = $c22_6_g ");

                $c18_1t_g = (isset($queries['c18_1t_g'])) ? $queries['c18_1t_g'] : false;
                if ($c18_1t_g) array_push($where, " n1.c18_1t_g = $c18_1t_g ");

                $c18_2t_g = (isset($queries['c18_2t_g'])) ? $queries['c18_2t_g'] : false;
                if ($c18_2t_g) array_push($where, " n1.c18_2t_g = $c18_2t_g ");

                $triptofano_g = (isset($queries['triptofano_g'])) ? $queries['triptofano_g'] : false;
                if ($triptofano_g) array_push($where, " n1.triptofano_g = $triptofano_g ");

                $treonina_g = (isset($queries['treonina_g'])) ? $queries['treonina_g'] : false;
                if ($treonina_g) array_push($where, " n1.treonina_g = $treonina_g ");

                $isoleucina_g = (isset($queries['isoleucina_g'])) ? $queries['isoleucina_g'] : false;
                if ($isoleucina_g) array_push($where, " n1.isoleucina_g = $isoleucina_g ");

                $leucina_g = (isset($queries['leucina_g'])) ? $queries['leucina_g'] : false;
                if ($leucina_g) array_push($where, " n1.leucina_g = $leucina_g ");

                $lisina_g = (isset($queries['lisina_g'])) ? $queries['lisina_g'] : false;
                if ($lisina_g) array_push($where, " n1.lisina_g = $lisina_g ");

                $metionina_g = (isset($queries['metionina_g'])) ? $queries['metionina_g'] : false;
                if ($metionina_g) array_push($where, " n1.metionina_g = $metionina_g ");

                $cistina_g = (isset($queries['cistina_g'])) ? $queries['cistina_g'] : false;
                if ($cistina_g) array_push($where, " n1.cistina_g = $cistina_g ");

                $fenilalanina_g = (isset($queries['fenilalanina_g'])) ? $queries['fenilalanina_g'] : false;
                if ($fenilalanina_g) array_push($where, " n1.fenilalanina_g = $fenilalanina_g ");

                $tirosina_g = (isset($queries['tirosina_g'])) ? $queries['tirosina_g'] : false;
                if ($tirosina_g) array_push($where, " n1.tirosina_g = $tirosina_g ");

                $valina_g = (isset($queries['valina_g'])) ? $queries['valina_g'] : false;
                if ($valina_g) array_push($where, " n1.valina_g = $valina_g ");

                $arginina_g = (isset($queries['arginina_g'])) ? $queries['arginina_g'] : false;
                if ($arginina_g) array_push($where, " n1.arginina_g = $arginina_g ");

                $histidina_g = (isset($queries['histidina_g'])) ? $queries['histidina_g'] : false;
                if ($histidina_g) array_push($where, " n1.histidina_g = $histidina_g ");

                $alanina_g = (isset($queries['alanina_g'])) ? $queries['alanina_g'] : false;
                if ($alanina_g) array_push($where, " n1.alanina_g = $alanina_g ");

                $acido_aspartico_g = (isset($queries['acido_aspartico_g'])) ? $queries['acido_aspartico_g'] : false;
                if ($acido_aspartico_g) array_push($where, " n1.acido_aspartico_g = $acido_aspartico_g ");

                $acido_glutamico_g = (isset($queries['acido_glutamico_g'])) ? $queries['acido_glutamico_g'] : false;
                if ($acido_glutamico_g) array_push($where, " n1.acido_glutamico_g = $acido_glutamico_g ");

                $glicina_g = (isset($queries['glicina_g'])) ? $queries['glicina_g'] : false;
                if ($glicina_g) array_push($where, " n1.glicina_g = $glicina_g ");

                $prolina_g = (isset($queries['prolina_g'])) ? $queries['prolina_g'] : false;
                if ($prolina_g) array_push($where, " n1.prolina_g = $prolina_g ");

                $serina_g = (isset($queries['serina_g'])) ? $queries['serina_g'] : false;
                if ($serina_g) array_push($where, " n1.serina_g = $serina_g ");

                $ds_fonte = (isset($queries['ds_fonte'])) ? $queries['ds_fonte'] : false;
                if ($ds_fonte) array_push($where, " n1.ds_fonte LIKE '%$ds_fonte%' ");

                $status = (isset($queries['status1'])) ? $queries['status1'] : false;
                if ($status) array_push($where, " n1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "n1.*"; 
		;
		
		if ($total) $fields = "COUNT(n1.id) as total";
		
		$ordem = "ORDER BY n1.numero_alimento ASC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM nutricaotacoalimentos n1
					
					WHERE n1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getNutricaotacoalimentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNutricaotacoalimentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNutricaotacoalimentoByIdHelper($id, $queries = array()) {
		$rows = new Nutricaotacoalimentos();
		return $rows->getNutricaotacoalimentoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaotacoalimentos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
                $row->id = (array_key_exists("id",$dados)) ? $dados["id"] : $row->id;
                $row->idsmedidas = (array_key_exists("idsmedidas",$dados)) ? $dados["idsmedidas"] : $row->idsmedidas;
                $row->idtipoalimento = (array_key_exists("idtipoalimento",$dados)) ? $dados["idtipoalimento"] : $row->idtipoalimento;
                $row->numero_alimento = (array_key_exists("numero_alimento",$dados)) ? $dados["numero_alimento"] : $row->numero_alimento;
                $row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
                $row->vl_g_poli = (array_key_exists("vl_g_poli",$dados)) ? $dados["vl_g_poli"] : $row->vl_g_poli;
                $row->vl_g_mon = (array_key_exists("vl_g_mon",$dados)) ? $dados["vl_g_mon"] : $row->vl_g_mon;
                $row->vl_g_sat = (array_key_exists("vl_g_sat",$dados)) ? $dados["vl_g_sat"] : $row->vl_g_sat;
                $row->vl_colest = (array_key_exists("vl_colest",$dados)) ? $dados["vl_colest"] : $row->vl_colest;
                $row->vl_b1 = (array_key_exists("vl_b1",$dados)) ? $dados["vl_b1"] : $row->vl_b1;
                $row->vl_b2 = (array_key_exists("vl_b2",$dados)) ? $dados["vl_b2"] : $row->vl_b2;
                $row->vl_b6 = (array_key_exists("vl_b6",$dados)) ? $dados["vl_b6"] : $row->vl_b6;
                $row->vl_b12 = (array_key_exists("vl_b12",$dados)) ? $dados["vl_b12"] : $row->vl_b12;
                $row->vl_nia = (array_key_exists("vl_nia",$dados)) ? $dados["vl_nia"] : $row->vl_nia;
                $row->vl_folic = (array_key_exists("vl_folic",$dados)) ? $dados["vl_folic"] : $row->vl_folic;
                $row->vl_pant = (array_key_exists("vl_pant",$dados)) ? $dados["vl_pant"] : $row->vl_pant;
                $row->vl_iodo = (array_key_exists("vl_iodo",$dados)) ? $dados["vl_iodo"] : $row->vl_iodo;
                $row->vl_mg = (array_key_exists("vl_mg",$dados)) ? $dados["vl_mg"] : $row->vl_mg;
                $row->vl_zinco = (array_key_exists("vl_zinco",$dados)) ? $dados["vl_zinco"] : $row->vl_zinco;
                $row->vl_mn = (array_key_exists("vl_mn",$dados)) ? $dados["vl_mn"] : $row->vl_mn;
                $row->vl_cobre = (array_key_exists("vl_cobre",$dados)) ? $dados["vl_cobre"] : $row->vl_cobre;
                $row->vl_selen = (array_key_exists("vl_selen",$dados)) ? $dados["vl_selen"] : $row->vl_selen;
                $row->vl_carb = (array_key_exists("vl_carb",$dados)) ? $dados["vl_carb"] : $row->vl_carb;
                $row->vl_g_tot = (array_key_exists("vl_g_tot",$dados)) ? $dados["vl_g_tot"] : $row->vl_g_tot;
                $row->vl_prot = (array_key_exists("vl_prot",$dados)) ? $dados["vl_prot"] : $row->vl_prot;
                $row->vl_fibra = (array_key_exists("vl_fibra",$dados)) ? $dados["vl_fibra"] : $row->vl_fibra;
                $row->vl_vit_a = (array_key_exists("vl_vit_a",$dados)) ? $dados["vl_vit_a"] : $row->vl_vit_a;
                $row->vl_vit_c = (array_key_exists("vl_vit_c",$dados)) ? $dados["vl_vit_c"] : $row->vl_vit_c;
                $row->vl_vit_d = (array_key_exists("vl_vit_d",$dados)) ? $dados["vl_vit_d"] : $row->vl_vit_d;
                $row->vl_vit_e = (array_key_exists("vl_vit_e",$dados)) ? $dados["vl_vit_e"] : $row->vl_vit_e;
                $row->vl_na = (array_key_exists("vl_na",$dados)) ? $dados["vl_na"] : $row->vl_na;
                $row->vl_ca = (array_key_exists("vl_ca",$dados)) ? $dados["vl_ca"] : $row->vl_ca;
                $row->vl_k = (array_key_exists("vl_k",$dados)) ? $dados["vl_k"] : $row->vl_k;
                $row->vl_p = (array_key_exists("vl_p",$dados)) ? $dados["vl_p"] : $row->vl_p;
                $row->vl_fe = (array_key_exists("vl_fe",$dados)) ? $dados["vl_fe"] : $row->vl_fe;
                $row->umidade_perc = (array_key_exists("umidade_perc",$dados)) ? $dados["umidade_perc"] : $row->umidade_perc;
                $row->energia_kcal = (array_key_exists("energia_kcal",$dados)) ? $dados["energia_kcal"] : $row->energia_kcal;
                $row->energia_kj = (array_key_exists("energia_kj",$dados)) ? $dados["energia_kj"] : $row->energia_kj;
                $row->proteina_g = (array_key_exists("proteina_g",$dados)) ? $dados["proteina_g"] : $row->proteina_g;
                $row->lipideos_g = (array_key_exists("lipideos_g",$dados)) ? $dados["lipideos_g"] : $row->lipideos_g;
                $row->colesterol_mg = (array_key_exists("colesterol_mg",$dados)) ? $dados["colesterol_mg"] : $row->colesterol_mg;
                $row->carboidrato_g = (array_key_exists("carboidrato_g",$dados)) ? $dados["carboidrato_g"] : $row->carboidrato_g;
                $row->fibra_alimentar_g = (array_key_exists("fibra_alimentar_g",$dados)) ? $dados["fibra_alimentar_g"] : $row->fibra_alimentar_g;
                $row->cinzas_g = (array_key_exists("cinzas_g",$dados)) ? $dados["cinzas_g"] : $row->cinzas_g;
                $row->calcio_mg = (array_key_exists("calcio_mg",$dados)) ? $dados["calcio_mg"] : $row->calcio_mg;
                $row->magnesio_mg = (array_key_exists("magnesio_mg",$dados)) ? $dados["magnesio_mg"] : $row->magnesio_mg;
                $row->manganes_mg = (array_key_exists("manganes_mg",$dados)) ? $dados["manganes_mg"] : $row->manganes_mg;
                $row->fosforo_mg = (array_key_exists("fosforo_mg",$dados)) ? $dados["fosforo_mg"] : $row->fosforo_mg;
                $row->ferro_mg = (array_key_exists("ferro_mg",$dados)) ? $dados["ferro_mg"] : $row->ferro_mg;
                $row->sodio_mg = (array_key_exists("sodio_mg",$dados)) ? $dados["sodio_mg"] : $row->sodio_mg;
                $row->potassio_mg = (array_key_exists("potassio_mg",$dados)) ? $dados["potassio_mg"] : $row->potassio_mg;
                $row->cobre_mg = (array_key_exists("cobre_mg",$dados)) ? $dados["cobre_mg"] : $row->cobre_mg;
                $row->zinco_mg = (array_key_exists("zinco_mg",$dados)) ? $dados["zinco_mg"] : $row->zinco_mg;
                $row->retinol_mcg = (array_key_exists("retinol_mcg",$dados)) ? $dados["retinol_mcg"] : $row->retinol_mcg;
                $row->re_mcg = (array_key_exists("re_mcg",$dados)) ? $dados["re_mcg"] : $row->re_mcg;
                $row->rae_mcg = (array_key_exists("rae_mcg",$dados)) ? $dados["rae_mcg"] : $row->rae_mcg;
                $row->tiamina_mg = (array_key_exists("tiamina_mg",$dados)) ? $dados["tiamina_mg"] : $row->tiamina_mg;
                $row->riboflavina_mg = (array_key_exists("riboflavina_mg",$dados)) ? $dados["riboflavina_mg"] : $row->riboflavina_mg;
                $row->piridoxina_mg = (array_key_exists("piridoxina_mg",$dados)) ? $dados["piridoxina_mg"] : $row->piridoxina_mg;
                $row->niacina_mg = (array_key_exists("niacina_mg",$dados)) ? $dados["niacina_mg"] : $row->niacina_mg;
                $row->vitamina_c_mg = (array_key_exists("vitamina_c_mg",$dados)) ? $dados["vitamina_c_mg"] : $row->vitamina_c_mg;
                $row->saturados_g = (array_key_exists("saturados_g",$dados)) ? $dados["saturados_g"] : $row->saturados_g;
                $row->mono_insaturados_g = (array_key_exists("mono_insaturados_g",$dados)) ? $dados["mono_insaturados_g"] : $row->mono_insaturados_g;
                $row->poli_insaturados_g = (array_key_exists("poli_insaturados_g",$dados)) ? $dados["poli_insaturados_g"] : $row->poli_insaturados_g;
                $row->c12_0_g = (array_key_exists("c12_0_g",$dados)) ? $dados["c12_0_g"] : $row->c12_0_g;
                $row->c14_0_g = (array_key_exists("c14_0_g",$dados)) ? $dados["c14_0_g"] : $row->c14_0_g;
                $row->c16_0_g = (array_key_exists("c16_0_g",$dados)) ? $dados["c16_0_g"] : $row->c16_0_g;
                $row->c18_0_g = (array_key_exists("c18_0_g",$dados)) ? $dados["c18_0_g"] : $row->c18_0_g;
                $row->c20_0_g = (array_key_exists("c20_0_g",$dados)) ? $dados["c20_0_g"] : $row->c20_0_g;
                $row->c22_0_g = (array_key_exists("c22_0_g",$dados)) ? $dados["c22_0_g"] : $row->c22_0_g;
                $row->c24_0_g = (array_key_exists("c24_0_g",$dados)) ? $dados["c24_0_g"] : $row->c24_0_g;
                $row->c14_1_g = (array_key_exists("c14_1_g",$dados)) ? $dados["c14_1_g"] : $row->c14_1_g;
                $row->c16_1_g = (array_key_exists("c16_1_g",$dados)) ? $dados["c16_1_g"] : $row->c16_1_g;
                $row->c18_1_g = (array_key_exists("c18_1_g",$dados)) ? $dados["c18_1_g"] : $row->c18_1_g;
                $row->c20_1_g = (array_key_exists("c20_1_g",$dados)) ? $dados["c20_1_g"] : $row->c20_1_g;
                $row->c18_2_n_6_g = (array_key_exists("c18_2_n_6_g",$dados)) ? $dados["c18_2_n_6_g"] : $row->c18_2_n_6_g;
                $row->c18_3_n_3_g = (array_key_exists("c18_3_n_3_g",$dados)) ? $dados["c18_3_n_3_g"] : $row->c18_3_n_3_g;
                $row->c20_4_g = (array_key_exists("c20_4_g",$dados)) ? $dados["c20_4_g"] : $row->c20_4_g;
                $row->c20_5_g = (array_key_exists("c20_5_g",$dados)) ? $dados["c20_5_g"] : $row->c20_5_g;
                $row->c22_5_g = (array_key_exists("c22_5_g",$dados)) ? $dados["c22_5_g"] : $row->c22_5_g;
                $row->c22_6_g = (array_key_exists("c22_6_g",$dados)) ? $dados["c22_6_g"] : $row->c22_6_g;
                $row->c18_1t_g = (array_key_exists("c18_1t_g",$dados)) ? $dados["c18_1t_g"] : $row->c18_1t_g;
                $row->c18_2t_g = (array_key_exists("c18_2t_g",$dados)) ? $dados["c18_2t_g"] : $row->c18_2t_g;
                $row->triptofano_g = (array_key_exists("triptofano_g",$dados)) ? $dados["triptofano_g"] : $row->triptofano_g;
                $row->treonina_g = (array_key_exists("treonina_g",$dados)) ? $dados["treonina_g"] : $row->treonina_g;
                $row->isoleucina_g = (array_key_exists("isoleucina_g",$dados)) ? $dados["isoleucina_g"] : $row->isoleucina_g;
                $row->leucina_g = (array_key_exists("leucina_g",$dados)) ? $dados["leucina_g"] : $row->leucina_g;
                $row->lisina_g = (array_key_exists("lisina_g",$dados)) ? $dados["lisina_g"] : $row->lisina_g;
                $row->metionina_g = (array_key_exists("metionina_g",$dados)) ? $dados["metionina_g"] : $row->metionina_g;
                $row->cistina_g = (array_key_exists("cistina_g",$dados)) ? $dados["cistina_g"] : $row->cistina_g;
                $row->fenilalanina_g = (array_key_exists("fenilalanina_g",$dados)) ? $dados["fenilalanina_g"] : $row->fenilalanina_g;
                $row->tirosina_g = (array_key_exists("tirosina_g",$dados)) ? $dados["tirosina_g"] : $row->tirosina_g;
                $row->valina_g = (array_key_exists("valina_g",$dados)) ? $dados["valina_g"] : $row->valina_g;
                $row->arginina_g = (array_key_exists("arginina_g",$dados)) ? $dados["arginina_g"] : $row->arginina_g;
                $row->histidina_g = (array_key_exists("histidina_g",$dados)) ? $dados["histidina_g"] : $row->histidina_g;
                $row->alanina_g = (array_key_exists("alanina_g",$dados)) ? $dados["alanina_g"] : $row->alanina_g;
                $row->acido_aspartico_g = (array_key_exists("acido_aspartico_g",$dados)) ? $dados["acido_aspartico_g"] : $row->acido_aspartico_g;
                $row->acido_glutamico_g = (array_key_exists("acido_glutamico_g",$dados)) ? $dados["acido_glutamico_g"] : $row->acido_glutamico_g;
                $row->glicina_g = (array_key_exists("glicina_g",$dados)) ? $dados["glicina_g"] : $row->glicina_g;
                $row->prolina_g = (array_key_exists("prolina_g",$dados)) ? $dados["prolina_g"] : $row->prolina_g;
                $row->serina_g = (array_key_exists("serina_g",$dados)) ? $dados["serina_g"] : $row->serina_g;
                $row->ds_fonte = (array_key_exists("ds_fonte",$dados)) ? $dados["ds_fonte"] : $row->ds_fonte;
                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
				
          $row->save();
          
          $registro = 'Adicionado';
		if(!$novoRegistro)
			$registro = 'Editado';

               Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), " id -> [" . $row->id . "] [" . $registro . "][status=>" . $row->status . "][excluido=>" . $row->excluido . "]");
		
		return $row;
	}
	
}