<?php

/**
 * Define o modelo Cidades
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Cidades extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "cidades";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";		

	public static function getCidadeById($idcidade, $status = null) {
		$idcidade = (int)$idcidade;
		$w = "";
		if (!is_null($status)) $w .= "AND status = '$status'";
		
		$rows = new Cidades();
		return $rows->fetchRow("id=$idcidade $w");
	}
	
	/**
	 * Retorna a quantidade total de cidades
	 * @param string $cidade
	 * @return int
	 */
	public function getTotalCidades($query = "") {
		$w = "";
		if (is_array($query)) {
			$params = array();
			if (isset($query['query'])) array_push($params, $this->getWhereCidade($query['query'], ""));
			if ((isset($query['bv_restrict'])) && ($query['bv_restrict'])) array_push($params, "(SELECT COUNT(i1.id) FROM imoveis i1 WHERE i1.excluido='nao' AND i1.status='Ativo' AND i1.perfil='".Imoveis::$OFERTA."' AND v1.id=i1.idcidade)>0");
			
			foreach ($params as $k=>$v) {
				if ($k>0) $w .= " AND ";
				$w .= $v;
			}
		} else $w = $this->getWhereCidade($query, $w);
		if ($w!="") $w = " WHERE $w ";
		
		$db = Zend_Registry::get('db');
		
		$strsql = "SELECT COUNT(v1.id) as total 
			FROM (SELECT c1.*, CONCAT(c1.nome, ' - ', c1.uf) as alias 
				FROM cidades c1 
				WHERE c1.excluido='nao') as v1 $w";

		
		$total = $db->fetchRow($strsql);

		return $total["total"];
	}  
	
	/**
	 * Retorna um array de cidades, o campo alias está no formato de Mn_View_Helper_FormatCidadeUf
	 * @param string $query_cidade
	 * @param int $page
	 * @param int $maxpage
	 * @param boolean $qtdRegioes
	 * @param boolean $qtdBairros
	 * @return array
	 */
	public function getCidades($query = "", $page = 0, $maxpage = 30, $qtdRegioes = false, $qtdBairros = false, $qtdUsuarios = false) {
		$w = "";
		if (is_array($query)) {
			$params = array();
			if (isset($query['query'])) array_push($params, $this->getWhereCidade($query['query'], ""));
			if ((isset($query['bv_restrict'])) && ($query['bv_restrict'])) array_push($params, "(SELECT COUNT(i1.id) FROM imoveis i1 WHERE i1.excluido='nao' AND i1.status='Ativo' AND i1.perfil='".Imoveis::$OFERTA."' AND v1.id=i1.idcidade)>0");
			if ((isset($query['bc_restrict'])) && ($query['bc_restrict'])) array_push($params, "(SELECT COUNT(i1.id) FROM imoveis i1 WHERE i1.excluido='nao' AND i1.status='Ativo' AND i1.perfil='".Imoveis::$PROCURA."' AND v1.id=i1.idcidade)>0");
			if ((isset($query['bp_restrict'])) && ($query['bp_restrict'])) array_push($params, "(SELECT COUNT(p1.id) FROM profissionais p1 WHERE p1.excluido='nao' AND p1.status='Ativo' AND v1.id=p1.idcidade)>0");
			
			foreach ($params as $k=>$v) {
				if ($k>0) $w .= " AND ";
				$w .= $v;
			}
		} else $w = $this->getWhereCidade($query, $w);
		if ($w!="") $w = " WHERE $w ";

		
		$qtd_regioes = "";
		if ($qtdRegioes) $qtd_regioes .= " ,(SELECT COUNT(r1.id) FROM cidadesregioes r1 WHERE r1.idcidade=v1.id AND r1.excluido='nao') as qtdregioes ";
		
		$qtd_bairros = "";
		if ($qtdBairros) $qtd_bairros .= " ,(SELECT COUNT(b1.id) FROM cidadesbairros b1 WHERE b1.idcidade=v1.id AND b1.excluido='nao') as qtdbairros ";
		
		$qtd_usuarios = "";
		if ($qtdUsuarios) $qtd_usuarios .= " ,(SELECT COUNT(u1.id) FROM pdusuarios u1 WHERE u1.idcidade=v1.id AND v1.excluido='nao') as qtdusuarios ";		
		
		
		$limit = "LIMIT ".($page*$maxpage).", $maxpage";
		if ($maxpage==0) $limit = "";
		
		$db = Zend_Registry::get('db');
		$strsql = "SELECT v1.* 
					$qtd_regioes
					$qtd_bairros
					$qtd_usuarios
					FROM (SELECT c1.*, CONCAT(c1.nome, ' - ', c1.uf) as alias
						FROM cidades c1 
						WHERE c1.excluido='nao') as v1 
					$w
					ORDER BY v1.alias
					$limit";
					
		return $db->fetchAll($strsql);
	}

	public static function getCidadesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$cidades = new Cidades();
		return $cidades->getCidades($queries, $page, $maxpage);
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param unknown_type $query
	 * @param unknown_type $w
	 */
	private function getWhereCidade($query, $w) {
		if ($query=="") return $w;
		
		$queries = explode("   ", $query);
		$r = "";
		foreach ($queries as $i=>$query) {
			if ($i>0) $r .= "AND ";
			$r .= "(v1.alias LIKE '$query%')"; 
		}
		if ($r!="") $w .= " ($r)";
		
		return $w;
	}
	
	/**
	 * Retorna a cidade por UF e Cidade
	 * @param string $uf
	 * @param cidade $cidade
	 */
	public function getCidadeByUfCidade($uf, $cidade) {
		$row = $this->fetchRow("uf='$uf' AND nome='$cidade' AND excluido='nao'");
		return $row;
	}
	

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Cidades
     */
	public function save($dados) {
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id");
		
		if (!$row) $row = $this->createRow();
		else {
			$x_rows = new X_Cidades();
			$x_rows->arquiva($row->toArray());
		} 
								
		if ($id>0)$row->id = $id;
		$row->idestado = (int)$dados['idestado'];
		$row->uf = $dados['uf'];
		$row->nome = $dados['nome'];
		$row->logusuario = $dados['logusuario'];
		$row->logdata = $dados['logdata'];		
		$row->excluido = $dados['excluido'];
		$row->save();

		return $row;
	}		
	
	
	
}