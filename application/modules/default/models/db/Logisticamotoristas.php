<?php

/**
 * Define o modelo Logisticamotoristas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Logisticamotoristas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "logisticamotoristas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLogisticamotoristasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$logisticamotoristas = new Logisticamotoristas();
		return $logisticamotoristas->getLogisticamotoristas($queries, $page, $maxpage);
	}
	
	public function getLogisticamotoristas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		
		$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " l1.idfuncionario = $idfuncionario ");

$rg = (isset($queries["rg"])) ? $queries["rg"] : false;
		if ($rg) array_push($where, " l1.rg LIKE '%$rg%' ");

$cpf = (isset($queries["cpf"])) ? $queries["cpf"] : false;
		if ($cpf) array_push($where, " l1.cpf LIKE '%$cpf%' ");

$nomerazao = (isset($queries["nomerazao"])) ? $queries["nomerazao"] : false;
		if ($nomerazao) array_push($where, " l1.nomerazao LIKE '%$nomerazao%' ");

$sobrenomefantasia = (isset($queries["sobrenomefantasia"])) ? $queries["sobrenomefantasia"] : false;
		if ($sobrenomefantasia) array_push($where, " l1.sobrenomefantasia LIKE '%$sobrenomefantasia%' ");

$email = (isset($queries["email"])) ? $queries["email"] : false;
		if ($email) array_push($where, " l1.email LIKE '%$email%' ");

$categoriacnh = (isset($queries["categoriacnh"])) ? $queries["categoriacnh"] : false;
		if ($categoriacnh) array_push($where, " l1.categoriacnh LIKE '%$categoriacnh%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*"; 
		;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM logisticamotoristas l1
					
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLogisticamotoristaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLogisticamotoristas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLogisticamotoristaByIdHelper($id, $queries = array()) {
		$rows = new Logisticamotoristas();
		return $rows->getLogisticamotoristaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Logisticamotoristas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->idarquivocnh = (array_key_exists("idarquivocnh",$dados)) ? $dados["idarquivocnh"] : $row->idarquivocnh;
 $row->idimagem = (array_key_exists("idimagem",$dados)) ? $dados["idimagem"] : $row->idimagem;
 $row->idcertificadotransporte = (array_key_exists("idcertificadotransporte",$dados)) ? $dados["idcertificadotransporte"] : $row->idcertificadotransporte;
 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
 $row->idfornecedor = (array_key_exists("idfornecedor",$dados)) ? $dados["idfornecedor"] : $row->idfornecedor;
 $row->rg = (array_key_exists("rg",$dados)) ? $dados["rg"] : $row->rg;
 $row->cpf = (array_key_exists("cpf",$dados)) ? $dados["cpf"] : $row->cpf;
 $row->tipoveiculo = (array_key_exists("tipoveiculo",$dados)) ? $dados["tipoveiculo"] : $row->tipoveiculo;
 $row->datanascimento = (array_key_exists("datanascimento",$dados)) ? $dados["datanascimento"] : $row->datanascimento;
 $row->nomerazao = (array_key_exists("nomerazao",$dados)) ? $dados["nomerazao"] : $row->nomerazao;
 $row->sobrenomefantasia = (array_key_exists("sobrenomefantasia",$dados)) ? $dados["sobrenomefantasia"] : $row->sobrenomefantasia;
 $row->email = (array_key_exists("email",$dados)) ? $dados["email"] : $row->email;
 $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
 $row->celular = (array_key_exists("celular",$dados)) ? $dados["celular"] : $row->celular;
 $row->categoriacnh = (array_key_exists("categoriacnh",$dados)) ? $dados["categoriacnh"] : $row->categoriacnh;
 $row->registrocnh = (array_key_exists("registrocnh",$dados)) ? $dados["registrocnh"] : $row->registrocnh;
 $row->datavalidadecnh = (array_key_exists("datavalidadecnh",$dados)) ? $dados["datavalidadecnh"] : $row->datavalidadecnh;
 $row->dataprimeiracnh = (array_key_exists("dataprimeiracnh",$dados)) ? $dados["dataprimeiracnh"] : $row->dataprimeiracnh;
 $row->dataemissaocnh = (array_key_exists("dataemissaocnh",$dados)) ? $dados["dataemissaocnh"] : $row->dataemissaocnh;
 $row->transporteescolar = (array_key_exists("transporteescolar",$dados)) ? $dados["transporteescolar"] : $row->transporteescolar;
 $row->funcionariopublico = (array_key_exists("funcionariopublico",$dados)) ? $dados["funcionariopublico"] : $row->funcionariopublico;
 $row->terceirizado = (array_key_exists("terceirizado",$dados)) ? $dados["terceirizado"] : $row->terceirizado;
 $row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
 $row->diasaviso_vencimento = (array_key_exists("diasaviso_vencimento",$dados)) ? $dados["diasaviso_vencimento"] : $row->diasaviso_vencimento;
 $row->datacertificado = (array_key_exists("datacertificado",$dados)) ? $dados["datacertificado"] : $row->datacertificado;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}