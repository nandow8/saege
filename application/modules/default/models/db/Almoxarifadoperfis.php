<?php

class Almoxarifadoperfis extends Zend_Db_Table_Abstract {
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "almoxarifadoperfis";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";
    
    public function getAlmoxarifadoPerfis($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $perfil = (isset($queries["perfil"])) ? $queries["perfil"] : false;
        $status = (isset($queries["status"])) ? $queries["status"] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id) array_push($where, " id = $id ");        
        if ($perfil) array_push($where, " perfil LIKE '%$perfil%' ");
        if ($status) array_push($where, " status = '$status' ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "*"; 

        if ($total) $fields = "COUNT(id) as total";
        
        $ordem = "ORDER BY perfil";
        if ($order) $ordem = $order; 
        
        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        $strsql = "SELECT $fields FROM almoxarifadoperfis WHERE excluido='nao' $w $ordem $limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getAlmoxarifadoPerfisById($id, $queries = array()) {
        if ($id==0) return false;

        $queries['id'] = $id;
        $rows = $this->getAlmoxarifadoPerfis($queries, 0, 0);

        if (sizeof($rows)==0) return false;
        return $rows[0];
    }
}

?>