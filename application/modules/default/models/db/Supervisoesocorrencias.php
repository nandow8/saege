<?php

/**
 * Define o modelo Supervisoesocorrencias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Supervisoesocorrencias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "supervisoesocorrencias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getSupervisoesocorrenciasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$Supervisoesocorrencias = new Supervisoesocorrencias();
		return $Supervisoesocorrencias->getSupervisoesocorrencias($queries, $page, $maxpage);
	}
	
	public function getSupervisoesocorrencias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " s1.id = $id ");
		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " s1.sequencial LIKE '%$sequencial%' ");

$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " s1.datalancamento >= '$datalancamento_i' ");

$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " s1.datalancamento <= '$datalancamento_f' ");

$horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
		if ($horalancamento) array_push($where, " s1.horalancamento = '$horalancamento' ");

$idusuariologado = (isset($queries["idusuariologado"])) ? $queries["idusuariologado"] : false;
		if ($idusuariologado) array_push($where, " s1.idusuariologado = $idusuariologado ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " s1.idescola = $idescola ");

$idveiculo = (isset($queries["idveiculo"])) ? $queries["idveiculo"] : false;
		if ($idveiculo) array_push($where, " s1.idveiculo = $idveiculo ");

$idmotivo = (isset($queries["idmotivo"])) ? $queries["idmotivo"] : false;
		if ($idmotivo) array_push($where, " s1.idmotivo = $idmotivo ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " s1.observacoes = '$observacoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " s1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "s1.*"; 
		;
		
		if ($total) $fields = "COUNT(s1.id) as total";
		
		$ordem = "ORDER BY s1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM supervisoesocorrencias s1
					
					WHERE s1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getSupervisoesocorrenciaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSupervisoesocorrencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSupervisoesocorrenciaByIdHelper($id, $queries = array()) {
		$rows = new Supervisoesocorrencias();
		return $rows->getSupervisoesocorrenciaById($id, $queries);
	}		
	
	public function getUltimoSupervisoesocorrencias($queries = array()) {
		$queries['order'] = 'ORDER BY s1.id DESC';
		$rows = $this->getSupervisoesocorrencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Supervisoesocorrencias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
 $row->horalancamento = (array_key_exists("horalancamento",$dados)) ? $dados["horalancamento"] : $row->horalancamento;
 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
 $row->idveiculo = (array_key_exists("idveiculo",$dados)) ? $dados["idveiculo"] : $row->idveiculo;
 $row->idmotivo = (array_key_exists("idmotivo",$dados)) ? $dados["idmotivo"] : $row->idmotivo;
 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}