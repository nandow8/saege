<?php

class Escolasalunos extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "escolasalunos";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";
    public static $_MATRICULA_PRE_MATRICULA = 'pre_matricula';
    public static $_MATRICULA_MATRICULA = 'matricula';
    public static $_MATRICULA_REMATRICULA = 'Rematricula';
    public static $_MATRICULA_EFETIVADA = 'efetivada';
    public static $_MATRICULA_CANCELADA = 'cancelada';
    public static $_MATRICULA_AGUARDANDO = 'aguardando';

    /**
     *
     */
    public static function getMatriculas($field = false) {
        $res = array(
            self::$_MATRICULA_PRE_MATRICULA => 'Pré Matriculado',
            self::$_MATRICULA_MATRICULA => 'Matriculado',
            self::$_MATRICULA_REMATRICULA => 'Rematricula',
            self::$_MATRICULA_EFETIVADA => 'Efetivada',
            self::$_MATRICULA_CANCELADA => 'Cancelada',
            self::$_MATRICULA_AGUARDANDO => 'Aguardando',
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static $_CLASSIFICACAO_A = 'A';
    public static $_CLASSIFICACAO_B = 'B';
    public static $_CLASSIFICACAO_C = 'C';
    public static $_CLASSIFICACAO_D = 'D';
    public static $_CLASSIFICACAO_E = 'E';
    public static $_CLASSIFICACAO_F = 'F';
    public static $_CLASSIFICACAO_OUTROS = 'OUTROS';

    /**
     *
     */
    public static function getClassificacoes($field = false) {
        $res = array(
            self::$_CLASSIFICACAO_A => 'A',
            self::$_CLASSIFICACAO_B => 'B',
            self::$_CLASSIFICACAO_C => 'C',
            self::$_CLASSIFICACAO_D => 'D',
            self::$_CLASSIFICACAO_E => 'E',
            self::$_CLASSIFICACAO_F => 'F',
            self::$_CLASSIFICACAO_OUTROS => 'Outros',
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static $_FORMA_INGRESSO_PRIMEIRA_MATRICULA = 'primeira_matricula';
    public static $_FORMA_INGRESSO_TRANSFERIDO = 'transferido';

    public static function getFormasIngresso($field = false) {
        $res = array(
            self::$_FORMA_INGRESSO_PRIMEIRA_MATRICULA => '1° Matrícula',
            self::$_FORMA_INGRESSO_TRANSFERIDO => 'Transferido'
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static $_COR_RACA_BRANCA = '01';
    public static $_COR_RACA_PRETA = '02';
    public static $_COR_RACA_PARDA = '03';
    public static $_COR_RACA_AMARELA = '04';
    public static $_COR_RACA_INDIGENA = '05';
    public static $_COR_RACA_NAO_DECLARADA = '06';

    public static function getCorraca($field = false) {
        $res = array(
            self::$_COR_RACA_BRANCA => 'Branca',
            self::$_COR_RACA_PRETA => 'Preta',
            self::$_COR_RACA_PARDA => 'Parda',
            self::$_COR_RACA_AMARELA => 'Amarela',
            self::$_COR_RACA_INDIGENA => 'Indígena',
            self::$_COR_RACA_NAO_DECLARADA => 'Não Declarada'
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static $_MOBILIDADE_TIPO_TEMPORARIO = 'temporario';
    public static $_MOBILIDADE_TIPO_PERMANENTE = 'permanente';

    public static function getMobilidadesTipos($field = false) {
        $res = array(
            self::$_MOBILIDADE_TIPO_TEMPORARIO => 'Temporário',
            self::$_MOBILIDADE_TIPO_PERMANENTE => 'Permanente'
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static $_NACIONALIDADE_BRASILEIRA = '1';
    public static $_NACIONALIDADE_ESTRANGEIRO = '2';
    public static $_NACIONALIDADE_BRASILEIRO_EXTERIOR = '3';

    public static function getNacionalidadesTipos($field = false) {
        $res = array(
            self::$_NACIONALIDADE_BRASILEIRA => 'Brasileira',
            self::$_NACIONALIDADE_ESTRANGEIRO => 'Estrangeiro',
            self::$_NACIONALIDADE_BRASILEIRO_EXTERIOR => 'Brasileiro nascido no exterior'
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static function maskCPF($cpf){
        if(empty($cpf)){
            $cpf = "--";
        }else{
            $cpfMask = "%s%s%s.%s%s%s.%s%s%s - %s%s";
            $cpf = vsprintf($cpfMask, str_split($cpf));
        }
        return $cpf;
    }

    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");

        if (!$usuario)
            return "RGM ou senha inválidos!";
        if ($usuario['status'] == 'Bloqueado')
            return "O usuário está bloqueado!";
        if ($usuario['senha'] != $senha)
            return "A senha é inválida!";

        $usuario = $usuario->toArray();

        if (!$ns)
            $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);

        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);

        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';

        $loginNameSpace->alunousuario = serialize($usuario);

        return true;
    }

    public static function getUsuario($field = null, $ns = false) {
        $ns = (!$ns) ? Mn_Util::getAdminNameSpace() : $ns;

        $session = new Zend_Session_Namespace($ns);
        if (!isset($session->alunousuario))
            return false;

        $escola = unserialize($session->alunousuario);
        if (is_null($field))
            return $escola;

        return $escola[$field];
    }

    public static function getEscolaAtiva($id, $field = false) {
        $usuarios = new Escolasusuarios();
        $usuario = $usuarios->fetchRow("id=$id");

        if (!$usuario)
            return false;


        $idescola = (int) $usuario['idescola'];

        $escolas = new Escolas();
        $escola = $escolas->fetchRow("id=$idescola");
        if (!$escola)
            return false;

        if (!$field)
            return $escola;

        return $escola[$field];
    }

    public static function getEscolasalunosHelper($queries = array(), $page = 0, $maxpage = 0) {
        $produtos = new Escolasalunos();
        return $produtos->getEscolasalunos($queries, $page, $maxpage);
    }

    public function getEscolasalunoshaversine($queries = array(), $lat, $lng, $radius = '500', $extra = '') {
        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        $idescola = (isset($queries['idescola'])) ? (int) $queries['idescola'] : false;
        $idsecretaria = (isset($queries['idsecretaria'])) ? (int) $queries['idsecretaria'] : false;
        $idserie = (isset($queries['idserie'])) ? (int) $queries['idserie'] : false;
        $idperiodo = (isset($queries['idperiodo'])) ? (int) $queries['idperiodo'] : false;
        $idsescolas = (isset($queries['idsescolas'])) ? $queries['idsescolas'] : false;
        $notids = (isset($queries['notids'])) ? $queries['notids'] : false;
        $idsalunos = (isset($queries['idsalunos'])) ? $queries['idsalunos'] : false;
        $chave = (isset($queries['chave'])) ? $queries['chave'] : false;
        $alunocomplete = (isset($queries['alunocomplete'])) ? $queries['alunocomplete'] : false;
        $matricula = (isset($queries['matricula'])) ? $queries['matricula'] : false;
        $rgm = (isset($queries['rgm'])) ? $queries['rgm'] : false;
        $idclassificacao = (isset($queries['idclassificacao'])) ? (int) $queries['idclassificacao'] : false;

        $has_latitude = (isset($queries['has_latitude'])) ? $queries['has_latitude'] : false;
        $has_longitude = (isset($queries['has_longitude'])) ? $queries['has_longitude'] : false;

        $status = (isset($queries['status'])) ? $queries['status'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id)
            array_push($where, " ea1.id=$id ");
        //if ($notids) array_push($where, " ea1.id NOT IN ($notids) ");
        if ($idsalunos)
            array_push($where, " ea1.id IN ($idsalunos) ");
        if ($idescola)
            array_push($where, " ea1.idescola=$idescola ");
        if ($idsecretaria)
            array_push($where, " e1.idsecretaria=$idsecretaria ");
        if ($idserie)
            array_push($where, " ea1.idserie=$idserie ");
        if ((int) $idperiodo > 0)
            array_push($where, " ea1.idperiodo=$idperiodo ");
        if ($idsescolas)
            array_push($where, " FIND_IN_SET(ea1.idescola, '$idsescolas') ");
        if ($chave)
            array_push($where, " ((ea1.nomerazao LIKE '%$chave%') OR (ea1.sobrenomefantasia LIKE '%$chave%') OR (ea1.email LIKE '%$chave%')) ");
        if ($has_latitude)
            array_push($where, " (ea1.latitude<>'' AND ea1.latitude IS NOT NULL) ");
        if ($has_longitude)
            array_push($where, " (ea1.longitude<>'' AND ea1.longitude IS NOT NULL) ");
        if ($status)
            array_push($where, " ea1.status='$status' ");
        if ($alunocomplete)
            array_push($where, " ((ea1.nomerazao LIKE '%$alunocomplete%') OR (ea1.sobrenomefantasia LIKE '%$alunocomplete%')) ");
        if ($matricula)
            array_push($where, " ea1.matricula='$matricula' ");
        if ($rgm)
            array_push($where, " ea1.rgm='$rgm' ");
        if ((int) $idclassificacao > 0) {
            array_push($where, " ea1.idclassificacao=$idclassificacao ");
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "distinct ea1.*, ( 3959 * acos( cos( radians( $lat ) ) * cos( radians( `latitude` ) ) * cos( radians( `longitude` ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( `latitude` ) ) ) ) AS distance";
        if ($total)
            $fields = "COUNT(ea1.id) as total";


        $ordem = "ORDER BY distance";
        if ($order)
            $ordem = $order;

        $limit = "";
        //if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        $strsql = "SELECT $fields
					FROM escolasalunos ea1
					WHERE ea1.excluido='nao'
						$w
					$ordem
					$limit";

        //if(!$total) die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
        /*
          $output  = "SELECT distinct *,
          ( 3959 * acos( cos( radians( $lat ) ) * cos( radians( `latitude` ) ) * cos( radians( `longitude` ) - radians( $lng ) ) + sin( radians( $lat ) ) * sin( radians( `latitude` ) ) ) ) AS distance
          FROM `escolasalunos` HAVING distance <= $radius
          ORDER BY distance;";
          return $output; */
    }

    public function removeSpecialCaracters($str) {

        $notAllowed = array(".", "-");

        foreach ($notAllowed as $k => $v) {
            $newstr = str_replace($v, '', $str);
            $str = $newstr;
        }

        return trim($newstr);
    }

    public function getEscolasalunos($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        $idescola = (isset($queries['idescola'])) ? (int) $queries['idescola'] : false;
        $idsecretaria = (isset($queries['idsecretaria'])) ? (int) $queries['idsecretaria'] : false;
        $idserie = (isset($queries['idserie'])) ? (int) $queries['idserie'] : false;
        $idsAllserie = (isset($queries['allseries'])) ? $queries['allseries'] : false;
        $idperiodo = (isset($queries['idperiodo'])) ? (int) $queries['idperiodo'] : false;
        $idsescolas = (isset($queries['idsescolas'])) ? $queries['idsescolas'] : false;
        $notids = (isset($queries['notids'])) ? $queries['notids'] : false;
        $idsalunos = (isset($queries['idsalunos'])) ? $queries['idsalunos'] : false;
        $chave = (isset($queries['chave'])) ? $queries['chave'] : false;
        $alunocomplete = (isset($queries['alunocomplete'])) ? $queries['alunocomplete'] : false;
        $matricula = (isset($queries['matricula'])) ? $queries['matricula'] : false;
        $idclassificacao = (isset($queries['idclassificacao'])) ? (int) $queries['idclassificacao'] : false;
        $has_latitude = (isset($queries['has_latitude'])) ? $queries['has_latitude'] : false;
        $has_longitude = (isset($queries['has_longitude'])) ? $queries['has_longitude'] : false;
        $verifica_nutricao = (isset($queries['verifica_nutricao'])) ? $queries['verifica_nutricao'] : false;
        $merendadiferenciada = (isset($queries['merendadiferenciada'])) ? $queries['merendadiferenciada'] : false;
        $merendadiferenciadaaprovacao = (isset($queries['merendadiferenciadaaprovacao'])) ? $queries['merendadiferenciadaaprovacao'] : false;
        $tipodeficiencia = (isset($queries['tipodeficiencia'])) ? $queries['tipodeficiencia'] : false;
        $observacaodeficiencia = (isset($queries['observacaodeficiencia'])) ? $queries['observacaodeficiencia'] : false;
        $nutricao = (isset($queries['nutricao'])) ? $queries['nutricao'] : false;
        $status = (isset($queries['status'])) ? $queries['status'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;

        $nomerazao = (isset($queries['nomerazao'])) ? $queries['nomerazao'] : false;
        $ra = (isset($queries['ra'])) ? $queries['ra'] : false;
        $nomemae = (isset($queries['nomemae'])) ? $queries['nomemae'] : false;
        $nomepai = (isset($queries['nomepai'])) ? $queries['nomepai'] : false;
        $rg = (isset($queries['rg'])) ? $this->removeSpecialCaracters($queries['rg']) : false;
        $cpf = (isset($queries['cpf'])) ? $this->removeSpecialCaracters($queries['cpf']) : false;
        $tipoensino = (isset($queries['tipoensino'])) ? $queries['tipoensino'] : false;

        /* Deficiencias */
        $defnecespecial = (isset($queries['defnecespecial'])) ? $queries['defnecespecial'] : false;
        $defmultipla = (isset($queries['defmultipla'])) ? $queries['defmultipla'] : false;
        $defcegueira = (isset($queries['defcegueira'])) ? $queries['defcegueira'] : false;
        $defbaixavisao = (isset($queries['defbaixavisao'])) ? $queries['defbaixavisao'] : false;
        $defsurdezseveraprofunda = (isset($queries['defsurdezseveraprofunda'])) ? $queries['defsurdezseveraprofunda'] : false;
        $defsurdezlevemoderada = (isset($queries['defsurdezlevemoderada'])) ? $queries['defsurdezlevemoderada'] : false;
        $defsurdocegueira = (isset($queries['defsurdocegueira'])) ? $queries['defsurdocegueira'] : false;
        $deffisicaparalisiacerebral = (isset($queries['deffisicaparalisiacerebral'])) ? $queries['deffisicaparalisiacerebral'] : false;
        $deffisicacadeirante = (isset($queries['deffisicacadeirante'])) ? $queries['deffisicacadeirante'] : false;
        $deffisicaoutros = (isset($queries['deffisicaoutros'])) ? $queries['deffisicaoutros'] : false;
        $defsindromedown = (isset($queries['defsindromedown'])) ? $queries['defsindromedown'] : false;
        $defintelectual = (isset($queries['defintelectual'])) ? $queries['defintelectual'] : false;
        $defautistaclassico = (isset($queries['defautistaclassico'])) ? $queries['defautistaclassico'] : false;
        $defsindromeasperger = (isset($queries['defsindromeasperger'])) ? $queries['defsindromeasperger'] : false;
        $defsindromerett = (isset($queries['defsindromerett'])) ? $queries['defsindromerett'] : false;
        $deftransdesintegrativoinf = (isset($queries['deftransdesintegrativoinf'])) ? $queries['deftransdesintegrativoinf'] : false;
        $defaltashabsuperdotacao = (isset($queries['defaltashabsuperdotacao'])) ? $queries['defaltashabsuperdotacao'] : false;
        $defpermanente = (isset($queries['defpermanente'])) ? $queries['defpermanente'] : false;
        $deftemporaria = (isset($queries['deftemporaria'])) ? $queries['deftemporaria'] : false;
        $defmobilidadereduzida = (isset($queries['defmobilidadereduzida'])) ? $queries['defmobilidadereduzida'] : false;
        $defauxilioleitor = (isset($queries['defauxilioleitor'])) ? $queries['defauxilioleitor'] : false;
        $defauxiliotranscricao = (isset($queries['defauxiliotranscricao'])) ? $queries['defauxiliotranscricao'] : false;
        $defguiainterprete = (isset($queries['defguiainterprete'])) ? $queries['defguiainterprete'] : false;
        $definterpretelibras = (isset($queries['definterpretelibras'])) ? $queries['definterpretelibras'] : false;
        $defleituralabial = (isset($queries['defleituralabial'])) ? $queries['defleituralabial'] : false;
        $defnenhum = (isset($queries['defnenhum'])) ? $queries['defnenhum'] : false;
        $defprovabraile = (isset($queries['defprovabraile'])) ? $queries['defprovabraile'] : false;
        $defprovaampliada = (isset($queries['defprovaampliada'])) ? $queries['defprovaampliada'] : false;
        $deftam16 = (isset($queries['deftam16'])) ? $queries['deftam16'] : false;
        $deftam20 = (isset($queries['deftam20'])) ? $queries['deftam20'] : false;
        $deftam24 = (isset($queries['deftam24'])) ? $queries['deftam24'] : false;
        /* Fim Deficiencias */

        $soapstatus = (isset($queries['soapstatus'])) ? $queries['soapstatus'] : false;
        $soaptipo = (isset($queries['soaptipo'])) ? $queries['soaptipo'] : false;
        $soapretorno = (isset($queries['soapretorno'])) ? $queries['soapretorno'] : false;

        $where = array();

        if ($idsAllserie) {
            $grauSerie = explode(',', $idsAllserie);
            $grau = $grauSerie[0];
            $serie = $grauSerie[1];
        }
        if (isset($grau) && isset($serie)) {
            array_push($where, " et.idgdae = $grau AND et.serie = $serie and et.serie = ea1.serie and ea1.grau = $grau
								and ea1.serie = $serie ");
            $subTipos = 'gdae_identificadoresescolassubtipos et,';
        } else {
            $subTipos = '';
        }

        if ($nomerazao)
            array_push($where, " ea1.nomerazao LIKE '%$nomerazao%'");
        if ($ra)
            array_push($where, " ea1.ra LIKE '%$ra%' ");
        if ($nomemae)
            array_push($where, " ea1.nomemae LIKE '%$nomemae%'");
        if ($nomepai)
            array_push($where, " ea1.nomepai LIKE '%$nomepai%'");
        if ($rg)
            array_push($where, " ea1.rg LIKE '%$rg%'");
        if ($cpf)
            array_push($where, " ea1.cpfcnpj LIKE '%$cpf%'");

        if ($id)
            array_push($where, " ea1.id=$id ");
        //if ($notids) array_push($where, " ea1.id NOT IN ($notids) ");
        if ($idsalunos)
            array_push($where, " ea1.id IN ($idsalunos) ");
        if ($idescola)
            array_push($where, " ea1.idescola=$idescola ");
        if ($idsecretaria)
            array_push($where, " e1.idsecretaria=$idsecretaria ");
        if ($idserie)
            array_push($where, " ea1.idserie=$idserie ");
        if ((int) $idperiodo > 0)
            array_push($where, " ea1.idperiodo=$idperiodo ");
        if ($idsescolas)
            array_push($where, " FIND_IN_SET(ea1.idescola, '$idsescolas') ");
        if ($chave)
            array_push($where, " ((ea1.nomerazao LIKE '%$chave%') OR (ea1.sobrenomefantasia LIKE '%$chave%') OR (ea1.email LIKE '%$chave%')) ");
        if ($status)
            array_push($where, " ea1.status='$status' ");

        if ($defnecespecial)
            array_push($where, " ea1.defnecespecial='$defnecespecial') ");
        if ($defmultipla)
            array_push($where, " ea1.defmultipla='$defmultipla') ");
        if ($defcegueira)
            array_push($where, " ea1.defcegueira='$defcegueira') ");
        if ($defbaixavisao)
            array_push($where, " ea1.defbaixavisao='$defbaixavisao') ");
        if ($defsurdezseveraprofunda)
            array_push($where, " ea1.defsurdezseveraprofunda='$defsurdezseveraprofunda') ");
        if ($defsurdezlevemoderada)
            array_push($where, " ea1.defsurdezlevemoderada='$defsurdezlevemoderada') ");
        if ($defsurdocegueira)
            array_push($where, " ea1.defsurdocegueira='$defsurdocegueira') ");
        if ($deffisicaparalisiacerebral)
            array_push($where, " ea1.deffisicaparalisiacerebral='$deffisicaparalisiacerebral') ");
        if ($deffisicacadeirante)
            array_push($where, " ea1.deffisicacadeirante='$deffisicacadeirante') ");
        if ($deffisicaoutros)
            array_push($where, " ea1.deffisicaoutros='$deffisicaoutros') ");
        if ($defsindromedown)
            array_push($where, " ea1.defsindromedownv$defsindromedown') ");
        if ($defintelectual)
            array_push($where, " ea1.defintelectual='$defintelectual') ");
        if ($defautistaclassico)
            array_push($where, " ea1.defautistaclassico='$defautistaclassico') ");
        if ($defsindromeasperger)
            array_push($where, " ea1.defsindromeasperger='$defsindromeasperger') ");
        if ($defsindromerett)
            array_push($where, " ea1.defsindromerett='$defsindromerett') ");
        if ($deftransdesintegrativoinf)
            array_push($where, " ea1.deftransdesintegrativoinf='$deftransdesintegrativoinf') ");
        if ($defaltashabsuperdotacao)
            array_push($where, " ea1.defaltashabsuperdotacao='$defaltashabsuperdotacao') ");
        if ($defpermanente)
            array_push($where, " ea1.defpermanente='$defpermanente') ");
        if ($deftemporaria)
            array_push($where, " ea1.deftemporaria='$deftemporaria') ");
        if ($defmobilidadereduzida)
            array_push($where, " ea1.defmobilidadereduzida='$defmobilidadereduzida') ");
        if ($defauxilioleitor)
            array_push($where, " ea1.defauxilioleitor='$defauxilioleitor') ");
        if ($defauxiliotranscricao)
            array_push($where, " ea1.defauxiliotranscricao='$defauxiliotranscricao') ");
        if ($defguiainterprete)
            array_push($where, " ea1.defguiainterprete='$defguiainterprete') ");
        if ($definterpretelibras)
            array_push($where, " ea1.definterpretelibras='$definterpretelibras') ");
        if ($defleituralabial)
            array_push($where, " ea1.defleituralabial='$defleituralabial') ");
        if ($defnenhum)
            array_push($where, " ea1.defnenhum='$defnenhum') ");
        if ($defprovabraile)
            array_push($where, " ea1.defprovabraile='$defprovabraile') ");
        if ($defprovaampliada)
            array_push($where, " ea1.defprovaampliada='$defprovaampliada') ");
        if ($deftam16)
            array_push($where, " ea1.deftam16='$deftam16') ");
        if ($deftam20)
            array_push($where, " ea1.deftam20='$deftam20') ");
        if ($deftam24)
            array_push($where, " ea1.deftam24='$deftam24') ");

        $soapstatus = (isset($queries["soapstatus"])) ? $queries["soapstatus"] : false;
        if ($soapstatus)
            array_push($where, " ea1.soapstatus LIKE '%$soapstatus%' ");

        $soaptipo = (isset($queries["soaptipo"])) ? $queries["soaptipo"] : false;
        if ($soaptipo)
            array_push($where, " ea1.soaptipo LIKE '%$soaptipo%' ");

        $soapretorno = (isset($queries["soapretorno"])) ? $queries["soapretorno"] : false;
        if ($soapretorno)
            array_push($where, " ea1.soapretorno LIKE '%$soapretorno%' ");

        if ($merendadiferenciada) {
            if ($merendadiferenciada == 'Sim') {
                array_push($where, " ea1.merendadiferenciada='$merendadiferenciada' ");
            } else {
                array_push($where, " IFNULL(ea1.merendadiferenciada, 'Não')='$merendadiferenciada' ");
            }
        }
        if ($merendadiferenciadaaprovacao) {
            if ($merendadiferenciada == 'Sim') {
                array_push($where, " ea1.merendadiferenciadaaprovacao='$merendadiferenciadaaprovacao' ");
            } else {
                array_push($where, " IFNULL(ea1.merendadiferenciadaaprovacao, 'Não')='$merendadiferenciadaaprovacao' ");
            }
        }

        if ($has_latitude)
            array_push($where, " (ea1.latitude<>'' AND ea1.latitude IS NOT NULL) ");
        if ($has_longitude)
            array_push($where, " (ea1.longitude<>'' AND ea1.longitude IS NOT NULL) ");

        if ($alunocomplete)
            array_push($where, " ((ea1.nomerazao LIKE '%$alunocomplete%') OR (ea1.sobrenomefantasia LIKE '%$alunocomplete%')) ");
        if ($matricula)
            array_push($where, " ea1.matricula='$matricula' ");
        //if ($nutricao) array_push($where, " nutricao='$nutricao' ");
        if ((int) $idclassificacao > 0) {
            array_push($where, " ea1.idclassificacao=$idclassificacao ");
        }

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'ea1.nomerazao';
                if ($sorting[0] == 'soapretorno')
                    $sorting[0] = 'ea1.soapretorno';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "ea1.*, e1.escola ";
        if ($total)
            $fields = "COUNT(ea1.id) as total";

        $ordem = "ORDER BY ea1.id";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        if ($verifica_nutricao == "0") {
            $_strsql = "";
            if ($nutricao)
                $_strsql = " WHERE nutricao='$nutricao'";
            $strsql = "SELECT v1.* FROM(SELECT $fields, CASE WHEN EXISTS(SELECT n1.id FROM nutricoesimcs n1 WHERE n1.excluido='nao' AND n1.idescolaaluno = ea1.id ) THEN 'Sim'  ELSE 'Não' END as nutricao
							FROM $subTipos escolasseries es, escolastiposensinos ete, escolasalunos ea1
								LEFT JOIN escolas e1 ON e1.id=ea1.idescola

							WHERE ea1.excluido='nao'
								AND e1.excluido = 'nao'
								AND ete.id=es.idserietipo
								AND es.id=ea1.idserie
								$w
								$ordem
								$limit) as v1
						$_strsql
							";
        }else {
            $strsql = "SELECT $fields
						FROM escolasalunos ea1
							LEFT JOIN escolas e1 ON e1.id=ea1.idescola
						WHERE ea1.excluido='nao'
							AND e1.excluido = 'nao'
							$w
						$ordem
						$limit";
        }

        //if(!$total) die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEscolaalunoById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getEscolasalunos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getEscolaalunoByIdHelper($id, $queries = array()) {
        $rows = new Escolasalunos();
        return $rows->getEscolaalunoById($id, $queries);
    }

    public static function getNomeByIdHelper($id, $queries = array()) {
        $rows = new Escolasalunos();
        $usuario = $rows->getEscolaalunoById($id, $queries);
        if (!$usuario)
            return false;
        return $usuario['nomerazao'] . ' ' . $usuario['sobrenomefantasia'];
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasalunos
     */
    public function save($dados) {
        
        $novoRegistro = true;
        $integracao = false;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) {
            $row = $this->createRow();
        } else {
            $oldstatus = $row->status;
            /* $novoRegistro = false;
              $historico = new Escolashistoricos_Escolasalunos();
              $_historico = $row->toArray();

              $historico->arquiva($_historico);
             */
        }
        $formaingresso = false;
        $dados_anterior = array();
        $idescolaanterior = 0;
        if (((int) $id > 0) && (isset($dados['formaingresso'])) && ($dados['formaingresso'] == Escolasalunos::$_FORMA_INGRESSO_TRANSFERIDO)) {
            $formaingresso = true;
            $dados_anterior = $row;
            $idescolaanterior = $row['idescola'];
        }

        $row->idescola = (array_key_exists('idescola', $dados)) ? $dados['idescola'] : $row->idescola;
        $row->idendereco = (array_key_exists('idendereco', $dados)) ? $dados['idendereco'] : $row->idendereco;
        $row->idenderecoindicativo = (array_key_exists('idenderecoindicativo', $dados)) ? $dados['idenderecoindicativo'] : $row->idenderecoindicativo;
        $row->idclassificacao = (array_key_exists('idclassificacao', $dados)) ? $dados['idclassificacao'] : $row->idclassificacao;
        $row->nomerazao = (array_key_exists('nomerazao', $dados)) ? $dados['nomerazao'] : $row->nomerazao;
        $row->sobrenomefantasia = (array_key_exists('sobrenomefantasia', $dados)) ? $dados['sobrenomefantasia'] : $row->sobrenomefantasia;
        $row->email = (array_key_exists('email', $dados)) ? $dados['email'] : $row->email;
        $row->senha = (array_key_exists('senha', $dados)) ? $dados['senha'] : $row->senha;
        $row->cpfcnpj = (array_key_exists('cpfcnpj', $dados)) ? $dados['cpfcnpj'] : $row->cpfcnpj;

        $row->nascimento = (array_key_exists('nascimento', $dados)) ? $dados['nascimento'] : $row->nascimento;
        $row->nascimentomunicipio = (array_key_exists('nascimentomunicipio', $dados)) ? $dados['nascimentomunicipio'] : $row->nascimentomunicipio;
        $row->nascimentouf = (array_key_exists('nascimentouf', $dados)) ? $dados['nascimentouf'] : $row->nascimentouf;
        $row->nomemae = (array_key_exists('nomemae', $dados)) ? $dados['nomemae'] : $row->nomemae;
        $row->cpfcnpjmae = (array_key_exists('cpfcnpjmae', $dados)) ? $dados['cpfcnpjmae'] : $row->cpfcnpjmae;
        $row->rgmae = (array_key_exists('rgmae', $dados)) ? $dados['rgmae'] : $row->rgmae;
        $row->nascimentomae = (array_key_exists('nascimentomae', $dados)) ? $dados['nascimentomae'] : $row->nascimentomae;
        $row->nomepai = (array_key_exists('nomepai', $dados)) ? $dados['nomepai'] : $row->nomepai;
        $row->cpfcnpjpai = (array_key_exists('cpfcnpjpai', $dados)) ? $dados['cpfcnpjpai'] : $row->cpfcnpjpai;
        $row->rgpai = (array_key_exists('rgpai', $dados)) ? $dados['rgpai'] : $row->rgpai;
        $row->nascimentopai = (array_key_exists('nascimentopai', $dados)) ? $dados['nascimentopai'] : $row->nascimentopai;
        $row->naturalidade = (array_key_exists('naturalidade', $dados)) ? $dados['naturalidade'] : $row->naturalidade;
        //$row->nacionalidade = (array_key_exists('nacionalidade', $dados)) ? $dados['nacionalidade'] : $row->nacionalidade;
        $row->inNacionalidade = (array_key_exists('inNacionalidade', $dados)) ? $dados['inNacionalidade'] : $row->inNacionalidade;        
        $row->inPaisOrigem = (array_key_exists('inPaisOrigem', $dados)) ? $dados['inPaisOrigem'] : $row->inPaisOrigem;        
        $row->inDiaEntBrasil = (array_key_exists('inDiaEntBrasil', $dados)) ? $dados['inDiaEntBrasil'] : $row->inDiaEntBrasil;        
        $row->inDataEmissaoRGRNE = (array_key_exists('inDataEmissaoRGRNE', $dados)) ? $dados['inDataEmissaoRGRNE'] : $row->inDataEmissaoRGRNE;        
        
        $row->nacionalidadeentradadata = (array_key_exists('nacionalidadeentradadata', $dados)) ? $dados['nacionalidadeentradadata'] : $row->nacionalidadeentradadata;
        $row->nacionalidadetipo = (array_key_exists('nacionalidadetipo', $dados)) ? $dados['nacionalidadetipo'] : $row->nacionalidadetipo;
        $row->rg = (array_key_exists('rg', $dados)) ? $dados['rg'] : $row->rg;
        $row->idserie = (array_key_exists('idserie', $dados)) ? $dados['idserie'] : $row->idserie;
        $row->classificacao = (array_key_exists('classificacao', $dados)) ? $dados['classificacao'] : $row->classificacao;

        $row->idperiodo = (array_key_exists('idperiodo', $dados)) ? $dados['idperiodo'] : $row->idperiodo;
        $row->telefoneresidencial = (array_key_exists('telefoneresidencial', $dados)) ? $dados['telefoneresidencial'] : $row->telefoneresidencial;
        $row->telefonecontato = (array_key_exists('telefonecontato', $dados)) ? $dados['telefonecontato'] : $row->telefonecontato;
        $row->nomecontato = (array_key_exists('nomecontato', $dados)) ? $dados['nomecontato'] : $row->nomecontato;
        $row->matricula = (array_key_exists('matricula', $dados)) ? $dados['matricula'] : $row->matricula;
        $row->numeromatricula = (array_key_exists('numeromatricula', $dados)) ? $dados['numeromatricula'] : $row->numeromatricula;
        $row->ra = (array_key_exists('ra', $dados)) ? $dados['ra'] : $row->ra;
        $row->raunidadefederativa = (array_key_exists('raunidadefederativa', $dados)) ? $dados['raunidadefederativa'] : $row->raunidadefederativa;
        if (($row->matricula == Escolasalunos::$_MATRICULA_EFETIVADA) && ((!$row->dataefetivacao) || ($row->dataefetivacao == "0000-00-00 00:00:00") || ($row->dataefetivacao == ""))):
            $row->dataefetivacao = date('Y-m-d G:i:s');
        endif;
        $row->usuarioefetivacao = (array_key_exists('usuarioefetivacao', $dados)) ? $dados['usuarioefetivacao'] : $row->usuarioefetivacao;

        $row->cgm = (array_key_exists('cgm', $dados)) ? $dados['cgm'] : $row->cgm;
        $row->nis = (array_key_exists('nis', $dados)) ? $dados['nis'] : $row->nis;
        $row->sexo = (array_key_exists('sexo', $dados)) ? $dados['sexo'] : $row->sexo;
        $row->corraca = (array_key_exists('corraca', $dados)) ? $dados['corraca'] : $row->corraca;
        $row->mobilidadereduzida = (array_key_exists('mobilidadereduzida', $dados)) ? $dados['mobilidadereduzida'] : $row->mobilidadereduzida;
        $row->mobilidadetipo = (array_key_exists('mobilidadetipo', $dados)) ? $dados['mobilidadetipo'] : $row->mobilidadetipo;
        if (($novoRegistro) || (!$dados['idarquivodatanascimento'] === false)) {
            $row->idarquivodatanascimento = (array_key_exists('idarquivodatanascimento', $dados)) ? $dados['idarquivodatanascimento'] : $row->idarquivodatanascimento;
        }
        if (($novoRegistro) || (!$dados['idimagemfoto'] === false)) {
            $row->idimagemfoto = (isset($dados['idimagemfoto'])) ? $dados['idimagemfoto'] : $row->idimagemfoto;
        }
        //$row->idfoto = (array_key_exists('idfoto',$dados)) ? $dados['idfoto'] : $row->idfoto;
        $row->etnia = (array_key_exists('etnia', $dados)) ? $dados['etnia'] : $row->etnia;
        $row->quilombola = (array_key_exists('quilombola', $dados)) ? $dados['quilombola'] : $row->quilombola;
        $row->nova_cert_tipo_livro_reg = (array_key_exists('nova_cert_tipo_livro_reg', $dados)) ? $dados['nova_cert_tipo_livro_reg'] : $row->nova_cert_tipo_livro_reg;
        $row->digrg = (array_key_exists('digrg', $dados)) ? $dados['digrg'] : $row->digrg;
        $row->ufrg = (array_key_exists('ufrg', $dados)) ? $dados['ufrg'] : $row->ufrg;
        $row->municipio_de_nascimento = (array_key_exists('municipio_de_nascimento', $dados)) ? $dados['municipio_de_nascimento'] : $row->municipio_de_nascimento;
        $row->uf_mun_de_nascimento = (array_key_exists('uf_mun_de_nascimento', $dados)) ? $dados['uf_mun_de_nascimento'] : $row->uf_mun_de_nascimento;
        $row->data_emis_certidao_nasc = (array_key_exists('data_emis_certidao_nasc', $dados)) ? $dados['data_emis_certidao_nasc'] : $row->data_emis_certidao_nasc;
        $row->numero_certidao_nasc = (array_key_exists('numero_certidao_nasc', $dados)) ? $dados['numero_certidao_nasc'] : $row->numero_certidao_nasc;
        $row->nova_cert_cod_nasc_serv = (array_key_exists('nova_cert_cod_nasc_serv', $dados)) ? $dados['nova_cert_cod_nasc_serv'] : $row->nova_cert_cod_nasc_serv;
        $row->nova_cert_cod_acervo = (array_key_exists('nova_cert_cod_acervo', $dados)) ? $dados['nova_cert_cod_acervo'] : $row->nova_cert_cod_acervo;
        $row->nova_cert_cod_serv_reg_civil = (array_key_exists('nova_cert_cod_serv_reg_civil', $dados)) ? $dados['nova_cert_cod_serv_reg_civil'] : $row->nova_cert_cod_serv_reg_civil;
        $row->nova_cert_num_livro = (array_key_exists('nova_cert_num_livro', $dados)) ? $dados['nova_cert_num_livro'] : $row->nova_cert_num_livro;
        $row->nova_cert_num_folha = (array_key_exists('nova_cert_num_folha', $dados)) ? $dados['nova_cert_num_folha'] : $row->nova_cert_num_folha;
        $row->nova_cert_ano_reg_nasc = (array_key_exists('nova_cert_ano_reg_nasc', $dados)) ? $dados['nova_cert_ano_reg_nasc'] : $row->nova_cert_ano_reg_nasc;
        $row->nova_cert_num_termo_reg = (array_key_exists('nova_cert_num_termo_reg', $dados)) ? $dados['nova_cert_num_termo_reg'] : $row->nova_cert_num_termo_reg;
        $row->num_folha_certidao_nasc = (array_key_exists('num_folha_certidao_nasc', $dados)) ? $dados['num_folha_certidao_nasc'] : $row->num_folha_certidao_nasc;
        $row->num_livro_certidao_nasc = (array_key_exists('num_livro_certidao_nasc', $dados)) ? $dados['num_livro_certidao_nasc'] : $row->num_livro_certidao_nasc;
        $row->numero_certidao_nasc_nova = (array_key_exists('numero_certidao_nasc_nova', $dados)) ? $dados['numero_certidao_nasc_nova'] : $row->numero_certidao_nasc_nova;
        $row->digver_certidao_nasc_nova = (array_key_exists('digver_certidao_nasc_nova', $dados)) ? $dados['digver_certidao_nasc_nova'] : $row->digver_certidao_nasc_nova;
        $row->tipocertidao = (array_key_exists('tipocertidao', $dados)) ? $dados['tipocertidao'] : $row->tipocertidao;

        $row->inDistritoCertidao = (array_key_exists('inDistritoCertidao', $dados)) ? $dados['inDistritoCertidao'] : $row->inDistritoCertidao;
        $row->inMunicipioComarca = (array_key_exists('inMunicipioComarca', $dados)) ? $dados['inMunicipioComarca'] : $row->inMunicipioComarca;
        $row->inUFComarca = (array_key_exists('inUFComarca', $dados)) ? $dados['inUFComarca'] : $row->inUFComarca;
        $row->inDataEmissCertMatr = (array_key_exists('inDataEmissCertMatr', $dados)) ? $dados['inDataEmissCertMatr'] : $row->inDataEmissCertMatr;

        $row->rendafamiliar = (array_key_exists('rendafamiliar', $dados)) ? $dados['rendafamiliar'] : $row->rendafamiliar;
        $row->passeescolar = (array_key_exists('passeescolar', $dados)) ? $dados['passeescolar'] : $row->passeescolar;
        $row->transporteescolar = (array_key_exists('transporteescolar', $dados)) ? $dados['transporteescolar'] : $row->transporteescolar;
        $row->restricaoalimentar = (array_key_exists('restricaoalimentar', $dados)) ? $dados['restricaoalimentar'] : $row->restricaoalimentar;
        $row->observacoes = (array_key_exists('observacoes', $dados)) ? $dados['observacoes'] : $row->observacoes;

        $row->tituloeleitoralmae = (array_key_exists('tituloeleitoralmae', $dados)) ? $dados['tituloeleitoralmae'] : $row->tituloeleitoralmae;

        if (($novoRegistro) || (!$dados['idarquivonascimentomae'] === false)) {
            $row->idarquivonascimentomae = (array_key_exists('idarquivonascimentomae', $dados)) ? $dados['idarquivonascimentomae'] : $row->idarquivonascimentomae;
        }

        $row->tituloeleitoralpai = (array_key_exists('tituloeleitoralpai', $dados)) ? $dados['tituloeleitoralpai'] : $row->tituloeleitoralpai;
        if (($novoRegistro) || (!$dados['idarquivonascimentopai'] === false)) {
            $row->idarquivonascimentopai = (array_key_exists('idarquivonascimentopai', $dados)) ? $dados['idarquivonascimentopai'] : $row->idarquivonascimentopai;
        }

        $row->filiacao = (array_key_exists('filiacao', $dados)) ? $dados['filiacao'] : $row->filiacao;
        $row->nomeresponsavel = (array_key_exists('nomeresponsavel', $dados)) ? $dados['nomeresponsavel'] : $row->nomeresponsavel;
        $row->cpfcnpjresponsavel = (array_key_exists('cpfcnpjresponsavel', $dados)) ? $dados['cpfcnpjresponsavel'] : $row->cpfcnpjresponsavel;
        $row->rgresponsavel = (array_key_exists('rgresponsavel', $dados)) ? $dados['rgresponsavel'] : $row->rgresponsavel;
        $row->nascimentoresponsavel = (array_key_exists('nascimentoresponsavel', $dados)) ? $dados['nascimentoresponsavel'] : $row->nascimentoresponsavel;
        $row->tituloeleitoralresponsavel = (array_key_exists('tituloeleitoralresponsavel', $dados)) ? $dados['tituloeleitoralresponsavel'] : $row->tituloeleitoralresponsavel;
        if (($novoRegistro) || (!$dados['idarquivonascimentoresponsavel'] === false)) {
            $row->idarquivonascimentoresponsavel = (array_key_exists('idarquivonascimentoresponsavel', $dados)) ? $dados['idarquivonascimentoresponsavel'] : $row->idarquivonascimentoresponsavel;
        }
        $row->necessidadesespeciais = (array_key_exists('necessidadesespeciais', $dados)) ? $dados['necessidadesespeciais'] : $row->necessidadesespeciais;
        $row->descricoesnecessidadesespeciais = (array_key_exists('descricoesnecessidadesespeciais', $dados)) ? $dados['descricoesnecessidadesespeciais'] : $row->descricoesnecessidadesespeciais;
        $row->idinstituicaoapoio = (array_key_exists('idinstituicaoapoio', $dados)) ? $dados['idinstituicaoapoio'] : $row->idinstituicaoapoio;

        $row->ensinoorigem = (array_key_exists('ensinoorigem', $dados)) ? $dados['ensinoorigem'] : $row->ensinoorigem;
        $row->situacaoanoanterior = (array_key_exists('situacaoanoanterior', $dados)) ? $dados['situacaoanoanterior'] : $row->situacaoanoanterior;
        $row->formaingresso = (array_key_exists('formaingresso', $dados)) ? $dados['formaingresso'] : $row->formaingresso;

        $row->motivotransferencia = (array_key_exists('motivotransferencia', $dados)) ? $dados['motivotransferencia'] : $row->motivotransferencia;

        $row->transferenciabaixa = (array_key_exists('transferenciabaixa', $dados)) ? $dados['transferenciabaixa'] : $row->transferenciabaixa;
        $row->transferencialogusuario = (array_key_exists('transferencialogusuario', $dados)) ? $dados['transferencialogusuario'] : $row->transferencialogusuario;
        $row->transferenciadata = (array_key_exists('transferenciadata', $dados)) ? $dados['transferenciadata'] : $row->transferenciadata;


        $row->motivo = (array_key_exists('motivo', $dados)) ? $dados['motivo'] : $row->motivo;
        $row->situacaoatual = (array_key_exists('situacaoatual', $dados)) ? $dados['situacaoatual'] : $row->situacaoatual;
        $row->redeorigem = (array_key_exists('redeorigem', $dados)) ? $dados['redeorigem'] : $row->redeorigem;

        $row->estadocivilmae = (array_key_exists('estadocivilmae', $dados)) ? $dados['estadocivilmae'] : $row->estadocivilmae;
        $row->estadocivilpai = (array_key_exists('estadocivilpai', $dados)) ? $dados['estadocivilpai'] : $row->estadocivilpai;
        $row->estadocivilresponsavel = (array_key_exists('estadocivilresponsavel', $dados)) ? $dados['estadocivilresponsavel'] : $row->estadocivilresponsavel;

        $row->latitude = (isset($dados['latitude'])) ? $dados['latitude'] : $row->latitude;
        $row->longitude = (isset($dados['longitude'])) ? $dados['longitude'] : $row->longitude;
        if ((!$row->integracao) || ($row->integracao == "")) {
            $integracao = true;
            $row->integracao = "Sim";
        }

        $row->falecimento = (isset($dados['falecimento'])) ? $dados['falecimento'] : $row->falecimento;
        $row->datafalecimento = (isset($dados['datafalecimento'])) ? $dados['datafalecimento'] : $row->datafalecimento;

        $row->abandono = (isset($dados['abandono'])) ? $dados['abandono'] : $row->abandono;
        $row->dataabandono = (isset($dados['dataabandono'])) ? $dados['dataabandono'] : $row->dataabandono;
        $row->motivoabandono = (isset($dados['motivoabandono'])) ? $dados['motivoabandono'] : $row->motivoabandono;
        $row->estornarabandono = (isset($dados['estornarabandono'])) ? $dados['estornarabandono'] : $row->estornarabandono;
        $row->dataestornoabandono = (isset($dados['dataestornoabandono'])) ? $dados['dataestornoabandono'] : $row->dataestornoabandono;

        $row->estornarbaixamatricula = (isset($dados['estornarbaixamatricula'])) ? $dados['estornarbaixamatricula'] : $row->estornarbaixamatricula;
        $row->dataestorno = (isset($dados['dataestorno'])) ? $dados['dataestorno'] : $row->dataestorno;
        $row->rgm = (isset($dados['rgm'])) ? $dados['rgm'] : $row->rgm;

        $row->perguntasprofessores = (isset($dados['perguntasprofessores'])) ? $dados['perguntasprofessores'] : $row->perguntasprofessores;
        $row->perguntassecretaria = (isset($dados['perguntassecretaria'])) ? $dados['perguntassecretaria'] : $row->perguntassecretaria;

        $row->remanejamento = (isset($dados['remanejamento'])) ? $dados['remanejamento'] : $row->remanejamento;
        $row->remanejamentodata = (isset($dados['remanejamentodata'])) ? $dados['remanejamentodata'] : $row->remanejamentodata;
        $row->remanejamentoavaliado = (isset($dados['remanejamentoavaliado'])) ? $dados['remanejamentoavaliado'] : $row->remanejamentoavaliado;
        $row->merendadiferenciada = (isset($dados['merendadiferenciada'])) ? $dados['merendadiferenciada'] : $row->merendadiferenciada;
        $row->merendadiferenciadaaprovacao = (isset($dados['merendadiferenciadaaprovacao'])) ? $dados['merendadiferenciadaaprovacao'] : $row->merendadiferenciadaaprovacao;
        $row->gdaefalecimento = (isset($dados['gdaefalecimento'])) ? $dados['gdaefalecimento'] : $row->gdaefalecimento;
        $row->gdaeabandono = (isset($dados['gdaeabandono'])) ? $dados['gdaeabandono'] : $row->gdaeabandono;
        $row->gdaeabandonoestorno = (isset($dados['gdaeabandonoestorno'])) ? $dados['gdaeabandonoestorno'] : $row->gdaeabandonoestorno;
        $row->gdaeremanejamento = (isset($dados['gdaeremanejamento'])) ? $dados['gdaeremanejamento'] : $row->gdaeremanejamento;

        $row->status = (array_key_exists('status', $dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido', $dados)) ? $dados['excluido'] : $row->excluido;
        $row->logusuario = (array_key_exists('logusuario', $dados)) ? $dados['logusuario'] : $row->logusuario;
        $row->logdata = (array_key_exists('logdata', $dados)) ? $dados['logdata'] : $row->logdata;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');
        }

        /* Deficiencias */
        $row->defnecespecial = (array_key_exists('defnecespecial', $dados)) ? $dados['defnecespecial'] : $row->defnecespecial;
        $row->defmultipla = (array_key_exists('defmultipla', $dados)) ? $dados['defmultipla'] : $row->defmultipla;
        $row->defcegueira = (array_key_exists('defcegueira', $dados)) ? $dados['defcegueira'] : $row->defcegueira;
        $row->defbaixavisao = (array_key_exists('defbaixavisao', $dados)) ? $dados['defbaixavisao'] : $row->defbaixavisao;
        $row->defsurdezseveraprofunda = (array_key_exists('defsurdezseveraprofunda', $dados)) ? $dados['defsurdezseveraprofunda'] : $row->defsurdezseveraprofunda;
        $row->defsurdezlevemoderada = (array_key_exists('defsurdezlevemoderada', $dados)) ? $dados['defsurdezlevemoderada'] : $row->defsurdezlevemoderada;
        $row->defsurdocegueira = (array_key_exists('defsurdocegueira', $dados)) ? $dados['defsurdocegueira'] : $row->defsurdocegueira;
        $row->deffisicaparalisiacerebral = (array_key_exists('deffisicaparalisiacerebral', $dados)) ? $dados['deffisicaparalisiacerebral'] : $row->deffisicaparalisiacerebral;
        $row->deffisicacadeirante = (array_key_exists('deffisicacadeirante', $dados)) ? $dados['deffisicacadeirante'] : $row->deffisicacadeirante;
        $row->deffisicaoutros = (array_key_exists('deffisicaoutros', $dados)) ? $dados['deffisicaoutros'] : $row->deffisicaoutros;
        $row->defsindromedown = (array_key_exists('defsindromedown', $dados)) ? $dados['defsindromedown'] : $row->defsindromedown;
        $row->defintelectual = (array_key_exists('defintelectual', $dados)) ? $dados['defintelectual'] : $row->defintelectual;
        $row->defautistaclassico = (array_key_exists('defautistaclassico', $dados)) ? $dados['defautistaclassico'] : $row->defautistaclassico;
        $row->defsindromeasperger = (array_key_exists('defsindromeasperger', $dados)) ? $dados['defsindromeasperger'] : $row->defsindromeasperger;
        $row->defsindromerett = (array_key_exists('defsindromerett', $dados)) ? $dados['defsindromerett'] : $row->defsindromerett;
        $row->deftransdesintegrativoinf = (array_key_exists('deftransdesintegrativoinf', $dados)) ? $dados['deftransdesintegrativoinf'] : $row->deftransdesintegrativoinf;
        $row->defaltashabsuperdotacao = (array_key_exists('defaltashabsuperdotacao', $dados)) ? $dados['defaltashabsuperdotacao'] : $row->defaltashabsuperdotacao;
        $row->defpermanente = (array_key_exists('defpermanente', $dados)) ? $dados['defpermanente'] : $row->defpermanente;
        $row->deftemporaria = (array_key_exists('deftemporaria', $dados)) ? $dados['deftemporaria'] : $row->deftemporaria;
        $row->defmobilidadereduzida = (array_key_exists('defmobilidadereduzida', $dados)) ? $dados['defmobilidadereduzida'] : $row->defmobilidadereduzida;
        $row->defauxilioleitor = (array_key_exists('defauxilioleitor', $dados)) ? $dados['defauxilioleitor'] : $row->defauxilioleitor;
        $row->defauxiliotranscricao = (array_key_exists('defauxiliotranscricao', $dados)) ? $dados['defauxiliotranscricao'] : $row->defauxiliotranscricao;
        $row->defguiainterprete = (array_key_exists('defguiainterprete', $dados)) ? $dados['defguiainterprete'] : $row->defguiainterprete;
        $row->definterpretelibras = (array_key_exists('definterpretelibras', $dados)) ? $dados['definterpretelibras'] : $row->definterpretelibras;
        $row->defleituralabial = (array_key_exists('defleituralabial', $dados)) ? $dados['defleituralabial'] : $row->defleituralabial;
        $row->defnenhum = (array_key_exists('defnenhum', $dados)) ? $dados['defnenhum'] : $row->defnenhum;
        $row->defprovabraile = (array_key_exists('defprovabraile', $dados)) ? $dados['defprovabraile'] : $row->defprovabraile;
        $row->defprovaampliada = (array_key_exists('defprovaampliada', $dados)) ? $dados['defprovaampliada'] : $row->defprovaampliada;
        $row->deftam16 = (array_key_exists('deftam16', $dados)) ? $dados['deftam16'] : $row->deftam16;
        $row->deftam20 = (array_key_exists('deftam20', $dados)) ? $dados['deftam20'] : $row->deftam20;
        $row->deftam24 = (array_key_exists('deftam24', $dados)) ? $dados['deftam24'] : $row->deftam24;
        /* Fim Deficiencias */

        $row->soapstatus = (array_key_exists("soapstatus", $dados)) ? $dados["soapstatus"] : $row->soapstatus;
        $row->soaptipo = (array_key_exists("soaptipo", $dados)) ? $dados["soaptipo"] : $row->soaptipo;
        $row->soapretorno = (array_key_exists("soapretorno", $dados)) ? $dados["soapretorno"] : $row->soapretorno;
        
        $alunoid = $row->save();        

        $enderecos = new Alunosenderecos();
        $count = $enderecos->getEnderecoByIdAluno($alunoid);

        if (!$count) {
            $dados['enderecos']['idaluno'] = $alunoid;
            $endereco = $enderecos->save($dados['enderecos']);
        } else {
            $dados['enderecos']['id'] = $count['id'];
            $endereco = $enderecos->save($dados['enderecos']);
        }


        /*
          if($integracao){
          $operacao = "RealizarMatriculaAntecipada";
          }else{
          $operacao = "AlterarEnderecoFichaAluno";
          }

          $integracoes = new Gdae_Integracoes();

          if((!$row->integracao) || ($row->integracao=="")) {
          $integracoes->Enviaralunos($row['id'], $operacao);
          }

          if(($row->falecimento) && ($row->falecimento=="Sim") && ($row->gdaefalecimento!="Sim")){
          $integracoes->Enviarbaixamatriculafalecimento($row['id']);
          }

          if(($row->abandono) && ($row->abandono=="Sim") && ($row->gdaeabandono!="Sim")){
          $integracoes->Enviarregistrarabandono($row['id']);
          $row->gdaeabandono = "Sim";
          $row->gdaeabandonoestorno = "Não";
          $this->save($row);
          }

          if(($row->estornarabandono) && ($row->estornarabandono=="Sim") && ($row->gdaeabandonoestorno!="Sim")){
          $integracoes->Enviarestornarregistroabandono($row['id']);
          $row->gdaeabandono = "Não";
          $row->gdaeabandonoestorno = "Sim";
          $this->save($row);
          }

          if(($row->remanejamento) && ($row->remanejamento=="Sim") && ($row->gdaeremanejamento!="Sim")){
          $integracoes->Enviarremanejamentomatriculara($row['id']);
          $row->gdaeremanejamento = "Sim";
          $this->save($row);
          }elseif (($row->remanejamento) && ($row->remanejamento=="Não")){
          $row->gdaeremanejamento = "Não";
          $this->save($row);
          }

          if($formaingresso){

          $idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
          //var_dump($idescolaanterior); die();
          $integracoes->Enviarestornarbaixamatriculatransferencia($row, $dados_anterior, $idescolaanterior);
          }

          if(($formaingresso=="transferido") && ($row->transferenciabaixa == "Sim")){
          $integracoes->Enviarbaixarmatriculatransferencia($row, $dados_anterior, $idescolaanterior);
          }

          //if(!$integracao) $integracoes->Enviaralterardadospessoaisfichaaluno($row['id']);
          //$row->excluido = "sim";
          if($row->excluido == "sim") $integracoes->Enviarexclusaomatriculara($row['id']);

          if (array_key_exists('programas', $dados)) $this->setProgramas($dados['programas'], $row->id, $row->logusuario, $row->logdata);
         */
        $this->setContapais($row->nomerazao . ' ' . $row->sobrenomefantasia, $row->id, $row->nascimento, $row->logusuario, $row->logdata);
        
        return $row;
    }

    private function setProgramas($dados, $idaluno, $logusuario, $logdata) {

        if (!is_array($dados))
            return;

        $itens = new Escolasalunosprogramas();
        $idsprogramas = $dados['idsprogramas'];
        $situacoes = $dados['situacoes'];
        $ids = array();
        $ids = $dados['ids'];
        if (!$ids)
            return false;

        foreach ($idsprogramas as $i => $id) {
            $d = array();

            $d['id'] = (isset($ids[$i])) ? trim(strip_tags((int) $ids[$i])) : 0;
            $d['idaluno'] = $idaluno;
            $d['idprograma'] = trim(strip_tags($idsprogramas[$i]));
            $d['situacao'] = trim(strip_tags($situacoes[$i]));
            $d['excluido'] = 'nao';
            $d['logusuario'] = $logusuario;
            $d['logdata'] = $logdata;

            //var_dump($bancos);die();
            $_row = $itens->save($d);
            array_push($ids, $_row->id);
        }
        $programas = implode(",", $ids);

        if ($programas == "")
            $programas = "0";
        $strsql = "DELETE FROM escolasalunosprogramas WHERE idaluno=$idaluno AND id NOT IN ($programas)";
        $db = Zend_Registry::get('db');
        $db->query($strsql);
    }

    private function setContapais($nome, $idaluno, $nascimento, $logusuario, $logdata) {
        $rows = new Escolasalunospais();
        $row = $rows->getEscolapaiByIdaluno($idaluno);

        $senha = date('d/m/Y', strtotime($nascimento));
        $senha = str_replace("/", "", $senha);

        if (!$row) {
            $nome = str_replace(" ", "_", $nome);
            $nome = strtolower($nome);
            $nome = $nome . $idaluno;
            $dados = array();
            $dados['idaluno'] = $idaluno;
            $dados['usuario'] = $nome;
            $dados['senha'] = $senha;
            $dados['status'] = 'Ativo';
            $dados['excluido'] = 'nao';
            $dados['logusuario'] = $logusuario;
            $dados['logdata'] = $logdata;
            //var_dump($dados); die();
            $rows->save($dados);
        }
    }

    public function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}
