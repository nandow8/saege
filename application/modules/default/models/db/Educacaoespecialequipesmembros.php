<?php

/**
 * Define o modelo Educacaoespecialequipesmembros
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Educacaoespecialequipesmembros extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "educacaoespecialequipesmembros";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEducacaoespecialequipesmembrosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$educacaoespecialequipesmembros = new Educacaoespecialequipesmembros();
		return $educacaoespecialequipesmembros->getEducacaoespecialequipesmembros($queries, $page, $maxpage);
	}
	
	public function getEducacaoespecialequipesmembros($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");

		$idparent = (isset($queries["idparent"])) ? (int)$queries["idparent"] : false;
		if ($idparent) array_push($where, " e1.idparent = $idparent ");

		$idfuncionario = (isset($queries["idfuncionario"])) ? (int)$queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " e1.idfuncionario = $idfuncionario ");

		$idcargo = (isset($queries["idcargo"])) ? (int)$queries["idcargo"] : false;
		if ($idcargo) array_push($where, " e1.idcargo = $idcargo ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*";
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM educacaoespecialequipesmembros e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		
		$db = Zend_Registry::get('db');				
		
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEducacaoespecialequipeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEducacaoespecialequipesmembros($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEducacaoespecialequipeByIdHelper($id, $queries = array()) {
		$rows = new Educacaoespecialequipesmembros();
		return $rows->getEducacaoespecialequipeById($id, $queries);
	}

	public function getEducacaoespecialequipeByIdParent($idparent, $queries = array()) {
		if ($idparent==0) return false;
		
		$queries['idparent'] = $idparent;
		$rows = $this->getEducacaoespecialequipesmembros($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		return $rows;
	}
	
	public static function getEducacaoespecialequipeByIdParentHelper($idparent, $queries = array()) {
		$rows = new Educacaoespecialequipesmembros();
		return $rows->getEducacaoespecialequipeByIdParent($idparent, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Educacaoespecialequipesmembros
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idparent = (array_key_exists("idparent",$dados)) ? $dados["idparent"] : $row->idparent;
		$row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
		$row->idcargo = (array_key_exists("idcargo",$dados)) ? $dados["idcargo"] : $row->idcargo;
						
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		$row->save();
		
		return $row;
	}
	
}