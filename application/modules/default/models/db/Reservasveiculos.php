<?php

/**
 * Define o modelo Rematriculas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Reservasveiculos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "reservasveiculos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getReservasveiculosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$reservasveiculos = new Reservasveiculos();
		return $reservasveiculos->getReservasveiculos($queries, $page, $maxpage);
	}
	
	public function getReservasveiculos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");
		
		
		$idveiculo = (isset($queries["idveiculo"])) ? $queries["idveiculo"] : false;
		if ($idveiculo) array_push($where, " r1.idveiculo = '$idveiculo' ");
		 
		$idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " r1.idusuario LIKE '%$idusuario%' ");
		
		$datareserva = (isset($queries["datareserva"])) ? $queries["datareserva"] : false;
		if ($datareserva) array_push($where, " r1.datareserva LIKE '%$datareserva%' "); 
         
        $solicitarreserva = (isset($queries["solicitarreserva"])) ? $queries["solicitarreserva"] : false;
		if ($solicitarreserva) array_push($where, " r1.solicitarreserva = '$solicitarreserva' "); 
        
        $observacao = (isset($queries["observacao"])) ? $queries["observacao"] : false;
		if ($observacao) array_push($where, " r1.observacao = '$observacao' "); 
        
        $localdestino = (isset($queries["localdestino"])) ? $queries["localdestino"] : false;
		if ($localdestino) array_push($where, " r1.localdestino = '$localdestino' "); 
        
        $periodo  = (isset($queries["periodo"])) ? $queries["periodo"] : false;
		if ($periodo) array_push($where, " r1.periodo = '$periodo' "); 
         
	  
		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " r1.status LIKE '%$status%' ");
  

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*"; 
		;
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM reservasveiculos r1
					
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getReservasveiculoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getReservasveiculos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getReservasveiculoByIdHelper($id, $queries = array()) {
		$rows = new Reservasveiculos();
		return $rows->getReservasveiculosById($id, $queries);
	}		

	public function getUltimoReservasveiculos($queries = array()) {
     
		$rows =  $this->getReservasveiculos();
		if (sizeof($rows)==0) return false; 
		return $rows[0];
		
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Reservasveiculos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 

                if($id == 0){
                    $row->datacriacao = date("Y-m-d");
                }

		$row->idveiculo = (array_key_exists("idveiculo",$dados)) ? $dados["idveiculo"] : $row->idveiculo;
		$row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
		$row->datareserva = (array_key_exists("datareserva",$dados)) ? $dados["datareserva"] : $row->datareserva;
		$row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
                $row->periodo = (array_key_exists("periodo",$dados)) ? $dados["periodo"] : $row->periodo;
                $row->solicitarreserva = (array_key_exists("solicitarreserva",$dados)) ? $dados["solicitarreserva"] : $row->solicitarreserva;
                $row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
                $row->localdestino = (array_key_exists("localdestino",$dados)) ? $dados["localdestino"] : $row->localdestino;
                $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->datacriacao = (array_key_exists("datacriacao",$dados)) ? $dados["datacriacao"] : $row->datacriacao;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
//                echo '<pre>';
//                print_r($row);
//                die();
                
		$row->save();
		
		return $row;
	}
	
}