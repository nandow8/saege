<?php

/**
 * Define o modelo Licencas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Licencas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "licencas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	/**
	 * Retorno o objeto do Cadastro em sessao ou um campo
	 * @param string $field
	 * @return string|array
	 */
	public static function getLicenca($field = null) {
		$session = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if (!isset($session->licenca)) return false;
	
		$cadastro = unserialize($session->licenca);

		if (is_null($field)) return $cadastro;
	
		return (isset($cadastro[$field])) ? $cadastro[$field] : false;
	}	

	public static function getLicencasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$licencas = new Licencas();
		return $licencas->getLicencas($queries, $page, $maxpage);
	}
	
	public function getLicencas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$diferent_id = (isset($queries['diferent_id'])) ? (int)$queries['diferent_id'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$licenca = (isset($queries['licenca'])) ? $queries['licenca'] : false;
		$alias = (isset($queries['alias'])) ? $queries['alias'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$email = (isset($queries['email'])) ? $queries['email'] : false;
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		if ($id) array_push($where, " l1.id=$id ");
		if ($diferent_id) array_push($where, " l1.id<>$diferent_id ");
		if ($status) array_push($where, " l1.status='$status' ");
		if ($licenca) array_push($where, " l1.licenca LIKE '%$licenca%' ");
		if ($alias) array_push($where, " l1.alias LIKE '%$alias%' ");
		if ($chave) array_push($where, " (c1.nomerazao LIKE '%$chave%' OR c1.sobrenomefantasia LIKE '%$chave%') ");
		if ($email) array_push($where, " c1.email LIKE '%$email%' ");
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*, c1.nomerazao, c1.sobrenomefantasia, c1.email";
		if ($total) $fields = "COUNT(l1.id) as total";
		
		
		$ordem = "ORDER BY l1.licenca, alias";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM licencas l1 
						INNER JOIN cadastros c1 ON c1.id=l1.idcadastro 
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getLicencaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLicencas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	
	public static function getLicencaByIdHelper($id, $queries = array()) {
		$rows = new Licencas();
		return $rows->getLicencaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Licencas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
				
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date('Y-m-d H:i:s');
		}
	
		
		$row->datacriacao = (array_key_exists('datacriacao',$dados)) ? $dados['datacriacao'] : $row->datacriacao;
		$row->idcadastro = (array_key_exists('idcadastro',$dados)) ? $dados['idcadastro'] : $row->idcadastro;
		$row->licenca = (array_key_exists('licenca',$dados)) ? $dados['licenca'] : $row->licenca;
		$row->alias = (array_key_exists('alias',$dados)) ? $dados['alias'] : $row->alias;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		$row->save();

		if (array_key_exists('cadastro',$dados)) {
			$cadastros = new Cadastros();
			$cadastro = $dados['cadastro'];
			$cadastro['id'] = (int)$row->idcadastro;
			$cadastro['idlicenca'] = $row->id;
			$cadastro['status'] = $row->status;
			$cadastro['excluido'] = $row->excluido;
			$cadastro['logusuario'] = $row->logusuario;
			$cadastro['logdata'] = $row->logdata;
			
			$cadastro = $cadastros->save($cadastro);
		}
		
		$row->idcadastro = $cadastro->id;
		$row->save();
		
		return $row;
	}
	
	
}