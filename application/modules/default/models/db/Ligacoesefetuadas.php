<?php

/**
* Define o modelo Ligacoesefetuadas
*
* @author		Alexandre Martin Narciso		
* @uses        Zend_Db_Table_Abstract
* @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
* @version     1.0
*/
class Ligacoesefetuadas extends Zend_Db_Table_Abstract {
	
	/**
	* Define o nome da tabela
	* @var string
	*/
	protected $_name = "ligacoesefetuadas";
	
	/**
	* Define a chave primaria
	* @var integer
	*/
	protected $_primary = "id";
	
	public static function getLigacoesefetuadasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$ligacoesefetuadas = new Ligacoesefetuadas();
		return $ligacoesefetuadas->getLigacoesefetuadas($queries, $page, $maxpage);
	}
	
	public function getLigacoesefetuadas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		$localsolicitante = (isset($queries["localsolicitante"])) ? $queries["localsolicitante"] : false;
		if ($localsolicitante) array_push($where, " l1.localsolicitante LIKE '%$localsolicitante%' ");
		$funcionario = (isset($queries["funcionario"])) ? $queries["funcionario"] : false;
		if ($funcionario) array_push($where, " l1.funcionario LIKE '%$funcionario%' "); 
		$falarcom = (isset($queries["falarcom"])) ? $queries["falarcom"] : false;
		if ($falarcom) array_push($where, " l1.falarcom LIKE '%$falarcom%' ");
		$dataefetuada = (isset($queries["dataefetuada"])) ? $queries["dataefetuada"] : false;
		if ($dataefetuada) array_push($where, " l1.dataefetuada LIKE '%$dataefetuada%' ");
		$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
		if ($telefone) array_push($where, " l1.telefone LIKE '%$telefone%' ");
		
		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");
		
		
		
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";
		
		$fields = "l1.*"; 
		$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado "; ;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
		
		$strsql = "SELECT $fields 
		FROM ligacoesefetuadas l1
		LEFT JOIN enderecos e_idendereco ON e_idendereco.id=l1.idendereco 
		LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado	
		WHERE l1.excluido='nao' 
		$w 
		$ordem	
		$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLigacaoefetuadaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLigacoesefetuadas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLigacaoefetuadaByIdHelper($id, $queries = array()) {
		$rows = new Ligacoesefetuadas();
		return $rows->getLigacaoefetuadaById($id, $queries);
	}		
	
	
	/**
	* Salva o dados (INSERT OU UPDATE)
	* @param array dados
	* @return Ligacoesefetuadas
	*/
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->localsolicitante = (array_key_exists("localsolicitante",$dados)) ? $dados["localsolicitante"] : $row->localsolicitante;
		$row->funcionario = (array_key_exists("funcionario",$dados)) ? $dados["funcionario"] : $row->funcionario;
		$row->falarcom = (array_key_exists("falarcom",$dados)) ? $dados["falarcom"] : $row->falarcom;
		$row->assunto = (array_key_exists("assunto",$dados)) ? $dados["assunto"] : $row->assunto;
		$row->dataefetuada = (array_key_exists("dataefetuada",$dados)) ? $dados["dataefetuada"] : $row->dataefetuada;
		$row->horaefetuada = (array_key_exists("horaefetuada",$dados)) ? $dados["horaefetuada"] : $row->horaefetuada; 
		$row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone; 
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
		
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		
		
		$row->save();
		
		return $row;
	}
	
}