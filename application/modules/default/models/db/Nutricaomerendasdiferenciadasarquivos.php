<?php

/**
 * Define o modelo Nutricaomerendasdiferenciadasarquivos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Nutricaomerendasdiferenciadasarquivos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "nutricaomerendasdiferenciadasarquivos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getArquivosByIdNutricaomerendas($idnutricaomerendasdiferenciada, $order = 'pa1.ordem') {
		
		$strsql = "SELECT pa1.idarquivo, pa1.idnutricaomerendasdiferenciada, pa1.legenda, a1.* 
					FROM nutricaomerendasdiferenciadasarquivos pa1 
						LEFT JOIN arquivos ar1 ON a1.id=pa1.idarquivo
					WHERE pa1.excluido='nao' 
						AND pa1.idnutricaomerendasdiferenciada=$idnutricaomerendasdiferenciada 
					ORDER BY $order"; 
						
		$db = Zend_Registry::get('db');
		return $db->fetchAll($strsql);
	}
	
	public function setArquivos($idnutricaomerendasdiferenciada, $arquivos, $legendas) {
		//if (!is_array($arquivos)) return;
		
		$logdata = date('Y-m-d G:i:s');
		foreach ($arquivos as $i=>$idarquivo) {
			$d = array();
			
			$legenda = $legendas[$i];
			
			$d['idnutricaomerendasdiferenciada'] = $idnutricaomerendasdiferenciada;
			$d['idarquivo'] = $idarquivo;
			$d['legenda'] = $legenda;
			$d['ordem'] = $i;
			$d['excluido'] = 'nao';
			$d['logusuario'] = Usuarios::getUsuario('id');
			$d['logdata'] = $logdata;
			$this->save($d);
		}
		
		$ids = implode(",", $arquivos);
		if ($ids=="") $ids = "0";
		$strsql = "DELETE FROM nutricaomerendasdiferenciadasarquivos WHERE idnutricaomerendasdiferenciada=$idnutricaomerendasdiferenciada AND idarquivo NOT IN ($ids)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);		
	}

	public static function getNutricaomerendasArquivos($idnutricaomerendasdiferenciada = NULL, $status = NULL, $inverte = false, $limite = NULL) {
		$db = Zend_Registry::get('db');
		
		$w = "";
		if (!is_null($idnutricaomerendasdiferenciada)) $w .= " AND pa1.idnutricaomerendasdiferenciada = " . (int)$idnutricaomerendasdiferenciada;
		if (!is_null($status)) $w .= " AND pa1.status = '$status'"; 
		
		$l = "";
		if (!is_null($limite)) $l .= " LIMIT '$limite'";
		
		$order = "ORDER BY pa1.ordem";
		if ($inverte) $order .= " DESC";
		
		$strsql = "SELECT pa1.* 
					FROM nutricaomerendasdiferenciadasarquivos pa1 
					WHERE pa1.excluido = 'nao' 
					$w
					$order
					$l";
		//die($strsql);			
		return $db->fetchAll($strsql);
	}

	public static function getNutricaomerendasdiferenciadasarquivosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaomerendasdiferenciadasarquivos = new Nutricaomerendasdiferenciadasarquivos();
		return $nutricaomerendasdiferenciadasarquivos->getNutricaomerendasdiferenciadasarquivos($queries, $page, $maxpage);
	}
	
	public function getNutricaomerendasdiferenciadasarquivos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		$idlicenca = (isset($queries['idlicenca'])) ? (int)$queries['idlicenca'] : false;
		if ($idlicenca) array_push($where, " e1.idlicenca = $idlicenca ");
		


		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM nutricaomerendasdiferenciadasarquivos e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getNutricaomerendasedificacaoarquivoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNutricaomerendasdiferenciadasarquivos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNutricaomerendasedificacaoarquivoByIdHelper($id, $queries = array()) {
		$rows = new Nutricaomerendasdiferenciadasarquivos();
		return $rows->getNutricaomerendasedificacaoarquivoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaomerendasdiferenciadasarquivos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$idnutricaomerendasdiferenciada = (int)$dados['idnutricaomerendasdiferenciada'];
		$idarquivo = (int)$dados['idarquivo'];
		$row = $this->fetchRow("idnutricaomerendasdiferenciada=" . $idnutricaomerendasdiferenciada . " AND idarquivo=" . $idarquivo);	
		

		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idnutricaomerendasdiferenciada = (array_key_exists("idnutricaomerendasdiferenciada",$dados)) ? $dados["idnutricaomerendasdiferenciada"] : $row->idnutricaomerendasdiferenciada;
		$row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
		$row->legenda = (array_key_exists("legenda",$dados)) ? $dados["legenda"] : $row->legenda;
		$row->ordem = (array_key_exists("ordem",$dados)) ? $dados["ordem"] : $row->ordem;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
	
}