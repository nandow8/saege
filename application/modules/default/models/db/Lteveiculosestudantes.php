<?php

class Lteveiculosestudantes extends Zend_Db_Table_Abstract {
    
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "lteveiculosestudantes";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getVeiculosEstudantesHelper($queries = array(), $page = 0, $maxpage = 0) { 
        $produtos = new Lteveiculosestudantes();
        return $produtos->getVeiculosEstudantes($queries, $page, $maxpage);
    }
    
    public function getVeiculosEstudantes($queries = array(), $page = 0, $maxpage = 0) { 
        $id          = (isset($queries['id'])) ? (int)$queries['id'] : false;

        $total          = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order          = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();
        
        if ($id)          array_push($where, " lve.id=$id ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "le.*, lve.*, lf.*";
        if ($total) $fields = "COUNT(lf.id) as total";
        
        
        $ordem = "ORDER BY le.id";
        if ($order) $ordem = $order; 
        
        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        $strsql = "SELECT $fields 
                    FROM lteestudantes le
                    LEFT JOIN lteveiculosestudantes lve ON lve.idestudante=le.id
                    LEFT JOIN logisticafrotas lf ON lve.idveiculo=lf.id
                    WHERE lve.id is null
                    AND le.excluido='nao'
                        $w 
                    $ordem  
                    $limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;                        
        $db = Zend_Registry::get('db');             
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();

        return $db->fetchAll($strsql);      
    }

    public function getEntidadesTransporte($queries = array(), $page = 0, $maxpage = 0) {
        $id       = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $entidade = (isset($queries['entidade'])) ? $queries['entidade'] : false;
        $tipo     = (isset($queries['tipo'])) ? $queries['tipo'] : false;
        $ntipo    = (isset($queries['ntipo'])) ? $queries['ntipo'] : false;

        $status         = (isset($queries['status'])) ? $queries['status'] : false;
        $excluido       = (isset($queries['excluido'])) ? $queries['excluido'] : false;
        $total          = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order          = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id)       array_push($where, " vle.id = $id ");
        if ($entidade) array_push($where, " vle.entidade LIKE '%$entidade%' ");
        if ($tipo)     array_push($where, " vle.tipo = '$tipo' ");
        if ($ntipo)    array_push($where, " vle.tipo != '$ntipo' ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "vle.*";

        $ordem = "ORDER BY vle.entidade";

        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        if ($total) {
            $strsql = "SELECT count(id) as total FROM view_lte_entidades vle WHERE 1 $w";
        } else {
            $strsql = "SELECT $fields FROM view_lte_entidades vle WHERE 1 $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db');

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();

        return $db->fetchAll($strsql); 
    }
    
    public function getViewAlunos($queries = array(), $page = 0, $maxpage = 0) {

        $id   = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $tipo = (isset($queries['tipo'])) ? $queries['tipo'] : false;

        $identidade  = (isset($queries['identidade'])) ? (int)$queries['identidade'] : 0;

        $total          = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order          = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();
        
        if ($id) array_push($where, " vle.id=$id ");
        if ($tipo) array_push($where, " vle.tipo='$tipo' ");
        if ($identidade) array_push($where, " vle.identidade=$identidade ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "vle.*";

        if ($total) $fields = "COUNT(vle.id) as total";
        
        
        $ordem = "ORDER BY vle.id";
        if ($order) $ordem = $order; 
        
        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        $strsql = "SELECT $fields 
                    FROM view_lte_estudantes vle
                    LEFT JOIN lteveiculosestudantes lve ON (lve.idestudante=vle.id AND lve.tipoestudante=vle.tipo)
                    WHERE vle.id not IN (SELECT lve2.idestudante FROM lteveiculosestudantes lve2 WHERE lve2.excluido='nao' AND lve2.tipoestudante='$tipo')
                    AND vle.tipotransporte = 'fretado'
                    $w
                    GROUP BY vle.id
                    $ordem
                    $limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;                        
        $db = Zend_Registry::get('db');             
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();

        return $db->fetchAll($strsql);      
    }

    public function getVeiculos($queries = array(), $page = 0, $maxpage = 0) { 
        $id          = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $status      = (isset($queries['status'])) ? (int)$queries['status'] : false;

        $total          = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order          = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();
        
        if ($id)     array_push($where, " lf.id=$id ");
        if ($status) array_push($where, " lf.status='$status' ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lf.*";
        if ($total) $fields = "COUNT(lf.id) as total";
        
        
        $ordem = "ORDER BY lf.id";
        if ($order) $ordem = $order; 
        
        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        $strsql = "SELECT $fields 
                    FROM logisticafrotas lf
                    WHERE lf.excluido='nao'
                    AND lf.escolar='sim'
                        $w
                    $ordem
                    $limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;                        
        $db = Zend_Registry::get('db');             
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();
        
        return $db->fetchAll($strsql);      
    }

    public function getVinculosVeiculos($queries = array(), $page = 0, $maxpage = 0) { 
        $id          = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $idveiculo   = (isset($queries['idveiculo'])) ? (int)$queries['idveiculo'] : false;

        $total          = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order          = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();
        
        if ($id)        array_push($where, " lve.id=$id ");
        if ($idveiculo) array_push($where, " lve.idveiculo=$idveiculo");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "le.nome, le.nomesocial, le.datanascimento, le.email, le.telefone, le.celular, le.facebook, le.endereco, le.numero, le.bairro, le.cidade, le.idestado, le.sexo, le.complemento, le.rg, le.ufrg, le.cpf, le.tituloeleitor, le.ra, le.identidade, le.curso, round(le.subsidio,2) subsidio, le.periodo, le.semestre, le.status, le.excluido, le.observacoes, le.latitude, le.longitude, le.datacriacao, le.idimagem, lve.*";
        if ($total) $fields = "COUNT(lve.id) as total";
        
        
        $ordem = "ORDER BY lve.id";
        if ($order) $ordem = $order; 
        
        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        $strsql = "SELECT $fields 
                    FROM lteestudantes le
                    INNER JOIN lteveiculosestudantes lve ON lve.idestudante=le.id
                    WHERE lve.excluido='nao'
                        $w 
                    $ordem  
                    $limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;                        
        $db = Zend_Registry::get('db');             
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        var_dump($strsql); die();

        return $db->fetchAll($strsql);      
    }

    public function getVeiculosEstudantesById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getVeiculosEstudantes($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }

    public function getVeiculosById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getVeiculos($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }

    public function getVinculos($queries = array(), $page = 0, $maxpage = 0) { 
        $id        = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $idveiculo = (isset($queries['idveiculo'])) ? (int)$queries['idveiculo'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();
        
        if ($id) array_push($where, " lve.id=$id ");
        if ($idveiculo) array_push($where, " lve.idveiculo=$idveiculo ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lve.*, vle.nome";
        if ($total) $fields = "COUNT(lve.id) as total";
        
        
        $ordem = "ORDER BY lve.id";
        if ($order) $ordem = $order; 
        
        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        $strsql = "SELECT $fields 
                    FROM lteveiculosestudantes lve
                    LEFT JOIN view_lte_estudantes vle ON (lve.idestudante=vle.id AND lve.tipoestudante=vle.tipo)
                    WHERE lve.excluido='nao'
                    AND vle.tipotransporte = 'fretado'
                    $w
                    $ordem  
                    $limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;                        
        $db = Zend_Registry::get('db');             
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);      
    }

    public function getVinculosById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getVinculos($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }
    
    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Salasatribuicoes
     */
    public function save($dados) {
        $novoRegistro = true;
        
        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");
        
        if (!$row) { 
            $row = $this->createRow();  
            $row->datacriacao = date('Y-m-d G:i:s');
            $row->excluido = 'nao';
            $row->status = 'Ativo';
        }else {
            $novoRegistro = false;
        }

        $row->idveiculo     = (array_key_exists('idveiculo',$dados)) ? $dados['idveiculo'] : $row->idveiculo;
        $row->idestudante   = (array_key_exists('idestudante',$dados)) ? $dados['idestudante'] : $row->idestudante;
        $row->tipoestudante = (array_key_exists('tipoestudante',$dados)) ? $dados['tipoestudante'] : $row->tipoestudante;
        
        $row->status        = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido      = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
        $row->logusuario    = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
        $row->logdata       = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;

        $row->save();
        
        Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Veículo ".(($id===0)?"incluido":"salvo")." com sucesso!");

        return $row;
    }
}