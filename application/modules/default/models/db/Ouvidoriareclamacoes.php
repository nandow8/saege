<?php

/**
 * Define o modelo Ouvidoriareclamacoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Ouvidoriareclamacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "ouvidoriareclamacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getOuvidoriareclamacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$ouvidoriareclamacoes = new Ouvidoriareclamacoes();
		return $ouvidoriareclamacoes->getOuvidoriareclamacoes($queries, $page, $maxpage);
	}
	
	public function getOuvidoriareclamacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " o1.id = $id ");
		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " o1.sequencial LIKE '%$sequencial%' ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " o1.tipo LIKE '%$tipo%' ");

$idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " o1.idusuario = $idusuario ");

$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " o1.iddepartamento = $iddepartamento ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " o1.idescola = $idescola ");

$tipocontato = (isset($queries["tipocontato"])) ? $queries["tipocontato"] : false;
		if ($tipocontato) array_push($where, " o1.tipocontato LIKE '%$tipocontato%' ");

$reclamante = (isset($queries["reclamante"])) ? $queries["reclamante"] : false;
		if ($reclamante) array_push($where, " o1.reclamante LIKE '%$reclamante%' ");

$telefone1 = (isset($queries["telefone1"])) ? $queries["telefone1"] : false;
		if ($telefone1) array_push($where, " o1.telefone1 LIKE '%$telefone1%' ");

$telefone2 = (isset($queries["telefone2"])) ? $queries["telefone2"] : false;
		if ($telefone2) array_push($where, " o1.telefone2 LIKE '%$telefone2%' ");

$contato = (isset($queries["contato"])) ? $queries["contato"] : false;
		if ($contato) array_push($where, " o1.contato LIKE '%$contato%' ");

$reclamado = (isset($queries["reclamado"])) ? $queries["reclamado"] : false;
		if ($reclamado) array_push($where, " o1.reclamado LIKE '%$reclamado%' ");

$idtipo = (isset($queries["idtipo"])) ? $queries["idtipo"] : false;
		if ($idtipo) array_push($where, " o1.idtipo = $idtipo ");

$reclamacaoassunto = (isset($queries["reclamacaoassunto"])) ? $queries["reclamacaoassunto"] : false;
		if ($reclamacaoassunto) array_push($where, " o1.reclamacaoassunto LIKE '%$reclamacaoassunto%' ");

$reclamacaodescricoes = (isset($queries["reclamacaodescricoes"])) ? $queries["reclamacaodescricoes"] : false;
		if ($reclamacaodescricoes) array_push($where, " o1.reclamacaodescricoes = '$reclamacaodescricoes' ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " o1.observacoes = '$observacoes' ");

$posicao = (isset($queries["posicao"])) ? $queries["posicao"] : false;
		if ($posicao) array_push($where, " o1.posicao LIKE '%$posicao%' ");

$encaminhar = (isset($queries["encaminhar"])) ? $queries["encaminhar"] : false;
		if ($encaminhar) array_push($where, " o1.encaminhar LIKE '%$encaminhar%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " o1.status LIKE '%$status%' ");

$processoposicao = (isset($queries["processoposicao"])) ? $queries["processoposicao"] : false;
		if ($processoposicao) array_push($where, " o1.processoposicao = '$processoposicao' ");

$processoobservacoes = (isset($queries["processoobservacoes"])) ? $queries["processoobservacoes"] : false;
		if ($processoobservacoes) array_push($where, " o1.processoobservacoes = '$processoobservacoes' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "o1.*"; 
		;
		
		if ($total) $fields = "COUNT(o1.id) as total";
		
		$ordem = "ORDER BY o1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM ouvidoriareclamacoes o1
					
					WHERE o1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getOuvidoriareclamacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getOuvidoriareclamacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getOuvidoriareclamacaoByIdHelper($id, $queries = array()) {
		$rows = new Ouvidoriareclamacoes();
		return $rows->getOuvidoriareclamacaoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ouvidoriareclamacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
 $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->tipocontato = (array_key_exists("tipocontato",$dados)) ? $dados["tipocontato"] : $row->tipocontato;
 $row->reclamante = (array_key_exists("reclamante",$dados)) ? $dados["reclamante"] : $row->reclamante;
 $row->telefone1 = (array_key_exists("telefone1",$dados)) ? $dados["telefone1"] : $row->telefone1;
 $row->telefone2 = (array_key_exists("telefone2",$dados)) ? $dados["telefone2"] : $row->telefone2;
 $row->contato = (array_key_exists("contato",$dados)) ? $dados["contato"] : $row->contato;
 $row->reclamado = (array_key_exists("reclamado",$dados)) ? $dados["reclamado"] : $row->reclamado;
 $row->idtipo = (array_key_exists("idtipo",$dados)) ? $dados["idtipo"] : $row->idtipo;
 $row->reclamacaoassunto = (array_key_exists("reclamacaoassunto",$dados)) ? $dados["reclamacaoassunto"] : $row->reclamacaoassunto;
 $row->reclamacaodescricoes = (array_key_exists("reclamacaodescricoes",$dados)) ? $dados["reclamacaodescricoes"] : $row->reclamacaodescricoes;
 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
 $row->posicao = (array_key_exists("posicao",$dados)) ? $dados["posicao"] : $row->posicao;
 $row->encaminhar = (array_key_exists("encaminhar",$dados)) ? $dados["encaminhar"] : $row->encaminhar;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->processoposicao = (array_key_exists("processoposicao",$dados)) ? $dados["processoposicao"] : $row->processoposicao;
 $row->processoobservacoes = (array_key_exists("processoobservacoes",$dados)) ? $dados["processoobservacoes"] : $row->processoobservacoes;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}