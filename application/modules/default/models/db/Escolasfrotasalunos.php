<?php

class Escolasfrotasalunos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasfrotasalunos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getEscolasfrotasalunosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolasfrotasalunos();
		return $produtos->getEscolasfrotasalunos($queries, $page, $maxpage);
	}
	
	public function getEscolasfrotasalunos($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " ef1.id=$id ");	
		
		if ($chave) array_push($where, " ((ef1.resumo LIKE '%$chave%') OR (ef1.descricoes LIKE '%$chave%')) ");
		
		$idgaragem = (isset($queries["idgaragem"])) ? $queries["idgaragem"] : false;
		if ($idgaragem) array_push($where, " ef1.idgaragem = '$idgaragem' ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " ef1.idescola = '$idescola' ");

		$idfrotas = (isset($queries['idfrotas'])) ? $queries['idfrotas'] : false;
		if ($idfrotas) array_push($where, " ef1.idfrotas='$idfrotas' ");

		if ($status) array_push($where, " ef1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		//$fields = "ef1.*, lf1.ano, lf1.modelo, lf1.placa, m1.marca, f1.fabricante";
		$fields = "ef1.*, lf1.placa, m1.marca, f1.fabricante";
		
		if ($total) $fields = "COUNT(ef1.id) as total";
		
		$ordem = "ORDER BY ef1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasfrotasalunos ef1 
					 LEFT JOIN logisticafrotas lf1 ON lf1.id = ef1.idfrotas
					 LEFT JOIN marcas m1 ON m1.id = lf1.idmarca 
					 LEFT JOIN fabricantes f1 ON f1.id = lf1.idfabricante
					WHERE ef1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getEscolasfrotasalunoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasfrotasalunos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolasfrotasalunoByIdHelper($id, $queries = array()) {
		$rows = new Escolasfrotasalunos();
		return $rows->getEscolasfrotasalunoById($id, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasfrotas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row){
                    $row = $this->createRow();
                    $row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->idfrotas = (array_key_exists('idfrotas',$dados)) ? $dados['idfrotas'] : $row->idfrotas;	
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idendereco;	
		$row->idgaragem = (array_key_exists('idgaragem',$dados)) ? $dados['idgaragem'] : $row->idgaragem;

		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
	
	
}