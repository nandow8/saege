<?php

/**
 * Define o modelo Protocolosprocessos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Protocolosprocessos extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "protocolosprocessos";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getProtocolosprocessosHelper($queries = array(), $page = 0, $maxpage = 0) {
        $protocolosprocessos = new Protocolosprocessos();
        return $protocolosprocessos->getProtocolosprocessos($queries, $page, $maxpage);
    }

    public function getProtocolosprocessos($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $usuariodados = Usuarios::getUsuarioByIdHelper(Usuarios::getUsuario('id'));

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " p1.id = $id ");


        $sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
        if ($sequencial)
            array_push($where, " p1.sequencial LIKE '%$sequencial%' ");

        $datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
        if ($datalancamento_i)
            array_push($where, " p1.datalancamento >= '$datalancamento_i' ");

        $datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
        if ($datalancamento_f)
            array_push($where, " p1.datalancamento <= '$datalancamento_f' ");

        $horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
        if ($horalancamento)
            array_push($where, " p1.horalancamento = '$horalancamento' ");

        $idtipoprotocolo = (isset($queries["idtipoprotocolo"])) ? (int) $queries["idtipoprotocolo"] : false;
        if ($idtipoprotocolo)
            array_push($where, " p1.idtipoprotocolo = $idtipoprotocolo ");

        $tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
        if ($tipo)
            array_push($where, " p1.tipo LIKE '%$tipo%' ");

        $idusuariologado = (isset($queries["idusuariologado"])) ? (int) $queries["idusuariologado"] : false;
        if ($idusuariologado)
            array_push($where, " p1.idusuariologado = $idusuariologado ");
        /*
          $origem = (isset($queries["origem"])) ? $queries["origem"] : false;
          if ($origem) array_push($where, " p1.origem LIKE '%$origem%' ");

          $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
          if ($idescola) array_push($where, " p1.idescola = $idescolaencaminhar ");

          $iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
          if ($iddepartamento) array_push($where, " p1.iddepartamento = $iddepartamento ");
         */
        $nomesolicitante = (isset($queries["nomesolicitante"])) ? $queries["nomesolicitante"] : false;
        if ($nomesolicitante)
            array_push($where, " p1.nomesolicitante LIKE '%$nomesolicitante%' ");

        $telefone1 = (isset($queries["telefone1"])) ? $queries["telefone1"] : false;
        if ($telefone1)
            array_push($where, " p1.telefone1 LIKE '%$telefone1%' ");

        $telefone2 = (isset($queries["telefone2"])) ? $queries["telefone2"] : false;
        if ($telefone2)
            array_push($where, " p1.telefone2 LIKE '%$telefone2%' ");

        $contato = (isset($queries["contato"])) ? $queries["contato"] : false;
        if ($contato)
            array_push($where, " p1.contato LIKE '%$contato%' ");

        $email = (isset($queries["email"])) ? $queries["email"] : false;
        if ($email)
            array_push($where, " p1.email LIKE '%$email%' ");

        $titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
        if ($titulo)
            array_push($where, " p1.titulo LIKE '%$titulo%' ");

        $idtiposolicitacao = (isset($queries["idtiposolicitacao"])) ? $queries["idtiposolicitacao"] : false;
        if ($idtiposolicitacao)
            array_push($where, " p1.idtiposolicitacao = $idtiposolicitacao ");

        $solicitacao = (isset($queries["solicitacao"])) ? $queries["solicitacao"] : false;
        if ($solicitacao)
            array_push($where, " p1.solicitacao LIKE '%$solicitacao%' ");

        $observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
        if ($observacoes)
            array_push($where, " p1.observacoes = '$observacoes' ");

        $posicao = (isset($queries["posicao"])) ? $queries["posicao"] : false;
        if ($posicao)
            array_push($where, " p1.posicao LIKE '%$posicao%' ");

        $encaminhar = (isset($queries["encaminhar"])) ? $queries["encaminhar"] : false;
        if ($encaminhar)
            array_push($where, " p1.encaminhar LIKE '%$encaminhar%' ");

        $idperfilencaminhar = (isset($queries["idperfilencaminhar"])) ? $queries["idperfilencaminhar"] : false;
        if ($idperfilencaminhar)
            array_push($where, " p1.idperfilencaminhar = '$idperfilencaminhar' ");

        //idescolaencaminhar
        if ($usuariodados['idperfil'] == '29') {
            $idescolaorigem = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($usuariodados['idfuncionario'])['idescola'];
            $idescoladestino = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($usuariodados['idfuncionario'])['idescola'];

            if ($idescolaorigem or $idescoladestino)
                array_push($where, "( COALESCE(p2.idescolaorigem, 0) = '$idescolaorigem' or COALESCE(p3.idescoladestino, 0) = '$idescoladestino' )");
        }

        $idlocalencaminhar = (isset($queries["idlocalencaminhar"])) ? $queries["idlocalencaminhar"] : false;
        if ($idlocalencaminhar)
            array_push($where, " p1.idlocalencaminhar = '$idlocalencaminhar' ");

        $idusuarioencaminhar = (isset($queries["idusuarioencaminhar"])) ? $queries["idusuarioencaminhar"] : false;
        if ($idusuarioencaminhar)
            array_push($where, " p1.idusuarioencaminhar = '$idusuarioencaminhar' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " p1.status LIKE '%$status%' ");

        $idorigemtramitacao = $usuariodados['idperfil'];
        $iddestinotramitacao = $usuariodados['idperfil'];
        
        if ($usuariodados['idperfil'] !== '30') { //Administrador
            if ($iddestinotramitacao or $idorigemtramitacao) {
                array_push($where, "(( COALESCE(p2.idperfilorigem,p3.idperfilorigem, '0') = '$idorigemtramitacao' or COALESCE(p2.idperfildestino,p3.idperfildestino,'0') = '$iddestinotramitacao' ) or "
                        . " ( COALESCE(p3.idperfilorigem,p2.idperfilorigem, '0') = '$idorigemtramitacao' or COALESCE(p3.idperfildestino,p2.idperfildestino,'0') = '$iddestinotramitacao' ) )");
            }
        }

        if ($usuariodados['idperfil'] !== '29') { //Escolas
            $idorigem = (isset($queries["idorigem"])) ? $queries["idorigem"] : false;
            if ($idorigem)
                array_push($where, " p2.idperfilorigem = '$idorigem' ");

            $iddestino = (isset($queries["iddestino"])) ? $queries["iddestino"] : false;
            if ($iddestino)
                array_push($where, " p3.idperfildestino = '$iddestino' ");
        }
        
        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY p1." . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "p1.*, "
                . "COALESCE(p2.idperfilorigem,p3.idperfilorigem) as idperfilorigem, "
                . "COALESCE(p2.idperfildestino,p3.idperfildestino) as idperfildestino ";
        ;

        if ($total)
            $fields = "COUNT(p1.id) as total";

        $ordem = "ORDER BY p1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM protocolosprocessos p1
                                        LEFT JOIN protocolosprocessostramitacoes p2 on (p1.id = p2.idprotocoloprocesso and p2.id = (SELECT x.id from protocolosprocessostramitacoes x where p1.id = x.idprotocoloprocesso order by x.id asc limit 1))
                                        LEFT JOIN protocolosprocessostramitacoes p3 on (p1.id = p3.idprotocoloprocesso and p3.id = (SELECT z.id from protocolosprocessostramitacoes z where p1.id = z.idprotocoloprocesso order by z.id desc limit 1))
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";

//        echo '<pre>', '$strsql<br>';
//        print_r($strsql);
//        echo '</pre>';
        
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getProtocoloprocessoById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getProtocolosprocessos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getProtocolosprocessoByIdHelper($id, $queries = array()) {
        $rows = new Protocolosprocessos();
        return $rows->getProtocoloprocessoById($id, $queries);
    }

    public function getUltimoProtocoloprocesso($queries = array()) {
        $queries['order'] = 'ORDER BY p1.id DESC';
        $rows = $this->getProtocolosprocessos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Protocolosprocessos
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        //Copia antes de alterar
        $olddados = $this->getProtocoloprocessoById($row->id);

        $row->sequencial = (array_key_exists("sequencial", $dados)) ? $dados["sequencial"] : $row->sequencial;
        $row->datalancamento = (array_key_exists("datalancamento", $dados)) ? $dados["datalancamento"] : $row->datalancamento;
        $row->horalancamento = (array_key_exists("horalancamento", $dados)) ? $dados["horalancamento"] : $row->horalancamento;
        $row->idtipoprotocolo = (array_key_exists("idtipoprotocolo", $dados)) ? $dados["idtipoprotocolo"] : $row->idtipoprotocolo;
        $row->tipo = (array_key_exists("tipo", $dados)) ? $dados["tipo"] : $row->tipo;
        $row->idusuariologado = (array_key_exists("idusuariologado", $dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
        $row->origem = (array_key_exists("origem", $dados)) ? $dados["origem"] : $row->origem;
        $row->idescola = (array_key_exists("idescola", $dados)) ? $dados["idescola"] : $row->idescola;
        $row->iddepartamento = (array_key_exists("iddepartamento", $dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
        $row->nomesolicitante = (array_key_exists("nomesolicitante", $dados)) ? $dados["nomesolicitante"] : $row->nomesolicitante;
        $row->telefone1 = (array_key_exists("telefone1", $dados)) ? $dados["telefone1"] : $row->telefone1;
        $row->telefone2 = (array_key_exists("telefone2", $dados)) ? $dados["telefone2"] : $row->telefone2;
        $row->contato = (array_key_exists("contato", $dados)) ? $dados["contato"] : $row->contato;
        $row->email = (array_key_exists("email", $dados)) ? $dados["email"] : $row->email;
        $row->titulo = (array_key_exists("titulo", $dados)) ? $dados["titulo"] : $row->titulo;
        $row->idtiposolicitacao = (array_key_exists("idtiposolicitacao", $dados)) ? $dados["idtiposolicitacao"] : $row->idtiposolicitacao;
        $row->solicitacao = (array_key_exists("solicitacao", $dados)) ? $dados["solicitacao"] : $row->solicitacao;
        $row->observacoes = (array_key_exists("observacoes", $dados)) ? $dados["observacoes"] : $row->observacoes;
        $row->iddocumento = (array_key_exists("iddocumento", $dados)) ? $dados["iddocumento"] : $row->iddocumento;
        $row->posicao = (array_key_exists("posicao", $dados)) ? $dados["posicao"] : $row->posicao;
        $row->encaminhar = (array_key_exists("encaminhar", $dados)) ? $dados["encaminhar"] : $row->encaminhar;
        $row->idperfilencaminhar = (array_key_exists("idperfilencaminhar", $dados)) ? $dados["idperfilencaminhar"] : $row->idperfilencaminhar;
        $row->idescolaencaminhar = (array_key_exists("idescolaencaminhar", $dados)) ? $dados["idescolaencaminhar"] : $row->idescolaencaminhar;
        $row->idlocalencaminhar = (array_key_exists("idlocalencaminhar", $dados)) ? $dados["idlocalencaminhar"] : $row->idlocalencaminhar;
        $row->idusuarioencaminhar = (array_key_exists("idusuarioencaminhar", $dados)) ? $dados["idusuarioencaminhar"] : $row->idusuarioencaminhar;
        //$row->iddocumentoencaminhar = (array_key_exists("iddocumentoencaminhar",$dados)) ? $dados["iddocumentoencaminhar"] : $row->iddocumentoencaminhar;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;

        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;

        $row->save();

        $idperfildestino = $id > 0 ? (array_key_exists("idperfilencaminharhistorico", $dados)) ? $dados["idperfilencaminharhistorico"] : 0 : $row->idperfilencaminhar;
        $idescoladestino = $id > 0 ? (array_key_exists("idperfilencaminharhistorico", $dados)) ? $dados["idperfilencaminharhistorico"] : $row->idperfilencaminhar == 29 ? (array_key_exists("idescolaencaminharhistorico", $dados)) ? $dados["idescolaencaminharhistorico"] : $row->idescolaencaminhar : 0 : $row->idescolaencaminhar;
        $observacao = $id > 0 ? (array_key_exists("observacoeshistorico", $dados)) ? $dados["observacoeshistorico"] :
                'Atenção! Primeira tramitacão do Protocolo. Verificar informações e aquivos no protocolo inicial.' :
                'Atenção! Primeira tramitacão do Protocolo. Verificar informações e aquivos no protocolo inicial.';

        //Salva o histórico de tramitações
        if (
                ($id == 0 ) ||
                ((($olddados['idperfilencaminhar'] <> $idperfildestino) && ($idperfildestino > 0) ) ||
                (($olddados['idperfilencaminhar'] == $idperfildestino) && ($olddados['idescolaencaminhar'] <> $idescoladestino))) ||
                (($row->posicao == 'Concluido') || ($row->posicao == 'Cancelado'))
        ) {

            $tramitacoes = new Protocolosprocessostramitacoes();

            $idusu = (int) $row->logusuario;

            $usuario = Usuarios::getUsuarioByIdHelper($idusu);

            $dadostramitacao = array();
            $dadostramitacao['idprotocoloprocesso'] = $row->id;
            $dadostramitacao['idusuario'] = $usuario['id'];
            $dadostramitacao['idfuncionario'] = $usuario['idfuncionario'];
            $dadostramitacao['idperfilorigem'] = $usuario['idperfil'];
            //$dadostramitacao['idlocalorigem'] =    ($olddados['idlocalencaminhar'] > 0)   ?? Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($usuario['idfuncionario'])['idlocal'];
            $dadostramitacao['idescolaorigem'] = ($usuario['idperfil'] == 29) ? Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($usuario['idfuncionario'])['idescola'] : 0;
            $dadostramitacao['idusuarioorigem'] = $usuario['id'];
            $dadostramitacao['idperfildestino'] = (($row->posicao == 'Concluido') || ($row->posicao == 'Cancelado')) ? $usuario['idperfil'] : $idperfildestino;
            $dadostramitacao['idlocaldestino'] = (($row->posicao == 'Concluido') || ($row->posicao == 'Cancelado')) ? 0 : $row->idlocalencaminhar;
            $dadostramitacao['idescoladestino'] = (($row->posicao == 'Concluido') || ($row->posicao == 'Cancelado')) ? ($usuario['idperfil'] == 29) ? Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($usuario['idfuncionario'])['idescola'] : 0 : $idescoladestino;
            $dadostramitacao['idusuariodestino'] = (($row->posicao == 'Concluido') || ($row->posicao == 'Cancelado')) ? $usuario['id'] : $row->idusuarioencaminhar;
            $dadostramitacao['datacriacao'] = date('Y-m-d G:s:i');
            $dadostramitacao['observacao'] = $observacao;
            $dadostramitacao['excluido'] = 'nao';
            if (array_key_exists("iddocumentoencaminhar", $dados)) {
                $dadostramitacao['iddocumentohistorico'] = $dados["iddocumentoencaminhar"];
            }

            if (($row->posicao == 'Concluido') || ($row->posicao == 'Cancelado')) {
                $dadostramitacao['status'] = strtolower($row->posicao);
            } else {
                $dadostramitacao['status'] = 'aberto';
            }


            $tramitacoes->save($dadostramitacao);
        }
        return $row;
    }

}
