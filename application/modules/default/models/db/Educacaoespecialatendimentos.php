<?php

/**
 * Define o modelo Educacaoespecialatendimentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Educacaoespecialatendimentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "educacaoespecialatendimentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getSequencial() {
		$db = Zend_Registry::get('db');

		$last_seq = $db->fetchRow("SELECT sequencial FROM educacaoespecialatendimentos ORDER BY datacriacao DESC LIMIT 0,1");

		if(!$last_seq) {
			return "1/".date("Y");
		}

		$seq = explode($last_seq, "/");
		$sequencial = ($seq[0]+1).'/'.date('Y');

		return $sequencial;	
	}
	
	public static function getEducacaoespecialatendimentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$educacaoespecialatendimentos = new Educacaoespecialatendimentos();
		return $educacaoespecialatendimentos->getEducacaoespecialatendimentos($queries, $page, $maxpage);
	}
	
	public function getEducacaoespecialatendimentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$dataabertura_i = (isset($queries["dataabertura_i"])) ? $queries["dataabertura_i"] : false;
		if ($dataabertura_i) array_push($where, " e1.dataabertura >= '$dataabertura_i' ");

$dataabertura_f = (isset($queries["dataabertura_f"])) ? $queries["dataabertura_f"] : false;
		if ($dataabertura_f) array_push($where, " e1.dataabertura <= '$dataabertura_f' ");

$numeroprocesso = (isset($queries["numeroprocesso"])) ? $queries["numeroprocesso"] : false;
		if ($numeroprocesso) array_push($where, " e1.numeroprocesso LIKE '%$numeroprocesso%' ");

$idprofissional = (isset($queries["idprofissional"])) ? $queries["idprofissional"] : false;
		if ($idprofissional) array_push($where, " e1.idprofissional = $idprofissional ");

$idsolicitacao = (isset($queries["idsolicitacao"])) ? $queries["idsolicitacao"] : false;
		if ($idsolicitacao) array_push($where, " e1.idsolicitacao = $idsolicitacao ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM educacaoespecialatendimentos e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEducacaoespecialatendimentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEducacaoespecialatendimentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEducacaoespecialatendimentoByIdHelper($id, $queries = array()) {
		$rows = new Educacaoespecialatendimentos();
		return $rows->getEducacaoespecialatendimentoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Educacaoespecialatendimentos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
		$row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;

		 $row->dataabertura = (array_key_exists("dataabertura",$dados)) ? $dados["dataabertura"] : $row->dataabertura;
 $row->numeroprocesso = (array_key_exists("numeroprocesso",$dados)) ? $dados["numeroprocesso"] : $row->numeroprocesso;
 $row->idprofissional = (array_key_exists("idprofissional",$dados)) ? $dados["idprofissional"] : $row->idprofissional;
 $row->idsolicitacao = (array_key_exists("idsolicitacao",$dados)) ? $dados["idsolicitacao"] : $row->idsolicitacao;
 $row->visitaunidade = (array_key_exists("visitaunidade",$dados)) ? $dados["visitaunidade"] : $row->visitaunidade;
 $row->idtriagem = (array_key_exists("idtriagem",$dados)) ? $dados["idtriagem"] : $row->idtriagem;
 $row->escutafamilia = (array_key_exists("escutafamilia",$dados)) ? $dados["escutafamilia"] : $row->escutafamilia;
 $row->escutaprofessor = (array_key_exists("escutaprofessor",$dados)) ? $dados["escutaprofessor"] : $row->escutaprofessor;
 $row->visitadomiciliar = (array_key_exists("visitadomiciliar",$dados)) ? $dados["visitadomiciliar"] : $row->visitadomiciliar;
 $row->idavaliacao = (array_key_exists("idavaliacao",$dados)) ? $dados["idavaliacao"] : $row->idavaliacao;
 $row->idencaminhamento = (array_key_exists("idencaminhamento",$dados)) ? $dados["idencaminhamento"] : $row->idencaminhamento;
 $row->datadevolutiva = (array_key_exists("datadevolutiva",$dados)) ? $dados["datadevolutiva"] : $row->datadevolutiva;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		$row->save();
		
		return $row;
	}
	
}