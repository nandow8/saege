<?php

/**
 * Define o modelo Documentacaosolicitacoespontos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Documentacaosolicitacoespontos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "documentacaosolicitacoespontos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDocumentacaosolicitacoespontosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$documentacaosolicitacoespontos = new Documentacaosolicitacoespontos();
		return $documentacaosolicitacoespontos->getDocumentacaosolicitacoespontos($queries, $page, $maxpage);
	}
	
	public function getDocumentacaosolicitacoespontos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$sum = (isset($queries['sum'])) ? $queries['sum'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		$idprofessor = (isset($queries["idprofessor"])) ? $queries["idprofessor"] : false;
		if ($idprofessor) array_push($where, " d1.idprofessor = $idprofessor ");
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " d1.idescola = $idescola ");

$idcurso = (isset($queries["idcurso"])) ? $queries["idcurso"] : false;
		if ($idcurso) array_push($where, " d1.idcurso = $idcurso ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " d1.status LIKE '%$status%' ");

$posicao = (isset($queries["posicao"])) ? $queries["posicao"] : false;
		if ($posicao) array_push($where, " d1.posicao LIKE '%$posicao%' ");
		
		$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " d1.datacriacao >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " d1.datacriacao <= '$data_f' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*, c1.titulo as curso"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		if ($sum) $fields = "SUM(d1.pontuacao) as total";
		
		$ordem = "ORDER BY d1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM documentacaosolicitacoespontos d1
					LEFT JOIN cursos c1 ON c1.id = d1.idcurso 
					
					WHERE d1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	

		if ($sum) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDocumentacaosolicitacoespontoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDocumentacaosolicitacoespontos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}

	
	public function getDocumentacaosolicitacoespontoByIdUsuarioByIdCurso($idusuario, $idcurso, $queries = array()) {
		if ($idusuario==0) return false;
		if ($idcurso==0) return false;
		
		$queries['idprofessor'] = $idusuario;
		$queries['idcurso'] = $idcurso;
		$rows = $this->getDocumentacaosolicitacoespontos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTotalpontosDocumentacaosolicitacoespontoByIdfuncionarioHelper($id, $queries = array()) {
		$rows = new Documentacaosolicitacoespontos();
		return $rows->getTotalpontosDocumentacaosolicitacoespontoByIdfuncionario($id, $queries);
	}		
	
	public function getTotalpontosDocumentacaosolicitacoespontoByIdfuncionario($idprofessor, $queries = array()) {
		if ($idprofessor==0) return false;
		
		$queries['sum'] = true;
		$queries['idprofessor'] = $idprofessor;
		$rows = $this->getDocumentacaosolicitacoespontos($queries, 0, 0);
		
		return $rows;
	}
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Documentacaosolicitacoespontos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
	 	$row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
		$row->idprofessor = (array_key_exists("idprofessor",$dados)) ? $dados["idprofessor"] : $row->idprofessor;
		$row->idcurso = (array_key_exists("idcurso",$dados)) ? $dados["idcurso"] : $row->idcurso;
		$row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
		$row->posicao = (array_key_exists("posicao",$dados)) ? $dados["posicao"] : $row->posicao;
		$row->pontuacao = (array_key_exists("pontuacao",$dados)) ? $dados["pontuacao"] : $row->pontuacao;
		$row->numerofilhos = (array_key_exists("numerofilhos",$dados)) ? $dados["numerofilhos"] : $row->numerofilhos;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		
		//return true;		
		$row->save();
		
		return $row;
	}
	
}