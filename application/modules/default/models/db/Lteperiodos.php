<?php

class Lteperiodos extends Zend_Db_Table_Abstract {
    
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "lteperiodos";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";     
    
    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");
            
        if (!$usuario) return "RGM ou senha inválidos!";
        if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
        if ($usuario['senha']!=$senha) return "A senha é inválida!";
        
        $usuario = $usuario->toArray();

        if (!$ns) $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);
        
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);
        
        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';
        
        $loginNameSpace->alunousuario = serialize($usuario);
                        
        return true;        
    }   
    
    public static function getUsuario($field = null, $ns = false) {
        $ns = (!$ns) ? Mn_Util::getAdminNameSpace() : $ns;
        
        $session = new Zend_Session_Namespace($ns);
        if (!isset($session->alunousuario)) return false;
        
        $escola = unserialize($session->alunousuario);
        if (is_null($field)) return $escola;
        
        return $escola[$field];     
    }   
    
    public function getPeriodos($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $datainicio = (isset($queries['datainicio'])) ? $queries['datainicio'] : false;
        $datafim = (isset($queries['datafim'])) ? $queries['datafim'] : false;
        $fechado = (isset($queries['fechado'])) ? $queries['fechado'] : false;
        $tipotransporte = (isset($queries['tipotransporte'])) ? $queries['tipotransporte'] : false;

        $total  = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order  = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " le.id = $id ");
        if ($datainicio && !$datafim) array_push($where, " datainicio=$datainicio ");
        if ($datainicio && $datafim) array_push($where, " datainicio>=$datainicio AND datafim<=$datafim ");
        if ($fechado) array_push($where, " le.fechado = 'sim' ");
        if ($tipotransporte) array_push($where, " le.tipotransporte = '$tipotransporte' ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "le.id id, le.datainicio datainicio, le.datafim datafim, le.diasletivos diasletivos, round(le.valorunitario,2) valorunitario, le.tipo tipo, le.tipotransporte tipotransporte, le.observacoes observacoes, le.status status, le.excluido excluido, le.fechado fechado, le.bolsista, le.datacriacao datacriacao, le.logusuario logusuario, le.logdata logdata, le.curso";

        $ordem = "ORDER BY le.datainicio desc";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(id) as total FROM lteperiodos le WHERE le.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM lteperiodos le WHERE le.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();
        
        return $db->fetchAll($strsql);
    }

    public function getPeriodosById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getPeriodos($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }
    
    public static function getPeriodosByIdHelper($id, $queries = array()) {
        $rows = new Lteperiodos();
        return $rows->getPeriodosById($id, $queries);
    }       

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Lteperiodo
     */
    public function save($dados) {
        
        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) {
            $row = $this->createRow();
        }else {
            
        }

        $row->datainicio = (array_key_exists('datainicio',$dados)) ? $dados['datainicio'] : $row->datainicio;
        $row->datafim = (array_key_exists('datafim',$dados)) ? $dados['datafim'] : $row->datafim;
        $row->diasletivos = (array_key_exists('diasletivos',$dados)) ? $dados['diasletivos'] : $row->diasletivos;
        $row->valorunitario = (array_key_exists('valorunitario',$dados)) ? $dados['valorunitario'] : $row->valorunitario;
        if ($id == 0) {
            $row->tipo = (array_key_exists('tipo',$dados)) ? $dados['tipo'] : $row->tipo;
            $row->tipotransporte = (array_key_exists('tipotransporte',$dados)) ? $dados['tipotransporte'] : $row->tipotransporte;
            $row->curso = (array_key_exists('curso',$dados)) ? $dados['curso'] : $row->curso;
            $row->bolsista = (array_key_exists('bolsista',$dados)) ? $dados['bolsista'] : $row->bolsista;
        }
        $row->observacoes = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;
        $row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
        $row->fechado = (array_key_exists('fechado',$dados)) ? $dados['fechado'] : $row->fechado;

        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;

        $row->usuariofechamento = (array_key_exists("usuariofechamento",$dados)) ? $dados["usuariofechamento"] : $row->usuariofechamento;
        $row->datafechamento = (array_key_exists("datafechamento",$dados)) ? $dados["datafechamento"] : $row->datafechamento;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');
            $row->usuariocriacao = Usuarios::getUsuario('id');
        }
        
        $row->save();
        
        if($id===0){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::incluir", Usuarios::getUsuario("id"), "Período adicionado com sucesso!");
        }elseif($id!== 0 AND isset($row->excluido) AND $row->excluido==="sim"){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::excluir", Usuarios::getUsuario("id"), "Período de ID ".$id." excluído com sucesso!");
        } else Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::atualizar", Usuarios::getUsuario("id"), "Período de ID ".$id." atualizado com sucesso!");
        
        return $row;
    }  
}
