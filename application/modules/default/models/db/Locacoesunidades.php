<?php

/**
 * Define o modelo Locacoesunidades
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Locacoesunidades extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "locacoesunidades";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLocacoesunidadesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$locacoesunidades = new Locacoesunidades();
		return $locacoesunidades->getLocacoesunidades($queries, $page, $maxpage);
	}
	
	public function getLocacoesunidades($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		
		$idlocador = (isset($queries["idlocador"])) ? $queries["idlocador"] : false;
		if ($idlocador) array_push($where, " l1.idlocador = $idlocador ");

$idimovel = (isset($queries["idimovel"])) ? $queries["idimovel"] : false;
		if ($idimovel) array_push($where, " l1.idimovel = $idimovel ");

$unidade = (isset($queries["unidade"])) ? $queries["unidade"] : false;
		if ($unidade) array_push($where, " l1.unidade LIKE '%$unidade%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");
		
		$idendereco_existe = (isset($queries['idendereco_existe'])) ? (int)$queries['idendereco_existe'] : false;  
		if ($idendereco_existe) array_push($where, " l1.idendereco > 0 ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*, li1.idendereco as liidendereco "; 
		;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM locacoesunidades l1
						LEFT JOIN locacoesimoveis li1 ON li1.id = l1.idimovel 
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLocacaounidadeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLocacoesunidades($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLocacaounidadeByIdHelper($id, $queries = array()) {
		$rows = new Locacoesunidades();
		return $rows->getLocacaounidadeById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Locacoesunidades
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idlocador = (array_key_exists("idlocador",$dados)) ? $dados["idlocador"] : $row->idlocador;
 $row->idimovel = (array_key_exists("idimovel",$dados)) ? $dados["idimovel"] : $row->idimovel;
 $row->unidade = (array_key_exists("unidade",$dados)) ? $dados["unidade"] : $row->unidade;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}