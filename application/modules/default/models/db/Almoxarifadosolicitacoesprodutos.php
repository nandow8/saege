<?php

class Almoxarifadosolicitacoesprodutos extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "almoxarifadosolicitacoesprodutos";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public function getSolicitacoesProdutos($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $idlocal = (isset($queries['idlocal'])) ? (int)$queries['idlocal'] : false;
        $idresponsavel = (isset($queries['idresponsavel'])) ? (int)$queries['idresponsavel'] : false;
        $idestoque = (isset($queries['idestoque'])) ? (int)$queries['idestoque'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " asp.id = $id ");
        if ($idlocal) array_push($where, " vae.idlocal = $idlocal ");
        if ($idresponsavel) array_push($where, " vae.idresponsavel = $idresponsavel ");
        if ($idestoque) array_push($where, " asp.idestoque = $idestoque ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "asp.id, asp.idestoque, vae.estoque, asp.idsolicitante, asp.idunidadefornecedora, vae.`local`, vae.responsavel, asp.datacriacao, asp.logdata ";

        $ordem = "ORDER BY asp.id";
        if ($order) $ordem = $order;

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(asp.id) as total FROM almoxarifadosolicitacoesprodutos asp LEFT JOIN view_almoxarifado_estoques vae ON (asp.idestoque = vae.idestoque) WHERE asp.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM almoxarifadosolicitacoesprodutos asp LEFT JOIN view_almoxarifado_estoques vae ON (asp.idestoque = vae.idestoque) WHERE asp.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        return $db->fetchAll($strsql);
    }

    public function getSolicitacoesProdutosById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getSolicitacoesProdutos($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadosolicitacoesprodutos
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) {
            $row = $this->createRow();
        }

        $row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;

        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');
        }

        $row->save();

        return $row;
    }
}