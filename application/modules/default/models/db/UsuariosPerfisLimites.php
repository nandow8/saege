<?php
class UsuariosPerfisLimites extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "usuariosperfislimites";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return UsuariosPerfisLimites
     */
	public function save($dados) {
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("idperfil=" . $dados['idperfil'] . " AND idlimite=" . $dados['idlimite']);
		
		if (!$row) $row = $this->createRow();
		else {
		} 				
		if ($id>0)$row->id = $id;
		
		$row->idperfil = $dados['idperfil'];
		$row->idlimite = $dados['idlimite'];
		$row->visualizar = $dados['visualizar'];
		$row->adicionar = $dados['adicionar'];
		$row->editar = $dados['editar'];
		$row->excluir = $dados['excluir'];
		$row->save();
	}		
	
	
	
}