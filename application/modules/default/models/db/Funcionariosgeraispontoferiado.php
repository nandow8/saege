<?php

/**
 * Define o modelo Rhlicencas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosgeraispontoferiado extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcinariosgeraispontoferiado";
	
    /**
     * Define a chave primaria
     * @var integer
    */
	protected $_primary = "id";
	
	public static function getFuncionariosgeraispontoferiadoHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$Funcionariosgeraispontoferiado = new Funcionariosgeraispontoferiado();
		return $Funcionariosgeraispontoferiado->getRhlicencas($queries, $page, $maxpage);
	}
	
	public function getFuncionariosgeraispontoferiado($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
                
                
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");
                
                $feriado_pontoferiado = (isset($queries["feriado_pontoferiado"])) ? $queries["feriado_pontoferiado"] : false;
		if ($feriado_pontoferiado) array_push($where, " r1.feriado_pontoferiado = $feriado_pontoferiado ");
                
                $data_pontoferiado = (isset($queries["data_pontoferiado"])) ? $queries["data_pontoferiado"] : false;
		if ($data_pontoferiado) array_push($where, " r1.data_pontoferiado = $data_pontoferiado ");
                
                $recorrente_pontoferiado = (isset($queries["recorrente_pontoferiado"])) ? $queries["recorrente_pontoferiado"] : false;
		if ($recorrente_pontoferiado) array_push($where, " r1.recorrente_pontoferiado = $recorrente_pontoferiado ");
                
                $status_pontohorarios = (isset($queries["status_pontohorarios"])) ? $queries["status_pontohorarios"] : false;
		if ($status_pontohorarios) array_push($where, " r1.status_pontohorarios = $status_pontohorarios ");
                
                $logusuario_pontohorarios = (isset($queries["logusuario_pontohorarios"])) ? $queries["logusuario_pontohorarios"] : false;
		if ($logusuario_pontohorarios) array_push($where, " r1.logusuario_pontohorarios = $logusuario_pontohorarios ");
                
                $logdata_pontohorarios = (isset($queries["logdata_pontohorarios"])) ? $queries["logdata_pontohorarios"] : false;
		if ($logdata_pontohorarios) array_push($where, " r1.logdata_pontohorarios = $logdata_pontohorarios ");
                
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*"; 
		
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcinariosgeraispontoferiado r1
					
					WHERE r1.status_pontohorarios='Ativo' 
						$w 
					$ordem	
					$limit";
                
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;
                
                
                $db = Zend_Registry::get('db');	
                //$strsql = "SELECT * FROM funcinariosgeraispontoferiado";
                //return $db->fetchRow($strsql);
                
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);	
                		
                //return $strsql;
	}	
	
	public function getRhlicencaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosgeraispontoferiado($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getRhlicencaByIdHelper($id, $queries = array()) {
		$rows = new Rhlicencas();
		return $rows->getRhlicencaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Rhlicencas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
                $row->motivo = (array_key_exists("motivo",$dados)) ? $dados["motivo"] : $row->motivo;
                $row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
                $row->datafinal = (array_key_exists("datafinal",$dados)) ? $dados["datafinal"] : $row->datafinal;
                if (is_null($row->datacriacao)) {
                                       $row->datacriacao = date("Y-m-d H:i:s");
                               }

                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;

				
		$row->save();
		
		return $row;
	}
        
        
        /*
         *      Autor: Thiago Torres Migliorati
         *      Função: Verifica se a data consta na tabela
         *      Descrição: Criei esta função para facilitar a verificação dos valores de feriados 
         *      Cadastrados na tabela funcionariosgeraispontoferiado
         *      True -> Caso o valor exista
         *      False -> Caso o valor não conste na tabela
         *      $data -> deverá estar no padrão internacinal
         */
	
        public function getVerificaFeriado($data){
            
            
            $status = "Bloqueado";
            // data_pontoferiado = '" . $data . "' "
            $strSql = "SELECT * FROM funcinariosgeraispontoferiado WHERE data_pontoferiado = $data";
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchAll($strSql);
            
            $contador = 0;
            foreach($row as $ince):
                     $contador++;
                endforeach;
            return $contador;
           
        }//end function verifica feriado
        
         /*
         *      Autor: Thiago Torres Migliorati
         *      Função: Retornar as informações do evento gravado pela dada escolhida
         *      Descrição: Criei esta função para facilitar a verificação dos valores de feriados 
         *      Cadastrados na tabela funcionariosgeraispontoferiado
         */
	
        public function getInformacoesFeriadoData($data){
            
            $strSql = "SELECT * FROM funcinariosgeraispontoferiado WHERE data_pontoferiado = '" . $data . "' and status = Ativo";
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchRow($strSql);
           
            return $row;
           
        }//end function verifica feriado
        
}