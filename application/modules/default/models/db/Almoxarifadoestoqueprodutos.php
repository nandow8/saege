<?php

class Almoxarifadoestoqueprodutos extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "almoxarifadoestoqueprodutos";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public function getEstoqueProdutos($queries = array(), $page = 0, $maxpage = 0) {

        $idproduto = (isset($queries['idproduto'])) ? (int)$queries['idproduto'] : false;
        $produto = (isset($queries['produto'])) ? $queries['produto'] : false;
        $codigo = (isset($queries['codigo'])) ? $queries['codigo'] : false;
        $categoria = (isset($queries['categoria'])) ? $queries['categoria'] : false;

        $idestoque = (isset($queries['idestoque'])) ? (int)$queries['idestoque'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $zerados = (isset($queries['zerados'])) ? $queries['zerados'] : false;

        $where = array();

        if ($idproduto) array_push($where, " p.id = $idproduto ");
        if ($produto) array_push($where, " p.produto LIKE '$produto%' ");
        if ($codigo) array_push($where, " p.codigoproduto = '$codigo' ");
        if ($categoria) array_push($where, "p.idcategoria = $categoria ");
        if ($idestoque) array_push($where, " ((ep.idestoque = $idestoque) OR (ep.idestoque IS NULL)) ");
        if (!$zerados) array_push($where, " ep.estoque > 0 ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "p.id, ep.idproduto, IFNULL(ep.estoque,0) estoque, IFNULL(ep.estoquebloqueado,0) estoquebloqueado, IFNULL(ep.estoqueminimo,0) estoqueminimo, IFNULL(ep.estoquemaximo,0) estoquemaximo, p.produto, p.descricoes, p.idimagem, p.codigoproduto, c.categoria, u.descricao unidademedida";

        $ordem = "ORDER BY ep.id";
        if ($order) $ordem = $order;

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(p.id) as total FROM almoxarifadoprodutos p LEFT JOIN almoxarifadoestoqueprodutos ep ON (p.id = ep.idproduto) LEFT JOIN almoxarifadocategoriasprodutos c ON (p.idcategoria = c.id) LEFT JOIN unidadescomerciais u ON (p.idunidademedida = u.id) WHERE p.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM almoxarifadoprodutos p LEFT JOIN almoxarifadoestoqueprodutos ep ON (p.id = ep.idproduto) LEFT JOIN almoxarifadocategoriasprodutos c ON (p.idcategoria = c.id) LEFT JOIN unidadescomerciais u ON (p.idunidademedida = u.id) WHERE p.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        // var_dump($strsql); die();

        return $db->fetchAll($strsql);
    }

    public function getEstoqueProdutosById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getEstoqueProdutos($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }

    public function save($dados) {
        $novoRegistro = true;
        
        // $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        // $row = $this->fetchRow("id=$id AND excluido='nao'");

        $idestoque = $dados['idestoque'];
        $idproduto = $dados['idproduto'];

        $row = $this->fetchRow("idestoque=$idestoque AND idproduto=$idproduto");
        
        if (!$row) {
            $row = $this->createRow();

            $row->idestoque = (array_key_exists("idestoque",$dados)) ? $dados["idestoque"] : $row->idestoque;
            $row->idproduto = (array_key_exists("idproduto",$dados)) ? $dados["idproduto"] : $row->idproduto;
            $row->estoque = (int)$dados["estoque"] + (int)$row->estoque;
            $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
            $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
            $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
            $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;

            if (is_null($row->datacriacao)) {
                $row->datacriacao = date("Y-m-d H:i:s");
            }

        } else {
            $row->estoque = (int)$dados["estoque"] + (int)$row->estoque;
            $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
            $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
        }

        $row->save();
        
        return $row;
    }
}