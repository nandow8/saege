<?php

/**
 * Define o modelo Eventosreservasveiculos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Eventosreservasveiculos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "eventossecretarias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEventosreservasveiculosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$eventossecretarias = new Eventosreservasveiculos();
		return $eventossecretarias->getEventosreservasveiculos($queries, $page, $maxpage);
	}
	
	public function getEventosreservasveiculos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		$idveiculo = (isset($queries["idveiculo"])) ? $queries["idveiculo"] : false;
		if ($idveiculo) array_push($where, " e1.idveiculo = $idveiculo ");

		$idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " e1.idusuario = $idusuario ");

                $datareserva = (isset($queries["datareserva"])) ? $queries["datareserva"] : false;
		if ($datareserva) array_push($where, " e1.datareserva = '$datareserva' ");

                $sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " e1.sequencial = '$sequencial' ");

                $periodo = (isset($queries["periodo"])) ? $queries["periodo"] : false;
		if ($periodo) array_push($where, " e1.periodo = '$periodo' ");

                $solicitarreserva = (isset($queries["solicitarreserva"])) ? $queries["solicitarreserva"] : false;
		if ($solicitarreserva) array_push($where, " e1.solicitarreserva = '$solicitarreserva' ");

                $observacao = (isset($queries["observacao"])) ? $queries["observacao"] : false;
		if ($observacao) array_push($where, " e1.observacao = '$observacao' ");

                $localdestino = (isset($queries["localdestino"])) ? $queries["localdestino"] : false;
		if ($localdestino) array_push($where, " e1.localdestino = '$localdestino' ");

                $status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status = '$status' ");

                $datacriacao = (isset($queries["datacriacao"])) ? $queries["datacriacao"] : false;
		if ($datacriacao) array_push($where, " e1.datacriacao = '$datacriacao' ");

                $excluido = (isset($queries["excluido"])) ? $queries["excluido"] : false;
		if ($excluido) array_push($where, " e1.excluido = '$excluido' ");

                $logusuario = (isset($queries["logusuario"])) ? $queries["logusuario"] : false;
		if ($logusuario) array_push($where, " e1.logusuario = '$logusuario' ");
                
                $logdata = (isset($queries["logdata"])) ? $queries["logdata"] : false;
		if ($logdata) array_push($where, " e1.logdata = '$logdata' ");

                $idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " e1.idfuncionario = '$idfuncionario' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='datareserva') $sorting[0]='l1.datareserva';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*, e2.placa"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM reservasveiculos e1
                                        inner join logisticafrotas e2 on e1.idveiculo = e2.id
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
	 
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	

                
		return $db->fetchAll($strsql);			
	}	
	
	public function getEventoreservasveiculoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEventosreservasveiculos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEventoreservasveiculoByIdHelper($id, $queries = array()) {
		$rows = new Eventosreservasveiculos();
		return $rows->getEventoreservasveiculoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Eventosreservasveiculos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
                $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
                $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;

                $row->especificoescola = (array_key_exists("especificoescola",$dados)) ? $dados["especificoescola"] : $row->especificoescola;
                $row->idusuariointerno = (array_key_exists("idusuariointerno",$dados)) ? $dados["idusuariointerno"] : $row->idusuariointerno;
                $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
                $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
                if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
                $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
                $row->datareserva = (array_key_exists("datareserva",$dados)) ? $dados["datareserva"] : $row->datareserva;
                $row->horainicio = (array_key_exists("horainicio",$dados)) ? $dados["horainicio"] : $row->horainicio;
                $row->datafim = (array_key_exists("datafim",$dados)) ? $dados["datafim"] : $row->datafim;
                $row->horafim = (array_key_exists("horafim",$dados)) ? $dados["horafim"] : $row->horafim;
                $row->hora = (array_key_exists("hora",$dados)) ? $dados["hora"] : $row->hora;
                $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
                $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
                $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
                $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
                $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
                $row->mostrarambos = (array_key_exists("mostrarambos",$dados)) ? $dados["mostrarambos"] : $row->mostrarambos;	
                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 	
				
		$row->save();
		
		return $row;
	}
	
}