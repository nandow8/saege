<?php

/**
 * Define o modelo Escolasalunosnotas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasalunosnotas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasalunosnotas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolasalunosnotasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasalunosnotas = new Escolasalunosnotas();
		return $escolasalunosnotas->getEscolasalunosnotas($queries, $page, $maxpage);
	}

	/*
		Retorna a soma das notas e o id da matéria de cada matéria que algum aluno ou turma cursou.
		@Param $tipo -> tipo de busca (aluno ou turma), para aluno $tipo = 'aluno', para turma $tipo = 'turma'
		@Param $id -> Quando $tipo = 'aluno' $id será o id do aluno, caso contrário será uma array contendo id da escola e id de vínculo da escola (turma) 
	*/
	public static function getSomaNotasMateria( $tipo , $id ){
		
		$db = Zend_Registry::get('db');

		if( $tipo == 'aluno')
			$idaluno = (int)$id;
		else {
			$idescola = (int)$id[0];
			$idescolavinculo = (int)$id[1];
		}

		$strSql = "SELECT SUM(e1.nota) as somaNotas, e1.idmateria FROM escolasalunosnotas e1 ";

		$strSql .= ($tipo == 'aluno') ? "WHERE e1.idaluno = " . $idaluno . "" : "";//"WHERE e1.idescola = " . $idescola . " AND e1.idescolavinculo = " . $idescolavinculo;

		$strSql .= " GROUP BY e1.idmateria";

		return $db->fetchAll( $strSql );
	}

	public static function getQtdNotasMateria( $tipo , $id , $idmateria ) {

		$db = Zend_Registry::get('db');

		if( $tipo == 'aluno')
			$idaluno = (int)$id;
		else {
			$idescola = (int)$id[0];
			$idescolavinculo = (int)$id[1];
		}

		$idmateria = (int)$idmateria;

		$strSql = "SELECT COUNT(e1.nota) as qtdNotas FROM escolasalunosnotas e1 WHERE ";

		$strSql .= ($tipo == 'aluno') 
		? "e1.idaluno = " . $idaluno 
		: "e1.idescola = ". $idescola ." AND e1.idescolavinculo = ". $idescolavinculo;

		$strSql .= " AND e1.idmateria = " . $idmateria . " GROUP BY e1.idmateria";

		return $db->fetchRow( $strSql );
	}

	public static function getQtdMateriasAluno( $idaluno ) {

		$db = Zend_Registry::get('db');

		$idaluno = (int)$idaluno;

		$strSql = "SELECT COUNT(DISTINCT e1.idmateria) as qtdMaterias FROM escolasalunosnotas e1 ";
		$strSql .= "WHERE e1.idaluno = " . $idaluno;

		return $db->fetchRow( $strSql );
	}
	
	public function getEscolasalunosnotas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();

		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$media = (isset($queries["media"])) ? $queries["media"] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");

$idescolavinculo = (isset($queries["idescolavinculo"])) ? $queries["idescolavinculo"] : false;
		if ($idescolavinculo) array_push($where, " e1.idescolavinculo = $idescolavinculo ");

$idmateria = (isset($queries["idmateria"])) ? $queries["idmateria"] : false;
		if ($idmateria) array_push($where, " e1.idmateria = $idmateria ");

$idprofessor = (isset($queries["idprofessor"])) ? $queries["idprofessor"] : false;
		if ($idprofessor) array_push($where, " e1.idprofessor = $idprofessor ");

$idaluno = (isset($queries["idaluno"])) ? $queries["idaluno"] : false;
		if ($idaluno) array_push($where, " e1.idaluno = $idaluno ");

$numeronota = (isset($queries["numeronota"])) ? $queries["numeronota"] : false;
		if ($numeronota)  array_push($where, " e1.numeronota = $numeronota ");

$nota = (isset($queries["nota"])) ? $queries["nota"] : false;
		if ($nota) array_push($where, " e1.nota = $nota ");

$bimestre = (isset($queries["bimestre"])) ? $queries["bimestre"] : false;
		if ($bimestre) array_push($where, " e1.bimestre = '$bimestre' ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " e1.tipo = '$tipo' ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " e1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " e1.data <= '$data_f' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		if ($media)	$fields = "SUM(e1.nota) as total";
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasalunosnotas e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	

		if ($media) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		if (($numeronota) && ($numeronota>0)){
			$row = $db->fetchRow($strsql);
			//var_dump($numeronota);
			//var_dump($strsql);
			return $row;
		}

		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolasalunosnotaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasalunosnotas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolasalunosnotaByIdHelper($id, $queries = array()) {
		$rows = new Escolasalunosnotas();
		return $rows->getEscolasalunosnotaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasalunosnotas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idescolavinculo = (array_key_exists("idescolavinculo",$dados)) ? $dados["idescolavinculo"] : $row->idescolavinculo;
 $row->idmateria = (array_key_exists("idmateria",$dados)) ? $dados["idmateria"] : $row->idmateria;
 $row->idprofessor = (array_key_exists("idprofessor",$dados)) ? $dados["idprofessor"] : $row->idprofessor;
 $row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
 $row->numeronota = (array_key_exists("numeronota",$dados)) ? $dados["numeronota"] : $row->numeronota;
 $row->nota = (array_key_exists("nota",$dados)) ? $dados["nota"] : $row->nota;
 $row->bimestre = (array_key_exists("bimestre",$dados)) ? $dados["bimestre"] : $row->bimestre;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
				
		$row->save();
		
		return $row;
	}

	/* ======================================================================================================
					      Get Escola Alunos Notas Nova versão com médias ponderadas
	   ====================================================================================================== */

	public static function getEscolasalunosnotasponderado($queries = array(), $page = 0, $maxpage = 0){ 
		
		$strMedia = 'select '; 
		$strMedia .= 'ecd.id, ';
		$strMedia .= 'ecdn.idtitulonota, ';
		$strMedia .= 'ecdn.percentualnota ';
		$strMedia .= 'from '; 
		$strMedia .= 'escolasconfiguracoesdiferenciadas as ecd ';
		$strMedia .= 'inner JOIN ';
		$strMedia .= 'escolasconfiguracoesdiferenciadasnotas as ecdn ';
		$strMedia .= 'ON ';
		$strMedia .= 'ecd.id = ecdn.idparent ';
		$strMedia .= 'where '; 
		$strMedia .= 'idescola = ' . $queries['idescola'] .' and idserie = ' . $queries['idserie'] . ' and idmateria = ' . $queries['idmateria'] . ' ';
		$strMedia .= 'order BY ';
		$strMedia .= 'ecdn.idtitulonota;';

		$db = Zend_Registry::get('db');	
		$row = $db->fetchAll($strMedia);

		$somanota = 00.00;

		foreach($row as $i=>$v):


			$strQuery = 'select '; 
			$strQuery .= 'enota.idescola, ';
			$strQuery .= 'enota.idmateria, ';
			$strQuery .= 'ecd.idserie, ';
			$strQuery .= 'ecdn.idtitulonota, ';
			$strQuery .= 'ecdn.percentualnota, ';
			$strQuery .= 'enota.nota, ';
			$strQuery .= 'enota.numeronota, ';
			$strQuery .= 'enota.id, ';
			$strQuery .= 'enota.bimestre ';
			$strQuery .= 'from  ';
			$strQuery .= 'escolasalunosnotas as enota '; 
			$strQuery .= 'inner join  ';
			$strQuery .= 'escolasconfiguracoesdiferenciadas as ecd ';
			$strQuery .= 'ON ';
			$strQuery .= 'enota.idmateria = ecd.idmateria ';
			$strQuery .= 'inner JOIN ';
			$strQuery .= 'escolasconfiguracoesdiferenciadasnotas as ecdn ';
			$strQuery .= 'ON ';
			$strQuery .= 'ecdn.idparent = ecd.id ';
			$strQuery .= 'where  ';
			$strQuery .= 'idescolavinculo = 220 ';
			$strQuery .= 'and bimestre = ' . $queries['bimestre'] . ' ';
			$strQuery .= 'and enota.idmateria = ' . $queries['idmateria'] . ' '; 
			$strQuery .= 'and idaluno = ' . $queries['idaluno'] . ' ';
			$strQuery .= "and idtitulonota = " . $v['idtitulonota'] . " ";
			$strQuery .= 'and numeronota = ' . ($i + 1);

			$db = Zend_Registry::get('db');	
			$row = $db->fetchAll($strQuery);

			if(isset($row[0]['nota'])){
				$somanota += (float)(($row[0]['nota'])*((float)$row[0]['percentualnota']/100));
			}else{
				$somanota += (float)(('00.00')*((float)100/100));
			}
			
			

		endforeach;
		
		return $somanota;	
		
				

	}// end function getEscolasalunosnotasponderado	
	
}