<?php

/**
 * Define o modelo Financeirocontasregistros
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Financeirocontasregistros extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "financeirocontasregistros";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFinanceirocontasregistrosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$financeirocontasregistros = new Financeirocontasregistros();
		return $financeirocontasregistros->getFinanceirocontasregistros($queries, $page, $maxpage);
	}
	
	public function getFinanceirocontasregistros($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$sum = (isset($queries['sum'])) ? (int)$queries['sum'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		
		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " f1.origem LIKE '%$origem%' ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " f1.idescola = $idescola ");

$idtipoconvenio = (isset($queries["idtipoconvenio"])) ? $queries["idtipoconvenio"] : false;
		if ($idtipoconvenio) array_push($where, " f1.idtipoconvenio = $idtipoconvenio ");

$idconvenio = (isset($queries["idconvenio"])) ? $queries["idconvenio"] : false;
		if ($idconvenio) array_push($where, " f1.idconvenio = $idconvenio ");

$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " f1.sequencial LIKE '%$sequencial%' ");

$fornecedor = (isset($queries["fornecedor"])) ? $queries["fornecedor"] : false;
		if ($fornecedor) array_push($where, " f1.fornecedor LIKE '%$fornecedor%' ");

$cpfcnpj = (isset($queries["cpfcnpj"])) ? $queries["cpfcnpj"] : false;
		if ($cpfcnpj) array_push($where, " f1.cpfcnpj LIKE '%$cpfcnpj%' ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " f1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " f1.data <= '$data_f' ");

$lancamento = (isset($queries["lancamento"])) ? $queries["lancamento"] : false;
		if ($lancamento) array_push($where, " f1.lancamento LIKE '%$lancamento%' ");

$idbanco = (isset($queries["idbanco"])) ? $queries["idbanco"] : false;
		if ($idbanco) array_push($where, " f1.idbanco = $idbanco ");

$banco = (isset($queries["banco"])) ? $queries["banco"] : false;
		if ($banco) array_push($where, " f1.banco LIKE '%$banco%' ");

$agencia = (isset($queries["agencia"])) ? $queries["agencia"] : false;
		if ($agencia) array_push($where, " f1.agencia LIKE '%$agencia%' ");

$conta = (isset($queries["conta"])) ? $queries["conta"] : false;
		if ($conta) array_push($where, " f1.conta LIKE '%$conta%' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " f1.titulo LIKE '%$titulo%' ");

//$valor = (isset($queries["valor"])) ? $queries["valor"] : false;
		//if ($valor) array_push($where, " f1.valor = $valor ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " f1.tipo LIKE '%$tipo%' ");

$natureza = (isset($queries["natureza"])) ? $queries["natureza"] : false;
		if ($natureza) array_push($where, " f1.natureza LIKE '%$natureza%' ");


$tiporeceita = (isset($queries["tiporeceita"])) ? $queries["tiporeceita"] : false;
		if ($tiporeceita) array_push($where, " f1.tiporeceita = $tiporeceita ");

$tipodespesa = (isset($queries["tipodespesa"])) ? $queries["tipodespesa"] : false;
		if ($tipodespesa) array_push($where, " f1.tipodespesa = $tipodespesa ");

$tipodocumento = (isset($queries["tipodocumento"])) ? $queries["tipodocumento"] : false;
		if ($tipodocumento) array_push($where, " f1.tipodocumento LIKE '%$tipodocumento%' ");

$faturado = (isset($queries["faturado"])) ? $queries["faturado"] : false;
		if ($faturado) array_push($where, " f1.faturado LIKE '%$faturado%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		if ($sum) $fields = "SUM(f1.valor) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM financeirocontasregistros f1
					
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";
		
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total || $sum) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFinanceirocontaregistroById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFinanceirocontasregistros($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFinanceirocontaregistroByIdHelper($id, $queries = array()) {
		$rows = new Financeirocontasregistros();
		return $rows->getFinanceirocontaregistroById($id, $queries);
	}		
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Financeirocontasregistros
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
 $row->idtipoconvenio = (array_key_exists("idtipoconvenio",$dados)) ? $dados["idtipoconvenio"] : $row->idtipoconvenio;
 $row->idconvenio = (array_key_exists("idconvenio",$dados)) ? $dados["idconvenio"] : $row->idconvenio;
 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->fornecedor = (array_key_exists("fornecedor",$dados)) ? $dados["fornecedor"] : $row->fornecedor;
 $row->cpfcnpj = (array_key_exists("cpfcnpj",$dados)) ? $dados["cpfcnpj"] : $row->cpfcnpj;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->idbanco = (array_key_exists("idbanco",$dados)) ? $dados["idbanco"] : $row->idbanco;
 $row->banco = (array_key_exists("banco",$dados)) ? $dados["banco"] : $row->banco;
 $row->agencia = (array_key_exists("agencia",$dados)) ? $dados["agencia"] : $row->agencia;
 $row->conta = (array_key_exists("conta",$dados)) ? $dados["conta"] : $row->conta;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->valor = (array_key_exists("valor",$dados)) ? $dados["valor"] : $row->valor;
 $row->datapagamento = (array_key_exists("datapagamento",$dados)) ? $dados["datapagamento"] : $row->datapagamento;
 $row->natureza = (array_key_exists("natureza",$dados)) ? $dados["natureza"] : $row->natureza;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->tiporeceita = (array_key_exists("tiporeceita",$dados)) ? $dados["tiporeceita"] : $row->tiporeceita;
 $row->tipodespesa = (array_key_exists("tipodespesa",$dados)) ? $dados["tipodespesa"] : $row->tipodespesa;
 $row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
 $row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
 $row->tipodocumento = (array_key_exists("tipodocumento",$dados)) ? $dados["tipodocumento"] : $row->tipodocumento;
 $row->ndocumento = (array_key_exists("ndocumento",$dados)) ? $dados["ndocumento"] : $row->ndocumento;
 $row->iddocumento = (array_key_exists("iddocumento",$dados)) ? $dados["iddocumento"] : $row->iddocumento;
 $row->idcheque = (array_key_exists("idcheque",$dados)) ? $dados["idcheque"] : $row->idcheque;
 $row->faturado = (array_key_exists("faturado",$dados)) ? $dados["faturado"] : $row->faturado;
 $row->numerocheque = (array_key_exists("numerocheque",$dados)) ? $dados["numerocheque"] : $row->numerocheque;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		//return true;		
		$row->save();
		
		return $row;
	}
	
}