<?php

/**
 * Define o modelo Patrimoniostrasnferencias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Patrimoniostrasnferencias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "patrimoniostrasnferencias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getPatrimoniostrasnferenciasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$patrimoniostrasnferencias = new Patrimoniostrasnferencias();
		return $patrimoniostrasnferencias->getPatrimoniostrasnferencias($queries, $page, $maxpage);
	}
	
	public function getPatrimoniostrasnferencias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " p1.sequencial LIKE '%$sequencial%' ");

		$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
				if ($datalancamento_i) array_push($where, " p1.datalancamento >= '$datalancamento_i' ");

		$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
				if ($datalancamento_f) array_push($where, " p1.datalancamento <= '$datalancamento_f' ");

		$horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
				if ($horalancamento) array_push($where, " p1.horalancamento = '$horalancamento' ");

		$idusuariologado = (isset($queries["idusuariologado"])) ? $queries["idusuariologado"] : false;
				if ($idusuariologado) array_push($where, " p1.idusuariologado = $idusuariologado ");

		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
				if ($origem) array_push($where, " p1.origem LIKE '%$origem%' ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
				if ($idescola) array_push($where, " p1.idescola = $idescola ");

		$iddepartamentoorigem = (isset($queries["iddepartamentoorigem"])) ? $queries["iddepartamentoorigem"] : false;
				if ($iddepartamentoorigem) array_push($where, " p1.iddepartamentoorigem = $iddepartamentoorigem ");

		$idpatrimonio = (isset($queries["idpatrimonio"])) ? $queries["idpatrimonio"] : false;
				if ($idpatrimonio) array_push($where, " p1.idpatrimonio = $idpatrimonio ");

		$transferencia = (isset($queries["transferencia"])) ? $queries["transferencia"] : false;
				if ($transferencia) array_push($where, " p1.transferencia LIKE '%$transferencia%' ");

		$origemtransferencia = (isset($queries["origemtransferencia"])) ? $queries["origemtransferencia"] : false;
				if ($origemtransferencia) array_push($where, " p1.origemtransferencia LIKE '%$origemtransferencia%' ");

		$idescolatransferencia = (isset($queries["idescolatransferencia"])) ? $queries["idescolatransferencia"] : false;
				if ($idescolatransferencia) array_push($where, " p1.idescolatransferencia = $idescolatransferencia ");

		$iddepartamentoorigemtransferencia = (isset($queries["iddepartamentoorigemtransferencia"])) ? $queries["iddepartamentoorigemtransferencia"] : false;
				if ($iddepartamentoorigemtransferencia) array_push($where, " p1.iddepartamentoorigemtransferencia = $iddepartamentoorigemtransferencia ");

		$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
				if ($observacoes) array_push($where, " p1.observacoes = '$observacoes' ");

		$baixatransferencia = (isset($queries["baixatransferencia"])) ? $queries["baixatransferencia"] : false;
				if ($baixatransferencia) array_push($where, " p1.baixatransferencia LIKE '%$baixatransferencia%' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status = '$status' ");


		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM patrimoniostrasnferencias p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPatrimoniostrasnferenciaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPatrimoniostrasnferencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPatrimoniostrasnferenciaByIdHelper($id, $queries = array()) {
		$rows = new Patrimoniostrasnferencias();
		return $rows->getPatrimoniostrasnferenciaById($id, $queries);
	}		
	
	public function getUltimoPatrimoniotrasnferencias($queries = array()) {
		$queries['order'] = 'ORDER BY p1.id DESC';
		$rows = $this->getPatrimoniostrasnferencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	/**
	* Gerar data atual (dia/mês/ano) com formato português
	* @return string data
	*/
	public function getCurrentData(){
		$data = date('D');
	    $mes = date('M');
	    $dia = date('d');
	    $ano = date('Y');
    
	    $semana = array(
	        'Sun' => 'Domingo', 
	        'Mon' => 'Segunda-Feira',
	        'Tue' => 'Terça-Feira',
	        'Wed' => 'Quarta-Feira',
	        'Thu' => 'Quinta-Feira',
	        'Fri' => 'Sexta-Feira',
	        'Sat' => 'Sábado'
	    );
    
	    $mes_extenso = array(
	        'Jan' => 'Janeiro',
	        'Feb' => 'Fevereiro',
	        'Mar' => 'Marco',
	        'Apr' => 'Abril',
	        'May' => 'Maio',
	        'Jun' => 'Junho',
	        'Jul' => 'Julho',
	        'Aug' => 'Agosto',
	        'Nov' => 'Novembro',
	        'Sep' => 'Setembro',
	        'Oct' => 'Outubro',
	        'Dec' => 'Dezembro'
	    );
	    
        $data = "{$dia} de " . $mes_extenso["$mes"] . " de {$ano}";
        return $data;
	}
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Patrimoniostrasnferencias
     */

	public function save($dados) {			
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		}
		
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
		 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
		 $row->horalancamento = (array_key_exists("horalancamento",$dados)) ? $dados["horalancamento"] : $row->horalancamento;
		 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
		 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		 $row->iddepartamentoorigem = (array_key_exists("iddepartamentoorigem",$dados)) ? $dados["iddepartamentoorigem"] : $row->iddepartamentoorigem;
		 $row->idpatrimonio = (array_key_exists("idpatrimonio",$dados)) ? $dados["idpatrimonio"] : $row->idpatrimonio;
		 $row->transferencia = (array_key_exists("transferencia",$dados)) ? $dados["transferencia"] : $row->transferencia;
		 $row->origemtransferencia = (array_key_exists("origemtransferencia",$dados)) ? $dados["origemtransferencia"] : $row->origemtransferencia;
		 $row->idescolatransferencia = (array_key_exists("idescolatransferencia",$dados)) ? $dados["idescolatransferencia"] : $row->idescolatransferencia;
		 
		 $row->iddepartamentodestino = (array_key_exists("iddepartamentodestino", $dados)) ? $dados['iddepartamentodestino'] : $row->iddepartamentodestino;

		 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
		 $row->baixatransferencia = (array_key_exists("baixatransferencia",$dados)) ? $dados["baixatransferencia"] : $row->baixatransferencia;
		 $row->iddocumento = (array_key_exists("iddocumento", $dados)) ? $dados["iddocumento"] : $row->iddocumento;
		 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		 $row->motivonegacao = (array_key_exists("motivonegacao",$dados)) ? $dados["motivonegacao"] : $row->motivonegacao;
		 

		 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
								
		 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		
		$row->save();
		
		return $row;
	}
	
}