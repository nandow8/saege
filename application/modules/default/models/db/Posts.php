<?php

/**
 * Define o modelo Posts
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Action
 * @copyright   Copyright (c) 2014 MN Solu��es (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Posts extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "posts";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";	
	
	public static $_TIPO_BANNER_HOME_770x400 = 'bannerhome770x400';
	
	public static function getTiposBanners($field = false) {
		$res = array(
				self::$_TIPO_BANNER_HOME_770x400 => 'Home 770px X 400px',
		);
	
		if ($field) return $res[$field];
	
		return $res;
	}	
	
	public static function getCursosAtivos() {
		$strsql = "select * from posts where excluido='nao' and status='Ativo' and idparent=15 order by ordem desc";
		$db = Zend_Registry::get('db');
		
		return $db->fetchAll($strsql);		
	}
	
	public static function getBanner($tipo) {
		$strsql = "select * from posts where tipocustom='$tipo' and excluido='nao' and status='Ativo' order by rand()";	
		$db = Zend_Registry::get('db');
		
		return $db->fetchRow($strsql);
	}
	
	public static function getBanners($tipo) {
		$strsql = "select * from posts where tipocustom='$tipo' and excluido='nao' and status='Ativo' order by rand()";	
		$db = Zend_Registry::get('db');
		
		return $db->fetchAll($strsql);
	}
	
	
	public static function setTipos($field = false) {
		$res = array(
			self::$_TIPO_APLICACAO => 'Área de Aplicação',
			self::$_TIPO_ADITIVOS => 'Aditivos',
			self::$_TIPO_PASTAS => 'Pastas'
		);
		
		if (!$field) return $res;
		return (isset($res[$field])) ? $res[$field] : false;
	}	
	
	public function swapOrdem($idparent, $ordem, $d, $inverte) {
		if ($ordem > 0) {
			if ($inverte) $d = ($d=="up") ? "down" : "up"; //Inverte os Sentidos
			if ($d=="up") {
				$strsql = "SELECT * FROM posts WHERE excluido = 'nao' AND idparent = $idparent AND ordem <= ".$ordem." ORDER BY ordem DESC LIMIT 2";
			} else {
				$strsql = "SELECT * FROM posts WHERE excluido = 'nao' AND idparent = $idparent AND ordem >= ".$ordem." ORDER BY ordem LIMIT 2";
			}
		
			$db = Zend_Registry::get("db");
			$rows = $db->fetchAll($strsql);
			if (sizeof($rows)==2) {
				$db->beginTransaction();
				try {
					$strsql = "UPDATE posts SET ordem = " . $rows[1]["ordem"] . 
						" WHERE id =" . $rows[0]["id"];
					$db->query($strsql);
				
					$strsql = "UPDATE posts SET ordem = " . $rows[0]["ordem"] . 
						" WHERE id =" . $rows[1]["id"];
					$db->query($strsql);
					$db->commit();
				} catch (Exception $e) {
					$db->rollBack();
					echo $e->getMessage();
				}
			}
		}			
	}

	public static function getMutirao($dia, $mes, $ano, $idestado, $idcidade) {
		$data = mktime(0, 0, 0, $mes, $dia, $ano);
		$data = date('Y-m-d', $data);
		
		$_westado = ($idestado==0) ? "" : " and c1.idestado=$idestado";
		$_wcidade = ($idcidade==0) ? "" : " and p1.idcidade=$idcidade";
		
		$strsql = "select p1.* 
					from posts p1 left join cidades c1 on c1.id=p1.idcidade 
					where p1.excluido='nao' 
						and p1.status='Ativo' 
						and p1.idparent=13 
						and date(p1.data)=date('$data') 
						$_westado
						$_wcidade
					order by p1.data";
		$db = Zend_Registry::get('db');
		
		return $db->fetchRow($strsql);
	}
	
	public static function getOutrosMutiroes($max = "") {
		if((int)$max > 0) $max = "limit 0,$max";
		$strsql = "select p1.*, c1.nome as cidade, c1.uf 
					from posts p1 left join cidades c1 on c1.id=p1.idcidade 
					where p1.idparent=13 
						and p1.excluido='nao' 
						and p1.status='Ativo'
						and p1.latitude <> ''
						and p1.longitude <> '' 
						and DATE(p1.data)>=DATE(NOW()) 
					order by data $max";
		
		$db = Zend_Registry::get('db');
		return $db->fetchAll($strsql);		
	}
	
	public static function getAlbuns() {
		$strsql = "select p1.*, (select count(i1.id) from postsimagens i1 where i1.idpost=p1.id and excluido='nao') as total from posts p1 where p1.excluido='nao' and p1.status='Ativo' and p1.idparent=18 order by p1.data desc";
		
		$db = Zend_Registry::get('db');
		return $db->fetchAll($strsql);	
	}
	
	
	
	public static function getPostsByIdParent($idparent = NULL, $status = NULL, $inverte = false, $limite = NULL) {
		$rows = new Posts();
		$resultado = $rows->getPosts($idparent, $status, $inverte, $limite);
		
		return $resultado;
	}
	
	public static function getPostsHelpers($idparent = NULL, $status = NULL, $inverte = false, $limite = NULL) {
		$rows = new Posts();
		return $rows->getPosts($idparent, $status, $inverte, $limite);
	}
	
	/**
	 * 
	 * Retorna os posts de um idparent
	 * @param int $idparent
	 * @param string $status
	 * @param boolean $inverte_ordem
	 */
	public function getPosts($idparent = NULL, $status = NULL, $inverte = false, $limite = NULL) {
		$db = Zend_Registry::get('db');
		
		if (!is_array($idparent)) {
			$w = "";
			if (!is_null($idparent)) $w .= " AND p1.idparent = " .(int)$idparent;
			if (!is_null($status)) $w .= " AND p1.status = '$status'"; 
						
			$l = "";
			if (!is_null($limite)) $l .= " LIMIT $limite";
			
			$order = "ORDER BY p1.ordem";
			if ($inverte) $order .= " DESC";
			
			$strsql = "SELECT p1.*
						FROM posts p1 
						WHERE p1.excluido = 'nao' 
						$w
						$order
						$l";
			
			return $db->fetchAll($strsql);
		}
		
		$queries = $idparent;
		
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idparent = (isset($queries['idparent'])) ? (int)$queries['idparent'] : false;
		$idparentparent = (isset($queries['idparentparent'])) ? (int)$queries['idparentparent'] : false;
		$inverte = (isset($queries['inverte'])) ? (int)$queries['inverte'] : false;
		$idtipo = (isset($queries['idtipo'])) ? (int)$queries['idtipo'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$destaque = (isset($queries['destaque'])) ? $queries['destaque'] : false;
		$sugerido = (isset($queries['sugerido'])) ? $queries['sugerido'] : false;
		$sugeridopor = (isset($queries['sugeridopor'])) ? $queries['sugeridopor'] : false;
		$aprovados = (isset($queries['aprovados'])) ? $queries['aprovados'] : false;
		$usuario_todos = (isset($queries['usuario_todos'])) ? $queries['usuario_todos'] : false;
		$idusuario = (isset($queries['idusuario'])) ? $queries['idusuario'] : false;
		
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$page = (isset($queries['paginaAtual'])) ? (int)$queries['paginaAtual'] : 0;
		$maxpage = (isset($queries['maxpp'])) ? (int)$queries['maxpp'] : 0; 
		
		$where = array();
		if ($id) array_push($where, " p1.id=$id ");
		if ($idparent) array_push($where, " p1.idparent=$idparent ");
		if ($idparentparent) array_push($where, " p2.idparent=$idparentparent ");
		if ($idtipo) array_push($where, " p1.idtipo=$idtipo ");
		if ($status) array_push($where, " p1.status='$status' ");
		if ($chave) array_push($where, " p1.topico='%$chave%' ");
		if ($destaque) array_push($where, " p1.destaque='$destaque' ");
		if ($sugerido) array_push($where, " p1.sugerido='$sugerido' ");
		if ($sugeridopor) array_push($where, " p1.sugeridopor='$sugeridopor' ");
		if ($aprovados) array_push($where, " (IFNULL(p1.sugeridopor,0)=0 OR (IFNULL(p1.sugeridopor,0)>0 AND p1.status='Ativo') ) ");
		if ($usuario_todos) array_push($where, " IFNULL(p1.idusuario,0)=0 ");
		if ($idusuario) array_push($where, " (IFNULL(p1.idusuario,0)=0 OR IFNULL(p1.idusuario,0)=$idusuario) ");

		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*, p2.idparent as idparentparent";
		if ($total) $fields = "COUNT(p1.id) as total";
		
		
		$ordem = "ORDER BY p1.topico ASC";
		if ($inverte) {
			$ordem = "ORDER BY p1.ordem DESC";
		}
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields
					FROM posts p1
						LEFT JOIN posts p2 ON p2.id=p1.idparent 
					WHERE p1.excluido='nao' 
						$w	
					$ordem 
					$limit";
		//die($strsql);			
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	

		return $db->fetchAll($strsql);			
	}

	public static function getPostHelpers($id = NULL, $idparent = NULL, $status = NULL) {
		$rows = new Posts();
		return $rows->getPost($id, $idparent, $status);
	}
	
	public function getPost($id = NULL, $idparent = NULL, $status = NULL) {
		$db = Zend_Registry::get('db');
		
		$w = "";
		if (!is_null($id)) $w .= " AND p1.id = " . (int)$id;
		if (!is_null($idparent)) $w .= " AND p1.idparent = " . (int)$idparent;
		if (!is_null($status)) $w .= " AND p1.status = '$status'"; 
			
		$strsql = "SELECT p1.* 
					FROM posts p1 
					WHERE p1.excluido = 'nao'						 
					$w";
		//die($strsql);			
		return $db->fetchRow($strsql);
	}

	public function getPostsCategoria($idparent = NULL, $status = NULL, $inverte = false) {
		$db = Zend_Registry::get('db');

		$order = "ORDER BY p1.ordem";
		if ($inverte) $order .= " DESC";
		
		$strsql = "SELECT p1 . * , p2.id as p2id, p2.idtipo as p2idtipo
					FROM posts p1
						INNER JOIN posts p2 ON p1.idparent = p2.id
							AND p2.idparent = $idparent
					WHERE p1.excluido = 'nao'
					
					$order";
		
		return $db->fetchAll($strsql);
	}
	
	public static function getPostsTipo($idparent = NULL, $status = NULL, $inverte = false, $idtipo = false) {
		$db = Zend_Registry::get('db');

		$order = "ORDER BY p1.ordem";
		if ($inverte) $order .= " DESC";
		
		$strsql = "SELECT p1 . * , p2.id as p2id, p2.idtipo as p2idtipo
					FROM posts p1
						INNER JOIN posts p2 ON p1.idparent = p2.id							
					WHERE p1.excluido = 'nao'
						AND p1.idparent = $idparent
						AND p2.idtipo = $idtipo
					
					$order";
		//die($strsql);
		return $db->fetchAll($strsql);
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		
		$row->idparent = (isset($dados['idparent'])) ? $dados['idparent'] : $row->idparent;
		$row->data = (isset($dados['data'])) ? $dados['data'] : $row->data;
		$row->topico = (isset($dados['topico'])) ? $dados['topico'] : $row->topico;
		$row->subtopico = (isset($dados['subtopico'])) ? $dados['subtopico'] : $row->subtopico;
		$row->texto = (isset($dados['texto'])) ? $dados['texto'] : $row->texto;
		$row->texto2 = (isset($dados['texto2'])) ? $dados['texto2'] : $row->texto2;
		$row->tipocustom = (isset($dados['tipocustom'])) ? $dados['tipocustom'] : $row->tipocustom;
		
		if (($novoRegistro) || (!$dados['idimagem']===false)) {
			$row->idimagem = (isset($dados['idimagem'])) ? $dados['idimagem'] : $row->idimagem;
		}

		if (($novoRegistro) || (!$dados['idbanner']===false)) {
			$row->idbanner = (isset($dados['idbanner'])) ? $dados['idbanner'] : $row->idbanner;
		}
		
		if (($novoRegistro) || (!$dados['idarquivo']===false)) {
			$row->idarquivo = (isset($dados['idarquivo'])) ? $dados['idarquivo'] : $row->idarquivo;
		}			
		
		$row->video = (isset($dados['video'])) ? $dados['video'] : $row->video;
		$row->link = (isset($dados['link'])) ? $dados['link'] : $row->link;
		$row->destaque = (isset($dados['destaque'])) ? $dados['destaque'] : $row->destaque;
		
		$row->sugerido = (isset($dados['sugerido'])) ? $dados['sugerido'] : $row->sugerido;
		$row->sugeridopor = (isset($dados['sugeridopor'])) ? $dados['sugeridopor'] : $row->sugeridopor;
		
		
		$row->status = (isset($dados['status'])) ? $dados['status'] : $row->status;
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;
		$row->visualizacao = (isset($dados['visualizacao'])) ? $dados['visualizacao'] : $row->visualizacao;
		$row->ordem = (isset($dados['ordem'])) ? $dados['ordem'] : $row->ordem;
		$row->logusuario = $dados['logusuario'];
		$row->logdata = $dados['logdata'];
		
		$row->idcurso = (isset($dados['idcurso'])) ? $dados['idcurso'] : $row->idcurso;
		$row->idusuario = (isset($dados['idusuario'])) ? $dados['idusuario'] : $row->idusuario;
		
		
		$row->save();
		
		if ($novoRegistro) {
			$row->ordem = $row->id;
			$row->save();			
		}

		return $row;
	}	
}