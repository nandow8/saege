<?php

/**
 * Define o modelo Almoxarifadoprodutossaidas
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 FJMX.
 * @version     1.0
 */
class Almoxarifadoprodutossaidas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoprodutossaidas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getAlmoxarifadoprodutossaidasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$almoxarifadoprodutossaidas = new Almoxarifadoprodutossaidas();
		return $almoxarifadoprodutossaidas->getAlmoxarifadoprodutossaidas($queries, $page, $maxpage);
	}
  
	public function getAlmoxarifadoprodutossaidas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " vap.id = $id ");  
		
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " aps.idescola = $idescola ");  
		
		$idperfil = (isset($queries['idperfil'])) ? (int)$queries['idperfil'] : false;
		if ($idperfil) array_push($where, " aps.idperfil = $idperfil ");  
		
		$idperfilbusca = (isset($queries['idperfilbusca'])) ? (int)$queries['idperfilbusca'] : false;
		if ($idperfilbusca) array_push($where, " aps.idperfilbusca = $idperfilbusca ");  
		
		$idcategoria = (isset($queries['idcategoria'])) ? (int)$queries['idcategoria'] : false;
		if ($idcategoria) array_push($where, " vap.idcategoria = $idcategoria ");  

		$produto = (isset($queries["produto"])) ? $queries["produto"] : false;
		if ($produto) array_push($where, " vap.produto LIKE '%$produto%' ");
		
		$escola = (isset($queries["escola"])) ? $queries["escola"] : false;
		if ($escola) array_push($where, " e.escola LIKE '%$escola%' ");
	
		$old_unidademedida = (isset($queries["old_unidademedida"])) ? $queries["old_unidademedida"] : false;
		if ($old_unidademedida) array_push($where, " vap.old_unidademedida LIKE '%$old_unidademedida%' ");

		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " vap.descricoes LIKE '%$descricoes%' ");
		
		$quantidade = (isset($queries["quantidade"])) ? $queries["quantidade"] : false;
		if ($quantidade) array_push($where, " aps.quantidade = $quantidade ");

		$codigoproduto = (isset($queries["codigoproduto"])) ? $queries["codigoproduto"] : false;
		if ($codigoproduto) array_push($where, " vap.codigoproduto LIKE '%$codigoproduto%' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " vap.status LIKE '%$status%' ");

		$idcategoria = (isset($queries["idcategoria"])) ? $queries["idcategoria"] : false;
		if ($idcategoria) array_push($where, " vap.idcategoria = '$idcategoria' ");

		$idunidademedida = (isset($queries["idunidademedida"])) ? $queries["idunidademedida"] : false;
		if ($idunidademedida) array_push($where, " vap.idunidademedida = '$idunidademedida' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "aps.*, vap.* "; 
		;
		
		if ($total) $fields = "COUNT(vap.id) as total";
		
		$ordem = "ORDER BY aps.idescola";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
		
		
		$strsql = "SELECT $fields
				   FROM almoxarifadoprodutossaidas aps 
				   JOIN view_almoxarifado_produtos vap 
				   ON aps.idcodigoalmoxarifadoproduto = vap.codigoproduto
				   WHERE aps.excluido='nao'  
				   $w  AND aps.quantidade > 0
				   $ordem 
				   $limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAlmoxarifadoprodutoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAlmoxarifadoprodutossaidas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAlmoxarifadoprodutoByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifadoprodutossaidas();
		return $rows->getAlmoxarifadoprodutoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadoprodutossaidas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		 
		$row->idcodigoalmoxarifadoproduto = (array_key_exists("idcodigoalmoxarifadoproduto",$dados)) ? $dados["idcodigoalmoxarifadoproduto"] : $row->idcodigoalmoxarifadoproduto;
		$row->idperfil = (array_key_exists("idperfil",$dados)) ? $dados["idperfil"] : $row->idperfil;
		$row->idperfilbusca = (array_key_exists("idperfilbusca",$dados)) ? $dados["idperfilbusca"] : $row->idperfilbusca;
		$row->quantidade = (array_key_exists("quantidade",$dados)) ? $dados["quantidade"] : $row->quantidade;
		$row->ultimaquantidadeinserida = (array_key_exists("ultimaquantidadeinserida",$dados)) ? $dados["ultimaquantidadeinserida"] : $row->ultimaquantidadeinserida;
		$row->ultimaquantidaderetirada = (array_key_exists("ultimaquantidaderetirada",$dados)) ? $dados["ultimaquantidaderetirada"] : $row->ultimaquantidaderetirada;
		$row->codigosolicitacao = (array_key_exists("codigosolicitacao",$dados)) ? $dados["codigosolicitacao"] : $row->codigosolicitacao;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
        }
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;

		$row->save();
		
		return $row;
	}
	
}