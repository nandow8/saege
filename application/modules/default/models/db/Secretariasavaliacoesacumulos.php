<?php

/**
 * Define o modelo Secretariasavaliacoesacumulos
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Secretariasavaliacoesacumulos extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "secretariasavaliacoesacumulos";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getSecretariasavaliacoesacumulosHelper($queries = array(), $page = 0, $maxpage = 0) {
        $secretariasavaliacoesacumulos = new Secretariasavaliacoesacumulos();
        return $secretariasavaliacoesacumulos->getSecretariasavaliacoesacumulos($queries, $page, $maxpage);
    }

    public function getSecretariasavaliacoesacumulos($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " s1.id = $id ");

        $idfuncionario = (isset($queries['idfuncionario'])) ? (int) $queries['idfuncionario'] : false;
        if ($idfuncionario)
            array_push($where, " s1.idfuncionario = $idfuncionario ");

        $idescola = (isset($queries['idescola'])) ? (int) $queries['idescola'] : false;
        if ($idescola)
            array_push($where, " s1.idescola = $idescola ");

        $sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
        if ($sequencial)
            array_push($where, " s1.sequencial LIKE '%$sequencial%' ");

        $datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
        if ($datalancamento_i)
            array_push($where, " s1.datalancamento >= '$datalancamento_i' ");

        $datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
        if ($datalancamento_f)
            array_push($where, " s1.datalancamento <= '$datalancamento_f' ");

        $horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
        if ($horalancamento)
            array_push($where, " s1.horalancamento = '$horalancamento' ");

        $idusuariologado = (isset($queries["idusuariologado"])) ? $queries["idusuariologado"] : false;
        if ($idusuariologado)
            array_push($where, " s1.idusuariologado = $idusuariologado ");

        $nome = (isset($queries["nome"])) ? $queries["nome"] : false;
        if ($nome)
            array_push($where, " s1.nome LIKE '%$nome%' ");

        $cargo = (isset($queries["cargo"])) ? $queries["cargo"] : false;
        if ($cargo)
            array_push($where, " s1.cargo LIKE '%$cargo%' ");

        $rf = (isset($queries["rf"])) ? $queries["rf"] : false;
        if ($rf)
            array_push($where, " s1.rf LIKE '%$rf%' ");

        $rg = (isset($queries["rg"])) ? $queries["rg"] : false;
        if ($rg)
            array_push($where, " s1.rg LIKE '%$rg%' ");

        $rgf = (isset($queries["rgf"])) ? $queries["rgf"] : false;
        if ($rgf)
            array_push($where, " f1.rgf LIKE '%$rgf%' ");

        $unidadetrabalho = (isset($queries["unidadetrabalho"])) ? $queries["unidadetrabalho"] : false;
        if ($unidadetrabalho)
            array_push($where, " s1.unidadetrabalho LIKE '%$unidadetrabalho%' ");

        $telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
        if ($telefone)
            array_push($where, " s1.telefone LIKE '%$telefone%' ");

        $idcargasemanal = (isset($queries["idcargasemanal"])) ? $queries["idcargasemanal"] : false;
        if ($idcargasemanal)
            array_push($where, " s1.idcargasemanal = $idcargasemanal ");

        $horariotrabalho = (isset($queries["horariotrabalho"])) ? $queries["horariotrabalho"] : false;
        if ($horariotrabalho)
            array_push($where, " s1.horariotrabalho LIKE '%$horariotrabalho%' ");

        $segundahorainicio1 = (isset($queries["segundahorainicio1"])) ? $queries["segundahorainicio1"] : false;
        if ($segundahorainicio1)
            array_push($where, " s1.segundahorainicio1 LIKE '%$segundahorainicio1%' ");

        $segundahorafim1 = (isset($queries["segundahorafim1"])) ? $queries["segundahorafim1"] : false;
        if ($segundahorafim1)
            array_push($where, " s1.segundahorafim1 LIKE '%$segundahorafim1%' ");

        $segundahorainicio2 = (isset($queries["segundahorainicio2"])) ? $queries["segundahorainicio2"] : false;
        if ($segundahorainicio2)
            array_push($where, " s1.segundahorainicio2 LIKE '%$segundahorainicio2%' ");

        $segundahorafim2 = (isset($queries["segundahorafim2"])) ? $queries["segundahorafim2"] : false;
        if ($segundahorafim2)
            array_push($where, " s1.segundahorafim2 LIKE '%$segundahorafim2%' ");

        $tercahorainicio1 = (isset($queries["tercahorainicio1"])) ? $queries["tercahorainicio1"] : false;
        if ($tercahorainicio1)
            array_push($where, " s1.tercahorainicio1 LIKE '%$tercahorainicio1%' ");

        $tercahorafim1 = (isset($queries["tercahorafim1"])) ? $queries["tercahorafim1"] : false;
        if ($tercahorafim1)
            array_push($where, " s1.tercahorafim1 LIKE '%$tercahorafim1%' ");

        $tercahorainicio2 = (isset($queries["tercahorainicio2"])) ? $queries["tercahorainicio2"] : false;
        if ($tercahorainicio2)
            array_push($where, " s1.tercahorainicio2 LIKE '%$tercahorainicio2%' ");

        $tercahorafim2 = (isset($queries["tercahorafim2"])) ? $queries["tercahorafim2"] : false;
        if ($tercahorafim2)
            array_push($where, " s1.tercahorafim2 LIKE '%$tercahorafim2%' ");

        $quartahorainicio1 = (isset($queries["quartahorainicio1"])) ? $queries["quartahorainicio1"] : false;
        if ($quartahorainicio1)
            array_push($where, " s1.quartahorainicio1 LIKE '%$quartahorainicio1%' ");

        $quartahorafim1 = (isset($queries["quartahorafim1"])) ? $queries["quartahorafim1"] : false;
        if ($quartahorafim1)
            array_push($where, " s1.quartahorafim1 LIKE '%$quartahorafim1%' ");

        $quartahorainicio2 = (isset($queries["quartahorainicio2"])) ? $queries["quartahorainicio2"] : false;
        if ($quartahorainicio2)
            array_push($where, " s1.quartahorainicio2 LIKE '%$quartahorainicio2%' ");

        $quartahorafim2 = (isset($queries["quartahorafim2"])) ? $queries["quartahorafim2"] : false;
        if ($quartahorafim2)
            array_push($where, " s1.quartahorafim2 LIKE '%$quartahorafim2%' ");

        $quintahorainicio1 = (isset($queries["quintahorainicio1"])) ? $queries["quintahorainicio1"] : false;
        if ($quintahorainicio1)
            array_push($where, " s1.quintahorainicio1 LIKE '%$quintahorainicio1%' ");

        $quintahorafim1 = (isset($queries["quintahorafim1"])) ? $queries["quintahorafim1"] : false;
        if ($quintahorafim1)
            array_push($where, " s1.quintahorafim1 LIKE '%$quintahorafim1%' ");

        $quintahorainicio2 = (isset($queries["quintahorainicio2"])) ? $queries["quintahorainicio2"] : false;
        if ($quintahorainicio2)
            array_push($where, " s1.quintahorainicio2 LIKE '%$quintahorainicio2%' ");

        $quintahorafim2 = (isset($queries["quintahorafim2"])) ? $queries["quintahorafim2"] : false;
        if ($quintahorafim2)
            array_push($where, " s1.quintahorafim2 LIKE '%$quintahorafim2%' ");

        $sextahorainicio1 = (isset($queries["sextahorainicio1"])) ? $queries["sextahorainicio1"] : false;
        if ($sextahorainicio1)
            array_push($where, " s1.sextahorainicio1 LIKE '%$sextahorainicio1%' ");

        $sextahorafim1 = (isset($queries["sextahorafim1"])) ? $queries["sextahorafim1"] : false;
        if ($sextahorafim1)
            array_push($where, " s1.sextahorafim1 LIKE '%$sextahorafim1%' ");

        $sextahorainicio2 = (isset($queries["sextahorainicio2"])) ? $queries["sextahorainicio2"] : false;
        if ($sextahorainicio2)
            array_push($where, " s1.sextahorainicio2 LIKE '%$sextahorainicio2%' ");

        $sextahorafim2 = (isset($queries["sextahorafim2"])) ? $queries["sextahorafim2"] : false;
        if ($sextahorafim2)
            array_push($where, " s1.sextahorafim2 LIKE '%$sextahorafim2%' ");

        $htpcsemana1 = (isset($queries["htpcsemana1"])) ? $queries["htpcsemana1"] : false;
        if ($htpcsemana1)
            array_push($where, " s1.htpcsemana1 LIKE '%$htpcsemana1%' ");

        $htpcsemanahorainicio1 = (isset($queries["htpcsemanahorainicio1"])) ? $queries["htpcsemanahorainicio1"] : false;
        if ($htpcsemanahorainicio1)
            array_push($where, " s1.htpcsemanahorainicio1 LIKE '%$htpcsemanahorainicio1%' ");

        $htpcsemanahorafim1 = (isset($queries["htpcsemanahorafim1"])) ? $queries["htpcsemanahorafim1"] : false;
        if ($htpcsemanahorafim1)
            array_push($where, " s1.htpcsemanahorafim1 LIKE '%$htpcsemanahorafim1%' ");

        $htpcsemana2 = (isset($queries["htpcsemana2"])) ? $queries["htpcsemana2"] : false;
        if ($htpcsemana2)
            array_push($where, " s1.htpcsemana2 LIKE '%$htpcsemana2%' ");

        $htpcsemanahorainicio2 = (isset($queries["htpcsemanahorainicio2"])) ? $queries["htpcsemanahorainicio2"] : false;
        if ($htpcsemanahorainicio2)
            array_push($where, " s1.htpcsemanahorainicio2 LIKE '%$htpcsemanahorainicio2%' ");

        $htpcsemanahorafim2 = (isset($queries["htpcsemanahorafim2"])) ? $queries["htpcsemanahorafim2"] : false;
        if ($htpcsemanahorafim2)
            array_push($where, " s1.htpcsemanahorafim2 LIKE '%$htpcsemanahorafim2%' ");

        $local = (isset($queries["local"])) ? $queries["local"] : false;
        if ($local)
            array_push($where, " s1.local LIKE '%$local%' ");

        $data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
        if ($data_i)
            array_push($where, " s1.data >= '$data_i' ");

        $data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
        if ($data_f)
            array_push($where, " s1.data <= '$data_f' ");

        $v2unidadetrabalho = (isset($queries["v2unidadetrabalho"])) ? $queries["v2unidadetrabalho"] : false;
        if ($v2unidadetrabalho)
            array_push($where, " s1.v2unidadetrabalho LIKE '%$v2unidadetrabalho%' ");

        $v2telefone = (isset($queries["v2telefone"])) ? $queries["v2telefone"] : false;
        if ($v2telefone)
            array_push($where, " s1.v2telefone LIKE '%$v2telefone%' ");

        $v2idcargasemanal = (isset($queries["v2idcargasemanal"])) ? $queries["v2idcargasemanal"] : false;
        if ($v2idcargasemanal)
            array_push($where, " s1.v2idcargasemanal = $v2idcargasemanal ");

        $v2horariotrabalho = (isset($queries["v2horariotrabalho"])) ? $queries["v2horariotrabalho"] : false;
        if ($v2horariotrabalho)
            array_push($where, " s1.v2horariotrabalho LIKE '%$v2horariotrabalho%' ");

        $v2segundahorainicio1 = (isset($queries["v2segundahorainicio1"])) ? $queries["v2segundahorainicio1"] : false;
        if ($v2segundahorainicio1)
            array_push($where, " s1.v2segundahorainicio1 LIKE '%$v2segundahorainicio1%' ");

        $v2segundahorafim1 = (isset($queries["v2segundahorafim1"])) ? $queries["v2segundahorafim1"] : false;
        if ($v2segundahorafim1)
            array_push($where, " s1.v2segundahorafim1 LIKE '%$v2segundahorafim1%' ");

        $v2segundahorainicio2 = (isset($queries["v2segundahorainicio2"])) ? $queries["v2segundahorainicio2"] : false;
        if ($v2segundahorainicio2)
            array_push($where, " s1.v2segundahorainicio2 LIKE '%$v2segundahorainicio2%' ");

        $v2segundahorafim2 = (isset($queries["v2segundahorafim2"])) ? $queries["v2segundahorafim2"] : false;
        if ($v2segundahorafim2)
            array_push($where, " s1.v2segundahorafim2 LIKE '%$v2segundahorafim2%' ");

        $v2tercahorainicio1 = (isset($queries["v2tercahorainicio1"])) ? $queries["v2tercahorainicio1"] : false;
        if ($v2tercahorainicio1)
            array_push($where, " s1.v2tercahorainicio1 LIKE '%$v2tercahorainicio1%' ");

        $v2tercahorafim1 = (isset($queries["v2tercahorafim1"])) ? $queries["v2tercahorafim1"] : false;
        if ($v2tercahorafim1)
            array_push($where, " s1.v2tercahorafim1 LIKE '%$v2tercahorafim1%' ");

        $v2tercahorainicio2 = (isset($queries["v2tercahorainicio2"])) ? $queries["v2tercahorainicio2"] : false;
        if ($v2tercahorainicio2)
            array_push($where, " s1.v2tercahorainicio2 LIKE '%$v2tercahorainicio2%' ");

        $v2tercahorafim2 = (isset($queries["v2tercahorafim2"])) ? $queries["v2tercahorafim2"] : false;
        if ($v2tercahorafim2)
            array_push($where, " s1.v2tercahorafim2 LIKE '%$v2tercahorafim2%' ");

        $v2quartahorainicio1 = (isset($queries["v2quartahorainicio1"])) ? $queries["v2quartahorainicio1"] : false;
        if ($v2quartahorainicio1)
            array_push($where, " s1.v2quartahorainicio1 LIKE '%$v2quartahorainicio1%' ");

        $v2quartahorafim1 = (isset($queries["v2quartahorafim1"])) ? $queries["v2quartahorafim1"] : false;
        if ($v2quartahorafim1)
            array_push($where, " s1.v2quartahorafim1 LIKE '%$v2quartahorafim1%' ");

        $v2quartahorainicio2 = (isset($queries["v2quartahorainicio2"])) ? $queries["v2quartahorainicio2"] : false;
        if ($v2quartahorainicio2)
            array_push($where, " s1.v2quartahorainicio2 LIKE '%$v2quartahorainicio2%' ");

        $v2quartahorafim2 = (isset($queries["v2quartahorafim2"])) ? $queries["v2quartahorafim2"] : false;
        if ($v2quartahorafim2)
            array_push($where, " s1.v2quartahorafim2 LIKE '%$v2quartahorafim2%' ");

        $v2quintahorainicio1 = (isset($queries["v2quintahorainicio1"])) ? $queries["v2quintahorainicio1"] : false;
        if ($v2quintahorainicio1)
            array_push($where, " s1.v2quintahorainicio1 LIKE '%$v2quintahorainicio1%' ");

        $v2quintahorafim1 = (isset($queries["v2quintahorafim1"])) ? $queries["v2quintahorafim1"] : false;
        if ($v2quintahorafim1)
            array_push($where, " s1.v2quintahorafim1 LIKE '%$v2quintahorafim1%' ");

        $v2quintahorainicio2 = (isset($queries["v2quintahorainicio2"])) ? $queries["v2quintahorainicio2"] : false;
        if ($v2quintahorainicio2)
            array_push($where, " s1.v2quintahorainicio2 LIKE '%$v2quintahorainicio2%' ");

        $v2quintahorafim2 = (isset($queries["v2quintahorafim2"])) ? $queries["v2quintahorafim2"] : false;
        if ($v2quintahorafim2)
            array_push($where, " s1.v2quintahorafim2 LIKE '%$v2quintahorafim2%' ");

        $v2sextahorainicio1 = (isset($queries["v2sextahorainicio1"])) ? $queries["v2sextahorainicio1"] : false;
        if ($v2sextahorainicio1)
            array_push($where, " s1.v2sextahorainicio1 LIKE '%$v2sextahorainicio1%' ");

        $v2sextahorafim1 = (isset($queries["v2sextahorafim1"])) ? $queries["v2sextahorafim1"] : false;
        if ($v2sextahorafim1)
            array_push($where, " s1.v2sextahorafim1 LIKE '%$v2sextahorafim1%' ");

        $v2sextahorainicio2 = (isset($queries["v2sextahorainicio2"])) ? $queries["v2sextahorainicio2"] : false;
        if ($v2sextahorainicio2)
            array_push($where, " s1.v2sextahorainicio2 LIKE '%$v2sextahorainicio2%' ");

        $v2sextahorafim2 = (isset($queries["v2sextahorafim2"])) ? $queries["v2sextahorafim2"] : false;
        if ($v2sextahorafim2)
            array_push($where, " s1.v2sextahorafim2 LIKE '%$v2sextahorafim2%' ");

        $v2htpcsemana1 = (isset($queries["v2htpcsemana1"])) ? $queries["v2htpcsemana1"] : false;
        if ($v2htpcsemana1)
            array_push($where, " s1.v2htpcsemana1 LIKE '%$v2htpcsemana1%' ");

        $v2htpcsemanahorainicio1 = (isset($queries["v2htpcsemanahorainicio1"])) ? $queries["v2htpcsemanahorainicio1"] : false;
        if ($v2htpcsemanahorainicio1)
            array_push($where, " s1.v2htpcsemanahorainicio1 LIKE '%$v2htpcsemanahorainicio1%' ");

        $v2htpcsemanahorafim1 = (isset($queries["v2htpcsemanahorafim1"])) ? $queries["v2htpcsemanahorafim1"] : false;
        if ($v2htpcsemanahorafim1)
            array_push($where, " s1.v2htpcsemanahorafim1 LIKE '%$v2htpcsemanahorafim1%' ");

        $v2htpcsemana2 = (isset($queries["v2htpcsemana2"])) ? $queries["v2htpcsemana2"] : false;
        if ($v2htpcsemana2)
            array_push($where, " s1.v2htpcsemana2 LIKE '%$v2htpcsemana2%' ");

        $v2htpcsemanahorainicio2 = (isset($queries["v2htpcsemanahorainicio2"])) ? $queries["v2htpcsemanahorainicio2"] : false;
        if ($v2htpcsemanahorainicio2)
            array_push($where, " s1.v2htpcsemanahorainicio2 LIKE '%$v2htpcsemanahorainicio2%' ");

        $v2htpcsemanahorafim2 = (isset($queries["v2htpcsemanahorafim2"])) ? $queries["v2htpcsemanahorafim2"] : false;
        if ($v2htpcsemanahorafim2)
            array_push($where, " s1.v2htpcsemanahorafim2 LIKE '%$v2htpcsemanahorafim2%' ");

        $v2local = (isset($queries["v2local"])) ? $queries["v2local"] : false;
        if ($v2local)
            array_push($where, " s1.v2local LIKE '%$v2local%' ");

        $v2data_i = (isset($queries["v2data_i"])) ? $queries["v2data_i"] : false;
        if ($v2data_i)
            array_push($where, " s1.v2data >= '$v2data_i' ");

        $v2data_f = (isset($queries["v2data_f"])) ? $queries["v2data_f"] : false;
        if ($v2data_f)
            array_push($where, " s1.v2data <= '$v2data_f' ");

        $penaslei = (isset($queries["penaslei"])) ? $queries["penaslei"] : false;
        if ($penaslei)
            array_push($where, " s1.penaslei LIKE '%$penaslei%' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " s1.status LIKE '%$status%' ");

        $deferido = (isset($queries["deferido"])) ? $queries["deferido"] : false;
        if ($deferido)
            array_push($where, " s1.deferido LIKE '%$deferido%' ");



        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "s1.*, f1.nome, f1.rgf";
        $fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado ";
        $fields .= ", e_v2idendereco.cep as e_v2idenderecocep, e_v2idendereco.endereco as e_v2idenderecoendereco, e_v2idendereco.numero as e_v2idendereconumero, e_v2idendereco.complemento as e_v2idenderecocomplemento, e_v2idendereco.bairro as e_v2idenderecobairro, e_v2idendereco.idestado as e_v2idenderecoidestado,e_v2idendereco.cidade as e_v2idenderecocidade,es_v2idendereco.uf as e_v2idenderecouf,es_v2idendereco.uf as e_v2idenderecoestado ";

        if ($total)
            $fields = "COUNT(s1.id) as total";

        $ordem = "ORDER BY s1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields
            FROM secretariasavaliacoesacumulos s1
             LEFT JOIN funcionariosgeraisescolas f1 ON f1.id = s1.idfuncionario
             LEFT JOIN enderecos e_idendereco ON e_idendereco.id=s1.idendereco
                    LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado	 LEFT JOIN enderecos e_v2idendereco ON e_v2idendereco.id=s1.v2idendereco
                    LEFT JOIN estados es_v2idendereco ON es_v2idendereco.id=e_v2idendereco.idestado
            WHERE s1.excluido='nao'
                    $w
            $ordem
            $limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getSecretariasavaliacaoacumuloById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getSecretariasavaliacoesacumulos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getSecretariasavaliacaoacumuloByIdHelper($id, $queries = array()) {
        $rows = new Secretariasavaliacoesacumulos();
        return $rows->getSecretariasavaliacaoacumuloById($id, $queries);
    }

    public function getUltimoSecretariasavaliacoesacumulo($queries = array()) {
        $queries['order'] = 'ORDER BY s1.id DESC';
        $rows = $this->getSecretariasavaliacoesacumulos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    /**
     * Gerar data atual (dia/mês/ano) com formato português
     * @return string data
     */
    public function getCurrentData() {
        $data = date('D');
        $mes = date('M');
        $dia = date('d');
        $ano = date('Y');

        $semana = array(
            'Sun' => 'Domingo',
            'Mon' => 'Segunda-Feira',
            'Tue' => 'Terça-Feira',
            'Wed' => 'Quarta-Feira',
            'Thu' => 'Quinta-Feira',
            'Fri' => 'Sexta-Feira',
            'Sat' => 'Sábado'
        );

        $mes_extenso = array(
            'Jan' => 'Janeiro',
            'Feb' => 'Fevereiro',
            'Mar' => 'Marco',
            'Apr' => 'Abril',
            'May' => 'Maio',
            'Jun' => 'Junho',
            'Jul' => 'Julho',
            'Aug' => 'Agosto',
            'Nov' => 'Novembro',
            'Sep' => 'Setembro',
            'Oct' => 'Outubro',
            'Dec' => 'Dezembro'
        );

        $data = "{$dia} de " . $mes_extenso["$mes"] . " de {$ano}";
        return $data;
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Secretariasavaliacoesacumulos
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idendereco = (array_key_exists("idendereco", $dados)) ? $dados["idendereco"] : $row->idendereco;
        $row->idfuncionario = (array_key_exists("idfuncionario", $dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
        $row->v2idendereco = (array_key_exists("v2idendereco", $dados)) ? $dados["v2idendereco"] : $row->v2idendereco;
        $row->sequencial = (array_key_exists("sequencial", $dados)) ? $dados["sequencial"] : $row->sequencial;
        $row->datalancamento = (array_key_exists("datalancamento", $dados)) ? $dados["datalancamento"] : $row->datalancamento;
        $row->horalancamento = (array_key_exists("horalancamento", $dados)) ? $dados["horalancamento"] : $row->horalancamento;
        $row->idusuariologado = (array_key_exists("idusuariologado", $dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
        $row->nome = (array_key_exists("nome", $dados)) ? $dados["nome"] : $row->nome;
        $row->cargo = (array_key_exists("cargo", $dados)) ? $dados["cargo"] : $row->cargo;
        $row->rf = (array_key_exists("rf", $dados)) ? $dados["rf"] : $row->rf;
        $row->rg = (array_key_exists("rg", $dados)) ? $dados["rg"] : $row->rg;
        $row->unidadetrabalho = (array_key_exists("unidadetrabalho", $dados)) ? $dados["unidadetrabalho"] : $row->unidadetrabalho;
        $row->telefone = (array_key_exists("telefone", $dados)) ? $dados["telefone"] : $row->telefone;
        $row->idcargasemanal = (array_key_exists("idcargasemanal", $dados)) ? $dados["idcargasemanal"] : $row->idcargasemanal;
        $row->horariotrabalho = (array_key_exists("horariotrabalho", $dados)) ? $dados["horariotrabalho"] : $row->horariotrabalho;
        $row->segundahorainicio1 = (array_key_exists("segundahorainicio1", $dados)) ? $dados["segundahorainicio1"] : $row->segundahorainicio1;
        $row->segundahorafim1 = (array_key_exists("segundahorafim1", $dados)) ? $dados["segundahorafim1"] : $row->segundahorafim1;
        $row->segundahorainicio2 = (array_key_exists("segundahorainicio2", $dados)) ? $dados["segundahorainicio2"] : $row->segundahorainicio2;
        $row->segundahorafim2 = (array_key_exists("segundahorafim2", $dados)) ? $dados["segundahorafim2"] : $row->segundahorafim2;
        $row->tercahorainicio1 = (array_key_exists("tercahorainicio1", $dados)) ? $dados["tercahorainicio1"] : $row->tercahorainicio1;
        $row->tercahorafim1 = (array_key_exists("tercahorafim1", $dados)) ? $dados["tercahorafim1"] : $row->tercahorafim1;
        $row->tercahorainicio2 = (array_key_exists("tercahorainicio2", $dados)) ? $dados["tercahorainicio2"] : $row->tercahorainicio2;
        $row->tercahorafim2 = (array_key_exists("tercahorafim2", $dados)) ? $dados["tercahorafim2"] : $row->tercahorafim2;
        $row->quartahorainicio1 = (array_key_exists("quartahorainicio1", $dados)) ? $dados["quartahorainicio1"] : $row->quartahorainicio1;
        $row->quartahorafim1 = (array_key_exists("quartahorafim1", $dados)) ? $dados["quartahorafim1"] : $row->quartahorafim1;
        $row->quartahorainicio2 = (array_key_exists("quartahorainicio2", $dados)) ? $dados["quartahorainicio2"] : $row->quartahorainicio2;
        $row->quartahorafim2 = (array_key_exists("quartahorafim2", $dados)) ? $dados["quartahorafim2"] : $row->quartahorafim2;
        $row->quintahorainicio1 = (array_key_exists("quintahorainicio1", $dados)) ? $dados["quintahorainicio1"] : $row->quintahorainicio1;
        $row->quintahorafim1 = (array_key_exists("quintahorafim1", $dados)) ? $dados["quintahorafim1"] : $row->quintahorafim1;
        $row->quintahorainicio2 = (array_key_exists("quintahorainicio2", $dados)) ? $dados["quintahorainicio2"] : $row->quintahorainicio2;
        $row->quintahorafim2 = (array_key_exists("quintahorafim2", $dados)) ? $dados["quintahorafim2"] : $row->quintahorafim2;
        $row->sextahorainicio1 = (array_key_exists("sextahorainicio1", $dados)) ? $dados["sextahorainicio1"] : $row->sextahorainicio1;
        $row->sextahorafim1 = (array_key_exists("sextahorafim1", $dados)) ? $dados["sextahorafim1"] : $row->sextahorafim1;
        $row->sextahorainicio2 = (array_key_exists("sextahorainicio2", $dados)) ? $dados["sextahorainicio2"] : $row->sextahorainicio2;
        $row->sextahorafim2 = (array_key_exists("sextahorafim2", $dados)) ? $dados["sextahorafim2"] : $row->sextahorafim2;
        $row->htpcsemana1 = (array_key_exists("htpcsemana1", $dados)) ? $dados["htpcsemana1"] : $row->htpcsemana1;
        $row->htpcsemanahorainicio1 = (array_key_exists("htpcsemanahorainicio1", $dados)) ? $dados["htpcsemanahorainicio1"] : $row->htpcsemanahorainicio1;
        $row->htpcsemanahorafim1 = (array_key_exists("htpcsemanahorafim1", $dados)) ? $dados["htpcsemanahorafim1"] : $row->htpcsemanahorafim1;
        $row->htpcsemana2 = (array_key_exists("htpcsemana2", $dados)) ? $dados["htpcsemana2"] : $row->htpcsemana2;
        $row->htpcsemanahorainicio2 = (array_key_exists("htpcsemanahorainicio2", $dados)) ? $dados["htpcsemanahorainicio2"] : $row->htpcsemanahorainicio2;
        $row->htpcsemanahorafim2 = (array_key_exists("htpcsemanahorafim2", $dados)) ? $dados["htpcsemanahorafim2"] : $row->htpcsemanahorafim2;
        $row->local = (array_key_exists("local", $dados)) ? $dados["local"] : $row->local;
        $row->data = (array_key_exists("data", $dados)) ? $dados["data"] : $row->data;
        $row->v2unidadetrabalho = (array_key_exists("v2unidadetrabalho", $dados)) ? $dados["v2unidadetrabalho"] : $row->v2unidadetrabalho;
        $row->v2telefone = (array_key_exists("v2telefone", $dados)) ? $dados["v2telefone"] : $row->v2telefone;
        $row->v2idcargasemanal = (array_key_exists("v2idcargasemanal", $dados)) ? $dados["v2idcargasemanal"] : $row->v2idcargasemanal;
        $row->v2horariotrabalho = (array_key_exists("v2horariotrabalho", $dados)) ? $dados["v2horariotrabalho"] : $row->v2horariotrabalho;
        $row->v2segundahorainicio1 = (array_key_exists("v2segundahorainicio1", $dados)) ? $dados["v2segundahorainicio1"] : $row->v2segundahorainicio1;
        $row->v2segundahorafim1 = (array_key_exists("v2segundahorafim1", $dados)) ? $dados["v2segundahorafim1"] : $row->v2segundahorafim1;
        $row->v2segundahorainicio2 = (array_key_exists("v2segundahorainicio2", $dados)) ? $dados["v2segundahorainicio2"] : $row->v2segundahorainicio2;
        $row->v2segundahorafim2 = (array_key_exists("v2segundahorafim2", $dados)) ? $dados["v2segundahorafim2"] : $row->v2segundahorafim2;
        $row->v2tercahorainicio1 = (array_key_exists("v2tercahorainicio1", $dados)) ? $dados["v2tercahorainicio1"] : $row->v2tercahorainicio1;
        $row->v2tercahorafim1 = (array_key_exists("v2tercahorafim1", $dados)) ? $dados["v2tercahorafim1"] : $row->v2tercahorafim1;
        $row->v2tercahorainicio2 = (array_key_exists("v2tercahorainicio2", $dados)) ? $dados["v2tercahorainicio2"] : $row->v2tercahorainicio2;
        $row->v2tercahorafim2 = (array_key_exists("v2tercahorafim2", $dados)) ? $dados["v2tercahorafim2"] : $row->v2tercahorafim2;
        $row->v2quartahorainicio1 = (array_key_exists("v2quartahorainicio1", $dados)) ? $dados["v2quartahorainicio1"] : $row->v2quartahorainicio1;
        $row->v2quartahorafim1 = (array_key_exists("v2quartahorafim1", $dados)) ? $dados["v2quartahorafim1"] : $row->v2quartahorafim1;
        $row->v2quartahorainicio2 = (array_key_exists("v2quartahorainicio2", $dados)) ? $dados["v2quartahorainicio2"] : $row->v2quartahorainicio2;
        $row->v2quartahorafim2 = (array_key_exists("v2quartahorafim2", $dados)) ? $dados["v2quartahorafim2"] : $row->v2quartahorafim2;
        $row->v2quintahorainicio1 = (array_key_exists("v2quintahorainicio1", $dados)) ? $dados["v2quintahorainicio1"] : $row->v2quintahorainicio1;
        $row->v2quintahorafim1 = (array_key_exists("v2quintahorafim1", $dados)) ? $dados["v2quintahorafim1"] : $row->v2quintahorafim1;
        $row->v2quintahorainicio2 = (array_key_exists("v2quintahorainicio2", $dados)) ? $dados["v2quintahorainicio2"] : $row->v2quintahorainicio2;
        $row->v2quintahorafim2 = (array_key_exists("v2quintahorafim2", $dados)) ? $dados["v2quintahorafim2"] : $row->v2quintahorafim2;
        $row->v2sextahorainicio1 = (array_key_exists("v2sextahorainicio1", $dados)) ? $dados["v2sextahorainicio1"] : $row->v2sextahorainicio1;
        $row->v2sextahorafim1 = (array_key_exists("v2sextahorafim1", $dados)) ? $dados["v2sextahorafim1"] : $row->v2sextahorafim1;
        $row->v2sextahorainicio2 = (array_key_exists("v2sextahorainicio2", $dados)) ? $dados["v2sextahorainicio2"] : $row->v2sextahorainicio2;
        $row->v2sextahorafim2 = (array_key_exists("v2sextahorafim2", $dados)) ? $dados["v2sextahorafim2"] : $row->v2sextahorafim2;
        $row->v2htpcsemana1 = (array_key_exists("v2htpcsemana1", $dados)) ? $dados["v2htpcsemana1"] : $row->v2htpcsemana1;
        $row->v2htpcsemanahorainicio1 = (array_key_exists("v2htpcsemanahorainicio1", $dados)) ? $dados["v2htpcsemanahorainicio1"] : $row->v2htpcsemanahorainicio1;
        $row->v2htpcsemanahorafim1 = (array_key_exists("v2htpcsemanahorafim1", $dados)) ? $dados["v2htpcsemanahorafim1"] : $row->v2htpcsemanahorafim1;
        $row->v2htpcsemana2 = (array_key_exists("v2htpcsemana2", $dados)) ? $dados["v2htpcsemana2"] : $row->v2htpcsemana2;
        $row->v2htpcsemanahorainicio2 = (array_key_exists("v2htpcsemanahorainicio2", $dados)) ? $dados["v2htpcsemanahorainicio2"] : $row->v2htpcsemanahorainicio2;
        $row->v2htpcsemanahorafim2 = (array_key_exists("v2htpcsemanahorafim2", $dados)) ? $dados["v2htpcsemanahorafim2"] : $row->v2htpcsemanahorafim2;
        $row->v2local = (array_key_exists("v2local", $dados)) ? $dados["v2local"] : $row->v2local;
        $row->v2data = (array_key_exists("v2data", $dados)) ? $dados["v2data"] : $row->v2data;
        $row->penasleis = (array_key_exists("penasleis", $dados)) ? $dados["penasleis"] : $row->penasleis;
        $row->idarquivo = (array_key_exists("idarquivo", $dados)) ? $dados["idarquivo"] : $row->idarquivo;
        $row->distancia = (array_key_exists("distancia", $dados)) ? $dados["distancia"] : $row->distancia;
        $row->transporte = (array_key_exists("transporte", $dados)) ? $dados["transporte"] : $row->transporte;
        $row->horas = (array_key_exists("horas", $dados)) ? $dados["horas"] : $row->horas;
        $row->minutos = (array_key_exists("minutos", $dados)) ? $dados["minutos"] : $row->minutos;

        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        $row->deferido = (array_key_exists("deferido", $dados)) ? $dados["deferido"] : $row->deferido;
        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }
        $row->idescola = (array_key_exists("idescola", $dados)) ? $dados["idescola"] : $row->idescola;
        $row->idlocal = (array_key_exists("idlocal", $dados)) ? $dados["idlocal"] : $row->idlocal;

        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;


        $row->save();

        return $row;
    }

}
