<?php

/**
 * Define o modelo Logs
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Logs extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "logs";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "lgid";

    public static function getLogsHelper($queries = array(), $page = 0, $maxpage = 0) {
        $logs = new Logs();
        return $logs->getLogs($queries, $page, $maxpage);
    }

    public function getLogs($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $lgid = (isset($queries['lgid'])) ? (int) $queries['lgid'] : false;
        if ($lgid)
            array_push($where, " l1.lgid = $lgid ");
        
        $data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
        if ($data_i){
            $data_i = substr($data_i, 6,4) . '-' . substr($data_i, 3,2). '-' . substr($data_i, 0,2);
            array_push($where, " l1.lgdtsis >= '$data_i 00:00:00' ");
        }
        
        $data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
        if ($data_f){
            $data_f = substr($data_f, 6,4) . '-' . substr($data_f, 3,2). '-' . substr($data_f, 0,2);;
            array_push($where, " l1.lgdtsis <= '$data_f 23:59:59' ");
        }
        
        $lgnomeusuario = (isset($queries['lgnomeusuario'])) ? $queries['lgnomeusuario'] : false;
        if ($lgnomeusuario)
            array_push($where, " l1.lgnomeusuario like '%$lgnomeusuario%' ");

        $lgmensagem = (isset($queries["lgmensagem"])) ? $queries["lgmensagem"] : false;
        if ($lgmensagem)
            array_push($where, " l1.lgmensagem like '%$lgmensagem%'");

        $lgtpmensagem = (isset($queries["lgtpmensagem"])) ? $queries["lgtpmensagem"] : false;
        if ($lgtpmensagem)
            array_push($where, " l1.lgtpmensagem like '%$lgtpmensagem%' ");

        $lgipusuario = (isset($queries["lgipusuario"])) ? $queries["lgipusuario"] : false;
        if ($lgipusuario)
            array_push($where, " l1.lgipusuario like '$lgipusuario%' ");


        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        
        if ($w != "")
            $w = "AND ($w)";

        $fields = "l1.*";
        
        if ($total)
            $fields = "COUNT(l1.lgid) as total";

        $ordem = "ORDER BY l1.lgid DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM logs l1
					
					WHERE 1 
						$w 
					$ordem	
					$limit";
        
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getLogById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['lgid'] = $id;
        $rows = $this->getLogs($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getLogByIdHelper($id, $queries = array()) {
        $rows = new Logs();
        return $rows->getLogById($id, $queries);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Logs
     */
    public function save($dados) {

        $row = $this->createRow();

        $row->lgdtsis = (array_key_exists("lgdtsis", $dados)) ? $dados["lgdtsis"] : $row->lgdtsis;
        $row->lgdtatual = (array_key_exists("lgdtatual", $dados)) ? $dados["lgdtatual"] : $row->lgdtatual;
        $row->lgsistema = (array_key_exists("lgsistema", $dados)) ? $dados["lgsistema"] : $row->lgsistema;
        $row->lgrotina = (array_key_exists("lgrotina", $dados)) ? $dados["lgrotina"] : $row->lgrotina;
        $row->lgnomeusuario = (array_key_exists("lgnomeusuario", $dados)) ? $dados["lgnomeusuario"] : $row->lgnomeusuario;
        $row->lgmensagem = (array_key_exists("lgmensagem", $dados)) ? $dados["lgmensagem"] : $row->lgmensagem;
        $row->lgtpmensagem = (array_key_exists("lgtpmensagem", $dados)) ? $dados["lgtpmensagem"] : $row->lgtpmensagem;
        $row->lgbrowser = (array_key_exists("lgbrowser", $dados)) ? $dados["lgbrowser"] : $row->lgbrowser;
        $row->lgipusuario = (array_key_exists("lgipusuario", $dados)) ? $dados["lgipusuario"] : $row->lgipusuario;

        $row->save();

        return $row;
    }

}
