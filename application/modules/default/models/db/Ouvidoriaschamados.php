<?php

/**
 * Define o modelo Ouvidoriaschamados
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Ouvidoriaschamados extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "ouvidoriaschamados";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getIdUltimaOuvidoriachamado() {
		$ouvidoriaschamados = new Ouvidoriaschamados();

		$ouvidoriachamado = $ouvidoriaschamados->getOuvidoriaschamados(array(),0,1);

		$ouvidoriac = $ouvidoriachamado[0];

		return $ouvidoriac['id'];
	}
	
	public static function getOuvidoriaschamadosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$ouvidoriaschamados = new Ouvidoriaschamados();
		return $ouvidoriaschamados->getOuvidoriaschamados($queries, $page, $maxpage);
	}
	
	public function getOuvidoriaschamados($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " o1.id = $id ");
		
		
		$tipoprocesso = (isset($queries["tipoprocesso"])) ? $queries["tipoprocesso"] : false;
		if ($tipoprocesso) array_push($where, " o1.tipoprocesso LIKE '%$tipoprocesso%' ");

$tipochamado = (isset($queries["tipochamado"])) ? $queries["tipochamado"] : false;
		if ($tipochamado) array_push($where, " o1.tipochamado LIKE '%$tipochamado%' ");

$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " o1.sequencial LIKE '%$sequencial%' ");

$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " o1.datalancamento >= '$datalancamento_i' ");

$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " o1.datalancamento <= '$datalancamento_f' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " o1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "o1.*"; 
		;
		
		if ($total) $fields = "COUNT(o1.id) as total";
		
		$ordem = "ORDER BY o1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM ouvidoriaschamados o1
					
					WHERE o1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getOuvidoriachamadoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getOuvidoriaschamados($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getOuvidoriachamadoByIdHelper($id, $queries = array()) {
		$rows = new Ouvidoriaschamados();
		return $rows->getOuvidoriachamadoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ouvidoriaschamados
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->tipoprocesso = (array_key_exists("tipoprocesso",$dados)) ? $dados["tipoprocesso"] : $row->tipoprocesso;
		$row->tipochamado = (array_key_exists("tipochamado",$dados)) ? $dados["tipochamado"] : $row->tipochamado;
		$row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
		$row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
		$row->horaabertura = (array_key_exists("horaabertura",$dados)) ? $dados["horaabertura"] : $row->horaabertura;
		$row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
		$row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
		$row->tipocontato = (array_key_exists("tipocontato",$dados)) ? $dados["tipocontato"] : $row->tipocontato;
		$row->nomesolicitante = (array_key_exists("nomesolicitante",$dados)) ? $dados["nomesolicitante"] : $row->nomesolicitante;
		$row->telefone1 = (array_key_exists("telefone1",$dados)) ? $dados["telefone1"] : $row->telefone1;
		$row->telefone2 = (array_key_exists("telefone2",$dados)) ? $dados["telefone2"] : $row->telefone2;
		$row->contato = (array_key_exists("contato",$dados)) ? $dados["contato"] : $row->contato;
		$row->idtiposchamado = (array_key_exists("idtiposchamado",$dados)) ? $dados["idtiposchamado"] : $row->idtiposchamado;
		$row->assunto = (array_key_exists("assunto",$dados)) ? $dados["assunto"] : $row->assunto;
		$row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
		$row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
		$row->iddocumento = (array_key_exists("iddocumento",$dados)) ? $dados["iddocumento"] : $row->iddocumento;
		$row->posicao = (array_key_exists("posicao",$dados)) ? $dados["posicao"] : $row->posicao;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->dataencaminhamento = (array_key_exists("dataencaminhamento", $dados)) ? $dados['dataencaminhamento'] : $row->dataencaminhamento;

		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}

		$row->save();
		
		return $row;
	}
	
}