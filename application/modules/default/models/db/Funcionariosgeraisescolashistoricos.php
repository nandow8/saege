<?php

/**
 * Define o modelo funcionariosgeraisescolashistoricos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class funcionariosgeraisescolashistoricos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosgeraisescolashistoricos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getfuncionariosgeraisescolashistoricosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosgeraisescolashistoricos = new funcionariosgeraisescolashistoricos();
		return $funcionariosgeraisescolashistoricos->getfuncionariosgeraisescolashistoricos($queries, $page, $maxpage);
	}
	
	public function getfuncionariosgeraisescolashistoricos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		$idparent = (isset($queries['idparent'])) ? (int)$queries['idparent'] : false;
		if ($idparent) array_push($where, " f1.idparent = $idparent ");

		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " f1.idsecretaria = $idsecretaria ");

$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
		if ($nome) array_push($where, " f1.nome LIKE '%$nome%' ");

$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
		if ($telefone) array_push($where, " f1.telefone LIKE '%$telefone%' ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " f1.tipo LIKE '%$tipo%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado "; ;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgeraisescolashistoricos f1
					 LEFT JOIN enderecos e_idendereco ON e_idendereco.id=f1.idendereco 
						LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado	
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariogeralescolahistoricoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getfuncionariosgeraisescolashistoricos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionariogeralescolahistoricoByIdHelper($id, $queries = array()) {
		$rows = new funcionariosgeraisescolashistoricos();
		return $rows->getFuncionariogeralescolahistoricoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return funcionariosgeraisescolashistoricos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idparent = (array_key_exists("idparent",$dados)) ? $dados["idparent"] : $row->idparent;
 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->iddepartamentoescola = (array_key_exists("iddepartamentoescola",$dados)) ? $dados["iddepartamentoescola"] : $row->iddepartamentoescola;
 $row->idimagem = (array_key_exists("idimagem",$dados)) ? $dados["idimagem"] : $row->idimagem;
 $row->idfuncao = (array_key_exists("idfuncao",$dados)) ? $dados["idfuncao"] : $row->idfuncao;
 $row->idcargo = (array_key_exists("idcargo",$dados)) ? $dados["idcargo"] : $row->idcargo;
 $row->datafimbolsa = (array_key_exists("datafimbolsa",$dados)) ? $dados["datafimbolsa"] : $row->datafimbolsa;
 $row->datainiciobolsa = (array_key_exists("datainiciobolsa",$dados)) ? $dados["datainiciobolsa"] : $row->datainiciobolsa;
 $row->bolsista = (array_key_exists("bolsista",$dados)) ? $dados["bolsista"] : $row->bolsista;
 $row->regime_outros = (array_key_exists("regime_outros",$dados)) ? $dados["regime_outros"] : $row->regime_outros;
 $row->regime = (array_key_exists("regime",$dados)) ? $dados["regime"] : $row->regime;
 $row->conta = (array_key_exists("conta",$dados)) ? $dados["conta"] : $row->conta;
 $row->dataregistroctps = (array_key_exists("dataregistroctps",$dados)) ? $dados["dataregistroctps"] : $row->dataregistroctps;
 $row->ctps = (array_key_exists("ctps",$dados)) ? $dados["ctps"] : $row->ctps;
 $row->filiacaopai = (array_key_exists("filiacaopai",$dados)) ? $dados["filiacaopai"] : $row->filiacaopai;
 $row->filiacaomae = (array_key_exists("filiacaomae",$dados)) ? $dados["filiacaomae"] : $row->filiacaomae;
 $row->carteiraserie = (array_key_exists("carteiraserie",$dados)) ? $dados["carteiraserie"] : $row->carteiraserie;
 $row->carteiranumero = (array_key_exists("carteiranumero",$dados)) ? $dados["carteiranumero"] : $row->carteiranumero;
 $row->celular = (array_key_exists("celular",$dados)) ? $dados["celular"] : $row->celular;
 $row->agencia = (array_key_exists("agencia",$dados)) ? $dados["agencia"] : $row->agencia;
 $row->banco = (array_key_exists("banco",$dados)) ? $dados["banco"] : $row->banco;
 $row->formapagamento = (array_key_exists("formapagamento",$dados)) ? $dados["formapagamento"] : $row->formapagamento;
 $row->salarioinicial = (array_key_exists("salarioinicial",$dados)) ? $dados["salarioinicial"] : $row->salarioinicial;
 $row->riscofuncao = (array_key_exists("riscofuncao",$dados)) ? $dados["riscofuncao"] : $row->riscofuncao;
 $row->descricaofuncao = (array_key_exists("descricaofuncao",$dados)) ? $dados["descricaofuncao"] : $row->descricaofuncao;
 $row->funcao = (array_key_exists("funcao",$dados)) ? $dados["funcao"] : $row->funcao;
 $row->setor = (array_key_exists("setor",$dados)) ? $dados["setor"] : $row->setor;
 $row->secretaria = (array_key_exists("secretaria",$dados)) ? $dados["secretaria"] : $row->secretaria;
 $row->matricula = (array_key_exists("matricula",$dados)) ? $dados["matricula"] : $row->matricula;
 $row->dataadmissaocomissionado = (array_key_exists("dataadmissaocomissionado",$dados)) ? $dados["dataadmissaocomissionado"] : $row->dataadmissaocomissionado;
 $row->cargocomissionado = (array_key_exists("cargocomissionado",$dados)) ? $dados["cargocomissionado"] : $row->cargocomissionado;
 $row->cargo = (array_key_exists("cargo",$dados)) ? $dados["cargo"] : $row->cargo;
 $row->cbo = (array_key_exists("cbo",$dados)) ? $dados["cbo"] : $row->cbo;
 $row->dataadmissao = (array_key_exists("dataadmissao",$dados)) ? $dados["dataadmissao"] : $row->dataadmissao;
 $row->filhosquantidade = (array_key_exists("filhosquantidade",$dados)) ? $dados["filhosquantidade"] : $row->filhosquantidade;
 $row->filhos = (array_key_exists("filhos",$dados)) ? $dados["filhos"] : $row->filhos;
 $row->conjugenome = (array_key_exists("conjugenome",$dados)) ? $dados["conjugenome"] : $row->conjugenome;
 $row->estadocivil = (array_key_exists("estadocivil",$dados)) ? $dados["estadocivil"] : $row->estadocivil;
 $row->grauinstrucao = (array_key_exists("grauinstrucao",$dados)) ? $dados["grauinstrucao"] : $row->grauinstrucao;
 $row->localnascimento = (array_key_exists("localnascimento",$dados)) ? $dados["localnascimento"] : $row->localnascimento;
 $row->nacionalidade = (array_key_exists("nacionalidade",$dados)) ? $dados["nacionalidade"] : $row->nacionalidade;
 $row->obs_estrangeiro = (array_key_exists("obs_estrangeiro",$dados)) ? $dados["obs_estrangeiro"] : $row->obs_estrangeiro;
 $row->estrangeiro = (array_key_exists("estrangeiro",$dados)) ? $dados["estrangeiro"] : $row->estrangeiro;
 $row->cnh_validade = (array_key_exists("cnh_validade",$dados)) ? $dados["cnh_validade"] : $row->cnh_validade;
 $row->cnh_categoria = (array_key_exists("cnh_categoria",$dados)) ? $dados["cnh_categoria"] : $row->cnh_categoria;
 $row->cnh_numero = (array_key_exists("cnh_numero",$dados)) ? $dados["cnh_numero"] : $row->cnh_numero;
 $row->cnh = (array_key_exists("cnh",$dados)) ? $dados["cnh"] : $row->cnh;
 $row->pispasep = (array_key_exists("pispasep",$dados)) ? $dados["pispasep"] : $row->pispasep;
 $row->cpf = (array_key_exists("cpf",$dados)) ? $dados["cpf"] : $row->cpf;
 $row->rg = (array_key_exists("rg",$dados)) ? $dados["rg"] : $row->rg;
 $row->secaoeleitoral = (array_key_exists("secaoeleitoral",$dados)) ? $dados["secaoeleitoral"] : $row->secaoeleitoral;
 $row->zonaeleitoral = (array_key_exists("zonaeleitoral",$dados)) ? $dados["zonaeleitoral"] : $row->zonaeleitoral;
 $row->tituloeleitor = (array_key_exists("tituloeleitor",$dados)) ? $dados["tituloeleitor"] : $row->tituloeleitor;
 $row->reservista_ra = (array_key_exists("reservista_ra",$dados)) ? $dados["reservista_ra"] : $row->reservista_ra;
 $row->reservista = (array_key_exists("reservista",$dados)) ? $dados["reservista"] : $row->reservista;
 $row->emissaocarteira = (array_key_exists("emissaocarteira",$dados)) ? $dados["emissaocarteira"] : $row->emissaocarteira;
 $row->datanascimento = (array_key_exists("datanascimento",$dados)) ? $dados["datanascimento"] : $row->datanascimento;
 $row->email = (array_key_exists("email",$dados)) ? $dados["email"] : $row->email;
 $row->sobrenomefantasia = (array_key_exists("sobrenomefantasia",$dados)) ? $dados["sobrenomefantasia"] : $row->sobrenomefantasia;
 $row->nomerazao = (array_key_exists("nomerazao",$dados)) ? $dados["nomerazao"] : $row->nomerazao;
 $row->professor = (array_key_exists("professor",$dados)) ? $dados["professor"] : $row->professor;
 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
 $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
 $row->modulo = (array_key_exists("modulo",$dados)) ? $dados["modulo"] : $row->modulo;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->idendereco = (array_key_exists("idendereco",$dados)) ? $dados["idendereco"] : $row->idendereco;

 $row->idhorarioponto = (array_key_exists("idhorarioponto",$dados)) ? $dados["idhorarioponto"] : $row->idhorarioponto;

 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
	public function arquiva($dados) {
		$dados['idparent'] = $dados['id'];
		unset($dados['id']);
			
		$this->createRow($dados)->save();	
	}
	
}