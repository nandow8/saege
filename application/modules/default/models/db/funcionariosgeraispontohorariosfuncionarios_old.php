<?php

/**
 * Define o modelo Atas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosgeraispontohorariosfuncionarios extends Zend_Db_Table_Abstract {

	
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosgeraispontohorariosfuncionarios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFuncionariosgeraispontohorariosfuncionariosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosgeraispontohorariosfuncionarios = new Funcionariosgeraispontohorariosfuncionarios();
		return $atas->getFuncionariosgeraispontohorariosfuncionarios($queries, $page, $maxpage);
	}
	
	public function getFuncionariosgeraispontohorariosfuncionarios($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
		
		
		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " a1.iddepartamento = $iddepartamento ");


        // 2	fkfuncionariosgeraispontodefinicaohorarios	bigint(20)			Sim	None
        $fkfuncionariosgeraispontodefinicaohorarios = (isset($queries["fkfuncionariosgeraispontodefinicaohorarios"])) ? $queries["fkfuncionariosgeraispontodefinicaohorarios"] : false;
        if ($fkfuncionariosgeraispontodefinicaohorarios) array_push($where, " a1.fkfuncionariosgeraispontodefinicaohorarios = $fkfuncionariosgeraispontodefinicaohorarios ");
        // 3	fkusuario	bigint(20)			Sim	None		
        $fkusuario = (isset($queries["fkusuario"])) ? $queries["fkusuario"] : false;
        if ($fkusuario) array_push($where, " a1.fkusuario = $fkusuario ");
        // 4	fkfuncionario
        $fkfuncionario = (isset($queries["fkfuncionario"])) ? $queries["fkfuncionario"] : false;
        if ($fkfuncionario) array_push($where, " a1.fkfuncionario = $fkfuncionario ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgeraispontohorariosfuncionarios a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	

$novastring = "SELECT * FROM funcionariosgeraispontohorariosfuncionarios";
$db = Zend_Registry::get('db');	

		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariosgeraispontohorariosfuncionariosById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosgeraispontohorariosfuncionarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionariosgeraispontohorariosfuncionariosByIdHelper($id, $queries = array()) {
		$rows = new Funcionariosgeraispontohorariosfuncionarios();
		return $rows->getFuncionariosgeraispontohorariosfuncionariosById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Funcionariosgeraispontohorariosfuncionarios
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
        } 
        
        // fkfuncionariosgeraispontodefinicaohorarios	bigint(20)			Sim	None		
        $row->fkfuncionariosgeraispontodefinicaohorarios = (array_key_exists("fkfuncionariosgeraispontodefinicaohorarios",$dados)) ? $dados["fkfuncionariosgeraispontodefinicaohorarios"] : $row->fkfuncionariosgeraispontodefinicaohorarios;
        // fkusuario	bigint(20)			Sim	None		
        $row->fkusuario = (array_key_exists("fkusuario",$dados)) ? $dados["fkusuario"] : $row->fkusuario;
        // fkfuncionario	bigint(20)			Sim	None	
        $row->fkfuncionario = (array_key_exists("fkfuncionario",$dados)) ? $dados["fkfuncionario"] : $row->fkfuncionario;
        
 
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}