<?php

/**
 * Define o modelo Atas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Feriados extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosgeraispontoferiados";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAtasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$atas = new Feriados();
		return $atas->getAtas($queries, $page, $maxpage);
	}
	
	public function getAtas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
                
                $data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
                                if ($data_i) array_push($where, " a1.data_pontoferiado >= '$data_i' ");

                $data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
                                if ($data_f) array_push($where, " a1.data_pontoferiado <= '$data_f' ");
                
                 //nome_pontoferiado	varchar(80)	utf8_general_ci		Não	None			 Muda Muda	 Elimina Elimina	
                //descricao_pontoferiado	varchar(500)	utf8_general_ci		Não	None			 Muda Muda	 Elimina Elimina	
                //data_pontoferiado	date			Não	None			 Muda Muda	 Elimina Elimina	
                //recorrente_pontoferiado	tinyint(1)			Não	None			 Muda Muda	 Elimina Elimina	
                //tipo_pontoferiado
                
                $nome_pontoferiado = (isset($queries["nome_pontoferiado"])) ? $queries["nome_pontoferiado"] : false;
		if ($nome_pontoferiado) array_push($where, " a1.nome_pontoferiado LIKE '%$nome_pontoferiado%' ");                
                                
                $descricao_pontoferiado = (isset($queries["descricao_pontoferiado"])) ? $queries["descricao_pontoferiado"] : false;
		if ($descricao_pontoferiado) array_push($where, " a1.descricao_pontoferiado LIKE '%$descricao_pontoferiado%' ");                
                           
                $recorrente_pontoferiado = (isset($queries["recorrente_pontoferiado"])) ? $queries["recorrente_pontoferiado"] : false;
		if ($recorrente_pontoferiado) array_push($where, " a1.recorrente_pontoferiado = $recorrente_pontoferiado ");
                
                $tipo_pontoferiado = (isset($queries["tipo_pontoferiado"])) ? $queries["tipo_pontoferiado"] : false;
		if ($tipo_pontoferiado) array_push($where, " a1.tipo_pontoferiado LIKE '%$tipo_pontoferiado%' ");  
                
                
                /*
                $iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " a1.iddepartamento = $iddepartamento ");

$idperiodo = (isset($queries["idperiodo"])) ? $queries["idperiodo"] : false;
		if ($idperiodo) array_push($where, " a1.idperiodo = $idperiodo ");

$idsfuncionarios = (isset($queries["idsfuncionarios"])) ? $queries["idsfuncionarios"] : false;
		if ($idsfuncionarios) array_push($where, " a1.idsfuncionarios = '$idsfuncionarios' ");

$pauta = (isset($queries["pauta"])) ? $queries["pauta"] : false;
		if ($pauta) array_push($where, " a1.pauta LIKE '%$pauta%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " a1.descricoes LIKE '%$descricoes%' ");
                */

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgeraispontoferiados a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');	
                
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
                //print_r($db->fetchAll($strsql));
                //die();
		return $db->fetchAll($strsql);			
	}	
	
	public function getAtaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAtas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAtaByIdHelper($id, $queries = array()) {
		$rows = new Atas();
		return $rows->getAtaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Atas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		}
                
		//$row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
                $row->nome_pontoferiado = (array_key_exists("nome_pontoferiado",$dados)) ? $dados["nome_pontoferiado"] : $row->nome_pontoferiado;
                $row->descricao_pontoferiado = (array_key_exists("descricao_pontoferiado",$dados)) ? $dados["descricao_pontoferiado"] : $row->descricao_pontoferiado;
                $row->data_pontoferiado = (array_key_exists("data_pontoferiado",$dados)) ? $dados["data_pontoferiado"] : $row->data_pontoferiado;
                $row->recorrente_pontoferiado = (array_key_exists("recorrente_pontoferiado",$dados)) ? $dados["recorrente_pontoferiado"] : $row->recorrente_pontoferiado;
                $row->tipo_pontoferiado = (array_key_exists("tipo_pontoferiado",$dados)) ? $dados["tipo_pontoferiado"] : $row->tipo_pontoferiado;
                $row->recorrente_pontoferiado = (array_key_exists("recorrente_pontoferiado",$dados)) ? $dados["recorrente_pontoferiado"] : $row->recorrente_pontoferiado;
 
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
        
        
         public function getVerificaFeriado($data){
           
             
            $strSql = "SELECT * FROM funcionariosgeraispontoferiados WHERE data_pontoferiado = '" . $data . "'";
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchAll($strSql);
            
            $contador = 0;
            foreach($row as $ince):
                     $contador++;
                endforeach;
            return $contador;
           
        }//end function verifica feriado
        
        /*
         *      Autor: Thiago Torres Migliorati
         *      Função: Retornar as informações do evento gravado pela dada escolhida
         *      Descrição: Criei esta função para facilitar a verificação dos valores de feriados 
         *      Cadastrados na tabela funcionariosgeraispontoferiado
         */
	
        public function getInformacoesFeriadoData($data){
            
            $strSql = "SELECT * FROM funcionariosgeraispontoferiados WHERE data_pontoferiado = '" . $data . "'";
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchRow($strSql);
           
            return $row;
           
        }//end function verifica feriado
	
}