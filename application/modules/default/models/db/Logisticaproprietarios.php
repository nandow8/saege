<?php

/**
 * Define o modelo Logisticaproprietarios
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Logisticaproprietarios extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "logisticaproprietarios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLogisticaproprietariosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$logisticaproprietarios = new Logisticaproprietarios();
		return $logisticaproprietarios->getLogisticaproprietarios($queries, $page, $maxpage);
	}
	
	public function getLogisticaproprietarios($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		
		

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*"; 
		;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM logisticaproprietarios l1
					
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLogisticaproprietarioById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLogisticaproprietarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLogisticaproprietarioByIdHelper($id, $queries = array()) {
		$rows = new Logisticaproprietarios();
		return $rows->getLogisticaproprietarioById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Logisticaproprietarios
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->pfpj = (array_key_exists("pfpj",$dados)) ? $dados["pfpj"] : $row->pfpj;
 $row->cpf = (array_key_exists("cpf",$dados)) ? $dados["cpf"] : $row->cpf;
 $row->cnpj = (array_key_exists("cnpj",$dados)) ? $dados["cnpj"] : $row->cnpj;
 $row->ie = (array_key_exists("ie",$dados)) ? $dados["ie"] : $row->ie;
 $row->im = (array_key_exists("im",$dados)) ? $dados["im"] : $row->im;
 $row->nomerazao = (array_key_exists("nomerazao",$dados)) ? $dados["nomerazao"] : $row->nomerazao;
 $row->sobrenomefantasia = (array_key_exists("sobrenomefantasia",$dados)) ? $dados["sobrenomefantasia"] : $row->sobrenomefantasia;
 $row->email = (array_key_exists("email",$dados)) ? $dados["email"] : $row->email;
 $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
 $row->celular = (array_key_exists("celular",$dados)) ? $dados["celular"] : $row->celular;
 $row->endereco = (array_key_exists("endereco",$dados)) ? $dados["endereco"] : $row->endereco;
 $row->numero = (array_key_exists("numero",$dados)) ? $dados["numero"] : $row->numero;
 $row->bairro = (array_key_exists("bairro",$dados)) ? $dados["bairro"] : $row->bairro;
 $row->cidade = (array_key_exists("cidade",$dados)) ? $dados["cidade"] : $row->cidade;
 $row->cep = (array_key_exists("cep",$dados)) ? $dados["cep"] : $row->cep;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}