<?php

/**
 * Define o modelo Financeirocontrolesdespesas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Financeirocontrolesdespesas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "financeirocontrolesdespesas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFinanceirocontrolesdespesasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$financeirocontrolesdespesas = new Financeirocontrolesdespesas();
		return $financeirocontrolesdespesas->getFinanceirocontrolesdespesas($queries, $page, $maxpage);
	}
	
	public function getFinanceirocontrolesdespesas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");

		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " f1.idescola = $idescola ");
		
		
		$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " f1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " f1.data <= '$data_f' ");

$idtipo = (isset($queries["idtipo"])) ? $queries["idtipo"] : false;
		if ($idtipo) array_push($where, " f1.idtipo = $idtipo ");

$valor = (isset($queries["valor"])) ? $queries["valor"] : false;
		if ($valor) array_push($where, " f1.valor = $valor ");

$consumo = (isset($queries["consumo"])) ? $queries["consumo"] : false;
		if ($consumo) array_push($where, " f1.consumo LIKE '%$consumo%' ");

$numeroidentificacao = (isset($queries["numeroidentificacao"])) ? $queries["numeroidentificacao"] : false;
		if ($numeroidentificacao) array_push($where, " f2.numeroidentificacao LIKE '%$numeroidentificacao%' ");

$datavencimento_i = (isset($queries["datavencimento_i"])) ? $queries["datavencimento_i"] : false;
		if ($datavencimento_i) array_push($where, " f1.datavencimento >= '$datavencimento_i' ");

$datavencimento_f = (isset($queries["datavencimento_f"])) ? $queries["datavencimento_f"] : false;
		if ($datavencimento_f) array_push($where, " f1.datavencimento <= '$datavencimento_f' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*, f2.tipo, f2.numeroidentificacao"; 
		;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM financeirocontrolesdespesas f1
					LEFT JOIN financeirotipos f2 ON f2.id = f1.idtipo 
					
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFinanceirocontrolesdespesaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFinanceirocontrolesdespesas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFinanceirocontrolesdespesaByIdHelper($id, $queries = array()) {
		$rows = new Financeirocontrolesdespesas();
		return $rows->getFinanceirocontrolesdespesaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Financeirocontrolesdespesas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->idtipo = (array_key_exists("idtipo",$dados)) ? $dados["idtipo"] : $row->idtipo;
 $row->valor = (array_key_exists("valor",$dados)) ? $dados["valor"] : $row->valor;
 $row->consumo = (array_key_exists("consumo",$dados)) ? $dados["consumo"] : $row->consumo;
 $row->datavencimento = (array_key_exists("datavencimento",$dados)) ? $dados["datavencimento"] : $row->datavencimento;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
 $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}