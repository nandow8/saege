<?php

class Nutricaocardapiosrefeicaocalendario extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "nutricaocardapiosrefeicaocalendario";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";     
    
    public static function getNutricaocardapiorefeicaocalendarioHelper($queries = array(), $page = 0, $maxpage = 0) { 
            $nutricaocalendario = new Nutricaocardapiosrefeicaocalendario();
            return $nutricaocalendario->getEventos($queries, $page, $maxpage);
    }    
    
    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");

        if (!$usuario) return "RGM ou senha inválidos!";
        if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
        if ($usuario['senha']!=$senha) return "A senha é inválida!";
        
        $usuario = $usuario->toArray();

        if (!$ns) $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);
        
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);
        
        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';
        
        $loginNameSpace->alunousuario = serialize($usuario);

        return true;        
    }

    public function getEventos($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        if ($id) array_push($where, " n1.id = $id ");
        
        $idnutricaocardapiosrefeicao = (isset($queries['idnutricaocardapiosrefeicao'])) ? (int)$queries['idnutricaocardapiosrefeicao'] : false;
        if ($idnutricaocardapiosrefeicao) array_push($where, " n1.idnutricaocardapiosrefeicao = $idnutricaocardapiosrefeicao ");
        
        $title = (isset($queries['title'])) ? $queries['title'] : false;
        if ($title) array_push($where, " n1.title LIKE $title ");
        
        $color = (isset($queries['color'])) ? $queries['color'] : false;
        if ($color) array_push($where, " n1.color = $color ");
        
        $start = (isset($queries['start'])) ? $queries['start'] : false;
        if ($start) array_push($where, " n1.start = $start ");
        
        $end = (isset($queries['end'])) ? $queries['end'] : false;

        $total  = (isset($queries['total'])) ? $queries['total'] : false;

        if ($sorting) {
                $sorting = explode('_', $sorting);
                if (sizeof($sorting)==2) {

                        if ($sorting[0]=='nome') $sorting[0]='l1.nome';

                        $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
                }
        }		

        $w = "";
        foreach ($where as $k=>$v) {
                if ($k>0) $w .= " AND ";
                $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "n1.*"; 
        ;

        if ($total) $fields = "COUNT(n1.id) as total";

        $ordem = "ORDER BY n1.id DESC";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        $strsql = "SELECT $fields 
                                FROM nutricaocardapiosrefeicaocalendario n1

                                WHERE n1.excluido='nao' 
                                        $w 
                                $ordem	
                                $limit";	

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
        $db = Zend_Registry::get('db');				
        if ($total) {
                $row = $db->fetchRow($strsql);
                return $row['total'];
        }	

        return $db->fetchAll($strsql);	        

    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaocardapiosrefeicaocalendario
     */
    public function save($dados) {

        $novoRegistro = true;
        
        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) { 
            $row = $this->createRow();  
        }else {
        }
        
        $row->idnutricaocardapiosrefeicao = (array_key_exists('idnutricaocardapiosrefeicao',$dados)) ? $dados['idnutricaocardapiosrefeicao'] : $row->idnutricaocardapiosrefeicao;
        $row->title = (array_key_exists('title',$dados)) ? $dados['title'] : $row->title;
        $row->color = (array_key_exists('color',$dados)) ? $dados['color'] : $row->color;
        $row->start = (array_key_exists('start',$dados)) ? $dados['start'] : $row->start;
        $row->end = (array_key_exists('end',$dados)) ? $dados['end'] : $row->end;

        $row->status    = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido    = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
        
        $row->save();

        $registro = 'Adicionado';
        if(!$novoRegistro)
                $registro = 'Editado';

       Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), " id -> [" . $row->id . "] [" . $registro . "][status=>" . $row->status . "][excluido=>" . $row->excluido . "]");
       
        return $row;
    }

    public static function verificacardapiosagendados($dataInicio, $dataFim, $tipo){


        $strSql = "select * from nutricaocardapiosrefeicaocalendario as nr inner join nutricaocardapiosrefeicao as nrc on nr.idnutricaocardapiosrefeicao = nrc.id  where start = '" . $dataInicio . "' and end = '" . $dataFim . "' and nrc.tipoensino = '" . $tipo . "' and nr.excluido = 'nao' ";

        $db = Zend_Registry::get('db');				
       
        $rows = $db->fetchAll($strSql);

        if(isset($rows)){
                return $rows;	
        }else{
                return '-----';
        }
                

    }//end verificacardapiosagendados


  
}