<?php

/**
 * Define o modelo Atas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class ViewCensovagasEscolasTotais extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "view_censovagas_escolas_totais";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getViewCensovagasEscolasTotaisHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$viewCensovagasEscolasTotais = new ViewCensovagasEscolasTotais();
		return $viewCensovagasEscolasTotais->getViewCensovagasEscolasTotais($queries, $page, $maxpage);
	}
	
	public function getViewCensovagasEscolasTotais($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        if ($id) array_push($where, " a1.id = $id ");
        
        // 2	idescola	bigint(20)			Não	0	
        $idescola = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
        if ($iddepartamento) array_push($where, " a1.iddepartamento = $iddepartamento ");	
        
        // 3	escola	text	utf8_general_ci		Sim	None	
        $escola = (isset($queries["escola"])) ? $queries["escola"] : false;
        if ($escola) array_push($where, " a1.escola = $escola ");	
        
        // 4	periodo	varchar(500)	utf8_general_ci		Sim	None	label:Período|filter|grid|required	
        $periodo = (isset($queries["periodo"])) ? $queries["periodo"] : false;
        if ($periodo) array_push($where, " a1.periodo = $periodo ");

		if ($sorting){
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM view_censovagas_escolas_totais a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getViewCensovagasEscolasTotaisById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getViewCensovagasEscolasTotais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public function getViewCensovagasEscolasTotaisByIdHelper($id, $queries = array()) {
		$rows = new ViewCensovagasEscolasTotais();
		return $rows->getViewCensovagasEscolasTotaisById($id, $queries);
    }		
    
    public static function getViewCensovagasEscolasTotaisResumo($idescola){


       
        //$arrayRetorno = array();

        $strQuery = "select * from view_censovagas_escolas_totais where idescola = '$idescola' and periodo like '%Manhã%'";

        $db = Zend_Registry::get('db');	        
        
        $row = $db->fetchAll($strQuery);

        //array_push($arrayRetorno, $row);

        return $row;

        // $strQuery = "select * from view_censovagas_escolas_totais where idescola = '$idescola' and periodo like '%Tarde%'";

        // $db = Zend_Registry::get('db');	        
        
        // $row = $db->fetchAll($strQuery);	
        
        // array_push($arrayRetorno, $row);

        // $strQuery = "select * from view_censovagas_escolas_totais where idescola = '$idescola' and periodo like '%Noite%'";

        // $db = Zend_Registry::get('db');	        
        
        // $row = $db->fetchAll($strQuery);	
        
        // array_push($arrayRetorno, $row);

        // $strQuery = "select * from view_censovagas_escolas_totais where idescola = '$idescola' and periodo like '%Integral%'";

        // $db = Zend_Registry::get('db');	        
        
        // $row = $db->fetchAll($strQuery);	
        
        // array_push($arrayRetorno, $row);

        // return $arrayRetorno; 

    }// end public function 
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return ViewCensovagasEscolasTotais
     */
	public function save($dados){
		/*echo '<pre>';
		print_r($dados);
		die();*/
		
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->idperiodo = (array_key_exists("idperiodo",$dados)) ? $dados["idperiodo"] : $row->idperiodo;
 $row->idsfuncionarios = (array_key_exists("idsfuncionarios",$dados)) ? $dados["idsfuncionarios"] : $row->idsfuncionarios;
 $row->pauta = (array_key_exists("pauta",$dados)) ? $dados["pauta"] : $row->pauta;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->idarquivoata = (array_key_exists("idarquivoata",$dados)) ? $dados["idarquivoata"] : $row->idarquivoata;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}