<?php

/**
 * Define o modelo Coordenacaofrequencias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Coordenacaofrequencias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "coordenacaofrequencias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCoordenacaofrequenciaHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$coordenacaofrequencias = new Coordenacaofrequencias();
		return $coordenacaofrequencias->getCoordenacaofrequencia($queries, $page, $maxpage);
	}
	
	public function getCoordenacaofrequencia($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");


		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");
		$nomeatividade = (isset($queries["nomeatividade"])) ? $queries["nomeatividade"] : false;
		if ($nomeatividade) array_push($where, " f1.nomeatividade LIKE '%$nomeatividade%' ");
		$nometurma = (isset($queries["nometurma"])) ? $queries["nometurma"] : false;
		if ($nometurma) array_push($where, " f1.nometurma LIKE '%$nometurma%' ");
		// $datainicio = (isset($queries["datainicio"])) ? $queries["datainicio"] : false;
		// if ($datainicio) array_push($where, " f1.datainicio = $datainicio ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='f1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM coordenacaofrequencias f1
					
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getCoordenacaofrequenciaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCoordenacaofrequencia($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCoordenacaofrequenciaByIdHelper($id, $queries = array()) {
		$rows = new Coordenacaofrequencias();
		return $rows->getCoordenacaofrequenciaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Coordenacaofrequencias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		
		 $row->nomeatividade = (array_key_exists("nomeatividade",$dados)) ? $dados["nomeatividade"] : $row->nomeatividade;
		 $row->ano = (array_key_exists("ano",$dados)) ? $dados["ano"] : $row->ano;
		 $row->nometurma = (array_key_exists("nometurma",$dados)) ? $dados["nometurma"] : $row->nometurma;
		 $row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
		 $row->datatermino = (array_key_exists("datatermino",$dados)) ? $dados["datatermino"] : $row->datatermino;
		 $row->horarioinicio = (array_key_exists("horarioinicio",$dados)) ? $dados["horarioinicio"] : $row->horarioinicio;
		 $row->horariofinal = (array_key_exists("horariofinal",$dados)) ? $dados["horariofinal"] : $row->horariofinal;
		 $row->diassemana = (array_key_exists("diassemana",$dados)) ? $dados["diassemana"] : $row->diassemana;
 
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}