<?php

/**
 * Define o modelo Protocolosdocumentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Protocolosdocumentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "protocolosdocumentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getProtocolosdocumentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$protocolosdocumentos = new Protocolosdocumentos();
		return $protocolosdocumentos->getProtocolosdocumentos($queries, $page, $maxpage);
	}
	
	public function getProtocolosdocumentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " p1.sequencial LIKE '%$sequencial%' ");

$datalancamento = (isset($queries["datalancamento"])) ? $queries["datalancamento"] : false;
		if ($datalancamento) array_push($where, " p1.datalancamento = '$datalancamento' ");

$horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
		if ($horalancamento) array_push($where, " p1.horalancamento = '$horalancamento' ");

$idtipoprotocolo = (isset($queries["idtipoprotocolo"])) ? $queries["idtipoprotocolo"] : false;
		if ($idtipoprotocolo) array_push($where, " p1.idtipoprotocolo = $idtipoprotocolo ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " p1.tipo LIKE '%$tipo%' ");

$idusuariologado = (isset($queries["idusuariologado"])) ? $queries["idusuariologado"] : false;
		if ($idusuariologado) array_push($where, " p1.idusuariologado = $idusuariologado ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " p1.origem LIKE '%$origem%' ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " p1.idescola = $idescola ");

$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " p1.iddepartamento = $iddepartamento ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " p1.titulo LIKE '%$titulo%' ");

$tiposolicitacao = (isset($queries["tiposolicitacao"])) ? $queries["tiposolicitacao"] : false;
		if ($tiposolicitacao) array_push($where, " p1.tiposolicitacao LIKE '%$tiposolicitacao%' ");

$solicitacao = (isset($queries["solicitacao"])) ? $queries["solicitacao"] : false;
		if ($solicitacao) array_push($where, " p1.solicitacao LIKE '%$solicitacao%' ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " p1.observacoes = '$observacoes' ");

$encaminhar = (isset($queries["encaminhar"])) ? $queries["encaminhar"] : false;
		if ($encaminhar) array_push($where, " p1.encaminhar = $encaminhar ");

$posicao = (isset($queries["posicao"])) ? $queries["posicao"] : false;
		if ($posicao) array_push($where, " p1.posicao LIKE '%$posicao%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM protocolosdocumentos p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getProtocolosdocumentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getProtocolosdocumentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getProtocolosdocumentoByIdHelper($id, $queries = array()) {
		$rows = new Protocolosdocumentos();
		return $rows->getProtocolosdocumentoById($id, $queries);
	}		
	
	public function getUltimoProtocolodocumento($queries = array()) {
		$queries['order'] = 'ORDER BY p1.id DESC';
		$rows = $this->getProtocolosdocumentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Protocolosdocumentos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
 $row->horalancamento = (array_key_exists("horalancamento",$dados)) ? $dados["horalancamento"] : $row->horalancamento;
 $row->idtipoprotocolo = (array_key_exists("idtipoprotocolo",$dados)) ? $dados["idtipoprotocolo"] : $row->idtipoprotocolo;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->tiposolicitacao = (array_key_exists("tiposolicitacao",$dados)) ? $dados["tiposolicitacao"] : $row->tiposolicitacao;
 $row->solicitacao = (array_key_exists("solicitacao",$dados)) ? $dados["solicitacao"] : $row->solicitacao;
 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
 $row->encaminhar = (array_key_exists("encaminhar",$dados)) ? $dados["encaminhar"] : $row->encaminhar;
 $row->posicao = (array_key_exists("posicao",$dados)) ? $dados["posicao"] : $row->posicao;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}