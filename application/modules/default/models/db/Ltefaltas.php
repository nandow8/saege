<?php

class Ltefaltas extends Zend_Db_Table_Abstract {
    
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "ltefaltas";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";     
    
    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");
            
        if (!$usuario) return "RGM ou senha inválidos!";
        if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
        if ($usuario['senha']!=$senha) return "A senha é inválida!";
        
        $usuario = $usuario->toArray();

        if (!$ns) $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);
        
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);
        
        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';
        
        $loginNameSpace->alunousuario = serialize($usuario);
                        
        return true;        
    }   
    
    public static function getUsuario($field = null, $ns = false) {
        $ns = (!$ns) ? Mn_Util::getAdminNameSpace() : $ns;
        
        $session = new Zend_Session_Namespace($ns);
        if (!isset($session->alunousuario)) return false;
        
        $escola = unserialize($session->alunousuario);
        if (is_null($field)) return $escola;
        
        return $escola[$field];     
    }   
    
    public function getFaltas($queries = array(), $page = 0, $maxpage = 0) {

        $id     = (isset($queries['id'])) ? (int)$queries['id'] : false;

        $total  = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order  = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id)         array_push($where, " lf.id = $id ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lf.*, lp.datainicio, lp.datafim, lp.diasletivos, ea.nomerazao, ea.passeescolar";

        $ordem = "ORDER BY lp.id";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(lf.id) as total 
                        FROM ltefaltas lf 
                            INNER JOIN lteperiodos lp ON (lf.idperiodo=lp.id) 
                                INNER JOIN escolasalunos ea on (lf.idestudante=ea.id)
                                    WHERE lf.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields
                        FROM ltefaltas lf 
                            INNER JOIN lteperiodos lp ON (lf.idperiodo=lp.id) 
                                INNER JOIN escolasalunos ea on (lf.idestudante=ea.id)
                                    WHERE lf.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();
        
        return $db->fetchAll($strsql);      
    }   
    
    public function getFaltasById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getFaltas($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }
    
    public static function getFaltasByIdHelper($id, $queries = array()) {
        $rows = new Ltefaltas();
        return $rows->getFaltasById($id, $queries);
    }       
    
    public static function getBairrosHelper($queries = array()) {
        $rows = new Ltefaltas();
        return $rows->fetchAll();;
    }    

    public static function getNomeByIdHelper($id, $queries = array()) {
        $rows = new Ltefaltas();
        $usuario = $rows->getFaltasById($id, $queries);
        if(!$usuario) return false;
        return $usuario['nome'];
    }   
    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ltefalta
     */
    public function save($dados) {

        $novoRegistro = true;
        
        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) { 
            $row = $this->createRow();  
        }else {
            /*
            $novoRegistro = false;
            $historico = new Escolashistoricos_Escolasalunos();         
            $_historico = $row->toArray();
            
            $historico->arquiva($_historico);
            */
        }

        $row->idestudante = (array_key_exists('idestudante',$dados)) ? $dados['idestudante'] : $row->idestudante;
        $row->idperiodo   = (array_key_exists('idperiodo',$dados)) ? $dados['idperiodo'] : $row->idperiodo;
        $row->faltas      = (array_key_exists('faltas',$dados)) ? $dados['faltas'] : $row->faltas;
        $row->observacoes = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;
        $row->status      = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido    = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');    
        }

        $row->save();
        
        if($id===0){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::incluir", Usuarios::getUsuario("id"), "Falta adicionada com sucesso!");
        }elseif($id!== 0 AND isset($row->excluido) AND $row->excluido==="sim"){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::excluir", Usuarios::getUsuario("id"), "Falta de ID ".$id." excluída com sucesso!");
        } else Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::atualizar", Usuarios::getUsuario("id"), "Falta de ID ".$id." atualizada com sucesso!");
        
        return $row;
    }
    
    public function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }   
}
