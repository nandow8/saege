<?php

class Ltecursos extends Zend_Db_Table_Abstract {
    
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "ltecursos";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";     
    
    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");
            
        if (!$usuario) return "RGM ou senha inválidos!";
        if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
        if ($usuario['senha']!=$senha) return "A senha é inválida!";
        
        $usuario = $usuario->toArray();

        if (!$ns) $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);
        
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);
        
        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';
        
        $loginNameSpace->alunousuario = serialize($usuario);
                        
        return true;        
    }   
    
    public static function getUsuario($field = null, $ns = false) {
        $ns = (!$ns) ? Mn_Util::getAdminNameSpace() : $ns;
        
        $session = new Zend_Session_Namespace($ns);
        if (!isset($session->alunousuario)) return false;
        
        $escola = unserialize($session->alunousuario);
        if (is_null($field)) return $escola;
        
        return $escola[$field];     
    }   
    
    public function getCursos($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $curso = (isset($queries['curso'])) ? $queries['curso'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " lc.id = $id ");
        if ($curso) array_push($where, " lc.curso LIKE '%$curso%' ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lc.*";

        $ordem = "ORDER BY id";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(id) as total FROM ltecursos lc WHERE lc.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM ltecursos lc WHERE lc.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        return $db->fetchAll($strsql);      
    }   
    
    public function getcursoById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getcursos($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }
    
    public static function getcursoByIdHelper($id, $queries = array()) {
        $rows = new Ltecursos();
        return $rows->getcursoById($id, $queries);
    }       
    
    public static function getCursosHelper($queries = array()) {
        $rows = new Ltecursos();
        return $rows->fetchAll();;
    }    
 
    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ltecurso
     */
    public function save($dados) {

        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) { 
            $row = $this->createRow();  
        }else {}

        $row->curso = (array_key_exists('curso',$dados)) ? $dados['curso'] : $row->curso;
        $row->observacoes = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;
        $row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;

        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');
            $row->usuariocriacao = Usuarios::getUsuario('id');
        }
        
        $row->save();
        
        if($id===0){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::incluir", Usuarios::getUsuario("id"), "Curso adicionado com sucesso!");
        }elseif($id!== 0 AND isset($row->excluido) AND $row->excluido==="sim"){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::excluir", Usuarios::getUsuario("id"), "Curso de ID ".$id." excluído com sucesso!");
        } else Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::atualizar", Usuarios::getUsuario("id"), "Curso de ID ".$id." atualizado com sucesso!");
        
        return $row;
    }
    
    public function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }   
}
