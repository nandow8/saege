<?php

/**
 * Define o modelo Logisticasabastecimentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Logisticasabastecimentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "logisticasabastecimentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLogisticasabastecimentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$logisticasabastecimentos = new Logisticasabastecimentos();
		return $logisticasabastecimentos->getLogisticasabastecimentos($queries, $page, $maxpage);
	}
	
	public function getLogisticasabastecimentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " l1.sequencial LIKE '%$sequencial%' ");

$datalancamento = (isset($queries["datalancamento"])) ? $queries["datalancamento"] : false;
		if ($datalancamento) array_push($where, " l1.datalancamento = '$datalancamento' ");

$horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
		if ($horalancamento) array_push($where, " l1.horalancamento = '$horalancamento' ");

$idfrota = (isset($queries["idfrota"])) ? $queries["idfrota"] : false;
		if ($idfrota) array_push($where, " l1.idfrota = $idfrota ");

$tipocombustivel = (isset($queries["tipocombustivel"])) ? $queries["tipocombustivel"] : false;
		if ($tipocombustivel) array_push($where, " l1.tipocombustivel LIKE '%$tipocombustivel%' ");

$quantidade = (isset($queries["quantidade"])) ? $queries["quantidade"] : false;
		if ($quantidade) array_push($where, " l1.quantidade = $quantidade ");

$kmatual = (isset($queries["kmatual"])) ? $queries["kmatual"] : false;
		if ($kmatual) array_push($where, " l1.kmatual = $kmatual ");

$idfornecedor = (isset($queries["idfornecedor"])) ? $queries["idfornecedor"] : false;
		if ($idfornecedor) array_push($where, " l1.idfornecedor = $idfornecedor ");

$idresponsavel = (isset($queries["idresponsavel"])) ? $queries["idresponsavel"] : false;
		if ($idresponsavel) array_push($where, " l1.idresponsavel = $idresponsavel ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*"; 
		;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM logisticasabastecimentos l1
					
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLogisticasabastecimentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLogisticasabastecimentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLogisticasabastecimentoByIdHelper($id, $queries = array()) {
		$rows = new Logisticasabastecimentos();
		return $rows->getLogisticasabastecimentoById($id, $queries);
	}		
	
	public function getUltimoLogisticasabastecimento($queries = array()) {
		$queries['order'] = 'ORDER BY l1.id DESC';
		$rows = $this->getLogisticasabastecimentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Logisticasabastecimentos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
 $row->horalancamento = (array_key_exists("horalancamento",$dados)) ? $dados["horalancamento"] : $row->horalancamento;
 $row->idfrota = (array_key_exists("idfrota",$dados)) ? $dados["idfrota"] : $row->idfrota;
 $row->tipocombustivel = (array_key_exists("tipocombustivel",$dados)) ? $dados["tipocombustivel"] : $row->tipocombustivel;
 $row->quantidade = (array_key_exists("quantidade",$dados)) ? $dados["quantidade"] : $row->quantidade;
 $row->kmatual = (array_key_exists("kmatual",$dados)) ? $dados["kmatual"] : $row->kmatual;
 $row->idfornecedor = (array_key_exists("idfornecedor",$dados)) ? $dados["idfornecedor"] : $row->idfornecedor;
 $row->idresponsavel = (array_key_exists("idresponsavel",$dados)) ? $dados["idresponsavel"] : $row->idresponsavel;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}