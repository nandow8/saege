<?php

/**
 * Define o modelo Logisticamultas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Logisticamultas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "logisticamultas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLogisticamultasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$logisticamultas = new Logisticamultas();
		return $logisticamultas->getLogisticamultas($queries, $page, $maxpage);
	}
	
	public function getLogisticamultas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		
		$idmotorista = (isset($queries["idmotorista"])) ? $queries["idmotorista"] : false;
		if ($idmotorista) array_push($where, " l1.idmotorista = $idmotorista ");

$idfrota = (isset($queries["idfrota"])) ? $queries["idfrota"] : false;
		if ($idfrota) array_push($where, " l1.idfrota = $idfrota ");

$idtipomulta = (isset($queries["idtipomulta"])) ? $queries["idtipomulta"] : false;
		if ($idtipomulta) array_push($where, " l1.idtipomulta = $idtipomulta ");

$ocorrenciamulta = (isset($queries["ocorrenciamulta"])) ? $queries["ocorrenciamulta"] : false;
		if ($ocorrenciamulta) array_push($where, " l1.ocorrenciamulta LIKE '%$ocorrenciamulta%' ");

$pontuacaomulta = (isset($queries["pontuacaomulta"])) ? $queries["pontuacaomulta"] : false;
		if ($pontuacaomulta) array_push($where, " l1.pontuacaomulta = '$pontuacaomulta' ");

$datamulta_i = (isset($queries["datamulta_i"])) ? $queries["datamulta_i"] : false;
		if ($datamulta_i) array_push($where, " l1.datamulta >= '$datamulta_i' ");

$datamulta_f = (isset($queries["datamulta_f"])) ? $queries["datamulta_f"] : false;
		if ($datamulta_f) array_push($where, " l1.datamulta <= '$datamulta_f' ");

$horamulta = (isset($queries["horamulta"])) ? $queries["horamulta"] : false;
		if ($horamulta) array_push($where, " l1.horamulta = '$horamulta' ");

$datavencimentomulta_i = (isset($queries["datavencimentomulta_i"])) ? $queries["datavencimentomulta_i"] : false;
		if ($datavencimentomulta_i) array_push($where, " l1.datavencimentomulta >= '$datavencimentomulta_i' ");

$datavencimentomulta_f = (isset($queries["datavencimentomulta_f"])) ? $queries["datavencimentomulta_f"] : false;
		if ($datavencimentomulta_f) array_push($where, " l1.datavencimentomulta <= '$datavencimentomulta_f' ");

$valormulta = (isset($queries["valormulta"])) ? $queries["valormulta"] : false;
		if ($valormulta) array_push($where, " l1.valormulta = $valormulta ");

$pago = (isset($queries["pago"])) ? $queries["pago"] : false;
		if ($pago) array_push($where, " l1.pago LIKE '%$pago%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*"; 
		;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM logisticamultas l1
					
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLogisticamultaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLogisticamultas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLogisticamultaByIdHelper($id, $queries = array()) {
		$rows = new Logisticamultas();
		return $rows->getLogisticamultaById($id, $queries);
	}		
	
	public function getUltimoLogisticamulta($queries = array()) {
		$queries['order'] = 'ORDER BY l1.id DESC';
		$rows = $this->getLogisticamultas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Logisticamultas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
 $row->idmotorista = (array_key_exists("idmotorista",$dados)) ? $dados["idmotorista"] : $row->idmotorista;
 $row->idfrota = (array_key_exists("idfrota",$dados)) ? $dados["idfrota"] : $row->idfrota;
 $row->idtipomulta = (array_key_exists("idtipomulta",$dados)) ? $dados["idtipomulta"] : $row->idtipomulta;
 $row->idcidade = (array_key_exists("idcidade",$dados)) ? $dados["idcidade"] : $row->idcidade;
 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->ocorrenciamulta = (array_key_exists("ocorrenciamulta",$dados)) ? $dados["ocorrenciamulta"] : $row->ocorrenciamulta;
 $row->pontuacaomulta = (array_key_exists("pontuacaomulta",$dados)) ? $dados["pontuacaomulta"] : $row->pontuacaomulta;
 $row->motivomulta = (array_key_exists("motivomulta",$dados)) ? $dados["motivomulta"] : $row->motivomulta;
 $row->numeroait = (array_key_exists("numeroait",$dados)) ? $dados["numeroait"] : $row->numeroait;
 $row->localmulta = (array_key_exists("localmulta",$dados)) ? $dados["localmulta"] : $row->localmulta;
 $row->datamulta = (array_key_exists("datamulta",$dados)) ? $dados["datamulta"] : $row->datamulta;
 $row->horamulta = (array_key_exists("horamulta",$dados)) ? $dados["horamulta"] : $row->horamulta;
 $row->orgaomulta = (array_key_exists("orgaomulta",$dados)) ? $dados["orgaomulta"] : $row->orgaomulta;
 $row->enquadramento = (array_key_exists("enquadramento",$dados)) ? $dados["enquadramento"] : $row->enquadramento;
 $row->datavencimentomulta = (array_key_exists("datavencimentomulta",$dados)) ? $dados["datavencimentomulta"] : $row->datavencimentomulta;
 $row->valormulta = (array_key_exists("valormulta",$dados)) ? $dados["valormulta"] : $row->valormulta;
 $row->pago = (array_key_exists("pago",$dados)) ? $dados["pago"] : $row->pago;
 if (is_null($row->datacriacao)) {
	 $row->datacriacao = date("Y-m-d H:i:s");
	}
$row->idmotivoinfracao = (array_key_exists("idmotivoinfracao",$dados)) ? $dados["idmotivoinfracao"] : $row->idmotivoinfracao;
	
	$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
	$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
	$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
	$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
	
	
	$row->save();
	
	return $row;
}

}