<?php

/**
 * Define o modelo Escolasprogramascontas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasprogramascontas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasprogramascontas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolasprogramascontasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasprogramascontas = new Escolasprogramascontas();
		return $escolasprogramascontas->getEscolasprogramascontas($queries, $page, $maxpage);
	}
	
	public function getEscolasprogramascontas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");

		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " e2.idescola = $idescola ");
		

		$idparent = (isset($queries['idparent'])) ? (int)$queries['idparent'] : false;
		if ($idparent) array_push($where, " e1.idparent = $idparent ");
		
		

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasprogramascontas e1
					LEFT JOIN escolasprogramasgov e2 ON e1.idparent = e2.id 
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolaprogramacontaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasprogramascontas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaprogramacontaByIdHelper($id, $queries = array()) {
		$rows = new Escolasprogramascontas();
		return $rows->getEscolaprogramacontaById($id, $queries);
	}		
	
	public function getEscolaprogramacontaByIdParent($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idparent'] = $id;
		$rows = $this->getEscolasprogramascontas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaprogramacontaByIdParentHelper($id, $queries = array()) {
		$rows = new Escolasprogramascontas();
		return $rows->getEscolaprogramacontaByIdParent($id, $queries);
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasprogramascontas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idparent = (array_key_exists("idparent",$dados)) ? $dados["idparent"] : $row->idparent;
 $row->banco = (array_key_exists("banco",$dados)) ? $dados["banco"] : $row->banco;
 $row->agencia = (array_key_exists("agencia",$dados)) ? $dados["agencia"] : $row->agencia;
 $row->conta = (array_key_exists("conta",$dados)) ? $dados["conta"] : $row->conta;
 $row->digito = (array_key_exists("digito",$dados)) ? $dados["digito"] : $row->digito;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		
		//return true;		
		$row->save();
		
		return $row;
	}
	
}