<?php

/**
 * Define o modelo Tiequipamentosmanutencoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Tiequipamentosmanutencoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "tiequipamentosmanutencoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getTiequipamentosmanutencoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$tiequipamentosmanutencoes = new Tiequipamentosmanutencoes();
		return $tiequipamentosmanutencoes->getTiequipamentosmanutencoes($queries, $page, $maxpage);
	}
	
	public function getTiequipamentosmanutencoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " t1.id = $id ");
		
		
		$idfornecedor = (isset($queries["idfornecedor"])) ? $queries["idfornecedor"] : false;
		if ($idfornecedor) array_push($where, " t1.idfornecedor = $idfornecedor ");

		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		if ($idsecretaria) array_push($where, " p1.idsecretaria=$idsecretaria ");

		$modulo = (isset($queries["modulo"])) ? $queries["modulo"] : false;
		if ($modulo) array_push($where, " p1.modulo LIKE '%$modulo%' ");

$idchamado = (isset($queries["idchamado"])) ? $queries["idchamado"] : false;
		if ($idchamado) array_push($where, " t1.idchamado = $idchamado ");

$previsao_i = (isset($queries["previsao_i"])) ? $queries["previsao_i"] : false;
		if ($previsao_i) array_push($where, " t1.previsao >= '$previsao_i' ");

$previsao_f = (isset($queries["previsao_f"])) ? $queries["previsao_f"] : false;
		if ($previsao_f) array_push($where, " t1.previsao <= '$previsao_f' ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " t1.observacoes = '$observacoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " t1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "t1.*"; 
		;
		
		if ($total) $fields = "COUNT(t1.id) as total";
		
		$ordem = "ORDER BY t1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM tiequipamentosmanutencoes t1
						LEFT JOIN protocolos p1 ON p1.id = t1.idchamado
					WHERE t1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getTiequipamentomanutencaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTiequipamentosmanutencoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTiequipamentomanutencaoByIdHelper($id, $queries = array()) {
		$rows = new Tiequipamentosmanutencoes();
		return $rows->getTiequipamentomanutencaoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Tiequipamentosmanutencoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfornecedor = (array_key_exists("idfornecedor",$dados)) ? $dados["idfornecedor"] : $row->idfornecedor;
 $row->idchamado = (array_key_exists("idchamado",$dados)) ? $dados["idchamado"] : $row->idchamado;
 $row->previsao = (array_key_exists("previsao",$dados)) ? $dados["previsao"] : $row->previsao;
 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();

		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Tiequipamentosmanutencoes fornecedor $row->idfornecedor chamado $row->idchamado  salvo com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Tiequipamentosmanutencoes fornecedor $row->idfornecedor chamado $row->idchamado  com o ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Tiequipamentosmanutencoes fornecedor $row->idfornecedor chamado $row->idchamado  com o ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Tiequipamentosmanutencoes fornecedor $row->idfornecedor chamado $row->idchamado  com o ID ".$id." atualizada com sucesso!");
		
		return $row;
	}
	
}