<?php

class Lteentidades extends Zend_Db_Table_Abstract {
    
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "lteentidades";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";     
    
    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");
            
        if (!$usuario) return "RGM ou senha inválidos!";
        if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
        if ($usuario['senha']!=$senha) return "A senha é inválida!";
        
        $usuario = $usuario->toArray();

        if (!$ns) $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);
        
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);
        
        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';
        
        $loginNameSpace->alunousuario = serialize($usuario);
                        
        return true;        
    }   
    
    public static function getUsuario($field = null, $ns = false) {
        $ns = (!$ns) ? Mn_Util::getAdminNameSpace() : $ns;
        
        $session = new Zend_Session_Namespace($ns);
        if (!isset($session->alunousuario)) return false;
        
        $escola = unserialize($session->alunousuario);
        if (is_null($field)) return $escola;
        
        return $escola[$field];     
    }   
    
    public function getEntidades($queries = array(), $page = 0, $maxpage = 0) {

        $id       = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $entidade = (isset($queries['entidade'])) ? $queries['entidade'] : false;
        $telefone = (isset($queries['telefone'])) ? $queries['telefone'] : false;
        $email    = (isset($queries['email'])) ? $queries['email'] : false;
        $tipo     = (isset($queries['tipo'])) ? $queries['tipo'] : false;
        $ntipo    = (isset($queries['ntipo'])) ? $queries['ntipo'] : false;

        $status   = (isset($queries['status'])) ? $queries['status'] : false;
        $excluido = (isset($queries['excluido'])) ? $queries['excluido'] : false;
        $total    = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order    = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id)       array_push($where, " le.id = $id ");
        if ($entidade) array_push($where, " le.entidade LIKE '%$entidade%' ");
        if ($telefone) array_push($where, " le.telefone LIKE '%$telefone%' ");
        if ($email)    array_push($where, " le.email LIKE '%$email%' ");
        if ($tipo)     array_push($where, " le.tipo = '$tipo' ");
        if ($ntipo)    array_push($where, " le.tipo != '$ntipo' ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "le.*, vch.cidade, round(vch.valorfretado,2) valorfretado, vch.subsidio";

        $ordem = "ORDER BY entidade";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(le.id) as total FROM lteentidades le LEFT JOIN view_cidade_historico vch ON (le.idcidade = vch.idcidade) WHERE le.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM lteentidades le LEFT JOIN view_cidade_historico vch ON (le.idcidade = vch.idcidade) WHERE le.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db');

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();
        
        return $db->fetchAll($strsql);      
    }

    function gravar($texto){
        $arquivo = "meu_arquivo.txt";     
        $fp = fopen($arquivo, "a+");
        fwrite($fp, $texto);
        fclose($fp);
    }

    public static function getEntidadesHelper() {
        $rows = new lteentidades();
        return $rows->fetchAll();
    }

    public function getEntidadeById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getEntidades($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }

    public function getEntidadesByTipo($tipo = false, $queries = array()) {
        if (!$tipo) return false;
        
        $queries['tipo'] = $tipo;
        $entidades = new Lteentidades();
        $rows = $entidades->getEntidades($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows;
    }
    
    public static function getEntidadeByIdHelper($id, $queries = array()) {
        $rows = new Lteentidades();
        return $rows->getEntidadeById($id, $queries);
    }       
    
    public static function getNomeByIdHelper($id, $queries = array()) {
        $rows = new Lteentidades();
        $usuario = $rows->getEntidadeById($id, $queries);
        if(!$usuario) return false;
        return $usuario['nome'];
    }   
    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Lteentidade
     */
    public function save($dados) {
        $novoRegistro = true;
        $integracao = false;
        
        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) { 
            $row = $this->createRow();  
        }else {
            /*
            $novoRegistro = false;
            $historico = new Escolashistoricos_Escolasalunos();         
            $_historico = $row->toArray();
            
            $historico->arquiva($_historico);
            */
        }

        $row->entidade    = (array_key_exists('entidade',$dados)) ? $dados['entidade'] : $row->entidade;
        $row->status      = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->email       = (array_key_exists('email',$dados)) ? $dados['email'] : $row->email;
        $row->telefone    = (array_key_exists('telefone',$dados)) ? $dados['telefone'] : $row->telefone;
        $row->cep         = (array_key_exists('cep',$dados)) ? $dados['cep'] : $row->cep;
        $row->endereco    = (array_key_exists('endereco',$dados)) ? $dados['endereco'] : $row->endereco;
        $row->numero      = (array_key_exists('numero',$dados)) ? $dados['numero'] : $row->numero;
        $row->complemento = (array_key_exists('complemento',$dados)) ? $dados['complemento'] : $row->complemento;
        $row->idbairro    = (array_key_exists('idbairro',$dados)) ? $dados['idbairro'] : $row->idbairro;
        $row->idestado    = (array_key_exists('idestado',$dados)) ? $dados['idestado'] : $row->idestado;
        $row->idcidade    = (array_key_exists('idcidade',$dados)) ? $dados['idcidade'] : $row->idcidade;
        $row->tipo        = (array_key_exists('tipo',$dados)) ? $dados['tipo'] : $row->tipo;
        $row->excluido    = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
        $row->observacoes = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;
        $row->latitude    = (isset($dados['latitude'])) ? $dados['latitude'] : $row->latitude;
        $row->longitude   = (isset($dados['longitude'])) ? $dados['longitude'] : $row->longitude;
        
        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');    
        }

        $row->save();
        
        if($id===0){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::incluir", Usuarios::getUsuario("id"), "Entidade adicionada com sucesso!");
        }elseif($id!== 0 AND isset($row->excluido) AND $row->excluido==="sim"){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::excluir", Usuarios::getUsuario("id"), "Entidade de ID ".$id." excluída com sucesso!");
        } else Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::atualizar", Usuarios::getUsuario("id"), "Entidade de ID ".$id." atualizada com sucesso!");
        
        return $row;
    }
    
    public function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }   
}
