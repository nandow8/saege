<?php

class Escolasimagens extends Zend_Db_Table_Abstract {

	/**
	 * Define o nome da tabela
	 * @var string
	 */
	protected $_name = "escolasimagens";
	
	/**
	 * Define a chave primaria
	 * @var integer
	 */
	protected $_primary = "id";
	
	
	public static function getImagemByIdEmpresa($idescola, $order = 'fi1.ordem') {
		$strsql = "SELECT fi1.idimagem, fi1.idescola, fi1.legenda, i1.* 
					FROM escolasimagens fi1 
						LEFT JOIN imagens i1 ON i1.id=fi1.idimagem
					WHERE fi1.excluido='nao' 
						AND fi1.idescola=$idescola 
					ORDER BY $order"; 
						
					
		$db = Zend_Registry::get('db');
		$rows = $db->fetchAll($strsql);
		if (sizeof($rows)==0) return 0;
		//var_dump($rows[0]['idimagem']); die();
		$idimagem = $rows[0]['idimagem'];
		return $idimagem;
	}
	
	public static function getImagensByIdPost($idescola, $order = 'fi1.ordem') {
		
		$strsql = "SELECT fi1.idimagem, fi1.idescola, fi1.legenda, i1.* 
					FROM escolasimagens fi1 
						LEFT JOIN imagens i1 ON i1.id=fi1.idimagem
					WHERE fi1.excluido='nao' 
						AND fi1.idescola=$idescola 
					ORDER BY $order"; 
						
					
		$db = Zend_Registry::get('db');
		return $db->fetchAll($strsql);
	}
	
	public function setImagens($idescola, $imagens, $legendas) {
		//if (!is_array($imagens)) return;
		
		$logdata = date('Y-m-d G:i:s');
		foreach ($imagens as $i=>$idimagem) {
			$d = array();
			
			$legenda = $legendas[$i];
			
			$d['idescola'] = $idescola;
			$d['idimagem'] = $idimagem;
			$d['legenda'] = $legenda;
			$d['ordem'] = $i;
			$d['excluido'] = 'nao';
			$d['logusuario'] = Usuarios::getUsuario('id');
			$d['logdata'] = $logdata;
			$this->save($d);
		}
		
		$ids = implode(",", $imagens);
		if ($ids=="") $ids = "0";
		$strsql = "DELETE FROM escolasimagens WHERE idescola=$idescola AND idimagem NOT IN ($ids)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);		
	}	

	public static function getEscolasImagens($idescola = NULL, $status = NULL, $inverte = false, $limite = NULL) {
		$db = Zend_Registry::get('db');
		
		$w = "";
		if (!is_null($idescola)) $w .= " AND fi1.idescola = " . (int)$idescola;
		if (!is_null($status)) $w .= " AND fi1.status = '$status'"; 
		
		$l = "";
		if (!is_null($limite)) $l .= " LIMIT '$limite'";
		
		$order = "ORDER BY fi1.ordem";
		if ($inverte) $order .= " DESC";
		
		$strsql = "SELECT fi1.* 
					FROM escolasimagens fi1 
					WHERE fi1.excluido = 'nao' 
					$w
					$order
					$l";
					
		//die($strsql);					
		return $db->fetchAll($strsql);
	}
	
	/**
	 * Salva o dados (INSERT OU UPDATE)
	 * @param array dados
	 * @return PropostasFacs
	 */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$idescola = (int)$dados['idescola'];
		$idimagem = (int)$dados['idimagem'];
		
		$row = $this->fetchRow("idescola=" . $idescola . " AND idimagem=" . $idimagem);		
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
				
		if ($id>0)$row->id = $id;
		
		$row->idescola = (isset($dados['idescola'])) ? $dados['idescola'] : $row->idescola;
		$row->idimagem = (isset($dados['idimagem'])) ? $dados['idimagem'] : $row->idimagem;
		$row->legenda = (isset($dados['legenda'])) ? $dados['legenda'] : $row->legenda;
		$row->ordem = (isset($dados['ordem'])) ? $dados['ordem'] : $row->ordem;
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (isset($dados['logusuario'])) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (isset($dados['logdata'])) ? $dados['logdata'] : $row->logdata;
		$row->save();
		
		return $row;
	}
	
}