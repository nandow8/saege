<?php

/**
 * Define o modelo Secretariasdeclaracoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Secretariasdeclaracoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "secretariasdeclaracoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getSecretariasdeclaracoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$secretariasdeclaracoes = new Secretariasdeclaracoes();
		return $secretariasdeclaracoes->getSecretariasdeclaracoes($queries, $page, $maxpage);
	}
	
	public function getSecretariasdeclaracoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " s1.id = $id ");
		
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " s1.idescola = $idescola ");
		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " s1.sequencial LIKE '%$sequencial%' ");

$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " s1.datalancamento >= '$datalancamento_i' ");

$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " s1.datalancamento <= '$datalancamento_f' ");

$horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
		if ($horalancamento) array_push($where, " s1.horalancamento = '$horalancamento' ");

$idusuariologado = (isset($queries["idusuariologado"])) ? $queries["idusuariologado"] : false;
		if ($idusuariologado) array_push($where, " s1.idusuariologado = $idusuariologado ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " s1.idescola = $idescola ");

$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : 1;
		if ($idsecretaria) array_push($where, " s1.idsecretaria = $idsecretaria ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " s1.tipo = '$tipo' ");

$serie = (isset($queries["serie"])) ? $queries["serie"] : false;
		if ($serie) array_push($where, " s1.serie LIKE '%$serie%' ");

$horainicio = (isset($queries["horainicio"])) ? $queries["horainicio"] : false;
		if ($horainicio) array_push($where, " s1.horainicio = '$horainicio' ");

$horafim = (isset($queries["horafim"])) ? $queries["horafim"] : false;
		if ($horafim) array_push($where, " s1.horafim = '$horafim' ");

$ano = (isset($queries["ano"])) ? $queries["ano"] : false;
		if ($ano) array_push($where, " s1.ano = '$ano' ");

$considerado = (isset($queries["considerado"])) ? $queries["considerado"] : false;
		if ($considerado) array_push($where, " s1.considerado LIKE '%$considerado%' ");

$proximoano = (isset($queries["proximoano"])) ? $queries["proximoano"] : false;
		if ($proximoano) array_push($where, " s1.proximoano LIKE '%$proximoano%' ");

$solicitacaovaga = (isset($queries["solicitacaovaga"])) ? $queries["solicitacaovaga"] : false;
		if ($solicitacaovaga) array_push($where, " s1.solicitacaovaga LIKE '%$solicitacaovaga%' ");

$validapor = (isset($queries["validapor"])) ? $queries["validapor"] : false;
		if ($validapor) array_push($where, " s1.validapor = '$validapor' ");

$vagaano = (isset($queries["vagaano"])) ? $queries["vagaano"] : false;
		if ($vagaano) array_push($where, " s1.vagaano = '$vagaano' ");

$vagadias = (isset($queries["vagadias"])) ? $queries["vagadias"] : false;
		if ($vagadias) array_push($where, " s1.vagadias = '$vagadias' ");

$anoregulamentado = (isset($queries["anoregulamentado"])) ? $queries["anoregulamentado"] : false;
		if ($anoregulamentado) array_push($where, " s1.anoregulamentado LIKE '%$anoregulamentado%' ");

$tituloregulamentado = (isset($queries["tituloregulamentado"])) ? $queries["tituloregulamentado"] : false;
		if ($tituloregulamentado) array_push($where, " s1.tituloregulamentado LIKE '%$tituloregulamentado%' ");

$proximoanoregulamentado = (isset($queries["proximoanoregulamentado"])) ? $queries["proximoanoregulamentado"] : false;
		if ($proximoanoregulamentado) array_push($where, " s1.proximoanoregulamentado = '$proximoanoregulamentado' ");

$diasregulamentado = (isset($queries["diasregulamentado"])) ? $queries["diasregulamentado"] : false;
		if ($diasregulamentado) array_push($where, " s1.diasregulamentado = '$diasregulamentado' ");

$idaluno = (isset($queries["idaluno"])) ? $queries["idaluno"] : false;
		if ($idaluno) array_push($where, " s1.idaluno = $idaluno ");

$iddiretora = (isset($queries["iddiretora"])) ? $queries["iddiretora"] : false;
		if ($iddiretora) array_push($where, " s1.iddiretora = $iddiretora ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " s1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "s1.*"; 
		;
		
		if ($total) $fields = "COUNT(s1.id) as total";
		
		$ordem = "ORDER BY s1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM secretariasdeclaracoes s1
					
					WHERE s1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getSecretariasdeclaracaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSecretariasdeclaracoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSecretariasdeclaracaoByIdHelper($id, $queries = array()) {
		$rows = new Secretariasdeclaracoes();
		return $rows->getSecretariasdeclaracaoById($id, $queries);
	}		
	

	public function getUltimoSecretariasdeclaracao($queries = array()) {
		$queries['order'] = 'ORDER BY s1.id DESC';
		$rows = $this->getSecretariasdeclaracoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Secretariasdeclaracoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 

		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
 $row->horalancamento = (array_key_exists("horalancamento",$dados)) ? $dados["horalancamento"] : $row->horalancamento;
 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->serie = (array_key_exists("serie",$dados)) ? $dados["serie"] : $row->serie;
 $row->horainicio = (array_key_exists("horainicio",$dados)) ? $dados["horainicio"] : $row->horainicio;
 $row->horafim = (array_key_exists("horafim",$dados)) ? $dados["horafim"] : $row->horafim;
 $row->ano = (array_key_exists("ano",$dados)) ? $dados["ano"] : $row->ano;
 $row->anosegundoitem = (array_key_exists("anosegundoitem",$dados)) ? $dados["anosegundoitem"] : $row->anosegundoitem;
 $row->considerado = (array_key_exists("considerado",$dados)) ? $dados["considerado"] : $row->considerado;
 $row->anoterceiroitem = (array_key_exists("anoterceiroitem",$dados)) ? $dados["anoterceiroitem"] : $row->anoterceiroitem;
 $row->proximoano = (array_key_exists("proximoano",$dados)) ? $dados["proximoano"] : $row->proximoano;
 $row->solicitacaovaga = (array_key_exists("solicitacaovaga",$dados)) ? $dados["solicitacaovaga"] : $row->solicitacaovaga;
 $row->validapor = (array_key_exists("validapor",$dados)) ? $dados["validapor"] : $row->validapor;
 $row->vagaano = (array_key_exists("vagaano",$dados)) ? $dados["vagaano"] : $row->vagaano;
 $row->vagadias = (array_key_exists("vagadias",$dados)) ? $dados["vagadias"] : $row->vagadias;
 $row->anoregulamentado = (array_key_exists("anoregulamentado",$dados)) ? $dados["anoregulamentado"] : $row->anoregulamentado;
 $row->tituloregulamentado = (array_key_exists("tituloregulamentado",$dados)) ? $dados["tituloregulamentado"] : $row->tituloregulamentado;
 $row->proximoanoregulamentado = (array_key_exists("proximoanoregulamentado",$dados)) ? $dados["proximoanoregulamentado"] : $row->proximoanoregulamentado;
 $row->diasregulamentado = (array_key_exists("diasregulamentado",$dados)) ? $dados["diasregulamentado"] : $row->diasregulamentado;
 $row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
 $row->iddiretora = (array_key_exists("iddiretora",$dados)) ? $dados["iddiretora"] : $row->iddiretora;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}