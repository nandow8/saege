<?php

/**
 * Define o modelo Chamadosprioridades
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Chamadosprioridades extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "chamadosprioridades";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getChamadosprioridadesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$chamadosprioridades = new Chamadosprioridades();
		return $chamadosprioridades->getChamadosprioridades($queries, $page, $maxpage);
	}
	
	public function getChamadosprioridades($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " c1.iddepartamento = $iddepartamento ");

$iddepartamentoescola = (isset($queries["iddepartamentoescola"])) ? $queries["iddepartamentoescola"] : false;
		if ($iddepartamentoescola) array_push($where, " c1.iddepartamentoescola = $iddepartamentoescola ");

$prioridadade = (isset($queries["prioridadade"])) ? $queries["prioridadade"] : false;
		if ($prioridadade) array_push($where, " c1.prioridadade LIKE '%$prioridadade%' ");

$departamento = (isset($queries["departamento"])) ? $queries["departamento"] : false;
		if ($departamento) array_push($where, " c1.departamento LIKE '%$departamento%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " c1.descricoes = '$descricoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM chamadosprioridades c1
					
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getChamadosprioridadeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getChamadosprioridades($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getChamadosprioridadeByIdHelper($id, $queries = array()) {
		$rows = new Chamadosprioridades();
		return $rows->getChamadosprioridadeById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Chamadosprioridades
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->iddepartamentoescola = (array_key_exists("iddepartamentoescola",$dados)) ? $dados["iddepartamentoescola"] : $row->iddepartamentoescola;
 $row->prioridadade = (array_key_exists("prioridadade",$dados)) ? $dados["prioridadade"] : $row->prioridadade;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->departamento = (array_key_exists("departamento",$dados)) ? $dados["departamento"] : $row->departamento;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Chamadosprioridades departamento $row->departamento salvo com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Chamadosprioridades departamento $row->departamento com o ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Chamadosprioridades departamento $row->departamento com o ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Chamadosprioridades departamento $row->departamento com o ID ".$id." atualizada com sucesso!");

		return $row;
	}
	
}