<?php

/**
 * Define o modelo Escolasusuariosemails
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasusuariosemails extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasusuariosemails";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolasusuariosemailsHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasusuariosemails = new Escolasusuariosemails();
		return $escolasusuariosemails->getEscolasusuariosemails($queries, $page, $maxpage);
	}
	
	public function getEscolasusuariosemails($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " e1.idsecretaria = $idsecretaria ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");

$idescolausuario = (isset($queries["idescolausuario"])) ? $queries["idescolausuario"] : false;
		if ($idescolausuario) array_push($where, " e1.idescolausuario = $idescolausuario ");

$email = (isset($queries["email"])) ? $queries["email"] : false;
		if ($email) array_push($where, " e1.email LIKE '%$email%' ");

$emailspais = (isset($queries["emailspais"])) ? $queries["emailspais"] : false;
		if ($emailspais) array_push($where, " e1.emailspais LIKE '%$emailspais%' ");

$emailsdepartamentos = (isset($queries["emailsdepartamentos"])) ? $queries["emailsdepartamentos"] : false;
		if ($emailsdepartamentos) array_push($where, " e1.emailsdepartamentos LIKE '%$emailsdepartamentos%' ");

$iddepartamentoescola = (isset($queries["iddepartamentoescola"])) ? $queries["iddepartamentoescola"] : false;
		if ($iddepartamentoescola) array_push($where, " e1.iddepartamentoescola = $iddepartamentoescola ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasusuariosemails e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolausuarioemailById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasusuariosemails($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolausuarioemailByIdHelper($id, $queries = array()) {
		$rows = new Escolasusuariosemails();
		return $rows->getEscolausuarioemailById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasusuariosemails
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idescolausuario = (array_key_exists("idescolausuario",$dados)) ? $dados["idescolausuario"] : $row->idescolausuario;
 $row->email = (array_key_exists("email",$dados)) ? $dados["email"] : $row->email;
 $row->emailspais = (array_key_exists("emailspais",$dados)) ? $dados["emailspais"] : $row->emailspais;
 $row->emailsdepartamentos = (array_key_exists("emailsdepartamentos",$dados)) ? $dados["emailsdepartamentos"] : $row->emailsdepartamentos;
 $row->iddepartamentoescola = (array_key_exists("iddepartamentoescola",$dados)) ? $dados["iddepartamentoescola"] : $row->iddepartamentoescola;

 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->idsdepartamentossecretarias = (array_key_exists("idsdepartamentossecretarias",$dados)) ? $dados["idsdepartamentossecretarias"] : $row->idsdepartamentossecretarias;
 $row->idsdepartamentosescolas = (array_key_exists("idsdepartamentosescolas",$dados)) ? $dados["idsdepartamentosescolas"] : $row->idsdepartamentosescolas;
 
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}