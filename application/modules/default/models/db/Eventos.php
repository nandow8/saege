<?php

/**
 * Define o modelo Eventos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Eventos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "eventos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEventosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$eventos = new Eventos();
		return $eventos->getEventos($queries, $page, $maxpage);
	}
	
	public function getEventos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");

$idperfil = (isset($queries["idperfil"])) ? $queries["idperfil"] : false;
		if ($idperfil) array_push($where, " e1.idperfil = $idperfil ");

$idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " e1.idusuario = $idusuario ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " e1.titulo LIKE '%$titulo%' ");

$datainicio_i = (isset($queries["datainicio_i"])) ? $queries["datainicio_i"] : false;
		if ($datainicio_i) array_push($where, " e1.datainicio >= '$datainicio_i' ");

$datainicio_f = (isset($queries["datainicio_f"])) ? $queries["datainicio_f"] : false;
		if ($datainicio_f) array_push($where, " e1.datainicio <= '$datainicio_f' ");

$datafim_i = (isset($queries["datafim_i"])) ? $queries["datafim_i"] : false;
		if ($datafim_i) array_push($where, " e1.datafim >= '$datafim_i' ");

$datafim_f = (isset($queries["datafim_f"])) ? $queries["datafim_f"] : false;
		if ($datafim_f) array_push($where, " e1.datafim <= '$datafim_f' ");

$horainicio = (isset($queries["horainicio"])) ? $queries["horainicio"] : false;
		if ($horainicio) array_push($where, " e1.horainicio = '$horainicio' ");

$horafimevento = (isset($queries["horafimevento"])) ? $queries["horafimevento"] : false;
		if ($horafimevento) array_push($where, " e1.horafimevento = '$horafimevento' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " e1.descricoes = '$descricoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM eventos e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEventoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEventos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEventoByIdHelper($id, $queries = array()) {
		$rows = new Eventos();
		return $rows->getEventoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Eventos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idperfil = (array_key_exists("idperfil",$dados)) ? $dados["idperfil"] : $row->idperfil;
 $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
 $row->datafim = (array_key_exists("datafim",$dados)) ? $dados["datafim"] : $row->datafim;
 $row->horainicio = (array_key_exists("horainicio",$dados)) ? $dados["horainicio"] : $row->horainicio;
 $row->horafimevento = (array_key_exists("horafimevento",$dados)) ? $dados["horafimevento"] : $row->horafimevento;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		//return true;		
		$row->save();
		
		return $row;
	}
	
}