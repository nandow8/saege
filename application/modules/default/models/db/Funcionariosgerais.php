<?php

/**
 * Define o modelo Funcionariosgerais
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosgerais extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosgerais";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFuncionariosgeraisHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosgerais = new Funcionariosgerais();
		return $funcionariosgerais->getFuncionariosgerais($queries, $page, $maxpage);
	}
	
	public function getFuncionariosgerais($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " f1.idsecretaria = $idsecretaria ");

$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
		if ($nome) array_push($where, " f1.nome LIKE '%$nome%' ");

$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
		if ($telefone) array_push($where, " f1.telefone LIKE '%$telefone%' ");


$modulo = (isset($queries["modulo"])) ? $queries["modulo"] : false;
		if ($modulo) array_push($where, " f1.modulo LIKE '%$modulo%' ");
$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " f1.tipo LIKE '%$tipo%' ");

$matrícula = (isset($queries["matrícula"])) ? $queries["matrícula"] : false;
		if ($matrícula) array_push($where, " f1.matrícula LIKE '%$matrícula%' ");

$secretaria = (isset($queries["secretaria"])) ? $queries["secretaria"] : false;
		if ($secretaria) array_push($where, " f1.secretaria LIKE '%$secretaria%' ");

$iddepartamentosecretaria = (isset($queries["iddepartamentosecretaria"])) ? $queries["iddepartamentosecretaria"] : false;
		if ($iddepartamentosecretaria) array_push($where, " f1.iddepartamentosecretaria = $iddepartamentosecretaria ");

$setor = (isset($queries["setor"])) ? $queries["setor"] : false;
		if ($setor) array_push($where, " f1.setor LIKE '%$setor%' ");

$funcao = (isset($queries["funcao"])) ? $queries["funcao"] : false;
		if ($funcao) array_push($where, " f1.funcao LIKE '%$funcao%' ");

$descricaofuncao = (isset($queries["descricaofuncao"])) ? $queries["descricaofuncao"] : false;
		if ($descricaofuncao) array_push($where, " f1.descricaofuncao = '$descricaofuncao' ");

$riscofuncao = (isset($queries["riscofuncao"])) ? $queries["riscofuncao"] : false;
		if ($riscofuncao) array_push($where, " f1.riscofuncao LIKE '%$riscofuncao%' ");

$email = (isset($queries["email"])) ? $queries["email"] : false;
		if ($email) array_push($where, " f1.email LIKE '%$email%' ");

$datanascimento_i = (isset($queries["datanascimento_i"])) ? $queries["datanascimento_i"] : false;
		if ($datanascimento_i) array_push($where, " f1.datanascimento >= '$datanascimento_i' ");

$datanascimento_f = (isset($queries["datanascimento_f"])) ? $queries["datanascimento_f"] : false;
		if ($datanascimento_f) array_push($where, " f1.datanascimento <= '$datanascimento_f' ");

$emissaocarteira_i = (isset($queries["emissaocarteira_i"])) ? $queries["emissaocarteira_i"] : false;
		if ($emissaocarteira_i) array_push($where, " f1.emissaocarteira >= '$emissaocarteira_i' ");

$emissaocarteira_f = (isset($queries["emissaocarteira_f"])) ? $queries["emissaocarteira_f"] : false;
		if ($emissaocarteira_f) array_push($where, " f1.emissaocarteira <= '$emissaocarteira_f' ");

$reservista = (isset($queries["reservista"])) ? $queries["reservista"] : false;
		if ($reservista) array_push($where, " f1.reservista LIKE '%$reservista%' ");

$rg = (isset($queries["rg"])) ? $queries["rg"] : false;
		if ($rg) array_push($where, " f1.rg LIKE '%$rg%' ");

$cpf = (isset($queries["cpf"])) ? $queries["cpf"] : false;
		if ($cpf) array_push($where, " f1.cpf LIKE '%$cpf%' ");

$pispasep = (isset($queries["pispasep"])) ? $queries["pispasep"] : false;
		if ($pispasep) array_push($where, " f1.pispasep LIKE '%$pispasep%' ");

$cnh = (isset($queries["cnh"])) ? $queries["cnh"] : false;
		if ($cnh) array_push($where, " f1.cnh LIKE '%$cnh%' ");

$cnh_numero = (isset($queries["cnh_numero"])) ? $queries["cnh_numero"] : false;
		if ($cnh_numero) array_push($where, " f1.cnh_numero LIKE '%$cnh_numero%' ");

$cnh_categoria = (isset($queries["cnh_categoria"])) ? $queries["cnh_categoria"] : false;
		if ($cnh_categoria) array_push($where, " f1.cnh_categoria LIKE '%$cnh_categoria%' ");

$cnh_validade_i = (isset($queries["cnh_validade_i"])) ? $queries["cnh_validade_i"] : false;
		if ($cnh_validade_i) array_push($where, " f1.cnh_validade >= '$cnh_validade_i' ");

$cnh_validade_f = (isset($queries["cnh_validade_f"])) ? $queries["cnh_validade_f"] : false;
		if ($cnh_validade_f) array_push($where, " f1.cnh_validade <= '$cnh_validade_f' ");

$estrangeiro = (isset($queries["estrangeiro"])) ? $queries["estrangeiro"] : false;
		if ($estrangeiro) array_push($where, " f1.estrangeiro LIKE '%$estrangeiro%' ");

$obs_estrangeiro = (isset($queries["obs_estrangeiro"])) ? $queries["obs_estrangeiro"] : false;
		if ($obs_estrangeiro) array_push($where, " f1.obs_estrangeiro = '$obs_estrangeiro' ");

$nacionalidade = (isset($queries["nacionalidade"])) ? $queries["nacionalidade"] : false;
		if ($nacionalidade) array_push($where, " f1.nacionalidade LIKE '%$nacionalidade%' ");

$localnascimento = (isset($queries["localnascimento"])) ? $queries["localnascimento"] : false;
		if ($localnascimento) array_push($where, " f1.localnascimento LIKE '%$localnascimento%' ");

$grauinstrucao = (isset($queries["grauinstrucao"])) ? $queries["grauinstrucao"] : false;
		if ($grauinstrucao) array_push($where, " f1.grauinstrucao LIKE '%$grauinstrucao%' ");

$estadocivil = (isset($queries["estadocivil"])) ? $queries["estadocivil"] : false;
		if ($estadocivil) array_push($where, " f1.estadocivil LIKE '%$estadocivil%' ");

$conjugenome = (isset($queries["conjugenome"])) ? $queries["conjugenome"] : false;
		if ($conjugenome) array_push($where, " f1.conjugenome LIKE '%$conjugenome%' ");

$filhos = (isset($queries["filhos"])) ? $queries["filhos"] : false;
		if ($filhos) array_push($where, " f1.filhos LIKE '%$filhos%' ");

$filhosquantidade = (isset($queries["filhosquantidade"])) ? $queries["filhosquantidade"] : false;
		if ($filhosquantidade) array_push($where, " f1.filhosquantidade = '$filhosquantidade' ");

$rgf = (isset($queries["rgf"])) ? $queries["rgf"] : false;
		if ($rgf) array_push($where, " f1.rgf = '$rgf' ");

$celular = (isset($queries["celular"])) ? $queries["celular"] : false;
		if ($celular) array_push($where, " f1.celular LIKE '%$celular%' ");

$dataadmissao_i = (isset($queries["dataadmissao_i"])) ? $queries["dataadmissao_i"] : false;
		if ($dataadmissao_i) array_push($where, " f1.dataadmissao >= '$dataadmissao_i' ");

$dataadmissao_f = (isset($queries["dataadmissao_f"])) ? $queries["dataadmissao_f"] : false;
		if ($dataadmissao_f) array_push($where, " f1.dataadmissao <= '$dataadmissao_f' ");

$carteiranumero = (isset($queries["carteiranumero"])) ? $queries["carteiranumero"] : false;
		if ($carteiranumero) array_push($where, " f1.carteiranumero LIKE '%$carteiranumero%' ");

$carteiraserie = (isset($queries["carteiraserie"])) ? $queries["carteiraserie"] : false;
		if ($carteiraserie) array_push($where, " f1.carteiraserie LIKE '%$carteiraserie%' ");

$filiacaomae = (isset($queries["filiacaomae"])) ? $queries["filiacaomae"] : false;
		if ($filiacaomae) array_push($where, " f1.filiacaomae LIKE '%$filiacaomae%' ");

$filiacaopai = (isset($queries["filiacaopai"])) ? $queries["filiacaopai"] : false;
		if ($filiacaopai) array_push($where, " f1.filiacaopai LIKE '%$filiacaopai%' ");

$ctps = (isset($queries["ctps"])) ? $queries["ctps"] : false;
		if ($ctps) array_push($where, " f1.ctps LIKE '%$ctps%' ");

$dataregistroctps_i = (isset($queries["dataregistroctps_i"])) ? $queries["dataregistroctps_i"] : false;
		if ($dataregistroctps_i) array_push($where, " f1.dataregistroctps >= '$dataregistroctps_i' ");

$dataregistroctps_f = (isset($queries["dataregistroctps_f"])) ? $queries["dataregistroctps_f"] : false;
		if ($dataregistroctps_f) array_push($where, " f1.dataregistroctps <= '$dataregistroctps_f' ");

$regime = (isset($queries["regime"])) ? $queries["regime"] : false;
		if ($regime) array_push($where, " f1.regime LIKE '%$regime%' ");

$regime_outros = (isset($queries["regime_outros"])) ? $queries["regime_outros"] : false;
		if ($regime_outros) array_push($where, " f1.regime_outros LIKE '%$regime_outros%' ");

$bolsista = (isset($queries["bolsista"])) ? $queries["bolsista"] : false;
		if ($bolsista) array_push($where, " f1.bolsista LIKE '%$bolsista%' ");

$datainiciobolsa_i = (isset($queries["datainiciobolsa_i"])) ? $queries["datainiciobolsa_i"] : false;
		if ($datainiciobolsa_i) array_push($where, " f1.datainiciobolsa >= '$datainiciobolsa_i' ");

$datainiciobolsa_f = (isset($queries["datainiciobolsa_f"])) ? $queries["datainiciobolsa_f"] : false;
		if ($datainiciobolsa_f) array_push($where, " f1.datainiciobolsa <= '$datainiciobolsa_f' ");

$datafimbolsa_i = (isset($queries["datafimbolsa_i"])) ? $queries["datafimbolsa_i"] : false;
		if ($datafimbolsa_i) array_push($where, " f1.datafimbolsa >= '$datafimbolsa_i' ");

$datafimbolsa_f = (isset($queries["datafimbolsa_f"])) ? $queries["datafimbolsa_f"] : false;
		if ($datafimbolsa_f) array_push($where, " f1.datafimbolsa <= '$datafimbolsa_f' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado "; ;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgerais f1
					 LEFT JOIN enderecos e_idendereco ON e_idendereco.id=f1.idendereco 
						LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado	
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariogeralById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosgerais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionariogeralByIdHelper($id, $queries = array()) {
		$rows = new Funcionariosgerais();
		return $rows->getFuncionariogeralById($id, $queries);
	}		
	
	public static function getFuncionariosgeraisByRgfHelper($rgf, $queries = array()) {
		if ((!isset($rgf)) || (!$rgf) || ($rgf=="")) return false;
		$_rows = new Funcionariosgerais();

		$queries['rgf'] = $rgf;
		$rows = $_rows->getFuncionariosgerais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Funcionariosgerais
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
			$historico = new Funcionariosgeraishistoricos();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
		 $row->idcargo = (array_key_exists("idcargo",$dados)) ? $dados["idcargo"] : $row->idcargo;
 $row->idimagem = (array_key_exists("idimagem",$dados)) ? $dados["idimagem"] : $row->idimagem;
 $row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
 $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
 $row->modulo = (array_key_exists("modulo",$dados)) ? $dados["modulo"] : $row->modulo;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->idendereco = (array_key_exists("idendereco",$dados)) ? $dados["idendereco"] : $row->idendereco;
 $row->rgf = (array_key_exists("rgf",$dados)) ? $dados["rgf"] : $row->rgf;
 //$row->secretaria = (array_key_exists("secretaria",$dados)) ? $dados["secretaria"] : $row->secretaria;
 $row->iddepartamentosecretaria = (array_key_exists("iddepartamentosecretaria",$dados)) ? $dados["iddepartamentosecretaria"] : $row->iddepartamentosecretaria;
 $row->setor = (array_key_exists("setor",$dados)) ? $dados["setor"] : $row->setor;
 $row->funcao = (array_key_exists("funcao",$dados)) ? $dados["funcao"] : $row->funcao;
 $row->descricaofuncao = (array_key_exists("descricaofuncao",$dados)) ? $dados["descricaofuncao"] : $row->descricaofuncao;
 $row->riscofuncao = (array_key_exists("riscofuncao",$dados)) ? $dados["riscofuncao"] : $row->riscofuncao;
 $row->email = (array_key_exists("email",$dados)) ? $dados["email"] : $row->email;
 $row->datanascimento = (array_key_exists("datanascimento",$dados)) ? $dados["datanascimento"] : $row->datanascimento;
 $row->emissaocarteira = (array_key_exists("emissaocarteira",$dados)) ? $dados["emissaocarteira"] : $row->emissaocarteira;
 $row->reservista = (array_key_exists("reservista",$dados)) ? $dados["reservista"] : $row->reservista;
 $row->rg = (array_key_exists("rg",$dados)) ? $dados["rg"] : $row->rg;
 $row->cpf = (array_key_exists("cpf",$dados)) ? $dados["cpf"] : $row->cpf;
 $row->pispasep = (array_key_exists("pispasep",$dados)) ? $dados["pispasep"] : $row->pispasep;
 $row->cnh = (array_key_exists("cnh",$dados)) ? $dados["cnh"] : $row->cnh;
 $row->cnh_numero = (array_key_exists("cnh_numero",$dados)) ? $dados["cnh_numero"] : $row->cnh_numero;
 $row->cnh_categoria = (array_key_exists("cnh_categoria",$dados)) ? $dados["cnh_categoria"] : $row->cnh_categoria;
 $row->cnh_validade = (array_key_exists("cnh_validade",$dados)) ? $dados["cnh_validade"] : $row->cnh_validade;
 $row->estrangeiro = (array_key_exists("estrangeiro",$dados)) ? $dados["estrangeiro"] : $row->estrangeiro;
 $row->obs_estrangeiro = (array_key_exists("obs_estrangeiro",$dados)) ? $dados["obs_estrangeiro"] : $row->obs_estrangeiro;
 $row->nacionalidade = (array_key_exists("nacionalidade",$dados)) ? $dados["nacionalidade"] : $row->nacionalidade;
 $row->localnascimento = (array_key_exists("localnascimento",$dados)) ? $dados["localnascimento"] : $row->localnascimento;
 $row->grauinstrucao = (array_key_exists("grauinstrucao",$dados)) ? $dados["grauinstrucao"] : $row->grauinstrucao;
 $row->estadocivil = (array_key_exists("estadocivil",$dados)) ? $dados["estadocivil"] : $row->estadocivil;
 $row->conjugenome = (array_key_exists("conjugenome",$dados)) ? $dados["conjugenome"] : $row->conjugenome;
 $row->filhos = (array_key_exists("filhos",$dados)) ? $dados["filhos"] : $row->filhos;
 $row->filhosquantidade = (array_key_exists("filhosquantidade",$dados)) ? $dados["filhosquantidade"] : $row->filhosquantidade;
 $row->celular = (array_key_exists("celular",$dados)) ? $dados["celular"] : $row->celular;
 $row->dataadmissao = (array_key_exists("dataadmissao",$dados)) ? $dados["dataadmissao"] : $row->dataadmissao;
 $row->carteiranumero = (array_key_exists("carteiranumero",$dados)) ? $dados["carteiranumero"] : $row->carteiranumero;
 $row->carteiraserie = (array_key_exists("carteiraserie",$dados)) ? $dados["carteiraserie"] : $row->carteiraserie;
 $row->filiacaomae = (array_key_exists("filiacaomae",$dados)) ? $dados["filiacaomae"] : $row->filiacaomae;
 $row->filiacaopai = (array_key_exists("filiacaopai",$dados)) ? $dados["filiacaopai"] : $row->filiacaopai;
 $row->ctps = (array_key_exists("ctps",$dados)) ? $dados["ctps"] : $row->ctps;
 $row->dataregistroctps = (array_key_exists("dataregistroctps",$dados)) ? $dados["dataregistroctps"] : $row->dataregistroctps;
 $row->regime = (array_key_exists("regime",$dados)) ? $dados["regime"] : $row->regime;
 $row->regime_outros = (array_key_exists("regime_outros",$dados)) ? $dados["regime_outros"] : $row->regime_outros;
 $row->bolsista = (array_key_exists("bolsista",$dados)) ? $dados["bolsista"] : $row->bolsista;
 $row->datainiciobolsa = (array_key_exists("datainiciobolsa",$dados)) ? $dados["datainiciobolsa"] : $row->datainiciobolsa;
 $row->datafimbolsa = (array_key_exists("datafimbolsa",$dados)) ? $dados["datafimbolsa"] : $row->datafimbolsa;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}

$row->codigovinculo = (array_key_exists("codigovinculo",$dados)) ? $dados["codigovinculo"] : $row->codigovinculo;
$row->vinculoempregaticio = (array_key_exists("vinculoempregaticio",$dados)) ? $dados["vinculoempregaticio"] : $row->vinculoempregaticio;
$row->codigodepartamento = (array_key_exists("codigodepartamento",$dados)) ? $dados["codigodepartamento"] : $row->codigodepartamento;
$row->descdepartamento = (array_key_exists("descdepartamento",$dados)) ? $dados["descdepartamento"] : $row->descdepartamento;
$row->desclocal = (array_key_exists("desclocal",$dados)) ? $dados["desclocal"] : $row->desclocal;
$row->idimportacao = (array_key_exists("idimportacao",$dados)) ? $dados["idimportacao"] : $row->idimportacao;
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}