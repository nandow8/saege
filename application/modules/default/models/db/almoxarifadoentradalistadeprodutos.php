<?php

/**
 * Define o modelo Almoxarifadoentradalistadeprodutos
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 FJMX.
 * @version     1.0
 */
class Almoxarifadoentradalistadeprodutos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoentradalistadeprodutos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAlmoxarifadoentradalistadeprodutosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$almoxarifadoentradalistadeprodutos = new Almoxarifadoentradalistadeprodutos();
		return $almoxarifadoentradalistadeprodutos->getAlmoxarifadoentradalistadeprodutos($queries, $page, $maxpage);
	}
	
	public function getAlmoxarifadoentradalistadeprodutos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " alep.id = $id ");
		
		$idfuncionario = (isset($queries['idfuncionario'])) ? (int)$queries['idfuncionario'] : false;
		if ($idfuncionario) array_push($where, " alep.idfuncionario = $idfuncionario ");
		$codigosolicitacao = (isset($queries["codigosolicitacao"])) ? $queries["codigosolicitacao"] : false;
		if ($codigosolicitacao) array_push($where, " alep.codigosolicitacao = $codigosolicitacao ");
		
		$produto = (isset($queries["produto"])) ? $queries["produto"] : false;
		if ($produto) array_push($where, " alep.produto LIKE '%$produto%' ");
		
		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " alep.descricoes LIKE '%$descricoes%' ");
		
		$codigoproduto = (isset($queries["codigoproduto"])) ? $queries["codigoproduto"] : false;
		if ($codigoproduto) array_push($where, " alep.codigoproduto LIKE '%$codigoproduto%' ");
		
		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " alep.status LIKE '%$status%' ");
		
		$idcategoria = (isset($queries["idcategoria"])) ? $queries["idcategoria"] : false;
		if ($idcategoria) array_push($where, " alep.idcategoria = '$idcategoria' ");
		
		$idunidademedida = (isset($queries["idunidademedida"])) ? $queries["idunidademedida"] : false;
		if ($idunidademedida) array_push($where, " alep.idunidademedida = '$idunidademedida' ");
		
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "($w)";
		
		$fields = "alep.*"; 
		;
		
		if ($total) $fields = "COUNT(alep.id) as total";
		
		$ordem = "ORDER BY alep.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
		
		$strsql = "SELECT $fields 
		FROM almoxarifadoentradalistadeprodutos alep

		WHERE 
		$w 
		$ordem	
		$limit";	
		// die($strsql );
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	 
	 
		return $db->fetchAll($strsql);			
	}	
	
	public function getAlmoxarifadoentradalistadeprodutosById($idfuncionario, $queries = array()) {
		
		if ($idfuncionario==0) return false; 
		$queries['idfuncionario'] = $idfuncionario;
		$rows = $this->getAlmoxarifadoentradalistadeprodutos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
 	 
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadoentradalistadeprodutos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
		$row->produto = (array_key_exists("produto",$dados)) ? $dados["produto"] : $row->produto;
		$row->unidademedida = (array_key_exists("unidademedida",$dados)) ? $dados["unidademedida"] : $row->unidademedida;
		$row->qtd_selecionada = (array_key_exists("qtd_selecionada",$dados)) ? $dados["qtd_selecionada"] : $row->qtd_selecionada;
		$row->qtd_liberado = (array_key_exists("qtd_liberado",$dados)) ? $dados["qtd_liberado"] : $row->qtd_liberado;
		$row->codigoproduto = (array_key_exists("codigoproduto",$dados)) ? $dados["codigoproduto"] : $row->codigoproduto;
		$row->motivoliberacao = (array_key_exists("motivoliberacao",$dados)) ? $dados["motivoliberacao"] : $row->motivoliberacao;
		$row->codigosolicitacao = (array_key_exists("codigosolicitacao",$dados)) ? $dados["codigosolicitacao"] : $row->codigosolicitacao;
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}  

		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		$row->save();
		
		return $row;
	}
	
}