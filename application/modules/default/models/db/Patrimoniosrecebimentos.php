<?php

/**
 * Define o modelo Patrimoniosrecebimentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Patrimoniosrecebimentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "patrimoniosrecebimentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getPatrimoniosrecebimentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$patrimoniosrecebimentos = new Patrimoniosrecebimentos();
		return $patrimoniosrecebimentos->getPatrimoniosrecebimentos($queries, $page, $maxpage);
	}
	
	public function getPatrimoniosrecebimentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " pr1.id = $id ");
		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " pr1.sequencial LIKE '%$sequencial%' ");

		$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " pr1.datalancamento >= '$datalancamento_i' ");

		$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " pr1.datalancamento <= '$datalancamento_f' ");

		$horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
		if ($horalancamento) array_push($where, " pr1.horalancamento = '$horalancamento' ");

		$idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " pr1.idusuario = $idusuario ");

		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " pr1.idsecretaria = $idsecretaria ");

		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " pr1.iddepartamento LIKE '%$iddepartamento%' ");

		$idpatrimonio = (isset($queries["idpatrimonio"])) ? $queries["idpatrimonio"] : false;
		if ($idpatrimonio) array_push($where, " pr1.idpatrimonio = '$idpatrimonio' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " pr1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "pr1.*, pt1.tipo, p1.cor, p1.patrimonio, p1.descricoes, p1.datacompra, m1.marca, ds1.departamento"; 
		;
		
		if ($total) $fields = "COUNT(pr1.id) as total";
		
		$ordem = "ORDER BY pr1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM patrimoniosrecebimentos pr1
					LEFT JOIN patrimonios p1 ON p1.id = pr1.idpatrimonio
					LEFT JOIN departamentossecretarias ds1 ON ds1.id = pr1.iddepartamento
					LEFT JOIN patrimoniostipos pt1 ON pt1.id = p1.idtipo
					LEFT JOIN marcas m1 ON m1.id = p1.idmarca
					WHERE pr1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPatrimoniosrecebimentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPatrimoniosrecebimentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPatrimoniosrecebimentoByIdHelper($id, $queries = array()) {
		$rows = new Patrimoniosrecebimentos();
		return $rows->getPatrimoniosrecebimentoById($id, $queries);
	}		
	
	public function getUltimoPatrimoniosrecebimento($queries = array()) {
		$queries['order'] = 'ORDER BY pr1.id DESC';
		$rows = $this->getPatrimoniosrecebimentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Patrimoniosrecebimentos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
		 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
		 $row->horalancamento = (array_key_exists("horalancamento",$dados)) ? $dados["horalancamento"] : $row->horalancamento;
		 $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
		 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
		 $row->idpatrimonio = (array_key_exists("idpatrimonio",$dados)) ? $dados["idpatrimonio"] : $row->idpatrimonio;

		 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}