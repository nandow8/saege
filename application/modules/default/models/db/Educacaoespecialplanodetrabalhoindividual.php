<?php

/**
 * Define o modelo Educacaoespecialplanodetrabalhoindividual
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 JFMX.  
 * @version     1.0
 */
class Educacaoespecialplanodetrabalhoindividual extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "educacaoespecialplanodetrabalhoindividual";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEducacaoespecialplanodetrabalhoindividualHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$educacaoespecialplanodetrabalhoindividual = new Educacaoespecialplanodetrabalhoindividual();
		return $educacaoespecialplanodetrabalhoindividual->getEducacaoespecialplanodetrabalhoindividual($queries, $page, $maxpage);
	}
	
	public function getEducacaoespecialplanodetrabalhoindividual($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
			
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id "); 
		
		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		if ($idaluno) array_push($where, " c1.idaluno = $idaluno "); 

		$conteudo = (isset($queries['conteudo'])) ? (int)$queries['conteudo'] : false;
		if ($conteudo) array_push($where, " c1.conteudo = '$conteudo' "); 

		$linguagem_oral_portugues = (isset($queries['linguagem_oral_portugues'])) ? (int)$queries['linguagem_oral_portugues'] : false;
		if ($linguagem_oral_portugues) array_push($where, " c1.linguagem_oral_portugues = '$linguagem_oral_portugues' "); 

		$raciocinio_logico_matematica = (isset($queries['raciocinio_logico_matematica'])) ? (int)$queries['raciocinio_logico_matematica'] : false;
		if ($raciocinio_logico_matematica) array_push($where, " c1.raciocinio_logico_matematica = '$raciocinio_logico_matematica "); 

		$coord_motora_fina_grossa = (isset($queries['coord_motora_fina_grossa'])) ? (int)$queries['coord_motora_fina_grossa'] : false;
		if ($coord_motora_fina_grossa) array_push($where, " c1.coord_motora_fina_grossa = '$coord_motora_fina_grossa' "); 

		$independencia_organizacao = (isset($queries['independencia_organizacao'])) ? (int)$queries['independencia_organizacao'] : false;
		if ($independencia_organizacao) array_push($where, " c1.independencia_organizacao = '$independencia_organizacao' "); 

		$boaconduta = (isset($queries['boaconduta'])) ? (int)$queries['boaconduta'] : false;
		if ($boaconduta) array_push($where, " c1.boaconduta = '$boaconduta' "); 

		$objetivosgerais = (isset($queries['objetivosgerais'])) ? (int)$queries['objetivosgerais'] : false;
		if ($objetivosgerais) array_push($where, " c1.objetivosgerais = '$objetivosgerais' ");  
		
		$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " c1.datacriacao >= '$data_i' ");

		$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " c1.datacriacao <= '$data_f' ");

        $status1 = (isset($queries["status1"])) ? $queries["status1"] : false;
		if ($status1) array_push($where, " c1.status = '$status1' "); 
  
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";       $w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*";  
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM educacaoespecialplanodetrabalhoindividual c1 
					WHERE c1.excluido='nao'
						$w 
					$ordem	
					$limit";
	   
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			if(isset($total)) {
				return 0;
			} 
			return $row['total'];
		}	
		// die($strsql);
		return $db->fetchAll($strsql);			
	}	
	
	public function getEducacaoespecialplanodetrabalhoindividualById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEducacaoespecialplanodetrabalhoindividual($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEducacaoespecialplanodetrabalhoindividualByIdHelper($id, $queries = array()) {
		$rows = new Educacaoespecialplanodetrabalhoindividual();
		return $rows->getEducacaoespecialplanodetrabalhoindividualById($id, $queries);
	} 
 
	// public static function getAno() {		
	// 	$strsql = "SELECT DISTINCT ano FROM coordenacaoquadrodesempenhoaprendizagem";	
		 		 					
	// 	$db = Zend_Registry::get('db');				
			
	// 	return $db->fetchAll($strsql);
	// }
	 
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Educacaoespecialplanodetrabalhoindividual
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else { $novoRegistro = false; } 
        
        $row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno; 
        $row->conteudo = (array_key_exists("conteudo",$dados)) ? $dados["conteudo"] : $row->conteudo; 
        $row->linguagem_oral_portugues = (array_key_exists("linguagem_oral_portugues",$dados)) ? $dados["linguagem_oral_portugues"] : $row->linguagem_oral_portugues; 
        $row->raciocinio_logico_matematica = (array_key_exists("raciocinio_logico_matematica",$dados)) ? $dados["raciocinio_logico_matematica"] : $row->raciocinio_logico_matematica; 
        $row->coord_motora_fina_grossa = (array_key_exists("coord_motora_fina_grossa",$dados)) ? $dados["coord_motora_fina_grossa"] : $row->coord_motora_fina_grossa; 
        $row->independencia_organizacao = (array_key_exists("independencia_organizacao",$dados)) ? $dados["independencia_organizacao"] : $row->independencia_organizacao; 
        $row->boaconduta = (array_key_exists("boaconduta",$dados)) ? $dados["boaconduta"] : $row->boaconduta; 
        $row->objetivosgerais = (array_key_exists("objetivosgerais",$dados)) ? $dados["objetivosgerais"] : $row->objetivosgerais; 
     
        if (is_null($row->datacriacao))  $row->datacriacao = date("Y-m-d H:i:s");
        $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status; 
        $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario; 
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		$row->save();
		
		return $row;
	}
	
}