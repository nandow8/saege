<?php

class Ltecidades extends Zend_Db_Table_Abstract {
    
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "ltecidades";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public function getcidades($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $cidade = (isset($queries['cidade'])) ? $queries['cidade'] : false;

        $total  = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order  = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " lc.id = $id ");
        if ($cidade) array_push($where, " vch.nomecidade LIKE '%$cidade%' ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lc.id, lc.status, lc.excluido, lc.datacriacao, vch.cidade, vch.valorfretado, vch.subsidio, vch.logusuario, vch.logdata";

        $ordem = "ORDER BY vch.cidade";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        if ($total) {
            $strsql = "SELECT count(lc.id) as total FROM ltecidades lc INNER JOIN view_cidade_historico vch ON lc.id = vch.idcidade WHERE lc.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM ltecidades lc INNER JOIN view_cidade_historico vch ON lc.id = vch.idcidade WHERE lc.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();
        $cidades = $db->fetchAll($strsql);

        $historico = new Ltehistoricocidades();

        foreach ($cidades as $key => $cidade) {
            $cidades[$key]['historico'] = $historico->getHistoricoCidadesByIdCidade($cidade['id']);
        }

        return $cidades;

    }

    public function getcidadesById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getcidades($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }

    public static function getcidadesHelper($queries = array()) {
        $rows = new Ltecidades();
        return $rows->getcidades();;
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ltecidades
     */
    public function save($dados) {

        $novoRegistro = true;
        $integracao = false;
        
        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) { 
            $row = $this->createRow();  
        }

        // $row->cidade = (array_key_exists('cidade',$dados)) ? $dados['cidade'] : $row->cidade;
        // $row->valorfretado = (array_key_exists('valorfretado',$dados)) ? $dados['valorfretado'] : $row->valorfretado;
        // $row->subsidio = (array_key_exists('subsidio',$dados)) ? $dados['subsidio'] : $row->subsidio;
        $row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');    
        }
        
        $row->save();
        
        if($id===0){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::incluir", Usuarios::getUsuario("id"), "Cidade adicionada com sucesso!");
        }elseif($id!== 0 AND isset($row->excluido) AND $row->excluido==="sim"){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::excluir", Usuarios::getUsuario("id"), "Cidade de ID ".$id." excluída com sucesso!");
        } else Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::atualizar", Usuarios::getUsuario("id"), "Cidade de ID ".$id." atualizada com sucesso!");
        
        return $row;
    }
}