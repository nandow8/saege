<?php

/**
 * Define o modelo Coordenacaocalendarios
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Coordenacaocalendarios extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "coordenacaocalendarios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCoordenacaocalendariosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$coordenacaocalendarios = new Coordenacaocalendarios();
		return $coordenacaocalendarios->getCoordenacaocalendarios($queries, $page, $maxpage);
	}
	
	public function getCoordenacaocalendarios($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		$andBusca = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " c1.idsecretaria = $idsecretaria ");

		$ano = (isset($queries["ano"])) ? $queries["ano"] : false;
		if ($ano) $andBusca['ano'] = " AND c1.ano = '$ano' " ;     // AND
		
		$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) $andBusca['titulo'] = " AND c1.titulo = '$titulo' " ;     // AND
		
		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) $andBusca['status'] = " AND c1.status = '$status' " ;     // AND 
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " c1.idescola = '$idescola' ");
		
		$idperfil = (isset($queries["idperfil"])) ? $queries["idperfil"] : false;
		if ($idperfil) array_push($where, " c1.idperfil = '$idperfil' ");
 
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		$w_and = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " OR ";              //  OOOOOORRRRRRRRR
			$w .= $v; 
		}
		if ($w!="") $w = "AND ($w)";

		if(isset($andBusca['ano'])) $w_and .= $andBusca['ano'];
		if(isset($andBusca['titulo'])) $w_and .= $andBusca['titulo'];
		if(isset($andBusca['status'])) $w_and .= $andBusca['status'];

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM coordenacaocalendarios c1
					
					WHERE c1.excluido='nao' 
						$w  $w_and 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		// die($strsql);
		return $db->fetchAll($strsql);			
	}	
	
	public function getCoordenacaocalendarioById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCoordenacaocalendarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCoordenacaocalendarioByIdHelper($id, $queries = array()) {
		$rows = new Coordenacaocalendarios();
		return $rows->getCoordenacaocalendarioById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Coordenacaocalendarios
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
		$row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
		$row->ano = (array_key_exists("ano",$dados)) ? $dados["ano"] : $row->ano;

		$row->primeirobimestreinicio = (array_key_exists("primeirobimestreinicio",$dados)) ? $dados["primeirobimestreinicio"] : $row->primeirobimestreinicio;
		$row->primeirobimestrefim = (array_key_exists("primeirobimestrefim",$dados)) ? $dados["primeirobimestrefim"] : $row->primeirobimestrefim;
		$row->segundobimestreinicio = (array_key_exists("segundobimestreinicio",$dados)) ? $dados["segundobimestreinicio"] : $row->segundobimestreinicio;
		$row->segundobimestrefim = (array_key_exists("segundobimestrefim",$dados)) ? $dados["segundobimestrefim"] : $row->segundobimestrefim;
		$row->terceirobimestreinicio = (array_key_exists("terceirobimestreinicio",$dados)) ? $dados["terceirobimestreinicio"] : $row->terceirobimestreinicio;
		$row->terceirobimestrefim = (array_key_exists("terceirobimestrefim",$dados)) ? $dados["terceirobimestrefim"] : $row->terceirobimestrefim;
		$row->quartobimestreinicio = (array_key_exists("quartobimestreinicio",$dados)) ? $dados["quartobimestreinicio"] : $row->quartobimestreinicio;
		$row->quartobimestrefim = (array_key_exists("quartobimestrefim",$dados)) ? $dados["quartobimestrefim"] : $row->quartobimestrefim;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->idescolaForm = (array_key_exists("idescolaForm",$dados)) ? $dados["idescolaForm"] : $row->idescolaForm;
		$row->idperfil = (array_key_exists("idperfil",$dados)) ? $dados["idperfil"] : $row->idperfil;
		$row->idcalendariobase = (array_key_exists("idcalendariobase",$dados)) ? $dados["idcalendariobase"] : $row->idcalendariobase;
		$row->salvaidcalendariobase = (array_key_exists("salvaidcalendariobase",$dados)) ? $dados["salvaidcalendariobase"] : $row->salvaidcalendariobase;

		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		if (is_null($row->datacriacao)) {
					$row->datacriacao = date("Y-m-d H:i:s");
				}
								
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}