<?php

class Lteuniversitarios extends Zend_Db_Table_Abstract {
    
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "lteuniversitarios";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";     
    
    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");
            
        if (!$usuario) return "RGM ou senha inválidos!";
        if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
        if ($usuario['senha']!=$senha) return "A senha é inválida!";
        
        $usuario = $usuario->toArray();

        if (!$ns) $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);
        
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);
        
        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';
        
        $loginNameSpace->alunousuario = serialize($usuario);
                        
        return true;        
    } 
    
    public function getPasseEscolar($queries = array(), $page = 0, $maxpage = 0) {

        $id    = (isset($queries['id'])) ? (int)$queries['id'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " lp.id = $id ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lp.*";

        $ordem = "ORDER BY lp.id";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(id) as total FROM ltepasseescolar lp WHERE lp.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM ltepasseescolar lp WHERE lp.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();
        
        return $db->fetchAll($strsql);      
    }

    public function getPeriodos($queries = array(), $page = 0, $maxpage = 0) {
        $id   = (isset($queries['id'])) ? $queries['id'] : false;

        $fechado = (isset($queries['fechado'])) ? $queries['fechado'] : false;

        $total  = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order  = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id) array_push($where, " lp.id = $id ");
        if ($fechado) array_push($where, " lp.fechado='$fechado' ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lp.id id, lp.datainicio datainicio, lp.datafim datafim, lp.diasletivos diasletivos, round(lp.valorunitario,2) valorunitario, lp.observacoes observacoes, lp.tipo, lp.tipotransporte tipotransporte, lp.status status, lp.excluido excluido, lp.datacriacao datacriacao, lp.logusuario logusuario, lp.logdata logdata";

        $ordem = "ORDER BY lp.datainicio desc";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(id) as total FROM lteperiodos lp WHERE lp.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM lteperiodos lp WHERE lp.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEstudantes($queries = array(), $page = 0, $maxpage = 0) {
        $id = (isset($queries['id'])) ? $queries['id'] : false;
        $identidade = (isset($queries['identidade'])) ? $queries['identidade'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id) array_push($where, " le.id = $id ");
        if ($identidade) array_push($where, " le.identidade = $identidade ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";
        if ($order) $ordem = $order; 
        
        $fields = "le.id, le.tipoestudante tipo, le.tipotransporte tipotransporte, le.nome nome, le.datanascimento datanascimento, CONCAT(le.endereco,',',le.numero,' ',le.complemento,' ',le.bairro) endereco, le2.entidade entidade, le.identidade identidade, le.curso";

        $ordem = "ORDER BY le.id";

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        if ($total) {
            $strsql = "SELECT count(le.id) FROM lteestudantes le LEFT JOIN lteentidades le2 ON le.identidade=le2.id WHERE le.excluido = 'nao' AND le.tipoestudante = 'universidade' AND le.status = 'Ativo' $w";
        } else {
            $strsql = "SELECT $fields FROM lteestudantes le LEFT JOIN lteentidades le2 ON le.identidade=le2.id WHERE le.excluido = 'nao' AND le.tipoestudante = 'universidade' AND le.status = 'Ativo' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();

        return $db->fetchAll($strsql);
    }

    public function getEstudantesPeriodo($queries = array(), $page = 0, $maxpage = 0) {
        $id = (isset($queries['id'])) ? $queries['id'] : false;
        $idperiodo = (isset($queries['idperiodo'])) ? $queries['idperiodo'] : false;

        $total  = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order  = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id) array_push($where, " lu.id = $id ");
        if ($idperiodo) array_push($where, " lu.idperiodo = $idperiodo ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";
        
        $fields = "lu.*";

        $ordem = "ORDER BY lu.id";
        if ($order) $ordem = $order;

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        if ($total) {
            $strsql = "SELECT COUNT(lu.id) FROM lteuniversitarios lu WHERE lu.excluido = 'nao' AND lu.status = 'Ativo' $w";
        } else {
            $strsql = "SELECT $fields FROM lteuniversitarios lu WHERE lu.excluido = 'nao' AND lu.status = 'Ativo' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();

        return $db->fetchAll($strsql);
    }
    
    public function getPeriodosById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getPeriodos($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }
    
    public function getEstudantesPasseByTipo($queries = array()) {
        // if ($tipo == '') return false;
        
        // $queries['tipo'] = $tipo;
        $rows = $this->getEstudantesPasse($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows;
    }

    public static function getPeriodosByIdHelper($id, $queries = array()) {
        $rows = new Ltepasseescolar();
        return $rows->getPeriodosById($id, $queries);
    }       
    
    public static function getPeriodosHelper($queries = array()) {
        $rows = new Ltepasseescolar();
        return $rows->fetchAll();;
    }    

    public static function getNomeByIdHelper($id, $queries = array()) {
        $rows = new Ltepasseescolar();
        $usuario = $rows->getPeriodosById($id, $queries);
        if(!$usuario) return false;
        return $usuario['nome'];
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Lteuniversitarios
     */
    public function save($dados) {
        echo '<pre>';
        print_r($dados);

        $novoRegistro = true;
        
        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) { 
            $row = $this->createRow();  
        }else {}

        $row->idestudante = (array_key_exists('idestudante',$dados)) ? $dados['idestudante'] : $row->idestudante;
        $row->idperiodo = (array_key_exists('idperiodo',$dados)) ? $dados['idperiodo'] : $row->idperiodo;
        $row->faltas = (array_key_exists('faltas',$dados)) ? $dados['faltas'] : $row->faltas;

        $row->logusuario = Usuarios::getUsuario('id');
        $row->logdata = date('Y-m-d G:i:s');

        $periodos = new Lteperiodos();
        $periodo = $periodos->getPeriodosById($row->idperiodo);

        $row->total = ($periodo['diasletivos'] - $row->faltas) * 2 * $periodo['valorunitario'];

        if ($row->total < 0) {
            $row->faltas = $periodo['diasletivos'];
            $row->total = 0;
        }

        $row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
        $row->observacoes = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');
            $row->usuariocriacao = Usuarios::getUsuario('id');
        }
        
        echo $id;
        echo '<pre>';
        echo $dados['excluido'];
        echo '<pre>';
        
        
        //print_r($row);
        //die();
        $row->save();

        return $row;
    } 
}
