<?php

/**
 * Define o modelo Escolasvinculos
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasvinculos extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "escolasvinculos";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getEscolasvinculosHelper($queries = array(), $page = 0, $maxpage = 0) {
        $escolasvinculos = new Escolasvinculos();
        return $escolasvinculos->getEscolasvinculos($queries, $page, $maxpage);
    }

    public function getEscolasvinculos($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        $groupby = (isset($queries['group'])) ? $queries['group'] : false;
        $die = (isset($queries['die'])) ? $queries['die'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " ev1.id = $id ");


        $ids = (isset($queries["ids"])) ? $queries["ids"] : false;
        if ($ids)
            array_push($where, " FIND_IN_SET(ev1.id, '$ids') ");

        $idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
        if ($idsecretaria)
            array_push($where, " ev1.idsecretaria = $idsecretaria ");

        $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
        if ($idescola)
            array_push($where, " ev1.idescola = $idescola ");

        $idsala = (isset($queries["idsala"])) ? $queries["idsala"] : false;
        if ($idsala)
            array_push($where, " ev1.idsala = $idsala ");
        $serie = (isset($queries["serie"])) ? $queries["serie"] : false;

        //AEE tem o id 0 - sendo visto como vazio pelo sistema
        //solução para corrigir esse problema
        if ($serie) {
            if ($serie == '100') {
                $serie = '0';
                array_push($where, " ess1.serie = '$serie' ");
            } else {
                array_push($where, " ess1.serie = '$serie' ");
            }
        }

        $idserie = (isset($queries["idserie"])) ? $queries["idserie"] : false;
        if ($idserie)
            array_push($where, " ev1.idserie = $idserie ");
        $idsseries = (isset($queries["idsseries"])) ? $queries["idsseries"] : false;
        if ($idsseries)
            array_push($where, " FIND_IN_SET(ev1.idserie, '$idsseries') ");

        $turma = (isset($queries["turma"])) ? $queries["turma"] : false;
        if ($turma)
            array_push($where, " et1.turma = '$turma' ");

        $idturma = (isset($queries["idturma"])) ? $queries["idturma"] : false;
        if ($idturma)
            array_push($where, " et1.turma = '$idturma' ");

        $idperiodo = (isset($queries["idperiodo"])) ? $queries["idperiodo"] : false;
        if ($idperiodo)
            array_push($where, " ev1.idperiodo = $idperiodo ");

        $idprofessor = (isset($queries["idprofessor"])) ? $queries["idprofessor"] : false;
        if ($idprofessor)
            array_push($where, " ev1.idprofessor = $idprofessor ");

        $not_idprofessor = (isset($queries["not_idprofessor"])) ? $queries["not_idprofessor"] : false;
        if ($not_idprofessor)
            array_push($where, " (ev1.idprofessor = 0 OR ev1.idprofessor = '' OR ev1.idprofessor IS NULL) ");

        $idtipoensino = (isset($queries["idtipoensino"])) ? $queries["idtipoensino"] : false;
        if ($idtipoensino)
            array_push($where, " ev1.idtipoensino = $idtipoensino ");

        $inicioaulas_i = (isset($queries["inicioaulas_i"])) ? $queries["inicioaulas_i"] : false;
        if ($inicioaulas_i)
            array_push($where, " ev1.inicioaulas >= '$inicioaulas_i' ");

        $inicioaulas_f = (isset($queries["inicioaulas_f"])) ? $queries["inicioaulas_f"] : false;
        if ($inicioaulas_f)
            array_push($where, " ev1.inicioaulas <= '$inicioaulas_f' ");

        $terminoaulas_i = (isset($queries["terminoaulas_i"])) ? $queries["terminoaulas_i"] : false;
        if ($terminoaulas_i)
            array_push($where, " ev1.terminoaulas >= '$terminoaulas_i' ");

        $terminoaulas_f = (isset($queries["terminoaulas_f"])) ? $queries["terminoaulas_f"] : false;
        if ($terminoaulas_f)
            array_push($where, " ev1.terminoaulas <= '$terminoaulas_f' ");

        $turno = (isset($queries["turno"])) ? $queries["turno"] : false;
        if ($turno)
            array_push($where, " ev1.turno LIKE '%$turno%' ");

        $ano = (isset($queries["ano"])) ? $queries["ano"] : false;
        if ($ano)
            array_push($where, " ev1.ano = '$ano' ");

        $classe = (isset($queries["classe"])) ? $queries["classe"] : false;
        if ($classe)
            array_push($where, " ev1.classe LIKE (REPLACE(REPLACE('$classe%',',',''), '.', ''))  ");

        $integracao = (isset($queries["integracao"])) ? $queries["integracao"] : false;
        if ($integracao)
            array_push($where, " ev1.integracao LIKE '%$integracao%' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " ev1.status LIKE '%$status%' ");

        $idssedatividadescomplementares = (isset($queries["idssedatividadescomplementares"])) ? $queries["idssedatividadescomplementares"] : false;
        if ($idssedatividadescomplementares)
            array_push($where, " FIND_IN_SET(ev1.idssedatividadescomplementares, '$idssedatividadescomplementares') ");

        $soapstatus = (isset($queries["soapstatus"])) ? $queries["soapstatus"] : false;
        if ($soapstatus)
            array_push($where, " ev1.soapstatus LIKE '%$soapstatus%' ");

        $soaptipo = (isset($queries["soaptipo"])) ? $queries["soaptipo"] : false;
        if ($soaptipo)
            array_push($where, " ev1.soaptipo LIKE '%$soaptipo%' ");

        $soapretorno = (isset($queries["soapretorno"])) ? $queries["soapretorno"] : false;
        if ($soapretorno)
            array_push($where, " ev1.soapretorno LIKE '%$soapretorno%' ");

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY ev1." . $sorting[0] . " " . $sorting[1];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "ev1.*, e1.escola, esa1.sala, esa1.quantidade, esa1.numerosala, ess1.serie, ess1.quantidadesaulas, et1.turma, et1.cor, ep1.periodo, ep1.horarioinicio, ep1.horariofim";

        if ($total)
            $fields = "COUNT(ev1.id) as total";

        $ordem = "ORDER BY ev1.id DESC";
        if ($order)
            $ordem = $order;

        $group = '';
        if ($groupby)
            $group = $groupby;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields
            FROM escolasvinculos ev1
	    LEFT JOIN escolas e1 ON e1.id=ev1.idescola
            LEFT JOIN escolassalas esa1 ON esa1.id=ev1.idsala
            LEFT JOIN escolasseries ess1 ON ess1.id = ev1.idserie
            LEFT JOIN escolasturmas et1 ON et1.id=ev1.idturma
            LEFT JOIN escolasperiodos ep1 ON ep1.id=ev1.idperiodo
            WHERE ev1.excluido='nao'
            AND e1.excluido = 'nao'
            $w
            $group
            $ordem
            $limit";
        
        //echo '<pre>'; print_r($strsql); echo '</pre>'; die();
        
        //if(!$total)die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;

        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEscolasvinculosprofessor($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();
        
        $fields = "ev1.*, e1.escola, esa1.sala, esa1.quantidade, esa1.numerosala, ess1.serie, ess1.quantidadesaulas, et1.turma, et1.cor, ep1.periodo, ep1.horarioinicio, ep1.horariofim";
        
        $strsql =  "SELECT $fields  FROM escolasvinculos ev1
                    LEFT JOIN escolas e1 ON e1.id=ev1.idescola
                    LEFT JOIN escolassalas esa1 ON esa1.id=ev1.idsala
                    LEFT JOIN escolasseries ess1 ON ess1.id = ev1.idserie
                    LEFT JOIN escolasturmas et1 ON et1.id=ev1.idturma
                    LEFT JOIN escolasperiodos ep1 ON ep1.id=ev1.idperiodo
                    LEFT JOIN escolasvinculosprofessoresmaterias evpm ON evpm.idescolavinculo = ev1.id
                    WHERE ev1.excluido='nao'
                    AND e1.excluido = 'nao'
                    AND ( ev1.idescola = $queries[idescola]  AND evpm.idprofessor = $queries[idprofessor] AND ev1.idserie = $queries[idserie] ) ";
       
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEscolavinculoById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getEscolasvinculos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getEscolavinculoByIdHelper($id, $queries = array()) {
        $rows = new Escolasvinculos();
        return $rows->getEscolavinculoById($id, $queries);
    }

    public function getNomeExtenso($idescola, $idserie, $queries = array()) {
        if ($idescola == 0)
            return false;
        if ($idserie == 0)
            return false;


        $queries['idescola'] = $idescola;
        $queries['idserie'] = $idserie;
        $rows = $this->getEscolasvinculos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        $row = $rows[0];

        $nome = (($row['serie'] == '0') ? 'AEE' : $row['serie'] ) . ' -> ' . $row['periodo'];
        return $nome;
    }

    public static function getNomeExtensoHelper($idescola, $idserie, $queries = array()) {
        $rows = new Escolasvinculos();
        return $rows->getNomeExtenso($idescola, $idserie, $queries);
    }

    public static function getIdsSeriesByIdsVinculosHelper($ids) {
        $rows = new Escolasvinculosprofessoresmaterias();

        $strsql = "SELECT GROUP_CONCAT(idserie) as ids FROM escolasvinculos e1 WHERE FIND_IN_SET(e1.id, '$ids')";

        $db = Zend_Registry::get('db');

        $row = $db->fetchRow($strsql);
        return $row['ids'];
    }

    public function getSeriesByEscolaId($id) {
        $idescola = $id;

        $where = array();
        $grp = '';
        if ($idescola != '')
            array_push($where, " es.idescola = $id");
        if ($idescola == '')
            $grp = " GROUP BY es.serie ";

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $strsql = "SELECT * FROM escolasseries es
				   WHERE es.status = 'Ativo'
				   $w
				   $grp";

        $db = Zend_Registry::get('db');
        $row = $db->fetchAll($strsql);

        return $row;
    }

    public static function getSeriesByEscolaIdHelper($id) {
        $rows = new Escolasvinculos();
        return $rows->getSeriesByEscolaId($id);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasvinculos
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idsecretaria = (array_key_exists("idsecretaria", $dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
        $row->idescola = (array_key_exists("idescola", $dados)) ? $dados["idescola"] : $row->idescola;
        $row->idsala = (array_key_exists("idsala", $dados)) ? $dados["idsala"] : $row->idsala;
        $row->idserie = (array_key_exists("idserie", $dados)) ? $dados["idserie"] : $row->idserie;
        $row->idturma = (array_key_exists("idturma", $dados)) ? $dados["idturma"] : $row->idturma;
        $row->idperiodo = (array_key_exists("idperiodo", $dados)) ? $dados["idperiodo"] : $row->idperiodo;
        $row->idsmaterias = (array_key_exists("idsmaterias", $dados)) ? $dados["idsmaterias"] : $row->idsmaterias;
        $row->idprofessor = (array_key_exists("idprofessor", $dados)) ? $dados["idprofessor"] : $row->idprofessor;
        $row->idtipoensino = (array_key_exists("idtipoensino", $dados)) ? $dados["idtipoensino"] : $row->idtipoensino;
        $row->inicioaulas = (array_key_exists("inicioaulas", $dados)) ? $dados["inicioaulas"] : $row->inicioaulas;
        $row->terminoaulas = (array_key_exists("terminoaulas", $dados)) ? $dados["terminoaulas"] : $row->terminoaulas;
        $row->turno = (array_key_exists("turno", $dados)) ? $dados["turno"] : $row->turno;
        $row->ano = (array_key_exists("ano", $dados)) ? $dados["ano"] : $row->ano;
        $row->integracao = (array_key_exists("integracao", $dados)) ? $dados["integracao"] : $row->integracao;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        $row->idssedatividadescomplementares = (array_key_exists("idssedatividadescomplementares", $dados)) ? $dados["idssedatividadescomplementares"] : $row->idssedatividadescomplementares;
        $row->idssedatividadescomplementares = $row->idssedatividadescomplementares != '' ? $row->idssedatividadescomplementares : null;
        $row->classe = (array_key_exists("classe", $dados)) ? $dados["classe"] : $row->classe;
        $row->soapstatus = (array_key_exists("soapstatus", $dados)) ? $dados["soapstatus"] : $row->soapstatus;
        $row->soaptipo = (array_key_exists("soaptipo", $dados)) ? $dados["soaptipo"] : $row->soaptipo;
        $row->soapretorno = (array_key_exists("soapretorno", $dados)) ? $dados["soapretorno"] : $row->soapretorno;
        $row->idtipodeclasse = (array_key_exists("idtipodeclasse", $dados)) ? $dados["idtipodeclasse"] : $row->idtipodeclasse;

        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;

        $row->save();

        return $row;
    }

}
