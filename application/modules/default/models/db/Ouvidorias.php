<?php

/**
 * Define o modelo Ouvidorias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Ouvidorias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "ouvidorias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getOuvidoriasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$ouvidorias = new Ouvidorias();
		return $ouvidorias->getOuvidorias($queries, $page, $maxpage);
	}
	
	public function getOuvidorias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " o1.id = $id ");
		
		
		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " o1.iddepartamento = $iddepartamento ");

$codigo = (isset($queries["codigo"])) ? $queries["codigo"] : false;
		if ($codigo) array_push($where, " o1.codigo LIKE '%$codigo%' ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " o1.tipo LIKE '%$tipo%' ");

$email = (isset($queries["email"])) ? $queries["email"] : false;
		if ($email) array_push($where, " o1.email LIKE '%$email%' ");

$reclamante = (isset($queries["reclamante"])) ? $queries["reclamante"] : false;
		if ($reclamante) array_push($where, " o1.reclamante LIKE '%$reclamante%' ");

$reclamacoes = (isset($queries["reclamacoes"])) ? $queries["reclamacoes"] : false;
		if ($reclamacoes) array_push($where, " o1.reclamacoes = '$reclamacoes' ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " o1.observacoes = '$observacoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " o1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "o1.*"; 
		;
		
		if ($total) $fields = "COUNT(o1.id) as total";
		
		$ordem = "ORDER BY o1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM ouvidorias o1
					
					WHERE o1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getOuvidoriaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getOuvidorias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getOuvidoriaByIdHelper($id, $queries = array()) {
		$rows = new Ouvidorias();
		return $rows->getOuvidoriaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ouvidorias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
			$historico = new Ouvidoriahistoricos();			
			$_historico = $row->toArray();
			if(array_key_exists('idsimagens',$dados)){
				$_historico['idsimagens'] = implode(',', $dados['idsimagens']);
			}
			$historico->arquiva($_historico);
		} 
		
		 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->codigo = (array_key_exists("codigo",$dados)) ? $dados["codigo"] : $row->codigo;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->email = (array_key_exists("email",$dados)) ? $dados["email"] : $row->email;
 $row->reclamante = (array_key_exists("reclamante",$dados)) ? $dados["reclamante"] : $row->reclamante;
 $row->reclamacoes = (array_key_exists("reclamacoes",$dados)) ? $dados["reclamacoes"] : $row->reclamacoes;
 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}