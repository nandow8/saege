




<?php

/**
 * Define o modelo Recepcaogeralvisitas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Recepcaogeralvisitas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "recepcaogeralvisitas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getRecepcaogeralvisitasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$recepcaogeralvisitas = new Recepcaogeralvisitas();
		return $recepcaogeralvisitas->getRecepcaogeralvisitas($queries, $page, $maxpage);
	}
	
	public function getRecepcaogeralvisitas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");
		
		
		$dataentrada_i = (isset($queries["dataentrada_i"])) ? $queries["dataentrada_i"] : false;
		if ($dataentrada_i) array_push($where, " r1.dataentrada >= '$dataentrada_i' ");

$dataentrada_f = (isset($queries["dataentrada_f"])) ? $queries["dataentrada_f"] : false;
		if ($dataentrada_f) array_push($where, " r1.dataentrada <= '$dataentrada_f' ");

$horaentrada = (isset($queries["horaentrada"])) ? $queries["horaentrada"] : false;
		if ($horaentrada) array_push($where, " r1.horaentrada = '$horaentrada' ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " r1.tipo LIKE '%$tipo%' ");

$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " r1.iddepartamento = $iddepartamento ");

$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " r1.idfuncionario = $idfuncionario ");

$cpf = (isset($queries["cpf"])) ? $queries["cpf"] : false;
		if ($cpf) array_push($where, " r1.cpf LIKE '%$cpf%' ");

$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
		if ($nome) array_push($where, " r1.nome LIKE '%$nome%' ");

$setor = (isset($queries["setor"])) ? $queries["setor"] : false;
		if ($setor) array_push($where, " r1.setor LIKE '%$setor%' ");

$falarcom = (isset($queries["falarcom"])) ? $queries["falarcom"] : false;
		if ($falarcom) array_push($where, " r1.falarcom LIKE '%$falarcom%' ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " r1.observacoes = '$observacoes' ");

$assunto = (isset($queries["assunto"])) ? $queries["assunto"] : false;
		if ($assunto) array_push($where, " r1.assunto LIKE '%$assunto%' ");

$datasaida_i = (isset($queries["datasaida_i"])) ? $queries["datasaida_i"] : false;
		if ($datasaida_i) array_push($where, " r1.datasaida >= '$datasaida_i' ");

$datasaida_f = (isset($queries["datasaida_f"])) ? $queries["datasaida_f"] : false;
		if ($datasaida_f) array_push($where, " r1.datasaida <= '$datasaida_f' ");

$horasaida = (isset($queries["horasaida"])) ? $queries["horasaida"] : false;
		if ($horasaida) array_push($where, " r1.horasaida = '$horasaida' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " r1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*"; 
		;
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM recepcaogeralvisitas r1
					
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getRecepcaogeralvisitaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getRecepcaogeralvisitas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getRecepcaogeralvisitaByIdHelper($id, $queries = array()) {
		$rows = new Recepcaogeralvisitas();
		return $rows->getRecepcaogeralvisitaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Recepcaogeralvisitas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
 $row->cpf = (array_key_exists("cpf",$dados)) ? $dados["cpf"] : $row->cpf;
 $row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
 $row->idfoto = (array_key_exists("idfoto",$dados)) ? $dados["idfoto"] : $row->idfoto;
 $row->idlocais = (array_key_exists("idlocais",$dados)) ? $dados["idlocais"] : $row->idlocais;
 
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}