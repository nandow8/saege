<?php

/**
 * Define o modelo Censonovosalunos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Censonovosalunos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "censonovosalunos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCensonovosalunosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$censonovosalunos = new Censonovosalunos();
		return $censonovosalunos->getCensonovosalunos($queries, $page, $maxpage);
	}
	
	public function getCensonovosalunos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();

	
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		$idcentralvaga = (isset($queries["idcentralvaga"])) ? $queries["idcentralvaga"] : false;
		if ($idcentralvaga) array_push($where, " c1.idcentralvaga = $idcentralvaga ");

		$aluno = (isset($queries["aluno"])) ? $queries["aluno"] : false;
		if ($aluno) array_push($where, " c1.aluno LIKE '%$aluno%' ");

		//codigoinscricao
		$codigoinscricao = (isset($queries["codigoinscricao"])) ? $queries["codigoinscricao"] : false;
		if ($codigoinscricao) array_push($where, " c1.codigoinscricao LIKE '%$codigoinscricao%' ");

		//email
		$email = (isset($queries["email"])) ? $queries["email"] : false;
		if ($email) array_push($where, " c1.email LIKE '%$email%' ");

		// id_tipo_ensino
		$id_tipo_ensino = (isset($queries["id_tipo_ensino"])) ? $queries["id_tipo_ensino"] : false;
		if ($id_tipo_ensino) array_push($where, " c1.id_tipo_ensino LIKE '%$id_tipo_ensino%' ");

		// statusprofissional
		$statusprofissional = (isset($queries["statusprofissional"])) ? $queries["statusprofissional"] : false;
		if ($statusprofissional) array_push($where, " c1.statusprofissional LIKE '%$statusprofissional%' ");

		// id_tipo_ensino
		$aluguel = (isset($queries["aluguel"])) ? $queries["aluguel"] : false;
		if ($aluguel) array_push($where, " c1.aluguel LIKE '%$aluguel%' ");

$datanascimento_i = (isset($queries["datanascimento_i"])) ? $queries["datanascimento_i"] : false;
		if ($datanascimento_i) array_push($where, " c1.datanascimento >= '$datanascimento_i' ");

$datanascimento_f = (isset($queries["datanascimento_f"])) ? $queries["datanascimento_f"] : false;
		if ($datanascimento_f) array_push($where, " c1.datanascimento <= '$datanascimento_f' ");

$sexo = (isset($queries["sexo"])) ? $queries["sexo"] : false;
		if ($sexo) array_push($where, " c1.sexo LIKE '%$sexo%' ");

$distancia = (isset($queries["distancia"])) ? $queries["distancia"] : false;
		if ($distancia) array_push($where, " c1.distancia LIKE '%$distancia%' ");

$escola = (isset($queries["escola"])) ? $queries["escola"] : false;
		if ($escola) array_push($where, " c1.escola LIKE '%$escola%' ");

$estado = (isset($queries["estado"])) ? $queries["estado"] : false;
		if ($estado) array_push($where, " c1.estado LIKE '%$estado%' ");

$matricula = (isset($queries["matricula"])) ? $queries["matricula"] : false;
		if ($matricula) array_push($where, " c1.matricula LIKE '%$matricula%' ");

$id_escola = (isset($queries["id_escola"])) ? $queries["id_escola"] : false;
		if ($id_escola) array_push($where, " c1.id_escola = '$id_escola' ");

$filiacao1 = (isset($queries["filiacao1"])) ? $queries["filiacao1"] : false;
		if ($filiacao1) array_push($where, " c1.filiacao1 LIKE '%$filiacao1%' ");

$nomefiliacao1 = (isset($queries["nomefiliacao1"])) ? $queries["nomefiliacao1"] : false;
		if ($nomefiliacao1) array_push($where, " c1.nomefiliacao1 LIKE '%$nomefiliacao1%' ");

$filiacao2 = (isset($queries["filiacao2"])) ? $queries["filiacao2"] : false;
		if ($filiacao2) array_push($where, " c1.filiacao2 LIKE '%$filiacao2%' ");

$nomefiliacao2 = (isset($queries["nomefiliacao2"])) ? $queries["nomefiliacao2"] : false;
		if ($nomefiliacao2) array_push($where, " c1.nomefiliacao2 LIKE '%$nomefiliacao2%' ");

$painaodeclarado = (isset($queries["painaodeclarado"])) ? $queries["painaodeclarado"] : false;
		if ($painaodeclarado) array_push($where, " c1.painaodeclarado LIKE '%$painaodeclarado%' ");

$temnomesocial = (isset($queries["temnomesocial"])) ? $queries["temnomesocial"] : false;
		if ($temnomesocial) array_push($where, " c1.temnomesocial LIKE '%$temnomesocial%' ");

$nomesocial = (isset($queries["nomesocial"])) ? $queries["nomesocial"] : false;
		if ($nomesocial) array_push($where, " c1.nomesocial LIKE '%$nomesocial%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");

$portadorespecialidade = (isset($queries["portadorespecialidade"])) ? $queries["portadorespecialidade"] : false;
		if ($portadorespecialidade) array_push($where, " c1.portadorespecialidade LIKE '%$portadorespecialidade%' ");

$etapa_ensino = (isset($queries["etapa_ensino"])) ? $queries["etapa_ensino"] : false;
		if ($etapa_ensino) array_push($where, " c1.etapa_ensino LIKE '%$etapa_ensino%' ");

$datadecorte = (isset($queries["datadecorte"])) ? $queries["datadecorte"] : false;
		if ($datadecorte) array_push($where, " c1.datadecorte LIKE '%$datadecorte%' ");

$painaodeclarado = (isset($queries["painaodeclarado"])) ? $queries["painaodeclarado"] : false;
		if ($painaodeclarado) array_push($where, " c1.painaodeclarado LIKE '%$painaodeclarado%' ");
 
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";

		
		$ordem = "ORDER BY c1.matricula DESC";

		/* Inclusão dos dados de ordem para classificação */

		if(isset($queries['distancia_tipo'])){

			if($queries['distancia_tipo'] == "crescente"){
				$ordem = "ORDER BY distancia ASC";
			}elseif($queries['distancia_tipo'] == "decrescente"){
				$ordem = "ORDER BY distancia DESC";
			}//end if 
			
		}//end if
		
		 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM censonovosalunos c1
					
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";
					

		

		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}
		
		return $db->fetchAll($strsql);			
	}	

	public static function getFilademandacrecheHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$censonovosalunos = new Censonovosalunos();
		return $censonovosalunos->getFilademandacreche($queries, $page, $maxpage);
	}
	
	public function getFilademandacreche($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$idcentralvaga = (isset($queries["idcentralvaga"])) ? $queries["idcentralvaga"] : false;
		if ($idcentralvaga) array_push($where, " c1.idcentralvaga = $idcentralvaga ");
 
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM censonovosalunos c1
					
					WHERE c1.excluido='nao' AND c1.matricula='Fila' AND c1.etapa_ensino = 'Creche'
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}
	
	public static function getFilademandaalunoHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$censonovosalunos = new Censonovosalunos();
		return $censonovosalunos->getFilademandaaluno($queries, $page, $maxpage);
	}
	
	public function getFilademandaaluno($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");

		$escola = (isset($queries['escola'])) ? (int)$queries['escola'] : false;
		if ($escola) array_push($where, " c1.escola = $escola ");

		// $matricula = (isset($queries['matricula'])) ? (int)$queries['matricula'] : false;
		// if ($matricula) array_push($where, " c1.matricula = $matricula ");
		
		
		$idcentralvaga = (isset($queries["idcentralvaga"])) ? $queries["idcentralvaga"] : false;
		if ($idcentralvaga) array_push($where, " c1.idcentralvaga = $idcentralvaga ");
 
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

	 
 
		$strsql = "SELECT $fields 
					FROM censonovosalunos c1
					
					WHERE c1.excluido='nao' AND c1.matricula='Fila' AND c1.etapa_ensino = 'Aluno'
						$w 
					$ordem	
					$limit";

		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}

	
	
	public function getXensonovosalunoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCensonovosalunos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getXensonovosalunoByIdHelper($id, $queries = array()) {
		$rows = new Censonovosalunos();
		return $rows->getXensonovosalunoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Censonovosalunos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idcentralvaga = (array_key_exists("idcentralvaga",$dados)) ? $dados["idcentralvaga"] : $row->idcentralvaga;
 $row->aluno = (array_key_exists("aluno",$dados)) ? $dados["aluno"] : $row->aluno;
 $row->datanascimento = (array_key_exists("datanascimento",$dados)) ? $dados["datanascimento"] : $row->datanascimento;
 $row->sexo = (array_key_exists("sexo",$dados)) ? $dados["sexo"] : $row->sexo;
 $row->filiacao1 = (array_key_exists("filiacao1",$dados)) ? $dados["filiacao1"] : $row->filiacao1;
 $row->nomefiliacao1 = (array_key_exists("nomefiliacao1",$dados)) ? $dados["nomefiliacao1"] : $row->nomefiliacao1;
 $row->filiacao2 = (array_key_exists("filiacao2",$dados)) ? $dados["filiacao2"] : $row->filiacao2;
 $row->nomefiliacao2 = (array_key_exists("nomefiliacao2",$dados)) ? $dados["nomefiliacao2"] : $row->nomefiliacao2;
 $row->painaodeclarado = (array_key_exists("painaodeclarado",$dados)) ? $dados["painaodeclarado"] : $row->painaodeclarado;
 $row->temnomesocial = (array_key_exists("temnomesocial",$dados)) ? $dados["temnomesocial"] : $row->temnomesocial;
 $row->nomesocial = (array_key_exists("nomesocial",$dados)) ? $dados["nomesocial"] : $row->nomesocial;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 
 $row->cpffiliacao = (array_key_exists("cpffiliacao",$dados)) ? $dados["cpffiliacao"] : $row->cpffiliacao;
 $row->cpfaluno = (array_key_exists("cpfaluno",$dados)) ? $dados["cpfaluno"] : $row->cpfaluno;
 $row->datanascimentoresponsavel = (array_key_exists("datanascimentoresponsavel",$dados)) ? $dados["datanascimentoresponsavel"] : $row->datanascimentoresponsavel;
 $row->cep = (array_key_exists("cep",$dados)) ? $dados["cep"] : $row->cep;
 $row->endereco = (array_key_exists("endereco",$dados)) ? $dados["endereco"] : $row->endereco;
 $row->numero = (array_key_exists("numero",$dados)) ? $dados["numero"] : $row->numero;
 $row->complemento = (array_key_exists("complemento",$dados)) ? $dados["complemento"] : $row->complemento;
 $row->bairro = (array_key_exists("bairro",$dados)) ? $dados["bairro"] : $row->bairro;
 $row->estado =     (array_key_exists("estado",$dados)) ? $dados["estado"] : $row->estado;
 $row->cidade =  (array_key_exists("cidade",$dados)) ? $dados["cidade"] : $row->cidade;
 $row->tipotelefone1 = (array_key_exists("tipotelefone1",$dados)) ? $dados["tipotelefone1"] : $row->tipotelefone1;
 $row->numerotelefone1 = (array_key_exists("numerotelefone1",$dados)) ? $dados["numerotelefone1"] : $row->numerotelefone1;
 $row->tipotelefone2 = (array_key_exists("tipotelefone2",$dados)) ? $dados["tipotelefone2"] : $row->tipotelefone2;
 $row->numerotelefone2 = (array_key_exists("numerotelefone2",$dados)) ? $dados["numerotelefone2"] : $row->numerotelefone2;
 $row->email = (array_key_exists("email",$dados)) ? $dados["email"] : $row->email;
 $row->codigoinscricao = (array_key_exists("codigoinscricao",$dados)) ? $dados["codigoinscricao"] : $row->codigoinscricao;
 $row->portadorespecialidade = (array_key_exists("portadorespecialidade",$dados)) ? $dados["portadorespecialidade"] : $row->portadorespecialidade;
 $row->auxiliosocial = (array_key_exists("auxiliosocial",$dados)) ? $dados["auxiliosocial"] : $row->auxiliosocial;
 $row->nis = (array_key_exists("nis",$dados)) ? $dados["nis"] : $row->nis;
 $row->rendafamiliar = (array_key_exists("rendafamiliar",$dados)) ? $dados["rendafamiliar"] : $row->rendafamiliar;
 $row->moradoresresidencia = (array_key_exists("moradoresresidencia",$dados)) ? $dados["moradoresresidencia"] : $row->moradoresresidencia;
 $row->statusprofissional = (array_key_exists("statusprofissional",$dados)) ? $dados["statusprofissional"] : $row->statusprofissional;
 $row->escola = (array_key_exists("escola",$dados)) ? $dados["escola"] : $row->escola;
 $row->etapa_ensino = (array_key_exists("etapa_ensino",$dados)) ? $dados["etapa_ensino"] : $row->etapa_ensino;
 $row->datadecorte = (array_key_exists("datadecorte",$dados)) ? $dados["datadecorte"] : $row->datadecorte;
 $row->matricula = (array_key_exists("matricula",$dados)) ? $dados["matricula"] : $row->matricula;
 $row->tipodeficiencia = (array_key_exists("tipodeficiencia",$dados)) ? $dados["tipodeficiencia"] : $row->tipodeficiencia;
 $row->observacaodeficiencia = (array_key_exists("observacaodeficiencia",$dados)) ? $dados["observacaodeficiencia"] : $row->observacaodeficiencia;
 $row->etnia = (array_key_exists("etnia",$dados)) ? $dados["etnia"] : $row->etnia;
 //$row->quilombola = (array_key_exists("quilombola",$dados)) ? $dados["quilombola"] : $row->quilombola;
 $row->latitude = (array_key_exists("latitude",$dados)) ? $dados["latitude"] : $row->latitude;
 $row->longitude = (array_key_exists("longitude",$dados)) ? $dados["longitude"] : $row->longitude;
 
 $row->distancia = (array_key_exists("distancia",$dados)) ? $dados["distancia"] : $row->distancia;   
 $row->id_escola = (array_key_exists("id_escola",$dados)) ? $dados["id_escola"] : $row->id_escola;
 $row->id_tipo_ensino = (array_key_exists("id_tipo_ensino",$dados)) ? $dados["id_tipo_ensino"] : $row->id_tipo_ensino;
 $row->nome_tipo_ensino = (array_key_exists("nome_tipo_ensino",$dados)) ? $dados["nome_tipo_ensino"] : $row->nome_tipo_ensino;
 $row->distancia = (array_key_exists("distanciaset",$dados)) ? $dados["distanciaset"] : $row->distancia;
  
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}
