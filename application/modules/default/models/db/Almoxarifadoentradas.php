<?php

/**
 * Define o modelo Almoxarifadoentradas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Almoxarifadoentradas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoentradas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAlmoxarifadoentradasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$almoxarifadoentradas = new Almoxarifadoentradas();
		return $almoxarifadoentradas->getAlmoxarifadoentradas($queries, $page, $maxpage);
	}
	
	public function getAlmoxarifadoentradas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " a1.idsecretaria = $idsecretaria ");

$aprovacao = (isset($queries["aprovacao"])) ? $queries["aprovacao"] : false;
		if ($aprovacao) array_push($where, " a1.aprovacao LIKE '%$aprovacao%' ");

$confirmaaprovacao = (isset($queries["confirmaaprovacao"])) ? $queries["confirmaaprovacao"] : false;
		if ($confirmaaprovacao) array_push($where, " a1.confirmaaprovacao LIKE '%$confirmaaprovacao%' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " a1.titulo LIKE '%$titulo%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " a1.descricoes = '$descricoes' ");

$iddepartamentosecretaria = (isset($queries["iddepartamentosecretaria"])) ? $queries["iddepartamentosecretaria"] : false;
		if ($iddepartamentosecretaria) array_push($where, " a1.iddepartamentosecretaria = $iddepartamentosecretaria ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " a1.idescola = $idescola ");

$iddepartamentoescola = (isset($queries["iddepartamentoescola"])) ? $queries["iddepartamentoescola"] : false;
		if ($iddepartamentoescola) array_push($where, " a1.iddepartamentoescola = $iddepartamentoescola ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadoentradas a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAlmoxarifadoentradaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAlmoxarifadoentradas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAlmoxarifadoentradaByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifadoentradas();
		return $rows->getAlmoxarifadoentradaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadoentradas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->idsaida = (array_key_exists("idsaida",$dados)) ? $dados["idsaida"] : $row->idsaida;
 $row->aprovacao = (array_key_exists("aprovacao",$dados)) ? $dados["aprovacao"] : $row->aprovacao;
 $row->confirmaaprovacao = (array_key_exists("confirmaaprovacao",$dados)) ? $dados["confirmaaprovacao"] : $row->confirmaaprovacao;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->iddepartamentosecretaria = (array_key_exists("iddepartamentosecretaria",$dados)) ? $dados["iddepartamentosecretaria"] : $row->iddepartamentosecretaria;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->iddepartamentoescola = (array_key_exists("iddepartamentoescola",$dados)) ? $dados["iddepartamentoescola"] : $row->iddepartamentoescola;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		if (array_key_exists('itens', $dados)) $this->setItens($dados['itens'], $row->id, $row->logusuario, $row->logdata);
		return $row;
	}
	

	private function setItens($dados, $identrada, $logusuario = false, $logdata = false) {
		if (!is_array($dados)) return;
		 
		$itens = new Almoxarifado_Entradasitens();
		$ids = $dados['idsentradas'];
		$idsprodutos = $dados['idsprodutos'];
		$idsfornecedores = $dados['idsfornecedores'];
		$quantidades = $dados['quantidades'];
		$itensstatus = $dados['itensstatus'];
		$itensobservacoes = $dados['itensobservacoes'];
		
		$ids = array();
		foreach ($idsprodutos as $i=>$id) {
			$d = array();
			
			$d['id'] = $ids[$i];
			$d['identrada'] = $identrada; 
			$d['idproduto'] = trim(strip_tags($idsprodutos[$i])); 
			$d['idfornecedor'] = trim(strip_tags($idsfornecedores[$i]));
			$d['quantidade'] = trim(strip_tags(Mn_Util::trataNum($quantidades[$i])));
			$d['observacoes'] = trim(strip_tags($itensobservacoes[$i]));
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $logusuario;
			$d['logdata'] = $logdata;
			//var_dump($d); die();
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
			//verificar as linhas comentadas acima, elas são responsaveis pelas 2 linhas mais cridas no cronogramasliehistorico
		}

		
		$itens = implode(",", $ids);
		
		if ($itens=="") $itens = "0";
		$strsql = "DELETE FROM almoxarifadoentradasitens WHERE identrada=$identrada AND id NOT IN ($itens)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);	
			
	}

}