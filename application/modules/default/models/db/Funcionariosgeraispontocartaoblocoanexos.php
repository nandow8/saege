<?php

/**
 * Define o modelo Atas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br) 
 * @version     1.0
 */
class Funcionariosgeraispontocartaoblocoanexos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosgeraispontocartaoblocoanexos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFuncionariosgeraispontocartaoblocoanexosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosgeraispontocartaoblocoanexos = new Funcionariosgeraispontocartaoblocoanexos();
		return $funcionariosgeraispontocartaoblocoanexos->getAtas($queries, $page, $maxpage);
	}
	
	public function getFuncionariosgeraispontocartaoblocoanexos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
        
        
// datainiciobloco	date			Sim	None		
$datainiciobloco = (isset($queries["datainiciobloco"])) ? $queries["datainiciobloco"] : false;
		if ($datainiciobloco) array_push($where, " a1.datainiciobloco = '$datainiciobloco' ");
// datafimbloco	date			Sim	None		
$datafimbloco = (isset($queries["datafimbloco"])) ? $queries["datafimbloco"] : false;
		if ($datafimbloco) array_push($where, " a1.datafimbloco = '$datafimbloco' ");
// usuarioid	varchar(15)	utf8_general_ci		Sim	None
$usuarioid = (isset($queries["usuarioid"])) ? $queries["usuarioid"] : false;
		if ($usuarioid) array_push($where, " a1.usuarioid = '$usuarioid' ");
// funcionarioid	varchar(15)	utf8_general_ci		Sim	None		
$funcionarioid = (isset($queries["funcionarioid"])) ? $queries["funcionarioid"] : false;
		if ($funcionarioid) array_push($where, " a1.funcionarioid = '$funcionarioid' ");
// rgf	varchar(15)	utf8_general_ci		Sim	None		
$rgf = (isset($queries["rgf"])) ? $queries["rgf"] : false;
		if ($rgf) array_push($where, " a1.rgf LIKE '%$rgf%' ");
// confirmacaorh	varchar(20)	utf8_general_ci		Não	None		

// idarquivofolhaponto	bigint(20)			Sim	None	label: Anexo	

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgeraispontocartaoblocoanexos a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariosgeraispontocartaoblocoanexosById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosgeraispontocartaoblocoanexos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionariosgeraispontocartaoblocoanexosByIdHelper($id, $queries = array()) {
		$rows = new Atas();
		return $rows->getFuncionariosgeraispontocartaoblocoanexosById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Atas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
        } 
        
// datainiciobloco	date			Sim	None		
$row->datainiciobloco = (array_key_exists("datainiciobloco",$dados)) ? $dados["datainiciobloco"] : $row->datainiciobloco;
// datafimbloco	date			Sim	None		
$row->datafimbloco = (array_key_exists("datafimbloco",$dados)) ? $dados["datafimbloco"] : $row->datafimbloco;
// usuarioid	varchar(15)	utf8_general_ci		Sim	None		
$row->usuarioid = (array_key_exists("usuarioid",$dados)) ? $dados["usuarioid"] : $row->usuarioid;
// funcionarioid	varchar(15)	utf8_general_ci		Sim	None		
$row->funcionarioid = (array_key_exists("funcionarioid",$dados)) ? $dados["funcionarioid"] : $row->funcionarioid;
// rgf	varchar(15)	utf8_general_ci		Sim	None		
$row->rgf = (array_key_exists("rgf",$dados)) ? $dados["rgf"] : $row->rgf;
// idlocal varchar(15)	utf8_general_ci		Sim	None		
$row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
// iddeptfuncionario varchar(15)	utf8_general_ci		Sim	None		
$row->iddept = (array_key_exists("iddept",$dados)) ? $dados["iddept"] : $row->iddept;
// confirmacaorh	varchar(20)	utf8_general_ci		Não	None		
$row->confirmacaorh = (array_key_exists("confirmacaorh",$dados)) ? $dados["confirmacaorh"] : $row->confirmacaorh;
// idarquivofolhaponto	bigint(20)			Sim	None	label: Anexo	
$row->idarquivofolhaponto = (array_key_exists("idarquivofolhaponto",$dados)) ? $dados["idarquivofolhaponto"] : $row->idarquivofolhaponto;
// status	varchar(20)	utf8_general_ci		Sim	None	label:Status|filter|grid|required|combo:Ativo,Bloqueado	
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;

				
		$row->save();

		$registro = 'Adicionado';
		if(!$novoRegistro)
			$registro = 'Editado';
			
		Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), " id -> [" . $row->id . "] [" . $registro . "][status=>" . $row->status . "][excluido=>" . $row->excluido . "]");

		
		return $row;
	}
	
}