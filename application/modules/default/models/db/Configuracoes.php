<?php
class Configuracoes extends Zend_Db_Table_Abstract {
	
	protected $_name = "configuracoes";
	protected $_primary = "id";
	
	public static function getPtrecModeloVersao() {
		$versao = (int)self::getConfig('ptrec_modelo_versao');
		return $versao;
	}
	
	public static function incPtrecModeloVersao() {
		$versao = self::getPtrecModeloVersao();
		$versao++; 
		
		$strsql = "UPDATE configuracoes SET valor=$versao WHERE chave='ptrec_modelo_versao'";
		$db = Zend_Registry::get('db');
		$db->query($strsql);
	}
	
	public static function getConfig($chave=null) {
		if (is_null($chave)) {
			$rows = new Configuracoes();
			$rows = $rows->fetchAll("excluido='nao'");
			$r = array();
			foreach ($rows as $row) $r[$row['chave']] = $row['valor'];
			return $r;
		} else {
			$rows = new Configuracoes();
			$row = $rows->fetchRow("chave='$chave'");
			if (!$row) return false;
			return $row["valor"];
		}
	}

	public function getConfiguracoes() {
		$db = Zend_Registry::get('db');
		$strsql = "SELECT c1.* FROM configuracoes c1 WHERE c1.excluido='nao' ORDER BY c1.descricao";
		return $db->fetchAll($strsql);
	}
	
	public function save($dados) {
		if ($dados['id']==0) return;  
		else $row = $this->fetchRow("id=".$dados['id']);
		
		$row->valor = $dados['valor'];
		$row->save();
	}		
	
	
}