<?php

class Escolasalunosprogramas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasalunosprogramas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getProgramasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolasalunosprogramas();
		return $produtos->getProgramas($queries, $page, $maxpage);
	}
	
	public function getProgramas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idprograma = (isset($queries['idprograma'])) ? $queries['idprograma'] : false;
		$idaluno = (isset($queries['idaluno'])) ? $queries['idaluno'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? $queries['idsecretaria'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " ap1.id=$id ");
		if ($idprograma) array_push($where, " ap1.idprograma=$idprograma ");
		if ($idaluno) array_push($where, " ap1.idaluno=$idaluno ");
		if ($idsecretaria) array_push($where, " p1.idsecretaria=$idsecretaria ");
		if ($chave) array_push($where, " ((p1.programa LIKE '%$chave%') OR (a1.nomerazao LIKE '%$chave%') OR (a1.sobrenomefantasia LIKE '%$chave%')) ");
		if ($status) array_push($where, " ap1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "ap1.*, p1.programa, p1.descricoes, a1.nomerazao, a1.sobrenomefantasia";
		if ($total) $fields = "COUNT(ap1.id) as total";
		
		
		$ordem = "ORDER BY ap1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasalunosprogramas ap1
						LEFT JOIN escolasprogramassociais p1 ON p1.id=ap1.idprograma
						LEFT JOIN escolasalunos a1 ON a1.id=ap1.idaluno
					WHERE ap1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getProgramaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getProgramas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getProgramaByIdHelper($id, $queries = array()) {
		$rows = new Escolasalunosprogramas();
		return $rows->getProgramaById($id, $queries);
	}		
	

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Programas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s'); 		
		}else {
			$novoRegistro = false;
		} 

		$row->idaluno = (array_key_exists('idaluno',$dados)) ? $dados['idaluno'] : $row->idaluno;
		$row->idprograma = (array_key_exists('idprograma',$dados)) ? $dados['idprograma'] : $row->idprograma;		
		$row->situacao = (array_key_exists('situacao',$dados)) ? $dados['situacao'] : $row->situacao;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
	
	
}