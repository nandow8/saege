<?php

/**
 * Define o modelo Agendaescolas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Agendaescolas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "agendaescolas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAgendaescolasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$agendaescolas = new Agendaescolas();
		return $agendaescolas->getAgendaescolas($queries, $page, $maxpage);
	}
	
	public function getAgendaescolas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " a1.idescola = $idescola ");

$idresponsavel = (isset($queries["idresponsavel"])) ? $queries["idresponsavel"] : false;
		if ($idresponsavel) array_push($where, " a1.idresponsavel = $idresponsavel ");

$nomerazao = (isset($queries["nomerazao"])) ? $queries["nomerazao"] : false;
		if ($nomerazao) array_push($where, " a1.nomerazao LIKE '%$nomerazao%' ");

$sobrenomefantasia = (isset($queries["sobrenomefantasia"])) ? $queries["sobrenomefantasia"] : false;
		if ($sobrenomefantasia) array_push($where, " a1.sobrenomefantasia LIKE '%$sobrenomefantasia%' ");

$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
		if ($telefone) array_push($where, " a1.telefone LIKE '%$telefone%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM agendaescolas a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAgendaescolaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAgendaescolas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAgendaescolaByIdHelper($id, $queries = array()) {
		$rows = new Agendaescolas();
		return $rows->getAgendaescolaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Agendaescolas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idresponsavel = (array_key_exists("idresponsavel",$dados)) ? $dados["idresponsavel"] : $row->idresponsavel;
 $row->idendereco = (array_key_exists("idendereco",$dados)) ? $dados["idendereco"] : $row->idendereco;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->nomerazao = (array_key_exists("nomerazao",$dados)) ? $dados["nomerazao"] : $row->nomerazao;
 $row->sobrenomefantasia = (array_key_exists("sobrenomefantasia",$dados)) ? $dados["sobrenomefantasia"] : $row->sobrenomefantasia;
 $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
 $row->telefone2 = (array_key_exists("telefone2",$dados)) ? $dados["telefone2"] : $row->telefone2;
 $row->local = (array_key_exists("local",$dados)) ? $dados["local"] : $row->local;
 $row->responsavel = (array_key_exists("responsavel",$dados)) ? $dados["responsavel"] : $row->responsavel;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}