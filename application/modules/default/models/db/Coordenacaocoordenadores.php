<?php

/**
 * Define o modelo Coordenacaocoordenadores
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Coordenacaocoordenadores extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "coordenacaocoordenadores";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getCoordenacaocoordenadoresNDHelper($queries = array(), $page = 0, $maxpage = 0) {
        $coordenacaocoordenadores = new Coordenacaocoordenadores();
        return $coordenacaocoordenadores->getCoordenacaocoordenadores($queries, $page, $maxpage);
    }

    public static function getCoordenacaocoordenadoresHelper($queries = array(), $page = 0, $maxpage = 0) {
        $coordenacaocoordenadores = new Coordenacaocoordenadores();
        return $coordenacaocoordenadores->getCoordenacaocoordenadores($queries, $page, $maxpage);
    }

    public function getCoordenacaocoordenadores($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " c1.id = $id ");


        $idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
        if ($idsecretaria)
            array_push($where, " c1.idsecretaria = $idsecretaria ");

        $idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
        if ($idfuncionario)
            array_push($where, " c1.idfuncionario = $idfuncionario ");

        $telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
        if ($telefone)
            array_push($where, " c1.telefone LIKE '%$telefone%' ");

        $email = (isset($queries["email"])) ? $queries["email"] : false;
        if ($email)
            array_push($where, " c1.email LIKE '%$email%' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " c1.status LIKE '%$status%' ");



        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "c1.*";
        ;

        if ($total)
            $fields = "COUNT(c1.id) as total";

        $ordem = "ORDER BY c1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields
                                    FROM coordenacaocoordenadores c1

                                    WHERE c1.excluido='nao'
                                            $w
                                    $ordem
                                    $limit";

        //if(!$total) die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getCoordenacaocoordenadorById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getCoordenacaocoordenadores($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getCoordenacaocoordenadorByIdHelper($id, $queries = array()) {
        $rows = new Coordenacaocoordenadores();
        return $rows->getCoordenacaocoordenadorById($id, $queries);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Coordenacaocoordenadores
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idsecretaria = (array_key_exists("idsecretaria", $dados)) ? $dados["idsecretaria"] : $row->idsecretaria;

        $row->idsescolas = (array_key_exists("idsescolas", $dados)) ? $dados["idsescolas"] : $row->idsescolas;
        $row->idfuncionario = (array_key_exists("idfuncionario", $dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
        $row->nomerazao = (array_key_exists("nomerazao", $dados)) ? $dados["nomerazao"] : $row->nomerazao;
        $row->sobrenomefantasia = (array_key_exists("sobrenomefantasia", $dados)) ? $dados["sobrenomefantasia"] : $row->sobrenomefantasia;
        $row->telefone = (array_key_exists("telefone", $dados)) ? $dados["telefone"] : $row->telefone;
        $row->email = (array_key_exists("email", $dados)) ? $dados["email"] : $row->email;

        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;

        //return true;
        $row->save();

        $this->setCoordenador($row->toArray());
        return $row;
    }

    public function setCoordenador($row) {
        $funcionario = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($row['idfuncionario']);

        $coordenador = "Sim";
        if (isset($funcionario['id'])) {
            if ($row['excluido'] == "sim") {
                $coordenador = "Não";
            }
            $funcionario['coordenador'] = $coordenador;
            $funcionario['logdata'] = $row['logdata'];
            $funcionario['logusuario'] = $row['logusuario'];

            $funcionariosgeraisescolas = new Funcionariosgeraisescolas();
            $funcionariosgeraisescolas->save($funcionario);
        }

        return false;
    }

}
