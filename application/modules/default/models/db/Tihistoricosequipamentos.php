<?php

/**
 * Define o modelo Tihistoricosequipamentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Tihistoricosequipamentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "tihistoricosequipamentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getTihistoricosequipamentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$tihistoricosequipamentos = new Tihistoricosequipamentos();
		return $tihistoricosequipamentos->getTihistoricosequipamentos($queries, $page, $maxpage);
	}
	
	public function getTihistoricosequipamentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " t1.id = $id ");
		
		
		$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " t1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " t1.data <= '$data_f' ");

$idtecnico = (isset($queries["idtecnico"])) ? $queries["idtecnico"] : false;
		if ($idtecnico) array_push($where, " t1.idtecnico = $idtecnico ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " t1.idescola = $idescola ");

$idpatrimonio = (isset($queries["idpatrimonio"])) ? $queries["idpatrimonio"] : false;
		if ($idpatrimonio) array_push($where, " t1.idpatrimonio = $idpatrimonio ");

$trocapecas = (isset($queries["trocapecas"])) ? $queries["trocapecas"] : false;
		if ($trocapecas) array_push($where, " t1.trocapecas LIKE '%$trocapecas%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " t1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "t1.*"; 
		;
		
		if ($total) $fields = "COUNT(t1.id) as total";
		
		$ordem = "ORDER BY t1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM tihistoricosequipamentos t1
					
					WHERE t1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getTihistoricosequipamentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTihistoricosequipamentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTihistoricosequipamentoByIdHelper($id, $queries = array()) {
		$rows = new Tihistoricosequipamentos();
		return $rows->getTihistoricosequipamentoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Tihistoricosequipamentos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->idtecnico = (array_key_exists("idtecnico",$dados)) ? $dados["idtecnico"] : $row->idtecnico;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idpatrimonio = (array_key_exists("idpatrimonio",$dados)) ? $dados["idpatrimonio"] : $row->idpatrimonio;
 $row->trocapecas = (array_key_exists("trocapecas",$dados)) ? $dados["trocapecas"] : $row->trocapecas;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		 		
		$row->save();

		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Tihistoricosequipamentos salvo com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Tihistoricosequipamentos com o ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Tihistoricosequipamentos com o ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Tihistoricosequipamentos com o ID ".$id." atualizada com sucesso!");
		
		return $row;
	}
	
}