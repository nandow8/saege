<?php
 
/**
 * Define o modelo Atas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosgeraispontoocorrencias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosgeraispontoocorrencias";
	
	/**
        * Define a chave primaria
        * @var integer
        */
	protected $_primary = "id";
	
	public static function getAtasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$atas = new Atas();
		return $atas->getAtas($queries, $page, $maxpage);
	}
	
	public function getAtas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
		
                $codigo = (isset($queries["codigo"])) ? $queries["codigo"] : false;
                                if ($codigo) array_push($where, " a1.codigo = $codigo ");

                $ocorrencia = (isset($queries["ocorrencia"])) ? $queries["ocorrencia"] : false;
                                if ($ocorrencia) array_push($where, " a1.ocorrencia LIKE '%$ocorrencia%' ");

                $descricao = (isset($queries["descricao"])) ? $queries["descricao"] : false;
                                if ($descricao) array_push($where, " a1.descricao LIKE '%$descricao%' ");

                $status = (isset($queries["status"])) ? $queries["status"] : false;
                                if ($status) array_push($where, " a1.status LIKE '%$status%' ");


		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgeraispontoocorrencias a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAtaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAtas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAtaByIdHelper($id, $queries = array()) {
		$rows = new Funcionariosgeraispontoocorrencias();
		return $rows->getAtaById($id, $queries);
	}
	
    /**
        * Salva o dados (INSERT OU UPDATE)
        * @param array dados
        * @return Atas
    */
	public function save($dados){
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
                
                //ocorrencia
                $row->ocorrencia = (array_key_exists("ocorrencia",$dados)) ? $dados["ocorrencia"] : $row->ocorrencia;
                //descricao
                $row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
				// prolongado
				$row->periodo = (array_key_exists("periodo",$dados)) ? $dados['periodo'] : $row->prolongado;
				// divergendia
				$row->divergencia = (array_key_exists("divergencia",$dados)) ? $dados["divergencia"] : $row->divergencia;
				// extra
				$row->extra = (array_key_exists("extra",$dados)) ? $dados["extra"] : $row->extra;
				// atraso
				$row->atraso = (array_key_exists("atraso",$dados)) ? $dados["atraso"] : $row->descricao;
			
				
				$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
				if (is_null($row->datacriacao)){
							$row->datacriacao = date("Y-m-d H:i:s");
						}
										
				$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
				$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
				$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
				
		
				
		$row->save();

		$registro = 'Adicionado';
		if(!$novoRegistro)
			$registro = 'Editado';
			
		Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), " id -> [" . $row->id . "] [" . $registro . "][status=>" . $row->status . "][excluido=>" . $row->excluido . "]");
		
		
		return $row;
	}

	public static function getpontoocorrencia($ocorrencia){

		$querie = "select * from funcionariosgeraispontoocorrencias where ocorrencia like '%" . $ocorrencia . "%'";

		$db = Zend_Registry::get('db');				
		
		return $db->fetchAll($querie);	


	}//end function 
	
}