<?php

/**
 * Define o modelo Educacaoespecialrespostas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Educacaoespecialrespostas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "educacaoespecialrespostas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEducacaoespecialrespostasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$educacaoespecialrespostas = new Educacaoespecialrespostas();
		return $educacaoespecialrespostas->getEducacaoespecialrespostas($queries, $page, $maxpage);
	}
	
	public function getEducacaoespecialrespostas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		$idlocal = (isset($queries['idlocal'])) ? (int)$queries['idlocal'] : false;
		if ($idlocal) array_push($where, " e1.idlocal = $idlocal ");
		 
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM educacaoespecialrespostas e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEducacaoespecialrespostasById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEducacaoespecialrespostas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEducacaoespecialrespostasByIdHelper($id, $queries = array()) {
		$rows = new Educacaoespecialrespostas();
		return $rows->getEducacaoespecialrespostasById($id, $queries);
	}		
 
	public static function getUltimoEducacaoespecialrespostas() {		 
		$strsql = "select id, idlocal + 1 as idlocal from educacaoespecialrespostas ORDER BY idlocal DESC LIMIT 1";	 
								
		$db = Zend_Registry::get('db');	 
		return $db->fetchAll($strsql);
	}
 
	public static function getEducacaoespecialrespostasByIdLocalHelper($idlocal) {		 
		$strsql = "SELECT e1.* FROM educacaoespecialrespostas e1 WHERE e1.excluido='nao'  and e1.idlocal = $idlocal";	 
								
		$db = Zend_Registry::get('db');	 
		return $db->fetchAll($strsql);
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Educacaoespecialrespostas
     */
	public function save($dados) {
		$novoRegistro = true;

		
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		}
		
		$row->local = (array_key_exists("local",$dados)) ? $dados["local"] : $row->local; 
		$row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal; 
		$row->perguntasimnao0 = (array_key_exists("perguntasimnao0",$dados)) ? $dados["perguntasimnao0"] : $row->perguntasimnao0;
		$row->perguntasimnao1 = (array_key_exists("perguntasimnao1",$dados)) ? $dados["perguntasimnao1"] : $row->perguntasimnao1;
		$row->perguntasimnao2 = (array_key_exists("perguntasimnao2",$dados)) ? $dados["perguntasimnao2"] : $row->perguntasimnao2;
		$row->perguntasimnao3 = (array_key_exists("perguntasimnao3",$dados)) ? $dados["perguntasimnao3"] : $row->perguntasimnao3;
		$row->perguntasimnao4 = (array_key_exists("perguntasimnao4",$dados)) ? $dados["perguntasimnao4"] : $row->perguntasimnao4;
		$row->perguntasimnao5 = (array_key_exists("perguntasimnao5",$dados)) ? $dados["perguntasimnao5"] : $row->perguntasimnao5;
		$row->perguntasimnao6 = (array_key_exists("perguntasimnao6",$dados)) ? $dados["perguntasimnao6"] : $row->perguntasimnao6;
		$row->perguntasimnao7 = (array_key_exists("perguntasimnao7",$dados)) ? $dados["perguntasimnao7"] : $row->perguntasimnao7;
		$row->perguntasimnao8 = (array_key_exists("perguntasimnao8",$dados)) ? $dados["perguntasimnao8"] : $row->perguntasimnao8;
		$row->perguntasimnao9 = (array_key_exists("perguntasimnao9",$dados)) ? $dados["perguntasimnao9"] : $row->perguntasimnao9;
		$row->perguntasimnao10 = (array_key_exists("perguntasimnao10",$dados)) ? $dados["perguntasimnao10"] : $row->perguntasimnao10; 
		$row->perguntasimnao11 = (array_key_exists("perguntasimnao11",$dados)) ? $dados["perguntasimnao11"] : $row->perguntasimnao11;
		$row->perguntasimnao12 = (array_key_exists("perguntasimnao12",$dados)) ? $dados["perguntasimnao12"] : $row->perguntasimnao12;
		$row->perguntasimnao13 = (array_key_exists("perguntasimnao13",$dados)) ? $dados["perguntasimnao13"] : $row->perguntasimnao13;
		$row->perguntasimnao14 = (array_key_exists("perguntasimnao14",$dados)) ? $dados["perguntasimnao14"] : $row->perguntasimnao14;
		$row->perguntasimnao15 = (array_key_exists("perguntasimnao15",$dados)) ? $dados["perguntasimnao15"] : $row->perguntasimnao15;
		$row->perguntasimnao16 = (array_key_exists("perguntasimnao16",$dados)) ? $dados["perguntasimnao16"] : $row->perguntasimnao16;
		$row->perguntasimnao17 = (array_key_exists("perguntasimnao17",$dados)) ? $dados["perguntasimnao17"] : $row->perguntasimnao17;
		$row->perguntasimnao18 = (array_key_exists("perguntasimnao18",$dados)) ? $dados["perguntasimnao18"] : $row->perguntasimnao18;
		$row->perguntasimnao19 = (array_key_exists("perguntasimnao19",$dados)) ? $dados["perguntasimnao19"] : $row->perguntasimnao19;
		$row->perguntasimnao20 = (array_key_exists("perguntasimnao20",$dados)) ? $dados["perguntasimnao20"] : $row->perguntasimnao20; 
		$row->perguntasimnao21 = (array_key_exists("perguntasimnao21",$dados)) ? $dados["perguntasimnao21"] : $row->perguntasimnao21;
		$row->perguntasimnao22 = (array_key_exists("perguntasimnao22",$dados)) ? $dados["perguntasimnao22"] : $row->perguntasimnao22;
		$row->perguntasimnao23 = (array_key_exists("perguntasimnao23",$dados)) ? $dados["perguntasimnao23"] : $row->perguntasimnao23;
		$row->perguntasimnao24 = (array_key_exists("perguntasimnao24",$dados)) ? $dados["perguntasimnao24"] : $row->perguntasimnao24;
		$row->perguntasimnao25 = (array_key_exists("perguntasimnao25",$dados)) ? $dados["perguntasimnao25"] : $row->perguntasimnao25;
		$row->perguntasimnao26 = (array_key_exists("perguntasimnao26",$dados)) ? $dados["perguntasimnao26"] : $row->perguntasimnao26;
		$row->perguntasimnao27 = (array_key_exists("perguntasimnao27",$dados)) ? $dados["perguntasimnao27"] : $row->perguntasimnao27;
		$row->perguntasimnao28 = (array_key_exists("perguntasimnao28",$dados)) ? $dados["perguntasimnao28"] : $row->perguntasimnao28;
		$row->perguntasimnao29 = (array_key_exists("perguntasimnao29",$dados)) ? $dados["perguntasimnao29"] : $row->perguntasimnao29;
		$row->perguntasimnao30 = (array_key_exists("perguntasimnao30",$dados)) ? $dados["perguntasimnao30"] : $row->perguntasimnao30; 
		$row->perguntasimnao31 = (array_key_exists("perguntasimnao31",$dados)) ? $dados["perguntasimnao31"] : $row->perguntasimnao31;
		$row->perguntasimnao32 = (array_key_exists("perguntasimnao32",$dados)) ? $dados["perguntasimnao32"] : $row->perguntasimnao32;
		$row->perguntasimnao33 = (array_key_exists("perguntasimnao33",$dados)) ? $dados["perguntasimnao33"] : $row->perguntasimnao33;
		$row->perguntasimnao34 = (array_key_exists("perguntasimnao34",$dados)) ? $dados["perguntasimnao34"] : $row->perguntasimnao34;
		$row->perguntasimnao35 = (array_key_exists("perguntasimnao35",$dados)) ? $dados["perguntasimnao35"] : $row->perguntasimnao35;
		$row->perguntasimnao36 = (array_key_exists("perguntasimnao36",$dados)) ? $dados["perguntasimnao36"] : $row->perguntasimnao36;
		$row->perguntasimnao37 = (array_key_exists("perguntasimnao37",$dados)) ? $dados["perguntasimnao37"] : $row->perguntasimnao37;
		$row->perguntasimnao38 = (array_key_exists("perguntasimnao38",$dados)) ? $dados["perguntasimnao38"] : $row->perguntasimnao38;
		$row->perguntasimnao39 = (array_key_exists("perguntasimnao39",$dados)) ? $dados["perguntasimnao39"] : $row->perguntasimnao39;
		$row->perguntasimnao40 = (array_key_exists("perguntasimnao40",$dados)) ? $dados["perguntasimnao40"] : $row->perguntasimnao40; 
		$row->perguntasimnao41 = (array_key_exists("perguntasimnao41",$dados)) ? $dados["perguntasimnao41"] : $row->perguntasimnao41;
		$row->perguntasimnao42 = (array_key_exists("perguntasimnao42",$dados)) ? $dados["perguntasimnao42"] : $row->perguntasimnao42;
		$row->perguntasimnao43 = (array_key_exists("perguntasimnao43",$dados)) ? $dados["perguntasimnao43"] : $row->perguntasimnao43;
		$row->perguntasimnao44 = (array_key_exists("perguntasimnao44",$dados)) ? $dados["perguntasimnao44"] : $row->perguntasimnao44;
		$row->perguntasimnao45 = (array_key_exists("perguntasimnao45",$dados)) ? $dados["perguntasimnao45"] : $row->perguntasimnao45;
		$row->perguntasimnao46 = (array_key_exists("perguntasimnao46",$dados)) ? $dados["perguntasimnao46"] : $row->perguntasimnao46;
		$row->perguntasimnao47 = (array_key_exists("perguntasimnao47",$dados)) ? $dados["perguntasimnao47"] : $row->perguntasimnao47;
		$row->perguntasimnao48 = (array_key_exists("perguntasimnao48",$dados)) ? $dados["perguntasimnao48"] : $row->perguntasimnao48;
		$row->perguntasimnao49 = (array_key_exists("perguntasimnao49",$dados)) ? $dados["perguntasimnao49"] : $row->perguntasimnao49;
		$row->perguntasimnao50 = (array_key_exists("perguntasimnao50",$dados)) ? $dados["perguntasimnao50"] : $row->perguntasimnao50; 
		$row->perguntasimnao51 = (array_key_exists("perguntasimnao51",$dados)) ? $dados["perguntasimnao51"] : $row->perguntasimnao51;
		$row->perguntasimnao52 = (array_key_exists("perguntasimnao52",$dados)) ? $dados["perguntasimnao52"] : $row->perguntasimnao52;
		$row->perguntasimnao53 = (array_key_exists("perguntasimnao53",$dados)) ? $dados["perguntasimnao53"] : $row->perguntasimnao53;
		$row->perguntasimnao54 = (array_key_exists("perguntasimnao54",$dados)) ? $dados["perguntasimnao54"] : $row->perguntasimnao54; 
	 
		$row->ca_sa0 = (array_key_exists("ca_sa0",$dados)) ? $dados["ca_sa0"] : $row->ca_sa0;
		$row->ca_sa1 = (array_key_exists("ca_sa1",$dados)) ? $dados["ca_sa1"] : $row->ca_sa1;
		$row->ca_sa2 = (array_key_exists("ca_sa2",$dados)) ? $dados["ca_sa2"] : $row->ca_sa2;
		$row->ca_sa3 = (array_key_exists("ca_sa3",$dados)) ? $dados["ca_sa3"] : $row->ca_sa3;
		$row->ca_sa4 = (array_key_exists("ca_sa4",$dados)) ? $dados["ca_sa4"] : $row->ca_sa4;
		$row->ca_sa5 = (array_key_exists("ca_sa5",$dados)) ? $dados["ca_sa5"] : $row->ca_sa5;
		$row->ca_sa6 = (array_key_exists("ca_sa6",$dados)) ? $dados["ca_sa6"] : $row->ca_sa6;
		$row->ca_sa7 = (array_key_exists("ca_sa7",$dados)) ? $dados["ca_sa7"] : $row->ca_sa7;
		$row->ca_sa8 = (array_key_exists("ca_sa8",$dados)) ? $dados["ca_sa8"] : $row->ca_sa8;
		$row->ca_sa9 = (array_key_exists("ca_sa9",$dados)) ? $dados["ca_sa9"] : $row->ca_sa9;
		$row->ca_sa10 = (array_key_exists("ca_sa10",$dados)) ? $dados["ca_sa10"] : $row->ca_sa10; 
		$row->ca_sa11 = (array_key_exists("ca_sa11",$dados)) ? $dados["ca_sa11"] : $row->ca_sa11;
		$row->ca_sa12 = (array_key_exists("ca_sa12",$dados)) ? $dados["ca_sa12"] : $row->ca_sa12;
		$row->ca_sa13 = (array_key_exists("ca_sa13",$dados)) ? $dados["ca_sa13"] : $row->ca_sa13;
		$row->ca_sa14 = (array_key_exists("ca_sa14",$dados)) ? $dados["ca_sa14"] : $row->ca_sa14;
		$row->ca_sa15 = (array_key_exists("ca_sa15",$dados)) ? $dados["ca_sa15"] : $row->ca_sa15;
		$row->ca_sa16 = (array_key_exists("ca_sa16",$dados)) ? $dados["ca_sa16"] : $row->ca_sa16;
		$row->ca_sa17 = (array_key_exists("ca_sa17",$dados)) ? $dados["ca_sa17"] : $row->ca_sa17;
		$row->ca_sa18 = (array_key_exists("ca_sa18",$dados)) ? $dados["ca_sa18"] : $row->ca_sa18;
		$row->ca_sa19 = (array_key_exists("ca_sa19",$dados)) ? $dados["ca_sa19"] : $row->ca_sa19;
		$row->ca_sa20 = (array_key_exists("ca_sa20",$dados)) ? $dados["ca_sa20"] : $row->ca_sa20; 
		$row->ca_sa21 = (array_key_exists("ca_sa21",$dados)) ? $dados["ca_sa21"] : $row->ca_sa21;
		$row->ca_sa22 = (array_key_exists("ca_sa22",$dados)) ? $dados["ca_sa22"] : $row->ca_sa22;
		$row->ca_sa23 = (array_key_exists("ca_sa23",$dados)) ? $dados["ca_sa23"] : $row->ca_sa23;
		$row->ca_sa24 = (array_key_exists("ca_sa24",$dados)) ? $dados["ca_sa24"] : $row->ca_sa24;
		$row->ca_sa25 = (array_key_exists("ca_sa25",$dados)) ? $dados["ca_sa25"] : $row->ca_sa25;
		$row->ca_sa26 = (array_key_exists("ca_sa26",$dados)) ? $dados["ca_sa26"] : $row->ca_sa26;
		$row->ca_sa27 = (array_key_exists("ca_sa27",$dados)) ? $dados["ca_sa27"] : $row->ca_sa27;
		$row->ca_sa28 = (array_key_exists("ca_sa28",$dados)) ? $dados["ca_sa28"] : $row->ca_sa28;
		$row->ca_sa29 = (array_key_exists("ca_sa29",$dados)) ? $dados["ca_sa29"] : $row->ca_sa29;
		$row->ca_sa30 = (array_key_exists("ca_sa30",$dados)) ? $dados["ca_sa30"] : $row->ca_sa30; 
		$row->ca_sa31 = (array_key_exists("ca_sa31",$dados)) ? $dados["ca_sa31"] : $row->ca_sa31;
		$row->ca_sa32 = (array_key_exists("ca_sa32",$dados)) ? $dados["ca_sa32"] : $row->ca_sa32;
		$row->ca_sa33 = (array_key_exists("ca_sa33",$dados)) ? $dados["ca_sa33"] : $row->ca_sa33;
		$row->ca_sa34 = (array_key_exists("ca_sa34",$dados)) ? $dados["ca_sa34"] : $row->ca_sa34;
		$row->ca_sa35 = (array_key_exists("ca_sa35",$dados)) ? $dados["ca_sa35"] : $row->ca_sa35;
		$row->ca_sa36 = (array_key_exists("ca_sa36",$dados)) ? $dados["ca_sa36"] : $row->ca_sa36;
		$row->ca_sa37 = (array_key_exists("ca_sa37",$dados)) ? $dados["ca_sa37"] : $row->ca_sa37;
		$row->ca_sa38 = (array_key_exists("ca_sa38",$dados)) ? $dados["ca_sa38"] : $row->ca_sa38;
		$row->ca_sa39 = (array_key_exists("ca_sa39",$dados)) ? $dados["ca_sa39"] : $row->ca_sa39;
		$row->ca_sa40 = (array_key_exists("ca_sa40",$dados)) ? $dados["ca_sa40"] : $row->ca_sa40; 
		$row->ca_sa41 = (array_key_exists("ca_sa41",$dados)) ? $dados["ca_sa41"] : $row->ca_sa41;
		$row->ca_sa42 = (array_key_exists("ca_sa42",$dados)) ? $dados["ca_sa42"] : $row->ca_sa42;
		$row->ca_sa43 = (array_key_exists("ca_sa43",$dados)) ? $dados["ca_sa43"] : $row->ca_sa43;
		$row->ca_sa44 = (array_key_exists("ca_sa44",$dados)) ? $dados["ca_sa44"] : $row->ca_sa44;
		$row->ca_sa45 = (array_key_exists("ca_sa45",$dados)) ? $dados["ca_sa45"] : $row->ca_sa45;
		$row->ca_sa46 = (array_key_exists("ca_sa46",$dados)) ? $dados["ca_sa46"] : $row->ca_sa46;
		$row->ca_sa47 = (array_key_exists("ca_sa47",$dados)) ? $dados["ca_sa47"] : $row->ca_sa47;
		$row->ca_sa48 = (array_key_exists("ca_sa48",$dados)) ? $dados["ca_sa48"] : $row->ca_sa48;
		$row->ca_sa49 = (array_key_exists("ca_sa49",$dados)) ? $dados["ca_sa49"] : $row->ca_sa49;
		$row->ca_sa50 = (array_key_exists("ca_sa50",$dados)) ? $dados["ca_sa50"] : $row->ca_sa50; 
		$row->ca_sa51 = (array_key_exists("ca_sa51",$dados)) ? $dados["ca_sa51"] : $row->ca_sa51;
		$row->ca_sa52 = (array_key_exists("ca_sa52",$dados)) ? $dados["ca_sa52"] : $row->ca_sa52;
		$row->ca_sa53 = (array_key_exists("ca_sa53",$dados)) ? $dados["ca_sa53"] : $row->ca_sa53;
		$row->ca_sa54 = (array_key_exists("ca_sa54",$dados)) ? $dados["ca_sa54"] : $row->ca_sa54; 
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido; 
		

		$row->save();
		
		return $row;
	}
	
}