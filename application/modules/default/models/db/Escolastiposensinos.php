<?php

class Escolastiposensinos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolastiposensinos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getTiposensinosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolastiposensinos();
		return $produtos->getTiposensinos($queries, $page, $maxpage);
	}
	
	public function getTiposensinos($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? $queries['idsecretaria'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " te1.id=$id ");
		if ($idsecretaria) array_push($where, " te1.idsecretaria=$idsecretaria ");	
		if ($chave) array_push($where, " ((te1.tipo LIKE '%$chave%') OR (te1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " te1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "te1.* ";
		if ($total) $fields = "COUNT(te1.id) as total";
		
		
		$ordem = "ORDER BY te1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolastiposensinos te1
					WHERE te1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getTipoensinoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTiposensinos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTipoensinoByIdHelper($id, $queries = array()) {
		$rows = new Escolastiposensinos();
		return $rows->getTipoensinoById($id, $queries);
	}		
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Tiposensinos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s'); 		
		}else {
			$novoRegistro = false;
		} 

		$row->tipo = (array_key_exists('tipo',$dados)) ? $dados['tipo'] : $row->tipo;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;			
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		
		$row->save();

		return $row;
	}

	public static function getensinosescolasid($id){

		$strSql = "select * from escolastiposensinos where id = '$id'";					

		$db = Zend_Registry::get('db');	
		
		return $db->fetchAll($strSql);		


	}//end function getensinosescolas
	
	
}