<?php

/**
 * Define o modelo Chamadosequipes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Chamadosequipes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "chamadosequipes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getChamadosequipesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$chamadosequipes = new Chamadosequipes();
		return $chamadosequipes->getChamadosequipes($queries, $page, $maxpage);
	}
	
	public function getChamadosequipes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " c1.iddepartamento = $iddepartamento ");

		$idtipoequipe = (isset($queries["idtipoequipe"])) ? $queries["idtipoequipe"] : false;
		if ($idtipoequipe) array_push($where, " c1.idtipoequipe = $idtipoequipe ");

$idsfuncionarios = (isset($queries["idsfuncionarios"])) ? $queries["idsfuncionarios"] : false;
		if ($idsfuncionarios) array_push($where, " c1.idsfuncionarios = '$idsfuncionarios' ");

$equipe = (isset($queries["equipe"])) ? $queries["equipe"] : false;
		if ($equipe) array_push($where, " c1.equipe LIKE '%$equipe%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " c1.descricoes = '$descricoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
							
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM chamadosequipes c1
					
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getChamadosequipeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getChamadosequipes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getChamadosequipeByIdHelper($id, $queries = array()) {
		$rows = new Chamadosequipes();
		return $rows->getChamadosequipeById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Chamadosequipes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
		 $row->idtipoequipe = (array_key_exists("idtipoequipe",$dados)) ? $dados["idtipoequipe"] : $row->idtipoequipe;
		 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->idsfuncionarios = (array_key_exists("idsfuncionarios",$dados)) ? $dados["idsfuncionarios"] : $row->idsfuncionarios;
 $row->equipe = (array_key_exists("equipe",$dados)) ? $dados["equipe"] : $row->equipe;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Chamadosequipes departamento $row->iddepartamento  salvo com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Chamadosequipes departamento $row->iddepartamento  com o ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Chamadosequipes departamento $row->iddepartamento  com o ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Chamadosequipes departamento $row->iddepartamento  com o ID ".$id." atualizada com sucesso!");

		return $row;
	}
	
}