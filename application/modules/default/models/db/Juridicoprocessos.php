<?php

/**
 * Define o modelo Juridicoprocessos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Juridicoprocessos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "juridicoprocessos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getJuridicoprocessosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$juridicoprocessos = new Juridicoprocessos();
		return $juridicoprocessos->getJuridicoprocessos($queries, $page, $maxpage);
	}
	
	public function getJuridicoprocessos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " j1.id = $id ");
		
		
		$idsadvogados = (isset($queries["idsadvogados"])) ? $queries["idsadvogados"] : false;
		if ($idsadvogados) array_push($where, " j1.idsadvogados = '$idsadvogados' ");

$processonumero = (isset($queries["processonumero"])) ? $queries["processonumero"] : false;
		if ($processonumero) array_push($where, " j1.processonumero LIKE '%$processonumero%' ");

$processooriginario = (isset($queries["processooriginario"])) ? $queries["processooriginario"] : false;
		if ($processooriginario) array_push($where, " j1.processooriginario LIKE '%$processooriginario%' ");

$idorigem = (isset($queries["idorigem"])) ? $queries["idorigem"] : false;
		if ($idorigem) array_push($where, " j1.idorigem = $idorigem ");

$representantelegal = (isset($queries["representantelegal"])) ? $queries["representantelegal"] : false;
		if ($representantelegal) array_push($where, " j1.representantelegal LIKE '%$representantelegal%' ");

$acao = (isset($queries["acao"])) ? $queries["acao"] : false;
		if ($acao) array_push($where, " j1.acao = '$acao' ");

$dataproximaaudiencia_i = (isset($queries["dataproximaaudiencia_i"])) ? $queries["dataproximaaudiencia_i"] : false;
		if ($dataproximaaudiencia_i) array_push($where, " j1.dataproximaaudiencia >= '$dataproximaaudiencia_i' ");

$dataproximaaudiencia_f = (isset($queries["dataproximaaudiencia_f"])) ? $queries["dataproximaaudiencia_f"] : false;
		if ($dataproximaaudiencia_f) array_push($where, " j1.dataproximaaudiencia <= '$dataproximaaudiencia_f' ");

$valorcausa = (isset($queries["valorcausa"])) ? $queries["valorcausa"] : false;
		if ($valorcausa) array_push($where, " j1.valorcausa = $valorcausa ");

$pasta = (isset($queries["pasta"])) ? $queries["pasta"] : false;
		if ($pasta) array_push($where, " j1.pasta LIKE '%$pasta%' ");

$datacitacao_i = (isset($queries["datacitacao_i"])) ? $queries["datacitacao_i"] : false;
		if ($datacitacao_i) array_push($where, " j1.datacitacao >= '$datacitacao_i' ");

$datacitacao_f = (isset($queries["datacitacao_f"])) ? $queries["datacitacao_f"] : false;
		if ($datacitacao_f) array_push($where, " j1.datacitacao <= '$datacitacao_f' ");

$datapenhora_i = (isset($queries["datapenhora_i"])) ? $queries["datapenhora_i"] : false;
		if ($datapenhora_i) array_push($where, " j1.datapenhora >= '$datapenhora_i' ");

$datapenhora_f = (isset($queries["datapenhora_f"])) ? $queries["datapenhora_f"] : false;
		if ($datapenhora_f) array_push($where, " j1.datapenhora <= '$datapenhora_f' ");

$datacontestacao_i = (isset($queries["datacontestacao_i"])) ? $queries["datacontestacao_i"] : false;
		if ($datacontestacao_i) array_push($where, " j1.datacontestacao >= '$datacontestacao_i' ");

$datacontestacao_f = (isset($queries["datacontestacao_f"])) ? $queries["datacontestacao_f"] : false;
		if ($datacontestacao_f) array_push($where, " j1.datacontestacao <= '$datacontestacao_f' ");

$dataembargos_i = (isset($queries["dataembargos_i"])) ? $queries["dataembargos_i"] : false;
		if ($dataembargos_i) array_push($where, " j1.dataembargos >= '$dataembargos_i' ");

$dataembargos_f = (isset($queries["dataembargos_f"])) ? $queries["dataembargos_f"] : false;
		if ($dataembargos_f) array_push($where, " j1.dataembargos <= '$dataembargos_f' ");

$datasentenca_i = (isset($queries["datasentenca_i"])) ? $queries["datasentenca_i"] : false;
		if ($datasentenca_i) array_push($where, " j1.datasentenca >= '$datasentenca_i' ");

$datasentenca_f = (isset($queries["datasentenca_f"])) ? $queries["datasentenca_f"] : false;
		if ($datasentenca_f) array_push($where, " j1.datasentenca <= '$datasentenca_f' ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " j1.observacoes = '$observacoes' ");

$movimentolancar = (isset($queries["movimentolancar"])) ? $queries["movimentolancar"] : false;
		if ($movimentolancar) array_push($where, " j1.movimentolancar LIKE '%$movimentolancar%' ");

$movimentodata_i = (isset($queries["movimentodata_i"])) ? $queries["movimentodata_i"] : false;
		if ($movimentodata_i) array_push($where, " j1.movimentodata >= '$movimentodata_i' ");

$movimentodata_f = (isset($queries["movimentodata_f"])) ? $queries["movimentodata_f"] : false;
		if ($movimentodata_f) array_push($where, " j1.movimentodata <= '$movimentodata_f' ");

$movimentodescricoes = (isset($queries["movimentodescricoes"])) ? $queries["movimentodescricoes"] : false;
		if ($movimentodescricoes) array_push($where, " j1.movimentodescricoes = '$movimentodescricoes' ");

$idstatus = (isset($queries["idstatus"])) ? $queries["idstatus"] : false;
		if ($idstatus) array_push($where, " j1.idstatus = $idstatus ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " j1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "j1.*"; 
		;
		
		if ($total) $fields = "COUNT(j1.id) as total";
		
		$ordem = "ORDER BY j1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM juridicoprocessos j1
					
					WHERE j1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getJuridicoprocessoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getJuridicoprocessos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getJuridicoprocessoByIdHelper($id, $queries = array()) {
		$rows = new Juridicoprocessos();
		return $rows->getJuridicoprocessoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Juridicoprocessos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsadvogados = (array_key_exists("idsadvogados",$dados)) ? $dados["idsadvogados"] : $row->idsadvogados;
 $row->processonumero = (array_key_exists("processonumero",$dados)) ? $dados["processonumero"] : $row->processonumero;
 $row->processooriginario = (array_key_exists("processooriginario",$dados)) ? $dados["processooriginario"] : $row->processooriginario;
 $row->idorigem = (array_key_exists("idorigem",$dados)) ? $dados["idorigem"] : $row->idorigem;
 $row->representantelegal = (array_key_exists("representantelegal",$dados)) ? $dados["representantelegal"] : $row->representantelegal;
 $row->acao = (array_key_exists("acao",$dados)) ? $dados["acao"] : $row->acao;
 $row->dataproximaaudiencia = (array_key_exists("dataproximaaudiencia",$dados)) ? $dados["dataproximaaudiencia"] : $row->dataproximaaudiencia;
 $row->valorcausa = (array_key_exists("valorcausa",$dados)) ? $dados["valorcausa"] : $row->valorcausa;
 $row->pasta = (array_key_exists("pasta",$dados)) ? $dados["pasta"] : $row->pasta;
 $row->datacitacao = (array_key_exists("datacitacao",$dados)) ? $dados["datacitacao"] : $row->datacitacao;
 $row->datapenhora = (array_key_exists("datapenhora",$dados)) ? $dados["datapenhora"] : $row->datapenhora;
 $row->datacontestacao = (array_key_exists("datacontestacao",$dados)) ? $dados["datacontestacao"] : $row->datacontestacao;
 $row->dataembargos = (array_key_exists("dataembargos",$dados)) ? $dados["dataembargos"] : $row->dataembargos;
 $row->datasentenca = (array_key_exists("datasentenca",$dados)) ? $dados["datasentenca"] : $row->datasentenca;
 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
 $row->movimentolancar = (array_key_exists("movimentolancar",$dados)) ? $dados["movimentolancar"] : $row->movimentolancar;
 $row->movimentodata = (array_key_exists("movimentodata",$dados)) ? $dados["movimentodata"] : $row->movimentodata;
 $row->movimentodescricoes = (array_key_exists("movimentodescricoes",$dados)) ? $dados["movimentodescricoes"] : $row->movimentodescricoes;
 $row->idstatus = (array_key_exists("idstatus",$dados)) ? $dados["idstatus"] : $row->idstatus;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}