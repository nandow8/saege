<?php

class Edificacoes_Infraestruturadeficiencia extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasedificacoesinfraestruturadeficiencia";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getInfraestruturasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Edificacoes_Infraestruturadeficiencia();
		return $produtos->getInfraestruturas($queries, $page, $maxpage);
	}
	
	public function getInfraestruturas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " id1.id=$id ");
		if ($idescola) array_push($where, " id1.idescola=$idescola ");
		//if ($chave) array_push($where, " ((id1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " id1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "id1.*";
		if ($total) $fields = "COUNT(id1.id) as total";
		
		
		$ordem = "ORDER BY id1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasedificacoesinfraestruturadeficiencia id1
					WHERE id1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getInfraestruturaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getInfraestruturas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getInfraestruturaByIdHelper($id, $queries = array()) {
		$rows = new Edificacoes_Infraestruturadeficiencia();
		return $rows->getInfraestruturaById($id, $queries);
	}	

	public function getInfraestruturaByIdEscola($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idescola'] = $id;
		$rows = $this->getInfraestruturas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Infraestruturas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
		} 

		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->sistemaseguranca = (array_key_exists('sistemaseguranca',$dados)) ? $dados['sistemaseguranca'] : $row->sistemaseguranca;
		$row->saidasemergencia = (array_key_exists('saidasemergencia',$dados)) ? $dados['saidasemergencia'] : $row->saidasemergencia;
		$row->redeeletrica = (array_key_exists('redeeletrica',$dados)) ? $dados['redeeletrica'] : $row->redeeletrica;
		$row->manutencoesperiodicas = (array_key_exists('manutencoesperiodicas',$dados)) ? $dados['manutencoesperiodicas'] : $row->manutencoesperiodicas;
		
		$row->acessosdeficiente = (array_key_exists('acessosdeficiente',$dados)) ? $dados['acessosdeficiente'] : $row->acessosdeficiente;
		$row->observacoes = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;
		$row->elevador = (array_key_exists('elevador',$dados)) ? $dados['elevador'] : $row->elevador;
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;

		$row->save();

		return $row;
	}
	
	
}