<?php

class Edificacoes_Informacoestecnicas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasedificacoesinformacoestecnicas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getInformacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Edificacoes_Informacoestecnicas();
		return $produtos->getInformacoes($queries, $page, $maxpage);
	}
	
	public function getInformacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " it1.id=$id ");
		if ($idescola) array_push($where, " it1.idescola=$idescola ");
		//if ($chave) array_push($where, " ((it1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " it1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "it1.*";
		if ($total) $fields = "COUNT(it1.id) as total";
		
		
		$ordem = "ORDER BY it1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasedificacoesinformacoestecnicas it1
					WHERE it1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getInformacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getInformacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getInformacaoByIdHelper($id, $queries = array()) {
		$rows = new Edificacoes_Informacoestecnicas();
		return $rows->getInformacaoById($id, $queries);
	}	

	public function getInformacaoByIdEscola($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idescola'] = $id;
		$rows = $this->getInformacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Informacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
		} 

		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->idcobertura = (array_key_exists('idcobertura',$dados)) ? $dados['idcobertura'] : $row->idcobertura;
		$row->idtipoconstrucao = (array_key_exists('idtipoconstrucao',$dados)) ? $dados['idtipoconstrucao'] : $row->idtipoconstrucao;
		$row->pavimentos = (array_key_exists('pavimentos',$dados)) ? $dados['pavimentos'] : $row->pavimentos;
		$row->areaconstruida = (array_key_exists('areaconstruida',$dados)) ? $dados['areaconstruida'] : $row->areaconstruida;
		$row->fracoes = (array_key_exists('fracoes',$dados)) ? $dados['fracoes'] : $row->fracoes;
		$row->pontosacessos = (array_key_exists('pontosacessos',$dados)) ? $dados['pontosacessos'] : $row->pontosacessos;
		$row->saidasemergencia = (array_key_exists('saidasemergencia',$dados)) ? $dados['saidasemergencia'] : $row->saidasemergencia;
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;

		$row->save();

		return $row;
	}
	
	
}