<?php

class Edificacoes_Edificacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasedificacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getEdificacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Edificacoes_Edificacoes();
		return $produtos->getEdificacoes($queries, $page, $maxpage);
	}
	
	public function getEdificacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " e1.id=$id ");
		if ($idescola) array_push($where, " e1.idescola=$idescola ");
		if ($chave) array_push($where, " ((e1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " e1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*";
		if ($total) $fields = "COUNT(e1.id) as total";
		
		
		$ordem = "ORDER BY e1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasedificacoes e1
					WHERE e1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getEdificacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEdificacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEdificacaoByIdHelper($id, $queries = array()) {
		$rows = new Edificacoes_Edificacoes();
		return $rows->getEdificacaoById($id, $queries);
	}	

	public function getEdificacaoByIdEscola($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idescola'] = $id;
		$rows = $this->getEdificacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Edificacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
		} 

		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->idtipoedificacao  = (array_key_exists('idtipoedificacao',$dados)) ? $dados['idtipoedificacao'] : $row->idtipoedificacao;
		
		$row->descricoes  = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes ;
		$row->dataconstrucao = (array_key_exists('dataconstrucao',$dados)) ? $dados['dataconstrucao'] : $row->dataconstrucao;
		$row->localizacao  = (array_key_exists('localizacao',$dados)) ? $dados['localizacao'] : $row->localizacao ;
		$row->referencia  = (array_key_exists('referencia',$dados)) ? $dados['referencia'] : $row->referencia ;
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;

		$row->save();

		return $row;
	}
	
	
}