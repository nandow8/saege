<?php

class Edificacoes_Tipos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasedificacoestipos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_ORIGEM_EDIFICACOES = 'edificacoes';
	public static $_ORIGEM_ESTRUTURA_COBERTURA = 'estruturacobertura';
	public static $_ORIGEM_CONSTRUCAO = 'construcao';	
	
	/**
	 * 
	 */
	public static function getOrigem($field = false) {
		$res = array(
			self::$_ORIGEM_EDIFICACOES => 'Tipos de itens de edificação',
			self::$_ORIGEM_ESTRUTURA_COBERTURA => 'Tipos de estrutura de cobertura',
			self::$_ORIGEM_CONSTRUCAO => 'Tipos de Construções'
		);
		
		if (!$field) return $res;
		return $res[$field];
	}
	
	public static function getTiposHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Edificacoes_Tipos();
		return $produtos->getTipos($queries, $page, $maxpage);
	}
	
	public function getTipos($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " t1.id=$id ");
		if ($idsecretaria) array_push($where, " t1.idsecretaria=$idsecretaria ");
		if ($chave) array_push($where, " ((t1.tipo LIKE '%$chave%') OR (t1.descricoes LIKE '%$chave%')) ");
		if ($origem) array_push($where, " t1.origem='$origem' ");
		if ($status) array_push($where, " t1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "t1.*";
		if ($total) $fields = "COUNT(t1.id) as total";
		
		
		$ordem = "ORDER BY t1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasedificacoestipos t1
					WHERE t1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getTipoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTipos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTiponomeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$fornecedores = new Edificacoes_Tipos();
		$queries['id'] = $id;
		$rows = $fornecedores->getTipos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$row = $rows[0];
		
		return $row['tipo'];
	}
	
	public static function getTipoByIdHelper($id, $queries = array()) {
		$rows = new Edificacoes_Tipos();
		return $rows->getTipoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Tipos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
		} 

		$row->tipo = (array_key_exists('tipo',$dados)) ? $dados['tipo'] : $row->tipo;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;
		$row->origem = (array_key_exists('origem',$dados)) ? $dados['origem'] : $row->origem;
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		
		
		$row->save();

		return $row;
	}
	
	
}