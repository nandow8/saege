<?php

/**
 * Define o modelo Escolasprogramasgov
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasprogramasgov extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasprogramasgov";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolasprogramasgovHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasprogramasgov = new Escolasprogramasgov();
		return $escolasprogramasgov->getEscolasprogramasgov($queries, $page, $maxpage);
	}
	
	public function getEscolasprogramasgov($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");

$idtipoconvenio = (isset($queries["idtipoconvenio"])) ? $queries["idtipoconvenio"] : false;
		if ($idtipoconvenio) array_push($where, " e1.idtipoconvenio = $idtipoconvenio ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " e1.titulo LIKE '%$titulo%' ");

$datainicio_i = (isset($queries["datainicio_i"])) ? $queries["datainicio_i"] : false;
		if ($datainicio_i) array_push($where, " e1.datainicio >= '$datainicio_i' ");

$datainicio_f = (isset($queries["datainicio_f"])) ? $queries["datainicio_f"] : false;
		if ($datainicio_f) array_push($where, " e1.datainicio <= '$datainicio_f' ");

$datafim_i = (isset($queries["datafim_i"])) ? $queries["datafim_i"] : false;
		if ($datafim_i) array_push($where, " e1.datafim >= '$datafim_i' ");

$datafim_f = (isset($queries["datafim_f"])) ? $queries["datafim_f"] : false;
		if ($datafim_f) array_push($where, " e1.datafim <= '$datafim_f' ");

$exercicio = (isset($queries["exercicio"])) ? $queries["exercicio"] : false;
		if($exercicio=='000') $exercicio = '';
		if ($exercicio) array_push($where, " e1.exercicio = '$exercicio' ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " e1.tipo LIKE '%$tipo%' ");

$cpfcnpj = (isset($queries["cpfcnpj"])) ? $queries["cpfcnpj"] : false;
		if ($cpfcnpj) array_push($where, " e1.cpfcnpj LIKE '%$cpfcnpj%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasprogramasgov e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolaprogramagovById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasprogramasgov($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaprogramagovByIdHelper($id, $queries = array()) {
		$rows = new Escolasprogramasgov();
		return $rows->getEscolaprogramagovById($id, $queries);
	}		
	
	
	public function getEscolaprogramagovByIdTipoConvenio($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idtipoconvenio'] = $id;
		$rows = $this->getEscolasprogramasgov($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaprogramagovByIdTipoConvenioHelper($id, $queries = array()) {
		$rows = new Escolasprogramasgov();
		return $rows->getEscolaprogramagovByIdTipoConvenio($id, $queries);
	}		
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasprogramasgov
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
 $row->idtipoconvenio = (array_key_exists("idtipoconvenio",$dados)) ? $dados["idtipoconvenio"] : $row->idtipoconvenio;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
 $row->datafim = (array_key_exists("datafim",$dados)) ? $dados["datafim"] : $row->datafim;
 $row->exercicio = (array_key_exists("exercicio",$dados)) ? $dados["exercicio"] : $row->exercicio;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->cpfcnpj = (array_key_exists("cpfcnpj",$dados)) ? $dados["cpfcnpj"] : $row->cpfcnpj;
 $row->saldoexercicioanterior_custeio = (array_key_exists("saldoexercicioanterior_custeio",$dados)) ? $dados["saldoexercicioanterior_custeio"] : $row->saldoexercicioanterior_custeio;
 $row->saldoexercicioanterior_capital = (array_key_exists("saldoexercicioanterior_capital",$dados)) ? $dados["saldoexercicioanterior_capital"] : $row->saldoexercicioanterior_capital;
 $row->valorcreditadofnde_custeio = (array_key_exists("valorcreditadofnde_custeio",$dados)) ? $dados["valorcreditadofnde_custeio"] : $row->valorcreditadofnde_custeio;
 $row->valorcreditadofnde_capital = (array_key_exists("valorcreditadofnde_capital",$dados)) ? $dados["valorcreditadofnde_capital"] : $row->valorcreditadofnde_capital;
 $row->recursosproprios_custeio = (array_key_exists("recursosproprios_custeio",$dados)) ? $dados["recursosproprios_custeio"] : $row->recursosproprios_custeio;
 $row->recursosproprios_capital = (array_key_exists("recursosproprios_capital",$dados)) ? $dados["recursosproprios_capital"] : $row->recursosproprios_capital;
 $row->rendimentoaplfinanceira_custeio = (array_key_exists("rendimentoaplfinanceira_custeio",$dados)) ? $dados["rendimentoaplfinanceira_custeio"] : $row->rendimentoaplfinanceira_custeio;
 $row->rendimentoaplfinanceira_capital = (array_key_exists("rendimentoaplfinanceira_capital",$dados)) ? $dados["rendimentoaplfinanceira_capital"] : $row->rendimentoaplfinanceira_capital;
 $row->devolucaorecursosfnde_custeio = (array_key_exists("devolucaorecursosfnde_custeio",$dados)) ? $dados["devolucaorecursosfnde_custeio"] : $row->devolucaorecursosfnde_custeio;
 $row->devolucaorecursosfnde_capital = (array_key_exists("devolucaorecursosfnde_capital",$dados)) ? $dados["devolucaorecursosfnde_capital"] : $row->devolucaorecursosfnde_capital;
 $row->valortotalreceita_custeio = (array_key_exists("valortotalreceita_custeio",$dados)) ? $dados["valortotalreceita_custeio"] : $row->valortotalreceita_custeio;
 $row->valortotalreceita_capital = (array_key_exists("valortotalreceita_capital",$dados)) ? $dados["valortotalreceita_capital"] : $row->valortotalreceita_capital;
 $row->valordespesarealizada_custeio = (array_key_exists("valordespesarealizada_custeio",$dados)) ? $dados["valordespesarealizada_custeio"] : $row->valordespesarealizada_custeio;
 $row->valordespesarealizada_capital = (array_key_exists("valordespesarealizada_capital",$dados)) ? $dados["valordespesarealizada_capital"] : $row->valordespesarealizada_capital;
 $row->saldoexercicioseguinte_custeio = (array_key_exists("saldoexercicioseguinte_custeio",$dados)) ? $dados["saldoexercicioseguinte_custeio"] : $row->saldoexercicioseguinte_custeio;
 $row->saldoexercicioseguinte_capital = (array_key_exists("saldoexercicioseguinte_capital",$dados)) ? $dados["saldoexercicioseguinte_capital"] : $row->saldoexercicioseguinte_capital;
 $row->saldodevolvido_custeio = (array_key_exists("saldodevolvido_custeio",$dados)) ? $dados["saldodevolvido_custeio"] : $row->saldodevolvido_custeio;
 $row->saldodevolvido_capital = (array_key_exists("saldodevolvido_capital",$dados)) ? $dados["saldodevolvido_capital"] : $row->saldodevolvido_capital;
 $row->saldoexercicioanterior_apm = (array_key_exists("saldoexercicioanterior_apm",$dados)) ? $dados["saldoexercicioanterior_apm"] : $row->saldoexercicioanterior_apm;
 $row->rendimentoaplicacaofinanceira_apm = (array_key_exists("rendimentoaplicacaofinanceira_apm",$dados)) ? $dados["rendimentoaplicacaofinanceira_apm"] : $row->rendimentoaplicacaofinanceira_apm;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
 $row->apm = (array_key_exists("apm",$dados)) ? $dados["apm"] : $row->apm;
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		//return true;		
		$row->save();
		if (array_key_exists('contas', $dados)) $this->setContas($dados['contas'], $row->id);
		
		return $row;
	}

	private function setContas($dados, $idparent) {
		
		if (!is_array($dados)) return;
		 
		$itens = new Escolasprogramascontas();
		$bancos = $dados['banco'];
		$agencias = $dados['agencia'];
		$contas = $dados['conta'];
		$digitos = $dados['digito'];
		$tipos = $dados['tipo'];
		
		$ids = array();
		foreach ($bancos as $i=>$id) {
			$d = array();
			
			$d['idparent'] = $idparent; 
			$d['banco'] = trim(strip_tags($bancos[$i]));
			$d['agencia'] = trim(strip_tags($agencias[$i]));
			$d['conta'] = trim(strip_tags($contas[$i]));
			$d['digito'] = trim(strip_tags($digitos[$i]));
			$d['tipo'] = trim($tipos);
                        
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			//$d['logusuario'] = $dados['logusuario'];
			//$d['logdata'] = $dados['logdata'];

                        //var_dump($bancos);die();
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
		}
		$contas = implode(",", $ids);
                
		if ($contas=="") $contas = "0";
		$strsql = "DELETE FROM escolasprogramascontas WHERE idparent=$idparent AND id NOT IN ($contas)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);
		
	}

	
}