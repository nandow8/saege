<?php

/**
 * Define o modelo Agendamentosvisitas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Agendamentosvisitas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "agendamentosvisitas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAgendamentosvisitasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$agendamentosvisitas = new Agendamentosvisitas();
		return $agendamentosvisitas->getAgendamentosvisitas($queries, $page, $maxpage);
	}
	
	public function getAgendamentosvisitas($queries = array(), $page = 0, $maxpage = 0) { 
					
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
		
		
		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " a1.iddepartamento = $iddepartamento");  // 30 admin que visualiza tudo

$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " a1.idfuncionario = $idfuncionario ");

$destino = (isset($queries["destino"])) ? $queries["destino"] : false;
		if ($destino) array_push($where, " a1.destino LIKE '%$destino%' ");

$iddepartamentodestino = (isset($queries["iddepartamentodestino"])) ? $queries["iddepartamentodestino"] : false;
		if ($iddepartamentodestino) array_push($where, " a1.iddepartamentodestino = $iddepartamentodestino ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " a1.idescola = $idescola ");

$datainicio_i = (isset($queries["datainicio_i"])) ? $queries["datainicio_i"] : false;
		if ($datainicio_i) array_push($where, " a1.datainicio >= '$datainicio_i' ");

$datainicio_f = (isset($queries["datainicio_f"])) ? $queries["datainicio_f"] : false;
		if ($datainicio_f) array_push($where, " a1.datainicio <= '$datainicio_f' ");

$horainicio = (isset($queries["horainicio"])) ? $queries["horainicio"] : false;
		if ($horainicio) array_push($where, " a1.horainicio = '$horainicio' ");

$datafim_i = (isset($queries["datafim_i"])) ? $queries["datafim_i"] : false;
		if ($datafim_i) array_push($where, " a1.datafim >= '$datafim_i' ");

$datafim_f = (isset($queries["datafim_f"])) ? $queries["datafim_f"] : false;
		if ($datafim_f) array_push($where, " a1.datafim <= '$datafim_f' ");

$horafim = (isset($queries["horafim"])) ? $queries["horafim"] : false;
		if ($horafim) array_push($where, " a1.horafim = '$horafim' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " a1.titulo LIKE '%$titulo%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " a1.descricoes LIKE '%$descricoes%' ");

$observações = (isset($queries["observações"])) ? $queries["observações"] : false;
		if ($observações) array_push($where, " a1.observações = '$observações' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");
		
$data  = (isset($queries['data'])) ? $queries['data'] : false;
	if ($data) array_push($where, " (a1.datainicio <='$data' AND a1.datafim >= '$data') ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM agendamentosvisitas a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";
		//echo $strsql;
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}

	public function getUltimoAgendamentosvisita($queries = array()) {
		$queries['order'] = 'ORDER BY a1.id DESC';
		$rows = $this->getAgendamentosvisitas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	
	public function getAgendamentosvisitaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAgendamentosvisitas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAgendamentosvisitaByIdHelper($id, $queries = array()) {
		$rows = new Agendamentosvisitas();
		return $rows->getAgendamentosvisitaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Agendamentosvisitas
     */
	public function save($dados) {
		
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
		 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
		 $row->destino = (array_key_exists("destino",$dados)) ? $dados["destino"] : $row->destino;
		 $row->iddepartamentodestino = (array_key_exists("iddepartamentodestino",$dados)) ? $dados["iddepartamentodestino"] : $row->iddepartamentodestino;
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		 $row->idlocais = (array_key_exists("idlocais",$dados)) ? $dados["idlocais"] : $row->idlocais;
		 $row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
		 $row->horainicio = (array_key_exists("horainicio",$dados)) ? $dados["horainicio"] : $row->horainicio;
		 $row->datafim = (array_key_exists("datafim",$dados)) ? $dados["datafim"] : $row->datafim;
		 $row->horafim = (array_key_exists("horafim",$dados)) ? $dados["horafim"] : $row->horafim;
		 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
		 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
		 $row->observações = (array_key_exists("observações",$dados)) ? $dados["observações"] : $row->observações;
		 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();

		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Agendamentosvisitas para o local $row->iddepartamento e destino $row->destino salvo com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Agendamentosvisitas para o local $row->iddepartamento e destino $row->destino com o ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Agendamentosvisitas para o local $row->iddepartamento e destino $row->destino com o ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Agendamentosvisitas para o local $row->iddepartamento e destino $row->destino com o ID ".$id." atualizada com sucesso!");
		
		return $row;
	}
	
}