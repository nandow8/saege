<?php

/**
 * Define o modelo Treinamento
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Treinamento extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "treinamento";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getTreinamentoHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$treinamento = new Treinamento();
		return $treinamento->getTreinamentos($queries, $page, $maxpage);
	}
	
	public function getTreinamentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " t1.id = $id ");


		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " t1.status LIKE '%$status%' ");
		$ano = (isset($queries["ano"])) ? $queries["ano"] : false;
		if ($ano) array_push($where, " t1.ano LIKE '%$ano%' ");
		$cursoid = (isset($queries["cursoid"])) ? $queries["cursoid"] : false;
		if ($cursoid) array_push($where, " t1.cursoid = $cursoid ");
		$informacoes = (isset($queries["informacoes"])) ? $queries["informacoes"] : false;
		if ($informacoes) array_push($where, " t1.informacoes LIKE '%$informacoes%' ");

		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " t1.sequencial LIKE '%$sequencial%' ");
		
		$data = (isset($queries['data'])) ? $queries['data'] : false;
		if ($data) array_push($where, " t1.data = '" . date( "Y-d-m" , strtotime($data)) . "'");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='t1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "t1.*"; 
		;
		
		if ($total) $fields = "COUNT(t1.id) as total";
		
		$ordem = "ORDER BY t1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM treinamento t1
					
					WHERE t1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getTreinamentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTreinamentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTreinamentoByIdHelper($id, $queries = array()) {
		$rows = new Treinamento();
		return $rows->getTreinamentoById($id, $queries);
	}

	public static function getUltimoTreinamentoHelper($queries = array()){
		$rows = new Treinamento();
		return $rows->getUltimoTreinamento($queries);
	}

	public function getUltimoTreinamento($queries = array()) {
		$queries['order'] = 'ORDER BY t1.id DESC';
		$rows = $this->getTreinamentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Treinamento
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		 
		$row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
		$row->ano = (array_key_exists("ano",$dados)) ? $dados["ano"] : $row->ano;
		$row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		$row->usuarioid = (array_key_exists("usuarioid",$dados)) ? $dados["usuarioid"] : $row->usuarioid;
		$row->cursoid = (array_key_exists("cursoid",$dados)) ? $dados["cursoid"] : $row->cursoid;
 		$row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
 		$row->datatermino = (array_key_exists("datatermino",$dados)) ? $dados["datatermino"] : $row->datatermino;
 		$row->diassemana = (array_key_exists("diassemana",$dados)) ? $dados["diassemana"] : $row->diassemana;
 		$row->horarioinicio = (array_key_exists("horarioinicio",$dados)) ? $dados["horarioinicio"] : $row->horarioinicio;
 		$row->horariofinal = (array_key_exists("horariofinal",$dados)) ? $dados["horariofinal"] : $row->horariofinal;
 		$row->qtdvagas = (array_key_exists("qtdvagas",$dados)) ? $dados["qtdvagas"] : $row->qtdvagas;
 		$row->local = (array_key_exists("local",$dados)) ? $dados["local"] : $row->local;
 		$row->informacoes = (array_key_exists("informacoes",$dados)) ? $dados["informacoes"] : $row->informacoes;
 		$row->datalimite = (array_key_exists("datalimite",$dados)) ? $dados["datalimite"] : $row->datalimite;

 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}