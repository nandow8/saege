<?php

class Almoxarifadosolicitacoes extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "almoxarifadosolicitacoes";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getAlmoxarifadosolicitacoesHelper($queries = array(), $page = 0, $maxpage = 0) {
        $almoxarifadosolicitacoes = new Almoxarifadosolicitacoes();
        return $almoxarifadosolicitacoes->getAlmoxarifadosolicitacoes($queries, $page, $maxpage);
    }

    public function getAlmoxarifadosolicitacoes($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " r1.id = $id ");

        $idfuncionarioresposta = (isset($queries['idfuncionarioresposta'])) ? (int) $queries['idfuncionarioresposta'] : false;
        if ($idfuncionarioresposta)
            array_push($where, " r1.idfuncionarioresposta = $idfuncionarioresposta ");

        $idlocal = (isset($queries['idlocal'])) ? (int) $queries['idlocal'] : false;
        if ($idlocal)
            array_push($where, " r1.idlocal = $idlocal ");

        $idescola = (isset($queries['idescola'])) ? (int) $queries['idescola'] : false;
        if ($idescola)
            array_push($where, " r1.idescola = $idescola ");

        $idperfil = (isset($queries['idperfil'])) ? (int) $queries['idperfil'] : false;
        if ($idperfil)
            array_push($where, " r1.idperfil = $idperfil ");

        $idperfilbusca = (isset($queries['idperfilbusca'])) ? (int) $queries['idperfilbusca'] : false;
        if ($idperfilbusca)
            array_push($where, " r1.idperfilbusca = $idperfilbusca ");

        $idperfilsolicitante = (isset($queries['idperfilsolicitante'])) ? (int) $queries['idperfilsolicitante'] : false;
        if ($idperfilsolicitante)
            array_push($where, " r1.idperfilsolicitante = $idperfilsolicitante ");

        $idsolicitante = (isset($queries['idsolicitante'])) ? (int) $queries['idsolicitante'] : false;
        if ($idsolicitante)
            array_push($where, " r1.idsolicitante = $idsolicitante ");

        $data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
        if ($data_i)
            array_push($where, " r1.datacriacao >= '$data_i' ");

        $data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
        if ($data_f)
            array_push($where, " r1.datacriacao <= '$data_f' ");

        $status1 = (isset($queries["status1"])) ? $queries["status1"] : false;
        if ($status1)
            array_push($where, " r1.status = '$status1' ");

        $dataentrada_i = (isset($queries["dataentrada_i"])) ? $queries["dataentrada_i"] : false;

        if ($dataentrada_i)
            array_push($where, " r1.dataentrada >= '$dataentrada_i' ");

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "r1.*";
        ;

        if ($total)
            $fields = "COUNT(r1.id) as total";

        $ordem = "ORDER BY r1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM almoxarifadosolicitacoes r1
					
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getSolicitacoes($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        $idlocal = (isset($queries['idlocal'])) ? (int) $queries['idlocal'] : false;
        $idresponsavel = (isset($queries['idresponsavel'])) ? (int) $queries['idresponsavel'] : false;
        $idestoque = (isset($queries['idestoque'])) ? (int) $queries['idestoque'] : false;

        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id)
            array_push($where, " asp.id = $id ");
        if ($idlocal)
            array_push($where, " vae.idlocal = $idlocal ");
        if ($idresponsavel)
            array_push($where, " vae.idresponsavel = $idresponsavel ");
        if ($idestoque)
            array_push($where, " asp.idestoque = $idestoque ");

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "vae.*, asp.id, asp.idestoque, asp.idsolicitante, u.nome, asp.idunidadefornecedora, asp.datacriacao, asp.logdata, ";

        $ordem = "ORDER BY asp.id";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        if ($total) {
            $strsql = "SELECT count(asp.id) as total FROM almoxarifadosolicitacoes asp INNER JOIN usuarios u ON (asp.idsolicitante = u.id) INNER JOIN funcionariosgeraisescolas fge ON (u.idfuncionario = fge.id) LEFT JOIN view_almoxarifado_estoques vae ON (asp.idestoque = vae.idestoque) WHERE asp.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM almoxarifadosolicitacoes asp INNER JOIN usuarios u ON (asp.idsolicitante = u.id) INNER JOIN funcionariosgeraisescolas fge ON (u.idfuncionario = fge.id) LEFT JOIN view_almoxarifado_estoques vae ON (asp.idestoque = vae.idestoque) WHERE asp.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;

        $db = Zend_Registry::get('db');

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();

        return $db->fetchAll($strsql);
    }

    public function getSolicitacoesProduto($queries = array()) {
        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;

        $strsql = "SELECT * FROM almoxarifadosolicitacoes WHERE id = $id";
        // echo $strsql;die;
        $db = Zend_Registry::get('db');
        return $db->fetchAll($strsql);
    }

    public function getSolicitacoesById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getAlmoxarifadosolicitacoes($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public function getEstoque($queries = array()) {
        $idfuncionario = (isset($queries['idfuncionario'])) ? (int) $queries['idfuncionario'] : false;
        $idperfil = (isset($queries['idperfil'])) ? (int) $queries['idperfil'] : false;
        $idescola = (isset($queries['idescola'])) ? (int) $queries['idescola'] : false;

        $where = array();

        if ($idfuncionario)
            array_push($where, " idfuncionario = $idfuncionario ");
        if ($idperfil)
            array_push($where, " idlocal = $idperfil ");
        if ($idescola)
            array_push($where, " idescola = $idescola ");

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "au.idfuncionario, au.idestoque, vae.estoque, vae.idlocal, vae.idescola, vae.escola, vae.perfil, vae.tipoestoque";

        $strsql = "SELECT $fields FROM almoxarifadousuarios au LEFT JOIN view_almoxarifado_estoques vae ON (au.idestoque = vae.idestoque)  WHERE au.excluido = 'nao' $w";

        $db = Zend_Registry::get('db');

        return $db->fetchAll($strsql);
    }

    public static function getAlmoxarifadosolicitacoesRespostaHelper() {
        $almoxarifadosolicitacoes = new Almoxarifadosolicitacoes();
        return $almoxarifadosolicitacoes->getAlmoxarifadosolicitacoesResposta($queries);
    }

    public function getAlmoxarifadosolicitacoesResposta() {

        $strsql = "SELECT distinct r1.idfuncionarioresposta 
					FROM almoxarifadosolicitacoes r1
					
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        return $db->fetchAll($strsql);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadosolicitacoes
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) {
            $oldstatus = $row['status'];
            $row = $this->createRow();
        } else {
            $novoRegistro = false;
        }

        $row->idestoque = (array_key_exists('idestoque', $dados)) ? $dados['idestoque'] : $row->idestoque;
        if ($dados['excluido'] == 'nao')
            $row->idsolicitante = (array_key_exists('idusuario', $dados)) ? $dados['idusuario'] : $row->idusuario;
        $row->idfuncionarioresposta = (array_key_exists("idfuncionarioresposta", $dados)) ? $dados["idfuncionarioresposta"] : $row->idfuncionarioresposta;
        $row->idescola = (array_key_exists('idescola', $dados)) ? $dados['idescola'] : $row->idescola;
        $row->idlocal = (array_key_exists('idlocal', $dados)) ? $dados['idlocal'] : $row->idlocal;
        $row->autorizacaonumero = (array_key_exists('autorizacaonumero', $dados)) ? $dados['autorizacaonumero'] : $row->autorizacaonumero;
        $row->idperfil = (array_key_exists('idperfil', $dados)) ? $dados['idperfil'] : $row->idperfil;
        $row->idperfilbusca = (array_key_exists('idperfilbusca', $dados)) ? $dados['idperfilbusca'] : $row->idperfilbusca;
        $row->idperfilsolicitante = (array_key_exists('idperfilsolicitante', $dados)) ? $dados['idperfilsolicitante'] : $row->idperfilsolicitante;
        $row->codigosolicitacao = (array_key_exists('codigosolicitacao', $dados)) ? $dados['codigosolicitacao'] : $row->codigosolicitacao;
        $row->verificasolicitacao = (array_key_exists('verificasolicitacao', $dados)) ? $dados['verificasolicitacao'] : $row->verificasolicitacao;
        $row->urgencia = (array_key_exists('urgencia', $dados)) ? $dados['urgencia'] : $row->urgencia;

        $row->status = (array_key_exists('status', $dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido', $dados)) ? $dados['excluido'] : $row->excluido;

        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');
        }

        $row->save();

        if($id===0){
            Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__ . "::Incluir", Usuarios::getUsuario("id"), "Solicitação, incluida com sucesso!");
        } else if ($row->excluido === 'Sim') {
            Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__ . "::Excluir", Usuarios::getUsuario("id"), "Solicitação ID = $id, excluida com sucesso!");
        } else if (isset ($oldstatus) AND $oldstatus !== $dados['status']) {
            Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__ . "::Status", Usuarios::getUsuario("id"), "Status da Solicitação ID = $id, ".$dados['status']." com sucesso!");
        } else {
            Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__ . "::Salvar", Usuarios::getUsuario("id"), "Solicitação, salva com sucesso!");
        }

        return $row;
    }

}
