<?php

/**
 * Define o modelo Mensagensalunos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Mensagensalunos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "mensagensalunos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getMensagensalunosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$mensagensalunos = new Mensagensalunos();
		return $mensagensalunos->getMensagensalunos($queries, $page, $maxpage);
	}
	
	public function getMensagensalunos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " m1.id = $id ");
		
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " m1.idescola = $idescola ");
		
		$idaluno = (isset($queries["idaluno"])) ? $queries["idaluno"] : false;
		if ($idaluno) array_push($where, " m1.idaluno = $idaluno ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " m1.titulo LIKE '%$titulo%' ");

$mensagem = (isset($queries["mensagem"])) ? $queries["mensagem"] : false;
		if ($mensagem) array_push($where, " m1.mensagem LIKE '%$mensagem%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " m1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "m1.*"; 
		;
		
		if ($total) $fields = "COUNT(m1.id) as total";
		
		$ordem = "ORDER BY m1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM mensagensalunos m1
					
					WHERE m1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getMensagensalunoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getMensagensalunos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getMensagensalunoByIdHelper($id, $queries = array()) {
		$rows = new Mensagensalunos();
		return $rows->getMensagensalunoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Mensagensalunos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
		$row->idserie = (array_key_exists("idserie",$dados)) ? $dados["idserie"] : $row->idserie;
		$row->serieturmaid = (array_key_exists("serieturmaid",$dados)) ? $dados["serieturmaid"] : $row->serieturmaid;
		$row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
		$row->mensagem = (array_key_exists("mensagem",$dados)) ? $dados["mensagem"] : $row->mensagem;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		if (is_null($row->datacriacao)) {
				$row->datacriacao = date("Y-m-d H:i:s");
			}
							
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
			
		//return true;		
		$row->save();
		
		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Mensagem email salva com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Mensagem email  do ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Mensagem email  do ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Mensagem email  do ID ".$id." atualizada com sucesso!");

		return $row;
	}
	
}