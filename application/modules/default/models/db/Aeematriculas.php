<?php

/**
 * Define o modelo Aeematriculas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Aeematriculas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "aeematriculas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAeematriculasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$aeematriculas = new Aeematriculas();
		return $aeematriculas->getAeematriculas($queries, $page, $maxpage);
	}
	
	public function getAeematriculas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
		
		
		$idaluno = (isset($queries["idaluno"])) ? $queries["idaluno"] : false;
		if ($idaluno) array_push($where, " a1.idaluno = $idaluno ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " a1.idescola = $idescola ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " a1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " a1.data <= '$data_f' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM aeematriculas a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAeematriculaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAeematriculas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAeematriculaByIdHelper($id, $queries = array()) {
		$rows = new Aeematriculas();
		return $rows->getAeematriculaById($id, $queries);
	}		
	
	public function getAeematriculaByAlunoId($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idaluno'] = $id;
		$rows = $this->getAeematriculas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAeematriculaByAlunoIdHelper($id, $queries = array()) {
		$rows = new Aeematriculas();
		return $rows->getAeematriculaByAlunoId($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Aeematriculas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->escolaregular = (array_key_exists("escolaregular",$dados)) ? $dados["escolaregular"] : $row->escolaregular;
		$row->idperiodo = (array_key_exists("idperiodo",$dados)) ? $dados["idperiodo"] : $row->idperiodo;
		$row->idauxiliar = (array_key_exists("idauxiliar",$dados)) ? $dados["idauxiliar"] : $row->idauxiliar;
		$row->idprofessor = (array_key_exists("idprofessor",$dados)) ? $dados["idprofessor"] : $row->idprofessor;
		$row->idprofessor2 = (array_key_exists("idprofessor2",$dados)) ? $dados["idprofessor2"] : $row->idprofessor2;
		$row->aulas = (array_key_exists("aulas",$dados)) ? $dados["aulas"] : $row->aulas;
		$row->iddeficiencia = (array_key_exists("iddeficiencia",$dados)) ? $dados["iddeficiencia"] : $row->iddeficiencia;
		$row->laudo = (array_key_exists("laudo",$dados)) ? $dados["laudo"] : $row->laudo;
		$row->cid = (array_key_exists("cid",$dados)) ? $dados["cid"] : $row->cid;
		$row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
		$row->idarquivotermocompromisso = (array_key_exists("idarquivotermocompromisso",$dados)) ? $dados["idarquivotermocompromisso"] : $row->idarquivotermocompromisso;
		$row->idarquivotermoautorizacaopasseio = (array_key_exists("idarquivotermoautorizacaopasseio",$dados)) ? $dados["idarquivotermoautorizacaopasseio"] : $row->idarquivotermoautorizacaopasseio;
		$row->idarquivotermodesistencia = (array_key_exists("idarquivotermodesistencia",$dados)) ? $dados["idarquivotermodesistencia"] : $row->idarquivotermodesistencia;
		$row->apoio = (array_key_exists("apoio",$dados)) ? $dados["apoio"] : $row->apoio;
		$row->necessidades = (array_key_exists("necessidades",$dados)) ? $dados["necessidades"] : $row->necessidades;
		$row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
		$row->transporte = (array_key_exists("transporte",$dados)) ? $dados["transporte"] : $row->transporte;
		$row->hipotesedeescrita = (array_key_exists("hipotesedeescrita",$dados)) ? $dados["hipotesedeescrita"] : $row->hipotesedeescrita;
		$row->transporteopcoes = (array_key_exists("transporteopcoes",$dados)) ? $dados["transporteopcoes"] : $row->transporteopcoes;
		$row->tipodeficiencia = (array_key_exists("tipodeficiencia",$dados)) ? $dados["tipodeficiencia"] : $row->tipodeficiencia;
		$row->atendimentoespecializado = (array_key_exists("atendimentoespecializado",$dados)) ? $dados["atendimentoespecializado"] : $row->atendimentoespecializado;
		$row->atendimentoespecializadosimnao = (array_key_exists("atendimentoespecializadosimnao",$dados)) ? $dados["atendimentoespecializadosimnao"] : $row->atendimentoespecializadosimnao;
		$row->atendimentoespecializadoObs = (array_key_exists("atendimentoespecializadoObs",$dados)) ? $dados["atendimentoespecializadoObs"] : $row->atendimentoespecializadoObs;
		$row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
		$row->responsavel = (array_key_exists("responsavel",$dados)) ? $dados["responsavel"] : $row->responsavel;
		$row->medicacao = (array_key_exists("medicacao",$dados)) ? $dados["medicacao"] : $row->medicacao;
		$row->qualmedicacao = (array_key_exists("qualmedicacao",$dados)) ? $dados["qualmedicacao"] : $row->qualmedicacao;
		$row->horarioMedicacao = (array_key_exists("horarioMedicacao",$dados)) ? $dados["horarioMedicacao"] : $row->horarioMedicacao;
		//  $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		if (is_null($row->datacriacao)) {
					$row->datacriacao = date("Y-m-d H:i:s");
				}
		if (is_null($row->data)) {
			$row->data = date("Y-m-d H:i:s");
		}
								
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
			 		
		$row->save();
		if (array_key_exists('materias', $dados)) $this->setMaterias($dados['materias'], $row->id);

		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::save", Usuarios::getUsuario("id"), "Aee matricula salva com sucesso!");
		}
		elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Aee matricula com ID ".$id." excluída com sucesso!");
		}
		elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Aee matricula com ID ".$id." - ". $dados['status']." com sucesso!");
		}
		else 
		Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Aee matricula com ID ".$id." atualizada com sucesso!");

		return $row;
	}

	private function setMaterias($dados, $idaee) {
		
		if (!is_array($dados)) return;
		 
		$itens = new Aeelistamaterias();
		$segundafeira = $dados['segundafeira'];
		$tercafeira = $dados['tercafeira'];
		$quartafeira = $dados['quartafeira'];
		$quintafeira = $dados['quintafeira'];
		$sextafeira = $dados['sextafeira'];

		$ids = array();
		foreach ($segundafeira as $i=>$id) {
		 
			$d = array();
			
			$d['idaee'] = $idaee; 
			$d['segundafeira'] = trim(strip_tags($segundafeira[$i]));
			$d['tercafeira'] = trim(strip_tags($tercafeira[$i]));       
			$d['quartafeira'] = trim(strip_tags($quartafeira[$i]));       
			$d['quintafeira'] = trim(strip_tags($quintafeira[$i]));       
			$d['sextafeira'] = trim(strip_tags($sextafeira[$i]));       
			$d['logusuario'] = $dados['logusuario'];
			$d['logdata'] = $dados['logdata'];
			if( empty($d['segundafeira']) AND  empty($d['tercafeira'])  AND  empty($d['quartafeira'])  AND  empty($d['quintafeira']) AND  empty($d['sextafeira'])){
				continue;	 
			}
				$_row = $itens->save($d); 		
			array_push($ids, $_row->id);
		}
		$materias = implode(",", $ids);
       
		if ($materias=="") $materias = "0";

		$strsql = "DELETE FROM aeelistamaterias WHERE idaee=$idaee AND id NOT IN ($materias)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);
		
	}
	
}