<?php

/**
 * Define o modelo Pontuacoesadidos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Pontuacoesadidos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "pontuacoesadidos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getPontuacoesadidosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$pontuacoesadidos = new Pontuacoesadidos();
		return $pontuacoesadidos->getPontuacoesadidos($queries, $page, $maxpage);
	}
	
	public function getPontuacoesadidos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		
		$idprofessor = (isset($queries["idprofessor"])) ? $queries["idprofessor"] : false;
		if ($idprofessor) array_push($where, " p1.idprofessor = $idprofessor ");

		$idtipopontuacao = (isset($queries["idtipopontuacao"])) ? $queries["idtipopontuacao"] : false;
		if ($idtipopontuacao) array_push($where, " p1.idtipopontuacao = $idtipopontuacao ");

		$adidos = (isset($queries["adidos"])) ? $queries["adidos"] : false;
		if ($adidos) array_push($where, " p1.adidos = '$adidos' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status LIKE '%$status%' ");		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM pontuacoesadidos p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		
		$db = Zend_Registry::get('db');				
		
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPontuacaoadidoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPontuacoesadidos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPontuacaoadidoByIdHelper($id, $queries = array()) {
		$rows = new Pontuacoesadidos();
		return $rows->getPontuacaoadidoById($id, $queries);
	}		
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Pontuacoesadidos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idprofessor = (array_key_exists("idprofessor",$dados)) ? $dados["idprofessor"] : $row->idprofessor;
		$row->idtipopontuacao = (array_key_exists("idtipopontuacao",$dados)) ? $dados["idtipopontuacao"] : $row->idtipopontuacao;
		$row->adidos = (array_key_exists("adidos",$dados)) ? $dados["adidos"] : $row->adidos;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->save();
		
		return $row;
	}
	
}