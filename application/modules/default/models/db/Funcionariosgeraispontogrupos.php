<?php

/**
 * Define o modelo Atas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0  
 */
class Funcionariosgeraispontogrupos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosgeraispontogrupos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAtasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$atas = new Atas();
		return $atas->getAtas($queries, $page, $maxpage);
	}
	
	public function getAtas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
              
                
                $idlocal = (isset($queries["idlocal"])) ? $queries["idlocal"] : false;
		if ($idlocal) array_push($where, " a1.idlocal = $idlocal ");
                
                $idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " a1.idfuncionario = $idfuncionario ");
                
                $rgf = (isset($queries["rgf"])) ? $queries["rgf"] : false;
		if ($rgf) array_push($where, " a1.rgf = $rgf ");
                
                $titulolocal = (isset($queries["titulolocal"])) ? $queries["titulolocal"] : false;
		if ($titulolocal) array_push($where, " a1.titulolocal LIKE '%$titulolocal%' ");
		
                /*
		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " a1.iddepartamento = $iddepartamento ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " a1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " a1.data <= '$data_f' ");

$idperiodo = (isset($queries["idperiodo"])) ? $queries["idperiodo"] : false;
		if ($idperiodo) array_push($where, " a1.idperiodo = $idperiodo ");

$idsfuncionarios = (isset($queries["idsfuncionarios"])) ? $queries["idsfuncionarios"] : false;
		if ($idsfuncionarios) array_push($where, " a1.idsfuncionarios = '$idsfuncionarios' ");

$pauta = (isset($queries["pauta"])) ? $queries["pauta"] : false;
		if ($pauta) array_push($where, " a1.pauta LIKE '%$pauta%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " a1.descricoes LIKE '%$descricoes%' ");
        */
                
                $status = (isset($queries["status"])) ? $queries["status"] : false;
                                if ($status) array_push($where, " a1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgeraispontogrupos a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
               	
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAtaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAtas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAtaByIdHelper($id, $queries = array()) {
		$rows = new Atas();
		return $rows->getAtaById($id, $queries);
	}	
        
        
        /*
         *      Autor: Thiago Torres
         *      Funçõa para retornar informações do funcionário cadastrado 
         *      Futuramente gostaria de retornar uma classe com essas funções
         */
        public static function retornaIdFuncionario($id){
            $id = "5";
            //$strSql = "SELECT * FROM funcionariosgeraispontosgrupos where id = 5";
            
            //$db = Zend_Registry::get('db');				
	    //$row = $db->fetchRow($strsql);
            
            //print_r($row['idfuncionario']);
            //die();
            return $id;
				
            
            
        }//end functino retornaIdfuncinoario
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Atas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
                
                $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
                $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
                $row->rgf = (array_key_exists("rgf",$dados)) ? $dados["rgf"] : $row->rgf;
                $row->titulolocal = (array_key_exists("titulolocal",$dados)) ? $dados["titulolocal"] : $row->titulolocal;
                $row->idhorario = (array_key_exists("idhorario",$dados)) ? $dados["idhorario"] : $row->idhorario;
                
                /*
                $row->horaentrada = (array_key_exists("horaentrada",$dados)) ? $dados["horaentrada"] : $row->horaentrada;
                $row->horasaida = (array_key_exists("horasaida",$dados)) ? $dados["horasaida"] : $row->horasaida;
                $row->horaentradapausa = (array_key_exists("horaentradapausa",$dados)) ? $dados["horaentradapausa"] : $row->horaentradapausa;
                $row->horasaidapausa = (array_key_exists("horasaidapausa",$dados)) ? $dados["horasaidapausa"] : $row->horasaidapausa;
/*                
		 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->idperiodo = (array_key_exists("idperiodo",$dados)) ? $dados["idperiodo"] : $row->idperiodo;
 $row->idsfuncionarios = (array_key_exists("idsfuncionarios",$dados)) ? $dados["idsfuncionarios"] : $row->idsfuncionarios;
 $row->pauta = (array_key_exists("pauta",$dados)) ? $dados["pauta"] : $row->pauta;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->idarquivoata = (array_key_exists("idarquivoata",$dados)) ? $dados["idarquivoata"] : $row->idarquivoata;
 */
 
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
        
        /*
         *  Função criada para a tabela localizada em PontosMensais,
         *  dataInicio -> Será a data para o filtro de lançamentos realizados
         *  dataFim -> Será a data final do filtro de lançamentos realizados
         *  page -> informações da paginação para o formulário 
         *  maxpage -> número máximo de páginas apresentadas nas tabelas 
         */
        public function FuncionariosGeraisPontosResumo($queries = array(), $page = 0, $maxpage = 0, $dataInicio, $dataFim){
            
            $where = array();
            
            
		
            $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
            $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
            $order = (isset($queries['order'])) ? $queries['order'] : false;

            $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
            if ($id) array_push($where, " a1.id = $id ");


            $idlocal = (isset($queries["idlocal"])) ? $queries["idlocal"] : false;
            if ($idlocal) array_push($where, " a1.idlocal = $idlocal ");

            $idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
            if ($idfuncionario) array_push($where, " a1.idfuncionario = $idfuncionario ");

            $rgf = (isset($queries["rgf"])) ? $queries["rgf"] : false;
            if ($rgf) array_push($where, " a1.rgf LIKE '%$rgf%' ");

            $titulolocal = (isset($queries["titulolocal"])) ? $queries["titulolocal"] : false;
            if ($titulolocal) array_push($where, " a1.titulolocal LIKE '%$titulolocal%' ");
		
               
                
            $status = (isset($queries["status"])) ? $queries["status"] : false;
                            if ($status) array_push($where, " a1.status LIKE '%$status%' ");

            if ($sorting) {
                    $sorting = explode('_', $sorting);
                    if (sizeof($sorting)==2) {

                            if ($sorting[0]=='nome') $sorting[0]='l1.nome';

                            $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
                    }
            }		

            $w = "";
            foreach ($where as $k=>$v) {
                    if ($k>0) $w .= " AND ";
                    $w .= $v;
            }
            if ($w!="") $w = "AND ($w)";

            $fields = "a1.*"; 
            ;

            if ($total) $fields = "COUNT(a1.id) as total";

            $ordem = "ORDER BY a1.id DESC";
            if ($order) $ordem = $order; 

            $limit = "";
            if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

            $strsql = "SELECT $fields 
                                    FROM funcionariosgeraispontogrupos a1

                                    WHERE a1.excluido='nao' 
                                            $w 
                                    $ordem	
                                    $limit";	

            
            if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
            $db = Zend_Registry::get('db');				
            if ($total) {
                    $row = $db->fetchRow($strsql);
                    return $row['total'];
            }	
            
            $rowsRetorno = array();
            $rows = $db->fetchAll($strsql);
            
            //Instanciando Model de Pontos para rechear o retorno da model Pontos Gerais
            $pontos = new Funcionariosgeraispontohora();
            
            $totalDiasdoPeriodo = $pontos->getDiasUteisPeriodo($dataInicio, $dataFim); // setando o total de dias uteis do período
            
            foreach($rows as $row):
                
                $row['nome_funcionario'] = $row['id'];
                $row['lancamentos'] = $pontos->getLancamentosConfirmados($dataInicio,$dataFim,$row['id'])['Total'];
                $row['totalDiasUteis'] = $totalDiasdoPeriodo;
                $row['confirmados'] = $pontos->getRhConfirmados($dataInicio,$dataFim,$row['id'])['Total'];
                $row['ocorrencias'] = $pontos->getOcorrencias($dataInicio,$dataFim,$row['id'])['Total'];
                array_push($rowsRetorno, $row);
              
            endforeach;
            
           // echo '<pre>';
           // print_r($rowsRetorno);
           // die();

            $rowsRetornoOcorrencias = array();
            // eu sei que poderia ter resolvido com uma query legal esse problema, mas iria demorar pra me entender no sql depois ajeito esse model
            if($queries['ocorrenciafiltro'] == 'ok'):
                
                foreach($rowsRetorno as $index):

                        if($index['ocorrencias'] != $index['lancamentos']):
                                array_push($rowsRetornoOcorrencias, $index);
                        endif;  

                endforeach;

                return $rowsRetornoOcorrencias;

            else:    
                return $rowsRetorno;	
            endif;    
           	
            
        }//end public funcition FuncionariosGeraisPontosResumo


        public static function getGrupoSelectRH($case){ 
            //  and (usuarios.idperfil = 29)
            //$strQueryOne = "SELECT * FROM usuarios INNER JOIN funcionariosgeraisescolas ON usuarios.idfuncionario = funcionariosgeraisescolas.id INNER JOIN usuariosperfislimites ON usuariosperfislimites.idperfil = usuarios.idperfil WHERE usuariosperfislimites.idlimite = 549 order by usuarios.desclocal";
            $strQueryOne = "SELECT * FROM usuarios INNER JOIN funcionariosgeraisescolas ON usuarios.idfuncionario = funcionariosgeraisescolas.id INNER JOIN usuariosperfislimites ON usuariosperfislimites.idperfil = usuarios.idperfil WHERE usuariosperfislimites.idlimite = 549 order by usuarios.desclocal";
            $strQueryTwo = "SELECT * FROM escolas";
            //$strQueryTwo = "SELECT usuarios.id FROM usuarios INNER JOIN funcionariosgeraisescolas ON usuarios.idfuncionario = funcionariosgeraisescolas.id INNER JOIN usuariosperfislimites ON usuariosperfislimites.idperfil = usuarios.idperfil WHERE usuariosperfislimites.idlimite = 549 order by usuarios.desclocal";
            //$strQuery = "SELECT * FROM usuarios INNER JOIN funcionariosgeraisescolas WHERE usuarios.idperfil in (SELECT idperfil FROM usuariosperfislimites WHERE usuariosperfislimites.idlimite = 188 AND usuariosperfislimites.editar = 'sim') GROUP BY usuarios.id";

            // $strQuery = "SELECT * FROM usuarios INNER JOIN funcionariosgeraisescolas";

            $db = Zend_Registry::get('db');
            $rowsOne = $db->fetchAll($strQueryOne);
          

            return $rowsOne;

        }//end public static function

        public static function getGrupoSelectRHNew(){

                
		$query = 'select';
		$query .= ' e.id as idescola,';
		$query .= ' e.escola as escola,';
		$query .= ' f.id as idfuncionario,';
		$query .= ' f.idescola as funcescola,';
		$query .= ' f.idsfuncionarios as funcionarios,';
		$query .= ' f.idperfil as idperfil,';
		$query .= ' f.idlocal as local,';
		$query .= ' u.idperfil as usuarioperfil,';
		$query .= ' u.id as idusuario';
		$query .= ' from';
		$query .= ' escolas as e';
		$query .= ' inner join';
		$query .= ' funcionariosgeraisescolas as f';
		$query .= ' on e.id = f.idescola';
		$query .= ' inner join';
		$query .= ' usuarios as u';
		$query .= ' on f.id = u.idfuncionario';
		$query .= ' group by e.id';	
		$query .= ' order by e.escola';

		$query = 'select * from escolas';

		$query = 'select '; 
		$query .= 'u.id as idusuario, ';
		$query .= 'u.idperfil, ';
		$query .= 'u.iddepartamento, ';
		$query .= 'u.rgf, ';
		$query .= 'u.senha, ';
		$query .= 'f.id as idfuncionario, ';
                $query .= 'f.idescola, ';
                $query .= 'f.nome, ';
		$query .= 'f.idlocal, ';
                $query .= 'f.desclocal, ';
                $query .= 'f.idsecretaria, ';
		$query .= 'e.id, ';
		$query .= 'e.escola, ';
		$query .= 'p.visualizar, ';
		$query .= 'p.adicionar, ';
		$query .= 'p.editar, ';
		$query .= 'p.excluir ';
		$query .= 'from '; 
		$query .= 'usuarios as u ';
		$query .= 'inner join '; 
		$query .= 'funcionariosgeraisescolas as f on u.idfuncionario = f.id '; 
		$query .= 'inner join '; 
		$query .= 'escolas as e on e.id = f.idescola ';
		$query .= 'inner join '; 
		$query .= 'usuariosperfislimites as p on p.idperfil = u.idperfil ';
		$query .= 'where ';
		$query .= 'p.idlimite = "188" ';
		$query .= 'and p.visualizar = "sim" ';
		$query .= 'and p.editar = "sim" ';
		$query .= 'and p.excluir = "sim" ';
		$query .= 'and p.adicionar = "sim" ';
		$query .= 'order by e.escola';

		$db = Zend_Registry::get('db');	

		return $db->fetchAll($query);
    

        }//end static function 

        public static function getDepartamentosRH(){

		$query = 'select '; 
		$query .= 'u.id as id, ';
		$query .= 'f.id as idfuncionario, ';
		$query .= 'u.nome as nome, ';
		$query .= 'f.rgf as rgf, ';
		$query .= 'u.senha as senha, ';
		$query .= 'f.idlocal as local, ';
		$query .= 'f.idsecretaria, ';
		$query .= 'f.idescola, ';
		$query .= 'u.idperfil as perfil, ';
		$query .= 'p.perfil as nomeperfil ';
		$query .= 'from ';
		$query .= 'usuarios as u '; 
		$query .= 'inner join '; 
		$query .= 'funcionariosgeraisescolas as f ';
		$query .= 'on u.idfuncionario = f.id ';
		$query .= 'inner join ';
		$query .= 'usuariosperfis as p '; 
		$query .= 'on p.id = u.idperfil ';
		$query .= 'inner join ';
		$query .= 'usuariosperfislimites as up on up.idperfil = u.idperfil ';
		$query .= 'where ';
		$query .= 'f.idescola is null ';
		$query .= 'and up.idlimite = "188" ';
		$query .= 'and up.visualizar = "sim" ';
		$query .= 'and up.editar = "sim" ';
		$query .= 'and up.excluir = "sim" ';
		$query .= 'and up.adicionar = "sim" ';
		$query .= 'order by ';
		$query .= 'up.idperfil ';

		$db = Zend_Registry::get('db');	

		return $db->fetchAll($query);
		
	}//end public function 

        public static function getfuncionarioByIdFunc($id){

                $strQuery = "SELECT * FROM usuarios where idfuncionario = $id";   

                $db = Zend_Registry::get('db');
                $rowsOne = $db->fetchAll($strQuery);
                
                return $rowsOne[0];
                
        }//end public static function
	
}