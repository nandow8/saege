<?php

/**
 * Define o modelo Escolasconfiguracoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasconfiguracoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasconfiguracoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolasconfiguracoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasconfiguracoes = new Escolasconfiguracoes();
		return $escolasconfiguracoes->getEscolasconfiguracoes($queries, $page, $maxpage);
	}
	
	public function getEscolasconfiguracoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " e1.idsecretaria = $idsecretaria ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");

$quantidadenotas = (isset($queries["quantidadenotas"])) ? $queries["quantidadenotas"] : false;
		if ($quantidadenotas) array_push($where, " e1.quantidadenotas = '$quantidadenotas' ");

$quantidadetrabalhos = (isset($queries["quantidadetrabalhos"])) ? $queries["quantidadetrabalhos"] : false;
		if ($quantidadetrabalhos) array_push($where, " e1.quantidadetrabalhos = '$quantidadetrabalhos' ");

$periodos = (isset($queries["periodos"])) ? $queries["periodos"] : false;
		if ($periodos) array_push($where, " e1.periodos LIKE '%$periodos%' ");

$datainicio1_i = (isset($queries["datainicio1_i"])) ? $queries["datainicio1_i"] : false;
		if ($datainicio1_i) array_push($where, " e1.datainicio1 >= '$datainicio1_i' ");

$datainicio1_f = (isset($queries["datainicio1_f"])) ? $queries["datainicio1_f"] : false;
		if ($datainicio1_f) array_push($where, " e1.datainicio1 <= '$datainicio1_f' ");

$datafim1_i = (isset($queries["datafim1_i"])) ? $queries["datafim1_i"] : false;
		if ($datafim1_i) array_push($where, " e1.datafim1 >= '$datafim1_i' ");

$datafim1_f = (isset($queries["datafim1_f"])) ? $queries["datafim1_f"] : false;
		if ($datafim1_f) array_push($where, " e1.datafim1 <= '$datafim1_f' ");

$datainicio2_i = (isset($queries["datainicio2_i"])) ? $queries["datainicio2_i"] : false;
		if ($datainicio2_i) array_push($where, " e1.datainicio2 >= '$datainicio2_i' ");

$datainicio2_f = (isset($queries["datainicio2_f"])) ? $queries["datainicio2_f"] : false;
		if ($datainicio2_f) array_push($where, " e1.datainicio2 <= '$datainicio2_f' ");

$datafim2_i = (isset($queries["datafim2_i"])) ? $queries["datafim2_i"] : false;
		if ($datafim2_i) array_push($where, " e1.datafim2 >= '$datafim2_i' ");

$datafim2_f = (isset($queries["datafim2_f"])) ? $queries["datafim2_f"] : false;
		if ($datafim2_f) array_push($where, " e1.datafim2 <= '$datafim2_f' ");

$datainicio3_i = (isset($queries["datainicio3_i"])) ? $queries["datainicio3_i"] : false;
		if ($datainicio3_i) array_push($where, " e1.datainicio3 >= '$datainicio3_i' ");

$datainicio3_f = (isset($queries["datainicio3_f"])) ? $queries["datainicio3_f"] : false;
		if ($datainicio3_f) array_push($where, " e1.datainicio3 <= '$datainicio3_f' ");

$datafim3_i = (isset($queries["datafim3_i"])) ? $queries["datafim3_i"] : false;
		if ($datafim3_i) array_push($where, " e1.datafim3 >= '$datafim3_i' ");

$datafim3_f = (isset($queries["datafim3_f"])) ? $queries["datafim3_f"] : false;
		if ($datafim3_f) array_push($where, " e1.datafim3 <= '$datafim3_f' ");

$datainicio4_i = (isset($queries["datainicio4_i"])) ? $queries["datainicio4_i"] : false;
		if ($datainicio4_i) array_push($where, " e1.datainicio4 >= '$datainicio4_i' ");

$datainicio4_f = (isset($queries["datainicio4_f"])) ? $queries["datainicio4_f"] : false;
		if ($datainicio4_f) array_push($where, " e1.datainicio4 <= '$datainicio4_f' ");

$datafim4_i = (isset($queries["datafim4_i"])) ? $queries["datafim4_i"] : false;
		if ($datafim4_i) array_push($where, " e1.datafim4 >= '$datafim4_i' ");

$datafim4_f = (isset($queries["datafim4_f"])) ? $queries["datafim4_f"] : false;
		if ($datafim4_f) array_push($where, " e1.datafim4 <= '$datafim4_f' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasconfiguracoes e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolasconfiguracaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasconfiguracoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolasconfiguracaoByIdHelper($id, $queries = array()) {
		$rows = new Escolasconfiguracoes();
		return $rows->getEscolasconfiguracaoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasconfiguracoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->quantidadenotas = (array_key_exists("quantidadenotas",$dados)) ? $dados["quantidadenotas"] : $row->quantidadenotas;
 $row->quantidadetrabalhos = (array_key_exists("quantidadetrabalhos",$dados)) ? $dados["quantidadetrabalhos"] : $row->quantidadetrabalhos;
 $row->quantidadetarefas = (array_key_exists("quantidadetarefas",$dados)) ? $dados["quantidadetarefas"] : $row->quantidadetarefas;


$row->percentualtrabalhos = (array_key_exists("percentualtrabalhos",$dados)) ? $dados["percentualtrabalhos"] : $row->percentualtrabalhos;
$row->percentualprovas = (array_key_exists("percentualprovas",$dados)) ? $dados["percentualprovas"] : $row->percentualprovas;
$row->percentualparticipacao = (array_key_exists("percentualparticipacao",$dados)) ? $dados["percentualparticipacao"] : $row->percentualparticipacao;
$row->percentualtarefas = (array_key_exists("percentualtarefas",$dados)) ? $dados["percentualtarefas"] : $row->percentualtarefas;
 $row->periodos = (array_key_exists("periodos",$dados)) ? $dados["periodos"] : $row->periodos;
 $row->datainicio1 = (array_key_exists("datainicio1",$dados)) ? $dados["datainicio1"] : $row->datainicio1;
 $row->datafim1 = (array_key_exists("datafim1",$dados)) ? $dados["datafim1"] : $row->datafim1;
 $row->datainicio2 = (array_key_exists("datainicio2",$dados)) ? $dados["datainicio2"] : $row->datainicio2;
 $row->datafim2 = (array_key_exists("datafim2",$dados)) ? $dados["datafim2"] : $row->datafim2;
 $row->datainicio3 = (array_key_exists("datainicio3",$dados)) ? $dados["datainicio3"] : $row->datainicio3;
 $row->datafim3 = (array_key_exists("datafim3",$dados)) ? $dados["datafim3"] : $row->datafim3;
 $row->datainicio4 = (array_key_exists("datainicio4",$dados)) ? $dados["datainicio4"] : $row->datainicio4;
 $row->datafim4 = (array_key_exists("datafim4",$dados)) ? $dados["datafim4"] : $row->datafim4;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}