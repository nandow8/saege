<?php

/**
 * Define o modelo Juridicoprocessoslancamentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Juridicoprocessoslancamentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "juridicoprocessoslancamentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getJuridicoprocessoslancamentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$juridicoprocessoslancamentos = new Juridicoprocessoslancamentos();
		return $juridicoprocessoslancamentos->getJuridicoprocessoslancamentos($queries, $page, $maxpage);
	}
	
	public function getJuridicoprocessoslancamentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " j1.id = $id ");
		
		
		$idjuridicoprocesso = (isset($queries["idjuridicoprocesso"])) ? $queries["idjuridicoprocesso"] : false;
		if ($idjuridicoprocesso) array_push($where, " j1.idjuridicoprocesso = $idjuridicoprocesso ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " j1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " j1.data <= '$data_f' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " j1.descricoes = '$descricoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " j1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "j1.*"; 
		;
		
		if ($total) $fields = "COUNT(j1.id) as total";
		
		$ordem = "ORDER BY j1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM juridicoprocessoslancamentos j1
					
					WHERE j1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getJuridicoprocessoslancamentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getJuridicoprocessoslancamentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getJuridicoprocessoslancamentoByIdHelper($id, $queries = array()) {
		$rows = new Juridicoprocessoslancamentos();
		return $rows->getJuridicoprocessoslancamentoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Juridicoprocessoslancamentos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idjuridicoprocesso = (array_key_exists("idjuridicoprocesso",$dados)) ? $dados["idjuridicoprocesso"] : $row->idjuridicoprocesso;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}