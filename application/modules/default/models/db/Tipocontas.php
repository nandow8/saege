<?php

/**
 * Define o modelo Tipocontas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Tipocontas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "tipocontas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getTipocontasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$tipocontas = new Tipocontas();
		return $tipocontas->getTipocontas($queries, $page, $maxpage);
	}
	
	public function getTipocontas($queries = array(), $page = 0, $maxpage = 0) {

		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$status = (isset($queries["status"])) ? $queries["status"] : false;
		$identificacao = (isset($queries["identificacao"])) ? $queries["identificacao"] : false;
		$descricao = (isset($queries["descricao"])) ? $queries["descricao"] : false;
		$local = (isset($queries["local"])) ? $queries["local"] : false;
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;

		if ($id) array_push($where, " c1.id = $id ");
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");
		if ($identificacao) array_push($where, " c1.identificacao LIKE '%$identificacao%' ");
		if ($descricao) array_push($where, " c1.descricao LIKE '%$descricao%' ");
		if ($local) array_push($where, " c1.local LIKE '%$local%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='identificacao') $sorting[0]='c1.identificacao';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}

		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*";

		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM tipocontas c1
					
					WHERE c1.excluido='nao' 
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getTipocontasById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTipocontas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTipocontasByIdHelper($id, $queries = array()) {
		$rows = new Tipocontas();
		return $rows->getTipocontasById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Tipocontas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		
 		$row->identificacao = (array_key_exists("identificacao",$dados)) ? $dados["identificacao"] : $row->identificacao;
 		$row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
 		$row->local = (array_key_exists("local", $dados)) ? $dados["local"] : $row->local;

 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;	
				
		$row->save();
		
		return $row;
	}
	
}