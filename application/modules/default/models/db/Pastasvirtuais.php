<?php

/**
 * Define o modelo Pastasvirtuais
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Pastasvirtuais extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "pastasvirtuais";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getPastasvirtuaisHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$pastasvirtuais = new Pastasvirtuais();
		return $pastasvirtuais->getPastasvirtuais($queries, $page, $maxpage);
	}
	
	public function getPastasvirtuais($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " p1.idsecretaria = $idsecretaria ");
		$idparent = (isset($queries["idparent"])) ? $queries["idparent"] : false;
		if ($idparent) array_push($where, " p1.idparent = $idparent ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " p1.titulo LIKE '%$titulo%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " p1.descricoes = '$descricoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status LIKE '%$status%' ");

$parentzero = (isset($queries['parentzero'])) ? $queries['parentzero'] : false;

$tipo = (isset($queries['tipo'])) ? $queries['tipo'] : false;

$origem = (isset($queries['origem'])) ? $queries['origem'] : false;

		if ($tipo) array_push($where, " p1.tipo='$tipo' ");
		if ($origem) array_push($where, " p1.origem='$origem' ");
		if ($parentzero) array_push($where, " p1.idparent=0 ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM pastasvirtuais p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPastavirtualById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPastasvirtuais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPastavirtualByIdHelper($id, $queries = array()) {
		$rows = new Pastasvirtuais();
		return $rows->getPastavirtualById($id, $queries);
	}	

	public static function getSubPastas($idpastaraiz, $queries = array()) {
		if ($idpastaraiz==0) return false;

		$queries = array();
		$queries['idparent'] = $idpastaraiz;
		$queries['status'] = "Ativo";

		$rows = new Pastasvirtuais();		
		return $rows->getPastasvirtuais($queries);
	}	

	public static function getBreadcrumbPastas($id, $breadcrumb, $baseurl, $queries = array()) {
		if ($id==0) return false;

		$rows = new Pastasvirtuais();
		$row = $rows->getPastavirtualById($id);

		if((isset($row['id'])) && (isset($row['idparent'])) && ($row['idparent'] > 0)):
			$breadcrumb .= Pastasvirtuais::getBreadcrumbPastas();
		elseif((isset($row['id'])) && ($row['idparent'] == 0)):
			return $breadcrumb = '<li class=""><a class="" href="/prefeitura_v3/admin/pastasvirtuais/editar/id/24">Jornalismo</a>></li>';
		endif;


		$queries = array();
		$queries['idparent'] = $id;
		$row = $rows->getPastasvirtuais($queries);

		return $rows->getPastavirtualById($id, $queries);
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Pastasvirtuais
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->idparent = (array_key_exists("idparent",$dados)) ? $dados["idparent"] : $row->idparent;
 $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->idusuariocriacao = (array_key_exists("idusuariocriacao",$dados)) ? $dados["idusuariocriacao"] : $row->idusuariocriacao;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}