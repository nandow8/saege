<?php

/**
 * Define o modelo Usuarios
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Usuarios extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "usuarios";
    public static $_TIPO_ADMIN = 'administrador';

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";
    public static $_DEPARTAMENTO_RH = 'rh';
    public static $_DEPARTAMENTO_JURIDICO = 'juridico';

    public static function getSecretariaAtiva($id, $field = false) {
        $usuarios = new Usuarios();
        $usuario = $usuarios->fetchRow("id=$id");

        if (!$usuario)
            return false;


        $idsecretaria = (int) $usuario['idsecretariaativa'];

        if ((int) $usuario['idsecretariaativa'] <= 0) {
            $secretaria = explode(',', $usuario['idsecretarias']);
            if ($secretaria[0])
                $idsecretaria = (int) $secretaria[0];
        }

        $secretarias = new Secretarias();
        $secretaria = $secretarias->fetchRow("id=$idsecretaria");
        if (!$secretaria)
            return false;

        if (!$field)
            return $secretaria;

        return $secretaria[$field];
    }

    public static function getEscolaAtiva($id, $field = false) {
        $usuarios = new Usuarios();
        $usuario = $usuarios->fetchRow("id=$id");

        if (!$usuario)
            return false;
        $usuario = $usuario->toArray();

        $funcionariosgeraisescolas = new Funcionariosgeraisescolas();
        $funcionariogeralescola = $funcionariosgeraisescolas->getFuncionariogeralescolaById($usuario['idfuncionario']);

        if (!$funcionariogeralescola)
            return false;

        $idescola = (int) $funcionariogeralescola['idescola'];

        if ((int) $funcionariogeralescola['idescola'] <= 0) {
            $_escola = explode(',', $funcionariogeralescola['idescolas']);
            if ($_escola[0])
                $idescola = (int) $_escola[0];
        }

        if ((int) $idescola <= 0)
            return false;

        $escolas = new Escolas();
        $escola = $escolas->fetchRow("id=$idescola");
        if (!$escola)
            return false;

        if (!$field)
            return $escola;

        return $escola[$field];
    }

    /**
     * Função que lista os Status para passar a solicitação entre as fases (PENDENTE, EM EXECUÇÃo, BAIXA ou CANCELADO)
     */
    public static function getDepartamentos($field = false) {
        $res = array(
            self::$_DEPARTAMENTO_RH => 'RH',
            self::$_DEPARTAMENTO_JURIDICO => 'Jurídico',
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static function isAdmin($idusuario) {
        $usuario = Usuarios::getUsuarioByIdHelper(Usuarios::getUsuario('id'));
        return ($usuario['tipo'] == 'administrador');
        //return  ($usuario['tipo']==Usuarios::$_TIPO_ADMIN);
    }

    public static function podeVisualizar($id, $idsolicitante) {
        $strsql = "SELECT * FROM usuarios WHERE excluido='nao' AND id=$id AND (id=$idsolicitante OR idparent=$idsolicitante)";
        $db = Zend_Registry::get('db');

        $row = $db->fetchRow($strsql);
        return !$row;
    }

    /**
     * Retorno o objeto do Usuario em sessao ou um campo
     * @param string $field
     * @return string|array
     */
    public static function getUsuario($field = null) {
        $session = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
        if (!isset($session->usuario))
            return false;

        $usuario = unserialize($session->usuario);
        if (is_null($field))
            return $usuario;

        return (isset($usuario[$field])) ? $usuario[$field] : false;
    }

    /**
     *
     * Envio de e-mails para a comunicação
     * @param string $email
     * @param string $senha
     */
    public static function sendComunicacao($nome, $email, $titulo, $descricoes, $observacoes) {
        $body = "<h1>" . $titulo . "</h1>";
        $body .= "<p>" . $descricoes . "</p>";
        $body .= "<p>" . $observacoes . "</p>";
        $body .= "<br>";
        $body .= "<p><small> Secretaria de Educação de Santa Isabel</small></p>";
        $body .= "<p><small> Departamento de Comunicação</small></p>";

        Mn_Util::sendMailComunicacao($email, $titulo, $body, null);
    }

//end sendComunicacao

    /**
     *
     * Cria o texto e envia a senha via email
     * @param string $email
     * @param string $senha
     */
    public static function sendSenha($email, $senha) {
        $body = 'Seus dados de acesso para o painel de controle da<br /> Prefeitura Municipal de Itaquaquecetuba é:<br><br>
			E-mail: ' . $email . '<br>
			Senha: ' . $senha . '<br><br>
			Para acessar a intranet entre em nosso site:<br>
			<a href="javascript:;">prefeitutamunicipalitaquaquecetuba.com.br/admin</a><br><br>
			* é recomendável que você altere a sua senha após o recebimento deste email';

        Mn_Util::sendMail($email, "Prefeitura Municipal de Itaquaquecetuba - Acesso ao Painel Administrativo", $body, null);
    }

    /**
     * Procede a autenticacao baseado e retorna o erro ou true
     * @param string $email
     * @param string $senha
     * @return string|boolean
     */
    public function autentica($email, $senha) {
        $usuarios = new Usuarios();
        $usuario = $usuarios->fetchRow("email='$email' AND excluido='nao'");

        if (!$usuario)
            return "E-mail ou senha inválidos!";
        if ($usuario['status'] == 'Bloqueado')
            return "O usuário está bloqueado!";
        if ($usuario['senha'] != $senha)
            return "A senha é inválida!";

        $perfis = new UsuariosPerfis();
        $perfil = $perfis->getPerfilById((int) $usuario['idperfil']);

        if ($perfil['master'] == "sim")
            unset($usuario['iddepartamento']);

        $usuario = $usuario->toArray();
        $usuario['master'] = $perfil['master'];
        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());

        if (((int) $usuario['idprefeitura'] <= 0) && ($perfil['master'] != "sim"))
            return "Prefeitura não encontrada!";

        $prefeitura = Prefeituras::getPrefeituraByIdHelper((int) $usuario['idprefeitura']);
        if ((!$prefeitura) && ($perfil['master'] != "sim"))
            return "O usuário não está vinculado a nenhuma Prefeitura!";

        //SP
        $idestado = 27;
        //Santa Isabel
        $idcidade = "Santa Isabel";

        $rows = new Prefeituras();
        $row = $rows->getPrefeituraByIdEstadoCidade($idestado, $idcidade, array('leftenderecos' => true));
        //var_dump($row); die('2');
        if (!$row) {
            $row = $rows->getPrefeituraByIdEstadoCidade($idestado, $idcidade, array('leftenderecos' => true, 'c1nome' => $idcidade));
        }
        $loginNameSpace->prefeituracidade = $row;

        $authprefeitura = $loginNameSpace->prefeituracidade;

        if (($authprefeitura['id'] != $usuario['idprefeitura']) && ($perfil['master'] != "sim"))
            return "O usuário não está vinculado a está Prefeitura!";
        $loginNameSpace->usuario = serialize($usuario);
        //
        return true;
    }

    /**
     * Procede a autenticacao baseado e retorna o erro ou true
     * @param string $email
     * @param string $senha
     * @return string|boolean
     */
    public function autenticargf($rgf, $senha) {
        $usuarios = new Usuarios();
        $usuario = $usuarios->fetchRow("rgf='$rgf' AND excluido='nao'");

        if (!$usuario)
            return "RGF ou senha inválidos!";
        if ($usuario['status'] == 'Bloqueado')
            return "O usuário está bloqueado!";
        if ($usuario['senha'] != $senha)
            return "A senha é inválida!";



        $funcionariosgeraisescolas = new Funcionariosgeraisescolas();
        $funcionariogeralescola = $funcionariosgeraisescolas->getFuncionariogeralescolaById($usuario['idfuncionario']);
        //var_dump($funcionariogeralescola);die();

        $perfis = new UsuariosPerfis();
        $perfil = $perfis->getPerfilById((int) $usuario['idperfil']);

        //if($perfil['master']=="sim") unset($usuario['iddepartamento']);

        $usuario = $usuario->toArray();
        $usuario['professor'] = ((isset($funcionariogeralescola['professor'])) && ($funcionariogeralescola['professor'] != "")) ? $funcionariogeralescola['professor'] : '';

        $usuario['idescola'] = ((isset($funcionariogeralescola['idescola'])) && ($funcionariogeralescola['idescola'] != "")) ? $funcionariogeralescola['idescola'] : '0';

        $usuario['idescolas'] = ((isset($funcionariogeralescola['idescolas'])) && ($funcionariogeralescola['idescolas'] != "")) ? $funcionariogeralescola['idescolas'] : '0';

        $usuario['idlocal'] = ((isset($funcionariogeralescola['idlocal'])) && ($funcionariogeralescola['idlocal'] != "")) ? $funcionariogeralescola['idlocal'] : '0';

        $usuario['pertenceescola'] = ((isset($funcionariogeralescola['pertenceescola'])) && ($funcionariogeralescola['pertenceescola'] != "")) ? $funcionariogeralescola['pertenceescola'] : 'Não';

        $usuario['master'] = $perfil['master'];
        $usuario['perfil_s'] = $perfil['perfil'];
        $usuario['individualgeral'] = $perfil['individualgeral'];
        $usuario['recebeprotocolo'] = $perfil['recebeprotocolo'];

        $loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());

        if (((int) $usuario['idprefeitura'] <= 0) && ($perfil['master'] != "sim"))
            return "Prefeitura não encontrada!";

        $prefeitura = Prefeituras::getPrefeituraByIdHelper((int) $usuario['idprefeitura']);

        //if((!$prefeitura) && ($perfil['master']!="sim")) return "O usuário não está vinculado a nenhuma Prefeitura!";
        //$authprefeitura = $loginNameSpace->prefeituracidade;
        //$prefeitura = Prefeituras::getPrefeituraByIdHelper((int)$usuario['idprefeitura']);
        //if((!$prefeitura) && ($perfil['master']!="sim")) return "O usuário não está vinculado a nenhuma Prefeitura!";
        //SP
        $idestado = 27;
        //Santa Isabel
        $idcidade = "Santa Isabel";

        $rows = new Prefeituras();
        $row = $rows->getPrefeituraByIdEstadoCidade($idestado, $idcidade, array('leftenderecos' => true));
        //var_dump($row); die('2');
        if (!$row) {
            $row = $rows->getPrefeituraByIdEstadoCidade($idestado, $idcidade, array('leftenderecos' => true, 'c1nome' => $idcidade));
        }
        $loginNameSpace->prefeituracidade = $row;

        $authprefeitura = $loginNameSpace->prefeituracidade;

        if (($authprefeitura['id'] != $usuario['idprefeitura']) && ($perfil['master'] != "sim"))
            return "O usuário não está vinculado a está Prefeitura!";

        $departamento = "";
        if ((int) $usuario['iddepartamento'] > 0) {
            $row_departamento = Departamentossecretarias::getDepartamentosecretariaByIdHelper($usuario['iddepartamento']);
            if (isset($row_departamento['id'])) {
                $departamento = $row_departamento['departamento'];
            }
        }
        $usuario['departamento'] = $departamento;

        $loginNameSpace->usuario = serialize($usuario);
        //

        return true;
    }

    /**
     * Retorna um array com os usuários do sistema
     * @return array
      public function getUsuarios() {
      $db = Zend_Registry::get('db');
      $strsql = "SELECT u1.*, p1.perfil
      FROM usuarios u1
      LEFT JOIN usuariosperfis p1 ON p1.id=u1.idperfil
      WHERE u1.excluido='nao'
      ORDER BY u1.nomerazao, sobrenomefantasia";

      return $db->fetchAll($strsql);
      }
     */
    public static function getUsuariosHelper($queries = array(), $page = 0, $maxpage = 0) {
        $usuarios = new Usuarios();
        return $usuarios->getUsuarios($queries, $page, $maxpage);
    }

    public function getUsuarios($queries = array(), $page = 0, $maxpage = 0) {
        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $idprefeitura = (isset($queries['idprefeitura'])) ? (int) $queries['idprefeitura'] : false;
        $master = (isset($queries['master'])) ? $queries['master'] : false;
        $status = (isset($queries['status'])) ? $queries['status'] : false;
        $email = (isset($queries['email'])) ? $queries['email'] : false;
        $secretaria = (isset($queries['secretaria'])) ? $queries['secretaria'] : false;
        $idsecretaria = (isset($queries['idsecretaria'])) ? $queries['idsecretaria'] : false;
        $idfuncionario = (isset($queries['idfuncionario'])) ? $queries['idfuncionario'] : false;
        $idperfil = (isset($queries['idperfil'])) ? $queries['idperfil'] : false;
        $iddepartamento = (isset($queries['iddepartamento'])) ? (int) $queries['iddepartamento'] : false;
        $chave = (isset($queries['chave'])) ? $queries['chave'] : false;
        $rgf = (isset($queries['rgf'])) ? $queries['rgf'] : false;
        $die = (isset($queries['die'])) ? $queries['die'] : false;
        $onlyid = (isset($queries['onlyid'])) ? $queries['onlyid'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();
        if ($id)
            array_push($where, " u1.id=$id ");
        if ($idprefeitura)
            array_push($where, " u1.idprefeitura=$idprefeitura ");
        if ($idperfil)
            array_push($where, " u1.idperfil=$idperfil ");
        if ($idsecretaria)
            array_push($where, " FIND_IN_SET($idsecretaria, u1.idsecretarias) ");
        if ($secretaria)
            array_push($where, " u1.secretaria='$secretaria' ");
        if ($idfuncionario)
            array_push($where, " u1.idfuncionario=$idfuncionario ");
        if ($iddepartamento)
            array_push($where, " u1.iddepartamento=$iddepartamento ");
        if ($status)
            array_push($where, " u1.status='$status' ");
        if ($rgf)
            array_push($where, " u1.rgf='$rgf' ");
        if ($master)
            array_push($where, " p1.master='$master' ");

        if ($email)
            array_push($where, " u1.email='$email' ");
        if ($chave)
            array_push($where, " (u1.nomerazao LIKE '%$chave%' OR u1.sobrenomefantasia LIKE '%$chave%' OR u1.email LIKE '%$chave%') ");
        if ($onlyid) {
            array_push($where, " (u1.id='$onlyid' OR u1.idparent=$onlyid) ");
        }

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nomerazao')
                    $sorting[0] = 'u1.nomerazao';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "u1.*, p1.perfil, p1.master, ds1.departamento";
        if ($total)
            $fields = "COUNT(u1.id) as total";


        $ordem = "ORDER BY u1.nomerazao, sobrenomefantasia";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields
                        FROM usuarios u1
                                LEFT JOIN usuariosperfis p1 ON p1.id=u1.idperfil
                                LEFT JOIN departamentossecretarias ds1 ON ds1.id = u1.iddepartamento
                        WHERE u1.excluido='nao'
                                $w
                        $ordem
                        $limit";

        //if($die) die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getUsuariosEscola($queries = array()) {
        $idescola = (isset($queries['idescola'])) ? (int) $queries['idescola'] : false;
        $idperfil = (isset($queries['idperfil'])) ? (int) $queries['idperfil'] : false;

        $where = array();
        if ($idescola)
            array_push($where, " FIND_IN_SET($idescola, f.idescolas)");
        if ($idperfil)
            array_push($where, " u.idperfil = '$idperfil'");

        $order = "ORDER BY f.id";
        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = " AND ($w)";

        $strsql = "SELECT u.* FROM usuarios as u
                    INNER JOIN funcionariosgeraisescolas as f on (f.id = u.idfuncionario)
                    WHERE f.excluido = 'nao' AND f.status='Ativo'
                $w
                $order";

        $db = Zend_Registry::get('db');

        return $db->fetchAll($strsql);
    }

    public function getUsuarioById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getUsuarios($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public function getUsuarioByEmail($email, $queries = array()) {

        $queries['email'] = $email;
        $rows = $this->getUsuarios($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getUsuarioByIdHelper($id, $queries = array()) {
        $rows = new Usuarios();
        return $rows->getUsuarioById($id, $queries);
    }

    public function getUsuarioByIdFuncionario($id, $queries = array()) {

        $queries['idfuncionario'] = $id;
        $rows = $this->getUsuarios($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getUsuarioByIdFuncionarioHelper($id, $queries = array()) {
        $rows = new Usuarios();
        return $rows->getUsuarioByIdFuncionario($id, $queries);
    }

    public static function getNomeDepartamentoUsuarioByIdHelper($id, $queries = array()) {
        $rows = new Usuarios();
        $usuario = $rows->getUsuarioById($id, $queries);
        $usuario['departamento'] = "Nenhum Departamento Encontrado";

        if ((int) $usuario['iddepartamento'] > 0) {
            $deparamento = Departamentosusuarios::getNomeDepartamentousuarioByIdHelper($usuario['iddepartamento']);
            $usuario['departamento'] = $deparamento;
        }

        return $usuario;
    }

    public static function getNomeByIdHelper($id, $queries = array()) {
        $rows = new Usuarios();
        $usuario = $rows->getUsuarioById($id, $queries);

        return $usuario['nomerazao'] . ' ' . $usuario['sobrenomefantasia'];
    }

    public static function getUsuarioByRgfHelper($rgf, $queries = array()) {
        if ((!isset($rgf)) || (!$rgf) || ($rgf == ""))
            return false;
        $_rows = new Usuarios();

        $queries['rgf'] = $rgf;
        $rows = $_rows->getUsuarios($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Usuarios
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idperfil = (array_key_exists('idperfil', $dados)) ? $dados['idperfil'] : $row->idperfil;
        $row->idparent = (array_key_exists('idparent', $dados)) ? $dados['idparent'] : $row->idparent;
        $row->idprefeitura = (array_key_exists('idprefeitura', $dados)) ? $dados['idprefeitura'] : $row->idprefeitura;
        $row->idfuncionario = (array_key_exists('idfuncionario', $dados)) ? $dados['idfuncionario'] : $row->idfuncionario;
        $row->secretaria = (array_key_exists('secretaria', $dados)) ? $dados['secretaria'] : $row->secretaria;
        $row->iddepartamento = (array_key_exists('iddepartamento', $dados)) ? $dados['iddepartamento'] : $row->iddepartamento;
        $row->idsecretarias = (array_key_exists('idsecretarias', $dados)) ? $dados['idsecretarias'] : $row->idsecretarias;
        $row->idsecretariaativa = (array_key_exists('idsecretariaativa', $dados)) ? $dados['idsecretariaativa'] : $row->idsecretariaativa;
        $row->nomerazao = (array_key_exists('nomerazao', $dados)) ? $dados['nomerazao'] : $row->nomerazao;
        $row->sobrenomefantasia = (array_key_exists('sobrenomefantasia', $dados)) ? $dados['sobrenomefantasia'] : $row->sobrenomefantasia;
        $row->rgf = (array_key_exists('rgf', $dados)) ? $dados['rgf'] : $row->rgf;
        $row->senha = (array_key_exists('senha', $dados)) ? $dados['senha'] : $row->senha;
        $row->email = (array_key_exists('email', $dados)) ? $dados['email'] : $row->email;
        $row->nome = (array_key_exists('nome', $dados)) ? $dados['nome'] : '';

        $row->codigovinculo = (array_key_exists("codigovinculo", $dados)) ? $dados["codigovinculo"] : $row->codigovinculo;
        $row->vinculoempregaticio = (array_key_exists("vinculoempregaticio", $dados)) ? $dados["vinculoempregaticio"] : $row->vinculoempregaticio;
        $row->codigodepartamento = (array_key_exists("codigodepartamento", $dados)) ? $dados["codigodepartamento"] : $row->codigodepartamento;
        $row->descdepartamento = (array_key_exists("descdepartamento", $dados)) ? $dados["descdepartamento"] : $row->descdepartamento;
        $row->desclocal = (array_key_exists("desclocal", $dados)) ? $dados["desclocal"] : $row->desclocal;
        $row->idimportacao = (array_key_exists("idimportacao", $dados)) ? $dados["idimportacao"] : $row->idimportacao;

        $row->status = (array_key_exists('status', $dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido', $dados)) ? $dados['excluido'] : $row->excluido;
        $row->logusuario = (array_key_exists('logusuario', $dados)) ? $dados['logusuario'] : $row->logusuario;
        $row->logdata = (array_key_exists('logdata', $dados)) ? $dados['logdata'] : $row->logdata;

        $funcionariosgeraisescolas = new Funcionariosgeraisescolas();

        if ((int) $row->idfuncionario <= 0) {

            $_dfun = Funcionariosgeraisescolas::getFuncionariogeraisescolaByRgfHelper($row->rgf);
            if (!isset($_dfun['id'])) {
                $funcionariosgeraisescolas = new Funcionariosgeraisescolas();
                $dadosfuncionarios = array();
                $dadosfuncionarios['id'] = (int) $row->idfuncionario;
                $dadosfuncionarios["nome"] = $row->nomerazao . ' ' . $row->sobrenomefantasia;
                $dadosfuncionarios["idsecretaria"] = $row->idsecretariaativa;
                $dadosfuncionarios['rgf'] = $row->rgf;
                $dadosfuncionarios["email"] = $row->email;
                $dadosfuncionarios["status"] = $row->status;
                $dadosfuncionarios['excluido'] = 'nao';
                $dadosfuncionarios['logusuario'] = $row->logusuario;
                $dadosfuncionarios['logdata'] = $row->logdata;
                $dadosfuncionarios['idperfil'] = $row->idperfil;
                $dadosfuncionarios['idsperfis'] = $row->idperfil;
                $_rowdf = $funcionariosgeraisescolas->save($dadosfuncionarios);
                $row->idfuncionario = $_rowdf['id'];
            } else {
                $row->idfuncionario = $_dfun['id'];
            }
        } else {
            $idfuncionario = $funcionariosgeraisescolas->getFuncionariogeralescolaById($row->idfuncionario);
            $idfuncionario['idperfil'] = $row->idperfil;
            $idfuncionario['idsperfis'] = $row->idperfil;
            $row->idfuncionario = $idfuncionario['id'];
            $funcionariosgeraisescolas->save($idfuncionario);
        }

        $row->save();

        //echo '<pre>'; print_r($row);echo '<hr><br>';print_r($funcionariosgeraisescolas); echo '</pre>'; die();

        return $row;
    }

}
