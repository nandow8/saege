<?php

/**
 * Define o modelo Documentacaosolicitacoesremocao
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Documentacaosolicitacoesremocao extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "documentacaosolicitacoesremocao";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDocumentacaosolicitacoesremocaoHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$documentacaosolicitacoesremocao = new Documentacaosolicitacoesremocao();
		return $documentacaosolicitacoesremocao->getDocumentacaosolicitacoesremocao($queries, $page, $maxpage);
	}
	
	public function getDocumentacaosolicitacoesremocao($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		
		$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " d1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " d1.data <= '$data_f' ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " d1.idescola = $idescola ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " d1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		
		$ordem = "ORDER BY d1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM documentacaosolicitacoesremocao d1
					
					WHERE d1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDocumentacaosolicitacaoremocaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDocumentacaosolicitacoesremocao($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDocumentacaosolicitacaoremocaoByIdHelper($id, $queries = array()) {
		$rows = new Documentacaosolicitacoesremocao();
		return $rows->getDocumentacaosolicitacaoremocaoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Documentacaosolicitacoesremocao
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
	 	$row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
		$row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		$row->horario = (array_key_exists("horario",$dados)) ? $dados["horario"] : $row->horario;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->idprofessor = (array_key_exists("idprofessor",$dados)) ? $dados["idprofessor"] : $row->idprofessor;
		$row->idvinculo = (array_key_exists("idvinculo",$dados)) ? $dados["idvinculo"] : $row->idvinculo;
		$row->pontuacao = (array_key_exists("pontuacao",$dados)) ? $dados["pontuacao"] : $row->pontuacao;
		$row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
	 	$row->idscursos = (array_key_exists("idscursos",$dados)) ? $dados["idscursos"] : $row->idscursos;
		$row->posicao = (array_key_exists("posicao",$dados)) ? $dados["posicao"] : $row->posicao;

		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		
		$row->save();
		
		return $row;
	}
	
}