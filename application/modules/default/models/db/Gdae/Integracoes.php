<?php

class Gdae_Integracoes extends Zend_Db_Table_Abstract {
	public function Enviarescolas($idescola){
		/*2.1*/
		$escolas = new Escolas();
		$escola = $escolas->getEscolaById((int)$idescola);
		
		$endereco = array();
		if((int)$escola['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($escola['idendereco']);
		}
		
		
		$dados = array();
		$dados['CodigoEscola'] = $escola['id'];
		$dados['NomeCompleto'] = $escola['escola'];
		$dados['NomeAbreviado'] = $escola['nomeabreviado'];
		$dados['CepLogradouro'] = (isset($endereco['cep'])) ? $endereco['cep'] : '';
		$dados['BairroLogradouro'] = (isset($endereco['bairro'])) ? $endereco['bairro'] : '';
		//$dados['CodDistrito'] = $escola['id'];
		$dados['NomeLogradouro'] = (isset($endereco['endereco'])) ? $endereco['endereco'] : '';
		$dados['NumeroLogradouro'] = (isset($endereco['numero'])) ? $endereco['numero'] : '';
		//$dados['CodMunicipioDNE'] = $escola['id'];
		$dados['CodSituacao'] = $escola['codsituacao'];
		$dados['CodRedeEnsino'] = $escola['codredeensino'];
		$dados['CodIdentificador'] = $escola['codidentificador'];
		
		var_dump($dados); die('2.1');
		$dados = (object)$dados;		
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'Escola', Gdae_Importaescolas::$REST);
	}
	
	
	public function Enviardepartamentos($iddepartamento){
		/*2.2*/
		$departamentos = new Departamentos();
		$departamento = $departamentos->getDepartamentoById((int)$iddepartamento);
		if(!$departamento) return false;
		
		$escolas = new Escolas();
		$escola = $escolas->getEscolaById($departamento['id']);
		
		$endereco = array();
		if((int)$escola['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($escola['idendereco']);
		}
		
		
		$dados = array();
		$dados['CodigoDiretoria'] = $departamento['id'];
		$dados['Nome'] = $departamento['nome'];
		$dados['CodSituacao'] = ((isset($departamento['status'])) && ($departamento['status']=="Ativo")) ? '1' : '0';
		$dados['CodMunicipioDNE'] = $departamento['codigomunicipiodne'];
		//$dados['NomeDistrito'] = $escola['id'];
		$dados['CepLogradouro'] = (isset($endereco['cep'])) ? $endereco['cep'] : '';
		$dados['BairroLogradouro'] = (isset($endereco['bairro'])) ? $endereco['bairro'] : '';
		$dados['NomeLogradouro'] = (isset($endereco['endereco'])) ? $endereco['endereco'] : '';
		$dados['NumeroLogradouro'] = (isset($endereco['numero'])) ? $endereco['numero'] : '';
		var_dump($dados); die('2.2');
		
		$dados = (object)$dados;		
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'Diretoria', Gdae_Importaescolas::$REST);
	}
	
	
	public function InclusaoColetaClasse($idvinculo, $operacao){
		/*2.3 - 2.4*/
		$vinculos = new Escolasvinculos();
		$vinculo = $vinculos->getVinculoById($idvinculo);
		if(!$vinculo) return false;
		if(!$operacao) return false;

		$gdae = new Gdae_Identificadoresescolas();
		$identificador = $gdae->getIdentificadorById($vinculo['idtipoensino']);
		//var_dump($operacao); die();
		
		$inicioaulas = explode(' ', $vinculo['inicioaulas']);
		$datainicio = $inicioaulas[0];
		$inDiaInicioAula = explode('-', $datainicio);
		
		$terminoaulas = explode(' ', $vinculo['terminoaulas']);
		$datatermino = $terminoaulas[0];
		$inDiaTerminoAula = explode('-', $datatermino);	

		$periodo = explode('as', $vinculo['periodo']);
		$inicio = str_replace(":", "", $periodo[0]);
		$termino = str_replace(":", "", $periodo[1]);
		
		
		$dados = array();
		$dados['inCodEscola'] = $vinculo['idescola'];
		//$dados['inCodUnidade'] = $vinculo['id'];
		$dados['inCapacidadeFisica'] = $vinculo['quantidade'];
		$dados['inDiaInicioAula'] = $inDiaInicioAula[2];
		$dados['inDiaTerminoAula'] = $inDiaTerminoAula[2];
		$dados['inHoraFinal'] = trim($inicio);
		$dados['inHoraInicial'] = trim($termino);
		$dados['inMesInicioAula'] = $inDiaInicioAula[1];
		$dados['inMesTerminoAula'] = $inDiaTerminoAula[1];
		$dados['inNumeroSala'] = $vinculo['numerosala'];
		//Série / Ano / Termo do Tipo de Ensino
		$dados['inSerieAno'] = $vinculo['serie'] . '/' . $vinculo['serie'] . '/' . $identificador['descricoes'];
		//TIPO
		$dados['inTipoEnsino'] = $vinculo['idtipoensino'];
		$dados['inTurma'] = $vinculo['turma'];
		$dados['inTurno'] = $vinculo['turno'];
		$dados['inAno'] = $vinculo['ano'];
		/*$dados['inProgMaisEducacao'] = $vinculo['id'];
		$dados['inAEE'] = $vinculo['id'];
		$dados['inATC'] = $vinculo['id'];*/
		//var_dump($vinculo); die();
		var_dump($dados); die('2.3 - 2.4');
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, $operacao, Gdae_Importaescolas::$REST);
	}	
	
	public function Enviaralunos($idaluno, $operacao){
		/*2.5 - 2.6*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		//var_dump($aluno); die();
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
		}
		
		$ano = 0;
		if((int)$aluno['idserie'] > 0):
			$series = new Escolasseries();
			$serie = $series->getSerieById($aluno['idserie']);
			if((isset($serie)) && ($serie)):
				$ano = $serie['ano']; 
			endif;
		endif;
		
		//0 ou 1 (0 - URBANO e 1- RURAL)
		$tipologradouro = 0;
		if($endereco['tipologradouro']==Enderecos::$_TIPO_LOGRADOURO_RURAL):
			$tipologradouro = 1;
		endif;
		//var_dump($endereco['tipologradouro']); die();
		$dados = array();
		$dados['inRA'] = $aluno['ra'];
		$digito = substr($aluno['ra'], -2);
		$dados['inDigitoRA'] = $digito;		
		$dados['inUFRA'] = $aluno['raunidadefederativa'];
		$dados['inAno'] = $ano;
		
		if($operacao=="AlterarEnderecoFichaAluno"){		
			/*
			$dados['inDDDCel'] = $aluno['nome'];
			$dados['inFoneCel'] = $aluno['telefoneresidencial'];
			
			$dados['inSMS'] = $aluno['nome'];				
			$dados['inIrmaoRA'] = $aluno['nome'];
			$dados['inIrmaoDigitoRA'] = $aluno['nome'];
			$dados['inIrmaoUFRA'] = $aluno['nome'];
			*/
		}
		
		$dados['EndResidencial.inCep'] = $endereco['cep'];
		$dados['EndResidencial.inCidade'] = $endereco['cidade'];
		$dados['EndResidencial.inUF'] = $endereco['uf'];
		$dados['EndResidencial.inLogradouro'] = $endereco['nomelogradouro'];
		$dados['EndResidencial.inNumero'] = $endereco['numero'];
		$dados['EndResidencial.inBairro'] = $endereco['bairro'];
		$dados['EndResidencial.inTipoLogradouro'] = $tipologradouro;		
		var_dump($dados); die('2.5 - 2.6');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, $operacao, Gdae_Importaescolas::$REST);
	}	
	
	public function Enviarmatriculainfo($id){	
		/*2.7*/
		$atribuicoes = new Escolassalasatribuicoes();
		$atribuicao = $atribuicoes->getSalaatribuicaoById($id);

		$salasalunos = new Escolassalasalunos();
		$salaluno = $salasalunos->getSalaalunoById($atribuicao['idescolasalasaluno']);
		
		$escolasvinculos = new Escolasvinculos();
		$escolavinculo = $escolasvinculos->getVinculoById($atribuicao['idvinculo']); 
		
		//SALA
		$idsala = $escolavinculo['idsala'];
		$salas = new Escolassalas();
		$sala = $salas->getSalaById($idsala); 
		
		//SERIE
		$idserie = $escolavinculo['idserie'];
		$series = new Escolasseries();
		$serie = $series->getSerieById($idserie); 		
		
		//TURMA
		$idturma = $escolavinculo['idturma'];		
		
		//ALUNO
		$idaluno = $atribuicao['idaluno'];
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById($idaluno);

		//TIPO ENSINO
		$idtipoensino = $escolavinculo['idtipoensino'];
		$tiposensinos = new Escolastiposensinos();
		$tipoensino = $tiposensinos->getTipoensinoById($idtipoensino);
		
		$datamatricula = $aluno['dataefetivacao'];
		$datamatricula = explode(' ', $datamatricula);
		$datamatricula_explode = $datamatricula[0];
		$datamatricula_final = explode('-', $datamatricula_explode);		
		$dia = $datamatricula_final[2];
		$mes = $datamatricula_final[1];

		$dados = array();
		$dados['inNumClasse'] = $sala['numerosala'];
		$dados['inSerieAno'] = $serie['serie'] . '/' . (int)$serie['ano'];
		$dados['inTipoEnsino'] = $tipoensino['codigo'];
		$dados['inRA'] = $aluno['ra'];
		$dados['inUFRA'] = $aluno['raunidadefederativa'];
		$dados['inDataMatricula'] = $dia;
		$dados['inMesMatricula'] = $mes;
		$dados['inNumAluno'] = "NUMERO CHAMADA";
		var_dump($dados); die('2.7');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'RealizarMatriculaInfoComRA', Gdae_Importaescolas::$REST);
	}

	public function Enviarmatriculainfosemra($id){	
		/*2.8*/
		$atribuicoes = new Escolassalasatribuicoes();
		$atribuicao = $atribuicoes->getSalaatribuicaoById($id);

		$salasalunos = new Escolassalasalunos();
		$salaluno = $salasalunos->getSalaalunoById($atribuicao['idescolasalasaluno']);
		
		$escolasvinculos = new Escolasvinculos();
		$escolavinculo = $escolasvinculos->getVinculoById($atribuicao['idvinculo']); 
		
		//SALA
		$idsala = $escolavinculo['idsala'];
		$salas = new Escolassalas();
		$sala = $salas->getSalaById($idsala); 
		
		//SERIE
		$idserie = $escolavinculo['idserie'];
		$series = new Escolasseries();
		$serie = $series->getSerieById($idserie); 		
		
		//TURMA
		$idturma = $escolavinculo['idturma'];		
		
		//ALUNO
		$idaluno = $atribuicao['idaluno'];
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById($idaluno);
		
		//TIPO ENSINO
		$idtipoensino = $escolavinculo['idtipoensino'];
		$tiposensinos = new Escolastiposensinos();
		$tipoensino = $tiposensinos->getTipoensinoById($idtipoensino);
		
		$datamatricula = $aluno['dataefetivacao'];
		$datamatricula = explode(' ', $datamatricula);
		$datamatricula_explode = $datamatricula[0];
		$datamatricula_final = explode('-', $datamatricula_explode);		
		$dia = $datamatricula_final[2];
		$mes = $datamatricula_final[1];
		
		$datanascimento = $aluno['nascimento'];
		$datanascimento = explode(' ', $datanascimento);
		$datanasicmento_explode = $datanascimento[0];
		$datnascimento_final = explode('-', $datanasicmento_explode);		
		$dia_n = $datnascimento_final[2];
		$mes_n = $datnascimento_final[1];
		$ano_n = $datnascimento_final[1];			

		$dados = array();
		$dados['inAno'] = '0000';
		$dados['inNumAluno'] = "NUMERO CHAMADA";
		$dados['inDataMatricula'] = $dia;
		$dados['inMesMatricula'] = $mes;
		$dados['inNomeAluno'] = $aluno['nomerazao'] . ' ' . $aluno['sobrenomefantasia'];
		
		$dados['Sexo'] = $aluno['sexo'];
		$dados['CorRaca'] = $aluno['corraca'];
		$dados['DiaNascimento'] = $dia_n;
		$dados['MesNascimento'] = $mes_n;
		$dados['AnoNascimento'] = $ano_n;
		$dados['Gemeo'] = "N";
		//$dados['IrmaoRA'] = "NUMERO CHAMADA";
		$dados['NomeMae'] = $aluno['nomemae'];
		//Nacionalidade do aluno (1 – Brasileira, 2 – Estrangeiro e 3 – Brasileiro nascido no exterior)
		$dados['inNacionalidade'] = $aluno['nacionalidade'];
		//NACIONALIDADE = 2
		//$dados['inDiaEntBrasil'] = "NUMERO CHAMADA";
		//$dados['inMesEntBrasil'] = "NUMERO CHAMADA";
		//$dados['inAnoEntBrasil'] = "NUMERO CHAMADA";
		
		//NACIONALIDADE = 1
		//$dados['inMunicipioNasc'] = "NUMERO CHAMADA";
		//$dados['inUFNasc'] = "NUMERO CHAMADA";
		
		$dados['inTipoEnsino'] = $tipoensino['codigo'];
		$dados['inSerieAno'] = $serie['serie'] . '/' . (int)$serie['ano'];
		//Aluno possui mobilidade reduzida? (S ou N)
		$dados['MobilidadeReduzida'] = ($aluno['mobilidadereduzida']=="Sim") ? 'S' : 'N';		
		var_dump($dados); die('2.8');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'RealizarMatriculaInfoSemRA', Gdae_Importaescolas::$REST);
	}

	public function Enviaralterardadospessoaisfichaaluno($idaluno){
		/*2.9*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		//var_dump($aluno); die();
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
		}
		
		$ano = 0;
		if((int)$aluno['idserie'] > 0):
			$series = new Escolasseries();
			$serie = $series->getSerieById($aluno['idserie']);
			if((isset($serie)) && ($serie)):
				$ano = $serie['ano']; 
			endif;
		endif;
		
		//0 ou 1 (0 - URBANO e 1- RURAL)
		$tipologradouro = 0;
		if($endereco['tipologradouro']==Enderecos::$_TIPO_LOGRADOURO_RURAL):
			$tipologradouro = 1;
		endif;
		//var_dump($endereco['tipologradouro']); die();
		$dados = array();
		$dados['inRA'] = $aluno['ra'];
		$dados['inUFRA'] = $aluno['raunidadefederativa'];
		//$dados['inAno'] = $ano;
		
		$dados['inNomeAluno'] = $aluno['nomerazao'] . ' ' . $aluno['sobrenomefantasia'];
		
		$sexo = "";
		if($aluno['sexo']=="Masculino"):
			$sexo = "M";
		elseif ($aluno['sexo']=="Feminino"):
			$sexo = "F";
		endif;
		$dados['inSexo'] = $sexo;
		$dados['inCorRaca'] = $aluno['corraca'];
		
		$datanascimento = $aluno['nascimento'];
		$datanascimento = explode(' ', $datanascimento);
		$datanasicmento_explode = $datanascimento[0];
		$datnascimento_final = explode('-', $datanasicmento_explode);		
		$dia_n = $datnascimento_final[2];
		$mes_n = $datnascimento_final[1];
		$ano_n = $datnascimento_final[0];			
		$dados['inDiaNascimento'] = $dia_n;
		$dados['inMesNascimento'] = $mes_n;
		$dados['inAnoNascimento'] = $ano_n;
		
		$dados['inGemeo'] = "N";
		
		/*
		$dados['inIrmaoRA'] = $aluno['ra'];
		$dados['inIrmaoDigitoRA'] = $aluno['ra'];
		$dados['inIrmaoUFRA'] = $aluno['ra'];
		*/
		
		$dados['inNomeMae'] = $aluno['nomemae'];
		
		if($aluno['mobilidadereduzida']=="Sim"):
			$dados['mobilidadereduzida'] = "S";
			if($aluno['mobilidadetipo']==Escolasalunos::$_MOBILIDADE_TIPO_PERMANENTE):
				$dados['inPermanente'] = "X";
				$dados['inTemporaria'] = "";
			elseif($aluno['mobilidadetipo']==Escolasalunos::$_MOBILIDADE_TIPO_TEMPORARIO):
				$dados['inPermanente'] = "";
				$dados['inTemporaria'] = "X";
			else:
				$dados['inPermanente'] = "";
				$dados['inTemporaria'] = "";
			endif;
		else:
			$dados['mobilidadereduzida'] = "N";
			$dados['inPermanente'] = "";
			$dados['inTemporaria'] = "";
		endif; 
		var_dump($dados); die('2.9');

		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'AlterarDadosPessoaisFichaAluno', Gdae_Importaescolas::$REST);
	}

	public function Enviaralterardocumentosfichaaluno($idaluno){
		/*2.10*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		//var_dump($aluno); die();
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
		}
		
		$ano = 0;
		if((int)$aluno['idserie'] > 0):
			$series = new Escolasseries();
			$serie = $series->getSerieById($aluno['idserie']);
			if((isset($serie)) && ($serie)):
				$ano = $serie['ano']; 
			endif;
		endif;
		
		//0 ou 1 (0 - URBANO e 1- RURAL)
		$tipologradouro = 0;
		if($endereco['tipologradouro']==Enderecos::$_TIPO_LOGRADOURO_RURAL):
			$tipologradouro = 1;
		endif;
		//var_dump($endereco['tipologradouro']); die();
		$dados = array();
		$dados['inRA'] = $aluno['ra'];
		$dados['inUFRA'] = $aluno['raunidadefederativa'];
		//$dados['inAno'] = $ano;
		
		$dados['inNacionalidade'] = $aluno['nacionalidadetipo'];
		
		if($aluno['nacionalidadetipo']=="2"):
			/*
			$dados['inPaisOrigem'] = $aluno['nomerazao'];
			$dados['inDiaEntBrasil'] = $aluno['nomerazao'];
			$dados['inMesEntBrasil'] = $aluno['nomerazao'];
			$dados['inAnoEntBrasil'] = $aluno['nomerazao'];
			*/
			$dados['inMunicipioNasc'] = "FALTA";
			$dados['inUFNasc'] = "FALTA";
		elseif ($aluno['inNacionalidade=']=="1"):
			/*
			$dados['inPaisOrigem'] = $aluno['nomerazao'];
			$dados['inDiaEntBrasil'] = $aluno['nomerazao'];
			$dados['inMesEntBrasil'] = $aluno['nomerazao'];
			$dados['inAnoEntBrasil'] = $aluno['nomerazao'];
			*/
			$dados['inMunicipioNasc'] = "FALTA";
			$dados['inUFNasc'] = "FALTA";
		endif;
		var_dump($dados); die('2.10');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'AlterarDocumentosFichaAluno', Gdae_Importaescolas::$REST);
	}

	public function EnviartrocaralunoclasseRA($idvinculo){
		/*2.11*/
		$vinculos = new Escolassalasatribuicoes();
		$vinculo = $vinculos->getSalaatribuicaoById($idvinculo);
		
		if(!$vinculo) return false;
		
		$gdae = new Gdae_Identificadoresescolas();
		$identificador = $gdae->getIdentificadorById($vinculo['idtipoensino']);
		
		$inicioaulas = explode(' ', $vinculo['inicioaulas']);
		$datainicio = $inicioaulas[0];
		$inDiaInicioAula = explode('-', $datainicio);
		
		$terminoaulas = explode(' ', $vinculo['terminoaulas']);
		$datatermino = $terminoaulas[0];
		$inDiaTerminoAula = explode('-', $datatermino);	

		$periodo = explode('as', $vinculo['periodo']);
		$inicio = str_replace(":", "", $periodo[0]);
		$termino = str_replace(":", "", $periodo[1]);		
		
		$idaluno = "1";
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		//var_dump($aluno); die();
		
		$dados = array();
		$dados['inCodEscola'] = $vinculo['idescola'];
		$dados['inRA'] = $aluno['ra'];				
		$dados['inUF'] = "FALTA";		
		
		//Tipo de ensino da classe destino (item 4.2)
		$dados['inTipoEnsino'] = $identificador['codigo'];
		$dados['inSerieAno'] = $vinculo['serie'];
		$dados['inNumAluno'] = "FALTA";
		$dados['inTurma'] = $vinculo['turma'];
		
		//Turno de funcionamento da classe destino (item 4.3)
		$dados['inTurno'] = "FALTA";
		$dados['inAno'] = "FALTA";
		$dados['inDiaTroca'] = "FALTA";
		$dados['inMesTroca'] = "FALTA";
		$dados['inAnoTroca'] = "FALTA";
		var_dump($dados); die('2.11');		

		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'TrocarAlunoClasseRA', Gdae_Importaescolas::$REST);
	}

	public function Enviarbaixarmatriculatransferencia($idaluno){
		/*2.12*/
		$idaluno = "1";
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		
		$dados = array();
		$dados['inCodEscola'] = $aluno['ra'];
		$dados['inRA'] = $aluno['ra'];				
		$dados['inUF'] = $aluno['nacionalidadetipo'];		
		
		//Tipo de ensino da classe destino (item 4.2)
		$dados['inTipoEnsino'] = $aluno['nacionalidadetipo'];
		$dados['inSerieAno'] = $aluno['nacionalidadetipo'];
		$dados['inDiaTransferencia'] = $aluno['nacionalidadetipo'];
		$dados['inMesTransferencia'] = $aluno['nacionalidadetipo'];
		//Motivo da baixa (item 4.5)
		$dados['inMotivo'] = $aluno['nacionalidadetipo'];
		var_dump($dados); die('2.12');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'BaixarMatriculaTransferencia', Gdae_Importaescolas::$REST);
	}

	public function Enviarestornarbaixamatriculatransferencia($atual, $anterior, $idescola){
		/*2.13*/
		$anterior = $anterior->toArray();
				
		$alunos = new Escolasalunos();
		$aluno = $atual->toArray();
		if(!$aluno) return false;
		
		$atribuicoes = Escolassalasatribuicoes::getSalaatribuicaoByIdAlunoHelper($idaluno);
		if(!$atribuicoes) return false;				

		if((isset($aluno['idescola'])) && (isset($idescola)) && ($aluno['idescola']!=$idescola)){
			$dados_transferencias = array();
			$dados_transferencias['idaluno'] = $aluno['id'];
			$dados_transferencias['idescolaanterior'] = $idescola;
			$dados_transferencias['idescolanova'] = $aluno['idescola'];
			$dados_transferencias['datatransferencia'] = date('Y-m-d G:i:s');
			$dados_transferencias['motivo'] = $aluno['motivotransferencia'];
			$transferencias = new Escolastransferenciasalunos();
			$transferencias->save($dados_transferencias);
			
			$dados = array();
			$dados['inCodEscola'] = $aluno['idescola'];
			$dados['inRA'] = $aluno['ra'];				
			
			//Tipo de ensino da classe destino (item 4.2)
			$dados['inTipoEnsino'] = $atribuicoes['idtipoensino'];
			$dados['inSerieAno'] = "FALTA";		
			var_dump($dados); die('2.13');
			
			$dados = (object)$dados;
			$rows = new Gdae_Importaescolas();
			$rows->envia_api($dados, 'EstornarBaixaMatrTransf', Gdae_Importaescolas::$REST);		
		}
		
		return;
		//var_dump($aluno); die('atual');
		//var_dump($anterior); die('anterior');
	}	
	
	public function Enviarbaixamatriculafalecimento($idaluno){
		/*2.14*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		
		$datafalecimento = $aluno['datafalecimento'];
		$datafalecimento = explode(' ', $datafalecimento);
		$datafalecimento_explode = $datafalecimento[0];
		$datafalecimento_final = explode('-', $datafalecimento_explode);		
		$dia_n = $datafalecimento_final[2];
		$mes_n = $datafalecimento_final[1];
		$ano_n = $datafalecimento_final[0];		
		
		$dados = array();
		$dados['inRA'] = $aluno['ra'];
		$dados['inDiaFalecimento'] = $dia_n;
		$dados['inMesFalecimento'] = $mes_n;
		$dados['inAnoFalecimento'] = $ano_n;
		var_dump($dados); die('2.14');
		$aluno['gdaefalecimento'] = "Sim";
		$aluno['logusuario'] = Usuarios::getUsuario('id');
		$aluno['logdata'] = date('Y-m-d G:i:s');
		$alunos->save($aluno);
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'BaixarMatrFalecimentoRA', Gdae_Importaescolas::$REST);
	}

	public function Enviarregistrarabandono($idaluno){
		/*2.15*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		
		if(!$aluno) return false;
		
		$atribuicoes = Escolassalasatribuicoes::getSalaatribuicaoByIdAlunoHelper($idaluno);
		if(!$atribuicoes) return false;		
		
		$dataabandono = $aluno['dataabandono'];
		$dataabandono = explode(' ', $dataabandono);
		$dataabandono_explode = $dataabandono[0];
		$dataabandono_final = explode('-', $dataabandono_explode);		
		$dia_n = $dataabandono_final[2];
		$mes_n = $dataabandono_final[1];
		$ano_n = $dataabandono_final[0];	

		$ano = 0;
		if((int)$aluno['idserie'] > 0):
			$series = new Escolasseries();
			$serie = $series->getSerieById($aluno['idserie']);
			if((isset($serie)) && ($serie)):
				$ano = $serie['ano']; 
			endif;
		endif;		
		
		$dados = array();
		$dados['inCodEscola'] = $aluno['idescola'];
		$dados['inRA'] = $aluno['ra'];				
		$dados['inUFRA'] = $aluno['raunidadefederativa'];
		
		//Tipo de ensino da classe destino (item 4.2)
		$dados['inTipoEnsino'] = $atribuicoes['idtipoensino'];
		$dados['inSerieAno'] = 'FALTA';
		$dados['inDiaAbandono'] = $dia_n;
		$dados['inMesAbandono'] = $mes_n;
		var_dump($dados); die('2.15');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'RegistrarAbandono', Gdae_Importaescolas::$REST);
	}	
	 
	public function Enviarestornarregistroabandono($idaluno){
		/*2.16*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		
		if(!$aluno) return false;
		
		$atribuicoes = Escolassalasatribuicoes::getSalaatribuicaoByIdAlunoHelper($idaluno);
		if(!$atribuicoes) return false;	
		
		$dataabandono = $aluno['dataestornoabandono'];
		$dataabandono = explode(' ', $dataabandono);
		$dataabandono_explode = $dataabandono[0];
		$dataabandono_final = explode('-', $dataabandono_explode);		
		$dia_n = $dataabandono_final[2];
		$mes_n = $dataabandono_final[1];
		$ano_n = $dataabandono_final[0];	
		
		
		$dados = array();
		$dados['inCodEscola'] = $aluno['id'];
		$dados['inRA'] = $aluno['ra'];	
		
		//Tipo de ensino da classe destino (item 4.2)
		$dados['inTipoEnsino'] = $atribuicoes['idtipoensino'];
		$dados['inSerie'] = 'FALTA';
		$dados['inDiaRetorno'] = $dia_n;
		$dados['inMesRetorno'] = $mes_n;
		var_dump($dados); die('2.16');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'EstornarRegistroAbandono', Gdae_Importaescolas::$REST);
	}

	public function Enviarremanejamentomatriculara($idaluno){
		/*2.17*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;
		
		$uf = "";
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idenderecoindicativo']);
			if(isset($endereco['uf'])){
				$uf = $endereco['uf'];
			}
		}		
		
		$atribuicoes = Escolassalasatribuicoes::getSalaatribuicaoByIdAlunoHelper($idaluno);
		if(!$atribuicoes) return false;	
				
		$dataestornoabandono = $aluno['remanejamentodata'];
		$dataestornoabandono = explode(' ', $dataestornoabandono);
		$dataestornoabandono_explode = $dataestornoabandono[0];
		$dataestornoabandono_final = explode('-', $dataestornoabandono_explode);		
		$dia_n = $dataestornoabandono_final[2];
		$mes_n = $dataestornoabandono_final[1];
		$ano_n = $dataestornoabandono_final[0];	
		
		
		$dados = array();
		$dados['inCodEscola'] = $aluno['id'];
		$dados['inNumClasse'] = "FALTA";
		$dados['inRA'] = $aluno['ra'];	
		$dados['inUF'] = $uf;
		//Tipo de ensino da classe destino (item 4.2)
		$dados['inTipoEnsino'] = $atribuicoes['idtipoensino'];
		$dados['inSerieAno'] = "FALTA";
		$dados['inAlunoAvaliado'] = $aluno['remanejamentoavaliado'];
		
		$dados['inDiaRemanejamento'] = $dia_n;
		$dados['inMesRemanejamento'] = $mes_n;
		$dados['inNumAluno'] = "FALTA";
		var_dump($dados); die('2.17');
		
		$rows = new Gdae_Importaescolas();		
		$dados = (object)$dados;
		$rows->envia_api($dados, 'RemanejarMatPorRA', Gdae_Importaescolas::$REST);
	}

	public function Enviarreclassificarmatriculara($idaluno){
		/*2.18*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;

		$atribuicao = Escolassalasatribuicoes::getSalaatribuicaoByIdAlunoHelper($idaluno);
		if(!$atribuicao) return false;	

		$vinculos = new Escolasvinculos();
		$vinculo = $vinculos->getVinculoById($atribuicao['idvinculo']);		
		if(!$vinculo) return false;
		
		$dataestornoabandono = $aluno['dataestornoabandono'];
		$dataestornoabandono = explode(' ', $dataestornoabandono);
		$dataestornoabandono_explode = $dataestornoabandono[0];
		$dataestornoabandono_final = explode('-', $dataestornoabandono_explode);		
		$dia_n = $dataestornoabandono_final[2];
		$mes_n = $dataestornoabandono_final[1];
		$ano_n = $dataestornoabandono_final[0];	
		
		$dados = array();
		$dados['inRA'] = $aluno['ra'];	
		//Tipo de ensino da classe destino (item 4.2)
		$dados['inTipoEnsino'] = $atribuicao['idtipoensino'];
		$dados['inDiaReclassificacao'] = $dia_n;
		$dados['inMesReclassificacao'] = $mes_n;
		//inNumAluno
		$dados['inNumAluno'] = "FALTA";
		$dados['inTurma'] = $vinculo['turma'];
		//Turno de funcionamento da classe destino (item 4.3)
		$dados['inTurno'] = $vinculo['turno'];
		var_dump($dados); die('2.18');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'ReclassificarMatriculas', Gdae_Importaescolas::$REST);
	}	
	
	public function Enviarregistrarnaocomparecimentora($idaluno){
		/*2.19*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;

		$atribuicao = Escolassalasatribuicoes::getSalaatribuicaoByIdAlunoHelper($idaluno);
		if(!$atribuicao) return false;	
		
		$dados = array();
		$dados['inCodEscola'] = $aluno['idescola'];
		$dados['inRA'] = $aluno['ra'];	
		//Tipo de ensino da classe destino (item 4.2)
		$dados['inTipoEnsino'] = $atribuicao['idtipoensino'];
		var_dump($dados); die('2.19');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'RegistrarNaoComparecimento', Gdae_Importaescolas::$REST);
	}

	public function Enviarexclusaomatriculara($idaluno){
		/*2.20*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;

		$atribuicoes = Escolassalasatribuicoes::getSalaatribuicaoByIdAlunoHelper($idaluno);
		if(!$atribuicoes) return false;
		
		$sala = Escolassalas::getSalaByIdHelper($atribuicoes['idsala']);
		if((!$sala) || (!$sala['numerosala'])) return false;
		
		$uf = "";
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idenderecoindicativo']);
			if(isset($endereco['uf'])){
				$uf = $endereco['uf'];
			}
		}		
		
		$dados = array();
		$dados['inRA'] = $aluno['ra'];
		$dados['inUF'] = $uf;	
		//Tipo de ensino da classe destino (item 4.2)
		$dados['inTipoEnsino'] = $atribuicoes['idtipoensino'];
		
		$dados['inAno'] = date('Y');
		$dados['inNumClasse'] = $sala['numerosala'];		
		var_dump($dados); die('2.20');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'ExcluirMatricula', Gdae_Importaescolas::$REST);
	}	
	
	public function Enviarrendimentoescolarclasse($idaluno){
		/*2.21*/
		$idaluno = "1";
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		
		$dados = array();
		$dados['inNumClasse'] = $aluno['nacionalidadetipo'];
		$dados['inSemestre'] = $aluno['nacionalidadetipo'];
		$dados['inAlunos'] = $aluno['nacionalidadetipo'];
		$dados['Aluno.inRA'] = $aluno['ra'];	
		$dados['Aluno.inUF'] = $aluno['ra'];
		
		//Rendimento do aluno (item 4.6)
		$dados['Aluno.inStatus'] = $aluno['ra'];
		var_dump($dados); die('2.21');
		
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'LancarRendimentoEscolarClasse', Gdae_Importaescolas::$REST);
	}
	
	public function EnviarencerrarrendimentoescolarCIE($idaluno){
		/*2.22*/
		$idaluno = "1";
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		
		$dados = array();
		$dados['inCodEscola'] = $aluno['idescola'];		
		//Semestre da classe: 0 – Anual; 1 – 1º Semestre; 2 – 2º Semestre
		$dados['inSemestre'] = "FALTA";
		
		$dados = (object)$dados;
		var_dump($dados); die('2.22');
		
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'EncerrarRendimentoEscolarPorCIE', Gdae_Importaescolas::$REST);
	}

	public function Enviaralterarenderecoindicativo($idaluno){
		/*2.23*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idenderecoindicativo']);
		}
		
		$ano = 0;
		if((int)$aluno['idserie'] > 0):
			$series = new Escolasseries();
			$serie = $series->getSerieById($aluno['idserie']);
			if((isset($serie)) && ($serie)):
				$ano = $serie['ano']; 
			endif;
		endif;
		
		//0 ou 1 (0 - URBANO e 1- RURAL)
		$tipologradouro = 0;
		if($endereco['tipologradouro']==Enderecos::$_TIPO_LOGRADOURO_RURAL):
			$tipologradouro = 1;
		endif;		
		
		$dados = array();
		$dados['inRA'] = $aluno['ra'];		
		$dados['EndResidencial.inCep'] = $endereco['cep'];
		$dados['EndResidencial.inCidade'] = $endereco['cidade'];
		$dados['EndResidencial.inUFEndereco'] = $endereco['uf'];
		$dados['EndResidencial.inLogradouro'] = $endereco['nomelogradouro'];
		$dados['EndResidencial.inNumero'] = $endereco['numero'];
		$dados['EndResidencial.inBairro'] = $endereco['bairro'];
		$dados['EndResidencial.inTipoLogradouro'] = $tipologradouro;
		
		$dados = (object)$dados;
		var_dump($dados); die('2.23');
		
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'AlterarEnderecoIndicativo', Gdae_Importaescolas::$REST);
	}

	public function Enviaexcluircoletaclasse($idsala){
		/*2.24*/ 
		$salas = new Escolassalas();
		$aluno = $salas->getSalaById((int)$idsala);
		
		$dados = array();
		$dados['inAno'] = date('Y');		
		$dados['inNumClasse'] = $aluno['numerosala'];
		var_dump($dados); die('2.24');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'ExcluirColetaClasse', Gdae_Importaescolas::$REST);
	}	
	
	public function Enviaassociarirmao($idaluno){
		/*2.25*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		
		$dados = array();
		$dados['inRA'] = $aluno['ra'];	
		$digito = substr($aluno['ra'], -2);	
		$dados['inDigitoRA'] = $digito;
		$dados['inUFRA'] = $aluno['raunidadefederativa'];	
		
		$dados['inIrmaoRA'] = "FALTA";
		$dados['inIrmaoDigitoRA'] = "FALTA";		
		$dados['inIrmaoUFRA'] = "FALTA";		
		var_dump($dados); die('2.25');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		//$rows->envia_api($dados, 'AssociarIrmao', Gdae_Importaescolas::$REST);
	}

	public function Enviarealizarmatriciculaantecipadafases($idaluno){
		/*2.26*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
		}
		
		$ano = 0;
		if((int)$aluno['idserie'] > 0):
			$series = new Escolasseries();
			$serie = $series->getSerieById($aluno['idserie']);
			if((isset($serie)) && ($serie)):
				$ano = $serie['ano']; 
			endif;
		endif;
		
		//0 ou 1 (0 - URBANO e 1- RURAL)
		$tipologradouro = 0;
		if($endereco['tipologradouro']==Enderecos::$_TIPO_LOGRADOURO_RURAL):
			$tipologradouro = 1;
		endif;	
		
		$atribuicoes = Escolassalasatribuicoes::getSalaatribuicaoByIdAlunoHelper($idaluno);
		
		if(!$atribuicoes) return false;	
		
		$dados = array();
		$dados['inAno'] = date('Y');		
		$dados['inCodEscola'] = $aluno['idescola'];
		$dados['inFase'] = "FALTA";		
		$dados['inNomeAluno'] = $aluno['nomerazao'] . ' ' . $aluno['sobrenomefantasia'];
		
		$sexo = "";
		if($sexo=="Feminino"){
			$sexo = "F";
		}elseif($sexo=="Masculino"){
			$sexo = "M";
		}
		//Sexo do aluno (“M” ou “F”)
		$dados['inSexo'] = $sexo;

		//Cor/raça do aluno (item 4.4)
		$dados['inCorRaca'] = $aluno['corraca'];
		
		$datanascimento = $aluno['nascimento'];
		$datanascimento = explode(' ', $datanascimento);
		$datanasicmento_explode = $datanascimento[0];
		$datnascimento_final = explode('-', $datanasicmento_explode);		
		$dia_n = $datnascimento_final[2];
		$mes_n = $datnascimento_final[1];
		$ano_n = $datnascimento_final[0];		
		$dados['inDiaNascimento'] = $dia_n;
		$dados['inMesNascimento'] = $mes_n;
		$dados['inAnoNascimento'] = $ano_n;
		
		$datanacionalidadeentradadata = $aluno['nacionalidadeentradadata'];
		
		$dia_nac = "";
		$mes_nac = "";
		$ano_nac = "";
		if((isset($datanacionalidadeentradadata)) && ($datanacionalidadeentradadata)):
			$datanacionalidadeentradadata = explode(' ', $datanacionalidadeentradadata);
			$datanacionalidadeentradadata_explode = $datanacionalidadeentradadata[0];
			$datanacionalidadeentradadata_final = explode('-', $datanacionalidadeentradadata_explode);		
			$dia_nac = $datanacionalidadeentradadata_final[2];
			$mes_nac = $datanacionalidadeentradadata_final[1];
			$ano_nac = $datanacionalidadeentradadata_final[0];		
			$dados['inDiaNascimento'] = $dia_nac;
			$dados['inMesNascimento'] = $mes_nac;
			$dados['inAnoNascimento'] = $ano_nac;		
		endif;
		
		/*
		$dados['inGemeo'] = $aluno['nacionalidadetipo'];
		$dados['inIrmaoRA'] = $aluno['nacionalidadetipo'];
		$dados['inNomeMae'] = $aluno['nacionalidadetipo'];
		*/
		$dados['inNacionalidade'] = $aluno['nacionalidadetipo'];
		//País de origem (item 4.7)
		$dados['inPaisOrigem'] = $aluno['nacionalidade'];
		
		//Obrigatório para inNacionalidade=”2”
		$dados['inDiaEntBrasil'] = $dia_nac;
		$dados['inMesEntBrasil'] = $mes_nac;
		$dados['inAnoEntBrasil'] = $ano_nac;
		
		//Obrigatório para inNacionalidade=”1”
		
		$dados['inMunicipioNasc'] = $aluno['nascimentomunicipio'];
		$dados['inUFNasc'] = $aluno['nascimentouf'];
		
		//Indicador para recebimento de SMS, deverá ser “S” ou “N”
		$dados['inSMS'] = "N";
		
		$dados['EndResidencial.inCep'] = $endereco['cep'];
		$dados['EndResidencial.inCidade'] = $endereco['cidade'];
		$dados['EndResidencial.inUF'] = $endereco['uf'];
		$dados['EndResidencial.inLogradouro'] = $endereco['nomelogradouro'];
		$dados['EndResidencial.inNumero'] = $endereco['numero'];
		$dados['EndResidencial.inBairro'] = $endereco['bairro'];
		$dados['EndResidencial.inTipoLogradouro'] = $tipologradouro;
				
		//Obrigatório para FoneResidencial ou FoneRecados não nulo.
		//$dados['EndResidencial.inDDD'] = "FALTA";
		
		//Obrigatório, SOMENTE se ocorreu compatibilização de demanda no município
		
		$dados['inTipoEnsino'] = $atribuicoes['idtipoensino'];
		
		$serieano = "00";
		$dados['inSerieAno'] = $serieano;
		
		
		$mobilidadereduzida = "";
		if($aluno['mobilidadereduzida']=="Sim"){
			$mobilidadereduzida = "S";
		}elseif($aluno['mobilidadereduzida']=="Não"){
			$mobilidadereduzida = "N";
		}
		
		//Aluno possui mobilidade reduzida? (S ou N)
		$dados['MobilidadeReduzida'] = $mobilidadereduzida;
		
		$mobilidadetipo = $aluno['mobilidadetipo'];
		if($aluno['mobilidadetipo']=="temporario"){
			$dados['inTemporaria'] = "X";
		}elseif($aluno['mobilidadetipo']=="permanente"){
			$dados['inPermanente'] = "X";
		}
		var_dump($dados); die('2.26');	
		
		//Obrigatório caso algum dado da certidão for informado
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'RealizarMatricAntecipadaFases', Gdae_Importaescolas::$REST);
	}	
	
	public function EnviamatriculaantecipadaRAfases($idaluno){
		/*2.27*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;
		
		$atribuicoes = Escolassalasatribuicoes::getSalaatribuicaoByIdAlunoHelper($idaluno);
		if(!$atribuicoes) return false;	
				
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
		}
		
		$ano = 0;
		if((int)$aluno['idserie'] > 0):
			$series = new Escolasseries();
			$serie = $series->getSerieById($aluno['idserie']);
			if((isset($serie)) && ($serie)):
				$ano = $serie['ano']; 
			endif;
		endif;
		
		//0 ou 1 (0 - URBANO e 1- RURAL)
		$tipologradouro = 0;
		if($endereco['tipologradouro']==Enderecos::$_TIPO_LOGRADOURO_RURAL):
			$tipologradouro = 1;
		endif;			
		
		$dados = array();
		$dados['inAno'] = date('Y');		
		$dados['inCodEscola'] = $aluno['idescola'];
		$dados['inFase'] = "FALTA";

		$dados['inRA'] = $aluno['ra'];
		$digito = substr($aluno['ra'], -2);	
		$dados['inDigitoRA'] = $digito;
		$dados['inUFRA'] = $aluno['raunidadefederativa'];
		//
		$dados['inTipoEnsino'] = $atribuicoes['idtipoensino'];
		//Série/ano da inscrição do aluno Informar '00' quando a classe da matricula não possuir serie / ano
		$dados['inSerieAno'] = "FALTA";
		//Sexo do aluno (“M” ou “F”)
		$sexo = "";
		if($aluno['sexo']=="Masculino"):
			$sexo = "M";
		elseif ($aluno['sexo']=="Feminino"):
			$sexo = "F";
		endif;		
		$dados['inSexo'] = $sexo;
		
		$dados['inNomeAluno'] = $aluno['nomerazao'] . ' ' . $aluno['sobrenomefantasia'];
		
		
		$dados['EndResidencial.inCep'] = $endereco['cep'];
		$dados['EndResidencial.inCidade'] = $endereco['cidade'];
		$dados['EndResidencial.inUF'] = $endereco['uf'];
		$dados['EndResidencial.inLogradouro'] = $endereco['nomelogradouro'];
		$dados['EndResidencial.inNumero'] = $endereco['numero'];
		$dados['EndResidencial.inBairro'] = $endereco['bairro'];
		$dados['EndResidencial.inTipoLogradouro'] = $tipologradouro;
		var_dump($dados); die('2.27');	
		//Obrigatório para FoneResidencial ou FoneRecados não nulo.
		//$dados['EndResidencial.inDDD'] = "FALTA";
		//Obrigatório, SOMENTE se ocorreu compatibilização de demanda no município
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'RealizarMatricAntecipadaRAFases', Gdae_Importaescolas::$REST);
	}

	public function Enviaalunoensinomedio($idaluno){
		/*2.28*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;		
		
		$atribuicoes = new Escolassalasatribuicoes();
		$atribuicao = $atribuicoes->getSalaatribuicaoByIdAluno($idaluno);
		if(!$atribuicao) return false;			

		$vinculos = new Escolasvinculos();
		$vinculo = $vinculos->getVinculoById($atribuicao['idvinculo']);
		if(!$vinculo) return false;			
		
		$uf = "";
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
			if(isset($endereco['uf'])){
				$uf = $endereco['uf'];
			}
		}
			
	
		$ano = 0;
		if((int)$aluno['idserie'] > 0):
			$series = new Escolasseries();
			$serie = $series->getSerieById($aluno['idserie']);
			if((isset($serie)) && ($serie)):
				$ano = $serie['ano']; 
			endif;
		endif;
		
		//0 ou 1 (0 - URBANO e 1- RURAL)
		$tipologradouro = 0;
		if($endereco['tipologradouro']==Enderecos::$_TIPO_LOGRADOURO_RURAL):
			$tipologradouro = 1;
		endif;			
		
		$dados = array();
		$dados['inAno'] = date('Y');	
		$dados['inRA'] = $aluno['ra'];
		$digito = substr($aluno['ra'], -2);	
		$dados['inDigitoRA'] = $digito;
		$dados['inUF'] = $endereco['uf'];

		$dados['inTurno'] = $vinculo['turno'];
		$dados['EndResidencial.inCep'] = $endereco['cep'];
		$dados['EndResidencial.inCidade'] = $endereco['cidade'];
		$dados['EndResidencial.inUF'] = $endereco['uf'];
		$dados['EndResidencial.inLogradouro'] = $endereco['nomelogradouro'];
		$dados['EndResidencial.inNumero'] = $endereco['numero'];
		$dados['EndResidencial.inBairro'] = $endereco['bairro'];
		$dados['EndResidencial.inTipoLogradouro'] = $tipologradouro;
		//Obrigatório para FoneResidencial ou FoneRecados não nulo.
		//$dados['EndResidencial.inDDD'] = $aluno['nacionalidadetipo'];
		//Obrigatório, SOMENTE se ocorreu compatibilização de demanda no município
		var_dump($dados); die("2.28");
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'DefinirAlunoEnsinoMedio', Gdae_Importaescolas::$REST);
	}	
	
	public function Enviacancelamentoinscricoesfases($idaluno){
		/*2.29*/
		$idaluno = "1";
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;
		
		$uf = "";		
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
			if(isset($endereco['uf'])){
				$uf = $endereco['uf'];
			}
		}
		
		$dados = array();
		$dados['inAno'] = date('Y');	
		$dados['inRA'] = $aluno['ra'];
		$digito = substr($aluno['ra'], -2);	
		$dados['inDigitoRA'] = $digito;
		$dados['inUF'] = $uf;
		$dados['inEscola'] = $aluno['idescola'];
		$dados['inFase'] = "FALTA";
		var_dump($dados); die('2.29');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'CancelarInscricaoDefinicao', Gdae_Importaescolas::$REST);
	}

	public function Enviaalunotransferencia($idaluno){
		/*2.30*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;		
		
		$atribuicoes = new Escolassalasatribuicoes();
		$atribuicao = $atribuicoes->getSalaatribuicaoByIdAluno($idaluno);
		if(!$atribuicao) return false;			

		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
		}
		
		$ano = 0;
		if((int)$aluno['idserie'] > 0):
			$series = new Escolasseries();
			$serie = $series->getSerieById($aluno['idserie']);
			if((isset($serie)) && ($serie)):
				$ano = $serie['ano']; 
			endif;
		endif;
		
		//0 ou 1 (0 - URBANO e 1- RURAL)
		$tipologradouro = 0;
		if($endereco['tipologradouro']==Enderecos::$_TIPO_LOGRADOURO_RURAL):
			$tipologradouro = 1;
		endif;		
		
		$dados = array();
		$dados['inAno'] = date('Y');	
		$dados['inRA'] = $aluno['ra'];
		$digito = substr($aluno['ra'], -2);	
		$dados['inDigitoRA'] = $digito;
		$dados['inUF'] = $endereco['uf'];
		$dados['inEscola'] = $aluno['idescola'];
		
		$dados['inTipoEnsino'] = $atribuicao['idtipoensino'];
		
		$dados['inAnoSerie'] = "FALTA";
		
		$dados['EndResidencial.inCep'] = $endereco['cep'];
		$dados['EndResidencial.inCidade'] = $endereco['cidade'];
		$dados['EndResidencial.inUF'] = $endereco['uf'];
		$dados['EndResidencial.inLogradouro'] = $endereco['nomelogradouro'];
		$dados['EndResidencial.inNumero'] = $endereco['numero'];
		$dados['EndResidencial.inBairro'] = $endereco['bairro'];
		$dados['EndResidencial.inTipoLogradouro'] = $tipologradouro;
		var_dump($dados); die('2.30');
		//Obrigatório para FoneResidencial ou FoneRecados não nulo.
		//$dados['EndResidencial.inDDD'] = $aluno['nacionalidadetipo'];
		//Obrigatório, SOMENTE se ocorreu compatibilização de demanda no município
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'InscreverAlunotransferencia', Gdae_Importaescolas::$REST);
	}	
	
	public function Enviacancelarinscricaoalunotransf($idaluno){
		/*2.31*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;
		$uf = "";
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
			if(isset($endereco['uf'])){
				$uf = $endereco['uf'];
			}
		}		
		
		$dados = array();
		$dados['inAno'] = date('Y');	
		$dados['inRA'] = $aluno['ra'];
		$digito = substr($aluno['ra'], -2);	
		$dados['inDigitoRA'] = $digito;
		$dados['inUF'] = $uf;
		$dados['inEscola'] = $aluno['idescola'];
		var_dump($dados); die('2.31');	
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'CancelarInscricaoAlunoTransf', Gdae_Importaescolas::$REST);
	}	
	
	public function Enviainscricaointencaotransferencia($idaluno){
		/*2.32*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;
		
		$atribuicoes = new Escolassalasatribuicoes();
		$atribuicao = $atribuicoes->getSalaatribuicaoByIdAluno($idaluno);
		if(!$atribuicao) return false;			
		
		$uf = "";
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
			if(isset($endereco['uf'])){
				$uf = $endereco['uf'];
			}
		}	
		
		$dados = array();
		$dados['inAno'] = date('Y');	
		$dados['inRA'] = $aluno['ra'];
		$digito = substr($aluno['ra'], -2);	
		$dados['inDigitoRA'] = $digito;
		$dados['inUF'] = $uf;
		$dados['inEscola'] = $aluno['idescola'];
		$dados['inTipoEnsino'] = $atribuicao['idtipoensino'];
		$dados['inAnoSerie'] = "FALTA";
		var_dump($dados); die('2.32');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'InscricaoIntencaoTransferencia', Gdae_Importaescolas::$REST);
	}

	public function Enviacancelarintencaotransferencia($idaluno){
		/*2.33*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;
		
		$uf = "";
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
			if(isset($endereco['uf'])){
				$uf = $endereco['uf'];
			}
		}			
		
		$dados = array();
		$dados['inAno'] = date('Y');	
		$dados['inRA'] = $aluno['ra'];
		$digito = substr($aluno['ra'], -2);	
		$dados['inDigitoRA'] = $digito;
		$dados['inUF'] = $uf;
		$dados['inEscola'] = $aluno['idescola'];
		var_dump($dados); die('2.33');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'CancelarIntencaoTransferencia', Gdae_Importaescolas::$REST);
	}	
	
	public function Enviainscreveralunopordeslocamento($idaluno){
		/*2.34*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;
		
		$uf = "";
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
			if(isset($endereco['uf'])){
				$uf = $endereco['uf'];
			}
		}	
		
		$ano = 0;
		if((int)$aluno['idserie'] > 0):
			$series = new Escolasseries();
			$serie = $series->getSerieById($aluno['idserie']);
			if((isset($serie)) && ($serie)):
				$ano = $serie['ano']; 
			endif;
		endif;
		
		//0 ou 1 (0 - URBANO e 1- RURAL)
		$tipologradouro = 0;
		if($endereco['tipologradouro']==Enderecos::$_TIPO_LOGRADOURO_RURAL):
			$tipologradouro = 1;
		endif;		
		
		$dados = array();
		$dados['inAno'] = date('Y');	
		$dados['inRA'] = $aluno['ra'];
		$digito = substr($aluno['ra'], -2);	
		$dados['inDigitoRA'] = $digito;
		$dados['inUF'] = $uf;
		$dados['inEscola'] = $aluno['idescola'];
		//Identifica se na inscrição haverá ou não mudança do endereço do aluno. Valores possíveis: 1 – Com mudança de endereço 2 – Sem mudança de endereço
		$dados['inTipoInscricao'] = "FALTA";
		//Obrigatório para inTipoInscricao = 1 - Identifica qual o motivo da mudança do endereço do aluno. Valores possíveis: 1 – Mudança de Residência 2 – Proximidade local trabalho e/ou horário de trabalho do aluno
		$dados['inMotivo'] = "FALTA";
		$dados['EndResidencial.inCep'] = $endereco['cep'];
		$dados['EndResidencial.inCidade'] = $endereco['cidade'];
		$dados['EndResidencial.inUF'] = $endereco['uf'];
		$dados['EndResidencial.inLogradouro'] = $endereco['nomelogradouro'];
		$dados['EndResidencial.inNumero'] = $endereco['numero'];
		$dados['EndResidencial.inBairro'] = $endereco['bairro'];
		$dados['EndResidencial.inTipoLogradouro'] = $tipologradouro;
		//Obrigatório para FoneResidencial ou FoneRecados não nulo.
		$dados['EndResidencial.inDDD'] = $aluno['nacionalidadetipo'];
		//Obrigatório para inTipoInscricao = 2 - Caso um seja informado os demais serão obrigatórios
		$tipologradouro_indicativo = 0;
		$endereco_indicativo = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco_indicativo = $enderecos->getEnderecoByIdendereco($aluno['idenderecoindicativo']);
			if($endereco_indicativo['tipologradouro']==Enderecos::$_TIPO_LOGRADOURO_RURAL):
				$tipologradouro_indicativo = 1;
			endif;				
		}			
		$dados['EndIndicativo.inCep'] = $endereco_indicativo['cep'];
		$dados['EndIndicativo.inCidade'] = $endereco_indicativo['cidade'];
		$dados['EndIndicativo.inUF'] = $endereco_indicativo['uf'];
		$dados['EndIndicativo.inLogradouro'] = $endereco_indicativo['nomelogradouro'];
		$dados['EndIndicativo.inNumero'] = $endereco_indicativo['numero'];
		$dados['EndIndicativo.inBairro'] = $endereco_indicativo['bairro'];		
		var_dump($dados); die('2.34');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'InscreverAlunoPorDeslocamento', Gdae_Importaescolas::$REST);
	}

	public function Enviacancelarinscricaoalunodeslocamento($idaluno){
		/*2.35*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;
		
		$uf = "";
		$endereco = array();
		if((int)$aluno['idendereco'] > 0){
			$enderecos = new Enderecos();
			$endereco = $enderecos->getEnderecoByIdendereco($aluno['idendereco']);
			if(isset($endereco['uf'])){
				$uf = $endereco['uf'];
			}
		}		
		
		$dados = array();
		$dados['inAno'] = date('Y');	
		$dados['inRA'] = $aluno['ra'];
		$digito = substr($aluno['ra'], -2);	
		$dados['inDigitoRA'] = $digito;
		$dados['inUF'] = $uf;
		$dados['inEscola'] = $aluno['idescola'];
		var_dump($dados); die('2.35');
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'CancelarInscricaoAlunoPorDeslocamento', Gdae_Importaescolas::$REST);
	}

	public function Enviaclassificarnumerochamadaclasse($idaluno){
		/*2.36*/
		$idaluno = "1";
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;
		
		$dados = array();
		$dados['inAno'] = date('Y');	
		$dados['inNumClasse'] = "FALTA";
		//Ordem de Classificação: - 1 - Geral - 2 – Primeiro meninos e depois meninas - 3 – Primeiro meninas e depois meninos
		$dados['inOrdemClassificacao'] = "FALTA";
		var_dump($dados); die('2.36');	
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'ClassificarGerarNumeroChamadaPorClasse', Gdae_Importaescolas::$REST);
	}

	public function Enviaclassificargerarchamadaescola($idaluno){
		/*2.37*/
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById((int)$idaluno);
		if(!$aluno) return false;
		
		$atribuicoes = new Escolassalasatribuicoes();
		$atribuicao = $atribuicoes->getSalaatribuicaoByIdAluno($idaluno);
		if(!$atribuicao) return false;	
		
		$dados = array();
		$dados['inAno'] = date('Y');	
		$dados['inCodigoEscola'] = $aluno['idescola'];
		//Tipo de ensino das classes (item 4.2)	
		$dados['inTipoEnsino'] = $atribuicao['idtipoensino'];
		$dados['inSerieAno'] = "FALTA";
		//Ordem de Classificação: - 1 - Geral - 2 – Primeiro meninos e depois meninas - 3 – Primeiro meninas e depois meninos
		$dados['inOrdemClassificacao'] = "FALTA";
		var_dump($dados); die('2.37');	
		
		$dados = (object)$dados;
		$rows = new Gdae_Importaescolas();
		$rows->envia_api($dados, 'ClassificarGerarNrChamadaClassePorEscola', Gdae_Importaescolas::$REST);
	}
		
}