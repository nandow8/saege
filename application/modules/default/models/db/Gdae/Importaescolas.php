<?php

class Gdae_Importaescolas extends Zend_Db_Table_Abstract {
	public static $PATCH = "PATCH";
	public static $POST = "POST";
	public static $GET = "GET";
	public static $REST = "REST";
	public static $PUT = "PUT";
	public static $DELETE = "DELETE";	
	
	public function envia_api($_postData, $recurso, $method) {
		
		$resourcePath = "http://ensembledes.cloudapp.net:57772/Servicos/" . $recurso;
		$api_key = base64_encode('3A958CAD25FF603526E2747E32345822:');
		$postData = json_encode($_postData);
		
		$this->json_envio = $postData;
		
		$postData = str_replace(
		    array("\r\n", "\n", "\r"), 
		    '',
		    $postData
		);
		var_dump($_postData); die();

		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		
		
		//curl_setopt($curl, CURLOPT_HTTPHEADER, json_decode('["Content-Type: application\/json","Authorization: Basic '.$api_key.'"]'));
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json;charset=UTF-8", "Authorization: Basic $api_key"));
		
		if ($method == self::$POST) {
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		} else if ($method == self::$PATCH) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		} else if ($method == self::$PUT) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		} else if ($method == self::$DELETE) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		} else if ($method == self::$REST) {
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "REST");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
		} else if ($method != self::$REST) {
			throw new ApiException('Method ' . $method . ' is not recognized.');
		}		

		
		
		curl_setopt($curl, CURLOPT_URL, $resourcePath);
		curl_setopt($curl, CURLOPT_USERAGENT, 'PHP-Client');
	
		$response = curl_exec($curl);
		$this->setRawresponse($response);
		//echo $response;
		//die();
		
		$this->json_retorno = $response;
				
		
		$http_header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
		$http_header = substr($response, 0, $http_header_size);
		$http_body = substr($response, $http_header_size);
		$response_info = curl_getinfo($curl);

		
		// Handle the response
		if ($response_info['http_code'] == 0) {
			throw new Exception("API call to $resourcePath timed out: ".serialize($response_info));
		} else if ($response_info['http_code'] >= 200 && $response_info['http_code'] <= 299 ) {
			$data = json_decode($response);
			if (json_last_error() > 0) { // if response is a string
				die('aaa1');
				$data = $http_body;
			}
		} else {
			throw new Exception("[".$response_info['http_code']."] Error connecting to the API ($resourcePath)");
		}
		return $data;
	
	}		
}
