<?php

/**
 * Define o modelo Solicitacoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Solicitacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "solicitacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getSolicitacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$solicitacoes = new Solicitacoes();
		return $solicitacoes->getSolicitacoes($queries, $page, $maxpage);
	}
	
	public function getSolicitacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " s1.id = $id ");
		
		$idprotocolo = (isset($queries['idprotocolo'])) ? (int)$queries['idprotocolo'] : false;
		if ($idprotocolo) array_push($where, " s1.idprotocolo = $idprotocolo ");
		
		$idusuariocriacao = (isset($queries["idusuariocriacao"])) ? $queries["idusuariocriacao"] : false;
		if ($idusuariocriacao) array_push($where, " s1.idusuariocriacao = $idusuariocriacao ");

		$iddepartamentosecretaria = (isset($queries["iddepartamentosecretaria"])) ? $queries["iddepartamentosecretaria"] : false;
		if ($iddepartamentosecretaria) array_push($where, " s1.iddepartamentosecretaria = $iddepartamentosecretaria ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " s1.idescola = $idescola ");

		$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " s1.titulo LIKE '%$titulo%' ");

		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " s1.descricoes = '$descricoes' ");

		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " s1.origem LIKE '%$origem%' ");

		$destinatario = (isset($queries["destinatario"])) ? $queries["destinatario"] : false;
		if ($destinatario) array_push($where, " s1.destinatario LIKE '%$destinatario%' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " s1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "s1.*"; 
;
		
		if ($total) $fields = "COUNT(s1.id) as total";
		
		
		$ordem = "ORDER BY s1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM solicitacoes s1
					
					WHERE s1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getSolicitacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSolicitacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSolicitacaoByIdHelper($id, $queries = array()) {
		$rows = new Solicitacoes();
		return $rows->getSolicitacaoById($id, $queries);
	}		
	
	
	public function getUltimaSolicitacaoByIdprotocolo($idprotocolo, $queries = array()) {
		if ($idprotocolo==0) return false;
		
		$queries['idprotocolo'] = $idprotocolo;
		$queries['order'] = "ORDER BY s1.id DESC";
		$rows = $this->getSolicitacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getUltimaSolicitacaoByIdprotocoloHelper($idprotocolo, $queries = array()) {
		$rows = new Solicitacoes();
		return $rows->getUltimaSolicitacaoByIdprotocolo($idprotocolo, $queries);
	}

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Solicitacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		

		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idprotocolo = (array_key_exists("idprotocolo",$dados)) ? $dados["idprotocolo"] : $row->idprotocolo;
		$row->idusuariocriacao = (array_key_exists("idusuariocriacao",$dados)) ? $dados["idusuariocriacao"] : $row->idusuariocriacao;
		$row->iddepartamentosecretaria = (array_key_exists("iddepartamentosecretaria",$dados)) ? $dados["iddepartamentosecretaria"] : $row->iddepartamentosecretaria;
		$row->idusuarioresponsavel = (array_key_exists("idusuarioresponsavel",$dados)) ? $dados["idusuarioresponsavel"] : $row->idusuarioresponsavel;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->idescolausuariocriacao = (array_key_exists("idescolausuariocriacao",$dados)) ? $dados["idescolausuariocriacao"] : $row->idescolausuariocriacao;
		$row->idescolausuarioresponsavel = (array_key_exists("idescolausuarioresponsavel",$dados)) ? $dados["idescolausuarioresponsavel"] : $row->idescolausuarioresponsavel;
		$row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
		$row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
		$row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
		$row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
		$row->destinatario = (array_key_exists("destinatario",$dados)) ? $dados["destinatario"] : $row->destinatario;

		$row->dataagendamento = (array_key_exists("dataagendamento",$dados)) ? $dados["dataagendamento"] : $row->dataagendamento;
		$row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
		$row->prioridade = (array_key_exists("prioridade",$dados)) ? $dados["prioridade"] : $row->prioridade;

		$row->diretora = (array_key_exists("diretora",$dados)) ? $dados["diretora"] : $row->diretora;
		$row->coordenador = (array_key_exists("coordenador",$dados)) ? $dados["coordenador"] : $row->coordenador;

		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
	
}