<?php

/**
* Define o modelo Ferias
*
* @author		Alexandre Martin Narciso		
* @uses        Zend_Db_Table_Abstract
* @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
* @version     1.0
*/
class Filademandacreches extends Zend_Db_Table_Abstract {
	
	/**
    * Define o nome da tabela
    * @var string
    */
	protected $_name = "ferias";
	
	/**
    * Define a chave primaria
    * @var integer
    */
    protected $_primary = "id";

    public static function getFeriasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$ferias = new Ferias();
		return $ferias->getFerias($queries, $page, $maxpage);
	}
    
    public function getFerias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
        
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		 
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";
        
		$fields = "f1.*"; 
		;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
		$strsql = "SELECT $fields 
        FROM ferias f1
        
        WHERE f1.excluido='nao' 
        $w 
        $ordem	
        $limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}
    
}