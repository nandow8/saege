<?php

/**
 * Define o modelo Coordenacaomaterialdidaticos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Coordenacaomaterialdidaticos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "coordenacaomaterialdidaticos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCoordenacaomaterialdidaticosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$coordenacaomaterialdidaticos = new Coordenacaomaterialdidaticos();
		return $coordenacaomaterialdidaticos->getCoordenacaomaterialdidaticos($queries, $page, $maxpage);
	}
	
	public function getCoordenacaomaterialdidaticos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " c1.idusuario LIKE %$idusuario% ");

		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " c1.sequencial LIKE '%$sequencial%' ");

		$idmateria = (isset($queries["idmateria"])) ? $queries["idmateria"] : false;
		if ($idmateria) array_push($where, " c1.idmateria = $idmateria ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " c1.idescola = $idescola ");

		$idserie = (isset($queries["idserie"])) ? $queries["idserie"] : false;
		if ($idserie) array_push($where, " c1.idserie = $idserie ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='c1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM coordenacaomaterialdidaticos c1
					
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getCoordenacaomaterialdidaticoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCoordenacaomaterialdidaticos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCoordenacaomaterialdidaticoByIdHelper($id, $queries = array()) {
		$rows = new Coordenacaomaterialdidaticos();
		return $rows->getCoordenacaomaterialdidaticoById($id, $queries);
	}		
	
	
	public static function getCoordenacaomaterialdidaticoByUltimoHelper() {
		$queries = array();	
		$queries['order'] = "ORDER BY c1.id DESC";
		$rows = new Coordenacaomaterialdidaticos();
		$_row = $rows->getCoordenacaomaterialdidaticos($queries);
		if(isset($_row[0]['id'])) return ($_row[0]['id']+1) ;
		
		return '1';
	}	

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Agendafuncionariosinternos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
		 
								
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
		 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		 $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
		 $row->idmateria = (array_key_exists("idmateria",$dados)) ? $dados["idmateria"] : $row->idmateria;
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		 $row->tipoensino = (array_key_exists("tipoensino",$dados)) ? $dados["tipoensino"] : $row->tipoensino;
		 $row->idserie = (array_key_exists("idserie",$dados)) ? $dados["idserie"] : $row->idserie;
		 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;

		 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
				
				
		$row->save();
		
		return $row;
	}
	
}