<?php

/**
 * Define o modelo Rhdeclaracoesdependentes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Rhdeclaracoesdependentes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "rhdeclaracoesdependentes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getRhdeclaracoesdependentesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$rhdeclaracoesdependentes = new Rhdeclaracoesdependentes();
		return $rhdeclaracoesdependentes->getRhdeclaracoesdependentes($queries, $page, $maxpage);
	}
	
	public function getRhdeclaracoesdependentes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " r1.idsecretaria = $idsecretaria ");

$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " r1.idfuncionario = $idfuncionario ");
		
		$idfuncionarioescola = (isset($queries["idfuncionarioescola"])) ? $queries["idfuncionarioescola"] : false;
		if ($idfuncionarioescola) array_push($where, " r1.idfuncionarioescola = $idfuncionarioescola ");

		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " r1.origem = '$origem' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " r1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*, f1.nome, f1.rg, f1.cpf, f1.estadocivil, f1.conjugenome, f1.grauinstrucao, f1.dataadmissao, f1.cargo, f1.idendereco, f1.telefone, f1.celular, f1.salarioinicial, f1.cargocomissionado, f1.dataadmissaocomissionado, f1.matricula, f1.secretaria, f1.idendereco, f1.idcargo"; 
		;
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM rhdeclaracoesdependentes r1
						LEFT JOIN funcionariosgerais f1 ON f1.id = r1.idfuncionario 
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getRhdeclaracaodependenteById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getRhdeclaracoesdependentes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getRhdeclaracaodependenteByIdHelper($id, $queries = array()) {
		$rows = new Rhdeclaracoesdependentes();
		return $rows->getRhdeclaracaodependenteById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Rhdeclaracoesdependentes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
 $row->idfuncionarioescola = (array_key_exists("idfuncionarioescola",$dados)) ? $dados["idfuncionarioescola"] : $row->idfuncionarioescola;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();

		if (array_key_exists('itens', $dados)) $this->setItens($dados['itens'], $row->id, $row->logusuario, $row->logdata);
		return $row;
	}
	private function setItens($dados, $iddeclaracao, $logusuario = false, $logdata = false) {
		if (!is_array($dados)) return;
		
		$itens = new Rhdeclaracoesdependentespessoas(); 
		$ids = $dados['idsdependentes'];
		$nomes = $dados['nomes'];
		$grauparentesco = $dados['grauparentesco'];
		$nascimento = $dados['nascimento'];
		
		$ids = array();
		foreach ($nomes as $i=>$id) {
			$d = array();
			
			$d['id'] = isset($ids[$i]) ? $ids[$i] : '0';
			$d['iddeclaracao'] = $iddeclaracao; 
			$d['nome'] = trim(strip_tags($nomes[$i])); 
			$d['grauparentesco'] = trim(strip_tags($grauparentesco[$i]));
			$_nascimento = trim(strip_tags($nascimento[$i]));
			$_nascimento = Mn_Util::stringToTime($_nascimento);		
			$_nascimento = date('Y-m-d G:i:s', $_nascimento);
			$d['nascimento'] = $_nascimento;
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $logusuario;
			$d['logdata'] = $logdata;
 
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);

		}

		
		$itens = implode(",", $ids);
		
		if ($itens=="") $itens = "0";
		$strsql = "DELETE FROM rhdeclaracoesdependentespessoas WHERE iddeclaracao=$iddeclaracao AND id NOT IN ($itens)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);	
			
	}	
}