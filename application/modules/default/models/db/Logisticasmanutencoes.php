<?php

/**
 * Define o modelo Logisticasmanutencoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Logisticasmanutencoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "logisticasmanutencoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLogisticasmanutencoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$logisticasmanutencoes = new Logisticasmanutencoes();
		return $logisticasmanutencoes->getLogisticasmanutencoes($queries, $page, $maxpage);
	}
	
	public function getLogisticasmanutencoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		
		$idfrota = (isset($queries["idfrota"])) ? $queries["idfrota"] : false;
		if ($idfrota) array_push($where, " l1.idfrota = $idfrota ");

$idtipomanutencao = (isset($queries["idtipomanutencao"])) ? $queries["idtipomanutencao"] : false;
		if ($idtipomanutencao) array_push($where, " l1.idtipomanutencao = $idtipomanutencao ");

$idfornecedor = (isset($queries["idfornecedor"])) ? $queries["idfornecedor"] : false;
		if ($idfornecedor) array_push($where, " l1.idfornecedor = $idfornecedor ");

$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " l1.idfuncionario = $idfuncionario ");

$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " l1.sequencial LIKE '%$sequencial%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " l1.descricoes = '$descricoes' ");

$numeroprocesso = (isset($queries["numeroprocesso"])) ? $queries["numeroprocesso"] : false;
		if ($numeroprocesso) array_push($where, " l1.numeroprocesso = $numeroprocesso ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " l1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " l1.data <= '$data_f' ");

$aprovacao = (isset($queries["aprovacao"])) ? $queries["aprovacao"] : false;
		if ($aprovacao) array_push($where, " l1.aprovacao LIKE '%$aprovacao%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*"; 
		;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM logisticasmanutencoes l1
					
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLogisticasmanutencaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLogisticasmanutencoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLogisticasmanutencaoByIdHelper($id, $queries = array()) {
		$rows = new Logisticasmanutencoes();
		return $rows->getLogisticasmanutencaoById($id, $queries);
	}		
	
	public function getUltimoLogisticamanutencoes($queries = array()) {
		$queries['order'] = 'ORDER BY l1.id DESC';
		$rows = $this->getLogisticasmanutencoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Logisticasmanutencoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfrota = (array_key_exists("idfrota",$dados)) ? $dados["idfrota"] : $row->idfrota;
 $row->idtipomanutencao = (array_key_exists("idtipomanutencao",$dados)) ? $dados["idtipomanutencao"] : $row->idtipomanutencao;
 $row->idfornecedor = (array_key_exists("idfornecedor",$dados)) ? $dados["idfornecedor"] : $row->idfornecedor;
 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->numeroprocesso = (array_key_exists("numeroprocesso",$dados)) ? $dados["numeroprocesso"] : $row->numeroprocesso;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->hora = (array_key_exists("hora",$dados)) ? $dados["hora"] : $row->hora;
 $row->aprovacao = (array_key_exists("aprovacao",$dados)) ? $dados["aprovacao"] : $row->aprovacao;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}