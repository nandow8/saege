<?php

/**
 * Define o modelo Patrimoniosdoacoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Patrimoniosdoacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "patrimoniosdoacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getPatrimoniosdoacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$patrimoniosdoacoes = new Patrimoniosdoacoes();
		return $patrimoniosdoacoes->getPatrimoniosdoacoes($queries, $page, $maxpage);
	}
	
	public function getPatrimoniosdoacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		$idlicenca = (isset($queries['idlicenca'])) ? (int)$queries['idlicenca'] : false;
		if ($idlicenca) array_push($where, " p1.idlicenca = $idlicenca ");
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " p1.idsecretaria = $idsecretaria ");

		$doacao = (isset($queries["doacao"])) ? $queries["doacao"] : false;
		if ($doacao) array_push($where, " p1.doacao LIKE '%$doacao%' ");

		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " p1.descricoes = '$descricoes' ");

		$idusuariocriacao = (isset($queries["idusuariocriacao"])) ? $queries["idusuariocriacao"] : false;
		if ($idusuariocriacao) array_push($where, " p1.idusuariocriacao = $idusuariocriacao ");

		$aprovacao = (isset($queries["aprovacao"])) ? $queries["aprovacao"] : false;
		if ($aprovacao) array_push($where, " p1.aprovacao LIKE '%$aprovacao%' ");

		$idusuarioaprovacao = (isset($queries["idusuarioaprovacao"])) ? $queries["idusuarioaprovacao"] : false;
		if ($idusuarioaprovacao) array_push($where, " p1.idusuarioaprovacao = $idusuarioaprovacao ");

		$descricoesaprovacao = (isset($queries["descricoesaprovacao"])) ? $queries["descricoesaprovacao"] : false;
		if ($descricoesaprovacao) array_push($where, " p1.descricoesaprovacao = '$descricoesaprovacao' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM patrimoniosdoacoes p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPatrimoniosdoacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPatrimoniosdoacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPatrimoniosdoacaoByIdHelper($id, $queries = array()) {
		$rows = new Patrimoniosdoacoes();
		return $rows->getPatrimoniosdoacaoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Patrimoniosdoacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$idlicenca = (!isset($dados['idlicenca'])) ? 0 : (int)$dados['idlicenca'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		

		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
		$row->doacao = (array_key_exists("doacao",$dados)) ? $dados["doacao"] : $row->doacao;
		$row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->idusuariocriacao = (array_key_exists("idusuariocriacao",$dados)) ? $dados["idusuariocriacao"] : $row->idusuariocriacao;
		$row->aprovacao = (array_key_exists("aprovacao",$dados)) ? $dados["aprovacao"] : $row->aprovacao;
		$row->idusuarioaprovacao = (array_key_exists("idusuarioaprovacao",$dados)) ? $dados["idusuarioaprovacao"] : $row->idusuarioaprovacao;
		$row->logdataaprovacao = (array_key_exists("logdataaprovacao",$dados)) ? $dados["logdataaprovacao"] : $row->logdataaprovacao;
		$row->descricoesaprovacao = (array_key_exists("descricoesaprovacao",$dados)) ? $dados["descricoesaprovacao"] : $row->descricoesaprovacao;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
	
}