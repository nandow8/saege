<?php

class Escolasprogramassociais extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasprogramassociais";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getProgramasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolasprogramassociais();
		return $produtos->getProgramas($queries, $page, $maxpage);
	}
	
	public function getProgramas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? $queries['idsecretaria'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " p1.id=$id ");
		if ($idsecretaria) array_push($where, " p1.idsecretaria=$idsecretaria ");
		if ($chave) array_push($where, " ((p1.programa LIKE '%$chave%') OR (p1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " p1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*";
		if ($total) $fields = "COUNT(p1.id) as total";
		
		
		$ordem = "ORDER BY p1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasprogramassociais p1
					WHERE p1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getProgramaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getProgramas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getProgramaByIdHelper($id, $queries = array()) {
		$rows = new Escolasprogramassociais();
		return $rows->getProgramaById($id, $queries);
	}		
	

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Programas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s'); 		
		}else {
			$novoRegistro = false;
		} 

		$row->programa = (array_key_exists('programa',$dados)) ? $dados['programa'] : $row->programa;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		
		$row->save();

		return $row;
	}
	
	
}