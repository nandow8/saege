<?php

/**
 * Define o modelo Salasrecursosprofessores
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Salasrecursosprofessores extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "salasrecursosprofessores";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getSalasrecursosprofessoresHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$salasrecursosprofessores = new Salasrecursosprofessores();
		return $salasrecursosprofessores->getSalasrecursosprofessores($queries, $page, $maxpage);
	}
	
	public function getSalasrecursosprofessores($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " s1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " s1.idescola = $idescola ");

$periodo = (isset($queries["periodo"])) ? $queries["periodo"] : false;
		if ($periodo) array_push($where, " s1.periodo LIKE '%$periodo%' ");

$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " s1.idfuncionario = $idfuncionario ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " s1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "s1.*"; 
		;
		
		if ($total) $fields = "COUNT(s1.id) as total";
		
		$ordem = "ORDER BY s1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM salasrecursosprofessores s1
					
					WHERE s1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getSalasrecursosprofessorById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSalasrecursosprofessores($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSalasrecursosprofessorByIdHelper($id, $queries = array()) {
		$rows = new Salasrecursosprofessores();
		return $rows->getSalasrecursosprofessorById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Salasrecursosprofessores
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->periodo = (array_key_exists("periodo",$dados)) ? $dados["periodo"] : $row->periodo;
		$row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();

		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::save", Usuarios::getUsuario("id"), "salva com sucesso!");
		}
		elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "ID ".$id." excluída com sucesso!");
		}
		elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "ID ".$id." - ". $dados['status']." com sucesso!");
		}
		else 
		Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "ID ".$id." atualizada com sucesso!");

		if (array_key_exists('segunda', $dados)) $this->setMaterias($dados['segunda'], $row->id);
		return $row;
	}

	private function setMaterias($dados, $idrecursosprofessores) {
		
		if (!is_array($dados)) return;
		 
		$itens = new Educacaoespecialsalasrecursosprofessoreslist();
		$diasemana = $dados['diasemana'];
		$horario = $dados['horario'];
		$horariofim = $dados['horariofim'];
		$aluno = $dados['aluno'];
		$professor = $dados['professor'];

		$ids = array();
		foreach ($aluno as $i=>$id) {
			
			$d = array();
			
			$d['idrecursosprofessores'] = $idrecursosprofessores; 
			$d['diasemana'] =  trim(strip_tags($diasemana[$i]));
			$d['horario'] = trim(strip_tags($horario[$i]));
			$d['horariofim'] = trim(strip_tags($horariofim[$i]));
			$d['aluno'] = trim(strip_tags($aluno[$i]));       
			$d['professor'] = trim(strip_tags($professor[$i]));       
			$d['logusuario'] = $dados['logusuario'];
			$d['logdata'] = $dados['logdata'];
			
			if(!empty($d['horario']) AND !empty($d['aluno']) AND !empty($d['professor'])  AND !empty($d['horariofim']))
				$_row = $itens->save($d); 		
			array_push($ids, $_row->id);
		}
		$segunda = implode(",", $ids);
       
		if ($segunda=="") $segunda = "0";
		$strsql = "DELETE FROM educacaoespecialsalasrecursosprofessoreslist WHERE idrecursosprofessores=$idrecursosprofessores AND id NOT IN ($segunda)";
		
		$db = Zend_Registry::get('db');
		$db->query($strsql);
		
	}
	
}