<?php

/**
 * Define o modelo Supervisaoroteiros
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Supervisaoroteiros extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "supervisaoroteiros";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getSupervisaoroteirosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$supervisaoroteiros = new Supervisaoroteiros();
		return $supervisaoroteiros->getSupervisaoroteiros($queries, $page, $maxpage);
	}
	
	public function getSupervisaoroteiros($queries = array(), $page = 0, $maxpage = 0) {
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " s1.id = $id ");
		
		
		$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " s1.data >= '$data_i' ");

		$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
				if ($data_f) array_push($where, " s1.data <= '$data_f' ");

		$idescolafuncionario = (isset($queries["idescolafuncionario"])) ? $queries["idescolafuncionario"] : false;
				if ($idescolafuncionario) array_push($where, " s1.idescolafuncionario = $idescolafuncionario ");

		$secretaria = (isset($queries["secretaria"])) ? $queries["secretaria"] : false;
				if ($secretaria) array_push($where, " s1.secretaria LIKE '%$secretaria%' ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
				if ($idescola) array_push($where, " s1.idescola = $idescola ");

		$idtipoensino = (isset($queries["idtipoensino"])) ? $queries["idtipoensino"] : false;
				if ($idtipoensino) array_push($where, " s1.idtipoensino = $idtipoensino ");

		$idperiodosupervisao = (isset($queries["idperiodosupervisao"])) ? $queries["idperiodosupervisao"] : false;
				if ($idperiodosupervisao) array_push($where, " s1.idperiodosupervisao = $idperiodosupervisao ");

		$idequipesupervisao = (isset($queries["idequipesupervisao"])) ? $queries["idequipesupervisao"] : false;
		if($idequipesupervisao) array_push($where, " s1.idequipesupervisao = $idequipesupervisao");

		$idveiculo = (isset($queries["idveiculo"])) ? $queries["idveiculo"] : false;
				if ($idveiculo) array_push($where, " s1.idveiculo = $idveiculo ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " s1.status LIKE '%$status%' ");

		$equipefunc = (isset($queries['tipo_filter'])) ? $queries['tipo_filter'] : false;
		if($equipefunc=="byfunc" AND !$idescolafuncionario){
			array_push($where, " s1.tipo = 'individual'");
		}else if($equipefunc=="byequipe" AND !$idequipesupervisao){
			array_push($where, " s1.tipo = 'equipe'");
		}

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "s1.*"; 
		;
		
		if ($total) $fields = "COUNT(s1.id) as total";
		
		$ordem = "ORDER BY s1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM supervisaoroteiros s1
					
					WHERE s1.excluido='nao' 
						$w 
					$ordem	
					$limit";	

		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getSupervisaoroteiroById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSupervisaoroteiros($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSupervisaoroteiroByIdHelper($id, $queries = array()) {
		$rows = new Supervisaoroteiros();
		return $rows->getSupervisaoroteiroById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Supervisaoroteiros
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		}
		
		$row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		$row->idescolafuncionario = (array_key_exists("idescolafuncionario",$dados)) ? $dados["idescolafuncionario"] : $row->idescolafuncionario;
		$row->secretaria = (array_key_exists("secretaria",$dados)) ? $dados["secretaria"] : $row->secretaria;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->idtipoensino = (array_key_exists("idtipoensino",$dados)) ? $dados["idtipoensino"] : $row->idtipoensino;
		$row->idequipesupervisao = (array_key_exists("idequipesupervisao",$dados)) ? $dados["idequipesupervisao"] : $row->idequipesupervisao;
		$row->idperiodosupervisao = (array_key_exists("idperiodosupervisao",$dados)) ? $dados["idperiodosupervisao"] : $row->idperiodosupervisao;
		$row->idveiculo = (array_key_exists("idveiculo",$dados)) ? $dados["idveiculo"] : $row->idveiculo;
		$row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
		$row->idsescolas = (array_key_exists("idsescolas",$dados)) ? $dados["idsescolas"] : $row->idsescolas;
		$row->periodo = (array_key_exists("periodo",$dados)) ? $dados["periodo"] : $row->periodo;

		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
				
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}