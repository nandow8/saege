<?php

class Protocolosprocessostramitacoesarquivos extends Zend_Db_Table_Abstract {

	/**
	 * Define o nome da tabela
	 * @var string
	 */
	protected $_name = "protocolosprocessostramitacoesarquivos";
	
	/**
	 * Define a chave primaria
	 * @var integer
	 */
	protected $_primary = "id";
	
	
	public static function getArquivosByIdProtocolosprocessostramitacoes($idprotocolosprocessostramitacoes, $order = 'pa1.ordem') {
		
		$strsql = "SELECT pa1.idarquivo, pa1.idprotocolosprocessostramitacoes, pa1.legenda, ar1.filename, ar1.size 
					FROM protocolosprocessostramitacoesarquivos pa1 
						LEFT JOIN arquivos ar1 ON ar1.id=pa1.idarquivo
					WHERE pa1.excluido='nao' 
						AND pa1.idprotocolosprocessostramitacoes=$idprotocolosprocessostramitacoes 
					ORDER BY $order"; 
						
		$db = Zend_Registry::get('db');
		return $db->fetchAll($strsql);
	}
	
	public function setArquivos($idprotocolosprocessostramitacoes, $arquivos, $legendas) {
                
		if (!is_array($arquivos)) return;
                
		$logdata = date('Y-m-d G:i:s');
		foreach ($arquivos as $i=>$idarquivo) {
			$d = array();
			
			$legenda = $legendas[$i];
			
			$d['idprotocolosprocessostramitacoes'] = $idprotocolosprocessostramitacoes;
			$d['idarquivo'] = $idarquivo;
			$d['legenda'] = $legenda;
			$d['ordem'] = $i;
			$d['excluido'] = 'nao';
			$d['logusuario'] = Usuarios::getUsuario('id');
			$d['logdata'] = $logdata;
			$this->save($d);
		}
		
		$ids = implode(",", $arquivos);

                if ($ids=="") $ids = "0";
                if ($idprotocolosprocessostramitacoes == "") $idprotocolosprocessostramitacoes = '0';
                
                $strsql = "DELETE FROM protocolosprocessostramitacoesarquivos WHERE idprotocolosprocessostramitacoes = $idprotocolosprocessostramitacoes AND idarquivo NOT IN ($ids)";

		$db = Zend_Registry::get('db');
		$db->query($strsql);		
	}	

	public static function getProtocolosprocessostramitacoesArquivos($idprotocolosprocessostramitacoes = NULL, $status = NULL, $inverte = false, $limite = NULL) {
		$db = Zend_Registry::get('db');
		
		$w = "";
		if (!is_null($idprotocolosprocessostramitacoes)) $w .= " AND pa1.idprotocolosprocessostramitacoes = " . (int)$idprotocolosprocessostramitacoes;
		//if (!is_null($status)) $w .= " AND pa1.status = '$status'"; 
		
		$l = "";
		if (!is_null($limite)) $l .= " LIMIT '$limite'";
		
		$order = "ORDER BY pa1.ordem";
		if ($inverte) $order .= " DESC";
		
		$strsql = "SELECT pa1.* 
					FROM protocolosprocessostramitacoesarquivos pa1 
					WHERE pa1.excluido = 'nao' 
					$w
					$order
					$l";
					
		return $db->fetchAll($strsql);
	}
	
	/**
	 * Salva o dados (INSERT OU UPDATE)
	 * @param array dados
	 * @return Arquivos
	 */
	public function save($dados) {
		$novoRegistro = true;
		die('Arquivios');
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$idprotocolosprocessostramitacoes = (int)$dados['idprotocolosprocessostramitacoes'];
		$idarquivo = (int)$dados['idarquivo'];
		
		$row = $this->fetchRow("idprotocolosprocessostramitacoes=" . $idprotocolosprocessostramitacoes . " AND idarquivo=" . $idarquivo);		
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
				
		if ($id>0)$row->id = $id;
		
		$row->idprotocolosprocessostramitacoes = (isset($dados['idprotocolosprocessostramitacoes'])) ? $dados['idprotocolosprocessostramitacoes'] : $row->idprotocolosprocessostramitacoes;
		$row->idarquivo = (isset($dados['idarquivo'])) ? $dados['idarquivo'] : $row->idarquivo;
		$row->legenda = (isset($dados['legenda'])) ? $dados['legenda'] : $row->legenda;
		$row->ordem = (isset($dados['ordem'])) ? $dados['ordem'] : $row->ordem;
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (isset($dados['logusuario'])) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (isset($dados['logdata'])) ? $dados['logdata'] : $row->logdata;
		$row->save();
		
		return $row;
	}
        
        
	public function getProtocoloprocessotramitacaoarquivoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getProtocolosprocessostramitacoesArquivos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
}