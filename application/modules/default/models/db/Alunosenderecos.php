<?php
class Alunosenderecos extends Zend_Db_Table_Abstract {
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasalunos_enderecos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";	

	public static function getEnderecosHelper($queries = array(), $page = 0, $maxpage = 0) {
        $atas = new Alunosenderecos();
        return $atas->getEnderecos($queries, $page, $maxpage);
    }

    public function getEnderecos($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " ed1.id = $id ");


        $idaluno = (isset($queries["idaluno"])) ? $queries["idaluno"] : false;
        if ($idaluno)
            array_push($where, " ed1.idaluno = $idaluno ");

        $cep = (isset($queries["cep"])) ? $queries["cep"] : false;
        if ($cep)
            array_push($where, " ed1.cep >= '$cep' ");

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "ed1.*";
        ;

        if ($total)
            $fields = "COUNT(ed1.id) as total";

        $ordem = "ORDER BY ed1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM escolasalunos_enderecos ed1
					
					WHERE ed1.excluido='nao' 
						$w 
					$ordem	
					$limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEnderecoById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getEnderecos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

     public static function getEnderecoByIdHelper($id, $queries = array()) {
        $rows = new Alunosenderecos();
        return $rows->getEnderecoById($id, $queries);
    }

    public function getEnderecoByIdAluno($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['idaluno'] = $id;
        $rows = $this->getEnderecos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getEnderecoByIdAlunoHelper($id, $queries = array()) {
        $rows = new Alunosenderecos();
        return $rows->getEnderecoByIdAluno($id, $queries);
    }

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Usuarios
     */
	public function save($dados) {
		
		$novoRegistro = true;
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {			
			$novoRegistro = false;
		}

		if ($id>0)$row->id = $id;
		$row->inCep = (isset($dados['inCep'])) ? $dados['inCep'] : $row->inCep;
		$row->inLogradouro = (isset($dados['inLogradouro'])) ? $dados['inLogradouro'] : $row->inLogradouro;
		$row->inNumero = (isset($dados['inNumero'])) ? $dados['inNumero'] : $row->inNumero;
		$row->inComplemento = (isset($dados['inComplemento'])) ? $dados['inComplemento'] : $row->inComplemento;
		$row->inBairro = (isset($dados['inBairro'])) ? $dados['inBairro'] : $row->inBairro;
		$row->inUF = (isset($dados['inUF'])) ? $dados['inUF'] : $row->inUF;
		$row->inCidade = (isset($dados['inCidade'])) ? $dados['inCidade'] : $row->inCidade;
		$row->inTipoLogradouro = (isset($dados['inTipoLogradouro'])) ? $dados['inTipoLogradouro'] : $row->inTipoLogradouro;
		
		$row->inDDD = (isset($dados['inDDD'])) ? $dados['inDDD'] : $row->inDDD;
		$row->inFoneResidencial = (isset($dados['inFoneResidencial'])) ? $dados['inFoneResidencial'] : $row->inFoneResidencial;
		$row->inFoneRecados = (isset($dados['inFoneRecados'])) ? $dados['inFoneRecados'] : $row->inFoneRecados ;
		$row->inNomeFoneRecado = (isset($dados['inNomeFoneRecado'])) ? $dados['inNomeFoneRecado'] : $row->inNomeFoneRecado;
		$row->inSMS = (isset($dados['inSMS'])) ? $dados['inSMS'] : $row->inSMS;
		$row->inDDDCel = (isset($dados['inDDDCel'])) ? $dados['inDDDCel'] : $row->inDDDCel;
		$row->inFoneCel = (isset($dados['inFoneCel'])) ? $dados['inFoneCel'] : $row->inFoneCel;
		
		$row->inLatitude = (isset($dados['inLatitude'])) ? $dados['inLatitude'] : $row->inLatitude;
		$row->inLongitude = (isset($dados['inLongitude'])) ? $dados['inLongitude'] : $row->inLongitude;
		
		$row->endIndicativoInBairro = (isset($dados['endIndicativoInBairro'])) ? $dados['endIndicativoInBairro'] : $row->endIndicativoInBairro;

		$row->endIndicativoinCidade = (isset($dados['endIndicativoinCidade'])) ? $dados['endIndicativoinCidade'] : $row->endIndicativoinCidade;

		$row->endIndicativoinLatitude = (isset($dados['endIndicativoinLatitude'])) ? $dados['endIndicativoinLatitude'] : $row->endIndicativoinLatitude;

		$row->endIndicativoinLongitude = (isset($dados['endIndicativoinLongitude'])) ? $dados['endIndicativoinLongitude'] : $row->endIndicativoinLongitude;

		$row->endIndicativoinLogradouro = (isset($dados['endIndicativoinLogradouro'])) ? $dados['endIndicativoinLogradouro'] : $row->endIndicativoinLogradouro;

		$row->endIndicativoinNumero = (isset($dados['endIndicativoinNumero'])) ? $dados['endIndicativoinNumero'] : $row->endIndicativoinNumero;

		$row->endIndicativoinUF = (isset($dados['endIndicativoinUF'])) ? $dados['endIndicativoinUF'] : $row->endIndicativoinUF;
		
		$row->idaluno = (isset($dados['idaluno'])) ? $dados['idaluno'] : $row->idaluno;
		$row->datacriacao = (isset($dados['datacriacao'])) ? $dados['datacriacao'] : $row->datacriacao;
		$row->logusuario = (isset($dados['logusuario'])) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (isset($dados['logdata'])) ? $dados['logdata'] : $row->logdata;
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;

                $row->soapstatus = (array_key_exists("soapstatus", $dados)) ? $dados["soapstatus"] : $row->soapstatus;
                $row->soaptipo = (array_key_exists("soaptipo", $dados)) ? $dados["soaptipo"] : $row->soaptipo;
                $row->soapretorno = (array_key_exists("soapretorno", $dados)) ? $dados["soapretorno"] : $row->soapretorno;
		
		$row->save();
		
		return $row;
	}
}