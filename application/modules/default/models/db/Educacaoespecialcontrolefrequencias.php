<?php

/**
 * Define o modelo Educacaoespecialcontrolefrequencias
 *
 * @author		Fernando Santos Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 FJMX
 * @version     1.0
 */
class Educacaoespecialcontrolefrequencias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "educacaoespecialcontrolefrequencias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEducacaoespecialcontrolefrequenciasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$educacaoespecialcontrolefrequencias = new Educacaoespecialcontrolefrequencias();
		return $educacaoespecialcontrolefrequencias->getEducacaoespecialcontrolefrequencias($queries, $page, $maxpage);
	}
	
	public function getEducacaoespecialcontrolefrequencias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");

		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		if ($idaluno) array_push($where, " e1.idaluno = $idaluno ");
		
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");
		
		// $data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
        // if ($data_i) array_push($where, " e1.datacriacao >= '$data_i' ");
		
        // $data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		// if ($data_f) array_push($where, " e1.datacriacao <= '$data_f' "); 
		
		$dataAno = (isset($queries['dataAno'])) ? (int)$queries['dataAno'] : false;
		if ($dataAno) array_push($where, "  e1.dataAno  = $dataAno ");
		
		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM educacaoespecialcontrolefrequencias e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		 
		return $db->fetchAll($strsql);			
	}	
	
	public function getEducacaoespecialcontrolefrequenciasById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEducacaoespecialcontrolefrequencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEducacaoespecialcontrolefrequenciasByIdHelper($id, $queries = array()) {
		$rows = new Educacaoespecialcontrolefrequencias();
		return $rows->getEducacaoespecialcontrolefrequenciasById($id, $queries);
	}	
	
	public function getEducacaoespecialcontrolefrequenciasByIdAluno($id) { 
		$queries['idaluno'] = $id;
		$rows = $this->getEducacaoespecialcontrolefrequencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEducacaoespecialcontrolefrequenciasByIdAlunoHelper($id) {
		$rows = new Educacaoespecialcontrolefrequencias();
		return $rows->getEducacaoespecialcontrolefrequenciasByIdAluno($id);
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Educacaoespecialcontrolefrequencias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno; 
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola; 
		$row->dataAno = (array_key_exists("dataAno",$dados)) ? $dados["dataAno"] : $row->dataAno; 
		$row->dialetivo = (array_key_exists("dialetivo",$dados)) ? $dados["dialetivo"] : $row->dialetivo;  
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
							
		$row->save();
		
		return $row;
	}
	
}