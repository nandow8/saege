<?php

/**
 * Define o modelo Patrimonioscomprasbens
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Patrimonioscomprasbens extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "patrimonioscomprasbens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getPatrimonioscomprasbensHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$patrimonioscomprasbens = new Patrimonioscomprasbens();
		return $patrimonioscomprasbens->getPatrimonioscomprasbens($queries, $page, $maxpage);
	}
	
	public function getPatrimonioscomprasbens($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " p1.sequencial LIKE '%$sequencial%' ");

		$nf = (isset($queries["nf"])) ? $queries["nf"] : false;
		if ($nf) array_push($where, " p1.nf = $nf ");

		$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " p1.datalancamento >= '$datalancamento_i' ");

		$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " p1.datalancamento <= '$datalancamento_f' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='sequencial') $sorting[0]='p1.sequencial';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*";
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM patrimonioscomprasbens p1
					
					WHERE p1.excluido='nao' 
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPatrimoniocomprabemById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPatrimonioscomprasbens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPatrimoniocomprabemByIdHelper($id, $queries = array()) {
		$rows = new Patrimonioscomprasbens();
		return $rows->getPatrimonioscomprabemById($id, $queries);
	}

	public function getUltimoPatrimoniocomprasbens($queries = array()) {
		$queries['order'] = 'ORDER BY p1.id DESC';
		$rows = $this->getPatrimonioscomprasbens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}		
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Patrimonioscomprasbens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->sequencial = (array_key_exists("sequencial", $dados) ? $dados["sequencial"] : $row->sequencial);
		$row->datalancamento = (array_key_exists("datalancamento", $dados) ? $dados["datalancamento"] : $row->datalancamento);
		$row->horaabertura = (array_key_exists("horaabertura", $dados) ? $dados["horaabertura"] : $row->horaabertura);
		$row->idusuariologado = (array_key_exists("idusuariologado", $dados) ? $dados["idusuariologado"] : $row->idusuariologado);
		$row->doacao = (array_key_exists("doacao",$dados)) ? $dados["doacao"] : $row->doacao;
		$row->nomedoador = (array_key_exists("nomedoador",$dados)) ? $dados["nomedoador"] : $row->nomedoador;
		$row->iddocumento = (array_key_exists("iddocumento",$dados)) ? $dados["iddocumento"] : $row->iddocumento;
		$row->nprocessocompra = (array_key_exists("nprocessocompra",$dados)) ? $dados["nprocessocompra"] : $row->nprocessocompra;
		$row->nlicitacao = (array_key_exists("nlicitacao",$dados)) ? $dados["nlicitacao"] : $row->nlicitacao;
		$row->nempenho = (array_key_exists("nempenho",$dados)) ? $dados["nempenho"] : $row->nempenho;
		$row->dataemissao = (array_key_exists("dataemissao",$dados)) ? $dados["dataemissao"] : $row->dataemissao;
		$row->nf = (array_key_exists("nf",$dados)) ? $dados["nf"] : $row->nf;
		$row->serie = (array_key_exists("serie",$dados)) ? $dados["serie"] : $row->serie;
		$row->danfe = (array_key_exists("danfe",$dados)) ? $dados["danfe"] : $row->danfe;
		$row->idfornecedor = (array_key_exists("idfornecedor",$dados)) ? $dados["idfornecedor"] : $row->idfornecedor;
		$row->itemnf = (array_key_exists("itemnf",$dados)) ? $dados["itemnf"] : $row->itemnf;
		$row->quantidade = (array_key_exists("quantidade",$dados)) ? $dados["quantidade"] : $row->quantidade;
		$row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
		$row->valorunitario = (array_key_exists("valorunitario",$dados)) ? $dados["valorunitario"] : $row->valorunitario;
		$row->valortotal = (array_key_exists("valortotal",$dados)) ? $dados["valortotal"] : $row->valortotal;
		$row->npatrimonioinicial = (array_key_exists("npatrimonioinicial",$dados)) ? $dados["npatrimonioinicial"] : $row->npatrimonioinicial;
		$row->codbarras = (array_key_exists("codbarras",$dados)) ? $dados["codbarras"] : $row->codbarras;
 
 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
				
		$row->save();
		
		return $row;
	}
}