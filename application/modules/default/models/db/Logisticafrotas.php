<?php

/**
 * Define o modelo Logisticafrotas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Logisticafrotas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "logisticafrotas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLogisticafrotasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$logisticafrotas = new Logisticafrotas();
		return $logisticafrotas->getLogisticafrotas($queries, $page, $maxpage);
	}
	
	public function getLogisticafrotas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
	
		$notids = (isset($queries['notids'])) ? $queries['notids'] : false;
		if ($notids) array_push($where, " NOT FIND_IN_SET (l1.id, '$notids')");
		
		$idfabricante = (isset($queries["idfabricante"])) ? $queries["idfabricante"] : false;
		if ($idfabricante) array_push($where, " l1.idfabricante = $idfabricante ");

$idmarca = (isset($queries["idmarca"])) ? $queries["idmarca"] : false;
		if ($idmarca) array_push($where, " l1.idmarca = $idmarca ");

$idmodelo = (isset($queries["idmodelo"])) ? $queries["idmodelo"] : false;
		if ($idmodelo) array_push($where, " l1.idmodelo = $idmodelo ");

$anomodelo = (isset($queries["anomodelo"])) ? $queries["anomodelo"] : false;
		if ($anomodelo) array_push($where, " l1.anomodelo = '$anomodelo' ");

$combustivel = (isset($queries["combustivel"])) ? $queries["combustivel"] : false;
		if ($combustivel) array_push($where, " l1.combustivel LIKE '%$combustivel%' ");

$renavam = (isset($queries["renavam"])) ? $queries["renavam"] : false;
		if ($renavam) array_push($where, " l1.renavam LIKE '%$renavam%' ");

$chassi = (isset($queries["chassi"])) ? $queries["chassi"] : false;
		if ($chassi) array_push($where, " l1.chassi LIKE '%$chassi%' ");

$placa = (isset($queries["placa"])) ? $queries["placa"] : false;
		if ($placa) array_push($where, " l1.placa LIKE '%$placa%' ");

$cor = (isset($queries["cor"])) ? $queries["cor"] : false;
		if ($cor) array_push($where, " l1.cor LIKE '%$cor%' ");

$capacidadepotencia = (isset($queries["capacidadepotencia"])) ? $queries["capacidadepotencia"] : false;
		if ($capacidadepotencia) array_push($where, " l1.capacidadepotencia LIKE '%$capacidadepotencia%' ");

$idproprietario = (isset($queries["idproprietario"])) ? $queries["idproprietario"] : false;
		if ($idproprietario) array_push($where, " l1.idproprietario = $idproprietario ");

$especie = (isset($queries["especie"])) ? $queries["especie"] : false;
		if ($especie) array_push($where, " l1.especie LIKE '%$especie%' ");

$categoria = (isset($queries["categoria"])) ? $queries["categoria"] : false;
		if ($categoria) array_push($where, " l1.categoria LIKE '%$categoria%' ");

$veiculo = (isset($queries["veiculo"])) ? $queries["veiculo"] : false;
		if ($veiculo) array_push($where, " l1.veiculo LIKE '%$veiculo%' ");

$anofabricacao = (isset($queries["anofabricacao"])) ? $queries["anofabricacao"] : false;
		if ($anofabricacao) array_push($where, " l1.anofabricacao = '$anofabricacao' ");

$emprestimo = (isset($queries["emprestimo"])) ? $queries["emprestimo"] : false;
		if ($emprestimo) array_push($where, " l1.emprestimo LIKE '%$emprestimo%' ");

$veiculopublico = (isset($queries["veiculopublico"])) ? $queries["veiculopublico"] : false;
		if ($veiculopublico) array_push($where, " l1.veiculopublico LIKE '%$veiculopublico%' ");

$quantidade = (isset($queries["quantidade"])) ? $queries["quantidade"] : false;
		if ($quantidade) array_push($where, " l1.quantidade = '$quantidade' ");

$escolar = (isset($queries["escolar"])) ? $queries["escolar"] : false;
		if ($escolar) array_push($where, " l1.escolar LIKE '%$escolar%' ");

$tipoveiculo = (isset($queries["tipoveiculo"])) ? $queries["tipoveiculo"] : false;
		if ($tipoveiculo) array_push($where, " l1.tipoveiculo LIKE '%$tipoveiculo%' ");

$veiculodisponiveldepartamento = (isset($queries["veiculodisponiveldepartamento"])) ? $queries["veiculodisponiveldepartamento"] : false;
		if ($veiculodisponiveldepartamento) array_push($where, " l1.veiculodisponiveldepartamento LIKE '%$veiculodisponiveldepartamento%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*, m1.marca, f1.fabricante, lm1.modelo "; 
		
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";


		$strsql = "SELECT $fields 
					FROM logisticafrotas l1
						 LEFT JOIN logisticamarcas m1 ON m1.id = l1.idmarca 
						 LEFT JOIN logisticafabricantes f1 ON f1.id = l1.idfabricante
						 LEFT JOIN logisticamodelos lm1 ON l1.idmodelo = lm1.id 
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLogisticafrotaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLogisticafrotas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLogisticafrotaByIdHelper($id, $queries = array()) {
		$rows = new Logisticafrotas();
		return $rows->getLogisticafrotaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Logisticafrotas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfabricante = (array_key_exists("idfabricante",$dados)) ? $dados["idfabricante"] : $row->idfabricante;
 $row->idmarca = (array_key_exists("idmarca",$dados)) ? $dados["idmarca"] : $row->idmarca;
 $row->idmodelo = (array_key_exists("idmodelo",$dados)) ? $dados["idmodelo"] : $row->idmodelo;
 $row->idvistoriaescolar = (array_key_exists("idvistoriaescolar",$dados)) ? $dados["idvistoriaescolar"] : $row->idvistoriaescolar;
 $row->vencimentovistoriaescolar = (array_key_exists("vencimentovistoriaescolar",$dados)) ? $dados["vencimentovistoriaescolar"] : $row->vencimentovistoriaescolar;
 $row->deficiente = (array_key_exists("deficiente",$dados)) ? $dados["deficiente"] : $row->deficiente;
 $row->idvistoriadeficiente = (array_key_exists("idvistoriadeficiente",$dados)) ? $dados["idvistoriadeficiente"] : $row->idvistoriadeficiente;
 $row->vencimentovistoriadeficiente = (array_key_exists("vencimentovistoriadeficiente",$dados)) ? $dados["vencimentovistoriadeficiente"] : $row->vencimentovistoriadeficiente;
 $row->idlaudoescolar = (array_key_exists("idlaudoescolar",$dados)) ? $dados["idlaudoescolar"] : $row->idlaudoescolar;
 $row->idtipo = (array_key_exists("idtipo",$dados)) ? $dados["idtipo"] : $row->idtipo;
 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
 $row->tipoveiculo = (array_key_exists("tipoveiculo",$dados)) ? $dados["tipoveiculo"] : $row->tipoveiculo;
 $row->anomodelo = (array_key_exists("anomodelo",$dados)) ? $dados["anomodelo"] : $row->anomodelo;
 $row->patrimonio = (array_key_exists("patrimonio",$dados)) ? $dados["patrimonio"] : $row->patrimonio;
 $row->situacaoveiculo = (array_key_exists("situacaoveiculo",$dados)) ? $dados["situacaoveiculo"] : $row->situacaoveiculo;
 $row->logisticafrotascol = (array_key_exists("logisticafrotascol",$dados)) ? $dados["logisticafrotascol"] : $row->logisticafrotascol;
 $row->kminicial = (array_key_exists("kminicial",$dados)) ? $dados["kminicial"] : $row->kminicial;
 $row->combustivel = (array_key_exists("combustivel",$dados)) ? $dados["combustivel"] : $row->combustivel;
 $row->renavam = (array_key_exists("renavam",$dados)) ? $dados["renavam"] : $row->renavam;
 $row->chassi = (array_key_exists("chassi",$dados)) ? $dados["chassi"] : $row->chassi;
 $row->placa = (array_key_exists("placa",$dados)) ? $dados["placa"] : $row->placa;
 $row->cor = (array_key_exists("cor",$dados)) ? $dados["cor"] : $row->cor;
 $row->capacidadepotencia = (array_key_exists("capacidadepotencia",$dados)) ? $dados["capacidadepotencia"] : $row->capacidadepotencia;
 $row->idproprietario = (array_key_exists("idproprietario",$dados)) ? $dados["idproprietario"] : $row->idproprietario;
 $row->especie = (array_key_exists("especie",$dados)) ? $dados["especie"] : $row->especie;
 $row->categoria = (array_key_exists("categoria",$dados)) ? $dados["categoria"] : $row->categoria;
 $row->veiculo = (array_key_exists("veiculo",$dados)) ? $dados["veiculo"] : $row->veiculo;
 $row->anofabricacao = (array_key_exists("anofabricacao",$dados)) ? $dados["anofabricacao"] : $row->anofabricacao;
 $row->emprestimo = (array_key_exists("emprestimo",$dados)) ? $dados["emprestimo"] : $row->emprestimo;
 $row->veiculopublico = (array_key_exists("veiculopublico",$dados)) ? $dados["veiculopublico"] : $row->veiculopublico;
 $row->quantidade = (array_key_exists("quantidade",$dados)) ? $dados["quantidade"] : $row->quantidade;
 $row->reservadepartamentos = (array_key_exists("reservadepartamentos",$dados)) ? $dados["reservadepartamentos"] : $row->reservadepartamentos;
 $row->escolar = (array_key_exists("escolar",$dados)) ? $dados["escolar"] : $row->escolar;
 $row->veiculodisponiveldepartamento = (array_key_exists("veiculodisponiveldepartamento",$dados)) ? $dados["veiculodisponiveldepartamento"] : $row->veiculodisponiveldepartamento;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
 			
		$row->save();
		
		return $row;
	}
	
}