<?php

/**
 * Define o modelo Protocolos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Protocolos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "protocolos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getProtocolosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$protocolos = new Protocolos();
		return $protocolos->getProtocolos($queries, $page, $maxpage);
	}
	
	public function getProtocolos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		if ($idsecretaria) array_push($where, " p1.idsecretaria=$idsecretaria ");

		$idusuariocriacao = (isset($queries["idusuariocriacao"])) ? $queries["idusuariocriacao"] : false;
		if ($idusuariocriacao) array_push($where, " p1.idusuariocriacao = $idusuariocriacao ");

$iddepartamentosecretaria = (isset($queries["iddepartamentosecretaria"])) ? $queries["iddepartamentosecretaria"] : false;
		if ($iddepartamentosecretaria) array_push($where, " p1.iddepartamentosecretaria = $iddepartamentosecretaria ");

$idusuarioresponsavel = (isset($queries["idusuarioresponsavel"])) ? $queries["idusuarioresponsavel"] : false;
		if ($idusuarioresponsavel) array_push($where, " p1.idusuarioresponsavel = $idusuarioresponsavel ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " p1.idescola = $idescola ");

$iddepartamentoescola = (isset($queries["iddepartamentoescola"])) ? $queries["iddepartamentoescola"] : false;
		if ($iddepartamentoescola) array_push($where, " p1.iddepartamentoescola = $iddepartamentoescola ");

$numeroprotocolo = (isset($queries["numeroprotocolo"])) ? $queries["numeroprotocolo"] : false;
		if ($numeroprotocolo) array_push($where, " p1.numeroprotocolo LIKE '%$numeroprotocolo%' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " p1.titulo LIKE '%$titulo%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " p1.descricoes = '$descricoes' ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " p1.origem LIKE '%$origem%' ");

$destinatario = (isset($queries["destinatario"])) ? $queries["destinatario"] : false;
		if ($destinatario) array_push($where, " p1.destinatario LIKE '%$destinatario%' ");

$origem_destino = (isset($queries["origem_destino"])) ? $queries["origem_destino"] : false;
		if ($origem_destino) array_push($where, " (p1.origem LIKE '%$origem_destino%' OR p1.destinatario LIKE '%$origem_destino%') ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status LIKE '%$status%' ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " p1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " p1.data <= '$data_f' ");

		$modulo = (isset($queries["modulo"])) ? $queries["modulo"] : false;
		if ($modulo) array_push($where, " p1.modulo LIKE '%$modulo%' ");

$meusprotocolos = (isset($queries["meusprotocolos"])) ? $queries["meusprotocolos"] : false;
$meusprotocolos_idusuariocriacao = (isset($queries["meusprotocolos_idusuariocriacao"])) ? $queries["meusprotocolos_idusuariocriacao"] : false;
$meusprotocolos_iddepartamentosecretaria = (isset($queries["meusprotocolos_iddepartamentosecretaria"])) ? $queries["meusprotocolos_iddepartamentosecretaria"] : false;
$meusprotocolos_idusuarioresponsavel = (isset($queries["meusprotocolos_idusuarioresponsavel"])) ? $queries["meusprotocolos_idusuarioresponsavel"] : false;
		if (($meusprotocolos) && ($meusprotocolos_idusuariocriacao) && ($meusprotocolos_iddepartamentosecretaria) && ($meusprotocolos_idusuarioresponsavel)) {
			array_push($where, " (p1.idusuariocriacao = $meusprotocolos_idusuariocriacao OR p1.iddepartamentosecretaria = $meusprotocolos_iddepartamentosecretaria OR p1.idusuarioresponsavel = $meusprotocolos_idusuarioresponsavel) ");
		}





		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM protocolos p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getProtocoloById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getProtocolos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getProtocoloByIdHelper($id, $queries = array()) {
		$rows = new Protocolos();
		return $rows->getProtocoloById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Protocolos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 

		$row->idsecretaria = (array_key_exists('idsecretaria',$dados)) ? $dados['idsecretaria'] : $row->idsecretaria;
		 $row->idusuariocriacao = (array_key_exists("idusuariocriacao",$dados)) ? $dados["idusuariocriacao"] : $row->idusuariocriacao;
		$row->iddepartamentosecretaria = (array_key_exists("iddepartamentosecretaria",$dados)) ? $dados["iddepartamentosecretaria"] : $row->iddepartamentosecretaria;
		$row->idusuarioresponsavel = (array_key_exists("idusuarioresponsavel",$dados)) ? $dados["idusuarioresponsavel"] : $row->idusuarioresponsavel;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->iddepartamentoescola = (array_key_exists("iddepartamentoescola",$dados)) ? $dados["iddepartamentoescola"] : $row->iddepartamentoescola;
		$row->idescolausuariocriacao = (array_key_exists("idescolausuariocriacao",$dados)) ? $dados["idescolausuariocriacao"] : $row->idescolausuariocriacao;
		$row->idescolausuarioresponsavel = (array_key_exists("idescolausuarioresponsavel",$dados)) ? $dados["idescolausuarioresponsavel"] : $row->idescolausuarioresponsavel;
		$row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
		$row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
		$row->numeroprotocolo = (array_key_exists("numeroprotocolo",$dados)) ? $dados["numeroprotocolo"] : $row->numeroprotocolo;
 		
 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
		$row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
		$row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
		$row->destinatario = (array_key_exists("destinatario",$dados)) ? $dados["destinatario"] : $row->destinatario;

		$row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
		$row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
		$row->rg = (array_key_exists("rg",$dados)) ? $dados["rg"] : $row->rg;

		$row->dataagendamento = (array_key_exists("dataagendamento",$dados)) ? $dados["dataagendamento"] : $row->dataagendamento;
		$row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
		$row->prioridade = (array_key_exists("prioridade",$dados)) ? $dados["prioridade"] : $row->prioridade;
$row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;

$row->diretora = (array_key_exists("diretora",$dados)) ? $dados["diretora"] : $row->diretora;
$row->coordenador = (array_key_exists("coordenador",$dados)) ? $dados["coordenador"] : $row->coordenador;


		$row->modulo = (array_key_exists("modulo",$dados)) ? $dados["modulo"] : $row->modulo;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		if($dados['id']<=0) $row->save();
		if($row->excluido=="sim") $row->save();
		if (!$row->numeroprotocolo) { 			
			$row->numeroprotocolo = "PROTOC" . str_pad($row->id, 4, "0", STR_PAD_LEFT);
			$row->save();

			//$this->setItens($row);
		}
		$this->setItens($row);

		return $row;
	}
	
	private function setItens($row_protocolo) {
		$protocolos = new Protocolos;
		$protocolo = $protocolos->getProtocoloById($row_protocolo['id']);

		$solicitacoes = new Solicitacoes();
		$ultimasolicitacao = $solicitacoes->getUltimaSolicitacaoByIdprotocolo($row_protocolo['id']);


		$dados = array();
		$dados['idprotocolo'] = $row_protocolo['id'];
		$dados['idsecretaria'] = $row_protocolo['idsecretaria'];
		$dados["idusuariocriacao"] = $row_protocolo['idusuariocriacao'];
		$dados["iddepartamentosecretaria"] = $row_protocolo['iddepartamentosecretaria'];
		$dados["idusuarioresponsavel"] = $row_protocolo['idusuarioresponsavel'];
		$dados["idescola"] = $row_protocolo['idescola'];
		$dados["idescolausuariocriacao"] = $row_protocolo['idescolausuariocriacao'];
		$dados["idescolausuarioresponsavel"] = $row_protocolo['idescolausuarioresponsavel'];
		$dados["idaluno"] = $row_protocolo['idaluno'];
		$dados["titulo"] = $row_protocolo['titulo'];
		$dados["descricoes"] = $row_protocolo['descricoes'];
		$dados["origem"] = "Secretaria";
		$dados["destinatario"] = $row_protocolo['destinatario'];
		$dados["observacoes"] = $row_protocolo['observacoes'];

		$dados["dataagendamento"] = $row_protocolo['dataagendamento'];
		$dados["tipo"] = $row_protocolo['tipo'];
		$dados["prioridade"] = $row_protocolo['prioridade'];
		


		$dados["status"] = $row_protocolo['status'];
		$dados['excluido'] = $row_protocolo['excluido'];
		$dados['logusuario'] = $row_protocolo['logusuario'];
		$dados['logdata'] = $row_protocolo['logdata'];
		//echo 'itemm 1';
		if((isset($row_protocolo['id'])) && ($row_protocolo['id']>0)){
			$alteracao = false;
			//echo 'itemm 2';
			
			//var_dump($ultimasolicitacao); 
			//var_dump($row_protocolo); 
			if(
				($ultimasolicitacao['iddepartamentosecretaria']!=$row_protocolo['iddepartamentosecretaria']) || 
				($ultimasolicitacao['idusuarioresponsavel']!=$row_protocolo['idusuarioresponsavel']) || 
				($ultimasolicitacao['idusuarioresponsavel']!=$row_protocolo['idusuarioresponsavel']) || 
				($ultimasolicitacao['idescola']!=$row_protocolo['idescola']) || 
				($ultimasolicitacao['idescolausuarioresponsavel']!=$row_protocolo['idescolausuarioresponsavel']) || 
				($ultimasolicitacao['idaluno']!=$row_protocolo['idaluno']) || 
				($ultimasolicitacao['titulo']!=$row_protocolo['titulo']) || 
				($ultimasolicitacao['descricoes']!=$row_protocolo['descricoes']) || 
				($ultimasolicitacao['observacoes']!=$row_protocolo['observacoes']) || 
				($ultimasolicitacao['dataagendamento']!=$row_protocolo['dataagendamento']) || 
				($ultimasolicitacao['tipo']!=$row_protocolo['tipo']) || 
				($ultimasolicitacao['prioridade']!=$row_protocolo['prioridade']) || 
				($ultimasolicitacao['status']!=$row_protocolo['status'])){
					
					if($row_protocolo['excluido']!="sim") $solicitacoes->save($dados);
			}elseif (!$ultimasolicitacao) {
			//	echo 'itemm 4';
				if($row_protocolo['excluido']!="sim") $solicitacoes->save($dados);
			}
		}

		
		//die();
		
	}	
}