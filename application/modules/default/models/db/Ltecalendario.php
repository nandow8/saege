<?php

class Ltecalendario extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "ltecalendario";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";     
    
    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");

        if (!$usuario) return "RGM ou senha inválidos!";
        if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
        if ($usuario['senha']!=$senha) return "A senha é inválida!";
        
        $usuario = $usuario->toArray();

        if (!$ns) $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);
        
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);
        
        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';
        
        $loginNameSpace->alunousuario = serialize($usuario);

        return true;        
    }

    public function getEventos($queries = array()) {
        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $start = (isset($queries['start'])) ? $queries['start'] : false;
        $end = (isset($queries['end'])) ? $queries['end'] : false;
        $municipal = (isset($queries['municipal'])) ? $queries['municipal'] : false;
        $estadual = (isset($queries['estadual'])) ? $queries['estadual'] : false;

        $total  = (isset($queries['total'])) ? $queries['total'] : false;

        $where = array();

        if ($id) array_push($where, " lc.id = $id ");
        if ($start && $end) array_push($where, " (date(lc.start) between '$start' and '$end' OR date(lc.end) between '$start' and '$end') ");
        if ($municipal) array_push($where, " lc.municipal = 'sim' ");
        if ($estadual) array_push($where, " lc.estadual = 'sim' ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lc.*, date(lc.start) dt_start, date(lc.end) dt_end";

        if ($total) {
            $strsql = "SELECT count(id) as total FROM ltecalendario lc WHERE lc.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM ltecalendario lc WHERE lc.excluido = 'nao' $w";
        }

        // var_dump($strsql); die();

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db');

        if ($total) {
            $row = $db->fetchRow($strsql);
            die ($row['total']);
        }

        // var_dump($strsql); die();

        return $db->fetchAll($strsql);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ltecalendario
     */
    public function save($dados) {

        $novoRegistro = true;
        
        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) { 
            $row = $this->createRow();  
        }else {
        }

        $row->title = (array_key_exists('title',$dados)) ? $dados['title'] : $row->title;
        $row->color = (array_key_exists('color',$dados)) ? $dados['color'] : $row->color;
        $row->start = (array_key_exists('start',$dados)) ? $dados['start'] : $row->start;
        $row->end = (array_key_exists('end',$dados)) ? $dados['end'] : $row->end;
        $row->municipal = (array_key_exists('municipal',$dados)) ? $dados['municipal'] : $row->municipal;
        $row->estadual = (array_key_exists('estadual',$dados)) ? $dados['estadual'] : $row->estadual;

        $row->excluido    = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
        
        $row->save();
        
        if($id===0){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::incluir", Usuarios::getUsuario("id"), "Evento adicionado com sucesso!");
        }elseif($id!== 0 AND isset($row->excluido) AND $row->excluido==="sim"){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::excluir", Usuarios::getUsuario("id"), "Evento de ID ".$id." excluído com sucesso!");
        } else Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::atualizar", Usuarios::getUsuario("id"), "Evento de ID ".$id." atualizado com sucesso!");
        
        return $row;
    }
}