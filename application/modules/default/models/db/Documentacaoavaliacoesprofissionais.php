<?php

/**
 * Define o modelo Documentacaoavaliacoesprofissionais
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Documentacaoavaliacoesprofissionais extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "documentacaoavaliacoesprofissionais";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDocumentacaoavaliacoesprofissionaisHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$documentacaoavaliacoesprofissionais = new Documentacaoavaliacoesprofissionais();
		return $documentacaoavaliacoesprofissionais->getDocumentacaoavaliacoesprofissionais($queries, $page, $maxpage);
	}
	
	public function getDocumentacaoavaliacoesprofissionais($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		
		$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " d1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " d1.data <= '$data_f' ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " d1.idescola = $idescola ");

$idprofessor = (isset($queries["idprofessor"])) ? $queries["idprofessor"] : false;
		if ($idprofessor) array_push($where, " d1.idprofessor = $idprofessor ");

$rgf = (isset($queries["rgf"])) ? $queries["rgf"] : false;
		if ($rgf) array_push($where, " d1.rgf LIKE '%$rgf%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " d1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		
		$ordem = "ORDER BY d1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM documentacaoavaliacoesprofissionais d1
					
					WHERE d1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDocumentacaoavaliacaoprofissionalById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDocumentacaoavaliacoesprofissionais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDocumentacaoavaliacaoprofissionalByIdHelper($id, $queries = array()) {
		$rows = new Documentacaoavaliacoesprofissionais();
		return $rows->getDocumentacaoavaliacaoprofissionalById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Documentacaoavaliacoesprofissionais
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
		$row->idprofessor = (array_key_exists("idprofessor",$dados)) ? $dados["idprofessor"] : $row->idprofessor;
		$row->rgf = (array_key_exists("rgf",$dados)) ? $dados["rgf"] : $row->rgf;
		$row->idperiodo = (array_key_exists("idperiodo",$dados)) ? $dados["idperiodo"] : $row->idperiodo;
		$row->idsala = (array_key_exists("idsala",$dados)) ? $dados["idsala"] : $row->idsala;
		$row->idserie = (array_key_exists("idserie",$dados)) ? $dados["idserie"] : $row->idserie;
		$row->idturma = (array_key_exists("idturma",$dados)) ? $dados["idturma"] : $row->idturma;
		$row->avaliacao = (array_key_exists("avaliacao",$dados)) ? $dados["avaliacao"] : $row->avaliacao;
		$row->atualizarpont = (array_key_exists("atualizarpont",$dados)) ? $dados["atualizarpont"] : $row->atualizarpont;
		$row->aceite = (array_key_exists("aceite",$dados)) ? $dados["aceite"] : $row->aceite;
		$row->aprovado = (array_key_exists("aprovado",$dados)) ? $dados["aprovado"] : $row->aprovado;

		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 
 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
		
		$row->save();
		if (array_key_exists('perguntas', $dados)) $this->setPerguntas($dados['perguntas'], $row->id);
		
		return $row;
	}

	private function setPerguntas($dados, $idparent) {
		
		if (!is_array($dados)) return;
		 
		$itens = new Documentacaoavaliacoesperguntas();

		$avaliacao_1 = $dados['avaliacao_1'];
		$avaliacao_2 = $dados['avaliacao_2'];
		$avaliacao_3 = $dados['avaliacao_3'];
		$avaliacao_4 = $dados['avaliacao_4'];
		$avaliacao_5 = $dados['avaliacao_5'];
		$avaliacao_6 = $dados['avaliacao_6'];
		$avaliacao_7 = $dados['avaliacao_7'];
		$avaliacao_8 = $dados['avaliacao_8'];
		$avaliacao_9 = $dados['avaliacao_9'];
		$avaliacao_10 = $dados['avaliacao_10'];
		$avaliacao_11 = $dados['avaliacao_11'];
		$avaliacao_12 = $dados['avaliacao_12'];
		$avaliacao_13 = $dados['avaliacao_13'];
		$avaliacao_14 = $dados['avaliacao_14'];
		$avaliacao_15 = $dados['avaliacao_15'];
		$avaliacao_16 = $dados['avaliacao_16'];
		
		$ids = array();
		//foreach ($bancos as $i=>$id) {
			$d = array();
			
			$d['idparent'] = $idparent; 
			$d['avaliacao_1'] = $avaliacao_1;
			$d['avaliacao_2'] = $avaliacao_2;
			$d['avaliacao_3'] = $avaliacao_3;
			$d['avaliacao_4'] = $avaliacao_4;
			$d['avaliacao_5'] = $avaliacao_5;
			$d['avaliacao_6'] = $avaliacao_6;
			$d['avaliacao_7'] = $avaliacao_7;
			$d['avaliacao_8'] = $avaliacao_8;
			$d['avaliacao_9'] = $avaliacao_9;
			$d['avaliacao_10'] = $avaliacao_10;
			$d['avaliacao_11'] = $avaliacao_11;
			$d['avaliacao_12'] = $avaliacao_12;
			$d['avaliacao_13'] = $avaliacao_13;
			$d['avaliacao_14'] = $avaliacao_14;
			$d['avaliacao_15'] = $avaliacao_15;
			$d['avaliacao_16'] = $avaliacao_16;
                        
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $dados['logusuario'];
			$d['logdata'] = $dados['logdata'];

                        //var_dump($d);die();
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
		//}
		$perguntas = implode(",", $ids);
                
		if ($perguntas=="") $perguntas = "0";
		$strsql = "DELETE FROM documentacaoavaliacoesperguntas WHERE idparent=$idparent AND id NOT IN ($perguntas)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);
		
	}
	
}