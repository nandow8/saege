<?php

/**
 * Define o modelo Atividadescomplementares
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Atividadescomplementares extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "atividadescomplementares";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAtividadescomplementaresHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$atividadescomplementares = new Atividadescomplementares();
		return $atividadescomplementares->getAtividadescomplementares($queries, $page, $maxpage);
	}
	
	public function getAtividadescomplementares($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " t1.id = $id ");


		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " t1.status LIKE '%$status%' ");

		$idtipoatividade = (isset($queries["idtipoatividade"])) ? $queries["idtipoatividade"] : false;
		if ($idtipoatividade) array_push($where, " t1.idtipoatividade = $idtipoatividade ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " t1.idescola = $idescola ");

		$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " t1.titulo LIKE '%$titulo%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='titulo') $sorting[0]='t1.titulo';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "t1.*"; 
		;
		
		if ($total) $fields = "COUNT(t1.id) as total";
		
		$ordem = "ORDER BY t1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM atividadescomplementares t1
					
					WHERE t1.excluido='nao' 
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAtividadescomplementaresById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAtividadescomplementares($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAtividadescomplementaresByIdHelper($id, $queries = array()) {
		$rows = new Atividadescomplementares();
		return $rows->getAtividadescomplementaresById($id, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Atividadescomplementares
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		}	
		
		$row->idtipoatividade = (array_key_exists("idtipoatividade",$dados)) ? $dados["idtipoatividade"] : $row->idtipoatividade;
		$row->idescola = (array_key_exists("idescola", $dados)) ? $dados["idescola"] : $row->idescola;
		$row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
		$row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
		$row->idescolavinculo = (array_key_exists("idescolavinculo",$dados)) ? $dados["idescolavinculo"] : $row->idescolavinculo;
		$row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
		$row->datafim = (array_key_exists("datafim",$dados)) ? $dados["datafim"] : $row->datafim;
		$row->horainicio = (array_key_exists("horainicio",$dados)) ? $dados["horainicio"] : $row->horainicio;
		$row->horafim = (array_key_exists("horafim",$dados)) ? $dados["horafim"] : $row->horafim;
		$row->diasemana = (array_key_exists("diasemana",$dados)) ? $dados["diasemana"] : $row->diasemana;
		$row->qtdvagas = (array_key_exists("qtdvagas",$dados)) ? $dados["qtdvagas"] : $row->qtdvagas;

 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;		
		
		$row->save();
		
		return $row;
	}	
}