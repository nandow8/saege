<?php

/**
 * Define o modelo Tipoavaliacoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Tipoavaliacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "tipoavaliacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getTipoavaliacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$tipoavaliacoes = new Tipoavaliacoes();
		return $tipoavaliacoes->getTipoavaliacoes($queries, $page, $maxpage);
	}
	
	public function getTipoavaliacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");

		$tipoavaliacao = (isset($queries['tipoavaliacao'])) ? $queries['tipoavaliacao'] : false;
		if($tipoavaliacao) array_push($where, " c1.tipoavaliacao LIKE '%$tipoavaliacao%'");

		$descricao = (isset($queries['descricao'])) ? $queries['descricao'] : false;
		if($descricao) array_push($where, " c1.descricao LIKE '%$descricao%'");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='tipoavaliacao') $sorting[0]='c1.tipoavaliacao';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*";
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM tipoavaliacoes c1
					
					WHERE c1.excluido='nao' 
					$w 
					$ordem	
					$limit";
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getTipoavaliacoesById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTipoavaliacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTipoavaliacoesByIdHelper($id, $queries = array()) {
		$rows = new Tipoavaliacoes();
		return $rows->getTipoavaliacoesById($id, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Tipoavaliacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 

 		$row->tipoavaliacao = (array_key_exists("tipoavaliacao",$dados)) ? $dados["tipoavaliacao"] : $row->tipoavaliacao;
 		$row->iddisciplina = (array_key_exists("iddisciplina", $dados)) ? $dados["iddisciplina"] : $row->iddisciplina;
 		$row->ano = (array_key_exists("ano", $dados)) ? $dados["ano"] : $row->ano;
 		$row->qtdquestoes = (array_key_exists("qtdquestoes", $dados)) ? $dados["qtdquestoes"] : $row->qtdquestoes;
 		$row->valorquestoes = (array_key_exists("valorquestoes", $dados)) ? $dados["valorquestoes"] : $row->valorquestoes;
 		$row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
 
 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}

 		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}	
}