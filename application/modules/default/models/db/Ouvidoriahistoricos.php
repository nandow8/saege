<?php

class Ouvidoriahistoricos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "ouvidoriahistoricos";
	
	/**
     * Define a chave primaria
     * @var integer
     */	
	protected $_primary = "id";
	
	public function getOuvidorias($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$identidade = (isset($queries['identidade'])) ? (int)$queries['identidade'] : false;
		$iddepartamento = (isset($queries['iddepartamento'])) ? (int)$queries['iddepartamento'] : false;		
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$codigo = (isset($queries['codigo'])) ? $queries['codigo'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$tipo = (isset($queries['tipo'])) ? $queries['tipo'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " o1.id=$id ");	
		if ($identidade) array_push($where, " o1.identidade=$identidade ");
		if ($iddepartamento) array_push($where, " o1.iddepartamento=$iddepartamento ");	
		if ($chave) array_push($where, " ((o1.reclamante LIKE '%$chave%') OR (o1.reclamacoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " o1.status='$status' ");
		if ($codigo) array_push($where, " o1.codigo='$codigo' ");
		if ($tipo) array_push($where, " o1.tipo='$tipo' ");
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "o1.*";
		if ($total) $fields = "COUNT(o1.id) as total";
		
		
		$ordem = "ORDER BY o1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM ouvidoriahistoricos o1
					WHERE o1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}
	
	public function getOuvidoriaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getOuvidorias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public function arquiva($dados) {
		$dados['identidade'] = $dados['id'];
		//$dados['idsimagens'] = $dados['idsimagens'];
		unset($dados['id']);
			
		$this->createRow($dados)->save();	
	}
	
}