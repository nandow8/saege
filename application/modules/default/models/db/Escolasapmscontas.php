<?php

class Escolasapmscontas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasapmscontas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getEscolasapmscontasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$apms = new Escolasapmscontas();
		return $apms->getEscolasapmscontas($queries, $page, $maxpage);
	}
	
	public function getEscolasapmscontas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;	
		$idparent = (isset($queries['idparent'])) ? (int)$queries['idparent'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$tipo = (isset($queries['tipo'])) ? $queries['tipo'] : false;
 

		$where = array();
		
		if ($id) array_push($where, " e1.id=$id ");		
		//if ($idescola) array_push($where, " e1.idescola=$idescola ");	
		if ($idparent) array_push($where, " e1.idparent=$idparent ");
		if ($status) array_push($where, " e1.status='$status' ");
		if ($tipo) array_push($where, " e1.tipo='$tipo' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*";
		if ($total) $fields = "COUNT(e1.id) as total";
		
		
		$ordem = "ORDER BY e1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasapmscontas e1
					WHERE e1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//die($strsql); 
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getApmById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasapmscontas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getApmByIdHelper($id, $queries = array()) {
		$rows = new Escolasapmscontas();
		return $rows->getApmById($id, $queries);
	}	
	
	public function getApmByIdParent($id, $queries = array()) {
		if ($id==0) return false;

		$queries['idparent'] = $id;
		$rows = $this->getEscolasapmscontas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getApmByIdParentHelper($id, $queries = array()) {
		$rows = new Escolasapmscontas();
		return $rows->getApmByIdParent($id, $queries);
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasapmscontas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s'); 		
		}else {
			$novoRegistro = false;
		} 

		$row->idparent = (array_key_exists('idparent',$dados)) ? $dados['idparent'] : $row->idparent;
        $row->banco = (array_key_exists('banco',$dados)) ? $dados['banco'] : $row->banco;
		$row->agencia = (array_key_exists('agencia',$dados)) ? $dados['agencia'] : $row->agencia;
		$row->conta = (array_key_exists('conta',$dados)) ? $dados['conta'] : $row->conta;
		$row->digito = (array_key_exists('digito',$dados)) ? $dados['digito'] : $row->digito;			
		$row->tipo = (array_key_exists('tipo',$dados)) ? $dados['tipo'] : $row->tipo;			
                
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
}