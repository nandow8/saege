<?php

/**
 * Define o modelo Educacaoespecialsalasrecursosprofessoreslist
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 FJMX.
 * @version     1.0
 */
class Educacaoespecialsalasrecursosprofessoreslist extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "educacaoespecialsalasrecursosprofessoreslist";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEducacaoespecialsalasrecursosprofessoreslistHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$educacaoespecialsalasrecursosprofessoreslist = new Educacaoespecialsalasrecursosprofessoreslist();
		return $educacaoespecialsalasrecursosprofessoreslist->getEducacaoespecialsalasrecursosprofessoreslist($queries, $page, $maxpage);
	}
	
	public function getEducacaoespecialsalasrecursosprofessoreslist($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " rpl.id = $id ");
		
		$idrecursosprofessores = (isset($queries["idrecursosprofessores"])) ? $queries["idrecursosprofessores"] : false;
		if ($idrecursosprofessores) array_push($where, " rpl.idrecursosprofessores = $idrecursosprofessores ");
		
		$diasemana = (isset($queries["diasemana"])) ? $queries["diasemana"] : false;
		if ($diasemana) array_push($where, " rpl.diasemana = '$diasemana' ");
		 
		$horario = (isset($queries["horario"])) ? $queries["horario"] : false;
		if ($horario) array_push($where, " rpl.horario = $horario ");
		 
		$aluno = (isset($queries["aluno"])) ? $queries["aluno"] : false;
		if ($aluno) array_push($where, " rpl.aluno = $aluno ");
		 
		$professor = (isset($queries["professor"])) ? $queries["professor"] : false;
		if ($professor) array_push($where, " rpl.professor = $professor ");
		
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='titulo') $sorting[0]='l1.titulo';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "rpl.*"; 
		;
		
		if ($total) $fields = "COUNT(rpl.id) as total";
		
		$ordem = "ORDER BY rpl.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM educacaoespecialsalasrecursosprofessoreslist rpl
					
					WHERE rpl.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		// echo $strsql; die;
		return $db->fetchAll($strsql);			
	}	
	
	public function getEducacaoespecialsalasrecursosprofessoreslistById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEducacaoespecialsalasrecursosprofessoreslist($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEducacaoespecialsalasrecursosprofessoreslistByIdHelper($id, $queries = array()) {
		$rows = new Educacaoespecialsalasrecursosprofessoreslist();
		return $rows->getEducacaoespecialsalasrecursosprofessoreslistById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Educacaoespecialsalasrecursosprofessoreslist
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idrecursosprofessores = (array_key_exists("idrecursosprofessores",$dados)) ? $dados["idrecursosprofessores"] : $row->idrecursosprofessores;
		$row->diasemana = (array_key_exists("diasemana",$dados)) ? $dados["diasemana"] : $row->diasemana;
		$row->horario = (array_key_exists("horario",$dados)) ? $dados["horario"] : $row->horario;
		$row->horariofim = (array_key_exists("horariofim",$dados)) ? $dados["horariofim"] : $row->horariofim;
		$row->aluno = (array_key_exists("aluno",$dados)) ? $dados["aluno"] : $row->aluno;
		$row->professor = (array_key_exists("professor",$dados)) ? $dados["professor"] : $row->professor;
		
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
		$row->excluido = 'nao';
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}