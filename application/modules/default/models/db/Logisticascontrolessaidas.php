<?php

/**
 * Define o modelo Logisticascontrolessaidas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Logisticascontrolessaidas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "logisticascontrolessaidas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLogisticascontrolessaidasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$logisticascontrolessaidas = new Logisticascontrolessaidas();
		return $logisticascontrolessaidas->getLogisticascontrolessaidas($queries, $page, $maxpage);
	}
	
	public function getLogisticascontrolessaidas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$ids = (isset($queries['ids'])) ? $queries['ids'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		
		$idfrota = (isset($queries["idfrota"])) ? $queries["idfrota"] : false;
		if ($idfrota) array_push($where, " l1.idfrota = $idfrota ");

$idmotorista = (isset($queries["idmotorista"])) ? $queries["idmotorista"] : false;
		if ($idmotorista) array_push($where, " l1.idmotorista = $idmotorista ");

// $iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
// 		if ($iddepartamento) array_push($where, " l1.iddepartamento = $iddepartamento ");

$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " l1.sequencial LIKE '%$sequencial%' ");

$datainicio_i = (isset($queries["datainicio_i"])) ? $queries["datainicio_i"] : false;
		if ($datainicio_i) array_push($where, " l1.datainicio >= '$datainicio_i' ");

$datainicio_f = (isset($queries["datainicio_f"])) ? $queries["datainicio_f"] : false;
		if ($datainicio_f) array_push($where, " l1.datainicio <= '$datainicio_f' ");

$horaentrada = (isset($queries["horaentrada"])) ? $queries["horaentrada"] : false;
		if ($horaentrada) array_push($where, " l1.horaentrada = '$horaentrada' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");

$diferentretorno = (isset($queries["diferentretorno"])) ? $queries["diferentretorno"] : false;
		if ($diferentretorno) array_push($where, " (l1.retorno <> '$diferentretorno' OR l1.retorno IS NULL) ");

$retorno = (isset($queries["retorno"])) ? $queries["retorno"] : false;
		if ($retorno) array_push($where, " (l1.retorno IS NULL) ");

$diferentstatus = (isset($queries["diferentstatus"])) ? $queries["diferentstatus"] : false;
		if ($diferentstatus) array_push($where, " le1.status <> '$diferentstatus' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*, lf1.placa as modelo, lf1.estado"; 
		if((isset($ids)) && ($ids)){
			$fields = " GROUP_CONCAT(l1.idfrota) as ids "; 
		}
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM logisticascontrolessaidas l1
					LEFT JOIN logisticafrotas lf1 ON lf1.id = l1.idfrota 
					LEFT JOIN logisticascontrolesdentradas le1 ON le1.idfrota = l1.idfrota 
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
	
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		if((isset($ids)) && ($ids)){
			$row = $db->fetchRow($strsql);
			return $row['ids'];
		}
		return $db->fetchAll($strsql);			
	}	
	
	public function getLogisticascontrolessaidaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLogisticascontrolessaidas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLogisticascontrolessaidaByIdHelper($id, $queries = array()) {
		$rows = new Logisticascontrolessaidas();
		return $rows->getLogisticascontrolessaidaById($id, $queries);
	}		
	
	public function getUltimoLogisticascontrolessaida($queries = array()) {
		$queries['order'] = 'ORDER BY l1.id DESC';
		$rows = $this->getLogisticascontrolessaidas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Logisticascontrolessaidas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfrota = (array_key_exists("idfrota",$dados)) ? $dados["idfrota"] : $row->idfrota;
 $row->idmotorista = (array_key_exists("idmotorista",$dados)) ? $dados["idmotorista"] : $row->idmotorista;
//  $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->inicialkm = (array_key_exists("inicialkm",$dados)) ? $dados["inicialkm"] : $row->inicialkm;
 $row->quantidadecombustivel = (array_key_exists("quantidadecombustivel",$dados)) ? $dados["quantidadecombustivel"] : $row->quantidadecombustivel;
 $row->mecanica = (array_key_exists("mecanica",$dados)) ? $dados["mecanica"] : $row->mecanica;
 $row->eletrica = (array_key_exists("eletrica",$dados)) ? $dados["eletrica"] : $row->eletrica;
 $row->funilaria = (array_key_exists("funilaria",$dados)) ? $dados["funilaria"] : $row->funilaria;
 $row->pintura = (array_key_exists("pintura",$dados)) ? $dados["pintura"] : $row->pintura;
 $row->pneus = (array_key_exists("pneus",$dados)) ? $dados["pneus"] : $row->pneus;
 $row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
 $row->macaco = (array_key_exists("macaco",$dados)) ? $dados["macaco"] : $row->macaco;
 $row->triangulo = (array_key_exists("triangulo",$dados)) ? $dados["triangulo"] : $row->triangulo;
 $row->estepe = (array_key_exists("estepe",$dados)) ? $dados["estepe"] : $row->estepe;
 $row->extintor = (array_key_exists("extintor",$dados)) ? $dados["extintor"] : $row->extintor;
 $row->chavederoda = (array_key_exists("chavederoda",$dados)) ? $dados["chavederoda"] : $row->chavederoda;
 $row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
 $row->horaentrada = (array_key_exists("horaentrada",$dados)) ? $dados["horaentrada"] : $row->horaentrada;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->retorno = (array_key_exists("retorno",$dados)) ? $dados["retorno"] : $row->retorno;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}