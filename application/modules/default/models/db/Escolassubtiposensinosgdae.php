<?php

class Escolassubtiposensinosgdae extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolassubtiposensinosgdae";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getSubtiposensinosgdaeHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolassubtiposensinosgdae();
		return $produtos->getSubtiposensinosgdae($queries, $page, $maxpage);
	}
	
	public function getSubtiposensinosgdae($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
                
                $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
                if ($id) array_push($where, " gda.id = '$id' ");
                
		$idgdae = (isset($queries['idgdae'])) ? $queries['idgdae'] : false;
                if ($idgdae) array_push($where, " gda.idgdae = '$idgdae' ");
                
                $serie = (isset($queries['serie'])) ? $queries['serie'] : false;
                if ($serie) array_push($where, " gda.serie = '$serie' ");
		
                $descricao = (isset($queries['descricao'])) ? $queries['descricao'] : false;
                if ($descricao) array_push($where, " (gda.descricao LIKE '%$descricao%') ");
                
		$status = (isset($queries['status'])) ? $queries['status'] : false;
                if ($status) array_push($where, " gda.status='$status' ");

		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "gda.* ";
		if ($total) $fields = "COUNT(gda.id) as total";
		
		
		$ordem = "ORDER BY gda.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM gdae_identificadoresescolassubtipos gda
					WHERE gda.excluido='nao'
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getSubtiposensinogdaeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSubtiposensinosgdae($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSubtiposensinosgdaeByIdHelper($id, $queries = array()) {
		$rows = new Escolassubtiposensinosgdae();
		return $rows->getSubtiposensinogdaeById($id, $queries);
	}		
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Tiposensinos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s'); 		
		}else {
			$novoRegistro = false;
		} 

		$row->tipo = (array_key_exists('tipo',$dados)) ? $dados['tipo'] : $row->tipo;
		$row->descricao = (array_key_exists('descricao',$dados)) ? $dados['descricao'] : $row->descricao;			
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
                $row->serie = (array_key_exists('serie',$dados)) ? $dados['serie'] : $row->serie;
		
		$row->save();

		return $row;
	}
	
	
}