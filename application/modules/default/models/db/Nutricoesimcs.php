<?php

/**
 * Define o modelo Nutricoesimcs
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Nutricoesimcs extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "nutricoesimcs";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getDiagnosticosHelper($queries = array()) {
        $diagnosticos = new Nutricoesimcs();
        return $diagnosticos->getDiagnosticos($queries);
    }

    public function getDiagnosticos($queries = array()) {
        $strsql = "SELECT descricao
					FROM diagnosticos
					GROUP BY descricao";

        $db = Zend_Registry::get('db');

        return $db->fetchAll($strsql);
    }

    /**
     * Gerar data atual (dia/mês/ano) com formato português
     * @return string data
     */
    public function getCurrentData() {
        $data = date('D');
        $mes = date('M');
        $dia = date('d');
        $ano = date('Y');

        $semana = array(
            'Sun' => 'Domingo',
            'Mon' => 'Segunda-Feira',
            'Tue' => 'Terça-Feira',
            'Wed' => 'Quarta-Feira',
            'Thu' => 'Quinta-Feira',
            'Fri' => 'Sexta-Feira',
            'Sat' => 'Sábado'
        );

        $mes_extenso = array(
            'Jan' => 'Janeiro',
            'Feb' => 'Fevereiro',
            'Mar' => 'Marco',
            'Apr' => 'Abril',
            'May' => 'Maio',
            'Jun' => 'Junho',
            'Jul' => 'Julho',
            'Aug' => 'Agosto',
            'Nov' => 'Novembro',
            'Sep' => 'Setembro',
            'Oct' => 'Outubro',
            'Dec' => 'Dezembro'
        );

        $data = "{$dia} de " . $mes_extenso["$mes"] . " de {$ano}";
        return $data;
    }

    public static function getNutricoesimcsHelper($queries = array(), $page = 0, $maxpage = 0) {
        $nutricoesimcs = new Nutricoesimcs();
        return $nutricoesimcs->getNutricoesimcs($queries, $page, $maxpage);
    }

    public function getNutricoesimcs($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " n1.id = $id ");

        $idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
        if ($idsecretaria)
            array_push($where, " n1.idsecretaria = $idsecretaria ");

        $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
        if ($idescola)
            array_push($where, " n1.idescola = $idescola ");

        $idescolaaluno = (isset($queries["idescolaaluno"])) ? $queries["idescolaaluno"] : false;
        if ($idescolaaluno)
            array_push($where, " n1.idescolaaluno = $idescolaaluno ");

        $idserie = (isset($queries["idserie"])) ? $queries["idserie"] : false;
        if ($idserie)
            array_push($where, " n1.idserie = $idserie ");

        $peso = (isset($queries["peso"])) ? $queries["peso"] : false;
        if ($peso)
            array_push($where, " n1.peso = $peso ");

        $altura = (isset($queries["altura"])) ? $queries["altura"] : false;
        if ($altura)
            array_push($where, " n1.altura = $altura ");

        $imc_i = (isset($queries["imc_i"])) ? $queries["imc_i"] : false;
        if ($imc_i)
            array_push($where, " n1.imc >= $imc_i ");

        $imc_f = (isset($queries["imc_f"])) ? $queries["imc_f"] : false;
        if ($imc_f)
            array_push($where, " n1.imc <= $imc_f ");
        $diagnostico = (isset($queries["diagnostico"])) ? $queries["diagnostico"] : false;
        if ($diagnostico)
            array_push($where, " diagnostico = '$diagnostico'");

        $datamedicao_i = (isset($queries["datamedicao_i"])) ? $queries["datamedicao_i"] : false;
        if ($datamedicao_i)
            array_push($where, " n1.datamedicao >= '$datamedicao_i' ");

        $datamedicao_f = (isset($queries["datamedicao_f"])) ? $queries["datamedicao_f"] : false;
        if ($datamedicao_f)
            array_push($where, " n1.datamedicao <= '$datamedicao_f' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " n1.status LIKE '%$status%' ");

        $idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
        if ($idusuario)
            array_push($where, " n1.idusuario = '$idusuario' ");

        $filtro_imc = (isset($queries["filtro_imc"])) ? $queries["filtro_imc"] : false;

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "n1.*";
        ;

        if ($total)
            $fields = "COUNT(n1.id) as total";

        $ordem = "ORDER BY n1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        if ($filtro_imc == "Sim") {
            $_strsql = "";
            $tipoimc = (isset($queries["tipoimc"])) ? $queries["tipoimc"] : false;

            if ($tipoimc)
                $_strsql = " WHERE tipoimc LIKE '%$tipoimc%'";
            //if ($tipoimc) array_push($where, " tipoimc LIKE '%$tipoimc%' ");
            $strsql = "SELECT v1.* FROM(
					SELECT $fields,
						CASE
							WHEN n1.imc < 17 THEN 'Muito abaixo do peso'
					        WHEN n1.imc >= 17 AND n1.imc <= 18.49 THEN 'Abaixo do peso'
							WHEN n1.imc >= 18.50 AND n1.imc <= 24.99 THEN 'Peso normal'
							WHEN n1.imc > 25 AND n1.imc < 30 THEN 'Acima do peso'
							WHEN n1.imc >= 30 AND n1.imc < 35 THEN 'Obesidade I (normal)'
							WHEN n1.imc >= 35 AND n1.imc < 40 THEN 'Obesidade II (severa)'
					       ELSE 'Obesidade III (mórbida)'
						END AS tipoimc
							FROM nutricoesimcs n1

							WHERE n1.excluido='nao'
								$w
								$ordem
								$limit) as v1
						$_strsql";
        }else {
            $strsql = "SELECT $fields
						FROM nutricoesimcs n1

						WHERE n1.excluido='nao'
							$w
						$ordem
						$limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getNutricaoimcById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getNutricoesimcs($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getNutricaoimcByIdHelper($id, $queries = array()) {
        $rows = new Nutricoesimcs();
        return $rows->getNutricaoimcById($id, $queries);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricoesimcs
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idsecretaria = (array_key_exists("idsecretaria", $dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
        $row->idescola = (array_key_exists("idescola", $dados)) ? $dados["idescola"] : $row->idescola;
        $row->idlocal = (array_key_exists("idlocal", $dados)) ? $dados["idlocal"] : $row->idlocal;
        $row->idserie = (array_key_exists("idserie", $dados)) ? $dados["idserie"] : $row->idserie;
        $row->idescolaaluno = (array_key_exists("idescolaaluno", $dados)) ? $dados["idescolaaluno"] : $row->idescolaaluno;
        $row->peso = (array_key_exists("peso", $dados)) ? $dados["peso"] : $row->peso;
        $row->altura = (array_key_exists("altura", $dados)) ? $dados["altura"] : $row->altura;
        $row->imc = (array_key_exists("imc", $dados)) ? $dados["imc"] : $row->imc;
        $row->diagnostico = (array_key_exists("diagnostico", $dados)) ? $dados["diagnostico"] : $row->diagnostico;
        $row->datamedicao = (array_key_exists("datamedicao", $dados)) ? $dados["datamedicao"] : $row->datamedicao;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->idusuario = (array_key_exists("idusuario", $dados)) ? $dados["idusuario"] : $row->idusuario;

        $row->save();

        return $row;
    }

}
