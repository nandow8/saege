<?php

/**
 * Define o modelo Programacoesaulas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Programacoesaulas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "programacoesaulas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getProgramacoesaulasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$programacoesaulas = new Programacoesaulas();
		return $programacoesaulas->getProgramacoesaulas($queries, $page, $maxpage);
	}
	
	public function getProgramacoesaulas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " p1.idescola = $idescola ");

$idescolausuario = (isset($queries["idescolausuario"])) ? $queries["idescolausuario"] : false;
		if ($idescolausuario) array_push($where, " p1.idescolausuario = $idescolausuario ");

$idserie = (isset($queries["idserie"])) ? $queries["idserie"] : false;
		if ($idserie) array_push($where, " p1.idserie = $idserie ");

$idturma = (isset($queries["idturma"])) ? $queries["idturma"] : false;
		if ($idturma) array_push($where, " p1.idturma = $idturma ");

$anoletivo = (isset($queries["anoletivo"])) ? $queries["anoletivo"] : false;
		if ($anoletivo) array_push($where, " p1.anoletivo = '$anoletivo' ");

$segmento = (isset($queries["segmento"])) ? $queries["segmento"] : false;
		if ($segmento) array_push($where, " p1.segmento LIKE '%$segmento%' ");

$componentecurricular = (isset($queries["componentecurricular"])) ? $queries["componentecurricular"] : false;
		if ($componentecurricular) array_push($where, " p1.componentecurricular LIKE '%$componentecurricular%' ");

$tema = (isset($queries["tema"])) ? $queries["tema"] : false;
		if ($tema) array_push($where, " p1.tema LIKE '%$tema%' ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " p1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " p1.data <= '$data_f' ");

$duracao = (isset($queries["duracao"])) ? $queries["duracao"] : false;
		if ($duracao) array_push($where, " p1.duracao LIKE '%$duracao%' ");

$objetivo = (isset($queries["objetivo"])) ? $queries["objetivo"] : false;
		if ($objetivo) array_push($where, " p1.objetivo = '$objetivo' ");

$metodologia = (isset($queries["metodologia"])) ? $queries["metodologia"] : false;
		if ($metodologia) array_push($where, " p1.metodologia = '$metodologia' ");

$conteudoprogramatico = (isset($queries["conteudoprogramatico"])) ? $queries["conteudoprogramatico"] : false;
		if ($conteudoprogramatico) array_push($where, " p1.conteudoprogramatico = '$conteudoprogramatico' ");

$material = (isset($queries["material"])) ? $queries["material"] : false;
		if ($material) array_push($where, " p1.material = '$material' ");

$bibliografia = (isset($queries["bibliografia"])) ? $queries["bibliografia"] : false;
		if ($bibliografia) array_push($where, " p1.bibliografia = '$bibliografia' ");

$publico = (isset($queries["publico"])) ? $queries["publico"] : false;
		if ($publico) array_push($where, " p1.publico LIKE '%$publico%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM programacoesaulas p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getProgramacoesaulaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getProgramacoesaulas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getProgramacoesaulaByIdHelper($id, $queries = array()) {
		$rows = new Programacoesaulas();
		return $rows->getProgramacoesaulaById($id, $queries);
	}		
	
	public function getProgramacaoaulaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getProgramacoesaulas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getProgramacaoaulaByIdHelper($id, $queries = array()) {
		$rows = new Programacoesaulas();
		return $rows->getProgramacaoaulaById($id, $queries);
	}
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Programacoesaulas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idescolausuario = (array_key_exists("idescolausuario",$dados)) ? $dados["idescolausuario"] : $row->idescolausuario;
 $row->idserie = (array_key_exists("idserie",$dados)) ? $dados["idserie"] : $row->idserie;
 $row->idturma = (array_key_exists("idturma",$dados)) ? $dados["idturma"] : $row->idturma;
 $row->anoletivo = (array_key_exists("anoletivo",$dados)) ? $dados["anoletivo"] : $row->anoletivo;
 $row->segmento = (array_key_exists("segmento",$dados)) ? $dados["segmento"] : $row->segmento;
 $row->componentecurricular = (array_key_exists("componentecurricular",$dados)) ? $dados["componentecurricular"] : $row->componentecurricular;
 $row->tema = (array_key_exists("tema",$dados)) ? $dados["tema"] : $row->tema;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->duracao = (array_key_exists("duracao",$dados)) ? $dados["duracao"] : $row->duracao;
 $row->objetivo = (array_key_exists("objetivo",$dados)) ? $dados["objetivo"] : $row->objetivo;
 $row->metodologia = (array_key_exists("metodologia",$dados)) ? $dados["metodologia"] : $row->metodologia;
 $row->conteudoprogramatico = (array_key_exists("conteudoprogramatico",$dados)) ? $dados["conteudoprogramatico"] : $row->conteudoprogramatico;
 $row->material = (array_key_exists("material",$dados)) ? $dados["material"] : $row->material;
 $row->bibliografia = (array_key_exists("bibliografia",$dados)) ? $dados["bibliografia"] : $row->bibliografia;
 $row->publico = (array_key_exists("publico",$dados)) ? $dados["publico"] : $row->publico;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}