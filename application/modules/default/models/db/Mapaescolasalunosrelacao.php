<?php

/**
 * Define o modelo Mapaescolasalunosrelacao
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Mapaescolasalunosrelacao extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "mapaescolasalunosrelacao";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getMapaescolasalunosrelacaoHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$mapaescolasalunosrelacao = new Mapaescolasalunosrelacao();
		return $mapaescolasalunosrelacao->getMapaescolasalunosrelacao($queries, $page, $maxpage);
	}
	
	public function getMapaescolasalunosrelacao($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$idmapa = (isset($queries['idmapa'])) ? (int)$queries['idmapa'] : false;
		if ($idmapa) array_push($where, " m1.idmapa = " . $idmapa);

		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		if( $idaluno ) array_push($where, " m1.idaluno = " . $idaluno);

		$idtipoleitura = (isset($queries['idtipoleitura'])) ? (int)$queries['idtipoleitura'] : false;
		if( $idtipoleitura ) array_push($where, " m1.idtipoleitura = " . $idtipoleitura);

		$idtipohipotese = (isset($queries['idtipohipotese'])) ? (int)$queries['idtipohipotese'] : false;
		if( $idtipohipotese ) array_push($where, " m1.idtipohipotese = " . $idtipohipotese);

		$idtipoescrita = (isset($queries['idtipoescrita'])) ? (int)$queries['idtipoescrita'] : false;
		if( $idtipoescrita ) array_push($where, " m1.idtipoescrita = " . $idtipoescrita);

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='idmapa') $sorting[0]='m1.idmapa';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "m1.*";
		
		if ($total) $fields = "COUNT(m1.id) as total";
		
		$ordem = "ORDER BY m1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM mapaescolasalunosrelacao m1 WHERE excluido='nao' 
				
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getMapaescolasalunosrelacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idmapa'] = $id;
		$rows = $this->getMapaescolasalunosrelacao($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getMapaescolasalunosrelacaoByIdHelper($id, $queries = array()) {
		$rows = new Mapaescolasalunosrelacao();
		return $rows->getMapaescolasalunosrelacaoById($id, $queries);
	}		

	/**
     * Altera todos excluidos de um certo mapa para 'sim'
     * @param array idmapa
     * @return Mapaescolasalunosrelacao
     */
	public function setExcluido($idmapa) {
		
		$row = $this->fetchAll("idmapa=$idmapa");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		foreach ($row as $k => $v) {

			$v->excluido = 'sim';	
				
			$v->save();
		}
		
		return $row;
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Mapaescolasalunosrelacao
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$idmapa = (!isset($dados['idmapa'])) ? 0 : (int)$dados['idmapa'];
		$idaluno = (!isset($dados['idaluno']) ? 0 : (int)$dados['idaluno']);
		$row = $this->fetchRow("idmapa=$idmapa AND idaluno=$idaluno");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		
		$row->idmapa = (array_key_exists("idmapa",$dados)) ? $dados["idmapa"] : $row->idmapa;
		$row->idaluno = (array_key_exists("idaluno", $dados)) ? $dados['idaluno'] : $row->idaluno;
		$row->idtipohipotese = (array_key_exists("idtipohipotese", $dados)) ? $dados['idtipohipotese'] : $row->idtipohipotese;
		$row->idtipoescrita = (array_key_exists('idtipoescrita', $dados)) ? $dados['idtipoescrita'] : $row->idtipoescrita;
		$row->idtipoleitura = (array_key_exists('idtipoleitura', $dados)) ? $dados['idtipoleitura'] : $row->idtipoleitura;
		$row->idtipoaspectonormativo = (array_key_exists('idtipoaspectonormativo', $dados)) ? $dados['idtipoaspectonormativo'] : $row->idtipoaspectonormativo;
		$row->inicial = (array_key_exists('inicial', $dados)) ? $dados['inicial'] : $row->inicial;
		$row->observacoes = (array_key_exists('observacoes', $dados)) ? $dados['observacoes'] : $row->observacoes;
		$row->excluido = (array_key_exists('excluido', $dados)) ? $dados['excluido'] : $row->excluido;	
				
		$row->save();
		
		return $row;
	}
	
}