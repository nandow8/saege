<?php
/**
 * Define o modelo Paises
 *
 * @author		Rafael Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Paises extends Zend_Db_Table_Abstract{

	/**
	* Define o nome da tabela
	* @var string
	*/
	protected $_name = "sed_paises";

	protected $_primary = "id";

	public function getPaises($queries = array(), $page=0, $maxpage=0){
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " p1.sequencial LIKE '%$sequencial%' ");

		$codigo = (isset($queries["codigo"])) ? $queries["codigo"] : false;
		if ($codigo) array_push($where, " p1.codigo == $codigo ");

		$nomepaisouarea = (isset($queries["nomepaisouarea"])) ? $queries["nomepaisouarea"] : false;
		if ($nomepaisouarea) array_push($where, " p1.nomepaisouarea == '$nomepaisouarea' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status = '$status' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id ASC";
		if ($order) $ordem = $order;
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM sed_paises p1					
					WHERE p1.excluido='nao'
						$w 
					$ordem	
					$limit";

		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);
	}
	
}
?>