<?php
/**
 * Define o modelo Enderecos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2012 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasenderecos extends Zend_Db_Table_Abstract {
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolas_enderecos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";	

	public static function getEnderecosHelper($queries = array(), $page = 0, $maxpage = 0) {
        $atas = new Escolasenderecos();
        return $atas->getEnderecos($queries, $page, $maxpage);
    }

    public function getEnderecos($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " ed1.id = $id ");


        $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
        if ($idescola)
            array_push($where, " ed1.idescola = $idescola ");

        $cep = (isset($queries["cep"])) ? $queries["cep"] : false;
        if ($cep)
            array_push($where, " ed1.cep >= '$cep' ");

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "ed1.*";
        ;

        if ($total)
            $fields = "COUNT(ed1.id) as total";

        $ordem = "ORDER BY ed1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM escolas_enderecos ed1
					
					WHERE ed1.excluido='nao' 
						$w 
					$ordem	
					$limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEnderecoById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getEnderecos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

     public static function getEnderecoByIdHelper($id, $queries = array()) {
        $rows = new Escolasenderecos();
        return $rows->getEnderecoById($id, $queries);
    }

    public function getEnderecoByIdEscola($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['idescola'] = $id;
        $rows = $this->getEnderecos($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getEnderecoByIdEscolaHelper($id, $queries = array()) {
        $rows = new Escolasenderecos();
        return $rows->getEnderecoByIdEscola($id, $queries);
    }

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Usuarios
     */
	public function save($dados) {
		
		$novoRegistro = true;
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {			
			$novoRegistro = false;
		}

		if ($id>0)$row->id = $id;
		$row->cep = (isset($dados['cep'])) ? $dados['cep'] : $row->cep;
		$row->endereco = (isset($dados['endereco'])) ? $dados['endereco'] : $row->endereco;
		$row->numero = (isset($dados['numero'])) ? $dados['numero'] : $row->numero;
		$row->complemento = (isset($dados['complemento'])) ? $dados['complemento'] : $row->complemento;
		$row->bairro = (isset($dados['bairro'])) ? $dados['bairro'] : $row->bairro;
		$row->idestado = (isset($dados['idestado'])) ? (int)$dados['idestado'] : (int)$row->idestado;
		$row->cidade = (isset($dados['cidade'])) ? $dados['cidade'] : $row->cidade;
		$row->idescola = (isset($dados['idescola'])) ? $dados['idescola'] : $row->idescola;
		$row->datacriacao = (isset($dados['datacriacao'])) ? $dados['datacriacao'] : $row->datacriacao;
		$row->logusuario = (isset($dados['logusuario'])) ? $dados['logusuario'] : $row->logusuario;
		
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;
		$row->save();

		
		return $row;
	}
}