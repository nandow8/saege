<?php

/**
 * Define o modelo Juridicoprocessospartes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Juridicoprocessospartes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "juridicoprocessospartes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getJuridicoprocessospartesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$juridicoprocessospartes = new Juridicoprocessospartes();
		return $juridicoprocessospartes->getJuridicoprocessospartes($queries, $page, $maxpage);
	}
	
	public function getJuridicoprocessospartes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " j1.id = $id ");
		
		
		$idjuridicoprocesso = (isset($queries["idjuridicoprocesso"])) ? $queries["idjuridicoprocesso"] : false;
		if ($idjuridicoprocesso) array_push($where, " j1.idjuridicoprocesso = $idjuridicoprocesso ");

$idparte = (isset($queries["idparte"])) ? $queries["idparte"] : false;
		if ($idparte) array_push($where, " j1.idparte = $idparte ");

$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
		if ($nome) array_push($where, " j1.nome LIKE '%$nome%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " j1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "j1.*"; 
		;
		
		if ($total) $fields = "COUNT(j1.id) as total";
		
		$ordem = "ORDER BY j1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM juridicoprocessospartes j1
					
					WHERE j1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getJuridicoprocessosparteById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getJuridicoprocessospartes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getJuridicoprocessosparteByIdHelper($id, $queries = array()) {
		$rows = new Juridicoprocessospartes();
		return $rows->getJuridicoprocessosparteById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Juridicoprocessospartes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idjuridicoprocesso = (array_key_exists("idjuridicoprocesso",$dados)) ? $dados["idjuridicoprocesso"] : $row->idjuridicoprocesso;
 $row->idparte = (array_key_exists("idparte",$dados)) ? $dados["idparte"] : $row->idparte;
 $row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}