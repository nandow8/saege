<?php

/**
 * Define o modelo Juridicosadvogados
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Juridicosadvogados extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "juridicosadvogados";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getJuridicosadvogadosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$juridicosadvogados = new Juridicosadvogados();
		return $juridicosadvogados->getJuridicosadvogados($queries, $page, $maxpage);
	}
	
	public function getJuridicosadvogados($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " j1.id = $id ");
		
		
		$advogado = (isset($queries["advogado"])) ? $queries["advogado"] : false;
		if ($advogado) array_push($where, " j1.advogado LIKE '%$advogado%' ");

$oab = (isset($queries["oab"])) ? $queries["oab"] : false;
		if ($oab) array_push($where, " j1.oab LIKE '%$oab%' ");

$email = (isset($queries["email"])) ? $queries["email"] : false;
		if ($email) array_push($where, " j1.email LIKE '%$email%' ");

$telefone1 = (isset($queries["telefone1"])) ? $queries["telefone1"] : false;
		if ($telefone1) array_push($where, " j1.telefone1 LIKE '%$telefone1%' ");

$telefone2 = (isset($queries["telefone2"])) ? $queries["telefone2"] : false;
		if ($telefone2) array_push($where, " j1.telefone2 LIKE '%$telefone2%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " j1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "j1.*"; 
		;
		
		if ($total) $fields = "COUNT(j1.id) as total";
		
		$ordem = "ORDER BY j1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM juridicosadvogados j1
					
					WHERE j1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getJuridicosadvogadoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getJuridicosadvogados($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getJuridicosadvogadoByIdHelper($id, $queries = array()) {
		$rows = new Juridicosadvogados();
		return $rows->getJuridicosadvogadoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Juridicosadvogados
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->advogado = (array_key_exists("advogado",$dados)) ? $dados["advogado"] : $row->advogado;
 $row->oab = (array_key_exists("oab",$dados)) ? $dados["oab"] : $row->oab;
 $row->email = (array_key_exists("email",$dados)) ? $dados["email"] : $row->email;
 $row->telefone1 = (array_key_exists("telefone1",$dados)) ? $dados["telefone1"] : $row->telefone1;
 $row->telefone2 = (array_key_exists("telefone2",$dados)) ? $dados["telefone2"] : $row->telefone2;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}