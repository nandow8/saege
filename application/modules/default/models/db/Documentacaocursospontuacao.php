<?php

/**
 * Define o modelo Documentacaocursospontuacao
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Documentacaocursospontuacao extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "documentacaocursospontuacao";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDocumentacaocursospontuacaoHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$documentacaocursospontuacao = new Documentacaocursospontuacao();
		return $documentacaocursospontuacao->getDocumentacaocursospontuacao($queries, $page, $maxpage);
	}
	
	public function getDocumentacaocursospontuacao($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		
		$curso = (isset($queries["curso"])) ? $queries["curso"] : false;
		if ($curso) array_push($where, " d1.curso = '$curso' ");

$valorpontos = (isset($queries["valorpontos"])) ? $queries["valorpontos"] : false;
		if ($valorpontos) array_push($where, " d1.valorpontos LIKE '%$valorpontos%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " d1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		
		$ordem = "ORDER BY d1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM documentacaocursospontuacao d1
					
					WHERE d1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDocumentacaocursopontuacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDocumentacaocursospontuacao($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDocumentacaocursopontuacaoByIdHelper($id, $queries = array()) {
		$rows = new Documentacaocursospontuacao();
		return $rows->getDocumentacaocursopontuacaoById($id, $queries);
	}		
	

	public function getdocumentacaopontuacaoByTitulo($titulo, $queries = array()) {
		if ((!$titulo) || ($titulo=="")) return false;
		
		$queries['curso'] = $titulo;
		$rows = $this->getDocumentacaocursospontuacao($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Documentacaocursospontuacao
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->curso = (array_key_exists("curso",$dados)) ? $dados["curso"] : $row->curso;
 $row->valorpontos = (array_key_exists("valorpontos",$dados)) ? $dados["valorpontos"] : $row->valorpontos;
 $row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		
		$row->save();
		
		return $row;
	}
	
}