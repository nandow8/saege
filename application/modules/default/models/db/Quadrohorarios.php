<?php

/**
 * Define o modelo Quadrohorarios
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Quadrohorarios extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "quadrohorarios";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getQuadrohorariosHelper($queries = array(), $page = 0, $maxpage = 0) {
        $quadrohorarios = new Quadrohorarios();
        return $quadrohorarios->getQuadrohorarios($queries, $page, $maxpage);
    }

    public function getQuadrohorarios($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        $fieldsmaterias = (isset($queries['fieldsmaterias'])) ? $queries['fieldsmaterias'] : false;
        $die = (isset($queries['die'])) ? $queries['die'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " q1.id = $id ");

        $idprofessor = (isset($queries['idprofessor'])) ? (int) $queries['idprofessor'] : false;
        if ($idprofessor)
            array_push($where, " q1.idprofessor = $idprofessor ");

        $idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
        if ($idsecretaria)
            array_push($where, " q1.idsecretaria = $idsecretaria ");

        $idescolavinculo = (isset($queries["idescolavinculo"])) ? $queries["idescolavinculo"] : false;
        if ($idescolavinculo)
            array_push($where, " q1.idescolavinculo = $idescolavinculo ");

        $diasemana = (isset($queries["diasemana"])) ? $queries["diasemana"] : false;
        if ($diasemana)
            array_push($where, " q1.diasemana LIKE '%$diasemana%' ");

        $periodo = (isset($queries["periodo"])) ? $queries["periodo"] : false;
        if ($periodo)
            array_push($where, " q1.periodo LIKE '%$periodo%' ");

        $idmateria = (isset($queries["idmateria"])) ? $queries["idmateria"] : false;
        if ($idmateria)
            array_push($where, " q1.idmateria = $idmateria ");

        $idprofessor = (isset($queries["idprofessor"])) ? $queries["idprofessor"] : false;
        if ($idprofessor)
            array_push($where, " q1.idprofessor = $idprofessor ");

        $idserie = (isset($queries["idserie"])) ? $queries["idserie"] : false;
        if ($idserie)
            array_push($where, " ev1.idserie = $idserie ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " q1.status LIKE '%$status%' ");

        $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
        if ($idescola)
            array_push($where, " ev1.idescola = $idescola ");

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "q1.*";
        ;
        if ($fieldsmaterias)
            $fields .= " 
, (

SELECT q2.id
	FROM quadrohorarios q2
		LEFT JOIN escolasvinculos ev2 ON ev2.id = q2.idescolavinculo 
		LEFT JOIN escolas es2 ON es2.id=ev2.idescola
		LEFT JOIN escolassalas esa2 ON esa2.id=ev2.idsala 
		LEFT JOIN escolasseries ess2 ON ess2.id = ev2.idserie
		LEFT JOIN escolasturmas et2 ON et2.id=ev2.idturma 
		LEFT JOIN escolasperiodos ep2 ON ep2.id=ev2.idperiodo 
	WHERE q2.excluido='nao' 
		AND q2.idescolavinculo <> q1.idescolavinculo 
		AND q2.idmateria = q1.idmateria 
		AND ev2.idescola = ev1.idescola 
		AND q2.idprofessor = q1.idprofessor 
	ORDER BY q2.idmateria ASC
	LIMIT 0,1
) AS idt,

(SELECT q2.idmateria
	FROM quadrohorarios q2
		LEFT JOIN escolasvinculos ev2 ON ev2.id = q2.idescolavinculo 
		LEFT JOIN escolas es2 ON es2.id=ev2.idescola
		LEFT JOIN escolassalas esa2 ON esa2.id=ev2.idsala 
		LEFT JOIN escolasseries ess2 ON ess2.id = ev2.idserie
		LEFT JOIN escolasturmas et2 ON et2.id=ev2.idturma 
		LEFT JOIN escolasperiodos ep2 ON ep2.id=ev2.idperiodo 
	WHERE q2.excluido='nao' 
		AND q2.idescolavinculo <> q1.idescolavinculo 
		AND q2.idmateria = q1.idmateria 
		AND ev2.idescola = ev1.idescola 
		AND q2.idprofessor = q1.idprofessor 
	ORDER BY q2.idmateria ASC
	LIMIT 0,1
) AS idmateriat,
 
(SELECT COUNT(q2.id) as total
	FROM quadrohorarios q2
		LEFT JOIN escolasvinculos ev2 ON ev2.id = q2.idescolavinculo 
		LEFT JOIN escolas es2 ON es2.id=ev2.idescola
		LEFT JOIN escolassalas esa2 ON esa2.id=ev2.idsala 
		LEFT JOIN escolasseries ess2 ON ess2.id = ev2.idserie
		LEFT JOIN escolasturmas et2 ON et2.id=ev2.idturma 
		LEFT JOIN escolasperiodos ep2 ON ep2.id=ev2.idperiodo 
	WHERE q2.excluido='nao' 
		AND q2.idescolavinculo <> q1.idescolavinculo 
		AND q2.idmateria = q1.idmateria 
		AND ev2.idescola = ev1.idescola 
		AND q2.idprofessor = q1.idprofessor 
	ORDER BY q2.idmateria ASC
	LIMIT 0,1
) AS totalmateria 
		";
        if ($total)
            $fields = "COUNT(q1.id) as total";

        $ordem = "ORDER BY q1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM quadrohorarios q1
						LEFT JOIN escolasvinculos ev1 ON ev1.id = q1.idescolavinculo 
						LEFT JOIN escolas es1 ON es1.id=ev1.idescola
						LEFT JOIN escolassalas esa1 ON esa1.id=ev1.idsala 
						LEFT JOIN escolasseries ess1 ON ess1.id = ev1.idserie
						LEFT JOIN escolasturmas et1 ON et1.id=ev1.idturma 
						LEFT JOIN escolasperiodos ep1 ON ep1.id=ev1.idperiodo 
					WHERE q1.excluido='nao' 
						$w 
					$ordem	
					$limit";

        //if($die) die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getQuadrohorarioById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getQuadrohorarios($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getQuadrohorarioByIdHelper($id, $queries = array()) {
        $rows = new Quadrohorarios();
        return $rows->getQuadrohorarioById($id, $queries);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Quadrohorarios
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idsecretaria = (array_key_exists("idsecretaria", $dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
        $row->idescolavinculo = (array_key_exists("idescolavinculo", $dados)) ? $dados["idescolavinculo"] : $row->idescolavinculo;
        $row->diasemana = (array_key_exists("diasemana", $dados)) ? $dados["diasemana"] : $row->diasemana;
        $row->periodo = (array_key_exists("periodo", $dados)) ? $dados["periodo"] : $row->periodo;
        $row->idmateria = (array_key_exists("idmateria", $dados)) ? $dados["idmateria"] : $row->idmateria;
        $row->idprofessor = (array_key_exists("idprofessor", $dados)) ? $dados["idprofessor"] : $row->idprofessor;
        $row->quadroperiodoaula = (array_key_exists("quadroperiodoaula", $dados)) ? $dados["quadroperiodoaula"] : $row->quadroperiodoaula;
        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;

        $row->aulasseg = (int) (array_key_exists("aulasseg", $dados)) ? $dados["aulasseg"] : $row->aulasseg;
        $row->aulaster = (int) (array_key_exists("aulaster", $dados)) ? $dados["aulaster"] : $row->aulaster;
        $row->aulasqua = (int) (array_key_exists("aulasqua", $dados)) ? $dados["aulasqua"] : $row->aulasqua;
        $row->aulasqui = (int) (array_key_exists("aulasqui", $dados)) ? $dados["aulasqui"] : $row->aulasqui;
        $row->aulassex = (int) (array_key_exists("aulassex", $dados)) ? $dados["aulassex"] : $row->aulassex;
        $row->aulassab = (int) (array_key_exists("aulassab", $dados)) ? $dados["aulassab"] : $row->aulassab;
        $row->aulasdom = (int) (array_key_exists("aulasdom", $dados)) ? $dados["aulasdom"] : $row->aulasdom;

        $row->quantidadesaulas = $row->aulasseg + $row->aulaster + $row->aulasqua + $row->aulasqui + $row->aulassex + $row->aulassab + $row->aulasdom;

        $row->save();

        return $row;
    }

}
