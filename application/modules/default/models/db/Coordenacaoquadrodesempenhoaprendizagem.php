<?php

/**
 * Define o modelo Coordenacaoquadrodesempenhoaprendizagem
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 JFMX.  
 * @version     1.0
 */
class Coordenacaoquadrodesempenhoaprendizagem extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "coordenacaoquadrodesempenhoaprendizagem";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCoordenacaoquadrodesempenhoaprendizagemHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$coordenacaoquadrodesempenhoaprendizagem = new Coordenacaoquadrodesempenhoaprendizagem();
		return $coordenacaoquadrodesempenhoaprendizagem->getCoordenacaoquadrodesempenhoaprendizagem($queries, $page, $maxpage);
	}
	
	public function getCoordenacaoquadrodesempenhoaprendizagem($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
			
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");


		//===========================================================================
				/* Campos que trocam a variavel $strsql */
		//===========================================================================
		 
			$qtd_transf_rem = (isset($queries['qtd_transf_rem'])) ? $queries['qtd_transf_rem'] : false; 
			$qtd_matriculados = (isset($queries['qtd_matriculados'])) ? $queries['qtd_matriculados'] : false;
			$qtd_evadidos_ou_faltosos = (isset($queries['qtd_evadidos_ou_faltosos'])) ? $queries['qtd_evadidos_ou_faltosos'] : false;
			$qtd_conc_satisfatorio_mat = (isset($queries['qtd_conc_satisfatorio_mat'])) ? $queries['qtd_conc_satisfatorio_mat'] : false;
			$qtd_conc_insatisfatorio_mat = (isset($queries['qtd_conc_insatisfatorio_mat'])) ? $queries['qtd_conc_insatisfatorio_mat'] : false;
			$qtd_conc_satisfatorio_port = (isset($queries['qtd_conc_satisfatorio_port'])) ? $queries['qtd_conc_satisfatorio_port'] : false;
			$qtd_conc_insatisfatorio_port = (isset($queries['qtd_conc_insatisfatorio_port'])) ? $queries['qtd_conc_insatisfatorio_port'] : false;
			// $idescola = (isset($queries['idescola'])) ? $idescola = " AND idescola = $queries[idescola]" : false;
			$idescolaBusca = (isset($queries['idescolaBusca'])) ? $idescolaBusca = " AND idescola = $queries[idescolaBusca]" : false;
			$ano = (isset($queries['ano'])) ? $ano = " AND ano = $queries[ano]" : false;
			  
		//===========================================================================
		//===========================================================================
			  
        $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " c1.idescola = $idescola "); 

        $status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' "); 
		
		$classe = (isset($queries["classe"])) ? $queries["classe"] : false;
		if ($classe) array_push($where, " c1.classe = '$classe' "); 
		
		$bimestre = (isset($queries["bimestre"])) ? $queries["bimestre"] : false;
		if ($bimestre) array_push($where, " c1.bimestre = '$bimestre' "); 
  
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM coordenacaoquadrodesempenhoaprendizagem c1 
					WHERE c1.excluido='nao' $idescolaBusca $ano
						$w 
					$ordem	
					$limit";
	 
		if($qtd_matriculados){
			$strsql = " SELECT  '-' as id, '' as idperfil, SUM(qtd_matriculados) as qtd_matriculados , SUBSTRING('$idescolaBusca',16, 18) as idescola, '' as  classe, '-' as qtd_transf_rem, 
								'-' as qtd_evadidos_ou_faltosos, '-' as qtd_conc_satisfatorio_mat, '-' as qtd_conc_insatisfatorio_mat,  '-' as qtd_conc_satisfatorio_port, 
								'-' as qtd_conc_insatisfatorio_port,  SUBSTRING('$ano',12,8)  as ano, 'Ativo' as status
						FROM coordenacaoquadrodesempenhoaprendizagem 
						WHERE bimestre = '$qtd_matriculados' AND idescola = $idescola $idescolaBusca $ano"; 
		}
	
		if($qtd_transf_rem){
			$strsql = " SELECT  '-' as id, '' as idperfil, '-' as qtd_matriculados , SUBSTRING('$idescolaBusca',16, 18) as idescola, '' as  classe, SUM(qtd_transf_rem) as qtd_transf_rem, 
								'-' as qtd_evadidos_ou_faltosos, '-' as qtd_conc_satisfatorio_mat, '-' as qtd_conc_insatisfatorio_mat,  '-' as qtd_conc_satisfatorio_port, 
								'-' as qtd_conc_insatisfatorio_port, SUBSTRING('$ano',12,8)  as ano, 'Ativo' as status
						FROM coordenacaoquadrodesempenhoaprendizagem 
						WHERE bimestre = '$qtd_transf_rem' AND idescola =  $idescola $idescolaBusca $ano";
		}
	
		if($qtd_evadidos_ou_faltosos){
			$strsql = " SELECT  '-' as id, '' as idperfil, '-' as qtd_matriculados ,  SUBSTRING('$idescolaBusca',16, 18) as idescola, '' as  classe, '-' as qtd_transf_rem, 
								SUM(qtd_evadidos_ou_faltosos) as qtd_evadidos_ou_faltosos, '-' as qtd_conc_satisfatorio_mat, '-' as qtd_conc_insatisfatorio_mat,  '-' as qtd_conc_satisfatorio_port, 
								'-' as qtd_conc_insatisfatorio_port, SUBSTRING('$ano',12,8)  as ano, 'Ativo' as status
						FROM coordenacaoquadrodesempenhoaprendizagem 
						WHERE bimestre = '$qtd_evadidos_ou_faltosos' AND idescola =  $idescola $idescolaBusca $ano";
		}
	
		if($qtd_conc_satisfatorio_mat){
			$strsql = " SELECT  '-' as id, '' as idperfil, '-' as qtd_matriculados ,  SUBSTRING('$idescolaBusca',16, 18) as idescola, '$qtd_conc_satisfatorio_mat' as  classe, '-' as qtd_transf_rem, 
								'-' as qtd_evadidos_ou_faltosos, SUM(qtd_conc_satisfatorio_mat) as qtd_conc_satisfatorio_mat, '-' as qtd_conc_insatisfatorio_mat,  '-' as qtd_conc_satisfatorio_port,
								'-' as qtd_conc_insatisfatorio_port, SUBSTRING('$ano',12,8)  as ano, 'Ativo' as status
						FROM coordenacaoquadrodesempenhoaprendizagem 
						WHERE classe = '$qtd_conc_satisfatorio_mat' AND idescola =  $idescola $idescolaBusca $ano";
		}
	
		if($qtd_conc_insatisfatorio_mat){
			$strsql = " SELECT '-' as id, '' as idperfil, '-' as qtd_matriculados ,  SUBSTRING('$idescolaBusca',16, 18) as idescola, '$qtd_conc_insatisfatorio_mat' as  classe, '-' as qtd_transf_rem, '-' as qtd_evadidos_ou_faltosos, 
							   '-' as qtd_conc_satisfatorio_mat, SUM(qtd_conc_insatisfatorio_mat) as qtd_conc_insatisfatorio_mat,  '-' as qtd_conc_satisfatorio_port, 
							   '-' as qtd_conc_insatisfatorio_port, SUBSTRING('$ano',12,8)  as ano, 'Ativo' as status
						FROM coordenacaoquadrodesempenhoaprendizagem 
						WHERE classe = '$qtd_conc_insatisfatorio_mat' AND idescola =  $idescola $idescolaBusca $ano";
		}
	
		if($qtd_conc_satisfatorio_port){
			$strsql = " SELECT '-' as id, '' as idperfil, '-' as qtd_matriculados ,  SUBSTRING('$idescolaBusca',16, 18) as idescola, '$qtd_conc_satisfatorio_port' as  classe, '-' as qtd_transf_rem, 
							   '-' as qtd_evadidos_ou_faltosos, '-' as qtd_conc_satisfatorio_mat, '-' as qtd_conc_insatisfatorio_mat,  
							   SUM(qtd_conc_satisfatorio_port) as qtd_conc_satisfatorio_port, '-' as qtd_conc_insatisfatorio_port, SUBSTRING('$ano',12,8)  as ano, 'Ativo' as status
						FROM coordenacaoquadrodesempenhoaprendizagem 
						WHERE classe = '$qtd_conc_satisfatorio_port' AND idescola =  $idescola $idescolaBusca $ano";
		}
	
		if($qtd_conc_insatisfatorio_port){
			$strsql = " SELECT '-' as id, '' as idperfil, '-' as qtd_matriculados ,  SUBSTRING('$idescolaBusca',16, 18) as idescola, '$qtd_conc_insatisfatorio_port' as  classe, '-' as qtd_transf_rem, '-' as qtd_evadidos_ou_faltosos,
							   '-' as qtd_conc_satisfatorio_mat, '-' as qtd_conc_insatisfatorio_mat,  '-' as qtd_conc_satisfatorio_port, 
							   SUM(qtd_conc_insatisfatorio_port) as qtd_conc_insatisfatorio_port, SUBSTRING('$ano',12,8)  as ano, 'Ativo' as status
						FROM coordenacaoquadrodesempenhoaprendizagem 
						WHERE classe = '$qtd_conc_insatisfatorio_port' AND idescola =  $idescola $idescolaBusca $ano";
		}
		

		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			if(isset($total)) {
				return 0;
			} 
			return $row['total'];
		}	
		// die($strsql);
		return $db->fetchAll($strsql);			
	}	
	
	public function getCoordenacaoquadrodesempenhoaprendizagemById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCoordenacaoquadrodesempenhoaprendizagem($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCoordenacaoquadrodesempenhoaprendizagemByIdHelper($id, $queries = array()) {
		$rows = new Coordenacaoquadrodesempenhoaprendizagem();
		return $rows->getCoordenacaoquadrodesempenhoaprendizagemById($id, $queries);
	} 
 
	public static function getAno() {		
		$strsql = "SELECT DISTINCT ano FROM coordenacaoquadrodesempenhoaprendizagem";	
		 		 					
		$db = Zend_Registry::get('db');				
			
		return $db->fetchAll($strsql);
	}
	 
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Coordenacaoquadrodesempenhoaprendizagem
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
        } 
        
        $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario; 
        $row->ano = (array_key_exists("ano",$dados)) ? $dados["ano"] : $row->ano; 
        $row->idperfil = (array_key_exists("idperfil",$dados)) ? $dados["idperfil"] : $row->idperfil; 
        $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola; 
        $row->idescolaBusca = (array_key_exists("idescolaBusca",$dados)) ? $dados["idescolaBusca"] : $row->idescolaBusca; 
        $row->bimestre = (array_key_exists("bimestre",$dados)) ? $dados["bimestre"] : $row->bimestre; 
        $row->classe = (array_key_exists("classe",$dados)) ? $dados["classe"] : $row->classe; 
        $row->qtd_matriculados = (array_key_exists("qtd_matriculados",$dados)) ? $dados["qtd_matriculados"] : $row->qtd_matriculados; 
        $row->qtd_incluidos = (array_key_exists("qtd_incluidos",$dados)) ? $dados["qtd_incluidos"] : $row->qtd_incluidos; 
        $row->qtd_transf_rem = (array_key_exists("qtd_transf_rem",$dados)) ? $dados["qtd_transf_rem"] : $row->qtd_transf_rem; 
        $row->qtd_evadidos_ou_faltosos = (array_key_exists("qtd_evadidos_ou_faltosos",$dados)) ? $dados["qtd_evadidos_ou_faltosos"] : $row->qtd_evadidos_ou_faltosos; 
        $row->qtd_cca_port = (array_key_exists("qtd_cca_port",$dados)) ? $dados["qtd_cca_port"] : $row->qtd_cca_port; 
        $row->qtd_cca_mat = (array_key_exists("qtd_cca_mat",$dados)) ? $dados["qtd_cca_mat"] : $row->qtd_cca_mat; 
        $row->qtd_conc_insatisfatorio_port = (array_key_exists("qtd_conc_insatisfatorio_port",$dados)) ? $dados["qtd_conc_insatisfatorio_port"] : $row->qtd_conc_insatisfatorio_port; 
        $row->qtd_conc_insatisfatorio_mat = (array_key_exists("qtd_conc_insatisfatorio_mat",$dados)) ? $dados["qtd_conc_insatisfatorio_mat"] : $row->qtd_conc_insatisfatorio_mat; 
        $row->qtd_conc_satisfatorio_port = (array_key_exists("qtd_conc_satisfatorio_port",$dados)) ? $dados["qtd_conc_satisfatorio_port"] : $row->qtd_conc_satisfatorio_port; 
        $row->qtd_conc_satisfatorio_mat = (array_key_exists("qtd_conc_satisfatorio_mat",$dados)) ? $dados["qtd_conc_satisfatorio_mat"] : $row->qtd_conc_satisfatorio_mat; 
		
        if (is_null($row->datacriacao))  $row->datacriacao = date("Y-m-d H:i:s");
        $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status; 
        $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario; 
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
  
		$row->save();
		
		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Quadro de desempenho aprendizagem salva com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Quadro de desempenho aprendizagem com o ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Quadro de desempenho aprendizagem com o ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Quadro de desempenho aprendizagem com o ID ".$id." atualizada com sucesso!");

		return $row;
	}
	
}