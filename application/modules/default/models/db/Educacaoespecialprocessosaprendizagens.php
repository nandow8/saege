<?php

/**
 * Define o modelo Educacaoespecialprocessosaprendizagens
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Educacaoespecialprocessosaprendizagens extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "educacaoespecialprocessosaprendizagens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEducacaoespecialprocessosaprendizagensHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$educacaoespecialprocessosaprendizagens = new Educacaoespecialprocessosaprendizagens();
		return $educacaoespecialprocessosaprendizagens->getEducacaoespecialprocessosaprendizagens($queries, $page, $maxpage);
	}
	
	public function getEducacaoespecialprocessosaprendizagens($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		 
		$escolaaee = (isset($queries["escolaaee"])) ? $queries["escolaaee"] : false;
		if ($escolaaee) array_push($where, " e1.escolaaee = '$escolaaee' ");

		$idaluno = (isset($queries["idaluno"])) ? $queries["idaluno"] : false;
		if ($idaluno) array_push($where, " e1.idaluno = $idaluno ");
		
		$nomealuno = (isset($queries["nomealuno"])) ? $queries["nomealuno"] : false;
		if ($nomealuno) array_push($where, " e1.nomealuno = '$nomealuno' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM educacaoespecialprocessosaprendizagens e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEducacaoespecialprocessoaprendizagemById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEducacaoespecialprocessosaprendizagens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEducacaoespecialprocessoaprendizagemByIdHelper($id, $queries = array()) {
		$rows = new Educacaoespecialprocessosaprendizagens();
		return $rows->getEducacaoespecialprocessoaprendizagemById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Educacaoespecialprocessosaprendizagens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
		$row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
		$row->tipoavaliacao = (array_key_exists("tipoavaliacao",$dados)) ? $dados["tipoavaliacao"] : $row->tipoavaliacao;
		$row->tipoevolucao = (array_key_exists("tipoevolucao",$dados)) ? $dados["tipoevolucao"] : $row->tipoevolucao;
		$row->escolaaee = (array_key_exists("escolaaee",$dados)) ? $dados["escolaaee"] : $row->escolaaee;
		$row->nomealuno = (array_key_exists("nomealuno",$dados)) ? $dados["nomealuno"] : $row->nomealuno;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
						
		$row->save();
		
		return $row;
	}
	
}