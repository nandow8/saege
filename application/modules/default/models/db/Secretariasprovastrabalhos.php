<?php

/**
 * Define o modelo Secretariasprovastrabalhos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Secretariasprovastrabalhos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "secretariasprovastrabalhos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getSecretariasprovastrabalhosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$secretariasprovastrabalhos = new Secretariasprovastrabalhos();
		return $secretariasprovastrabalhos->getSecretariasprovastrabalhos($queries, $page, $maxpage);
	}
	
	public function getSecretariasprovastrabalhos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " s1.id = $id ");
		
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " s1.idescola = $idescola ");
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " s1.sequencial LIKE '%$sequencial%' ");

$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " s1.datalancamento >= '$datalancamento_i' ");

$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " s1.datalancamento <= '$datalancamento_f' ");

$horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
		if ($horalancamento) array_push($where, " s1.horalancamento = '$horalancamento' ");

$idusuariologado = (isset($queries["idusuariologado"])) ? $queries["idusuariologado"] : false;
		if ($idusuariologado) array_push($where, " s1.idusuariologado = $idusuariologado ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " s1.idescola = $idescola ");

$idserie = (isset($queries["idserie"])) ? $queries["idserie"] : false;
		if ($idserie) array_push($where, " s1.idserie = $idserie ");

$idescolavinculo = (isset($queries["idescolavinculo"])) ? $queries["idescolavinculo"] : false;
		if ($idescolavinculo) array_push($where, " s1.idescolavinculo = $idescolavinculo ");

$idperiodo = (isset($queries["idperiodo"])) ? $queries["idperiodo"] : false;
		if ($idperiodo) array_push($where, " s1.idperiodo = $idperiodo ");

$idaluno = (isset($queries["idaluno"])) ? $queries["idaluno"] : false;
		if ($idaluno) array_push($where, " s1.idaluno = $idaluno ");

$material = (isset($queries["material"])) ? $queries["material"] : false;
		if ($material) array_push($where, " s1.material LIKE '%$material%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " s1.descricoes = '$descricoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " s1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "s1.*"; 
		;
		
		if ($total) $fields = "COUNT(s1.id) as total";
		
		$ordem = "ORDER BY s1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM secretariasprovastrabalhos s1
					
					WHERE s1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getSecretariasprovastrabalhoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSecretariasprovastrabalhos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSecretariasprovastrabalhoByIdHelper($id, $queries = array()) {
		$rows = new Secretariasprovastrabalhos();
		return $rows->getSecretariasprovastrabalhoById($id, $queries);
	}		
	

	public function getUltimoSecretariasprovastrabalho($queries = array()) {
		$queries['order'] = 'ORDER BY s1.id DESC';
		$rows = $this->getSecretariasprovastrabalhos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Secretariasprovastrabalhos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
 $row->horalancamento = (array_key_exists("horalancamento",$dados)) ? $dados["horalancamento"] : $row->horalancamento;
 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
 $row->idserie = (array_key_exists("idserie",$dados)) ? $dados["idserie"] : $row->idserie;
 $row->idescolavinculo = (array_key_exists("idescolavinculo",$dados)) ? $dados["idescolavinculo"] : $row->idescolavinculo;
 $row->idperiodo = (array_key_exists("idperiodo",$dados)) ? $dados["idperiodo"] : $row->idperiodo;
 $row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
 $row->material = (array_key_exists("material",$dados)) ? $dados["material"] : $row->material;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}