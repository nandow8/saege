<?php

class Estabelecimentos_Escolasarquivos extends Zend_Db_Table_Abstract {

	/**
	 * Define o nome da tabela
	 * @var string
	 */
	protected $_name = "escolasedificacoesarquivos";
	
	/**
	 * Define a chave primaria
	 * @var integer
	 */
	protected $_primary = "id";
	
	
	public static function getArquivosByIdEscolas($idedificacoes, $order = 'pa1.ordem') {
		
		$strsql = "SELECT pa1.idarquivo, pa1.idedificacoes, pa1.legenda, a1.* 
					FROM escolasedificacoesarquivos pa1 
						LEFT JOIN arquivos ar1 ON a1.id=pa1.idarquivo
					WHERE pa1.excluido='nao' 
						AND pa1.idedificacoes=$idedificacoes 
					ORDER BY $order"; 
						
		$db = Zend_Registry::get('db');
		return $db->fetchAll($strsql);
	}
	
	public function setArquivos($idedificacoes, $arquivos, $legendas) {
		//if (!is_array($arquivos)) return;
		
		$logdata = date('Y-m-d G:i:s');
		foreach ($arquivos as $i=>$idarquivo) {
			$d = array();
			
			$legenda = $legendas[$i];
			
			$d['idedificacoes'] = $idedificacoes;
			$d['idarquivo'] = $idarquivo;
			$d['legenda'] = $legenda;
			$d['ordem'] = $i;
			$d['excluido'] = 'nao';
			$d['logusuario'] = Usuarios::getUsuario('id');
			$d['logdata'] = $logdata;
			$this->save($d);
		}
		
		$ids = implode(",", $arquivos);
		if ($ids=="") $ids = "0";
		$strsql = "DELETE FROM escolasedificacoesarquivos WHERE idedificacoes=$idedificacoes AND idarquivo NOT IN ($ids)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);		
	}	

	public static function getEscolasArquivos($idedificacoes = NULL, $status = NULL, $inverte = false, $limite = NULL) {
		$db = Zend_Registry::get('db');
		
		$w = "";
		if (!is_null($idedificacoes)) $w .= " AND pa1.idedificacoes = " . (int)$idedificacoes;
		if (!is_null($status)) $w .= " AND pa1.status = '$status'"; 
		
		$l = "";
		if (!is_null($limite)) $l .= " LIMIT '$limite'";
		
		$order = "ORDER BY pa1.ordem";
		if ($inverte) $order .= " DESC";
		
		$strsql = "SELECT pa1.* 
					FROM escolasedificacoesarquivos pa1 
					WHERE pa1.excluido = 'nao' 
					$w
					$order
					$l";
		//die($strsql);			
		return $db->fetchAll($strsql);
	}
	
	/**
	 * Salva o dados (INSERT OU UPDATE)
	 * @param array dados
	 * @return Arquivos
	 */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$idedificacoes = (int)$dados['idedificacoes'];
		$idarquivo = (int)$dados['idarquivo'];
		
		$row = $this->fetchRow("idedificacoes=" . $idedificacoes . " AND idarquivo=" . $idarquivo);		
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
				
		if ($id>0)$row->id = $id;
		
		$row->idedificacoes = (isset($dados['idedificacoes'])) ? $dados['idedificacoes'] : $row->idedificacoes;
		$row->idarquivo = (isset($dados['idarquivo'])) ? $dados['idarquivo'] : $row->idarquivo;
		$row->legenda = (isset($dados['legenda'])) ? $dados['legenda'] : $row->legenda;
		$row->ordem = (isset($dados['ordem'])) ? $dados['ordem'] : $row->ordem;
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (isset($dados['logusuario'])) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (isset($dados['logdata'])) ? $dados['logdata'] : $row->logdata;
		$row->save();
		
		return $row;
	}
	
}