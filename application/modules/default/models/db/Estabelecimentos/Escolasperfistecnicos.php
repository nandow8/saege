<?php

class Estabelecimentos_Escolasperfistecnicos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasperfistecnicos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getPerfistecnicosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Estabelecimentos_Escolasterrenos();
		return $produtos->getPerfistecnicos($queries, $page, $maxpage);
	}
	
	public function getPerfistecnicos($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " a1.id=$id ");
		if ($idescola) array_push($where, " a1.idescola=$idescola ");
		if ($chave) array_push($where, " ((a1.caixaresiduos LIKE '%$chave%') OR (a1.tratamentoresiduos LIKE '%$chave%')) ");
		if ($origem) array_push($where, " a1.origem='$origem' ");
		if ($status) array_push($where, " a1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*";
		if ($total) $fields = "COUNT(a1.id) as total";
		
		
		$ordem = "ORDER BY a1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasperfistecnicos a1
					WHERE a1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getPerfiltecnicoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPerfistecnicos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public function getPerfiltecnicoByIdEscola($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idescola'] = $id;
		$rows = $this->getPerfistecnicos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPerfiltecniconomeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$fornecedores = new Estabelecimentos_Escolasterrenos();
		$queries['id'] = $id;
		$rows = $fornecedores->getPerfistecnicos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$row = $rows[0];
		
		return $row['tipo'];
	}
	
	public static function getPerfiltecnicoByIdHelper($id, $queries = array()) {
		$rows = new Estabelecimentos_Escolasterrenos();
		return $rows->getPerfiltecnicoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Perfistecnicos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
		} 

		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->existencia = (array_key_exists('existencia',$dados)) ? $dados['existencia'] : $row->existencia;
		$row->quantidade = (array_key_exists('quantidade',$dados)) ? $dados['quantidade'] : $row->quantidade;
		$row->capacidademedia = (array_key_exists('capacidademedia',$dados)) ? $dados['capacidademedia'] : $row->capacidademedia;
		$row->salasdeapoio = (array_key_exists('salasdeapoio',$dados)) ? $dados['salasdeapoio'] : $row->salasdeapoio;
		$row->docentes = (array_key_exists('docentes',$dados)) ? $dados['docentes'] : $row->docentes;
		$row->apoioadministrativo = (array_key_exists('apoioadministrativo',$dados)) ? $dados['apoioadministrativo'] : $row->apoioadministrativo;
		$row->apoiotecnico = (array_key_exists('apoiotecnico',$dados)) ? $dados['apoiotecnico'] : $row->apoiotecnico;
		$row->apoiooperacional = (array_key_exists('apoiooperacional',$dados)) ? $dados['apoiooperacional'] : $row->apoiooperacional;
		$row->laboratoriosinformatica = (array_key_exists('laboratoriosinformatica',$dados)) ? $dados['laboratoriosinformatica'] : $row->laboratoriosinformatica;
		$row->laboratoriosfisica = (array_key_exists('laboratoriosfisica',$dados)) ? $dados['laboratoriosfisica'] : $row->laboratoriosfisica;
		$row->laboratoriosquimica = (array_key_exists('laboratoriosquimica',$dados)) ? $dados['laboratoriosquimica'] : $row->laboratoriosquimica;
		$row->laboratoriosapoio = (array_key_exists('laboratoriosapoio',$dados)) ? $dados['laboratoriosapoio'] : $row->laboratoriosapoio;
		$row->bibliotecas = (array_key_exists('bibliotecas',$dados)) ? $dados['bibliotecas'] : $row->bibliotecas;
		$row->salasleitura = (array_key_exists('salasleitura',$dados)) ? $dados['salasleitura'] : $row->salasleitura;
		$row->estacionamentos = (array_key_exists('estacionamentos',$dados)) ? $dados['estacionamentos'] : $row->estacionamentos;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		/*
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		*/
		
		$row->save();

		return $row;
	}
	
	
}