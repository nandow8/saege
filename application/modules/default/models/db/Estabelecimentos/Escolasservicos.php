<?php

class Estabelecimentos_Escolasservicos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasservicos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getServicosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Estabelecimentos_Escolasservicos();
		return $produtos->getServicos($queries, $page, $maxpage);
	}
	
	public function getServicos($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " a1.id=$id ");
		if ($idescola) array_push($where, " a1.idescola=$idescola ");
		if ($chave) array_push($where, " ((a1.caixaresiduos LIKE '%$chave%') OR (a1.tratamentoresiduos LIKE '%$chave%')) ");
		if ($origem) array_push($where, " a1.origem='$origem' ");
		if ($status) array_push($where, " a1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*";
		if ($total) $fields = "COUNT(a1.id) as total";
		
		
		$ordem = "ORDER BY a1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasservicos a1
					WHERE a1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getServicoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getServicos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public function getServicoByIdEscola($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idescola'] = $id;
		$rows = $this->getServicos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getServiconomeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$fornecedores = new Estabelecimentos_Escolasservicos();
		$queries['id'] = $id;
		$rows = $fornecedores->getServicos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$row = $rows[0];
		
		return $row['tipo'];
	}
	
	public static function getServicoByIdHelper($id, $queries = array()) {
		$rows = new Estabelecimentos_Escolasservicos();
		return $rows->getServicoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Servicos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
		} 

		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->coletalixo = (array_key_exists('coletalixo',$dados)) ? $dados['coletalixo'] : $row->coletalixo;
		$row->transporteescolar = (array_key_exists('transporteescolar',$dados)) ? $dados['transporteescolar'] : $row->transporteescolar;
		$row->transportefuncional = (array_key_exists('transportefuncional',$dados)) ? $dados['transportefuncional'] : $row->transportefuncional;
		$row->servicosclinicos = (array_key_exists('servicosclinicos',$dados)) ? $dados['servicosclinicos'] : $row->servicosclinicos;
		$row->servicosodontologicos = (array_key_exists('servicosodontologicos',$dados)) ? $dados['servicosodontologicos'] : $row->servicosodontologicos;
		$row->atividadescomunitarias = (array_key_exists('atividadescomunitarias',$dados)) ? $dados['atividadescomunitarias'] : $row->atividadescomunitarias;
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		/*
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		*/
		
		$row->save();

		return $row;
	}
	
	
}