<?php
//escolasterrenos
class Estabelecimentos_Escolasterrenos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasterrenos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getTerrenosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Estabelecimentos_Escolasterrenos();
		return $produtos->getTerrenos($queries, $page, $maxpage);
	}
	
	public function getTerrenos($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " a1.id=$id ");
		if ($idescola) array_push($where, " a1.idescola=$idescola ");
		if ($chave) array_push($where, " ((a1.caixaresiduos LIKE '%$chave%') OR (a1.tratamentoresiduos LIKE '%$chave%')) ");
		if ($origem) array_push($where, " a1.origem='$origem' ");
		if ($status) array_push($where, " a1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*";
		if ($total) $fields = "COUNT(a1.id) as total";
		
		
		$ordem = "ORDER BY a1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasterrenos a1
					WHERE a1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getTerrenoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTerrenos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public function getTerrenoByIdEscola($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idescola'] = $id;
		$rows = $this->getTerrenos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTerrenonomeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$fornecedores = new Estabelecimentos_Escolasterrenos();
		$queries['id'] = $id;
		$rows = $fornecedores->getTerrenos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$row = $rows[0];
		
		return $row['tipo'];
	}
	
	public static function getTerrenoByIdHelper($id, $queries = array()) {
		$rows = new Estabelecimentos_Escolasterrenos();
		return $rows->getTerrenoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Terrenos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
		} 

		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->idtipoterreno = (array_key_exists('idtipoterreno',$dados)) ? $dados['idtipoterreno'] : $row->idtipoterreno;
		$row->idtipodelimitacao = (array_key_exists('idtipodelimitacao',$dados)) ? $dados['idtipodelimitacao'] : $row->idtipodelimitacao;
		$row->dimensao = (array_key_exists('dimensao',$dados)) ? $dados['dimensao'] : $row->dimensao;		
		$row->areaverde = (array_key_exists('areaverde',$dados)) ? $dados['areaverde'] : $row->areaverde;
		$row->areaconstruida = (array_key_exists('areaconstruida',$dados)) ? $dados['areaconstruida'] : $row->areaconstruida;
		$row->arealivre = (array_key_exists('arealivre',$dados)) ? $dados['arealivre'] : $row->arealivre;
		$row->areatotal = (array_key_exists('areatotal',$dados)) ? $dados['areatotal'] : $row->areatotal;
		$row->quantidade = (array_key_exists('quantidade',$dados)) ? $dados['quantidade'] : $row->quantidade;
		$row->avaliacaoresumida = (array_key_exists('avaliacaoresumida',$dados)) ? $dados['avaliacaoresumida'] : $row->avaliacaoresumida;
		$row->confrontacao = (array_key_exists('confrontacao',$dados)) ? $dados['confrontacao'] : $row->confrontacao;
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		/*
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		*/
		
		$row->save();

		return $row;
	}
	
	
}