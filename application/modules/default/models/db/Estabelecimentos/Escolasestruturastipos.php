<?php

class Estabelecimentos_Escolasestruturastipos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasestruturastipos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_ORIGEM_ABASTECIMENTO_ESGOTO = 'abastacimento_esgoto';
	public static $_ORIGEM_ABASTECIMENTO_TUBULACAO_ESGOTO = 'abastacimento_tubulacao';
	public static $_ORIGEM_ABASTECIMENTO_REDE_AGUA = 'abastacimento_rede_agua';
	public static $_ORIGEM_ABASTECIMENTO_TIPO_TERRENO = 'abastacimento_terreno';
	public static $_ORIGEM_ABASTECIMENTO_DELIMITACAO = 'abastacimento_delimitacao';
	//public static $_ORIGEM_PERFIL_LOCALIZACAO_UNIDADE = 'perfil_unidade';
	//public static $_ORIGEM_PERFIL_LOCALIZACAO_DOCUMENTO = 'perfil_documento';
	public static $_ORIGEM_PERFIL_LOCALIZACAO_REDES_AGUA = 'perfil_rede_agua';
	public static $_ORIGEM_PERFIL_LOCALIZACAO_REDES_ESGOTO = 'perfil_esgoto';
	public static $_ORIGEM_PERFIL_LOCALIZACAO_REDES_ELETRICA = 'perfil_rede_eletrica';
		
	
	/**
	 * 
	 */
	public static function getOrigens($field = false) {
		$res = array(
			self::$_ORIGEM_ABASTECIMENTO_ESGOTO => 'Tipo de rede de esgoto',
			self::$_ORIGEM_ABASTECIMENTO_TUBULACAO_ESGOTO => 'Tipo de tubulação de esgoto',
			self::$_ORIGEM_ABASTECIMENTO_REDE_AGUA => 'Tipo de rede de água',
			self::$_ORIGEM_ABASTECIMENTO_TIPO_TERRENO => 'Tipos de terreno',
			self::$_ORIGEM_ABASTECIMENTO_DELIMITACAO => 'Tipo de delimitação',
			//self::$_ORIGEM_PERFIL_LOCALIZACAO_UNIDADE => 'Tipo de localização da unidade',
			self::$_ORIGEM_PERFIL_LOCALIZACAO_REDES_AGUA => 'Tipos de redes de água',
			self::$_ORIGEM_PERFIL_LOCALIZACAO_REDES_ESGOTO => 'Tipos de redes de esgoto',
			self::$_ORIGEM_PERFIL_LOCALIZACAO_REDES_ELETRICA => 'Tipos de redes elétricas'			
			//self::$_ORIGEM_PERFIL_LOCALIZACAO_DOCUMENTO => 'Tipos de documentos de propriedade'
		);
		
		if (!$field) return $res;
		return $res[$field];
	}
	
	public static function getEscolasestruturastiposHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasestruturastipos = new Escolasestruturastipos();
		return $escolasestruturastipos->getEscolasestruturastipos($queries, $page, $maxpage);
	}
	
	public function getEscolasestruturastipos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " e1.idsecretaria = $idsecretaria ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " e1.tipo LIKE '%$tipo%' ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " e1.origem LIKE '%$origem%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " e1.descricoes = '$descricoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasestruturastipos e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolaestruturastipoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasestruturastipos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaestruturastipoByIdHelper($id, $queries = array()) {
		$rows = new Escolasestruturastipos();
		return $rows->getEscolaestruturastipoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasestruturastipos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}	
	
	
	
}