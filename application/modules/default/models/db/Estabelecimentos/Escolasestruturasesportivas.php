<?php

class Estabelecimentos_Escolasestruturasesportivas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasestruturasesportivas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getEstruturasesportivasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Estabelecimentos_Escolasterrenos();
		return $produtos->getEstruturasesportivas($queries, $page, $maxpage);
	}
	
	public function getEstruturasesportivas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " a1.id=$id ");
		if ($idescola) array_push($where, " a1.idescola=$idescola ");
		if ($chave) array_push($where, " ((a1.caixaresiduos LIKE '%$chave%') OR (a1.tratamentoresiduos LIKE '%$chave%')) ");
		if ($origem) array_push($where, " a1.origem='$origem' ");
		if ($status) array_push($where, " a1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*";
		if ($total) $fields = "COUNT(a1.id) as total";
		
		
		$ordem = "ORDER BY a1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasestruturasesportivas a1
					WHERE a1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getEstruturaesportivaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEstruturasesportivas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public function getEstruturaesportivaByIdEscola($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idescola'] = $id;
		$rows = $this->getEstruturasesportivas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEstruturaesportivanomeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$fornecedores = new Estabelecimentos_Escolasterrenos();
		$queries['id'] = $id;
		$rows = $fornecedores->getEstruturasesportivas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$row = $rows[0];
		
		return $row['tipo'];
	}
	
	public static function getEstruturaesportivaByIdHelper($id, $queries = array()) {
		$rows = new Estabelecimentos_Escolasterrenos();
		return $rows->getEstruturaesportivaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Estruturasesportivas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
		} 

		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->existencia = (array_key_exists('existencia',$dados)) ? $dados['existencia'] : $row->existencia;
		$row->quantidade = (array_key_exists('quantidade',$dados)) ? $dados['quantidade'] : $row->quantidade;
		$row->capacidademedia = (array_key_exists('capacidademedia',$dados)) ? $dados['capacidademedia'] : $row->capacidademedia;
		$row->quadrasabertas = (array_key_exists('quadrasabertas',$dados)) ? $dados['quadrasabertas'] : $row->quadrasabertas;
		$row->quadrasfechadas = (array_key_exists('quadrasfechadas',$dados)) ? $dados['quadrasfechadas'] : $row->quadrasfechadas;
		$row->piscinaspadroes = (array_key_exists('piscinaspadroes',$dados)) ? $dados['piscinaspadroes'] : $row->piscinaspadroes;
		$row->piscinassemiolimpicas = (array_key_exists('piscinassemiolimpicas',$dados)) ? $dados['piscinassemiolimpicas'] : $row->piscinassemiolimpicas;
		$row->piscinasolimipicas = (array_key_exists('piscinasolimipicas',$dados)) ? $dados['piscinasolimipicas'] : $row->piscinasolimipicas;
		$row->saltoornamental = (array_key_exists('saltoornamental',$dados)) ? $dados['saltoornamental'] : $row->saltoornamental;
		$row->camposdefutebol = (array_key_exists('camposdefutebol',$dados)) ? $dados['camposdefutebol'] : $row->camposdefutebol;
		$row->pistasatletismo = (array_key_exists('pistasatletismo',$dados)) ? $dados['pistasatletismo'] : $row->pistasatletismo;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		/*
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		*/
		
		$row->save();

		return $row;
	}
	
	
}