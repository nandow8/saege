<?php

/**
 * Define o modelo SEDTiposdeClasses
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class SEDTiposdeClasses extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "sed_tiposdeclasses";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getSEDTiposdeClassesHelper($queries = array(), $page = 0, $maxpage = 0) {
        $sedtiposdeclasses = new SEDTiposdeClasses();
        return $sedtiposdeclasses->getSEDTiposdeClasses($queries, $page, $maxpage);
    }

    public function getSEDTiposdeClasses($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " t1.id = $id ");

        $ids = (isset($queries['ids'])) ? $queries['ids'] : false;
        if ($ids)
            array_push($where, " t1.id in ($ids) ");

        $tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
        if ($tipo)
            array_push($where, " t1.tipo = $tipo ");

        $descricao = (isset($queries["descricao"])) ? $queries["descricao"] : false;
        if ($descricao)
            array_push($where, " t1.descricao LIKE '%$descricao%' ");

        $abreviacao = (isset($queries["abreviacao"])) ? $queries["abreviacao"] : false;
        if ($abreviacao)
            array_push($where, " t1.abreviacao LIKE '%$abreviacao%' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " t1.status LIKE '%$status%' ");

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'descricao')
                    $sorting[0] = 't1.descricao';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "t1.*";

        if ($total)
            $fields = "COUNT(t1.id) as total";

        $ordem = "ORDER BY t1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM sed_tiposdeclasses t1
					
					WHERE t1.excluido='nao' 
					$w 
					$ordem	
					$limit";

        
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        return $db->fetchAll($strsql);
    }

    public function getSEDTiposdeClassesById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getSEDTiposdeClasses($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getSEDTiposdeClassesByIdHelper($id, $queries = array()) {
        $rows = new SEDTiposdeClasses();
        return $rows->getSEDTiposdeClassesById($id, $queries);
    }


}
