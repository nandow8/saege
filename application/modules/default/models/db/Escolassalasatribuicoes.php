<?php

class Escolassalasatribuicoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolassalasatribuicoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getSalasatribuicoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolassalasatribuicoes();
		return $produtos->getSalasatribuicoes($queries, $page, $maxpage);
	}
	
	public function getSalasatribuicoes($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$notids = (isset($queries['notids'])) ? $queries['notids'] : false;
		$idescolasalasaluno = (isset($queries['idescolasalasaluno'])) ? (int)$queries['idescolasalasaluno'] : false;
		$idvinculo = (isset($queries['idvinculo'])) ? (int)$queries['idvinculo'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		$idprofessor = (isset($queries['idprofessor'])) ? (int)$queries['idprofessor'] : false;
		$idsala = (isset($queries['idsala'])) ? (int)$queries['idsala'] : false;
		$idperiodo = (isset($queries['idperiodo'])) ? (int)$queries['idperiodo'] : false;
		$idserie = (isset($queries['idserie'])) ? (int)$queries['idserie'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? $queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$die = (isset($queries['die'])) ? $queries['die'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " es1.id=$id ");
		if (($idescolasalasaluno > 0) && ($notids)) array_push($where, " es1.id NOT IN ($notids) ");
		if ($idescolasalasaluno) array_push($where, " es1.idescolasalasaluno=$idescolasalasaluno ");
		if ($idvinculo) array_push($where, " es1.idvinculo=$idvinculo ");
		if ($idescola) array_push($where, " ea1.idescola=$idescola ");		
		if ($idsecretaria) array_push($where, " e1.idsecretaria=$idsecretaria ");
		if ($idsala) array_push($where, " es1.idsala=$idsala ");
		if ($idperiodo) array_push($where, " es1.idperiodo=$idperiodo ");
		if ($idserie) array_push($where, " es1.idserie=$idserie ");
		if ($idaluno) array_push($where, " es1.idaluno=$idaluno ");
		if ($idprofessor) array_push($where, " ev1.idprofessor=$idprofessor ");
		if ($chave) array_push($where, " ((es1.serie LIKE '%$chave%')) ");
		if ($status) array_push($where, " es1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "es1.*, e1.escola, a1.nomerazao, a1.sobrenomefantasia, a1.idclassificacao, ev1.idtipoensino, ev1.idprofessor, ev1.idescola";		
		if ($total) $fields = "COUNT(es1.id) as total";
		
		
		$ordem = "ORDER BY es1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolassalasatribuicoes es1
						LEFT JOIN escolasvinculos ev1 ON ev1.id=es1.idvinculo
						LEFT JOIN escolassalasalunos ea1 ON ea1.id=es1.idescolasalasaluno 
						LEFT JOIN escolas e1 ON e1.id=ea1.idescola 
						LEFT JOIN escolasalunos a1 ON a1.id = es1.idaluno
					WHERE es1.excluido='nao'
						 AND e1.excluido = 'nao'
						 AND ev1.excluido = 'nao'
						 AND es1.excluido = 'nao'  
						 AND ea1.excluido = 'nao' 
						$w 
					$ordem	
					$limit";	
					//if($die) die($strsql);
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getSalaatribuicaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSalasatribuicoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSalaatribuicaoByIdHelper($id, $queries = array()) {
		$rows = new Escolassalasatribuicoes();
		return $rows->getSalaatribuicaoById($id, $queries);
	}		
	
	
	public function getSalaatribuicaoByIdAluno($id, $queries = array()) {
		if ($id==0) return false;
		//echo $id; die('aaa');
		$queries['idaluno'] = $id;
		$rows = $this->getSalasatribuicoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSalaatribuicaoByIdAlunoHelper($id, $queries = array()) {
		
		$rows = new Escolassalasatribuicoes();
		return $rows->getSalaatribuicaoByIdAluno($id, $queries);
	}

	public static function getAlunosByIdProfessoresHelper($idprofessor, $queries = array()) {
		if((int)$idprofessor < 0) return false;
		$queries['idprofessor'] = $idprofessor; 
		$rows = new Escolassalasatribuicoes();
		return $rows->getSalasatribuicoes($queries);
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Salasatribuicoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 	
			$row->datacriacao = date('Y-m-d G:i:s');	
		}else {
			$novoRegistro = false;
			/*
			$historico = new Escolashistoricos_Escolassalasatribuicoes();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);*/
		} 

		if((int)$id > 0){
			$this->verificasala($id, $dados);
		}		
		$row->idescolasalasaluno = (array_key_exists('idescolasalasaluno',$dados)) ? $dados['idescolasalasaluno'] : $row->idescolasalasaluno;
		$row->idvinculo = (array_key_exists('idvinculo',$dados)) ? $dados['idvinculo'] : $row->idvinculo;
		$row->idserie = (array_key_exists('idserie',$dados)) ? $dados['idserie'] : $row->idserie;
		$row->idsala = (array_key_exists('idsala',$dados)) ? $dados['idsala'] : $row->idsala;
		$row->idperiodo = (array_key_exists('idperiodo',$dados)) ? $dados['idperiodo'] : $row->idperiodo;
		$row->idaluno = (array_key_exists('idaluno',$dados)) ? $dados['idaluno'] : $row->idaluno;
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		/*
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		*/
		$row->save();
		
		/*
		//VERIFICA RA
		$alunos = new Escolasalunos();
		$aluno = $alunos->getEscolaalunoById($row->idaluno);
		if($aluno){
			if((!$aluno['ra']) || ($aluno['ra']=="")){
				$gdae = new Gdae_Integracoes();
				$gdae->Enviarmatriculainfo($row['id']);
			}else {
				$gdae = new Gdae_Integracoes();
				$gdae->Enviarmatriculainfosemra($row['id']);
			}
		}
		*/
		return $row;
	}
	
	private function verificasala($id, $dados = array()){
		$rows = new Escolassalasatribuicoes();
		$row = $rows->getSalaatribuicaoById($id);
		//var_dump($dados); die('aa');
		//var_dump($row); die('bb');		
		if(!$row) return ;

	}
}