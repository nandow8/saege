<?php

class Escolasalunosconsultased extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "escolasalunosconsultased";

    /**
     * Define a chave primaria
     * @var integer
     */


    public static function getEscolasalunosconsultasedHelper($queries = array(), $page = 0, $maxpage = 0) {
        $produtos = new Escolasalunosconsultased();
        return $produtos->getEscolasalunosconsultased($queries, $page, $maxpage);
    }

    
    public function getEscolasalunosconsultased($queries = array(), $page = 0, $maxpage = 0) {

        
        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;

        $w = "";

        if ($w != "")
            $w = "AND ($w)";

        $fields = "ea1.* ";

        
        $ordem = "ORDER BY ea1.id";
        
        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
                        FROM escolasalunosconsultased ea1
                        WHERE ea1.excluido='nao'
                                $w 
                        $ordem	
                        $limit";

        //if(!$total) die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        
        if (false) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEscolaalunoById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getEscolasalunosconsultased($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getEscolaalunoByIdHelper($id, $queries = array()) {
        $rows = new Escolasalunosconsultased();
        return $rows->getEscolaalunoById($id, $queries);
    }

    public static function getNomeByIdHelper($id, $queries = array()) {
        $rows = new Escolasalunosconsultased();
        $usuario = $rows->getEscolaalunoById($id, $queries);
        if (!$usuario)
            return false;
        return $usuario['nomerazao'] . ' ' . $usuario['sobrenomefantasia'];
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasalunosconsultased
     */
    public function save($dados) {

        $novoRegistro = true;
        $integracao = false;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) {
            $row = $this->createRow();
        } else {
            /* $novoRegistro = false;
              $historico = new Escolashistoricos_Escolasalunosconsultased();
              $_historico = $row->toArray();

              $historico->arquiva($_historico);
             */
        }

        $row->status = (array_key_exists('status', $dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido', $dados)) ? $dados['excluido'] : $row->excluido;
        $row->logusuario = (array_key_exists('logusuario', $dados)) ? $dados['logusuario'] : $row->logusuario;
        $row->logdata = (array_key_exists('logdata', $dados)) ? $dados['logdata'] : $row->logdata;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');
        }


        $row->save();

        return $row;
    }

}