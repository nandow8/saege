<?php

/**
 * Define o modelo Educacaoespecialrelatoriodiariolist
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 FJMX.
 * @version     1.0
 */
class Educacaoespecialrelatoriodiariolist extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "educacaoespecialrelatoriodiariolist";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEducacaoespecialrelatoriodiariolistHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$Educacaoespecialrelatoriodiariolist = new Educacaoespecialrelatoriodiariolist();
		return $Educacaoespecialrelatoriodiariolist->getEducacaoespecialrelatoriodiariolist($queries, $page, $maxpage);
	}
	
	public function getEducacaoespecialrelatoriodiariolist($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");
		 
		$idrelatoriodiario = (isset($queries["idrelatoriodiario"])) ? $queries["idrelatoriodiario"] : false;
		if ($idrelatoriodiario) array_push($where, " r1.idrelatoriodiario = $idrelatoriodiario ");
		 
		$titulos = (isset($queries["titulos"])) ? $queries["titulos"] : false;
		if ($titulos) array_push($where, " r1.titulos = $titulos ");
		 
		$datas = (isset($queries["datas"])) ? $queries["datas"] : false;
		if ($datas) array_push($where, " r1.datas = $datas ");
		 
		$evolucoes = (isset($queries["evolucoes"])) ? $queries["evolucoes"] : false;
		if ($evolucoes) array_push($where, " r1.evolucoes = $evolucoes ");
		
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='titulo') $sorting[0]='l1.titulo';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*"; 
		;
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM educacaoespecialrelatoriodiariolist r1
					
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEducacaoespecialrelatoriodiariolistById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEducacaoespecialrelatoriodiariolist($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEducacaoespecialrelatoriodiariolistByIdHelper($id, $queries = array()) {
		$rows = new Educacaoespecialrelatoriodiariolist();
		return $rows->getEducacaoespecialrelatoriodiariolistById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Educacaoespecialrelatoriodiariolist
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idrelatoriodiario = (array_key_exists("idrelatoriodiario",$dados)) ? $dados["idrelatoriodiario"] : $row->idrelatoriodiario;
		$row->titulos = (array_key_exists("titulos",$dados)) ? $dados["titulos"] : $row->titulos;
		$row->datas = (array_key_exists("datas",$dados)) ? $dados["datas"] : $row->datas;
		$row->evolucoes = (array_key_exists("evolucoes",$dados)) ? $dados["evolucoes"] : $row->evolucoes;
		
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
		$row->excluido = 'nao';
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}