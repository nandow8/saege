<?php

/**
 * Define o modelo Comunicacaocomunicados
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Comunicacaocomunicados extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "comunicacaocomunicados";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getComunicacaocomunicadosHelper($queries = array(), $page = 0, $maxpage = 0) {
        $comunicacaocomunicados = new Comunicacaocomunicados();
        return $comunicacaocomunicados->getComunicacaocomunicados($queries, $page, $maxpage);
    }

    public function getComunicacaocomunicados($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;

        /**
         * PESQUISA - Mostrar todos os registros do perfil logado
         * quando não houver nenhum filtro ativo
         */
        $perfilorigem = (isset($queries["perfilorigem"])) ? $queries["perfilorigem"] : false;
        $perfildestino = (isset($queries["perfildestino"])) ? $queries["perfildestino"] : false;
        $escolaorigem = (isset($queries["escolaorigem"])) ? $queries["escolaorigem"] : false;
        $escoladestino = (isset($queries["escoladestino"])) ? $queries["escoladestino"] : false;
        $idusuariocriacao = (isset($queries["idusuariocriacao"])) ? $queries["idusuariocriacao"] : false;
        $idescola = (isset($queries['idescola'])) ? (int) $queries['idescola'] : false;
        $idperfil = Usuarios::getUsuario('idperfil');
       
        $flag = ($perfilorigem || $perfildestino || $escolaorigem || $escoladestino) ? true : false;
        
        if (!$flag AND $idperfil != 30 AND $idperfil !=1) {
            if (($idusuariocriacao) && ($idperfil) && ((int) $idusuariocriacao > 0) && ((int) $idperfil > 0)) {
                if ($idperfil == 29) {
                    array_push($where, " c1.idescolacriacao = $idescola OR c1.idescola = $idescola OR FIND_IN_SET($idescola, c1.idsescolas)");
                }
                array_push($where, " (c1.idperfil = $idperfil OR c1.idperfilcriacao = $idperfil) AND (c1.idusuariocriacao = $idusuariocriacao OR FIND_IN_SET('$idusuariocriacao', c1.idsprofessores)) ");
            }
            
        } else {
            
            if ($perfilorigem AND ($idperfil == 30 OR $idperfil==1)) {

                if ($perfilorigem == $idperfil AND $perfilorigem != 29) {
                    array_push($where, " c1.idusuariocriacao = $idusuariocriacao");
                }
                
                array_push($where, " c1.idperfilcriacao = $perfilorigem");
            } else if ($perfilorigem AND $idperfil != 30 AND $idperfil!=1) {

                if ($perfilorigem == $idperfil) {
                    array_push($where, " (c1.idusuariocriacao = $idusuariocriacao OR FIND_IN_SET('$idusuariocriacao', c1.idsprofessores))");
                }
                
                if ($perfilorigem == 29 AND $idperfil == 29 AND !empty($escolaorigem) AND $escolaorigem != $idescola) {                    
                    array_push($where, " (c1.idescola = $idescola OR FIND_IN_SET($idescola, c1.idsescolas))");
                }

                if ($perfilorigem == Usuarios::getUsuario('idperfil') AND $perfilorigem != 29) {
                    array_push($where, " c1.idperfilcriacao = $idperfil");

                } else if($perfilorigem == Usuarios::getUsuario('idperfil') AND $perfilorigem == 29){
                    array_push($where, " c1.idperfilcriacao = '$perfilorigem'");
                }else
                    array_push($where, " c1.idperfilcriacao = '$perfilorigem' AND FIND_IN_SET('$idusuariocriacao', c1.idsprofessores)");
            }

            if ($perfildestino AND ($idperfil == 30 OR $idperfil == 1)) {

                array_push($where, " c1.idperfil = $perfildestino");
            } else if ($perfildestino AND $idperfil != 30) {
                if ($perfildestino == 29 AND $idperfil == 29 AND !empty($escoladestino) AND $escoladestino != $idescola) {
                    array_push($where, " c1.idescolacriacao = $idescola");
                }

                if ($perfildestino == $idperfil) {
                    array_push($where, " (c1.idusuariocriacao = $idusuariocriacao OR FIND_IN_SET('$idusuariocriacao', c1.idsprofessores))");

                    array_push($where, " (c1.idperfil = $idperfil)");
                } else {
                    array_push($where, " c1.idperfil = $perfildestino");
                    array_push($where, " c1.idperfilcriacao = $idperfil AND c1.idusuariocriacao = $idusuariocriacao");
                }
            }

            if ($escolaorigem) {
                array_push($where, " c1.idescolacriacao = $escolaorigem");
            }
            if ($escoladestino) {
                array_push($where, " c1.idescola = $escoladestino OR FIND_IN_SET($escoladestino, c1.idsescolas)");
            }
        }
        if ($id)
            array_push($where, " c1.id = $id ");

        //$verificaescola = (isset($queries["verificaescola"])) ? $queries["verificaescola"] : false;
        //$verificaperfil = (isset($queries["verificaperfil"])) ? $queries["verificaperfil"] : false;
        //var_dump($where); die();

        $idlocais = (isset($queries["idlocais"])) ? $queries["idlocais"] : false;
        if ($idlocais)
            array_push($where, " c1.idlocais LIKE '%$idlocais%' ");

        $idsdepartamentossecrtaria = (isset($queries["idsdepartamentossecrtaria"])) ? $queries["idsdepartamentossecrtaria"] : false;
        if ($idsdepartamentossecrtaria)
            array_push($where, " c1.idsdepartamentossecrtaria LIKE '%$idsdepartamentossecrtaria%' ");

        $datacriacao = (isset($queries["datacriacao"])) ? date('d/m/Y', strtotime($queries["datacriacao"])) : false;
        if ($datacriacao)
            array_push($where, " c1.datacriacao LIKE '%$datacriacao%'");

        $idsalunos = (isset($queries["idsalunos"])) ? $queries["idsalunos"] : false;
        if ($idsalunos)
            array_push($where, " FIND_IN_SET('$idsalunos', c1.idsalunos) ");

        $idsprofessores = (isset($queries["idsprofessores"])) ? $queries["idsprofessores"] : false;
        if ($idsprofessores)
            array_push($where, " FIND_IN_SET('$idsprofessores', c1.idsprofessores) ");

        $titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
        if ($titulo)
            array_push($where, " c1.titulo LIKE '%$titulo%' ");

        $descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
        if ($descricoes)
            array_push($where, " c1.descricoes = '$descricoes' ");

        $observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
        if ($observacoes)
            array_push($where, " c1.observacoes = '$observacoes' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " c1.status LIKE '%$status%' ");

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "c1.*";
        ;

        if ($total)
            $fields = "COUNT(c1.id) as total";

        $ordem = "ORDER BY c1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields
                    FROM comunicacaocomunicados c1
                    WHERE c1.excluido = 'nao'
                    $w
                    $ordem
                    $limit";
                    
        //if(!$total)die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getComunicacaocomunicadoById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getComunicacaocomunicados($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getComunicacaocomunicadoByIdHelper($id, $queries = array()) {
        $rows = new Comunicacaocomunicados();
        return $rows->getComunicacaocomunicadoById($id, $queries);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Comunicacaocomunicados
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id = $id AND excluido = 'nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idsecretaria = (array_key_exists("idsecretaria", $dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
        $row->idimagem = (array_key_exists("idimagem", $dados)) ? $dados["idimagem"] : $row->idimagem;
        $row->idlocais = (array_key_exists("idlocais", $dados)) ? $dados["idlocais"] : $row->idlocais;
        $row->idsdepartamentossecrtaria = (array_key_exists("idsdepartamentossecrtaria", $dados)) ? $dados["idsdepartamentossecrtaria"] : $row->idsdepartamentossecrtaria;
        $row->idsprofessores = (array_key_exists("idsprofessores", $dados)) ? $dados["idsprofessores"] : $row->idsprofessores;
        $row->idsescolas = (array_key_exists("idsescolas", $dados)) ? $dados["idsescolas"] : $row->idsescolas;
        $row->idsalunos = (array_key_exists("idsalunos", $dados)) ? $dados["idsalunos"] : $row->idsalunos;
        $row->titulo = (array_key_exists("titulo", $dados)) ? $dados["titulo"] : $row->titulo;
        $row->descricoes = (array_key_exists("descricoes", $dados)) ? $dados["descricoes"] : $row->descricoes;
        $row->observacoes = (array_key_exists("observacoes", $dados)) ? $dados["observacoes"] : $row->observacoes;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;

        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }
        $row->idlocal = (array_key_exists("idlocal", $dados)) ? $dados["idlocal"] : $row->idlocal;
        $row->idescola = (array_key_exists("idescola", $dados)) ? $dados["idescola"] : $row->idescola;
        $row->idperfil = (array_key_exists("idperfil", $dados)) ? $dados["idperfil"] : $row->idperfil;

        $row->idusuariocriacao = (array_key_exists("idusuariocriacao", $dados)) ? $dados["idusuariocriacao"] : $row->idusuariocriacao;
        $row->idescolacriacao = (array_key_exists("idescolacriacao", $dados)) ? $dados["idescolacriacao"] : $row->idescolacriacao;
        $row->idperfilcriacao = (array_key_exists("idperfilcriacao", $dados)) ? $dados["idperfilcriacao"] : $row->idperfilcriacao;
        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;

        $row->save();
        
        Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Comunicado ".(($id===0)?"Incluido":"Salvo")." com sucesso!");
        
        return $row;
    }

}
