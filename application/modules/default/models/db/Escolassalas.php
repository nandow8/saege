<?php

/**
 * Define o modelo Escolassalas
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolassalas extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "escolassalas";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getEscolassalasHelper($queries = array(), $page = 0, $maxpage = 0) {
        $escolassalas = new Escolassalas();
        return $escolassalas->getEscolassalas($queries, $page, $maxpage);
    }

    public function getEscolassalas($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " e1.id = $id ");

        $idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
        if ($idsecretaria)
            array_push($where, " e1.idsecretaria = $idsecretaria ");

        $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
        if ($idescola)
            array_push($where, " e1.idescola = $idescola ");

        $sala = (isset($queries["sala"])) ? $queries["sala"] : false;
        if ($sala)
            array_push($where, " e1.sala LIKE '%$sala%' ");

        $quantidade = (isset($queries["quantidade"])) ? $queries["quantidade"] : false;
        if ($quantidade)
            array_push($where, " e1.quantidade = '$quantidade' ");

        $numerosala = (isset($queries["numerosala"])) ? $queries["numerosala"] : false;
        if ($numerosala)
            array_push($where, " e1.numerosala LIKE '%$numerosala%' ");

        $ano = (isset($queries["ano"])) ? $queries["ano"] : false;
        if ($ano)
            array_push($where, " e1.ano = '$ano' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " e1.status LIKE '%$status%' ");

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "e1.*";
        ;

        if ($total)
            $fields = "COUNT(e1.id) as total";

        $ordem = "ORDER BY e1.id ASC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields
					FROM escolassalas e1

					WHERE e1.excluido='nao'
						$w
					$ordem
					$limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEscolasalaById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getEscolassalas($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getEscolasalaByIdHelper($id, $queries = array()) {
        $rows = new Escolassalas();
        return $rows->getEscolasalaById($id, $queries);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolassalas
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idsecretaria = (array_key_exists("idsecretaria", $dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
        $row->idescola = (array_key_exists("idescola", $dados)) ? $dados["idescola"] : $row->idescola;
        $row->sala = (array_key_exists("sala", $dados)) ? $dados["sala"] : $row->sala;
        $row->metragem = (array_key_exists("metragem", $dados)) ? $dados["metragem"] : $row->metragem;
        $row->quantidade = (array_key_exists("quantidade", $dados)) ? $dados["quantidade"] : $row->quantidade;
        $row->numerosala = (array_key_exists("numerosala", $dados)) ? $dados["numerosala"] : $row->numerosala;
        $row->ano = (array_key_exists("ano", $dados)) ? $dados["ano"] : $row->ano;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;

        $row->save();

        return $row;
    }

}
