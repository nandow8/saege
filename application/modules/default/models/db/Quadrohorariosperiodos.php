<?php

/**
 * Define o modelo Quadrohorariosperiodos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Quadrohorariosperiodos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "quadrohorariosperiodos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getQuadrohorariosperiodosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$quadrohorariosperiodos = new Quadrohorariosperiodos();
		return $quadrohorariosperiodos->getQuadrohorariosperiodos($queries, $page, $maxpage);
	}
	
	public function getQuadrohorariosperiodos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$die = (isset($queries['die'])) ? $queries['die'] : false;
		$materia = (isset($queries['materia'])) ? $queries['materia'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " q1.id = $id ");

		$idquadrohorario = (isset($queries['idquadrohorario'])) ? (int)$queries['idquadrohorario'] : false;
		if ($idquadrohorario) array_push($where, " q1.idquadrohorario = $idquadrohorario ");		
		
		$idescolavinculo = (isset($queries["idescolavinculo"])) ? $queries["idescolavinculo"] : false;
		if ($idescolavinculo) array_push($where, " qm1.idescolavinculo = $idescolavinculo ");

		$diferenteidescolavinculo = (isset($queries["diferenteidescolavinculo"])) ? (int)$queries["diferenteidescolavinculo"] : false;
		if ($diferenteidescolavinculo) array_push($where, " q1.idescolavinculo <> $diferenteidescolavinculo ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " ev1.idescola = $idescola ");

		$idmateria = (isset($queries["idmateria"])) ? $queries["idmateria"] : false;
		if ($idmateria) array_push($where, " qm1.idmateria = $idmateria ");

		$diassemana = (isset($queries["diassemana"])) ? $queries["diassemana"] : false;
		if ($diassemana) array_push($where, " q1.diassemana LIKE '%$diassemana%' ");

$periodo = (isset($queries["periodo"])) ? $queries["periodo"] : false;
		if ($periodo) array_push($where, " q1.periodo = '$periodo' ");

$idprofessor = (isset($queries["idprofessor"])) ? $queries["idprofessor"] : false;
		if ($idprofessor) array_push($where, " qm1.idprofessor = $idprofessor ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " q1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "q1.*, qm1.idmateria, qm1.idprofessor"; 
		;
		
		if ($total) $fields = "COUNT(q1.id) as total";
		
		$ordem = "ORDER BY q1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM quadrohorariosperiodos q1 
						LEFT JOIN quadrohorarios qm1 ON qm1.id = q1.idquadrohorario 
						LEFT JOIN escolasvinculos ev1 ON ev1.id=q1.idescolavinculo
						LEFT JOIN escolas e1 ON e1.id=ev1.idescola
						LEFT JOIN escolassalas esa1 ON esa1.id=ev1.idsala 
						LEFT JOIN escolasseries ess1 ON ess1.id = ev1.idserie
						LEFT JOIN escolasturmas et1 ON et1.id=ev1.idturma 
						LEFT JOIN escolasperiodos ep1 ON ep1.id=ev1.idperiodo 
					WHERE q1.excluido='nao' 
						$w 
					$ordem	
					$limit";	

		//echo $strsql;die();
		//if($die) echo $strsql . ' _<br />';
		//if ($diferenteidescolavinculo) die($strsql);
		//if($die) die($strsql);


		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	

		if ($materia){
			$row = $db->fetchAll($strsql);
			//var_dump($row); die();
			$row = (isset($row[0])) ? $row[0] : false;
			return $row;
		}
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getQuadrohorariosperiodoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getQuadrohorariosperiodos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getQuadrohorariosperiodoByIdHelper($id, $queries = array()) {
		$rows = new Quadrohorariosperiodos();
		return $rows->getQuadrohorariosperiodoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Quadrohorariosperiodos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescolavinculo = (array_key_exists("idescolavinculo",$dados)) ? $dados["idescolavinculo"] : $row->idescolavinculo;
 $row->idquadrohorario = (array_key_exists("idquadrohorario",$dados)) ? $dados["idquadrohorario"] : $row->idquadrohorario;
 $row->diassemana = (array_key_exists("diassemana",$dados)) ? $dados["diassemana"] : $row->diassemana;
 $row->periodo = (array_key_exists("periodo",$dados)) ? $dados["periodo"] : $row->periodo;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}