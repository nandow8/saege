<?php

/**
 * Define o modelo Escolasusuarios
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasusuarios extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasusuarios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolaAtiva($id, $field = false) {
		$id = (int)$id;
		$usuarios = new Escolasusuarios();
		$usuario = $usuarios->fetchRow("id=$id");
		
		if (!$usuario) return false;
		
		
		$idescola = (int)$usuario['idescola'];
		
		$escolas = new Escolas();
		$escola = $escolas->fetchRow("id=$idescola");
		if (!$escola) return false;
		
		if (!$field) return $escola;
		
		return $escola[$field];
	}
	
	public function transferAuthFromAdmin($admin_usuario, $cliente) {
		$queries = array();
		
		$queries["id"]=-1*$admin_usuario["id"];
		$queries["idescola"]=$cliente['id'];
		$queries["idescolas"]=$cliente['id'];
		$queries["nomerazao"]=$admin_usuario['nomerazao'];
		$queries["sobrenomefantasia"]=$admin_usuario['sobrenomefantasia'];
		$queries["email"]=$admin_usuario['email'];
		$queries["senha"]=$admin_usuario['senha'];
		$queries["status"]=$admin_usuario['status'];
		$queries["excluido"]="nao";
		$queries["idperfil"]=0;
		$queries["master"]="sim";
		$queries["logusuario"]=NULL;
		$queries["logdata"]=NULL;
		$queries["origem"]="admin";
		
		return $queries;
	}
	
	public static function getUsuario($field = null, $ns = false) {
		$ns = (!$ns) ? Mn_Util::getAdminNameSpace() : $ns;
		
		$session = new Zend_Session_Namespace($ns);
		if (!isset($session->escolausuario)) return false;
		
		$escola = unserialize($session->escolausuario);
		if (is_null($field)) return $escola;
		
		return $escola[$field];		
	}

	/**
	 * 
	 * Cria o texto e envia a senha via email
	 * @param string $email
	 * @param string $senha
	 */
	public static function sendSenha($email, $senha) {
		$body = 'Seus dados de acesso para o painel de controle da<br /> Prefeituta Municipal de Itaquaquecetuba é:<br><br>
			E-mail: '.$email.'<br>
			Senha: '.$senha.'<br><br>
			Para acessar a intranet entre em nosso site:<br>
			<a href="javascript:;">prefeitutamunicipalitaquaquecetuba.com.br/</a><br><br>
			* é recomendável que você altere a sua senha após o recebimento deste email';
		
		Mn_Util::sendMail($email, "Prefeituta Municipal de Itaquaquecetuba - Acesso ao Painel", $body, null);
	}
	
	public function autentica($emailacesso, $senha, $ns = false) {
		$usuario = $this->fetchRow("email='$emailacesso' AND excluido='nao'");

			
		if (!$usuario) return "E-mail ou senha inválidos!";
		if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
		if ($usuario['senha']!=$senha) return "A senha é inválida!";
		
		$usuario = $usuario->toArray();
		
		$perfis = new Escolasusuariosperfis();
		$perfil = $perfis->getPerfilById($usuario['idperfil']);
		
		if($perfil['origem']=="SYS"){
			$idsescolas = explode(',', $usuario['idescolas']);
			if(isset($idsescolas[0])) $usuario['idescola'] = $idsescolas[0];	
			$usuarios = new Escolasusuarios();
			$usuarios->save($usuario);
		}
		
		//var_dump($usuario['idescola']); die('PERFIL - Escolas Usuarios');
		
		if (!$ns) $ns = Mn_Util::getAdminNameSpace();
		$loginNameSpace = new Zend_Session_Namespace($ns);
		
		$escolas = new Escolas();
		$escola = $escolas->getEscolaById($usuario['idescola']);
		
		unset($escola['id']);
		unset($escola['email']);
		$usuario = array_merge($usuario, $escola);
		$usuario['senha'] = $senha;
		
		
		
		if ((int)$usuario['idprefeitura'] <= 0) return "Prefeitura não encontrada!";
		
		$prefeitura = Prefeituras::getPrefeituraByIdHelper((int)$usuario['idprefeitura']);
		if(!$prefeitura) return "O usuário não está vinculado a nenhuma Prefeitura!";
		
		$authprefeitura = $loginNameSpace->prefeituracidade_default;
		/*
		var_dump($authprefeitura['id']);  echo '<br /><br /><br />';
		var_dump($usuario['idprefeitura']); die();*/
		if(($authprefeitura['id'] != $usuario['idprefeitura'])) return "O usuário não está vinculado a está Prefeitura!";
		
		$loginNameSpace->escolausuario = serialize($usuario);
						
		return true;		
	}
	
	public function autenticargf($rgf, $senha, $ns = false) {
		$usuario = $this->fetchRow("rgf='$rgf' AND excluido='nao'");
		//var_dump($usuario->toArray()); die();
			
		if (!$usuario) return "RGF ou senha inválidos!";
		if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
		if ($usuario['senha']!=$senha) return "A senha é inválida!";
		
		$usuario = $usuario->toArray();
		
		$perfis = new Escolasusuariosperfis();
		$perfil = $perfis->getPerfilById($usuario['idperfil']);
		
		if($perfil['origem']=="SYS"){
			$idsescolas = explode(',', $usuario['idescolas']);
			if(isset($idsescolas[0])) $usuario['idescola'] = $idsescolas[0];	
			$usuarios = new Escolasusuarios();
			$usuarios->save($usuario);
		}
		
		//var_dump($usuario['idescola']); die('PERFIL - Escolas Usuarios');
		
		if (!$ns) $ns = Mn_Util::getAdminNameSpace();
		$loginNameSpace = new Zend_Session_Namespace($ns);
		
		$escolas = new Escolas();
		$escola = $escolas->getEscolaById($usuario['idescola']);
		
		unset($escola['id']);
		unset($escola['email']);
		$usuario = array_merge($usuario, $escola);
		$usuario['senha'] = $senha;
		
		
		
		if ((int)$usuario['idprefeitura'] <= 0) return "Prefeitura não encontrada!";
		
		$prefeitura = Prefeituras::getPrefeituraByIdHelper((int)$usuario['idprefeitura']);
		if(!$prefeitura) return "O usuário não está vinculado a nenhuma Prefeitura!";
		
		$authprefeitura = $loginNameSpace->prefeituracidade_default;
		/*
		var_dump($authprefeitura['id']);  echo '<br /><br /><br />';
		var_dump($usuario['idprefeitura']); die();*/
		//if(($authprefeitura['id'] != $usuario['idprefeitura'])) return "O usuário não está vinculado a está Prefeitura!";

		
		$loginNameSpace->escolausuario = serialize($usuario);
						
		return true;		
	}
	
	public static function getEscolasusuariosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasusuarios = new Escolasusuarios();
		return $escolasusuarios->getEscolasusuarios($queries, $page, $maxpage);
	}
	
	public function getEscolasusuarios($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idfuncionario = (isset($queries['idfuncionario'])) ? (int)$queries['idfuncionario'] : false;
		$idprefeitura = (isset($queries['idprefeitura'])) ? (int)$queries['idprefeitura'] : false;
		$diferenteid = (isset($queries['diferenteid'])) ? (int)$queries['diferenteid'] : false;
		$idescola = (isset($queries['idescola'])) ? $queries['idescola'] : false;
		$iddepartamentoescola = (isset($queries['iddepartamentoescola'])) ? $queries['iddepartamentoescola'] : false;
		$idescolas = (isset($queries['idescolas'])) ? $queries['idescolas'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? $queries['idsecretaria'] : false;
		$idcargo = (isset($queries['idcargo'])) ? $queries['idcargo'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$email = (isset($queries['email'])) ? $queries['email'] : false;
		$professor = (isset($queries['professor'])) ? $queries['professor'] : false;
		$rgf = (isset($queries['rgf'])) ? $queries['rgf'] : false;
		$recebesolicitacoes = (isset($queries['recebesolicitacoes'])) ? $queries['recebesolicitacoes'] : false;
		$escola = (isset($queries['escola'])) ? $queries['escola'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		if ($id) array_push($where, " eu1.id=$id ");
		if ($idfuncionario) array_push($where, " eu1.idfuncionario=$idfuncionario ");
		if ($idprefeitura) array_push($where, " eu1.idprefeitura=$idprefeitura ");
		if ($diferenteid) array_push($where, " eu1.id<>$diferenteid ");
		if ($idescola) array_push($where, " eu1.idescola=$idescola ");
		if ($iddepartamentoescola) array_push($where, " c1.iddepartamentoescola=$iddepartamentoescola ");
		if ($idescolas) array_push($where, " FIND_IN_SET('$idescolas', eu1.idescolas) ");
		if ($idcargo) array_push($where, " eu1.idcargo=$idcargo ");
		if ($idsecretaria) array_push($where, " e1.idsecretaria=$idsecretaria ");
		//if ($chave) array_push($where, " (eu1.usuario LIKE '%$chave%' OR ((REPLACE(REPLACE(eu1.cnpj, '.', ''), '-', '') LIKE REPLACE(REPLACE('%$chave%', '.', ''), '-', '')))  ) ");
		if ($chave) array_push($where, " ((eu1.email LIKE '%$chave%') OR (eu1.nomerazao LIKE '%$chave%') OR (eu1.sobrenomefantasia LIKE '%$chave%')) ");
		if ($email) array_push($where, " eu1.email='$email' ");
		if ($status) array_push($where, " eu1.status='$status' ");
		if ($professor) array_push($where, " eu1.professor='$professor' ");
		if ($rgf) array_push($where, " eu1.rgf='$rgf' ");
		if ($escola) array_push($where, " e1.escola LIKE '%$escola%' ");
		if ($recebesolicitacoes) array_push($where, " eu1.recebesolicitacoes='$recebesolicitacoes' ");
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "eu1.*, e1.escola, e1.idendereco as e1idendereco, p1.perfil, c1.cargo, c1.iddepartamentoescola as iddepartamento, d1.departamento";
		if ($total) $fields = "COUNT(eu1.id) as total";
		
		
		$ordem = "ORDER BY eu1.email";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasusuarios eu1
						LEFT JOIN escolas e1 ON e1.id=eu1.idescola
						LEFT JOIN escolasusuariosperfis p1 ON p1.id=eu1.idperfil 
						LEFT JOIN escolascargos c1 ON c1.id=eu1.idcargo
						LEFT JOIN departamentosescolas d1 ON d1.id=c1.iddepartamentoescola
					WHERE eu1.excluido='nao'
						AND e1.excluido = 'nao' 
						AND p1.excluido = 'nao'
						$w 
					$ordem	
					$limit";	
		//if($idfuncionario)die($strsql);
		//if(!$total) echo $strsql;
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public static function getEscolausuarioHelper($id, $queries = array()) {
		$rows = new Escolasusuarios();
		return $rows->getEscolausuarioById($id, $queries);
	}
	
	
	public function getEscolausuarioById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasusuarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	

	public function getEscolausuarioByIdfuncionario($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idfuncionario'] = $id;
		$rows = $this->getEscolasusuarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}

	public static function getEscolausuarioByIdescolaHelper($idescola, $queries = array()) {
		$rows = new Escolasusuarios();
		return $rows->getEscolausuarioByIdescola($idescola, $queries);
	}
	
	public function getEscolausuarioByIdescola($idescola, $queries = array()) {
		if ($idescola==0) return false;
		
		$queries['idescolas'] = $idescola;
		$rows = $this->getEscolasusuarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNomeByIdHelper($id, $queries = array()) {
		$rows = new Escolasusuarios();
		$usuario = $rows->getEscolausuarioById($id, $queries);
		
		return $usuario['nomerazao'] . ' ' . $usuario['sobrenomefantasia'];
	}
	
	public static function getEscolasusuariosByRgfHelper($rgf, $queries = array()) {
		if ((!isset($rgf)) || (!$rgf) || ($rgf=="")) return false;
		$_rows = new Escolasusuarios();
		
		$queries['rgf'] = $rgf;
		$rows = $_rows->getEscolasusuarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasusuarios
     */
	public function save($dados, $ignore_excluido = false) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		if (!$ignore_excluido) {
			$row = $this->fetchRow("id=$id AND excluido='nao'");
		} else {
			$row = $this->fetchRow("id=$id");
		}
		
		if (!$row) {
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s'); 
		}
		else {
			$novoRegistro = false;
		} 
		$row->idfuncionario = (array_key_exists('idfuncionario', $dados)) ? $dados['idfuncionario'] : $row->idfuncionario;
		$row->idescolas = (array_key_exists('idescolas',$dados)) ? $dados['idescolas'] : $row->idescolas;
		$row->idescola = (array_key_exists('idescola', $dados)) ? $dados['idescola'] : $row->idescola;
		$row->idcargo = (array_key_exists('idcargo', $dados)) ? $dados['idcargo'] : $row->idcargo;
		$row->idperfil = (array_key_exists('idperfil', $dados)) ? $dados['idperfil'] : $row->idperfil;
		$row->idprefeitura = (array_key_exists('idprefeitura',$dados)) ? $dados['idprefeitura'] : $row->idprefeitura;
		$row->nomerazao = (array_key_exists('nomerazao', $dados)) ? $dados['nomerazao'] : $row->nomerazao;
		$row->sobrenomefantasia = (array_key_exists('sobrenomefantasia', $dados)) ? $dados['sobrenomefantasia'] : $row->sobrenomefantasia;
		$row->rgf = (array_key_exists('rgf', $dados)) ? $dados['rgf'] : $row->rgf;
		$row->email = (array_key_exists('email', $dados)) ? $dados['email'] : $row->email;
		$row->senha = ((isset($dados['id'])) && (($dados['id']>0)) && ($dados['senha']=='')) ? $row->senha : $dados['senha'];		
		$row->status = (array_key_exists('status', $dados)) ? $dados['status'] : $row->status;
		$row->professor = (array_key_exists('professor', $dados)) ? $dados['professor'] : $row->professor;
		
		$row->funcoes = (array_key_exists('funcoes', $dados)) ? $dados['funcoes'] : $row->funcoes;
		$row->unidadesdesenvolvimento = (array_key_exists('unidadesdesenvolvimento', $dados)) ? $dados['unidadesdesenvolvimento'] : $row->unidadesdesenvolvimento;
		$row->observacoes = (array_key_exists('observacoes', $dados)) ? $dados['observacoes'] : $row->observacoes;
		$row->periodoatividades = (array_key_exists('periodoatividades', $dados)) ? $dados['periodoatividades'] : $row->periodoatividades;
		$row->situacaogestao = (array_key_exists('situacaogestao', $dados)) ? $dados['situacaogestao'] : $row->situacaogestao;
		
		

		$row->enviaemails = (array_key_exists('enviaemails', $dados)) ? $dados['enviaemails'] : $row->enviaemails;
		$row->idsdepartamentos = (array_key_exists('idsdepartamentos', $dados)) ? $dados['idsdepartamentos'] : $row->idsdepartamentos;

		$row->master = (array_key_exists('master', $dados)) ? $dados['master'] : $row->master;
		$row->recebesolicitacoes = (array_key_exists('recebesolicitacoes', $dados)) ? $dados['recebesolicitacoes'] : $row->recebesolicitacoes;

/*$row->codigovinculo = (array_key_exists("codigovinculo",$dados)) ? $dados["codigovinculo"] : $row->codigovinculo;

$row->vinculoempregaticio = (array_key_exists("vinculoempregaticio",$dados)) ? $dados["vinculoempregaticio"] : $row->vinculoempregaticio;
$row->codigodepartamento = (array_key_exists("codigodepartamento",$dados)) ? $dados["codigodepartamento"] : $row->codigodepartamento;
$row->descdepartamento = (array_key_exists("descdepartamento",$dados)) ? $dados["descdepartamento"] : $row->descdepartamento;
$row->desclocal = (array_key_exists("desclocal",$dados)) ? $dados["desclocal"] : $row->desclocal;
$row->idimportacao = (array_key_exists("idimportacao",$dados)) ? $dados["idimportacao"] : $row->idimportacao;*/

		$row->excluido = (array_key_exists('excluido', $dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario', $dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata', $dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();
		
		return $row;
	}
	
	
}