<?php

/**
 * Define o modelo Nutricaorefeicoesingredientes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Nutricaorefeicoesingredientes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "nutricaorefeicoesingredientes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getNutricaorefeicoesingredientesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaorefeicoesingredientes = new Nutricaorefeicoesingredientes();
		return $nutricaorefeicoesingredientes->getNutricaorefeicoesingredientes($queries, $page, $maxpage);
	}
	
	public function getNutricaorefeicoesingredientes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

                $id = (isset($queries['id'])) ? (int)$queries['id'] : false; 
                if ($id) array_push($where, " n1.id = $id "); 

                $idnutricaorefeicao = (isset($queries['idnutricaorefeicao'])) ? (int)$queries['idnutricaorefeicao'] : false; 
                if ($idnutricaorefeicao) array_push($where, " n1.idnutricaorefeicao = $idnutricaorefeicao "); 

                $idnutricaotacoalimento = (isset($queries['idnutricaotacoalimento'])) ? (int)$queries['idnutricaotacoalimento'] : false; 
                if ($idnutricaotacoalimento) array_push($where, " n1.idnutricaotacoalimento = $idnutricaotacoalimento "); 
                
                $descricaotacoalimento = (isset($queries['descricaotacoalimento'])) ? (float)$queries['descricaotacoalimento'] : false; 
                if ($descricaotacoalimento) array_push($where, " n1.descricaotacoalimento LIKE '%$descricaotacoalimento%' ");   

                $quantidade = (isset($queries['quantidade'])) ? (float)$queries['quantidade'] : false; 
                if ($quantidade) array_push($where, " n1.quantidade = $quantidade "); 
                
                $observacao = (isset($queries['observacao'])) ? (float)$queries['observacao'] : false; 
                if ($observacao) array_push($where, " n1.observacao LIKE '%$observacao%' ");                 
                
                $unidade = (isset($queries['unidade'])) ? (float)$queries['unidade'] : false; 
                if ($unidade) array_push($where, " n1.unidade LIKE '%$unidade%' "); 
                
                $status = (isset($queries['status'])) ? $queries['status'] : false; 
                if ($status) array_push($where, " n1.status LIKE '%$status%' "); 
                
                $excluido = (isset($queries['excluido'])) ? $queries['excluido'] : false; 
                if ($excluido) array_push($where, " n1.excluido = $excluido "); 
                
                $logdata = (isset($queries['logdata'])) ? $queries['logdata'] : false; 
                if ($logdata) array_push($where, " n1.logdata = $logdata "); 
                
                $logusuario = (isset($queries['logusuario'])) ? (int)$queries['logusuario'] : false; 
                if ($logusuario) array_push($where, " n1.logusuario = $logusuario "); 
                

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "n1.*"; 
		;
		
		if ($total) $fields = "COUNT(n1.id) as total";
		
		$ordem = "ORDER BY n1.id ASC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM nutricaorefeicoesingredientes n1
					
					WHERE n1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getNutricaotacoalimentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNutricaorefeicoesingredientes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNutricaotacoalimentoByIdHelper($id, $queries = array()) {
		$rows = new Nutricaorefeicoesingredientes();
		return $rows->getNutricaotacoalimentoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaorefeicoesingredientes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
                $row->id = (array_key_exists("id",$dados)) ? $dados["id"] : $row->id;
                $row->idnutricaorefeicao = (array_key_exists("idnutricaorefeicao",$dados)) ? $dados["idnutricaorefeicao"] : $row->idnutricaorefeicao;
                $row->idnutricaotacoalimento = (array_key_exists("idnutricaotacoalimento",$dados)) ? $dados["idnutricaotacoalimento"] : $row->idnutricaotacoalimento;
                $row->descricaotacoalimento = (array_key_exists("descricaotacoalimento",$dados)) ? $dados["descricaotacoalimento"] : $row->descricaotacoalimento;
                $row->quantidade = (array_key_exists("quantidade",$dados)) ? $dados["quantidade"] : $row->quantidade;
                $row->unidade = (array_key_exists("unidade",$dados)) ? $dados["unidade"] : $row->unidade;
                $row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;

		$row->save();

		$registro = 'Adicionado';
		if(!$novoRegistro)
			$registro = 'Editado';

			Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), " id -> [" . $row->id . "] [" . $registro . "][status=>" . $row->status . "][excluido=>" . $row->excluido . "]");
		
		return $row;
	}
	
}