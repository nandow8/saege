<?php

/**
 * Define o modelo Pontosmensais
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Pontosmensais extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "pontosmensais";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getPontosmensaisHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$pontosmensais = new Pontosmensais();
		return $pontosmensais->getPontosmensais($queries, $page, $maxpage);
	}
	
	public function getPontosmensais($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " p1.origem = '$origem' ");
		
		$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " p1.idfuncionario = $idfuncionario ");

$dia_i = (isset($queries["dia_i"])) ? $queries["dia_i"] : false;
		if ($dia_i) array_push($where, " p1.dia >= '$dia_i' ");

$dia_f = (isset($queries["dia_f"])) ? $queries["dia_f"] : false;
		if ($dia_f) array_push($where, " p1.dia <= '$dia_f' ");

$diasemana = (isset($queries["diasemana"])) ? $queries["diasemana"] : false;
		if ($diasemana) array_push($where, " p1.diasemana LIKE '%$diasemana%' ");

$horaentrada = (isset($queries["horaentrada"])) ? $queries["horaentrada"] : false;
		if ($horaentrada) array_push($where, " p1.horaentrada = '$horaentrada' ");

$horasaida = (isset($queries["horasaida"])) ? $queries["horasaida"] : false;
		if ($horasaida) array_push($where, " p1.horasaida = '$horasaida' ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " p1.observacoes = '$observacoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM pontosmensais p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPontosmensalById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPontosmensais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPontosmensalByIdHelper($id, $queries = array()) {
		$rows = new Pontosmensais();
		return $rows->getPontosmensalById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Pontosmensais
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		$row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
		 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
		 $row->idfuncionarioescola = (array_key_exists("idfuncionarioescola",$dados)) ? $dados["idfuncionarioescola"] : $row->idfuncionarioescola;
 $row->dia = (array_key_exists("dia",$dados)) ? $dados["dia"] : $row->dia;
 $row->diasemana = (array_key_exists("diasemana",$dados)) ? $dados["diasemana"] : $row->diasemana;
 $row->horaentrada = (array_key_exists("horaentrada",$dados)) ? $dados["horaentrada"] : $row->horaentrada;
 $row->horasaida = (array_key_exists("horasaida",$dados)) ? $dados["horasaida"] : $row->horasaida;
 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}