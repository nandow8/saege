<?php

/**
 * Define o modelo Protocolosprocessostramitacoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Protocolosprocessostramitacoes extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "protocolosprocessostramitacoes";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getProtocolosprocessostramitacoesHelper($queries = array(), $page = 0, $maxpage = 0) {
        $protocolosprocessostramitacoes = new Protocolosprocessostramitacoes();
        return $protocolosprocessostramitacoes->getProtocolosprocessostramitacoes($queries, $page, $maxpage);
    }

    public function getProtocolosprocessostramitacoes($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " p1.id = $id ");

        $idprotocoloprocesso = (isset($queries['idprotocoloprocesso'])) ? (int) $queries['idprotocoloprocesso'] : false;
        if ($idprotocoloprocesso)
            array_push($where, " p1.idprotocoloprocesso = '$idprotocoloprocesso' ");

        $idusuario = (isset($queries['idusuario'])) ? (int) $queries['idusuario'] : false;
        if ($idusuario)
            array_push($where, " p1.idusuario = '$idusuario' ");

        $idfuncionario = (isset($queries['idfuncionario'])) ? (int) $queries['idfuncionario'] : false;
        if ($idfuncionario)
            array_push($where, " p1.idfuncionario = '$idfuncionario' ");

        $idperfilorigem = (isset($queries['idperfilorigem'])) ? (int) $queries['idperfilorigem'] : false;
        if ($idperfilorigem)
            array_push($where, " p1.idperfilorigem = '$idperfilorigem' ");

        $idlocalorigem = (isset($queries['idlocalorigem'])) ? (int) $queries['idlocalorigem'] : false;
        if ($idlocalorigem)
            array_push($where, " p1.idlocalorigem = '$idlocalorigem' ");

        $idescolaorigem = (isset($queries['idescolaorigem'])) ? (int) $queries['idescolaorigem'] : false;
        if ($idescolaorigem)
            array_push($where, " p1.idescolaorigem = '$idescolaorigem' ");

        $idusuarioorigem = (isset($queries['idusuarioorigem'])) ? (int) $queries['idusuarioorigem'] : false;
        if ($idusuarioorigem)
            array_push($where, " p1.idusuarioorigem = '$idusuarioorigem' ");

        $idperfildestino = (isset($queries['idperfildestino'])) ? (int) $queries['idperfildestino'] : false;
        if ($idperfildestino)
            array_push($where, " p1.idperfildestino = '$idperfildestino' ");

        $idlocaldestino = (isset($queries['idlocaldestino'])) ? (int) $queries['idlocaldestino'] : false;
        if ($idlocaldestino)
            array_push($where, " p1.idlocaldestino = '$idlocaldestino' ");

        $idescoladestino = (isset($queries['idescoladestino'])) ? (int) $queries['idescoladestino'] : false;
        if ($idescoladestino)
            array_push($where, " p1.idescoladestino = '$idescoladestino' ");

        $idusuariodestino = (isset($queries['idusuariodestino'])) ? (int) $queries['idusuariodestino'] : false;
        if ($idusuariodestino)
            array_push($where, " p1.idusuariodestino = '$idusuariodestino' ");

        $datacriacao_i = (isset($queries['datacriacao_i'])) ? $queries['datacriacao_i'] : false;
        if ($datacriacao_i)
            array_push($where, " p1.datacriacao >= '$datacriacao_i' ");

        $datacriacao_f = (isset($queries['datacriacao_f'])) ? $queries['datacriacao_f'] : false;
        if ($datacriacao_f)
            array_push($where, " p1.datacriacao <= '$datacriacao_f' ");

        $observacao = (isset($queries['observacao'])) ? $queries['observacao'] : false;
        if ($observacao)
            array_push($where, " p1.observacao LIKE '%$observacao%' ");

        $excluido = (isset($queries['excluido'])) ? $queries['excluido'] : false;
        if ($excluido)
            array_push($where, " p1.excluido LIKE '%$excluido%' ");

        $status = (isset($queries['status'])) ? $queries['status'] : false;
        if ($status)
            array_push($where, " p1.status = '$status' ");

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "p1.*";
        ;

        if ($total)
            $fields = "COUNT(p1.id) as total";

        $ordem = "ORDER BY p1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields,
                                  (select count(COALESCE(p2.id,0)) from protocolosprocessostramitacoesarquivos p2 where p1.id = p2.idprotocolosprocessostramitacoes) as arquivios  
                                    FROM protocolosprocessostramitacoes p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";


        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getProtocoloprocessotramitacaoById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getProtocolosprocessostramitacoes($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getProtocoloprocessotramitacaoByIdHelper($id, $queries = array()) {
        $rows = new Protocolosprocessostramitacoes();
        return $rows->getProtocoloprocessotramitacaoById($id, $queries);
    }

    public function getUltimoProtocoloprocessotramitacao($queries = array()) {
        $queries['order'] = 'ORDER BY p1.id DESC';
        $rows = $this->getProtocolosprocessostramitacoes($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Protocolosprocessostramitacoes
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idprotocoloprocesso = (array_key_exists("idprotocoloprocesso", $dados)) ? $dados["idprotocoloprocesso"] : $row->idprotocoloprocesso;
        $row->idusuario = (array_key_exists("idusuario", $dados)) ? $dados["idusuario"] : $row->idusuario;
        $row->idfuncionario = (array_key_exists("idfuncionario", $dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
        $row->idperfilorigem = (array_key_exists("idperfilorigem", $dados)) ? $dados["idperfilorigem"] : $row->idperfilorigem;
        $row->idlocalorigem = (array_key_exists("idlocalorigem", $dados)) ? $dados["idlocalorigem"] : $row->idlocalorigem;
        $row->idescolaorigem = (array_key_exists("idescolaorigem", $dados)) ? $dados["idescolaorigem"] : $row->idescolaorigem;
        $row->idusuarioorigem = (array_key_exists("idusuarioorigem", $dados)) ? $dados["idusuarioorigem"] : $row->idusuarioorigem;
        $row->idperfildestino = (array_key_exists("idperfildestino", $dados)) ? $dados["idperfildestino"] : $row->idperfildestino;
        $row->idlocaldestino = (array_key_exists("idlocaldestino", $dados)) ? $dados["idlocaldestino"] : $row->idlocaldestino;
        $row->idescoladestino = (array_key_exists("idescoladestino", $dados)) ? $dados["idescoladestino"] : $row->idescoladestino;
        $row->idusuariodestino = (array_key_exists("idusuariodestino", $dados)) ? $dados["idusuariodestino"] : $row->idusuariodestino;
        $row->datacriacao = (array_key_exists("datacriacao", $dados)) ? $dados["datacriacao"] : $row->datacriacao;
        $row->observacao = (array_key_exists("observacao", $dados)) ? $dados["observacao"] : $row->observacao;
        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        $row->iddocumentohistorico = (array_key_exists("iddocumentohistorico", $dados)) ? $dados["iddocumentohistorico"] : $row->iddocumentohistorico;

        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        
        $row->save();

        return $row;
    }

}
