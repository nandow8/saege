<?php

class Escolasalunosassociairmao extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "escolasalunosassociairmao";

    /**
     * Define a chave primaria
     * @var integer
     */

    public static function getIrmaosAssociadosByIdAlunoHelper($id){
        $queries = array();
        if(!$id) return false;
        
        $queries["idaluno"] = $id;
        $associairmao = new Escolasalunosassociairmao();
        return $associairmao->getIrmaosAssociados($queries);
    }
    
    public function getIrmaosAssociadosById($id, $queries = array()){
        if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getIrmaosAssociados($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
    }

    public static function getEscolasalunosassociairmaoByIdHelper($id, $queries = array()){
        $rows = new Escolasalunosassociairmao();
        return $rows->getIrmaosAssociadosById($id, $queries);
    }


    public function getIrmaosAssociados($queries = array(), $page = 0, $maxpage = 0){
        $where = array();
			
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        

        $id = (isset($queries['id'])) ? $queries['id'] : false;
            if($id) array_push($where, " ea1.id = $id");

        $idaluno = (isset($queries['idaluno'])) ? $queries['idaluno'] : false;
            if($idaluno) array_push($where, " ea1.idaluno = $idaluno");

        $inRA = (isset($queries['inRA'])) ? $queries['inRA'] : false;
            if($inRA) array_push($where, " ea1.inRA = '$inRA'");

        $inDigitoRA = (isset($queries['inDigitoRA'])) ? $queries['inDigitoRA'] : false;
            if($inDigitoRA) array_push($where, " ea1.inDigitoRA = '$inDigitoRA'");
        $inIrmaoRA = (isset($queries['inIrmaoRA'])) ? $queries['inIrmaoRA'] : false;
            if($inIrmaoRA) array_push($where, " ea1.inIrmaoRA = '$inIrmaoRA'");
        $inIrmaoDigitoRA = (isset($queries['inIrmaoDigitoRA'])) ? $queries['inIrmaoDigitoRA'] : false;
            if($inIrmaoDigitoRA) array_push($where, " ea1.inIrmaoDigitoRA = '$inIrmaoDigitoRA'");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "ea1.* ";

        $ordem = "ORDER BY ea1.id";
        if ($order) $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields
                FROM escolasalunosassociairmao ea1
                WHERE ea1.id is not null
                $w
                $ordem
                $limit";
     //echo $strsql;
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        
        if (false) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }
    public function getUltimoEscolasalunosassociairmao($queries = array()) {
		$queries['order'] = 'ORDER BY ea1.id DESC';
		$rows = $this->getIrmaosAssociados($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
    /**
     * Excluir dados
     * @param int $id
     */
    public function excluirEscolasalunosassociairmaoById($id){

        $strsql = "DELETE FROM escolasalunosassociairmao WHERE id = $id";
        
        $db = Zend_Registry::get('db');
      
        return $db->query($strsql);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasalunosassociairmao
     */
    public function save($dados) {
        $novoRegistro = true;
        
        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id");
        
        if (!$row) {
            $row = $this->createRow();
        }
       
        $row->idaluno = (array_key_exists('idaluno', $dados)) ? $dados['idaluno'] : $row->idaluno;
        $row->inRA = (array_key_exists('ra', $dados)) ? $dados['ra'] : $row->ra;       
        $row->inDigitoRA = (array_key_exists('digra', $dados)) ? $dados['digra'] : $row->digra;
        $row->inUFRA = (array_key_exists('ufra', $dados)) ? $dados['ufra'] : $row->ufra;
        $row->inIrmaoRA = (array_key_exists('inIrmaoRA', $dados)) ? $dados['inIrmaoRA'] : $row->inIrmaoRA;
        $row->inIrmaoDigitoRA = (array_key_exists('inIrmaoDigitoRA', $dados)) ? $dados['inIrmaoDigitoRA'] : $row->inIrmaoDigitoRA;
        $row->inIrmaoUFRA = (array_key_exists('inIrmaoUFRA', $dados)) ? $dados['inIrmaoUFRA'] : $row->inIrmaoUFRA;
        $row->inGemeo = (array_key_exists('inGemeo', $dados)) ? $dados['inGemeo'] : $row->inGemeo;
        $row->inNome = (array_key_exists('inNome', $dados)) ? $dados['inNome'] : $row->inNome;
        $row->inMae = (array_key_exists('inMae', $dados)) ? $dados['inMae'] : $row->inMae;
        $row->inPai = (array_key_exists('inPai', $dados)) ? $dados['inPai'] : $row->inPai;
        $row->inNascimento = (array_key_exists('inNascimento', $dados)) ? $dados['inNascimento'] : $row->inNascimento;
                
        $row->logusuario = (array_key_exists('logusuario', $dados)) ? $dados['logusuario'] : $row->logusuario;
        $row->logdata = (array_key_exists('logdata', $dados)) ? $dados['logdata'] : $row->logdata;

        $row->soapstatus = (array_key_exists("soapstatus", $dados)) ? $dados["soapstatus"] : $row->soapstatus;
        $row->soaptipo = (array_key_exists("soaptipo", $dados)) ? $dados["soaptipo"] : $row->soaptipo;
        $row->soapretorno = (array_key_exists("soapretorno", $dados)) ? $dados["soapretorno"] : $row->soapretorno;
        
        $row->save();

        return $row;
    }

    }
?>