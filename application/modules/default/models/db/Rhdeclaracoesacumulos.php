<?php

/**
 * Define o modelo Rhdeclaracoesacumulos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Rhdeclaracoesacumulos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "rhdeclaracoesacumulos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_DOMINGO = '1';
	public static $_SEGUNDA = '2';
	public static $_TERCA = '3';
	public static $_QUARTA = '4';
	public static $_QUINTA = '5';
	public static $_SEXTA = '6';
	public static $_SABADO = '7';	
	
	
	public static function getDiasSemana($field = false) {
		$res = array(
			self::$_DOMINGO => 'Domingo',
			self::$_SEGUNDA => 'Segunda Feira',
			self::$_TERCA => 'Terça Feira',
			self::$_QUARTA => 'Quarta Feira',
			self::$_QUINTA => 'Quinta Feira',
			self::$_SEXTA => 'Sexta Feira',
			self::$_SABADO => 'Sábado'
		);
		
		if (!$field) return $res;
		return $res[$field];
	}
	
	public static function getRhdeclaracoesacumulosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$rhdeclaracoesacumulos = new Rhdeclaracoesacumulos();
		return $rhdeclaracoesacumulos->getRhdeclaracoesacumulos($queries, $page, $maxpage);
	}
	
	public function getRhdeclaracoesacumulos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");
		
		
		$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " r1.idfuncionario = $idfuncionario ");
		
		$idfuncionarioescola = (isset($queries["idfuncionarioescola"])) ? $queries["idfuncionarioescola"] : false;
		if ($idfuncionarioescola) array_push($where, " r1.idfuncionarioescola = $idfuncionarioescola ");

		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " r1.origem = '$origem' ");

$acumulo = (isset($queries["acumulo"])) ? $queries["acumulo"] : false;
		if ($acumulo) array_push($where, " r1.acumulo LIKE '%$acumulo%' ");

$instituicaoempresa = (isset($queries["instituicaoempresa"])) ? $queries["instituicaoempresa"] : false;
		if ($instituicaoempresa) array_push($where, " r1.instituicaoempresa LIKE '%$instituicaoempresa%' ");

$diassemana = (isset($queries["diassemana"])) ? $queries["diassemana"] : false;
		if ($diassemana) array_push($where, " r1.diassemana LIKE '%$diassemana%' ");

$horainicio = (isset($queries["horainicio"])) ? $queries["horainicio"] : false;
		if ($horainicio) array_push($where, " r1.horainicio = '$horainicio' ");

$horafim = (isset($queries["horafim"])) ? $queries["horafim"] : false;
		if ($horafim) array_push($where, " r1.horafim = '$horafim' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " r1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*, f1.nome, f1.rg, f1.cpf, f1.estadocivil, f1.conjugenome, f1.grauinstrucao, f1.dataadmissao, f1.cargo, f1.idendereco, f1.telefone, f1.celular, f1.salarioinicial"; 
		;
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM rhdeclaracoesacumulos r1
						LEFT JOIN funcionariosgerais f1 ON f1.id = r1.idfuncionario 
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getRhdeclaracaoacumulosById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getRhdeclaracoesacumulos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getRhdeclaracaoacumulosByIdHelper($id, $queries = array()) {
		$rows = new Rhdeclaracoesacumulos();
		return $rows->getRhdeclaracaoacumulosById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Rhdeclaracoesacumulos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
 $row->idfuncionarioescola = (array_key_exists("idfuncionarioescola",$dados)) ? $dados["idfuncionarioescola"] : $row->idfuncionarioescola;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->acumulo = (array_key_exists("acumulo",$dados)) ? $dados["acumulo"] : $row->acumulo;
 $row->instituicaoempresa = (array_key_exists("instituicaoempresa",$dados)) ? $dados["instituicaoempresa"] : $row->instituicaoempresa;
 $row->diassemana = (array_key_exists("diassemana",$dados)) ? $dados["diassemana"] : $row->diassemana;
 $row->horainicio = (array_key_exists("horainicio",$dados)) ? $dados["horainicio"] : $row->horainicio;
 $row->horafim = (array_key_exists("horafim",$dados)) ? $dados["horafim"] : $row->horafim;
 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}