<?php

class Escolasfrotas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasfrotas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getEscolasfrotasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolasfrotas();
		return $produtos->getEscolasfrotas($queries, $page, $maxpage);
	}
	
	public function getEscolasfrotas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " ef1.id=$id ");	

		$notids = (isset($queries['notids'])) ? $queries['notids'] : false;
		if ($notids) array_push($where, " NOT FIND_IN_SET (ef1.id, '$notids')");

		if ($chave) array_push($where, " ((ef1.resumo LIKE '%$chave%') OR (ef1.descricoes LIKE '%$chave%')) ");
		
		

		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " ef1.descricoes LIKE '%$descricoes%' ");

		$idfrotas = (isset($queries['idfrotas'])) ? $queries['idfrotas'] : false;
		if ($idfrotas) array_push($where, " ef1.idfrotas='$idfrotas' ");


		if ($status) array_push($where, " ef1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "ef1.*, lf1.anofabricacao as ano, lf1.cor as modelo, lf1.placa, m1.marca, f1.fabricante";
		
		if ($total) $fields = "COUNT(ef1.id) as total";
		
		$ordem = "ORDER BY ef1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasfrotas ef1 
					 LEFT JOIN logisticafrotas lf1 ON lf1.id = ef1.idfrotas
					 LEFT JOIN marcas m1 ON m1.id = lf1.idmarca 
					 LEFT JOIN fabricantes f1 ON f1.id = lf1.idfabricante
					WHERE ef1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getEscolasfrotaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasfrotas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolasfrotaByIdHelper($id, $queries = array()) {
		$rows = new Escolasfrotas();
		return $rows->getEscolasfrotaById($id, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasfrotas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row){
                    $row = $this->createRow();
                    $row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->idfrotas = (array_key_exists('idfrotas',$dados)) ? $dados['idfrotas'] : $row->idfrotas;	
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idendereco;	
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;

		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
	
	
}