<?php

/**
 * Define o modelo Funcionariosgeraisescolas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosgeraisescolas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosgeraisescolas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFuncionariosgeraisescolasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosgeraisescolas = new Funcionariosgeraisescolas();
		return $funcionariosgeraisescolas->getFuncionariosgeraisescolas($queries, $page, $maxpage);
	}
	
	public function getFuncionariosgeraisescolas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$die = (isset($queries['die'])) ? $queries['die'] : false;
		$meuid = (isset($queries['meuid'])) ? $queries['meuid'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " f1.idsecretaria = $idsecretaria ");



		$idperfil = (isset($queries["idperfil"])) ? $queries["idperfil"] : false;
		if ($idperfil) array_push($where, " f1.idperfil = $idperfil ");

		$diferenteidperfil = (isset($queries["diferenteidperfil"])) ? $queries["diferenteidperfil"] : false;
		if ($diferenteidperfil) array_push($where, " (f1.idperfil <> '$diferenteidperfil' OR f1.idperfil IS NULL OR f1.idperfil = '') ");

		$idsfuncionarios = (isset($queries["idsfuncionarios"])) ? $queries["idsfuncionarios"] : false;
		if ($idsfuncionarios) array_push($where, " FIND_IN_SET ('$idsfuncionarios', f1.idsfuncionarios) ");

		$idsfuncionariossel = (isset($queries["idsfuncionariossel"])) ? $queries["idsfuncionariossel"] : false;

		$idhorarioponto = (isset($queries["idhorarioponto"])) ? $queries["idhorarioponto"] : false;
		if ($idhorarioponto) array_push($where, " f1.idhorarioponto = $idhorarioponto ");

		$idsfuncionariosgerais = (isset($queries["idsfuncionariosgerais"])) ? $queries["idsfuncionariosgerais"] : false;
		if ($idsfuncionariosgerais) array_push($where, " (FIND_IN_SET(f1.id,'$idsfuncionariosgerais')) ");

		$idsperfis = (isset($queries["idsperfis"])) ? $queries["idsperfis"] : false;
		if ($idsperfis) array_push($where, " ((FIND_IN_SET ('$idsperfis', f1.idsperfis)) OR (f1.idperfil = $idsperfis)) ");

		$idsperfis_null = (isset($queries["idsperfis_null"])) ? $queries["idsperfis_null"] : false;
		if ($idsperfis_null) array_push($where, " (((f1.idsperfis IS NULL) OR(f1.idsperfis = '')) AND (f1.idperfil = $idsperfis_null)) ");

		$diferenteidsperfis = (isset($queries["diferenteidsperfis"])) ? $queries["diferenteidsperfis"] : false;
		if ($diferenteidsperfis) array_push($where, " NOT FIND_IN_SET ('$diferenteidsperfis', f1.idsperfis) ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;

		$idescolas = (isset($queries["idescolas"])) ? $queries["idescolas"] : false;
		if ($idescolas) array_push($where, " FIND_IN_SET ('$idescolas', f1.idescolas) ");

		$idtipoequipe = (isset($queries["idtipoequipe"])) ? $queries["idtipoequipe"] : false;
		if ($idtipoequipe) array_push($where, " f1.idtipoequipe = $idtipoequipe ");

		$modulo = (isset($queries["modulo"])) ? $queries["modulo"] : false;
		if ($modulo) array_push($where, " f1.modulo LIKE '%$modulo%' ");

		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " f1.iddepartamento = $iddepartamento ");

		$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
		if ($nome) array_push($where, " f1.nome LIKE '%$nome%' ");

		$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
		if ($telefone) array_push($where, " f1.telefone LIKE '%$telefone%' ");
		
		$idlocal = (isset($queries["idlocal"])) ? $queries["idlocal"] : false;
		if ($idlocal) array_push($where, " f1.idlocal = '$idlocal' ");

		$professor = (isset($queries["professor"])) ? $queries["professor"] : false;
		if ($professor) array_push($where, " f1.professor LIKE '%$professor%' ");

		$coordenador = (isset($queries["coordenador"])) ? $queries["coordenador"] : false;
		if ($coordenador) array_push($where, " f1.coordenador LIKE '%$coordenador%' ");

		$diretor = (isset($queries["diretor"])) ? $queries["diretor"] : false;
		if ($diretor) array_push($where, " f1.diretor LIKE '%$diretor%' ");

		$ndiretor = (isset($queries["ndiretor"])) ? $queries["ndiretor"] : false;
		if ($ndiretor) array_push($where, " (f1.diretor != '$ndiretor' || f1.diretor IS NULL) ");

		$tipoensino = (isset($queries["tipoensino"])) ? $queries["tipoensino"] : false;
		if ($tipoensino) array_push($where, " f1.tipoensino LIKE '%$tipoensino%' ");		

		$rgf = (isset($queries["rgf"])) ? $queries["rgf"] : false;
		if ($rgf) array_push($where, " f1.rgf = '$rgf' ");		

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");

		$pertenceescola = (isset($queries["pertenceescola"])) ? $queries["pertenceescola"] : false;
		if ($pertenceescola) array_push($where, " f1.pertenceescola LIKE '%$pertenceescola%' ");

		$desclocal = (isset($queries["desclocal"])) ? $queries["desclocal"] : false;
		if ($desclocal) array_push($where, " f1.desclocal LIKE '%$desclocal%' ");
                
        $coordenacao = (isset($queries["coordenacao"])) ? $queries["coordenacao"] : false;

        $meusfuncionarios_or = (isset($queries["meusfuncionarios_or"])) ? $queries["meusfuncionarios_or"] : false;

        if(($meusfuncionarios_or) && ($idsfuncionariossel)){
        	if ($idescola) array_push($where, " (f1.idescola = $idescola OR f1.id IN ($idsfuncionariossel)) ");
        }else{
			if(($meuid) && ($idsfuncionariossel)){
				if ($idsfuncionariossel) array_push($where, " (f1.id IN ($idsfuncionariossel) OR (FIND_IN_SET(f1.idsfuncionarios, $meuid))) ");
			}else{
				if ($idsfuncionariossel) array_push($where, " f1.id IN ($idsfuncionariossel) ");
			}
			
			if ($idescola) array_push($where, " f1.idescola = $idescola ");
			
        }


$possuimultiplasescolas = (isset($queries["possuimultiplasescolas"])) ? $queries["possuimultiplasescolas"] : false;
		if ($possuimultiplasescolas) array_push($where, " (f1.idescolas IS NOT NULL AND f1.idescolas <> '') ");

$possuimultiplosfuncionarios = (isset($queries["possuimultiplosfuncionarios"])) ? $queries["possuimultiplosfuncionarios"] : false;
		if ($possuimultiplosfuncionarios) array_push($where, " (f1.idsfuncionarios IS NOT NULL AND f1.idsfuncionarios <> '') ");


		
		$ids = (isset($queries["ids"])) ? $queries["ids"] : false;
		if ($ids) array_push($where, " f1.id IN ($ids) ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
				
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";
                
		$fields = "f1.*"; 
		$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado ";
		
		if ($total){
                    $fields = "COUNT(f1.id) as total";
                };

				$ordem = " ORDER BY f1.nome ";
				
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgeraisescolas f1
					 LEFT JOIN enderecos e_idendereco ON e_idendereco.id=f1.idendereco 
						LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado
						".(($coordenacao) ? " LEFT JOIN departamentossecretarias ds ON ds.id=f1.iddepartamento " : "")."
					WHERE f1.excluido='nao' ".(($coordenacao) ? " AND ds.departamento='Coordenação' " : "")."
						$w 
					$ordem	
					$limit";
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		 
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariogeralescolaById($id, $queries = array()){
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosgeraisescolas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		return $rows[0];
	}
	
	public static function getFuncionariogeralescolaByIdHelper($id, $queries = array()) {
		$rows = new Funcionariosgeraisescolas();
		return $rows->getFuncionariogeralescolaById($id, $queries);
	}		
	
	public static function getFuncionariogeraisescolaByRgfHelper($rgf, $queries = array()) {
		if ((!isset($rgf)) || (!$rgf) || ($rgf=="")) return false;
		$_rows = new Funcionariosgeraisescolas();
		
		$queries['rgf'] = $rgf;
		$rows = $_rows->getFuncionariosgeraisescolas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Funcionariosgeraisescolas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		

		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");

		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
			$historico = new Funcionariosgeraisescolashistoricos();	

			$_historico = $row->toArray();
			//$_historico['idhorarioponto'] = (int)$dados['idhorarioponto'];

			$historico->arquiva($_historico);
		} 
		
		//print_r($dados['id']);
		//die();

                $row->idsecretaria = (int)(array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
                $row->idescola = (int)(array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
                $row->idescolas = (array_key_exists("idescolas",$dados)) ? $dados["idescolas"] : $row->idescolas;


                $row->idperfil = (int)(array_key_exists("idperfil",$dados)) ? (int)$dados["idperfil"] : (int)$row->idperfil;
                $row->idsperfis = (array_key_exists("idsperfis",$dados)) ? $dados["idsperfis"] : $row->idsperfis;
                $row->idsfuncionarios = (array_key_exists("idsfuncionarios",$dados)) ? $dados["idsfuncionarios"] : $row->idsfuncionarios;
		 
                $row->iddepartamentoescola = (int)(array_key_exists("iddepartamentoescola",$dados)) ? (int)$dados["iddepartamentoescola"] : (int)$row->iddepartamentoescola;
                $row->idimagem = (int)(array_key_exists("idimagem",$dados)) ? (int)$dados["idimagem"] : (int)$row->idimagem;

                $row->idfuncao = (int)(array_key_exists("idfuncao",$dados)) ? (int)$dados["idfuncao"] : (int)$row->idfuncao;
                $row->idcargo = (int)(array_key_exists("idcargo",$dados)) ? (int)$dados["idcargo"] : (int)$row->idcargo;
                $row->idtipoequipe = (array_key_exists("idtipoequipe",$dados)) ? (int)$dados["idtipoequipe"] : (int)$row->idtipoequipe;
                $row->idlocal = (int)(array_key_exists("idlocal",$dados)) ? (int)$dados["idlocal"] : (int)$row->idlocal;

                $row->datafimbolsa = (array_key_exists("datafimbolsa",$dados)) ? $dados["datafimbolsa"] : $row->datafimbolsa;
                $row->datainiciobolsa = (array_key_exists("datainiciobolsa",$dados)) ? $dados["datainiciobolsa"] : $row->datainiciobolsa;
                $row->bolsista = (array_key_exists("bolsista",$dados)) ? $dados["bolsista"] : $row->bolsista;
                $row->regime_outros = (array_key_exists("regime_outros",$dados)) ? $dados["regime_outros"] : $row->regime_outros;
                $row->regime = (array_key_exists("regime",$dados)) ? $dados["regime"] : $row->regime;
                $row->conta = (array_key_exists("conta",$dados)) ? $dados["conta"] : $row->conta;
                $row->dataregistroctps = (array_key_exists("dataregistroctps",$dados)) ? $dados["dataregistroctps"] : $row->dataregistroctps;
                $row->ctps = (array_key_exists("ctps",$dados)) ? $dados["ctps"] : $row->ctps;
                $row->filiacaopai = (array_key_exists("filiacaopai",$dados)) ? $dados["filiacaopai"] : $row->filiacaopai;
                $row->filiacaomae = (array_key_exists("filiacaomae",$dados)) ? $dados["filiacaomae"] : $row->filiacaomae;
                $row->carteiraserie = (array_key_exists("carteiraserie",$dados)) ? $dados["carteiraserie"] : $row->carteiraserie;
                $row->carteiranumero = (array_key_exists("carteiranumero",$dados)) ? $dados["carteiranumero"] : $row->carteiranumero;
                $row->celular = (array_key_exists("celular",$dados)) ? $dados["celular"] : $row->celular;
                $row->agencia = (array_key_exists("agencia",$dados)) ? $dados["agencia"] : $row->agencia;
                $row->banco = (array_key_exists("banco",$dados)) ? $dados["banco"] : $row->banco;
                $row->formapagamento = (array_key_exists("formapagamento",$dados)) ? $dados["formapagamento"] : $row->formapagamento;
                $row->salarioinicial = (array_key_exists("salarioinicial",$dados)) ? $dados["salarioinicial"] : $row->salarioinicial;
                $row->riscofuncao = (array_key_exists("riscofuncao",$dados)) ? $dados["riscofuncao"] : $row->riscofuncao;
                $row->descricaofuncao = (array_key_exists("descricaofuncao",$dados)) ? $dados["descricaofuncao"] : $row->descricaofuncao;
                $row->funcao = (array_key_exists("funcao",$dados)) ? $dados["funcao"] : $row->funcao;
                $row->setor = (array_key_exists("setor",$dados)) ? $dados["setor"] : $row->setor;
                $row->secretaria = (array_key_exists("secretaria",$dados)) ? $dados["secretaria"] : $row->secretaria;
                $row->matricula = (array_key_exists("matricula",$dados)) ? $dados["matricula"] : $row->matricula;
                $row->dataadmissaocomissionado = (array_key_exists("dataadmissaocomissionado",$dados)) ? $dados["dataadmissaocomissionado"] : $row->dataadmissaocomissionado;
                $row->cargocomissionado = (array_key_exists("cargocomissionado",$dados)) ? $dados["cargocomissionado"] : $row->cargocomissionado;
                $row->cargo = (array_key_exists("cargo",$dados)) ? $dados["cargo"] : $row->cargo;
                $row->cbo = (array_key_exists("cbo",$dados)) ? $dados["cbo"] : $row->cbo;
                $row->dataadmissao = (array_key_exists("dataadmissao",$dados)) ? $dados["dataadmissao"] : $row->dataadmissao;
                $row->filhosquantidade = (array_key_exists("filhosquantidade",$dados)) ? $dados["filhosquantidade"] : $row->filhosquantidade;
                $row->filhos = (array_key_exists("filhos",$dados)) ? $dados["filhos"] : $row->filhos;
                $row->conjugenome = (array_key_exists("conjugenome",$dados)) ? $dados["conjugenome"] : $row->conjugenome;
                $row->estadocivil = (array_key_exists("estadocivil",$dados)) ? $dados["estadocivil"] : $row->estadocivil;
                $row->grauinstrucao = (array_key_exists("grauinstrucao",$dados)) ? $dados["grauinstrucao"] : $row->grauinstrucao;
                $row->localnascimento = (array_key_exists("localnascimento",$dados)) ? $dados["localnascimento"] : $row->localnascimento;
                $row->nacionalidade = (array_key_exists("nacionalidade",$dados)) ? $dados["nacionalidade"] : $row->nacionalidade;
                $row->obs_estrangeiro = (array_key_exists("obs_estrangeiro",$dados)) ? $dados["obs_estrangeiro"] : $row->obs_estrangeiro;
                $row->estrangeiro = (array_key_exists("estrangeiro",$dados)) ? $dados["estrangeiro"] : $row->estrangeiro;
                $row->cnh_validade = (array_key_exists("cnh_validade",$dados)) ? $dados["cnh_validade"] : $row->cnh_validade;
                $row->cnh_categoria = (array_key_exists("cnh_categoria",$dados)) ? $dados["cnh_categoria"] : $row->cnh_categoria;
                $row->cnh_numero = (array_key_exists("cnh_numero",$dados)) ? $dados["cnh_numero"] : $row->cnh_numero;
                $row->cnh = (array_key_exists("cnh",$dados)) ? $dados["cnh"] : $row->cnh;
                //aaaa

                $row->cnh_primeirahabilitacao = (array_key_exists("cnh_primeirahabilitacao",$dados)) ? $dados["cnh_primeirahabilitacao"] : $row->cnh_primeirahabilitacao;
                $row->cnh_habilitacaoemissao = (array_key_exists("cnh_habilitacaoemissao",$dados)) ? $dados["cnh_habilitacaoemissao"] : $row->cnh_habilitacaoemissao;
                $row->cnh_idarquivo = (array_key_exists("cnh_idarquivo",$dados)) ? $dados["cnh_idarquivo"] : $row->cnh_idarquivo;
                $row->cnh_transporteescolar = (array_key_exists("cnh_transporteescolar",$dados)) ? $dados["cnh_transporteescolar"] : $row->cnh_transporteescolar;
                $row->cnh_idcertificadotransporte = (array_key_exists("cnh_idcertificadotransporte",$dados)) ? $dados["cnh_idcertificadotransporte"] : $row->cnh_idcertificadotransporte;

                $row->pispasep = (array_key_exists("pispasep",$dados)) ? $dados["pispasep"] : $row->pispasep;
                $row->cpf = (array_key_exists("cpf",$dados)) ? $dados["cpf"] : $row->cpf;
                $row->rg = (array_key_exists("rg",$dados)) ? $dados["rg"] : $row->rg;
                $row->rgf = (array_key_exists("rgf",$dados)) ? $dados["rgf"] : $row->rgf;
                $row->secaoeleitoral = (array_key_exists("secaoeleitoral",$dados)) ? $dados["secaoeleitoral"] : $row->secaoeleitoral;
                $row->zonaeleitoral = (array_key_exists("zonaeleitoral",$dados)) ? $dados["zonaeleitoral"] : $row->zonaeleitoral;
                $row->tituloeleitor = (array_key_exists("tituloeleitor",$dados)) ? $dados["tituloeleitor"] : $row->tituloeleitor;
                $row->reservista_ra = (array_key_exists("reservista_ra",$dados)) ? $dados["reservista_ra"] : $row->reservista_ra;
                $row->reservista = (array_key_exists("reservista",$dados)) ? $dados["reservista"] : $row->reservista;
                $row->emissaocarteira = (array_key_exists("emissaocarteira",$dados)) ? $dados["emissaocarteira"] : $row->emissaocarteira;
                $row->datanascimento = (array_key_exists("datanascimento",$dados)) ? $dados["datanascimento"] : $row->datanascimento;
                $row->email = (array_key_exists("email",$dados)) ? $dados["email"] : $row->email;
                $row->sobrenomefantasia = (array_key_exists("sobrenomefantasia",$dados)) ? $dados["sobrenomefantasia"] : $row->sobrenomefantasia;
                $row->nomerazao = (array_key_exists("nomerazao",$dados)) ? $dados["nomerazao"] : $row->nomerazao;
                $row->professor = (array_key_exists("professor",$dados)) ? $dados["professor"] : $row->professor;
                $row->polivalente = (array_key_exists("polivalente",$dados)) ? $dados["polivalente"] : $row->polivalente;
                $row->diretor = (array_key_exists("diretor",$dados)) ? $dados["diretor"] : $row->diretor;
                $row->coordenador = (array_key_exists("coordenador",$dados)) ? $dados["coordenador"] : $row->coordenador;
                $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;

                $row->idcargosecretaria = (int)(array_key_exists("idcargosecretaria",$dados)) ? (int)$dados["idcargosecretaria"] : (int)$row->idcargosecretaria;
                $row->idfuncaosecretaria = (int)(array_key_exists("idfuncaosecretaria",$dados)) ? (int)$dados["idfuncaosecretaria"] : (int)$row->idfuncaosecretaria;

                $row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
                $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
                $row->modulo = (array_key_exists("modulo",$dados)) ? $dados["modulo"] : $row->modulo;
                $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
                $row->tipoensino = (array_key_exists("tipoensino",$dados)) ? $dados["tipoensino"] : $row->tipoensino;
                $row->idsmaterias = (array_key_exists("idsmaterias",$dados)) ? $dados["idsmaterias"] : $row->idsmaterias;
                $row->pertenceescola = (array_key_exists("pertenceescola",$dados)) ? $dados["pertenceescola"] : $row->pertenceescola;

                $row->iddepartamentorh = (int)(array_key_exists("iddepartamentorh",$dados)) ? (int)$dados["iddepartamentorh"] : (int)$row->iddepartamentorh;
                $row->idcargorh = (int)(array_key_exists("idcargorh",$dados)) ? (int)$dados["idcargorh"] : (int)$row->idcargorh;
                $row->idfuncaorh = (int)(array_key_exists("idfuncaorh",$dados)) ? (int)$dados["idfuncaorh"] : (int)$row->idfuncaorh;

                $row->idendereco = (int)(array_key_exists("idendereco",$dados)) ? (int)$dados["idendereco"] : (int)$row->idendereco;
                if (is_null($row->datacriacao)) {
                                       $row->datacriacao = date("Y-m-d H:i:s");
                               }

                $row->codigovinculo = (array_key_exists("codigovinculo",$dados)) ? $dados["codigovinculo"] : $row->codigovinculo;
                $row->vinculoempregaticio = (array_key_exists("vinculoempregaticio",$dados)) ? $dados["vinculoempregaticio"] : $row->vinculoempregaticio;
                $row->codigodepartamento = (array_key_exists("codigodepartamento",$dados)) ? $dados["codigodepartamento"] : $row->codigodepartamento;
                $row->descdepartamento = (array_key_exists("descdepartamento",$dados)) ? $dados["descdepartamento"] : $row->descdepartamento;
                $row->desclocal = (array_key_exists("desclocal",$dados)) ? $dados["desclocal"] : $row->desclocal;
                $row->idimportacao = (int)(array_key_exists("idimportacao",$dados)) ? $dados["idimportacao"] : $row->idimportacao;

                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 
		$_row = $row->save();
		//$_row = $_row->toArray();
		//var_dump($_row['idsfuncionarios']); die();

		return $row;
	}
	

	 	/*
			*  Função criada para a tabela localizada em PontosMensais,
			*  dataInicio -> Será a data para o filtro de lançamentos realizados
			*  dataFim -> Será a data final do filtro de lançamentos realizados
			*  page -> informações da paginação para o formulário 
			*  maxpage -> número máximo de páginas apresentadas nas tabelas 
         */
        public function oldFuncionariosGeraisPontosResumo($queries = array(), $page = 0, $maxpage = 0, $dataInicio, $dataFim, $idusuario, $estados){
			$where = array();


			$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
			$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
			$order = (isset($queries['order'])) ? $queries['order'] : false;

			$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
			if ($id) array_push($where, " f1.id = $id ");
			
			
			$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
			if ($idsecretaria) array_push($where, " f1.idsecretaria = $idsecretaria ");



			$idperfil = (isset($queries["idperfil"])) ? $queries["idperfil"] : false;
			if ($idperfil) array_push($where, " f1.idperfil = $idperfil ");

			$diferenteidperfil = (isset($queries["diferenteidperfil"])) ? $queries["diferenteidperfil"] : false;
			if ($diferenteidperfil) array_push($where, " (f1.idperfil <> '$diferenteidperfil' OR f1.idperfil IS NULL OR f1.idperfil = '') ");

			$idsfuncionarios = (isset($queries["idsfuncionarios"])) ? $queries["idsfuncionarios"] : false;
			if ($idsfuncionarios) array_push($where, " FIND_IN_SET ('$idsfuncionarios', f1.idsfuncionarios) ");

			$ids = (isset($queries["ids"])) ? $queries["ids"] : false;
			if ($ids) array_push($where, " FIND_IN_SET (f1.id, '$ids') ");

			$idhorarioponto = (isset($queries["idhorarioponto"])) ? $queries["idhorarioponto"] : false;
			if ($idhorarioponto) array_push($where, " f1.idhorarioponto = $idhorarioponto ");

			$idsperfis = (isset($queries["idsperfis"])) ? $queries["idsperfis"] : false;
			if ($idsperfis) array_push($where, " ((FIND_IN_SET ('$idsperfis', f1.idsperfis)) OR (f1.idperfil = $idsperfis)) ");

			$diferenteidsperfis = (isset($queries["diferenteidsperfis"])) ? $queries["diferenteidsperfis"] : false;
			if ($diferenteidsperfis) array_push($where, " NOT FIND_IN_SET ('$diferenteidsperfis', f1.idsperfis) ");

			$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
			if ($idescola) array_push($where, " f1.idescola = $idescola ");

			$idescolas = (isset($queries["idescolas"])) ? $queries["idescolas"] : false;
			if ($idescolas) array_push($where, " FIND_IN_SET ('$idescolas', f1.idescolas) ");

			$idtipoequipe = (isset($queries["idtipoequipe"])) ? $queries["idtipoequipe"] : false;
			if ($idtipoequipe) array_push($where, " f1.idtipoequipe = $idtipoequipe ");

			$modulo = (isset($queries["modulo"])) ? $queries["modulo"] : false;
			if ($modulo) array_push($where, " f1.modulo LIKE '%$modulo%' ");

			$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
			if ($iddepartamento) array_push($where, " f1.iddepartamento = $iddepartamento ");

			$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
			if ($nome) array_push($where, " f1.nome LIKE '%$nome%' ");

			$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
			if ($telefone) array_push($where, " f1.telefone LIKE '%$telefone%' ");
			
			$idlocal = (isset($queries["idlocal"])) ? $queries["idlocal"] : false;
			if ($idlocal) array_push($where, " f1.idlocal = '$idlocal' ");

			$professor = (isset($queries["professor"])) ? $queries["professor"] : false;
			if ($professor) array_push($where, " f1.professor LIKE '%$professor%' ");

			$coordenador = (isset($queries["coordenador"])) ? $queries["coordenador"] : false;
			if ($coordenador) array_push($where, " f1.coordenador LIKE '%$coordenador%' ");

			$tipoensino = (isset($queries["tipoensino"])) ? $queries["tipoensino"] : false;
			if ($tipoensino) array_push($where, " f1.tipoensino LIKE '%$tipoensino%' ");		

			$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
			if ($tipo) array_push($where, " f1.tipo LIKE '%$tipo%' ");

			$rgf = (isset($queries["rgf"])) ? $queries["rgf"] : false;
			if ($rgf) array_push($where, " f1.rgf = '$rgf' ");		

			$status = (isset($queries["status"])) ? $queries["status"] : false;
			if ($status) array_push($where, " f1.status LIKE '%$status%' ");

			$pertenceescola = (isset($queries["pertenceescola"])) ? $queries["pertenceescola"] : false;
			if ($pertenceescola) array_push($where, " f1.pertenceescola LIKE '%$pertenceescola%' ");

			$coordenacao = (isset($queries["coordenacao"])) ? $queries["coordenacao"] : false;

			if ($sorting) {
				$sorting = explode('_', $sorting);
				if (sizeof($sorting)==2) {
					
					if ($sorting[0]=='nome') $sorting[0]='l1.nome';
					
					$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
				}
			}		
			
			$w = "";
			foreach ($where as $k=>$v) {
				if ($k>0) $w .= " AND ";
				$w .= $v;
			}
			if ($w!="") $w = "AND ($w)";

			$fields = "f1.*"; 
			$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado ";
			
			if ($total) $fields = "COUNT(f1.id) as total";
			
			$ordem = "ORDER BY f1.id DESC";
			if ($order) $ordem = $order; 
			
			$limit = "";
			if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

			$strsql = "SELECT $fields 
						FROM funcionariosgeraisescolas f1
						LEFT JOIN enderecos e_idendereco ON e_idendereco.id=f1.idendereco 
							LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado
							".(($coordenacao) ? " LEFT JOIN departamentossecretarias ds ON ds.id=f1.iddepartamento " : "")."
						WHERE f1.excluido='nao' ".(($coordFuncionariosGeraisPontosResumoenacao) ? " AND ds.departamento='Coordenação' " : "")."
							$w 
						$ordem	
						$limit";
			//die($strsql);
			if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
			$db = Zend_Registry::get('db');				
			if ($total) {
				$row = $db->fetchRow($strsql);
				return $row['total'];
			}	
			
			$rowsRetorno = array();
			$rows = $db->fetchAll($strsql);

            //Instanciando Model de Pontos para rechear o retorno da model Pontos Gerais
            $pontos = new Funcionariosgeraispontohora();
            
            $totalDiasdoPeriodo = $pontos->getDiasUteisPeriodo($dataInicio, $dataFim); // setando o total de dias uteis do período
			
			//$idperfil = (isset($queries['idsperfis'])) ? $queries['idsperfis'] : '14';

			if((Usuarios::getUsuario('idperfil') == '14') || (Usuarios::getUsuario('idperfil') == '1')){
						$idusuario = (isset($idusuario)) ? $idusuario : Usuarios::getUsuario('idperfil');
			}

			
            foreach($rows as $row):
                
                $row['nome_funcionario'] = $row['id'];
                $row['lancamentos'] =  $pontos->getLancamentosConfirmados($dataInicio,$dataFim,$row['id'],$idusuario)['Total'];
                $row['totalDiasUteis'] = $totalDiasdoPeriodo;
                $row['confirmados'] = $pontos->getRhConfirmados($dataInicio,$dataFim,$row['id'],$idusuario)['Total'];
                $row['ocorrencias'] = $pontos->getOcorrencias($dataInicio,$dataFim,$row['id'],$idusuario)['Total'];
                array_push($rowsRetorno, $row);
              
            endforeach;
            
        

            $rowsRetornoOcorrencias = array();
            // eu sei que poderia ter resolvido com uma query legal, mas iria demorar pra me entender no sql depois ajeito esse model
            if($estados != ''):

				switch($estados):

					case "Sem Registros":

						foreach($rowsRetorno as $index):

							if(((int)$index['lancamentos'] == 0) AND ((int)$index['confirmados'] == 0)):
									array_push($rowsRetornoOcorrencias, $index);
							endif;  

						endforeach;
				
					break;
					case "Aguardando":

						foreach($rowsRetorno as $index):

							if(((int)$index['lancamentos'] == (int)$index['totalDiasUteis']) AND ((int)$index['confirmados'] == 0)):
									array_push($rowsRetornoOcorrencias, $index);
							endif;  

						endforeach;

					break;
					case "Incompleto":

						foreach($rowsRetorno as $index):

							if(((int)$index['lancamentos'] < (int)$index['totalDiasUteis']) AND ((int)$index['confirmados'] == 0) AND (((int)$index['lancamentos'] > 0))):
									array_push($rowsRetornoOcorrencias, $index);
							endif;  

						endforeach;

					break;
					case "Finalizado":

						foreach($rowsRetorno as $index):

							if(((int)$index['lancamentos'] == (int)$index['confirmados']) AND ((int)$index['lancamentos'] >= (int)$index['totalDiasUteis'])):
									array_push($rowsRetornoOcorrencias, $index);
							endif;  

						endforeach;

					break;

					case "Ocorrencias":

						foreach($rowsRetorno as $index):

							if( ((int)$index['ocorrencias']) > 0 ):
									array_push($rowsRetornoOcorrencias, $index);
							endif;  

						endforeach;

					break;
					
				endswitch;

               
				
                return $rowsRetornoOcorrencias;

            else:    
				return $rowsRetorno;
            endif;    

            
            
		}//end public funcition FuncionariosGeraisPontosResumo
		
		public function FuncionariosGeraisPontosResumo($queries = array(), $page = 0, $maxpage = 0, $dataInicio, $dataFim, $idusuario, $estados){

			$where = array();

			
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$die = (isset($queries['die'])) ? $queries['die'] : false;
		$meuid = (isset($queries['meuid'])) ? $queries['meuid'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " f1.idsecretaria = $idsecretaria ");



		$idperfil = (isset($queries["idperfil"])) ? $queries["idperfil"] : false;
		if ($idperfil) array_push($where, " f1.idperfil = $idperfil ");

		$diferenteidperfil = (isset($queries["diferenteidperfil"])) ? $queries["diferenteidperfil"] : false;
		if ($diferenteidperfil) array_push($where, " (f1.idperfil <> '$diferenteidperfil' OR f1.idperfil IS NULL OR f1.idperfil = '') ");

		$idsfuncionarios = (isset($queries["idsfuncionarios"])) ? $queries["idsfuncionarios"] : false;
		if ($idsfuncionarios) array_push($where, " FIND_IN_SET ('$idsfuncionarios', f1.idsfuncionarios) ");

		$idsfuncionariossel = (isset($queries["idsfuncionariossel"])) ? $queries["idsfuncionariossel"] : false;

		$idhorarioponto = (isset($queries["idhorarioponto"])) ? $queries["idhorarioponto"] : false;
		if ($idhorarioponto) array_push($where, " f1.idhorarioponto = $idhorarioponto ");

		$idsperfis = (isset($queries["idsperfis"])) ? $queries["idsperfis"] : false;
		if ($idsperfis) array_push($where, " ((FIND_IN_SET ('$idsperfis', f1.idsperfis)) OR (f1.idperfil = $idsperfis)) ");

		$idsfuncionariosgerais = (isset($queries["idsfuncionariosgerais"])) ? $queries["idsfuncionariosgerais"] : false;
		if ($idsfuncionariosgerais) array_push($where, " (FIND_IN_SET(f1.id,'$idsfuncionariosgerais')) ");

		$idsperfis_null = (isset($queries["idsperfis_null"])) ? $queries["idsperfis_null"] : false;
		if ($idsperfis_null) array_push($where, " (((f1.idsperfis IS NULL) OR(f1.idsperfis = '')) AND (f1.idperfil = $idsperfis_null)) ");

		$diferenteidsperfis = (isset($queries["diferenteidsperfis"])) ? $queries["diferenteidsperfis"] : false;
		if ($diferenteidsperfis) array_push($where, " NOT FIND_IN_SET ('$diferenteidsperfis', f1.idsperfis) ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;

		$idescolas = (isset($queries["idescolas"])) ? $queries["idescolas"] : false;
		if ($idescolas) array_push($where, " FIND_IN_SET ('$idescolas', f1.idescolas) ");

		$idtipoequipe = (isset($queries["idtipoequipe"])) ? $queries["idtipoequipe"] : false;
		if ($idtipoequipe) array_push($where, " f1.idtipoequipe = $idtipoequipe ");

		$modulo = (isset($queries["modulo"])) ? $queries["modulo"] : false;
		if ($modulo) array_push($where, " f1.modulo LIKE '%$modulo%' ");

		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " f1.iddepartamento = $iddepartamento ");

		$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
		if ($nome) array_push($where, " f1.nome LIKE '%$nome%' ");

		$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
		if ($telefone) array_push($where, " f1.telefone LIKE '%$telefone%' ");
		
		$idlocal = (isset($queries["idlocal"])) ? $queries["idlocal"] : false;
		if ($idlocal) array_push($where, " f1.idlocal = '$idlocal' ");

		$professor = (isset($queries["professor"])) ? $queries["professor"] : false;
		if ($professor) array_push($where, " f1.professor LIKE '%$professor%' ");

		$coordenador = (isset($queries["coordenador"])) ? $queries["coordenador"] : false;
		if ($coordenador) array_push($where, " f1.coordenador LIKE '%$coordenador%' ");

		$diretor = (isset($queries["diretor"])) ? $queries["diretor"] : false;
		if ($diretor) array_push($where, " f1.diretor LIKE '%$diretor%' ");

		$ndiretor = (isset($queries["ndiretor"])) ? $queries["ndiretor"] : false;
		if ($ndiretor) array_push($where, " (f1.diretor != '$ndiretor' || f1.diretor IS NULL) ");

		$tipoensino = (isset($queries["tipoensino"])) ? $queries["tipoensino"] : false;
		if ($tipoensino) array_push($where, " f1.tipoensino LIKE '%$tipoensino%' ");		

		$rgf = (isset($queries["rgf"])) ? $queries["rgf"] : false;
		if ($rgf) array_push($where, " f1.rgf = '$rgf' ");		

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");

		$pertenceescola = (isset($queries["pertenceescola"])) ? $queries["pertenceescola"] : false;
		if ($pertenceescola) array_push($where, " f1.pertenceescola LIKE '%$pertenceescola%' ");

		$desclocal = (isset($queries["desclocal"])) ? $queries["desclocal"] : false;
		if ($desclocal) array_push($where, " f1.desclocal LIKE '%$desclocal%' ");
                
                $coordenacao = (isset($queries["coordenacao"])) ? $queries["coordenacao"] : false;


        $meusfuncionarios_or = (isset($queries["meusfuncionarios_or"])) ? $queries["meusfuncionarios_or"] : false;

        if(($meusfuncionarios_or) && ($idsfuncionariossel)){
        	if ($idescola) array_push($where, " (f1.idescola = $idescola OR f1.id IN ($idsfuncionariossel)) ");
        }else{
			if(($meuid) && ($idsfuncionariossel)){
				if ($idsfuncionariossel) array_push($where, " (f1.id IN ($idsfuncionariossel) OR (FIND_IN_SET(f1.idsfuncionarios, $meuid))) ");
			}else{
				if ($idsfuncionariossel) array_push($where, " f1.id IN ($idsfuncionariossel) ");
			}
			
			if ($idescola) array_push($where, " f1.idescola = $idescola ");
			
        }
		
		$ids = (isset($queries["ids"])) ? $queries["ids"] : false;
		if ($ids) array_push($where, " f1.id IN ($ids) ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";
                
		$fields = "f1.*"; 
		$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado ";
		
		if ($total) {
                    $fields = "COUNT(f1.id) as total";
                };

                $ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgeraisescolas f1
					 LEFT JOIN enderecos e_idendereco ON e_idendereco.id=f1.idendereco 
						LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado
						".(($coordenacao) ? " LEFT JOIN departamentossecretarias ds ON ds.id=f1.iddepartamento " : "")."
					WHERE f1.excluido='nao' ".(($coordenacao) ? " AND ds.departamento='Coordenação' " : "")."
						$w 
					$ordem	
					$limit";
		//if(($die) && (!$total)) {var_dump($strsql); die();}
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');		

		// if ($total) {
		// 	$row = $db->fetchRow($strsql);
		// 	return $row['total'];
		// }	

			$rowsRetorno = array();

			$rows = $db->fetchAll($strsql);

            //Instanciando Model de Pontos para rechear o retorno da model Pontos Gerais
            $pontos = new Funcionariosgeraispontohora();
            
            $totalDiasdoPeriodo = $pontos->getDiasUteisPeriodo($dataInicio, $dataFim); // setando o total de dias uteis do período
			

			if((Usuarios::getUsuario('idperfil') == '14') || (Usuarios::getUsuario('idperfil') == '1')){
						$idusuario = (isset($idusuario)) ? $idusuario : Usuarios::getUsuario('idperfil');
			}
			
            foreach($rows as $row):
                
                $row['nome_funcionario'] = $row['id'];
                $row['lancamentos'] = $pontos->getLancamentosConfirmados($dataInicio,$dataFim,$row['id'],$idusuario)['Total'];
                $row['totalDiasUteis'] = $totalDiasdoPeriodo;
                $row['confirmados'] = $pontos->getRhConfirmados($dataInicio,$dataFim,$row['id'],$idusuario)['Total'];
				$row['ocorrencias'] = $pontos->getOcorrencias($dataInicio,$dataFim,$row['id'],$idusuario)['Total'];
				$row['totalhorasnormaislancados'] = $pontos->getTotalHorasLancados($dataInicio,$dataFim,$row['id'],$idusuario);
				$row['totalhorasnormais'] = '5151';
				$row['totaldiasfaltas'] = $pontos->getTotalFaltasLancados($dataInicio,$dataFim,$row['id'],$idusuario);
				$row['totaldiasocorrencias'] = $pontos->getTotalOcorrenciasLancados($dataInicio,$dataFim,$row['id'],$idusuario);

                array_push($rowsRetorno, $row);
              
            endforeach;
            
        

            $rowsRetornoOcorrencias = array();
            // eu sei que poderia ter resolvido com uma query legal, mas iria demorar pra me entender no sql depois ajeito esse model
            if($estados != ''):

				switch($estados):

					case "Sem Registros":

						foreach($rowsRetorno as $index):

							if(((int)$index['lancamentos'] == 0) AND ((int)$index['confirmados'] == 0)):
									array_push($rowsRetornoOcorrencias, $index);
							endif;  

						endforeach;
				
					break;
					case "Aguardando":

						foreach($rowsRetorno as $index):

							if(((int)$index['lancamentos'] == (int)$index['totalDiasUteis']) AND ((int)$index['confirmados'] == 0)):
									array_push($rowsRetornoOcorrencias, $index);
							endif;  

						endforeach;

					break;
					case "Incompleto":

						foreach($rowsRetorno as $index):

							if(((int)$index['lancamentos'] < (int)$index['totalDiasUteis']) AND ((int)$index['confirmados'] == 0) AND (((int)$index['lancamentos'] > 0))):
									array_push($rowsRetornoOcorrencias, $index);
							endif;  

						endforeach;

					break;
					case "Finalizado":

						foreach($rowsRetorno as $index):

							if(((int)$index['lancamentos'] == (int)$index['confirmados']) AND ((int)$index['lancamentos'] >= (int)$index['totalDiasUteis'])):
									array_push($rowsRetornoOcorrencias, $index);
							endif;  

						endforeach;

					break;

					case "Ocorrencias":

						foreach($rowsRetorno as $index):

							if( ((int)$index['ocorrencias']) > 0 ):
									array_push($rowsRetornoOcorrencias, $index);
							endif;  

						endforeach;

					break;
					
				endswitch;
				
                return $rowsRetornoOcorrencias;

			else: 
				
				return $rowsRetorno;
				
            endif;    
		
		//return $db->fetchAll($strsql);	
		}//end public function funcionariosgeraispontosresumo


}