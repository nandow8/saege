<?php

class Pastasvirtuaisarquivos extends Zend_Db_Table_Abstract {

	/**
	 * Define o nome da tabela
	 * @var string
	 */
	protected $_name = "pastasvirtuaisarquivos";
	
	/**
	 * Define a chave primaria
	 * @var integer
	 */
	protected $_primary = "id";
	
	
	public static function getArquivosByIdPastasvirtuais($idpastavirtual, $order = 'pa1.ordem') {
		
		$strsql = "SELECT pa1.idarquivo, pa1.idpastavirtual, pa1.legenda, a1.* 
					FROM pastasvirtuaisarquivos pa1 
						LEFT JOIN arquivos ar1 ON a1.id=pa1.idarquivo
					WHERE pa1.excluido='nao' 
						AND pa1.idpastavirtual=$idpastavirtual 
					ORDER BY $order"; 
						
		$db = Zend_Registry::get('db');
		return $db->fetchAll($strsql);
	}
	
	public function setArquivos($idpastavirtual, $arquivos, $legendas) {
		//if (!is_array($arquivos)) return;
		
		$logdata = date('Y-m-d G:i:s');
		foreach ($arquivos as $i=>$idarquivo) {
			$d = array();
			
			$legenda = $legendas[$i];
			
			$d['idpastavirtual'] = $idpastavirtual;
			$d['idarquivo'] = $idarquivo;
			$d['legenda'] = $legenda;
			$d['ordem'] = $i;
			$d['excluido'] = 'nao';
			$d['logusuario'] = Usuarios::getUsuario('id');
			$d['logdata'] = $logdata;
			$this->save($d);
		}
		
		$ids = implode(",", $arquivos);
		if ($ids=="") $ids = "0";
		$strsql = "DELETE FROM pastasvirtuaisarquivos WHERE idpastavirtual=$idpastavirtual AND idarquivo NOT IN ($ids)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);		
	}	

	public static function getPastasvirtuaisArquivos($idpastavirtual = NULL, $status = NULL, $inverte = false, $limite = NULL) {
		$db = Zend_Registry::get('db');
		
		$w = "";
		if (!is_null($idpastavirtual)) $w .= " AND pa1.idpastavirtual = " . (int)$idpastavirtual;
		if (!is_null($status)) $w .= " AND pa1.status = '$status'"; 
		
		$l = "";
		if (!is_null($limite)) $l .= " LIMIT '$limite'";
		
		$order = "ORDER BY pa1.ordem";
		if ($inverte) $order .= " DESC";
		
		$strsql = "SELECT pa1.* 
					FROM pastasvirtuaisarquivos pa1 
					WHERE pa1.excluido = 'nao' 
					$w
					$order
					$l";
					
		return $db->fetchAll($strsql);
	}
	
	/**
	 * Salva o dados (INSERT OU UPDATE)
	 * @param array dados
	 * @return Arquivos
	 */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$idpastavirtual = (int)$dados['idpastavirtual'];
		$idarquivo = (int)$dados['idarquivo'];
		
		$row = $this->fetchRow("idpastavirtual=" . $idpastavirtual . " AND idarquivo=" . $idarquivo);		
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
				
		if ($id>0)$row->id = $id;
		
		$row->idpastavirtual = (isset($dados['idpastavirtual'])) ? $dados['idpastavirtual'] : $row->idpastavirtual;
		$row->idarquivo = (isset($dados['idarquivo'])) ? $dados['idarquivo'] : $row->idarquivo;
		$row->legenda = (isset($dados['legenda'])) ? $dados['legenda'] : $row->legenda;
		$row->ordem = (isset($dados['ordem'])) ? $dados['ordem'] : $row->ordem;
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (isset($dados['logusuario'])) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (isset($dados['logdata'])) ? $dados['logdata'] : $row->logdata;
		$row->save();
		
		return $row;
	}
	
}