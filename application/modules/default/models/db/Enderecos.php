<?php

/**
 * Define o modelo Enderecos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2012 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Enderecos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "enderecos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";	
	
	
	public static function getEnderecoById($id) {
		$id = (int)$id;
		if ($id==0) return false;
		
		$strsql = "SELECT en1.*, es1.nome as estado, es1.uf as uf, es1.capital as capital FROM enderecos en1
		LEFT JOIN estados es1 on en1.idestado = es1.id
		WHERE en1.id = $id AND en1.excluido='nao'";
		$db = Zend_Registry::get('db');
		//die($strsql);
		return $db->fetchRow($strsql);
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Usuarios
     */
	public function save($dados) {
		$novoRegistro = true;
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
				
		if ($id>0)$row->id = $id;
		$row->cep = (isset($dados['cep'])) ? $dados['cep'] : $row->cep;
		$row->endereco = (isset($dados['endereco'])) ? $dados['endereco'] : $row->endereco;
		$row->numero = (isset($dados['numero'])) ? $dados['numero'] : $row->numero;
		$row->complemento = (isset($dados['complemento'])) ? $dados['complemento'] : $row->complemento;
		$row->bairro = (isset($dados['bairro'])) ? $dados['bairro'] : $row->bairro;
		$row->idestado = (isset($dados['idestado'])) ? (int)$dados['idestado'] : (int)$row->idestado;
		$row->cidade = (isset($dados['cidade'])) ? $dados['cidade'] : $row->cidade;
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;
		$row->save();

		
		return $row;
	}
	
}