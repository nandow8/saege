<?php

/**
 * Define o modelo Documentacoespontuacoesfuncionarios
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Documentacoespontuacoesfuncionarios extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "documentacoespontuacoesfuncionarios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDocumentacoespontuacoesfuncionariosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$documentacoespontuacoesfuncionarios = new Documentacoespontuacoesfuncionarios();
		return $documentacoespontuacoesfuncionarios->getDocumentacoespontuacoesfuncionarios($queries, $page, $maxpage);
	}
	
	public function getDocumentacoespontuacoesfuncionarios($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		
		$idescolausuario = (isset($queries["idescolausuario"])) ? $queries["idescolausuario"] : false;
		if ($idescolausuario) array_push($where, " d1.idescolausuario = $idescolausuario ");

$iddocumentacaopontuacao = (isset($queries["iddocumentacaopontuacao"])) ? $queries["iddocumentacaopontuacao"] : false;
		if ($iddocumentacaopontuacao) array_push($where, " d1.iddocumentacaopontuacao = $iddocumentacaopontuacao ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " d1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " d1.data <= '$data_f' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " d1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		
		$ordem = "ORDER BY d1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM documentacoespontuacoesfuncionarios d1
					
					WHERE d1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDocumentacaopontuacaofuncionarioById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDocumentacoespontuacoesfuncionarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDocumentacaopontuacaofuncionarioByIdHelper($id, $queries = array()) {
		$rows = new Documentacoespontuacoesfuncionarios();
		return $rows->getDocumentacaopontuacaofuncionarioById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Documentacoespontuacoesfuncionarios
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescolausuario = (array_key_exists("idescolausuario",$dados)) ? $dados["idescolausuario"] : $row->idescolausuario;
		 $row->iddocumentacaopontuacao = (array_key_exists("iddocumentacaopontuacao",$dados)) ? $dados["iddocumentacaopontuacao"] : $row->iddocumentacaopontuacao;
		 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
		 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}