<?php

/**
 * Define o modelo Almoxarifadoprodutos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosgeraispontocartaoblocosdatas extends Zend_Db_Table_Abstract {
	
	/**
            * Define o nome da tabela
            * @var string
        */
	protected $_name = "funcionariosgeraispontocartaoblocosdatas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFuncionariosgeraispontocartaoblocosdatasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosgeraispontocartaofuncionario = new Funcionariosgeraispontocartaofuncionario();
		return $funcionariosgeraispontocartaofuncionario->getFuncionariosgeraispontocartaofuncionario($queries, $page, $maxpage);
	}
	
	public function getFuncionariosgeraispontocartaoblocosdatas($queries = array(), $page = 0, $maxpage = 0){ 
            
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
               
                //labelbloco	varchar(80)	utf8_general_ci		Sim	NULL		
                $labelbloco = (isset($queries["labelbloco"])) ? $queries["labelbloco"] : false;
                if ($labelbloco) array_push($where, " a1.labelbloco = $labelbloco ");	
                //datainiciobloco	date			Sim	NULL		
                $datainiciobloco = (isset($queries["datainiciobloco"])) ? $queries["datainiciobloco"] : false;
                if ($datainiciobloco) array_push($where, " a1.datainiciobloco = $datainiciobloco ");	
                //datafimbloco	date			Sim	NULL		
                $datafimbloco = (isset($queries["datafimbloco"])) ? $queries["datafimbloco"] : false;
                if ($datafimbloco) array_push($where, " a1.datafimbloco = $datafimbloco ");	
                //obsbloco	text	utf8_general_ci		Sim	NULL		
                $obsbloco = (isset($queries["obsbloco"])) ? $queries["obsbloco"] : false;
                if ($obsbloco) array_push($where, " a1.obsbloco = $obsbloco ");	

				$indexarquivo = (isset($queries["indexarquivo"])) ? $queries["indexarquivo"] : false;
                if ($indexarquivo) array_push($where, " a1.indexarquivo = $indexarquivo ");	


$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgeraispontocartaoblocosdatas a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariosgeraispontocartaoblocosdatasById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosgeraispontocartaoblocosdatas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionariosgeraispontocartaoblocosdatasByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifadoprodutos();
		return $rows->getAlmoxarifadoprodutoById($id, $queries);
	}		
	
        /**
        * Salva o dados (INSERT OU UPDATE)
        * @param array dados
        * @return Almoxarifadoprodutos
        */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
                
		// labelbloco	varchar(80)	utf8_general_ci		Sim	None		
		$row->labelbloco = (array_key_exists("labelbloco",$dados)) ? $dados["labelbloco"] : $row->labelbloco;
		// datainiciobloco	date			Sim	None		
		$row->datainiciobloco = (array_key_exists("datainiciobloco",$dados)) ? $dados["datainiciobloco"] : $row->datainiciobloco;
		// datafimbloco	date			Sim	None		
		$row->datafimbloco = (array_key_exists("datafimbloco",$dados)) ? $dados["datafimbloco"] : $row->datafimbloco;
		// obsbloco	varchar(200)	utf8_general_ci		Sim	None		
		$row->obsbloco = (array_key_exists("obsbloco",$dados)) ? $dados["obsbloco"] : $row->obsbloco;
		// indexarquivo	bigint(20)			Sim	None			   
		$row->indexarquivo = (array_key_exists("indexarquivo",$dados)) ? $dados["indexarquivo"] : $row->indexarquivo;

                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();

		$registro = 'Adicionado';
		if(!$novoRegistro)
			$registro = 'Editado';
			
		Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), " id -> [" . $row->id . "] [" . $registro . "][status=>" . $row->status . "][excluido=>" . $row->excluido . "]");
		
		return $row;
	}
	
}