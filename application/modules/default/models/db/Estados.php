<?php

/**
 * Define o modelo Estados
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Estados extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "estados";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getUFestadoById($id) {
		$db = Zend_Registry::get('db');
		$strsql = "SELECT * FROM estados WHERE id='$id' LIMIT 0,1";
		
		$uf = $db->fetchAll($strsql);
		if (!$uf) return 0;
		return $uf;
	}

	public static function getIdEstadoByUF($uf) {
		$db = Zend_Registry::get('db');
		$strsql = "SELECT * FROM estados WHERE uf='$uf' LIMIT 0,1";
		
		$uf = $db->fetchRow($strsql);
		if (!$uf) return 0;
		return $uf['id'];
	}
	
	public static function getNomeEstadoById($id) {
		$db = Zend_Registry::get('db');
		$strsql = "SELECT nome FROM estados WHERE id='$id' LIMIT 0,1";
		
		$estado = $db->fetchRow($strsql);
		if (!$estado) return 0;
		return $estado['nome'];
	}

	public static function getEstadosHelper() {
		$rows = new Estados();
		return $rows->fetchAll();
	}
	
	/**
     * Retorna um array com os estados do sistema
     * @return array
     */
	public function getEstados($qtdUsuarios = false) {
		$qtd_usuarios = "";
		if ($qtdUsuarios) $qtd_usuarios .= "
			 ,(SELECT COUNT(u1.id) 
			 	FROM pdusuarios u1, cidades c2 
			 	WHERE c2.id=u1.idcidade 
			 		AND c2.idestado=e1.id 
			 		AND u1.excluido='nao' 
			 		AND c2.excluido='nao') as qtdusuarios ";		
		
		$db = Zend_Registry::get('db');
		$strsql = "SELECT e1.*, 
						(SELECT COUNT(c1.id) 
						FROM cidades c1 
						WHERE c1.idestado=e1.id 
							AND c1.excluido='nao') as qtdcidades 
						$qtd_usuarios	
					FROM estados e1 
					ORDER BY e1.nome";

		return $db->fetchAll($strsql);
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Estados
     */
	public function save($dados) {
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id");
		
		if (!$row) $row = $this->createRow();
		else {
			$x_rows = new X_Estados();
			$x_rows->arquiva($row->toArray());
		} 
						
		if ($id>0)$row->id = $id;
		$row->nome = $dados['nome'];
		$row->uf = $dados['uf'];
		$row->preposicao = $dados['preposicao'];
		$row->capital = $dados['capital'];
		$row->capitalpreposicao = $dados['capitalpreposicao'];
		$row->logusuario = $dados['logusuario'];
		$row->logdata = $dados['logdata'];
		
		$row->save();

		return $row;
	}	
	
	
}