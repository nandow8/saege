<?php

/**
 * Define o modelo Logisticaocorrencias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Logisticaocorrencias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "logisticaocorrencias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLogisticaocorrenciasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$logisticaocorrencias = new Logisticaocorrencias();
		return $logisticaocorrencias->getLogisticaocorrencias($queries, $page, $maxpage);
	}
	
	public function getLogisticaocorrencias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		
		$idfrota = (isset($queries["idfrota"])) ? $queries["idfrota"] : false;
		if ($idfrota) array_push($where, " l1.idfrota = $idfrota ");

$idtipoocorrencia = (isset($queries["idtipoocorrencia"])) ? $queries["idtipoocorrencia"] : false;
		if ($idtipoocorrencia) array_push($where, " l1.idtipoocorrencia = $idtipoocorrencia ");

$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " l1.sequencial LIKE '%$sequencial%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*"; 
		;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM logisticaocorrencias l1
					
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLogisticaocorrenciaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLogisticaocorrencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLogisticaocorrenciaByIdHelper($id, $queries = array()) {
		$rows = new Logisticaocorrencias();
		return $rows->getLogisticaocorrenciaById($id, $queries);
	}		
	
	public function getUltimoLogisticaocorrencia($queries = array()) {
		$queries['order'] = 'ORDER BY l1.id DESC';
		$rows = $this->getLogisticaocorrencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Logisticaocorrencias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfrota = (array_key_exists("idfrota",$dados)) ? $dados["idfrota"] : $row->idfrota;
 $row->idtipoocorrencia = (array_key_exists("idtipoocorrencia",$dados)) ? $dados["idtipoocorrencia"] : $row->idtipoocorrencia;
 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
 $row->idimagem = (array_key_exists("idimagem",$dados)) ? $dados["idimagem"] : $row->idimagem;
 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}