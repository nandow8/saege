<?php

/**
 * Define o modelo Documentacaoavaliacoesperguntas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Documentacaoavaliacoesperguntas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "documentacaoavaliacoesperguntas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDocumentacaoavaliacoesperguntasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$documentacaoavaliacoesperguntas = new Documentacaoavaliacoesperguntas();
		return $documentacaoavaliacoesperguntas->getDocumentacaoavaliacoesperguntas($queries, $page, $maxpage);
	}
	
	public function getDocumentacaoavaliacoesperguntas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		
		

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		
		$ordem = "ORDER BY d1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM documentacaoavaliacoesperguntas d1
					
					WHERE d1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDocumentacaoavaliacaoperguntaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDocumentacaoavaliacoesperguntas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDocumentacaoavaliacaoperguntaByIdHelper($id, $queries = array()) {
		$rows = new Documentacaoavaliacoesperguntas();
		return $rows->getDocumentacaoavaliacaoperguntaById($id, $queries);
	}		
	
	public function getDocumentacaoavaliacaoperguntaByIdParent($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idparent'] = $id;
		$rows = $this->getDocumentacaoavaliacoesperguntas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDocumentacaoavaliacaoperguntaByIdParentHelper($id, $queries = array()) {
		$rows = new Documentacaoavaliacoesperguntas();
		return $rows->getDocumentacaoavaliacaoperguntaByIdParent($id, $queries);
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Documentacaoavaliacoesperguntas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idparent = (array_key_exists("idparent",$dados)) ? $dados["idparent"] : $row->idparent;
 $row->avaliacao_1 = (array_key_exists("avaliacao_1",$dados)) ? $dados["avaliacao_1"] : $row->avaliacao_1;
 $row->avaliacao_2 = (array_key_exists("avaliacao_2",$dados)) ? $dados["avaliacao_2"] : $row->avaliacao_2;
 $row->avaliacao_3 = (array_key_exists("avaliacao_3",$dados)) ? $dados["avaliacao_3"] : $row->avaliacao_3;
 $row->avaliacao_4 = (array_key_exists("avaliacao_4",$dados)) ? $dados["avaliacao_4"] : $row->avaliacao_4;
 $row->avaliacao_5 = (array_key_exists("avaliacao_5",$dados)) ? $dados["avaliacao_5"] : $row->avaliacao_5;
 $row->avaliacao_6 = (array_key_exists("avaliacao_6",$dados)) ? $dados["avaliacao_6"] : $row->avaliacao_6;
 $row->avaliacao_7 = (array_key_exists("avaliacao_7",$dados)) ? $dados["avaliacao_7"] : $row->avaliacao_7;
 $row->avaliacao_8 = (array_key_exists("avaliacao_8",$dados)) ? $dados["avaliacao_8"] : $row->avaliacao_8;
 $row->avaliacao_9 = (array_key_exists("avaliacao_9",$dados)) ? $dados["avaliacao_9"] : $row->avaliacao_9;
 $row->avaliacao_10 = (array_key_exists("avaliacao_10",$dados)) ? $dados["avaliacao_10"] : $row->avaliacao_10;
 $row->avaliacao_11 = (array_key_exists("avaliacao_11",$dados)) ? $dados["avaliacao_11"] : $row->avaliacao_11;
 $row->avaliacao_12 = (array_key_exists("avaliacao_12",$dados)) ? $dados["avaliacao_12"] : $row->avaliacao_12;
 $row->avaliacao_13 = (array_key_exists("avaliacao_13",$dados)) ? $dados["avaliacao_13"] : $row->avaliacao_13;
 $row->avaliacao_14 = (array_key_exists("avaliacao_14",$dados)) ? $dados["avaliacao_14"] : $row->avaliacao_14;
 $row->avaliacao_15 = (array_key_exists("avaliacao_15",$dados)) ? $dados["avaliacao_15"] : $row->avaliacao_15;
 $row->avaliacao_16 = (array_key_exists("avaliacao_16",$dados)) ? $dados["avaliacao_16"] : $row->avaliacao_16;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		//return true;		
		$row->save();
		
		return $row;
	}
	
}