<?php

/**
 * Define o modelo Juridicooficiosaprovacao
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Juridicooficiosaprovacao extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "juridicooficiosaprovacao";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getJuridicooficiosaprovacaoHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$juridicooficiosaprovacao = new Juridicooficios();
		return $juridicooficiosaprovacao->getJuridicooficiosaprovacao($queries, $page, $maxpage);
	}
	
	public function getJuridicooficiosaprovacao($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " j1.id = $id ");
		
		
		$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " j1.datalancamento >= '$datalancamento_i' ");

                $datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " j1.datalancamento <= '$datalancamento_f' ");

                $idtipo = (isset($queries["idtipo"])) ? $queries["idtipo"] : false;
		if ($idtipo) array_push($where, " j1.idtipo = $idtipo ");

                $numero = (isset($queries["numero"])) ? $queries["numero"] : false;
		if ($numero) array_push($where, " j1.numero LIKE '%$numero%' ");

                $titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " j1.titulo LIKE '%$titulo%' ");

                $numeroof = (isset($queries["numeroof"])) ? $queries["numeroof"] : false;
		if ($numeroof) array_push($where, " j1.numeroof LIKE '%$numeroof%' ");

                $dataof_i = (isset($queries["dataof_i"])) ? $queries["dataof_i"] : false;
		if ($dataof_i) array_push($where, " j1.dataof >= '$dataof_i' ");

                $dataof_f = (isset($queries["dataof_f"])) ? $queries["dataof_f"] : false;
    		if ($dataof_f) array_push($where, " j1.dataof <= '$dataof_f' ");

                $proc = (isset($queries["proc"])) ? $queries["proc"] : false;
		if ($proc) array_push($where, " j1.proc LIKE '%$proc%' ");

                $dataproc_i = (isset($queries["dataproc_i"])) ? $queries["dataproc_i"] : false;
		if ($dataproc_i) array_push($where, " j1.dataproc >= '$dataproc_i' ");

                $dataproc_f = (isset($queries["dataproc_f"])) ? $queries["dataproc_f"] : false;
		if ($dataproc_f) array_push($where, " j1.dataproc <= '$dataproc_f' ");

                $numeroreinteracao = (isset($queries["numeroreinteracao"])) ? $queries["numeroreinteracao"] : false;
		if ($numeroreinteracao) array_push($where, " j1.numeroreinteracao LIKE '%$numeroreinteracao%' ");

                $datareinteracao_i = (isset($queries["datareinteracao_i"])) ? $queries["datareinteracao_i"] : false;
		if ($datareinteracao_i) array_push($where, " j1.datareinteracao >= '$datareinteracao_i' ");

                $datareinteracao_f = (isset($queries["datareinteracao_f"])) ? $queries["datareinteracao_f"] : false;
		if ($datareinteracao_f) array_push($where, " j1.datareinteracao <= '$datareinteracao_f' ");

                $datadevolucao_i = (isset($queries["datadevolucao_i"])) ? $queries["datadevolucao_i"] : false;
		if ($datadevolucao_i) array_push($where, " j1.datadevolucao >= '$datadevolucao_i' ");

                $datadevolucao_f = (isset($queries["datadevolucao_f"])) ? $queries["datadevolucao_f"] : false;
		if ($datadevolucao_f) array_push($where, " j1.datadevolucao <= '$datadevolucao_f' ");

                $posicao = (isset($queries["posicao"])) ? $queries["posicao"] : false;
		if ($posicao) array_push($where, " j1.posicao = '$posicao' ");

                $status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " j1.status LIKE '%$status%' ");

                $cienvioflag = (isset($queries["cienvioflag"])) ? $queries["cienvioflag"] : false;
		if ($cienvioflag) array_push($where, " j1.cienvioflag LIKE '%$cienvioflag%' ");

                $cienviodt = (isset($queries["cienviodt"])) ? $queries["cienviodt"] : false;
		if ($cienviodt) array_push($where, " j1.cienviodt LIKE '%$cienviodt%' ");

                $cienvio = (isset($queries["cienvio"])) ? $queries["cienvio"] : false;
		if ($cienvio) array_push($where, " j1.cienvio LIKE '%$cienvio%' ");

                $idimgcienvio = (isset($queries["idimgcienvio"])) ? $queries["idimgcienvio"] : false;
		if ($idimgcienvio) array_push($where, " j1.idimgcienvio = $idimgcienvio ");
                
                $ciretornoflag = (isset($queries["ciretornoflag"])) ? $queries["ciretornoflag"] : false;
		if ($ciretornoflag) array_push($where, " j1.ciretornoflag LIKE '%$ciretornoflag%' ");

                $ciretornodt = (isset($queries["ciretornodt"])) ? $queries["ciretornodt"] : false;
		if ($ciretornodt) array_push($where, " j1.ciretornodt LIKE '%$ciretornodt%' ");

                $ciretorno = (isset($queries["ciretorno"])) ? $queries["ciretorno"] : false;
		if ($ciretorno) array_push($where, " j1.ciretorno LIKE '%$ciretorno%' ");

                $idimgciretorno = (isset($queries["idimgciretorno"])) ? $queries["idimgciretorno"] : false;
		if ($idimgciretorno) array_push($where, " j1.idimgciretorno = $idimgciretorno ");                
                
                if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "j1.*"; 
		;
		
		if ($total) $fields = "COUNT(j1.id) as total";
		
		$ordem = "ORDER BY j1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM juridicooficios j1
					
					WHERE j1.excluido='nao' and j1.status in ('Encaminhado', 'Processado', 'Finalizado') 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getJuridicooficioaprovacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getJuridicooficiosaprovacao($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getJuridicooficioaprovacaoByIdHelper($id, $queries = array()) {
		$rows = new Juridicooficios();
		return $rows->getJuridicooficioaprovacaoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Juridicooficiosaprovacao
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
                $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
                $row->idtipo = (array_key_exists("idtipo",$dados)) ? $dados["idtipo"] : $row->idtipo;
                $row->numero = (array_key_exists("numero",$dados)) ? $dados["numero"] : $row->numero;
                $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
                $row->numeroof = (array_key_exists("numeroof",$dados)) ? $dados["numeroof"] : $row->numeroof;
                $row->dataof = (array_key_exists("dataof",$dados)) ? $dados["dataof"] : $row->dataof;
                $row->proc = (array_key_exists("proc",$dados)) ? $dados["proc"] : $row->proc;
                $row->dataproc = (array_key_exists("dataproc",$dados)) ? $dados["dataproc"] : $row->dataproc;
                $row->numeroreinteracao = (array_key_exists("numeroreinteracao",$dados)) ? $dados["numeroreinteracao"] : $row->numeroreinteracao;
                $row->datareinteracao = (array_key_exists("datareinteracao",$dados)) ? $dados["datareinteracao"] : $row->datareinteracao;
                $row->iddocumentooriginal = (array_key_exists("iddocumentooriginal",$dados)) ? $dados["iddocumentooriginal"] : $row->iddocumentooriginal;
                $row->datadevolucao = (array_key_exists("datadevolucao",$dados)) ? $dados["datadevolucao"] : $row->datadevolucao;
                $row->posicao = (array_key_exists("posicao",$dados)) ? $dados["posicao"] : $row->posicao;
                $row->iddocumentoenviado = (array_key_exists("iddocumentoenviado",$dados)) ? $dados["iddocumentoenviado"] : $row->iddocumentoenviado;
                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->cienvioflag = (array_key_exists("cienvioflag", $dados)) ? $dados["cienvioflag"] : $row->cienvioflag;		
                $row->cienviodt = (array_key_exists("cienviodt", $dados)) ? $dados["cienviodt"] : $row->cienviodt;		
                $row->cienvio = (array_key_exists("cienvio", $dados)) ? $dados["cienvio"] : $row->cienvio;		
                $row->idimgcienvio = (array_key_exists("idimgcienvio", $dados)) ? $dados["idimgcienvio"] : $row->idimgcienvio;
                $row->ciretornoflag = (array_key_exists("ciretornoflag", $dados)) ? $dados["ciretornoflag"] : $row->ciretornoflag;		
                $row->ciretornodt = (array_key_exists("ciretornodt", $dados)) ? $dados["ciretornodt"] : $row->ciretornodt;		
                $row->ciretorno = (array_key_exists("ciretorno", $dados)) ? $dados["ciretorno"] : $row->ciretorno;		
                $row->idimgciretorno = (array_key_exists("idimgciretorno", $dados)) ? $dados["idimgciretorno"] : $row->idimgciretorno;
       
		$row->save();
		
		return $row;
	}
	
}
