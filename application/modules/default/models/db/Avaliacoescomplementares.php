<?php

/**
 * Define o modelo Avaliacoescomplementares
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Avaliacoescomplementares extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "avaliacoescomplementares";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAvaliacoescomplementaresHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$avaliacoescomplementares = new Avaliacoescomplementares();
		return $avaliacoescomplementares->getAvaliacoescomplementares($queries, $page, $maxpage);
	}
	
	public function getAvaliacoescomplementares($queries = array(), $page = 0, $maxpage = 0) { 
		
		$where = array();		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$idtipoatividade = (isset($queries['idtipoatividade'])) ? (int)$queries['idtipoatividade'] : false;
		if ($idtipoatividade) array_push($where, " a1.idtipoatividade = $idtipoatividade ");

		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		if ($idaluno) array_push($where, " a1.idaluno = $idaluno ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");

		$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " a1.titulo LIKE '%$titulo%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='sequencial') $sorting[0]='a1.sequencial';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*";
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM avaliacoescomplementares a1
					
					WHERE a1.excluido='nao' 
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}

	public function getUltimoAvaliacaocomplementar($queries = array()) {
		$queries['order'] = 'ORDER BY a1.id DESC';
		$rows = $this->getavaliacoescomplementares($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	
	public function getAvaliacoescomplementaresById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAvaliacoescomplementares($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;

		return $rows[0];
	}
	
	public static function getAvaliacoescomplementaresByIdHelper($id, $queries = array()) {
		$rows = new Avaliacoescomplementares();
		return $rows->getAvaliacoescomplementaresById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Avaliacoescomplementares
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else $novoRegistro = false;		
		
		$row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
		$row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
		$row->horalancamento = (array_key_exists("horalancamento",$dados)) ? $dados["horalancamento"] : $row->horalancamento;
		$row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
		$row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 		$row->iddiretor = (array_key_exists("iddiretor",$dados)) ? $dados["iddiretor"] : $row->iddiretor;
 		$row->idcoordenador = (array_key_exists("idcoordenador",$dados)) ? $dados["idcoordenador"] : $row->idcoordenador;
 		$row->idserie = (array_key_exists("idserie",$dados)) ? $dados["idserie"] : $row->idserie;
 		$row->idturma = (array_key_exists("idturma",$dados)) ? $dados["idturma"] : $row->idturma;
 		$row->idtipoavaliacao = (array_key_exists("idtipoavaliacao",$dados)) ? $dados["idtipoavaliacao"] : $row->idtipoavaliacao;

 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;		
				
		$row->save();
		
		return $row;
	}	
}