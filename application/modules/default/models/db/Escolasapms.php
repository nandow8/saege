<?php

/**
 * Define o modelo Escolasapms
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasapms extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasapms";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolasapmsHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasapms = new Escolasapms();
		return $escolasapms->getEscolasapms($queries, $page, $maxpage);
	}
	
	public function getEscolasapms($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " e1.tipo LIKE '%$tipo%' ");

$cpfcnpj = (isset($queries["cpfcnpj"])) ? $queries["cpfcnpj"] : false;
		if ($cpfcnpj) array_push($where, " e1.cpfcnpj LIKE '%$cpfcnpj%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");

$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
if ($idsecretaria) array_push($where, " es1.idsecretaria=$idsecretaria ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		$group_by = " GROUP BY e1.id ";

		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasapms e1
						LEFT JOIN escolas es1 ON es1.id = e1.idescola 
						LEFT JOIN escolasapmscontas e2 ON e1.id = e2.idparent 
					WHERE e1.excluido='nao' 
						$w 
						$group_by
					$ordem	
					$limit";	
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolaapmById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasapms($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaapmByIdHelper($id, $queries = array()) {
		$rows = new Escolasapms();
		return $rows->getEscolaapmById($id, $queries);
	}		
	
	public function getEscolaapmByIdEscola($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idescola'] = $id;
		$rows = $this->getEscolasapms($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaapmByIdEscolaHelper($id, $queries = array()) {
		$rows = new Escolasapms();
		return $rows->getEscolaapmByIdEscola($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasapms
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		 $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
 $row->cpfcnpj = (array_key_exists("cpfcnpj",$dados)) ? $dados["cpfcnpj"] : $row->cpfcnpj;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
		 $row->saldoexercicioanterior = (array_key_exists("saldoexercicioanterior",$dados)) ? $dados["saldoexercicioanterior"] : $row->saldoexercicioanterior;
		 $row->rendimentoaplicacaofinanceira = (array_key_exists("rendimentoaplicacaofinanceira",$dados)) ? $dados["rendimentoaplicacaofinanceira"] : $row->rendimentoaplicacaofinanceira;
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		 
				
		$row->save();
		if (array_key_exists('contas', $dados)) $this->setContas($dados['contas'], $row->id);

		return $row;
	}
	
	private function setContas($dados, $idparent) {
		
		if (!is_array($dados)) return;
		 
		$itens = new Escolasapmscontas();
		$bancos = $dados['banco'];
		$agencias = $dados['agencia'];
		$contas = $dados['conta'];
		$digitos = $dados['digito'];
		$tipos = $dados['tipo'];
		
		$ids = array();
		foreach ($bancos as $i=>$id) {
			$d = array();
			
			$d['idparent'] = $idparent; 
			$d['banco'] = trim(strip_tags($bancos[$i]));
			$d['agencia'] = trim(strip_tags($agencias[$i]));
			$d['conta'] = trim(strip_tags($contas[$i]));
			$d['digito'] = trim(strip_tags($digitos[$i]));
			$d['tipo'] = trim($tipos);
                        
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $dados['logusuario'];
			$d['logdata'] = $dados['logdata'];

                        //var_dump($bancos);die();
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
		}
		$contas = implode(",", $ids);
                
		if ($contas=="") $contas = "0";
		$strsql = "DELETE FROM escolasapmscontas WHERE idparent=$idparent AND id NOT IN ($contas)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);
		
	}

}