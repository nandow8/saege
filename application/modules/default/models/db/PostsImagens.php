<?php

/**
 * Define o modelo PostsImagens
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Action
 * @copyright   Copyright (c) 2014 MN Solu��es (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class PostsImagens extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "postsimagens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";		
	
	public function swapOrdem($idpost, $ordem, $d, $inverte) {
		if ($ordem > 0) {
			if ($inverte) $d = ($d=="up") ? "down" : "up"; //Inverte os Sentidos
			if ($d=="up") {
				$strsql = "SELECT * FROM postsimagens WHERE excluido='nao' AND idpost=$idpost AND ordem <= ".$ordem." ORDER BY ordem DESC LIMIT 2";
			} else {
				$strsql = "SELECT * FROM postsimagens WHERE excluido='nao' AND idpost=$idpost AND ordem >= ".$ordem." ORDER BY ordem LIMIT 2";
			}
		
			$db = Zend_Registry::get("db");
			$rows = $db->fetchAll($strsql);
			if (sizeof($rows)==2) {
				$db->beginTransaction();
				try {
					$strsql = "UPDATE postsimagens SET ordem = " . $rows[1]["ordem"] . 
						" WHERE id =" . $rows[0]["id"];
					$db->query($strsql);
				
					$strsql = "UPDATE postsimagens SET ordem = " . $rows[0]["ordem"] . 
						" WHERE id =" . $rows[1]["id"];
					$db->query($strsql);
					$db->commit();
				} catch (Exception $e) {
					$db->rollBack();
					echo $e->getMessage();
				}
			}
		}			
	}	

	public static function getPostsImagens($idpost = NULL, $status = NULL, $inverte = false, $limite = NULL) {
		$db = Zend_Registry::get('db');
		
		$w = "";
		if (!is_null($idpost)) $w .= " AND pi1.idpost = " . (int)$idpost;
		if (!is_null($status)) $w .= " AND pi1.status = '$status'"; 
		
		$l = "";
		if (!is_null($limite)) $l .= " LIMIT '$limite'";
		
		$order = "ORDER BY pi1.ordem";
		if ($inverte) $order .= " DESC";
		
		$strsql = "SELECT pi1.* 
					FROM postsimagens pi1 
					WHERE pi1.excluido = 'nao' 
					$w
					$order
					$l";		

		//die($strsql);	
		return $db->fetchAll($strsql);
	}	
	
	public static function getImagensByIdPost($idpost, $order = 'pi1.ordem') {
		$db = Zend_Registry::get('db');
		
		$strsql = "SELECT pi1.idimagem, pi1.idpost, i1.* 
					FROM postsimagens pi1 
						LEFT JOIN imagens i1 ON i1.id=pi1.idimagem
					WHERE pi1.excluido='nao' 
						AND pi1.idpost=$idpost 
					ORDER BY $order";	
		
		return $db->fetchAll($strsql);
	}
	
	public function setImagens($idpost, $imagens) {
		if (!is_array($imagens)) return;
		
		$logdata = date('Y-m-d G:i:s');
		foreach ($imagens as $i=>$idimagem) {
			$d = array();
			
			$d['idpost'] = $idpost;
			$d['idimagem'] = $idimagem;
			$d['ordem'] = $i;
			$d['excluido'] = 'nao';
			$this->save($d);
		}
		
		$db = Zend_Registry::get('db');
		
		$ids = implode(",", $imagens);
		if ($ids=="") $ids = "0";
		
		$strsql = "DELETE FROM postsimagens WHERE idpost = $idpost AND idimagem NOT IN ($ids)";
		
		$db->query($strsql);		
	}
	
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$idpost = (int)$dados['idpost'];
		$idimagem = (int)$dados['idimagem'];
		
		$row = $this->fetchRow("idpost=" . $idpost . " AND idimagem=" . $idimagem);
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 

		if ($id>0)$row->id = $id;
		$row->idpost = (isset($dados['idpost'])) ? $dados['idpost'] : $row->idpost;
		$row->legenda = (isset($dados['legenda'])) ? $dados['legenda'] : $row->legenda;
		
		if (($novoRegistro) || (!$dados['idimagem']===false)) {
			if (!$novoRegistro) {
				$imagens = new Imagens();
				if ($dados['idimagem']===false) $imagens->excluir($row->idimagem);				
			}			
			$row->idimagem = (isset($dados['idimagem'])) ? $dados['idimagem'] : $row->idimagem;
		}		
		
		$row->status = (isset($dados['status'])) ? $dados['status'] : $row->status;
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;
		$row->ordem = (isset($dados['ordem'])) ? $dados['ordem'] : $row->ordem;
		$row->save();
		
		if ($novoRegistro) {
			$row->ordem = $row->id;
			$row->save();			
		}
		
		return $row;
	}		
	
}