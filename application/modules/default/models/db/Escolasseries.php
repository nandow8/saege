<?php

/**
 * Define o modelo Escolasseries
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasseries extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "escolasseries";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getEscolasseriesHelper($queries = array(), $page = 0, $maxpage = 0) {
        $escolasseries = new Escolasseries();
        return $escolasseries->getEscolasseries($queries, $page, $maxpage);
    }

    public function getEscolasseries($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " e1.id = $id ");

        $idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
        if ($idsecretaria)
            array_push($where, " e1.idsecretaria = $idsecretaria ");

        $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
        if ($idescola)
            array_push($where, " e1.idescola = $idescola ");

        $idsseries = (isset($queries["idsseries"])) ? $queries["idsseries"] : false;
        if ($idsseries)
            array_push($where, " FIND_IN_SET(e1.id, '$idsseries') ");

        $idserietipo = (isset($queries["idserietipo"])) ? $queries["idserietipo"] : false;
        if ($idserietipo)
            array_push($where, " e1.idserietipo = $idserietipo ");

        $serie = (isset($queries["serie"])) ? $queries["serie"] : false;
        if ($serie)
            array_push($where, " e1.serie LIKE '%$serie%' ");

        $ano = (isset($queries["ano"])) ? $queries["ano"] : false;
        if ($ano)
            array_push($where, " e1.ano = '$ano' ");

        $termo = (isset($queries["termo"])) ? $queries["termo"] : false;
        if ($termo)
            array_push($where, " e1.termo LIKE '%$termo%' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " e1.status LIKE '%$status%' ");

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY e1." . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "e1.*";
        $fields1 = "ge.descricoes";
        $fields2 = "ges.idgdae, ges.descricao";
        ;

        if ($total)
            $fields = "COUNT(e1.id) as total";

        $ordem = "ORDER BY e1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields, $fields1, $fields2
					FROM
                        escolasseries e1
                        inner join gdae_identificadoresescolas ge on (e1.idserietipo = ge.codigo)
                        inner join gdae_identificadoresescolassubtipos ges on (ges.serie = e1.serie and e1.idserietipo = ges.idgdae)
					WHERE
                        e1.excluido='nao'
                        and FIND_IN_SET(ge.codigo,(select idsensinostipos from escolas where id = e1.idescola))
                        and FIND_IN_SET(ges.id,(select idsensinossubtipos from escolas where id = e1.idescola))
                        $w
                        $ordem
                        $limit
                        ";
        //die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEscolaserieById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getEscolasseries($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getEscolaserieByIdHelper($id, $queries = array()) {
        $rows = new Escolasseries();
        return $rows->getEscolaserieById($id, $queries);
    }

    public function getSeriesByIdEscola($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['idescola'] = $id;

        $rows = $this->getEscolasseries($queries, 0, 0);
        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getSeriesByIdEscolaHelper($id, $queries = array()) {
        $rows = new Escolasseries();
        return $rows->getEscolaserieById($id, $queries);
    }

    /**
     * Retorna somente a turmas
     */
    public function getEscolasseriesAgrupadas($queries = array()) {
        $where = array();

        $idescola = (isset($queries['idescola'])) ? $queries['idescola'] : '';
        if ($idescola)
            array_push($where, " idescola=$idescola ");

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = " AND ($w)";

        $strsql = "SELECT DISTINCT serie "
                . "FROM escolasseries as es1 "
                . "WHERE es1.excluido='nao' "
                . $w
                . " AND ( es1.status LIKE '%Ativo%' )";
        echo $strsql;
        $db = Zend_Registry::get('db');

        $rows = $db->fetchAll($strsql);
        if (sizeof($rows) == 0)
            return false;
        return $rows;
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasseries
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idsecretaria = (array_key_exists("idsecretaria", $dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
        $row->idescola = (array_key_exists("idescola", $dados)) ? $dados["idescola"] : $row->idescola;
        $row->idserietipo = (array_key_exists("idserietipo", $dados)) ? $dados["idserietipo"] : $row->idserietipo;
        $row->serie = (array_key_exists("serie", $dados)) ? $dados["serie"] : $row->serie;
        $row->tipoensino = (array_key_exists("tipoensino", $dados)) ? $dados["tipoensino"] : $row->tipoensino;
        $row->quantidadesaulas = (array_key_exists("quantidadesaulas", $dados)) ? $dados["quantidadesaulas"] : $row->quantidadesaulas;
        $row->ano = (array_key_exists("ano", $dados)) ? $dados["ano"] : $row->ano;
        $row->termo = (array_key_exists("termo", $dados)) ? $dados["termo"] : $row->termo;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        $row->idsmaterias = (array_key_exists("idsmaterias", $dados)) ? $dados["idsmaterias"] : $row->idsmaterias;
        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;


        $row->save();

        return $row;
    }

    public static function getEscolasseriesdoprofessorHelper($queries = array(), $page = 0, $maxpage = 0) {
        $escolasseries = new Escolasseries();
        return $escolasseries->getEscolasseriesdoprofessor($queries, $page, $maxpage);
    }

    public function getEscolasseriesdoprofessor($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();
        
        $fields = "e1.*";
        $fields1 = "ge.descricoes";
        $fields2 = "ges.idgdae, ges.descricao"; 
        
        $strsql = "SELECT DISTINCT $fields, $fields1, $fields2
					FROM
                        escolasseries e1
                        inner join gdae_identificadoresescolas ge on (e1.idserietipo = ge.codigo)
                        inner join gdae_identificadoresescolassubtipos ges on (ges.serie = e1.serie and e1.idserietipo = ges.idgdae)
                        inner join escolasvinculos elv ON elv.idserie = e1.id
					WHERE
                        e1.excluido='nao'
                        and FIND_IN_SET(ge.codigo,(select idsensinostipos from escolas where id = e1.idescola))
                        and FIND_IN_SET(ges.id,(select idsensinossubtipos from escolas where id = e1.idescola))
                        AND elv.id IN ( $queries[vinculos] ) 
                        AND e1.idescola = $queries[idescola]
                        ";
       
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public static function getVinculosprofessorHelper($idprofessor) {
        $escolasseries = new Escolasseries();
        return $escolasseries->getEscolasseriesdoprofessor($idprofessor);
	}
	

    public function getVinculosprofessor($idprofessor) {

		$strsql = "SELECT GROUP_CONCAT(idescolavinculo) as ids FROM `escolasvinculosprofessoresmaterias` ev 
                    JOIN funcionariosgeraisescolas f ON ev.idprofessor = f.id
                    WHERE idprofessor = $idprofessor";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        return $db->fetchAll($strsql);
    }

}
