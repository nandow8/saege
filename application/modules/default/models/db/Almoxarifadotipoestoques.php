<?php

class Almoxarifadotipoestoques extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "almoxarifadotipoestoques";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public function getTiposEstoques($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " at.id = $id ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "at.*";

        $ordem = "ORDER BY at.descricao";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(at.id) as total FROM almoxarifadotipoestoques at WHERE at.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM almoxarifadotipoestoques at WHERE at.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        return $db->fetchAll($strsql);
    }
}