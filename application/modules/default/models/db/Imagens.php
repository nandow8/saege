<?php

/**
 * Define o modelo Documentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Imagens extends Zend_Db_Table_Abstract {
	
	
	public static $INDISPONIVEL = "indisponivel";
	public static $USUARIO = "usuario";
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "imagens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";			
	
	/**
	 * Define o tipo de imagem indisponível
	 * @param string $val
	 * @return string
	 */
	public function indisponiveis($val) {
		$i = array(
			self::$INDISPONIVEL => 'noimage.gif',
			self::$USUARIO => 'indisponivel3x4.jpg'
		);
		
		if (isset($i[$val])) return $i[$val];
		return $i[self::$INDISPONIVEL];
	}
	
	public static function clonar($id, $field = false) {
		$id = (int)$id;
		$rows = new Imagens();
		$row = $rows->fetchRow("id=".$id);
		
		if (!$row) return false;

		$row_new = $row->toArray();
		unset($row_new['id']);
		
		$row_new = $rows->save($row_new);
		
		$uploadDirOrig = $rows->getUploadDir("") . $row['id'] . '.dat';;
		$uploadDirDest = $rows->getUploadDir("") . $row_new['id'] . '.dat';
		copy( $uploadDirOrig , $uploadDirDest );
		if (!$field) return $row_new->toArray();
		
		$row_new = $row_new->toArray();
		return $row_new[$field];
	}
	
	
	/**
	 * Localiza o arquivo para download da imagem, caso não encontre então localiza o arquivo de indisponibilidade e retorna um array contendo informações de acesso
	 * @param string $id
	 * @param string $indisponivel
	 * @return array
	 */
	public function downloadImagem($id, $indisponivel = "") {
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		$filename = "indisponivel.jpg";
		$sourceFilename = $this->indisponiveis($indisponivel);
		
		if ($row) {
			$filename = $row['filename'];
			$sourceFilename = $row['id'] . '.dat';

			$uploadDir = $this->getUploadDir("");
			$sourceFilename = $uploadDir . $sourceFilename;
		} else {
			$uploadDir = $this->getUploadDir("indisponiveis");
			$sourceFilename = $uploadDir . $sourceFilename;			
		}
		

		$filesize = filesize($sourceFilename);	
		
		return array(
			'filename' => $filename,
			'sourceFilename' => $sourceFilename,
			'filesize' => $filesize,
		);
		
	}
	
	/**
	 * Acessa o caminho publico de url_imagens e faz o download das imagens
	 * @param string $url_imagens
	 * @param string $ns
	 * @param string $filename
	 * @return int
	 */
	public function getV1Imagens($url_imagens, $ns, $filename, $usuarioId) {
		try {
			$url_imagens = $url_imagens . $ns . $filename;
	
			
			$imagem = $this->getNew();
			
			$uploadDir = $this->getUploadDir("");
			$uploadFileOriginal = $uploadDir . $imagem['id'] . ".dat";
			
			$source=@fopen($url_imagens,"r");
	
			if ($source===false) return 0;
			
			
	    	$destination = @fopen($uploadFileOriginal, "w"); 
	    	while ($a=fread($source,1024)) fwrite($destination,$a);
			
	    	fclose($destination);
	    	fclose($source);
	    	
	    	$dados = array(
	    		'id'=>$imagem['id'],
				'filename' => $filename,
				'size' => filesize($uploadFileOriginal),
				'logdata' => date('Y-m-d G:i:s'), 
				'logusuario' => $usuarioId,
				'logusuariotipo' => 'admin',
				'excluido' => 'nao'
	    	);
	    	$this->save($dados);
	    	
		    if (0 === error_reporting()) {
		    	echo "reporting";
        		die();
    		}	    	
	    	
	    	return $imagem['id'];
		} catch (Exception $e) {
			echo "catch";
		}
	}
	
	/**
	 * Executa o upload da imagem vinda do formulário
	 * @param unknown_type $file_key
	 * @return Imagem
	 */
	public function getImagemFromForm($file_key, $usuarioId = NULL, $usuarioTipo = NULL, $apenas_copia = false) {
		$max_size = 20000000;
		$max_width = 800;
		$max_height = 600;
	
		if (!isset($_FILES[$file_key])) return false;
		if ($_FILES[$file_key]['size']==0) return false;
		
  		$upload = $_FILES[$file_key]["name"];
  		$tmp_name = $_FILES[$file_key]["tmp_name"];	
  		$size = $_FILES[$file_key]["size"];
  		
  		if ($size>$max_size) throw new Exception("A imagem deve ter até ".number_format($max_size/1000, 3)." kb");
  		
  		$dimensoes = getimagesize($tmp_name);
  		if (!$dimensoes) throw new Exception("A imagem não é um arquivo válido");
  		
  		$width = $dimensoes[0];
  		$height = $dimensoes[1];
  		
		$si = new SimpleImage();
		$si->load($tmp_name);
		if ($width>$height) {
			//if ($width>$max_width) $si->resizeToWidth($max_width);
		} else {
			//if ($height>$max_height) $si->resizeToWidth($max_height);
		}

  		$imagem = $this->getNew();
			
  		$uploadDir = $this->getUploadDir("");
  		$filename = $uploadDir . $imagem['id'] . '.dat';
  		
  		if ($apenas_copia) {
  			copy($tmp_name, $filename);
  		} else {
  			$si->save($filename);
  		}
  		
	   	$dados = array(
	   		'id'=>$imagem['id'],
			'filename' => $_FILES[$file_key]["name"],
			'size' => $_FILES[$file_key]["size"],
			'logdata' => date('Y-m-d G:i:s'), 
			'logusuario' => $usuarioId,
			'logusuariotipo' => $usuarioTipo,
			'excluido' => 'nao'
	   	);
	   	$row = $this->save($dados);
	   	return $imagem['id'];
	}
	
	
	public function getImagemFromFormsByPos($file_key, $pos = 0, $usuarioId = NULL, $usuarioTipo = NULL) {
		if (!isset($_FILES[$file_key])) return false;
		if ($_FILES[$file_key]['size'][$pos]==0) return false;
		
  		$upload = $_FILES[$file_key]["name"][$pos];
  		$tmp_name = $_FILES[$file_key]["tmp_name"][$pos];	
  		$size = $_FILES[$file_key]["size"][$pos];
  		

  		$imagem = $this->getNew();
			
  		$uploadDir = $this->getUploadDir("");
  		$filename = $uploadDir . $imagem['id'] . '.dat';
  		
  		move_uploaded_file($tmp_name,$filename);
  		
  		$dados = array();
  		$dados['id'] = $imagem['id'];
  		$dados['filename'] = $_FILES[$file_key]["name"][$pos];
  		$dados['size'] = $_FILES[$file_key]["size"][$pos];
  		$dados['ordem'] = $imagem['id'];
  		$dados['logdata'] = date('Y-m-d G:i:s');
  		$dados['logusuario'] = $usuarioId;
  		$dados['logusuariotipo'] = $usuarioTipo;
  		$dados['excluido'] = 'nao';
  		
	   	$row = $this->save($dados);
	   	return $imagem['id'];

	}
	
	public function saveBase64($base64, $usuarioId = NULL, $usuarioTipo = NULL) {
		$imagem = $this->getNew();
		
		$img = base64_decode($base64);
		$uploadDir = $this->getUploadDir("");
		$filename = $uploadDir . $imagem['id'] . '.dat';
		
		$fp = fopen($filename, 'w');
		fwrite($fp, $img);
		fclose($fp);			
			
	   	$dados = array(
	   		'id'=>$imagem['id'],
			'filename' => $imagem['id'] . '.dat',
			'size' => strlen($img),
			'logdata' => date('Y-m-d G:i:s'), 
			'logusuario' => $usuarioId,
			'logusuariotipo' => $usuarioTipo,
			'excluido' => 'nao'
	   	);
	   	$row = $this->save($dados);

	   	return $row;
	}
	
	/**
	 * Excluir todas as imagens do servidor de imagem
	 */
	public function deleteAllFilesFromServer() {
		$uploadDir = $this->getUploadDir("");
		foreach(glob($uploadDir.'*.*') as $v) unlink($v);
		
		$uploadDir = $this->getUploadDir("lixeira");
		foreach(glob($uploadDir.'*.*') as $v) unlink($v);		
	}

	/**
	 * Exclui registro e envia imagem para a lixeira
	 * @param int $idimagem
	 */
	public function excluir($idimagem) {
		$row = $this->fetchRow("id=$idimagem");
		if (!$row) return; 
		
		$row = $row->toArray();
		$row['excluido'] = 'sim';
		$this->save($row);
		
		$filename = $row['filename'];
		$sourceFilename = $row['id'] . '.dat';

		$uploadDir = $this->getUploadDir("");
		$origem = $uploadDir . $sourceFilename;	
		
		$uploadDir = $this->getUploadDir("lixeira");
		$destino  = $uploadDir . $sourceFilename;
		
		if (file_exists($origem)) {
			rename($origem, $destino);	
		}
	}
	
	/**
	 * Gera um novo registro
	 * @return Imagens
	 */
	public function getNew() {
		$row = $this->createRow();
		$row->save();
		$row->ordem = $row->id;
		$row->save();
		return $row;
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Imagens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id");
		
		if (!$row) $row = $this->createRow();
		else if (!is_null($row['filename'])) {
			$novoRegistro = false;
		} 
								
		if ($id>0)$row->id = $id;
		
		$row->filename = $dados['filename'];
		$row->size = $dados['size'];
		$row->logdata = $dados['logdata'];
		$row->logusuario = $dados['logusuario'];
		$row->logusuariotipo = $dados['logusuariotipo'];
		$row->excluido = $dados['excluido'];
		$row->save();
		
		if ($novoRegistro) {
			$row->ordem = $row->id;
			$row->save();
		}
		
		return $row;
	}		
	

	/**
	 * Localiza a extensão de um arquivo
	 * @param string $filename
	 * @return string
	 */
	private function findexts ($filename) {
		$filename = strtolower($filename) ;
		$exts = preg_split('/\./', $filename, -1, PREG_SPLIT_OFFSET_CAPTURE);
		return $exts[sizeof($exts)-1][0];
	} 		

	/**
	 * Retorna o caminho absoluto do servidor de imagens
	 * @param string $subdir
	 * @return string
	 */
	public function getUploadDir($subdir = "") {
		$root = realpath(dirname(__FILE__) . '/../../../');
		$root = str_replace(chr(92), '/', $root);
		
		if ((substr($root, strlen($root)-1, 1))!="/") $root = $root."/";
		$root .= "servidor/imagens/";
		
		if (((substr($subdir, strlen($subdir)-1, 1))!="/") && ($subdir!="")) $subdir = $subdir."/";
		
		return $root . $subdir;
	} 			
	
	
}