<?php

/**
 * Define o modelo Aeelistamaterias
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 FJMX.
 * @version     1.0
 */
class Aeelistamaterias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "aeelistamaterias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAeelistamateriasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$Aeelistamaterias = new Aeelistamaterias();
		return $Aeelistamaterias->getAeelistamaterias($queries, $page, $maxpage);
	}
	
	public function getAeelistamaterias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " aee1.id = $id ");
		 
		$idaee = (isset($queries["idaee"])) ? $queries["idaee"] : false;
		if ($idaee) array_push($where, " aee1.idaee = $idaee ");
		 
		$materia = (isset($queries["materia"])) ? $queries["materia"] : false;
		if ($materia) array_push($where, " aee1.materia = $materia ");
		 
		$diassemana = (isset($queries["diassemana"])) ? $queries["diassemana"] : false;
		if ($diassemana) array_push($where, " aee1.diassemana = $diassemana ");
		 
		$horaaulainicio = (isset($queries["horaaulainicio"])) ? $queries["horaaulainicio"] : false;
		if ($horaaulainicio) array_push($where, " aee1.horaaulainicio = $horaaulainicio ");
		 
		$horaaulafim = (isset($queries["horaaulafim"])) ? $queries["horaaulafim"] : false;
		if ($horaaulafim) array_push($where, " aee1.horaaulafim = $horaaulafim ");

		 


		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='materia') $sorting[0]='l1.materia';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "aee1.*"; 
		;
		
		if ($total) $fields = "COUNT(aee1.id) as total";
		
		$ordem = "ORDER BY aee1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM aeelistamaterias aee1
					
					WHERE aee1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAeelistamateriasById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAeelistamaterias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAeelistamateriasByIdHelper($id, $queries = array()) {
		$rows = new Aeelistamaterias();
		return $rows->getAeelistamateriasById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Aeelistamaterias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idaee = (array_key_exists("idaee",$dados)) ? $dados["idaee"] : $row->idaee;
		$row->segundafeira = (array_key_exists("segundafeira",$dados)) ? $dados["segundafeira"] : $row->segundafeira;
		$row->tercafeira = (array_key_exists("tercafeira",$dados)) ? $dados["tercafeira"] : $row->tercafeira;
		$row->quartafeira = (array_key_exists("quartafeira",$dados)) ? $dados["quartafeira"] : $row->quartafeira;
		$row->quintafeira = (array_key_exists("quintafeira",$dados)) ? $dados["quintafeira"] : $row->quintafeira;
		$row->sextafeira = (array_key_exists("sextafeira",$dados)) ? $dados["sextafeira"] : $row->sextafeira;
		$row->excluido = 'nao';
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}