<?php

class Almoxarifado_Entradasitenshistoricos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoentradasitenshistoricos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
 	public function arquiva($dados) {
		$dados['identidade'] = $dados['id'];
		unset($dados['id']);
			
		$this->createRow($dados)->save();	
	}
}