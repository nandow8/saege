<?php

class Almoxarifado_Entradas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoentradas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_ORIGEM_SECRETARIA = 'secretaria';
	public static $_ORIGEM_ESCOLA = 'escola';	
	
	/**
	 * 
	 */
	public static function getOrigem($field = false) {
		$res = array(
			self::$_ORIGEM_SECRETARIA => 'Secretária',
			self::$_ORIGEM_ESCOLA => 'Escola'
		);
		
		if (!$field) return $res;
		return $res[$field];
	}
	
	public static function getEntradasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Almoxarifado_Entradas();
		return $produtos->getEntradas($queries, $page, $maxpage);
	}
	
	public function getEntradas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$idsaida = (isset($queries['idsaida'])) ? (int)$queries['idsaida'] : false;		
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$aprovacao = (isset($queries['aprovacao'])) ? $queries['aprovacao'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$iddepartamentosecretaria = (isset($queries['iddepartamentosecretaria'])) ? $queries['iddepartamentosecretaria'] : false;
		$idescola = (isset($queries['idescola'])) ? $queries['idescola'] : false;
		$iddepartamentoescola = (isset($queries['iddepartamentoescola'])) ? $queries['iddepartamentoescola'] : false;
		
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " ae1.id=$id ");	
		if ($idsecretaria) array_push($where, " ae1.idsecretaria=$idsecretaria ");
		if ($idsaida) array_push($where, " ae1.idsaida=$idsaida ");	
		if ($chave) array_push($where, " ((ae1.titulo LIKE '%$chave%') OR (ae1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " ae1.status='$status' ");
		if ($origem) array_push($where, " ae1.origem='$origem' ");
		if ($iddepartamentosecretaria) array_push($where, " ae1.iddepartamentosecretaria=$iddepartamentosecretaria ");
		if ($idescola) array_push($where, " ae1.idescola=$idescola ");
		if ($iddepartamentoescola) array_push($where, " ae1.iddepartamentoescola=$iddepartamentoescola ");
		if ($aprovacao) array_push($where, " ae1.aprovacao='$aprovacao' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "ae1.*";
		if ($total) $fields = "COUNT(ae1.id) as total";
		
		
		$ordem = "ORDER BY ae1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadoentradas ae1
					WHERE ae1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getEntradaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEntradas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEntradaByIdSaidaHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Entradas();
		return $rows->getEntradaById($id, $queries);
	}

	public function getEntradaByIdSaida($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idsaida'] = $id;
		$rows = $this->getEntradas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEntradaByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Entradas();
		return $rows->getEntradaById($id, $queries);
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Entradas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();	
			$row->datacriacao = date('Y-m-d G:i:s');	
		}else {
			$novoRegistro = false;
			
			$historico = new Almoxarifado_Entradashistoricos();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);
		} 	
		$row->idsaida = (array_key_exists('idsaida',$dados)) ? $dados['idsaida'] : $row->idsaida;
		$row->titulo = (array_key_exists('titulo',$dados)) ? $dados['titulo'] : $row->titulo;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;

		$row->origem = (array_key_exists('origem',$dados)) ? $dados['origem'] : $row->origem;
		$row->iddepartamentosecretaria = (array_key_exists('iddepartamentosecretaria',$dados)) ? $dados['iddepartamentosecretaria'] : $row->iddepartamentosecretaria;
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->iddepartamentoescola = (array_key_exists('iddepartamentoescola',$dados)) ? $dados['iddepartamentoescola'] : $row->iddepartamentoescola;
		
		$row->aprovacao = (array_key_exists('aprovacao',$dados)) ? $dados['aprovacao'] : $row->aprovacao;
		$row->confirmaaprovacao = (array_key_exists('confirmaaprovacao',$dados)) ? $dados['confirmaaprovacao'] : $row->confirmaaprovacao;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		//var_dump($row->toArray()); die();
		$row->save();

		if (array_key_exists('itens', $dados)) $this->setItens($dados['itens'], $row->id, $row->logusuario, $row->logdata);
		return $row;
	}
	
	private function setItens($dados, $identrada, $logusuario = false, $logdata = false) {
		if (!is_array($dados)) return;
		 
		$itens = new Almoxarifado_Entradasitens();
		$ids = $dados['idsentradas'];
		$idsprodutos = $dados['idsprodutos'];
		$idsfornecedores = $dados['idsfornecedores'];
		$quantidades = $dados['quantidades'];
		$itensstatus = $dados['itensstatus'];
		$itensobservacoes = $dados['itensobservacoes'];
		
		$ids = array();
		foreach ($idsprodutos as $i=>$id) {
			$d = array();
			
			$d['id'] = $ids[$i];
			$d['identrada'] = $identrada; 
			$d['idproduto'] = trim(strip_tags($idsprodutos[$i])); 
			$d['idfornecedor'] = trim(strip_tags($idsfornecedores[$i]));
			$d['quantidade'] = trim(strip_tags(Mn_Util::trataNum($quantidades[$i])));
			$d['observacoes'] = trim(strip_tags($itensobservacoes[$i]));
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $logusuario;
			$d['logdata'] = $logdata;
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
			//verificar as linhas comentadas acima, elas são responsaveis pelas 2 linhas mais cridas no cronogramasliehistorico
		}

		
		$itens = implode(",", $ids);
		
		if ($itens=="") $itens = "0";
		$strsql = "DELETE FROM almoxarifadoentradasitens WHERE identrada=$identrada AND id NOT IN ($itens)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);	
			
	}
}