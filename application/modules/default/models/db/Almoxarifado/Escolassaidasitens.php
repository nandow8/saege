<?php

class Almoxarifado_Escolassaidasitens extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoescolassaidasitens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getEscolassaidasitensHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Almoxarifado_Escolassaidasitens();
		return $produtos->getEscolassaidasitens($queries, $page, $maxpage);
	}
	
	public function getEscolassaidasitens($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$diferentidsaida = (isset($queries['diferentidsaida'])) ? (int)$queries['diferentidsaida'] : false;
		$idproduto = (isset($queries['idproduto'])) ? (int)$queries['idproduto'] : false;
		$idsaida = (isset($queries['idsaida'])) ? (int)$queries['idsaida'] : false;	
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$somaentrada = (isset($queries['somaentrada'])) ? $queries['somaentrada'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " si1.id=$id ");
		if ($diferentidsaida) array_push($where, " si1.idsaida<>$diferentidsaida ");	
		if ($idsaida) array_push($where, " si1.idsaida=$idsaida ");
		if ($idproduto) array_push($where, " si1.idproduto=$idproduto ");
		if ($idescola) array_push($where, " s1.idescola=$idescola ");	
		if ($chave) array_push($where, " ((si1.observacoes LIKE '%$chave%') OR (s1.titulo LIKE '%$chave%') OR (s1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " si1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "si1.*, s1.titulo,  s1.descricoes, f1.razaosocial, f1.nomefantasia, f1.cpfcnpj, p1.produto, p1.descricoes as p1descricoes, p1.codigoproduto";
		if ($total) $fields = "COUNT(si1.id) as total";
		if ($somaentrada)  $fields = "SUM(si1.quantidade) as total";
		
		$ordem = "ORDER BY si1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadoescolassaidasitens si1
						LEFT JOIN almoxarifadoescolassaidas s1 ON s1.id = si1.idsaida 
						LEFT JOIN fornecedores f1 ON f1.id = si1.idfornecedor
						LEFT JOIN almoxarifadoprodutos p1 ON p1.id = si1.idproduto
					WHERE si1.excluido='nao'
						AND s1.excluido = 'nao'
						$w 
					$ordem	
					$limit";	
		//if($somaentrada) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		if ($somaentrada) {
			$row = $db->fetchRow($strsql);
			return (int)$row['total'];		
		}	
		return $db->fetchAll($strsql);		
	}	
	
	public function getEscolasaidaitemById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolassaidasitens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolasaidaitemByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Escolassaidasitens();
		return $rows->getEscolasaidaitemById($id, $queries);
	}		
	
	public function getQuantidadeByIdProduto($idproduto, $queries = array()) {
		if ($idproduto==0) return false;
		$queries['somaentrada'] = true;
		$queries['idproduto'] = $idproduto;
		$rows = $this->getEscolassaidasitens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows;
	}

	public static function getQuantidadeByIdProdutoHelper($idproduto, $queries = array()) {
		$rows = new Almoxarifado_Escolassaidasitens();
		return $rows->getQuantidadeByIdProduto($idproduto, $queries);
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolassaidasitens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();	
			$row->datacriacao = date('Y-m-d G:i:s');	
		}else {
			$novoRegistro = false;
			
			$historico = new Almoxarifado_Escolassaidasitenshistoricos();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);
		}
		$row->idsaida = (array_key_exists('idsaida',$dados)) ? $dados['idsaida'] : $row->idsaida;
		$row->idproduto = (array_key_exists('idproduto',$dados)) ? $dados['idproduto'] : $row->idproduto;
		$row->idfornecedor = (array_key_exists('idfornecedor',$dados)) ? $dados['idfornecedor'] : $row->idfornecedor;
		$row->idmarca = (array_key_exists('idmarca',$dados)) ? $dados['idmarca'] : $row->idmarca;
		
		$row->quantidade = (array_key_exists('quantidade',$dados)) ? $dados['quantidade'] : $row->quantidade;
		$row->observacoes = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
	
	
}