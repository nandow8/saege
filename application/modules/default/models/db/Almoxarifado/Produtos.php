<?php

class Almoxarifado_Produtos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoprodutos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getProdutosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Almoxarifado_Produtos();
		return $produtos->getProdutos($queries, $page, $maxpage);
	}
	
	public function getProdutos($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " ap1.id=$id ");
		if ($idsecretaria) array_push($where, " ap1.idsecretaria=$idsecretaria ");
		if ($chave) array_push($where, " ((ap1.produto LIKE '%$chave%') OR (ap1.descricoes LIKE '%$chave%') OR (ap1.codigoproduto LIKE '%$chave%')) ");
		if ($status) array_push($where, " ap1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "ap1.*";
		if ($total) $fields = "COUNT(ap1.id) as total";
		
		
		$ordem = "ORDER BY ap1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadoprodutos ap1
					WHERE ap1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getProdutoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getProdutos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getProdutonomeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$produtos = new Almoxarifado_Produtos();
		$queries['id'] = $id;
		$rows = $produtos->getProdutos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$row = $rows[0];
		
		return $row['razaosocial'] . ' ' .$row['nomefantasia'] ;
	}
	
	public static function getProdutoByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Produtos();
		return $rows->getProdutoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Produtos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
			$row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->produto = (array_key_exists('produto',$dados)) ? $dados['produto'] : $row->produto;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;
		$row->codigoproduto = (array_key_exists('codigoproduto',$dados)) ? $dados['codigoproduto'] : $row->codigoproduto;		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		
		$row->save();

		return $row;
	}
	
	
}