<?php

class Almoxarifado_Escolassaidas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoescolassaidas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getEscolassaidasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Almoxarifado_Escolassaidas();
		return $produtos->getEscolassaidas($queries, $page, $maxpage);
	}
	
	public function getEscolassaidas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;		
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$aprovacao = (isset($queries['aprovacao'])) ? $queries['aprovacao'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " es1.id=$id ");	
		if ($idsecretaria) array_push($where, " es1.idsecretaria=$idsecretaria ");
		if ($idescola) array_push($where, " es1.idescola=$idescola ");	
		if ($chave) array_push($where, " ((es1.titulo LIKE '%$chave%') OR (es1.descricoes LIKE '%$chave%')) ");
		if ($aprovacao) array_push($where, " es1.aprovacao='$aprovacao' ");
		if ($status) array_push($where, " es1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "es1.*";
		if ($total) $fields = "COUNT(es1.id) as total";
		
		
		$ordem = "ORDER BY es1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadoescolassaidas es1
					WHERE es1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getEscolasaidaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolassaidas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolasaidaByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Escolassaidas();
		return $rows->getEscolasaidaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Saidas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();	
			$row->datacriacao = date('Y-m-d G:i:s');	
		}else {
			$novoRegistro = false;
			
			$historico = new Almoxarifado_Escolassaidashistoricos();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);
		} 	
		$row->titulo = (array_key_exists('titulo',$dados)) ? $dados['titulo'] : $row->titulo;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;

		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->iddepartamentoescola = (array_key_exists('iddepartamentoescola',$dados)) ? $dados['iddepartamentoescola'] : $row->iddepartamentoescola;
		
		$row->aprovacao = (array_key_exists('aprovacao',$dados)) ? $dados['aprovacao'] : $row->aprovacao;
		$row->confirmaaprovacao = (array_key_exists('confirmaaprovacao',$dados)) ? $dados['confirmaaprovacao'] : $row->confirmaaprovacao;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
		}
		
		$row->save();

		if (array_key_exists('itens', $dados)) $this->setItens($dados['itens'], $row->id, $row->logusuario, $row->logdata);
		return $row;
	}
	
	private function setItens($dados, $idsaida, $logusuario = false, $logdata = false) {
		if (!is_array($dados)) return;
		 
		$itens = new Almoxarifado_Escolassaidasitens(); 
		$ids = $dados['idssaidas'];
		$idsprodutos = $dados['idsprodutos'];
		$idsfornecedores = $dados['idsfornecedores'];
		$quantidades = $dados['quantidades'];
		$itensstatus = $dados['itensstatus'];
		$itensobservacoes = $dados['itensobservacoes'];
		
		$ids = array();
		foreach ($idsprodutos as $i=>$id) {
			$d = array();
			
			$d['id'] = $ids[$i];
			$d['idsaida'] = $idsaida; 
			$d['idproduto'] = trim(strip_tags($idsprodutos[$i])); 
			$d['idfornecedor'] = trim(strip_tags($idsfornecedores[$i]));
			$d['quantidade'] = trim(strip_tags(Mn_Util::trataNum($quantidades[$i])));
			$d['observacoes'] = trim(strip_tags($itensobservacoes[$i]));
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $logusuario;
			$d['logdata'] = $logdata;
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
			//verificar as linhas comentadas acima, elas são responsaveis pelas 2 linhas mais cridas no cronogramasliehistorico
		}

		
		$itens = implode(",", $ids);
		
		if ($itens=="") $itens = "0";
		$strsql = "DELETE FROM almoxarifadoescolassaidasitens WHERE idsaida=$idsaida AND id NOT IN ($itens)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);	
			
	}
}