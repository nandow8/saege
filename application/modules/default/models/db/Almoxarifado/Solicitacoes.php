<?php

class Almoxarifado_Solicitacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadosolicitacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_ORIGEM_SECRETARIA = 'secretaria';
	public static $_ORIGEM_ESCOLA = 'escola';	
	
	/**
	 * 
	 */
	public static function getOrigem($field = false) {
		$res = array(
			self::$_ORIGEM_SECRETARIA => 'Secretária',
			self::$_ORIGEM_ESCOLA => 'Escola'
		);
		
		if (!$field) return $res;
		return $res[$field];
	}
	
	public static function getSolicitacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Almoxarifado_Solicitacoes();
		return $produtos->getSolicitacoes($queries, $page, $maxpage);
	}
	
	public function getSolicitacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;	
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$aprovacao = (isset($queries['aprovacao'])) ? $queries['aprovacao'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " as1.id=$id ");
		if ($idsecretaria) array_push($where, " as1.idsecretaria=$idsecretaria ");
		if ($idescola) array_push($where, " as1.idescola=$idescola ");	
		if ($chave) array_push($where, " ((as1.titulo LIKE '%$chave%') OR (as1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " as1.status='$status' ");
		if ($aprovacao) array_push($where, " as1.aprovacao='$aprovacao' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "as1.*";
		if ($total) $fields = "COUNT(as1.id) as total";
		
		
		$ordem = "ORDER BY as1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadosolicitacoes as1
					WHERE as1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getSolicitacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSolicitacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSolicitacaoByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Solicitacoes();
		return $rows->getSolicitacaoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Solicitacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();	
			$row->datacriacao = date('Y-m-d G:i:s');	
		}else {
			$novoRegistro = false;
			
			$historico = new Almoxarifado_Solicitacoeshistoricos();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);
		} 	
		$row->titulo = (array_key_exists('titulo',$dados)) ? $dados['titulo'] : $row->titulo;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;
		$row->quantidade = (array_key_exists('quantidade',$dados)) ? $dados['quantidade'] : $row->quantidade;
		$row->origem = (array_key_exists('origem',$dados)) ? $dados['origem'] : $row->origem;
		$row->idusuariosecretaria = (array_key_exists('idusuariosecretaria',$dados)) ? $dados['idusuariosecretaria'] : $row->idusuariosecretaria;
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->idusuarioescola = (array_key_exists('idusuarioescola',$dados)) ? $dados['idusuarioescola'] : $row->idusuarioescola;
		
		$row->aprovacao = (array_key_exists('aprovacao',$dados)) ? $dados['aprovacao'] : $row->aprovacao;
		$row->confirmaaprovacao = (array_key_exists('confirmaaprovacao',$dados)) ? $dados['confirmaaprovacao'] : $row->confirmaaprovacao;
		$row->confirmasolicitacao = (array_key_exists('confirmasolicitacao',$dados)) ? $dados['confirmasolicitacao'] : $row->confirmasolicitacao;
		$row->idusuarioaprovacao = (array_key_exists('idusuarioaprovacao',$dados)) ? $dados['idusuarioaprovacao'] : $row->idusuarioaprovacao;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			
			if((isset($dados['origem_emissor'])) && ($row->origem==Almoxarifado_Solicitacoes::$_ORIGEM_ESCOLA)):
				$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
			elseif ((!isset($dados['origem_emissor'])) && ($row->origem==Almoxarifado_Solicitacoes::$_ORIGEM_ESCOLA)):
				$row->idsecretaria = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'idsecretaria');
			else:
				$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
			endif;
		}
		//var_dump($row->toArray()); die();
		$row->save();

		if (($row->aprovacao == "Sim") && (array_key_exists('itens', $dados))):
			$this->setItens($dados['itens'], $row->id, $row->logusuario, $row->logdata);
		endif;
		
		return $row;
	}
	
	private function setItens($dados, $idsolicitacao, $logusuario = false, $logdata = false) {
		
		if (!is_array($dados)) return;
		 
		$saidas = new Almoxarifado_Saidas();
		$saida = $saidas->getSaidaByIdSolicitacao($idsolicitacao);
		
		if($saida) $dados['id'] = $saida['id'];
		$dados['solicitacao'] = 'Sim';
		$dados['idsolicitacao'] = $idsolicitacao;
		
		$saidas->save($dados);
	}
}