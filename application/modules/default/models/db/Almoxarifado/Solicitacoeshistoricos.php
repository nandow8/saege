<?php

class Almoxarifado_Solicitacoeshistoricos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadosolicitacoeshistoricos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
 	public function arquiva($dados) {
		$dados['identidade'] = $dados['id'];
		unset($dados['id']);
			
		$this->createRow($dados)->save();	
	}
}