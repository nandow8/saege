<?php

class Almoxarifado_Fornecedores extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadofornecedores";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getFornecedoresHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Almoxarifado_Fornecedores();
		return $produtos->getFornecedores($queries, $page, $maxpage);
	}
	
	public function getFornecedores($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " af1.id=$id ");
		if ($idsecretaria) array_push($where, " af1.idsecretaria=$idsecretaria ");
		if ($chave) array_push($where, " ((af1.razaosocial LIKE '%$chave%') OR (af1.nomefantasia LIKE '%$chave%') OR (af1.cpfcnpj LIKE '%$chave%')) ");
		if ($status) array_push($where, " af1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "af1.*";
		if ($total) $fields = "COUNT(af1.id) as total";
		
		
		$ordem = "ORDER BY af1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadofornecedores af1
					WHERE af1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getFornecedorById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFornecedores($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFornecedornomeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$fornecedores = new Almoxarifado_Fornecedores();
		$queries['id'] = $id;
		$rows = $fornecedores->getFornecedores($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$row = $rows[0];
		
		return $row['razaosocial'] . ' ' .$row['nomefantasia'] ;
	}
	
	public static function getFornecedorByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Fornecedores();
		return $rows->getFornecedorById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Fornecedores
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
		} 

		$row->idendereco = (array_key_exists('idendereco',$dados)) ? $dados['idendereco'] : $row->idendereco;
		$row->razaosocial = (array_key_exists('razaosocial',$dados)) ? $dados['razaosocial'] : $row->razaosocial;
		$row->nomefantasia = (array_key_exists('nomefantasia',$dados)) ? $dados['nomefantasia'] : $row->nomefantasia;
		$row->cpfcnpj = (array_key_exists('cpfcnpj',$dados)) ? $dados['cpfcnpj'] : $row->cpfcnpj;
		$row->telefone = (array_key_exists('telefone',$dados)) ? $dados['telefone'] : $row->telefone;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		
		$row->save();

		return $row;
	}
	
	
}