<?php

class Almoxarifado_Saidas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadosaidas";
	
	/** 
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_ORIGEM_SECRETARIA = 'secretaria';
	public static $_ORIGEM_ESCOLA = 'escola';	
	
	public static function getSaidasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Almoxarifado_Saidas();
		return $produtos->getSaidas($queries, $page, $maxpage);
	}
	
	public function getSaidas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;	
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$solicitacao = (isset($queries['solicitacao'])) ? $queries['solicitacao'] : false;
		$idsolicitacao = (isset($queries['idsolicitacao'])) ? $queries['idsolicitacao'] : false;
		$iddepartamentosecretaria = (isset($queries['iddepartamentosecretaria'])) ? $queries['iddepartamentosecretaria'] : false;
		$idescola = (isset($queries['idescola'])) ? $queries['idescola'] : false;
		$iddepartamentoescola = (isset($queries['iddepartamentoescola'])) ? $queries['iddepartamentoescola'] : false;
		$aprovacao = (isset($queries['aprovacao'])) ? $queries['aprovacao'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " as1.id=$id ");		
		if ($chave) array_push($where, " ((as1.titulo LIKE '%$chave%') OR (as1.descricoes LIKE '%$chave%')) ");
		if ($idsecretaria) array_push($where, " as1.idsecretaria=$idsecretaria ");		
		if ($origem) array_push($where, " as1.origem='$origem' ");
		if ($iddepartamentosecretaria) array_push($where, " as1.iddepartamentosecretaria=$iddepartamentosecretaria ");
		if ($idescola) array_push($where, " as1.idescola=$idescola ");
		if ($iddepartamentoescola) array_push($where, " as1.iddepartamentoescola=$iddepartamentoescola ");
		
		if ($idsolicitacao) array_push($where, " as1.idsolicitacao=$idsolicitacao ");
		if ($solicitacao) array_push($where, " IFNULL(as1.solicitacao, 'Não')='$solicitacao' ");
		
		if ($aprovacao) array_push($where, " as1.aprovacao='$aprovacao' ");
		if ($status) array_push($where, " as1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "as1.*";
		if ($total) $fields = "COUNT(as1.id) as total";
		
		
		$ordem = "ORDER BY as1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadosaidas as1
					WHERE as1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getSaidaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSaidas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSaidaByIdSolicitacaoHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Saidas();
		return $rows->getSaidaByIdSolicitacao($id, $queries);
	}

	public function getSaidaByIdSolicitacao($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idsolicitacao'] = $id;
		$rows = $this->getSaidas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSaidaByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Saidas();
		return $rows->getSaidaById($id, $queries);
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Saidas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();	
			$row->datacriacao = date('Y-m-d G:i:s');	
		}else {
			$novoRegistro = false;
			
			$historico = new Almoxarifado_Saidashistoricos();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);
		} 	
		$row->titulo = (array_key_exists('titulo',$dados)) ? $dados['titulo'] : $row->titulo;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;
		
		$row->origem = (array_key_exists('origem',$dados)) ? $dados['origem'] : $row->origem;
		$row->iddepartamentosecretaria = (array_key_exists('iddepartamentosecretaria',$dados)) ? $dados['iddepartamentosecretaria'] : $row->iddepartamentosecretaria;
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->iddepartamentoescola = (array_key_exists('iddepartamentoescola',$dados)) ? $dados['iddepartamentoescola'] : $row->iddepartamentoescola;

		$row->idsolicitacao = (array_key_exists('idsolicitacao',$dados)) ? $dados['idsolicitacao'] : $row->idsolicitacao;
		$row->solicitacao = (array_key_exists('solicitacao',$dados)) ? $dados['solicitacao'] : $row->solicitacao;
		$row->idusuarioaprovacao = (array_key_exists('idusuarioaprovacao',$dados)) ? $dados['idusuarioaprovacao'] : $row->idusuarioaprovacao;
		
		$row->aprovacao = (array_key_exists('aprovacao',$dados)) ? $dados['aprovacao'] : $row->aprovacao;
		$row->confirmaaprovacao = (array_key_exists('confirmaaprovacao',$dados)) ? $dados['confirmaaprovacao'] : $row->confirmaaprovacao;
		$row->baixalocal = (array_key_exists('baixalocal',$dados)) ? $dados['baixalocal'] : $row->baixalocal;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		
		$row->save();

		if (array_key_exists('itens', $dados)) $this->setItens($dados['itens'], $row->id, $row->logusuario, $row->logdata);
		if($dados['origem']==Almoxarifado_Entradas::$_ORIGEM_ESCOLA):
			if (array_key_exists('itens', $dados)) $this->setItensEscolas($dados['itens'], $row->id, $row->logusuario, $row->logdata, $row->idescola, $row->iddepartamentoescola, $row->toArray());
		endif;
		return $row;
	}
	
	private function setItens($dados, $idsaida, $logusuario = false, $logdata = false) {
		if (!is_array($dados)) return;
		 
		$itens = new Almoxarifado_Saidasitens();
		$ids = $dados['idssaidas'];
		$idsprodutos = $dados['idsprodutos'];
		$idsfornecedores = $dados['idsfornecedores'];
		$quantidades = $dados['quantidades'];
		$itensstatus = $dados['itensstatus'];
		$itensobservacoes = $dados['itensobservacoes'];

		foreach ($idsprodutos as $i=>$id) {
			$v_id = $ids[$i];
			$v_qtd = trim(strip_tags(Mn_Util::trataNum($quantidades[$i])));
			$verifica_produto = $itens->getSaidaitemByIdSaidaIdproduto($idsaida, trim(strip_tags($idsprodutos[$i])));

			if((isset($verifica_produto['id'])) && ($verifica_produto['id']!=$v_id)) $v_qtd = (int)$verifica_produto['quantidade'] + $v_qtd;
			if(isset($verifica_produto['id'])) $v_id = $verifica_produto['id'];
			
			$d = array();
			$d['id'] = $v_id;
			$d['idsaida'] = $idsaida; 
			$d['idproduto'] = trim(strip_tags($idsprodutos[$i])); 
			$d['idfornecedor'] = trim(strip_tags($idsfornecedores[$i]));
			$d['quantidade'] = $v_qtd;
			$d['observacoes'] = trim(strip_tags($itensobservacoes[$i]));
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $logusuario;
			$d['logdata'] = $logdata;
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
			//verificar as linhas comentadas acima, elas são responsaveis pelas 2 linhas mais cridas no cronogramasliehistorico
		}

		
		$itens = implode(",", $ids);
		
		if ($itens=="") $itens = "0";
		$strsql = "DELETE FROM almoxarifadosaidasitens WHERE idsaida=$idsaida AND id NOT IN ($itens)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);	
			
	}
	
	private function setItensEscolas($dados, $isaida, $logusuario = false, $logdata = false, $idescola = false, $iddepartamento = false, $saidas = array()) {
		if (!is_array($dados)) return;
		if ((int)$idescola <= 0) return;
		 
		$entradas = new Almoxarifado_Entradas();
		$vinculo_saida = $entradas->getEntradaByIdSaida($saidas['id']);
		$dados_entradas = array();
		if(isset($vinculo_saida['id'])) $dados_entradas['id'] = $vinculo_saida['id'];
		$dados_entradas['idsaida'] = $saidas['id'];
		$dados_entradas['titulo'] = $saidas['titulo'];
		$dados_entradas['descricoes'] = $saidas['descricoes'];
		$dados_entradas['aprovacao'] = $saidas['aprovacao'];
		$dados_entradas['confirmaaprovacao'] = 'sim';
		
		$dados_entradas['origem'] = Almoxarifado_Entradas::$_ORIGEM_ESCOLA;
		$dados_entradas['iddepartamentosecretaria'] = 0;
		$dados_entradas['idescola'] = $idescola;
		$dados_entradas['iddepartamentoescola'] = $iddepartamento;			
			
		$dados_entradas['status'] = $saidas['status'];
		$dados_entradas['excluido'] = 'nao';
		$dados_entradas['logusuario'] = $logusuario;
		$dados_entradas['logdata'] = $logdata;
		$r_entrada = $entradas->save($dados_entradas);
		
		
		$itens = new Almoxarifado_Entradasitens();
		$ids = $dados['idssaidas'];
		$idsprodutos = $dados['idsprodutos'];
		$idsfornecedores = $dados['idsfornecedores'];
		$quantidades = $dados['quantidades'];
		$itensstatus = $dados['itensstatus'];
		$itensobservacoes = $dados['itensobservacoes'];
		$identrada = $r_entrada['id'];
		
		
		$_saidas = new Almoxarifado_Saidasitens();
		$rows = $_saidas->getSaidasitens(array('idsaida'=>$isaida));
		
		foreach ($rows as $i=>$rowsaida) {
			$d = array();
			//var_dump($rowsaida); die();
			//$d['id'] = $v_id;
			$d['identrada'] = $identrada; 
			$d['idproduto'] = trim(strip_tags($rowsaida['idproduto'])); 
			$d['idfornecedor'] = trim(strip_tags($rowsaida['idfornecedor'])); 
			$d['quantidade'] = trim(strip_tags($rowsaida['quantidade'])); 
			$d['observacoes'] = trim(strip_tags($rowsaida['observacoes'])); 
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $logusuario;
			$d['logdata'] = $logdata;
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
		}
		/*
		foreach ($idsprodutos as $i=>$id) {
			$v_id = $ids[$i];
			$v_qtd = trim(strip_tags(Mn_Util::trataNum($quantidades[$i])));
			$verifica_produto = $itens->getEntradaitemByIdSaidaIdproduto($identrada, trim(strip_tags($idsprodutos[$i])));
			echo 'verifica_p_id = ' . $verifica_produto['id']. '<br />';
			echo '$v_id = ' .$v_id. '<br />';
			echo '$v_qtd = ' . $v_qtd. '<br />';
			echo '$verifica_produto_qtd = ' . (int)$verifica_produto['quantidade']. '<br />';
			echo '$quantidades = ' . (int)$quantidades[$i]. '<br />';
			if((isset($verifica_produto['id'])) && ($verifica_produto['id']!=$v_id)) $v_qtd = (int)$verifica_produto['quantidade'] + $v_qtd;
			if(isset($verifica_produto['id'])) $v_id = $verifica_produto['id'];
			
			$d = array();
			
			//$d['id'] = $v_id;
			$d['identrada'] = $identrada; 
			$d['idproduto'] = trim(strip_tags($idsprodutos[$i])); 
			$d['idfornecedor'] = trim(strip_tags($idsfornecedores[$i]));
			$d['quantidade'] = $v_qtd;
			$d['observacoes'] = trim(strip_tags($itensobservacoes[$i]));
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $logusuario;
			$d['logdata'] = $logdata;
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
			//verificar as linhas comentadas acima, elas são responsaveis pelas 2 linhas mais cridas no cronogramasliehistorico
		}
		*/
		
		$itens = implode(",", $ids);
		
		if ($itens=="") $itens = "0";
		$strsql = "DELETE FROM almoxarifadoentradasitens WHERE identrada=$identrada AND id NOT IN ($itens)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);	
				
			
	}
}