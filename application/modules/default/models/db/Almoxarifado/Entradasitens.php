<?php

class Almoxarifado_Entradasitens extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoentradasitens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getEntradasitensHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Almoxarifado_Entradasitens();
		return $produtos->getEntradasitens($queries, $page, $maxpage);
	}
	
	public function getEntradasitens($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$identrada = (isset($queries['identrada'])) ? (int)$queries['identrada'] : false;
		$idproduto = (isset($queries['idproduto'])) ? (int)$queries['idproduto'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;	
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;

		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$iddepartamentosecretaria = (isset($queries['iddepartamentosecretaria'])) ? $queries['iddepartamentosecretaria'] : false;		
		$iddepartamentoescola = (isset($queries['iddepartamentoescola'])) ? $queries['iddepartamentoescola'] : false;
		
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$somaentrada = (isset($queries['somaentrada'])) ? $queries['somaentrada'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " ei1.id=$id ");	
		if ($idsecretaria) array_push($where, " e1.idsecretaria=$idsecretaria ");
		if ($identrada) array_push($where, " ei1.identrada=$identrada ");	
		if ($idproduto) array_push($where, " ei1.idproduto=$idproduto ");
		if ($idescola) array_push($where, " e1.idescola=$idescola ");
		
		if ($origem) array_push($where, " e1.origem='$origem' ");
		if ($iddepartamentosecretaria) array_push($where, " e1.iddepartamentosecretaria=$iddepartamentosecretaria ");
		if ($iddepartamentoescola) array_push($where, " e1.iddepartamentoescola=$iddepartamentoescola ");
		
		if ($chave) array_push($where, " ((ei1.observacoes LIKE '%$chave%') OR (e1.titulo LIKE '%$chave%') OR (e1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " ei1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "ei1.*, e1.titulo,  e1.descricoes, f1.razaosocial, f1.nomefantasia, f1.cpfcnpj, p1.produto, p1.descricoes as p1descricoes, p1.codigoproduto";
		if ($total) $fields = "COUNT(ei1.id) as total";
		if ($somaentrada)  $fields = "SUM(ei1.quantidade) as total";
		
		
		$ordem = "ORDER BY ei1.id";
		if ($order) $ordem = $order; 
                
                $group_by = " GROUP BY p1.id ";
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadoentradasitens ei1
						LEFT JOIN almoxarifadoentradas e1 ON e1.id = ei1.identrada 
						LEFT JOIN fornecedores f1 ON f1.id = ei1.idfornecedor
						
						LEFT JOIN almoxarifadoprodutos p1 ON p1.id = ei1.idproduto
					WHERE ei1.excluido='nao'
						AND e1.excluido = 'nao'
						$w 
                        $group_by
					$ordem	
					$limit";
		
		//if($somaentrada) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}
		if ($somaentrada) {
			$row = $db->fetchRow($strsql);
			return $row['total'];		
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getEntradaitemById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEntradasitens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEntradaitemByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Entradasitens();
		return $rows->getEntradaitemById($id, $queries);
	}		

	public function getQuantidadeByIdProduto($idproduto, $queries = array()) {
		if ($idproduto==0) return false;
		$queries['somaentrada'] = true;
		$queries['idproduto'] = $idproduto;
		$rows = $this->getEntradasitens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows;
	}

	public static function getQuantidadeByIdProdutoHelper($idproduto, $queries = array()) {
		$rows = new Almoxarifado_Entradasitens();
		return $rows->getQuantidadeByIdProduto($idproduto, $queries);
	}
	
	public function getEntradaitemByIdSaidaIdproduto($idsaida, $idproduto, $queries = array()) {
		if ($idsaida==0) return false;
		if ($idproduto==0) return false;
		
		$queries['identrada'] = $idsaida;
		$queries['idproduto'] = $idproduto;
		$rows = $this->getEntradasitens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEntradaitemByIdSaidaIdprodutoHelper($id, $queries = array()) {
		$rows = new Almoxarifado_Entradasitens();
		return $rows->getSaidaitemByIdEntradaIdproduto($id, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Entradasitens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();	
			$row->datacriacao = date('Y-m-d G:i:s');	
		}else {
			$novoRegistro = false;
			
			$historico = new Almoxarifado_Entradasitenshistoricos();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);
		}
		$row->identrada = (array_key_exists('identrada',$dados)) ? $dados['identrada'] : $row->identrada;
		$row->idproduto = (array_key_exists('idproduto',$dados)) ? $dados['idproduto'] : $row->idproduto;
		$row->idfornecedor = (array_key_exists('idfornecedor',$dados)) ? $dados['idfornecedor'] : $row->idfornecedor;
		$row->idmarca = (array_key_exists('idmarca',$dados)) ? $dados['idmarca'] : $row->idmarca;
		
		$row->quantidade = (array_key_exists('quantidade',$dados)) ? $dados['quantidade'] : $row->quantidade;
		$row->observacoes = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		//if (array_key_exists('itens', $dados)) $this->setItens($dados['itens'], $row->id);
		return $row;
	}
	
	
}