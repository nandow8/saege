<?php

class Lterelatorios extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    // protected $_name = "lterelatorios";
    protected $_name = "view_lte_relatorio";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";     
   
    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");

        if (!$usuario) return "RGM ou senha inválidos!";
        if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
        if ($usuario['senha']!=$senha) return "A senha é inválida!";
        
        $usuario = $usuario->toArray();

        if (!$ns) $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);
        
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);
        
        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';
        
        $loginNameSpace->alunousuario = serialize($usuario);

        return true;        
    }

    public function getRelatorio($queries = array()) {

        $idperiodo = (isset($queries['idperiodo'])) ? (int)$queries['idperiodo'] : false;
        $tipoestudante = (isset($queries['tipoestudante'])) ? $queries['tipoestudante'] : false;
        $tipotransporte = (isset($queries['tipotransporte'])) ? $queries['tipotransporte'] : false;
        $identidade = (isset($queries['identidade'])) ? $queries['identidade'] : false;
        
        $where = array();

        if ($idperiodo) array_push($where, " idperiodo = $idperiodo ");
        if ($tipoestudante) array_push($where, " tipoestudante = '$tipoestudante' ");
        if ($tipotransporte) array_push($where, " tipotransporte = '$tipotransporte' ");
        if ($identidade) array_push($where, " identidade = $identidade ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "vlr.*";

        $strsql = "SELECT $fields FROM view_lte_relatorio vlr WHERE 1 $w ORDER BY vlr.entidade, vlr.nome";

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db');

        // var_dump($strsql); die();

        return $db->fetchAll($strsql);
    }

    public function getUniversitarios($queries = array()) {

        $idperiodo = (isset($queries['idperiodo'])) ? (int)$queries['idperiodo'] : false;
        $identidade = (isset($queries['identidade'])) ? $queries['identidade'] : false;
        $subsidio = (isset($queries['subsidio'])) ? $queries['subsidio'] : false;
        
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($idperiodo) array_push($where, " lu.idperiodo = $idperiodo ");
        if ($identidade) array_push($where, " le.identidade = $identidade ");
        if ($subsidio) array_push($where, " vch.valorfretado * vch.subsidio / 100 = $subsidio ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "le.nome, le.filiacao, le2.entidade, le.cpf, vch.cidade, vch.valorfretado, vch.valorfretado * vch.subsidio / 100 subsidio";

        $ordem = "ORDER BY le.nome";

        if ($order) $ordem = $order; 

        $strsql = "SELECT $fields FROM lteperiodosuniversitarios lpu INNER JOIN lteuniversitarios lu ON (lpu.id = lu.idperiodo) INNER JOIN lteestudantes le ON (lu.idestudante = le.id) LEFT JOIN lteentidades le2 ON (le.identidade = le2.id) LEFT JOIN view_cidade_historico vch ON (le2.idcidade = vch.idcidade) WHERE lu.excluido = 'nao' AND lu.status = 'Ativo' AND le.tipoestudante = 'universidade' $w $ordem";

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db');

        // var_dump($strsql); die();

        return $db->fetchAll($strsql);
    }

}