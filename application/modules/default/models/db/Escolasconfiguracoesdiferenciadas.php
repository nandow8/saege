<?php

/**
 * Define o modelo Escolasconfiguracoesdiferenciadas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasconfiguracoesdiferenciadas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasconfiguracoesdiferenciadas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolasconfiguracoesdiferenciadasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasconfiguracoesdiferenciadas = new Escolasconfiguracoesdiferenciadas();
		return $escolasconfiguracoesdiferenciadas->getEscolasconfiguracoesdiferenciadas($queries, $page, $maxpage);
	}
	
	public function getEscolasconfiguracoesdiferenciadas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$group_by = (isset($queries['group_by'])) ? $queries['group_by'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");

		$idserie = (isset($queries["idserie"])) ? $queries["idserie"] : false;
				if ($idserie) array_push($where, " FIND_IN_SET($idserie, e1.idsseries)");

		$idmateria = (isset($queries["idmateria"])) ? $queries["idmateria"] : false;
				if ($idmateria) array_push($where, " e1.idmateria = $idmateria ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*, e2.percentualnota, e3.titulo"; 
		
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$group = " ";
		if ($group_by) $group = $group_by; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasconfiguracoesdiferenciadas e1
					LEFT JOIN escolasconfiguracoesdiferenciadasnotas e2 ON e2.idparent = e1.id
					LEFT JOIN escolastitulosnotas e3 ON e3.id = e2.idtitulonota
					
					WHERE e1.excluido='nao' 
						$w 
					$group
					$ordem	
					$limit";	
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolaconfiguracaodiferenciadaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasconfiguracoesdiferenciadas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaconfiguracaodiferenciadaByIdHelper($id, $queries = array()) {
		$rows = new Escolasconfiguracoesdiferenciadas();
		return $rows->getEscolaconfiguracaodiferenciadaById($id, $queries);
	}	
	
	public function getEscolaconfiguracaodiferenciadaByIdEscola($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idescola'] = $id;
		$rows = $this->getEscolasconfiguracoesdiferenciadas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaconfiguracaodiferenciadaByIdEscolaHelper($id, $queries = array()) {
		$rows = new Escolasconfiguracoesdiferenciadas();
		return $rows->getEscolaconfiguracaodiferenciadaByIdEscola($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasconfiguracoesdiferenciadas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->idsseries = (array_key_exists("idsseries",$dados)) ? $dados["idsseries"] : $row->idsseries;
		$row->idmateria = (array_key_exists("idmateria",$dados)) ? $dados["idmateria"] : $row->idmateria;
		$row->idstitulos = (array_key_exists("idstitulos",$dados)) ? $dados["idstitulos"] : $row->idstitulos;
		$row->percentualnotas = (array_key_exists("percentualnotas",$dados)) ? $dados["percentualnotas"] : $row->percentualnotas;
		
		if (is_null($row->datacriacao)) {
		$row->datacriacao = date("Y-m-d H:i:s");
		}
					
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
	
		//return true;		
		$row->save();

		if((array_key_exists("notas",$dados))) $this->setNotasConfiguracoesDiferenciadas($dados['notas'], $row->id);
		
		return $row;
	}

	private function setNotasConfiguracoesDiferenciadas($dados, $idparent)
	{
		if(!$idparent || !isset($dados)) return false;

		$idstitulos = $dados['idstitulos'];
		$percentualnotas = $dados['percentualnotas'];

		$configuracoesnotas = new Escolasconfiguracoesdiferenciadasnotas();

		$ids = array();
		foreach ($idstitulos as $i => $id) {

			$_nota = MN_Util::trataNum(trim($percentualnotas[$i]));

			$d = array();			
			$d["idparent"] = $idparent;
			$d["idtitulonota"] = $id;
			$d["percentualnota"] = $_nota;
			
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $dados['logusuario'];
			$d['logdata'] = date('Y-m-d G:i:s');
			$row = $configuracoesnotas->save($d);

			array_push($ids, $row->id);
		}

		$itens = implode(",", $ids);
		
		if ($itens=="") $itens = "0";
		$strsql = "DELETE FROM escolasconfiguracoesdiferenciadasnotas WHERE idparent=$idparent AND id NOT IN ($itens)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);
	}
	
}