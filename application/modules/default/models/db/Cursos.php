<?php

/**
 * Define o modelo Cursos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Cursos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "cursos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCursosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$cursos = new Cursos();
		return $cursos->getCursos($queries, $page, $maxpage);
	}
	
	public function getCursos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " c1.idsecretaria = $idsecretaria ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " c1.titulo LIKE '%$titulo%' ");

$cargahoraria = (isset($queries["cargahoraria"])) ? $queries["cargahoraria"] : false;
		if ($cargahoraria) array_push($where, " c1.cargahoraria LIKE '%$cargahoraria%' ");

$pontuacao = (isset($queries["pontuacao"])) ? $queries["pontuacao"] : false;
		if ($pontuacao) array_push($where, " c1.pontuacao LIKE '%$pontuacao%' ");

$instituicaoensino = (isset($queries["instituicaoensino"])) ? $queries["instituicaoensino"] : false;
		if ($instituicaoensino) array_push($where, " c1.instituicaoensino = '$instituicaoensino' ");

$periodo = (isset($queries["periodo"])) ? $queries["periodo"] : false;
		if ($periodo) array_push($where, " c1.periodo = '$periodo' ");

$instrutor = (isset($queries["instrutor"])) ? $queries["instrutor"] : false;
		if ($instrutor) array_push($where, " c1.instrutor = '$instrutor' ");

		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " c1.descricoes = '$descricoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");

		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " c1.origem LIKE '%$origem%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM cursos c1
					
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getCursoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCursos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCursoByIdHelper($id, $queries = array()) {
		$rows = new Cursos();
		return $rows->getCursoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Cursos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
		 $row->idendereco = (array_key_exists("idendereco",$dados)) ? $dados["idendereco"] : $row->idendereco;
		 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
		 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
		 $row->cargahoraria = (array_key_exists("cargahoraria",$dados)) ? $dados["cargahoraria"] : $row->cargahoraria;
		 $row->pontuacao = (array_key_exists("pontuacao",$dados)) ? $dados["pontuacao"] : $row->pontuacao;
		 $row->instituicaoensino = (array_key_exists("instituicaoensino",$dados)) ? $dados["instituicaoensino"] : $row->instituicaoensino;
		 $row->periodo = (array_key_exists("periodo",$dados)) ? $dados["periodo"] : $row->periodo;
		 $row->instrutor = (array_key_exists("instrutor",$dados)) ? $dados["instrutor"] : $row->instrutor;
		 $row->quantidadevagas = (array_key_exists("quantidadevagas",$dados)) ? $dados["quantidadevagas"] : $row->quantidadevagas;
		 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
		 if (is_null($row->datacriacao)) {
					$row->datacriacao = date("Y-m-d H:i:s");
				}
								
		 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}