<?php

/**
 * Define o modelo Supervisaotipospontuacoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Supervisaotipospontuacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "supervisaotipospontuacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getSupervisaotipospontuacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$supervisoestipospontuacoes = new Supervisaotipospontuacoes();
		return $supervisoestipospontuacoes->getSupervisaotipospontuacoes($queries, $page, $maxpage);
	}
	
	public function getSupervisaotipospontuacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " s1.id = $id ");
		
		
		$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " s1.titulo LIKE '%$titulo%' ");

		$descricao = (isset($queries["descricao"])) ? $queries["descricao"] : false;
		if ($descricao) array_push($where, " s1.descricao	LIKE '%$descricao%' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " s1.status LIKE '%$status%' ");	
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "s1.*"; 
		;
		
		if ($total) $fields = "COUNT(s1.id) as total";
		
		$ordem = "ORDER BY s1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM supervisaotipospontuacoes s1
					
					WHERE s1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		
		$db = Zend_Registry::get('db');				
		
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getSupervisaotipopontuacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSupervisaotipospontuacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSupervisaotipopontuacaoByIdHelper($id, $queries = array()) {
		$rows = new Supervisaotipospontuacoes();
		return $rows->getSupervisaotipopontuacaoById($id, $queries);
	}		
		
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Supervisaotipospontuacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
		$row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}

		$row->save();
		
		return $row;
	}
	
}