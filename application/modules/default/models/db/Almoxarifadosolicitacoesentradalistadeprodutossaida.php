<?php

/**
 * Define o modelo Almoxarifadosolicitacoesentradalistadeprodutossaida
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 FJMX.
 * @version     1.0
 */
class Almoxarifadosolicitacoesentradalistadeprodutossaida extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadosolicitacoesentradalistadeprodutossaida";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAlmoxarifadosolicitacoesentradalistadeprodutossaidaHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$almoxarifadosolicitacoesentradalistadeprodutossaida = new Almoxarifadosolicitacoesentradalistadeprodutossaida();
		return $almoxarifadosolicitacoesentradalistadeprodutossaida->getAlmoxarifadosolicitacoesentradalistadeprodutossaida($queries, $page, $maxpage);
	}
	
	public function getAlmoxarifadosolicitacoesentradalistadeprodutossaida($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " alep.id = $id ");
		
		$idusuario = (isset($queries['idusuario'])) ? (int)$queries['idusuario'] : false;
		if ($idusuario) array_push($where, " alep.idusuario = $idusuario ");
		
		$codigosolicitacao = (isset($queries["codigosolicitacao"])) ? $queries["codigosolicitacao"] : false;
		if ($codigosolicitacao) array_push($where, " alep.codigosolicitacao = $codigosolicitacao ");
		
		$produto = (isset($queries["produto"])) ? $queries["produto"] : false;
		if ($produto) array_push($where, " alep.produto LIKE '%$produto%' ");
		
		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " alep.descricoes LIKE '%$descricoes%' ");
		
		$codigoproduto = (isset($queries["codigoproduto"])) ? $queries["codigoproduto"] : false;
		if ($codigoproduto) array_push($where, " alep.codigoproduto LIKE '%$codigoproduto%' ");
		
		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " alep.status LIKE '%$status%' ");
		
		$idcategoria = (isset($queries["idcategoria"])) ? $queries["idcategoria"] : false;
		if ($idcategoria) array_push($where, " alep.idcategoria = '$idcategoria' ");
		
		$idunidademedida = (isset($queries["idunidademedida"])) ? $queries["idunidademedida"] : false;
		if ($idunidademedida) array_push($where, " alep.idunidademedida = '$idunidademedida' ");
		
		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "($w)";
		
		$fields = "alep.*"; 
		;
		
		if ($total) $fields = "COUNT(alep.id) as total";
		
		$ordem = "ORDER BY alep.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
		
		$strsql = "SELECT $fields 
		FROM almoxarifadosolicitacoesentradalistadeprodutossaida alep

		WHERE 
		$w 
		$ordem	
		$limit";	
		// die($strsql );
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	 
	 
		return $db->fetchAll($strsql);			
	}	
	
	public function getAlmoxarifadosolicitacoesentradalistadeprodutossaidaById($idusuario, $queries = array()) {
		
		if ($idusuario==0) return false; 
		$queries['idusuario'] = $idusuario;
		$rows = $this->getAlmoxarifadosolicitacoesentradalistadeprodutossaida($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAlmoxarifadosolicitacoesentradalistadeprodutossaidaByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifadoprodutos();
		return $rows->getAlmoxarifadosolicitacoesentradalistadeprodutossaidaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadosolicitacoesentradalistadeprodutossaida
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
		$row->idperfil = (array_key_exists("idperfil",$dados)) ? $dados["idperfil"] : $row->idperfil;
		$row->codigoproduto = (array_key_exists("codigoproduto",$dados)) ? $dados["codigoproduto"] : $row->codigoproduto;
		$row->codigosolicitacao = (array_key_exists("codigosolicitacao",$dados)) ? $dados["codigosolicitacao"] : $row->codigosolicitacao;
		$row->qtd_selecionada = (array_key_exists("qtd_selecionada",$dados)) ? $dados["qtd_selecionada"] : $row->qtd_selecionada;
		$row->produto = (array_key_exists("produto",$dados)) ? $dados["produto"] : $row->produto;
		$row->unidademedida = (array_key_exists("unidademedida",$dados)) ? $dados["unidademedida"] : $row->unidademedida;
		 
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}  

		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
	
		$row->save();
		
		return $row;
	}
	
}