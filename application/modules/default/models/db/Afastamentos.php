<?php

/**
 * Define o modelo Afastamentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Afastamentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "afastamentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAfastamentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$afastamentos = new Afastamentos();
		return $afastamentos->getAfastamentos($queries, $page, $maxpage);
	}
	
	public function getAfastamentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " f1.idsecretaria = $idsecretaria ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " f1.origem LIKE '%$origem%' ");

$idfuncionariosecretaria = (isset($queries["idfuncionariosecretaria"])) ? $queries["idfuncionariosecretaria"] : false;
		if ($idfuncionariosecretaria) array_push($where, " f1.idfuncionariosecretaria = $idfuncionariosecretaria ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " f1.idescola = $idescola ");

$idfuncionarioescola = (isset($queries["idfuncionarioescola"])) ? $queries["idfuncionarioescola"] : false;
		if ($idfuncionarioescola) array_push($where, " f1.idfuncionarioescola = $idfuncionarioescola ");

$dias = (isset($queries["dias"])) ? $queries["dias"] : false;
		if ($dias) array_push($where, " f1.dias = '$dias' ");

$datainicio_i = (isset($queries["datainicio_i"])) ? $queries["datainicio_i"] : false;
		if ($datainicio_i) array_push($where, " f1.datainicio >= '$datainicio_i' ");

$datainicio_f = (isset($queries["datainicio_f"])) ? $queries["datainicio_f"] : false;
		if ($datainicio_f) array_push($where, " f1.datainicio <= '$datainicio_f' ");

$datafim_i = (isset($queries["datafim_i"])) ? $queries["datafim_i"] : false;
		if ($datafim_i) array_push($where, " f1.datafim >= '$datafim_i' ");

$datafim_f = (isset($queries["datafim_f"])) ? $queries["datafim_f"] : false;
		if ($datafim_f) array_push($where, " f1.datafim <= '$datafim_f' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " f1.descricoes = '$descricoes' ");

$aprovacao = (isset($queries["aprovacao"])) ? $queries["aprovacao"] : false;
		if ($aprovacao) array_push($where, " f1.aprovacao LIKE '%$aprovacao%' ");

$confirmacaoaprovacao = (isset($queries["confirmacaoaprovacao"])) ? $queries["confirmacaoaprovacao"] : false;
		if ($confirmacaoaprovacao) array_push($where, " f1.confirmacaoaprovacao LIKE '%$confirmacaoaprovacao%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM afastamentos f1
					
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFeriaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAfastamentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFeriaByIdHelper($id, $queries = array()) {
		$rows = new Afastamentos();
		return $rows->getFeriaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Afastamentos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->idfuncionariosecretaria = (array_key_exists("idfuncionariosecretaria",$dados)) ? $dados["idfuncionariosecretaria"] : $row->idfuncionariosecretaria;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idfuncionarioescola = (array_key_exists("idfuncionarioescola",$dados)) ? $dados["idfuncionarioescola"] : $row->idfuncionarioescola;
 $row->dias = (array_key_exists("dias",$dados)) ? $dados["dias"] : $row->dias;
 $row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
 $row->datafim = (array_key_exists("datafim",$dados)) ? $dados["datafim"] : $row->datafim;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->aprovacao = (array_key_exists("aprovacao",$dados)) ? $dados["aprovacao"] : $row->aprovacao;
 $row->confirmacaoaprovacao = (array_key_exists("confirmacaoaprovacao",$dados)) ? $dados["confirmacaoaprovacao"] : $row->confirmacaoaprovacao;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}