<?php

/**
 * Define o modelo Escolasusuarioslimites
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasusuarioslimites extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasusuarioslimites";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";		
	
	/**
	 * 
	 * Retorna o array com a lista de limites registrados
	 */
	public function getLimites() {
		$select = $this->select()->order("id");
		return $this->fetchAll($select);
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasusuarioslimites
     */
	public function save($dados) {
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id");
		
		if (!$row) $row = $this->createRow();

				
		if ($id>0)$row->id = $id;
		$row->limite = $dados['limite'];
		$row->modulo = $dados['modulo'];
		$row->alias = $dados['alias'];
		$row->save();
	}	
	
}