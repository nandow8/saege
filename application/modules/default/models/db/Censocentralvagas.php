<?php

/**
 * Define o modelo Censocentralvagas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Censocentralvagas extends Zend_Db_Table_Abstract {
	
    /**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "censocentralvagas";
	
    /**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCensocentralvagasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$censocentralvagas = new Censocentralvagas();
		return $censocentralvagas->getCensocentralvagas($queries, $page, $maxpage);
	}
	
	public function getCensocentralvagas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " c1.idusuario = $idusuario ");

                $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " c1.idescola = $idescola ");

                $idserie = (isset($queries["idserie"])) ? $queries["idserie"] : false;
		if ($idserie) array_push($where, " c1.idserie = $idserie ");

                $idturma = (isset($queries["idturma"])) ? $queries["idturma"] : false;
		if ($idturma) array_push($where, " c1.idturma = $idturma ");

                $idsala = (isset($queries["idsala"])) ? $queries["idsala"] : false;
		if ($idsala) array_push($where, " c1.idsala = $idsala ");

                $idperiodo = (isset($queries["idperiodo"])) ? $queries["idperiodo"] : false;
		if ($idperiodo) array_push($where, " c1.idperiodo = $idperiodo ");

                $idensino = (isset($queries["idensino"])) ? $queries["idensino"] : false;
		if ($idensino) array_push($where, " c1.idensino = $idensino ");

                $totalvagas = (isset($queries["totalvagas"])) ? $queries["totalvagas"] : false;
		if ($totalvagas) array_push($where, " c1.totalvagas = '$totalvagas' ");

                $status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");
                
                $turno = (isset($queries["turno"])) ? $queries["turno"] : false;
		if ($turno) array_push($where, " c1.turno LIKE '%$turno%' ");

		if ($sorting) { 
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM censocentralvagas c1
					
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getCensocentralvagaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCensocentralvagas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCensocentralvagaByIdHelper($id, $queries = array()) {
		$rows = new Censocentralvagas();
		return $rows->getCensocentralvagaById($id, $queries);
	}		
	
	
    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Censocentralvagas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
                $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
                $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
                $row->idserie = (array_key_exists("idserie",$dados)) ? $dados["idserie"] : $row->idserie;
                $row->idturma = (array_key_exists("idturma",$dados)) ? $dados["idturma"] : $row->idturma;
                $row->idsala = (array_key_exists("idsala",$dados)) ? $dados["idsala"] : $row->idsala;
                $row->idperiodo = (array_key_exists("idperiodo",$dados)) ? $dados["idperiodo"] : $row->idperiodo;
                $row->idensino = (array_key_exists("idensino",$dados)) ? $dados["idensino"] : $row->idensino;
                $row->totalvagas = (array_key_exists("totalvagas",$dados)) ? $dados["totalvagas"] : $row->totalvagas;
                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
	public function getCensocentralvagasresumo($idescola) { 
		
		$strsql = "select idescola, sum(totalvagas) as total from censocentralvagas where idescola = '$idescola' group by idescola order by idescola desc";

		//$db = Zend_Registry::get('db');	

		//$row =  $db->fetchRow($strsql);	
		
		//return
		
		return $idescola;
	}

}