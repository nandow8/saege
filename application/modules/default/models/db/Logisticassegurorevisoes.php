<?php

/**
 * Define o modelo Logisticassegurorevisoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Logisticassegurorevisoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "logisticassegurorevisoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLogisticassegurorevisoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$logisticassegurorevisoes = new Logisticassegurorevisoes();
		return $logisticassegurorevisoes->getLogisticassegurorevisoes($queries, $page, $maxpage);
	}
	
	public function getLogisticassegurorevisoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		
		$idveiculo = (isset($queries["idveiculo"])) ? $queries["idveiculo"] : false;
		if ($idveiculo) array_push($where, " l1.idveiculo = $idveiculo ");

$seguro = (isset($queries["seguro"])) ? $queries["seguro"] : false;
		if ($seguro) array_push($where, " l1.seguro LIKE '%$seguro%' ");

$seguradora = (isset($queries["seguradora"])) ? $queries["seguradora"] : false;
		if ($seguradora) array_push($where, " l1.seguradora LIKE '%$seguradora%' ");

$meslicenciamento_vencimento = (isset($queries["meslicenciamento_vencimento"])) ? $queries["meslicenciamento_vencimento"] : false;
		if ($meslicenciamento_vencimento) array_push($where, " l1.meslicenciamento_vencimento LIKE '%$meslicenciamento_vencimento%' ");

$numero_apolice = (isset($queries["numero_apolice"])) ? $queries["numero_apolice"] : false;
		if ($numero_apolice) array_push($where, " l1.numero_apolice LIKE '%$numero_apolice%' ");

$revisao = (isset($queries["revisao"])) ? $queries["revisao"] : false;
		if ($revisao) array_push($where, " l1.revisao LIKE '%$revisao%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*"; 
		;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM logisticassegurorevisoes l1
					
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLogisticassegurorevisaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLogisticassegurorevisoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLogisticassegurorevisaoByIdHelper($id, $queries = array()) {
		$rows = new Logisticassegurorevisoes();
		return $rows->getLogisticassegurorevisaoById($id, $queries);
	}		
	

	public function getUltimoLogisticasegurorevisao($queries = array()) {
		$queries['order'] = 'ORDER BY l1.id DESC';
		$rows = $this->getLogisticassegurorevisoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Logisticassegurorevisoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idveiculo = (array_key_exists("idveiculo",$dados)) ? $dados["idveiculo"] : $row->idveiculo;
 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->meslicenciamento_vencimento = (array_key_exists("meslicenciamento_vencimento",$dados)) ? $dados["meslicenciamento_vencimento"] : $row->meslicenciamento_vencimento;
 $row->diasaviso_vencimento = (array_key_exists("diasaviso_vencimento",$dados)) ? $dados["diasaviso_vencimento"] : $row->diasaviso_vencimento;
 $row->seguro = (array_key_exists("seguro",$dados)) ? $dados["seguro"] : $row->seguro;
 $row->seguradora = (array_key_exists("seguradora",$dados)) ? $dados["seguradora"] : $row->seguradora;
 $row->numero_apolice = (array_key_exists("numero_apolice",$dados)) ? $dados["numero_apolice"] : $row->numero_apolice;
 $row->corretora = (array_key_exists("corretora",$dados)) ? $dados["corretora"] : $row->corretora;
 $row->telefone_seguradora = (array_key_exists("telefone_seguradora",$dados)) ? $dados["telefone_seguradora"] : $row->telefone_seguradora;
 $row->periodo_vigenciainicial = (array_key_exists("periodo_vigenciainicial",$dados)) ? $dados["periodo_vigenciainicial"] : $row->periodo_vigenciainicial;
 $row->periodo_vigenciafinal = (array_key_exists("periodo_vigenciafinal",$dados)) ? $dados["periodo_vigenciafinal"] : $row->periodo_vigenciafinal;
 $row->diasaviso_vencimentoseguro = (array_key_exists("diasaviso_vencimentoseguro",$dados)) ? $dados["diasaviso_vencimentoseguro"] : $row->diasaviso_vencimentoseguro;
 $row->revisao = (array_key_exists("revisao",$dados)) ? $dados["revisao"] : $row->revisao;
 $row->concessionaria = (array_key_exists("concessionaria",$dados)) ? $dados["concessionaria"] : $row->concessionaria;
 $row->telefone_concessionaria = (array_key_exists("telefone_concessionaria",$dados)) ? $dados["telefone_concessionaria"] : $row->telefone_concessionaria;
 $row->km_atual = (array_key_exists("km_atual",$dados)) ? $dados["km_atual"] : $row->km_atual;
 $row->revisao1 = (array_key_exists("revisao1",$dados)) ? $dados["revisao1"] : $row->revisao1;
 $row->avisar_vencimentorevisao1 = (array_key_exists("avisar_vencimentorevisao1",$dados)) ? $dados["avisar_vencimentorevisao1"] : $row->avisar_vencimentorevisao1;
 $row->revisao2 = (array_key_exists("revisao2",$dados)) ? $dados["revisao2"] : $row->revisao2;
 $row->avisar_vencimentorevisao2 = (array_key_exists("avisar_vencimentorevisao2",$dados)) ? $dados["avisar_vencimentorevisao2"] : $row->avisar_vencimentorevisao2;
 $row->revisao3 = (array_key_exists("revisao3",$dados)) ? $dados["revisao3"] : $row->revisao3;
 $row->avisar_vencimentorevisao3 = (array_key_exists("avisar_vencimentorevisao3",$dados)) ? $dados["avisar_vencimentorevisao3"] : $row->avisar_vencimentorevisao3;
 $row->revisao4 = (array_key_exists("revisao4",$dados)) ? $dados["revisao4"] : $row->revisao4;
 $row->avisar_vencimentorevisao4 = (array_key_exists("avisar_vencimentorevisao4",$dados)) ? $dados["avisar_vencimentorevisao4"] : $row->avisar_vencimentorevisao4;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}