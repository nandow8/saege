<?php

/**
 * Define o modelo Dadospessoais
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Dadospessoais extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "dadospessoais";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDadospessoaisHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$dadospessoais = new Dadospessoais();
		return $dadospessoais->getDadospessoais($queries, $page, $maxpage);
	}
	
	public function getDadospessoais($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		if ($origem) array_push($where, " d1.origem = '$origem' ");

		$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " d1.idfuncionario = $idfuncionario ");

		$idfuncionarioescola = (isset($queries["idfuncionarioescola"])) ? $queries["idfuncionarioescola"] : false;
		if ($idfuncionarioescola) array_push($where, " d1.idfuncionarioescola = $idfuncionarioescola ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " d1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		
		$ordem = "ORDER BY d1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM dadospessoais d1
					
					WHERE d1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDadopessoalById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDadospessoais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDadopessoalByIdHelper($id, $queries = array()) {
		$rows = new Dadospessoais();
		return $rows->getDadopessoalById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Dadospessoais
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
		 $row->idfuncionarioescola = (array_key_exists("idfuncionarioescola",$dados)) ? $dados["idfuncionarioescola"] : $row->idfuncionarioescola;
		 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->idfoto3x4 = (array_key_exists("idfoto3x4",$dados)) ? $dados["idfoto3x4"] : $row->idfoto3x4;
 $row->idfoto2x2 = (array_key_exists("idfoto2x2",$dados)) ? $dados["idfoto2x2"] : $row->idfoto2x2;
 $row->idrg = (array_key_exists("idrg",$dados)) ? $dados["idrg"] : $row->idrg;
 $row->idcpf = (array_key_exists("idcpf",$dados)) ? $dados["idcpf"] : $row->idcpf;
 $row->idtituloeleitor = (array_key_exists("idtituloeleitor",$dados)) ? $dados["idtituloeleitor"] : $row->idtituloeleitor;
 $row->idreservista = (array_key_exists("idreservista",$dados)) ? $dados["idreservista"] : $row->idreservista;
 $row->idpispaseb = (array_key_exists("idpispaseb",$dados)) ? $dados["idpispaseb"] : $row->idpispaseb;
 $row->idcomprovanteeleicao = (array_key_exists("idcomprovanteeleicao",$dados)) ? $dados["idcomprovanteeleicao"] : $row->idcomprovanteeleicao;
 $row->idcomprovanteendereco = (array_key_exists("idcomprovanteendereco",$dados)) ? $dados["idcomprovanteendereco"] : $row->idcomprovanteendereco;
 $row->idcertidaonascimento = (array_key_exists("idcertidaonascimento",$dados)) ? $dados["idcertidaonascimento"] : $row->idcertidaonascimento;
 $row->idcertidaocasamento = (array_key_exists("idcertidaocasamento",$dados)) ? $dados["idcertidaocasamento"] : $row->idcertidaocasamento;
 $row->idantecedentes = (array_key_exists("idantecedentes",$dados)) ? $dados["idantecedentes"] : $row->idantecedentes;
 $row->iddgraduacao = (array_key_exists("iddgraduacao",$dados)) ? $dados["iddgraduacao"] : $row->iddgraduacao;
 $row->idcargo = (array_key_exists("idcargo",$dados)) ? $dados["idcargo"] : $row->idcargo;
 $row->idexame = (array_key_exists("idexame",$dados)) ? $dados["idexame"] : $row->idexame;
 $row->idcnh = (array_key_exists("idcnh",$dados)) ? $dados["idcnh"] : $row->idcnh;
 $row->idconta = (array_key_exists("idconta",$dados)) ? $dados["idconta"] : $row->idconta;
 $row->iddeclaracaobens = (array_key_exists("iddeclaracaobens",$dados)) ? $dados["iddeclaracaobens"] : $row->iddeclaracaobens;
 $row->iddeclaracaobensmiposto = (array_key_exists("iddeclaracaobensmiposto",$dados)) ? $dados["iddeclaracaobensmiposto"] : $row->iddeclaracaobensmiposto;
 $row->idacumulo = (array_key_exists("idacumulo",$dados)) ? $dados["idacumulo"] : $row->idacumulo;
 $row->iddaposentadoria = (array_key_exists("iddaposentadoria",$dados)) ? $dados["iddaposentadoria"] : $row->iddaposentadoria;
 $row->idfamiliar = (array_key_exists("idfamiliar",$dados)) ? $dados["idfamiliar"] : $row->idfamiliar;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}