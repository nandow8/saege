<?php

/**
 * Define o modelo Mensagens
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Mensagens extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "mensagens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getMensagensHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$mensagens = new Mensagens();
		return $mensagens->getMensagens($queries, $page, $maxpage);
	}
	
	public function getMensagens($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " m1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " m1.idescola = $idescola ");

$destinatario = (isset($queries["destinatario"])) ? $queries["destinatario"] : false;
		if ($destinatario) array_push($where, " m1.destinatario LIKE '%$destinatario%' ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " m1.origem LIKE '%$origem%' ");

$idescolausuario = (isset($queries["idescolausuario"])) ? $queries["idescolausuario"] : false;
		if ($idescolausuario) array_push($where, " m1.idescolausuario LIKE '%$idescolausuario%' ");

$idaluno = (isset($queries["idaluno"])) ? $queries["idaluno"] : false;
		if ($idaluno) array_push($where, " m1.idaluno = $idaluno ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " m1.titulo LIKE '%$titulo%' ");

$mensagem = (isset($queries["mensagem"])) ? $queries["mensagem"] : false;
		if ($mensagem) array_push($where, " m1.mensagem = '$mensagem' ");

$lido = (isset($queries["lido"])) ? $queries["lido"] : false;
		if ($lido) array_push($where, " m1.lido LIKE '%$lido%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " m1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "m1.*"; 
		;
		
		if ($total) $fields = "COUNT(m1.id) as total";
		
		$ordem = "ORDER BY m1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM mensagens m1
					
					WHERE m1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getMensagemById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getMensagens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getMensagemByIdHelper($id, $queries = array()) {
		$rows = new Mensagens();
		return $rows->getMensagemById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Mensagens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->destinatario = (array_key_exists("destinatario",$dados)) ? $dados["destinatario"] : $row->destinatario;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->idescolausuario = (array_key_exists("idescolausuario",$dados)) ? $dados["idescolausuario"] : $row->idescolausuario;
 $row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->mensagem = (array_key_exists("mensagem",$dados)) ? $dados["mensagem"] : $row->mensagem;
 $row->lido = (array_key_exists("lido",$dados)) ? $dados["lido"] : $row->lido;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}