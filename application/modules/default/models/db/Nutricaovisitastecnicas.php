<?php

/**
 * Define o modelo Nutricaovisitastecnicas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */

class Nutricaovisitastecnicas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "nutricaovisitastecnicas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getNutricaovisitastecnicasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaovisitastecnicas = new Nutricaovisitastecnicas();
		return $nutricaovisitastecnicas->getNutricaovisitastecnicas($queries, $page, $maxpage);
	}
	
	public function getNutricaovisitastecnicas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " n1.id = $id ");
		
		
		$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " n1.datalancamento >= '$datalancamento_i' ");

		$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
				if ($datalancamento_f) array_push($where, " n1.datalancamento <= '$datalancamento_f' ");

		$horavisita = (isset($queries["horavisita"])) ? $queries["horavisita"] : false;
		if ($horavisita) array_push($where, " n1.horavisita = '$horavisita' ");

		$queries['idescola'] = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper(Usuarios::getUsuario('idfuncionario'))['idescola']; 
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " n1.idescola = $idescola ");
		 
		$idperfil = (isset($queries["idperfil"])) ? $queries["idperfil"] : false;
		if ($idperfil) array_push($where, " n1.idperfil = $idperfil ");

		$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " n1.idfuncionario = $idfuncionario ");

		$funcao = (isset($queries["funcao"])) ? $queries["funcao"] : false;
		if ($funcao) array_push($where, " n1.funcao LIKE '%$funcao%' ");

		$idusuariovisita = (isset($queries["idusuariovisita"])) ? $queries["idusuariovisita"] : false;
		if ($idusuariovisita) array_push($where, " n1.idusuariovisita = $idusuariovisita ");

		$equipeproducaocompleta = (isset($queries["equipeproducaocompleta"])) ? $queries["equipeproducaocompleta"] : false;
		if ($equipeproducaocompleta) array_push($where, " n1.equipeproducaocompleta LIKE '%$equipeproducaocompleta%' ");

		$producoesdescricoes = (isset($queries["producoesdescricoes"])) ? $queries["producoesdescricoes"] : false;
		if ($producoesdescricoes) array_push($where, " n1.producoesdescricoes = '$producoesdescricoes' ");

		$producoesobservacoes = (isset($queries["producoesobservacoes"])) ? $queries["producoesobservacoes"] : false;
		if ($producoesobservacoes) array_push($where, " n1.producoesobservacoes = '$producoesobservacoes' ");

		$producoesorganizacoeshigienes = (isset($queries["producoesorganizacoeshigienes"])) ? $queries["producoesorganizacoeshigienes"] : false;
		if ($producoesorganizacoeshigienes) array_push($where, " n1.producoesorganizacoeshigienes LIKE '%$producoesorganizacoeshigienes%' ");

		$estoqueorganizacoeshigienes = (isset($queries["estoqueorganizacoeshigienes"])) ? $queries["estoqueorganizacoeshigienes"] : false;
		if ($estoqueorganizacoeshigienes) array_push($where, " n1.estoqueorganizacoeshigienes LIKE '%$estoqueorganizacoeshigienes%' ");

		$cardapioprogramado = (isset($queries["cardapioprogramado"])) ? $queries["cardapioprogramado"] : false;
		if ($cardapioprogramado) array_push($where, " n1.cardapioprogramado LIKE '%$cardapioprogramado%' ");

		$cardapioservido = (isset($queries["cardapioservido"])) ? $queries["cardapioservido"] : false;
		if ($cardapioservido) array_push($where, " n1.cardapioservido = '$cardapioservido' ");

		$cardapioobservacao = (isset($queries["cardapioobservacao"])) ? $queries["cardapioobservacao"] : false;
		if ($cardapioobservacao) array_push($where, " n1.cardapioobservacao = '$cardapioobservacao' ");

		$fluxograma = (isset($queries["fluxograma"])) ? $queries["fluxograma"] : false;
		if ($fluxograma) array_push($where, " n1.fluxograma = '$fluxograma' ");

		$equipamentosfuncionamentos = (isset($queries["equipamentosfuncionamentos"])) ? $queries["equipamentosfuncionamentos"] : false;
		if ($equipamentosfuncionamentos) array_push($where, " n1.equipamentosfuncionamentos = '$equipamentosfuncionamentos' ");

		$equipamentosdesuso = (isset($queries["equipamentosdesuso"])) ? $queries["equipamentosdesuso"] : false;
		if ($equipamentosdesuso) array_push($where, " n1.equipamentosdesuso = '$equipamentosdesuso' ");

		$episfuncionarios = (isset($queries["episfuncionarios"])) ? $queries["episfuncionarios"] : false;
		if ($episfuncionarios) array_push($where, " n1.episfuncionarios = '$episfuncionarios' ");

		$termometro = (isset($queries["termometro"])) ? $queries["termometro"] : false;
		if ($termometro) array_push($where, " n1.termometro = '$termometro' ");

		$controlepragas = (isset($queries["controlepragas"])) ? $queries["controlepragas"] : false;
		if ($controlepragas) array_push($where, " n1.controlepragas = '$controlepragas' ");

		$caixadagua = (isset($queries["caixadagua"])) ? $queries["caixadagua"] : false;
		if ($caixadagua) array_push($where, " n1.caixadagua = '$caixadagua' ");

		$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " n1.observacoes = '$observacoes' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " n1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "n1.*"; 
		;
		
		if ($total) $fields = "COUNT(n1.id) as total";
		
		$ordem = "ORDER BY n1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM nutricaovisitastecnicas n1
					
					WHERE n1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		// die($strsql);
		return $db->fetchAll($strsql);			
	}	
	
	public function getNutricaovisitastecnicaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNutricaovisitastecnicas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNutricaovisitastecnicaByIdHelper($id, $queries = array()) {
		$rows = new Nutricaovisitastecnicas();
		return $rows->getNutricaovisitastecnicaById($id, $queries);
	}		
	
	
	public function getNutricaovisitastecnicaByDestino($queries = array()) {
		
		$queries['order'] = 'ORDER BY n1.id DESC';
		$rows = $this->getNutricaovisitastecnicas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	 /**
     * Gerar data atual (dia/mês/ano) com formato português
     * @return string data
     */
    public function getCurrentData() {
        $data = date('D');
        $mes = date('M');
        $dia = date('d');
        $ano = date('Y');

        $semana = array(
            'Sun' => 'Domingo',
            'Mon' => 'Segunda-Feira',
            'Tue' => 'Terça-Feira',
            'Wed' => 'Quarta-Feira',
            'Thu' => 'Quinta-Feira',
            'Fri' => 'Sexta-Feira',
            'Sat' => 'Sábado'
        );

        $mes_extenso = array(
            'Jan' => 'Janeiro',
            'Feb' => 'Fevereiro',
            'Mar' => 'Marco',
            'Apr' => 'Abril',
            'May' => 'Maio',
            'Jun' => 'Junho',
            'Jul' => 'Julho',
            'Aug' => 'Agosto',
            'Nov' => 'Novembro',
            'Sep' => 'Setembro',
            'Oct' => 'Outubro',
            'Dec' => 'Dezembro'
        );

        $data = "{$dia} de " . $mes_extenso["$mes"] . " de {$ano}";
        return $data;
    }

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaovisitastecnicas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];

		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$oldstatus = $row['status'];
			$novoRegistro = false;
		} 
		
		$row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
		$row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
		$row->horavisita = (array_key_exists("horavisita",$dados)) ? $dados["horavisita"] : $row->horavisita;
		$row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->idperfil = (array_key_exists("idperfil",$dados)) ? $dados["idperfil"] : $row->idperfil;
		$row->nomefuncionario = (array_key_exists("nomefuncionario",$dados)) ? $dados["nomefuncionario"] : $row->nomefuncionario;
		$row->funcao = (array_key_exists("funcao",$dados)) ? $dados["funcao"] : $row->funcao;
		$row->idusuariovisita = (array_key_exists("idusuariovisita",$dados)) ? $dados["idusuariovisita"] : $row->idusuariovisita;
		$row->equipeproducaocompleta = (array_key_exists("equipeproducaocompleta",$dados)) ? $dados["equipeproducaocompleta"] : $row->equipeproducaocompleta;
		$row->producoesdescricoes = (array_key_exists("producoesdescricoes",$dados)) ? $dados["producoesdescricoes"] : $row->producoesdescricoes;
		$row->producoesobservacoes = (array_key_exists("producoesobservacoes",$dados)) ? $dados["producoesobservacoes"] : $row->producoesobservacoes;
		$row->producoesorganizacoeshigienes = (array_key_exists("producoesorganizacoeshigienes",$dados)) ? $dados["producoesorganizacoeshigienes"] : $row->producoesorganizacoeshigienes;
		$row->estoqueorganizacoeshigienes = (array_key_exists("estoqueorganizacoeshigienes",$dados)) ? $dados["estoqueorganizacoeshigienes"] : $row->estoqueorganizacoeshigienes;
		$row->estoqueorganizacoeshigienesexecutado = (array_key_exists("estoqueorganizacoeshigienesexecutado",$dados)) ? $dados["estoqueorganizacoeshigienesexecutado"] : $row->estoqueorganizacoeshigienesexecutado;
		$row->estoqueorganizacoeshigienesdeestoque = (array_key_exists("estoqueorganizacoeshigienesdeestoque",$dados)) ? $dados["estoqueorganizacoeshigienesdeestoque"] : $row->estoqueorganizacoeshigienesdeestoque;
		$row->amostrasalimentocoletadas = (array_key_exists("amostrasalimentocoletadas", $dados)) ? $dados["amostrasalimentocoletadas"] : $row->amostrasalimentocoletadas;
		$row->cardapioprogramado = (array_key_exists("cardapioprogramado",$dados)) ? $dados["cardapioprogramado"] : $row->cardapioprogramado;
		$row->cardapioservido = (array_key_exists("cardapioservido",$dados)) ? $dados["cardapioservido"] : $row->cardapioservido;
		$row->cardapioobservacao = (array_key_exists("cardapioobservacao",$dados)) ? $dados["cardapioobservacao"] : $row->cardapioobservacao;
		$row->fluxograma = (array_key_exists("fluxograma",$dados)) ? $dados["fluxograma"] : $row->fluxograma;
		$row->equipamentosfuncionamentos = (array_key_exists("equipamentosfuncionamentos",$dados)) ? $dados["equipamentosfuncionamentos"] : $row->equipamentosfuncionamentos;
		$row->equipamentosdesuso = (array_key_exists("equipamentosdesuso",$dados)) ? $dados["equipamentosdesuso"] : $row->equipamentosdesuso;
		$row->episfuncionarios = (array_key_exists("episfuncionarios",$dados)) ? $dados["episfuncionarios"] : $row->episfuncionarios;
		$row->termometro = (array_key_exists("termometro",$dados)) ? $dados["termometro"] : $row->termometro;
		$row->controlepragas = (array_key_exists("controlepragas",$dados)) ? $dados["controlepragas"] : $row->controlepragas;
		$row->caixadagua = (array_key_exists("caixadagua",$dados)) ? $dados["caixadagua"] : $row->caixadagua;
		$row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
		$row->obs_estoqueorganizacoeshigienesexecutado = (array_key_exists("obs_estoqueorganizacoeshigienesexecutado",$dados)) ? $dados["obs_estoqueorganizacoeshigienesexecutado"] : $row->obs_estoqueorganizacoeshigienesexecutado;
		$row->obs_estoqueorganizacoeshigienesdeestoque = (array_key_exists("obs_estoqueorganizacoeshigienesdeestoque",$dados)) ? $dados["obs_estoqueorganizacoeshigienesdeestoque"] : $row->obs_estoqueorganizacoeshigienesdeestoque;
		$row->obs_producoesorganizacoeshigienes = (array_key_exists("obs_producoesorganizacoeshigienes",$dados)) ? $dados["obs_producoesorganizacoeshigienes"] : $row->obs_producoesorganizacoeshigienes;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->iddocumento = (array_key_exists("iddocumento",$dados)) ? $dados["iddocumento"] : $row->iddocumento;
		$row->dataciente = (array_key_exists("dataciente",$dados)) ? $dados["dataciente"] : $row->dataciente;
		if (is_null($row->datacriacao)) {
	    	$row->datacriacao = date("Y-m-d H:i:s");
		}

		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		$row->save();
		
		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Visita técnica salva com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Visita técnica do ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Visita técnica do ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Visita técnica do ID ".$id." atualizada com sucesso!");

		return $row;
	}
	
}