<?php

/**
 * Define o modelo Agendafuncionariosinternos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Agendafuncionariosinternos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "agendafuncionariosinternos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAgendafuncionariosinternosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$agendafuncionariosinternos = new Agendafuncionariosinternos();
		return $agendafuncionariosinternos->getAgendafuncionariosinternos($queries, $page, $maxpage);
	}
	
	public function getAgendafuncionariosinternos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
		
		
		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " a1.iddepartamento = $iddepartamento ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " a1.titulo LIKE '%$titulo%' ");

$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
		if ($nome) array_push($where, " a1.nome LIKE '%$nome%' ");

$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
		if ($telefone) array_push($where, " a1.telefone LIKE '%$telefone%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");

$idlocal = (isset($queries["idlocal"])) ? $queries["idlocal"] : false; 
    if ($idlocal) array_push($where, " f1.idlocal = '$idlocal' "); 
 



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields, f1.nome, l1.titulo
					FROM agendafuncionariosinternos a1
					JOIN funcionariosgeraisescolas f1 ON a1.idsprofessores = f1.id
					JOIN locais l1 ON l1.id = a1.idlocais
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
	 
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAgendafuncionariointernoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAgendafuncionariosinternos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAgendafuncionariointernoByIdHelper($id, $queries = array()) {
		$rows = new Agendafuncionariosinternos();
		return $rows->getAgendafuncionariointernoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Agendafuncionariosinternos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
		 $row->idcargo = (array_key_exists("idcargo",$dados)) ? $dados["idcargo"] : $row->idcargo;
		 if (is_null($row->datacriacao)) {
					$row->datacriacao = date("Y-m-d H:i:s");
				}
		 
		 if (is_null($row->datacriacao)) {
					$row->datacriacao = date("Y-m-d H:i:s");
				}
								
		 $row->nomerazao = (array_key_exists("nomerazao",$dados)) ? $dados["nomerazao"] : $row->nomerazao;
		 $row->sobrenomefantasia = (array_key_exists("sobrenomefantasia",$dados)) ? $dados["sobrenomefantasia"] : $row->sobrenomefantasia;
		 $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
		 $row->celular = (array_key_exists("celular",$dados)) ? $dados["celular"] : $row->celular;
		 $row->idlocais = (array_key_exists("idlocais",$dados)) ? $dados["idlocais"] : $row->idlocais;
		 $row->idsprofessores = (array_key_exists("idsprofessores",$dados)) ? $dados["idsprofessores"] : $row->idsprofessores;
		 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
				
				
		$row->save();
		
		return $row;
	}
	
}