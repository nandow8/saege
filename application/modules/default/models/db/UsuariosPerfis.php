<?php

/**
 * Define o modelo Perfis de Usuário (UsuariosPerfis)
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class UsuariosPerfis extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "usuariosperfis";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getPerfilByIdHelper($idperfil) {
        $usuariosperfis = new UsuariosPerfis();

        $row = $usuariosperfis->getPerfilById($idperfil);

        return $row;
    }

    public static function getPerfilNomeByIdHelper($id, $queries = array()) {
        $rows = new UsuariosPerfis();
        $row = $rows->getPerfilById($id, $queries);
        return $row['perfil'];
    }

    /**
     * Verifica se o perfil pode manipular ações dos patrimônios
     * @param idperfil int
     * @return boolean
     */
    public static function isAllowedCrudPatrimonio($idperfil) {

        $masters = array(1, 12, 15, 30);
        if ((int) $idperfil <= 0) {
            return false;
        }
        if (in_array($idperfil, $masters)) {
            return true;
        }
        return false;
    }

    public function getPerfilById($idperfil) {
        if ((int) $idperfil <= 0)
            return false;

        $db = Zend_Registry::get('db');
        $strsql = "SELECT p1.*
					FROM usuariosperfis p1
					WHERE p1.excluido='nao'
						AND p1.id = $idperfil;
					ORDER BY p1.perfil";
        return $db->fetchRow($strsql);
    }

    /**
     * Retorna um array com a lista de perfis e limites
     * $param int idperfil
     * @return array
     */
    public function getLimitesByIdperfil($idperfil) {
        $db = Zend_Registry::get('db');
        $strsql = "SELECT l1.*,
						pl1.visualizar,
						pl1.adicionar,
						pl1.editar,
						pl1.excluir
					FROM usuarioslimites l1
						LEFT JOIN usuariosperfislimites pl1 ON pl1.idperfil=$idperfil
							AND pl1.idlimite = l1.id
					ORDER BY modulo, ordemalias, limite";

        return $db->fetchAll($strsql);
    }

    public static function getPerfisHelpers($queries = array(), $page = 0, $maxpage = 0) {
        $rows = new UsuariosPerfis();
        return $rows->getPerfis($queries, $page, $maxpage);
    }

    public function getPerfis($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " p1.id=$id ");

        $status = (isset($queries['status'])) ? $queries['status'] : false;
        if ($status)
            array_push($where, " p1.status='$status' ");

        $individualgeral = (isset($queries['individualgeral'])) ? $queries['individualgeral'] : false;
        if ($individualgeral) {

            if ($individualgeral == "ambos") {
                array_push($where, " (p1.individualgeral='Apenas usuário logado' OR p1.individualgeral='Írá visualizar todos') ");
            } else {
                array_push($where, " p1.individualgeral='$individualgeral' ");
            }
        }

        $chave = (isset($queries['chave'])) ? $queries['chave'] : false;
        if ($chave)
            array_push($where, " p1.perfil LIKE '%$chave%' ");

        $recebeprotocolo = (isset($queries["recebeprotocolo"])) ? $queries["recebeprotocolo"] : false;
        if ($recebeprotocolo)
            array_push($where, " p1.recebeprotocolo = '$recebeprotocolo' ");

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {
                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "p1.*";
        $fields .= ", (SELECT COUNT(u1.id)
                    FROM usuarios u1
                    WHERE u1.idperfil=p1.id
                            AND u1.excluido='nao') as qtdusuarios";

        if ($total)
            $fields = "COUNT(p1.id) as total";


        $ordem = "ORDER BY p1.perfil";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields
            FROM usuariosperfis p1
            WHERE p1.excluido = 'nao'
            $w
            $ordem
            $limit";

//if(!$total) die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return UsuariosPerfis
     */
    public function save($dados) {
        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {

        }

        if ($id > 0)
            $row->id = $id;
        $row->perfil = $dados['perfil'];
        $row->status = $dados['status'];
        $row->individualgeral = $dados['individualgeral'];
        $row->excluido = $dados['excluido'];
        $row->logusuario = $dados['logusuario'];
        $row->logdata = $dados['logdata'];
        $row->recebeprotocolo = $dados['recebeprotocolo'];
        $row->save();

        if (isset($dados['limites']))
            $this->setLimites($dados['limites'], $row->id);
    }

    /**
     * Define os limites do perfil
     * @param array $dados
     * @param int $idperfil
     */
    private function setLimites($dados, $idperfil) {
        $perfisLimites = new UsuariosPerfisLimites();

        try {
            if (!empty($dados)) {

                foreach ($this->getLimitesByIdperfil($idperfil) as $k => $v) {
                    $idlimite = $v['id'];

                    $visualizar = isset($dados['visualizar'][$idlimite]) ? $dados['visualizar'][$idlimite] : 'nao';

                    $adicionar = isset($dados['adicionar'][$idlimite]) ? $dados['adicionar'][$idlimite] : 'nao';

                    $editar = isset($dados['editar'][$idlimite]) ? $dados['editar'][$idlimite] : 'nao';

                    $excluir = isset($dados['excluir'][$idlimite]) ? $dados['excluir'][$idlimite] : 'nao';

                    $dados_limites = array(
                        'idperfil' => $idperfil,
                        'idlimite' => $idlimite,
                        'visualizar' => $visualizar,
                        'adicionar' => $adicionar,
                        'editar' => $editar,
                        'excluir' => $excluir
                    );

                    $perfisLimites->save($dados_limites);
                }

                Fjmx_Util::geraLog(date('Y-m-d'), "Usuários Perfis", Usuarios::getUsuario("id"), "Dados do perfil '$idperfil', adicionados com sucesso!");
            } else {
                Fjmx_Util::geraLog(date('Y-m-d'), "Usuários Perfis", Usuarios::getUsuario("id"), "Não existem dados selecionados para o perfil '$idperfil', verifique.", "E");
            }
        } catch (Exception $ex) {
            Fjmx_Util::geraLog(date('Y-m-d'), "Usuários Perfis", Usuarios::getUsuario("id"), "Erro ao cadastrar perfil. Exception: " . $ex, "E");
        }
    }

}
