<?php

/**
 * Define o modelo Eventossecretarias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Eventossecretarias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "eventossecretarias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEventossecretariasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$eventossecretarias = new Eventossecretarias();
		return $eventossecretarias->getEventossecretarias($queries, $page, $maxpage);
	}
	
	public function getEventossecretarias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$die = (isset($queries['die'])) ? $queries['die'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");

		$idevento = (isset($queries['idevento'])) ? (int)$queries['idevento'] : false;
		if ($idevento) array_push($where, " e1.idevento = $idevento ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " e1.idsecretaria = $idsecretaria ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");

$idusuariointerno = (isset($queries["idusuariointerno"])) ? $queries["idusuariointerno"] : false;
		if ($idusuariointerno) array_push($where, " e1.idusuariointerno = $idusuariointerno ");

$idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " e1.idusuario = $idusuario ");

$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " e1.iddepartamento = $iddepartamento ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " e1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " e1.data <= '$data_f' ");

$datainicio_i = (isset($queries["datainicio_i"])) ? $queries["datainicio_i"] : false;
		if ($datainicio_i) array_push($where, " e1.datainicio >= '$datainicio_i' ");

$datainicio_f = (isset($queries["datainicio_f"])) ? $queries["datainicio_f"] : false;
		if ($datainicio_f) array_push($where, " e1.datainicio <= '$datainicio_f' ");

$horainicio = (isset($queries["horainicio"])) ? $queries["horainicio"] : false;
		if ($horainicio) array_push($where, " e1.horainicio = '$horainicio' ");

$datafim_i = (isset($queries["datafim_i"])) ? $queries["datafim_i"] : false;
		if ($datafim_i) array_push($where, " e1.datafim >= '$datafim_i' ");

$datafim_f = (isset($queries["datafim_f"])) ? $queries["datafim_f"] : false;
		if ($datafim_f) array_push($where, " e1.datafim <= '$datafim_f' ");

$horafim = (isset($queries["horafim"])) ? $queries["horafim"] : false;
		if ($horafim) array_push($where, " e1.horafim = '$horafim' ");

$hora = (isset($queries["hora"])) ? $queries["hora"] : false;
		if ($hora) array_push($where, " e1.hora = '$hora' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " e1.titulo LIKE '%$titulo%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " e1.descricoes = '$descricoes' ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " e1.observacoes = '$observacoes' ");

		$especificoescola = (isset($queries["especificoescola"])) ? $queries["especificoescola"] : false;
		if ($especificoescola) array_push($where, " e1.especificoescola = '$especificoescola' ");

		

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo=="Externa") {
			array_push($where, " (e1.tipo LIKE '%$tipo%' OR e1.mostrarambos = 'Sim') ");
		}elseif ($tipo) {
			array_push($where, " e1.tipo LIKE '%$tipo%' ");
		}

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");

		$mostrarambos = (isset($queries["mostrarambos"])) ? $queries["mostrarambos"] : false;
		if ($mostrarambos) array_push($where, " e1.mostrarambos LIKE '%$mostrarambos%' ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " e1.origem LIKE '%$origem%' ");

$data  = (isset($queries['data'])) ? $queries['data'] : false;
	if ($data) array_push($where, " (e1.datainicio <='$data' AND e1.datafim >= '$data') ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM eventossecretarias e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
	 	
	 	if($die) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}



	public function getEventossecretariasUnion($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$die = (isset($queries['die'])) ? $queries['die'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");

		$idevento = (isset($queries['idevento'])) ? (int)$queries['idevento'] : false;
		if ($idevento) array_push($where, " e1.idevento = $idevento ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " e1.idsecretaria = $idsecretaria ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");

$idusuariointerno = (isset($queries["idusuariointerno"])) ? $queries["idusuariointerno"] : false;
		if ($idusuariointerno) array_push($where, " e1.idusuariointerno = $idusuariointerno ");

$idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " e1.idusuario = $idusuario ");

$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " e1.iddepartamento = $iddepartamento ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " e1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " e1.data <= '$data_f' ");

$datainicio_i = (isset($queries["datainicio_i"])) ? $queries["datainicio_i"] : false;
		if ($datainicio_i) array_push($where, " e1.datainicio >= '$datainicio_i' ");

$datainicio_f = (isset($queries["datainicio_f"])) ? $queries["datainicio_f"] : false;
		if ($datainicio_f) array_push($where, " e1.datainicio <= '$datainicio_f' ");

$horainicio = (isset($queries["horainicio"])) ? $queries["horainicio"] : false;
		if ($horainicio) array_push($where, " e1.horainicio = '$horainicio' ");

$datafim_i = (isset($queries["datafim_i"])) ? $queries["datafim_i"] : false;
		if ($datafim_i) array_push($where, " e1.datafim >= '$datafim_i' ");

$datafim_f = (isset($queries["datafim_f"])) ? $queries["datafim_f"] : false;
		if ($datafim_f) array_push($where, " e1.datafim <= '$datafim_f' ");

$horafim = (isset($queries["horafim"])) ? $queries["horafim"] : false;
		if ($horafim) array_push($where, " e1.horafim = '$horafim' ");

$hora = (isset($queries["hora"])) ? $queries["hora"] : false;
		if ($hora) array_push($where, " e1.hora = '$hora' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " e1.titulo LIKE '%$titulo%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " e1.descricoes = '$descricoes' ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " e1.observacoes = '$observacoes' ");

		$especificoescola = (isset($queries["especificoescola"])) ? $queries["especificoescola"] : false;
		if ($especificoescola) array_push($where, " e1.especificoescola = '$especificoescola' ");

		

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo=="Externa") {
			array_push($where, " (e1.tipo LIKE '%$tipo%' OR e1.mostrarambos = 'Sim') ");
		}elseif ($tipo) {
			array_push($where, " e1.tipo LIKE '%$tipo%' ");
		}

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");

		$mostrarambos = (isset($queries["mostrarambos"])) ? $queries["mostrarambos"] : false;
		if ($mostrarambos) array_push($where, " e1.mostrarambos LIKE '%$mostrarambos%' ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " e1.origem LIKE '%$origem%' ");

$data  = (isset($queries['data'])) ? $queries['data'] : false;
	if ($data) array_push($where, " (e1.datainicio <='$data' AND e1.datafim >= '$data') ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.id, e1.idescola, e1.idusuario, e1.data, e1.datainicio, e1.horainicio, e1.datafim, e1.horafim, e1.hora, e1.titulo, e1.descricoes, e1.status, e1.excluido"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM eventossecretarias e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
	 	//if($die)die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}
	
	
	public static function getEventossecretariasUnionHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$eventossecretarias = new Eventossecretarias();
		return $eventossecretarias->getEventossecretariasUnion($queries, $page, $maxpage);
	}
	
	public function getEventosecretariaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEventossecretarias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}

	public function getEventosecretariaByIdevento($idevento, $queries = array()) {
		if ($idevento==0) return false;
		
		$queries['idevento'] = $idevento;
		$rows = $this->getEventossecretarias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEventosecretariaByIdHelper($id, $queries = array()) {
		$rows = new Eventossecretarias();
		return $rows->getEventosecretariaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Eventossecretarias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		$row->idevento = (array_key_exists("idevento",$dados)) ? $dados["idevento"] : $row->idevento;
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;

		 $row->especificoescola = (array_key_exists("especificoescola",$dados)) ? $dados["especificoescola"] : $row->especificoescola;
 $row->idusuariointerno = (array_key_exists("idusuariointerno",$dados)) ? $dados["idusuariointerno"] : $row->idusuariointerno;
 $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
 $row->horainicio = (array_key_exists("horainicio",$dados)) ? $dados["horainicio"] : $row->horainicio;
 $row->datafim = (array_key_exists("datafim",$dados)) ? $dados["datafim"] : $row->datafim;
 $row->horafim = (array_key_exists("horafim",$dados)) ? $dados["horafim"] : $row->horafim;
 $row->hora = (array_key_exists("hora",$dados)) ? $dados["hora"] : $row->hora;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->mostrarambos = (array_key_exists("mostrarambos",$dados)) ? $dados["mostrarambos"] : $row->mostrarambos;	
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 	
				
		$row->save();
                
                Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), "Evento ".(($id===0)?"Incluido":"Salvo")." com sucesso!");
		
		return $row;
	}
	
}