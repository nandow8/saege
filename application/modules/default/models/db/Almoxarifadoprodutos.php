<?php

/**
 * Define o modelo Almoxarifadoprodutos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Almoxarifadoprodutos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoprodutos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAlmoxarifadoprodutosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$almoxarifadoprodutos = new Almoxarifadoprodutos();
		return $almoxarifadoprodutos->getAlmoxarifadoprodutos($queries, $page, $maxpage);
	}
	
	public function getAlmoxarifadoprodutos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array(); 

		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " vap.id = $id ");
		
		
		// $idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		// if ($idsecretaria) array_push($where, " vap.idsecretaria = $idsecretaria ");

		$produto = (isset($queries["produto"])) ? $queries["produto"] : false;
		if ($produto) array_push($where, " vap.produto LIKE '%$produto%' ");

		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " vap.descricoes LIKE '%$descricoes%' ");

		$codigoproduto = (isset($queries["codigoproduto"])) ? $queries["codigoproduto"] : false;
		if ($codigoproduto) array_push($where, " vap.codigoproduto LIKE '%$codigoproduto%' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " vap.status LIKE '%$status%' ");

		$idcategoria = (isset($queries["idcategoria"])) ? $queries["idcategoria"] : false;
		if ($idcategoria) array_push($where, " vap.idcategoria = '$idcategoria' ");

		$idunidademedida = (isset($queries["idunidademedida"])) ? $queries["idunidademedida"] : false;
		if ($idunidademedida) array_push($where, " vap.idunidademedida = '$idunidademedida' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "vap.*"; 
		;
		
		if ($total) $fields = "COUNT(vap.id) as total";
		
		$ordem = "ORDER BY vap.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
		FROM view_almoxarifado_produtos vap

		WHERE 1
		$w 
		$ordem	
		$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		 
		return $db->fetchAll($strsql);			
	}	
	
	public function getAlmoxarifadoprodutoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAlmoxarifadoprodutos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAlmoxarifadoprodutoByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifadoprodutos();
		return $rows->getAlmoxarifadoprodutoById($id, $queries);
	}	
	
	public function getAlmoxarifadoprodutoByCodigoProduto($codigoproduto, $queries = array()) {
		if ($codigoproduto==0) return false;
		
		$queries['codigoproduto'] = $codigoproduto;
		$rows = $this->getAlmoxarifadoprodutos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAlmoxarifadoprodutoByCodigoProdutoHelper($codigoproduto, $queries = array()) {
		$rows = new Almoxarifadoprodutos();
		return $rows->getAlmoxarifadoprodutoByCodigoProduto($codigoproduto, $queries);
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadoprodutos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
		$row->produto = (array_key_exists("produto",$dados)) ? $dados["produto"] : $row->produto;
		$row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
		$row->codigoproduto = (array_key_exists("codigoproduto",$dados)) ? $dados["codigoproduto"] : $row->codigoproduto;
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}

		$row->idcategoria = (array_key_exists("idcategoria",$dados)) ? $dados["idcategoria"] : $row->idcategoria;
		$row->categoria_id = (array_key_exists("categoria_id",$dados)) ? $dados["categoria_id"] : $row->categoria_id;

		$row->old_unidademedida = (array_key_exists("old_unidademedida",$dados)) ? $dados["old_unidademedida"] : $row->old_unidademedida;
		$row->qtdmin = (array_key_exists("qtdmin",$dados)) ? $dados["qtdmin"] : $row->qtdmin;

		$row->idimagem = (array_key_exists("idimagem",$dados)) ? $dados["idimagem"] : $row->idimagem;

		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		
		$row->save();
		
		return $row;
	}
	
}