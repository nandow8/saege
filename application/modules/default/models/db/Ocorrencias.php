<?php

/**
 * Define o modelo Ocorrencias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Ocorrencias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "ocorrencias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getOcorrenciasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$ocorrencias = new Ocorrencias();
		return $ocorrencias->getOcorrencias($queries, $page, $maxpage);
	}
	
	public function getOcorrencias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " o1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " o1.idescola = $idescola ");

$idescolavinculo = (isset($queries["idescolavinculo"])) ? $queries["idescolavinculo"] : false;
		if ($idescolavinculo) array_push($where, " o1.idescolavinculo = $idescolavinculo ");

$idmateria = (isset($queries["idmateria"])) ? $queries["idmateria"] : false;
		if ($idmateria) array_push($where, " o1.idmateria = $idmateria ");

$idprofessor = (isset($queries["idprofessor"])) ? $queries["idprofessor"] : false;
		if ($idprofessor) array_push($where, " o1.idprofessor = $idprofessor ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " o1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " o1.data <= '$data_f' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " o1.titulo LIKE '%$titulo%' ");

$texto = (isset($queries["texto"])) ? $queries["texto"] : false;
		if ($texto) array_push($where, " o1.texto = '$texto' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " o1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "o1.*"; 
		;
		
		if ($total) $fields = "COUNT(o1.id) as total";
		
		$ordem = "ORDER BY o1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM ocorrencias o1
					
					WHERE o1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getOcorrenciaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getOcorrencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getOcorrenciaByIdHelper($id, $queries = array()) {
		$rows = new Ocorrencias();
		return $rows->getOcorrenciaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ocorrencias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idescolavinculo = (array_key_exists("idescolavinculo",$dados)) ? $dados["idescolavinculo"] : $row->idescolavinculo;
 $row->idmateria = (array_key_exists("idmateria",$dados)) ? $dados["idmateria"] : $row->idmateria;
 $row->idprofessor = (array_key_exists("idprofessor",$dados)) ? $dados["idprofessor"] : $row->idprofessor;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->texto = (array_key_exists("texto",$dados)) ? $dados["texto"] : $row->texto;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}