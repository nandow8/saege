<?php

/**
 * Define o modelo Funcionariosgerais
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosgeraispontohorarios extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosgeraispontohorarios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFuncionariosgeraisHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosgerais = new Funcionariosgerais();
		return $funcionariosgerais->getFuncionariosgerais($queries, $page, $maxpage);
	}
	
	public function getFuncionariosgerais($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
               
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		// 2
		$titulo_pontohorarios = (isset($queries["titulo_pontohorarios"])) ? $queries["titulo_pontohorarios"] : false;
		if ($titulo_pontohorarios) array_push($where, " f1.titulo_pontohorarios = $titulo_pontohorarios ");
                // 3
                $entrada_pontohorarios = (isset($queries["entrada_pontohorarios"])) ? $queries["entrada_pontohorarios"] : false;
		if ($entrada_pontohorarios) array_push($where, " f1.entrada_pontohorarios = $entrada_pontohorarios ");
                // 4
                $saida_pontohorarios = (isset($queries["saida_pontohorarios"])) ? $queries["saida_pontohorarios"] : false;
		if ($saida_pontohorarios) array_push($where, " f1.saida_pontohorarios = $saida_pontohorarios ");
                // 5
                $entradapausa_pontohorarios = (isset($queries["entradapausa_pontohorarios"])) ? $queries["entradapausa_pontohorarios"] : false;
		if ($entradapausa_pontohorarios) array_push($where, " f1.entradapausa_pontohorarios = $entradapausa_pontohorarios ");
                // 6
                $saidapausa_pontohorarios = (isset($queries["saidapausa_pontohorarios"])) ? $queries["saidapausa_pontohorarios"] : false;
		if ($saidapausa_pontohorarios) array_push($where, " f1.saidapausa_pontohorarios = $saidapausa_pontohorarios ");
                // 7
                $status_pontohorarios = (isset($queries["status_pontohorarios"])) ? $queries["status_pontohorarios"] : false;
		if ($status_pontohorarios) array_push($where, " f1.status_pontohorarios = $status_pontohorarios ");
                // 8
                $logusuario_pontohorarios = (isset($queries["logusuario_pontohorarios"])) ? $queries["logusuario_pontohorarios"] : false;
		if ($logusuario_pontohorarios) array_push($where, " f1.logusuario_pontohorarios = $logusuario_pontohorarios ");
                // 9
                $logdata_pontohorarios = (isset($queries["logdata_pontohorarios"])) ? $queries["logdata_pontohorarios"] : false;
		if ($logdata_pontohorarios) array_push($where, " f1.logdata_pontohorarios = $logdata_pontohorarios ");
                
                
                //idfuncionario
                $idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " f1.idfuncionario = $idfuncionario ");
                //idrhfuncionario
                $idrhfuncionario = (isset($queries["idrhfuncionario"])) ? $queries["idrhfuncionario"] : false;
		if ($idrhfuncionario) array_push($where, " f1.idrhfuncionario = $idrhfuncionario ");


		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado "; ;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgerais f1
					 LEFT JOIN enderecos e_idendereco ON e_idendereco.id=f1.idendereco 
						LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado	
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariogeralById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosgerais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionariogeralByIdHelper($id, $queries = array()) {
		$rows = new Funcionariosgerais();
		return $rows->getFuncionariogeralById($id, $queries);
	}		
	
	public static function getFuncionariosgeraisByRgfHelper($rgf, $queries = array()) {
		if ((!isset($rgf)) || (!$rgf) || ($rgf=="")) return false;
		$_rows = new Funcionariosgerais();

		$queries['rgf'] = $rgf;
		$rows = $_rows->getFuncionariosgerais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	/**
        * Salva o dados (INSERT OU UPDATE)
        * @param array dados
        * @return Funcionariosgerais
        */
        
       
        public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		//$row = $this->fetchRow("id=$id AND excluido='nao'");
                $row = $this->fetchRow("id=$id ");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
			$historico = new Funcionariosgeraispontohorarios();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);
		} 
             
		
                $row->titulo_pontohorarios = (array_key_exists("titulo_pontohorarios",$dados)) ? $dados["titulo_pontohorarios"] : $row->titulo_pontohorarios;
                $row->data_pontohorarios = (array_key_exists("data_pontohorarios",$dados)) ? $dados["data_pontohorarios"] : $row->data_pontohorarios;
                $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
                $row->idrhfuncionario = (array_key_exists("idrhfuncionario",$dados)) ? $dados["idrhfuncionario"] : $row->idrhfuncionario;
                $row->entrada_pontohorarios = (array_key_exists("entrada_pontohorarios",$dados)) ? $dados["entrada_pontohorarios"] : $row->entrada_pontohorarios;
                $row->saida_pontohorarios = (array_key_exists("saida_pontohorarios",$dados)) ? $dados["saida_pontohorarios"] : $row->saida_pontohorarios;
                $row->entradapausa_pontohorarios = (array_key_exists("entradapausa_pontohorarios",$dados)) ? $dados["entradapausa_pontohorarios"] : $row->entradapausa_pontohorarios;
                $row->saidapausa_pontohorarios = (array_key_exists("saidapausa_pontohorarios",$dados)) ? $dados["saidapausa_pontohorarios"] : $row->saidapausa_pontohorarios;
                $row->status_pontohorarios  = (array_key_exists("status_pontohorarios ",$dados)) ? $dados["status_pontohorarios "] : $row->status_pontohorarios ;
                $row->logusuario_pontohorarios = (array_key_exists("logusuario_pontohorarios",$dados)) ? $dados["logusuario_pontohorarios"] : $row->logusuario_pontohorarios;
                $row->logdata_pontohorarios = (array_key_exists("logdata_pontohorarios",$dados)) ? $dados["logdata_pontohorarios"] : $row->logdata_pontohorarios;
                
            
                $row->save();
                
                $registro = 'Adicionado';
		if(!$novoRegistro)
			$registro = 'Editado';
			
		Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), " id -> [" . $row->id . "] [" . $registro . "][status=>" . $row->status . "][excluido=>" . $row->excluido . "]");
		
		
		return $row;
        }//end function save
        
        
        /*
         *  Autor : Thiago Torres Migliorati
         *  Função para verificar se há um registro no local 
         *  Criação 04/05/2018
         * 
         */
        
        public function getVerificaRegistro($data, $id){
            
            $strSql = "SELECT * FROM funcionariosgeraispontohorarios WHERE data_pontohorarios = '" . $data . "' and idrhfuncionario = " . $id;
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchAll($strSql);
           
            $counter = 0;
            
            foreach($row as $index):
                $counter++;
            endforeach;
            
            return $counter;
            
        }//end function getVerificaRegistro
        
         public function getRetornaRegistro($id){
            
            $strSql = "SELECT * FROM funcionariosgeraispontohorarios WHERE idrhfuncionario = " . $id;
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchRow($strSql);
            
            return $row;
            
        }//end function getVerificaRegistro

        
        
        // apenas para testes, mas vai que...
        public static function getDiasUteisView($inicio, $fim){
            
                $strSql = "select * from view_pontosmensais_dias_uteis where datainiciobloco = '" . $inicio . "' and datafimbloco = '" . $fim . "'";
            
                $db = Zend_Registry::get('db');
                $row = $db->fetchAll($strSql);
                
                return $row;
                //return $row[0];
            
        }//end function getDiasUteisView

        public static function getDiasUteisPeriodo($inicio, $fim){

                $dataInicial =  date("Y-m-d", strtotime($inicio));
                $dataObjInicio = new DateTime($dataInicial);

                $dataFinal =  date("Y-m-d", strtotime($fim));
                $dataObjFim = new DateTime($dataFinal);

                $dataObjFimEnd = $dataObjFim->modify( '+1 day' ); 
                $interval = new DateInterval('P1D');
                $daterange = new DatePeriod($dataObjInicio, $interval ,$dataObjFim);

                $weekend = 0;

                // contagem de finais de semana
                foreach($daterange as $date){
                        if (($date->format("w") == 0) || ($date->format("w") == 6))
                                $weekend++;
                }//end foreach

                $totalPeriodo = $dataObjInicio->diff($dataObjFimEnd);

                $total = ((int)$totalPeriodo->days - (int)$weekend);

                return $total;

        }// end static function 

        public static function getFeriadosValidos($inicio, $fim){

                $querie = "select * from funcionariosgeraispontoferiados where data_pontoferiado >= '" . $inicio . "' and data_pontoferiado <= '" . $fim . "' and excluido = 'nao'";

                $db = Zend_Registry::get('db');
                $row = $db->fetchAll($querie);

                $counter = 0;

                foreach($row as $auxiliar): 
                       
                        $data = date("Y-m-d", strtotime($auxiliar['data_pontoferiado']));
                        $objData = new DateTime($data);
                        // Verifica se o feriado não cai em um sábado ou domingo
                        if(($objData->format('w') != 0) || ($objData->format('w') != 6)){
                               $counter++;
                        }// end if

                endforeach;

               
                return $counter;

        }//end function getFeriadosValidos

        /* ===============================================================================================================================
                                Novas funções a serem utlizadas em Pontos Mensais
        ================================================================================================================================*/


        // Funcionarios Gerais Lançamentos Realizados
        public static function getFGLanc($idusuario, $idlocal, $dept, $inicio, $fim){

                $querie = 'select count(*) as Total from funcionariosgeraispontohorarios';
                $querie .= ' where idrhfuncionario = ' . $idusuario . ' and idlocalfuncionario = ' . $idlocal . ' and iddeptfuncionario = ' . $dept;
                $querie .= ' and  data_pontohorarios >= "' . $inicio . '" and data_pontohorarios <= "' . $fim . '"';
               

                $db = Zend_Registry::get('db');
                $row = $db->fetchRow($querie);
                
                return $row['Total'];

        }//end getFuncionariosGeraisLancamentos

        // Funcionarios Gerais Lançamentos nos Finais de SemanaRealizados
        public static function getFGLancFDS($idusuario, $idlocal, $dept, $inicio, $fim){

                $querie = 'select count(*) as Total from funcionariosgeraispontohorarios';
                $querie .= ' where idrhfuncionario = ' . $idusuario . ' and idlocalfuncionario = ' . $idlocal . ' and iddeptfuncionario = ' . $dept;
                $querie .= ' and  data_pontohorarios >= "' . $inicio . '" and data_pontohorarios <= "' . $fim . '"';
                $querie .= ' and WEEKDAY(data_pontohorarios) in(5,6)';

                $db = Zend_Registry::get('db');
                $row = $db->fetchRow($querie);
                
                return $row['Total'];

        }//end getFuncionariosGeraisLancamentos

        // Funcionarios Gerais Lançamentos nos Feriados
        public static function getFGLancFeriados($idusuario, $idlocal, $dept, $inicio, $fim){

                $querie = 'select count(*) as Total from funcionariosgeraispontohorarios';
                $querie .= ' where idrhfuncionario = ' . $idusuario . ' and idlocalfuncionario = ' . $idlocal . ' and iddeptfuncionario = ' . $dept;
                $querie .= ' and data_pontohorarios >= "' . $inicio . '" and data_pontohorarios <= "' . $fim . '"';
                $querie .= ' and data_pontohorarios in(select data_pontoferiado from funcionariosgeraispontoferiados where data_pontoferiado BETWEEN "' . $inicio . '" and "' . $fim . '" );';

               

                $db = Zend_Registry::get('db');
                $row = $db->fetchRow($querie);
                
                return $row['Total'];

                $db = Zend_Registry::get('db');
                $row = $db->fetchRow($querie);
                
                return $row['Total'];

        }//end getFuncionariosGeraisLancamentos

        public static function getFGFInjustificadas($idusuario, $idlocal, $dept, $inicio, $fim){

                $querie = 'select count(*) as Total from funcionariosgeraispontohorarios';
                $querie .= ' where idrhfuncionario = ' . $idusuario . ' and idlocalfuncionario = ' . $idlocal . ' and iddeptfuncionario = ' . $dept;
                $querie .= ' and  data_pontohorarios >= "' . $inicio . '" and data_pontohorarios <= "' . $fim . '"';
                $querie .= ' and titulo_pontohorarios = "FALTA INJUSTIFICADA" ';

                $db = Zend_Registry::get('db');
                $row = $db->fetchRow($querie);
                
                return $row['Total'];
                //return $row['Total'];

        }// end getFGFInjustificadas

        public static function getFGFInjustificadasDetalhes($idusuario, $idlocal, $dept, $inicio, $fim){

                $querie = 'select * from funcionariosgeraispontohorarios';
                $querie .= ' where idrhfuncionario = ' . $idusuario . ' and idlocalfuncionario = ' . $idlocal . ' and iddeptfuncionario = ' . $dept;
                $querie .= ' and  data_pontohorarios >= "' . $inicio . '" and data_pontohorarios <= "' . $fim . '"';
                $querie .= ' and (titulo_pontohorarios = "FALTA INJUSTIFICADA"  or titulo_pontohorarios = "Horas Injustificadas") ';

                $db = Zend_Registry::get('db');
                $row = $db->fetchAll($querie);
                
                return $row;
                //return $row['Total'];

        }// end getFGFInjustificadasDetalhes

        public static function getFGFJustificados($idusuario, $idlocal, $dept, $inicio, $fim){

                $querie = 'select count(*) as Total from funcionariosgeraispontohorarios';
                $querie .= ' where idrhfuncionario = ' . $idusuario . ' and idlocalfuncionario = ' . $idlocal . ' and iddeptfuncionario = ' . $dept;
                $querie .= ' and  data_pontohorarios >= "' . $inicio . '" and data_pontohorarios <= "' . $fim . '"';
                $querie .= ' and titulo_pontohorarios != "FALTA INJUSTIFICADA" ';
                $querie .= ' and titulo_pontohorarios != "Horas Injustificadas" ';
                $querie .= ' and titulo_pontohorarios != "Dia Trabalhado" ';

                $db = Zend_Registry::get('db');
                $row = $db->fetchRow($querie);
              
                return $row['Total'];

        }// end getFGFJustificadosDetalhes

        public static function getFGFJustificadosDetalhes($idusuario, $idlocal, $dept, $inicio, $fim){

                $querie = 'select * from funcionariosgeraispontohorarios';
                $querie .= ' where idrhfuncionario = ' . $idusuario . ' and idlocalfuncionario = ' . $idlocal . ' and iddeptfuncionario = ' . $dept;
                $querie .= ' and  data_pontohorarios >= "' . $inicio . '" and data_pontohorarios <= "' . $fim . '"';
                $querie .= ' and titulo_pontohorarios != "FALTA INJUSTIFICADA" ';
                $querie .= ' and titulo_pontohorarios != "Horas Injustificadas" ';
                $querie .= ' and titulo_pontohorarios != "Dia Trabalhado" ';
                $querie .= ' and titulo_pontohorarios != "Ferias" ';
                $querie .= ' and titulo_pontohorarios != "Afastamento" ';
              

                $db = Zend_Registry::get('db');
                $row = $db->fetchAll($querie);
                
                return $row;
                //return $row['Total'];

        }// end getFGFJustificadosDetalhesAtestados

        public static function getFGFJustificadosDetalhesAtestados($idusuario, $idlocal, $dept, $inicio, $fim){

                $querie = 'select count(*) as Total, titulo_pontohorarios as titulo, data_pontohorarios as inicio from funcionariosgeraispontohorarios';
                $querie .= ' where idrhfuncionario = ' . $idusuario . ' and idlocalfuncionario = ' . $idlocal . ' and iddeptfuncionario = ' . $dept;
                $querie .= ' and  data_pontohorarios >= "' . $inicio . '" and data_pontohorarios <= "' . $fim . '"';
                $querie .= ' and titulo_pontohorarios != "FALTA INJUSTIFICADA" ';
                $querie .= ' and titulo_pontohorarios != "Dia Trabalhado" ';
                $querie .= ' and titulo_pontohorarios like "%@%"';
                $querie .= ' group by titulo_pontohorarios';

                $db = Zend_Registry::get('db');
                $row = $db->fetchAll($querie);
                
                return $row;
                //return $row['Total'];

        }// end getFGFJustificadosDetalhesAtestados

        
        public static function getFGRhconfirmados($idusuario, $idlocal, $dept, $inicio, $fim){

                $querie = 'select count(*) as Total from funcionariosgeraispontohorarios';
                $querie .= ' where idrhfuncionario = ' . $idusuario . ' and idlocalfuncionario = ' . $idlocal . ' and iddeptfuncionario = ' . $dept;
                $querie .= ' and  data_pontohorarios >= "' . $inicio . '" and data_pontohorarios <= "' . $fim . '"';
                $querie .= ' and status_pontohorarios = "ok" ';

                $db = Zend_Registry::get('db');
                $row = $db->fetchRow($querie);
              
                return $row['Total'];

        }//end public functino getOcorrenciasLancadasDetalhes

         // Funcionarios Gerais Lançamentos Atrasados que não estão cadastrados como Falta Injustificada
         public static function getFGLancAtrasosTotal($idusuario, $idlocal, $dept, $inicio, $fim){

                // $querie = 'select * from funcionariosgeraispontohorarios';
                // $querie .= ' where idrhfuncionario = ' . $idusuario . ' and idlocalfuncionario = ' . $idlocal . ' and iddeptfuncionario = ' . $dept;
                // $querie .= ' and  data_pontohorarios >= "' . $inicio . '" and data_pontohorarios <= "' . $fim . '" ';
                // $querie .= ' and titulo_pontohorarios != "Falta Injustificada"';

                $querie = 'select ';
                $querie .= 'count(*) as Total ';
                $querie .= 'from '; 
                $querie .= 'funcionariosgeraispontohorarios as f ';
                $querie .= 'inner join ';
                $querie .= 'funcionariosgeraispontodefinicaohorarios as h on ';
                $querie .= 'f.fkhorarioid = h.id ';
                $querie .= 'where ';
                $querie .= 'idrhfuncionario = ' . $idusuario;
                $querie .= ' and idlocalfuncionario = ' . $idlocal; 
                $querie .= ' and iddeptfuncionario = ' . $dept;
                $querie .= ' and data_pontohorarios >= "' . $inicio . '"';
                $querie .= ' and data_pontohorarios <= "' . $fim . '"';
                $querie .= ' and titulo_pontohorarios != "Falta Injustificada"';
                $querie .= ' and TIMEDIFF(TIMEDIFF(f.entradapausa_pontohorarios, f.saidapausa_pontohorarios), TIMEDIFF(f.entrada_pontohorarios, f.saida_pontohorarios )) < TIMEDIFF(TIMEDIFF(h.hora_pausa_entrada, h.hora_pausa_saida), TIMEDIFF(h.hora_entrada, h.hora_saida))';

                $db = Zend_Registry::get('db');
                $rows = $db->fetchRow($querie);
                
                if($rows): 
                        return $rows['Total'];
                else: 
                        return 'final';
                endif;

        }//end getFuncionariosGeraisLancamentos

        // Função para retornaro toral de lançamentos realizados em uma escola ou departamento
        public static function getFGLRHindexTotais($local, $dept, $inicio, $fim){

                $query = 'select '; 
                $query .= 'count(*) as Total ';
                $query .= 'from ';
                $query .= 'funcionariosgeraispontohorarios '; 
                $query .= 'where '; 
                $query .= 'idlocalfuncionario = ' . $local ; 
                $query .= ' and iddeptfuncionario = ' . $dept; 
                $query .= " and data_pontohorarios >= '" . $inicio . "' ";
                $query .= " and data_pontohorarios <= '" . $fim . "' ";

                $db = Zend_Registry::get('db');
                $rows = $db->fetchRow($query);

                return $rows['Total'];

        }// end functio getFGLRHindex

        //Função para buscar o total de funcionarios de Meus Funcionários
        public static function getFGEscolasTotal($id){

                $usuario = Usuarios::getUsuarioByIdHelper($id);
                $idperfil = $usuario['idperfil'];
                $idescola = Funcionariosgeraisescolas::getFuncionariogeralescolaByIdHelper($usuario['idfuncionario'])['idescola'];

                $queries = array();

                if((int)$idescola > 0){
                         
                    $idfuncionario = $usuario['idfuncionario'];
                    $meusfuncionarios = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaImplodeByIdescolaHelper($idfuncionario, $idescola, array());
        
                    if((isset($meusfuncionarios)) && ($meusfuncionarios) && ($meusfuncionarios!="")){
                        $queries['idsfuncionariossel'] = $meusfuncionarios;
                    }else{
                        $queries['idsfuncionariossel'] = '-1';
                    }
        
                }elseif((int)$idperfil > 0){

                    $idfuncionario = $usuario['idfuncionario'];
                    $meusfuncionarios = Meusfuncionariosgeraisescolas::getMeusfuncionariosgeraisescolaImplodeByIdperfilHelper($idfuncionario, $idperfil, array());
        
                    if((isset($meusfuncionarios)) && ($meusfuncionarios) && ($meusfuncionarios!="")){
                        $queries['idsfuncionariossel'] = $meusfuncionarios;
                    }else{
                        $queries['idsfuncionariossel'] = '-1';
                    }
                }

               
                return $queries;


        }// end function getFGEscolasTotal

        public static function getFGL(){

        }//end getFGL

        public static function getTotalFerias($idusuario, $idlocal, $dept, $inicio, $fim){

                $querie = "select "; 
                $querie .= "f.idfuncionarioferias, ";
                $querie .= "f.datainicio, ";
                $querie .= "f.datafim, ";
                $querie .= "DATEDIFF(f.datafim, f.datainicio) as ferias, ";
                $querie .= "DATEDIFF('2018-10-16','" . $inicio . "') as totalfolhaponto, ";
                $querie .= "case  ";
		$querie .= "when ((f.datainicio < " . $inicio . ") and (f.datafim >= '" . $inicio . "'))  then "; 
                $querie .= "DATEDIFF('2018-10-16',f.datainicio) ";
		$querie .= "when ((f.datainicio > '" . $inicio . "') and (f.datafim < '" . $fim . "')) then "; 
                $querie .= "DATEDIFF(f.datafim,f.datainicio) ";
		$querie .= "when ((f.datainicio = '" . $inicio . "') and (f.datafim = '" . $fim . "')) then "; 
                $querie .= "DATEDIFF(f.datafim,f.datainicio) ";
		$querie .= "when ((f.datainicio >= '" . $inicio . "') and (f.datafim > '" . $fim . "')) then "; 
                $querie .= "DATEDIFF(f.datainicio, '2018-10-16') ";
		$querie .= "else ";
                $querie .= "'0' ";
                $querie .= "end as Total, ";
                $querie .= "case "; 
		$querie .= "when ((f.datainicio < '" . $inicio . "') and (f.datafim >= '" . $inicio . "')) then "; 
                $querie .= "'" . $inicio . "' "; 
		$querie .= "when ((f.datainicio > '" . $inicio . "') and (f.datafim < '" . $fim . "')) then "; 
                $querie .= "f.datainicio "; 
		$querie .= "when ((f.datainicio = '" . $inicio . "') and (f.datafim = '" . $fim . "')) then ";  
                $querie .= "f.datainicio "; 
		$querie .= "when ((f.datainicio >= '" . $inicio . "') and (f.datafim > '" . $fim . "')) then ";  
                $querie .= "f.datainicio "; 
		$querie .= "else "; 
                $querie .= "0 "; 
                $querie .= "end as Inicio, "; 
                $querie .= "case "; 
		$querie .= "when ((f.datainicio < '" . $inicio . "') and (f.datafim >= '" . $inicio . "')) then "; 
                $querie .= "f.datafim "; 
		$querie .= "when ((f.datainicio > '" . $inicio . "') and (f.datafim < '" . $fim . "')) then ";  
                $querie .= "f.datafim "; 
		$querie .= "when ((f.datainicio = '" . $inicio . "') and (f.datafim = '" . $fim . "')) then ";  
                $querie .= "f.datafim "; 
		$querie .= "when ((f.datainicio >= '" . $inicio . "') and (f.datafim > '" . $fim . "')) then ";  
                $querie .= "'" . $fim . "' "; 
		$querie .= "else "; 
                $querie .= "0 "; 
                $querie .= "end as Fim "; 
                $querie .= "from "; 
                $querie .= "ferias as f ";
                $querie .= "where ";
                $querie .= "f.idfuncionarioferias = " . $idusuario . " ";
                $querie .= "and ";
                $querie .= "f.progresso = 'aprovado' ";

                $db = Zend_Registry::get('db');
                $rows = $db->fetchRow($querie);

                // Somatório dos valores sábados e domingos

                $dateObjInicio =  date("d-m-Y", strtotime($rows['Inicio']));
                $brDateInicio = new DateTime($dateObjInicio);
                
                $dateObjFim =  date('d-m-Y', strtotime($rows['Fim']));
                $brDateFim = new DateTime($dateObjFim);
                
                $interval = new DateInterval('P1D');

                $endOne = new DateTime($dateObjFim);
                $endOne->modify('+1 day');

                $period = new DatePeriod($brDateInicio, $interval, $brDateFim);

                $contadorDiasUteis = 0;
                $contadorWeekends = 0;

                foreach($period as $index): 
                        if($index->format("w") == "0" || $index->format('w') == "6"){
                                $contadorWeekends++;
                        }else{
                                $contadorDiasUteis++;
                        }
                endforeach;

                return $contadorDiasUteis;

                // return 'retorno';

        }//end function g


	
}