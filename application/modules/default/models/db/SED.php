<?php

/**
 * <b>INTEGRAÇÃO – SED</b>
 * 
 * Integração do Sistema de Cadastro de Alunos (SCA) e Secretaria Digital (SED)
 * 
 * Está classe é composta de vários métodos que fazem as operações usando SOAP,
 * consulte a documentação de cada método ou o documento referente a cada método.
 *  
 * @author Júlio César Santos <julio@fjmx.com.br>
 * 
 * @see https://homologacaointegracaosed.educacao.sp.gov.br/documentacao/CadastrodeAlunos/
 * 
 */

class SED {

    /**
     * @see DOC01 - SEESP - Integracao - Incluir Coleta Classe.pdf / Versão 1.7
     * @see DOC02 - SEESP - Integracao - Alterar Coleta Classe.pdf / Versão 1.8
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * @param String $tipo  - Tipo de Operação a ser realizada, "Inc" para inclusão e "Alt" para alteração.
     * 
     */
    public static function ColetaClasse($dados, $tipo) {
/*
        $AlunosClasse = EscolasAlunos::getEscolasalunosHelper(array('classe' => $dados['classe']));
        
        // ATENÇÃO Verificar se sala já tem alunos.
        if ($AlunosClasse) {
            
        } 
*/
        if ($tipo === 'Alt' && (isset($dados["classe"]) && $dados["classe"] > 0 )) {
            $soaptipo = 'Alterar' ;
        }else {
            $soaptipo = 'Incluir';
        }

        $soapstatus = '';
        $soaptipo = ($tipo === 'Alt') ? 'Alterar' : 'Incluir';

        $escola = Escolas::getEscolaByIdHelper($dados["idescola"]);
        $serie = Escolasseries::getEscolaserieByIdHelper($dados['idserie']);
        $sala = Escolassalas::getEscolasalaByIdHelper($dados['idsala']);
        $periodo = Escolasperiodos::getEscolaperiodoByIdHelper($dados['idperiodo']);
        $turma = Escolasturmas::getescolaturmaByIdHelper($dados['idturma']);
        $gdae = Gdae_Identificadoresescolas::getIdentificadorByIdHelper($serie['idgdae']);

        if (!$escola || !$serie || !$sala || !$periodo || !$turma || !$gdae) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($escola) ? '' : '<br>' . 'Escolas') .
                    (isset($serie) ? '' : '<br>' . 'Séries') .
                    (isset($sala) ? '' : '<br>' . 'Salas') .
                    (isset($periodo) ? '' : '<br>' . 'Período') .
                    (isset($turma) ? '' : '<br>' . 'Turmas') .
                    (isset($gdae) ? '' : '<br>' . 'Escolas tipos de ensino.');

            Fjmx_Util::setStatusSED($dados["id"], 'Escolasvinculos', $soapstatus, $soaptipo, '');

            return false;
        }

        // Trata códigos das atividades complementares setadas em escolas vínculos
        $atcs = '';
        if (isset($vinculo[0]['idssedatividadescomplementares'])) {
            $atc = SEDAtividadesComplementares::getSEDAtividadesComplementaresHelper(array('ids' => $vinculo[0]['idssedatividadescomplementares']));
            foreach ($atc as &$row) {
                $atcs = $atcs . $row['codigo'] . ';';
            }
            $atcs = substr_replace($atcs, '', -1);
        }

        if ($dados) {

            $complexType = [
                'inCapacidadeFisica' => Fjmx_Util::ValidaCampoSoap($sala["quantidade"], "S", "NU", 2), // Quantidade máxima permitida de alunos na sala
                'inCursoSemestral1' => Fjmx_Util::ValidaCampoSoap('', "N", "AL", 1), // Marca com um X se o Curso inicia no 1o Semestre do Ano
                'inCursoSemestral2' => Fjmx_Util::ValidaCampoSoap('', "N", "AL", 1), // Marca com um X se o Curso inicia no 2o Semestre do Ano
                'inCodigoINEP' => Fjmx_Util::ValidaCampoSoap('', "N", "AL"), // Código INEP para os Tipos de Ensino 25 ou 35
                'inNumeroSala' => Fjmx_Util::ValidaCampoSoap($sala["numerosala"], "S", "AL", 3), // Número da sala, com base no cadastro de dependências da unidade escolar
                'inSerieAno' => Fjmx_Util::ValidaCampoSoap($serie['serie'], "N", "AL", 2), // Série / Ano / Termo do Tipo de Ensino
                'inTipoClasse' => Fjmx_Util::ValidaCampoSoap($dados['idtipodeclasse'], "N", "AL", 2), // Tipo da Classe (item 2.2.3)
                'inTipoEnsino' => Fjmx_Util::ValidaCampoSoap($gdae['codigo'], "S", "AL", 2), // Tipo de ensino da classe (item 2.2.1)
                'inTurma' => Fjmx_Util::ValidaCampoSoap($turma['turma'], "S", "AL", 2), // Turma (Ex. A, B, 1, 2, A1, B1)
                'inTurno' => Fjmx_Util::ValidaCampoSoap($dados['turno'], "S", "AL", 1, 'MANHÃ,1;INTERMEDIARIO,2;TARDE,3;VESPERTINO,4;NOITE,5;INTEGRAL,6'), // Turno de funcionamento da classe (item 2.2.2)
                'inAno' => Fjmx_Util::ValidaCampoSoap($dados['ano'], "S", "NU", 4), // Ano letivo o qual a alteração de classe se aplica.
                'inSegundaFeira' => Fjmx_Util::ValidaCampoSoap($escola["segunda"], "N", "AL", 1, "0, ;1,X"), // Dia da semana contemplado pela classe. Marcar com “X”
                'inTercaFeira' => Fjmx_Util::ValidaCampoSoap($escola["terca"], "N", "AL", 1, "0, ;1,X"), // Dia da semana contemplado pela classe. Marcar com “X”
                'inQuartaFeira' => Fjmx_Util::ValidaCampoSoap($escola["quarta"], "N", "AL", 1, "0, ;1,X"), // Dia da semana contemplado pela classe. Marcar com “X”
                'inQuintaFeira' => Fjmx_Util::ValidaCampoSoap($escola["quinta"], "N", "AL", 1, "0, ;1,X"), // Dia da semana contemplado pela classe. Marcar com “X”
                'inSextaFeira' => Fjmx_Util::ValidaCampoSoap($escola["sexta"], "N", "AL", 1, "0, ;1,X"), // Dia da semana contemplado pela classe. Marcar com “X”
                'inSabado' => Fjmx_Util::ValidaCampoSoap($escola["sabado"], "N", "AL", 1, "0, ;1,X"), // Dia da semana contemplado pela classe. Marcar com “X”
                'inDiaInicioAula' => Fjmx_Util::ValidaCampoSoap(substr($dados["inicioaulas"], -2, 2), "S", "NU", 2), // Dia início da Aula (dd)
                'inDiaTerminoAula' => Fjmx_Util::ValidaCampoSoap(substr($dados["terminoaulas"], -2, 2), "S", "NU", 2), // Dia fim da Aula (dd)
                'inHoraInicial' => Fjmx_Util::ValidaCampoSoap(substr($periodo["horarioinicio"], 0, 5), "S", "NU", 4, ':,'), // Horário de início do curso (hhmm)
                'inHoraFinal' => Fjmx_Util::ValidaCampoSoap(substr($periodo["horariofim"], 0, 5), "S", "NU", 4, ':,'), // Horário de término do curso (hhmm)
                'inMesInicioAula' => Fjmx_Util::ValidaCampoSoap(substr($dados["inicioaulas"], -5, 2), "S", "NU", 2), // Mês de início das aulas (mm)
                'inMesTerminoAula' => Fjmx_Util::ValidaCampoSoap(substr($dados["terminoaulas"], -5, 2), "S", "NU", 2), // Mês de término das aulas (mm)
            ];

            if (($dados['turno'] === 'INTEGRAL') || ((int) $dados['turno'] === 6)) {
                $complexType = array_merge($complexType, ['inProgMaisEducacao' => Fjmx_Util::ValidaCampoSoap('N', "S", "AL", 1)]);                                    // Classe participa do programa Mais Educação ( “S” / “N”)? Obrigatório para Turno = 6
            }

            if ($tipo === 'Alt') {
                $complexType = array_merge($complexType, ['inNumClasse' => Fjmx_Util::ValidaCampoSoap($dados["classe"], "S", "NU", 9)]);                              // Número da classe que será alterada.
            }

            if ($tipo === 'Inc') {
                $complexType = array_merge($complexType, ['inCodEscola' => Fjmx_Util::ValidaCampoSoap($escola["cod_esc"], "S", "NU", 7)]);                            // Código CIE da escola.
            }

            if ($gdae['codigo'] === '33' && $dados['idtipoclasse'] === '8') {
                $complexType = array_merge($complexType, ['inFlagConvenioEst' => Fjmx_Util::ValidaCampoSoap('', "N", "AL", 1)]);                                      // Classe possui convênio com o Estado ( “S” / “N”)? Exclusivo para Tipo de Ensino 33 e Tipo de Classe 8
            }

            if ($gdae['codigo'] === '32') {
                $complexType = array_merge($complexType, ['inAEE' => Fjmx_Util::ValidaCampoSoap(str_replace(',', ';', $dados['idsatividadesaee']), "N", "AL", 40)]);  // Atividades (AEE) desenvolvidas na classe multifuncional. Codificação detalhada no item 2.4. Os códigos deverão ser separados por ponto e vírgula (Ex: 1;2;5,11). Será obrigatório quando inTipoEnsino = 32
            }

            if ($gdae['codigo'] === '26') {
                $complexType = array_merge($complexType, ['inATC' => Fjmx_Util::ValidaCampoSoap($atcs, "N", "AL", 40)]);                                              // Atividades (ATC) desenvolvidas na classe Codificação detalhada no item 2.5. Os códigos deverão ser separados por ponto e vírgula (Ex: 10101, 10103;15001). Será obrigatório quando inTipoEnsino = 26
            }

            $soapstatus = Fjmx_Util::getSOAP($soaptipo.__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], Escolasvinculos, $soapstatus, $soaptipo, $soaptipo.__FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC04 - SEESP - Integracao - Consultar Ficha Aluno
     */

    public static function ConsultarFichaAluno($dados, $option = 'ConsultaRA') {

        $soapstatus = '';
        $soaptipo = 'Consultar';

        if ($dados) {
            if ($option === 'ConsultaRA') { // OPÇÃO 1 - ConsultaRA
                $complexType = [$option => [
                        'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Alfanumérico (12) - Obrigatório; comprimento máximo. Número de Registro do Aluno.
                        'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Alfanumérico (02) - Quando houver Letra Maiúsculo. Digito de Registro do Aluno.
                        'inUF' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)     // Alfanumérico (02) - Obrigatório; comprimento máximo. Letra Maiúscula. UF do Aluno.
                ]];
            } else if ($option === 'ConsultaFonetica') { //OPÇÃO 2 - ConsultaFonetica
                $complexType = [$option => [
                        'inNomeAluno' => Fjmx_Util::ValidaCampoSoap($dados["NomeAluno"], "S", "AL", 60), // Obrigatório, caso inNomeSocial não informado - Nome completo do aluno.
                        'inNomeSocial' => Fjmx_Util::ValidaCampoSoap($dados["NomeSocial"], "S", "AL", 50), // Obrigatório, caso inNomeAluno não informado - Nome social do aluno, atendendo o Decreto 55588.
                        'inNomeMae' => Fjmx_Util::ValidaCampoSoap($dados['NomeMae'], "S", "AL", 60), // Obrigatório, caso os parâmetros da data de nascimento não sejam informados - Nome completo da mãe do aluno.
                        'inDiaNascimento' => Fjmx_Util::ValidaCampoSoap(substr($dados["Nascimento"], 0, 2), "S", "NU", 2), // Obrigatório, caso os demais parâmetros da data de nascimento sejam informados e inNomeMae não informado - Dia de nascimento do aluno (formato dd).
                        'inMesNascimento' => Fjmx_Util::ValidaCampoSoap(substr($dados["Nascimento"], 3, 2), "S", "NU", 2), // Obrigatório, caso os demais parâmetros da data de nascimento sejam informados e inNomeMae não informado - Mês de nascimento do aluno (formato mm).
                        'inAnoNascimento' => Fjmx_Util::ValidaCampoSoap(substr($dados["Nascimento"], 6, 4), "S", "NU", 4)     // Obrigatório, caso os demais parâmetros da data de nascimento sejam informados e inNomeMae não informado - Ano de nascimento do aluno (formato aaaa).
                ]];
            } else if ($option === 'ConsultaNomeCompleto') { //OPÇÃO 3 – ConsultaNomeCompleto
                $complexType = [$option => [
                        'inNomeComplAluno' => Fjmx_Util::ValidaCampoSoap($dados["NomeComplAluno"], "N", "AL", 60), // Nome completo do aluno
                        'inNomeComplMae' => Fjmx_Util::ValidaCampoSoap($dados["NomeComplMae"], "N", "AL", 60), // Nome completo da mãe do aluno
                        'inNomeComplPai' => Fjmx_Util::ValidaCampoSoap($dados["NomeComplPai"], "N", "AL", 60)     // Nome completo do pai do aluno                
                ]];
            } else if ($option === 'ConsultaDocumentos') { //OPÇÃO 4 – ConsultaDocumentos
                $complexType = [$option => [
                        'inNumRG' => Fjmx_Util::ValidaCampoSoap($dados["NumRG"], "N", "AL", 16), // Numero do RG ou RNE do aluno
                        'inDigitoRG' => Fjmx_Util::ValidaCampoSoap($dados["DigitoRG"], "N", "AL", 2), // Digito do RG do aluno
                        'inUFRG' => Fjmx_Util::ValidaCampoSoap($dados["UFRG"], "N", "AL", 2), // Sigla UF do RG do aluno
                        'inCPF' => Fjmx_Util::ValidaCampoSoap($dados["CPF"], "N", "AL", 11), // Numero do CPF do aluno
                        'inNumINEP' => Fjmx_Util::ValidaCampoSoap($dados["NumINEP"], "N", "AL", 13), // Numero do código INEP do aluno
                        'inNumNIS' => Fjmx_Util::ValidaCampoSoap($dados["NumNIS"], "N", "AL", 13), // Numero NIS do aluno
                        'inNumCertidaoNova' => Fjmx_Util::ValidaCampoSoap($dados["NumCertidaoNova"], "N", "AL", 33), // Numero da certidão de nascimento do aluno, formato novo, válido a partir de 2010
                        'inNumCertidaoNasc' => Fjmx_Util::ValidaCampoSoap($dados["NumCertidaoNasc"], "N", "AL", 8), // Numero da certidão de nascimento do aluno, formato antigo, válido até de 2010
                        'inLivroCertidaoNasc' => Fjmx_Util::ValidaCampoSoap($dados["LivroCertidaoNasc"], "N", "AL", 6), // Identificação do livro onde a certidão está contida
                        'inFolhaCertidaoNasc' => Fjmx_Util::ValidaCampoSoap($dados["FolhaCertidaoNasc"], "N", "AL", 6)  // Identificação da folha onde a cetidão está contida
                ]];
            }

            return Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
        } else {
            return false;
        }
    }

    /**
     * @see DOC05 - SEESP - Integracao - Alterar Endereço Ficha Aluno.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * @param String $tipo  - Tipo de Operação a ser realizada, "Inc" para inclusão e "Alt" para alteração.
     * 
     */

    public static function AlterarEnderecoFichaAluno($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        $endereco = ($dados['enderecos']);

        if (!$endereco) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($endereco) ? '' : '<br>' . 'Não existe endereço.');

            Fjmx_Util::setStatusSED($dados["id"], Alunosenderecos, $soapstatus, $soaptipo, '');

            return false;
        }

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2), // UF do RA do Aluno
                'inCep' => Fjmx_Util::ValidaCampoSoap($endereco['inCep'], "S", "AL", 8, '-,'), // CEP do aluno.
                'inCidade' => Fjmx_Util::ValidaCampoSoap($endereco['inCidade'], "S", "AL", 22), // Cidade.
                'inUF' => Fjmx_Util::ValidaCampoSoap($endereco['inUF'], "S", "AL", 2), // Unidade Federetiva do endereço.
                'inLogradouro' => Fjmx_Util::ValidaCampoSoap($endereco['inLogradouro'], "N", "AL", 60), // Rua do aluno
                'inNumero' => Fjmx_Util::ValidaCampoSoap($endereco['inNumero'], "N", "AL", 6), // Número da residência
                'inComplemento' => Fjmx_Util::ValidaCampoSoap($endereco['inComplemento'], "N", "AL", 13), // Complemento de endereço
                'inBairro' => Fjmx_Util::ValidaCampoSoap($endereco['inBairro'], "N", "AL", 30), // Bairro
                'inTipoLogradouro' => Fjmx_Util::ValidaCampoSoap($endereco['inTipoLogradouro'], "N", "AL", 1), // Tipo de logradouro. Deve ser preenchido com 0 ou 1 (0 - URBANO e 1- RURAL
                'inDDD' => Fjmx_Util::ValidaCampoSoap($endereco['inDDD'], "N", "AL", 2), // DDD do telefone
                'inFoneResidencial' => Fjmx_Util::ValidaCampoSoap($endereco['inFoneResidencial'], "N", "AL", 9), // Telefone residencial
                'inFoneRecados' => Fjmx_Util::ValidaCampoSoap($endereco['inFoneRecados'], "N", "AL", 9), // Telefone de recados
                'inNomeFoneRecado' => Fjmx_Util::ValidaCampoSoap($endereco['inNomeFoneRecado'], "N", "AL", 10), // Contato do recado
                'inSMS' => Fjmx_Util::ValidaCampoSoap($endereco['inSMS'], "N", "AL", 1), // Torna-se obrigatório em conjunto com os campos inDDDCel e inFoneCel
                'inDDDCel' => Fjmx_Util::ValidaCampoSoap($endereco['inDDDCel'], "N", "AL", 2), // Torna-se
                'inFoneCel' => Fjmx_Util::ValidaCampoSoap($endereco['inFoneCel'], "N", "AL", 9), // Torna-se obrigatório em conjunto com os campos inSMS e inDDDCel. Telefone celular, apenas números, ex: 999999999.
                'inLatitude' => Fjmx_Util::ValidaCampoSoap($endereco['inLatitude'], "N", "AL", 17), // Coordenada de latitude do endereço residencial
                'inLongitude' => Fjmx_Util::ValidaCampoSoap($endereco['inLongitude'], "N", "AL", 17), // Coordenada de longitude do endereço residencial
                'endIndicativoinBairro' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoInBairro'], "N", "AL", 30), // Bairro
                'endIndicativoinCidade' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinCidade'], "S", "AL", 22), // Cidade.
                'endIndicativoinLatitude' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinLatitude'], "N", "AL", 17), // Coordenada de latitude do endereço residencial
                'endIndicativoinLongitude' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinLongitude'], "N", "AL", 17), // Coordenada de longitude do endereço residencial
                'endIndicativoinLogradouro' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinLogradouro'], "N", "AL", 60), // Rua do aluno
                'endIndicativoinNumero' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinNumero'], "N", "AL", 6), // Número da residência
                'endIndicativoinUF' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinUF'], "S", "AL", 2)       // Unidade Federetiva do endereço.
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');

            Fjmx_Util::setStatusSED($dados["id"], Alunosenderecos, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC09 - SEESP - Integracao - Realizar Matrícula Info Com RA.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function RealizarMatriculaInfoComRA($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC10 - SEESP - Integracao - Realizar Matrícula Info Sem RA.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function RealizarMatriculaInfoSemRA($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC11 - SEESP - Integracao - Alterar Dados Pessoais Ficha Aluno.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * @param String $tipo  - Tipo de Operação a ser realizada, "Inc" para inclusão e "Alt" para alteração.
     * 
     */

    public static function DadosPessoaisFichaAluno($dados, $tipo) {

        if ($tipo === 'Alt' && (int) $dados["id"] === 0) {
            $tipo = 'Inc';
        }

        $soapstatus = '';
        $soaptipo = ($tipo === 'Alt') ? 'Alterar' : 'Incluir';
        
        $endereco = Alunosenderecos::getEnderecosHelper(array('idaluno'=>$dados['id']));
//        $serie = Escolasseries::getEscolaserieByIdHelper($dados['idserie']);
//        $sala = Escolassalas::getEscolasalaByIdHelper($dados['idsala']);
//        $periodo = Escolasperiodos::getEscolaperiodoByIdHelper($dados['idperiodo']);
//        $turma = Escolasturmas::getescolaturmaByIdHelper($dados['idturma']);
//        $gdae = Gdae_Identificadoresescolas::getIdentificadorByIdHelper($serie['idgdae']);
        if (!$endereco /* || !$serie || !$sala || !$periodo|| !$turma || !$gdae */) {

            $soapstatus = 'Faltam informações para o processamento...' .
//                    (isset($escola) ? '' : '<br>' . 'Escolas') .
//                    (isset($serie) ? '' : '<br>' . 'Séries') .
//                    (isset($sala) ? '' : '<br>' . 'Salas') .
//                    (isset($periodo) ? '' : '<br>' . 'Período').
//                    (isset($turma) ? '' : '<br>' . 'Turmas') .
                    (isset($endereco) ? '' : '<br>' . 'Não existe endereço.');

            Fjmx_Util::setStatusSED($dados["id"], 'Escolasalunos', $soapstatus, $soaptipo, __FUNCTION__);

            return false;
        }

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2), // UF do RA do Aluno
                'inNomeAluno' => Fjmx_Util::ValidaCampoSoap($dados["nomerazao"], "S", "AL", 60), // Nome do Aluno
                'inSexo' => Fjmx_Util::ValidaCampoSoap($dados["sexo"], "S", "AL", 1, "Masculino,M;Feminino,F"), // Sexo (“M” ou “F”)
                'inCorRaca' => Fjmx_Util::ValidaCampoSoap($dados["etnia"], "S", "NU", 2, "BRANCA,1;PRETA,2;PARDA,3;AMARELA,4;INDÍGENA,5;NÃO DECLARADA,6"), // Cor/Raça do aluno
                'inDiaNascimento' => Fjmx_Util::ValidaCampoSoap(substr($dados["nascimento"], 8, 2), "S", "NU", 2), // Dia do nascimento do aluno no formato “dd”
                'inMesNascimento' => Fjmx_Util::ValidaCampoSoap(substr($dados["nascimento"], 5, 2), "S", "NU", 2), // Mês do nascimento do aluno no formato “mm”
                'inAnoNascimento' => Fjmx_Util::ValidaCampoSoap(substr($dados["nascimento"], 0, 4), "S", "NU", 4), // Ano do nascimento do aluno no formato “aaaa”
                'inNomeMae' => Fjmx_Util::ValidaCampoSoap($dados["nomemae"], "S", "AL", 60), // Nome da mãe
                'inNomePai' => Fjmx_Util::ValidaCampoSoap($dados["nomepai"], "N", "AL", 60), // Nome do pai
                'inNomeSocial' => Fjmx_Util::ValidaCampoSoap($dados["sobrenomefantasia"], "N", "AL", 50), // Nome Social do Aluno, atendendo o decreto 55588
                'inEmail' => Fjmx_Util::ValidaCampoSoap($dados["email"], "N", "AL", 40), // Email do aluno
                'InBolsaFamilia' => Fjmx_Util::ValidaCampoSoap($dados["bolsafamilia"], "S", "AL", 1, "Sim,S;Nao,N"), // Bolsa Família (“S” ou “N”)
                'InQuilombola' => Fjmx_Util::ValidaCampoSoap($dados["quilombola"], "S", "AL", 1, 'Sim,S;Nao,N'), // Quilombola (“S” ou “N”)
                'inNecEspecial' => Fjmx_Util::ValidaCampoSoap($dados["defnecespecial"], "S", "AL", 1, "0,N;1,S"), // Indica se o aluno possui alguma necessidade especial.
                'inMultipla' => Fjmx_Util::ValidaCampoSoap($dados["defmultipla"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 1 - MULTIPLA
                'inCegueira' => Fjmx_Util::ValidaCampoSoap($dados["defcegueira"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 2 - CEGUEIRA
                'inBaixaVisao' => Fjmx_Util::ValidaCampoSoap($dados["defbaixavisao"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 3 - BAIXA VISAO
                'inSurdezSeveraProfunda' => Fjmx_Util::ValidaCampoSoap($dados["defsurdezseveraprofunda"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 4 - SURDEZ SEVERA OU PROFUNDA
                'inSurdezLeveModerada' => Fjmx_Util::ValidaCampoSoap($dados["defsurdezlevemoderada"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 5 - SURDEZ LEVE OU MODERADA
                'inSurdocegueira' => Fjmx_Util::ValidaCampoSoap($dados["defsurdocegueira"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 6 - SURDOCEGUEIRA
                'inFisicaParalisiaCerebral' => Fjmx_Util::ValidaCampoSoap($dados["deffisicaparalisiacerebral"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 7 - FISICA - PARALISIA CEREBRAL
                'inFisicaCadeirante' => Fjmx_Util::ValidaCampoSoap($dados["deffisicacadeirante"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 8 - FISICA - CADEIRANTE
                'inFisicaOutros' => Fjmx_Util::ValidaCampoSoap($dados["deffisicaoutros"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 9 - FISICA - OUTROS (
                'inSindromeDown' => Fjmx_Util::ValidaCampoSoap($dados["defsindromedown"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 10 - SINDROME DE DOWN
                'inIntelectual' => Fjmx_Util::ValidaCampoSoap($dados["defintelectual"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 11 - INTELECTUAL
                'inAutistaClassico' => Fjmx_Util::ValidaCampoSoap($dados["defautistaclassico"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 20 - AUTISTA CLASSICO
                'inSindromeAsperger' => Fjmx_Util::ValidaCampoSoap($dados["defsindromeasperger"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 21 - SINDROME DE ASPERGER
                'inSindromeRett' => Fjmx_Util::ValidaCampoSoap($dados["defsindromerett"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 22 - SINDROME DE RETT
                'inTransDesintegrativoInf' => Fjmx_Util::ValidaCampoSoap($dados["deftransdesintegrativoinf"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 23 - TRANSTORNO DESINTEGRATIVO DA INFANCIA
                'inAltasHabSuperdotacao' => Fjmx_Util::ValidaCampoSoap($dados["defaltashabsuperdotacao"], "S", "NU", 1, "0, ;1,X"), // Necessidade Especial 30 - ALTAS HABILIDADES/SUPERDOTACAO
                'inPermanente' => Fjmx_Util::ValidaCampoSoap($dados["defpermanente"], "S", "NU", 1, "0, ;1,X"), //Comprimento máximo. Obrigatório preencher ou ‘inPermanente’ ou ‘inTemporaria’, caso inMobilidadeReduzida = ‘S’.		Tipo de mobilidade reduzida. (Apenas preencher se houver mobilidade reduzida permanente, informar: ‘X’)
                'inTemporaria' => Fjmx_Util::ValidaCampoSoap($dados["deftemporaria"], "S", "NU", 1, "0, ;1,X"), //Comprimento máximo. Obrigatório preencher ou ‘inPermanente’ ou ‘inTemporaria’, caso inMobilidadeReduzida = ‘S’.		Tipo de mobilidade reduzida. (Apenas preencher se houver mobilidade reduzida temporária, informar: ‘X’)
                'inMobilidadeReduzida' => Fjmx_Util::ValidaCampoSoap($dados["defmobilidadereduzida"], "S", "NU", 1, "0,N;1,S"), // Aluno possui mobilidade reduzida? (S ou N) Sim ou Não.
                'inAuxilioLeitor' => Fjmx_Util::ValidaCampoSoap($dados["defauxilioleitor"], "S", "NU", 1, "0, ;1,X"), // Indicador de um tipo de recurso para auxiliar na avaliação do aluno com necessidade especial
                'inAuxilioTranscricao' => Fjmx_Util::ValidaCampoSoap($dados["defauxiliotranscricao"], "S", "NU", 1, "0, ;1,X"), // Indicador de um tipo de recurso para auxiliar na avaliação do aluno com necessidade especial
                'inGuiaInterprete' => Fjmx_Util::ValidaCampoSoap($dados["defguiainterprete"], "S", "NU", 1, "0, ;1,X"), // Indicador de um tipo de recurso para auxiliar na avaliação do aluno com necessidade especial
                'inInterpreteLibras' => Fjmx_Util::ValidaCampoSoap($dados["definterpretelibras"], "S", "NU", 1, "0, ;1,X"), // Indicador de um tipo de recurso para auxiliar na avaliação do aluno com necessidade especial
                'inLeituraLabial' => Fjmx_Util::ValidaCampoSoap($dados["defleituralabial"], "S", "NU", 1, "0, ;1,X"), // Indicador de um tipo de recurso para auxiliar na avaliação do aluno com necessidade especial
                'inNenhum' => Fjmx_Util::ValidaCampoSoap($dados["defnenhum"], "S", "NU", 1, "0, ;1,X"), // Indicador de um tipo de recurso para auxiliar na avaliação do aluno com necessidade especial
                'inProvaBraile' => Fjmx_Util::ValidaCampoSoap($dados["defprovabraile"], "S", "NU", 1, "0, ;1,X"), // Indicador de um tipo de recurso para auxiliar na avaliação do aluno com necessidade especial
                'inProvaAmpliada' => Fjmx_Util::ValidaCampoSoap($dados["defprovaampliada"], "S", "NU", 1, "0, ;1,X"), // Indicador de um tipo de recurso para auxiliar na avaliação do aluno com necessidade especial
                'inTam16' => Fjmx_Util::ValidaCampoSoap($dados["deftam16"], "S", "NU", 1, "0, ;1,X"), // Indicador de prova com tamanho de fonte 16.
                'inTam20' => Fjmx_Util::ValidaCampoSoap($dados["deftam20"], "S", "NU", 1, "0, ;1,X"), // Indicador de prova com tamanho de fonte 20.
                'inTam24' => Fjmx_Util::ValidaCampoSoap($dados["deftam24"], "S", "NU", 1, "0, ;1,X"), // Indicador de prova com tamanho de fonte 24.
            ];

            // 27/10/2017 1.5 Desativação dos parâmetros inGêmeo e inIrmaoRA, inIrmaoDigitoRA e inIrmaoUFRA - Alexandre Mendes
            if (true) {
                $complexType = array_merge($complexType, ['inGemeo' => 'N']);   // Indica se o aluno possui irmão gêmeo.
//                $complexType = array_merge($complexType, ['inIrmaoRA' => '']);    // Número do RA do irmão do aluno
//                $complexType = array_merge($complexType, ['inIrmaoDigitoRA' => '']);    // Digito do RA do irmão do Aluno
//                $complexType = array_merge($complexType, ['inIrmaoUFRA' => '']);    // UF do RA do irmão Aluno
//                $complexType = array_merge($complexType, ['inUF' => 'SP']); //Fjmx_Util::ValidaCampoSoap($endereco['inUF'], "S", "AL", 2) ]);        // Unidade Federetiva do endereço.
            }

            // Requeridos durante o teste
            if (false) { 
                $complexType = array_merge($complexType, ['inCep' => Fjmx_Util::ValidaCampoSoap($endereco['inCep'], "S", "AL", 8, '-,')]);   // CEP do aluno.
                $complexType = array_merge($complexType, ['inCidade' => Fjmx_Util::ValidaCampoSoap($endereco['inCidade'], "S", "AL", 22)]);        // Cidade.
                $complexType = array_merge($complexType, ['inUF' => Fjmx_Util::ValidaCampoSoap($endereco['inUF'], "S", "AL", 2)]);        // Unidade Federetiva do endereço.
                $complexType = array_merge($complexType, ['inLogradouro' => Fjmx_Util::ValidaCampoSoap($endereco['inLogradouro'], "N", "AL", 60)]);       // Rua do aluno
                $complexType = array_merge($complexType, ['inNumero' => Fjmx_Util::ValidaCampoSoap($endereco['inNumero'], "N", "AL", 6)]);        // Número da residência
                $complexType = array_merge($complexType, ['inComplemento' => Fjmx_Util::ValidaCampoSoap($endereco['inComplemento'], "N", "AL", 13)]);       // Complemento de endereço
                $complexType = array_merge($complexType, ['inBairro' => Fjmx_Util::ValidaCampoSoap($endereco['inBairro'], "N", "AL", 30)]);       // Bairro
                $complexType = array_merge($complexType, ['inTipoLogradouro' => Fjmx_Util::ValidaCampoSoap($endereco['inTipoLogradouro'], "N", "AL", 1)]);        // Tipo de logradouro. Deve ser preenchido com 0 ou 1 (0 - URBANO e 1- RURAL
                $complexType = array_merge($complexType, ['inDDD' => Fjmx_Util::ValidaCampoSoap($endereco['inDDD'], "N", "AL", 2)]);        // DDD do telefone
                $complexType = array_merge($complexType, ['inFoneResidencial' => Fjmx_Util::ValidaCampoSoap($endereco['inFoneResidencial'], "N", "AL", 9)]);        // Telefone residencial
                $complexType = array_merge($complexType, ['inFoneRecados' => Fjmx_Util::ValidaCampoSoap($endereco['inFoneRecados'], "N", "AL", 9)]);        // Telefone de recados
                $complexType = array_merge($complexType, ['inNomeFoneRecado' => Fjmx_Util::ValidaCampoSoap($endereco['inNomeFoneRecado'], "N", "AL", 10)]);       // Contato do recado
                $complexType = array_merge($complexType, ['inSMS' => Fjmx_Util::ValidaCampoSoap($endereco['inSMS'], "N", "AL", 1)]);        // Torna-se obrigatório em conjunto com os campos inDDDCel e inFoneCel
                $complexType = array_merge($complexType, ['inDDDCel' => Fjmx_Util::ValidaCampoSoap($endereco['inDDDCel'], "N", "AL", 2)]);        // Torna-se
                $complexType = array_merge($complexType, ['inFoneCel' => Fjmx_Util::ValidaCampoSoap($endereco['inFoneCel'], "N", "AL", 9)]);        // Torna-se obrigatório em conjunto com os campos inSMS e inDDDCel. Telefone celular, apenas números, ex: 999999999.
                $complexType = array_merge($complexType, ['inLatitude' => Fjmx_Util::ValidaCampoSoap($endereco['inLatitude'], "N", "AL", 17)]);       // Coordenada de latitude do endereço residencial
                $complexType = array_merge($complexType, ['inLongitude' => Fjmx_Util::ValidaCampoSoap($endereco['inLongitude'], "N", "AL", 17)]);       // Coordenada de longitude do endereço residencial
                $complexType = array_merge($complexType, ['endIndicativoinBairro' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoInBairro'], "N", "AL", 30)]);       // Bairro
                $complexType = array_merge($complexType, ['endIndicativoinCidade' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinCidade'], "S", "AL", 22)]);        // Cidade.
                $complexType = array_merge($complexType, ['endIndicativoinLatitude' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinLatitude'], "N", "AL", 17)]);       // Coordenada de latitude do endereço residencial
                $complexType = array_merge($complexType, ['endIndicativoinLongitude' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinLongitude'], "N", "AL", 17)]);       // Coordenada de longitude do endereço residencial
                $complexType = array_merge($complexType, ['endIndicativoinLogradouro' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinLogradouro'], "N", "AL", 60)]);       // Rua do aluno
                $complexType = array_merge($complexType, ['endIndicativoinNumero' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinNumero'], "N", "AL", 6)]);        // Número da residência
                $complexType = array_merge($complexType, ['endIndicativoinUF' => Fjmx_Util::ValidaCampoSoap($endereco['endIndicativoinUF'], "S", "AL", 2)]);        // Unidade Federetiva do endereço.
            } else {
                SED::AlterarEnderecoFichaAluno($dados, 'Alterar');
                SED::AlterarDocumentosFichaAluno($dados, 'Alterar');
            }

            $soapstatus = Fjmx_Util::getSOAP($soaptipo.__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], 'Escolasalunos', $soapstatus, $soaptipo, $soaptipo.__FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC12 - SEESP - Integracao - Alterar Documentos Ficha Aluno.pdf / Versão 1.7
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function AlterarDocumentosFichaAluno($dados) {
        $soapstatus = '';
        $soaptipo = 'Alterar';
        $tipocertidao = (isset($dados['tipocertidao'])) ? $dados['tipocertidao'] : '';
        
        $Aluno = Escolasalunos::getEscolasalunosHelper($dados['id']);

        if (!$Aluno) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($Aluno) ? '' : '<br>' . 'Não existe Aluno.');

            Fjmx_Util::setStatusSED($dados["id"], Escolasalunos, $soapstatus, $soaptipo, __FUNCTION__);

            return false;
        }
        
        if ($dados) {
            $complexType = [
                'inRA'              => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12),     // Número do RA do aluno
                'inDigitoRA'        => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2),   // Digito do RA do Aluno
                'inUFRA'            => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),    // UF do RA do Aluno
                //'inNacionalidade'   => Fjmx_Util::ValidaCampoSoap($dados["nacionalidadetipo"], "S", "AL", 1),        // Nacionalidade do aluno (1 – Brasileira, 2 – Estrangeiro e 3 – Brasileiro nascido no exterior) 
                //'inPaisOrigem'      => Fjmx_Util::ValidaCampoSoap($dados["nacionalidade"], "S", "AL", 40),       // Código ou Nome do país do origem do aluno estrangeiro (item 2.1.1)
                'inDiaEntBrasil'    => Fjmx_Util::ValidaCampoSoap(substr($dados["nacionalidadeentradadata"], 8, 2), "S", "AL", 2),        // Dia de entrada no Brasil para alunos estrangeiros
                'inMesEntBrasil'    => Fjmx_Util::ValidaCampoSoap(substr($dados["nacionalidadeentradadata"], 5, 2), "S", "AL", 2),        // Mês de entrada no Brasil para alunos estrangeiros
                'inAnoEntBrasil'    => Fjmx_Util::ValidaCampoSoap(substr($dados["nacionalidadeentradadata"], 0, 4), "S", "AL", 4),        // Ano de entrada no Brasil para alunos estrangeiros
                'inRGRNE01'         => Fjmx_Util::ValidaCampoSoap($dados["rg"], "S", "AL", 14),       // Número do Registro Civil (R.G. ou R.N.E)
                'inRGRNE02'         => Fjmx_Util::ValidaCampoSoap($dados["digrg"], "S", "AL", 2),        // Dígito do Registro Civil (R.G.)    
                'inRGRNE03'         => Fjmx_Util::ValidaCampoSoap($dados["ufrg"], "S", "AL", 2),        // Estado do Registro Civil (R.G)
                'inDiaEmissao'      => Fjmx_Util::ValidaCampoSoap(substr($dados["inDataEmissaoRGRNE"], 8, 2), "S", "AL", 2),        // Dia de emissão do RG
                'inMesEmissao'      => Fjmx_Util::ValidaCampoSoap(substr($dados["inDataEmissaoRGRNE"], 5, 2), "S", "AL", 2),        // Mês de emissão do RG
                'inAnoEmissao'      => Fjmx_Util::ValidaCampoSoap(substr($dados["inDataEmissaoRGRNE"], 0, 4), "S", "AL", 4),        // Ano de emissão do RG
                'inNumNIS'          => Fjmx_Util::ValidaCampoSoap($dados["nis"], "S", "AL", 11),       // Número de identificação social
                'inMunicipioNasc'   => Fjmx_Util::ValidaCampoSoap($dados["nascimentomunicipio"], "S", "AL", 22),       // Nome ou código IBGE do município de nascimento do aluno
                'inUFNasc'          => Fjmx_Util::ValidaCampoSoap($dados["nascimentouf"], "S", "AL", 2),        // Estado de nascimento do aluno.   
                'inCPF'             => Fjmx_Util::ValidaCampoSoap($dados["cpfcnpj"], "S", "NU", 11)        // Número de CPF do aluno.
            ];

            if ($tipocertidao==2) {
                // Opção 1 – Certidão Nova
                $complexType = array_merge($complexType, ['inCertMatr01' => Fjmx_Util::ValidaCampoSoap($dados["nova_cert_cod_nasc_serv"], "S", "AL", 6)]);                        // Código do cartório
                $complexType = array_merge($complexType, ['inCertMatr02' => Fjmx_Util::ValidaCampoSoap($dados["nova_cert_cod_acervo"], "S", "AL", 2)]);                           // Código do acervo
                $complexType = array_merge($complexType, ['inCertMatr03' => Fjmx_Util::ValidaCampoSoap($dados["nova_cert_cod_serv_reg_civil"], "S", "AL", 2)]);                   // Serviço de registro civil das pessoas naturais
                $complexType = array_merge($complexType, ['inCertMatr04' => Fjmx_Util::ValidaCampoSoap($dados["nova_cert_ano_reg_nasc"], "S", "AL", 4)]);                         // Ano do registro    
                $complexType = array_merge($complexType, ['inCertMatr05' => Fjmx_Util::ValidaCampoSoap($dados["nova_cert_tipo_livro_reg"], "S", "AL", 1)]);                       // Tipo do Livro
                $complexType = array_merge($complexType, ['inCertMatr06' => Fjmx_Util::ValidaCampoSoap($dados["nova_cert_num_livro"], "S", "AL", 5)]);                            // Número do Livro
                $complexType = array_merge($complexType, ['inCertMatr07' => Fjmx_Util::ValidaCampoSoap($dados["nova_cert_num_folha"], "S", "AL", 3)]);                            // Número da Folha
                $complexType = array_merge($complexType, ['inCertMatr08' => Fjmx_Util::ValidaCampoSoap($dados["nova_cert_num_termo_reg"], "S", "AL", 7)]);                        // Número do Termo
                $complexType = array_merge($complexType, ['inCertMatr09' => Fjmx_Util::ValidaCampoSoap($dados["digver_certidao_nasc_nova"], "S", "AL", 2)]);                      // Dígito de Controle                
                $complexType = array_merge($complexType, ['inDiaEmissCertMatr' => Fjmx_Util::ValidaCampoSoap(substr($dados["data_emis_certidao_nasc"], 8, 2), "S", "AL", 2)]);    // Dia de emissão da Certidão Nova de Nascimento (formato “dd”)
                $complexType = array_merge($complexType, ['inMesEmissCertMatr' => Fjmx_Util::ValidaCampoSoap(substr($dados["data_emis_certidao_nasc"], 5, 2), "S", "AL", 2)]);    // Mês da Emissão da Certidão Nova de Nascimento (formato “mm”)
                $complexType = array_merge($complexType, ['inAnoEmissCertMatr' => Fjmx_Util::ValidaCampoSoap(substr($dados["data_emis_certidao_nasc"], 0, 4), "S", "AL", 4)]);    // Ano de emissão da Certidão Nova de Nascimento (formato “aaaa”)
                
            } else {               
                // Opção 2– Certidão Antiga
                $complexType = array_merge($complexType, ['inDiaEmisCertidao' => Fjmx_Util::ValidaCampoSoap(substr($dados["data_emis_certidao_nasc"], 8, 2), "S", "AL", 2)]);     // Dia de emissão da Certidão de Nascimento (formato “dd”)
                $complexType = array_merge($complexType, ['inMesEmisCertidao' => Fjmx_Util::ValidaCampoSoap(substr($dados["data_emis_certidao_nasc"], 5, 2), "S", "AL", 2)]);     // Mês da Emissão da Certidão de Nascimento (formato “mm”)
                $complexType = array_merge($complexType, ['inAnoEmisCertidao' => Fjmx_Util::ValidaCampoSoap(substr($dados["data_emis_certidao_nasc"], 0, 4), "S", "AL", 4)]);     // Ano de emissão da Certidão de Nascimento (formato “aaaa”)
                $complexType = array_merge($complexType, ['inDistritoCertidao' => Fjmx_Util::ValidaCampoSoap($dados["inDistritoCertidao"], "S", "AL", 22)]);                      // Distrito da Certidão de Nascimento
                $complexType = array_merge($complexType, ['inFolha' => Fjmx_Util::ValidaCampoSoap($dados["num_folha_certidao_nasc"], "S", "AL", 4)]);                             // Número da folha do Registro de Nascimento do aluno
                $complexType = array_merge($complexType, ['inLivro' => Fjmx_Util::ValidaCampoSoap($dados["num_livro_certidao_nasc"], "S", "AL", 4)]);                             // Número do livro de Registro de Nascimento
                $complexType = array_merge($complexType, ['inMunicipioComarca' => Fjmx_Util::ValidaCampoSoap($dados["inMunicipioComarca"], "S", "AL", 22)]);                      // Nome ou código IBGE do Município Comarca do Registro de Nascimento
                $complexType = array_merge($complexType, ['inNumCertidao' => Fjmx_Util::ValidaCampoSoap($dados["numero_certidao_nasc"], "S", "AL", 6)]);                          // Número da certidão
                $complexType = array_merge($complexType, ['inUFComarca' => Fjmx_Util::ValidaCampoSoap($dados["inUFComarca"], "S", "AL", 2)]);                                     // UF da Comarca do Registro de Nascimento

            }

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], Escolasalunos, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC13 - SEESP - Integracao - Trocar Aluno Classe RA.pdf / Versão 1.4
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function TrocarAlunoDeClassePorRA($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inCodEscola'           => Fjmx_Util::ValidaCampoSoap($dados["inCodEscola"], "N", "NU", 7),         // Código CIE da escola.
                'inRA'                  => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12),                 // Número do RA do aluno.
                'inDigitoRA'            => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2),               // Digito do RA do Aluno
                'inUFRA'                => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),                // UF do RA do Aluno
                'inTipoEnsino'          => Fjmx_Util::ValidaCampoSoap($dados["inTipoEnsino"], "S", "NU", 2),        // Tipo de Ensino da classe de destino
                'inSerieAno'            => Fjmx_Util::ValidaCampoSoap($dados["inSerieAno"], "S", "NU", 2),          // Série / Ano da classe de destino
                'inNumAluno'            => Fjmx_Util::ValidaCampoSoap($dados["inNumAluno"], "S", "NU", 7),          // Número de chamada do aluno na classe de destino
                'inNumClasseOrigem'     => Fjmx_Util::ValidaCampoSoap($dados["inNumClasse"], "S", "NU", 9),         // Número de classe origem onde o aluno está matriculado.
                'inNumClasseDestino'    => Fjmx_Util::ValidaCampoSoap($dados["inNumClasseDestino"], "S", "NU", 9),  // Número de classe de destino da matricula do aluno.
                'inAno'                 => Fjmx_Util::ValidaCampoSoap($dados["inAno"], "S", "NU", 4),               // Ano letivo da classe
                'inDiaTroca'            => Fjmx_Util::ValidaCampoSoap($dados["inDiaTroca"], "S", "NU", 2),          // Dia da efetivação da troca (Ex. 01, 07, 21)
                'inMesTroca'            => Fjmx_Util::ValidaCampoSoap($dados["inMesTroca"], "S", "NU", 2),          // Mês da efetivação da troca (Ex. 03, 09, 11)
                'inAnoTroca'            => Fjmx_Util::ValidaCampoSoap($dados["inAnoTroca"], "S", "NU", 4),          // Ano da efetivação da troca.
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC14 - SEESP - Integracao - Baixar Matricula Transferencia.pdf / Versão 1.3
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function BaixarMatriculaTransferencia($dados) {

        $soapstatus = '';
        $soaptipo = 'Excluir';

        if ($dados) {
            $complexType = [
                'inRA'                  => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12),                 // Número do RA do aluno
                'inDigitoRA'            => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2),               // Digito do RA do Aluno
                'inUF'                  => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),                // UF do RA do Aluno
                'inNumClasse'           => Fjmx_Util::ValidaCampoSoap($dados["inNumClasse"], "S", "NU", 9),         // Numero da classe que terá a matricula encerrada pela baixa. (formato 999999999)
                'inDiaTransferencia'    => Fjmx_Util::ValidaCampoSoap($dados["inDiaTransferencia"], "S", "AL", 2),  // Dia da baixa (formato “dd”)
                'inMesTransferencia'    => Fjmx_Util::ValidaCampoSoap($dados["inMesTransferencia"], "S", "AL", 2),  // Mês da baixa (formato “mm”)
                'inMotivo'              => Fjmx_Util::ValidaCampoSoap($dados["inMotivo"], "S", "AL", 2)             // Motivo da baixa, mencionados no item 2.1
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType);
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC15 - SEESP - Integracao - Estornar Baixa Matricula Transferencia.pdf / Versão 1.3
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function EstornarBaixaMatTransferencia($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA'          => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12),     // Número do RA do aluno
                'inDigitoRA'    => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2),   // Digito do RA do Aluno
                'inUFRA'        => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),    // UF do RA do Aluno
                'inNumClasse'   => Fjmx_Util::ValidaCampoSoap($dados["classe"], "S", "NU", 9)   // Numero da classe que terá a matricula encerrada pela baixa. (formato 999999999) 
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType);
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC17 - SEESP - Integracao - Registrar Abandono.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function RegistrarAbandono($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        $Aluno = Escolasalunos::getEscolaalunoByIdHelper($dados['id']);

        if (!$Aluno) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($Aluno) ? '' : '<br>' . 'Não existe Aluno.');

            Fjmx_Util::setStatusSED($dados["id"], Escolasalunos, $soapstatus, $soaptipo, __FUNCTION__);

            return false;
        }

        if ($dados) {
            $complexType = [
                'inRA'          => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12),                             // Número do RA do aluno
                'inDigitoRA'    => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2),                           // Digito do RA do Aluno
                'inUFRA'        => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),                            // UF do RA do Aluno
                'inDiaAbandono' => Fjmx_Util::ValidaCampoSoap(substr($dados["dataabandono"], 8, 2), "S", "NU", 2),      // Dia do abandono (formato “dd”).
                'inMesAbandono' => Fjmx_Util::ValidaCampoSoap(substr($dados["dataabandono"], 5, 2), "S", "NU", 2),      // Mês do abandono (formato “mm”).
                'inNumClasse'   => Fjmx_Util::ValidaCampoSoap($dados["classe"], "S", "NU", 9)                           // Numero da classe que terá a matricula encerrada pela baixa. (formato 999999999) 
            ];


            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');

            $soapResult = json_decode(json_encode($soapstatus), True);

            Fjmx_Util::setStatusSED($dados["id"], Escolasalunos, $soapstatus, $soaptipo, __FUNCTION__);

            if(isset($soapResult["Mensagens"]["BaixarMatricula"]["outErro"])){
                return $soapResult;
            }else return true;
        }
    }

    /**
     * @see DOC18 - SEESP - Integracao - Estornar Registro Abandono.pdf / Versão 1.5
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function EstornarRegistroAbandono($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        $Aluno = Escolasalunos::getEscolaalunoByIdHelper($dados['id']);

        if (!$Aluno) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($Aluno) ? '' : '<br>' . 'Não existe Aluno.');

            Fjmx_Util::setStatusSED($dados["id"], Escolasalunos, $soapstatus, $soaptipo, __FUNCTION__);

            return false;
        }

        if ($dados) {
            $complexType = [
                'inRA'          => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12),     // Número do RA do aluno
                'inDigitoRA'    => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2),   // Digito do RA do Aluno
                'inUFRA'        => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),    // UF do RA do Aluno
                'inNumClasse'   => Fjmx_Util::ValidaCampoSoap($dados["classe"], "S", "NU", 9)   // Numero da classe que terá a matricula encerrada pela baixa. (formato 999999999) 
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            $soapResult = json_decode(json_encode($soapstatus), True);

            Fjmx_Util::setStatusSED($dados["id"], Escolasalunos, $soapstatus, $soaptipo, __FUNCTION__);            

            if(isset($soapResult["Mensagens"]["EstornarRegAbandono"]["outErro"])){
                return $soapResult;
            }else return true;
        }
    }

    /**
     * @see DOC19 - SEESP - Integracao - Baixar Matrícula Falecimento RA.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function BaixarMatrFalecimentoRA($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA'              => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12),     // Número do RA do aluno
                'inDigitoRA'        => Fjmx_Util::ValidaCampoSoap($dados["digra"], "N", "AL", 2),   // Digito do RA do Aluno
                'inUFRA'            => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),    // UF do RA do Aluno
                'inDiaFalecimento'  => Fjmx_Util::ValidaCampoSoap(substr($dados["datafalecimento"], 8, 2), "S", "NU", 2),        // Dia do falecimento (formato “dd”).
                'inMesFalecimento'  => Fjmx_Util::ValidaCampoSoap(substr($dados["datafalecimento"], 5, 2), "S", "NU", 2),        // Mês do falecimento (formato “mm”).
                'inAnoFalecimento'  => Fjmx_Util::ValidaCampoSoap(substr($dados["datafalecimento"], 0, 4), "S", "NU", 4)        // Ano do falecimento (formato “aaaa”).
            ];


            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            $soapResult = json_decode(json_encode($soapstatus), True);
                        
            Fjmx_Util::setStatusSED($dados["id"], Escolasalunos, $soapstatus, $soaptipo, __FUNCTION__);
            
            if(isset($soapResult["Mensagens"]["BaixarMatricula"]["outErro"])){
                return $soapResult;
            }else return true;


        }
    }

    /**
     * @see DOC20 - SEESP - Integracao - Remanejar Matricula por RA.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function RemanejarMatriculaporRA($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC21 - SEESP - Integracao - Reclassificar Matrículas.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ReclassificarMatriculas($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC22 - SEESP - Integracao - Registrar Nao Comparecimento.pdf / Versão: 1.2
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function RegistrarNaoComparecimento($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2), // UF do RA do Aluno
                'inNumClasse' => Fjmx_Util::ValidaCampoSoap($dados["classe"], "S", "AL", 9)       // Numero da classe que terá a matricula encerrada pela baixa. (formato 999999999)
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            $soapResult = json_decode(json_encode($soapstatus), True);
            
            Fjmx_Util::setStatusSED($dados["id"], Escolasalunos, $soapstatus, $soaptipo, __FUNCTION__);

            if(isset($soapResult["Mensagens"]["BaixarMatricula"]["outErro"])){
                return $soapResult;
            }else return true;
        }
    }

    /**
     * @see DOC23 - SEESP - Integracao - Excluir Matricula.pdf / Versão: 1.2
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ExcluirMatricula($dados) {

        $soapstatus = '';
        $soaptipo = 'Excluir';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),        // UF do RA do Aluno
                'inSerieAno' => Fjmx_Util::ValidaCampoSoap($serie['serie'], "N", "AL", 2), // Série / Ano / Termo do Tipo de Ensino
                'inTipoEnsino' => Fjmx_Util::ValidaCampoSoap($gdae['codigo'], "S", "NU", 2), // Tipo de ensino da classe (item 2.2.1)
                'inNumClasse' => Fjmx_Util::ValidaCampoSoap($dados['classe'], "S", "NU", 9), // Numero da CLasse
                'inAno' => Fjmx_Util::ValidaCampoSoap($dados['ano'], "S", "NU", 4) // Ano letivo o qual a alteração de classe se aplica.
                
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], Matriculas, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC24 - SEESP - Integracao - Consultar Matrículas RA.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ConsultarMatrículasRA($dados) {

        $soapstatus = '';
        $soaptipo = 'Consultar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC25 - SEESP - Integracao - Consultar Matrícula Classe RA.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ConsultarMatriculaClasseRA($dados) {

        $soapstatus = '';
        $soaptipo = 'Consultar';

        if ($dados) {
            $complexType = [
                'inRA'          => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12),     // Número do RA do aluno
                'inDigitoRA'    => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2),   // Digito do RA do Aluno
                'inUFRA'        => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),    // UF do RA do Aluno
                'inNumClasse'   => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "NU", 9),    // Número da classe ou string do tipo “NAOCOMPAR”, indicando que o aluno não compareceu.
                'inSituacao'    => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "N", "AL", 2)     // Situação da matricula do aluno na classe, deve ser VAZIO quando a matricula do aluno estiver ativa, demais situações no item 2.1.1
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC26 - SEESP - Integracao - Consultar Escola CIE.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ConsultarEscolaPorCIE($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inCodEscola' => Fjmx_Util::ValidaCampoSoap($dados["inCodEscola"], "S", "NU", 7) // Código CIE da escola. (9999999)
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC27 - SEESP - Integracao - Consultar Formacao Classe.pdf / Versao 1.3
     * 
     * A finalidade desse serviço é a consulta de alunos matriculados na classe, independente da situação da
     * matricula, seguindo as regras do Sistema de Cadastro de Alunos e Secretaria Digital. A consulta será
     * feita a partir do numero da classe e o retorno é uma relação com RA do aluno, nome do aluno, numero
     * do aluno na classe, situação da matricula, além de dados da classe.
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ConsultaFormacaoClasse($dados) {
        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [                
                'inNumClasse' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "NU", 9)// Número da classe. (999999999)
            ];
           
            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');            
            Fjmx_Util::setStatusSED($dados["ufra"], Escolasvinculos, $soapstatus, $soaptipo, __FUNCTION__);

            $soapResult = json_decode(json_encode($soapstatus), True);

            return $soapResult;
            
        }else{ return false; }
    }

    /**
     * @see DOC36 - SEESP - Integracao - Excluir Coleta Classe.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ExcluirColetaClasse($dados) {

        $soapstatus = '';
        $soaptipo = 'Excluir';

        if ($dados) {

            $complexType = [
                'inAno' => Fjmx_Util::ValidaCampoSoap($dados['ano'], "S", "NU", 4), // Número da classe que será excluída.
                'inNumClasse' => Fjmx_Util::ValidaCampoSoap($dados['classe'], "S", "NU", 9) // Ano letivo ao qual a classe pertence.
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], Escolasvinculos, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }

        return false;
    }

    /**
     * @see DOC37 - SEESP - Integracao - Associar Irmao.pdf / Versão 1.2
     * 
     * @access public
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     * @return boolean Retorna verdadeiro ou falso para a execução.
     */


    public static function AssociarIrmao($dados) {
        $soapstatus = '';
        $soaptipo = 'Incluir';

        if ($dados) {
            $complexType = [
                'inRA'              => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12),             // Número do RA do aluno
                'inDigitoRA'        => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2),           // Digito do RA do Aluno
                'inUFRA'            => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),            // UF do RA do Aluno
                'inGemeo'           => Fjmx_Util::ValidaCampoSoap($dados["inGemeo"], "N", "AL", 1),         // Indica se o aluno possui irmão gêmeo.
                'inIrmaoRA'         => Fjmx_Util::ValidaCampoSoap($dados["inIrmaoRA"], "S", "NU", 12),      // Número do RA do irmão do aluno (999999999999)
                'inIrmaoDigitoRA'   => Fjmx_Util::ValidaCampoSoap($dados["inIrmaoDigitoRA"], "S", "AL", 2), // Digito do RA do irmão do Aluno
                'inIrmaoUFRA'       => Fjmx_Util::ValidaCampoSoap($dados["inIrmaoUFRA"], "S", "AL", 2)      // UF do RA do irmão Aluno
            ];
           
            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            
            $result = json_decode(json_encode($soapstatus), True);
           
            if(isset($result["Mensagens"]["MsgAssociarIrmao"]["outErro"])){
                
                $retorno = $result["Mensagens"]["MsgAssociarIrmao"]["outErro"];
                $processoid = $result["outProcessoID"];
                
                Fjmx_Util::geraLog(date('Y-m-d'), __FUNCTION__, Usuarios::getUsuario('id'), "Operação: '$soaptipo' - Retorno: " . str_replace("<br>", "", $retorno) . " - $processoid.", "E");
                return false;
            }else{
                //salvo com sucesso
                return true;
            }
        }
    }

    /**
     * @see DOC38 - SEESP - Integracao - Consultar Irmao.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     * @return boolean Retorna verdadeiro ou falso para a execução.
     */

    public static function ConsultarIrmao($dados) {

        $soapstatus = '';
        $soaptipo = 'Consultar';

        $Irmao = Escolasalunosassociairmao::getEscolasalunosassociairmaoByIdHelper($dados['id']);

        if (!$Irmao) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($Irmao) ? '' : '<br>' . 'Não existe irmão cadastrado.');

            Fjmx_Util::setStatusSED($dados["id"], Escolasalunosassociairmao, $soapstatus, $soaptipo, __FUNCTION__);

            return false;
        }

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], Escolasalunosassociairmao, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC39 - SEESP - Integracao - Excluir Irmao.pdf / Versão 1.3
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     * @return boolean Retorna verdadeiro ou falso para a execução.
     */

    public static function ExcluirIrmao($dados) {

        $soapstatus = '';
        $soaptipo = 'Excluir';
        $Irmao = Escolasalunosassociairmao::getEscolasalunosassociairmaoByIdHelper($dados["id"]);

        if (!$Irmao) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($Irmao) ? '' : '<br>' . 'Não existe irmão cadastrado.');

            Fjmx_Util::setStatusSED($dados["id"], Escolasalunosassociairmao, $soapstatus, $soaptipo, __FUNCTION__);

            return false;
        }

        if ($dados) {
            $complexType = [
                'inRA'              => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12),             // Número do RA do aluno
                'inDigitoRA'        => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2),           // Digito do RA do Aluno
                'inUFRA'            => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2),            // UF do RA do Aluno
                'inGemeo'           => Fjmx_Util::ValidaCampoSoap($dados["inGemeo"], "N", "AL", 1),         // Indica se o aluno possui irmão gêmeo.
                'inIrmaoRA'         => Fjmx_Util::ValidaCampoSoap($dados["inIrmaoRA"], "S", "NU", 12),      // Número do RA do irmão do aluno (999999999999)
                'inIrmaoDigitoRA'   => Fjmx_Util::ValidaCampoSoap($dados["inIrmaoDigitoRA"], "S", "AL", 2), // Digito do RA do irmão do Aluno
                'inIrmaoUFRA'       => Fjmx_Util::ValidaCampoSoap($dados["inIrmaoUFRA"], "S", "AL", 2)      // UF do RA do irmão Aluno
            ];
           
            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');          

            Fjmx_Util::geraLog(date('Y-m-d'), 'Excluir irmão', Usuarios::getUsuario('id'), "Operação: '$soaptipo' - Retorno: Aluno do RA '$dados[ra]' - '$dados[digra]' - '$dados[ufra]' excluído com sucesso");

            return true;
        }
    }

    /**
     * @see DOC40 - SEESP - Integracao - Realizar Matric Antecipada Fases.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function RealizarMatricAntecipadaFases($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC41 - SEESP - Integracao - Realizar Matric Antecipada RA Fases.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function RealizarMatricAntecipadaRAFases($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        $xxx = XXXX::getXXXXByIdHelper($dados['id']);

        if (!$xxx) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($xxx) ? '' : '<br>' . 'Não existe endereço.');

            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return false;
        }

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC43 - SEESP - Integracao - Cancelar Inscricao e Definicao.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function CancelarInscricaoeDefinicao($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        $xxx = XXXX::getXXXXByIdHelper($dados['id']);

        if (!$xxx) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($xxx) ? '' : '<br>' . 'Não existe endereço.');

            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return false;
        }

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC44 - SEESP - Integracao - Inscrever Aluno Transferência.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function InscreverAlunoTransferencia($dados) {

        $soapstatus = '';
        $soaptipo = 'Incluir';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC45 - SEESP - Integracao - Cancelar Inscricao Aluno Transf.pdf
     * 
     * @access public
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function CancelarInscricaoAlunoTransf($dados) {

        $soapstatus = '';
        $soaptipo = 'Excluir';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC46 - SEESP - Integracao - Inscrever Intencao Transferência.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function InscreverIntencaoTransferencia($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC47 - SEESP - Integracao - Cancelar Intencao Transferência.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function CancelarIntencaoTransferencia($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC48 - SEESP - Integracao - Inscrever Aluno por Deslocamento.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function InscreverAlunoporDeslocamento($dados) {

        $soapstatus = '';
        $soaptipo = 'Incluir';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC49 - SEESP - Integracao - Cancelar Inscricao Aluno por Deslocamento.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function CancelarInscricaoAlunoporDeslocamento($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC52 - SEESP - Integracao - Consultar Quadro Resumo.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ConsultarQuadroResumo($dados) {

        $soapstatus = '';
        $soaptipo = 'Consultar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC53 - SEESP - Integracao - Classificar Gerar Numero Chamada Por Classe .pdf
          * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
*/

    public static function ClassificarGerarNumeroChamadaPorClasse($dados, $tipo) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC54 - SEESP - Integracao - Classificar Gerar Nr Chamada Por Escola .pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ClassificarGerarNrChamadaPorEscola($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        $xxx = XXXX::getXXXXByIdHelper($dados['id']);

        if (!$xxx) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($xxx) ? '' : '<br>' . 'Não existe endereço.');

            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return false;
        }

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC55 - SEESP - Integracao - Consultar Coleta Classe.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ConsultarColetaClasse($dados) {

        $soapstatus = '';
        $soaptipo = 'Consultar';

        if ($dados) {

            $complexType = [
                'inAno'         => Fjmx_Util::ValidaCampoSoap($dados['ano'], "S", "NU", 4),         // Ano letivo o qual a alteração de classe se aplica.
                'inNumClasse'   => Fjmx_Util::ValidaCampoSoap($dados["classe"], "S", "NU", 9)       // Numero da classe que terá a matricula encerrada pela baixa. (formato 999999999) 
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');

            return $soapstatus;
        }
    }

    /**
     * @see DOC56 - SEESP - Integracao - Consultar Cep.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ConsultarCep($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        $xxx = XXXX::getXXXXByIdHelper($dados['id']);

        if (!$xxx) {

            $soapstatus = 'Faltam informações para o processamento...' .
                    (isset($xxx) ? '' : '<br>' . 'Não existe endereço.');

            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return false;
        }

        if ($dados) {
            $complexType = [
                'inCep' => Fjmx_Util::ValidaCampoSoap($dados["inCep"], "S", "NU", 8)    // Número do CEP que será consultado (99999999).
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC60 - SEESP - Integracao - Consultar Rendimento Escolar Classe.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ConsultarRendimentoEscolarClasse($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC61 - SEESP - Integracao - Cancelar Encerramento de Rendimento Escolar.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function CancelarEncerramentodeRendimentoEscolar($dados, $tipo) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC62 - SEESP - Integracao - Consultar Totais Por Escola.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function ConsultarTotaisPorEscola($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC64 - SEESP - Integracao - Lançar Rendimento por Aluno.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function LancarRendimentoporAluno($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

    /**
     * @see DOC65 - SEESP - Integracao - Alterar Rendimento por Aluno.pdf
     * 
     * @access public 
     * 
     * @param Array $dados  - Array com os dados que serão passados para a execução.
     * 
     */

    public static function AlterarRendimentoporAluno($dados) {

        $soapstatus = '';
        $soaptipo = 'Alterar';

        if ($dados) {
            $complexType = [
                'inRA' => Fjmx_Util::ValidaCampoSoap($dados["ra"], "S", "NU", 12), // Número do RA do aluno
                'inDigitoRA' => Fjmx_Util::ValidaCampoSoap($dados["digra"], "S", "AL", 2), // Digito do RA do Aluno
                'inUFRA' => Fjmx_Util::ValidaCampoSoap($dados["ufra"], "S", "AL", 2)        // UF do RA do Aluno
            ];

            $soapstatus = Fjmx_Util::getSOAP(__FUNCTION__, $complexType, 'H');
            Fjmx_Util::setStatusSED($dados["id"], XXXX, $soapstatus, $soaptipo, __FUNCTION__);

            return true;
        }
    }

}

?>
