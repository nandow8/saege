<?php

/**
 * Define o modelo Educacaoespecialrelatoriosdiarios
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Educacaoespecialrelatoriosdiarios extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "educacaoespecialrelatoriosdiarios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEducacaoespecialrelatoriosdiariosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$educacaoespecialrelatoriosdiarios = new Educacaoespecialrelatoriosdiarios();
		return $educacaoespecialrelatoriosdiarios->getEducacaoespecialrelatoriosdiarios($queries, $page, $maxpage);
	}
	
	public function getEducacaoespecialrelatoriosdiarios($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " e1.datacriacao >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " e1.datacriacao <= '$data_f' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " e1.titulo LIKE '%$titulo%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " e1.descricoes = '$descricoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM educacaoespecialrelatoriosdiarios e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEducacaoespecialrelatoriodiarioById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEducacaoespecialrelatoriosdiarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEducacaoespecialrelatoriodiarioByIdHelper($id, $queries = array()) {
		$rows = new Educacaoespecialrelatoriosdiarios();
		return $rows->getEducacaoespecialrelatoriodiarioById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Educacaoespecialrelatoriosdiarios
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
		$row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		$row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
		$row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
		$row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		if (is_null($row->datacriacao)) {
		    $row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
					
		$row->save();
		if (array_key_exists('materias', $dados)) $this->setMaterias($dados['materias'], $row->id);
		return $row;
	}

	private function setMaterias($dados, $idrelatoriodiario) {
		
		if (!is_array($dados)) return;
		 
		$itens = new Educacaoespecialrelatoriodiariolist();
		$datas = $dados['datas'];
		$titulos = $dados['titulos'];
		$evolucoes = $dados['evolucoes'];

		$ids = array();
		foreach ($titulos as $i=>$id) {
			
			$d = array();
			
			$d['idrelatoriodiario'] = $idrelatoriodiario; 
			$d['datas'] = trim(strip_tags($datas[$i]));
			$d['titulos'] = trim(strip_tags($titulos[$i]));       
			$d['evolucoes'] = trim(strip_tags($evolucoes[$i]));       
			$d['logusuario'] = $dados['logusuario'];
			$d['logdata'] = $dados['logdata'];
			
			$_row = $itens->save($d); 		
			array_push($ids, $_row->id);
		}
		$materias = implode(",", $ids);
       
		if ($materias=="") $materias = "0";

		$strsql = "DELETE FROM educacaoespecialrelatoriodiariolist WHERE idrelatoriodiario=$idrelatoriodiario AND id NOT IN ($materias)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);
		
	}
	
}