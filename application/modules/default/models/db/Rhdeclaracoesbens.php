<?php

/**
 * Define o modelo Rhdeclaracoesbens
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Rhdeclaracoesbens extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "rhdeclaracoesbens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getRhdeclaracoesbensHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$rhdeclaracoesbens = new Rhdeclaracoesbens();
		return $rhdeclaracoesbens->getRhdeclaracoesbens($queries, $page, $maxpage);
	}
	
	public function getRhdeclaracoesbens($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");
		
		
		$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " r1.idfuncionario = $idfuncionario ");
		
		$idfuncionarioescola = (isset($queries["idfuncionarioescola"])) ? $queries["idfuncionarioescola"] : false;
		if ($idfuncionarioescola) array_push($where, " r1.idfuncionarioescola = $idfuncionarioescola ");

		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " r1.origem = '$origem' ");

$ref = (isset($queries["ref"])) ? $queries["ref"] : false;
		if ($ref) array_push($where, " r1.ref LIKE '%$ref%' ");

$portarianumero = (isset($queries["portarianumero"])) ? $queries["portarianumero"] : false;
		if ($portarianumero) array_push($where, " r1.portarianumero LIKE '%$portarianumero%' ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " r1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " r1.data <= '$data_f' ");

$bens = (isset($queries["bens"])) ? $queries["bens"] : false;
		if ($bens) array_push($where, " r1.bens = '$bens' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " r1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*, f1.nome, f1.rg, f1.cpf, f1.estadocivil, f1.conjugenome, f1.grauinstrucao, f1.dataadmissao, f1.cargo, f1.idendereco, f1.telefone, f1.celular, f1.salarioinicial, f1.idcargo"; 
		;
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM rhdeclaracoesbens r1
						LEFT JOIN funcionariosgerais f1 ON f1.id = r1.idfuncionario
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getRhdeclaracaobensById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getRhdeclaracoesbens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getRhdeclaracaobensByIdHelper($id, $queries = array()) {
		$rows = new Rhdeclaracoesbens();
		return $rows->getRhdeclaracaobensById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Rhdeclaracoesbens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
 $row->idfuncionarioescola = (array_key_exists("idfuncionarioescola",$dados)) ? $dados["idfuncionarioescola"] : $row->idfuncionarioescola;
 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->ref = (array_key_exists("ref",$dados)) ? $dados["ref"] : $row->ref;
 $row->portarianumero = (array_key_exists("portarianumero",$dados)) ? $dados["portarianumero"] : $row->portarianumero;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->bens = (array_key_exists("bens",$dados)) ? $dados["bens"] : $row->bens;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}