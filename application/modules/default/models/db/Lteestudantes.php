<?php

class Lteestudantes extends Zend_Db_Table_Abstract {

	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "lteestudantes";

	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public function autenticargm($rgm, $senha, $ns = false) {
		$usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");

		if (!$usuario) return "RGM ou senha inválidos!";
		if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
		if ($usuario['senha']!=$senha) return "A senha é inválida!";

		$usuario = $usuario->toArray();

		if (!$ns) $ns = Mn_Util::getAdminNameSpace();
		$loginNameSpace = new Zend_Session_Namespace($ns);

		$escolas = new Escolas();
		$escola = $escolas->getEscolaById($usuario['idescola']);

		unset($escola['id']);
		unset($escola['email']);
		$usuario = array_merge($usuario, $escola);
		$usuario['senha'] = $senha;
		$usuario['tipousuario'] = 'aluno';

		$loginNameSpace->alunousuario = serialize($usuario);

		return true;
	}

	public static function getUsuario($field = null, $ns = false) {
		$ns = (!$ns) ? Mn_Util::getAdminNameSpace() : $ns;

		$session = new Zend_Session_Namespace($ns);
		if (!isset($session->alunousuario)) return false;

		$escola = unserialize($session->alunousuario);
		if (is_null($field)) return $escola;

		return $escola[$field];
	}

	public function getEstudantes($queries = array(), $page = 0, $maxpage = 0) {

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$nome = (isset($queries['nome'])) ? $queries['nome'] : false;
		$filiacao = (isset($queries['filiacao'])) ? $queries['filiacao'] : false;
		$matricula = (isset($queries['matricula'])) ? $queries['matricula'] : false;
		$curso = (isset($queries['curso'])) ? $queries['curso'] : false;
		$subsidio = (isset($queries['subsidio'])) ? $queries['subsidio'] : false;
		$cpf = (isset($queries['cpf'])) ? $queries['cpf'] : false;
		$ra = (isset($queries['ra'])) ? $queries['ra'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$tipoestudante = (isset($queries['tipoestudante'])) ? $queries['tipoestudante'] : false;
		$tipotransporte = (isset($queries['tipotransporte'])) ? $queries['tipotransporte'] : false;
		$entidade = (isset($queries['entidade'])) ? $queries['entidade'] : false;
		$identidade = (isset($queries['identidade'])) ? (int)$queries['identidade'] : false;
		$subsidio = (isset($queries['subsidio'])) ? $queries['subsidio']: false;
		$eja = (isset($queries['eja'])) ? $queries['eja']: false;

		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$where = array();

		if ($id) array_push($where, " le1.id = $id ");
		if ($nome) array_push($where, " le1.nome LIKE '%$nome%' ");
		if ($filiacao) array_push($where, " le1.filiacao LIKE '%$filiacao%' ");
		if ($matricula) array_push($where, " le1.matricula LIKE '%$matricula%' ");
		if ($curso) array_push($where, " le1.curso LIKE '%$curso%' ");
		if ($subsidio) array_push($where, " vch.valorfretado*vch.subsidio/100 = '$subsidio' ");
		if ($cpf) array_push($where, " le1.cpf LIKE '%$cpf%' ");
		if ($ra) array_push($where, " le1.ra LIKE '%$ra%' ");
		if ($status) array_push($where, " le1.status = '$status' ");
		if ($tipoestudante)	array_push($where, " le1.tipoestudante = '$tipoestudante' ");
		if ($tipotransporte) array_push($where, " le1.tipotransporte = '$tipotransporte' ");
		if ($entidade) array_push($where, " le2.entidade LIKE '%$entidade%' ");
		if ($identidade) array_push($where, " le1.identidade = $identidade ");
		if ($subsidio) array_push($where, " le1.subsidio = '$subsidio' ");
		if ($eja) array_push($where, " le1.eja = '$eja' ");

		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "le1.id, le1.nome, le1.nomesocial,le1.nomemae, le1.datanascimento, le1.email, le1.telefone, le1.celular, le1.facebook, le1.endereco, le1.numero, "
                        . "le1.bairro, le1.cidade, le1.idestado, le1.sexo, le1.complemento, le1.rg, le1.ufrg, le1.cpf, le1.tituloeleitor, le1.zonaeleitor, le1.secaoeleitor, "
                        . "le1.nis, le1.ra, le1.identidade, le1.curso, le1.idcurso, le1.serie, le1.periodo, le1.semestre, le1.status, le1.filiacao, le1.matricula, le1.excluido, "
                        . "le1.observacoes, le1.latitude, le1.longitude, le1.tipoestudante, le1.tipotransporte, le1.eja, le1.bolsista, le1.datacriacao, le1.idimagem, le1.idanxrg, "
                        . "le1.idanxcpf, le1.idanxtituloeleitor, le1.idanxcomprovantevotacao, le1.idanxcomprovanteendereco, le1.idanxdeclaracaoendereco, le1.idanxaetusi, "
                        . "le1.idanxmatricula, le1.idanxestudo, le1.idanxcancelamento, le2.entidade, le1.logdata, le1.logusuario, vch.cidade cidade_entidade, vch.valorfretado, vch.subsidio";

		$ordem = "ORDER BY le1.nome";
		if ($order) $ordem = $order;

		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		if ($total) {
			$strsql = "SELECT count(le1.id) as total FROM lteestudantes le1 LEFT JOIN lteentidades le2 ON le1.identidade = le2.id LEFT JOIN view_cidade_historico vch ON le2.idcidade = vch.idcidade WHERE le1.excluido = 'nao' $w";
		} else {
			$strsql = "SELECT $fields FROM lteestudantes le1 LEFT JOIN lteentidades le2 ON le1.identidade = le2.id LEFT JOIN view_cidade_historico vch ON le2.idcidade = vch.idcidade WHERE le1.excluido = 'nao' $w $ordem $limit";
		}

		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

		$db = Zend_Registry::get('db');

		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}

		// var_dump($strsql); die();

		return $db->fetchAll($strsql);
	}

	public function getEstudanteById($id, $queries = array()) {
		if ($id==0) return false;

		$queries['id'] = $id;
		$rows = $this->getEstudantes($queries, 0, 0);

		if (sizeof($rows)==0) return false;
		return $rows[0];
	}

	public static function getEstudanteByIdHelper($id, $queries = array()) {
		$rows = new Lteestudantes();
		return $rows->getEstudanteById($id, $queries);
	}

	public static function getNomeByIdHelper($id, $queries = array()) {
		$rows = new Lteestudantes();
		$usuario = $rows->getEstudanteById($id, $queries);
		if(!$usuario) return false;
		return $usuario['nome'];
	}
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Lteestudante
     */
	public function save($dados) {

		$novoRegistro = true;
		$integracao = false;

		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");

		if (!$row) {
			$row = $this->createRow();
		}else {
			/*
			$novoRegistro = false;
			$historico = new Escolashistoricos_Escolasalunos();
			$_historico = $row->toArray();

			$historico->arquiva($_historico);
			*/
		}

		$row->nome 			 = (array_key_exists('nome',$dados)) ? $dados['nome'] : $row->nome;
		$row->nomesocial 	 = (array_key_exists('nomesocial',$dados)) ? $dados['nomesocial'] : $row->nomesocial;
		$row->nomemae		 = (array_key_exists('nomemae',$dados)) ? $dados['nomemae'] : $row->nomemae;
		$row->sexo 			 = (array_key_exists('sexo',$dados)) ? $dados['sexo'] : $row->sexo;
		$row->datanascimento = (array_key_exists('datanascimento',$dados)) ? $dados['datanascimento'] : $row->datanascimento;
		$row->status 		 = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->cpf 			 = (array_key_exists('cpf',$dados)) ? $dados['cpf'] : $row->cpf;
		$row->rg 			 = (array_key_exists('rg',$dados)) ? $dados['rg'] : $row->rg;
		$row->ufrg 			 = (array_key_exists('ufrg',$dados)) ? $dados['ufrg'] : $row->ufrg;
		$row->ra 			 = (array_key_exists('ra',$dados)) ? $dados['ra'] : $row->ra;
		$row->tituloeleitor  = (array_key_exists('tituloeleitor',$dados)) ? $dados['tituloeleitor'] : $row->tituloeleitor;
		$row->zonaeleitor  	 = (array_key_exists('zonaeleitor',$dados)) ? $dados['zonaeleitor'] : $row->zonaeleitor;
		$row->secaoeleitor   = (array_key_exists('secaoeleitor',$dados)) ? $dados['secaoeleitor'] : $row->secaoeleitor;
		$row->nis   		 = (array_key_exists('nis',$dados)) ? $dados['nis'] : $row->nis;
		$row->email 		 = (array_key_exists('email',$dados)) ? $dados['email'] : $row->email;
		$row->telefone 		 = (array_key_exists('telefone',$dados)) ? $dados['telefone'] : $row->telefone;
		$row->celular 		 = (array_key_exists('celular',$dados)) ? $dados['celular'] : $row->celular;
		$row->facebook 		 = (array_key_exists('facebook',$dados)) ? $dados['facebook'] : $row->facebook;
		// $row->cep 		 = (array_key_exists('cep',$dados)) ? $dados['cep'] : $row->cep;
		$row->endereco 		 = (array_key_exists('endereco',$dados)) ? $dados['endereco'] : $row->endereco;
		$row->numero 		 = (array_key_exists('numero',$dados)) ? $dados['numero'] : $row->numero;
		$row->complemento 	 = (array_key_exists('complemento',$dados)) ? $dados['complemento'] : $row->complemento;
		$row->bairro 		 = (array_key_exists('bairro',$dados)) ? $dados['bairro'] : $row->bairro;
		$row->idestado 		 = (array_key_exists('idestado',$dados)) ? $dados['idestado'] : $row->idestado;
		$row->cidade 		 = (array_key_exists('cidade',$dados)) ? $dados['cidade'] : $row->cidade;
		$row->excluido 		 = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->observacoes 	 = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;
		$row->latitude 		 = (isset($dados['latitude'])) ? $dados['latitude'] : $row->latitude;
		$row->longitude 	 = (isset($dados['longitude'])) ? $dados['longitude'] : $row->longitude;

		$row->tipoestudante	 = (array_key_exists('tipoestudante',$dados)) ? $dados['tipoestudante'] : $row->tipoestudante;
		$row->tipotransporte = (array_key_exists('tipotransporte',$dados)) ? $dados['tipotransporte'] : $row->tipotransporte;

		$row->identidade 	 = (array_key_exists('identidade',$dados)) ? $dados['identidade'] : $row->identidade;
		$row->curso 		 = (array_key_exists('curso',$dados)) ? $dados['curso'] : $row->curso;
		$row->idcurso 		 = (array_key_exists('idcurso',$dados)) ? $dados['idcurso'] : $row->idcurso;
		$row->semestre 		 = (array_key_exists('semestre',$dados)) ? $dados['semestre'] : $row->semestre;
		$row->periodo 		 = (array_key_exists('periodo',$dados)) ? $dados['periodo'] : $row->periodo;
		$row->subsidio 		 = (array_key_exists('subsidio',$dados)) ? $dados['subsidio'] : $row->subsidio;

		$row->filiacao 		 = (array_key_exists('filiacao',$dados)) ? $dados['filiacao'] : $row->filiacao;
		$row->matricula 		 = (array_key_exists('matricula',$dados)) ? $dados['matricula'] : $row->matricula;
		$row->eja	 		 = (array_key_exists('eja',$dados)) ? $dados['eja'] : $row->eja;
		$row->bolsista 		 = (array_key_exists('bolsista',$dados)) ? $dados['bolsista'] : $row->bolsista;

		$row->serie 		 = (array_key_exists('serie',$dados)) ? $dados['serie'] : $row->serie;

		$row->idimagem 	 	 = (array_key_exists('idimagem',$dados)) ? $dados['idimagem'] : $row->idimagem;

		$row->idanxrg 	 	 = (array_key_exists('idanxrg',$dados)) ? $dados['idanxrg'] : $row->idanxrg;
		$row->idanxcpf 	 	 = (array_key_exists('idanxcpf',$dados)) ? $dados['idanxcpf'] : $row->idanxcpf;
		$row->idanxtituloeleitor = (array_key_exists('idanxtituloeleitor',$dados)) ? $dados['idanxtituloeleitor'] : $row->idanxtituloeleitor;
		$row->idanxcomprovantevotacao = (array_key_exists('idanxcomprovantevotacao',$dados)) ? $dados['idanxcomprovantevotacao'] : $row->idanxcomprovantevotacao;
		$row->idanxcomprovanteendereco = (array_key_exists('idanxcomprovanteendereco',$dados)) ? $dados['idanxcomprovanteendereco'] : $row->idanxcomprovanteendereco;
		$row->idanxdeclaracaoendereco = (array_key_exists('idanxdeclaracaoendereco',$dados)) ? $dados['idanxdeclaracaoendereco'] : $row->idanxdeclaracaoendereco;
		$row->idanxaetusi = (array_key_exists('idanxaetusi',$dados)) ? $dados['idanxaetusi'] : $row->idanxaetusi;
		$row->idanxmatricula = (array_key_exists('idanxmatricula',$dados)) ? $dados['idanxmatricula'] : $row->idanxmatricula;
		$row->idanxestudo = (array_key_exists('idanxestudo',$dados)) ? $dados['idanxestudo'] : $row->idanxdeclaracaoestudo;

		$row->idanxcancelamento = (array_key_exists('idanxcancelamento',$dados)) ? $dados['idanxcancelamento'] : $row->idanxcancelamento;

                if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
                
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;

		$row->save();
                
		if($id===0){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::incluir", Usuarios::getUsuario("id"), "Estudante adicionado com sucesso!");
        }elseif($id!== 0 AND isset($row->excluido) AND $row->excluido==="sim"){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::excluir", Usuarios::getUsuario("id"), "Estudante de ID ".$id." excluído com sucesso!");
        } else Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::atualizar", Usuarios::getUsuario("id"), "Estudante de ID ".$id." atualizado com sucesso!");                
		
		return $row;
	}

	public function generateRandomString($length = 8) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}
}
