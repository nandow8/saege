<?php

/**
 * Define o modelo PostsVideos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Action
 * @copyright   Copyright (c) 2014 MN Solu��es (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class PostsVideos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "postsvideos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";		
	
	public function swapOrdem($idpost, $ordem, $d, $inverte) {
		if ($ordem > 0) {
			if ($inverte) $d = ($d=="up") ? "down" : "up"; //Inverte os Sentidos
			if ($d=="up") {
				$strsql = "SELECT * FROM postsvideos WHERE excluido='nao' AND idpost=$idpost AND ordem <= ".$ordem." ORDER BY ordem DESC LIMIT 2";
			} else {
				$strsql = "SELECT * FROM postsvideos WHERE excluido='nao' AND idpost=$idpost AND ordem >= ".$ordem." ORDER BY ordem LIMIT 2";
			}
		
			$db = Zend_Registry::get("db");
			$rows = $db->fetchAll($strsql);
			if (sizeof($rows)==2) {
				$db->beginTransaction();
				try {
					$strsql = "UPDATE postsvideos SET ordem = " . $rows[1]["ordem"] . 
						" WHERE id =" . $rows[0]["id"];
					$db->query($strsql);
				
					$strsql = "UPDATE postsvideos SET ordem = " . $rows[0]["ordem"] . 
						" WHERE id =" . $rows[1]["id"];
					$db->query($strsql);
					$db->commit();
				} catch (Exception $e) {
					$db->rollBack();
					echo $e->getMessage();
				}
			}
		}			
	}	

	public static function getPostsVideos($idpost = NULL, $status = NULL, $inverte = false, $limite = NULL) {
		$db = Zend_Registry::get('db');
		
		$w = "";
		if (!is_null($idpost)) $w .= " AND pv1.idpost = " . (int)$idpost;
		if (!is_null($status)) $w .= " AND pv1.status = '$status'"; 
		
		$l = "";
		if (!is_null($limite)) $l .= " LIMIT '$limite'";
		
		$order = "ORDER BY pv1.ordem";
		if ($inverte) $order .= " DESC";
		
		$strsql = "SELECT pv1.* 
					FROM postsvideos pv1 
					WHERE pv1.excluido = 'nao' 
					$w
					$order
					$l";			
					
		return $db->fetchAll($strsql);
	}	
	
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 

		if ($id>0)$row->id = $id;
		$row->idpost = (isset($dados['idpost'])) ? $dados['idpost'] : $row->idpost;
		$row->legenda = (isset($dados['legenda'])) ? $dados['legenda'] : $row->legenda;	
		$row->texto = (isset($dados['texto'])) ? $dados['texto'] : $row->texto;			
		$row->video = (isset($dados['video'])) ? $dados['video'] : $row->video;		
		$row->status = (isset($dados['status'])) ? $dados['status'] : $row->status;
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;
		$row->ordem = (isset($dados['ordem'])) ? $dados['ordem'] : $row->ordem;
		$row->save();
		
		if ($novoRegistro) {
			$row->ordem = $row->id;
			$row->save();			
		}
		
		return $row;
	}		
	
}