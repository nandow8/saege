<?php

/**
 * Define o modelo Avaliacoesalunosrelacoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Avaliacoesalunosrelacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "avaliacoesalunosrelacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAvaliacoesalunosrelacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$avaliacoesalunosrelacoes = new Avaliacoesalunosrelacoes();
		return $avaliacoesalunosrelacoes->getAvaliacoesalunosrelacoes($queries, $page, $maxpage);
	}
	
	public function getAvaliacoesalunosrelacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$idavaliacao = (isset($queries['idavaliacao'])) ? (int)$queries['idavaliacao'] : false;
		if ($idavaliacao) array_push($where, " a1.idavaliacao = " . $idavaliacao);

		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		if( $idaluno ) array_push($where, " a1.idaluno = " . $idaluno);

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='idavaliacao') $sorting[0]='a1.idavaliacao';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*";
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM avaliacoesalunosrelacoes a1 WHERE excluido='nao' 
				
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAvaliacoesalunosrelacoesById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idavaliacao'] = $id;
		$rows = $this->getAvaliacoesalunosrelacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAvaliacoesalunosrelacoesByIdHelper($id, $queries = array()) {
		$rows = new Avaliacoesalunosrelacoes();
		return $rows->getAvaliacoesalunosrelacoesById($id, $queries);
	}		

	/**
     * Altera todas as relacao de avaliação-alunos para excluído->sim
     * @param array idavaliacao
     * @return Avaliacoesalunosrelacoes
     */
	public function setExcluido($idavaliacao) {
		
		$row = $this->fetchAll("idavaliacao=$idavaliacao");
		
		if (!$row) $row = $this->createRow();
		
		foreach ($row as $k => $v) {

			$v->excluido = 'sim';	
				
			$v->save();
		}
		
		return $row;
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Avaliacoesalunosrelacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$idavaliacao = (!isset($dados['idavaliacao'])) ? 0 : (int)$dados['idavaliacao'];
		$idaluno = (!isset($dados['idaluno']) ? 0 : (int)$dados['idaluno']);
		$notas = (!isset($dados['notas'])) ? 0 : $dados['notas'];
		$media = (!isset($dados['media']) ? 0 : (float)$dados['media']);
		$row = $this->fetchRow("idavaliacao=$idavaliacao AND idaluno=$idaluno");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		
		$row->idavaliacao = (array_key_exists("idavaliacao",$dados)) ? $dados["idavaliacao"] : $row->idavaliacao;
		$row->idaluno = (array_key_exists("idaluno", $dados)) ? $dados['idaluno'] : $row->idaluno;
		$row->notas = (array_key_exists("notas", $dados)) ? $dados['notas'] : $row->notas;
		$row->media = (array_key_exists("media", $dados)) ? $dados['media'] : $row->media;
		$row->excluido = (array_key_exists('excluido', $dados)) ? $dados['excluido'] : $row->excluido;	
				
		$row->save();
		
		return $row;
	}
}