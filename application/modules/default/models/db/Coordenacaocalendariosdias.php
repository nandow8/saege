<?php

/**
 * Define o modelo Coordenacaocalendariosdias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Coordenacaocalendariosdias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "coordenacaocalendariosdias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCoordenacaocalendariosdiasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$coordenacaocalendariosdias = new Coordenacaocalendariosdias();
		return $coordenacaocalendariosdias->getCoordenacaocalendariosdias($queries, $page, $maxpage);
	}
	
	public function getCoordenacaocalendariosdias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$die = (isset($queries['die'])) ? $queries['die'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$idcalendario = (isset($queries["idcalendario"])) ? $queries["idcalendario"] : false;
		if ($idcalendario) array_push($where, " c1.idcalendario = $idcalendario ");

$idtipo = (isset($queries["idtipo"])) ? $queries["idtipo"] : false;
		if ($idtipo) array_push($where, " c1.idtipo = $idtipo ");


$ano = (isset($queries["ano"])) ? $queries["ano"] : false;
		if ($ano) array_push($where, " cc1.ano = '$ano' ");

$dialetivo = (isset($queries["dialetivo"])) ? $queries["dialetivo"] : false;
		if ($dialetivo) array_push($where, " ct1.dialetivo = '$dialetivo' ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " c1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " c1.data <= '$data_f' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");

$diferentdialetivo = (isset($queries["diferentdialetivo"])) ? $queries["diferentdialetivo"] : false;
		if ($diferentdialetivo) array_push($where, " (ct1.dialetivo <> '$diferentdialetivo' OR ct1.dialetivo IS NULL) ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*, ct1.cor, ct1.sigla, ct1.dialetivo, cc1.ano"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM coordenacaocalendariosdias c1
						LEFT JOIN coordenacaocalendarios cc1 ON cc1.id = c1.idcalendario 
						LEFT JOIN coordenacaocalendariostipos ct1 ON ct1.id = c1.idtipo 
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		if($die)die($strsql);
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getCoordenacaocalendariodiaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCoordenacaocalendariosdias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCoordenacaocalendariodiaByIdHelper($id, $queries = array()) {
		$rows = new Coordenacaocalendariosdias();
		return $rows->getCoordenacaocalendariodiaById($id, $queries);
	}		
	




	public function getCoordenacaocalendariodiaByIdTipoData($queries = array()) {
		$rows = $this->getCoordenacaocalendariosdias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCoordenacaocalendariodiaByIdTipoDataHelper($queries = array()) {
		$rows = new Coordenacaocalendariosdias();
		return $rows->getCoordenacaocalendariodiaByIdTipoData($queries);
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Coordenacaocalendariosdias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idcalendario = (array_key_exists("idcalendario",$dados)) ? $dados["idcalendario"] : $row->idcalendario;
		$row->idtipo = (array_key_exists("idtipo",$dados)) ? $dados["idtipo"] : $row->idtipo;
		$row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}