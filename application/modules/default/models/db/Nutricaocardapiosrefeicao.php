<?php

/**
 * Define o modelo Nutricaocardapiosrefeicao
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Nutricaocardapiosrefeicao extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "nutricaocardapiosrefeicao";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getNutricaocardapiosrefeicaoHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaocardapiosrefeicao = new Nutricaocardapiosrefeicao();
		return $nutricaocardapiosrefeicao->getNutricaocardapiosrefeicao($queries, $page, $maxpage);
	}
	
	public static function getCardapioDoDiaHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaocardapiosrefeicao = new Nutricaocardapiosrefeicao();
		return $nutricaocardapiosrefeicao->getCardapioDoDia($queries, $page, $maxpage);
	}

        public function getNutricaocardapiosrefeicao($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " n1.id = $id ");
		
		$refeicao = (isset($queries["refeicao"])) ? $queries["refeicao"] : false;
		if ($refeicao) array_push($where, " n1.refeicao LIKE '%$refeicao%' ");

		$idsrefeicoes = (isset($queries["idsrefeicoes"])) ? $queries["idsrefeicoes"] : false;
		if ($idsrefeicoes) array_push($where, " n1.idsrefeicoes LIKE '%$idsrefeicoes%' ");                

		$tipoensino = (isset($queries['tipoensino'])) ? $queries['tipoensino'] : false;
		if ($tipoensino) array_push($where, " n1.tipoensino LIKE '%$tipoensino%' ");

		$tiporefeicao = (isset($queries['tiporefeicao'])) ? $queries['tiporefeicao'] : false;
		if ($tiporefeicao) array_push($where, " n1.tiporefeicao LIKE '%$tiporefeicao%' ");
                
                $status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " n1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "n1.*"; 
		;
		
		if ($total) $fields = "COUNT(n1.id) as total";
		
		$ordem = "ORDER BY n1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM nutricaocardapiosrefeicao n1
					
					WHERE n1.excluido='nao' 
						$w 
					$ordem	
					$limit";

		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	

	public function getCardapioDoDia($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " n1.id = $id ");
		
		$refeicao = (isset($queries["refeicao"])) ? $queries["refeicao"] : false;
		if ($refeicao) array_push($where, " n1.refeicao LIKE '%$refeicao%' ");

		$idsrefeicoes = (isset($queries["idsrefeicoes"])) ? $queries["idsrefeicoes"] : false;
		if ($idsrefeicoes) array_push($where, " n1.idsrefeicoes LIKE '%$idsrefeicoes%' ");                

		$tipoensino = (isset($queries['tipoensino'])) ? $queries['tipoensino'] : false;
		if ($tipoensino) array_push($where, " n1.tipoensino = '$tipoensino' ");

		$tiporefeicao = (isset($queries['tiporefeicao'])) ? $queries['tiporefeicao'] : false;
		if ($tiporefeicao) array_push($where, " n1.tiporefeicao = '$tiporefeicao' ");
                
                $status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " n1.status LIKE '%$status%' ");

                $start = (isset($queries["start"])) ? $queries["start"] : false;
		if ($start) array_push($where, " n1.start between $start and $start ");

                if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "a1.*";
        ;

        if ($total)
            $fields = "COUNT(a1.id) as total";

        $ordem = "ORDER BY a1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM nutricaocardapiosrefeicao a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);	
	}        
        
        
	public function getNutricaocardapiorefeicaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNutricaocardapiosrefeicao($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNutricaocardapiorefeicaoByIdHelper($id, $queries = array()) {
		$rows = new Nutricaocardapiosrefeicao();
		return $rows->getNutricaocardapiorefeicaoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaocardapiosrefeicao
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
                $row->refeicao = (array_key_exists("refeicao",$dados)) ? $dados["refeicao"] : $row->refeicao;
                $row->idsrefeicoes = (array_key_exists("idsrefeicoes",$dados)) ? $dados["idsrefeicoes"] : $row->idsrefeicoes;
                $row->tipoensino = (array_key_exists("tipoensino",$dados)) ? $dados["tipoensino"] : $row->tipoensino;
                $row->tiporefeicao = (array_key_exists("tiporefeicao",$dados)) ? $dados["tiporefeicao"] : $row->tiporefeicao;
                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();

		$registro = 'Adicionado';
		if(!$novoRegistro)
			$registro = 'Editado';

		Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), " id -> [" . $row->id . "] [" . $registro . "][status=>" . $row->status . "][excluido=>" . $row->excluido . "]");
		
		return $row;
	}
	
}