<?php

/**
 * Define o modelo Nutricaomerendas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Nutricaomerendas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "nutricaomerendas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getNutricaomerendasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaomerendas = new Nutricaomerendas();
		return $nutricaomerendas->getNutricaomerendas($queries, $page, $maxpage);
	}
	
	public function getNutricaomerendas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " n1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " n1.idsecretaria = $idsecretaria ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " n1.idescola = $idescola ");

$idescolaanterior = (isset($queries["idescolaanterior"])) ? $queries["idescolaanterior"] : false;
		if ($idescolaanterior) array_push($where, " n1.idescolaanterior = $idescolaanterior ");

$idaluno = (isset($queries["idaluno"])) ? $queries["idaluno"] : false;
		if ($idaluno) array_push($where, " n1.idaluno = $idaluno ");

$aluno = (isset($queries["aluno"])) ? $queries["aluno"] : false;
		if ($aluno) array_push($where, " n1.aluno LIKE '%$aluno%' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " n1.titulo LIKE '%$titulo%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " n1.descricoes = '$descricoes' ");

$aprovacao = (isset($queries["aprovacao"])) ? $queries["aprovacao"] : false;
		if ($aprovacao) array_push($where, " n1.aprovacao LIKE '%$aprovacao%' ");

$dataaprovacao_i = (isset($queries["dataaprovacao_i"])) ? $queries["dataaprovacao_i"] : false;
		if ($dataaprovacao_i) array_push($where, " n1.dataaprovacao >= '$dataaprovacao_i' ");

$dataaprovacao_f = (isset($queries["dataaprovacao_f"])) ? $queries["dataaprovacao_f"] : false;
		if ($dataaprovacao_f) array_push($where, " n1.dataaprovacao <= '$dataaprovacao_f' ");

$cancelado = (isset($queries["cancelado"])) ? $queries["cancelado"] : false;
		if ($cancelado) array_push($where, " n1.cancelado LIKE '%$cancelado%' ");

$datacancelamento_i = (isset($queries["datacancelamento_i"])) ? $queries["datacancelamento_i"] : false;
		if ($datacancelamento_i) array_push($where, " n1.datacancelamento >= '$datacancelamento_i' ");

$datacancelamento_f = (isset($queries["datacancelamento_f"])) ? $queries["datacancelamento_f"] : false;
		if ($datacancelamento_f) array_push($where, " n1.datacancelamento <= '$datacancelamento_f' ");

$descricoescancelamento = (isset($queries["descricoescancelamento"])) ? $queries["descricoescancelamento"] : false;
		if ($descricoescancelamento) array_push($where, " n1.descricoescancelamento = '$descricoescancelamento' ");

$transferencia = (isset($queries["transferencia"])) ? $queries["transferencia"] : false;
		if ($transferencia) array_push($where, " n1.transferencia LIKE '%$transferencia%' ");

$transferenciaaprovacao = (isset($queries["transferenciaaprovacao"])) ? $queries["transferenciaaprovacao"] : false;
		if ($transferenciaaprovacao) array_push($where, " n1.transferenciaaprovacao LIKE '%$transferenciaaprovacao%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " n1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "n1.*"; 
		;
		
		if ($total) $fields = "COUNT(n1.id) as total";
		
		$ordem = "ORDER BY n1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM nutricaomerendas n1
					
					WHERE n1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if(!$total)die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getNutricaomerendaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNutricaomerendas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNutricaomerendaByIdHelper($id, $queries = array()) {
		$rows = new Nutricaomerendas();
		return $rows->getNutricaomerendaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaomerendas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idescolaanterior = (array_key_exists("idescolaanterior",$dados)) ? $dados["idescolaanterior"] : $row->idescolaanterior;
 $row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
 $row->aluno = (array_key_exists("aluno",$dados)) ? $dados["aluno"] : $row->aluno;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->aprovacao = (array_key_exists("aprovacao",$dados)) ? $dados["aprovacao"] : $row->aprovacao;
 $row->dataaprovacao = (array_key_exists("dataaprovacao",$dados)) ? $dados["dataaprovacao"] : $row->dataaprovacao;
 $row->cancelado = (array_key_exists("cancelado",$dados)) ? $dados["cancelado"] : $row->cancelado;
 $row->datacancelamento = (array_key_exists("datacancelamento",$dados)) ? $dados["datacancelamento"] : $row->datacancelamento;
 $row->descricoescancelamento = (array_key_exists("descricoescancelamento",$dados)) ? $dados["descricoescancelamento"] : $row->descricoescancelamento;
 $row->transferencia = (array_key_exists("transferencia",$dados)) ? $dados["transferencia"] : $row->transferencia;
 $row->transferenciaaprovacao = (array_key_exists("transferenciaaprovacao",$dados)) ? $dados["transferenciaaprovacao"] : $row->transferenciaaprovacao;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}


 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
  $row->quantidaderefeicao = (array_key_exists("quantidaderefeicao",$dados)) ? $dados["quantidaderefeicao"] : $row->quantidaderefeicao;
   $row->quantidadesobremesa = (array_key_exists("quantidadesobremesa",$dados)) ? $dados["quantidadesobremesa"] : $row->quantidadesobremesa;
    $row->dia = (array_key_exists("dia",$dados)) ? $dados["dia"] : $row->dia;
     $row->quantidade = (array_key_exists("quantidade",$dados)) ? $dados["quantidade"] : $row->quantidade;
	$row->propriacompra = (array_key_exists("propriacompra",$dados)) ? $dados["propriacompra"] : $row->propriacompra;					
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdataescola = (array_key_exists("logdataescola",$dados)) ? $dados["logdataescola"] : $row->logdataescola;
 $row->logusuarioescola = (array_key_exists("logusuarioescola",$dados)) ? $dados["logusuarioescola"] : $row->logusuarioescola;
		
				
		$row->save();
		
		return $row;
	}
	
}