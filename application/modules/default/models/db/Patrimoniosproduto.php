<?php

/**
 * Define o modelo Patrimoniosproduto
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Patrimoniosproduto extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "patrimoniosproduto";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getPatrimoniosprodutoHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$patrimoniosproduto = new Patrimoniosproduto();
		return $patrimoniosproduto->getPatrimoniosproduto($queries, $page, $maxpage);
	}
	
	public function getPatrimoniosproduto($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");		

		$idcomprasbens = (isset($queries["idcomprasbens"])) ? $queries["idcomprasbens"] : false;
		if ($idcomprasbens) array_push($where, " p1.idcomprasbens = $idcomprasbens ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='sequencial') $sorting[0]='p1.sequencial';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*";
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM patrimoniosproduto p1
					
					WHERE p1.excluido='nao' 
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPatrimoniocomprabemById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPatrimoniosproduto($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPatrimoniocomprabemByIdHelper($id, $queries = array()) {
		$rows = new Patrimoniosproduto();
		return $rows->getPatrimonioscomprabemById($id, $queries);
	}

	public function getUltimoPatrimoniocomprasbens($queries = array()) {
		$queries['order'] = 'ORDER BY p1.id DESC';
		$rows = $this->getPatrimoniosproduto($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}		
	
	/**
     * Altera todas as relacao de ptrimonois produto para excluído->sim
     * @param array idcomprasbens
     * @return patrimonios produto
     */
	public function setExcluido($idcomprasbens) {
		
		$row = $this->fetchAll("idcomprasbens=$idcomprasbens");
		
		if (!$row) $row = $this->createRow();
		
		foreach ($row as $k => $v) {

			$v->excluido = 'sim';	
				
			$v->save();
		}
		
		return $row;
	}	

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Patrimoniosproduto
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("idcomprasbens=".$dados['idcomprasbens']." AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idcomprasbens = (array_key_exists("idcomprasbens", $dados) ? $dados["idcomprasbens"] : $row->idcomprasbens);
		$row->idgrupobens = (array_key_exists("idgrupobens", $dados) ? $dados["idgrupobens"] : $row->idgrupobens);
		$row->foto = (array_key_exists("idfoto", $dados) ? $dados["idfoto"] : $row->foto);
		$row->nome = (array_key_exists("nome", $dados) ? $dados["nome"] : $row->nome);
		$row->idcor = (array_key_exists("idcor", $dados) ? $dados["idcor"] : $row->idcor);
		$row->idmarca = (array_key_exists("idmarca", $dados) ? $dados["idmarca"] : $row->idmarca);
		$row->idtipo = (array_key_exists("idtipo", $dados) ? $dados["idtipo"] : $row->idtipo);
		$row->idmodelo = (array_key_exists("idmodelo", $dados) ? $dados["idmodelo"] : $row->idmodelo);
		$row->caracteristica = (array_key_exists("caracteristica", $dados) ? $dados["caracteristica"] : $row->caracteristica);
		$row->dimensao = (array_key_exists("dimensao", $dados) ? $dados["dimensao"] : $row->dimensao);
		$row->serie = (array_key_exists("serieprod", $dados) ? $dados["serieprod"] : $row->serie);
		$row->nlote = (array_key_exists("nlote", $dados) ? $dados["nlote"] : $row->nlote);
		$row->anofabricacao = (array_key_exists("anofabricacao", $dados) ? $dados["anofabricacao"] : $row->anofabricacao);
		$row->inflamavel = (array_key_exists("inflamavel", $dados) ? $dados["inflamavel"] : $row->inflamavel);
						
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
			
		$row->save();
		
		return $row;
	}
}