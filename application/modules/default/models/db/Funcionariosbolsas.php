<?php

/**
 * Define o modelo Funcionariosbolsas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosbolsas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosbolsas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFuncionariosbolsasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosbolsas = new Funcionariosbolsas();
		return $funcionariosbolsas->getFuncionariosbolsas($queries, $page, $maxpage);
	}
	
	public function getFuncionariosbolsas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		
		$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " f1.idfuncionario = $idfuncionario ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " f1.titulo LIKE '%$titulo%' ");

$descricao = (isset($queries["descricao"])) ? $queries["descricao"] : false;
		if ($descricao) array_push($where, " f1.descricao = '$descricao' ");

$datainicio_i = (isset($queries["datainicio_i"])) ? $queries["datainicio_i"] : false;
		if ($datainicio_i) array_push($where, " f1.datainicio >= '$datainicio_i' ");

$datainicio_f = (isset($queries["datainicio_f"])) ? $queries["datainicio_f"] : false;
		if ($datainicio_f) array_push($where, " f1.datainicio <= '$datainicio_f' ");

$datafim_i = (isset($queries["datafim_i"])) ? $queries["datafim_i"] : false;
		if ($datafim_i) array_push($where, " f1.datafim >= '$datafim_i' ");

$datafim_f = (isset($queries["datafim_f"])) ? $queries["datafim_f"] : false;
		if ($datafim_f) array_push($where, " f1.datafim <= '$datafim_f' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosbolsas f1
					
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariobolsaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosbolsas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionariobolsaByIdHelper($id, $queries = array()) {
		$rows = new Funcionariosbolsas();
		return $rows->getFuncionariobolsaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Funcionariosbolsas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
 $row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
 $row->datafim = (array_key_exists("datafim",$dados)) ? $dados["datafim"] : $row->datafim;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}