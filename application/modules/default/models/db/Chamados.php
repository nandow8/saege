<?php

/**
 * Define o modelo Chamados
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Chamados extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "chamados";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getChamadosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$chamados = new Chamados();
		return $chamados->getChamados($queries, $page, $maxpage);
	}
	
	public function getChamados($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$destino = (isset($queries["destino"])) ? $queries["destino"] : false;
		if ($destino) array_push($where, " c1.destino LIKE '%$destino%' ");

$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " c1.iddepartamento = $iddepartamento ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " c1.idescola = $idescola ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " c1.descricoes = '$descricoes' ");

$idprioridade = (isset($queries["idprioridade"])) ? $queries["idprioridade"] : false;
		if ($idprioridade) array_push($where, " c1.idprioridade = $idprioridade ");

$idstatus = (isset($queries["idstatus"])) ? $queries["idstatus"] : false;
		if ($idstatus) array_push($where, " c1.idstatus = $idstatus ");

$dataagendada_i = (isset($queries["dataagendada_i"])) ? $queries["dataagendada_i"] : false;
		if ($dataagendada_i) array_push($where, " c1.dataagendada >= '$dataagendada_i' ");

$dataagendada_f = (isset($queries["dataagendada_f"])) ? $queries["dataagendada_f"] : false;
		if ($dataagendada_f) array_push($where, " c1.dataagendada <= '$dataagendada_f' ");

$finalizacao = (isset($queries["finalizacao"])) ? $queries["finalizacao"] : false;
		if ($finalizacao) array_push($where, " c1.finalizacao LIKE '%$finalizacao%' ");

$atendido = (isset($queries["atendido"])) ? $queries["atendido"] : false;
		if ($atendido) array_push($where, " c1.atendido LIKE '%$atendido%' ");

$idtipo = (isset($queries["idtipo"])) ? $queries["idtipo"] : false;
		if ($idtipo) array_push($where, " c1.idtipo = $idtipo ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " c1.observacoes = '$observacoes' ");

$servicoesdescricoes = (isset($queries["servicoesdescricoes"])) ? $queries["servicoesdescricoes"] : false;
		if ($servicoesdescricoes) array_push($where, " c1.servicoesdescricoes = '$servicoesdescricoes' ");

$servicosmateriais = (isset($queries["servicosmateriais"])) ? $queries["servicosmateriais"] : false;
		if ($servicosmateriais) array_push($where, " c1.servicosmateriais = '$servicosmateriais' ");

$idsfuncionarios = (isset($queries["idsfuncionarios"])) ? $queries["idsfuncionarios"] : false;
		if ($idsfuncionarios) array_push($where, " c1.idsfuncionarios = '$idsfuncionarios' ");

$patrimoniosobservacoes = (isset($queries["patrimoniosobservacoes"])) ? $queries["patrimoniosobservacoes"] : false;
		if ($patrimoniosobservacoes) array_push($where, " c1.patrimoniosobservacoes = '$patrimoniosobservacoes' ");

$defeitoconstatado = (isset($queries["defeitoconstatado"])) ? $queries["defeitoconstatado"] : false;
		if ($defeitoconstatado) array_push($where, " c1.defeitoconstatado = '$defeitoconstatado' ");

$servicosexecutados = (isset($queries["servicosexecutados"])) ? $queries["servicosexecutados"] : false;
		if ($servicosexecutados) array_push($where, " c1.servicosexecutados = '$servicosexecutados' ");

$substituicaopecas = (isset($queries["substituicaopecas"])) ? $queries["substituicaopecas"] : false;
		if ($substituicaopecas) array_push($where, " c1.substituicaopecas LIKE '%$substituicaopecas%' ");

$statusatendimento = (isset($queries["statusatendimento"])) ? $queries["statusatendimento"] : false;
		if ($statusatendimento) array_push($where, " c1.statusatendimento LIKE '%$statusatendimento%' ");

$tiobservacoesresposta = (isset($queries["tiobservacoesresposta"])) ? $queries["tiobservacoesresposta"] : false;
		if ($tiobservacoesresposta) array_push($where, " c1.tiobservacoesresposta = '$tiobservacoesresposta' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");

$idusuariocriacao = (isset($queries["idusuariocriacao"])) ? $queries["idusuariocriacao"] : false;
		if ($idusuariocriacao) array_push($where, " c1.idusuariocriacao = $idusuariocriacao ");

$iddepartamentocriacao = (isset($queries["iddepartamentocriacao"])) ? $queries["iddepartamentocriacao"] : false;
		if ($iddepartamentocriacao) array_push($where, " c1.iddepartamentocriacao = $iddepartamentocriacao ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM chamados c1
					
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getChamadoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getChamados($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getChamadoByIdHelper($id, $queries = array()) {
		$rows = new Chamados();
		return $rows->getChamadoById($id, $queries);
	}		
	
	public function getUltimoChamadoByDestino($destino, $queries = array()) {
		if (!(isset($destino))) return false;
		
		$queries['destino'] = $destino;
		$queries['order'] = 'ORDER BY c1.id DESC';
		$rows = $this->getChamados($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Chamados
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->destino = (array_key_exists("destino",$dados)) ? $dados["destino"] : $row->destino;
 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->idprioridade = (array_key_exists("idprioridade",$dados)) ? $dados["idprioridade"] : $row->idprioridade;
 $row->idstatus = (array_key_exists("idstatus",$dados)) ? $dados["idstatus"] : $row->idstatus;
 $row->dataagendada = (array_key_exists("dataagendada",$dados)) ? $dados["dataagendada"] : $row->dataagendada;
 $row->finalizacao = (array_key_exists("finalizacao",$dados)) ? $dados["finalizacao"] : $row->finalizacao;
 $row->finalizacaodata = (array_key_exists("finalizacaodata",$dados)) ? $dados["finalizacaodata"] : $row->finalizacaodata;
 $row->atendido = (array_key_exists("atendido",$dados)) ? $dados["atendido"] : $row->atendido;
 $row->idtipo = (array_key_exists("idtipo",$dados)) ? $dados["idtipo"] : $row->idtipo;
 $row->idtipoequipamento = (array_key_exists("idtipoequipamento",$dados)) ? $dados["idtipoequipamento"] : $row->idtipoequipamento;
 $row->idtipodaequipe = (array_key_exists("idtipodaequipe",$dados)) ? $dados["idtipodaequipe"] : $row->idtipodaequipe;
 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
 $row->servicoesdescricoes = (array_key_exists("servicoesdescricoes",$dados)) ? $dados["servicoesdescricoes"] : $row->servicoesdescricoes;
 $row->servicosmateriais = (array_key_exists("servicosmateriais",$dados)) ? $dados["servicosmateriais"] : $row->servicosmateriais;
 $row->idsfuncionarios = (array_key_exists("idsfuncionarios",$dados)) ? $dados["idsfuncionarios"] : $row->idsfuncionarios;
 $row->idspatrimonios = (array_key_exists("idspatrimonios",$dados)) ? $dados["idspatrimonios"] : $row->idspatrimonios;
 $row->patrimoniosobservacoes = (array_key_exists("patrimoniosobservacoes",$dados)) ? $dados["patrimoniosobservacoes"] : $row->patrimoniosobservacoes;
 $row->defeitoconstatado = (array_key_exists("defeitoconstatado",$dados)) ? $dados["defeitoconstatado"] : $row->defeitoconstatado;
 $row->servicosexecutados = (array_key_exists("servicosexecutados",$dados)) ? $dados["servicosexecutados"] : $row->servicosexecutados;
 $row->substituicaopecas = (array_key_exists("substituicaopecas",$dados)) ? $dados["substituicaopecas"] : $row->substituicaopecas;
 $row->idspecas = (array_key_exists("idspecas",$dados)) ? $dados["idspecas"] : $row->idspecas;
 $row->tiobservacoesresposta = (array_key_exists("tiobservacoesresposta",$dados)) ? $dados["tiobservacoesresposta"] : $row->tiobservacoesresposta;
 $row->idresponsavel = (array_key_exists("idresponsavel",$dados)) ? $dados["idresponsavel"] : $row->idresponsavel;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->idusuariocriacao = (array_key_exists("idusuariocriacao",$dados)) ? $dados["idusuariocriacao"] : $row->idusuariocriacao;
 $row->iddepartamentocriacao = (array_key_exists("iddepartamentocriacao",$dados)) ? $dados["iddepartamentocriacao"] : $row->iddepartamentocriacao;
 $row->observacaosubstituicao = (array_key_exists("observacaosubstituicao",$dados)) ? $dados["observacaosubstituicao"] : $row->observacaosubstituicao;
 $row->servicosexecutados = (array_key_exists("servicosexecutados",$dados)) ? $dados["servicosexecutados"] : $row->servicosexecutados;
 $row->defeitoconstatado = (array_key_exists("defeitoconstatado",$dados)) ? $dados["defeitoconstatado"] : $row->defeitoconstatado;
 $row->pecaid = (array_key_exists("pecaid",$dados)) ? $dados["pecaid"] : $row->pecaid;
 $row->substituicaopecas = (array_key_exists("substituicaopecas",$dados)) ? $dados["substituicaopecas"] : $row->substituicaopecas;
 $row->statusatendimento = (array_key_exists("statusatendimento",$dados)) ? $dados["statusatendimento"] : $row->statusatendimento;
 $row->obsEngenheiro = (array_key_exists("obsEngenheiro",$dados)) ? $dados["obsEngenheiro"] : $row->obsEngenheiro;
 $row->obsDataTi = (array_key_exists("obsDataTi",$dados)) ? $dados["obsDataTi"] : $row->obsDataTi;
 

		$row->save();
		
		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "chamado para o setor  $row->destino salvo com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "chamado para o setor  $row->destino com o ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "chamado para o setor  $row->destino com o ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "chamado para o setor  $row->destino com o ID ".$id." atualizada com sucesso!");

		return $row;
	}
	
}