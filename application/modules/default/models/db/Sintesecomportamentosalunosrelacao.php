<?php

/**
 * Define o modelo Sintesecomportamentosalunosrelacao
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Sintesecomportamentosalunosrelacao extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "sinetesecomportamentosdesempenhosrelacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getSintesecomportamentosalunosrelacaoHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$sintesecomportamentosdesempenhosrelacoes = new Sintesecomportamentosalunosrelacao();
		return $sintesecomportamentosdesempenhosrelacoes->getSintesecomportamentosalunosrelacao($queries, $page, $maxpage);
	}
	
	public function getSintesecomportamentosalunosrelacao($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$idsintese = (isset($queries['idsintese'])) ? (int)$queries['idsintese'] : false;
		if ($idsintese) array_push($where, " a1.idsintese = " . $idsintese);

		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		if( $idaluno ) array_push($where, " a1.idaluno = " . $idaluno);

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='idavaliacao') $sorting[0]='a1.idavaliacao';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*";
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM sintesecomportamentosdesempenhosrelacoes a1 WHERE excluido='nao' 
				
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getSintesecomportamentosalunosrelacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idsintese'] = $id;
		$rows = $this->getSintesecomportamentosalunosrelacao($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSintesecomportamentosalunosrelacaoByIdHelper($id, $queries = array()) {
		$rows = new Sintesecomportamentosalunosrelacao();
		return $rows->getSintesecomportamentosalunosrelacaoById($id, $queries);
	}		

	/**
     * Altera todas as relacao de avaliação-alunos para excluído->sim
     * @param array idsintese
     * @return Sintesecomportamentosalunosrelacao
     */
	public function setExcluido($idsintese) {
		
		$row = $this->fetchAll("idsintese=$idsintese");
		
		if (!$row) $row = $this->createRow();
		
		foreach ($row as $k => $v) {

			$v->excluido = 'sim';	
				
			$v->save();
		}
		
		return $row;
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Sintesecomportamentosalunosrelacao
     */
	public function save($dados) {
		$novoRegistro = true;

		$idsintese = (!isset($dados['idsintese'])) ? 0 : (int)$dados['idsintese'];
		$idaluno = (!isset($dados['idaluno']) ? 0 : (int)$dados['idaluno']);
		$concentracao = (!isset($dados['concentracao']) ? '' : $dados['concentracao']);
		$aquicisao = (!isset($dados['aquicisao']) ? '' : $dados['aquicisao']);
		$raciocinio = (!isset($dados['raciocinio']) ? '' : $dados['raciocinio']);
		$motivacao = (!isset($dados['motivacao']) ? '' : $dados['motivacao']);
		$comportamento = (!isset($dados['comportamento']) ? '' : $dados['comportamento']);
		$relacaointerpessoal = (!isset($dados['relacaointerpessoal']) ? '' : $dados['relacaointerpessoal']);
		$cooperacao = (!isset($dados['cooperacao']) ? '' : $dados['cooperacao']);
		$leitura = (!isset($dados['leitura']) ? '' : $dados['leitura']);
		$escrita = (!isset($dados['escrita']) ? '' : $dados['escrita']);
		$matematica = (!isset($dados['matematica']) ? '' : $dados['matematica']);
		$organizacao = (!isset($dados['organizacao']) ? '' : $dados['organizacao']);
		$assiduidade = (!isset($dados['assiduidade']) ? '' : $dados['assiduidade']);
		$rendimento = (!isset($dados['rendimento']) ? '' : $dados['rendimento']);
		$infocomplementar = (!isset($dados['infocomplementar']) ? '' : $dados['infocomplementar']);
		$row = $this->fetchRow("idsintese=$idsintese AND idaluno=$idaluno");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		}		
		
		$row->idsintese = (array_key_exists('idsintese', $dados)) ? $dados['idsintese'] : $row->idsintese;
		$row->idaluno = (array_key_exists('idaluno', $dados)) ? $dados['idaluno'] : $row->idaluno;
		$row->concentracao = (array_key_exists('concentracao', $dados)) ? $dados['concentracao'] : $row->concentracao;
		$row->aquicisao = (array_key_exists('aquicisao', $dados)) ? $dados['aquicisao'] : $row->aquicisao;
		$row->raciocinio = (array_key_exists('raciocinio', $dados)) ? $dados['raciocinio'] : $row->raciocinio;
		$row->motivacao = (array_key_exists('motivacao', $dados)) ? $dados['motivacao'] : $row->motivacao;
		$row->comportamento = (array_key_exists('comportamento', $dados)) ? $dados['comportamento'] : $row->comportamento;
		$row->relacaointerpessoal = (array_key_exists('relacaointerpessoal', $dados)) ? $dados['relacaointerpessoal'] : $row->relacaointerpessoal;
		$row->cooperacao = (array_key_exists('cooperacao', $dados)) ? $dados['cooperacao'] : $row->cooperacao;
		$row->leitura = (array_key_exists('leitura', $dados)) ? $dados['leitura'] : $row->leitura;
		$row->escrita = (array_key_exists('escrita', $dados)) ? $dados['escrita'] : $row->escrita;
		$row->matematica = (array_key_exists('matematica', $dados)) ? $dados['matematica'] : $row->matematica;
		$row->organizacao = (array_key_exists('organizacao', $dados)) ? $dados['organizacao'] : $row->organizacao;
		$row->assiduidade = (array_key_exists('assiduidade', $dados)) ? $dados['assiduidade'] : $row->assiduidade;
		$row->rendimento = (array_key_exists('rendimento', $dados)) ? $dados['rendimento'] : $row->rendimento;
		$row->infocomplementar = (array_key_exists('infocomplementar', $dados)) ? $dados['infocomplementar'] : $row->infocomplementar;
		$row->excluido = (array_key_exists('excluido', $dados)) ? $dados['excluido'] : $row->excluido;	
				
		$row->save();
		
		return $row;
	}
}