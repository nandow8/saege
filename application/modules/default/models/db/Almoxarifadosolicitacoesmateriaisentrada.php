<?php

/**
 * Define o modelo Almoxarifadosolicitacoesmateriaisentrada
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Almoxarifadosolicitacoesmateriaisentrada extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadosolicitacoesmateriaisentrada";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAlmoxarifadosolicitacoesmateriaisentradaHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$almoxarifadosolicitacoesmateriaisentrada = new Almoxarifadosolicitacoesmateriaisentrada();
		return $almoxarifadosolicitacoesmateriaisentrada->getAlmoxarifadosolicitacoesmateriaisentrada($queries, $page, $maxpage);
	}
	
	public function getAlmoxarifadosolicitacoesmateriaisentrada($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
		
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " a1.idescola = $idescola "); 
		
		$idperfil = (isset($queries['idperfil'])) ? (int)$queries['idperfil'] : false;
		if ($idperfil) array_push($where, " a1.idperfil = $idperfil "); 
		
		$idperfilbusca = (isset($queries['idperfilbusca'])) ? (int)$queries['idperfilbusca'] : false;
		if ($idperfilbusca) array_push($where, " a1.idperfilbusca = $idperfilbusca "); 
        
        $idusuario = (isset($queries['idusuario'])) ? (int)$queries['idusuario'] : false;
        if ($idusuario) array_push($where, " a1.idusuario = $idusuario ");
		
		$observacao = (isset($queries["observacao"])) ? $queries["observacao"] : false;
		if ($observacao) array_push($where, " a1.observacao = '$observacao' "); 

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*";
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadosolicitacoesmateriaisentrada a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		 
		return $db->fetchAll($strsql);			
	}	
	
	public function getAlmoxarifadosolicitacaomaterialentradaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAlmoxarifadosolicitacoesmateriaisentrada($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAlmoxarifadosolicitacaomaterialentradaByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifadosolicitacoesmateriaisentrada();
		return $rows->getAlmoxarifadosolicitacaomaterialentradaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadosolicitacoesmateriaisentrada
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idusuario = (array_key_exists("idusuario", $dados)) ? $dados["idusuario"] : $row->idusuario;
		$row->idperfil = (array_key_exists("idperfil", $dados)) ? $dados["idperfil"] : $row->idperfil;
		$row->idperfilbusca = (array_key_exists("idperfilbusca", $dados)) ? $dados["idperfilbusca"] : $row->idperfilbusca;
		$row->data = (array_key_exists("data", $dados)) ? $dados["data"] : $row->data;
		$row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
        $row->idlocal = (array_key_exists('idlocal',$dados)) ? $dados['idlocal'] : $row->idlocal;
		$row->codigosolicitacao = (array_key_exists("codigosolicitacao",$dados)) ? $dados["codigosolicitacao"] : $row->codigosolicitacao;
 		
 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
// echo '<pre>'; print_r($row);die;
		$row->save();
		
		return $row;
	}
	
}