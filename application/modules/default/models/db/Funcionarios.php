<?php

class Funcionarios extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionarios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_ESTADO_CIVIL_SOLTEIRO = 'solteiro';
	public static $_ESTADO_CIVIL_CASADO = 'casado';
	public static $_ESTADO_CIVIL_SEPARADO_JUDICIALMENTE = 'separadojudicialmente';
	public static $_ESTADO_CIVIL_DIVORCIADO = 'divorciado';
	public static $_ESTADO_CIVIL_DESQUITADO = 'desquitado';
	public static $_ESTADO_CIVIL_UNIAO_ESTAVEL = 'uniaoestavel';
	public static $_ESTADO_CIVIL_VIUVO = 'viuvo';	
	
	/**
	 * 
	 */
	public static function getEstadoCivil($field = false) {
		$res = array(
			self::$_ESTADO_CIVIL_SOLTEIRO => 'Solteiro',
			self::$_ESTADO_CIVIL_CASADO => 'Casado',
			self::$_ESTADO_CIVIL_SEPARADO_JUDICIALMENTE => 'Separado Judicialmente',
			self::$_ESTADO_CIVIL_DIVORCIADO => 'Divorciado',
			self::$_ESTADO_CIVIL_DESQUITADO => 'Desquitado',
			self::$_ESTADO_CIVIL_UNIAO_ESTAVEL => 'União Estável',
			self::$_ESTADO_CIVIL_VIUVO => 'Viúvo'
		);
		
		if (!$field) return $res;
		return $res[$field];
	}
	
	public static $_ESTADO_ESCOLARIDADE_ANALFABETO = 'analfabeto';
	public static $_ESTADO_ESCOLARIDADE_FUNDAMENTAL = 'fundamental';
	public static $_ESTADO_ESCOLARIDADE_MEDIO = 'ensinomedio';
	public static $_ESTADO_ESCOLARIDADE_SUPERIOR_INCOPLETO = 'ensinosuperiorincompleto';
	public static $_ESTADO_ESCOLARIDADE_SUPERIOR_ANDAMENTO = 'ensinosuperioremandamento';
	public static $_ESTADO_ESCOLARIDADE_SUPERIOR_COMPLETO = 'ensinosuperiorcompleto';
	
	/**
	 * 
	 */
	public static function getEscolaridade($field = false) {
		$res = array(
			self::$_ESTADO_ESCOLARIDADE_ANALFABETO => 'Analfabeto',
			self::$_ESTADO_ESCOLARIDADE_FUNDAMENTAL => 'Ensino Fundamental',
			self::$_ESTADO_ESCOLARIDADE_MEDIO => 'Ensino Médio',
			self::$_ESTADO_ESCOLARIDADE_SUPERIOR_INCOPLETO => 'Ensino Superior Incompleto',
			self::$_ESTADO_ESCOLARIDADE_SUPERIOR_ANDAMENTO => 'Ensino Superior em Andamento',
			self::$_ESTADO_ESCOLARIDADE_SUPERIOR_COMPLETO => 'Ensino Superior Completo'
		);
		
		if (!$field) return $res;
		return $res[$field];
	}
	
	public static function getFuncionariosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Funcionarios();
		return $produtos->getFuncionarios($queries, $page, $maxpage);
	}
	
	public function getFuncionarios($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;	
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;	
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$bolsista = (isset($queries['bolsista'])) ? $queries['bolsista'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " f1.id=$id ");		
		if ($idescola) array_push($where, " f1.idescola=$idescola ");
		if ($idsecretaria) array_push($where, " f1.idsecretaria=$idsecretaria ");
		if ($bolsista) array_push($where, " f1.bolsista='$bolsista' ");
		if ($chave) array_push($where, " ((f1.nomerazao LIKE '%$chave%') OR (f1.sobrenomefantasia LIKE '%$chave%')) ");
		if ($status) array_push($where, " f1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*";
		if ($total) $fields = "COUNT(f1.id) as total";
		
		
		$ordem = "ORDER BY f1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionarios f1
					WHERE f1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getFuncionarioById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionarioByIdHelper($id, $queries = array()) {
		$rows = new Funcionarios();
		return $rows->getFuncionarioById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Funcionarios
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s'); 		
		}else {
			$novoRegistro = false;
			$historico = new Funcionarioshistoricos();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);
		} 	
		$row->iddepartamento = (array_key_exists('iddepartamento',$dados)) ? $dados['iddepartamento'] : $row->iddepartamento;
		$row->idendereco = (array_key_exists('idendereco',$dados)) ? $dados['idendereco'] : $row->idendereco; 	
		$row->professor = (array_key_exists('professor',$dados)) ? $dados['professor'] : $row->professor; 
		$row->nomerazao = (array_key_exists('nomerazao',$dados)) ? $dados['nomerazao'] : $row->nomerazao;
		$row->sobrenomefantasia = (array_key_exists('sobrenomefantasia',$dados)) ? $dados['sobrenomefantasia'] : $row->sobrenomefantasia;
		$row->email = (array_key_exists('email',$dados)) ? $dados['email'] : $row->email;		
		$row->datanascimento = (array_key_exists('datanascimento',$dados)) ? $dados['datanascimento'] : $row->datanascimento;
		$row->emissaocarteira = (array_key_exists('emissaocarteira',$dados)) ? $dados['emissaocarteira'] : $row->emissaocarteira;
		$row->reservista = (array_key_exists('reservista',$dados)) ? $dados['reservista'] : $row->reservista;
		$row->reservista_ra = (array_key_exists('reservista_ra',$dados)) ? $dados['reservista_ra'] : $row->reservista_ra;
		$row->tituloeleitor = (array_key_exists('tituloeleitor',$dados)) ? $dados['tituloeleitor'] : $row->tituloeleitor;
		$row->zonaeleitoral = (array_key_exists('zonaeleitoral',$dados)) ? $dados['zonaeleitoral'] : $row->zonaeleitoral;
		$row->secaoeleitoral = (array_key_exists('secaoeleitoral',$dados)) ? $dados['secaoeleitoral'] : $row->secaoeleitoral;
		
		$row->rg = (array_key_exists('rg',$dados)) ? $dados['rg'] : $row->rg;
		$row->cpf = (array_key_exists('cpf',$dados)) ? $dados['cpf'] : $row->cpf;
		$row->pispasep = (array_key_exists('pispasep',$dados)) ? $dados['pispasep'] : $row->pispasep;
		$row->cnh = (array_key_exists('cnh',$dados)) ? $dados['cnh'] : $row->cnh;
		$row->cnh_numero = (array_key_exists('cnh_numero',$dados)) ? $dados['cnh_numero'] : $row->cnh_numero;
		$row->cnh_categoria = (array_key_exists('cnh_categoria',$dados)) ? $dados['cnh_categoria'] : $row->cnh_categoria;
		$row->cnh_validade = (array_key_exists('cnh_validade',$dados)) ? $dados['cnh_validade'] : $row->cnh_validade;
		$row->estrangeiro = (array_key_exists('estrangeiro',$dados)) ? $dados['estrangeiro'] : $row->estrangeiro;
		$row->obs_estrangeiro = (array_key_exists('obs_estrangeiro',$dados)) ? $dados['obs_estrangeiro'] : $row->obs_estrangeiro;
		$row->nacionalidade = (array_key_exists('nacionalidade',$dados)) ? $dados['nacionalidade'] : $row->nacionalidade;
		$row->localnascimento = (array_key_exists('localnascimento',$dados)) ? $dados['localnascimento'] : $row->localnascimento;
		$row->grauinstrucao = (array_key_exists('grauinstrucao',$dados)) ? $dados['grauinstrucao'] : $row->grauinstrucao;
		$row->estadocivil = (array_key_exists('estadocivil',$dados)) ? $dados['estadocivil'] : $row->estadocivil;
		$row->conjugenome = (array_key_exists('conjugenome',$dados)) ? $dados['conjugenome'] : $row->conjugenome;
		$row->filhos = (array_key_exists('filhos',$dados)) ? $dados['filhos'] : $row->filhos;
		$row->filhosquantidade = (array_key_exists('filhosquantidade',$dados)) ? $dados['filhosquantidade'] : $row->filhosquantidade;
		$row->telefone = (array_key_exists('telefone',$dados)) ? $dados['telefone'] : $row->telefone;
		$row->dataadmissao = (array_key_exists('dataadmissao',$dados)) ? $dados['dataadmissao'] : $row->nomerazao;
		$row->cbo = (array_key_exists('cbo',$dados)) ? $dados['cbo'] : $row->cbo;
		$row->cargo = (array_key_exists('cargo',$dados)) ? $dados['cargo'] : $row->cargo;
		
		$row->cargocomissionado = (array_key_exists('cargocomissionado',$dados)) ? $dados['cargocomissionado'] : $row->cargocomissionado;
		$row->dataadmissaocomissionado = (array_key_exists('dataadmissaocomissionado',$dados)) ? $dados['dataadmissaocomissionado'] : $row->dataadmissaocomissionado;
		$row->matricula = (array_key_exists('matricula',$dados)) ? $dados['matricula'] : $row->matricula;
		$row->secretaria = (array_key_exists('secretaria',$dados)) ? $dados['secretaria'] : $row->secretaria;
		
		$row->funcao = (array_key_exists('funcao',$dados)) ? $dados['funcao'] : $row->funcao;
		$row->setor = (array_key_exists('setor',$dados)) ? $dados['setor'] : $row->setor;
		$row->descricaofuncao = (array_key_exists('descricaofuncao',$dados)) ? $dados['descricaofuncao'] : $row->descricaofuncao;
		$row->riscofuncao = (array_key_exists('riscofuncao',$dados)) ? $dados['riscofuncao'] : $row->riscofuncao;
		$row->salarioinicial = (array_key_exists('salarioinicial',$dados)) ? $dados['salarioinicial'] : $row->salarioinicial;
		$row->formapagamento = (array_key_exists('formapagamento',$dados)) ? $dados['formapagamento'] : $row->formapagamento;
		$row->banco = (array_key_exists('banco',$dados)) ? $dados['banco'] : $row->banco;
		$row->agencia = (array_key_exists('agencia',$dados)) ? $dados['agencia'] : $row->agencia;
		$row->conta = (array_key_exists('conta',$dados)) ? $dados['conta'] : $row->conta;
		
		$row->celular = (array_key_exists('celular',$dados)) ? $dados['celular'] : $row->celular;
		$row->carteiranumero = (array_key_exists('carteiranumero',$dados)) ? $dados['carteiranumero'] : $row->carteiranumero;
		$row->carteiraserie = (array_key_exists('carteiraserie',$dados)) ? $dados['carteiraserie'] : $row->carteiraserie;
		$row->filiacaomae = (array_key_exists('filiacaomae',$dados)) ? $dados['filiacaomae'] : $row->filiacaomae;
		$row->filiacaopai = (array_key_exists('filiacaopai',$dados)) ? $dados['filiacaopai'] : $row->filiacaopai;
		$row->dataregistroctps = (array_key_exists('dataregistroctps',$dados)) ? $dados['dataregistroctps'] : $row->dataregistroctps;
		
		$row->regime = (array_key_exists('regime',$dados)) ? $dados['regime'] : $row->regime;
		$row->regime_outros = (array_key_exists('regime_outros',$dados)) ? $dados['regime_outros'] : $row->regime_outros;
		$row->bolsista = (array_key_exists('bolsista',$dados)) ? $dados['bolsista'] : $row->bolsista;
		$row->datainiciobolsa = (array_key_exists('datainiciobolsa',$dados)) ? $dados['datainiciobolsa'] : $row->datainiciobolsa;
		$row->datafimbolsa = (array_key_exists('datafimbolsa',$dados)) ? $dados['datafimbolsa'] : $row->datafimbolsa;
		
		$row->ctps = (array_key_exists('ctps',$dados)) ? $dados['ctps'] : $row->ctps;
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
                
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		
		$row->save();

		return $row;
	}
	
	
}