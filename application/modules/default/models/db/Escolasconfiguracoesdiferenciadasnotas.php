<?php

/**
 * Define o modelo Escolasconfiguracoesdiferenciadasnotas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasconfiguracoesdiferenciadasnotas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasconfiguracoesdiferenciadasnotas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolasconfiguracoesdiferenciadasnotasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasconfiguracoesdiferenciadasnotas = new Escolasconfiguracoesdiferenciadasnotas();
		return $escolasconfiguracoesdiferenciadasnotas->getEscolasconfiguracoesdiferenciadasnotas($queries, $page, $maxpage);
	}
	
	public function getEscolasconfiguracoesdiferenciadasnotas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$idparent = (isset($queries["idparent"])) ? $queries["idparent"] : false;
		if ($idparent) array_push($where, " e1.idparent = $idparent ");

		$idtitulonota = (isset($queries["idtitulonota"])) ? $queries["idtitulonota"] : false;
				if ($idtitulonota) array_push($where, " e1.idtitulonota = $idtitulonota ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasconfiguracoesdiferenciadasnotas e1
					
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";
		//echo '<pre>';
		//print_r($strsql);	
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolaconfiguracaodiferenciadanotaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasconfiguracoesdiferenciadasnotas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaconfiguracaodiferenciadanotaByIdHelper($id, $queries = array()) {
		$rows = new Escolasconfiguracoesdiferenciadasnotas();
		return $rows->getEscolaconfiguracaodiferenciadanotaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasconfiguracoesdiferenciadasnotas
     */
	public function save($dados) {		
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idparent = (array_key_exists("idparent",$dados)) ? $dados["idparent"] : $row->idparent;
 $row->idtitulonota = (array_key_exists("idtitulonota",$dados)) ? $dados["idtitulonota"] : $row->idtitulonota;
 $row->percentualnota = (array_key_exists("percentualnota",$dados)) ? $dados["percentualnota"] : $row->percentualnota;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		//return true;		
		$row->save();
		
		return $row;
	}
	
}