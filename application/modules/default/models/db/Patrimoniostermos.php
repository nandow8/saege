<?php
/**
 * Define o modelo Patrimoniostermos
 *
 * @author		Rafael Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Patrimoniostermos extends Zend_Db_Table_Abstract{

	/**
	* Define o nome da tabela
	* @var string
	*/
	protected $_name = "patrimoniostermos";

	protected $_primary = "id";

	public static function getPatrimoniostermosByIdHelper($id, $queries = array()){
		$rows = new Patrimoniostermos;
		return $rows->getPatrimoniostermosaceite($id, $queries);
	}

	public function getPatrimoniostermosaceiteById($id, $queries=array()){
		$queries['id'] = $id;

		$patrimoniostermos = $this->getPatrimoniostermosaceite($queries, 0, 0);

		if(sizeof($patrimoniostermos)==0) return false;
		return $patrimoniostermos[0];
	}

	public function getPatrimoniostermosaceiteHelper($queries = array(), $page=0, $maxpage=0 ){
		$patrimoniostermos = new Patrimoniostermos;
		return $patrimoniostermos->getPatrimoniostermosaceite($queries, $page, $maxpage);
	}

	public function getPatrimoniostermosaceite($queries = array(), $page=0, $maxpage=0){
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " p1.sequencial LIKE '%$sequencial%' ");

		$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " p1.datalancamento >= '$datalancamento_i' ");

		$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " p1.datalancamento <= '$datalancamento_f' ");

		$horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
		if ($horalancamento) array_push($where, " p1.horalancamento = '$horalancamento' ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " p1.idescola = $idescola ");

		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " p1.iddepartamento = $iddepartamento ");

		$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " p1.observacoes = '$observacoes' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status = '$status' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM patrimoniostermos p1					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";

		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);
	}
	
	public function getUltimoPatrimoniotermos($queries = array()) {
		$queries['order'] = 'ORDER BY p1.id DESC';
		$rows = $this->getPatrimoniostermosaceite($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	/**
	* Salva ou atualiza dados
	* @param array dados
	* @param return Patrimoniostermos
	*/

	public function save($dados){
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		}

		$row->sequencial = (array_key_exists("sequencial", $dados)) ? $dados['sequencial'] : $row->sequencial;
		$row->horalancamento = (array_key_exists("horalancamento", $dados)) ? $dados['horalancamento'] : $row->horalancamento;
		$row->idusuario = (array_key_exists("idusuario", $dados)) ? $dados['idusuario'] : $row->idusuario;

		$row->datalancamento = (array_key_exists("datalancamento", $dados)) ? $dados['datalancamento'] : $row->datalancamento;
		$row->iddepartamento = (array_key_exists("iddepartamento", $dados)) ? $dados['iddepartamento'] : $row->iddepartamento;
		$row->idescola = (array_key_exists("idescola", $dados)) ? $dados['idescola'] : $row->idescola;
		$row->iddocumento = (array_key_exists("iddocumento", $dados)) ? $dados['iddocumento'] : $row->iddocumento;		
		$row->observacoes = (array_key_exists("status",$dados)) ? $dados["observacoes"] : $row->observacoes;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;

	
		$row->save();
		return $row;
	}
}
?>