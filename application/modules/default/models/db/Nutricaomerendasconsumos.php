<?php

/**
 * Define o modelo Nutricaomerendasconsumos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Nutricaomerendasconsumos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "nutricaomerendasconsumos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getNutricaomerendasconsumosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaomerendasconsumos = new Nutricaomerendasconsumos();
		return $nutricaomerendasconsumos->getNutricaomerendasconsumos($queries, $page, $maxpage);
	}
	
	public function getNutricaomerendasconsumos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " n1.id = $id ");
		
		
		$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " n1.datalancamento >= '$datalancamento_i' ");

                $datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " n1.datalancamento <= '$datalancamento_f' ");

                $idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " n1.idusuario LIKE '%$idusuario%' ");

                $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " n1.idescola = '$idescola' ");

                $idlocal = (isset($queries["idlocal"])) ? $queries["idlocal"] : false;
		if ($idlocal) array_push($where, " n1.idlocal = '$idlocal' ");

                $tipoensino = (isset($queries["tipoensino"])) ? $queries["tipoensino"] : false;
		if ($tipoensino) array_push($where, " n1.tipoensino LIKE '%$tipoensino%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "n1.*"; 
		;
		
		if ($total) $fields = "COUNT(n1.id) as total";
		
		$ordem = "ORDER BY n1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM nutricaomerendasconsumos n1
					
					WHERE n1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getNutricaomerendasconsumoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNutricaomerendasconsumos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNutricaomerendasconsumoByIdHelper($id, $queries = array()) {
		$rows = new Nutricaomerendasconsumos();
		return $rows->getNutricaomerendasconsumoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaomerendasconsumos
     */
	public function save($dados) {
		$novoRegistro = true;
        
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
                
                $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
                $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
                $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
                $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
                $row->periodos = (array_key_exists("periodos",$dados)) ? $dados["periodos"] : $row->periodos;
                $row->tipoensino = (array_key_exists("tipoensino",$dados)) ? $dados["tipoensino"] : $row->tipoensino;
                $row->turmas = (array_key_exists("turmas",$dados)) ? $dados["turmas"] : $row->turmas;
                $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
                $row->aluno = (array_key_exists("aluno",$dados)) ? $dados["aluno"] : $row->aluno;
                $row->qtdrefeicao = (array_key_exists("qtdrefeicao",$dados)) ? $dados["qtdrefeicao"] : $row->qtdrefeicao;
                $row->qtdrefeicaorepeticoes = (array_key_exists("qtdrefeicaorepeticoes",$dados)) ? $dados["qtdrefeicaorepeticoes"] : $row->qtdrefeicaorepeticoes;
                $row->qtdsobremesa = (array_key_exists("qtdsobremesa",$dados)) ? $dados["qtdsobremesa"] : $row->qtdsobremesa;
                $row->qtdsobremesarepeticoes = (array_key_exists("qtdsobremesarepeticoes",$dados)) ? $dados["qtdsobremesarepeticoes"] : $row->qtdsobremesarepeticoes;
                $row->qtdcafemanha = (array_key_exists("qtdcafemanha",$dados)) ? $dados["qtdcafemanha"] : $row->qtdcafemanha;
                $row->qtdcafemanharepeticoes = (array_key_exists("qtdcafemanharepeticoes",$dados)) ? $dados["qtdcafemanharepeticoes"] : $row->qtdcafemanharepeticoes;
                $row->qtdalmoco = (array_key_exists("qtdalmoco",$dados)) ? $dados["qtdalmoco"] : $row->qtdalmoco;
                $row->qtdalmocorepeticoes = (array_key_exists("qtdalmocorepeticoes",$dados)) ? $dados["qtdalmocorepeticoes"] : $row->qtdalmocorepeticoes;
                $row->qtdcafetarde = (array_key_exists("qtdcafetarde",$dados)) ? $dados["qtdcafetarde"] : $row->qtdcafetarde;
                $row->qtdcafetarderepeticoes = (array_key_exists("qtdcafetarderepeticoes",$dados)) ? $dados["qtdcafetarderepeticoes"] : $row->qtdcafetarderepeticoes;
                $row->qtdjantar = (array_key_exists("qtdjantar",$dados)) ? $dados["qtdjantar"] : $row->qtdjantar;
                $row->qtdjantarrepeticoes = (array_key_exists("qtdjantarrepeticoes",$dados)) ? $dados["qtdjantarrepeticoes"] : $row->qtdjantarrepeticoes;
                $row->qtdmamadeira = (array_key_exists("qtdmamadeira",$dados)) ? $dados["qtdmamadeira"] : $row->qtdmamadeira;
                $row->qtdmamadeirarepeticoes = (array_key_exists("qtdmamadeirarepeticoes",$dados)) ? $dados["qtdmamadeirarepeticoes"] : $row->qtdmamadeirarepeticoes;
                $row->qtddesjejum = (array_key_exists("qtddesjejum",$dados)) ? $dados["qtddesjejum"] : $row->qtddesjejum;
                $row->qtddesjejumrepeticoes = (array_key_exists("qtddesjejumrepeticoes",$dados)) ? $dados["qtddesjejumrepeticoes"] : $row->qtddesjejumrepeticoes;
                $row->qtdcolacao = (array_key_exists("qtdcolacao",$dados)) ? $dados["qtdcolacao"] : $row->qtdcolacao;
                $row->qtdcolacaorepeticoes = (array_key_exists("qtdcolacaorepeticoes",$dados)) ? $dados["qtdcolacaorepeticoes"] : $row->qtdcolacaorepeticoes;
                $row->qtdlanche = (array_key_exists("qtdlanche",$dados)) ? $dados["qtdlanche"] : $row->qtdlanche;
                $row->qtdlancherepeticoes = (array_key_exists("qtdlancherepeticoes",$dados)) ? $dados["qtdlancherepeticoes"] : $row->qtdlancherepeticoes;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;

               
		$row->save();
		
		return $row;
	}
	
}