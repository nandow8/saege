<?php

/**
 * Define o modelo Departamentossecretarias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Departamentossecretarias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "departamentossecretarias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDepartamentossecretariasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$departamentossecretarias = new Departamentossecretarias();
		return $departamentossecretarias->getDepartamentossecretarias($queries, $page, $maxpage);
	}
	
	public function getDepartamentossecretarias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$_importacao = (isset($queries['_importacao'])) ? $queries['_importacao'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		
		$departamento = (isset($queries["departamento"])) ? $queries["departamento"] : false;
		if ($departamento) array_push($where, " d1.departamento LIKE '%$departamento%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " d1.descricoes = '$descricoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " d1.status LIKE '%$status%' ");

		$_importacao = (isset($queries["_importacao"])) ? $queries["_importacao"] : false;
		if ($_importacao) {
			array_push($where, " (d1.importacao <> 'Sim'  OR d1.importacao IS NULL) ");
		}

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		
		$ordem = "ORDER BY d1.departamento ASC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM departamentossecretarias d1
					
					WHERE d1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDepartamentosecretariaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDepartamentossecretarias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDepartamentosecretariaByIdHelper($id, $queries = array()) {
		$rows = new Departamentossecretarias();
		return $rows->getDepartamentosecretariaById($id, $queries);
	}		
	
	
	public static function getDepartamentosecretariaByNome($departamento, $queries = array()) {
		if ((!isset($departamento)) || (!$departamento) || ($departamento=="")) return false;
		$_rows = new Departamentossecretarias();

		$queries['departamento'] = $departamento;
		$rows = $_rows->getDepartamentossecretarias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Departamentossecretarias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->departamento = (array_key_exists("departamento",$dados)) ? $dados["departamento"] : $row->departamento;
 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
 $row->codigoimportacao = (array_key_exists("codigoimportacao",$dados)) ? $dados["codigoimportacao"] : $row->codigoimportacao;
 $row->importacao = (array_key_exists("importacao",$dados)) ? $dados["importacao"] : $row->importacao;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}