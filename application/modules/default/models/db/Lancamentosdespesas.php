<?php

/**
 * Define o modelo Lancamentosdespesas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Lancamentosdespesas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "lancamentosdespesas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLancamentosdespesasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$lancamentosdespesas = new Lancamentosdespesas();
		return $lancamentosdespesas->getLancamentosdespesas($queries, $page, $maxpage);
	}
	
	public function getLancamentosdespesas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$sum = (isset($queries['sum'])) ? (int)$queries['sum'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " l1.idescola = $idescola ");
		
		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " l1.origem LIKE '%$origem%' ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " l1.idescola = $idescola ");

$idtipoconvenio = (isset($queries["idtipoconvenio"])) ? $queries["idtipoconvenio"] : false;
		if ($idtipoconvenio) array_push($where, " l1.idtipoconvenio = $idtipoconvenio ");

$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " l1.sequencial LIKE '%$sequencial%' ");

$banco = (isset($queries["banco"])) ? $queries["banco"] : false;
		if ($banco) array_push($where, " l1.banco LIKE '%$banco%' ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " l1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " l1.data <= '$data_f' ");

$idtipodespesa = (isset($queries["idtipodespesa"])) ? $queries["idtipodespesa"] : false;
		if ($idtipodespesa) array_push($where, " l1.idtipodespesa = $idtipodespesa ");

$fornecedor = (isset($queries["fornecedor"])) ? $queries["fornecedor"] : false;
		if ($fornecedor) array_push($where, " l1.fornecedor LIKE '%$fornecedor%' ");

$cnpj = (isset($queries["cnpj"])) ? $queries["cnpj"] : false;
		if ($cnpj) array_push($where, " l1.cnpj LIKE '%$cnpj%' ");

$idbanco = (isset($queries["idbanco"])) ? $queries["idbanco"] : false;
		if ($idbanco) array_push($where, " l1.idbanco = $idbanco ");

$descricaodespesa = (isset($queries["descricaodespesa"])) ? $queries["descricaodespesa"] : false;
		if ($descricaodespesa) array_push($where, " l1.descricaodespesa = '$descricaodespesa' ");

$numerocheque = (isset($queries["numerocheque"])) ? $queries["numerocheque"] : false;
		if ($numerocheque) array_push($where, " l1.numerocheque LIKE '%$numerocheque%' ");

$tipodocumento = (isset($queries["tipodocumento"])) ? $queries["tipodocumento"] : false;
		if ($tipodocumento) array_push($where, " l1.tipodocumento LIKE '%$tipodocumento%' ");

$numerodocumento = (isset($queries["numerodocumento"])) ? $queries["numerodocumento"] : false;
		if ($numerodocumento) array_push($where, " l1.numerodocumento LIKE '%$numerodocumento%' ");

$iddocumento = (isset($queries["iddocumento"])) ? $queries["iddocumento"] : false;
		if ($iddocumento) array_push($where, " l1.iddocumento = $iddocumento ");

$valor = (isset($queries["valor"])) ? $queries["valor"] : false;
		if ($valor) array_push($where, " l1.valor = $valor ");

$observacao = (isset($queries["observacao"])) ? $queries["observacao"] : false;
		if ($observacao) array_push($where, " l1.observacao = '$observacao' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*, t1.tipo, b1.banco, b1.agencia, b1.conta"; 
		;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		if ($sum) $fields = "SUM(l1.valor) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM lancamentosdespesas l1
					LEFT JOIN financeirodespesastipos t1 ON t1.id = l1.idtipodespesa
					LEFT JOIN bancos b1 ON b1.id = l1.idbanco
					
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total || $sum) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLancamentosdespesaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLancamentosdespesas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLancamentosdespesaByIdHelper($id, $queries = array()) {
		$rows = new Lancamentosdespesas();
		return $rows->getLancamentosdespesaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Lancamentosdespesas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
 $row->idtipoconvenio = (array_key_exists("idtipoconvenio",$dados)) ? $dados["idtipoconvenio"] : $row->idtipoconvenio;
 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->banco = (array_key_exists("banco",$dados)) ? $dados["banco"] : $row->banco;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->idtipodespesa = (array_key_exists("idtipodespesa",$dados)) ? $dados["idtipodespesa"] : $row->idtipodespesa;
 $row->fornecedor = (array_key_exists("fornecedor",$dados)) ? $dados["fornecedor"] : $row->fornecedor;
 $row->cnpj = (array_key_exists("cnpj",$dados)) ? $dados["cnpj"] : $row->cnpj;
 $row->idbanco = (array_key_exists("idbanco",$dados)) ? $dados["idbanco"] : $row->idbanco;
 $row->descricaodespesa = (array_key_exists("descricaodespesa",$dados)) ? $dados["descricaodespesa"] : $row->descricaodespesa;
 $row->numerocheque = (array_key_exists("numerocheque",$dados)) ? $dados["numerocheque"] : $row->numerocheque;
 $row->idcheque = (array_key_exists("idcheque",$dados)) ? $dados["idcheque"] : $row->idcheque;
 $row->tipodocumento = (array_key_exists("tipodocumento",$dados)) ? $dados["tipodocumento"] : $row->tipodocumento;
 $row->numerodocumento = (array_key_exists("numerodocumento",$dados)) ? $dados["numerodocumento"] : $row->numerodocumento;
 $row->iddocumento = (array_key_exists("iddocumento",$dados)) ? $dados["iddocumento"] : $row->iddocumento;
 $row->valor = (array_key_exists("valor",$dados)) ? $dados["valor"] : $row->valor;
 $row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}