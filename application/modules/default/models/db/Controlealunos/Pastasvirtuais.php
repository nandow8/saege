<?php

class Controlealunos_Pastasvirtuais extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "controlealunospastasvirtuais";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_TIPO_PARTICULAR = 'particular';
	public static $_TIPO_PUBLICA = 'publica';
	
	/**
	 * 
	 */
	public static function getTipos($field = false) {
		$res = array(
			self::$_TIPO_PARTICULAR => 'Particular',
			self::$_TIPO_PUBLICA => 'Pública',
		);
		
		if (!$field) return $res;
		return $res[$field];
	}
	
	public static function getPastasvirtuaisHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Controlealunos_Pastasvirtuais();
		return $produtos->getPastasvirtuais($queries, $page, $maxpage);
	}
	
	public function getPastasvirtuais($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;	
		$idparent = (isset($queries['idparent'])) ? (int)$queries['idparent'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$idusuario = (isset($queries['idusuario'])) ? (int)$queries['idusuario'] : false;
		$parentzero = (isset($queries['parentzero'])) ? $queries['parentzero'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$tipo = (isset($queries['tipo'])) ? $queries['tipo'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " pv1.id=$id ");
		if ($idescola) array_push($where, " pv1.idescola=$idescola ");		
		if ($idusuario) array_push($where, " pv1.idusuario=$idusuario ");
		if ($idparent) array_push($where, " pv1.idparent=$idparent ");		
		if ($chave) array_push($where, " ((pv1.titulo LIKE '%$chave%') OR (pv1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " pv1.status='$status' ");
		if ($tipo) array_push($where, " pv1.tipo='$tipo' ");
		if ($parentzero) array_push($where, " pv1.idparent=0 ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "pv1.*";
		if ($total) $fields = "COUNT(pv1.id) as total";
		
		
		$ordem = "ORDER BY pv1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM controlealunospastasvirtuais pv1
					WHERE pv1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getPastasvirtuaisById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPastasvirtuais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPastasvirtuaisByIdHelper($id, $queries = array()) {
		$rows = new Controlealunos_Pastasvirtuais();
		return $rows->getPastasvirtuaisById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Pastasvirtuais
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s');
			$row->idusuariocriacao = (array_key_exists('idusuariocriacao',$dados)) ? $dados['idusuariocriacao'] : $row->idusuariocriacao; 		
		}else {
			$novoRegistro = false;
		} 
		
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->idparent = (array_key_exists('idparent',$dados)) ? $dados['idparent'] : $row->idparent;
		$row->idusuario = (array_key_exists('idusuario',$dados)) ? $dados['idusuario'] : $row->idusuario;
		$row->titulo = (array_key_exists('titulo',$dados)) ? $dados['titulo'] : $row->titulo;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;		
		$row->tipo = (array_key_exists('tipo',$dados)) ? $dados['tipo'] : $row->tipo;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
	
	
}