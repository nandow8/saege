<?php

class Controlealunos_Professoresmaterias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "controleprofessoresmaterias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getProfessoresmateriasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Controlealunos_Professoresmaterias();
		return $produtos->getProfessoresmaterias($queries, $page, $maxpage);
	}
	
	public function getProfessoresmaterias($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$idprofessor = (isset($queries['idprofessor'])) ? (int)$queries['idprofessor'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$left_materias = (isset($queries['left_materias'])) ? $queries['left_materias'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " pf1.id=$id ");	
		if ($idescola) array_push($where, " pf1.idescola=$idescola ");
		if ($idprofessor) array_push($where, " pf1.idprofessor=$idprofessor ");
		//if ($chave) array_push($where, " ((pf1.materia LIKE '%$chave%') OR (pf1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " pf1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "pf1.*, eu1.nomerazao, eu1.sobrenomefantasia";
		if ($left_materias) $fields = "pf1.*, m1.materia, m1.id as m1id";
		if ($total) $fields = "COUNT(pf1.id) as total";
		
		
		$ordem = "ORDER BY pf1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM controleprofessoresmaterias pf1
						LEFT JOIN escolasusuarios eu1 ON eu1.id = pf1.idprofessor
					WHERE pf1.excluido='nao'
						$w 
					$ordem	
					$limit";	
					
		if ($left_materias){
			$strsql = "SELECT $fields 
					FROM controleprofessoresmaterias pf1
						LEFT JOIN escolasusuarios eu1 ON eu1.id = pf1.idprofessor
						LEFT JOIN controlealunosmaterias m1 ON FIND_IN_SET(m1.id, pf1.idsmaterias)
					WHERE pf1.excluido='nao'
						$w 
					$ordem	
					$limit";
		//die($strsql);
		}
		//if(!$total) die($strsql);
		//LEFT JOIN diplomasalunos da1 ON FIND_IN_SET(da1.id, d1.idsalunos)
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getProfessormateriaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getProfessoresmaterias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getProfessormateriaByIdHelper($id, $queries = array()) {
		$rows = new Controlealunos_Professoresmaterias();
		return $rows->getProfessormateriaById($id, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Professoresmaterias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row){
                    $row = $this->createRow();
                    $row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->idprofessor = (array_key_exists('idprofessor',$dados)) ? $dados['idprofessor'] : $row->idprofessor;	
		$row->idsmaterias = (array_key_exists('idsmaterias',$dados)) ? $dados['idsmaterias'] : $row->idsmaterias;	
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		if ((int)$row->idescola==0) {
			$row->idescola = Escolasusuarios::getEscolaAtiva(Escolasusuarios::getUsuario('id'), 'id');
		}
		$row->save();

		return $row;
	}
	
	
}