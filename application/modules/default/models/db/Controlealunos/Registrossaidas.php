<?php

class Controlealunos_Registrossaidas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "controlealunosregistrossaidas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getRegistrossaidasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Controlealunos_Registrossaidas();
		return $produtos->getRegistrossaidas($queries, $page, $maxpage);
	}
	
	public function getRegistrossaidas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;		
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " rs1.id=$id ");
		if ($idaluno) array_push($where, " rs1.idaluno=$idaluno ");		
		if ($idescola) array_push($where, " rs1.idescola=$idescola ");
		$resumo = (isset($queries["resumo"])) ? $queries["resumo"] : false;
		if ($resumo) array_push($where, " r1.resumo LIKE '%$resumo%' ");

		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " r1.descricoes LIKE '%$descricoes%' ");
		if ($chave) array_push($where, " ((rs1.titulo LIKE '%$chave%') OR (rs1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " rs1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "rs1.*, e1.escola";
		if ($total) $fields = "COUNT(rs1.id) as total";
		
		
		$ordem = "ORDER BY rs1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM controlealunosregistrossaidas rs1
						LEFT JOIN escolas e1 ON e1.id = rs1.idescola
					WHERE rs1.excluido='nao'
						AND e1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getRegistrosaidaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getRegistrossaidas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getRegistrosaidaByIdHelper($id, $queries = array()) {
		$rows = new Controlealunos_Registrossaidas();
		return $rows->getRegistrosaidaById($id, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Registrossaidas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row){
                    $row = $this->createRow();
                    $row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->idaluno = (array_key_exists('idaluno',$dados)) ? $dados['idaluno'] : $row->idaluno;
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;		
		$row->aluno = (array_key_exists('aluno',$dados)) ? $dados['aluno'] : $row->aluno;		
		$row->titulo = (array_key_exists('titulo',$dados)) ? $dados['titulo'] : $row->titulo;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;	
		$row->data = (array_key_exists('data',$dados)) ? $dados['data'] : $row->data;	
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
	
	
}