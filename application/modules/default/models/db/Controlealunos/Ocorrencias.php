<?php

class Controlealunos_Ocorrencias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "controlealunosocorrencias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getOcorrenciasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Controlealunos_Ocorrencias();
		return $produtos->getOcorrencias($queries, $page, $maxpage);
	}
	
	public function getOcorrencias($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;	
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " o1.id=$id ");	
		if ($idescola) array_push($where, " o1.idescola=$idescola ");
		
		if ($chave) array_push($where, " ((o1.ocorrencia LIKE '%$chave%') OR (o1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " o1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "o1.*, e1.escola";
		if ($total) $fields = "COUNT(o1.id) as total";
		
		
		$ordem = "ORDER BY o1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM controlealunosocorrencias o1
						LEFT JOIN escolas e1 ON e1.id = o1.idescola
					WHERE o1.excluido='nao'
						AND e1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getOcorrenciaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getOcorrencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getOcorrenciaByIdHelper($id, $queries = array()) {
		$rows = new Controlealunos_Ocorrencias();
		return $rows->getOcorrenciaById($id, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ocorrencias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row){
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;	
		$row->ocorrencia = (array_key_exists('ocorrencia',$dados)) ? $dados['ocorrencia'] : $row->ocorrencia;	
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
	
	
}