<?php

class Controlealunos_Professoresnotas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "controleprofessoresnotas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_BIMESTRE_1 = '1';
	public static $_BIMESTRE_2 = '2';
	public static $_BIMESTRE_3 = '3';
	public static $_BIMESTRE_4 = '4';	
	
	/**
	 * 
	 */
	public static function getBimestres($field = false) {
		$res = array(
			self::$_BIMESTRE_1 => '1° Bimestre',
			self::$_BIMESTRE_2 => '2° Bimestre',
			self::$_BIMESTRE_3 => '3° Bimestre',
			self::$_BIMESTRE_4 => '4° Bimestre'
		);
		
		if (!$field) return $res;
		return $res[$field];
	}
	
	public static function getProfessoresnotasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Controlealunos_Professoresnotas();
		return $produtos->getProfessoresnotas($queries, $page, $maxpage);
	}
	
	public function getProfessoresnotas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idvinculo = (isset($queries['idvinculo'])) ? (int)$queries['idvinculo'] : false;
		$idprofessor = (isset($queries['idprofessor'])) ? (int)$queries['idprofessor'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " pn1.id=$id ");
		if ($idvinculo) array_push($where, " pn1.idvinculo=$idvinculo ");	
		if ($idescola) array_push($where, " pn1.idescola=$idescola ");
		if ($idprofessor) array_push($where, " pn1.idprofessor=$idprofessor ");
		
		//if ($chave) array_push($where, " ((pn1.materia LIKE '%$chave%') OR (pn1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " pn1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "pn1.*";
		if ($total) $fields = "COUNT(pn1.id) as total";
		
		
		$ordem = "ORDER BY pn1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM controleprofessoresnotas pn1
					WHERE pn1.excluido='nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getProfessornotaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getProfessoresnotas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getProfessornotaByIdHelper($id, $queries = array()) {
		$rows = new Controlealunos_Professoresnotas();
		return $rows->getProfessornotaById($id, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Professoresnotas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row){
                    $row = $this->createRow();
                    $row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->idmateria = (array_key_exists('idmateria',$dados)) ? $dados['idmateria'] : $row->idmateria;
		$row->idvinculo = (array_key_exists('idvinculo',$dados)) ? $dados['idvinculo'] : $row->idvinculo;
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;	
		$row->idprofessor = (array_key_exists('idprofessor',$dados)) ? $dados['idprofessor'] : $row->idprofessor;
		$row->bimestre = (array_key_exists('bimestre',$dados)) ? $dados['bimestre'] : $row->bimestre;	
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();
		if (array_key_exists('itens', $dados)) $this->setItens($dados['itens'], $row->id, $row->logusuario, $row->logdata);
		return $row;
	}
	
	private function setItens($dados, $idprofessoresnotas, $logusuario = false, $logdata = false) {
		
		if (!is_array($dados)) return;
		
		$itens = new Controlealunos_Notas();
		//$idsitens = $dados['idsitens'];
		$idsalunos = $dados['idsalunos'];
		$idsmaterias = $dados['idsmaterias'];
		$trabalhos1 = $dados['trabalhos1'];
		$trabalhos2 = $dados['trabalhos2'];
		$trabalhos3 = $dados['trabalhos3'];
		
		$notas = $dados['notas'];
		$notas2 = $dados['notas2'];
		$notas3 = $dados['notas3'];
		
		$ids = array();
		foreach ($idsalunos as $i=>$id) {
			$d = array();
	
			$d['id'] = Controlealunos_Notas::getIdByIdAlunoMateriaHelper($idsmaterias[$i], $idsalunos[$i], array('idprofessoresnotas'=>$idprofessoresnotas));
			$d['idprofessoresnotas'] = $idprofessoresnotas; 
			$d['idprofessor'] = $logusuario;
			$d['idmateria'] = trim(strip_tags($idsmaterias[$i]));
			$d['trabalho1'] = trim(strip_tags(Mn_Util::trataNum($trabalhos1[$i])));
			$d['trabalho2'] = trim(strip_tags(Mn_Util::trataNum($trabalhos2[$i])));
			$d['trabalho3'] = trim(strip_tags(Mn_Util::trataNum($trabalhos3[$i])));
			
			$d['nota'] = trim(strip_tags(Mn_Util::trataNum($notas[$i])));
			$d['nota2'] = trim(strip_tags(Mn_Util::trataNum($notas2[$i])));
			$d['nota3'] = trim(strip_tags(Mn_Util::trataNum($notas3[$i])));
			$d['idaluno'] = trim(strip_tags($idsalunos[$i]));

			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $logusuario;
			$d['logdata'] = $logdata;
			//var_dump($d); die();
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
			//verificar as linhas comentadas acima, elas são responsaveis pelas 2 linhas mais cridas no cronogramasliehistorico
		}
		//die('aqui');
		/*
		$idsexcluir = implode(",", $ids);
		
		$excluidos = Escolassalasatribuicoes::getSalasatribuicoesHelper(array('idescolasalasaluno'=>$idescolasalasaluno, 'notids'=>$idsexcluir));
		$rows = new Escolassalasatribuicoes();
		foreach ($excluidos as $e=>$excluido):
			$excluido['excluido'] = 'sim';
			$excluido['logusuario'] = $logusuario;
			$excluido['logdata'] = $logdata;
			//
			$rows->save($excluido);	
		endforeach;
		*/
		
	}
}