<?php

class Controlealunos_Frequencias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "controlealunosfrequencias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_PRESENCA_PRESENTE = 'presente';
	public static $_PRESENCA_AUSENTE = 'ausente';
	
	/**
	 * 
	 */
	public static function getPresencas($field = false) {
		$res = array(
			self::$_PRESENCA_PRESENTE => 'Presente',
			self::$_PRESENCA_AUSENTE => 'Ausente',
		);
		
		if (!$field) return $res;
		return $res[$field];
	}
	
	public static function getFrequenciasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Controlealunos_Frequencias();
		return $produtos->getFrequencias($queries, $page, $maxpage);
	}
	
	public function getFrequencias($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;	
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		$idprofessor = (isset($queries['idprofessor'])) ? (int)$queries['idprofessor'] : false;
		$idmateria = (isset($queries['idmateria'])) ? (int)$queries['idmateria'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		
		$datainicio = (isset($queries['datainicio'])) ? $queries['datainicio'] : false;
		$datafim = (isset($queries['datafim'])) ? $queries['datafim'] : false;
		
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$presenca = (isset($queries['presenca'])) ? $queries['presenca'] : false;
		$total = (isset($queries['total'])) ? $queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		//data
		$where = array();
		
		if ($id) array_push($where, " f1.id=$id ");	
		if ($idescola) array_push($where, " f1.idescola=$idescola ");
		if ($idaluno) array_push($where, " f1.idaluno=$idaluno ");
		if ($idprofessor) array_push($where, " f1.idprofessor=$idprofessor ");
		if ($idmateria) array_push($where, " f1.idmateria=$idmateria ");
		
		if ($chave) array_push($where, " ((e1.escola LIKE '%$chave%') OR (m1.materia LIKE '%$chave%')) ");
		
		if ($datainicio) array_push($where, " f1.data >= '$datainicio' ");
		if ($datafim) array_push($where, " f1.data <= '$datafim' ");
		
		if ($status) array_push($where, " f1.status='$status' ");		
		if ($presenca) array_push($where, " f1.presenca='$presenca' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*, e1.escola, m1.materia";
		if ($total) $fields = "COUNT(f1.id) as total";
		
		
		$ordem = "ORDER BY f1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM controlealunosfrequencias f1
						LEFT JOIN escolas e1 ON e1.id = f1.idescola
						LEFT JOIN controlealunosmaterias m1 ON m1.id = f1.idmateria  
					WHERE f1.excluido='nao'
						AND e1.excluido='nao'
						AND m1.excluido='nao'  
						$w 
					$ordem	
					$limit";	
		//if(!total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getFrequenciaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFrequencias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFrequenciaByIdHelper($id, $queries = array()) {
		$rows = new Controlealunos_Frequencias();
		return $rows->getFrequenciaById($id, $queries);
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Frequencias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row){
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->idprofessor = (array_key_exists('idprofessor',$dados)) ? $dados['idprofessor'] : $row->idprofessor;
		$row->idescolausuario = (array_key_exists('idescolausuario',$dados)) ? $dados['idescolausuario'] : $row->idescolausuario;
		$row->idmateria = (array_key_exists('idmateria',$dados)) ? $dados['idmateria'] : $row->idmateria;
		$row->idaluno = (array_key_exists('idaluno',$dados)) ? $dados['idaluno'] : $row->idaluno;	
		$row->data = (array_key_exists('data',$dados)) ? $dados['data'] : $row->data;
		$row->presenca = (array_key_exists('presenca',$dados)) ? $dados['presenca'] : $row->presenca;
		$row->observacoes = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		if (array_key_exists('itens', $dados)) $this->setItens($dados['itens'], $row->id, $row->idmateria, $row->logusuario, $row->logdata);
		
		return $row;
	}
	
	private function setItens($dados, $idfrequencia, $idmateria, $logusuario = false, $logdata = false) {
		
		if (!is_array($dados)) return;
		 
		$itens = new Controlealunos_Frequenciasitens();
		//$idsitens = $dados['idsitens'];
		$idsalunos = $dados['idsalunos'];
		$idsseries = $dados['idsseries'];
		$idsturmas = $dados['idsturmas'];
		$presencas = $dados['presencas'];
		
		$ids = array();
		foreach ($idsalunos as $i=>$id) {
			$d = array();
			
			$d['id'] = Controlealunos_Frequenciasitens::getIdByIdAlunoMateriaHelper($idmateria, $idsalunos[$i], array('idfrequencia'=>$idfrequencia));
			$d['idfrequencia'] = $idfrequencia; 
			$d['idprofessor'] = $logusuario;
			$d['idmateria'] = $idmateria;
			$d['idaluno'] = trim(strip_tags($idsalunos[$i]));
			$d['idserie'] = trim(strip_tags($idsseries[$i]));
			$d['idturma'] = trim(strip_tags($idsturmas[$i]));
			
			$d['presenca'] = trim(strip_tags($presencas[$i]));
			
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $logusuario;
			$d['logdata'] = $logdata;
			//var_dump($d); die();
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
			//verificar as linhas comentadas acima, elas são responsaveis pelas 2 linhas mais cridas no cronogramasliehistorico
		}
		//die('aqui');
		/*
		$idsexcluir = implode(",", $ids);
		
		$excluidos = Escolassalasatribuicoes::getSalasatribuicoesHelper(array('idescolasalasaluno'=>$idescolasalasaluno, 'notids'=>$idsexcluir));
		$rows = new Escolassalasatribuicoes();
		foreach ($excluidos as $e=>$excluido):
			$excluido['excluido'] = 'sim';
			$excluido['logusuario'] = $logusuario;
			$excluido['logdata'] = $logdata;
			//
			$rows->save($excluido);	
		endforeach;
		*/
		
	}
}