<?php

class Controlealunos_Notas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "controlealunosnotas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getNotasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Controlealunos_Notas();
		return $produtos->getNotas($queries, $page, $maxpage);
	}
	
	public function getNotas($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idprofessoresnotas = (isset($queries['idprofessoresnotas'])) ? (int)$queries['idprofessoresnotas'] : false;
		$idprofessor = (isset($queries['idprofessor'])) ? (int)$queries['idprofessor'] : false;
		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		$idmateria = (isset($queries['idmateria'])) ? (int)$queries['idmateria'] : false;
		$bimestre = (isset($queries['bimestre'])) ? (int)$queries['bimestre'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$left_materias = (isset($queries['left_materias'])) ? $queries['left_materias'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " n1.id=$id ");
		if ($idprofessoresnotas) array_push($where, " n1.idprofessoresnotas=$idprofessoresnotas ");	
		if ($idprofessor) array_push($where, " n1.idprofessor=$idprofessor ");
		if ($idaluno) array_push($where, " n1.idaluno=$idaluno ");
		if ($idmateria) array_push($where, " n1.idmateria=$idmateria ");
		if ($bimestre) array_push($where, " pf1.bimestre=$bimestre ");
		//if ($chave) array_push($where, " ((n1.materia LIKE '%$chave%') OR (n1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " n1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "n1.*, pf1.bimestre";
		
		if ($total) $fields = "COUNT(n1.id) as total";
		
		
		$ordem = "ORDER BY n1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM controlealunosnotas n1
						LEFT JOIN controleprofessoresnotas pf1 ON pf1.id = n1.idprofessoresnotas
					WHERE n1.excluido='nao'
						AND pf1.excluido='nao'
						$w 
					$ordem	
					$limit";	
					
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getNotaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNotas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNotaByIdHelper($id, $queries = array()) {
		$rows = new Controlealunos_Notas();
		return $rows->getNotaById($id, $queries);
	}		
	
	public function getIdByIdAlunoMateria($idmateria, $idaluno, $queries = array()) {
		if ($idmateria==0) return false;
		if ($idaluno==0) return false;
		
		$queries['idmateria'] = $idmateria;
		$queries['idaluno'] = $idaluno;
		$rows = $this->getNotas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$notas = $rows[0];
		
		return $notas['id'];
	}
	
	public static function getIdByIdAlunoMateriaHelper($idmateria, $idaluno, $queries = array()) {
		if ($idmateria==0) return false;
		if ($idaluno==0) return false;
		
		$rows = new Controlealunos_Notas();
		return $rows->getIdByIdAlunoMateria($idmateria, $idaluno, $queries);
	}

	public static function getIdByIdNotasAlunoMateriaHelper($idmateria, $idaluno, $queries = array()) {
		if ($idmateria==0) return false;
		if ($idaluno==0) return false;
		
		$rows = new Controlealunos_Notas();
		return $rows->getIdByIdNotasAlunoMateria($idmateria, $idaluno, $queries);
	}
	
	public function getIdByIdNotasAlunoMateria($idmateria, $idaluno, $queries = array()) {
		if ($idmateria==0) return false;
		if ($idaluno==0) return false;
		
		$queries['idmateria'] = $idmateria;
		$queries['idaluno'] = $idaluno;
		$rows = $this->getNotas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$notas = $rows[0];
		
		return $notas;
	}
	
	
	public function getNotaByIdAlunoMateria($idprofessoresnotas, $idprofessor, $idmateria, $idaluno, $queries = array()) {
		if ($idprofessoresnotas==0) return false;
		if ($idprofessor==0) return false;
		if ($idmateria==0) return false;
		if ($idaluno==0) return false;
		
		$queries['idprofessoresnotas'] = $idprofessoresnotas;
		$queries['idprofessor'] = $idprofessor;
		$queries['idmateria'] = $idmateria;
		$queries['idaluno'] = $idaluno;
		$rows = $this->getNotas($queries, 0, 0);
		
		if (sizeof($rows)==0) return 0.00;
		
		$notas = $rows[0];
		
		//return $notas['nota'];
		return $rows[0];
	}

	
	public static function getNotaByIdAlunoMateriaHelper($idprofessoresnotas, $idprofessor, $idmateria, $idaluno, $queries = array()) {
		if ($idprofessoresnotas==0) return false;
		if ($idprofessor==0) return false;
		if ($idmateria==0) return false;
		if ($idaluno==0) return false;
		
		$rows = new Controlealunos_Notas();
		return $rows->getNotaByIdAlunoMateria($idprofessoresnotas, $idprofessor, $idmateria, $idaluno, $queries);
	}	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Notas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row){
                    $row = $this->createRow();
                    $row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->idprofessoresnotas = (array_key_exists('idprofessoresnotas',$dados)) ? $dados['idprofessoresnotas'] : $row->idprofessoresnotas;	
		$row->idaluno = (array_key_exists('idaluno',$dados)) ? $dados['idaluno'] : $row->idaluno;
		$row->idprofessor = (array_key_exists('idprofessor',$dados)) ? $dados['idprofessor'] : $row->idprofessor;	
		$row->idmateria = (array_key_exists('idmateria',$dados)) ? $dados['idmateria'] : $row->idmateria;
		$row->nota = (array_key_exists('nota',$dados)) ? $dados['nota'] : $row->nota;
		$row->nota2 = (array_key_exists('nota',$dados)) ? $dados['nota2'] : $row->nota2;
		$row->nota3 = (array_key_exists('nota3',$dados)) ? $dados['nota3'] : $row->nota3;
		$row->trabalho1 = (array_key_exists('trabalho1',$dados)) ? $dados['trabalho1'] : $row->trabalho1;
		$row->trabalho2 = (array_key_exists('trabalho2',$dados)) ? $dados['trabalho2'] : $row->trabalho2;
		$row->trabalho3 = (array_key_exists('trabalho3',$dados)) ? $dados['trabalho3'] : $row->trabalho3;
		$row->mediafinal = (array_key_exists('mediafinal',$dados)) ? $dados['mediafinal'] : $row->mediafinal;	
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
	
	
}