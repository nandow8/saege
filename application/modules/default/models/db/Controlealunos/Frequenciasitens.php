<?php

class Controlealunos_Frequenciasitens extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "controlealunosfrequenciasitens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getFrequenciasitensHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Controlealunos_Frequenciasitens();
		return $produtos->getFrequenciasitens($queries, $page, $maxpage);
	}
	
	public function getFrequenciasitens($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idfrequencia = (isset($queries['idfrequencia'])) ? (int)$queries['idfrequencia'] : false;
		$idprofessor = (isset($queries['idprofessor'])) ? (int)$queries['idprofessor'] : false;
		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		$idmateria = (isset($queries['idmateria'])) ? (int)$queries['idmateria'] : false;
		
		$datainicio = (isset($queries['datainicio'])) ? $queries['datainicio'] : false;
		$datafim = (isset($queries['datafim'])) ? $queries['datafim'] : false;
		
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$presenca = (isset($queries['presenca'])) ? $queries['presenca'] : false;
		$left_materias = (isset($queries['left_materias'])) ? $queries['left_materias'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " fi1.id=$id ");
		if ($idfrequencia) array_push($where, " fi1.idfrequencia=$idfrequencia ");	
		if ($idprofessor) array_push($where, " fi1.idprofessor=$idprofessor ");
		if ($idaluno) array_push($where, " fi1.idaluno=$idaluno ");
		if ($idmateria) array_push($where, " fi1.idmateria=$idmateria ");
		if ($presenca) array_push($where, " fi1.presenca='$presenca' ");
		if ($datainicio) array_push($where, " f1.data >= '$datainicio' ");
		if ($datafim) array_push($where, " f1.data <= '$datafim' ");
		
		//if ($chave) array_push($where, " ((fi1.materia LIKE '%$chave%') OR (fi1.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " fi1.status='$status' ");
		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "fi1.*, f1.data";
		
		if ($total) $fields = "COUNT(fi1.id) as total";
		
		
		$ordem = "ORDER BY fi1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM controlealunosfrequenciasitens fi1
						LEFT JOIN controlealunosfrequencias f1 ON f1.id = fi1.idfrequencia
					WHERE fi1.excluido='nao'
						AND f1.excluido = 'nao'
						$w 
					$ordem	
					$limit";
		//echo $strsql;	
		//die($strsql);
		//if(!total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getFrequenciaitemById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFrequenciasitens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFrequenciaitemByIdHelper($id, $queries = array()) {
		$rows = new Controlealunos_Frequenciasitens();
		return $rows->getFrequenciaitemById($id, $queries);
	}		
	
	public function getIdByIdAlunoMateria($idmateria, $idaluno, $queries = array()) {
		if ($idmateria==0) return false;
		if ($idaluno==0) return false;
		
		$queries['idmateria'] = $idmateria;
		$queries['idaluno'] = $idaluno;
		$rows = $this->getFrequenciasitens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$notas = $rows[0];
		
		return $notas['id'];
	}

	
	public static function getIdByIdAlunoMateriaHelper($idmateria, $idaluno, $queries = array()) {
		if ($idmateria==0) return false;
		if ($idaluno==0) return false;
		
		$rows = new Controlealunos_Frequenciasitens();
		return $rows->getIdByIdAlunoMateria($idmateria, $idaluno, $queries);
	}
		
	public function getFrequenciaByIdAlunoMateria($idaluno, $idmateria, $queries = array()) {
		if ($idaluno==0) return false;
		if ($idmateria==0) return false;
		
		$queries['idaluno'] = $idaluno;
		$queries['idmateria'] = $idmateria;
		
		if(isset($queries['bimestre'])){
			$idbimestre = $queries['bimestre'];
			if($idbimestre==1){
				$datainicio = '2016-01-01';
				$datafim = '2016-03-31';
				
				$queries['datainicio'] = $datainicio;
				$queries['datafim'] = $datafim;
			}elseif($idbimestre==2){
				$datainicio = '2016-04-01';
				$datafim = '2016-06-30';
				
				$queries['datainicio'] = $datainicio;
				$queries['datafim'] = $datafim;
			}elseif($idbimestre==3){
				$datainicio = '2016-07-01';
				$datafim = '2016-09-30';
				
				$queries['datainicio'] = $datainicio;
				$queries['datafim'] = $datafim;
			}elseif($idbimestre==4){
				$datainicio = '2016-10-01';
				$datafim = '2016-12-31';
				
				$queries['datainicio'] = $datainicio;
				$queries['datafim'] = $datafim;
			}
		}
		
		$rows = $this->getFrequenciasitens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFrequenciaByIdAlunoMateriaHelper($idaluno, $idmateria, $queries = array()) {
		$rows = new Controlealunos_Frequenciasitens();
		return $rows->getFrequenciaByIdAlunoMateria($idaluno, $idmateria, $queries);
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Frequenciasitens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row){
                    $row = $this->createRow();
                    $row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->idfrequencia = (array_key_exists('idfrequencia',$dados)) ? $dados['idfrequencia'] : $row->idfrequencia;	
		$row->idaluno = (array_key_exists('idaluno',$dados)) ? $dados['idaluno'] : $row->idaluno;
		$row->idprofessor = (array_key_exists('idprofessor',$dados)) ? $dados['idprofessor'] : $row->idprofessor;	
		$row->idmateria = (array_key_exists('idmateria',$dados)) ? $dados['idmateria'] : $row->idmateria;
		$row->idserie = (array_key_exists('idserie',$dados)) ? $dados['idserie'] : $row->idserie;
		$row->idturma = (array_key_exists('idturma',$dados)) ? $dados['idturma'] : $row->idturma;
		$row->presenca = (array_key_exists('presenca',$dados)) ? $dados['presenca'] : $row->presenca;
	
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
	
	
}