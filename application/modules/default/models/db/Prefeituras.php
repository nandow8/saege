<?php

class Prefeituras extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "prefeituras";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getPrefeiturasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Prefeituras();
		return $produtos->getPrefeituras($queries, $page, $maxpage);
	}
	
	public function getPrefeituras($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idestado = (isset($queries['idestado'])) ? (int)$queries['idestado'] : false;
		$idcidade = (isset($queries['idcidade'])) ? (int)$queries['idcidade'] : false;
		$idssecretarias = (isset($queries['idssecretarias'])) ? $queries['idssecretarias'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$c1nome = (isset($queries['c1nome'])) ? $queries['c1nome'] : false;
		$cidade = (isset($queries['cidade'])) ? $queries['cidade'] : false;
		$groupestado = (isset($queries['groupestado'])) ? $queries['groupestado'] : false;
		$groupcidade = (isset($queries['groupcidade'])) ? $queries['groupcidade'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$leftenderecos = (isset($queries['leftenderecos'])) ? $queries['leftenderecos'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " p1.id=$id ");
		if ($idestado) array_push($where, " p1.idestado=$idestado ");
		if ($idcidade) array_push($where, " p1.idcidade=$idcidade ");
		if ($chave) array_push($where, " ((p1.texto LIKE '%$chave%')) ");
		if ($idssecretarias) array_push($where, " FIND_IN_SET('$idssecretarias', p1.idssecretarias)  "); 
		if ($status) array_push($where, " p1.status='$status' ");
		if (($cidade) && ($leftenderecos) && (!$c1nome)) array_push($where, " en1.cidade='$cidade' ");
		if (($c1nome) && ($leftenderecos)) array_push($where, " c1.nome='$c1nome' ");
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*, e1.nome as e1nome, c1.nome as c1nome ";
		if ($leftenderecos) $fields = "p1.*, e1.nome as e1nome, en1.cidade as c1nome, c1.nome";
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$left = "
			LEFT JOIN estados e1 ON e1.id = p1.idestado 
			LEFT JOIN cidades c1 ON c1.id = p1.idcidade 
		";
		$ordem = "ORDER BY p1.id";
		if ($order) $ordem = $order; 
		if ($groupestado) $ordem = "GROUP BY e1.nome ";
		if ($groupcidade) $ordem = "GROUP BY c1.nome ";
		if ($leftenderecos) {

			$left = "
			    LEFT JOIN enderecos en1 ON en1.id = p1.idendereco
				LEFT JOIN estados e1 ON e1.id = p1.idestado 
				LEFT JOIN cidades c1 ON c1.id = p1.idcidade 
			";
		}
		//GROUP BY e1.nome;
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM prefeituras p1 
						$left
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if ($leftenderecos) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getPrefeituraById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPrefeituras($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPrefeituraByIdHelper($id, $queries = array()) {
		$rows = new Prefeituras();
		return $rows->getPrefeituraById($id, $queries);
	}	
	
	
	
	public function getPrefeituraByIdEstadoIdCidade($idestado, $idcidade, $queries = array()) {
		if ($idestado==0) return false;
		if ($idcidade==0) return false;
		
		$queries['idestado'] = $idestado;
		$queries['idcidade'] = $idcidade;
		$rows = $this->getPrefeituras($queries, 0, 0);
		//var_dump($rows); die();
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}

	public function getPrefeituraByIdEstadoCidade($idestado, $cidade, $queries = array()) {
		if ($idestado==0) return false;
		if ($cidade=="") return false;
		
		$queries['idestado'] = $idestado;
		$queries['cidade'] = $cidade;
		$rows = $this->getPrefeituras($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPrefeituraByIdEstadoIdCIdadeHelper($idestado, $idcidade, $queries = array()) {
		$rows = new Prefeituras();
		return $rows->getPrefeituraByIdEstadoIdCidade($idestado, $idcidade, $queries);
	}	

	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Prefeituras
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s'); 		
		}else {
			$novoRegistro = false;
		} 

		if (($novoRegistro) || (!$dados['idimagem']===false)) {
			$row->idimagem = (isset($dados['idimagem'])) ? $dados['idimagem'] : $row->idimagem;
		}
		$row->idendereco = (array_key_exists('idendereco',$dados)) ? $dados['idendereco'] : $row->idendereco;
		$row->idestado = (array_key_exists('idestado',$dados)) ? (int)$dados['idestado'] : (int)$row->idestado;
		$row->idcidade = (array_key_exists('idcidade',$dados)) ? (int)$dados['idcidade'] : (int)$row->idcidade;
		$row->idssecretarias = (array_key_exists('idssecretarias',$dados)) ? $dados['idssecretarias'] : $row->idssecretarias;
		
		$row->cpfcnpj = (array_key_exists('cpfcnpj',$dados)) ? $dados['cpfcnpj'] : $row->cpfcnpj;
		$row->telefone = (array_key_exists('telefone',$dados)) ? $dados['telefone'] : $row->telefone;
		$row->contato = (array_key_exists('contato',$dados)) ? $dados['contato'] : $row->contato;
		
		$row->datacontrato = (array_key_exists('datacontrato',$dados)) ? $dados['datacontrato'] : $row->datacontrato;
		$row->databloqueio = (array_key_exists('databloqueio',$dados)) ? $dados['databloqueio'] : $row->databloqueio;
		$row->prefeito = (array_key_exists('prefeito',$dados)) ? $dados['prefeito'] : $row->prefeito;
		$row->secretario = (array_key_exists('secretario',$dados)) ? $dados['secretario'] : $row->secretario;
		$row->partido = (array_key_exists('partido',$dados)) ? $dados['partido'] : $row->partido;
		$row->texto = (array_key_exists('texto',$dados)) ? $dados['texto'] : $row->texto;	
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		//var_dump($row->idimagem); die('aa');
		$row->save();

		return $row;
	}
	
	
}