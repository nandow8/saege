<?php

/**
 * Define o modelo Rhsolicitacoescontascorrentes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Rhsolicitacoescontascorrentes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "rhsolicitacoescontascorrentes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getRhsolicitacoescontascorrentesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$rhsolicitacoescontascorrentes = new Rhsolicitacoescontascorrentes();
		return $rhsolicitacoescontascorrentes->getRhsolicitacoescontascorrentes($queries, $page, $maxpage);
	}
	
	public function getRhsolicitacoescontascorrentes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");

		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " r1.origem = '$origem' ");

		$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " r1.idfuncionario = $idfuncionario ");		
		
		$idfuncionarioescola = (isset($queries["idfuncionarioescola"])) ? $queries["idfuncionarioescola"] : false;
		if ($idfuncionarioescola) array_push($where, " r1.idfuncionarioescola = $idfuncionarioescola ");

$salario = (isset($queries["salario"])) ? $queries["salario"] : false;
		if ($salario) array_push($where, " r1.salario = $salario ");

$cargo = (isset($queries["cargo"])) ? $queries["cargo"] : false;
		if ($cargo) array_push($where, " r1.cargo LIKE '%$cargo%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " r1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*, f1.nome, f1.rg, f1.cpf, f1.estadocivil, f1.conjugenome, f1.grauinstrucao, f1.dataadmissao, f1.cargo, f1.idendereco, f1.telefone, f1.celular, f1.salarioinicial"; 
		;
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM rhsolicitacoescontascorrentes r1
						LEFT JOIN funcionariosgerais f1 ON f1.id = r1.idfuncionario 
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getRhsolicitacaocontascorrentesById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getRhsolicitacoescontascorrentes($queries, 0, 0);

		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getRhsolicitacaocontascorrentesByIdHelper($id, $queries = array()) {
		$rows = new Rhsolicitacoescontascorrentes();
		return $rows->getRhsolicitacaocontascorrentesById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Rhsolicitacoescontascorrentes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
		$row->idfuncionarioescola = (array_key_exists("idfuncionarioescola",$dados)) ? $dados["idfuncionarioescola"] : $row->idfuncionarioescola;
		$row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
		$row->salario = (array_key_exists("salario",$dados)) ? $dados["salario"] : $row->salario;
		$row->cargo = (array_key_exists("cargo",$dados)) ? $dados["cargo"] : $row->cargo;

		$row->nomebanco = (array_key_exists("nomebanco",$dados)) ? $dados["nomebanco"] : $row->nomebanco;
		$row->agencia = (array_key_exists("agencia",$dados)) ? $dados["agencia"] : $row->agencia;
		$row->idassinatura = (array_key_exists("idassinatura",$dados)) ? $dados["idassinatura"] : $row->idassinatura;
		$row->nomeresponsavel = (array_key_exists("nomeresponsavel",$dados)) ? $dados["nomeresponsavel"] : $row->nomeresponsavel;
		$row->cargoresponsavel = (array_key_exists("cargoresponsavel",$dados)) ? $dados["cargoresponsavel"] : $row->cargoresponsavel;
		$row->nomeempresa = (array_key_exists("nomeempresa",$dados)) ? $dados["nomeempresa"] : $row->nomeempresa;
		$row->cnpjempresa = (array_key_exists("cnpjempresa",$dados)) ? $dados["cnpjempresa"] : $row->cnpjempresa;
		$row->telefoneempresa = (array_key_exists("telefoneempresa",$dados)) ? $dados["telefoneempresa"] : $row->telefoneempresa;
 		
 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		$row->save();
		
		return $row;
	}
	
}