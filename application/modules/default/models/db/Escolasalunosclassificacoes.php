<?php

class Escolasalunosclassificacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasalunosclassificacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getClassificacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolasalunosclassificacoes();
		return $produtos->getClassificacoes($queries, $page, $maxpage);
	}
	
	public function getClassificacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " es1.id=$id ");
		if ($idescola) array_push($where, " es1.idescola=$idescola ");
		if ($chave) array_push($where, " ((es1.classificacao LIKE '%$chave%')) ");
		if ($status) array_push($where, " es1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "es1.*, e1.escola";
		if ($total) $fields = "COUNT(es1.id) as total";
		
		
		$ordem = "ORDER BY es1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasalunosclassificacoes es1
						LEFT JOIN escolas e1 ON e1.id=es1.idescola
					WHERE es1.excluido='nao'
						AND e1.excluido = 'nao'
						$w 
					$ordem	
					$limit";	
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getClassificacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getClassificacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getClassificacaoByIdHelper($id, $queries = array()) {
		$rows = new Escolasalunosclassificacoes();
		return $rows->getClassificacaoById($id, $queries);
	}	

	public function getClassificacaoNomeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getClassificacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		$row = $rows[0];
		return $row['classificacao'];
	}
	
	public static function getClassificacaoNomeByIdHelper($id, $queries = array()) {
		$rows = new Escolasalunosclassificacoes();
		return $rows->getClassificacaoNomeById($id, $queries);
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Classificacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
			
			$historico = new Escolashistoricos_Escolasalunosclassificacoes();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);
		} 
		
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		$row->classificacao = (array_key_exists('classificacao',$dados)) ? $dados['classificacao'] : $row->classificacao;		
		$row->cor = (array_key_exists('cor',$dados)) ? $dados['cor'] : $row->cor;
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		
		$row->save();

		return $row;
	}
	
	
}