<?php

/**
 * Define o modelo Rhdeclaracoesparentescospessoas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Rhdeclaracoesparentescospessoas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "rhdeclaracoesparentescospessoas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getRhdeclaracoesparentescospessoasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$rhdeclaracoesparentescospessoas = new Rhdeclaracoesparentescospessoas();
		return $rhdeclaracoesparentescospessoas->getRhdeclaracoesparentescospessoas($queries, $page, $maxpage);
	}
	
	public function getRhdeclaracoesparentescospessoas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");
		
		
		$iddeclaracao = (isset($queries["iddeclaracao"])) ? $queries["iddeclaracao"] : false;
		if ($iddeclaracao) array_push($where, " r1.iddeclaracao = $iddeclaracao ");

$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
		if ($nome) array_push($where, " r1.nome LIKE '%$nome%' ");

$grauparentesco = (isset($queries["grauparentesco"])) ? $queries["grauparentesco"] : false;
		if ($grauparentesco) array_push($where, " r1.grauparentesco LIKE '%$grauparentesco%' ");

$localtrabalho = (isset($queries["localtrabalho"])) ? $queries["localtrabalho"] : false;
		if ($localtrabalho) array_push($where, " r1.localtrabalho LIKE '%$localtrabalho%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " r1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*"; 
		;
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM rhdeclaracoesparentescospessoas r1
					
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getRhdeclaracaoparentescopessoaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getRhdeclaracoesparentescospessoas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getRhdeclaracaoparentescopessoaByIdHelper($id, $queries = array()) {
		$rows = new Rhdeclaracoesparentescospessoas();
		return $rows->getRhdeclaracaoparentescopessoaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Rhdeclaracoesparentescospessoas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->iddeclaracao = (array_key_exists("iddeclaracao",$dados)) ? $dados["iddeclaracao"] : $row->iddeclaracao;
 $row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
 $row->grauparentesco = (array_key_exists("grauparentesco",$dados)) ? $dados["grauparentesco"] : $row->grauparentesco;
 $row->localtrabalho = (array_key_exists("localtrabalho",$dados)) ? $dados["localtrabalho"] : $row->localtrabalho;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}