<?php

/**
 * Define o modelo Departamentosescolas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Departamentosescolas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "departamentosescolas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDepartamentosescolasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$departamentosescolas = new Departamentosescolas();
		return $departamentosescolas->getDepartamentosescolas($queries, $page, $maxpage);
	}
	
	public function getDepartamentosescolas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " (d1.idsecretaria = $idsecretaria OR d1.origem='SYS') ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " d1.idescola = $idescola ");

$departamento = (isset($queries["departamento"])) ? $queries["departamento"] : false;
		if ($departamento) array_push($where, " d1.departamento LIKE '%$departamento%' ");

$departamentocerto = (isset($queries["departamentocerto"])) ? $queries["departamentocerto"] : false;
		if ($departamentocerto) array_push($where, " d1.departamento = '$departamentocerto' ");

$descricao = (isset($queries["descricao"])) ? $queries["descricao"] : false;
		if ($descricao) array_push($where, " d1.descricao = '$descricao' ");

$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
		if ($nome) array_push($where, " d1.nome LIKE '%$nome%' ");

$gdae = (isset($queries["gdae"])) ? $queries["gdae"] : false;
		if ($gdae) array_push($where, " d1.gdae LIKE '%$gdae%' ");

$perguntas = (isset($queries["perguntas"])) ? $queries["perguntas"] : false;
		if ($perguntas) array_push($where, " d1.perguntas LIKE '%$perguntas%' ");

$planosdeaulaacesso = (isset($queries["planosdeaulaacesso"])) ? $queries["planosdeaulaacesso"] : false;
		if ($planosdeaulaacesso) array_push($where, " d1.planosdeaulaacesso LIKE '%planosdeaulaacesso%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " d1.status LIKE '%$status%' ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " d1.origem LIKE '%$origem%' ");

$semorigem = (isset($queries["semorigem"])) ? $queries["semorigem"] : false;
		if ($semorigem) array_push($where, " d1.origem <> '$semorigem' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		
		$ordem = "ORDER BY d1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM departamentosescolas d1
					
					WHERE d1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDepartamentoescolaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDepartamentosescolas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDepartamentoescolaByIdHelper($id, $queries = array()) {
		$rows = new Departamentosescolas();
		return $rows->getDepartamentoescolaById($id, $queries);
	}		
	

	
	public function getDepartamentoescolaBydepartamento($departamento, $queries = array()) {
		if ((!isset($departamento))||($departamento=="")) return false;
		
		$queries['departamentocerto'] = $departamento;
		$rows = $this->getDepartamentosescolas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDepartamentoescolaBydepartamentoHelper($departamento, $queries = array()) {
		$rows = new Departamentosescolas();
		return $rows->getDepartamentoescolaBydepartamento($departamento, $queries);
	}	


	public function getDepartamentoescolaByCoddep($departamento, $queries = array()) {
		if ((!isset($departamento))||($departamento=="")) return false;
		
		$queries['departamentocerto'] = $departamento;
		$rows = $this->getDepartamentosescolas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDepartamentoescolaByCoddepHelper($departamento, $queries = array()) {
		$rows = new Departamentosescolas();
		return $rows->getDepartamentoescolaByCoddep($departamento, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Departamentosescolas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idarquivoimportacao = (array_key_exists("idarquivoimportacao",$dados)) ? $dados["idarquivoimportacao"] : $row->idarquivoimportacao;
 $row->coddep = (array_key_exists("coddep",$dados)) ? $dados["coddep"] : $row->coddep;
 $row->departamento = (array_key_exists("departamento",$dados)) ? $dados["departamento"] : $row->departamento;
 $row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
 $row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
 $row->gdae = (array_key_exists("gdae",$dados)) ? $dados["gdae"] : $row->gdae;
 $row->codigomunicipiodne = (array_key_exists("codigomunicipiodne",$dados)) ? $dados["codigomunicipiodne"] : $row->codigomunicipiodne;
 $row->perguntas = (array_key_exists("perguntas",$dados)) ? $dados["perguntas"] : $row->perguntas;
 $row->planosdeaulaacesso = (array_key_exists("planosdeaulaacesso",$dados)) ? $dados["planosdeaulaacesso"] : $row->planosdeaulaacesso;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}