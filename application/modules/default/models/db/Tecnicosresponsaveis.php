<?php

/**
 * Define o modelo Tecnicosresponsaveis
 *
 * @author		Fernando Alves		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2018 FJMX
 * @version     1.0
 */
class Tecnicosresponsaveis extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "tecnicosresponsaveis";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getTecnicosresponsaveisHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$tecnicosresponsaveis = new Tecnicosresponsaveis();
		return $tecnicosresponsaveis->getTecnicosresponsaveis($queries, $page, $maxpage);
	}
	
	public function getTecnicosresponsaveis($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " t1.id = $id ");

		$rgf_funcionario = (isset($queries['rgf_funcionario'])) ? (int)$queries['rgf_funcionario'] : false;
		if ($rgf_funcionario) array_push($where, " t1.rgf_funcionario = $rgf_funcionario ");
		
		$tipodetecnico = (isset($queries['tipodetecnico'])) ? $queries['tipodetecnico'] : false;
		if ($tipodetecnico) array_push($where, " t1.tipodetecnico LIKE '%$tipodetecnico%' ");
		
		
		$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " t1.titulo LIKE '%$titulo%' ");

$texto = (isset($queries["texto"])) ? $queries["texto"] : false;
		if ($texto) array_push($where, " t1.texto = '$texto' ");

$valor1 = (isset($queries["valor1"])) ? $queries["valor1"] : false;
		if ($valor1) array_push($where, " t1.valor1 = $valor1 ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " t1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " t1.data <= '$data_f' ");

$idbanco = (isset($queries["idbanco"])) ? $queries["idbanco"] : false;
		if ($idbanco) array_push($where, " t1.idbanco = $idbanco ");

$departamento = (isset($queries["departamento"])) ? $queries["departamento"] : false;
		if ($departamento) array_push($where, " t1.departamento = $departamento ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " t1.status LIKE '%$status%' ");

$idtipoequipe = (isset($queries["idtipoequipe"])) ? $queries["idtipoequipe"] : false;
		if ($idtipoequipe) array_push($where, " t1.idtipoequipe LIKE '%$idtipoequipe%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "t1.*"; 
		;
		
		if ($total) $fields = "COUNT(t1.id) as total";
		
		$ordem = "ORDER BY t1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM tecnicosresponsaveis t1
					
					WHERE t1.excluido='nao' 
						$w 
					$ordem	
					$limit";
					
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getTecnicosresponsavelById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTecnicosresponsaveis($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTecnicosresponsavelByIdHelper($id, $queries = array()) {
		$rows = new Tecnicosresponsaveis();
		return $rows->getTecnicosresponsavelById($id, $queries);
	}		
	 
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Tecnicosresponsaveis
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
	    $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
		$row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
		
		$row->id_funcionario = (array_key_exists("id_funcionario",$dados)) ? $dados["id_funcionario"] : $row->id_funcionario;
		$row->rgf_funcionario = (array_key_exists("rgf_funcionario",$dados)) ? $dados["rgf_funcionario"] : $row->rgf_funcionario;

        $row->tipodetecnico = (array_key_exists("tipodetecnico",$dados)) ? $dados["tipodetecnico"] : $row->tipodetecnico;
        $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
        $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
        $row->idtipoequipe = (array_key_exists("idtipoequipe",$dados)) ? $dados["idtipoequipe"] : $row->idtipoequipe;
        $row->departamento = (array_key_exists("departamento",$dados)) ? $dados["departamento"] : $row->departamento;
		
				
		$row->save();
		
		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Tecnicosresponsaveis departamento $row->departamento salvo com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Tecnicosresponsaveis departamento $row->departamento com o ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Tecnicosresponsaveis departamento $row->departamento com o ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Tecnicosresponsaveis departamento $row->departamento com o ID ".$id." atualizada com sucesso!");

		return $row;
	}
	
}