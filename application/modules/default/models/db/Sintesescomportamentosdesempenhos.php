<?php

/**
 * Define o modelo Sintesescomportamentosdesempenhos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Sintesescomportamentosdesempenhos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "sintesescomportamentosdesempenhos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getSequencial() {
		$scds = new Sintesescomportamentosdesempenhos();

		$scd = $scds->getSintesescomportamentosdesempenhos(array(),0,1);
		$scd = $scd[0];
		$idultimo = $scd['id'];

		$sequencial = str_pad($idultimo+1, 5, '0',STR_PAD_LEFT) . '/' . (string)date('Y');

		return $sequencial;
	}
	
	public static function getSintesescomportamentosdesempenhosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$sintesescomportamentosdesempenhos = new Sintesescomportamentosdesempenhos();
		return $sintesescomportamentosdesempenhos->getSintesescomportamentosdesempenhos($queries, $page, $maxpage);
	}
	
	public function getSintesescomportamentosdesempenhos($queries = array(), $page = 0, $maxpage = 0) { 
		
		$where = array();		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$sequencial = (isset($queries['sequencial'])) ? $queries['sequencial'] : false;
		if ($sequencial) array_push($where, " a1.sequencial LIKE '%$sequencial%' ");

		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " a1.idescola = $idescola ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='sequencial') $sorting[0]='a1.sequencial';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*";
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM sintesescomportamentosdesempenhos a1
					
					WHERE a1.excluido='nao' 
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getSintesescomportamentosdesempenhosById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSintesescomportamentosdesempenhos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;

		return $rows[0];
	}
	
	public static function getSintesescomportamentosdesempenhosByIdHelper($id, $queries = array()) {
		$rows = new Sintesescomportamentosdesempenhos();
		return $rows->getSintesescomportamentosdesempenhosById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Sintesescomportamentosdesempenhos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else $novoRegistro = false;		
		
		$row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
		$row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->idprofessor = (array_key_exists("idprofessor",$dados)) ? $dados["idprofessor"] : $row->idprofessor;
 		$row->iddiretor = (array_key_exists("iddiretor",$dados)) ? $dados["iddiretor"] : $row->iddiretor;
 		$row->idcoordenador = (array_key_exists("idcoordenador",$dados)) ? $dados["idcoordenador"] : $row->idcoordenador;
 		$row->idserie = (array_key_exists("idserie",$dados)) ? $dados["idserie"] : $row->idserie;
 		$row->idturma = (array_key_exists("idturma",$dados)) ? $dados["idturma"] : $row->idturma;
 		$row->periodo = (array_key_exists("periodo",$dados)) ? $dados["periodo"] : $row->periodo;
 		
 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;		
				
		$row->save();
		
		return $row;
	}	
}