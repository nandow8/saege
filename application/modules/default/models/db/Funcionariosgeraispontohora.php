<?php

/**
 * Define o modelo Funcionariosgerais
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosgeraispontohora extends Zend_Db_Table_Abstract { 
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosgeraispontohorarios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFuncionariosgeraisHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosgerais = new Funcionariosgerais();
		return $funcionariosgerais->getFuncionariosgerais($queries, $page, $maxpage);
	}
	
	public function getFuncionariosgerais($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
                
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		// 2
		$titulo_pontohorarios = (isset($queries["titulo_pontohorarios"])) ? $queries["titulo_pontohorarios"] : false;
		if ($titulo_pontohorarios) array_push($where, " f1.titulo_pontohorarios = $titulo_pontohorarios ");
                // 3
                $entrada_pontohorarios = (isset($queries["entrada_pontohorarios"])) ? $queries["entrada_pontohorarios"] : false;
		if ($entrada_pontohorarios) array_push($where, " f1.entrada_pontohorarios = $entrada_pontohorarios ");
                // 4
                $saida_pontohorarios = (isset($queries["saida_pontohorarios"])) ? $queries["saida_pontohorarios"] : false;
		if ($saida_pontohorarios) array_push($where, " f1.saida_pontohorarios = $saida_pontohorarios ");
                // 5
                $entradapausa_pontohorarios = (isset($queries["entradapausa_pontohorarios"])) ? $queries["entradapausa_pontohorarios"] : false;
		if ($entradapausa_pontohorarios) array_push($where, " f1.entradapausa_pontohorarios = $entradapausa_pontohorarios ");
                // 6
                $saidapausa_pontohorarios = (isset($queries["saidapausa_pontohorarios"])) ? $queries["saidapausa_pontohorarios"] : false;
		if ($saidapausa_pontohorarios) array_push($where, " f1.saidapausa_pontohorarios = $saidapausa_pontohorarios ");
                // 7
                $status_pontohorarios = (isset($queries["status_pontohorarios"])) ? $queries["status_pontohorarios"] : false;
		if ($status_pontohorarios) array_push($where, " f1.status_pontohorarios = $status_pontohorarios ");
                // 8
                $logusuario_pontohorarios = (isset($queries["logusuario_pontohorarios"])) ? $queries["logusuario_pontohorarios"] : false;
		if ($logusuario_pontohorarios) array_push($where, " f1.logusuario_pontohorarios = $logusuario_pontohorarios ");
                // 9
                $logdata_pontohorarios = (isset($queries["logdata_pontohorarios"])) ? $queries["logdata_pontohorarios"] : false;
		if ($logdata_pontohorarios) array_push($where, " f1.logdata_pontohorarios = $logdata_pontohorarios ");
                
                //idfuncionario
                $idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " f1.idfuncionario = $idfuncionario ");
                //idrhfuncionario
                $idrhfuncionario = (isset($queries["idrhfuncionario"])) ? $queries["idrhfuncionario"] : false;
		if ($idrhfuncionario) array_push($where, " f1.idrhfuncionario = $idrhfuncionario ");
                
                /* ----------------     Código nativo    ------------------------*/
                /*
                $nome = (isset($queries["nome"])) ? $queries["nome"] : false;
                                if ($nome) array_push($where, " f1.nome LIKE '%$nome%' ");

                $telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
                                if ($telefone) array_push($where, " f1.telefone LIKE '%$telefone%' ");


                $modulo = (isset($queries["modulo"])) ? $queries["modulo"] : false;
                                if ($modulo) array_push($where, " f1.modulo LIKE '%$modulo%' ");
                $tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
                                if ($tipo) array_push($where, " f1.tipo LIKE '%$tipo%' ");

                $matrícula = (isset($queries["matrícula"])) ? $queries["matrícula"] : false;
                                if ($matrícula) array_push($where, " f1.matrícula LIKE '%$matrícula%' ");

                $secretaria = (isset($queries["secretaria"])) ? $queries["secretaria"] : false;
                                if ($secretaria) array_push($where, " f1.secretaria LIKE '%$secretaria%' ");

                $iddepartamentosecretaria = (isset($queries["iddepartamentosecretaria"])) ? $queries["iddepartamentosecretaria"] : false;
                                if ($iddepartamentosecretaria) array_push($where, " f1.iddepartamentosecretaria = $iddepartamentosecretaria ");

                $setor = (isset($queries["setor"])) ? $queries["setor"] : false;
                                if ($setor) array_push($where, " f1.setor LIKE '%$setor%' ");

                $funcao = (isset($queries["funcao"])) ? $queries["funcao"] : false;
                                if ($funcao) array_push($where, " f1.funcao LIKE '%$funcao%' ");

                $descricaofuncao = (isset($queries["descricaofuncao"])) ? $queries["descricaofuncao"] : false;
                                if ($descricaofuncao) array_push($where, " f1.descricaofuncao = '$descricaofuncao' ");

                $riscofuncao = (isset($queries["riscofuncao"])) ? $queries["riscofuncao"] : false;
                                if ($riscofuncao) array_push($where, " f1.riscofuncao LIKE '%$riscofuncao%' ");

                $email = (isset($queries["email"])) ? $queries["email"] : false;
                                if ($email) array_push($where, " f1.email LIKE '%$email%' ");

                $datanascimento_i = (isset($queries["datanascimento_i"])) ? $queries["datanascimento_i"] : false;
                                if ($datanascimento_i) array_push($where, " f1.datanascimento >= '$datanascimento_i' ");

                $datanascimento_f = (isset($queries["datanascimento_f"])) ? $queries["datanascimento_f"] : false;
                                if ($datanascimento_f) array_push($where, " f1.datanascimento <= '$datanascimento_f' ");

                $emissaocarteira_i = (isset($queries["emissaocarteira_i"])) ? $queries["emissaocarteira_i"] : false;
                                if ($emissaocarteira_i) array_push($where, " f1.emissaocarteira >= '$emissaocarteira_i' ");

                $emissaocarteira_f = (isset($queries["emissaocarteira_f"])) ? $queries["emissaocarteira_f"] : false;
                                if ($emissaocarteira_f) array_push($where, " f1.emissaocarteira <= '$emissaocarteira_f' ");

                $reservista = (isset($queries["reservista"])) ? $queries["reservista"] : false;
                                if ($reservista) array_push($where, " f1.reservista LIKE '%$reservista%' ");

                $rg = (isset($queries["rg"])) ? $queries["rg"] : false;
                                if ($rg) array_push($where, " f1.rg LIKE '%$rg%' ");

                $cpf = (isset($queries["cpf"])) ? $queries["cpf"] : false;
                                if ($cpf) array_push($where, " f1.cpf LIKE '%$cpf%' ");

                $pispasep = (isset($queries["pispasep"])) ? $queries["pispasep"] : false;
                                if ($pispasep) array_push($where, " f1.pispasep LIKE '%$pispasep%' ");

                $cnh = (isset($queries["cnh"])) ? $queries["cnh"] : false;
                                if ($cnh) array_push($where, " f1.cnh LIKE '%$cnh%' ");

                $cnh_numero = (isset($queries["cnh_numero"])) ? $queries["cnh_numero"] : false;
                                if ($cnh_numero) array_push($where, " f1.cnh_numero LIKE '%$cnh_numero%' ");

                $cnh_categoria = (isset($queries["cnh_categoria"])) ? $queries["cnh_categoria"] : false;
                                if ($cnh_categoria) array_push($where, " f1.cnh_categoria LIKE '%$cnh_categoria%' ");

                $cnh_validade_i = (isset($queries["cnh_validade_i"])) ? $queries["cnh_validade_i"] : false;
                                if ($cnh_validade_i) array_push($where, " f1.cnh_validade >= '$cnh_validade_i' ");

                $cnh_validade_f = (isset($queries["cnh_validade_f"])) ? $queries["cnh_validade_f"] : false;
                                if ($cnh_validade_f) array_push($where, " f1.cnh_validade <= '$cnh_validade_f' ");

                $estrangeiro = (isset($queries["estrangeiro"])) ? $queries["estrangeiro"] : false;
                                if ($estrangeiro) array_push($where, " f1.estrangeiro LIKE '%$estrangeiro%' ");

                $obs_estrangeiro = (isset($queries["obs_estrangeiro"])) ? $queries["obs_estrangeiro"] : false;
                                if ($obs_estrangeiro) array_push($where, " f1.obs_estrangeiro = '$obs_estrangeiro' ");

                $nacionalidade = (isset($queries["nacionalidade"])) ? $queries["nacionalidade"] : false;
                                if ($nacionalidade) array_push($where, " f1.nacionalidade LIKE '%$nacionalidade%' ");

                $localnascimento = (isset($queries["localnascimento"])) ? $queries["localnascimento"] : false;
                                if ($localnascimento) array_push($where, " f1.localnascimento LIKE '%$localnascimento%' ");

                $grauinstrucao = (isset($queries["grauinstrucao"])) ? $queries["grauinstrucao"] : false;
                                if ($grauinstrucao) array_push($where, " f1.grauinstrucao LIKE '%$grauinstrucao%' ");

                $estadocivil = (isset($queries["estadocivil"])) ? $queries["estadocivil"] : false;
                                if ($estadocivil) array_push($where, " f1.estadocivil LIKE '%$estadocivil%' ");

                $conjugenome = (isset($queries["conjugenome"])) ? $queries["conjugenome"] : false;
                                if ($conjugenome) array_push($where, " f1.conjugenome LIKE '%$conjugenome%' ");

                $filhos = (isset($queries["filhos"])) ? $queries["filhos"] : false;
                                if ($filhos) array_push($where, " f1.filhos LIKE '%$filhos%' ");

                $filhosquantidade = (isset($queries["filhosquantidade"])) ? $queries["filhosquantidade"] : false;
                                if ($filhosquantidade) array_push($where, " f1.filhosquantidade = '$filhosquantidade' ");

                $rgf = (isset($queries["rgf"])) ? $queries["rgf"] : false;
                                if ($rgf) array_push($where, " f1.rgf = '$rgf' ");

                $celular = (isset($queries["celular"])) ? $queries["celular"] : false;
                                if ($celular) array_push($where, " f1.celular LIKE '%$celular%' ");

                $dataadmissao_i = (isset($queries["dataadmissao_i"])) ? $queries["dataadmissao_i"] : false;
                                if ($dataadmissao_i) array_push($where, " f1.dataadmissao >= '$dataadmissao_i' ");

                $dataadmissao_f = (isset($queries["dataadmissao_f"])) ? $queries["dataadmissao_f"] : false;
                                if ($dataadmissao_f) array_push($where, " f1.dataadmissao <= '$dataadmissao_f' ");

                $carteiranumero = (isset($queries["carteiranumero"])) ? $queries["carteiranumero"] : false;
                                if ($carteiranumero) array_push($where, " f1.carteiranumero LIKE '%$carteiranumero%' ");

                $carteiraserie = (isset($queries["carteiraserie"])) ? $queries["carteiraserie"] : false;
                                if ($carteiraserie) array_push($where, " f1.carteiraserie LIKE '%$carteiraserie%' ");

                $filiacaomae = (isset($queries["filiacaomae"])) ? $queries["filiacaomae"] : false;
                                if ($filiacaomae) array_push($where, " f1.filiacaomae LIKE '%$filiacaomae%' ");

                $filiacaopai = (isset($queries["filiacaopai"])) ? $queries["filiacaopai"] : false;
                                if ($filiacaopai) array_push($where, " f1.filiacaopai LIKE '%$filiacaopai%' ");

                $ctps = (isset($queries["ctps"])) ? $queries["ctps"] : false;
                                if ($ctps) array_push($where, " f1.ctps LIKE '%$ctps%' ");

                $dataregistroctps_i = (isset($queries["dataregistroctps_i"])) ? $queries["dataregistroctps_i"] : false;
                                if ($dataregistroctps_i) array_push($where, " f1.dataregistroctps >= '$dataregistroctps_i' ");

                $dataregistroctps_f = (isset($queries["dataregistroctps_f"])) ? $queries["dataregistroctps_f"] : false;
                                if ($dataregistroctps_f) array_push($where, " f1.dataregistroctps <= '$dataregistroctps_f' ");

                $regime = (isset($queries["regime"])) ? $queries["regime"] : false;
                                if ($regime) array_push($where, " f1.regime LIKE '%$regime%' ");

                $regime_outros = (isset($queries["regime_outros"])) ? $queries["regime_outros"] : false;
                                if ($regime_outros) array_push($where, " f1.regime_outros LIKE '%$regime_outros%' ");

                $bolsista = (isset($queries["bolsista"])) ? $queries["bolsista"] : false;
                                if ($bolsista) array_push($where, " f1.bolsista LIKE '%$bolsista%' ");

                $datainiciobolsa_i = (isset($queries["datainiciobolsa_i"])) ? $queries["datainiciobolsa_i"] : false;
                                if ($datainiciobolsa_i) array_push($where, " f1.datainiciobolsa >= '$datainiciobolsa_i' ");

                $datainiciobolsa_f = (isset($queries["datainiciobolsa_f"])) ? $queries["datainiciobolsa_f"] : false;
                                if ($datainiciobolsa_f) array_push($where, " f1.datainiciobolsa <= '$datainiciobolsa_f' ");

                $datafimbolsa_i = (isset($queries["datafimbolsa_i"])) ? $queries["datafimbolsa_i"] : false;
                                if ($datafimbolsa_i) array_push($where, " f1.datafimbolsa >= '$datafimbolsa_i' ");

                $datafimbolsa_f = (isset($queries["datafimbolsa_f"])) ? $queries["datafimbolsa_f"] : false;
                                if ($datafimbolsa_f) array_push($where, " f1.datafimbolsa <= '$datafimbolsa_f' ");

                $status = (isset($queries["status"])) ? $queries["status"] : false;
                                if ($status) array_push($where, " f1.status LIKE '%$status%' ");
                */


		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado "; ;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosgerais f1
					 LEFT JOIN enderecos e_idendereco ON e_idendereco.id=f1.idendereco 
						LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado	
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariogeralById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosgerais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionariogeralByIdHelper($id, $queries = array()) {
		$rows = new Funcionariosgerais();
		return $rows->getFuncionariogeralById($id, $queries);
	}		
	
	public static function getFuncionariosgeraisByRgfHelper($rgf, $queries = array()){
		if ((!isset($rgf)) || (!$rgf) || ($rgf=="")) return false;
		$_rows = new Funcionariosgerais();

		$queries['rgf'] = $rgf;
		$rows = $_rows->getFuncionariosgerais($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	
        public function envio(){
            return 'envio';
        }//end function
        
       
        public function save($dados) {
		$novoRegistro = true;
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
                $row = $this->fetchRow("id=$id");
                
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		}//end if/else 
             
		
                $row->titulo_pontohorarios = (array_key_exists("titulo_pontohorarios",$dados)) ? $dados["titulo_pontohorarios"] : $row->titulo_pontohorarios;
                $row->data_pontohorarios = (array_key_exists("data_pontohorarios",$dados)) ? $dados["data_pontohorarios"] : $row->data_pontohorarios;
                
                $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
                $row->idrhfuncionario = (array_key_exists("idrhfuncionario",$dados)) ? $dados["idrhfuncionario"] : $row->idrhfuncionario;
                $row->idlocalfuncionario = (array_key_exists("idlocalfuncionario",$dados)) ? $dados["idlocalfuncionario"] : $row->idlocalfuncionario;
                $row->iddeptfuncionario = (array_key_exists("iddeptfuncionario",$dados)) ? $dados["iddeptfuncionario"] : $row->iddeptfuncionario;

                $row->entrada_pontohorarios = (array_key_exists("entrada_pontohorarios",$dados)) ? $dados["entrada_pontohorarios"] : $row->entrada_pontohorarios;
                $row->saida_pontohorarios = (array_key_exists("saida_pontohorarios",$dados)) ? $dados["saida_pontohorarios"] : $row->saida_pontohorarios;
                $row->entradapausa_pontohorarios = (array_key_exists("entradapausa_pontohorarios",$dados)) ? $dados["entradapausa_pontohorarios"] : $row->entradapausa_pontohorarios;

                $row->fkhorariorotulo = (array_key_exists("fkhorariorotulo",$dados)) ? $dados["fkhorariorotulo"] : $row->fkhorariorotulo;
                $row->fkhorarioid = (array_key_exists("fkhorarioid",$dados)) ? $dados["fkhorarioid"] : $row->fkhorarioid;
                
                $row->status_pontohorarios = (array_key_exists("status_pontohorarios",$dados)) ? $dados["status_pontohorarios"] : $row->status_pontohorarios;
                $row->confirmacao_status = (array_key_exists("confirmacao_status",$dados)) ? $dados["confirmacao_status"] : $row->confirmacao_status;
                
                $row->saidapausa_pontohorarios = (array_key_exists("saidapausa_pontohorarios",$dados)) ? $dados["saidapausa_pontohorarios"] : $row->saidapausa_pontohorarios;
                $row->status_pontohorarios  = (array_key_exists("status_pontohorarios ",$dados)) ? $dados["status_pontohorarios "] : $row->status_pontohorarios ;
                $row->logusuario_pontohorarios = (array_key_exists("logusuario_pontohorarios",$dados)) ? $dados["logusuario_pontohorarios"] : $row->logusuario_pontohorarios;
                $row->logdata_pontohorarios = (array_key_exists("logdata_pontohorarios",$dados)) ? $dados["logdata_pontohorarios"] : $row->logdata_pontohorarios;
               
		$row->save();
		
		return $row;
	}//end function save
        
        
        
        /*
         *  Autor : Thiago Torres Migliorati
         *  Função para verificar se há um registro no local 
         *  Criação 04/05/2018
         *  Fiz pq estava a fim de fazer, e me facilitaria a vida, já viram o tanto de campos que eu deveria editar lá no topo da página
         */
        
        public function getVerificaRegistro($data, $idfuncionario, $idrhfuncionario, $idlocalfuncionario, $iddeptfuncionario){
            
            $strSql = "SELECT * FROM funcionariosgeraispontohorarios WHERE data_pontohorarios = '" . $data . "' AND idrhfuncionario = " . $idfuncionario . " AND idlocalfuncionario = " . $idlocalfuncionario . " AND iddeptfuncionario = " . $iddeptfuncionario;
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchAll($strSql);
           
            $counter = 0;
            
            foreach($row as $index):
                $counter++;
            endforeach;
            
            return $counter;
            
        }//end function getVerificaRegistro
        
         public function getRetornaRegistroData($id, $userId, $idlocalfuncionario, $iddeptfuncionario, $data){
            
            // $strSql = "SELECT * FROM funcionariosgeraispontohorarios WHERE idrhfuncionario = " . $id . " AND idfuncionario = " . $userId . " AND data_pontohorarios = '$data'"; 
            $strSql = "SELECT * FROM funcionariosgeraispontohorarios WHERE idrhfuncionario = $id AND idlocalfuncionario = $idlocalfuncionario AND iddeptfuncionario = $iddeptfuncionario AND data_pontohorarios = '$data'";

            $db = Zend_Registry::get('db');
            $row = $db->fetchRow($strSql);
            
            return $row;
            
        }//end function getVerificaRegistro
        
         public function getRetornaRegistro($id){
            
            $strSql = "SELECT * FROM funcionariosgeraispontohorarios WHERE idrhfuncionario = " . $id;
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchRow($strSql);
            
            return $row;
            
        }//end function getVerificaRegistro
        
        public function getLancamentosConfirmados($dataInicio, $dataFim, $idfuncionario, $idusuario){
            
             $strQuery = "SELECT count(*) AS Total FROM funcionariosgeraispontohorarios WHERE idrhfuncionario = $idfuncionario and idfuncionario = $idusuario and data_pontohorarios >= '$dataInicio' and data_pontohorarios <= '$dataFim'";
            
             $db = Zend_Registry::get('db');
             $row = $db->fetchRow($strQuery);
             
             return $row;
            
        }//end function getLancamentosConfirmados
        
        public function getRhConfirmados($dataInicio, $dataFim,  $idfuncionario, $idusuario){
            
            $strQuery = "SELECT count(*) AS Total FROM funcionariosgeraispontohorarios WHERE idrhfuncionario = $idfuncionario and idfuncionario = $idusuario and  status_pontohorarios = 'ok' and data_pontohorarios >= '$dataInicio' and data_pontohorarios <= '$dataFim'";
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchRow($strQuery);
           
            return $row;
            
        }//end public function 

        public static function getRhConfirmadosTotal(){
            
                $strQuery = "SELECT count(*) AS Total FROM funcionariosgeraispontohorarios WHERE idrhfuncionario = $idfuncionario and idfuncionario = $idusuario and  status_pontohorarios = 'ok' and data_pontohorarios >= '$dataInicio' and data_pontohorarios <= '$dataFim'";
                
                
               
                // return $row;

                return 24;
                
            }//end public function 
       
        /* Pensei em não fazer essa, mas vai que dá alguma merda então é melhor não apenas subtrair o valor do total */
        public function getOcorrencias($dataInicio, $dataFim,  $idfuncionario, $idusuario){
            
            $strQuery = "SELECT count(*) AS Total FROM funcionariosgeraispontohorarios WHERE idrhfuncionario = $idfuncionario and idfuncionario = $idusuario and  titulo_pontohorarios != 'Dia Trabalhado' and data_pontohorarios >= '$dataInicio' and data_pontohorarios <= '$dataFim'";
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchRow($strQuery);
           
            return $row;
            
        }// end function getOcorrencias
        
        public function getConfirmadosRH($dataInicio, $dataFim,  $idfuncionario, $idusuario){
            
            $strQuery = "SELECT count(*) AS Total FROM funcionariosgeraispontohorarios WHERE idrhfuncionario = $idfuncionario and idfuncionario = $idusuario and status_pontohorarios = 'ok' and data_pontohorarios >= '$dataInicio' and data_pontohorarios <= '$dataFim'";
            
            $db = Zend_Registry::get('db');
            $row = $db->fetchRow($strQuery);
            $retorno = array('Total'=> 'voltou');
            return $retorno;
            
        }// end function getConfirmadosRH
        
        public function getDiasUteisPeriodo($dataInicio, $dataFim){
            
            // Contabilizar sábados e domingos
            $dateObjInicio =  date("d-m-Y", strtotime($dataInicio));
            $brDateInicio = new DateTime($dateObjInicio);

            $dateObjFim =  date('d-m-Y', strtotime($dataFim));
            $brDateFim = new DateTime($dateObjFim);

            $interval = new DateInterval('P1D');

            $endOne = new DateTime($dateObjFim);
            $endOne->modify('+1 day');

            $period = new DatePeriod($brDateInicio, $interval, $endOne);
            
            $counter = 0;
            
            foreach($period as $date):
                if(!($date->format("w") == "0" || $date->format('w') == "6")):
                    if(!(Feriados::getVerificaFeriado($date->format("Y-m-d")) > 0 && (Feriados::getInformacoesFeriadoData($date->format("Y-m-d"))['status'] == 'Ativo'))):
                        $counter++;
                    endif;
                endif;
            endforeach;
            
            return $counter;
       
        }//end function getDiasUteisPerio

        
        public function getTotalHorasLancados($dataInicio, $dataFim,  $idfuncionario, $idusuario){
        
        $strQueryLancados = "SELECT count(*), fkhorarioid, sum(TIMEDIFF(TIMEDIFF(hora_pausa_entrada,hora_pausa_saida),TIMEDIFF(hora_entrada,hora_saida))) AS totalhoras, hora_entrada, hora_saida, hora_pausa_entrada, hora_pausa_saida, TIMEDIFF(TIMEDIFF(hora_pausa_entrada,hora_pausa_saida),TIMEDIFF(hora_entrada,hora_saida)) AS ToralHorasTrabalhadasDia FROM funcionariosgeraispontohorarios INNER JOIN funcionariosgeraispontodefinicaohorarios ON funcionariosgeraispontohorarios.fkhorarioid = funcionariosgeraispontodefinicaohorarios.id WHERE idrhfuncionario = $idfuncionario and idfuncionario = $idusuario and  titulo_pontohorarios = 'Dia Trabalhado' and data_pontohorarios >= '$dataInicio' and data_pontohorarios <= '$dataFim' group by fkhorarioid";
        
        $db = Zend_Registry::get('db');

        /* Total horas lançadas */
        $rowLancados = $db->fetchAll($strQueryLancados);

        /* Total Horas do mês */

        $retorno = array();
        $retorno['totalhorasmes'] = '*';
        $retorno['totalhorastrabalhadas'] = 0;

        foreach($rowLancados as $index):
                $retorno['totalhorastrabalhadas'] += number_format(((int)$index['totalhoras']/3600), 3, '.', '');
        endforeach;
      
        return $retorno;
        
        }// end function getOcorrencias
            
        public function getTotalFaltasLancados($dataInicio, $dataFim,  $idfuncionario, $idusuario){
        
                $strQueryLancados = "SELECT * FROM funcionariosgeraispontohorarios INNER JOIN funcionariosgeraispontodefinicaohorarios ON funcionariosgeraispontohorarios.fkhorarioid = funcionariosgeraispontodefinicaohorarios.id WHERE idrhfuncionario = $idfuncionario and idfuncionario = $idusuario and  titulo_pontohorarios = 'FALTA INJUSTIFICADA' and data_pontohorarios >= '$dataInicio' and data_pontohorarios <= '$dataFim'";
                
                $db = Zend_Registry::get('db');
        
                /* Total horas lançadas */
                $rowLancados = $db->fetchAll($strQueryLancados);
                
                return $rowLancados;
                
        }// end function getOcorrencias

        public function getTotalOcorrenciasLancados($dataInicio, $dataFim,  $idfuncionario, $idusuario){
        
                $strQueryLancados = "SELECT * FROM funcionariosgeraispontohorarios INNER JOIN funcionariosgeraispontodefinicaohorarios ON funcionariosgeraispontohorarios.fkhorarioid = funcionariosgeraispontodefinicaohorarios.id WHERE idrhfuncionario = $idfuncionario and idfuncionario = $idusuario and  titulo_pontohorarios != 'FALTA INJUSTIFICADA' and titulo_pontohorarios != 'Dia Trabalhado' and data_pontohorarios >= '$dataInicio' and data_pontohorarios <= '$dataFim'";
                
                $db = Zend_Registry::get('db');
        
                /* Total horas lançadas */
                $rowLancados = $db->fetchAll($strQueryLancados);
                
                return $rowLancados;
                
        }// end function getOcorrencias
	
}