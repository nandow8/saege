<?php

class Escolassalasalunos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolassalasalunos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static $_MOTIVO_TRANSFERENCIA_MUDANCA_RESIDENCIA = '01';
	public static $_MOTIVO_TRANSFERENCIA_MUDANCA_ESCOLA = '02';
	public static $_MOTIVO_TRANSFERENCIA_MUDANCA_ESTADO = '03';
	public static $_MOTIVO_TRANSFERENCIA_MUDANCA_PAIS = '04';
	public static $_MOTIVO_TRANSFERENCIA_MUDANCA_PARTICULAR = '05';
	
	public static function getMotivosTransferencias($field = false) {
		$res = array(
			self::$_MOTIVO_TRANSFERENCIA_MUDANCA_RESIDENCIA => 'Mudança de Residência',		
			self::$_MOTIVO_TRANSFERENCIA_MUDANCA_ESCOLA => 'Mudança para outra escola particular/pública',		
			self::$_MOTIVO_TRANSFERENCIA_MUDANCA_ESTADO => 'Mudança para outro estado',		
			self::$_MOTIVO_TRANSFERENCIA_MUDANCA_PAIS => 'Mudança para outro país',		
			self::$_MOTIVO_TRANSFERENCIA_MUDANCA_PARTICULAR => 'Mudança para escola de rede particular '
		);
		
		if (!$field) return $res;
		return $res[$field];
	}		
	
	public static function getSalasalunosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolassalasalunos();
		return $produtos->getSalasalunos($queries, $page, $maxpage);
	}
	

	public function getSalasalunos($queries = array(), $page = 0, $maxpage = 0) {
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		//$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$idserie = (isset($queries['idserie'])) ? (int)$queries['idserie'] : false;
		$serie = (isset($queries['serie'])) ? (int)$queries['serie'] : false;
		$allseries = (isset($queries['allseries'])) ? (int)$queries['allseries'] : false;

		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " es1.id=$id ");
		//if ($idaluno) array_push($where, " es1.idaluno=$idaluno ");
		if ($idescola) array_push($where, " es1.idescola=$idescola ");
		if ($idserie) array_push($where, " es1.idserie=$idserie ");

		if($serie==100 || $allseries==100) {
            $idserie = '0';
            array_push($where, " ess1.serie = '$idserie' ");
        }else{
        	if ($serie) array_push($where, " ess1.serie='$serie' ");
			if($allseries) array_push($where, " ess1.serie='$allseries' ");
        }

		//if ($idsecretaria) array_push($where, " e1.idsecretaria=$idsecretaria ");
		if ($chave) array_push($where, " ((es1.serie LIKE '%$chave%')) ");
		if ($status) array_push($where, " es1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

	    $fields = "es1.*, e1.escola";
		if ($total) $fields = "COUNT(es1.id) as total";
		
		
		$ordem = "ORDER BY es1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolassalasalunos es1
						LEFT JOIN escolas e1 ON e1.id=es1.idescola
						LEFT JOIN escolasseries ess1 ON es1.idserie = ess1.id
					WHERE es1.excluido='nao'
						AND e1.excluido = 'nao'
						$w
					$ordem
					$limit";
		//die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getSalaalunoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getSalasalunos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getSalaalunoByIdHelper($id, $queries = array()) {
		$rows = new Escolassalasalunos();
		return $rows->getSalaalunoById($id, $queries);
	}	

	public function getIdsSeries($idescola, $queries = array()) {
		if ($idescola==0) return false;
		
		$queries['idescola'] = $idescola;
		$rows = $this->getSalasalunos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		
		$ids = "";
		
		foreach ($rows as $i=>$row):
			if($i==0) $ids = $row['idserie'];
			else $ids = $ids . ',' . $row['idserie'];
		endforeach;
		
		return $ids;
	}
	
	public static function getIdsSeriesHelper($idescola, $queries = array()) {
		$rows = new Escolassalasalunos();
		return $rows->getIdsSeries($idescola, $queries);
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Salasalunos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow(); 		
		}else {
			$novoRegistro = false;
			/*$historico = new Escolashistoricos_Escolassalasalunos();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);*/

		} 
		
		$row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
		//$row->idlocal = (array_key_exists('idlocal',$dados)) ? $dados['idlocal'] : $row->idlocal;
		$row->idserie = (array_key_exists('idserie',$dados)) ? $dados['idserie'] : $row->idserie;
		$row->idperiodo = (array_key_exists('idperiodo',$dados)) ? $dados['idperiodo'] : $row->idperiodo;
		$row->idsalunos = (array_key_exists('idsalunos',$dados)) ? $dados['idsalunos'] : $row->idsalunos;
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		/*
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		*/
                
		$row->save();
		
		if (array_key_exists('itens', $dados)) $this->setItens($dados['itens'], $row->id, $row->idserie, $row->logusuario, $row->logdata);
		return $row;
	}
	
	private function setItens($dados, $idescolasalasaluno, $idserie, $logusuario = false, $logdata = false) {
		
		if (!is_array($dados)) return;
		 
		$itens = new Escolassalasatribuicoes();
		$idsitens = $dados['idsitens'];
		$idsvinculos = $dados['idsvinculos'];
		$idssalas = $dados['idssalas'];
		$idsperiodos = $dados['idsperiodos'];
		$idsalunos = $dados['idsalunos'];
		
		$ids = array();
		foreach ($idsalunos as $i=>$id) {
			$d = array();
			
			$d['id'] = $idsitens[$i];
			$d['idescolasalasaluno'] = $idescolasalasaluno; 
			$d['idvinculo'] = trim(strip_tags($idsvinculos[$i]));
			$d['idserie'] = $idserie;
			$d['idsala'] = trim(strip_tags($idssalas[$i]));
			$d['idperiodo'] = trim(strip_tags($idsperiodos[$i]));
			$d['idaluno'] = trim(strip_tags($idsalunos[$i]));

			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $logusuario;
			$d['logdata'] = $logdata;
			//var_dump($d); die();
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
			//verificar as linhas comentadas acima, elas são responsaveis pelas 2 linhas mais cridas no cronogramasliehistorico
		}
		//die('aqui');
		$idsexcluir = implode(",", $ids);
		
		$excluidos = Escolassalasatribuicoes::getSalasatribuicoesHelper(array('idescolasalasaluno'=>$idescolasalasaluno, 'notids'=>$idsexcluir));
		$rows = new Escolassalasatribuicoes();
		foreach ($excluidos as $e=>$excluido):
			$excluido['excluido'] = 'sim';
			$excluido['logusuario'] = $logusuario;
			$excluido['logdata'] = $logdata;
			//
			$rows->save($excluido);	
		endforeach;
		
	}
}