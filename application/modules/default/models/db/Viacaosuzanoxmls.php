<?php

/**
 * Define o modelo Viacaosuzanoxmls
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Viacaosuzanoxmls extends Zend_Db_Table_Abstract {
	
    /**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "viacaosuzanoxmls";
	
    /**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getViacaosuzanoxmlsHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$viacaosuzanoxmls = new Viacaosuzanoxmls();
		return $viacaosuzanoxmls->getViacaosuzanoxmls($queries, $page, $maxpage);
	}
	
	public function getViacaosuzanoxmls($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;


		if ($sorting) { 
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY 1 ASC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM viacaosuzano_xml c1
					
					WHERE 1
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getViacaosuzanoxmlById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getViacaosuzanoxmls($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getViacaosuzanoxmlByIdHelper($id, $queries = array()) {
		$rows = new Viacaosuzanoxmls();
		return $rows->getCensocentralvagaById($id, $queries);
	}		
	


}