<?php

/**
 * Define o modelo Arquivos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Arquivos extends Zend_Db_Table_Abstract {
	
	
	public static $INDISPONIVEL = "indisponivel";
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "arquivos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";			
	
	/**
	 * Define o tipo de imagem indisponível
	 * @param string $val
	 * @return string
	 */
	public function indisponiveis($val) {
		$i = array(
			self::$INDISPONIVEL => 'noimage.gif'
		);
		
		if (isset($i[$val])) return $i[$val];
		return $i[self::$INDISPONIVEL];
	}
	
	/**
	 * Localiza o arquivo para download da imagem, caso não encontre então localiza o arquivo de indisponibilidade e retorna um array contendo informações de acesso
	 * @param string $id
	 * @param string $indisponivel
	 * @return array
	 */
	public function downloadArquivo($id, $indisponivel = "") {
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		$filename = "indisponivel.jpg";
		$sourceFilename = $this->indisponiveis($indisponivel);
		
		if ($row) {
			$filename = $row['filename'];
			$sourceFilename = $row['id'] . '.dat';

			$uploadDir = $this->getUploadDir("");
			$sourceFilename = $uploadDir . $sourceFilename;
		} else {
			$uploadDir = $this->getUploadDir("indisponiveis");
			$sourceFilename = $uploadDir . $sourceFilename;			
		}
		

		$filesize = filesize($sourceFilename);	
		
		return array(
			'filename' => $filename,
			'sourceFilename' => $sourceFilename,
			'filesize' => $filesize,
		);
		
	}
	

	/**
	 * Executa o upload da imagem vinda do formulário
	 * @param unknown_type $file_key
	 * @return Imagem
	 */
	public function getArquivoFromForm($file_key, $usuarioId = NULL, $usuarioTipo = NULL) {

		if (!isset($_FILES[$file_key])) return false;
		if ($_FILES[$file_key]['size']==0) return false;
		
  		$upload = $_FILES[$file_key]["name"];
  		$tmp_name = $_FILES[$file_key]["tmp_name"];	
  		$size = $_FILES[$file_key]["size"];
  		
  		
  		$arquivo = $this->getNew();
			
  		$uploadDir = $this->getUploadDir("");
  		$filename = $uploadDir . $arquivo['id'] . '.dat';
  		
  		move_uploaded_file($tmp_name,$filename);
	   	$dados = array(
	   		'id'=>$arquivo['id'],
			'filename' => $_FILES[$file_key]["name"],
			'size' => $_FILES[$file_key]["size"],
			'logdata' => date('Y-m-d G:i:s'), 
			'logusuario' => $usuarioId,
			'logusuariotipo' => $usuarioTipo,
			'excluido' => 'nao'
	   	);
	   	$row = $this->save($dados);
	   	return $arquivo['id'];
	}
	
	/**
	 * Executa o upload da imagem vinda do formulário
	 * @param unknown_type $file_key
	 * @return Imagem
	 */
	public function getArquivoFromFormsByPos($file_key, $pos = 0, $usuarioId = NULL, $usuarioTipo = NULL) {

		if (!isset($_FILES[$file_key])) return false;
		if ($_FILES[$file_key]['size'][$pos]==0) return false;
		
  		$upload = $_FILES[$file_key]["name"][$pos];
  		$tmp_name = $_FILES[$file_key]["tmp_name"][$pos];	
  		$size = $_FILES[$file_key]["size"][$pos];
  		

  		$arquivo = $this->getNew();
			
  		$uploadDir = $this->getUploadDir("");
  		$filename = $uploadDir . $arquivo['id'] . '.dat';
  		
  		move_uploaded_file($tmp_name,$filename);
	   	$dados = array(
	   		'id'=>$arquivo['id'],
			'filename' => $_FILES[$file_key]["name"][$pos],
			'size' => $_FILES[$file_key]["size"][$pos],
			'logdata' => date('Y-m-d G:i:s'), 
			'logusuario' => $usuarioId,
			'logusuariotipo' => $usuarioTipo,
			'excluido' => 'nao'
	   	);
	   	$row = $this->save($dados);
	   	return $arquivo['id'];		
	}	
	
	/**
	 * Exluir todas as imagens do servidor de imagem
	 */
	public function deleteAllFilesFromServer() {
		$uploadDir = $this->getUploadDir("");
		foreach(glob($uploadDir.'*.*') as $v) unlink($v);
		
		$uploadDir = $this->getUploadDir("lixeira");
		foreach(glob($uploadDir.'*.*') as $v) unlink($v);		
	}

	/**
	 * Exclui registro e envia imagem para a lixeira
	 * @param int $idimagem
	 */
	public function excluir($idimagem) {
		$row = $this->fetchRow("id=$idimagem");
		if (!$row) return; 
		
		$row = $row->toArray();
		$row['excluido'] = 'sim';
		$this->save($row);
		
		$filename = $row['filename'];
		$sourceFilename = $row['id'] . '.dat';

		$uploadDir = $this->getUploadDir("");
		$origem = $uploadDir . $sourceFilename;	
		
		$uploadDir = $this->getUploadDir("lixeira");
		$destino  = $uploadDir . $sourceFilename;
		
		if (file_exists($origem)) {
			rename($origem, $destino);	
		}
	}
	
	public function saveBase64($base64, $usuarioId = NULL, $usuarioTipo = NULL, $decode = true) {
		$arquivo = $this->getNew();
		
		if ($decode) $img = base64_decode($base64);
		else $img = $base64;
		
		$uploadDir = $this->getUploadDir("");
		$filename = $uploadDir . $arquivo['id'] . '.dat';
		
		$fp = fopen($filename, 'w');
		fwrite($fp, $img);
		fclose($fp);			
			
	   	$dados = array(
	   		'id'=>$arquivo['id'],
			'filename' => $arquivo['id'] . '.dat',
			'size' => strlen($img),
			'logdata' => date('Y-m-d G:i:s'), 
			'logusuario' => $usuarioId,
			'logusuariotipo' => $usuarioTipo,
			'excluido' => 'nao'
	   	);
	   	$row = $this->save($dados);

	   	return $row;
	}
	
	/**
	 * Gera um novo registro
	 * @return Imagens
	 */
	public function getNew() {
		$row = $this->createRow();
		$row->save();
		$row->ordem = $row->id;
		$row->save();
		return $row;
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Imagens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id");
		
		if (!$row) $row = $this->createRow();
		else if (!is_null($row['filename'])) {
			$novoRegistro = false;
		}
								
		if ($id>0)$row->id = $id;
		
		$row->filename = $dados['filename'];
		$row->size = $dados['size'];
		$row->logdata = $dados['logdata'];
		$row->logusuario = $dados['logusuario'];
		$row->logusuariotipo = $dados['logusuariotipo'];
		$row->excluido = $dados['excluido'];
		$row->save();
		
		if ($novoRegistro) {
			$row->ordem = $row->id;
			$row->save();
		}
		
		return $row;
	}		
	

	/**
	 * Localiza a extensão de um arquivo
	 * @param string $filename
	 * @return string
	 */
	private function findexts ($filename) {
		$filename = strtolower($filename) ;
		$exts = preg_split('/\./', $filename, -1, PREG_SPLIT_OFFSET_CAPTURE);
		return $exts[sizeof($exts)-1][0];
	} 		

	/**
	 * Retorna o caminho absoluto do servidor de imagens
	 * @param string $subdir
	 * @return string
	 */
	function getUploadDir($subdir = "") {
		$root = realpath(dirname(__FILE__) . '/../../../');
		$root = str_replace(chr(92), '/', $root);
		
		if ((substr($root, strlen($root)-1, 1))!="/") $root = $root."/";
		$root .= "servidor/arquivos/";
		
		if (((substr($subdir, strlen($subdir)-1, 1))!="/") && ($subdir!="")) $subdir = $subdir."/";
		
		return $root . $subdir;
	} 			
	
	
}