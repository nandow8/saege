<?php

/**
 * Define o modelo Financeiroregistrosfechamentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Financeiroregistrosfechamentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "financeiroregistrosfechamentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFinanceiroregistrosfechamentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$financeiroregistrosfechamentos = new Financeiroregistrosfechamentos();
		return $financeiroregistrosfechamentos->getFinanceiroregistrosfechamentos($queries, $page, $maxpage);
	}
	
	public function getFinanceiroregistrosfechamentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " f1.idescola = $idescola ");

$idtipoconvenio = (isset($queries["idtipoconvenio"])) ? $queries["idtipoconvenio"] : false;
		if ($idtipoconvenio) array_push($where, " f1.idtipoconvenio = $idtipoconvenio ");

$idescolasapms = (isset($queries["idescolasapms"])) ? $queries["idescolasapms"] : false;
		if ($idescolasapms) array_push($where, " f1.idescolasapms = $idescolasapms ");

$idprogramasgov = (isset($queries["idprogramasgov"])) ? $queries["idprogramasgov"] : false;
		if ($idprogramasgov) array_push($where, " f1.idprogramasgov = $idprogramasgov ");

$saldoaplicacaofinanceira = (isset($queries["saldoaplicacaofinanceira"])) ? $queries["saldoaplicacaofinanceira"] : false;
		if ($saldoaplicacaofinanceira) array_push($where, " f1.saldoaplicacaofinanceira = $saldoaplicacaofinanceira ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM financeiroregistrosfechamentos f1
					
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFinanceiroregistrofechamentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFinanceiroregistrosfechamentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFinanceiroregistrofechamentoByIdHelper($id, $queries = array()) {
		$rows = new Financeiroregistrosfechamentos();
		return $rows->getFinanceiroregistrofechamentoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Financeiroregistrosfechamentos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idtipoconvenio = (array_key_exists("idtipoconvenio",$dados)) ? $dados["idtipoconvenio"] : $row->idtipoconvenio;
 $row->idescolasapms = (array_key_exists("idescolasapms",$dados)) ? $dados["idescolasapms"] : $row->idescolasapms;
 $row->idprogramasgov = (array_key_exists("idprogramasgov",$dados)) ? $dados["idprogramasgov"] : $row->idprogramasgov;
 $row->saldoaplicacaofinanceira = (array_key_exists("saldoaplicacaofinanceira",$dados)) ? $dados["saldoaplicacaofinanceira"] : $row->saldoaplicacaofinanceira;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
		 $row->finalizado = (array_key_exists("finalizado",$dados)) ? $dados["finalizado"] : $row->finalizado;

						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		//return true;		
		$row->save();
		if((array_key_exists("finalizado",$dados)) && $row->finalizado == 'sim') $this->setFechamentosContas($row);
		
		return $row;
	}

	private function setFechamentosContas($dados)
	{
		$contasregistros = new Financeirocontasregistros();

		$rows = $contasregistros->getFinanceirocontasregistros(array('faturado' => 'nao', 'idtipoconvenio' => $dados->idtipoconvenio, 'idprogramasgov' => $dados->idprogramasgov, 'idescola' => $dados->idescola));

		foreach($rows as $k=>$row)
		{
			$row['faturado'] = 'sim';
			$row['logdata'] = date('Y-m-d G:i:s');

			//var_dump($row);die();
			$contasregistros->save($row);

		}
	}
}