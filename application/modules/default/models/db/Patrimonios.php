<?php

/**
 * Define o modelo Patrimonios
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Patrimonios extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "patrimonios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getPatrimoniosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$patrimonios = new Patrimonios();
		return $patrimonios->getPatrimonios($queries, $page, $maxpage);
	}
	
	public function getPatrimonios($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
			
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " p1.idsecretaria = $idsecretaria ");

$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " p1.iddepartamento = $iddepartamento ");

$idsetor = (isset($queries["idsetor"])) ? $queries["idsetor"] : false;
		if ($idsetor) array_push($where, " p1.idsetor = $idsetor ");

$idtipo = (isset($queries["idtipo"])) ? $queries["idtipo"] : false;
		if ($idtipo) array_push($where, " p1.idtipo = $idtipo ");

$idmarca = (isset($queries["idmarca"])) ? $queries["idmarca"] : false;
		if ($idmarca) array_push($where, " p1.idmarca = $idmarca ");

$idfornecedor = (isset($queries["idfornecedor"])) ? $queries["idfornecedor"] : false;
		if ($idfornecedor) array_push($where, " p1.idfornecedor = $idfornecedor ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " p1.origem LIKE '%$origem%' ");

$iddepartamentosecretaria = (isset($queries["iddepartamentosecretaria"])) ? $queries["iddepartamentosecretaria"] : false;
		if ($iddepartamentosecretaria) array_push($where, " p1.iddepartamentosecretaria = $iddepartamentosecretaria ");

$idresponsavelsecretaria = (isset($queries["idresponsavelsecretaria"])) ? $queries["idresponsavelsecretaria"] : false;
		if ($idresponsavelsecretaria) array_push($where, " p1.idresponsavelsecretaria = $idresponsavelsecretaria ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " p1.idescola = $idescola ");

$idresponsavelescola = (isset($queries["idresponsavelescola"])) ? $queries["idresponsavelescola"] : false;
		if ($idresponsavelescola) array_push($where, " p1.idresponsavelescola = $idresponsavelescola ");

$numeropatrimonio = (isset($queries["numeropatrimonio"])) ? $queries["numeropatrimonio"] : false;
		if ($numeropatrimonio) array_push($where, " p1.numeropatrimonio LIKE '%$numeropatrimonio%' ");

$patrimonio = (isset($queries["patrimonio"])) ? $queries["patrimonio"] : false;
		if ($patrimonio) array_push($where, " p1.patrimonio LIKE '%$patrimonio%' ");

$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " p1.descricoes = '$descricoes' ");

$cor = (isset($queries["cor"])) ? $queries["cor"] : false;
		if ($cor) array_push($where, " p1.cor LIKE '%$cor%' ");

$aprovado = (isset($queries["aprovado"])) ? $queries["aprovado"] : false;
		if ($aprovado) array_push($where, " p1.aprovado LIKE '%$aprovado%' ");

$modelo = (isset($queries["modelo"])) ? $queries["modelo"] : false;
		if ($modelo) array_push($where, " p1.modelo LIKE '%$modelo%' ");

$serie = (isset($queries["serie"])) ? $queries["serie"] : false;
		if ($serie) array_push($where, " p1.serie LIKE '%$serie%' ");

$numerolote = (isset($queries["numerolote"])) ? $queries["numerolote"] : false;
		if ($numerolote) array_push($where, " p1.numerolote LIKE '%$numerolote%' ");

$anofabricacao = (isset($queries["anofabricacao"])) ? $queries["anofabricacao"] : false;
		if ($anofabricacao) array_push($where, " p1.anofabricacao = '$anofabricacao' ");

$vidautil = (isset($queries["vidautil"])) ? $queries["vidautil"] : false;
		if ($vidautil) array_push($where, " p1.vidautil = '$vidautil' ");

$quantidade = (isset($queries["quantidade"])) ? $queries["quantidade"] : false;
		if ($quantidade) array_push($where, " p1.quantidade = '$quantidade' ");

$aprovacao = (isset($queries["aprovacao"])) ? $queries["aprovacao"] : false;
		if ($aprovacao) array_push($where, " p1.aprovacao LIKE '%$aprovacao%' ");

$statusaprovacao = (isset($queries["statusaprovacao"])) ? $queries["statusaprovacao"] : false;
		if ($statusaprovacao) array_push($where, " p1.statusaprovacao LIKE '%$statusaprovacao%' ");

$observacoesaprovacoes = (isset($queries["observacoesaprovacoes"])) ? $queries["observacoesaprovacoes"] : false;
		if ($observacoesaprovacoes) array_push($where, " p1.observacoesaprovacoes = '$observacoesaprovacoes' ");

$baixa = (isset($queries["baixa"])) ? $queries["baixa"] : false;
		if ($baixa) array_push($where, " p1.baixa LIKE '%$baixa%' ");

$statusbaixa = (isset($queries["statusbaixa"])) ? $queries["statusbaixa"] : false;
		if ($statusbaixa) array_push($where, " p1.statusbaixa LIKE '%$statusbaixa%' ");

$observacoesbaixa = (isset($queries["observacoesbaixa"])) ? $queries["observacoesbaixa"] : false;
		if ($observacoesbaixa) array_push($where, " p1.observacoesbaixa = '$observacoesbaixa' ");

$roubofurto = (isset($queries["roubofurto"])) ? $queries["roubofurto"] : false;
		if ($roubofurto) array_push($where, " p1.roubofurto LIKE '%$roubofurto%' ");

$statusroubofurto = (isset($queries["statusroubofurto"])) ? $queries["statusroubofurto"] : false;
		if ($statusroubofurto) array_push($where, " p1.statusroubofurto LIKE '%$statusroubofurto%' ");

$observacoesroubofurto = (isset($queries["observacoesroubofurto"])) ? $queries["observacoesroubofurto"] : false;
		if ($observacoesroubofurto) array_push($where, " p1.observacoesroubofurto = '$observacoesroubofurto' ");

$dataroubofurto_i = (isset($queries["dataroubofurto_i"])) ? $queries["dataroubofurto_i"] : false;
		if ($dataroubofurto_i) array_push($where, " p1.dataroubofurto >= '$dataroubofurto_i' ");

$dataroubofurto_f = (isset($queries["dataroubofurto_f"])) ? $queries["dataroubofurto_f"] : false;
		if ($dataroubofurto_f) array_push($where, " p1.dataroubofurto <= '$dataroubofurto_f' ");

$depreciacao = (isset($queries["depreciacao"])) ? $queries["depreciacao"] : false;
		if ($depreciacao) array_push($where, " p1.depreciacao LIKE '%$depreciacao%' ");

$descricoesdepreciacao = (isset($queries["descricoesdepreciacao"])) ? $queries["descricoesdepreciacao"] : false;
		if ($descricoesdepreciacao) array_push($where, " p1.descricoesdepreciacao = '$descricoesdepreciacao' ");

$valordepreciacao = (isset($queries["valordepreciacao"])) ? $queries["valordepreciacao"] : false;
		if ($valordepreciacao) array_push($where, " p1.valordepreciacao = $valordepreciacao ");

$percentualdepreciacao = (isset($queries["percentualdepreciacao"])) ? $queries["percentualdepreciacao"] : false;
		if ($percentualdepreciacao) array_push($where, " p1.percentualdepreciacao = $percentualdepreciacao ");

$tipodepreciacao = (isset($queries["tipodepreciacao"])) ? $queries["tipodepreciacao"] : false;
		if ($tipodepreciacao) array_push($where, " p1.tipodepreciacao LIKE '%$tipodepreciacao%' ");

$caracteristicas = (isset($queries["caracteristicas"])) ? $queries["caracteristicas"] : false;
		if ($caracteristicas) array_push($where, " p1.caracteristicas = '$caracteristicas' ");

$dimensoes = (isset($queries["dimensoes"])) ? $queries["dimensoes"] : false;
		if ($dimensoes) array_push($where, " p1.dimensoes = '$dimensoes' ");

$datacompra_i = (isset($queries["datacompra_i"])) ? $queries["datacompra_i"] : false;
		if ($datacompra_i) array_push($where, " p1.datacompra >= '$datacompra_i' ");

$datacompra_f = (isset($queries["datacompra_f"])) ? $queries["datacompra_f"] : false;
		if ($datacompra_f) array_push($where, " p1.datacompra <= '$datacompra_f' ");

$descricoesdobem = (isset($queries["descricoesdobem"])) ? $queries["descricoesdobem"] : false;
		if ($descricoesdobem) array_push($where, " p1.descricoesdobem = '$descricoesdobem' ");

$codigosbarras = (isset($queries["codigosbarras"])) ? $queries["codigosbarras"] : false;
		if ($codigosbarras) array_push($where, " p1.codigosbarras LIKE '%$codigosbarras%' ");

$qrcode = (isset($queries["qrcode"])) ? $queries["qrcode"] : false;
		if ($qrcode) array_push($where, " p1.qrcode = '$qrcode' ");

$idperfil = (isset($queries["idperfil"])) ? $queries["idperfil"] : false;
		if ($idperfil) array_push($where, " p1.idperfil = '$idperfil' ");

$codescola = (isset($queries["codescola"])) ? $queries["codescola"] : false;
		if ($codescola) array_push($where, " p1.codescola = '$codescola' ");

$atribuido = (isset($queries["atribuido"])) ? $queries["atribuido"] : '';
		if($atribuido && $atribuido == "atribuido"):
			array_push($where, " p1.idperfil is not null AND p1.idperfil > 0");
		elseif($atribuido && $atribuido == "naoatribuido"):
			array_push($where, " (p1.idperfil is null OR p1.idperfil = 0)");
		endif;

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.statusbaixa LIKE '%$status%' ");

$origemaprovacao = (isset($queries["origemaprovacao"])) ? $queries["origemaprovacao"] : false;
		if ($origemaprovacao) array_push($where, " p1.origemaprovacao LIKE '%$origemaprovacao%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*, m1.marca, pt1.tipo"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM patrimonios p1
					LEFT JOIN patrimoniostipos pt1 ON pt1.id = p1.idtipo
					LEFT JOIN marcas m1 ON m1.id = p1.idmarca
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPatrimonioById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPatrimonios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPatrimonioByIdHelper($id, $queries = array()) {
		$rows = new Patrimonios();
		return $rows->getPatrimonioById($id, $queries);
	}
	/**
	 * Verifique se o n° do patrimônio existe no banco
	 * @param int id - id do patrimônio
	 * @param int numberPatrimonio - número do patrimônio
	 * @return boolean - (true ou false)
	*/
	public function approvePatrimonioNumber($id, $numberPatrimonio){
		if($id==0 && $numberPatrimonio==0) return false;

		$fields = " COUNT(numeropatrimonio) as total";

		$strsql = "SELECT $fields 
					FROM patrimonios p1
					LEFT JOIN patrimoniostipos pt1 ON pt1.id = p1.idtipo
					LEFT JOIN marcas m1 ON m1.id = p1.idmarca
					WHERE p1.id != '$id' AND p1.numeropatrimonio = '$numberPatrimonio'";	
		
		$db = Zend_Registry::get('db');

		$rows = $db->fetchAll($strsql);
		if(sizeof($rows)==0) return false;
		return $rows[0];
	}
	public static function approvePatrimonioNumberHelper($id, $numberPatrimonio){
		$rows = new Patrimonios();
		return $rows->approvePatrimonioNumber($id, $numberPatrimonio);
	}
	/**
	* Gerar data atual (dia/mês/ano) com formato português
	* @return string data
	*/
	public function getCurrentData(){
		$data = date('D');
	    $mes = date('M');
	    $dia = date('d');
	    $ano = date('Y');
    
	    $semana = array(
	        'Sun' => 'Domingo', 
	        'Mon' => 'Segunda-Feira',
	        'Tue' => 'Terça-Feira',
	        'Wed' => 'Quarta-Feira',
	        'Thu' => 'Quinta-Feira',
	        'Fri' => 'Sexta-Feira',
	        'Sat' => 'Sábado'
	    );
    
	    $mes_extenso = array(
	        'Jan' => 'Janeiro',
	        'Feb' => 'Fevereiro',
	        'Mar' => 'Marco',
	        'Apr' => 'Abril',
	        'May' => 'Maio',
	        'Jun' => 'Junho',
	        'Jul' => 'Julho',
	        'Aug' => 'Agosto',
	        'Nov' => 'Novembro',
	        'Sep' => 'Setembro',
	        'Oct' => 'Outubro',
	        'Dec' => 'Dezembro'
	    );
	    
        $data = "{$dia} de " . $mes_extenso["$mes"] . " de {$ano}";
        return $data;
	}
	
	/**
	* Diminui o tamanho da palavra
	* @param string word
	* @param int max
	* @return - string palavra com X caracteres
	*/
	public static function cutWord($word, $max){
		if(strlen($word) > $max){
			$word = substr($word, 0, $max);
		}
		return $word;
	}

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Patrimonios
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
	 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
	 $row->idsetor = (array_key_exists("idsetor",$dados)) ? $dados["idsetor"] : $row->idsetor;
	 $row->idtipo = (array_key_exists("idtipo",$dados)) ? $dados["idtipo"] : $row->idtipo;
	 $row->idmarca = (array_key_exists("idmarca",$dados)) ? $dados["idmarca"] : $row->idmarca;
	 $row->idfornecedor = (array_key_exists("idfornecedor",$dados)) ? $dados["idfornecedor"] : $row->idfornecedor;
	 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
	 $row->iddepartamentosecretaria = (array_key_exists("iddepartamentosecretaria",$dados)) ? $dados["iddepartamentosecretaria"] : $row->iddepartamentosecretaria;
	 $row->idresponsavelsecretaria = (array_key_exists("idresponsavelsecretaria",$dados)) ? $dados["idresponsavelsecretaria"] : $row->idresponsavelsecretaria;
	 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
	 $row->idresponsavelescola = (array_key_exists("idresponsavelescola",$dados)) ? $dados["idresponsavelescola"] : $row->idresponsavelescola;
	 $row->numeropatrimonio = (array_key_exists("numeropatrimonio",$dados)) ? $dados["numeropatrimonio"] : $row->numeropatrimonio;
	 $row->patrimonio = (array_key_exists("patrimonio",$dados)) ? $dados["patrimonio"] : $row->patrimonio;
	 $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
	 $row->cor = (array_key_exists("cor",$dados)) ? $dados["cor"] : $row->cor;
	 $row->aprovado = (array_key_exists("aprovado",$dados)) ? $dados["aprovado"] : $row->aprovado;
	 $row->modelo = (array_key_exists("modelo",$dados)) ? $dados["modelo"] : $row->modelo;
	 $row->serie = (array_key_exists("serie",$dados)) ? $dados["serie"] : $row->serie;
	 $row->numerolote = (array_key_exists("numerolote",$dados)) ? $dados["numerolote"] : $row->numerolote;
	 $row->anofabricacao = (array_key_exists("anofabricacao",$dados)) ? $dados["anofabricacao"] : $row->anofabricacao;
	 $row->vidautil = (array_key_exists("vidautil",$dados)) ? $dados["vidautil"] : $row->vidautil;
	 $row->quantidade = (array_key_exists("quantidade",$dados)) ? $dados["quantidade"] : $row->quantidade;
	 $row->aprovacao = (array_key_exists("aprovacao",$dados)) ? $dados["aprovacao"] : $row->aprovacao;
	 $row->statusaprovacao = (array_key_exists("statusaprovacao",$dados)) ? $dados["statusaprovacao"] : $row->statusaprovacao;
	 $row->observacoesaprovacoes = (array_key_exists("observacoesaprovacoes",$dados)) ? $dados["observacoesaprovacoes"] : $row->observacoesaprovacoes;
	 $row->baixa = (array_key_exists("baixa",$dados)) ? $dados["baixa"] : $row->baixa;
	 $row->statusbaixa = (array_key_exists("statusbaixa",$dados)) ? $dados["statusbaixa"] : $row->statusbaixa;
	 $row->observacoesbaixa = (array_key_exists("observacoesbaixa",$dados)) ? $dados["observacoesbaixa"] : $row->observacoesbaixa;
	 $row->roubofurto = (array_key_exists("roubofurto",$dados)) ? $dados["roubofurto"] : $row->roubofurto;
	 $row->statusroubofurto = (array_key_exists("statusroubofurto",$dados)) ? $dados["statusroubofurto"] : $row->statusroubofurto;
	 $row->observacoesroubofurto = (array_key_exists("observacoesroubofurto",$dados)) ? $dados["observacoesroubofurto"] : $row->observacoesroubofurto;
	 $row->dataroubofurto = (array_key_exists("dataroubofurto",$dados)) ? $dados["dataroubofurto"] : $row->dataroubofurto;
	 $row->idarquivoroubofurto = (array_key_exists("idarquivoroubofurto",$dados)) ? $dados["idarquivoroubofurto"] : $row->idarquivoroubofurto;
	 $row->depreciacao = (array_key_exists("depreciacao",$dados)) ? $dados["depreciacao"] : $row->depreciacao;
	 $row->descricoesdepreciacao = (array_key_exists("descricoesdepreciacao",$dados)) ? $dados["descricoesdepreciacao"] : $row->descricoesdepreciacao;
	 $row->valordepreciacao = (array_key_exists("valordepreciacao",$dados)) ? $dados["valordepreciacao"] : $row->valordepreciacao;
	 $row->percentualdepreciacao = (array_key_exists("percentualdepreciacao",$dados)) ? $dados["percentualdepreciacao"] : $row->percentualdepreciacao;
	 $row->tipodepreciacao = (array_key_exists("tipodepreciacao",$dados)) ? $dados["tipodepreciacao"] : $row->tipodepreciacao;
	 $row->idfoto = (array_key_exists("idfoto",$dados)) ? $dados["idfoto"] : $row->idfoto;
	 $row->caracteristicas = (array_key_exists("caracteristicas",$dados)) ? $dados["caracteristicas"] : $row->caracteristicas;
	 $row->dimensoes = (array_key_exists("dimensoes",$dados)) ? $dados["dimensoes"] : $row->dimensoes;
	 $row->datacompra = (array_key_exists("datacompra",$dados)) ? $dados["datacompra"] : $row->datacompra;
	 $row->descricoesdobem = (array_key_exists("descricoesdobem",$dados)) ? $dados["descricoesdobem"] : $row->descricoesdobem;
	 $row->codigosbarras = (array_key_exists("codigosbarras",$dados)) ? $dados["codigosbarras"] : $row->codigosbarras;
	 $row->qrcode = (array_key_exists("qrcode",$dados)) ? $dados["qrcode"] : $row->qrcode;
	 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
	 //$row->origemaprovacao = (array_key_exists("origemaprovacao",$dados)) ? $dados["origemaprovacao"] : $row->origemaprovacao;

	 //$row->logdataescola = (array_key_exists("logdataescola",$dados)) ? $dados["logdataescola"] : $row->logdataescola;
	 
	 //$row->logusuarioescola = (array_key_exists("logusuarioescola",$dados)) ? $dados["logusuarioescola"] : $row->logusuarioescola;
	$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
	$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
	$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
	$row->idperfil = (array_key_exists("idperfil", $dados)) ? $dados["idperfil"] : '';
	$row->codescola = (array_key_exists("codescola", $dados)) ? $dados["codescola"] : '';

	
		$row->save();
		
		return $row;
	}
	
}