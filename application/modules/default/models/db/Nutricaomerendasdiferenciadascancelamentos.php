<?php

/**
 * Define o modelo Nutricaomerendasdiferenciadascancelamentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Nutricaomerendasdiferenciadascancelamentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "nutricaomerendasdiferenciadascancelamentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getNutricaomerendasdiferenciadascancelamentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaomerendasdiferenciadascancelamentos = new Nutricaomerendasdiferenciadascancelamentos();
		return $nutricaomerendasdiferenciadascancelamentos->getNutricaomerendasdiferenciadascancelamentos($queries, $page, $maxpage);
	}
	
	public function getNutricaomerendasdiferenciadascancelamentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " n1.id = $id ");
		
		
		$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " n1.datalancamento >= '$datalancamento_i' ");

                $datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " n1.datalancamento <= '$datalancamento_f' ");

                $idusuario = (isset($queries["idusuario"])) ? $queries["idusuario"] : false;
		if ($idusuario) array_push($where, " n1.idusuario = '$idusuario' ");

                $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " n1.idescola = '$idescola' ");

                $idlocal = (isset($queries["idlocal"])) ? $queries["idlocal"] : false;
		if ($idlocal) array_push($where, " n1.idlocal = '$idlocal' ");

		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		if ($idaluno) array_push($where, " n1.idaluno = $idaluno ");		

                $motivo = (isset($queries["motivo"])) ? $queries["motivo"] : false;
		if ($motivo) array_push($where, " n1.motivo LIKE '%$motivo%' ");

                $posicao = (isset($queries["posicao"])) ? $queries["posicao"] : false;
		if ($posicao) array_push($where, " n1.posicao LIKE '%$posicao%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "n1.*"; 
		;
		
		if ($total) $fields = "COUNT(n1.id) as total";
		
		$ordem = "ORDER BY n1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM nutricaomerendasdiferenciadascancelamentos n1
                                            
					WHERE n1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
                
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
                		
		return $db->fetchAll($strsql);			
	}	
	
	public function getNutricaomerendasdiferenciadascancelamentosById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNutricaomerendasdiferenciadascancelamentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNutricaomerendasdiferenciadascancelamentosByIdHelper($id, $queries = array()) {
		$rows = new Nutricaomerendasdiferenciadascancelamentos();
		return $rows->getNutricaomerendasdiferenciadascancelamentosById($id, $queries);
	}		
	
	public function getNutricaomerendasdiferenciadascancelamentosByDestino($queries = array()) {
		
		$queries['order'] = 'ORDER BY n1.id DESC';
		$rows = $this->getNutricaomerendasdiferenciadascancelamentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaomerendasdiferenciadascancelamentos
     */
	public function save($dados) {
		
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
                $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
                $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
                $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
                $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
                $row->idaluno = (array_key_exists("idaluno",$dados)) ? $dados["idaluno"] : $row->idaluno;
                $row->aluno = (array_key_exists("aluno",$dados)) ? $dados["aluno"] : $row->aluno;
                $row->medico = (array_key_exists("medico",$dados)) ? $dados["medico"] : $row->medico;
                $row->motivo = (array_key_exists("motivo",$dados)) ? $dados["motivo"] : $row->motivo;
                $row->crmcrn = (array_key_exists("crmcrn",$dados)) ? $dados["crmcrn"] : $row->crmcrn;
                $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
                $row->datalaudo = (array_key_exists("datalaudo",$dados)) ? $dados["datalaudo"] : $row->datalaudo;
                $row->laudo = (array_key_exists("laudo",$dados)) ? $dados["laudo"] : $row->laudo;
                $row->prescricao = (array_key_exists("prescricao",$dados)) ? $dados["prescricao"] : $row->prescricao;
                $row->justificativa = (array_key_exists("justificativa",$dados)) ? $dados["justificativa"] : $row->justificativa;
                $row->posicao = (array_key_exists("posicao",$dados)) ? $dados["posicao"] : $row->posicao;
                if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}
