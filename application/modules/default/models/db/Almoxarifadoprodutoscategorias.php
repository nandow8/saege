<?php

/**
 * Controle da classe Model Almoxarifadoprodutoscategorias do sistema
 *
 * @author		Fernando Alves		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) FJMX.
 * @version     1.0
 */
class Almoxarifadoprodutoscategorias extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "almoxarifadoprodutoscategorias";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getAlmoxarifadoprodutoscategoriasHelper($queries = array(), $page = 0, $maxpage = 0) {
        $almoxarifadoprodutoscategorias = new Almoxarifadoprodutoscategorias();
        return $almoxarifadoprodutoscategorias->getAlmoxarifadoprodutoscategorias($queries, $page, $maxpage);
    }

    public function getAlmoxarifadoprodutoscategorias($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " a1.id = $id ");
            
        $nome = (isset($queries["nome"])) ? $queries["nome"] : false;
        if ($nome)
            array_push($where, " a1.nome LIKE '%$nome%' ");        

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " a1.status LIKE '%$status%' ");
 
        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "a1.*";
        ;

        if ($total)
            $fields = "COUNT(a1.id) as total";

        $ordem = "ORDER BY a1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM almoxarifadoprodutoscategorias a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getAlmoxarifadoprodutoscategoriasById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getAlmoxarifadoprodutoscategorias($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getAlmoxarifadoprodutoscategoriasByIdHelper($id, $queries = array()) {
        $rows = new Almoxarifadoprodutoscategorias();
        return $rows->getAlmoxarifadoprodutoscategoriasById($id, $queries);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadoprodutoscategorias
     */
    public function save($dados) {
        /* echo '<pre>';
          print_r($dados);
          die(); */

        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        } 
       
        $row->nome = (array_key_exists("nome", $dados)) ? $dados["nome"] : $row->nome;
        $row->descricao = (array_key_exists("descricao", $dados)) ? $dados["descricao"] : $row->descricao;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;


        $row->save();

        return $row;
    }

}
