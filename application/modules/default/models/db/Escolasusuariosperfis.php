<?php

/**
 * Define o modelo Perfis de Usuário (Escolasusuariosperfis)
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasusuariosperfis extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasusuariosperfis";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";	
	
	/**
	 * Retorna um array com a lista de perfis e limites
	 * $param int idperfil
	 * @return array
	 */
	public function getLimitesByIdperfil($idperfil) {
		$db = Zend_Registry::get('db');
		$strsql = "SELECT l1.*, 
						pl1.visualizar, 
						pl1.adicionar, 
						pl1.editar, 
						pl1.excluir 
					FROM escolasusuarioslimites l1 
						LEFT JOIN escolasusuariosperfislimites pl1 ON pl1.idperfil=$idperfil 
							AND pl1.idlimite = l1.id
					ORDER BY modulo, ordemalias, limite";
		return $db->fetchAll($strsql);
	}	
	
	/**
	 * Retorna um array com  a lista de todos os perfil
	 * @return array
	 */
	public function getPerfis($idescola = "", $queries=array(), $page = 0, $maxpage = 0) {
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idescola = (isset($queries['idescola'])) ? $queries['idescola'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? $queries['idsecretaria'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		//var_dump($queries); die();
		$_strsql = "";
		if((int)$idescola > 0) $_strsql = " AND (p1.idescola = $idescola OR p1.idescola = 0)";
		$where = array();
		
		if ($id > 0) array_push($where, " p1.id=$id ");
		if ($idescola) array_push($where, " p1.idescola=$idescola ");
		//if ($idsecretaria) array_push($where, " p1.idsecretaria=$idsecretaria ");
		if ($idsecretaria) array_push($where, " ( FIND_IN_SET(p1.idsecretaria, '$idsecretaria')  ) ");		
		if ($chave) array_push($where, " ((p1.perfil LIKE '%$chave%')) ");
		if ($status) array_push($where, " p1.status='$status' ");
		if ($origem) {
			array_push($where, " p1.origem='$origem' ");
		}else{
			//array_push($where, " p1.origem='ADM' ");
		}
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $_strsql = "AND ($w)";
		
		$fields = "p1.*, e1.escola,
						(SELECT COUNT(u1.id) 
						FROM escolasusuarios u1 
						WHERE u1.idperfil=p1.id 
							AND u1.excluido='nao') as qtdusuarios ";
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
		
		$db = Zend_Registry::get('db');
		$strsql = "SELECT $fields 
					FROM escolasusuariosperfis p1 
						LEFT JOIN escolas e1 ON e1.id=p1.idescola 
					WHERE p1.excluido='nao' 
						$_strsql
					ORDER BY p1.perfil 
						$limit";
		//if(!$total)die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);	
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasusuariosperfis
     */
	public function save($dados) {
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			//$x_rows = new X_Escolasusuariosperfis();
		//	$x_rows->arquiva($row->toArray());
		} 
				
		//if ($id>0)$row->id = $id;
		$row->perfil = $dados['perfil']; 
		$row->idescola = $dados['idescola'];
		$row->status = $dados['status'];
		$row->planosaulas = (isset($dados['planosaulas'])) ? $dados['planosaulas'] : '';
		if(!$row) $row->multiplasescolas = $dados['multiplasescolas'];
		if(!$row) $row->origem = $dados['origem']; 
		$row->excluido = $dados['excluido'];
		$row->logusuario = $dados['logusuario'];
		$row->logdata = $dados['logdata'];
		
		if((isset($dados['origem'])) && ($dados['origem']=="admin")){
			if ((int)$row->idsecretaria==0) {
				$row->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
			}
		}elseif (Escolasusuarios::getUsuario()){

			if ((int)$row->idsecretaria==0) {
				$row->idsecretaria = Escolasusuarios::getEscolaAtiva( Escolasusuarios::getUsuario('id'), 'idsecretaria' );
			}
		}
		$row->save();
		
		if (isset($dados['limites'])) $this->setLimites($dados['limites'], $row->id);
	}
	
	/**
	  * 
	  * MULTIPLOS ACESSOS A ESCOLAS
	  * 
	  * 
	  * */
	
	public function getPerfilOrigem($origem, $queries = array()) {
		if (!$origem) return false;
		
		$queries['origem'] = $origem;		
		$rows = $this->getPerfis('', $queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}

	public function getPerfilByIdSecretaria($idsecretaria, $queries = array()) {
		if (!$idsecretaria) return false;
		
		$queries['idsecretaria'] = $idsecretaria;		
		$rows = $this->getPerfis('', $queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public function getPerfilById($id, $queries = array()) {
		if ($id <= 0) return false;
		
		$queries['id'] = $id;		
		$rows = $this->getPerfis('', $queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	/**
	 * Define os limites do perfil
	 * @param array $dados
	 * @param int $idperfil
	 */
	private function setLimites($dados, $idperfil) {
		$perfisLimites = new Escolasusuariosperfislimites();

		foreach ($this->getLimitesByIdperfil($idperfil) as $k=>$v) {
			$idlimite = $v['id'];
			$visualizar = isset($dados['visualizar'][$idlimite]) 
				? $dados['visualizar'][$idlimite] 
				: 'nao';

			$adicionar = isset($dados['adicionar'][$idlimite]) 
				? $dados['adicionar'][$v['id']] 
				: 'nao';
				
			$editar = isset($dados['editar'][$idlimite]) 
				? $dados['editar'][$idlimite] 
				: 'nao';
				
			$excluir = isset($dados['excluir'][$idlimite]) 
				? $dados['excluir'][$idlimite] 
				: 'nao';
							
			$dados_limites = array(
				'idperfil' => $idperfil,
				'idlimite' => $idlimite,
				'visualizar' => $visualizar,
				'adicionar' => $adicionar,
				'editar' => $editar,
				'excluir' => $excluir
			);
			
			var_dump($dados_limites);
			
			$perfisLimites->save($dados_limites);			
		}
	}
	
}