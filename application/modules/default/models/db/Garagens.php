<?php

class Garagens extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "garagens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getGaragensHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Garagens();
		return $produtos->getGaragens($queries, $page, $maxpage);
	}
	
	public function getGaragens($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " g1.id=$id ");	
		
		if ($chave) array_push($where, " ((g1.resumo LIKE '%$chave%') OR (g1.descricoes LIKE '%$chave%')) ");
		
		$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " g1.titulo LIKE '%$titulo%' ");

		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " g1.descricoes LIKE '%$descricoes%' ");

		if ($status) array_push($where, " g1.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		//$fields = "g1.*, lf1.ano, lf1.modelo, lf1.placa, m1.marca, f1.fabricante";
		$fields = "g1.*, lf1.placa, m1.marca, f1.fabricante";
		$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado "; 
		
		if ($total) $fields = "COUNT(g1.id) as total";
		
		$ordem = "ORDER BY g1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM garagens g1 
					 LEFT JOIN logisticafrotas lf1 ON lf1.id = g1.idfrotas
					 LEFT JOIN marcas m1 ON m1.id = lf1.idmarca 
					 LEFT JOIN fabricantes f1 ON f1.id = lf1.idfabricante
					 LEFT JOIN enderecos e_idendereco ON e_idendereco.id=g1.idendereco 
					 LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado	
					WHERE g1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getGaragemById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getGaragens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getGaragemByIdHelper($id, $queries = array()) {
		$rows = new Garagens();
		return $rows->getGaragemById($id, $queries);
	}	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Garagens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row){
                    $row = $this->createRow();
                    $row->datacriacao = date('Y-m-d G:i:s');
		}else {
			$novoRegistro = false;
		} 

		$row->idfrotas = (array_key_exists('idfrotas',$dados)) ? $dados['idfrotas'] : $row->idfrotas;	
		$row->idendereco = (array_key_exists('idendereco',$dados)) ? $dados['idendereco'] : $row->idendereco;	
		$row->titulo = (array_key_exists('titulo',$dados)) ? $dados['titulo'] : $row->titulo;		
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;	
		$row->quantidade = (array_key_exists('quantidade',$dados)) ? $dados['quantidade'] : $row->quantidade;		
		$row->latitude = (array_key_exists('latitude',$dados)) ? $dados['latitude'] : $row->latitude;		
		$row->longitude = (array_key_exists('longitude',$dados)) ? $dados['longitude'] : $row->longitude;

		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		$row->save();

		return $row;
	}
	
	
}