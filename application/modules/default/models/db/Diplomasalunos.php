<?php

/**
 * Define o modelo Diplomasalunos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Diplomasalunos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "diplomasalunos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDiplomasalunosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$diplomasalunos = new Diplomasalunos();
		return $diplomasalunos->getDiplomasalunos($queries, $page, $maxpage);
	}
	
	public function getDiplomasalunos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		$idsecretaria = (isset($queries['idsecretaria'])) ? (int)$queries['idsecretaria'] : false;
		if ($idsecretaria) array_push($where, " d1.idsecretaria = $idsecretaria ");		

		$nomerazao = (isset($queries["nomerazao"])) ? $queries["nomerazao"] : false;
		if ($nomerazao) array_push($where, " d1.nomerazao LIKE '%$nomerazao%' ");

$sobrenomefantasia = (isset($queries["sobrenomefantasia"])) ? $queries["sobrenomefantasia"] : false;
		if ($sobrenomefantasia) array_push($where, " d1.sobrenomefantasia LIKE '%$sobrenomefantasia%' ");

$cpfcnpj = (isset($queries["cpfcnpj"])) ? $queries["cpfcnpj"] : false;
		if ($cpfcnpj) array_push($where, " d1.cpfcnpj LIKE '%$cpfcnpj%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " d1.status LIKE '%$status%' ");

		$ids = (isset($queries['ids'])) ? $queries['ids'] : false;
		if ($ids) array_push($where, " FIND_IN_SET( d1.id, '$ids' ) ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		
		$ordem = "ORDER BY d1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM diplomasalunos d1
					
					WHERE d1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDiplomaalunoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDiplomasalunos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDiplomaalunoByIdHelper($id, $queries = array()) {
		$rows = new Diplomasalunos();
		return $rows->getDiplomaalunoById($id, $queries);
	}		
	
	public static function getNomesDiplomaalunoByIdsHelper($ids, $queries = array()) {
		$queries['ids'] = $ids;
		$diplomas = new Diplomasalunos();
		$rows = $diplomas->getDiplomasalunos($queries);
		
		$nomes = '';
		   
		if(sizeof($rows) > 0) :
			foreach ($rows as $i=>$row) : 
				$nomes .= $row['nomerazao'] . ' ' . $row['sobrenomefantasia'] . '<br />';
			endforeach;
		endif;
		
		return $nomes ;
	}
	
	public static function getNomeDiplomaalunoByIdHelper($id, $queries = array()) {
		$rows = new Diplomasalunos();
		$row = $rows->getDiplomaalunoById($id, $queries);
		
		$nomes = '';
		
		if($row):
			$nomes .= $row['nomerazao'] . ' ' . $row['sobrenomefantasia'];
		endif;
		
		return $nomes ;
	}	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Diplomasalunos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? (int)$dados["idsecretaria"] : (int)$row->idsecretaria;
 $row->nomerazao = (array_key_exists("nomerazao",$dados)) ? $dados["nomerazao"] : $row->nomerazao;
 $row->sobrenomefantasia = (array_key_exists("sobrenomefantasia",$dados)) ? $dados["sobrenomefantasia"] : $row->sobrenomefantasia;
 $row->cpfcnpj = (array_key_exists("cpfcnpj",$dados)) ? $dados["cpfcnpj"] : $row->cpfcnpj;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}