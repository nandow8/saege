<?php

/**
 * Define o modelo Coordenacaocalendariostipos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Coordenacaocalendariostipos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "coordenacaocalendariostipos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCoordenacaocalendariostiposHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$coordenacaocalendariostipos = new Coordenacaocalendariostipos();
		return $coordenacaocalendariostipos->getCoordenacaocalendariostipos($queries, $page, $maxpage);
	}
	
	public function getCoordenacaocalendariostipos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " c1.idescola = $idescola ");

$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " c1.tipo LIKE '%$tipo%' ");

$cor = (isset($queries["cor"])) ? $queries["cor"] : false;
		if ($cor) array_push($where, " c1.cor LIKE '%$cor%' ");

$sigla = (isset($queries["sigla"])) ? $queries["sigla"] : false;
		if ($sigla) array_push($where, " c1.sigla LIKE '%$sigla%' ");

$dialetivo = (isset($queries["dialetivo"])) ? $queries["dialetivo"] : false;
		if ($dialetivo) array_push($where, " c1.dialetivo LIKE '%$dialetivo%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " c1.origem LIKE '%$origem%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM coordenacaocalendariostipos c1
					
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if($maxpage) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getCoordenacaocalendariotipoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCoordenacaocalendariostipos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCoordenacaocalendariotipoByIdHelper($id, $queries = array()) {
		$rows = new Coordenacaocalendariostipos();
		return $rows->getCoordenacaocalendariotipoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Coordenacaocalendariostipos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->cor = (array_key_exists("cor",$dados)) ? $dados["cor"] : $row->cor;
 $row->sigla = (array_key_exists("sigla",$dados)) ? $dados["sigla"] : $row->sigla;
 $row->dialetivo = (array_key_exists("dialetivo",$dados)) ? $dados["dialetivo"] : $row->dialetivo;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}