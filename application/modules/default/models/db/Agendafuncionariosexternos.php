<?php

/**
 * Define o modelo Agendafuncionariosexternos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Agendafuncionariosexternos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "agendafuncionariosexternos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAgendafuncionariosexternosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$agendafuncionariosexternos = new Agendafuncionariosexternos();
		return $agendafuncionariosexternos->getAgendafuncionariosexternos($queries, $page, $maxpage);
	}
	
	public function getAgendafuncionariosexternos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
		
		
		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " a1.iddepartamento = $iddepartamento ");

$nomerazao = (isset($queries["nomerazao"])) ? $queries["nomerazao"] : false;
		if ($nomerazao) array_push($where, " a1.nomerazao LIKE '%$nomerazao%' ");

$sobrenomefantasia = (isset($queries["sobrenomefantasia"])) ? $queries["sobrenomefantasia"] : false;
		if ($sobrenomefantasia) array_push($where, " a1.sobrenomefantasia LIKE '%$sobrenomefantasia%' ");

$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
		if ($telefone) array_push($where, " a1.telefone LIKE '%$telefone%' ");

$funcionario = (isset($queries["funcionario"])) ? $queries["funcionario"] : false;
		if ($funcionario) array_push($where, " a1.funcionario LIKE '%$funcionario%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM agendafuncionariosexternos a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAgendafuncionarioexternoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAgendafuncionariosexternos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAgendafuncionarioexternoByIdHelper($id, $queries = array()) {
		$rows = new Agendafuncionariosexternos();
		return $rows->getAgendafuncionarioexternoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Agendafuncionariosexternos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->nomerazao = (array_key_exists("nomerazao",$dados)) ? $dados["nomerazao"] : $row->nomerazao;
 $row->sobrenomefantasia = (array_key_exists("sobrenomefantasia",$dados)) ? $dados["sobrenomefantasia"] : $row->sobrenomefantasia;
 $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
 $row->celular = (array_key_exists("celular",$dados)) ? $dados["celular"] : $row->celular;
 $row->funcionario = (array_key_exists("funcionario",$dados)) ? $dados["funcionario"] : $row->funcionario;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}