<?php

/**
 * Define o modelo Almoxarifadoabcsbalancetes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Almoxarifadoabcsbalancetes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "almoxarifadoabcsbalancetes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getAlmoxarifadoabcsbalancetesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$almoxarifadoabcsbalancetes = new Almoxarifadoabcsbalancetes();
		return $almoxarifadoabcsbalancetes->getAlmoxarifadoabcsbalancetes($queries, $page, $maxpage);
	}
	
	public function getAlmoxarifadoabcsbalancetes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " a1.id = $id ");
		
		
		$tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
		if ($tipo) array_push($where, " a1.tipo LIKE '%$tipo%' ");

$dia_i_i = (isset($queries["dia_i_i"])) ? $queries["dia_i_i"] : false;
		if ($dia_i_i) array_push($where, " a1.dia_i >= '$dia_i_i' ");

$dia_i_f = (isset($queries["dia_i_f"])) ? $queries["dia_i_f"] : false;
		if ($dia_i_f) array_push($where, " a1.dia_i <= '$dia_i_f' ");

$dia_f_i = (isset($queries["dia_f_i"])) ? $queries["dia_f_i"] : false;
		if ($dia_f_i) array_push($where, " a1.dia_f >= '$dia_f_i' ");

$dia_f_f = (isset($queries["dia_f_f"])) ? $queries["dia_f_f"] : false;
		if ($dia_f_f) array_push($where, " a1.dia_f <= '$dia_f_f' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " a1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "a1.*"; 
		;
		
		if ($total) $fields = "COUNT(a1.id) as total";
		
		$ordem = "ORDER BY a1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM almoxarifadoabcsbalancetes a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getAlmoxarifadoabcbalanceteById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getAlmoxarifadoabcsbalancetes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getAlmoxarifadoabcbalanceteByIdHelper($id, $queries = array()) {
		$rows = new Almoxarifadoabcsbalancetes();
		return $rows->getAlmoxarifadoabcbalanceteById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadoabcsbalancetes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->classificacao = (array_key_exists("classificacao",$dados)) ? $dados["classificacao"] : $row->classificacao;
 $row->dia_i = (array_key_exists("dia_i",$dados)) ? $dados["dia_i"] : $row->dia_i;
 $row->dia_f = (array_key_exists("dia_f",$dados)) ? $dados["dia_f"] : $row->dia_f;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		
				
		$row->save();
		
		return $row;
	}
	
}