<?php

/**
 * Define o modelo Cadastrolivros
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Cadastrolivros extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "cadastrolivros";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCadastrolivrosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$cadastrolivros = new Cadastrolivros();
		return $cadastrolivros->getCadastrolivros($queries, $page, $maxpage);
	}
	
	public function getCadastrolivros($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
		$primeiroautor = (isset($queries["primeiroautor"])) ? $queries["primeiroautor"] : false;
		if ($primeiroautor) array_push($where, " c1.primeiroautor LIKE '%$primeiroautor%' ");

$segundoautor = (isset($queries["segundoautor"])) ? $queries["segundoautor"] : false;
		if ($segundoautor) array_push($where, " c1.segundoautor LIKE '%$segundoautor%' ");

$terceiroautor = (isset($queries["terceiroautor"])) ? $queries["terceiroautor"] : false;
		if ($terceiroautor) array_push($where, " c1.terceiroautor LIKE '%$terceiroautor%' ");

$local = (isset($queries["local"])) ? $queries["local"] : false;
		if ($local) array_push($where, " c1.local LIKE '%$local%' ");

$edicao = (isset($queries["edicao"])) ? $queries["edicao"] : false;
		if ($edicao) array_push($where, " c1.edicao LIKE '%$edicao%' ");

$volumes = (isset($queries["volumes"])) ? $queries["volumes"] : false;
		if ($volumes) array_push($where, " c1.volumes LIKE '%$volumes%' ");

$paginas = (isset($queries["paginas"])) ? $queries["paginas"] : false;
		if ($paginas) array_push($where, " c1.paginas = '$paginas' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " c1.titulo LIKE '%$titulo%' ");

$subtitulo = (isset($queries["subtitulo"])) ? $queries["subtitulo"] : false;
		if ($subtitulo) array_push($where, " c1.subtitulo LIKE '%$subtitulo%' ");

$editora = (isset($queries["editora"])) ? $queries["editora"] : false;
		if ($editora) array_push($where, " c1.editora LIKE '%$editora%' ");

$ano = (isset($queries["ano"])) ? $queries["ano"] : false;
		if ($ano) array_push($where, " c1.ano = '$ano' ");

$serie = (isset($queries["serie"])) ? $queries["serie"] : false;
		if ($serie) array_push($where, " c1.serie LIKE '%$serie%' ");

$isbn = (isset($queries["isbn"])) ? $queries["isbn"] : false;
		if ($isbn) array_push($where, " c1.isbn LIKE '%$isbn%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM cadastrolivros c1
					
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getCadastrolivroById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCadastrolivros($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCadastrolivroByIdHelper($id, $queries = array()) {
		$rows = new Cadastrolivros();
		return $rows->getCadastrolivroById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Cadastrolivros
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->primeiroautor = (array_key_exists("primeiroautor",$dados)) ? $dados["primeiroautor"] : $row->primeiroautor;
 $row->segundoautor = (array_key_exists("segundoautor",$dados)) ? $dados["segundoautor"] : $row->segundoautor;
 $row->terceiroautor = (array_key_exists("terceiroautor",$dados)) ? $dados["terceiroautor"] : $row->terceiroautor;
 $row->local = (array_key_exists("local",$dados)) ? $dados["local"] : $row->local;
 $row->edicao = (array_key_exists("edicao",$dados)) ? $dados["edicao"] : $row->edicao;
 $row->volumes = (array_key_exists("volumes",$dados)) ? $dados["volumes"] : $row->volumes;
 $row->paginas = (array_key_exists("paginas",$dados)) ? $dados["paginas"] : $row->paginas;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->subtitulo = (array_key_exists("subtitulo",$dados)) ? $dados["subtitulo"] : $row->subtitulo;
 $row->editora = (array_key_exists("editora",$dados)) ? $dados["editora"] : $row->editora;
 $row->ano = (array_key_exists("ano",$dados)) ? $dados["ano"] : $row->ano;
 $row->serie = (array_key_exists("serie",$dados)) ? $dados["serie"] : $row->serie;
 $row->isbn = (array_key_exists("isbn",$dados)) ? $dados["isbn"] : $row->isbn;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		//return true;		
		$row->save();
		
		return $row;
	}
	
}