<?php

class Escolas extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "escolas";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";
    public static $_SITUACOES_DA_ESCOLA_ATIVA = 'ativa';
    public static $_SITUACOES_DA_ESCOLA_EXTINTA = 'extinta';
    public static $_SITUACOES_DA_ESCOLA_ATIVIDADES_SUSPENSAS = 'atividades_suspensas';
    public static $_SITUACOES_DA_ESCOLA_NAO_INSTALADA = 'nao_instalada';
    public static $_SITUACOES_DA_ESCOLA_EM_RECESSO = 'em_recesso';
    public static $_SITUACOES_DA_ESCOLA_PARALISACAO_TEMPORARIA = 'paralisacao_temporaria';
    public static $_SITUACOES_DA_ESCOLA_CASSADA = 'cassada';
    public static $_SITUACOES_DA_ESCOLA_DESATIVADA = 'desativada';
    public static $_SITUACOES_DA_ESCOLA_AGUARDANDO_APROVACAO = 'aguardando_aprovacao';
    public static $_SITUACOES_DA_ESCOLA_ARQUIVO_MORTO = 'arquivo_morto';
    public static $_SITUACOES_DA_ESCOLA_PROVISORIA = 'provisoria';

    public static function getSituacoesescolas($field = false) {
        $res = array(
            self::$_SITUACOES_DA_ESCOLA_ATIVA => 'Ativa',
            self::$_SITUACOES_DA_ESCOLA_EXTINTA => 'Extinta',
            self::$_SITUACOES_DA_ESCOLA_ATIVIDADES_SUSPENSAS => 'Atividades Suspensas',
            self::$_SITUACOES_DA_ESCOLA_NAO_INSTALADA => 'Não Instalada',
            self::$_SITUACOES_DA_ESCOLA_EM_RECESSO => 'Em Recesso',
            self::$_SITUACOES_DA_ESCOLA_PARALISACAO_TEMPORARIA => 'Paralisação Temporária',
            self::$_SITUACOES_DA_ESCOLA_CASSADA => 'Cassada',
            self::$_SITUACOES_DA_ESCOLA_DESATIVADA => 'Desativada',
            self::$_SITUACOES_DA_ESCOLA_AGUARDANDO_APROVACAO => 'Aguardando aprovação',
            self::$_SITUACOES_DA_ESCOLA_ARQUIVO_MORTO => 'Arquivo Morto',
            self::$_SITUACOES_DA_ESCOLA_PROVISORIA => 'Provisória'
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static $_TIPO_ZONEAMENTO_RURAL = '1';
    public static $_TIPO_ZONEAMENTO_URBANO = '2';

    public static function getZoneamentos($field = false) {
        $res = array(
            self::$_TIPO_ZONEAMENTO_RURAL => 'Rural',
            self::$_TIPO_ZONEAMENTO_URBANO => 'Urbano'
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static $_REDE_ENSINO_ESTADUAL = '1-E';
    public static $_REDE_ENSINO_MUNICIPAL = '2-M';
    public static $_REDE_ENSINO_PRIVADA = '3-P';
    public static $_REDE_ENSINO_FEDERAL = '4-F';
    public static $_REDE_ENSINO_ESTADUAL_OUTROS = '5-EO';

    public static function getRedesEnsinos($field = false) {
        $res = array(
            self::$_REDE_ENSINO_ESTADUAL => 'Estadual',
            self::$_REDE_ENSINO_MUNICIPAL => 'Municipal',
            self::$_REDE_ENSINO_PRIVADA => 'Privada',
            self::$_REDE_ENSINO_FEDERAL => 'Federal',
            self::$_REDE_ENSINO_ESTADUAL_OUTROS => 'Estadual Outros (Centro Paula Souza)'
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static $_TIPO_IMOVEL_PROPRIO = 'proprio';
    public static $_TIPO_IMOVEL_ALUGADO = 'alugado';
    public static $_TIPO_IMOVEL_CESSAO_USO = 'cessao_uso';

    public static function getTiposImoveis($field = false) {
        $res = array(
            self::$_TIPO_IMOVEL_PROPRIO => 'Próprio',
            self::$_TIPO_IMOVEL_ALUGADO => 'Alugado',
            self::$_TIPO_IMOVEL_CESSAO_USO => 'Cessão de uso'
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static $_MOTIVO_TRANSFERENCIA_MUDANCA_RESIDENCIA = '01';
    public static $_MOTIVO_TRANSFERENCIA_MUDANCA_ESCOLA = '02';
    public static $_MOTIVO_TRANSFERENCIA_MUDANCA_ESTADO = '03';
    public static $_MOTIVO_TRANSFERENCIA_MUDANCA_PAIS = '04';
    public static $_MOTIVO_TRANSFERENCIA_MUDANCA_PARTICULAR = '05';

    public static function getMotivosTransferencias($field = false) {
        $res = array(
            self::$_MOTIVO_TRANSFERENCIA_MUDANCA_RESIDENCIA => 'Mudança de Residência',
            self::$_MOTIVO_TRANSFERENCIA_MUDANCA_ESCOLA => 'Mudança para outra escola particular/pública',
            self::$_MOTIVO_TRANSFERENCIA_MUDANCA_ESTADO => 'Mudança para outro estado',
            self::$_MOTIVO_TRANSFERENCIA_MUDANCA_PAIS => 'Mudança para outro país',
            self::$_MOTIVO_TRANSFERENCIA_MUDANCA_PARTICULAR => 'Mudança para escola de rede particular '
        );

        if (!$field)
            return $res;
        return $res[$field];
    }

    public static function getEscolasByUsuario($idusuario) {
        $idusuario = (int) $idusuario;
        $escolasusuarios = new Escolasusuarios();
        $usuario = $escolasusuarios->fetchRow("id=$idusuario");

        if (!$usuario)
            return array();

        $strsql = "SELECT * FROM escolas WHERE FIND_IN_SET(id, '" . $usuario['idescolas'] . "') AND excluido='nao' ORDER BY escola";
        //die($strsql);
        $db = Zend_Registry::get('db');
        $rows = $db->fetchAll($strsql);

        return $rows;
    }

    public static function getEscolasHelper($queries = array(), $page = 0, $maxpage = 0) {
        $produtos = new Escolas();
        return $produtos->getEscolas($queries, $page, $maxpage);
    }

    public function getEscolas($queries = array(), $page = 0, $maxpage = 0) {
        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        $idendereco = (isset($queries['idendereco'])) ? (int) $queries['idendereco'] : false;
        $idescola = (isset($queries['idescola'])) ? (int) $queries['idescola'] : false;
        $idsecretaria = (isset($queries['idsecretaria'])) ? (int) $queries['idsecretaria'] : false;
        $chave = (isset($queries['chave'])) ? $queries['chave'] : false;
        $status = (isset($queries['status'])) ? $queries['status'] : false;
        $escola = (isset($queries['escola'])) ? $queries['escola'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id)
            array_push($where, " e1.id=$id ");
        if ($idendereco)
            array_push($where, " e1.idendereco=$idendereco ");
        if ($idsecretaria)
            array_push($where, " e1.idsecretaria=$idsecretaria ");
        if ($idescola)
            array_push($where, " e1.id=$idescola ");
        if ($chave)
            array_push($where, " ((e1.escola LIKE '%$chave%')) ");
        if ($status)
            array_push($where, " e1.status='$status' ");
        if ($escola)
            array_push($where, " e1.escola='$escola' ");

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "e1.*";
        if ($total)
            $fields = "COUNT(e1.id) as total";


        $ordem = "ORDER BY e1.id";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields
					FROM escolas e1
					WHERE e1.excluido='nao'
						$w
					$ordem
					$limit";

        //if(!$total) die($strsql);
        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEscolaById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getEscolas($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getEscolaByIdHelper($id, $queries = array()) {
        $rows = new Escolas();
        return $rows->getEscolaById($id, $queries);
    }

    public static function getEscolaNomeByIdHelper($id, $queries = array()) {
        $rows = new Escolas();
        $row = $rows->getEscolaById($id, $queries);
        return $row['escola'];
    }

    public static function getEscolasByCoordenadorHelper($idcoordenador, $queries = array()) {
        $rows = new Escolas();
        return $rows->getEscolasByCoordenador($idcoordenador, $queries);
    }

    public static function getEscolasByCoordenador($idcoordenador, $queries = array()) {
        $fields = 'e1.*';
        $strsql = "SELECT $fields
					FROM escolas e1
					INNER JOIN coordenacaocoordenadores cc1 ON FIND_IN_SET(e1.id, cc1.idsescolas)
					WHERE e1.excluido='nao'
					AND cc1.idfuncionario = $idcoordenador
					AND cc1.excluido = 'nao'
					";

        //die($strsql);

        $db = Zend_Registry::get('db');


        return $db->fetchAll($strsql);
    }

    public static function getEscolaByEscolaNome($escola, $queries = array()) {
        if ((!isset($escola)) || (!$escola) || ($escola == ""))
            return false;
        $_rows = new Escolas();

        $queries['escola'] = $escola;
        $rows = $_rows->getEscolas($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    /**
     * Função: Garantir que a primeira letra seja maiúscula
     * @param str escola - nome da escola
     * @return str - o nome da escola formatado
     */
    public static function upPrimeiraLetra($string) {
        $school = strtolower($string);
        $escola = ucwords($school);

        return $escola;
    }

    /**
     * Diminui o tamanho da palavra
     * @param string word
     * @param int max
     * @return - string palavra com X caracteres
     */
    public static function cutWord($word, $max) {
        if (strlen($word) > $max) {
            $word = substr($word, 0, $max);
        }
        return $word;
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolas
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) {
            $row = $this->createRow();
            $row->datacriacao = date('Y-m-d G:i:s');
        } else {
            $novoRegistro = false;
        }
        $row->idendereco = (array_key_exists('idendereco', $dados)) ? $dados['idendereco'] : $row->idendereco;
        $row->idsecretaria = (array_key_exists('idsecretaria', $dados)) ? $dados['idsecretaria'] : $row->idsecretaria;
        $row->idsensinostipos = (array_key_exists('idsensinostipos', $dados)) ? $dados['idsensinostipos'] : $row->idsensinostipos;
        $row->idsensinossubtipos = (array_key_exists('idsensinossubtipos', $dados)) ? $dados['idsensinossubtipos'] : $row->idsensinossubtipos;
        $row->idsatividadesaee = (array_key_exists('idsatividadesaee', $dados)) ? $dados['idsatividadesaee'] : $row->idsatividadesaee;
        $row->idsmaterias = (array_key_exists('idsmaterias', $dados)) ? $dados['idsmaterias'] : $row->idsmaterias;
        $row->escola = (array_key_exists('escola', $dados)) ? $dados['escola'] : $row->escola;
        $row->telefone = (array_key_exists('telefone', $dados)) ? $dados['telefone'] : $row->telefone;

        $row->diassemana = (array_key_exists('diassemana', $dados)) ? $dados['diassemana'] : $row->diassemana;
        $row->quantidadesaulas = (array_key_exists('quantidadesaulas', $dados)) ? $dados['quantidadesaulas'] : $row->quantidadesaulas;

        $row->tipozoneamento = (array_key_exists('tipozoneamento', $dados)) ? $dados['tipozoneamento'] : $row->tipozoneamento;
        $row->tipoimovel = (array_key_exists('tipoimovel', $dados)) ? $dados['tipoimovel'] : $row->tipoimovel;

        $row->latitude = (isset($dados['latitude'])) ? $dados['latitude'] : $row->latitude;
        $row->longitude = (isset($dados['longitude'])) ? $dados['longitude'] : $row->longitude;

        $row->nomeabreviado = (isset($dados['nomeabreviado'])) ? $dados['nomeabreviado'] : $row->nomeabreviado;
        $row->codsituacao = (isset($dados['codsituacao'])) ? $dados['codsituacao'] : $row->codsituacao;
        $row->codredeensino = (isset($dados['codredeensino'])) ? $dados['codredeensino'] : $row->codredeensino;
        $row->codidentificador = (isset($dados['codidentificador'])) ? $dados['codidentificador'] : $row->codidentificador;

        $row->status = (array_key_exists('status', $dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido', $dados)) ? $dados['excluido'] : $row->excluido;
        $row->logusuario = (array_key_exists('logusuario', $dados)) ? $dados['logusuario'] : $row->logusuario;
        $row->logdata = (array_key_exists('logdata', $dados)) ? $dados['logdata'] : $row->logdata;

        /* Gestor Escolar */
        $row->gestorescolarnome = (array_key_exists('gestorescolarnome', $dados)) ? $dados['gestorescolarnome'] : $row->gestorescolarnome;
        $row->gestorescolarcpf = (array_key_exists('gestorescolarcpf', $dados)) ? $dados['gestorescolarcpf'] : $row->gestorescolarcpf;
        $row->gestorescolarcargo = (array_key_exists('gestorescolarcargo', $dados)) ? $dados['gestorescolarcargo'] : $row->gestorescolarcargo;
        $row->gestorescolaremail = (array_key_exists('gestorescolaremail', $dados)) ? $dados['gestorescolaremail'] : $row->gestorescolaremail;

        /* Dias da Semana */
        $row->segunda = (array_key_exists('segunda', $dados)) ? $dados['segunda'] : $row->segunda;
        $row->terca = (array_key_exists('terca', $dados)) ? $dados['terca'] : $row->terca;
        $row->quarta = (array_key_exists('quarta', $dados)) ? $dados['quarta'] : $row->quarta;
        $row->quinta = (array_key_exists('quinta', $dados)) ? $dados['quinta'] : $row->quinta;
        $row->sexta = (array_key_exists('sexta', $dados)) ? $dados['sexta'] : $row->sexta;
        $row->sabado = (array_key_exists('sabado', $dados)) ? $dados['sabado'] : $row->sabado;
        $row->domingo = (array_key_exists('domingo', $dados)) ? $dados['domingo'] : $row->domingo;

        if ((int) $row->idsecretaria == 0) {
            $row->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        }


        $escolaid = $row->save();

        $enderecos = new Escolasenderecos();
        $count = $enderecos->getEnderecoByIdEscola($escolaid);

        if (!$count) {
            $dados['endereco']['idescola'] = $escolaid;
            $endereco = $enderecos->save($dados['endereco']);
        } else {
            $dados['endereco']['id'] = $count['id'];
            $endereco = $enderecos->save($dados['endereco']);
        }


        $perfis = new Escolasusuariosperfis();
        $perfil = $perfis->getPerfilByIdSecretaria(Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id'), array('origem' => 'SYS'));
        //CRIA UM PERFIL DE PROFESSOR PARA
        if (!$perfil) {
            $dadosperfis = array();
            $dadosperfis['idsecretaria'] = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
            $dadosperfis['idescola'] = 0;
            $dadosperfis['perfil'] = "Professores (GERAL)";
            $dadosperfis['status'] = 'Ativo';
            $dadosperfis['multiplasescolas'] = 'Sim';
            $dadosperfis['origem'] = 'SYS';
            $dadosperfis['excluido'] = 'nao';
            $dadosperfis['logusuario'] = $row->logusuario;
            $dadosperfis['logdata'] = '$row->logdata';
            $perfis->save($dadosperfis);
        }

        if (array_key_exists('tiposesgotos', $dados))
            $this->setItens($dados['tiposesgotos'], $row->id, $row->logusuario, $row->logdata);
        if (array_key_exists('redeagua', $dados))
            $this->setItensRedeagua($dados['redeagua'], $row->id, $row->logusuario, $row->logdata);
        if (array_key_exists('infraestrutura', $dados))
            $this->setItensInfraestrutura($dados['infraestrutura'], $row->id, $row->logusuario, $row->logdata);
        /* if (array_key_exists('terrenos', $dados)) $this->setItensTerrenos($dados['terrenos'], $row->id, $row->logusuario, $row->logdata); */
        if (array_key_exists('perfistecnicos', $dados))
            $this->setItensPerfistecnicos($dados['perfistecnicos'], $row->id, $row->logusuario, $row->logdata);
        if (array_key_exists('esportivos', $dados))
            $this->setItensEsportivos($dados['esportivos'], $row->id, $row->logusuario, $row->logdata);
        if (array_key_exists('instalacoes', $dados))
            $this->setItensInstalacoes($dados['instalacoes'], $row->id, $row->logusuario, $row->logdata);
        /* if (array_key_exists('documentacoes', $dados)) $this->setItensDocumentacoes($dados['documentacoes'], $row->id, $row->logusuario, $row->logdata);
         */
        if (array_key_exists('servicos', $dados))
            $this->setItensServicos($dados['servicos'], $row->id, $row->logusuario, $row->logdata);
        if (array_key_exists('edificacoes', $dados))
            $this->setItensEdificacoes($dados['edificacoes'], $row->id, $row->logusuario, $row->logdata);
        if (array_key_exists('edificacoesinformacoes', $dados))
            $this->setItensEdificacoesInformacoes($dados['edificacoesinformacoes'], $row->id, $row->logusuario, $row->logdata);
        if (array_key_exists('deficiencientesinformacoes', $dados))
            $this->setItensEdificacoesDeficientes($dados['deficiencientesinformacoes'], $row->id, $row->logusuario, $row->logdata);
        /*
          $gdae = new Gdae_Integracoes();
          $gdae->Enviarescolas($row['id']);
         */

        if (array_key_exists('edificacoes', $dados))
            $this->setItensEdificacoes($dados['edificacoes'], $row->id, $row->logusuario, $row->logdata);
        return $row;
    }

    private function setItens($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $tipos = new Estabelecimentos_Escolasabastecimentos();
        $tipo = $tipos->getAbastecimentoByIdEscola($idescola);
        if ($tipo):
            $dados['id'] = $tipo['id'];
        endif;
        $_row = $tipos->save($dados);
    }

    private function setItensRedeagua($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $tipos = new Estabelecimentos_Escolasredes();
        $tipo = $tipos->getRedeByIdEscola($idescola);
        if ($tipo):
            $dados['id'] = $tipo['id'];
        endif;
        $_row = $tipos->save($dados);
    }

    private function setItensInfraestrutura($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $tipos = new Estabelecimentos_Escolasinfraestruturas();
        $tipo = $tipos->getInfraestruturaByIdEscola($idescola);
        if ($tipo):
            $dados['id'] = $tipo['id'];
        endif;
        $_row = $tipos->save($dados);
    }

    private function setItensTerrenos($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $tipos = new Estabelecimentos_Escolasterrenos();
        $tipo = $tipos->getTerrenoByIdEscola($idescola);
        if ($tipo):
            $dados['id'] = $tipo['id'];
        endif;
        $_row = $tipos->save($dados);
    }

    private function setItensPerfistecnicos($dados, $idescola, $logusuario = false, $logdata = false) {
        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $tipos = new Estabelecimentos_Escolasperfistecnicos();
        $tipo = $tipos->getPerfilTecnicoByIdEscola($idescola);
        if ($tipo):
            $dados['id'] = $tipo['id'];
        endif;
        $_row = $tipos->save($dados);
    }

    private function setItensEsportivos($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $tipos = new Estabelecimentos_Escolasestruturasesportivas();
        $tipo = $tipos->getEstruturaesportivaByIdEscola($idescola);
        if ($tipo):
            $dados['id'] = $tipo['id'];
        endif;
        $_row = $tipos->save($dados);
    }

    private function setItensInstalacoes($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $tipos = new Estabelecimentos_Escolasinstalacoes();
        $tipo = $tipos->getInstalacaoByIdEscola($idescola);
        if ($tipo):
            $dados['id'] = $tipo['id'];
        endif;
        $_row = $tipos->save($dados);
    }

    private function setItensDocumentacoes($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $tipos = new Estabelecimentos_Escolasdocumentacoes();
        $tipo = $tipos->getDocumentacaoByIdEscola($idescola);
        if ($tipo):
            $dados['id'] = $tipo['id'];
        endif;
        $_row = $tipos->save($dados);
    }

    private function setItensServicos($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $tipos = new Estabelecimentos_Escolasservicos();
        $tipo = $tipos->getServicoByIdEscola($idescola);
        if ($tipo):
            $dados['id'] = $tipo['id'];
        endif;
        $_row = $tipos->save($dados);
    }

    private function setItensEdificacoes($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $edificacoes = new Edificacoes_Edificacoes();
        $edificacao = $edificacoes->getEdificacaoByIdEscola($idescola);

        if ($edificacao):
            $dados['id'] = $edificacao['id'];
        endif;

        $row = $edificacoes->save($dados);


        $multiplosarquivos = new Estabelecimentos_Escolasarquivos();
        $multiplosarquivos->setArquivos($row['id'], $dados['idsarquivos'], $dados['legendasarquivos']);
    }

    private function setItensEdificacoesInformacoes($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $informacoes = new Edificacoes_Informacoestecnicas();
        $informacao = $informacoes->getInformacaoByIdEscola($idescola);

        if ($informacao):
            $dados['id'] = $informacao['id'];
        endif;

        $_row = $informacoes->save($dados);
    }

    private function setItensEdificacoesDeficientes($dados, $idescola, $logusuario = false, $logdata = false) {

        if (!is_array($dados))
            return;
        $dados['idescola'] = $idescola;
        $infraestruturasdeficientes = new Edificacoes_Infraestruturadeficiencia();
        $infraestrutura = $infraestruturasdeficientes->getInfraestruturaByIdEscola($idescola);

        if ($infraestrutura):
            $dados['id'] = $infraestrutura['id'];
        endif;

        $_row = $infraestruturasdeficientes->save($dados);
    }

    public function getEscolasByEnsino($idensino) {

        $sqlQuery = "select * from escolas where  idsensinossubtipos LIKE '%$idensino%' ";

        $db = Zend_Registry::get('db');

        return $db->fetchAll($sqlQuery);
    }

//end function getEscolasByEnsino

    public static function getEscolasRH() {

        $query = 'select';
        $query .= ' e.id as idescola,';
        $query .= ' e.escola as escola,';
        $query .= ' f.id as idfuncionario,';
        $query .= ' f.idescola as funcescola,';
        $query .= ' f.idsfuncionarios as funcionarios,';
        $query .= ' f.idperfil as perfil,';
        $query .= ' f.idlocal as local,';
        $query .= ' u.idperfil as usuarioperfil,';
        $query .= ' u.id as idusuario';
        $query .= ' from';
        $query .= ' escolas as e';
        $query .= ' inner join';
        $query .= ' funcionariosgeraisescolas as f';
        $query .= ' on e.id = f.idescola';
        $query .= ' inner join';
        $query .= ' usuarios as u';
        $query .= ' on f.id = u.idfuncionario';
        $query .= ' group by e.id';
        $query .= ' order by e.escola';

        $query = 'select * from escolas';

        $query = 'select ';
        $query .= 'u.id as idusuario, ';
        $query .= 'u.idperfil, ';
        $query .= 'u.iddepartamento, ';
        $query .= 'u.rgf, ';
        $query .= 'u.senha, ';
        $query .= 'f.id as idfuncionario, ';
        $query .= 'f.idescola, ';
        $query .= 'f.idlocal, ';
        $query .= 'e.id, ';
        $query .= 'e.escola, ';
        $query .= 'p.visualizar, ';
        $query .= 'p.adicionar, ';
        $query .= 'p.editar, ';
        $query .= 'p.excluir ';
        $query .= 'from ';
        $query .= 'usuarios as u ';
        $query .= 'inner join ';
        $query .= 'funcionariosgeraisescolas as f on u.idfuncionario = f.id ';
        $query .= 'inner join ';
        $query .= 'escolas as e on e.id = f.idescola ';
        $query .= 'inner join ';
        $query .= 'usuariosperfislimites as p on p.idperfil = u.idperfil ';
        $query .= 'where ';
        $query .= 'p.idlimite = "188" ';
        $query .= 'and p.visualizar = "sim" ';
        $query .= 'and p.editar = "sim" ';
        $query .= 'and p.excluir = "sim" ';
        $query .= 'and p.adicionar = "sim" ';
        $query .= 'order by e.escola';

        $db = Zend_Registry::get('db');

        return $db->fetchAll($query);
    }

//end functino getEscolasRH

    public static function getDepartamentosRH() {

        $query = 'select ';
        $query .= 'u.id as id, ';
        $query .= 'f.id as idfuncionario, ';
        $query .= 'u.nome as nome, ';
        $query .= 'f.rgf as rgf, ';
        $query .= 'u.senha as senha, ';
        $query .= 'f.idlocal as local, ';
        $query .= 'u.idperfil as perfil, ';
        $query .= 'p.perfil as nomeperfil ';
        $query .= 'from ';
        $query .= 'usuarios as u ';
        $query .= 'inner join ';
        $query .= 'funcionariosgeraisescolas as f ';
        $query .= 'on u.idfuncionario = f.id ';
        $query .= 'inner join ';
        $query .= 'usuariosperfis as p ';
        $query .= 'on p.id = u.idperfil ';
        $query .= 'inner join ';
        $query .= 'usuariosperfislimites as up on up.idperfil = u.idperfil ';
        $query .= 'where ';
        $query .= 'f.idescola is null ';
        $query .= 'and up.idlimite = "188" ';
        $query .= 'and up.visualizar = "sim" ';
        $query .= 'and up.editar = "sim" ';
        $query .= 'and up.excluir = "sim" ';
        $query .= 'and up.adicionar = "sim" ';
        $query .= 'order by ';
        $query .= 'up.idperfil ';

        $db = Zend_Registry::get('db');

        return $db->fetchAll($query);
    }

//end public function
}
