<?php

class Ouvidoriaimagens extends Zend_Db_Table_Abstract {

	/**
	 * Define o nome da tabela
	 * @var string
	 */
	protected $_name = "ouvidoriaimagens";
	
	/**
	 * Define a chave primaria
	 * @var integer
	 */
	protected $_primary = "id";
	
	
	public static function getImagensByIdPost($idouvidoria, $order = 'oi1.ordem') {
		
		$strsql = "SELECT oi1.idimagem, oi1.idouvidoria, oi1.legenda, i1.* 
					FROM ouvidoriaimagens oi1 
						LEFT JOIN imagens i1 ON i1.id=oi1.idimagem
					WHERE oi1.excluido='nao' 
						AND oi1.idouvidoria=$idouvidoria 
					ORDER BY $order"; 
						
					
		$db = Zend_Registry::get('db');
		return $db->fetchAll($strsql);
	}
	
	public function setImagens($idouvidoria, $imagens, $legendas) {
		//if (!is_array($imagens)) return;
		
		$logdata = date('Y-m-d G:i:s');
		foreach ($imagens as $i=>$idimagem) {
			$d = array();
			
			$legenda = $legendas[$i];
			
			$d['idouvidoria'] = $idouvidoria;
			$d['idimagem'] = $idimagem;
			$d['legenda'] = $legenda;
			$d['ordem'] = $i;
			$d['excluido'] = 'nao';
			$d['logusuario'] = Usuarios::getUsuario('id');
			$d['logdata'] = $logdata;
			$this->save($d);
		}
		
		$ids = implode(",", $imagens);
		if ($ids=="") $ids = "0";
		$strsql = "DELETE FROM ouvidoriaimagens WHERE idouvidoria=$idouvidoria AND idimagem NOT IN ($ids)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);		
	}	

	public static function getOuvidoriaImagens($idouvidoria = NULL, $status = NULL, $inverte = false, $limite = NULL, $idsimagens = NULL) {
		$db = Zend_Registry::get('db');
		
		$w = "";
		if (!is_null($idouvidoria)) $w .= " AND oi1.idouvidoria = " . (int)$idouvidoria;
		if (!is_null($status)) $w .= " AND oi1.status = '$status'"; 
		if (!is_null($idsimagens)) $w .= " AND FIND_IN_SET(oi1.idimagem, '" . $idsimagens . "')";
		
		$l = "";
		if (!is_null($limite)) $l .= " LIMIT '$limite'";
		
		$order = "ORDER BY oi1.ordem";
		if ($inverte) $order .= " DESC";
		
		$strsql = "SELECT oi1.* 
					FROM ouvidoriaimagens oi1 
					WHERE oi1.excluido = 'nao' 
					$w
					$order
					$l";
					
		//die($strsql);					
		return $db->fetchAll($strsql);
	}
	
	/**
	 * Salva o dados (INSERT OU UPDATE)
	 * @param array dados
	 * @return PropostasFacs
	 */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$idouvidoria = (int)$dados['idouvidoria'];
		$idimagem = (int)$dados['idimagem'];
		
		$row = $this->fetchRow("idouvidoria=" . $idouvidoria . " AND idimagem=" . $idimagem);		
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
				
		if ($id>0)$row->id = $id;
		
		$row->idouvidoria = (isset($dados['idouvidoria'])) ? $dados['idouvidoria'] : $row->idouvidoria;
		$row->idimagem = (isset($dados['idimagem'])) ? $dados['idimagem'] : $row->idimagem;
		$row->legenda = (isset($dados['legenda'])) ? $dados['legenda'] : $row->legenda;
		$row->ordem = (isset($dados['ordem'])) ? $dados['ordem'] : $row->ordem;
		$row->excluido = (isset($dados['excluido'])) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (isset($dados['logusuario'])) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (isset($dados['logdata'])) ? $dados['logdata'] : $row->logdata;
		$row->save();
		
		return $row;
	}
	
}