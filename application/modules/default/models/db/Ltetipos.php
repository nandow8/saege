<?php

class Ltetipos extends Zend_Db_Table_Abstract {
    
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "ltetipos";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    // protected $_primary = "";

    public function gettipos($queries = array()) {
        $tipo = (isset($queries['tipo'])) ? $queries['tipo'] : false;

        $order  = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lt.*";

        $ordem = "ORDER BY tipo";
        if ($order) $ordem = $order;

        $strsql = "SELECT $fields FROM ltetipos lt WHERE lt.excluido = 'nao' $w $ordem";

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        return $db->fetchAll($strsql); 
    }
}