<?php

/**
 * Define o modelo Coordenacaofrotasescolas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Coordenacaofrotasescolas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "coordenacaofrotasescolas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCoordenacaofrotasescolasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$coordenacaofrotasescolas = new Coordenacaofrotasescolas();
		return $coordenacaofrotasescolas->getCoordenacaofrotasescolas($queries, $page, $maxpage);
	}
	
	public function getCoordenacaofrotasescolas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");		
		
		$idfrota = (isset($queries["idfrota"])) ? $queries["idfrota"] : false;
		if ($idfrota) array_push($where, " c1.idfrota = $idfrota ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " c1.idescola = $idescola ");

		$descricao = (isset($queries["descricao"])) ? $queries["descricao"] : false;
		if ($descricao) array_push($where, " c1.descricao LIKE '%$descricao%' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");


		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='idfrota') $sorting[0]='c1.idfrota';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM coordenacaofrotasescolas c1
					
					WHERE c1.excluido='nao' 
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getCoordenacaomaterialdidaticoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCoordenacaofrotasescolas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCoordenacaomaterialdidaticoByIdHelper($id, $queries = array()) {
		$rows = new Coordenacaofrotasescolas();
		return $rows->getCoordenacaomaterialdidaticoById($id, $queries);
	}		
	
	
	public static function getCoordenacaomaterialdidaticoByUltimoHelper() {
		$queries = array();	
		$queries['order'] = "ORDER BY c1.id DESC";
		$rows = new Coordenacaofrotasescolas();
		$_row = $rows->getCoordenacaofrotasescolas($queries);
		if(isset($_row[0]['id'])) return ($_row[0]['id']+1) ;
		
		return '1';
	}	

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Agendafuncionariosinternos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
		 
								
		 $row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		 $row->idfrota = (array_key_exists("idfrota",$dados)) ? $dados["idfrota"] : $row->idfrota;

		 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
						
		$row->save();
		
		return $row;
	}
	
}