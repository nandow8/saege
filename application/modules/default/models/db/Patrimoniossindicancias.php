<?php

/**
 * Define o modelo Patrimoniossindicancias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Patrimoniossindicancias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "patrimoniossindicancias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public function getUltimoPatrimoniobaixabens($queries = array()) {
		$queries['order'] = 'ORDER BY p1.id DESC';
		$rows = $this->getPatrimoniossindicancias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	
	public static function getPatrimoniossindicanciasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$patrimoniossindicancias = new Patrimoniossindicancias();
		return $patrimoniossindicancias->getPatrimoniossindicancias($queries, $page, $maxpage);
	}
	
	public function getPatrimoniossindicancias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		
		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " p1.sequencial LIKE '%$sequencial%' ");

$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " p1.datalancamento >= '$datalancamento_i' ");

$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " p1.datalancamento <= '$datalancamento_f' ");

$horalancamento = (isset($queries["horalancamento"])) ? $queries["horalancamento"] : false;
		if ($horalancamento) array_push($where, " p1.horalancamento = '$horalancamento' ");

$idusuariologado = (isset($queries["idusuariologado"])) ? $queries["idusuariologado"] : false;
		if ($idusuariologado) array_push($where, " p1.idusuariologado = $idusuariologado ");

$idpatrimonio = (isset($queries["idpatrimonio"])) ? $queries["idpatrimonio"] : false;
		if ($idpatrimonio) array_push($where, " p1.idpatrimonio = $idpatrimonio ");

$ultimalocalizacao = (isset($queries["ultimalocalizacao"])) ? $queries["ultimalocalizacao"] : false;
		if ($ultimalocalizacao) array_push($where, " p1.ultimalocalizacao = $ultimalocalizacao ");

$idresponsavel = (isset($queries["idresponsavel"])) ? $queries["idresponsavel"] : false;
		if ($idresponsavel) array_push($where, " p1.idresponsavel = $idresponsavel ");

$motivosindicancia = (isset($queries["motivosindicancia"])) ? $queries["motivosindicancia"] : false;
		if ($motivosindicancia) array_push($where, " p1.motivosindicancia LIKE '%$motivosindicancia%' ");

$numeoprocesso = (isset($queries["numeoprocesso"])) ? $queries["numeoprocesso"] : false;
		if ($numeoprocesso) array_push($where, " p1.numeoprocesso LIKE '%$numeoprocesso%' ");

$descricoesprocessos = (isset($queries["descricoesprocessos"])) ? $queries["descricoesprocessos"] : false;
		if ($descricoesprocessos) array_push($where, " p1.descricoesprocessos = '$descricoesprocessos' ");

$depoimentosindicante = (isset($queries["depoimentosindicante"])) ? $queries["depoimentosindicante"] : false;
		if ($depoimentosindicante) array_push($where, " p1.depoimentosindicante = '$depoimentosindicante' ");

$parecersindicancia = (isset($queries["parecersindicancia"])) ? $queries["parecersindicancia"] : false;
		if ($parecersindicancia) array_push($where, " p1.parecersindicancia LIKE '%$parecersindicancia%' ");

$processodisciplinar = (isset($queries["processodisciplinar"])) ? $queries["processodisciplinar"] : false;
		if ($processodisciplinar) array_push($where, " p1.processodisciplinar LIKE '%$processodisciplinar%' ");

$txtparecersindicancia = (isset($queries["txtparecersindicancia"])) ? $queries["txtparecersindicancia"] : false;
		if ($txtparecersindicancia) array_push($where, " p1.txtparecersindicancia = '$txtparecersindicancia' ");

$observacoes = (isset($queries["observacoes"])) ? $queries["observacoes"] : false;
		if ($observacoes) array_push($where, " p1.observacoes = '$observacoes' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM patrimoniossindicancias p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPatrimoniossindicanciaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPatrimoniossindicancias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPatrimoniossindicanciaByIdHelper($id, $queries = array()) {
		$rows = new Patrimoniossindicancias();
		return $rows->getPatrimoniossindicanciaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Patrimoniossindicancias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
 $row->horalancamento = (array_key_exists("horalancamento",$dados)) ? $dados["horalancamento"] : $row->horalancamento;
 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
 $row->idpatrimonio = (array_key_exists("idpatrimonio",$dados)) ? $dados["idpatrimonio"] : $row->idpatrimonio;
 $row->ultimalocalizacao = (array_key_exists("ultimalocalizacao",$dados)) ? $dados["ultimalocalizacao"] : $row->ultimalocalizacao;
 $row->idresponsavel = (array_key_exists("idresponsavel",$dados)) ? $dados["idresponsavel"] : $row->idresponsavel;
 $row->motivosindicancia = (array_key_exists("motivosindicancia",$dados)) ? $dados["motivosindicancia"] : $row->motivosindicancia;
 $row->numeoprocesso = (array_key_exists("numeoprocesso",$dados)) ? $dados["numeoprocesso"] : $row->numeoprocesso;
 $row->descricoesprocessos = (array_key_exists("descricoesprocessos",$dados)) ? $dados["descricoesprocessos"] : $row->descricoesprocessos;
 $row->depoimentosindicante = (array_key_exists("depoimentosindicante",$dados)) ? $dados["depoimentosindicante"] : $row->depoimentosindicante;
 $row->parecersindicancia = (array_key_exists("parecersindicancia",$dados)) ? $dados["parecersindicancia"] : $row->parecersindicancia;
 $row->processodisciplinar = (array_key_exists("processodisciplinar",$dados)) ? $dados["processodisciplinar"] : $row->processodisciplinar;
 $row->txtparecersindicancia = (array_key_exists("txtparecersindicancia",$dados)) ? $dados["txtparecersindicancia"] : $row->txtparecersindicancia;
 $row->observacoes = (array_key_exists("observacoes",$dados)) ? $dados["observacoes"] : $row->observacoes;
 $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		return true;		
		$row->save();
		
		return $row;
	}
	
}