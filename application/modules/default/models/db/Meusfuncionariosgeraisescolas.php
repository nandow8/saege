<?php

/**
 * Define o modelo Meusfuncionariosgeraisescolas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Meusfuncionariosgeraisescolas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "meusfuncionariosgeraisescolas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getMeusfuncionariosgeraisescolasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		return $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas($queries, $page, $maxpage);
	}
	
	public function getMeusfuncionariosgeraisescolas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " m1.id = $id ");
		
		
		$idfuncionarioemissor = (isset($queries["idfuncionarioemissor"])) ? $queries["idfuncionarioemissor"] : false;
		if ($idfuncionarioemissor) array_push($where, " m1.idfuncionarioemissor = $idfuncionarioemissor ");

		$diferentidfuncionarioemissor = (isset($queries["diferentidfuncionarioemissor"])) ? $queries["diferentidfuncionarioemissor"] : false;
		if ($diferentidfuncionarioemissor) array_push($where, " m1.idfuncionarioadicionado <> $diferentidfuncionarioemissor ");

$idfuncionarioadicionado = (isset($queries["idfuncionarioadicionado"])) ? $queries["idfuncionarioadicionado"] : false;
		if ($idfuncionarioadicionado) array_push($where, " m1.idfuncionarioadicionado = $idfuncionarioadicionado ");

$idescolaemissor = (isset($queries["idescolaemissor"])) ? $queries["idescolaemissor"] : false;
		if ($idescolaemissor) array_push($where, " m1.idescolaemissor = $idescolaemissor ");

$idescolaadicionado = (isset($queries["idescolaadicionado"])) ? $queries["idescolaadicionado"] : false;
		if ($idescolaadicionado) array_push($where, " m1.idescolaadicionado = $idescolaadicionado ");

$idescolaselecionado = (isset($queries["idescolaselecionado"])) ? $queries["idescolaselecionado"] : false;
		if ($idescolaselecionado) array_push($where, " m1.idescolaselecionado = $idescolaselecionado ");

		$diferenteidescola = (isset($queries["diferenteidescola"])) ? $queries["diferenteidescola"] : false;
		if ($diferenteidescola) array_push($where, " m1.idescolaselecionado <> $diferenteidescola ");





$idperfilemissor = (isset($queries["idperfilemissor"])) ? $queries["idperfilemissor"] : false;
		if ($idperfilemissor) array_push($where, " m1.idperfilemissor = $idperfilemissor ");

$idperfiladicionado = (isset($queries["idperfiladicionado"])) ? $queries["idperfiladicionado"] : false;
		if ($idperfiladicionado) array_push($where, " m1.idperfiladicionado = $idperfiladicionado ");

$idperfilselecionado = (isset($queries["idperfilselecionado"])) ? $queries["idperfilselecionado"] : false;
		if ($idperfilselecionado) array_push($where, " m1.idperfilselecionado = $idperfilselecionado ");

		$diferenteidperfil = (isset($queries["diferenteidperfil"])) ? $queries["diferenteidperfil"] : false;
		if ($diferenteidperfil) array_push($where, " m1.idperfilselecionado <> $diferenteidperfil ");




$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " m1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "m1.*"; 
		;
		
		if ($total) $fields = "COUNT(m1.id) as total";
		
		$ordem = "ORDER BY m1.id DESC";
		if ($order) $ordem = $order;
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM meusfuncionariosgeraisescolas m1
					WHERE m1.excluido='nao'
					$w
					$ordem
					$limit";

		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}
		return $db->fetchAll($strsql);
	}
	
	public function getMeusfuncionariosgeraisescolaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getMeusfuncionariosgeraisescolas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getMeusfuncionariosgeraisescolaByIdHelper($id, $queries = array()) {
		$rows = new Meusfuncionariosgeraisescolas();
		return $rows->getMeusfuncionariosgeraisescolaById($id, $queries);
	}	


	public static function getMeusfuncionariosgeraisescolaImplodeByIdescolaHelper($idfuncionario, $idescola, $queries = array()) {
		//$queries['idfuncionarioemissor'] = $idfuncionario;
		//$queries['idescolaselecionado'] = $idescola;
		$queries['idescolaselecionado'] = $idescola;
		// $queries['order'] = "GROUP BY m1.idfuncionarioadicionado"; // Retirado Pois estava dando problemas

		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		$rows = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas($queries);

		$idsfuncionarios = "";
		$_idsfuncionarios = array();
		$funcionariopertence = false;
		foreach ($rows as $k => $v) {
			if(($v['idfuncionarioadicionado']==$idfuncionario) || ($v['idfuncionarioemissor']==$idfuncionario)){
				$funcionariopertence = true;
			}
			array_push($_idsfuncionarios, $v['idfuncionarioadicionado']);
		}

		$_idsfuncionarios = implode(',', $_idsfuncionarios);

		if((isset($_idsfuncionarios)) && ($_idsfuncionarios) && ($_idsfuncionarios!="")){
			$idsfuncionarios = $_idsfuncionarios;
		}

		if(!$funcionariopertence){
			$idsfuncionarios = false;
		}
		
		return $idsfuncionarios;
	}


	public static function getMeusfuncionariosgeraisescolaImplodeByIdescolaGrupoHelper($idfuncionario, $idescola, $queries = array()) {
		$queries['idescolaselecionado'] = $idescola;
		// $queries['order'] = "GROUP BY m1.idfuncionarioadicionado"; // Retirado pois estava dando problemas

		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();

		
		$rows = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas($queries);

		$idsfuncionarios = "";
		$_idsfuncionarios = array();
		$funcionariopertence = false;
		foreach ($rows as $k => $v) {
			if($v['idfuncionarioadicionado']==$idfuncionario){
				$funcionariopertence = true;
			}
			array_push($_idsfuncionarios, $v['idfuncionarioadicionado']);
		}

		$_idsfuncionarios = implode(',', $_idsfuncionarios);

		if((isset($_idsfuncionarios)) && ($_idsfuncionarios) && ($_idsfuncionarios!="")){
			$idsfuncionarios = $_idsfuncionarios;
		}

		if(!$funcionariopertence){
			$idsfuncionarios = false;
		}
		
		return $idsfuncionarios;
	}


	public static function getMeusfuncionariosgeraisescolaImplodeByIdperfilHelper($idfuncionario, $idperfil, $queries = array()) {
		//$queries['idfuncionarioemissor'] = $idfuncionario;
		//$queries['idperfilselecionado'] = $idperfil;
		
		if((int)$idperfil<=0) return false;
		$queries['idperfilselecionado'] = $idperfil;
		//$queries['order'] = "GROUP BY m1.idfuncionarioadicionado";  // Retirado pois estava dando problemas

		
		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		$rows = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas($queries);
		//print_r();
		//die();
		$idsfuncionarios = "";
		$_idsfuncionarios = array();
		$funcionariopertence = false;
		foreach ($rows as $k => $v) {
			if(($v['idfuncionarioadicionado']==$idfuncionario) || ($v['idfuncionarioemissor']==$idfuncionario)){
				$funcionariopertence = true;
			}
			array_push($_idsfuncionarios, $v['idfuncionarioadicionado']);
		}

		$_idsfuncionarios = implode(',', $_idsfuncionarios);

		if((isset($_idsfuncionarios)) && ($_idsfuncionarios) && ($_idsfuncionarios!="")){
			$idsfuncionarios = $_idsfuncionarios;
		}

		if(!$funcionariopertence){
			$idsfuncionarios = false;
		}

		return $idsfuncionarios;
	}


	public static function getMeusfuncionariosgeraisescolaValida($idfuncionario, $idescola, $queries = array()) {
		$queries['idfuncionarioadicionado'] = $idfuncionario;
		$queries['diferenteidescola'] = $idescola;

		$meusfuncionariosgeraisescolas = new Meusfuncionariosgeraisescolas();
		$rows = $meusfuncionariosgeraisescolas->getMeusfuncionariosgeraisescolas($queries);

		$erro = false;
		if(sizeof($rows) > 0){
			$erro = true;
		}
		
		return $erro;
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Meusfuncionariosgeraisescolas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfuncionarioemissor = (array_key_exists("idfuncionarioemissor",$dados)) ? $dados["idfuncionarioemissor"] : $row->idfuncionarioemissor;
 $row->idfuncionarioadicionado = (array_key_exists("idfuncionarioadicionado",$dados)) ? $dados["idfuncionarioadicionado"] : $row->idfuncionarioadicionado;
 $row->idescolaemissor = (array_key_exists("idescolaemissor",$dados)) ? $dados["idescolaemissor"] : $row->idescolaemissor;
 $row->idescolaadicionado = (array_key_exists("idescolaadicionado",$dados)) ? $dados["idescolaadicionado"] : $row->idescolaadicionado;
 $row->idescolaselecionado = (array_key_exists("idescolaselecionado",$dados)) ? $dados["idescolaselecionado"] : $row->idescolaselecionado;

$row->idperfilemissor = (array_key_exists("idperfilemissor",$dados)) ? $dados["idperfilemissor"] : $row->idperfilemissor;

$row->idperfiladicionado = (array_key_exists("idperfiladicionado",$dados)) ? $dados["idperfiladicionado"] : $row->idperfiladicionado;

$row->idperfilselecionado = (array_key_exists("idperfilselecionado",$dados)) ? $dados["idperfilselecionado"] : $row->idperfilselecionado;


 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		//return true;		
		$row->save();
		
		return $row;
	}
	
}