<?php

/**
 * Define o modelo Patrimoniosemprestimos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Patrimoniosemprestimos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "patrimoniosemprestimos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public function getUltimoPatrimonioemprestimo($queries = array()) {
		$queries['order'] = 'ORDER BY p1.id DESC';
		$rows = $this->getPatrimoniosemprestimos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}	
	
	public static function getPatrimoniosemprestimosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$patrimoniosemprestimos = new Patrimoniosemprestimos();
		return $patrimoniosemprestimos->getPatrimoniosemprestimos($queries, $page, $maxpage);
	}
	
	public function getPatrimoniosemprestimos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " p1.id = $id ");
		
		
		$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " p1.iddepartamento = $iddepartamento ");

$idpatrimonio = (isset($queries["idpatrimonio"])) ? $queries["idpatrimonio"] : false;
		if ($idpatrimonio) array_push($where, " p1.idpatrimonio = $idpatrimonio ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " p1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "p1.*"; 
		;
		
		if ($total) $fields = "COUNT(p1.id) as total";
		
		$ordem = "ORDER BY p1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM patrimoniosemprestimos p1
					
					WHERE p1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getPatrimonioemprestimoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getPatrimoniosemprestimos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getPatrimonioemprestimoByIdHelper($id, $queries = array()) {
		$rows = new Patrimoniosemprestimos();
		return $rows->getPatrimonioemprestimoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Patrimoniosemprestimos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
 $row->horaabertura = (array_key_exists("horaabertura",$dados)) ? $dados["horaabertura"] : $row->horaabertura;
 $row->idusuario = (array_key_exists("idusuario",$dados)) ? $dados["idusuario"] : $row->idusuario;
 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->idpatrimonio = (array_key_exists("idpatrimonio",$dados)) ? $dados["idpatrimonio"] : $row->idpatrimonio;
 $row->idfoto = (array_key_exists("idfoto",$dados)) ? $dados["idfoto"] : $row->idfoto;
 $row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
 $row->cor = (array_key_exists("cor",$dados)) ? $dados["cor"] : $row->cor;
 $row->marca = (array_key_exists("marca",$dados)) ? $dados["marca"] : $row->marca;
 $row->tipo = (array_key_exists("tipo",$dados)) ? $dados["tipo"] : $row->tipo;
 $row->modelo = (array_key_exists("modelo",$dados)) ? $dados["modelo"] : $row->modelo;
 $row->caracteristica = (array_key_exists("caracteristica",$dados)) ? $dados["caracteristica"] : $row->caracteristica;
 $row->dimensao = (array_key_exists("dimensao",$dados)) ? $dados["dimensao"] : $row->dimensao;
 $row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
 $row->datacompra = (array_key_exists("datacompra",$dados)) ? $dados["datacompra"] : $row->datacompra;
 $row->descricaobem = (array_key_exists("descricaobem",$dados)) ? $dados["descricaobem"] : $row->descricaobem;
 $row->codbarras = (array_key_exists("codbarras",$dados)) ? $dados["codbarras"] : $row->codbarras;
 $row->qrcode = (array_key_exists("qrcode",$dados)) ? $dados["qrcode"] : $row->qrcode;
 $row->idlocaldestinatario = (array_key_exists("idlocaldestinatario",$dados)) ? $dados["idlocaldestinatario"] : $row->idlocaldestinatario;
 $row->departamento = (array_key_exists("departamento",$dados)) ? $dados["departamento"] : $row->departamento;
 $row->nomeresponsavel = (array_key_exists("nomeresponsavel",$dados)) ? $dados["nomeresponsavel"] : $row->nomeresponsavel;
 $row->RGF = (array_key_exists("RGF",$dados)) ? $dados["RGF"] : $row->RGF;
 $row->idcondicoesemprestimo = (array_key_exists("idcondicoesemprestimo",$dados)) ? $dados["idcondicoesemprestimo"] : $row->idcondicoesemprestimo;
 $row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
 $row->responsavelrecebimento = (array_key_exists("responsavelrecebimento",$dados)) ? $dados["responsavelrecebimento"] : $row->responsavelrecebimento;
 $row->rgf_recebimento = (array_key_exists("rgf_recebimento",$dados)) ? $dados["rgf_recebimento"] : $row->rgf_recebimento;
 $row->emprestarbem = (array_key_exists("emprestarbem",$dados)) ? $dados["emprestarbem"] : $row->emprestarbem;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		
		return true;		
		$row->save();
		
		return $row;
	}
	
}