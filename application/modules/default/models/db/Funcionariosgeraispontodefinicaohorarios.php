<?php
/**
 * Define o modelo Titipoequipamentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosgeraispontodefinicaohorarios extends Zend_Db_Table_Abstract {
	
	/**
            * Define o nome da tabela
            * @var string
        */
	protected $_name = "funcionariosgeraispontodefinicaohorarios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFuncionariosgeraispontodefinicaohorariosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosgeraispontodefinicaohorarios = new Funcionariosgeraispontodefinicaohorarios();
		return $funcionariosgeraispontodefinicaohorarios->getFuncionariosgeraispontodefinicaohorarios($queries, $page, $maxpage);
	}
	
	public function getFuncionariosgeraispontodefinicaohorarios($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " t1.id = $id ");
		
//		rotulohorario	varchar(80)	utf8_general_ci		Sim	NULL		
$rotulohorario = (isset($queries["rotulohorario"])) ? $queries["rotulohorario"] : false;
		if ($rotulohorario) array_push($where, " t1.rotulohorario LIKE '%$rotulohorario%' ");
////3	hora_entrada	time			Sim	NULL		
$hora_entrada = (isset($queries["hora_entrada"])) ? $queries["hora_entrada"] : false;
		if ($hora_entrada) array_push($where, " t1.hora_entrada LIKE '%$hora_entrada%' ");
////4	hora_saida	time			Sim	NULL		
$hora_saida = (isset($queries["hora_saida"])) ? $queries["hora_saida"] : false;
		if ($hora_saida) array_push($where, " t1.hora_saida LIKE '%$hora_saida%' ");
////5	hora_saida	time			Sim	NULL		
$pausa = (isset($queries["pausa"])) ? $queries["pausa"] : false;
		if ($pausa) array_push($where, " t1.pausa LIKE '%$pausa%' ");
////6	hora_pausa_entrada	time			Sim	NULL		
$hora_pausa_entrada = (isset($queries["hora_pausa_entrada"])) ? $queries["hora_pausa_entrada"] : false;
		if ($hora_pausa_entrada) array_push($where, " t1.hora_pausa_entrada LIKE '%$hora_pausa_entrada%' ");
////7	hora_pausa_saida	time			Sim	NULL		
$hora_pausa_saida = (isset($queries["hora_pausa_saida"])) ? $queries["hora_pausa_saida"] : false;
		if ($hora_pausa_saida) array_push($where, " t1.hora_pausa_saida LIKE '%$hora_pausa_saida%' ");
		
$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " t1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "t1.*"; 
		;
		
		if ($total) $fields = "COUNT(t1.id) as total";
		
		$ordem = "ORDER BY t1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
                            FROM funcionariosgeraispontodefinicaohorarios t1

                            WHERE t1.excluido='nao' 
                                    $w 
                            $ordem	
                            $limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariosgeraispontodefinicaohorariosById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosgeraispontodefinicaohorarios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionariosgeraispontodefinicaohorariosByIdHelper($id, $queries = array()) {
		$rows = new Funcionariosgeraispontodefinicaohorarios();
		return $rows->getFuncionariosgeraispontodefinicaohorariosById($id, $queries);
	}	
	
	/**
            * Salva o dados (INSERT OU UPDATE)
            * @param array dados
            * @return Titipoequipamentos
        */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
                
              
                
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 

	
$row->rotulohorario = (array_key_exists("rotulohorario",$dados)) ? $dados["rotulohorario"] : $row->rotulohorario;
	
$row->hora_entrada = (array_key_exists("hora_entrada",$dados)) ? $dados["hora_entrada"] : $row->hora_entrada;
	
$row->hora_saida = (array_key_exists("hora_saida",$dados)) ? $dados["hora_saida"] : $row->hora_saida;

$row->pausa = (array_key_exists("pausa",$dados)) ? $dados["pausa"] : $row->pausa;
	
$row->hora_pausa_entrada = (array_key_exists("hora_pausa_entrada",$dados)) ? $dados["hora_pausa_entrada"] : $row->hora_pausa_entrada;

$row->hora_pausa_saida = (array_key_exists("hora_pausa_saida",$dados)) ? $dados["hora_pausa_saida"] : $row->hora_pausa_saida;
                
                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
				
		$row->save();

		$registro = 'Adicionado';
		if(!$novoRegistro)
			$registro = 'Editado';
			
		Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), " id -> [" . $row->id . "] [" . $registro . "][status=>" . $row->status . "][excluido=>" . $row->excluido . "]");
		
		return $row;
	}
        
         public static function getTotalHorasTrabalhadasByIdHelper($id){
            
            $strSql = "SELECT TIMEDIFF(TIMEDIFF(`hora_saida`,`hora_entrada`), TIMEDIFF(`hora_pausa_saida`,`hora_pausa_entrada`)) diferenca FROM funcionariosgeraispontodefinicaohorarios WHERE id = $id";
            
            $db = Zend_Registry::get('db');
            return $db->fetchRow($strSql);	
        }//end function getTotalHorasTrabalhadas
	
}



