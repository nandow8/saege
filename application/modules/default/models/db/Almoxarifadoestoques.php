<?php

class Almoxarifadoestoques extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "almoxarifadoestoques";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public function getEstoques($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $idlocal = (isset($queries['idlocal'])) ? (int)$queries['idlocal'] : false;
        $idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
        $idresponsavel = (isset($queries['idresponsavel'])) ? (int)$queries['idresponsavel'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " ae.id = $id ");
        if ($idlocal) array_push($where, " ae.idlocal = $idlocal ");
        if ($idescola) array_push($where, " ae.idescola = $idescola ");
        if ($idresponsavel) array_push($where, " ae.idresponsavel = $idresponsavel ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "ae.*, up.perfil, e.escola, at.descricao tipoestoque";

        $ordem = "ORDER BY ae.id";
        if ($order) $ordem = $order;

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(ae.id) as total FROM almoxarifadoestoques ae LEFT JOIN usuariosperfis up ON (ae.idlocal = up.id) LEFT JOIN escolas e ON (ae.idescola = e.id) LEFT JOIN almoxarifadotipoestoques at ON (ae.idtipoestoque = at.id) WHERE ae.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM almoxarifadoestoques ae LEFT JOIN usuariosperfis up ON (ae.idlocal = up.id) LEFT JOIN escolas e ON (ae.idescola = e.id) LEFT JOIN almoxarifadotipoestoques at ON (ae.idtipoestoque = at.id) WHERE ae.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        // var_dump($strsql); die();

        return $db->fetchAll($strsql);
    }

    public function getEstoquesById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getEstoques($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadoestoques
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) {
            $row = $this->createRow();
        }

        $row->estoque = (array_key_exists('estoque',$dados)) ? $dados['estoque'] : $row->estoque;
        $row->idlocal = (array_key_exists('idlocal',$dados)) ? $dados['idlocal'] : $row->idlocal;
        $row->idescola = (array_key_exists('idescola',$dados)) ? $dados['idescola'] : $row->idescola;
        $row->idtipoestoque = (array_key_exists('idtipoestoque',$dados)) ? $dados['idtipoestoque'] : $row->idtipoestoque;

        $row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;

        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');
        }

        $row->save();

        return $row;
    }
}