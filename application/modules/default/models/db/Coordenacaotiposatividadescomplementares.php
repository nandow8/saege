<?php

/**
 * Define o modelo Coordenacaotiposatividadescomplementares
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Coordenacaotiposatividadescomplementares extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "coordenacaotiposatividadescomplementares";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getCoordenacaotiposatividadescomplementaresHelper($queries = array(), $page = 0, $maxpage = 0) {
        $coordenacaotiposatividadescomplementares = new Coordenacaotiposatividadescomplementares();
        return $coordenacaotiposatividadescomplementares->getCoordenacaotiposatividadescomplementares($queries, $page, $maxpage);
    }

    public function getCoordenacaotiposatividadescomplementares($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " c1.id = $id ");

        $ids = (isset($queries['ids'])) ? $queries['ids'] : false;
        if ($ids)
            array_push($where, " c1.id in ($ids) ");

        $tipo = (isset($queries["tipo"])) ? $queries["tipo"] : false;
        if ($tipo)
            array_push($where, " c1.tipo LIKE '%$tipo%' ");

        $descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
        if ($descricoes)
            array_push($where, " c1.descricoes = '$descricoes' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " c1.status LIKE '%$status%' ");



        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "c1.*";
        ;

        if ($total)
            $fields = "COUNT(c1.id) as total";

        $ordem = "ORDER BY c1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM coordenacaotiposatividadescomplementares c1
					
					WHERE c1.excluido='nao' 
						$w 
					$ordem	
					$limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        //echo '<pre>'; print_r($strsql); die();
        
        return $db->fetchAll($strsql);
    }

    public function getCoordenacaotiposatividadescomplementarById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getCoordenacaotiposatividadescomplementares($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getCoordenacaotiposatividadescomplementarByIdHelper($id, $queries = array()) {
        $rows = new Coordenacaotiposatividadescomplementares();
        return $rows->getCoordenacaotiposatividadescomplementarById($id, $queries);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Coordenacaotiposatividadescomplementares
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->tipo = (array_key_exists("tipo", $dados)) ? $dados["tipo"] : $row->tipo;
        $row->descricoes = (array_key_exists("descricoes", $dados)) ? $dados["descricoes"] : $row->descricoes;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;


        $row->save();

        return $row;
    }

}
