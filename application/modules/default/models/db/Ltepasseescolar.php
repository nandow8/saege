<?php

class Ltepasseescolar extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "ltepasseescolar";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";     
    
    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");

        if (!$usuario) return "RGM ou senha inválidos!";
        if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
        if ($usuario['senha']!=$senha) return "A senha é inválida!";
        
        $usuario = $usuario->toArray();

        if (!$ns) $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);
        
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);
        
        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';
        
        $loginNameSpace->alunousuario = serialize($usuario);

        return true;        
    } 
    
    public function getPasseEscolar($queries = array(), $page = 0, $maxpage = 0) {

        $id    = (isset($queries['id'])) ? (int)$queries['id'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " lp.id = $id ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lp.*";

        $ordem = "ORDER BY lp.id";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(id) as total FROM ltepasseescolar lp WHERE lp.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM ltepasseescolar lp WHERE lp.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();
        
        return $db->fetchAll($strsql);      
    }

    public function getPeriodos($queries = array(), $page = 0, $maxpage = 0) {
        $id   = (isset($queries['id'])) ? $queries['id'] : false;

        $fechado = (isset($queries['fechado'])) ? $queries['fechado'] : false;

        $total  = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order  = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id) array_push($where, " lp.id = $id ");
        if ($fechado) array_push($where, " lp.fechado='$fechado' ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lp.id id, lp.datainicio datainicio, lp.datafim datafim, lp.diasletivos diasletivos, round(lp.valorunitario,2) valorunitario, lp.observacoes observacoes, lp.tipo, lp.tipotransporte tipotransporte, lp.status status, lp.excluido excluido, lp.datacriacao datacriacao, lp.logusuario logusuario, lp.logdata logdata, lp.curso";

        $ordem = "ORDER BY lp.datainicio desc";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(id) as total FROM lteperiodos lp WHERE lp.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM lteperiodos lp WHERE lp.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getEstudantesPasse($queries = array(), $page = 0, $maxpage = 0) {
        $id = (isset($queries['id'])) ? $queries['id'] : false;
        $identidade = (isset($queries['identidade'])) ? $queries['identidade'] : false;
        $tipo = (isset($queries['tipo'])) ? $queries['tipo'] : false;
        $tipotransporte = (isset($queries['tipotransporte'])) ? $queries['tipotransporte'] : false;
        $curso = (isset($queries['curso'])) ? $queries['curso'] : false;

        $idperiodo = (isset($queries['idperiodo'])) ? $queries['idperiodo'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id) array_push($where, " vle.id = $id ");
        if ($identidade) array_push($where, " vle.identidade = $identidade ");
        if ($tipo) array_push($where, " vle.tipo = '$tipo' ");
        if ($tipotransporte) array_push($where, " vle.tipotransporte = '$tipotransporte' ");
        if ($curso) array_push($where, " vle.curso = '$curso' ");
        if ($idperiodo) array_push($where, " lpe.idperiodo = $idperiodo ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";
        
        $fields = "vle.*, lpe.id idpasse, ifnull(lpe.faltas, 0) faltas, ifnull(round(lpe.total,2), 0) total";

        $ordem = "ORDER BY vle.nome";

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        if ($total) {
            $strsql = "SELECT COUNT(vle.id) FROM view_lte_estudantes vle 
            inner join ltepasseescolar lpe on (vle.id = lpe.idestudante)
            WHERE lpe.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM view_lte_estudantes vle 
            inner join ltepasseescolar lpe on (vle.id = lpe.idestudante)
            WHERE lpe.excluido = 'nao' $w $ordem";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();

        return $db->fetchAll($strsql);
    }

    public function getEstudantesPasse2($queries = array(), $page = 0, $maxpage = 0) {
        $id   = (isset($queries['id'])) ? $queries['id'] : false;
        $tipo   = (isset($queries['tipo'])) ? $queries['tipo'] : false;
        $tipotransporte = (isset($queries['tipotransporte'])) ? $queries['tipotransporte'] : false;
        $curso = (isset($queries['curso'])) ? $queries['curso'] : false;
        $bolsista = (isset($queries['bolsista'])) ? $queries['bolsista'] : false;

        $total  = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order  = (isset($queries['order'])) ? $queries['order'] : false;

        $where = array();

        if ($id)   array_push($where, " vle.id = $id ");
        if ($tipo) array_push($where, " vle.tipo = '$tipo' ");
        if ($tipotransporte) array_push($where, " vle.tipotransporte = '$tipotransporte' ");
        if ($curso) array_push($where, " vle.curso = '$curso' ");
        if ($bolsista == 'sim') {
            array_push($where, " vle.bolsista = '$bolsista' ");
        }
        else { 
            array_push($where, " (vle.bolsista = 'nao' || vle.bolsista is null) ");
        }

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";
        
        $fields = "vle.*, null idpasse, 0 faltas";

        $ordem = "ORDER BY vle.nome";

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        if ($total) {
            $strsql = "SELECT COUNT(vle.id) FROM view_lte_estudantes vle 
            WHERE 1 $w";
        } else {
            $strsql = "SELECT $fields FROM view_lte_estudantes vle 
            WHERE 1 $w $ordem";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        // var_dump($strsql); die();

        return $db->fetchAll($strsql);
    }
    
    public function getPeriodosById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getPeriodos($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }
    
    public function getEstudantesPasseByTipo($queries = array()) {
        // if ($tipo == '') return false;

        // $queries['tipo'] = $tipo;
        $rows = $this->getEstudantesPasse($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows;
    }

    public static function getPeriodosByIdHelper($id, $queries = array()) {
        $rows = new Ltepasseescolar();
        return $rows->getPeriodosById($id, $queries);
    }       
    
    public static function getPeriodosHelper($queries = array()) {
        $rows = new Ltepasseescolar();
        return $rows->fetchAll();;
    }    

    public static function getNomeByIdHelper($id, $queries = array()) {
        $rows = new Ltepasseescolar();
        $usuario = $rows->getPeriodosById($id, $queries);
        if(!$usuario) return false;
        return $usuario['nome'];
    }   
    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ltepasseescolar
     */
    public function save($dados) {
        // var_dump($dados); die();

        $novoRegistro = true;
        
        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) { 
            $row = $this->createRow();  
        }else {
        }

        $row->idestudante   = (array_key_exists('idestudante',$dados)) ? $dados['idestudante'] : $row->idestudante;
        $row->tipoestudante = (array_key_exists('tipoestudante',$dados)) ? $dados['tipoestudante'] : $row->tipoestudante;
        $row->idperiodo     = (array_key_exists('idperiodo',$dados)) ? $dados['idperiodo'] : $row->idperiodo;
        $row->faltas        = (array_key_exists('faltas',$dados)) ? $dados['faltas'] : $row->faltas;

        $periodos = new Lteperiodos();
        $periodo = $periodos->getPeriodosById($row->idperiodo);

        $row->total = ($periodo['diasletivos'] - $row->faltas) * 2 * $periodo['valorunitario'];

        if ($row->total < 0) {
            $row->faltas = $periodo['diasletivos'];
            $row->total = 0;
        }

        // $row->observacoes   = (array_key_exists('observacoes',$dados)) ? $dados['observacoes'] : $row->observacoes;
        $row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');    
        }
        
        $row->save();
        
        // if($id===0){
        //     Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::incluir", Usuarios::getUsuario("id"), "Passe escolar adicionado com sucesso!");
        // }elseif($id!== 0 AND isset($row->excluido) AND $row->excluido==="sim"){
        //     Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::excluir", Usuarios::getUsuario("id"), "Passe escolar de ID ".$id." excluído com sucesso!");
        // } else Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::atualizar", Usuarios::getUsuario("id"), "Passe escolar de ID ".$id." atualizado com sucesso!");

        return $row;
    } 
}
