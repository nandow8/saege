<?php

/**
 * Define o modelo Nutricaorefeicoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Nutricaorefeicoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "nutricaorefeicoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getNutricaorefeicoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaorefeicoes = new Nutricaorefeicoes();
		return $nutricaorefeicoes->getNutricaorefeicoes($queries, $page, $maxpage);
	}
	
	public function getNutricaorefeicoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " n1.id = $id ");
		
		$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " n1.titulo LIKE '%$titulo%' ");
                
                $porcoes = (isset($queries["porcoes"])) ? $queries["porcoes"] : false;
		if ($porcoes) array_push($where, " n1.porcoes = '$porcoes' ");
                
		$favorita = (isset($queries["favorita"])) ? $queries["favorita"] : false;
		if ($favorita) array_push($where, " n1.favorita LIKE '%$favorita%' ");

                $descricao = (isset($queries["descricao"])) ? $queries["descricao"] : false;
		if ($descricao) array_push($where, " n1.descricao LIKE '%$descricao%' ");                
                
                $status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " n1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "n1.*"; 
		;
		
		if ($total) $fields = "COUNT(n1.id) as total";
		
		$ordem = "ORDER BY n1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM nutricaorefeicoes n1
					
					WHERE n1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getNutricaorefeicaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNutricaorefeicoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNutricaorefeicaoByIdHelper($id, $queries = array()) {
		$rows = new Nutricaorefeicoes();
		return $rows->getNutricaorefeicaoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaorefeicoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 

                
                $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
                $row->favorita = (array_key_exists("favorita",$dados)) ? $dados["favorita"] : $row->favorita;
                $row->porcoes = (array_key_exists("porcoes",$dados)) ? $dados["porcoes"] : $row->porcoes;
                $row->idimagem = (array_key_exists("idimagem",$dados)) ? $dados["idimagem"] : $row->idimagem;
                $row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
		$row->save();

				$registro = 'Adicionado';
				if(!$novoRegistro)
					$registro = 'Editado';

					Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__, Usuarios::getUsuario("id"), " id -> [" . $row->id . "] [" . $registro . "][status=>" . $row->status . "][excluido=>" . $row->excluido . "]");
                
                if (array_key_exists('ingredientes', $dados)) {
                    
                    $this->setRefeicao($dados['ingredientes'], $row->id);
                }
		
		return $row;
	}
	
	private function setRefeicao($dados, $idparent) {

		if (!is_array($dados)) return;
	 
		$itens = new Nutricaorefeicoesingredientes();
                
                $idnutricaorefeicao = $dados['idnutricaorefeicao'];
                $idnutricaotacoalimento = $dados['idnutricaotacoalimento'];
                $descricaotacoalimento = $dados['descricaotacoalimento'];
                $quantidade = $dados['quantidade'];
                $unidade = $dados['unidade'];
                $observacao = $dados['observacao'];
 	
		$ids = array();
		foreach ($idnutricaorefeicao as $i=>$id) {
			$d = array();
			
			$d['idparent'] = $idparent; 
			$d['idnutricaorefeicao'] = trim(strip_tags($idnutricaorefeicao[$i]));
			$d['idnutricaotacoalimento'] = trim(strip_tags($idnutricaotacoalimento[$i]));
                        $d['descricaotacoalimento'] = trim(strip_tags($descricaotacoalimento[$i]));
			$d['quantidade'] = trim(strip_tags($quantidade[$i]));
			$d['unidade'] = trim(strip_tags($unidade[$i]));
			$d['observacao'] = trim(strip_tags($observacao[$i]));
                        
			$d['status'] = 'Ativo';
			$d['excluido'] = 'nao';
			$d['logusuario'] = $dados['logusuario'];
			$d['logdata'] = $dados['logdata'];

                        //var_dump($bancos);die();
			$_row = $itens->save($d);			
			array_push($ids, $_row->id);
		}
		$refeicoes = implode(",", $ids);
                
		if ($refeicoes=="") $refeicoes = "0";
		$strsql = "DELETE FROM nutricaorefeicoesingredientes WHERE idnutricaorefeicao=$idparent AND id NOT IN ($refeicoes)";
		$db = Zend_Registry::get('db');
		$db->query($strsql);
		
	}        
}