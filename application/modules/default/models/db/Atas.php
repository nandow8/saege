<?php

/**
 * Define o modelo Atas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Atas extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "atas";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getAtasHelper($queries = array(), $page = 0, $maxpage = 0) {
        $atas = new Atas();
        return $atas->getAtas($queries, $page, $maxpage);
    }

    public function getAtas($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " a1.id = $id ");


        $iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
        if ($iddepartamento)
            array_push($where, " a1.iddepartamento = $iddepartamento ");

        $data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
        if ($data_i)
            array_push($where, " a1.data >= '$data_i' ");

        $data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
        if ($data_f)
            array_push($where, " a1.data <= '$data_f' ");

        $idperiodo = (isset($queries["idperiodo"])) ? $queries["idperiodo"] : false;
        if ($idperiodo)
            array_push($where, " a1.idperiodo = $idperiodo ");

        $idsfuncionarios = (isset($queries["idsfuncionarios"])) ? $queries["idsfuncionarios"] : false;
        if ($idsfuncionarios)
            array_push($where, " a1.idsfuncionarios = '$idsfuncionarios' ");

        $pauta = (isset($queries["pauta"])) ? $queries["pauta"] : false;
        if ($pauta)
            array_push($where, " a1.pauta LIKE '%$pauta%' ");

        $descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
        if ($descricoes)
            array_push($where, " a1.descricoes LIKE '%$descricoes%' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " a1.status LIKE '%$status%' ");



        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "a1.*";
        ;

        if ($total)
            $fields = "COUNT(a1.id) as total";

        $ordem = "ORDER BY a1.id DESC";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields 
					FROM atas a1
					
					WHERE a1.excluido='nao' 
						$w 
					$ordem	
					$limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getAtaById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getAtas($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getAtaByIdHelper($id, $queries = array()) {
        $rows = new Atas();
        return $rows->getAtaById($id, $queries);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Atas
     */
    public function save($dados) {
        /* echo '<pre>';
          print_r($dados);
          die(); */

        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->iddepartamento = (array_key_exists("iddepartamento", $dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
        $row->data = (array_key_exists("data", $dados)) ? $dados["data"] : $row->data;
        $row->idperiodo = (array_key_exists("idperiodo", $dados)) ? $dados["idperiodo"] : $row->idperiodo;
        $row->idsfuncionarios = (array_key_exists("idsfuncionarios", $dados)) ? $dados["idsfuncionarios"] : $row->idsfuncionarios;
        $row->pauta = (array_key_exists("pauta", $dados)) ? $dados["pauta"] : $row->pauta;
        $row->descricoes = (array_key_exists("descricoes", $dados)) ? $dados["descricoes"] : $row->descricoes;
        $row->idarquivoata = (array_key_exists("idarquivoata", $dados)) ? $dados["idarquivoata"] : $row->idarquivoata;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;


        $row->save();

        if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "tiatas para o departamento $row->iddepartamento para os funcionarios $row->idsfuncionarios salvo com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "tiatas para o departamento $row->iddepartamento para os funcionarios $row->idsfuncionarios com o ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "tiatas para o departamento $row->iddepartamento para os funcionarios $row->idsfuncionarios com o ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "tiatas para o departamento $row->iddepartamento para os funcionarios $row->idsfuncionarios com o ID ".$id." atualizada com sucesso!");

        return $row;
    }

}
