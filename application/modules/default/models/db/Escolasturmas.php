<?php

/**
 * Define o modelo Escolasturmas
 *
 * @author		Alexandre Martin Narciso
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasturmas extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "escolasturmas";

    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public static function getEscolasturmasHelper($queries = array(), $page = 0, $maxpage = 0) {
        $escolasturmas = new Escolasturmas();
        return $escolasturmas->getEscolasturmas($queries, $page, $maxpage);
    }

    public function getEscolasturmas($queries = array(), $page = 0, $maxpage = 0) {
        $where = array();

        $sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
        $total = (isset($queries['total'])) ? (int) $queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        $groupby = (isset($queries['group'])) ? $queries['group'] : false;

        $id = (isset($queries['id'])) ? (int) $queries['id'] : false;
        if ($id)
            array_push($where, " e1.id = $id ");


        $idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
        if ($idsecretaria)
            array_push($where, " e1.idsecretaria = $idsecretaria ");

        $idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
        if ($idescola)
            array_push($where, " e1.idescola = $idescola ");

        $turma = (isset($queries["turma"])) ? $queries["turma"] : false;
        if ($turma)
            array_push($where, " e1.turma LIKE '%$turma%' ");

        $cor = (isset($queries["cor"])) ? $queries["cor"] : false;
        if ($cor)
            array_push($where, " e1.cor LIKE '%$cor%' ");

        $status = (isset($queries["status"])) ? $queries["status"] : false;
        if ($status)
            array_push($where, " e1.status LIKE '%$status%' ");

        if ($sorting) {
            $sorting = explode('_', $sorting);
            if (sizeof($sorting) == 2) {

                if ($sorting[0] == 'nome')
                    $sorting[0] = 'l1.nome';

                $order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
            }
        }

        $w = "";
        foreach ($where as $k => $v) {
            if ($k > 0)
                $w .= " AND ";
            $w .= $v;
        }
        if ($w != "")
            $w = "AND ($w)";

        $fields = "e1.*";
        ;

        if ($total)
            $fields = "COUNT(e1.id) as total";

        $ordem = "ORDER BY e1.id";
        if ($order)
            $ordem = $order;

        $limit = "";
        if ($maxpage > 0)
            $limit = "LIMIT " . ($page * $maxpage) . ", $maxpage";

        $strsql = "SELECT $fields
					FROM escolasturmas e1
					WHERE e1.excluido='nao'
					$w
					$ordem
					$limit";

        if ((isset($queries['return_sql'])) && ($queries['return_sql']))
            return $strsql;
        $db = Zend_Registry::get('db');
        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }

        return $db->fetchAll($strsql);
    }

    public function getturmasByIdEscola($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['idescola'] = $id;
        $rows = $this->getEscolasturmas($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public function getescolaturmaById($id, $queries = array()) {
        if ($id == 0)
            return false;

        $queries['id'] = $id;
        $rows = $this->getEscolasturmas($queries, 0, 0);

        if (sizeof($rows) == 0)
            return false;
        return $rows[0];
    }

    public static function getescolaturmaByIdHelper($id, $queries = array()) {
        $rows = new Escolasturmas();
        return $rows->getescolaturmaById($id, $queries);
    }

    /**
     * Retorna somente a turmas
     */
    public function getEscolasturmasAgrupadas() {

        $strsql = "SELECT DISTINCT turma FROM escolasturmas as t1 WHERE t1.excluido='nao' AND ( t1.status LIKE '%Ativo%' )";
        $db = Zend_Registry::get('db');

        $rows = $db->fetchAll($strsql);
        if (sizeof($rows) == 0)
            return false;
        return $rows;
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasturmas
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int) $dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row)
            $row = $this->createRow();
        else {
            $novoRegistro = false;
        }

        $row->idsecretaria = (array_key_exists("idsecretaria", $dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
        $row->idescola = (array_key_exists("idescola", $dados)) ? $dados["idescola"] : $row->idescola;
        $row->turma = (array_key_exists("turma", $dados)) ? $dados["turma"] : $row->turma;
        $row->cor = (array_key_exists("cor", $dados)) ? $dados["cor"] : $row->cor;
        $row->status = (array_key_exists("status", $dados)) ? $dados["status"] : $row->status;
        if (is_null($row->datacriacao)) {
            $row->datacriacao = date("Y-m-d H:i:s");
        }

        $row->excluido = (array_key_exists("excluido", $dados)) ? $dados["excluido"] : $row->excluido;
        $row->logdata = (array_key_exists("logdata", $dados)) ? $dados["logdata"] : $row->logdata;
        $row->logusuario = (array_key_exists("logusuario", $dados)) ? $dados["logusuario"] : $row->logusuario;


        $row->save();

        return $row;
    }

}
