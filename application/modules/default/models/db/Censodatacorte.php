<?php

/**
 * Define o modelo Censodatacorte
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Censodatacorte extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "censodatacorte";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getCensodatacorteHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$censodatacorte = new Censodatacorte();
		return $censodatacorte->getCensodatacorte($queries, $page, $maxpage);
	}
	
	public function getCensodatacorte($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " c1.id = $id ");
		
		
 

$nome = (isset($queries["nome"])) ? $queries["nome"] : false;
		if ($nome) array_push($where, " c1.nome LIKE '%$nome%' ");

$data_inicio = (isset($queries["data_inicio"])) ? $queries["data_inicio"] : false;
		if ($data_inicio) array_push($where, " c1.data_inicio >= '$data_inicio' ");

$data_fim = (isset($queries["data_fim"])) ? $queries["data_fim"] : false;
		if ($data_fim) array_push($where, " c1.data_fim <= '$data_fim' ");
 
$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " c1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "c1.*"; 
		;
		
		if ($total) $fields = "COUNT(c1.id) as total";
		
		$ordem = "ORDER BY c1.status DESC";
		 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

	 
		$strsql = "SELECT $fields 
					FROM censodatacorte c1
					
					WHERE c1.excluido='nao'
						$w 
					$ordem	
					$limit";
				  
		 //if($total);die($strsql);

		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
 
	public function getCensodatacorteById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getCensodatacorte($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getCensodatacorteByIdHelper($id, $queries = array()) {
		$rows = new Censodatacorte();
		return $rows->getCensodatacorteById($id, $queries);
	}	
	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Censodatacorte
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		
 $row->nome = (array_key_exists("nome",$dados)) ? $dados["nome"] : $row->nome;
 $row->data_inicio = (array_key_exists("data_inicio",$dados)) ? $dados["data_inicio"] : $row->data_inicio;
 $row->data_fim = (array_key_exists("data_fim",$dados)) ? $dados["data_fim"] : $row->data_fim;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
  
 
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}