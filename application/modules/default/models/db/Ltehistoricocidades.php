<?php

class Ltehistoricocidades extends Zend_Db_Table_Abstract {
    
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "ltehistoricocidades";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public function gethistoricocidades($queries = array(), $page = 0, $maxpage = 0) {
        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $idcidade = (isset($queries['idcidade'])) ? (int)$queries['idcidade'] : false;

        $total  = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order  = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " lhc.id = $id ");
        if ($idcidade) array_push($where, " lhc.idcidade = $idcidade ");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "lhc.*, u.nomerazao";

        $ordem = "ORDER BY logdata desc";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

        if ($total) {
            $strsql = "SELECT count(id) as total FROM ltehistoricocidades lhc LEFT JOIN ltecidades lc ON lhc.idcidade = lc.id LEFT JOIN usuarios u ON lhc.logusuario = u.id WHERE 1 $w";
        } else {
            $strsql = "SELECT $fields FROM ltehistoricocidades lhc LEFT JOIN ltecidades lc ON lhc.idcidade = lc.id LEFT JOIN usuarios u ON lhc.logusuario = u.id WHERE 1 $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        return $db->fetchAll($strsql); 
    }

    public static function getHistoricoCidadesByIdCidade($idcidade, $queries = array()) {
        $queries['idcidade'] = $idcidade;

        $historicos = new Ltehistoricocidades();
        $rows = $historicos->gethistoricocidades($queries);
        return $rows;
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ltehistoricocidades
     */
    public function save($dados) {

        $id = 0;

        $row = $this->createRow();

        $row->idcidade = (int)$dados['idcidade'];
        $row->cidade = $dados['cidade'];
        $row->valorfretado = $dados['valorfretado'];
        $row->subsidio = $dados['subsidio'];
        $row->logusuario = $dados["logusuario"];
        $row->logdata = $dados["logdata"];
        
        $row->save();
               
        Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::incluir", Usuarios::getUsuario("id"), "Histórico da cidade adicionada com sucesso!");
      
        return $row;
    }
}