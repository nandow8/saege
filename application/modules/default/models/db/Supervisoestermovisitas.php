<?php

class Supervisoestermovisitas extends Zend_DB_Table_Abstract{
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "termovisitasupervisao";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getTermoVisitaHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$protocolos = new Supervisoestermovisitas();
		return $protocolos->getTermoVisita($queries, $page, $maxpage);
	}
	
	public function getTermoVisita($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " t1.id = $id ");
		
		$datacriacao = (isset($queries["datacriacao"])) ? $queries["datacriacao"] : false;
		if($datacriacao) array_push($where, " t1.datacriacao = '$datacriacao' ");

		$idusuariocriacao = (isset($queries["idusuariocriacao"])) ? $queries["idusuariocriacao"] : false;
		if ($idusuariocriacao) array_push($where, " t1.idusuariocriacao = $idusuariocriacao ");

		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
				if ($idescola) array_push($where, " t1.idescola = $idescola ");


		$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
				if ($sequencial) array_push($where, " t1.sequencial LIKE '%$sequencial%' ");

		$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
				if ($titulo) array_push($where, " t1.titulo LIKE '%$titulo%' ");

		$descricao = (isset($queries["descricao"])) ? $queries["descricao"] : false;
				if ($descricao) array_push($where, " t1.descricao = '$descricao' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
				if ($status) array_push($where, " t1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "t1.*"; 
		;
		
		if ($total) $fields = "COUNT(t1.id) as total";
		
		$ordem = "ORDER BY t1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM termovisitasupervisao t1
					
					WHERE t1.excluido='nao' 
						$w 
					$ordem	
					$limit";		
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);
	}	
	
	public function getTermoVisitaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTermoVisita($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTermoVisitaByIdHelper($id, $queries = array()) {
		$rows = new Supervisoestermovisitas();
		return $rows->getTermoVisitaById($id, $queries);
	}		
	
	public static function getSupervisoresHelper($queries = array()){
		$queries['idperfil'] = 15;
		$usuarios = new Usuarios;
		return $usuarios->getUsuarios($queries);

	}

	/**
	* Gerar data atual (dia/mês/ano) com formato português
	* @return string data
	*/
	public function getCurrentData(){
		$data = date('D');
	    $mes = date('M');
	    $dia = date('d');
	    $ano = date('Y');
    
	    $semana = array(
	        'Sun' => 'Domingo', 
	        'Mon' => 'Segunda-Feira',
	        'Tue' => 'Terça-Feira',
	        'Wed' => 'Quarta-Feira',
	        'Thu' => 'Quinta-Feira',
	        'Fri' => 'Sexta-Feira',
	        'Sat' => 'Sábado'
	    );
    
	    $mes_extenso = array(
	        'Jan' => 'Janeiro',
	        'Feb' => 'Fevereiro',
	        'Mar' => 'Marco',
	        'Apr' => 'Abril',
	        'May' => 'Maio',
	        'Jun' => 'Junho',
	        'Jul' => 'Julho',
	        'Aug' => 'Agosto',
	        'Nov' => 'Novembro',
	        'Sep' => 'Setembro',
	        'Oct' => 'Outubro',
	        'Dec' => 'Dezembro'
	    );
	    
        $data = "{$dia} de " . $mes_extenso["$mes"] . " de {$ano}";
        return $data;
	}
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Protocolos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow(); 	
		else {			
			$novoRegistro = false;
		} 
		
		$row->idusuariocriacao = (array_key_exists("idusuariocriacao",$dados)) ? $dados["idusuariocriacao"] : $row->idusuariocriacao;		
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 		
 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d");
		}
		 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;

		$row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
		$row->descricao = (array_key_exists("descricao",$dados)) ? $dados["descricao"] : $row->descricao;
				
		$row->datacriacao = (array_key_exists("datacriacao",$dados)) ? $dados["datacriacao"] : $row->datacriacao;

		$row->diretor = (array_key_exists("diretor",$dados)) ? $dados["diretor"] : $row->diretor;
		 $row->idusuarioaceite = (array_key_exists("idusuarioaceite",$dados)) ? $dados["idusuarioaceite"] : $row->idusuarioaceite;	
		$row->dataaceite = (array_key_exists("dataaceite",$dados)) ? $dados["dataaceite"] : $row->dataaceite;

		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;

		$row->save();
		return $row;
	}
	
	/*private function setItens($row_protocolo) {
		$protocolos = new Protocolos;
		$protocolo = $protocolos->getProtocoloById($row_protocolo['id']);

		$solicitacoes = new Solicitacoes();
		$ultimasolicitacao = $solicitacoes->getUltimaSolicitacaoByIdprotocolo($row_protocolo['id']);


		$dados = array();
		$dados['idprotocolo'] = $row_protocolo['id'];
		$dados['idsecretaria'] = $row_protocolo['idsecretaria'];
		$dados["idusuariocriacao"] = $row_protocolo['idusuariocriacao'];
		$dados["iddepartamentosecretaria"] = $row_protocolo['iddepartamentosecretaria'];
		$dados["idusuarioresponsavel"] = $row_protocolo['idusuarioresponsavel'];
		$dados["idescola"] = $row_protocolo['idescola'];
		$dados["idescolausuariocriacao"] = $row_protocolo['idescolausuariocriacao'];
		$dados["idescolausuarioresponsavel"] = $row_protocolo['idescolausuarioresponsavel'];
		$dados["idaluno"] = $row_protocolo['idaluno'];
		$dados["titulo"] = $row_protocolo['titulo'];
		$dados["descricao"] = $row_protocolo['descricao'];
		$dados["origem"] = "Secretaria";
		$dados["destinatario"] = $row_protocolo['destinatario'];
		$dados["observacoes"] = $row_protocolo['observacoes'];

		$dados["dataagendamento"] = $row_protocolo['dataagendamento'];
		$dados["tipo"] = $row_protocolo['tipo'];
		$dados["prioridade"] = $row_protocolo['prioridade'];
		


		$dados["status"] = $row_protocolo['status'];
		$dados['excluido'] = $row_protocolo['excluido'];
		$dados['logusuario'] = $row_protocolo['logusuario'];
		$dados['logdata'] = $row_protocolo['logdata'];
		//echo 'itemm 1';
		if((isset($row_protocolo['id'])) && ($row_protocolo['id']>0)){
			$alteracao = false;
			//echo 'itemm 2';
			
			//var_dump($ultimasolicitacao); 
			//var_dump($row_protocolo); 
			if(
				($ultimasolicitacao['iddepartamentosecretaria']!=$row_protocolo['iddepartamentosecretaria']) || 
				($ultimasolicitacao['idusuarioresponsavel']!=$row_protocolo['idusuarioresponsavel']) || 
				($ultimasolicitacao['idusuarioresponsavel']!=$row_protocolo['idusuarioresponsavel']) || 
				($ultimasolicitacao['idescola']!=$row_protocolo['idescola']) || 
				($ultimasolicitacao['idescolausuarioresponsavel']!=$row_protocolo['idescolausuarioresponsavel']) || 
				($ultimasolicitacao['idaluno']!=$row_protocolo['idaluno']) || 
				($ultimasolicitacao['titulo']!=$row_protocolo['titulo']) || 
				($ultimasolicitacao['descricao']!=$row_protocolo['descricao']) || 
				($ultimasolicitacao['observacoes']!=$row_protocolo['observacoes']) || 
				($ultimasolicitacao['dataagendamento']!=$row_protocolo['dataagendamento']) || 
				($ultimasolicitacao['tipo']!=$row_protocolo['tipo']) || 
				($ultimasolicitacao['prioridade']!=$row_protocolo['prioridade']) || 
				($ultimasolicitacao['status']!=$row_protocolo['status'])){
					
					if($row_protocolo['excluido']!="sim") $solicitacoes->save($dados);
			}elseif (!$ultimasolicitacao) {
			//	echo 'itemm 4';
				if($row_protocolo['excluido']!="sim") $solicitacoes->save($dados);
			}
		}

		
		//die();
		
	}*/
}