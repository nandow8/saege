<?php

/**
 * Define o modelo Fornecedores
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Fornecedores extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "fornecedores";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFornecedoresHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$fornecedores = new Fornecedores();
		return $fornecedores->getFornecedores($queries, $page, $maxpage);
	}
	
	public function getFornecedores($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? $queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " f1.idsecretaria = $idsecretaria ");

$razaosocial = (isset($queries["razaosocial"])) ? $queries["razaosocial"] : false;
		if ($razaosocial) array_push($where, " f1.razaosocial LIKE '%$razaosocial%' ");

$nomefantasia = (isset($queries["nomefantasia"])) ? $queries["nomefantasia"] : false;
		if ($nomefantasia) array_push($where, " f1.nomefantasia LIKE '%$nomefantasia%' ");

$cpfcnpj = (isset($queries["cpfcnpj"])) ? $queries["cpfcnpj"] : false;
		if ($cpfcnpj) array_push($where, " f1.cpfcnpj LIKE '%$cpfcnpj%' ");

$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
		if ($telefone) array_push($where, " f1.telefone LIKE '%$telefone%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");

$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " f1.origem LIKE '%$origem%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado "; ;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM fornecedores f1
					 LEFT JOIN enderecos e_idendereco ON e_idendereco.id=f1.idendereco 
						LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado	
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFornecedorById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFornecedores($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFornecedorByIdHelper($id, $queries = array()) {
		$rows = new Fornecedores();
		return $rows->getFornecedorById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Fornecedores
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;
 $row->razaosocial = (array_key_exists("razaosocial",$dados)) ? $dados["razaosocial"] : $row->razaosocial;
 $row->nomefantasia = (array_key_exists("nomefantasia",$dados)) ? $dados["nomefantasia"] : $row->nomefantasia;
 $row->cpfcnpj = (array_key_exists("cpfcnpj",$dados)) ? $dados["cpfcnpj"] : $row->cpfcnpj;
 $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->idendereco = (array_key_exists("idendereco",$dados)) ? $dados["idendereco"] : $row->idendereco;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();

		if($id==0){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::incluir", Usuarios::getUsuario("id"), "Fornecedores salvo com sucesso!");
		}elseif($dados['excluido'] == 'sim'){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::excluir", Usuarios::getUsuario("id"), "Fornecedores com o ID ".$id." excluída com sucesso!");
		}elseif(isset($oldstatus) AND $oldstatus != $dados['status']){
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::status", Usuarios::getUsuario("id"), "Fornecedores com o ID ".$id." - ". $dados['status']." com sucesso!");
		}else 
			Fjmx_Util::geraLog(date('Y-m-d'), __CLASS__."::atualizar", Usuarios::getUsuario("id"), "Fornecedores com o ID ".$id." atualizada com sucesso!");
		
		return $row;
	}
	
}