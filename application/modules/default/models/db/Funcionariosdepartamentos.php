<?php

/**
 * Define o modelo Funcionariosdepartamentos
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Funcionariosdepartamentos extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "funcionariosdepartamentos";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFuncionariosdepartamentosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$funcionariosdepartamentos = new Funcionariosdepartamentos();
		return $funcionariosdepartamentos->getFuncionariosdepartamentos($queries, $page, $maxpage);
	}
	
	public function getFuncionariosdepartamentos($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		
		$idfuncionario = (isset($queries["idfuncionario"])) ? $queries["idfuncionario"] : false;
		if ($idfuncionario) array_push($where, " f1.idfuncionario = $idfuncionario ");

$iddepartamento = (isset($queries["iddepartamento"])) ? $queries["iddepartamento"] : false;
		if ($iddepartamento) array_push($where, " f1.iddepartamento = $iddepartamento ");

$telefone = (isset($queries["telefone"])) ? $queries["telefone"] : false;
		if ($telefone) array_push($where, " f1.telefone LIKE '%$telefone%' ");

$funcao = (isset($queries["funcao"])) ? $queries["funcao"] : false;
		if ($funcao) array_push($where, " f1.funcao LIKE '%$funcao%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*"; 
		;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM funcionariosdepartamentos f1
					
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFuncionariodepartamentoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFuncionariosdepartamentos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFuncionariodepartamentoByIdHelper($id, $queries = array()) {
		$rows = new Funcionariosdepartamentos();
		return $rows->getFuncionariodepartamentoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Funcionariosdepartamentos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idfuncionario = (array_key_exists("idfuncionario",$dados)) ? $dados["idfuncionario"] : $row->idfuncionario;
 $row->iddepartamento = (array_key_exists("iddepartamento",$dados)) ? $dados["iddepartamento"] : $row->iddepartamento;
 $row->telefone = (array_key_exists("telefone",$dados)) ? $dados["telefone"] : $row->telefone;
 $row->funcao = (array_key_exists("funcao",$dados)) ? $dados["funcao"] : $row->funcao;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
		//return true;		
		$row->save();
		
		return $row;
	}
	
}