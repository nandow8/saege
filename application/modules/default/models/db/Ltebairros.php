<?php

class Ltebairros extends Zend_Db_Table_Abstract {
    
    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "ltebairros";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";     
    
    public function autenticargm($rgm, $senha, $ns = false) {
        $usuario = $this->fetchRow("rgm='$rgm' AND excluido='nao'");
            
        if (!$usuario) return "RGM ou senha inválidos!";
        if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
        if ($usuario['senha']!=$senha) return "A senha é inválida!";
        
        $usuario = $usuario->toArray();

        if (!$ns) $ns = Mn_Util::getAdminNameSpace();
        $loginNameSpace = new Zend_Session_Namespace($ns);
        
        $escolas = new Escolas();
        $escola = $escolas->getEscolaById($usuario['idescola']);
        
        unset($escola['id']);
        unset($escola['email']);
        $usuario = array_merge($usuario, $escola);
        $usuario['senha'] = $senha;
        $usuario['tipousuario'] = 'aluno';
        
        $loginNameSpace->alunousuario = serialize($usuario);
                        
        return true;        
    }   
    
    public static function getUsuario($field = null, $ns = false) {
        $ns = (!$ns) ? Mn_Util::getAdminNameSpace() : $ns;
        
        $session = new Zend_Session_Namespace($ns);
        if (!isset($session->alunousuario)) return false;
        
        $escola = unserialize($session->alunousuario);
        if (is_null($field)) return $escola;
        
        return $escola[$field];     
    }   
    
    public function getbairros($queries = array(), $page = 0, $maxpage = 0) {

        $id     = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $bairro = (isset($queries['bairro'])) ? $queries['bairro'] : false;
        $cidade = (isset($queries['cidade'])) ? $queries['cidade'] : false;

        $total  = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order  = (isset($queries['order'])) ? $queries['order'] : false;
        $excluido = (isset($queries['excluido'])) ? $queries['excluido'] : false;
        $where = array();

        if ($id)         array_push($where, " le.id = $id ");
        if ($bairro)    array_push($where, " le.bairro LIKE '%$bairro%' ");
        if ($cidade)      array_push($where, " le.cidade LIKE '%$cidade%' ");
        if($excluido) array_push($where, " le.excluido = '$excluido'");

        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "le.*";

        $ordem = "ORDER BY bairro";
        if ($order) $ordem = $order; 

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(id) as total FROM ltebairros le WHERE le.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM ltebairros le WHERE le.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        return $db->fetchAll($strsql);      
    }   
    
    public function getbairroById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getbairros($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }
    
    public static function getbairroByIdHelper($id, $queries = array()) {
        $rows = new Ltebairros();
        return $rows->getbairroById($id, $queries);
    }       
    
    public static function getBairrosHelper($queries = array()) {
        $rows = new Ltebairros();
        return $rows->getbairros($queries);
    }    

    public static function getNomeByIdHelper($id, $queries = array()) {
        $rows = new Ltebairros();
        $usuario = $rows->getbairroById($id, $queries);
        if(!$usuario) return false;
        return $usuario['nome'];
    }   
    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Ltebairro
     */
    public function save($dados) {

        $novoRegistro = true;
        $integracao = false;
        
        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) { 
            $row = $this->createRow();  
        }else {
            /*
            $novoRegistro = false;
            $historico = new Escolashistoricos_Escolasalunos();         
            $_historico = $row->toArray();
            
            $historico->arquiva($_historico);
            */
        }

        $row->bairro     = (array_key_exists('bairro',$dados)) ? $dados['bairro'] : $row->bairro;
        $row->cidade      = (array_key_exists('cidade',$dados)) ? $dados['cidade'] : $row->cidade;
        $row->status      = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido    = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');
            $row->usuariocriacao = Usuarios::getUsuario('id');
        }
        
        $row->save();
        
        if($id===0){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::incluir", Usuarios::getUsuario("id"), "Bairro adicionado com sucesso!");
        }elseif($id!== 0 AND isset($row->excluido) AND $row->excluido==="sim"){
            Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::excluir", Usuarios::getUsuario("id"), "Bairro de ID ".$id." excluído com sucesso!");
        } else Fjmx_Util::geraLog(date('Y-m-d'), __METHOD__."::atualizar", Usuarios::getUsuario("id"), "Bairro de ID ".$id." atualizado com sucesso!");
 
        return $row;
    }
    
    public function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }   
}
