<?php

class Escolasalunospais extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasalunospais";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	
	public function autentica($cpfcnpj, $senha, $ns = false) {
		$rows = new Escolasalunospais();

		$_rows = new Escolasalunos();
		$usuario = $_rows->fetchRow("cpfcnpj='$cpfcnpj' AND senha = '$senha' AND excluido='nao'");
		//$usuario = $rows->getEscolapaiByUsuario($_usuario);
		//$usuario = $this->fetchRow("usuario='$_usuario' AND excluido='nao'");
		
		if (!$usuario) return "RGF ou senha inválidos!";
		if ($usuario['status']=='Bloqueado') return "O usuário está bloqueado!";
		if ($usuario['senha']!=$senha) return "A senha é inválida!";
		
		$escolasalunos = new Escolasalunos();
		$usuario = $escolasalunos->getEscolaalunoById($usuario['id']);
		
		if (!$ns) $ns = Mn_Util::getAdminNameSpace();
		$loginNameSpace = new Zend_Session_Namespace($ns);
		
		$escolas = new Escolas();
		$escola = $escolas->getEscolaById($usuario['idescola']);
		
		unset($escola['id']);
		unset($escola['email']);
		$usuario = array_merge($usuario, $escola);
		$usuario['senha'] = $senha;
		$usuario['tipousuario'] = 'pais';
		
		$loginNameSpace->alunousuario = serialize($usuario);
						
		return true;		
	}	
	
	public static function getUsuario($field = null, $ns = false) {
		$ns = (!$ns) ? Mn_Util::getAdminNameSpace() : $ns;
		
		$session = new Zend_Session_Namespace($ns);
		if (!isset($session->alunousuario)) return false;
		
		$escola = unserialize($session->alunousuario);
		if (is_null($field)) return $escola;
		
		return $escola[$field];		
	}

	public static function getEscolaAtiva($id, $field = false) {
		$usuarios = new Escolasusuarios();
		$usuario = $usuarios->fetchRow("id=$id");
		
		if (!$usuario) return false;
		
		
		$idescola = (int)$usuario['idescola'];
		
		$escolas = new Escolas();
		$escola = $escolas->fetchRow("id=$idescola");
		if (!$escola) return false;
		
		if (!$field) return $escola;
		
		return $escola[$field];
	}	
	
	public static function getEscolasalunosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolasalunospais();
		return $produtos->getEscolasalunos($queries, $page, $maxpage);
	}
	
	public function getEscolasalunos($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		$idaluno = (isset($queries['idaluno'])) ? (int)$queries['idaluno'] : false;
		$usuario = (isset($queries['usuario'])) ? $queries['usuario'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " ea1.id=$id ");
		if ($idescola) array_push($where, " ea1.idescola=$idescola ");
		if ($idaluno) array_push($where, " ep1.idaluno=$idaluno ");
		if ($status) array_push($where, " ep1.status='$status' ");
		if ($usuario) array_push($where, " ep1.usuario='$usuario' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "ep1.*, e1.escola, ea1.idescola";
		if ($total) $fields = "COUNT(ea1.id) as total";
		
		
		$ordem = "ORDER BY ea1.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasalunospais ep1
						LEFT JOIN escolasalunos ea1 ON ea1.id=ep1.idaluno
						LEFT JOIN escolas e1 ON e1.id=ea1.idescola
					WHERE ea1.excluido='nao'
						AND e1.excluido = 'nao'
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getEscolaalunoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasalunos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolapaiByIdalunoHelper($idaluno, $queries = array()) {
		$rows = new Escolasalunospais();
		return $rows->getEscolapaiByIdaluno($idaluno, $queries);
	}	

	public function getEscolapaiByIdaluno($idaluno, $queries = array()) {
		if ($idaluno==0) return false;
		
		$queries['idaluno'] = $idaluno;
		$rows = $this->getEscolasalunos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaalunoByIdHelper($id, $queries = array()) {
		$rows = new Escolasalunospais();
		return $rows->getEscolaalunoById($id, $queries);
	}		
	
	public function getEscolapaiByUsuario($usuario, $queries = array()) {
		if (!$usuario) return false;
		
		$queries['usuario'] = $usuario;
		$rows = $this->getEscolasalunos($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaalunoByUsuarioHelper($usuario, $queries = array()) {
		$rows = new Escolasalunospais();
		return $rows->getEscolapaiByUsuario($usuario, $queries);
	}	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasalunos
     */
	public function save($dados) {
		$novoRegistro = true;
		$integracao = false;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();	
		}else {
			$novoRegistro = false;
			/*$historico = new Escolashistoricos_Escolasalunos();			
			$_historico = $row->toArray();
			
			$historico->arquiva($_historico);*/
		} 

		$row->idaluno = (array_key_exists('idaluno',$dados)) ? $dados['idaluno'] : $row->idaluno;
		$row->usuario = (array_key_exists('usuario',$dados)) ? $dados['usuario'] : $row->usuario;
		$row->senha = (array_key_exists('senha',$dados)) ? $dados['senha'] : $row->senha;		
		
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if (!$row->datacriacao) {
			$row->datacriacao = date('Y-m-d G:i:s'); 	
		}
		
		$row->save();

		if($row->excluido == "sim") $integracoes->Enviarexclusaomatriculara($row['id']);
		
		return $row;
	}
}