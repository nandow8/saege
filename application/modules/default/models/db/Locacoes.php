<?php

/**
 * Define o modelo Locacoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Locacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "locacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getLocacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$locacoes = new Locacoes();
		return $locacoes->getLocacoes($queries, $page, $maxpage);
	}
	
	public function getLocacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " l1.id = $id ");
		
		
		$idunidade = (isset($queries["idunidade"])) ? $queries["idunidade"] : false;
		if ($idunidade) array_push($where, " l1.idunidade = $idunidade ");

$datainicio_i = (isset($queries["datainicio_i"])) ? $queries["datainicio_i"] : false;
		if ($datainicio_i) array_push($where, " l1.datainicio >= '$datainicio_i' ");

$datainicio_f = (isset($queries["datainicio_f"])) ? $queries["datainicio_f"] : false;
		if ($datainicio_f) array_push($where, " l1.datainicio <= '$datainicio_f' ");

$datafim_i = (isset($queries["datafim_i"])) ? $queries["datafim_i"] : false;
		if ($datafim_i) array_push($where, " l1.datafim >= '$datafim_i' ");

$datafim_f = (isset($queries["datafim_f"])) ? $queries["datafim_f"] : false;
		if ($datafim_f) array_push($where, " l1.datafim <= '$datafim_f' ");

$datasolicitacao_i = (isset($queries["datasolicitacao_i"])) ? $queries["datasolicitacao_i"] : false;
		if ($datasolicitacao_i) array_push($where, " l1.datasolicitacao >= '$datasolicitacao_i' ");

$datasolicitacao_f = (isset($queries["datasolicitacao_f"])) ? $queries["datasolicitacao_f"] : false;
		if ($datasolicitacao_f) array_push($where, " l1.datasolicitacao <= '$datasolicitacao_f' ");

$numeroaditivo = (isset($queries["numeroaditivo"])) ? $queries["numeroaditivo"] : false;
		if ($numeroaditivo) array_push($where, " l1.numeroaditivo = '$numeroaditivo' ");

$valormensal = (isset($queries["valormensal"])) ? $queries["valormensal"] : false;
		if ($valormensal) array_push($where, " l1.valormensal = $valormensal ");

$total = (isset($queries["total"])) ? $queries["total"] : false;
		if ($total) array_push($where, " l1.total = $total ");

$siano = (isset($queries["siano"])) ? $queries["siano"] : false;
		if ($siano) array_push($where, " l1.siano LIKE '%$siano%' ");

$procano = (isset($queries["procano"])) ? $queries["procano"] : false;
		if ($procano) array_push($where, " l1.procano LIKE '%$procano%' ");

$recursos = (isset($queries["recursos"])) ? $queries["recursos"] : false;
		if ($recursos) array_push($where, " l1.recursos LIKE '%$recursos%' ");

$informacoesadicionais = (isset($queries["informacoesadicionais"])) ? $queries["informacoesadicionais"] : false;
		if ($informacoesadicionais) array_push($where, " l1.informacoesadicionais = '$informacoesadicionais' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " l1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "l1.*"; 
		;
		
		if ($total) $fields = "COUNT(l1.id) as total";
		
		$ordem = "ORDER BY l1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM locacoes l1
					
					WHERE l1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getLocacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getLocacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getLocacaoByIdHelper($id, $queries = array()) {
		$rows = new Locacoes();
		return $rows->getLocacaoById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Locacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idunidade = (array_key_exists("idunidade",$dados)) ? $dados["idunidade"] : $row->idunidade;
		$row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
		$row->datafim = (array_key_exists("datafim",$dados)) ? $dados["datafim"] : $row->datafim;
		$row->datasolicitacao = (array_key_exists("datasolicitacao",$dados)) ? $dados["datasolicitacao"] : $row->datasolicitacao;
		$row->numeroaditivo = (array_key_exists("numeroaditivo",$dados)) ? $dados["numeroaditivo"] : $row->numeroaditivo;
		$row->valormensal = (array_key_exists("valormensal",$dados)) ? $dados["valormensal"] : $row->valormensal;
		$row->total = (array_key_exists("total",$dados)) ? $dados["total"] : $row->total;
		$row->siano = (array_key_exists("siano",$dados)) ? $dados["siano"] : $row->siano;
		$row->procano = (array_key_exists("procano",$dados)) ? $dados["procano"] : $row->procano;
		$row->recursos = (array_key_exists("recursos",$dados)) ? $dados["recursos"] : $row->recursos;
		$row->informacoesadicionais = (array_key_exists("informacoesadicionais",$dados)) ? $dados["informacoesadicionais"] : $row->informacoesadicionais;
		$row->flag = (array_key_exists("flag",$dados)) ? $dados["flag"] : $row->flag;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->idimagemimovel = (array_key_exists("idimagemimovel", $dados)) ? $dados["idimagemimovel"] : $row->idimagemimovel;		$row->idimagemimovel = (array_key_exists("idimagemimovel", $dados)) ? $dados["idimagemimovel"] : $row->idimagemimovel;
		$row->idimagemlaudos = (array_key_exists("idimagemlaudos", $dados)) ? $dados["idimagemlaudos"] : $row->idimagemlaudos;

		$row->save();
		
		return $row;
	}
	
}