<?php

/**
 * Define o modelo Documentacaojornadas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Documentacaojornadas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "documentacaojornadas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getDocumentacaojornadasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$documentacaojornadas = new Documentacaojornadas();
		return $documentacaojornadas->getDocumentacaojornadas($queries, $page, $maxpage);
	}
	
	public function getDocumentacaojornadas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		$group = (isset($queries['group'])) ? $queries['group'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " d1.id = $id ");
		
		
		$idprofessor = (isset($queries["idprofessor"])) ? $queries["idprofessor"] : false;
		if ($idprofessor) array_push($where, " d1.idprofessor = $idprofessor ");

		$aprovacao = (isset($queries["aprovacao"])) ? $queries["aprovacao"] : false;
		if ($aprovacao) array_push($where, " d1.aprovacao LIKE '%$aprovacao%' ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " d1.status = '$status' ");

		$situacao = (isset($queries["situacao"])) ? $queries["situacao"] : false;
		if ($situacao) array_push($where, " d2.status = '$situacao' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "d1.*, SUM(c1.pontuacao) as pontuacao, d2.status as situacao"; 
		;
		
		if ($total) $fields = "COUNT(d1.id) as total";
		
		$ordem = "ORDER BY d1.id DESC";
		if ($order) $ordem = $order; 

		$group_by = "";
		if ($group) $group_by = $group; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM documentacaojornadas d1
					LEFT JOIN escolasalunos ea1 ON ea1.id = d1.idprofessor 
					LEFT JOIN documentacaoanexos d2 ON d1.idprofessor = d2.idprofessor
                	LEFT JOIN cursos c1 ON c1.id = d2.iddocumentacao 
					WHERE d1.excluido='nao' 
						$w 
					$group_by
					$ordem	
					$limit";	
		
		//if(!$total)die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getDocumentacaojornadaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getDocumentacaojornadas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getDocumentacaojornadaByIdHelper($id, $queries = array()) {
		$rows = new Documentacaojornadas();
		return $rows->getDocumentacaojornadaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Documentacaojornadas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idprofessor = (array_key_exists("idprofessor",$dados)) ? $dados["idprofessor"] : $row->idprofessor;
		 $row->aprovacao = (array_key_exists("aprovacao",$dados)) ? $dados["aprovacao"] : $row->aprovacao;
		 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		 if (is_null($row->datacriacao)) {
					$row->datacriacao = date("Y-m-d H:i:s");
				}
								
		 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
				
				
		$row->save();
		
		return $row;
	}
	
}