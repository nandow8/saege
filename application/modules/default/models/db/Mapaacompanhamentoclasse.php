<?php

/**
 * Define o modelo Mapaacompanhamentoclasse
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Mapaacompanhamentoclasse extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "mapaacompanhamentoclasse";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public function getUltimoMapaacompanhamentoclasse($queries = array()) {
		$queries['order'] = 'ORDER BY m1.id DESC';
		$rows = $this->getMapaacompanhamentoclasse($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}

	public static function getMapaacompanhamentoclasseHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$mapaacompanhamentoclasse = new Mapaacompanhamentoclasse();
		return $mapaacompanhamentoclasse->getMapaacompanhamentoclasse($queries, $page, $maxpage);
	}
	
	public function getMapaacompanhamentoclasse($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$sequencial = (isset($queries['sequencial'])) ? (int)$queries['sequencial'] : false;
		if ($sequencial) array_push($where, " m1.sequencial LIKE '%$sequencial%' ");

		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " m1.idescola = $idescola ");

		$idserie = (isset($queries['idserie'])) ? (int)$queries['idserie'] : false;
		if ($idserie) array_push($where, " m1.idserie = $idserie ");

		$idturma = (isset($queries['idturma'])) ? (int)$queries['idturma'] : false;
		if ($idturma) array_push($where, " m1.idturma = $idturma ");

		$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " m1.status LIKE '%$status%' ");

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='titulo') $sorting[0]='m1.titulo';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "m1.*"; 
		;
		
		if ($total) $fields = "COUNT(m1.id) as total";
		
		$ordem = "ORDER BY m1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM mapaacompanhamentoclasse m1
					
					WHERE m1.excluido='nao' 
					$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getMapaacompanhamentoclasseById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getMapaacompanhamentoclasse($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getMapaacompanhamentoclasseByIdHelper($id, $queries = array()) {
		$rows = new Mapaacompanhamentoclasse();
		return $rows->getMapaacompanhamentoclasseById($id, $queries);
	}		

	public function getLastId() {  
        $strsql = "SELECT id FROM mapaacompanhamentoclasse ORDER BY id DESC LIMIT 1 ";
        
        $db = Zend_Registry::get('db');
        return $db->fetchAll($strsql);
	}
	
	public static function getgetLastIdHelper() {
		$rows = new Mapaacompanhamentoclasse();
		return $rows->getLastId();
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Mapaacompanhamentoclasse
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		
		$row->idusuariologado = (array_key_exists("idusuariologado", $dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
		$row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
		$row->horalancamento = (array_key_exists("horalancamento",$dados)) ? $dados["horalancamento"] : $row->horalancamento;
 		$row->iddiretor = (array_key_exists("iddiretor",$dados)) ? $dados["iddiretor"] : $row->iddiretor;
 		$row->idcoordenador = (array_key_exists("idcoordenador",$dados)) ? $dados["idcoordenador"] : $row->idcoordenador;
 		$row->idserie = (array_key_exists("idserie",$dados)) ? $dados["idserie"] : $row->idserie;
 		$row->idturma = (array_key_exists("idturma",$dados)) ? $dados["idturma"] : $row->idturma;
 		$row->idperiodo = (array_key_exists("idperiodo",$dados)) ? $dados["idperiodo"] : $row->idperiodo;
 		$row->periodonotas = (array_key_exists("periodonotas", $dados)) ? $dados["periodonotas"] : $row->periodonotas;

 		if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;		
				
		$row->save();
		
		return $row;
	}
	
}