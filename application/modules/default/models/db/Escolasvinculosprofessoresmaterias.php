<?php

/**
 * Define o modelo Escolasvinculosprofessoresmaterias
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasvinculosprofessoresmaterias extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasvinculosprofessoresmaterias";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolasvinculosprofessoresmateriasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasvinculosprofessoresmaterias = new Escolasvinculosprofessoresmaterias();
		return $escolasvinculosprofessoresmaterias->getEscolasvinculosprofessoresmaterias($queries, $page, $maxpage);
	}
	
	public function getEscolasvinculosprofessoresmaterias($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$die = (isset($queries['die'])) ? $queries['die'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		
		$idescolavinculo = (isset($queries["idescolavinculo"])) ? $queries["idescolavinculo"] : false;
		if ($idescolavinculo) array_push($where, " e1.idescolavinculo = $idescolavinculo ");

$idprofessor = (isset($queries["idprofessor"])) ? $queries["idprofessor"] : false;
		if ($idprofessor) array_push($where, " e1.idprofessor = $idprofessor ");

$idmateria = (isset($queries["idmateria"])) ? $queries["idmateria"] : false;
		if ($idmateria) array_push($where, " e1.idmateria = $idmateria ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " ev1.idescola = $idescola ");

$idsala = (isset($queries["idsala"])) ? $queries["idsala"] : false;
		if ($idsala) array_push($where, " ev1.idsala = $idsala ");

$idserie = (isset($queries["idserie"])) ? $queries["idserie"] : false;
		if ($idserie) array_push($where, " ev1.idserie = $idserie ");

$idturma = (isset($queries["idturma"])) ? $queries["idturma"] : false;
		if ($idturma) array_push($where, " ev1.idturma = $idturma ");

$idperiodo = (isset($queries["idperiodo"])) ? $queries["idperiodo"] : false;
		if ($idperiodo) array_push($where, " ev1.idperiodo = $idperiodo ");



$quantidadeaulas = (isset($queries["quantidadeaulas"])) ? $queries["quantidadeaulas"] : false;
		if ($quantidadeaulas) array_push($where, " e1.quantidadeaulas = '$quantidadeaulas' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " e1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*, ev1.idescola, ev1.idsala, ev1.idserie, ev1.idturma, ev1.idperiodo, es1.escola, esa1.sala, esa1.quantidade, esa1.numerosala, ess1.serie, ess1.quantidadesaulas, et1.turma, et1.cor, ep1.periodo, ep1.horarioinicio, ep1.horariofim"; 
		;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasvinculosprofessoresmaterias e1
						LEFT JOIN escolasvinculos ev1 ON ev1.id = e1.idescolavinculo 
						LEFT JOIN escolas es1 ON es1.id=ev1.idescola
						LEFT JOIN escolassalas esa1 ON esa1.id=ev1.idsala 
						LEFT JOIN escolasseries ess1 ON ess1.id = ev1.idserie
						LEFT JOIN escolasturmas et1 ON et1.id=ev1.idturma 
						LEFT JOIN escolasperiodos ep1 ON ep1.id=ev1.idperiodo 
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		//if(!$total) die($strsql);
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolasvinculosprofessormateriaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasvinculosprofessoresmaterias($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolasvinculosprofessormateriaByIdHelper($id, $queries = array()) {
		$rows = new Escolasvinculosprofessoresmaterias();
		return $rows->getEscolasvinculosprofessormateriaById($id, $queries);
	}	


	public static function getIdsEscolasvinculosprofessorByIdHelper($idprofessor, $queries = array()) {
		$rows = new Escolasvinculosprofessoresmaterias();

		if($queries['idescola']){
			$idescola = " AND f.idescola = $queries[idescola]";
		}

		$strsql = "SELECT GROUP_CONCAT(ev.idescolavinculo) as ids, f.idescola FROM `escolasvinculosprofessoresmaterias` ev 
					JOIN funcionariosgeraisescolas f ON ev.idprofessor = f.id 
					WHERE idprofessor =  $idprofessor  $idescola AND ev.status = 'Ativo' AND ev.excluido = 'nao'";	
		// die($strsql);
		$db = Zend_Registry::get('db');				
	 
		$row = $db->fetchRow($strsql);
		return $row['ids'];		
	}	
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasvinculosprofessoresmaterias
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescolavinculo = (array_key_exists("idescolavinculo",$dados)) ? $dados["idescolavinculo"] : $row->idescolavinculo;
 $row->idprofessor = (array_key_exists("idprofessor",$dados)) ? $dados["idprofessor"] : $row->idprofessor;
 $row->idmateria = (array_key_exists("idmateria",$dados)) ? $dados["idmateria"] : $row->idmateria;
 $row->quantidadeaulas = (array_key_exists("quantidadeaulas",$dados)) ? $dados["quantidadeaulas"] : $row->quantidadeaulas;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
			
		$row->save();
		
		return $row;
	}
	
}