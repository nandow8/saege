<?php

/**
 * Define o modelo Financeirolancamentosreceitas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Financeirolancamentosreceitas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "financeirolancamentosreceitas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getFinanceirolancamentosreceitasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$financeirolancamentosreceitas = new Financeirolancamentosreceitas();
		return $financeirolancamentosreceitas->getFinanceirolancamentosreceitas($queries, $page, $maxpage);
	}
	
	public function getFinanceirolancamentosreceitas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$sum = (isset($queries['sum'])) ? (int)$queries['sum'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " f1.id = $id ");
		
		$idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;
		if ($idescola) array_push($where, " f1.idescola = $idescola ");
		
		$idtipoconvenio = (isset($queries["idtipoconvenio"])) ? $queries["idtipoconvenio"] : false;
		if ($idtipoconvenio) array_push($where, " f1.idtipoconvenio = $idtipoconvenio ");
		
$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " f1.idescola = $idescola ");
 

$sequencial = (isset($queries["sequencial"])) ? $queries["sequencial"] : false;
		if ($sequencial) array_push($where, " f1.sequencial LIKE '%$sequencial%' ");

$banco = (isset($queries["banco"])) ? $queries["banco"] : false;
		if ($banco) array_push($where, " f1.banco LIKE '%$banco%' ");

$agencia = (isset($queries["agencia"])) ? $queries["agencia"] : false;
		if ($agencia) array_push($where, " f1.agencia LIKE '%$agencia%' ");

$conta = (isset($queries["conta"])) ? $queries["conta"] : false;
		if ($conta) array_push($where, " f1.conta LIKE '%$conta%' ");

$data_i = (isset($queries["data_i"])) ? $queries["data_i"] : false;
		if ($data_i) array_push($where, " f1.data >= '$data_i' ");

$data_f = (isset($queries["data_f"])) ? $queries["data_f"] : false;
		if ($data_f) array_push($where, " f1.data <= '$data_f' ");

$valor = (isset($queries["valor"])) ? $queries["valor"] : false;
		if ($valor) array_push($where, " f1.valor = $valor ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " f1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "f1.*, f2.titulo as titulo"; 
		;
		
		if ($total) $fields = "COUNT(f1.id) as total";
		if ($sum) $fields = "SUM(f1.valor) as total";
		
		$ordem = "ORDER BY f1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM financeirolancamentosreceitas f1
					LEFT JOIN financeirotiposreceitas f2 ON f2.id = f1.idtiporeceita
					
					WHERE f1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total || $sum) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getFinanceirolancamentoreceitaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getFinanceirolancamentosreceitas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getFinanceirolancamentoreceitaByIdHelper($id, $queries = array()) {
		$rows = new Financeirolancamentosreceitas();
		return $rows->getFinanceirolancamentoreceitaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Financeirolancamentosreceitas
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		 $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
		 $row->idtipoconvenio = (array_key_exists("idtipoconvenio",$dados)) ? $dados["idtipoconvenio"] : $row->idtipoconvenio;
		 $row->idbanco = (array_key_exists("idbanco",$dados)) ? $dados["idbanco"] : $row->idbanco;
		 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
 $row->sequencial = (array_key_exists("sequencial",$dados)) ? $dados["sequencial"] : $row->sequencial;
 $row->banco = (array_key_exists("banco",$dados)) ? $dados["banco"] : $row->banco;
 $row->agencia = (array_key_exists("agencia",$dados)) ? $dados["agencia"] : $row->agencia;
 $row->conta = (array_key_exists("conta",$dados)) ? $dados["conta"] : $row->conta;
 $row->data = (array_key_exists("data",$dados)) ? $dados["data"] : $row->data;
 $row->idtiporeceita = (array_key_exists("idtiporeceita",$dados)) ? $dados["idtiporeceita"] : $row->idtiporeceita;
 $row->descricaoreceita = (array_key_exists("descricaoreceita",$dados)) ? $dados["descricaoreceita"] : $row->descricaoreceita;
 $row->ndocumento = (array_key_exists("ndocumento",$dados)) ? $dados["ndocumento"] : $row->ndocumento;
 $row->valor = (array_key_exists("valor",$dados)) ? $dados["valor"] : $row->valor;
 $row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
 $row->iddocumento = (array_key_exists("iddocumento",$dados)) ? $dados["iddocumento"] : $row->iddocumento;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}