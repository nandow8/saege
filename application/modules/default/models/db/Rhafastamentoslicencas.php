<?php

/**
 * Define o modelo Rhafastamentoslicencas
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Rhafastamentoslicencas extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "rhafastamentoslicencas";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getRhafastamentoslicencasHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$rhafastamentoslicencas = new Rhafastamentoslicencas();
		return $rhafastamentoslicencas->getRhafastamentoslicencas($queries, $page, $maxpage);
	}
	
	public function getRhafastamentoslicencas($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");
                
                $idusuariologado = (isset($queries['idusuariologado'])) ? (int)$queries['idusuariologado'] : false;
                if ($idusuariologado) array_push($where, " (FIND_IN_SET($idusuariologado, g1.idsfuncionarios) >= 1) ");

                $idlocal = (isset($queries['idlocal'])) ? (int)$queries['idlocal'] : false;
		if ($idlocal) array_push($where, " r1.idlocal = $idlocal ");
		
		$idsecretaria = (isset($queries["idsecretaria"])) ? (int)$queries["idsecretaria"] : false;
		if ($idsecretaria) array_push($where, " r1.idsecretaria = $idsecretaria ");

                $origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " r1.origem LIKE '%$origem%' ");

                $idfuncionariosecretaria = (isset($queries["idfuncionariosecretaria"])) ? (int)$queries["idfuncionariosecretaria"] : false;
		if ($idfuncionariosecretaria) array_push($where, " r1.idfuncionariosecretaria = $idfuncionariosecretaria ");

                $idescola = (isset($queries["idescola"])) ? (int)$queries["idescola"] : false;
		if ($idescola) array_push($where, " r1.idescola = $idescola ");

                $idfuncionarioescola = (isset($queries["idfuncionarioescola"])) ? (int)$queries["idfuncionarioescola"] : false;
		if ($idfuncionarioescola) array_push($where, " r1.idfuncionarioescola = $idfuncionarioescola ");

                $dias = (isset($queries["dias"])) ? $queries["dias"] : false;
		if ($dias) array_push($where, " r1.dias = '$dias' ");

                $datainicio_i = (isset($queries["datainicio_i"])) ? $queries["datainicio_i"] : false;
		if ($datainicio_i) array_push($where, " r1.datainicio >= '$datainicio_i' ");

                $datainicio_f = (isset($queries["datainicio_f"])) ? $queries["datainicio_f"] : false;
		if ($datainicio_f) array_push($where, " r1.datainicio <= '$datainicio_f' ");

                $datafim_i = (isset($queries["datafim_i"])) ? $queries["datafim_i"] : false;
		if ($datafim_i) array_push($where, " r1.datafim >= '$datafim_i' ");

                $datafim_f = (isset($queries["datafim_f"])) ? $queries["datafim_f"] : false;
		if ($datafim_f) array_push($where, " r1.datafim <= '$datafim_f' ");

                $descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " r1.descricoes = '$descricoes' ");

                $aprovacao = (isset($queries["aprovacao"])) ? $queries["aprovacao"] : false;
		if ($aprovacao) array_push($where, " r1.aprovacao LIKE '%$aprovacao%' ");

                $confirmacaoaprovacao = (isset($queries["confirmacaoaprovacao"])) ? $queries["confirmacaoaprovacao"] : false;
		if ($confirmacaoaprovacao) array_push($where, " r1.confirmacaoaprovacao LIKE '%$confirmacaoaprovacao%' ");

                $status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " r1.status LIKE '%$status%' ");

                $idfuncionarioafastadolicenca = (isset($queries["idfuncionarioafastadolicenca"])) ? (int)$queries["idfuncionarioafastadolicenca"] : false;
		if ($idfuncionarioafastadolicenca) array_push($where, " r1.idfuncionarioafastadolicenca = '$idfuncionarioafastadolicenca' ");               

                $progresso = (isset($queries["progresso"])) ? $queries["progresso"] : false;
		if ($progresso) array_push($where, " r1.progresso LIKE '%$progresso%' ");

                $observacao = (isset($queries["observacao"])) ? $queries["observacao"] : false;
		if ($observacao) array_push($where, " r1.observacao LIKE '%$observacao%' ");
                
		$idsfuncionariossel = (isset($queries["idsfuncionariossel"])) ? $queries["idsfuncionariossel"] : false;
		if ($idsfuncionariossel) array_push($where, " r1.idfuncionarioafastadolicenca IN ($idsfuncionariossel) ");                

		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*"; 
		;
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM rhafastamentoslicencas r1
                                        inner join funcionariosgeraisescolas g1 on (r1.idfuncionarioafastadolicenca = g1.id)

					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
                
                // echo '<pre>','$strsql', $strsql,'</pre>';

		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getRhafastamentoslicencaById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getRhafastamentoslicencas($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getRhafastamentoslicencaByIdHelper($id, $queries = array()) {
		$rows = new Rhafastamentoslicencas();
		return $rows->getRhafastamentoslicencaById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Rhafastamentoslicencas
     */
	public function save($dados) {
		$novoRegistro = true;
		
               
                
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
                $row->idsecretaria = (array_key_exists("idsecretaria",$dados)) ? $dados["idsecretaria"] : $row->idsecretaria;

                $row->idarquivo = (array_key_exists("idarquivo",$dados)) ? $dados["idarquivo"] : $row->idarquivo;
                $row->idarquivoaprovacao = (array_key_exists("idarquivoaprovacao",$dados)) ? $dados["idarquivoaprovacao"] : $row->idarquivoaprovacao;

                $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
                $row->idfuncionariosecretaria = (array_key_exists("idfuncionariosecretaria",$dados)) ? $dados["idfuncionariosecretaria"] : $row->idfuncionariosecretaria;
                $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
                $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
                $row->idfuncionarioescola = (array_key_exists("idfuncionarioescola",$dados)) ? $dados["idfuncionarioescola"] : $row->idfuncionarioescola;
                $row->dias = (array_key_exists("dias",$dados)) ? $dados["dias"] : $row->dias;
                $row->motivo = (array_key_exists("motivo",$dados)) ? $dados["motivo"] : $row->motivo;
                $row->datainicio = (array_key_exists("datainicio",$dados)) ? $dados["datainicio"] : $row->datainicio;
                $row->datafim = (array_key_exists("datafim",$dados)) ? $dados["datafim"] : $row->datafim;
                $row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
                $row->aprovacao = (array_key_exists("aprovacao",$dados)) ? $dados["aprovacao"] : $row->aprovacao;
                $row->confirmacaoaprovacao = (array_key_exists("confirmacaoaprovacao",$dados)) ? $dados["confirmacaoaprovacao"] : $row->confirmacaoaprovacao;
                if (is_null($row->datacriacao)) {
                                       $row->datacriacao = date("Y-m-d H:i:s");
                               }

                $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
                $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
                $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
                $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
                $row->idfuncionarioafastadolicenca  = (array_key_exists("idfuncionarioafastadolicenca",$dados)) ? $dados["idfuncionarioafastadolicenca"] : $row->idfuncionarioafastadolicenca;
		$row->progresso = (array_key_exists("progresso",$dados)) ? $dados["progresso"] : $row->progresso;
                $row->observacao = (array_key_exists("observacao",$dados)) ? $dados["observacao"] : $row->observacao;
                
		$row->save();
		
		return $row;
	}
	
}