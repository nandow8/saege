<?php

class Escolastiposensinosgdae extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolastiposensinosgdae";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";

	public static function getTiposensinosgdaeHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$produtos = new Escolastiposensinosgdae();
		return $produtos->getTiposensinosgdae($queries, $page, $maxpage);
	}
	
	public function getTiposensinosgdae($queries = array(), $page = 0, $maxpage = 0) { 
		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		$idsecretaria = (isset($queries['idsecretaria'])) ? $queries['idsecretaria'] : false;
		$origem = (isset($queries['origem'])) ? $queries['origem'] : false;
		$chave = (isset($queries['chave'])) ? $queries['chave'] : false;
		$status = (isset($queries['status'])) ? $queries['status'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;
		
		$where = array();
		
		if ($id) array_push($where, " gda.id=$id ");
		if ($idsecretaria) array_push($where, " gda.idsecretaria=$idsecretaria ");	
		if ($chave) array_push($where, " ((gda.tipo LIKE '%$chave%') OR (gda.descricoes LIKE '%$chave%')) ");
		if ($status) array_push($where, " gda.status='$status' ");
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "gda.* ";
		if ($total) $fields = "COUNT(gda.id) as total";
		
		
		$ordem = "ORDER BY gda.id";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM gdae_identificadoresescolas gda
					WHERE gda.excluido='nao'
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);		
	}	
	
	public function getTipoensinogdaeById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getTiposensinosgdae($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getTipoensinoByIdHelper($id, $queries = array()) {
		$rows = new Escolastiposensinosgdae();
		return $rows->getTipoensinogdaeById($id, $queries);
	}		
	
	public static function getTipoensinoBySerieGrauHelper($queries = array()){
		$rows = new Escolastiposensinosgdae();
		return $rows->getTipoensinoBySerieGrau($queries);
	}

	public static function getTipoensinoBySerieGrau($queries = array()){
		
		$grau = (isset($queries['grau'])) ? (int)$queries['grau'] : false;
		$serie = (isset($queries['serie'])) ? (int)$queries['serie'] : false;

		$fields = 'gda.*';

		$where = array();
		if($grau) array_push($where, " gda.serie=$serie");
		if($serie) array_push($where, " gda.idgdae=$grau");

		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$strsql = "SELECT $fields 
			FROM gdae_identificadoresescolassubtipos gda
			WHERE gda.excluido='nao'
				$w";

		$db = Zend_Registry::get('db');		
		return $db->fetchAll($strsql);		
	}

	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Tiposensinos
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) { 
			$row = $this->createRow();
			$row->datacriacao = date('Y-m-d G:i:s'); 		
		}else {
			$novoRegistro = false;
		} 

		$row->tipo = (array_key_exists('tipo',$dados)) ? $dados['tipo'] : $row->tipo;
		$row->descricoes = (array_key_exists('descricoes',$dados)) ? $dados['descricoes'] : $row->descricoes;			
		$row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
		$row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;
		$row->logusuario = (array_key_exists('logusuario',$dados)) ? $dados['logusuario'] : $row->logusuario;
		$row->logdata = (array_key_exists('logdata',$dados)) ? $dados['logdata'] : $row->logdata;
		
		if ((int)$row->idsecretaria==0) {
			$row->idsecretaria = Usuarios::getSecretariaAtiva( Usuarios::getUsuario('id'), 'id' );
		}
		
		$row->save();

		return $row;
	}
	
	
}