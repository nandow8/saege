<?php

/**
 * Define o modelo Recepcaogabinetes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Recepcaogabinetes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "recepcaogabinetes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getRecepcaogabinetesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$recepcaogabinetes = new Recepcaogabinetes();
		return $recepcaogabinetes->getRecepcaogabinetes($queries, $page, $maxpage);
	}
	
	public function getRecepcaogabinetes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " r1.id = $id ");
		
		
		$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " r1.datalancamento >= '$datalancamento_i' ");

$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " r1.datalancamento <= '$datalancamento_f' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " r1.titulo LIKE '%$titulo%' ");

$numeroprotocolo = (isset($queries["numeroprotocolo"])) ? $queries["numeroprotocolo"] : false;
		if ($numeroprotocolo) array_push($where, " r1.numeroprotocolo LIKE '%$numeroprotocolo%' ");

$interessado = (isset($queries["interessado"])) ? $queries["interessado"] : false;
		if ($interessado) array_push($where, " r1.interessado LIKE '%$interessado%' ");

$numof = (isset($queries["numof"])) ? $queries["numof"] : false;
		if ($numof) array_push($where, " r1.numof LIKE '%$numof%' ");

$dataof_i = (isset($queries["dataof_i"])) ? $queries["dataof_i"] : false;
		if ($dataof_i) array_push($where, " r1.dataof >= '$dataof_i' ");

$dataof_f = (isset($queries["dataof_f"])) ? $queries["dataof_f"] : false;
		if ($dataof_f) array_push($where, " r1.dataof <= '$dataof_f' ");

$proc = (isset($queries["proc"])) ? $queries["proc"] : false;
		if ($proc) array_push($where, " r1.proc LIKE '%$proc%' ");

$procdata_i = (isset($queries["procdata_i"])) ? $queries["procdata_i"] : false;
		if ($procdata_i) array_push($where, " r1.procdata >= '$procdata_i' ");

$procdata_f = (isset($queries["procdata_f"])) ? $queries["procdata_f"] : false;
		if ($procdata_f) array_push($where, " r1.procdata <= '$procdata_f' ");

$assunto = (isset($queries["assunto"])) ? $queries["assunto"] : false;
		if ($assunto) array_push($where, " r1.assunto LIKE '%$assunto%' ");

$despacho = (isset($queries["despacho"])) ? $queries["despacho"] : false;
		if ($despacho) array_push($where, " r1.despacho LIKE '%$despacho%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " r1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "r1.*"; 
		;
		
		if ($total) $fields = "COUNT(r1.id) as total";
		
		$ordem = "ORDER BY r1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM recepcaogabinetes r1
					
					WHERE r1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getRecepcaogabineteById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getRecepcaogabinetes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getRecepcaogabineteByIdHelper($id, $queries = array()) {
		$rows = new Recepcaogabinetes();
		return $rows->getRecepcaogabineteById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Recepcaogabinetes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->numeroprotocolo = (array_key_exists("numeroprotocolo",$dados)) ? $dados["numeroprotocolo"] : $row->numeroprotocolo;
 $row->interessado = (array_key_exists("interessado",$dados)) ? $dados["interessado"] : $row->interessado;
 $row->numof = (array_key_exists("numof",$dados)) ? $dados["numof"] : $row->numof;
 $row->dataof = (array_key_exists("dataof",$dados)) ? $dados["dataof"] : $row->dataof;
 $row->proc = (array_key_exists("proc",$dados)) ? $dados["proc"] : $row->proc;
 $row->procdata = (array_key_exists("procdata",$dados)) ? $dados["procdata"] : $row->procdata;
 $row->assunto = (array_key_exists("assunto",$dados)) ? $dados["assunto"] : $row->assunto;
 $row->despacho = (array_key_exists("despacho",$dados)) ? $dados["despacho"] : $row->despacho;
 $row->iddocumento = (array_key_exists("iddocumento",$dados)) ? $dados["iddocumento"] : $row->iddocumento;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
}