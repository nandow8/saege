<?php

/**
 * Define o modelo Escolasedificacoes
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Escolasedificacoes extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "escolasedificacoes";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getEscolasedificacoesHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$escolasedificacoes = new Escolasedificacoes();
		return $escolasedificacoes->getEscolasedificacoes($queries, $page, $maxpage);
	}
	
	public function getEscolasedificacoes($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " e1.id = $id ");
		
		$idlicenca = (isset($queries['idlicenca'])) ? (int)$queries['idlicenca'] : false;
		if ($idlicenca) array_push($where, " e1.idlicenca = $idlicenca ");
		
		$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " e1.idescola = $idescola ");

		$idtipoedificacao = (isset($queries["idtipoedificacao"])) ? $queries["idtipoedificacao"] : false;
		if ($idtipoedificacao) array_push($where, " e1.idtipoedificacao = $idtipoedificacao ");

		$descricoes = (isset($queries["descricoes"])) ? $queries["descricoes"] : false;
		if ($descricoes) array_push($where, " e1.descricoes = '$descricoes' ");

		$dataconstrucao_i = (isset($queries["dataconstrucao_i"])) ? $queries["dataconstrucao_i"] : false;
		if ($dataconstrucao_i) array_push($where, " e1.dataconstrucao >= '$dataconstrucao_i' ");

		$dataconstrucao_f = (isset($queries["dataconstrucao_f"])) ? $queries["dataconstrucao_f"] : false;
		if ($dataconstrucao_f) array_push($where, " e1.dataconstrucao <= '$dataconstrucao_f' ");

		$localizacao = (isset($queries["localizacao"])) ? $queries["localizacao"] : false;
		if ($localizacao) array_push($where, " e1.localizacao = '$localizacao' ");

		$referencia = (isset($queries["referencia"])) ? $queries["referencia"] : false;
		if ($referencia) array_push($where, " e1.referencia = '$referencia' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "e1.*"; 
$fields .= ", e_idendereco.cep as e_idenderecocep, e_idendereco.endereco as e_idenderecoendereco, e_idendereco.numero as e_idendereconumero, e_idendereco.complemento as e_idenderecocomplemento, e_idendereco.bairro as e_idenderecobairro, e_idendereco.idestado as e_idenderecoidestado,e_idendereco.cidade as e_idenderecocidade,es_idendereco.uf as e_idenderecouf,es_idendereco.uf as e_idenderecoestado "; ;
		
		if ($total) $fields = "COUNT(e1.id) as total";
		
		
		$ordem = "ORDER BY e1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM escolasedificacoes e1
											LEFT JOIN enderecos e_idendereco ON e_idendereco.id=e1.idendereco 
						LEFT JOIN estados es_idendereco ON es_idendereco.id=e_idendereco.idestado	
					WHERE e1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getEscolaedificacaoById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getEscolasedificacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getEscolaedificacaoByIdHelper($id, $queries = array()) {
		$rows = new Escolasedificacoes();
		return $rows->getEscolaedificacaoById($id, $queries);
	}		
	
	public function getEdificacaoByIdEscola($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['idescola'] = $id;
		$rows = $this->getEscolasedificacoes($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Escolasedificacoes
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$idlicenca = (!isset($dados['idlicenca'])) ? 0 : (int)$dados['idlicenca'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		

		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		$row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
		$row->idtipoedificacao = (array_key_exists("idtipoedificacao",$dados)) ? $dados["idtipoedificacao"] : $row->idtipoedificacao;
		$row->idendereco = (array_key_exists("idendereco",$dados)) ? $dados["idendereco"] : $row->idendereco;
		$row->descricoes = (array_key_exists("descricoes",$dados)) ? $dados["descricoes"] : $row->descricoes;
		$row->dataconstrucao = (array_key_exists("dataconstrucao",$dados)) ? $dados["dataconstrucao"] : $row->dataconstrucao;
		$row->localizacao = (array_key_exists("localizacao",$dados)) ? $dados["localizacao"] : $row->localizacao;
		$row->referencia = (array_key_exists("referencia",$dados)) ? $dados["referencia"] : $row->referencia;
		$row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
		$row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
		$row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		$row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
		
				
		$row->save();
		
		return $row;
	}
	
	
}