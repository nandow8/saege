<?php

/**
 * Define o modelo Nutricaomerendascontrolecardapios
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Nutricaomerendascontrolecardapios extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "nutricaomerendascontrolecardapios";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getNutricaomerendascontrolecardapiosHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$nutricaomerendascontrolecardapios = new Nutricaomerendascontrolecardapios();
		return $nutricaomerendascontrolecardapios->getNutricaomerendascontrolecardapios($queries, $page, $maxpage);
	}
	
	public function getNutricaomerendascontrolecardapios($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " n1.id = $id ");
		
		
		$datalancamento_i = (isset($queries["datalancamento_i"])) ? $queries["datalancamento_i"] : false;
		if ($datalancamento_i) array_push($where, " n1.datalancamento >= '$datalancamento_i' ");

$datalancamento_f = (isset($queries["datalancamento_f"])) ? $queries["datalancamento_f"] : false;
		if ($datalancamento_f) array_push($where, " n1.datalancamento <= '$datalancamento_f' ");

$idescola = (isset($queries["idescola"])) ? $queries["idescola"] : false;
		if ($idescola) array_push($where, " n1.idescola = $idescola ");

$idlocal = (isset($queries["idlocal"])) ? $queries["idlocal"] : false;
		if ($idlocal) array_push($where, " n1.idlocal = '$idlocal' ");

$titulo = (isset($queries["titulo"])) ? $queries["titulo"] : false;
		if ($titulo) array_push($where, " n1.titulo LIKE '%$titulo%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " n1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "n1.*"; 
		;
		
		if ($total) $fields = "COUNT(n1.id) as total";
		
		$ordem = "ORDER BY n1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM nutricaomerendascontrolecardapios n1
					
					WHERE n1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getNutricaomerendacontrolecardapioById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getNutricaomerendascontrolecardapios($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getNutricaomerendacontrolecardapioByIdHelper($id, $queries = array()) {
		$rows = new Nutricaomerendascontrolecardapios();
		return $rows->getNutricaomerendacontrolecardapioById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Nutricaomerendascontrolecardapios
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->datalancamento = (array_key_exists("datalancamento",$dados)) ? $dados["datalancamento"] : $row->datalancamento;
 $row->idusuariologado = (array_key_exists("idusuariologado",$dados)) ? $dados["idusuariologado"] : $row->idusuariologado;
 $row->idescola = (array_key_exists("idescola",$dados)) ? $dados["idescola"] : $row->idescola;
 $row->idlocal = (array_key_exists("idlocal",$dados)) ? $dados["idlocal"] : $row->idlocal;
 $row->idtipoensino = (array_key_exists("idtipoensino",$dados)) ? $dados["idtipoensino"] : $row->idtipoensino;
 $row->tipoensino = (array_key_exists("tipoensino",$dados)) ? $dados["tipoensino"] : $row->tipoensino;
 $row->nutricionistaresponsavel = (array_key_exists("nutricionistaresponsavel",$dados)) ? $dados["nutricionistaresponsavel"] : $row->nutricionistaresponsavel;
 $row->cnr = (array_key_exists("cnr",$dados)) ? $dados["cnr"] : $row->cnr;
 $row->datas = (array_key_exists("datas",$dados)) ? $dados["datas"] : $row->datas;
 $row->titulo = (array_key_exists("titulo",$dados)) ? $dados["titulo"] : $row->titulo;
 $row->refeicao = (array_key_exists("refeicao",$dados)) ? $dados["refeicao"] : $row->refeicao;
 $row->sobremesa = (array_key_exists("sobremesa",$dados)) ? $dados["sobremesa"] : $row->sobremesa;
 $row->lanchemanha = (array_key_exists("lanchemanha",$dados)) ? $dados["lanchemanha"] : $row->lanchemanha;
 $row->lanchetarde = (array_key_exists("lanchetarde",$dados)) ? $dados["lanchetarde"] : $row->lanchetarde;
 $row->jantar = (array_key_exists("jantar",$dados)) ? $dados["jantar"] : $row->jantar;
 $row->mamadeira = (array_key_exists("mamadeira",$dados)) ? $dados["mamadeira"] : $row->mamadeira;
 $row->desjejum = (array_key_exists("desjejum",$dados)) ? $dados["desjejum"] : $row->desjejum;
 $row->colacao = (array_key_exists("colacao",$dados)) ? $dados["colacao"] : $row->colacao;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
			
		$row->save();
		
		return $row;
	}
	
}