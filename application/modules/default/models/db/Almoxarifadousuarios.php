<?php

class Almoxarifadousuarios extends Zend_Db_Table_Abstract {

    /**
     * Define o nome da tabela
     * @var string
     */
    protected $_name = "almoxarifadousuarios";
    
    /**
     * Define a chave primaria
     * @var integer
     */
    protected $_primary = "id";

    public function getAlmoxarifadoUsuarios($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $idlocal = (isset($queries['idlocal'])) ? (int)$queries['idlocal'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " au.id = $id ");
        if ($idlocal) array_push($where, " au.idlocal = $idlocal ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "au.*, fge.nome, ap.perfil";

        $ordem = "ORDER BY au.id";
        if ($order) $ordem = $order;

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(au.id) as total FROM almoxarifadousuarios au INNER JOIN funcionariosgeraisescolas fge ON (au.idfuncionario = fge.id) LEFT JOIN almoxarifadoperfis ap ON (au.idperfilalmoxarifado = ap.id) WHERE au.excluido = 'nao' $w";
        } else {
            $strsql = "SELECT $fields FROM almoxarifadousuarios au INNER JOIN funcionariosgeraisescolas fge ON (au.idfuncionario = fge.id) LEFT JOIN almoxarifadoperfis ap ON (au.idperfilalmoxarifado = ap.id) WHERE au.excluido = 'nao' $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        return $db->fetchAll($strsql);
    }

    public function getAlmoxarifadoUsuariosById($id, $queries = array()) {
        if ($id==0) return false;
        
        $queries['id'] = $id;
        $rows = $this->getAlmoxarifadoUsuarios($queries, 0, 0);
        
        if (sizeof($rows)==0) return false;
        return $rows[0];
    }

    public function getUsuarios($queries = array(), $page = 0, $maxpage = 0) {

        $id = (isset($queries['id'])) ? (int)$queries['id'] : false;
        $idlocal = (isset($queries['idlocal'])) ? (int)$queries['idlocal'] : false;
        $idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($id) array_push($where, " id = $id ");
        if ($idlocal) array_push($where, " idlocal = $idlocal ");
        if ($idescola) array_push($where, " idescola = $idescola ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "*";

        $ordem = "ORDER BY id";
        if ($order) $ordem = $order;

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(id) as total FROM view_usuariosfuncionarios WHERE 1 $w";
        } else {
            $strsql = "SELECT $fields FROM view_usuariosfuncionarios WHERE 1 $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        return $db->fetchAll($strsql);
    }

    public function getFuncionarios($queries = array(), $page = 0, $maxpage = 0) {

        $idlocal = (isset($queries['idlocal'])) ? (int)$queries['idlocal'] : false;
        $idescola = (isset($queries['idescola'])) ? (int)$queries['idescola'] : false;

        $total = (isset($queries['total'])) ? (int)$queries['total'] : false;
        $order = (isset($queries['order'])) ? $queries['order'] : false;
        
        $where = array();

        if ($idlocal) array_push($where, " idperfil = $idlocal "); //a tabela usuariosperfis é tratada como local
        if ($idescola) array_push($where, " idescola = $idescola ");
        
        $w = "";
        foreach ($where as $k=>$v) {
            if ($k>0) $w .= " AND ";
            $w .= $v;
        }
        if ($w!="") $w = "AND ($w)";

        $fields = "*";

        $ordem = "ORDER BY id";
        if ($order) $ordem = $order;

        $limit = "";
        if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";
        
        if ($total) {
            $strsql = "SELECT count(id) as total FROM view_usuariosfuncionarios WHERE 1 $w";
        } else {
            $strsql = "SELECT $fields FROM view_usuariosfuncionarios WHERE 1 $w $ordem $limit";
        }

        if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;

        $db = Zend_Registry::get('db'); 

        if ($total) {
            $row = $db->fetchRow($strsql);
            return $row['total'];
        }
        
        return $db->fetchAll($strsql);
    }

    /**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Almoxarifadousuarios
     */
    public function save($dados) {
        $novoRegistro = true;

        $id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
        $row = $this->fetchRow("id=$id AND excluido='nao'");

        if (!$row) {
            $row = $this->createRow();
        }

        $row->idfuncionario = (array_key_exists('idfuncionario',$dados)) ? $dados['idfuncionario'] : $row->idfuncionario;

        $row->idestoque = (array_key_exists('idestoque',$dados)) ? $dados['idestoque'] : $row->idestoque;

        $row->idperfilalmoxarifado = (array_key_exists('idperfilalmoxarifado',$dados)) ? $dados['idperfilalmoxarifado'] : $row->idperfilalmoxarifado;

        $row->status = (array_key_exists('status',$dados)) ? $dados['status'] : $row->status;
        $row->excluido = (array_key_exists('excluido',$dados)) ? $dados['excluido'] : $row->excluido;

        $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
        $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;

        if (!$row->datacriacao) {
            $row->datacriacao = date('Y-m-d G:i:s');
        }

        $row->save();

        return $row;
    }
}