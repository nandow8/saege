<?php

/**
 * Define o modelo Juridicosorigens
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Db_Table_Abstract
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Juridicosorigens extends Zend_Db_Table_Abstract {
	
	/**
     * Define o nome da tabela
     * @var string
     */
	protected $_name = "juridicosorigens";
	
	/**
     * Define a chave primaria
     * @var integer
     */
	protected $_primary = "id";
	
	public static function getJuridicosorigensHelper($queries = array(), $page = 0, $maxpage = 0) { 
		$juridicosorigens = new Juridicosorigens();
		return $juridicosorigens->getJuridicosorigens($queries, $page, $maxpage);
	}
	
	public function getJuridicosorigens($queries = array(), $page = 0, $maxpage = 0) { 
		$where = array();
		
		$sorting = (isset($queries['sorting'])) ? $queries['sorting'] : false;
		$total = (isset($queries['total'])) ? (int)$queries['total'] : false;
		$order = (isset($queries['order'])) ? $queries['order'] : false;

		$id = (isset($queries['id'])) ? (int)$queries['id'] : false;
		if ($id) array_push($where, " j1.id = $id ");
		
		
		$origem = (isset($queries["origem"])) ? $queries["origem"] : false;
		if ($origem) array_push($where, " j1.origem LIKE '%$origem%' ");

$turma = (isset($queries["turma"])) ? $queries["turma"] : false;
		if ($turma) array_push($where, " j1.turma LIKE '%$turma%' ");

$vara = (isset($queries["vara"])) ? $queries["vara"] : false;
		if ($vara) array_push($where, " j1.vara LIKE '%$vara%' ");

$status = (isset($queries["status"])) ? $queries["status"] : false;
		if ($status) array_push($where, " j1.status LIKE '%$status%' ");



		if ($sorting) {
			$sorting = explode('_', $sorting);
			if (sizeof($sorting)==2) {
				
				if ($sorting[0]=='nome') $sorting[0]='l1.nome';
				
				$order = "ORDER BY " . $sorting[0] . " " . $sorting['1'];
			}
		}		
		
		$w = "";
		foreach ($where as $k=>$v) {
			if ($k>0) $w .= " AND ";
			$w .= $v;
		}
		if ($w!="") $w = "AND ($w)";

		$fields = "j1.*"; 
		;
		
		if ($total) $fields = "COUNT(j1.id) as total";
		
		$ordem = "ORDER BY j1.id DESC";
		if ($order) $ordem = $order; 
		
		$limit = "";
		if ($maxpage>0) $limit = "LIMIT ".($page*$maxpage).", $maxpage";

		$strsql = "SELECT $fields 
					FROM juridicosorigens j1
					
					WHERE j1.excluido='nao' 
						$w 
					$ordem	
					$limit";	
		
		if ((isset($queries['return_sql'])) && ($queries['return_sql'])) return $strsql;						
		$db = Zend_Registry::get('db');				
		if ($total) {
			$row = $db->fetchRow($strsql);
			return $row['total'];
		}	
		
		return $db->fetchAll($strsql);			
	}	
	
	public function getJuridicosorigemById($id, $queries = array()) {
		if ($id==0) return false;
		
		$queries['id'] = $id;
		$rows = $this->getJuridicosorigens($queries, 0, 0);
		
		if (sizeof($rows)==0) return false;
		return $rows[0];
	}
	
	public static function getJuridicosorigemByIdHelper($id, $queries = array()) {
		$rows = new Juridicosorigens();
		return $rows->getJuridicosorigemById($id, $queries);
	}		
	
	
	/**
     * Salva o dados (INSERT OU UPDATE)
     * @param array dados
     * @return Juridicosorigens
     */
	public function save($dados) {
		$novoRegistro = true;
		
		$id = (!isset($dados['id'])) ? 0 : (int)$dados['id'];
		$row = $this->fetchRow("id=$id AND excluido='nao'");
		
		if (!$row) $row = $this->createRow();
		else {
			$novoRegistro = false;
		} 
		
		 $row->origem = (array_key_exists("origem",$dados)) ? $dados["origem"] : $row->origem;
 $row->turma = (array_key_exists("turma",$dados)) ? $dados["turma"] : $row->turma;
 $row->vara = (array_key_exists("vara",$dados)) ? $dados["vara"] : $row->vara;
 $row->status = (array_key_exists("status",$dados)) ? $dados["status"] : $row->status;
 if (is_null($row->datacriacao)) {
			$row->datacriacao = date("Y-m-d H:i:s");
		}
						
 $row->excluido = (array_key_exists("excluido",$dados)) ? $dados["excluido"] : $row->excluido;
 $row->logusuario = (array_key_exists("logusuario",$dados)) ? $dados["logusuario"] : $row->logusuario;
 $row->logdata = (array_key_exists("logdata",$dados)) ? $dados["logdata"] : $row->logdata;
		
				
		$row->save();
		
		return $row;
	}
	
}