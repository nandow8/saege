<?php 

require_once BASE_PATH . '/library/tcpdf6/tcpdf.php';

//define('MY_PATH', 'http://sagg.com.br/atendimento');
define('MY_PATH', 'http://mnsolucoes.com');

class RelatorioPDF {
	
	private $_action;
	private $_rows;
	private $_titles = array();
	private $_cabecalho = array();
	
	
	public function __construct($action, $rows) {
		$this->_action = $action;
		$this->_rows = $rows;
		
		$this->view = $action->view;
		
		if (sizeof($rows)==0) {
			throw new \Exception('Nenhum registro foi encontrado!');
		}
	}	
	
	public static function renderCells($fields, $action) {
		$s = '';
		$i = 0;
		foreach ($fields as $k=>$v) {
			if ($i>0) $s .= '<br />';
			$s .= $k . ': ' . $action->view->formatTextoBranco($v);
			$i++;
		}
		return $s;
	}
	
	public function generate($fname = "file.pdf", $titlesName = array()) {
		$this->getTitlesPerRows($titlesName);
	
		// create new PDF document
		$pdf = new RelPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, array(210.0, 297.0), true, 'UTF-8', false);
		$this->view->pdf = $pdf;
		$this->view->titlesName = $titlesName;
		$this->view->titles = $this->getTitles();
		$this->view->cabecalho = $this->getCabecalho();
		
		$pdf->setPageOrientation('l');
		
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Eleitorial');
		$pdf->SetTitle('Relatório');
		$pdf->SetSubject('Sistema');
		
		$this->view->setScriptPath(APPLICATION_PATH  . '/modules/default/views/scripts/');
		
			
		$header= $this->view->render('relatorios/pdf/header.phtml');
		$pdf->setHeaderHtml($header);
		$pdf->setPrintHeader(true);
		$pdf->SetHeaderMargin(5);
		
		
		$footer= $this->view->render('relatorios/pdf/footer.phtml');
		$pdf->setFooterHtml($footer);
		$pdf->setPrintFooter(true);
		$pdf->SetFooterMargin(0);
		
		
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		
		//set margins
		$pdf->SetMargins(10, 35, 10);
		
		
		//set auto page breaks
		//$pdf->SetAutoPageBreak(FALSE);
		$pdf->SetAutoPageBreak(TRUE, 25);
		
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		
		// set font
		$pdf->SetFont('verdana', '', 6, 'false');
		
		// add a page
		$pdf->AddPage();
		
		$html = $this->view->render('relatorios/pdf/body.phtml');
		$pdf->writeHTML($html, true, 0, true, 0);
			
		$filename = $fname;
			
		$pdf->Output($filename, 'D');
		
		die();		
		
	}
	
	private function getTitlesPerRows($titlesName) {
		$_titles = array();
		
		$c = 64;
		foreach ($titlesName as $k=>$v) {
			$c++;
			$_titles[chr($c)] = $k;
		}
		
		$this->setTitles($_titles);
	}
	
	public function getAction() {
		return $this->_action;
	}
	public function setAction($_action) {
		$this->_action = $_action;
		return $this;
	}
	public function getRows() {
		return $this->_rows;
	}
	public function setRows($_rows) {
		$this->_rows = $_rows;
		return $this;
	}
	public function getTitles() {
		return $this->_titles;
	}
	public function setTitles($_titles) {
		$this->_titles = $_titles;
		return $this;
	}
	public function getCabecalho() {
		return $this->_cabecalho;
	}
	public function setCabecalho($_cabecalho) {
		$this->_cabecalho = $_cabecalho;
		return $this;
	}
	
	

	
	
	
}