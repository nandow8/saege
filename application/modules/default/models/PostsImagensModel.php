<?php

/**
 * Controle da classe de PostsImagensModel
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2014 MN Soluções (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class PostsImagensModel {
	
	/**
	 * Controller
	 * @var Zend_Controller_Action
	 */
	protected $_action = null;
	
	/**
	 * Método construtor
	 * @param Zend_Controller_Action $action
	 */
	public function __construct($action) {
		$this->_action = $action;
	}

	/**
	 * Valida e grava os dados do formulário
	 */
	public function getPost() {
		if (!isset($this->_action->view->post_var)) $this->_action->view->post_var = $_POST; 
		else $this->_action->view->post_var = array_merge($this->_action->view->post_var, $_POST);
		$erros = array();
		$dados = array();
		
		$id = (int)trim($this->_action->getRequest()->getPost("id"));
		$dados['id'] = $id;
		
		$idpost = (int)trim($this->_action->getRequest()->getPost("idpost"));
		if ($idpost=="") array_push($erros, "Selecione uma <b>Galeria</b>");
		
		$legenda = trim($this->_action->getRequest()->getPost("legenda"));
		if ($legenda=="") array_push($erros, "Preencha a <b>Legenda</b>");
		$dados['legenda'] = $legenda;
				
		$idimagem = false;
		$imagens = new Imagens();
		try {
			$idimagem = $imagens->getImagemFromForm('imagem');
		} catch (Exception $e) {
			$idimagem = false;
			array_push($erros,$e->getMessage());
		}
		
		$excluir_imagem = trim($this->_action->getRequest()->getPost("excluir_imagem"));
		if ($excluir_imagem=='excluir') $idimagem = -1;
		$dados['idimagem'] = $idimagem;
				
		$status = trim($this->_action->getRequest()->getPost("status"));
		if ($status=="") array_push($erros, "Selecione um <b>Status</b>");
		$dados['status'] = $status;
		
		if (sizeof($erros)>0) return $erros; 
		
		if ($idpost) {
			$dados['idpost'] = $idpost;			
		} else {
			$dados['idpost'] = $this->_action->_idpost;
		}
		$dados['excluido'] = 'nao';
	
		$posts = new PostsImagens();
		$posts->save($dados);
		
		return "";   		
	}
		
}