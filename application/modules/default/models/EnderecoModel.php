<?php

class EnderecoModel {
	
	/**
	 * Controller
	 * @var Zend_Controller_Action
	 */
	protected $_action = null;
	
	/**
	 * Método construtor
	 * @param Zend_Controller_Action $action
	 */
	public function __construct($action) {
		$this->_action = $action;
	}
	
	/**
	 * Redireciona a página
	 * @param unknown_type $url
	 * @param array $options
	 */
    protected function _redirect($url, array $options = array())
    {
    	$helper = new Zend_Controller_Action_HelperBroker($this->_action);		
        $helper->redirector->gotoUrl($url, $options);
    }	
    
	/**
	 * Valida e grava os dados do formulário
	 * @return Enderecos
	 */    
    public function getPost($idendereco = 0, $prefix = '') {
		$cep = trim($this->_action->getRequest()->getPost($prefix."cep"));
		$endereco = trim($this->_action->getRequest()->getPost($prefix."endereco"));
		$numero = trim($this->_action->getRequest()->getPost($prefix."numero"));
		$complemento = trim($this->_action->getRequest()->getPost($prefix."complemento"));
		$bairro = trim($this->_action->getRequest()->getPost($prefix."bairro"));
		$idestado = trim((int)$this->_action->getRequest()->getPost($prefix."idestado"));
		$cidade = trim($this->_action->getRequest()->getPost($prefix."cidade"));		
		
		//var_dump($numero);die();
		
		$dados = array(
			'id' => $idendereco,
			'cep' => $cep,
			'endereco' => $endereco,
			'numero' => $numero,
			'complemento' => $complemento,
			'bairro' => $bairro,
			'idestado' => (int)$idestado,
			'cidade' => $cidade,
			'excluido' => 'nao',
		);
		
		$enderecos = new Enderecos();
		$endereco = $enderecos->save($dados);		
		
		return $endereco;
    }

}
