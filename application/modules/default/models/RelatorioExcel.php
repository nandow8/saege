<?php 


class RelatorioExcel {
	
	private $_action;
	private $_rows;
	private $_titles = array();
	
	
	public function __construct($action, $rows) {
		$this->_action = $action;
		$this->_rows = $rows;
		
		foreach ($this->_rows as $k=>$v) {
			$this->_rows[$k] = str_replace('<br />', chr(13).chr(10), $v);
		}
		
		if (sizeof($rows)==0) {
			throw new \Exception('Nenhum registro foi encontrado!');
		}
	}	
	
	public static function renderCells($fields, $action) {
		$s = '';
		$i = 0;
		foreach ($fields as $k=>$v) {
			if ($i>0) $s .= '<br />';
			$s .= $k . ': ' . $action->view->formatTextoBranco($v);
			$i++;
		}
		return $s;
	}	
	
	public function generate($fname = "file.xlsx", $titlesName = array()) {
		$this->getTitlesPerRows($titlesName);
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
		$rowCount = 1;

		foreach ($this->getTitles() as $k=>$v) {				
			$objPHPExcel->getActiveSheet()->SetCellValue($k.$rowCount, $titlesName[$v]['label']);
			$objPHPExcel->getActiveSheet()->getStyle($k.$rowCount)->getFont()->setBold(true);
		}
		
		$rowCount++;
		foreach ($this->getRows() as $k=>$row) {
				foreach ($this->getTitles() as $k=>$v) {
				$value = $row[$v];
				if((isset($titlesName[$v]['type'])) && ($titlesName[$v]['type']=="data")){
					$value = Mn_Util::formPostVarData($row[$v]);
				}						
			
				$objPHPExcel->getActiveSheet()->SetCellValue($k.$rowCount, $value);
				if (strpos($row[$v], chr(13).chr(10))!==false) {
					$objPHPExcel->getActiveSheet()->getColumnDimension($k)->setAutoSize(true);
					$objPHPExcel->getActiveSheet()->getStyle($k.$rowCount)->getAlignment()->setWrapText(true);
				}
				$objPHPExcel->getActiveSheet()->getStyle($k.$rowCount)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
			}			
			$rowCount++;
		}
		

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);

		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="'.$fname.'"');
		$objWriter->save('php://output');		
	}
	
	private function getTitlesPerRows($titlesName) {
		$_titles = array();
		
		$c = 64;
		foreach ($titlesName as $k=>$v) {
			$c++;
			$_titles[chr($c)] = $k;
		}
		
		$this->setTitles($_titles);
	}
	
	public function getAction() {
		return $this->_action;
	}
	public function setAction($_action) {
		$this->_action = $_action;
		return $this;
	}
	public function getRows() {
		return $this->_rows;
	}
	public function setRows($_rows) {
		$this->_rows = $_rows;
		return $this;
	}
	public function getTitles() {
		return $this->_titles;
	}
	public function setTitles($_titles) {
		$this->_titles = $_titles;
		return $this;
	}
	

	
	
	
}