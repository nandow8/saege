﻿<?php

/**
 * Controle da classe de PostsModel
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2014 MN Soluções (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class PostsModel {
	
	/**
	 * Controller
	 * @var Zend_Controller_Action
	 */
	protected $_action = null;
	
	/**
	 * Método construtor
	 * @param Zend_Controller_Action $action
	 */
	public function __construct($action) {
		$this->_action = $action;
	}

	

	
	
	/**
	 * Valida e grava os dados do formulário
	 */
	public function getPost() {
		if (!isset($this->_action->view->post_var)) $this->_action->view->post_var = $_POST; 
		else $this->_action->view->post_var = array_merge($this->_action->view->post_var, $_POST);
		$erros = array();
		$dados = array();
		
		foreach ($_POST as $k=>$v) {
			$_POST[$k] = str_replace("'", "´", $v);
		}
		
		$id = (int)trim($this->_action->getRequest()->getPost("id"));
		$dados['id'] = $id;
		
		$dados['idparent'] = $this->_action->_idparent;
		
		if ((isset($this->_action->_data)) && ($this->_action->_data)) {
			$data = trim($this->_action->getRequest()->getPost("data"));
			if ($data!='') {
				$data = explode("/", $data);
				if (sizeof($data)==3) {
					$data[0] = (int)$data[0];
					$data[1] = (int)$data[1];
					$data[2] = (int)$data[2];
					$data = mktime(0, 0, 0, $data[1], $data[0], $data[2]);
				} else $data = null;
				//if (is_null($data)) array_push($erros, "Selecione uma <b>Data</b>");
				$dados['data'] = date('Y-m-d', $data);
			} 
		}
		
		if ((isset($this->_action->_topico)) && ($this->_action->_topico)) {
			$topico = trim($this->_action->getRequest()->getPost("topico"));
			if ($topico=="") array_push($erros, "Preencha o <b>Tópico</b>");
			$dados['topico'] = $topico;
		}
		
		if ((isset($this->_action->_subtopico)) && ($this->_action->_subtopico)) {
			$subtopico = trim($this->_action->getRequest()->getPost("subtopico"));			
			$dados['subtopico'] = $subtopico;
		}
		
		if ((isset($this->_action->_texto)) && ($this->_action->_texto)) {
			$texto = trim($this->_action->getRequest()->getPost("texto"));
			$dados['texto'] = $texto;
		}
		
		if ((isset($this->_action->_texto2)) && ($this->_action->_texto2)) {
			$texto2 = trim($this->_action->getRequest()->getPost("texto2"));
			$dados['texto2'] = $texto2;
		}	

		if ((isset($this->_action->_tipocustom)) && ($this->_action->_tipocustom)) {
			$tipocustom = trim($this->_action->getRequest()->getPost("tipocustom"));
			$dados['tipocustom'] = $tipocustom;
		}		
		
		if ((isset($this->_action->_imagem)) && ($this->_action->_imagem)) {
			$idimagem = false;
			$imagens = new Imagens();
			try {
				$idimagem = $imagens->getImagemFromForm('imagem');
			} catch (Exception $e) {
				$idimagem = false;
				array_push($erros,$e->getMessage());
			}

			
			$excluir_imagem = trim($this->_action->getRequest()->getPost("excluir_imagem"));
			if ($excluir_imagem=='excluir') $idimagem = -1;
			$dados['idimagem'] = $idimagem;
		}
		
		if ((isset($this->_action->_banner)) && ($this->_action->_banner)) {
			$idbanner = false;
			$imagens = new Imagens();
			try {
				$idbanner = $imagens->getImagemFromForm('banner');
			} catch (Exception $e) {
				$idbanner = false;
				array_push($erros,$e->getMessage());
			}
			
			$excluir_banner = trim($this->_action->getRequest()->getPost("excluir_banner"));
			if ($excluir_banner=='excluir') $idbanner = -1;
			$dados['idbanner'] = $idbanner;
		}
		
		if ((isset($this->_action->_arquivo)) && ($this->_action->_arquivo)) {
			$dados['idarquivo'] = $this->getArquivo();
		}	
		
		if ((isset($this->_action->_video)) && ($this->_action->_video)) {
			$video = trim($this->_action->getRequest()->getPost("video"));
			$videos_excluir = trim($this->_action->getRequest()->getPost("videos_excluir"));
			
			if ($videos_excluir=='sim') $dados['video'] = NULL;
			else {
				if ($video!='') $dados['video'] = $video;
			}
		}
		
		if ((isset($this->_action->_link)) && ($this->_action->_link)) {
			$link = trim($this->_action->getRequest()->getPost("link"));
			$dados['link'] = $link;
		}

		if ((isset($this->_action->_destaque)) && ($this->_action->_destaque)) {
			$destaque = trim($this->_action->getRequest()->getPost("destaque"));
			if ($destaque=="") array_push($erros, "Selecione um <b>Destaque</b>");
			$dados['destaque'] = $destaque;
		}
		
		if ((int)$this->_action->getRequest()->getPost("idcategoria")>0) {
			$dados['idparent'] = (int)$this->_action->getRequest()->getPost("idcategoria");
		}
		
		$status = trim($this->_action->getRequest()->getPost("status1"));
		if ($status=="") array_push($erros, "Selecione um <b>Status</b>");
		$dados['status'] = $status;

		//sugerido POR
		if ($id==0) {
			if ((isset($this->_action->_sugerido)) && ($this->_action->_sugerido)) {
				$dados['sugerido'] = $this->_action->_sugerido;
			}
				
			if ((isset($this->_action->_sugeridopor)) && ($this->_action->_sugeridopor)) {
				$dados['sugeridopor'] = $this->_action->_sugeridopor;
				$dados['status'] = 'Bloqueado';
			}
		}	
		//fim sugerido por	
		
		if ((isset($this->_action->_idusuario)) && ($this->_action->_idusuario)) {
			$idusuario = (int)trim($this->_action->getRequest()->getPost("idusuario"));
			$dados['idusuario'] = $idusuario;
		}		
		
		if (sizeof($erros)>0) return $erros; 
		$dados['excluido'] = 'nao';
		$dados['logusuario'] = $this->_action->_usuario['id'];
		$dados['logdata'] = date('Y-m-d G:i:s');
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {
		
			$posts = new Posts();
			$row = $posts->save($dados);
				
			$db->commit();
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		

		return "";   		
	}
	
    private function getArquivo($filename = 'arquivo') {
		$idarquivo = false;
		$arquivos = new Arquivos();
		try {
			$idarquivo = $arquivos->getArquivoFromForm($filename);
		} catch (Exception $e) {
			$idarquivo = false;
			array_push($erros,$e->getMessage());
		}
		
		$excluir_arquivo = trim($this->_action->getRequest()->getPost("excluir_".$filename));
		if ($excluir_arquivo=='excluir') $idarquivo = -1;
		return $idarquivo;    	
    }  	
		
}