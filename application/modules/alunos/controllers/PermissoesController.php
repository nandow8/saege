<?php
class Alunos_PermissoesController extends Zend_Controller_Action {
	
	public function preDispatch() {
		//TODO Coloar isAccess na view
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->alunousuario)) $this->_redirect("/alunos/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		$this->_usuario = unserialize($loginNameSpace->alunousuario);

		if(Escolasalunos::getUsuario('tipousuario')!="pais"){
			$this->_redirect("/alunos/");
		}
	}	

	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Permissões')
		);
		
		$rows = new Escolasalunos();
		$row = $rows->getEscolaalunoById(Escolasalunos::getUsuario('id'));
		
		$this->view->post_var = $row;
	}	
	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'permissoes',	'name' => 'Permissões'),
			array('url' => null,'name' => 'Editar Registro')
		);	
		//var_dump(Escolasalunos::getUsuario()); die();
		$this->preForm();
		$rows = new Escolasalunos();
		$row = $rows->getEscolaalunoById(Escolasalunos::getUsuario('id'));
		
		$this->view->post_var = $row;
		if ($this->_request->isPost()) {
			$erros = $this->getPost();
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Permissão editada com sucesso.";
			
			$this->_redirect('alunos/' . $this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
    
	private function preForm() 
        {
            //$this->view->idsecretaria = Usuarios::getSecretariaAtiva(Usuarios::getUsuario('id'), 'id');
        }	    
	
    private function getPost() {
		if (!isset($this->view->post_var)) {
			$this->view->post_var = $_POST;
		} else {
			$this->view->post_var = array_merge($this->view->post_var, $_POST);
		}
			
		$id = Escolasalunos::getUsuario('id');
		$perguntasprofessores = trim($this->getRequest()->getPost("perguntasprofessores"));
		$perguntassecretaria = trim($this->getRequest()->getPost("perguntassecretaria"));
		
		
		
		$erros = array();
			
		if ($id==0) array_push($erros, 'Falha crítica.');
		//if (""==$valor) array_push($erros, 'Preencha o campo VALOR.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$configuracoes = new Escolasalunos();
		$dados = array();
		$dados['id'] = $id;
		if (($perguntasprofessores) && (isset($perguntasprofessores))) $dados['perguntasprofessores'] = $perguntasprofessores;
		if (($perguntassecretaria) && (isset($perguntassecretaria))) $dados['perguntassecretaria'] = $perguntassecretaria;

		$dados['logusuario'] = Escolasalunos::getUsuario('id');
		$dados['logdata'] = date('Y-m-d G:i:s');
		
		$configuracoes->save($dados);
				
		return "";    	
    }   	
	
}