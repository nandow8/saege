<?php

/**
 * Controle da classe programacoesaulas do sistema
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Alunos_ProgramacoesaulasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Programacaoaula
	 */
	protected $_cadastro = null;	
	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->alunousuario)) $this->_redirect("/alunos/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		$this->_usuario = unserialize($loginNameSpace->alunousuario);
	}

	/**
	 *
	 * Action de edição de perfil de acesso
	 */
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'programacoesaulas', 'name' => 'Planos de Aulas'),
			array('url' => null,'name' => 'Visualizar Plano')
		);
		
		$id = (int)$this->_request->getParam("id");
		$programacoesaulas = new Programacoesaulas();
		$programacaoaula = $programacoesaulas->getProgramacaoaulaById($id, array());
		
		if (!$programacaoaula) 
			$this->_redirect($this->getRequest()->getControllerName());
		
		$this->view->post_var = $programacaoaula;
		$this->preForm();
	
		$this->view->visualizar = true;
		return true;
	}
	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Planos de Aulas')
		);
		
		$ns = new Zend_Session_Namespace('default_programacoesaulas');
		$programacoesaulas = new Programacoesaulas();
		$queries = array();	
		$queries = array();	
		$queries['idescola'] = Escolasalunos::getUsuario('idescola');
		$queries['idserie'] = Escolasalunos::getUsuario('idserie');
		$queries['publico'] = "Sim";
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect($this->getRequest()->getControllerName());
    		die();	
    	}
    	
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		
			if ($this->view->post_var["idserie"]!="") $queries["idserie"] = $this->view->post_var["idserie"];
if ($this->view->post_var["idturma"]!="") $queries["idturma"] = $this->view->post_var["idturma"];
if ($this->view->post_var["tema"]!="") $queries["tema"] = $this->view->post_var["tema"];
if ($this->view->post_var["objetivo"]!="") $queries["objetivo"] = $this->view->post_var["objetivo"];
if ($this->view->post_var["status1"]!="") $queries["status"] = $this->view->post_var["status1"];
    		
    		if ($this->view->post_var['sorting']!='') $queries['sorting'] = $this->view->post_var['sorting'];
    	}		
		
		//PAGINACAO
    	$maxpp = 20;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $programacoesaulas->getProgramacoesaulas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $programacoesaulas->getProgramacoesaulas($queries, $paginaAtual, $maxpp);	
	}
	 
    /**
     * Atribui valores ao view
     * @param int $idprogramacaoaula
     */    
    private function preForm($idprogramacaoaula = 0) {
    }    
  
    
    private function getArquivo($filename) {
    	$idarquivo = false;
    	$arquivos = new Arquivos();
    	 
    	try {
    		$idarquivo = $arquivos->getArquivoFromForm($filename);
    	} catch (Exception $e) {
    		$idarquivo = false;
    		array_push($erros,$e->getMessage());
    	}
    
    	$excluir_arquivo = trim($this->getRequest()->getPost("excluir_".$filename));
    	 
    	if ($excluir_arquivo=='excluir') $idarquivo = -1;

    	return $idarquivo;
    }    
    
    private function getImagem($imagem, $apenas_copia = false) {
    	$idimagem = false;
    	$imagens = new Imagens();
    	
    	try {
    		ini_set('memory_limit', '-1');
    		$idimagem = $imagens->getImagemFromForm($imagem, NULL, NULL, $apenas_copia);
    
    	} catch (Exception $e) {
    		$idimagem = false;
    		array_push($erros,$e->getMessage());
    	}
    	
    	$excluir_imagem = trim($this->getRequest()->getPost("excluir_" . $imagem));
    	if ($excluir_imagem=='excluir_'  . $imagem) $idimagem = -1;
    	return $idimagem;
    }
    
}