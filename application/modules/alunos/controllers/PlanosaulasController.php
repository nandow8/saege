<?php

class Alunos_PlanosaulasController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var programacoesaulas
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		if ($this->getRequest()->getActionName()=='buscacepxml') return true;		
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->alunousuario)) $this->_redirect("/alunos/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		$this->_usuario = unserialize($loginNameSpace->alunousuario);	
	}	
	
	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Planos de aulas')
		);
		
		
		$ns = new Zend_Session_Namespace('alunos_programacoesaulas');
		$programacoesaulas = new Escolasprogramacoesaulas();
		$queries = array();	
		$queries['idescola'] = Escolasalunos::getEscolaAtiva(Escolasalunos::getUsuario('id'), 'id');		
		$queries['publico'] = "Sim";
		
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ((isset($queries['status'])) && ($this->view->post_var['status']!='')) $queries['status'] = $this->view->post_var['status'];
    		if ((isset($this->view->post_var['chave'])) && ($this->view->post_var['chave']!='')) $queries['chave'] = $this->view->post_var['chave'];
    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $programacoesaulas->getProgramacoes($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $programacoesaulas->getProgramacoes($queries, $paginaAtual, $maxpp);	
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'programacoesaulas',	'name' => 'Planos de aulas'),
			array('url' => null,'name' => 'Visualizar plano')
		);	

		$id = (int)$this->_request->getParam("id");
		$programacoesaulas = new Escolasprogramacoesaulas();
		$programacao = $programacoesaulas->getProgramacaoById($id, array('publico'=>'Sim'));
		
		if (!$programacao) 
			$this->_redirect('alunos/' . $this->getRequest()->getControllerName());
			
		$salas = new Escolassalas();
    	$this->view->salas = $salas->getSalas(array('idescola'=>$programacao['idescola'], 'status'=>'Ativo')); 
		
		$series = new Escolasseries();
    	$this->view->series = $series->getSeries(array('idescola'=>$programacao['idescola'], 'status'=>'Ativo'));

		$turmas = new Escolasturmas();
    	$this->view->turmas = $turmas->getTurmas(array('idescola'=>$programacao['idescola'], 'status'=>'Ativo')); 		
		
		$this->view->post_var = $programacao;
		
		$this->preForm();
		$this->view->visualizar = true;		
	}	
    
    /**
     * Atribui valores ao view
     * @param int $idprogramacoesaulas
     */    
    private function preForm($idprogramacoesaulas = 0) {

    }  
}