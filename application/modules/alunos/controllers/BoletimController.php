<?php

class Alunos_BoletimController extends Zend_Controller_Action {
	
	protected $_usuario = null;
	
	public function preDispatch() {		
		if ($this->getRequest()->getActionName()=='buscacepxml') return true;		
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->alunousuario)) $this->_redirect("/alunos/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		$this->_usuario = unserialize($loginNameSpace->alunousuario);
	}
	
	public function indexAction() {
		//$notas = Controlealunos_Notas::getIdByIdNotasAlunoMateriaHelper((int)$this->idmateria, (int)$row['idaluno'], array('bimestre'=>(int)$this->bimestre, 'idprofessor'=>Escolasusuarios::getUsuario('id')));
		$notas = new Controlealunos_Notas();
		$rows = $notas->getNotas(array('idaluno'=>Escolasalunos::getUsuario('id'), 'order'=>'GROUP BY pf1.bimestre ASC'));
		
		$this->view->rows = $rows;
	}
}