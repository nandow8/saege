<?php

class Alunos_PerguntassecretariaController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Pacotepergunta
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		//TODO Coloar isAccess na view
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->alunousuario)) $this->_redirect("/alunos/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		$this->_usuario = unserialize($loginNameSpace->alunousuario);

		//var_dump(Escolasalunos::getUsuario('perguntasprofessores')); die();
		if(Escolasalunos::getUsuario('perguntasprofessores')!="Sim"){
			$this->_redirect("/alunos/");
		}
	}		

	/**
     * Listagem
     */
	public function indexAction() {
		$this->view->bread_crumb = array(
			array('url' => false,'name' => 'Perguntas')
		);
		
		
		$ns = new Zend_Session_Namespace('default_perguntas_secretaria');
		$perguntas = new Perguntas();
		$queries = array();	
		$queries['diferent_idescolasusuario'] = true;
		$queries['idescolaaluno'] = Escolasalunos::getUsuario('id');
		//PESQUISA
    	if ($this->getRequest()->isPost()) {
    		$ns->pesquisa = serialize($_POST);
    		$this->_redirect('/' . $this->getRequest()->getControllerName());
    		die();	
    	}
    	if (isset($ns->pesquisa)) $this->view->post_var = unserialize($ns->pesquisa);
    	
    	if (isset($this->view->post_var)) {	
    		
    		foreach ($this->view->post_var as $k=>$v) $this->view->post_var[$k] = trim($v);
    		if ($this->view->post_var['status']!='') $queries['status'] = $this->view->post_var['status'];
    		if ($this->view->post_var['chave']!='') $queries['chave'] = $this->view->post_var['chave'];
    		
    	}		
		
		//PAGINACAO
    	$maxpp = 15;
		
    	$paginaAtual = (int)$this->getRequest()->getParam('p');
		if ($paginaAtual>0) $ns->paginaAtual = $paginaAtual;
		$paginaAtual = isset($ns->paginaAtual) ? (int)$ns->paginaAtual : 1;
		if ($paginaAtual==0) $paginaAtual = 1;
		
		$queries['total'] = true;
		$totalRegistros = $perguntas->getPerguntas($queries);
		$paginaTotal = ceil($totalRegistros/$maxpp);
		$queries['total'] = false;
		if ($paginaAtual>$paginaTotal) $paginaAtual = $paginaTotal;
		$paginaAtual--;
		if ($paginaAtual<0) $paginaAtual = 0;
		
		$this->view->pagina_atual = $paginaAtual+1;
		$this->view->maxpp = $maxpp;
		$this->view->total_registros = $totalRegistros;
		$this->view->pagina_total = $paginaTotal;    		
		
		$this->view->rows = $perguntas->getPerguntas($queries, $paginaAtual, $maxpp);	
	}
	
	public function visualizarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'perguntassecretaria',	'name' => 'Perguntas'),
			array('url' => null,'name' => 'Visualizar pergunta')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$perguntas = new Perguntas();
		$pergunta = $perguntas->getPerguntaById($id);
		
		if (!$pergunta) 
			$this->_redirect('alunos/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $pergunta;
		$this->preForm($id);
		$this->view->visualizar = true;		
	}
	
	/**
	 * 
	 * Action de edição de pergunta
	 */	
	public function editarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'perguntassecretaria',	'name' => 'Perguntas'),
			array('url' => null,'name' => 'Editar pergunta')
		);	
				
		$id = (int)$this->_request->getParam("id");
		$perguntas = new Perguntas();
		$pergunta = $perguntas->getPerguntaById($id);
		
		if (!$pergunta) 
			$this->_redirect('alunos/' . $this->getRequest()->getControllerName());
		
		
		$this->view->post_var = $pergunta;
		
		$historicos = new Perguntashistoricos();
		$rows = false;
		if($id > 0){
			$rows = $historicos->getPerguntas(array('identidade'=>$id, 'order'=>'ORDER BY p1.logdata DESC'));
		}
		
		$this->view->historicos = $rows;		
		
		$this->preForm($id);
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost($pergunta);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Pergunta editada com sucesso.";
			
			$this->_redirect('alunos/'.$this->getRequest()->getControllerName());	
		}		
		return true;		
    }  		
	
	/**
	 * 
	 * Action de adição de perguntas 
	 */
	public function adicionarAction() {
		$this->view->bread_crumb = array(
			array('url' => 'perguntassecretaria',	'name' => 'Perguntas'),
			array('url' => null,'name' => 'Adicionar pergunta')
		);	
				
		$this->preForm();
		if ($this->getRequest()->isPost()) {
			$erros = $this->getPost(false);
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Pergunta adicionada com sucesso.";
			
			$this->_redirect('alunos/'.$this->getRequest()->getControllerName());	
		}
		return true;		
    }	
    
    /**
     * Atribui valores ao view
     * @param int $idpergunta
     */    
    private function preForm($id = 0) {
    	$departamentos = new Departamentos();
    	$this->view->departamentos = $departamentos->getDepartamentos(array('idescola'=>Escolasalunos::getUsuario('idescola'), 'perguntas'=>'Sim'));
    	
		$historicos = new Perguntashistoricos();
		$rows = false;
		if($id > 0){
			$rows = $historicos->getPerguntas(array('identidade'=>$id, 'order'=>'ORDER BY p1.logdata DESC'));
		}
		
		$this->view->historicos = $rows;
    }    
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost($_registro = false) {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = (int)$this->getRequest()->getPost("id");
		$origem = strip_tags(trim($this->getRequest()->getPost("origem")));
		$destinatario = Perguntas::$_DESTINATORIO_SECRETARIA;
		
		$idescolaaluno = strip_tags(trim($this->getRequest()->getPost("idescolaaluno")));
		
		$idescolasusuario = strip_tags(trim($this->getRequest()->getPost("idescolasusuario")));		
		$idsecretaria = strip_tags(trim($this->getRequest()->getPost("idsecretaria")));
		
		$pergunta = strip_tags(trim($this->getRequest()->getPost("pergunta")));
		$texto = strip_tags(trim($this->getRequest()->getPost("texto")));
		$resposta = strip_tags(trim($this->getRequest()->getPost("resposta")));
		
		$status = strip_tags(trim($this->getRequest()->getPost("status")));
		
		$erros = array();

		if (""==$pergunta) array_push($erros, 'Preencha o campo PERGUNTA.');
		//if ((""==$status) && ($id>0)) array_push($erros, 'Selecione um STATUS.');
		
		$perguntas = new Perguntas();
		//$row = $perguntas->fetchRow("excluido='nao' AND pergunta='$pergunta' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe uma ESCOLA com esse NOME.');
		
		if (sizeof($erros)>0) return $erros; 
		
		$db = Zend_Registry::get('db');
		$db->beginTransaction();
		try {				
			$dados = array();
			$dados['id'] = $id;
			if($id <= 0)$dados['origem'] = Perguntas::$_ORIGEM_ALUNO;			
			$dados['destinatario'] = $destinatario;
			$dados['idescolaaluno'] = Escolasalunos::getUsuario('id');
			$dados['idescolasusuario'] = $idescolasusuario;
			$dados['idsecretaria'] = $idsecretaria;
			$dados['pergunta'] = $pergunta;
			$dados['texto'] = $texto;
			$dados['resposta'] = $resposta;
			
			if($id <= 0)$dados['status'] = Perguntas::$_STATUS_ABERTO;
			$dados['modulo'] = 'aluno';
			$dados['excluido'] = 'nao';
			$dados['logescolaaluno'] = Escolasusuarios::getUsuario('id');
			$dados['logdata'] = date('Y-m-d G:i:s');
			$row = $perguntas->save($dados);
			
			$db->commit();
			
		} catch (Exception $e) {
			echo $e->getMessage();
			$db->rollBack();
			die();
		}		
		



		
		return "";    	
    }
}