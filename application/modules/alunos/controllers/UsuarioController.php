<?php

class Alunos_UsuarioController extends Zend_Controller_Action {
	
	/**
	 * Propriedade protegida que contem os dados do usário logado
	 * @var Usuario
	 */
	protected $_usuario = null;	
	
	/**
     * Verificação de permissao de acesso
     */	
	public function preDispatch() {
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->alunousuario)) $this->_redirect("/alunos/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		$this->_usuario = unserialize($loginNameSpace->alunousuario);
		
		$messageNameSpace = new Zend_Session_Namespace("message");
		if ($messageNameSpace->crudmessage) {
			$this->view->crudMessage = $messageNameSpace->crudmessage;
			unset($messageNameSpace->crudmessage);
		}				
	}		
	
	public function indexAction() {
		$this->_redirect('/alunos/usuario/meusdados');
	}
	
	
	/**
	 * 
	 * Action de edição de usuários
	 */	
	public function meusdadosAction() {
		$this->view->bread_crumb = array(
			array('url' => null,'name' => 'Meus Dados')
		);	
				
		$id = Escolasalunos::getUsuario('id');
		$usuarios = new Escolasalunos();
		$usuario = $usuarios->fetchRow("id=".$id." AND excluido='nao'");
		
		if (!$usuario) 
			$this->_redirect($this->getRequest()->getControllerName());	
		
		
		$this->view->post_var = $usuario->toArray();
		$this->preForm();
		
		if ($this->_request->isPost()) {
			$erros = $this->getPost();
			if ($erros!="") {
				$this->view->erros = $erros;
				return false; 
			}
			
			$message = new Zend_Session_Namespace("message");
			$message->crudmessage = "Usuário editado com sucesso.";
			
			$this->_redirect('/alunos/'.$this->getRequest()->getControllerName() . '/meusdados');	
		}		
		return true;		
    }  		
    
    /**
     * Atribui valores ao view
     * @param int $idusuario
     */    
    private function preForm($idusuario = 0) {
    }       
    
	/**
	 * Valida e grava os dados do formulário
	 */    
    private function getPost() {
		if (!isset($this->view->post_var)) $this->view->post_var = $_POST; 
		else $this->view->post_var = array_merge($this->view->post_var, $_POST);
		
		$id = Escolasalunos::getUsuario('id');
		$senha = strip_tags(trim($this->getRequest()->getPost("senha")));
		
		$erros = array();
			
		if (($id==0) && (""==$senha)) array_push($erros, 'Preencha o campo SENHA.');
		
		$usuarios = new Escolasalunos();
		//$row = $usuarios->fetchRow("excluido='nao' AND rgm='$rgm' AND id<>".$id);
		//if ($row) array_push($erros, 'Já existe usuário com esse RGM DE ACESSO.');
			
		if (sizeof($erros)>0) return $erros; 
		
		$dados = array(
			'id' => $id,			
			'excluido' => 'nao',
			'logusuario' => $this->_usuario['id'],
			'logdata' => date('Y-m-d G:i:s'),
		);
		if ($senha!='') $dados['senha'] = $senha;
					
		$usuarios->save($dados);
				
		return "";
    }        

}