<?php

/**
 * Controle da classe Index
 *
 * @author		Thales Gabriel		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2014 MN Soluções
 * @version     1.0
 */
class Alunos_IndexController extends Zend_Controller_Action {
	
	protected $_usuario = null;
	
	public function preDispatch() {		
		if ($this->getRequest()->getActionName()=='buscacepxml') return true;		
		$loginNameSpace = new Zend_Session_Namespace(Mn_Util::getAdminNameSpace());
		if(!isset($loginNameSpace->alunousuario)) $this->_redirect("/alunos/auth/login?redirectUrl=" . Mn_Util::setMVCReturnUrl($this->getRequest()->getParams()));
		
		$this->_usuario = unserialize($loginNameSpace->alunousuario);
	}
	
	public function indexAction() {
		/*return;
		$solicitacoes = new Solicitacoes();
		$queries = array();
		$queries['idusuarioemissor'] = $this->_usuario['id'];
		$queries['idescola'] = Escolasalunos::getEscolaAtiva(Escolasalunos::getUsuario('id'), 'id');
		$queries['idsecretaria'] = Escolasalunos::getEscolaAtiva(Escolasalunos::getUsuario('id'), 'idsecretaria');
		//PAGINACAO
    	$maxpp = 5;		
		$this->view->solicitacoes = $solicitacoes->getSolicitacoes($queries, '0', $maxpp);	
		*/
		$maxpp = 5;	
		$queries = array();	
		$queries['idescola'] = Escolasalunos::getEscolaAtiva(Escolasalunos::getUsuario('id'), 'id');
		$queries['idsecretaria'] = Escolasalunos::getEscolaAtiva(Escolasalunos::getUsuario('id'), 'idsecretaria');
		
		//$comunicados = new Comunicacao_Comunicados();
		$this->view->comunicados = array();//$comunicados->getComunicados($queries, '0', $maxpp);
		
	}
	
    public function setgraficoschamadostecnicosAction(){
    	$this->_helper->layout->disableLayout();
    	
		$chamadostecnicos = new Ti_Chamados();
		$querieschamadostecnicos = array();	
		$querieschamadostecnicos['idescola'] = Escolasalunos::getEscolaAtiva(Escolasalunos::getUsuario('id'), 'id');
		$querieschamadostecnicos['idsecretaria'] = Escolasalunos::getEscolaAtiva(Escolasalunos::getUsuario('id'), 'idsecretaria');
		
		$querieschamadostecnicos['total'] = true;
		$querieschamadostecnicos['status'] = Ti_Chamados::$_STATUS_ABERTO;
		$this->view->chamadostecnicosabertos = $chamadostecnicos->getChamados($querieschamadostecnicos);;
		
		$querieschamadostecnicos['status'] = Ti_Chamados::$_STATUS_PENDENTE;
		$this->view->chamadostecnicospendente = $chamadostecnicos->getChamados($querieschamadostecnicos);
		
		$querieschamadostecnicos['status'] = Ti_Chamados::$_STATUS_ENCERRADO;
		$this->view->chamadostecnicosencerrado = $chamadostecnicos->getChamados($querieschamadostecnicos);
		
		$querieschamadostecnicos['status'] = Ti_Chamados::$_STATUS_CANCELADO;
		$this->view->chamadostecnicoscancelado = $chamadostecnicos->getChamados($querieschamadostecnicos);
    }
    
    public function setgraficossolicitacoesAction(){
    	$this->_helper->layout->disableLayout();
    	
		$solicitacoes = new Solicitacoes();
		$queries_solicitacoes = array();
		$queries_solicitacoes['order'] = "ORDER BY s1.id DESC";
		$queries_solicitacoes['idescola'] = Escolasalunos::getEscolaAtiva(Escolasalunos::getUsuario('id'), 'id');
		$queries_solicitacoes['idsecretaria'] = Escolasalunos::getEscolaAtiva(Escolasalunos::getUsuario('id'), 'idsecretaria');
		if($this->_usuario['recebesolicitacoes']!="Sim"):
			$queries_solicitacoes['idusuarioretorno_todos'] = $this->_usuario['id'];
		endif;		
		
		$queries_solicitacoes['total'] = true;
		
		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_PENDENTE;
		$this->view->solicitacoespendente = $solicitacoes->getSolicitacoes($queries_solicitacoes);

		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_EXECUCAO;
		$this->view->solicitacoesexecucao = $solicitacoes->getSolicitacoes($queries_solicitacoes);
		
		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_FINALIZADO;
		$this->view->solicitacoesencerrado = $solicitacoes->getSolicitacoes($queries_solicitacoes);
		
		$queries_solicitacoes['status'] = Solicitacoes::$_STATUS_CANCELADO;
		$this->view->solicitacoescancelado = $solicitacoes->getSolicitacoes($queries_solicitacoes);
    }
	
	public function buscacepxmlAction() {
		$this->_response->setHeader("content-type", "text/xml");
	
		//error_reporting(0);
		$cep = $this->getRequest()->getPost('cep');
		try {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,"cepDest=$cep");
			curl_setopt($ch, CURLOPT_URL, "http://mnsolucoes.com.br/cep/index.php");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$output = curl_exec($ch);
			curl_close($ch);
			 
			if (!strpos($output, 'Calculo')) {
				throw new Exception('Não foi possível realizar a consulta!');
			}
				
			$output = utf8_encode($output);
			$output = str_replace('ISO-8859-1', 'UTF-8', $output);
				
			$xml =  simplexml_load_string(trim($output));
			$output = $xml->asXML();
	
				
		} catch (Exception $e) {
			echo '000@Não foi possível realizar a consulta!';
			die();
		}
	
		$endereco = $xml->Endereco;
		$bairro = $xml->Bairro;
		$cidade = $xml->Cidade;
		$uf = $xml->UF;
	
		$idestado = 0;
		$nomeestado = "";
		$idcidade = 0;
		$estados = new Estados();
		$estado = $estados->fetchRow("uf='$uf'");

		if ($estado) $idestado = $estado['id'];
		if ($estado) $nomeestado = $estado['nome'];
		if ($estado){
			$cidades = new Cidades();
			$row_cidade = $cidades->fetchRow("idestado=".$estado['id']." AND nome LIKE '%$cidade%'");
			if($row_cidade) $idcidade = $row_cidade['id'];
		}
	
	
		$res = "001@$endereco@$cidade@$uf@$idestado@$bairro@$nomeestado@$idcidade";
	
		echo $res;
		die();
	}	
	
	public function fileuploadAction() {
		
		error_reporting(E_ALL | E_STRICT);
	
		$upload_handler = new UploadHandler();
		
		header('Pragma: no-cache');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Content-Disposition: inline; filename="files.json"');
		header('X-Content-Type-Options: nosniff');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, DELETE');
		header('Access-Control-Allow-Headers: X-File-Name, X-File-Type, X-File-Size');
		
		switch ($_SERVER['REQUEST_METHOD']) {
		    case 'OPTIONS':
		        break;
		    case 'HEAD':
		    case 'GET':
		        $upload_handler->get();
		        break;
		    case 'POST':
		        if (isset($_REQUEST['_method']) && $_REQUEST['_method'] === 'DELETE') {
		            $upload_handler->delete();
		        } else {
		           // $upload_handler->post();

		        	
					$arquivos = new Arquivos();
					$idarquivo = $arquivos->getArquivoFromFormsByPos('files', 0);
		        
					$result = array();
					$result['idarquivo'] = $idarquivo;
					$result['filename'] = $_FILES['files']["name"][0];
					$result['idx'] = (int)$_POST['idx'];
					
					echo '[' . json_encode ($result) . ']';		        	
		        	
		        	
		        }
		        break;
		    case 'DELETE':
		        $upload_handler->delete();
		        break;
		    default:
		        header('HTTP/1.1 405 Method Not Allowed');
		}		
		die();
	}
	
    public function changeescolaAction() {
            $id = (int)$this->getRequest()->getPost('id');

            $escolasusuarios = new Escolasalunos();
            $usuario = $escolasusuarios->fetchRow("id=" . Escolasalunos::getUsuario('id'));

            $usuario = $usuario->toArray();

            $usuario['idescola'] = $id;
            $escolasusuarios->save($usuario);

            die('OK');
    }
}