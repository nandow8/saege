<?php

/**
 * Bootstrap
 *
 * @author		Alexandre Martin Narciso		
 * @uses        Zend_Controller_Action
 * @copyright   Copyright (c) 2011 MN Solucoes. (http://www.mnsolucoes.com.br)
 * @version     1.0
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
	
	/**
	 * Override da função init para rotear o permlink dos usuários
	 */
	protected function _initRouter() {
        $rota_cliente = new Zend_Controller_Router_Route(
		    'escola/:alias/*',
		    array(
		    	'module'=>'default', 
		    	'controller'=>'auth', 
		    	'action' => 'login')
		);	

		$this->bootstrap('frontController');
    	$router = $this->getResource('frontController')
                   ->getRouter();		
                   
		$router->addRoute('escola', $rota_cliente); 
		
		$this->initHelpers();

	}	
	
    /**
     * Initialize action helpers
     * 
     * @return void
     */
    public function initHelpers() {
    	$path = realpath(APPLICATION_PATH . '/../library/Mn/Controller/Action/Helper');

		Zend_Controller_Action_HelperBroker::addPath($path);    	
    	Zend_Controller_Action_HelperBroker::addPrefix('Mn_Controller_Action_Helper');

		$hooks = Zend_Controller_Action_HelperBroker::getStaticHelper('Prerequest');
        Zend_Controller_Action_HelperBroker::addHelper($hooks);    	
    }	

	
}

